﻿Module PruebasFTP


    Public Sub ChilkatExample()

            Dim ftp As New Chilkat.Ftp2
            Dim success As Boolean = ftp.UnlockComponent("SMANRIFTP_keysbDt3oHn3")
            If (success <> True) Then
                Console.WriteLine(ftp.LastErrorText)
                Exit Sub
            End If

            ftp.Hostname = "209.172.62.232"
            ftp.Username = "gera"
            ftp.Password = "y8Shy7$3"

            '  Connect and login to the FTP server.
            success = ftp.Connect()
            If (success <> True) Then
                Console.WriteLine(ftp.LastErrorText)
                Exit Sub
            End If

            '  Change to the remote directory where the file is located.
            '  This step is only necessary if the file is not in the root directory
            '  for the FTP account.
            success = ftp.ChangeRemoteDir("Revendedoras")
            If (success <> True) Then
                Console.WriteLine(ftp.LastErrorText)
                Exit Sub
            End If

            Dim localFilename As String = "c:/temp/archivo4.csv"
            Dim remoteFilename As String = "Consulta_Q554_P364154_20180412042633520.csv"

            '  Download a file.
            success = ftp.GetFile(remoteFilename, localFilename)
            If (success <> True) Then
                Console.WriteLine(ftp.LastErrorText)
                Exit Sub
            End If

            success = ftp.Disconnect()
            Debug.WriteLine("File Uploaded!")

        End Sub


End Module
