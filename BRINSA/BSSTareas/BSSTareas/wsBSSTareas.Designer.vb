﻿Imports System.ServiceProcess

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class wsBSSTareas
    Inherits System.ServiceProcess.ServiceBase

    'UserService reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    ' Punto de entrada principal del proceso
    <MTAThread()>
    <System.Diagnostics.DebuggerNonUserCode()>
    Shared Sub Main()

        If (Environment.UserInteractive) Then
            Dim PruebaTareas As New wsBSSTareas
            Console.WriteLine("Inicia Servicio Consola")
            PruebaTareas.revisaTareas()
        Else
            Dim ServicesToRun() As System.ServiceProcess.ServiceBase

            ' Puede que más de un servicio de NT se ejecute con el mismo proceso. Para agregar
            ' otro servicio a este proceso, cambie la siguiente línea para
            ' crear un segundo objeto de servicio. Por ejemplo,
            '
            '   ServicesToRun = New System.ServiceProcess.ServiceBase () {New Service1, New MySecondUserService}
            '
            ServicesToRun = New System.ServiceProcess.ServiceBase() {New wsBSSTareas}

            System.ServiceProcess.ServiceBase.Run(ServicesToRun)
        End If
    End Sub

    'Requerido por el Diseñador de componentes
    Private components As System.ComponentModel.IContainer

    ' NOTA: el Diseñador de componentes requiere el siguiente procedimiento
    ' Se puede modificar usando el Diseñador de componentes.
    ' No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.EventLog1 = New System.Diagnostics.EventLog()
        CType(Me.EventLog1, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'EventLog1
        '
        Me.EventLog1.Log = "Application"
        Me.EventLog1.Source = "BSSTareas"
        '
        'wsBSSTareas
        '
        Me.ServiceName = "wsBSSTareas"
        CType(Me.EventLog1, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

    Friend WithEvents Timer1 As Windows.Forms.Timer
    Friend WithEvents EventLog1 As EventLog
End Class
