﻿Imports System
Imports System.IO
Imports System.Text

Module InterfaceMP2
    Public DB_DB2 As ADODB.Connection
    Public DB_mp2 As ADODB.Connection
    Public DB_DB2_Param As String
    Public DB_mp2_Param As String
    Public sLib As String
    Public gsConnString As String
    Public SessionId As Double
    Public OrdenesOK As String
    Public TransaccOK As Boolean

    Public Sub Proceso()
        Dim bOK As Boolean = False
        Dim qVersion As String

        qVersion = "(" & My.Application.Info.Version.Major & "." & _
                        My.Application.Info.Version.Minor & "." & _
                        My.Application.Info.Version.Revision & ")"

        Console.WriteLine("INTERFACE MP2-LX")
        Console.WriteLine("Version " & qVersion)

        If Not CargaPrm() Then
            End
        End If
        SessionId = CalcConsec("MP2SESSION", False, "99999999")
        Console.WriteLine("Sesion " & SessionId)


        RegistroT = ""

        '****************
        Select Case CheckVer("MP2-BPCS")
            Case ChkVerApp.AppDeshabilitada
                wrtLog("2", "Aplicacion Deshabilitada. " & qVersion)
            Case ChkVerApp.AppSeparadorDecimal
                wrtLog("2", "Configuracion Decimal debe tener punto como separador." & qVersion)
            Case ChkVerApp.AppVersionIncorrect
                wrtLog("2", "Version Incorrecta del Programa." & qVersion)
                'Case ChkVerApp.AppEjecutando
                '    wrtLog("2", "El Programa ya esta en ejecucion." & qVersion)
            Case ChkVerApp.AppOK
                bOK = True
        End Select

        'bOK = True
        If bOK Then
            Try
                AppEjecutando("MP2-BPCS", 2)
                Console.WriteLine("Inicio Proceso - Ordenes")

                'PruebaNota()
                'Actualiza la fecha de las ordenes de compra de almacen generadas por Excel OCALMAC
                'OrdenesAlmacen()

                'Actualizar en BPCS el HPO para que las ordenes de servicio se autoreciban

                OrdenesServicio()
                OrdenesEliminadas()
                CargaOrdenes()

                RegistroT = "Ordenes Enviadas"
                If OrdenesOK <> "" Then
                    wrtLog("0", OrdenesOK)
                End If
                CargaTransacciones()
                AppEjecutando("MP2-BPCS", 1)
            Catch ex As Exception
                WrtSqlError(lastSQL, ex.ToString)
            End Try

        End If

        DB_DB2.Close()
        DB_MP2.Close()
        '        Console.WriteLine("Presione una ecla.")
        '       Console.ReadKey()
    End Sub

    Function CargaPrm() As Boolean
        Dim gsProvider As String
        Dim gsDatasource As String
        Dim rs As New ADODB.Recordset
        Dim mp2Param(5) As String
        Dim AS400Param(5) As String
        Dim Resultado As Boolean = True
        Dim i As Integer

        gsProvider = "IBMDA400"
        gsDatasource = My.Settings.AS400
        sLib = My.Settings.AMBIENTE
        gsLIB = sLib

        gsConnString = "Provider=" & gsProvider & ";Data Source=" & gsDatasource & ";"
        gsConnString = gsConnString & "Persist Security Info=False;Default Collection=" & sLib & ";"
        gsConnString = gsConnString & "Password=LXAPL;User ID=APLLX;"

        DB_DB2_Param = gsDatasource & "/" & sLib

        DB_DB2 = New ADODB.Connection
        DB_MP2 = New ADODB.Connection
        DB_DB2.Open(gsConnString)
        Console.WriteLine("AMBIENTE: " & sLib)

        rs.Open(" SELECT * FROM " & sLib & ".ZCC WHERE CCTABL = 'LXLONG'", DB_DB2)
        Do While Not rs.EOF
            Select Case rs("CCCODE").Value
                Case "LXUSER"
                    AS400Param(0) = rs("CCDESC").Value
                Case "LXPASS"
                    AS400Param(1) = rs("CCDESC").Value
                Case "MP2SERV"
                    mp2Param(0) = rs("CCDESC").Value
                Case "MP2CATALOG"
                    mp2Param(1) = rs("CCDESC").Value
                Case "MP2USER"
                    mp2Param(2) = rs("CCDESC").Value
                Case "MP2PASS"
                    mp2Param(3) = rs("CCDESC").Value
            End Select
            rs.MoveNext()
        Loop
        rs.Close()

        For i = 0 To 1
            If AS400Param(i) = "" Then
                Resultado = False
            End If
        Next

        gsConnString = "Provider=" & gsProvider & ";Data Source=" & gsDatasource & ";"
        gsConnString = gsConnString & "Persist Security Info=False;Default Collection=" & sLib & ";"
        gsConnString = gsConnString & "Password=" & AS400Param(1) & ";User ID=" & AS400Param(0) & ";"
        DB_DB2.Close()
        If Resultado Then
            DB_DB2.Open(gsConnString)
            Console.WriteLine("Conexion LX OK")
        Else
            Console.WriteLine("Conexion LX Falló")
        End If


        For i = 0 To 3
            If mp2Param(i) = "" Then
                Resultado = False
            End If
        Next
        If Resultado Then
            DB_MP2_Param = mp2Param(0) & "/" & mp2Param(1)
            DB_MP2.ConnectionTimeout = 0
            DB_MP2.Open("Provider=SQLOLEDB.1;Data Source=" & mp2Param(0) & ";Initial Catalog=" & mp2Param(1) & ";User Id=" & mp2Param(2) & ";Password=" & mp2Param(3) & ";")
            Console.WriteLine("Conexion MP2 OK")
        Else
            Console.WriteLine("Conexion MP2 Falló.")
        End If

        Return Resultado

    End Function

    Sub wrtLog(ByVal estado As String, ByVal mensaje As String)
        Dim r As Double
        Dim msg As String = Left(mensaje & "<br>" & String.Format(Now(), "MMdd HHmm"), 600)
        fSql = "UPDATE inputlog SET mensaje_t='" & msg & "', sessionId = " + SessionId.ToString()
        fSql += " WHERE registro_t = '" + RegistroT + "'"
        DB_MP2.Execute(fSql, r)
        If r = 0 Then
            fSql = " INSERT INTO inputlog("
            vSql = " VALUES( "
            mkSql(tDato.tStr, "estado_t  ", estado)
            mkSql(tDato.tStr, "registro_t", RegistroT)
            mkSql(tDato.tStr, "mensaje_t ", msg)
            mkSql(tDato.tStr, "sessionId ", SessionId, True)
            DB_MP2.Execute(fSql + vSql)
        End If
        Console.WriteLine(mensaje)

    End Sub

    Public Sub wrtLog2(ByVal txt As String)
        Dim fEntrada As String = ""
    
        Dim appPath As String = CurDir()

        fEntrada = appPath & "\mp2_lx.txt"
        Try
            Using w As StreamWriter = File.AppendText(fEntrada)
                Log(txt, w)
                w.Close()
            End Using
        Catch E As Exception
            'msgbox E.Message
        End Try

    End Sub

    Sub Log(ByVal logMessage As String, ByVal w As TextWriter)
        w.Write(ControlChars.CrLf & "Log Entry : ")
        w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString())
        w.WriteLine("  :{0}", logMessage)
        w.WriteLine("--------------------------------------------------------------------------")
        ' Update the underlying file.
        w.Flush()
    End Sub

End Module
