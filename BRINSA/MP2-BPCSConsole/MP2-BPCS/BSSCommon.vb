﻿Option Strict Off
Option Explicit On
Module BSSCommon

    Public Enum tDato
        tNum = 1
        tStr = 2
    End Enum

    Public Enum ChkVerApp
        AppOK = 1
        AppVersionIncorrect = 2
        AppSeparadorDecimal = 3
        AppDeshabilitada = 4
        AppEjecutando = 5
    End Enum
    Public fSql As String
    Public vSql As String
    Public lastSQL As String
    Public regActualizados As Long
    Public gsLIB As String
    Public gsKeyWords As String = ""
    Public ModuloActual As String = ""


    Public Function CalcConsec(ByRef sID As String, ByRef bLee As Boolean, ByRef TopeMax As String) As String
        Dim rs As New ADODB.Recordset
        Dim locID As String
        Dim sql As String
        Dim sConsec As String = ""
        Dim tMax As Double
        Dim sFmt As String
        Dim nDig As Integer

        Try

            nDig = Len(TopeMax)
            sFmt = New String("0", nDig)

            sql = "SELECT CCDESC FROM ZCCL01 WHERE CCTABL = 'SECUENCE' AND CCCODE='" & sID & "'"
            tMax = Val(TopeMax)

            With rs
                .CursorLocation = ADODB.CursorLocationEnum.adUseServer
                .Open(sql, DB_DB2, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)
                If Not (.BOF And .EOF) Then
                    locID = Val(.Fields("CCDESC").Value) + 1
                    If locID > tMax Then
                        locID = 1
                    End If
                    sConsec = Right(sFmt & locID, nDig)
                    .Fields("CCDESC").Value = sConsec
                    .Update()
                    .Close()
                Else
                    locID = 1
                    .Close()
                    sConsec = Right(sFmt & locID, nDig)
                    DB_DB2.Execute(" INSERT INTO ZCC (CCID, CCTABL, CCCODE, CCDESC ) " & " VALUES( 'CC', 'SECUENCE', '" & sID & "', '" & sConsec & "' )")
                End If
            End With

        Catch ex As Exception
            Return ""
        End Try
        Return sConsec

    End Function

    Public Function CheckVer(ByRef Aplicacion As String) As ChkVerApp
        Dim rs As ADODB.Recordset
        Dim lVer As String = "No hay registro"
        Dim result As ChkVerApp = ChkVerApp.AppOK
        Dim locVer As String

        locVer = "(" & My.Application.Info.Version.Major & "." & _
                 My.Application.Info.Version.Minor & "." & _
                 My.Application.Info.Version.Revision & ")"

        rs = New ADODB.Recordset
        With rs
            .Open("SELECT CCSDSC, CCUDC1, CCNOT2  FROM ZCCL01 WHERE CCTABL='RFVBVER' AND CCCODE = '" & Aplicacion & "'", DB_DB2)
            If Not (.EOF And .BOF) Then
                lVer = Trim(.Fields("CCSDSC").Value)
                If InStr(lVer, "*NOCHK") > 0 Then
                    result = ChkVerApp.AppOK
                End If

                If result = ChkVerApp.AppOK And InStr(lVer, locVer) > 0 Then
                    result = ChkVerApp.AppOK
                Else
                    result = ChkVerApp.AppVersionIncorrect
                End If
                If result = ChkVerApp.AppOK And rs.Fields("CCUDC1").Value = 0 Then
                    result = ChkVerApp.AppDeshabilitada
                End If
                'If result = ChkVerApp.AppOK And rs.Fields("CCUDC1").Value = 2 Then
                '    result = ChkVerApp.AppEjecutando
                'End If
                .Close()
            End If
        End With

        If result = ChkVerApp.AppOK And InStr(1, "" & Val("0.7"), ",") > 0 Then
            result = ChkVerApp.AppSeparadorDecimal
        End If

        Return result

    End Function

    Public Sub AppEjecutando(ByRef Aplicacion As String, ByVal Estado As Integer)

        DB_DB2.Execute("UPDATE ZCC SET CCNOT1='" & Now() & "', CCNOT2='" & SessionId & "', CCUDC1 = " + CStr(Estado) + " WHERE CCTABL='RFVBVER' AND CCCODE = '" & Aplicacion & "'")

    End Sub

    Function CodeCB(ByRef valor As String, Optional ByRef sep As String = "") As String
        Dim aCode As Object

        aCode = Split(valor, IIf(sep = "", " ", sep))
        CodeCB = aCode(0)

    End Function

    Function DescCB(ByRef valor As String, Optional ByRef sep As String = "") As String
        Dim aCode As Object

        aCode = Split(valor, IIf(sep = "", " ", sep))
        DescCB = Trim(Mid(valor, Len(aCode(0)) + 1))

    End Function

    Sub mkSql(ByVal tipoF As tDato, ByVal campo As String, ByVal valor As Object, Optional ByVal fin As Boolean = False)
        fSql += campo + IIf(fin, " )", ", ")
        If IsDBNull(valor) Then
            valor = IIf(tipoF = 1, "", "0")
        End If
        vSql += IIf(tipoF = tDato.tStr, "'", "") + CStr(valor) + IIf(tipoF = tDato.tStr, "'", "") + IIf(fin, " )", ", ")
        If campo = "SNDESC" And Len(valor) > 50 Then
            MsgBox("50")
        End If
    End Sub

    Sub mkUpd(ByVal tipoF As tDato, ByVal campo As String, ByVal valor As Object, Optional ByVal fin As Boolean = False)
        fSql += CStr(campo) + " = " + IIf(tipoF = tDato.tStr, "'", "") + valor + IIf(tipoF = tDato.tStr, "'", "") + IIf(fin, "", ", ")
    End Sub

    Public Function Busca(ByVal Sql As String, ByVal DB As ADODB.Connection, Optional ByRef Retorna As String = "") As Boolean
        Dim rs As New ADODB.Recordset
        Dim r As Boolean
        rs.Open(Sql, DB)
        If Not rs.EOF Then
            If Retorna <> "" Then
                Retorna = CStr(rs.Fields(0).Value)
            End If
        End If
        r = Not rs.EOF
        rs.Close()
        Return r

    End Function

    Function CampoAlfa(ByVal dato As String) As String
        Dim i As Integer
        CampoAlfa = ""
        For i = 1 To Len(dato)
            If Asc(Mid(dato, i, 1)) >= 32 And Asc(Mid(dato, i, 1)) < 127 Then
                CampoAlfa = CampoAlfa & Mid(dato, i, 1)
            End If
        Next
    End Function

    Public Function sbmLXjva(prefijo As String, arch As String, orden As String, linea As String) As String
        Dim job As String = CalcConsec("LXCJOB", False, "999999")

        fSql = " SBMJOB CMD( RUNJVA CLASS(OrdenCompra) PARM('-lib' '{LIB}' '-arch' '{ARCH}' '-orden' '{ORDEN}' '-linea' '{LINEA}' ) {CLASSPATH} )"
        fSql += " JOB(LX" & prefijo & job & ") JOBQ(" & getLXParam("JOBQ-OCMP2", "") & ") "

        fSql = Replace(fSql, "{CLASSPATH}", jvaClassPath("RJLXOC"))
        fSql = Replace(fSql, "{LIB}", sLib)
        fSql = Replace(fSql, "{ARCH}", arch)
        fSql = Replace(fSql, "{ORDEN}", orden)
        fSql = Replace(fSql, "{LINEA}", linea)

        DB_DB2.Execute("{{" & fSql & "}}")

        Return ""
    End Function

    Public Function sbmLXjvaRecibeOC(nBatch As String) As String
        Dim job As String = CalcConsec("LXCJOB", False, "999999")

        fSql = " SBMJOB CMD( RUNJVA CLASS(OrdenCompraMP2) PARM('-lib' '{LIB}' '-batch' '{BATCH}' ) {CLASSPATH} )"
        fSql += " JOB(MP2_" & job & ") JOBQ(" & getLXParam("JOBQ-OCMP2", "") & ") "

        fSql = Replace(fSql, "{CLASSPATH}", jvaClassPath("RJLXOCMP2"))
        fSql = Replace(fSql, "{LIB}", sLib)
        fSql = Replace(fSql, "{BATCH}", nBatch)

        DB_DB2.Execute("{{" & fSql & "}}")

        Return ""
    End Function

    'jvaClassPath()
    Function jvaClassPath(Jar) As String
        Dim s As String
        Dim rs As New ADODB.Recordset
        Dim fSql As String
        Dim resultado As String = ""

        s = " CLASSPATH('/{LIB}/:/{LIB}/" & Jar & ".jar:"
        fSql = " SELECT CCNOT1 FROM RFPARAM WHERE CCTABL = 'LXLONG' AND CCCODE = 'CLASSPATH' ORDER BY CCCODEN"
        rs.Open(fSql, DB_DB2)
        Do While Not rs.EOF
            s = s & rs("CCNOT1").Value
            rs.MoveNext()
        Loop
        s = s & "')"
        rs.Close()
        resultado = Replace(s, "{LIB}", gsLIB)
        Return resultado

    End Function


    Function getLXParam(prm As String, ByVal Campo As String) As String
        Dim rs As New ADODB.Recordset
        Dim resultado As String = ""

        If Campo = "" Then
            Campo = "CCDESC"
        End If
        rs.Open("SELECT " & Campo & " AS DATO FROM RFPARAM WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER('" & prm & "')", DB_DB2)
        If Not rs.EOF Then
            resultado = rs("DATO").Value
        End If
        rs.Close()
        rs = Nothing
        Return resultado

    End Function

    Sub WrtSqlError(sql As String, ByVal Descrip As String)
        Dim conn As New ADODB.Connection
        Dim fSql As String
        Dim vSql As String
        Dim aApl() As String
        Dim sApp As String = "VB.NET"

        Dim gsConnString As String = "Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=APLLX;Password=LXAPL;Persist Security Info=True;Default Collection=ERPLX834F;Force Translate=0;"
        If gsKeyWords.Trim <> "" Then
            Descrip = gsKeyWords & "<br>" & Descrip
        End If
        Descrip = Left(Descrip, 20000)

        sApp += My.Application.Info.ProductName & " v" & My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & "." & My.Application.Info.Version.Revision

        Try
            aApl = Split(System.Reflection.Assembly.GetExecutingAssembly.FullName, ", ")
            If UBound(aApl) > 0 Then
                sApp = aApl(0) & IIf(ModuloActual <> "", "." & ModuloActual, "")
            End If

            conn.Open(gsConnString)
            fSql = " INSERT INTO RFLOG( "
            vSql = " VALUES ( "
            fSql = fSql & " USUARIO   , " : vSql = vSql & "'" & DB_DB2_Param & " " & DB_MP2_Param & " " & System.Net.Dns.GetHostName() & "', "      '//502A
            fSql = fSql & " PROGRAMA  , " : vSql = vSql & "'" & "" & sApp & "', "      '//502A
            fSql = fSql & " ALERT     , " : vSql = vSql & "1, "      '//1P0
            fSql = fSql & " EVENTO    , " : vSql = vSql & "'" & Replace(Descrip, "'", "''") & "', "      '//20002A
            fSql = fSql & " TXTSQL    ) " : vSql = vSql & "'" & Replace(sql, "'", "''") & "' ) "      '//5002A
            conn.Execute(fSql & vSql)
            conn.Close()
        Catch ex As Exception

        End Try

    End Sub

    Public Function CalcConsec(ByVal sID As String, ByVal TopeMax As String) As String
        Dim rs As New ADODB.Recordset
        Dim locID As String
        Dim sql As String = ""
        Dim sConsec As String = ""
        Dim tMax As Double
        Dim sFmt As String
        Dim nDig As Integer
        Dim aTabla() As String
        Dim sTabla As String

        Try

            If sID.Contains(".") Then
                aTabla = sID.Split(".")
                sTabla = aTabla(0) & "." & "ZCCL01"
                sID = aTabla(1)
            Else
                sTabla = "ZCCL01"
            End If
            nDig = Len(TopeMax)
            sFmt = New String("0", nDig)

            sql = "SELECT CCDESC FROM " & sTabla & " WHERE CCTABL = 'SECUENCE' AND CCCODE='" & sID & "'"
            lastSQL = sql
            tMax = Val(TopeMax)

            With rs
                .CursorLocation = ADODB.CursorLocationEnum.adUseServer
                .Open(sql, DB_DB2, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)
                If Not (.BOF And .EOF) Then
                    locID = Val(.Fields("CCDESC").Value) + 1
                    If locID > tMax Then
                        locID = 1
                    End If
                    sConsec = Right(sFmt & locID, nDig)
                    .Fields("CCDESC").Value = sConsec
                    .Update()
                    .Close()
                Else
                    locID = 1
                    .Close()
                    sConsec = Right(sFmt & locID, nDig)
                    sql = " INSERT INTO " & sTabla & " (CCID, CCTABL, CCCODE, CCDESC ) " & " VALUES( 'CC', 'SECUENCE', '" & sID & "', '" & sConsec & "' )"
                    DB_DB2.Execute(sql)
                End If
            End With
        Catch ex As Exception
            WrtSqlError(sql, Err.Description)
        End Try
        Return sConsec

    End Function
    Function rs_Open(fSql As String, DB As ADODB.Connection) As ADODB.Recordset
        lastSQL = fSql
        Return DB.Execute(lastSQL)
    End Function

    Function DB_DB2_Execute(fSql As String, ByRef r As Integer) As Integer
        r = DB_Execute(fSql, DB_DB2)
        Return r
    End Function
    Function DB_DB2_Execute(fSql As String)
        Return DB_Execute(fSql, DB_DB2)
    End Function

    Function DB_mp2_Execute(fSql As String, ByRef r As Integer) As Integer
        r = DB_Execute(fSql, DB_mp2)
        Return r
    End Function

    Function DB_mp2_Execute(fSql As String)
        Return DB_Execute(fSql, DB_mp2)
    End Function

    Function DB_Execute(fSql As String, DB As ADODB.Connection) As Long
        lastSQL = fSql
        DB.Execute(lastSQL, regActualizados)
        Return regActualizados
    End Function


End Module
