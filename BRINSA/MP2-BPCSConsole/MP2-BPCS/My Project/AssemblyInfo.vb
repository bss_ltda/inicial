﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("MP2-BPCS")> 
<Assembly: AssemblyDescription("Intercace entre MP2 y LX")> 
<Assembly: AssemblyCompany("BSSLTDA")> 
<Assembly: AssemblyProduct("Interface MP2-BPCS")> 
<Assembly: AssemblyCopyright("Copyright © BSS 2016")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("080a1d64-c2bd-4fac-82b3-fa1bc7d856cd")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("2.1.0.1")>
<Assembly: AssemblyFileVersion("2.1.0.1")>
