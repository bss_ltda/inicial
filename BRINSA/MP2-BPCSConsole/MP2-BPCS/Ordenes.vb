﻿Module Ordenes
    Public Enum OrdenExiste
        OrdenNueva = 1
        OrdenActualizar = 2
        OrdenEnCola = 3
        OrdenRecibida = 4
        OrdenLineaBorrada = 5
        OrdenProductoDiferente = 6
        OrdenIgual = 7
        OrdenEnInputOrd = 8
    End Enum

    Sub CargaOrdenes()
        Dim rs As New ADODB.Recordset
        Dim Sql As String = ""
        Try
            RegistroT = ""

            Sql = " SELECT ponum FROM po WHERE status='Open'"
            'Sql = " SELECT ponum FROM po WHERE (PONUM = 643858)"

            rs = rs_Open(Sql, DB_mp2)
            Do While Not rs.EOF
                Console.WriteLine("INICIA ORDEN " + rs.Fields("ponum").Value)
                If RevisaOrden(rs.Fields("ponum").Value) Then
                    Console.WriteLine("VALIDADA " + rs.Fields("ponum").Value)
                    If SubeOrden(rs.Fields("ponum").Value) Then
                        Sql = "UPDATE po SET status = 'Receiving' WHERE (ponum = '" + rs.Fields("ponum").Value + "' )"
                        DB_mp2_Execute(Sql)
                        OrdenesOK += " " & rs.Fields("ponum").Value
                        Console.WriteLine("PASO OK " + rs.Fields("ponum").Value)
                    End If
                Else
                    'Console.WriteLine("NO PASO " + rs.Fields("ponum").Value)
                End If
                rs.MoveNext()
            Loop
            rs.Close()
        Catch ex As Exception
            wrtLog2(Err.Description)
        End Try


    End Sub

    Function RevisaOrden(ByVal sPoNum As String) As Boolean
        Dim Sql As String = ""
        Dim rs As New ADODB.Recordset
        Dim r As Boolean = True
        Dim msgErr As String

        Sql = " SELECT "
        Sql += " 	  Detalle.PONUM"
        Sql += " 	, Detalle.LINENUMBER"
        Sql += " 	, Detalle.ITEMNUM"
        Sql += " 	, Detalle.UNITCOST"
        Sql += " 	, Detalle.COSTCENTER"
        Sql += " 	, ISNULL( Detalle.UOP, '') AS UOP"
        Sql += " 	, Detalle.QTYREQUESTED"
        Sql += " 	, Detalle.ORDERWAREHOUSE"
        Sql += " 	, Detalle.DESTID"
        Sql += " 	, Orden.VENDORID"
        Sql += " 	, Detalle.SITEID"
        Sql += " 	, Detalle.SERVICECODE"
        Sql += " 	, Detalle.ITEMTYPE"
        Sql += " 	, Detalle.SEQNUM"
        Sql += " 	, Detalle.DUEDATE"
        Sql += " 	, Detalle.DATEGENERATED "
        Sql += " FROM "
        Sql += " 	purreq Detalle "
        Sql += " 	INNER JOIN po Orden ON Detalle.ponum = Orden.ponum "
        Sql += " WHERE"
        Sql += "  (Detalle.ponum = '" + sPoNum + "')"
        Sql += "   AND (Detalle.BPCS = 1)"
        rs = rs_Open(Sql, DB_mp2)
        If rs.EOF Then
            RegistroT = sPoNum
            wrtLog("1", "[ Orden abierta sin detalle.]")
        End If
        Do While Not rs.EOF
            msgErr = ValidaRegistro(rs)
            If msgErr = "" Then
                msgErr = CCActivo(CStr(rs.Fields("PONUM").Value), CStr(rs.Fields("LINENUMBER").Value))
            End If
            If msgErr <> "" Then
                RegistroT = rs.Fields("PONUM").Value & "-"
                If IsDBNull(rs.Fields("LINENUMBER").Value) Then
                    RegistroT = RegistroT & "Valor Nulo"
                Else
                    RegistroT = RegistroT & CStr(rs.Fields("LINENUMBER").Value)
                End If
                msgErr = Replace(msgErr, "][", "]<br>[")
                wrtLog("1", msgErr)
                r = False
            End If
            rs.MoveNext()
        Loop
        rs.Close()
        Return r

    End Function

    Function SubeOrden(ByVal sPoNum As String) As Boolean
        Dim Sql As String = ""
        Dim rs As New ADODB.Recordset
        Dim rsA As New ADODB.Recordset
        Dim sTipoUpd As String = ""
        Dim r As Integer
        Dim txtCompl As String
        Dim bOK As Boolean = True

        Sql = " SELECT "
        Sql += " 	  Detalle.PONUM"
        Sql += " 	, Detalle.REQUISITIONNUM"
        Sql += " 	, Detalle.QTYREQUESTED"
        Sql += " 	, Detalle.LINENUMBER"
        Sql += " 	, Detalle.UNITCOST"
        Sql += " 	, Detalle.ITEMNUM"
        Sql += " 	, ISNULL( Detalle.UOP, '' ) AS UOP"
        Sql += " 	, Detalle.ORDERWAREHOUSE"
        Sql += " 	, Detalle.COSTCENTER"
        Sql += " 	, Orden.VENDORID"
        Sql += " 	, Orden.BUYER"
        Sql += " 	, Detalle.SITEID"
        Sql += " 	, CASE WHEN (Detalle.ITEMTYPE <> 'Service') "
        Sql += " 		THEN COALESCE(CONVERT(varchar, Detalle.DUEDATE, 112), '') "
        Sql += " 		ELSE COALESCE(CONVERT(varchar, Detalle.DATEGENERATED, 112), '') END "
        Sql += " 		AS DATESERVICE"
        Sql += " 	, Detalle.SERVICECODE"
        Sql += " 	, Detalle.ITEMTYPE"
        Sql += " 	, Detalle.SEQNUM"
        Sql += " 	, Orden.NOTES "
        Sql += " FROM "
        Sql += " 	po Orden "
        Sql += " INNER JOIN "
        Sql += " 	purreq Detalle "
        Sql += " 	ON Orden.PONUM = Detalle.PONUM"
        Sql += " WHERE"
        Sql += "    (Detalle.ponum = '" + sPoNum + "')"
        Sql += " 	AND (Detalle.BPCS = 1)"
        Sql += " ORDER BY "
        Sql += " 	Detalle.LINENUMBER"

        rs = rs_Open(Sql, DB_mp2)
        Do While Not rs.EOF
            If Not IsDBNull(rs.Fields("LINENUMBER").Value) Then
                fSql = " INSERT INTO HPP500("
                vSql = " VALUES( "
                txtCompl = ""
                Select Case ValidaCantidad(rs, txtCompl)
                    Case OrdenExiste.OrdenNueva
                        sTipoUpd = "Create" + txtCompl
                    Case OrdenExiste.OrdenActualizar
                        sTipoUpd = "Changel"
                End Select

                mkSql(tDato.tStr, "MPPRID", "MP2")
                mkSql(tDato.tStr, "MPACTL", sTipoUpd)
                mkSql(tDato.tStr, "MPUSR", "MP2")
                mkSql(tDato.tNum, "MPDTNV", Format(Now(), "yyyyMMdd"))
                mkSql(tDato.tNum, "MPTMNV", Format(Now(), "HHmmss"))

                If rs.Fields("ITEMTYPE").Value = "Service" Then
                    mkSql(tDato.tStr, "PPROD", rs.Fields("SERVICECODE").Value)
                    mkSql(tDato.tNum, "PONIIT", 1)
                Else
                    mkSql(tDato.tStr, "PPROD", IIf(rs.Fields("ITEMTYPE").Value = "Service",
                                                   rs.Fields("SERVICECODE").Value,
                                                   rs.Fields("ITEMNUM").Value))
                    mkSql(tDato.tNum, "PONIIT", 0)
                End If
                mkSql(tDato.tStr, "POPRF", rs.Fields("COSTCENTER").Value)
                'Requisicion
                mkSql(tDato.tStr, "PHSVIA", rs.Fields("REQUISITIONNUM").Value)
                'Usuario Creacion 
                mkSql(tDato.tStr, "PHCBY", Left(rs.Fields("BUYER").Value, 10))
                Sql = "SELECT CCDESC, CCSDSC, CCNOT1, CCNOT2 "
                Sql += " FROM ZCCL01 "
                Sql += " WHERE (CCTABL = 'RMP2BPCS') "
                Sql += " AND (CCALTC = '" + IIf(rs.Fields("ITEMTYPE").Value = "Service", "S", "B") + "') "
                Sql += " AND (CCCODE = '" + rs.Fields("SITEID").Value + "')"

                rsA.Open(Sql, DB_DB2)
                mkSql(tDato.tStr, "PHFAC ", rsA.Fields("CCDESC").Value)
                mkSql(tDato.tStr, "PHWHSE", rsA.Fields("CCSDSC").Value)
                mkSql(tDato.tStr, "PWHSE ", rsA.Fields("CCSDSC").Value)
                mkSql(tDato.tStr, "PHSHIP", rsA.Fields("CCNOT1").Value)
                mkSql(tDato.tStr, "PHBUYC", rsA.Fields("CCNOT2").Value)
                rsA.Close()

                mkSql(tDato.tNum, "PHSHTP", 0)
                'mkSql(tDato.tStr, "PNOTAS", rs.Fields("NOTES").Value)
                mkSql(tDato.tNum, "PQORD", rs.Fields("QTYREQUESTED").Value)
                mkSql(tDato.tNum, "PLINE", rs.Fields("LINENUMBER").Value)
                mkSql(tDato.tStr, "PODEST", rs.Fields("LINENUMBER").Value)
                mkSql(tDato.tNum, "PECST", rs.Fields("UNITCOST").Value)
                mkSql(tDato.tNum, "PHVEND", rs.Fields("VENDORID").Value)
                mkSql(tDato.tNum, "PDDTE", rs.Fields("DATESERVICE").Value)
                mkSql(tDato.tNum, "PHORD", rs.Fields("PONUM").Value, True)

                DB_DB2_Execute(fSql + vSql, r)
                If r = 1 Then
                    Sql = "UPDATE purreq SET BPCS = 2 "
                    Sql += " WHERE (PONUM = '" + rs.Fields("PONUM").Value + "' )"
                    Sql += " AND (LINENUMBER = " + CStr(rs.Fields("LINENUMBER").Value) + " )"
                    DB_mp2_Execute(Sql)

                    If sTipoUpd = "Changel" Then
                        'Sql = "DELETE FROM inputord "
                        'Sql += " WHERE (ponum_t = '" + rs.Fields("PONUM").Value + "' )"
                        'Sql += " AND (linea_t = " + CStr(rs.Fields("LINENUMBER").Value) + " )"
                        'DB_mp2_Execute(Sql, r)
                    Else
                        'Insert el registro en inputord para el control de enviados
                        fSql = " INSERT INTO inputord( "
                        vSql = " VALUES( "
                        mkSql(tDato.tStr, "ponum_t", rs.Fields("PONUM").Value)
                        mkSql(tDato.tNum, "linea_t", rs.Fields("LINENUMBER").Value)
                        mkSql(tDato.tStr, "accion", "U")
                        mkSql(tDato.tNum, "estado", 2)
                        mkSql(tDato.tNum, "servicio", IIf(rs.Fields("ITEMTYPE").Value = "Service", 1, 0))
                        mkSql(tDato.tStr, "seqnum_purreq", rs.Fields("SEQNUM").Value, True)
                        DB_mp2_Execute(fSql & vSql, r)
                    End If
                Else
                    bOK = False
                    wrtLog("1", "[ No se puede crear la Orden " & sPoNum & " ]")
                End If

                If Not IsDBNull(rs.Fields("NOTES").Value) And txtCompl = "" Then
                    NotasOrden(rs.Fields("PONUM").Value, rs.Fields("NOTES").Value)
                End If
            Else
                RegistroT = rs.Fields("QTYREQUESTED").Value
                wrtLog("1", "[ Linea de la Orden " & rs.Fields("QTYREQUESTED").Value & " con valor nulo.]")
            End If
            rs.MoveNext()
        Loop

        If bOK Then
            sbmLXjva("OC", "HPP500", sPoNum, "0")
        End If

        rs.Close()
        Return (bOK)

    End Function

    Function ValidaRegistro(ByVal rs_PO As ADODB.Recordset) As String
        Dim rs As New ADODB.Recordset
        Dim Sql As String = ""
        Dim msgErr As String = ""
        Dim msgErrCompl As String = ""
        Dim Facility As String = ""
        Dim Item As String = ""
        Dim aLoc() As String

        'Valida Si ya existe la orden y si la Cantidad es la misma
        If Not IsDBNull(rs_PO.Fields("LINENUMBER").Value) Then
            Select Case ValidaCantidad(rs_PO)
                Case OrdenExiste.OrdenEnCola
                    msgErr += "[ Orden en Cola. Revise el log del SMG "
                    msgErr += "<a href=""mptlog.asp?s_DPEDID=" & rs_PO.Fields("PONUM").Value & """ target=""_blank"">mptlog</a>]"
                Case OrdenExiste.OrdenEnInputOrd
                    msgErr += "[ Orden ya habia sido enviada. Revise inputord]"
                Case OrdenExiste.OrdenRecibida
                    msgErr += "[ Cantidad no puede ser menor a la recibida]"
                Case OrdenExiste.OrdenLineaBorrada
                    msgErr += "[ Linea Borrada en BPCS]"
                Case OrdenExiste.OrdenProductoDiferente
                    msgErr += "[ Producto diferente entre MP2 y BPCS]"
                Case OrdenExiste.OrdenIgual
                    Sql = "UPDATE purreq SET BPCS = 2 "
                    Sql += " WHERE (PONUM = '" + rs_PO.Fields("PONUM").Value + "' )"
                    Sql += " AND (LINENUMBER = " + CStr(rs_PO.Fields("LINENUMBER").Value) + " )"
                    DB_mp2_Execute(Sql)
            End Select
        Else
            msgErr += "[ Linea de la orden con valor nulo.]"
        End If


        If msgErr <> "" Then
            Return msgErr
        End If
        If Not IsDBNull(rs_PO.Fields("COSTCENTER").Value) Then
            If Len(rs_PO.Fields("COSTCENTER").Value) > 10 Then
                msgErr += "[Codigo Centro de Costo muy largo.]"
                Return msgErr
            End If
        Else
            msgErr += "[ Centro de Costo con valor nulo.]"
        End If

        If msgErr <> "" Then
            Return msgErr
        End If

        'Valida producto y unidad de medida
        If IsDBNull(rs_PO.Fields("ITEMTYPE").Value) Then
            Return "[ Falta el tipo de producto o servicio en el detalle. ]"
        End If

        If rs_PO.Fields("ITEMTYPE").Value = "Service" Then
            If IsDBNull(rs_PO.Fields("SERVICECODE").Value) Then
                Return "[ Falta el codigo del servicio ]"
            Else
                Item = rs_PO.Fields("SERVICECODE").Value
                Sql = "SELECT IUMS FROM IIML01 WHERE IPROD='" + Item + "'"
                rs = rs_Open(Sql, DB_DB2)
                If rs.EOF Then
                    msgErr = "[ Servicio No Encontrado " + Item + " ]"
                End If
                If rs_PO.Fields("DATEGENERATED").Value < CDate(Format(Now, "yyyy-MM-dd")) Then
                    msgErr = "[ Fecha de la orden no puede ser menor a la actual "
                    msgErr += Format(rs_PO.Fields("DATEGENERATED").Value, "yyyy-MM-dd") + " ]"
                End If
                rs.Close()
            End If
        Else
            Item = rs_PO.Fields("ITEMNUM").Value
            Sql = "SELECT IUMS FROM IIML01 WHERE IPROD='" + Item + "'"
            rs = rs_Open(Sql, DB_DB2)
            If Not rs.EOF Then
                If rs.Fields(0).Value <> rs_PO.Fields("UOP").Value Then
                    msgErr = "[ Unidad de Medida BPCS: " + rs.Fields(0).Value + " MP2: " + rs_PO.Fields("UOP").Value + " ]"
                    msgErr += "[ Producto " + Item + " ]"
                End If
            Else
                msgErr = "[ Producto " & Item & " No Encontrado]"
            End If
            If IsDBNull(rs_PO.Fields("DUEDATE").Value) Then
                msgErr = "[ Fecha de la orden no puede ser NULL ]"
            ElseIf rs_PO.Fields("DUEDATE").Value < CDate(Format(Now, "yyyy-MM-dd")) Then
                msgErr = "[ Fecha de la orden no puede ser menor a la actual "
                msgErr += Format(rs_PO.Fields("DUEDATE").Value, "yyyy-MM-dd") + " ]"
            End If
            rs.Close()
        End If

        If IsDBNull(rs_PO.Fields("UNITCOST").Value) Then
            msgErr = "[ Costo Nulo ]"
        ElseIf rs_PO.Fields("UNITCOST").Value = 0 Then
            msgErr = "[ Costo Unitario en 0 ]"
        End If

        'Parametros del Facility
        Facility = "CCDESC"
        Sql = "SELECT CCDESC, CCSDSC, CCNOT1, CCNOT2 "
        Sql += " FROM ZCCL01 "
        Sql += " WHERE (CCTABL = 'RMP2BPCS') "
        Sql += " AND (CCALTC = '" + IIf(rs_PO.Fields("ITEMTYPE").Value = "Service", "S", "B") + "') "
        Sql += " AND (CCCODE = '" + rs_PO.Fields("SITEID").Value + "')"
        If Not Busca(Sql, DB_DB2, Facility) Then
            msgErr += "[ Parametros Facility=" + rs_PO.Fields("SITEID").Value + "," + rs_PO.Fields("ITEMTYPE").Value + " ]"
        End If

        'validar impuesto
        Sql = " SELECT VENDOR "
        Sql += " FROM ZRTL01 ZRT "
        Sql += " INNER JOIN AVML01 AVM ON AVM.VTAXCD = ZRT.RTCVCD "
        Sql += " INNER JOIN IIML01 IIM ON IIM.TAXC1 = ZRT.RTICDE "
        Sql += " WHERE (AVM.VENDOR = " + rs_PO.Fields("VENDORID").Value + ") "
        Sql += " AND (IIM.IPROD = '" + Item + "')"
        If Not Busca(Sql, DB_DB2) Then
            msgErr += "[ No existe relacion entre codigo de Impuestos del Producto " & Item & " y "
            msgErr += " el Proveedor " + rs_PO.Fields("VENDORID").Value + " ]"
        End If

        'Bodega
        Sql = "SELECT WMFAC FROM IWML01 "
        Sql += " WHERE (LWHS = '" + rs_PO.Fields("ORDERWAREHOUSE").Value + "') "
        Sql += " AND (WMFAC = '" + Facility + "')"
        If Not Busca(Sql, DB_DB2) Then
            msgErr += "[ Bodega: " & rs_PO.Fields("ORDERWAREHOUSE").Value
            msgErr += " no Pertenece al Facility: " + Facility + " ]"
        End If

        'Localizacion para ordenes de bienes
        If rs_PO.Fields("ITEMTYPE").Value <> "Service" Then
            If IsDBNull(rs_PO.Fields("DESTID").Value) Then
                msgErr += "[Localizacion null]"
                ReDim aLoc(1)
                aLoc(0) = "NULL"
            ElseIf InStr(rs_PO.Fields("DESTID").Value, "-") > 0 Then
                aLoc = Split(rs_PO.Fields("DESTID").Value, "-")
            Else
                ReDim aLoc(1)
                aLoc(0) = Left(Trim(rs_PO.Fields("DESTID").Value), 5)
            End If
            Sql = "SELECT WLOC FROM ILML01 "
            Sql += " WHERE (WWHS = '" + rs_PO.Fields("ORDERWAREHOUSE").Value + "') "
            Sql += " AND (WLOC = '" + Trim(aLoc(0)) + "')"
            If Not Busca(Sql, DB_DB2) Then
                msgErr += "[ No se encontro Bodega: " & rs_PO.Fields("ORDERWAREHOUSE").Value & ","
                msgErr += " Localizacion: "
                If IsDBNull(rs_PO.Fields("DESTID").Value) Then
                    msgErr += " NULL ]"
                Else
                    msgErr += rs_PO.Fields("DESTID").Value & " ]"
                End If
            End If

            'Proveedor
            Sql = "SELECT VENDOR FROM AVML01 "
            Sql += " WHERE (VENDOR = " + rs_PO.Fields("VENDORID").Value + ") AND (NOT VHOLD = 'Y')"
            If Not Busca(Sql, DB_DB2) Then
                msgErr += "[ Proveedor Inactivo=" + rs_PO.Fields("VENDORID").Value + " ]"
            End If
        End If

        Return msgErr

    End Function

    Function ValidaCantidad(ByVal rs_PO As ADODB.Recordset, Optional ByRef txtCompl As String = "") As OrdenExiste
        Dim Sql As String
        Dim rs As New ADODB.Recordset
        Dim rsHPO As New ADODB.Recordset
        Dim r As Integer = OrdenExiste.OrdenNueva
        Dim ItemNum As String

        'Coloca txtCompl en L para Createl
        Sql = "SELECT PQORD, PPROD FROM HPP500 "
        Sql += " WHERE "
        Sql += " (PHORD = " + rs_PO.Fields("PONUM").Value + ") "
        rs = rs_Open(Sql, DB_DB2)
        If Not rs.EOF Then
            txtCompl = "l"
        End If
        rs.Close()

        Sql = "SELECT PQORD, PPROD FROM HPP500 "
        Sql += " WHERE "
        Sql += " (PHORD = " + rs_PO.Fields("PONUM").Value + ") "
        Sql += " AND (PLINE = " + CStr(rs_PO.Fields("LINENUMBER").Value) + ")"
        rs = rs_Open(Sql, DB_DB2)
        If rs.EOF Then
            r = OrdenExiste.OrdenNueva
        End If
        rs.Close()

        If r = OrdenExiste.OrdenNueva Then
            Sql = "SELECT ponum_t FROM inputord "
            Sql += " WHERE "
            Sql += " (ponum_t = '" + rs_PO.Fields("PONUM").Value + "' ) "
            Sql += " AND (linea_t = " + CStr(rs_PO.Fields("LINENUMBER").Value) + ")"
            rs = rs_Open(Sql, DB_mp2)
            If Not rs.EOF Then
                r = OrdenExiste.OrdenEnInputOrd
            End If
            rs.Close()
            Return r
        End If

        Sql = "SELECT PID, PQREC, PQORD, PPROD FROM HPO "
        Sql += " WHERE "
        Sql += " (PORD = " + rs_PO.Fields("PONUM").Value + ") "
        Sql += " AND (PODEST = '" + CStr(rs_PO.Fields("LINENUMBER").Value) + "')"
        rsHPO.Open(Sql, DB_DB2)

        If rs_PO.Fields("ITEMTYPE").Value <> "Service" Then
            ItemNum = rs_PO.Fields("ITEMNUM").Value
        Else
            ItemNum = rs_PO.Fields("SERVICECODE").Value
        End If
        If rsHPO.EOF Then
            r = OrdenExiste.OrdenEnCola
        ElseIf rsHPO.Fields("PPROD").Value <> ItemNum Then
            r = OrdenExiste.OrdenProductoDiferente
        ElseIf rsHPO.Fields("PID").Value = "PZ" Then
            r = OrdenExiste.OrdenLineaBorrada
        ElseIf rsHPO.Fields("PQREC").Value <= rs_PO.Fields("QTYREQUESTED").Value _
           And rsHPO.Fields("PQORD").Value <> rs_PO.Fields("QTYREQUESTED").Value Then
            r = OrdenExiste.OrdenActualizar
        ElseIf rsHPO.Fields("PQREC").Value <= rs_PO.Fields("QTYREQUESTED").Value _
           And rsHPO.Fields("PQORD").Value = rs_PO.Fields("QTYREQUESTED").Value Then
            r = OrdenExiste.OrdenIgual
        Else
            r = OrdenExiste.OrdenRecibida
        End If

        rs.Close()
        rsHPO.Close()

        Return r

    End Function

    Sub OrdenesEliminadas(Optional ByVal Orden = "", Optional ByVal Linea = 0)
        Dim rs As New ADODB.Recordset
        Dim rsA As New ADODB.Recordset
        Dim Sql As String
        Dim r As Integer

        Sql = "SELECT ponum_t AS PONUM, linea_t AS LINENUMBER "
        Sql += " FROM inputord "
        Sql += " WHERE"
        If Orden <> "" And Linea <> 0 Then
            Sql += " ponum_t = '" + Orden + "' "
            Sql += " AND linea_t = " + Linea
        Else
            Sql += " (accion = 'D') "
            Sql += " AND (estado = 0)"
        End If
        rs = rs_Open(Sql, DB_mp2)
        Do While Not rs.EOF
            Sql = "SELECT * FROM HPO "
            Sql += " WHERE " '(MPACTL LIKE 'Create%') AND
            Sql += " (PORD = '" + rs.Fields("PONUM").Value + "') "
            Sql += " AND (PODEST = '" + CStr(rs.Fields("LINENUMBER").Value) + "' )"
            rsA.Open(Sql, DB_DB2)
            If Not rsA.EOF Then
                fSql = " INSERT INTO HPP500("
                vSql = " VALUES( "
                mkSql(tDato.tNum, "MPTMNV", Format(Now(), "HHmmss"))
                mkSql(tDato.tNum, "MPDTNV", Format(Now(), "yyyyMMdd"))
                mkSql(tDato.tStr, "MPUSR", "mp2")
                mkSql(tDato.tStr, "MPPRID", "MP2")
                mkSql(tDato.tStr, "MPACTL", "Deletel")
                mkSql(tDato.tNum, "PHORD", rsA.Fields("PORD").Value)
                mkSql(tDato.tNum, "PLINE", rsA.Fields("PLINE").Value, True)
                DB_DB2_Execute(fSql + vSql, r)

                sbmLXjva("OC", "HPP500", rsA.Fields("PORD").Value, rsA.Fields("PLINE").Value)

                Sql = "UPDATE inputord SET "
                Sql += " estado = 2 "
                Sql += " WHERE"
                Sql += " ponum_t = '" + rs.Fields("PONUM").Value + "' "
                Sql += " AND linea_t = " + CStr(rs.Fields("LINENUMBER").Value)
                DB_mp2_Execute(Sql, r)
            End If
            rsA.Close()
            rs.MoveNext()
        Loop
        rs.Close()

    End Sub

    Sub OrdenesServicio()
        Dim rs As New ADODB.Recordset
        Dim rsA As New ADODB.Recordset
        Dim Sql As String
        Dim r As Integer

        'Actualizar en BPCS el HPO para que las ordenes de servicio se autoreciban
        'Sql = "SELECT count(*) "
        'Sql += " FROM inputord "
        'Sql += " WHERE"
        'Sql += " (servicio = 1) "
        'rs =  rs_Open((Sql, DB_MP2)
        'Debug.Print(rs(0).Value)
        'rs.Close()

        Sql = "SELECT ponum_t AS PONUM, linea_t AS LINENUMBER "
        Sql += " FROM inputord "
        Sql += " WHERE"
        Sql += " (servicio = 1) "
        rs = rs_Open(Sql, DB_mp2)
        Do While Not rs.EOF
            Sql = "UPDATE HPO SET "
            Sql += " PONIIT = '1', PQREC = PQORD "
            Sql += " WHERE"
            Sql += " PORD = " + rs.Fields("PONUM").Value
            Sql += " AND PODEST = '" + CStr(rs.Fields("LINENUMBER").Value) & "'"
            DB_DB2_Execute(Sql, r)

            Sql = "UPDATE inputord SET "
            Sql += " servicio = " & (r + 2)
            Sql += " WHERE"
            Sql += " ponum_t = '" + rs.Fields("PONUM").Value + "' "
            Sql += " AND linea_t = " + CStr(rs.Fields("LINENUMBER").Value)
            DB_mp2_Execute(Sql, r)
            rs.MoveNext()
        Loop
        rs.Close()
    End Sub

    Sub PruebaNota()
        Dim rs As New ADODB.Recordset

        rs = rs_Open("SELECT NOTES FROM PO WHERE PONUM = '554596' ", DB_mp2)
        NotasOrden("554596", rs("NOTES").Value)

    End Sub

    Sub NotasOrden(ByVal Orden As String, ByVal Nota As String)
        Dim numSeq As String
        Dim locNota As String
        Dim lineaNota As String

        locNota = Trim(Nota)
        numSeq = "( SELECT IFNULL( MAX( SNSEQ ) , 0 ) + 1 FROM ESN WHERE SNTYPE='P' AND SNCUST=" & Orden & " )"
        Do While locNota <> ""
            lineaNota = CampoAlfa(Left(locNota, 50))
            fSql = " INSERT INTO ESN("
            vSql = " VALUES( "
            mkSql(tDato.tStr, "SNID", "SN")                           'Record ID; SN/SZ
            mkSql(tDato.tStr, "SNTYPE", "P")                          'Type:C,O,P,S,V,M,R
            mkSql(tDato.tNum, "SNCUST", Orden)                        'Customer Number
            mkSql(tDato.tNum, "SNSHIP", 0)                            'Ship to Number
            mkSql(tDato.tNum, "SNSEQ", numSeq)                        'Note Sequence Number
            mkSql(tDato.tStr, "SNDESC", lineaNota)                    'Note Description
            mkSql(tDato.tStr, "SNPRT", "Y")                           'Print on Ack Y/N
            mkSql(tDato.tStr, "SNPIC", "Y")                           'Print on Pic Y/N
            mkSql(tDato.tStr, "SNINV", "Y")                           'Print on Inv Y/N
            mkSql(tDato.tStr, "SNSTMT", "Y")                          'Print on Cust Statements Y/
            mkSql(tDato.tStr, "SNDOCR", "")                           'Document Number
            mkSql(tDato.tNum, "SNENDT", Format(Now(), "yyyyMMdd"))    'Entered Date
            mkSql(tDato.tNum, "SNENTM", Format(Now(), "HHmmss"))      'Entered Time
            mkSql(tDato.tStr, "SNENUS", "MP2", True)                  'Entered User
            DB_DB2_Execute(fSql & vSql)
            locNota = Mid(locNota, 51)
        Loop

    End Sub

    Function CCActivo(ByVal Orden As String, ByVal Linea As String) As String
        Dim sql As String = ""

        sql = "UPDATE purreq SET BPCS = BPCS "
        sql += " WHERE (PONUM = '" + Orden + "' )"
        sql += " AND (LINENUMBER = " + Linea + " )"
        On Error Resume Next
        DB_mp2_Execute(sql)
        If Err.Number <> 0 Then
            CCActivo = Err.Description
        End If
        On Error GoTo 0

    End Function

    'Actualiza la fecha de las ordenes de compra de almacen generadas por Excel OCALMAC
    Sub OrdenesAlmacen()
        Dim rs As New ADODB.Recordset
        Dim rsA As New ADODB.Recordset
        Dim Sql As String
        Dim r As Integer

        'Actualizar en BPCS el HPO con la fecha por linea establecida
        Sql = "SELECT * FROM HPP500 WHERE MPUSR = 'OCOMPRA' AND PHORD > 800000 AND PHID <> 'OK'"
        rs = rs_Open(Sql, DB_DB2)

        Do While Not rs.EOF
            Sql = "UPDATE HPO SET "
            Sql += " PDDTE = " & CStr(rs.Fields("PDDTE").Value) & ", "
            Sql += " HVDUE = " & CStr(rs.Fields("PDDTE").Value) & ", "
            Sql += " POADR4 = '" & CStr(rs.Fields("PDDTE").Value) & "', "
            Sql += " POADR5 = '" & Format(Now(), "yyyyMMddHHmm") & "' "
            Sql += " WHERE"
            Sql += " PORD = " & CStr(rs.Fields("PHORD").Value)
            Sql += " AND PODEST = '" & rs.Fields("PODEST").Value & "'"
            DB_DB2_Execute(Sql, r)
            If r > 0 Then
                Sql = "INSERT INTO RFALHPO "
                Sql += "SELECT * FROM HPO WHERE"
                Sql += " PORD = " & CStr(rs.Fields("PHORD").Value)
                Sql += " AND PODEST = '" & rs.Fields("PODEST").Value & "'"
                DB_DB2_Execute(Sql, r)

                Sql = "UPDATE HPP500 SET "
                Sql += " PHID = 'OK'"
                Sql += " WHERE"
                Sql += " PHORD = " & rs.Fields("PHORD").Value
                Sql += " AND PODEST = '" & rs.Fields("PODEST").Value & "'"
                DB_DB2_Execute(Sql, r)
            End If
            rs.MoveNext()
        Loop
        rs.Close()
    End Sub


End Module



