﻿Module Transacciones

    Public aReg() As String
    Public RegistroT As String
    Public TipoTransacc As String
    Public Cantidad As Double
    Public Movimiento As Boolean
    Public nBatch As String

    Public Enum TransaccEstado
        TrasaccOK = 0
        NoAplica = 1
        EnError = 2
        RecibirOC = 3
    End Enum

    Sub CargaTransacciones()
        Dim rs As New ADODB.Recordset
        Dim NumerosT As String = ""
        Dim sep As String = ""
        TransaccOK = True
        Dim fSql As String

        '' '' ''Dim sqlMP2 As String  PARA EL PROBLEMA DE ENVIADOS EL 23 DE OCT 2015
        '' '' ''sqlMP2 = "SELECT *  FROM [MP2].[dbo].[InputMp2Bck]  where Mensaje_t ='Procesada 2812205'"
        '' '' ''sqlMP2 &= "and year( Fecha_t ) = 2015 and month( Fecha_t) = 10 and day(Fecha_t) = 23"
        '' '' ''sqlMP2 &= " Order By numero_t"

        'para revisar un registro con problema
        'rs.Open("SELECT * FROM inputmp2 where Numero_t = 1202591", DB_MP2)

        'Reprocesar un registro que esta en el backup
        'fSql = " SELECT [Numero_t] "
        'fSql = fSql & "      ,[Fecha_t] "
        'fSql = fSql & "      ,[Registro_t] "
        'fSql = fSql & "      ,[Mensaje_t] "
        'fSql = fSql & "  FROM [MP2].[dbo].[InputMp2Bck] "
        'fSql = fSql & "  where Numero_t = 1215127 "

        fSql = "SELECT * FROM inputmp2 "
        'fSql &= " where registro_t Like '%646678%' "
        fSql &= " order by fecha_t desc"
        rs = rs_Open(fSql, DB_mp2)
        Do While Not rs.EOF
            RegistroT = rs.Fields("Registro_T").Value
            aReg = Split(rs.Fields("Registro_T").Value, "|")
            Debug.Print(rs.Fields("Registro_T").Value)
            Select Case ValidaTransaccion()
                Case TransaccEstado.RecibirOC
                    If nBatch = "" Then
                        nBatch = CalcConsec("PRDFOCREC", False, "99999999")
                    End If
                    If RecibeOC(rs.Fields("Numero_t").Value) Then
                        NumerosT &= sep & rs.Fields("Numero_t").Value
                        sep = ", "
                    End If
                Case TransaccEstado.TrasaccOK
                    If SubeTransaccion(rs.Fields("Numero_t").Value) Then
                        If Movimiento Then
                            'Si la transacciones tipo movimiento genera transaccion con signo contrario
                            aReg(9) = aReg(17)
                            aReg(10) = aReg(18)
                            Cantidad *= -1
                            If SubeTransaccion(rs.Fields("Numero_t").Value) Then
                                BackupInput(rs.Fields("Numero_t").Value, "Procesada " + CStr(SessionId))
                            End If
                        Else
                            BackupInput(rs.Fields("Numero_t").Value, "Procesada " + CStr(SessionId))
                        End If
                    End If
                Case TransaccEstado.NoAplica
                    BackupInput(rs.Fields("Numero_t").Value, "No procesada " + CStr(SessionId))
                Case TransaccEstado.EnError
                    TransaccOK = False
            End Select
            rs.MoveNext()
        Loop
        If nBatch <> "" Then
            If NumerosT <> "" Then
                sbmLXjvaRecibeOC(nBatch)
                BackupInput(NumerosT, "Procesada " + CStr(SessionId))
            End If
        End If

    End Sub

    Function ValidaTransaccion() As TransaccEstado
        Dim rs As New ADODB.Recordset
        Dim r As TransaccEstado = TransaccEstado.TrasaccOK
        Dim Sql As String
        Dim chkSaldo As Boolean = False

        TipoTransacc = ""
        Cantidad = 0
        If aReg(1) <> "ITH" Then
            Return TransaccEstado.NoAplica
        End If

        If Val(aReg(7)) >= 90000000 Then 'Producto no Comodities
            Return TransaccEstado.NoAplica
        End If

        '[8]  tipo transac
        '[6]  orden
        '[19] linea 
        '[20] unidades

        r = SetTipoTransacc(chkSaldo)
        If r <> TransaccEstado.TrasaccOK Then
            Return r
        End If

        r = CantidadTransacc()
        If r <> TransaccEstado.TrasaccOK Then
            Return r
        End If

        If aReg(8) = "UB" Or aReg(8) = "UA" Or aReg(8) = "UC" Or aReg(8) = "UI" Then
            'Orden Ocupada
            If aReg(6) = "*" Then
                wrtLog("1", "[ * en Orden de Compra " & aReg(6) & " ] [6]")
                r = TransaccEstado.EnError
            ElseIf CLng(aReg(6)) > 0 Then
                Sql = "SELECT PHBUSY FROM HPH WHERE PHORD = " & CStr(CLng(aReg(6))) & " And PHBUSY IN( '', '0' ) "
                If Not Busca(Sql, DB_DB2) Then
                    wrtLog("1", "[ Orden de Compra Ocupada " & aReg(6) & " ] [6]")
                    r = TransaccEstado.EnError
                End If
            End If
        End If

        'Bodega
        Sql = "SELECT WMFAC FROM IWML01 "
        Sql += " WHERE (LWHS = '" + aReg(9) + "') "
        'Sql += " AND (WMFAC = '" + ? + "')"
        If Not Busca(Sql, DB_DB2) Then
            wrtLog("1", "[ No Encontro Bodega " + aReg(9) + " ] [9]")
            r = TransaccEstado.EnError
        End If

        'Localizacion
        Sql = "SELECT WLOC FROM ILML01 "
        Sql += " WHERE (WWHS = '" + aReg(9) + "') "
        Sql += " AND (WLOC = '" + aReg(10) + "')"
        If Not Busca(Sql, DB_DB2) Then
            wrtLog("1", "[ No Encontro Localizacion " + aReg(10) + " ] [10]")
            r = TransaccEstado.EnError
        End If

        If Movimiento Then
            'Localizacion destino
            Sql = "SELECT WLOC FROM ILML01 "
            Sql += " WHERE (WWHS = '" + aReg(17) + "') "
            Sql += " AND (WLOC = '" + aReg(18) + "')"
            If Not Busca(Sql, DB_DB2) Then
                wrtLog("1", "[ No Encontro Localizacion " + aReg(18) + " ] [18]")
                r = TransaccEstado.EnError
            End If
        End If

        '// Cambio | 2016-11-08 15:33:30 | No se validad aReg(6) para numero de la orden
        fSql = "SELECT * FROM ITEL01 WHERE TTYPE='" + aReg(8) + "' AND TPO ='Y'"
        rs = rs_Open(fSql, DB_DB2)
        If Not rs.EOF Then
            If Not IsNumeric(aReg(6)) Then
                wrtLog("1", "[ Falta el numero de la orden " + aReg(6) + " ] [6]")
                r = TransaccEstado.EnError
            End If
        End If
        rs.Close()


        'Chequeo de Inventario Disponible
        If chkSaldo Then
            Sql = " SELECT LOPB - LISSU + LADJU + LRCT - LIALOC AS SALDO "
            Sql += " FROM ILIL01 "
            Sql += " WHERE "
            Sql += " LWHS = '" + aReg(9) + "'"
            Sql += " AND LLOC = '" + aReg(10) + "'"
            Sql += " AND LPROD = '" + aReg(7) + "' "
            rs = rs_Open(Sql, DB_DB2)
            If Not rs.EOF Then
                If rs.Fields("SALDO").Value <= 0 Then
                    wrtLog("1", "No Encontro Inventario para BOD[" + aReg(9) + "] LOC[" + aReg(10) + "] PROD[" + aReg(7) + "]")
                    r = TransaccEstado.EnError
                End If
            Else
                wrtLog("1", "No Encontro Inventario para BOD[" + aReg(9) + "] LOC[" + aReg(10) + "] PROD[" + aReg(7) + "]")
                r = TransaccEstado.EnError
            End If
            rs.Close()
        End If

        Sql = "SELECT IUMS FROM IIML01 WHERE IPROD='" + aReg(7) + "'"
        rs = rs_Open(Sql, DB_DB2)
        If Not rs.EOF Then
            'If aReg(20) <> rs.Fields("IUMS").Value Then
            '    wrtLog("1", "[ Unidad de Medida BPCS=" + aReg(20) + " MP2=" + rs.Fields("IUMS").Value + "]")
            '    r = TransaccEstado.EnError
            'End If
        Else
            wrtLog("1", "[ No encontro Producto " + aReg(7) + " ] [7]")
            r = TransaccEstado.EnError
        End If
        rs.Close()

        If aReg(15) = "Recibir compras" And r = TransaccEstado.TrasaccOK Then
            Debug.Print(aReg(6))
            'If CDbl(aReg(5)) > 0 Then
            r = TransaccEstado.RecibirOC
            'End If
        End If

        Return r

    End Function

    Function SubeTransaccion(ByVal NumeroT As Double) As Boolean
        Dim r As Integer = 0
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim rs2 As New ADODB.Recordset
        Dim qArchivo As String = ""
        Dim qValor As Double = 0
        Dim tipPreliq As String = ""

        fSql = "SELECT * FROM ITEL01 WHERE TTYPE='" + TipoTransacc + "' AND TPO='Y'"
        rs = rs_Open(fSql, DB_DB2)
        If Not rs.EOF Then
            rs.Close()
            fSql = "SELECT * FROM HPOL01 WHERE PORD=" + CStr(CLng(aReg(6))) + " AND PLINE=" & QueLinea() & " AND PPROD = '" & aReg(7) & "'"
            rs = rs_Open(fSql, DB_DB2)
            If Not rs.EOF Then
                If Left(TipoTransacc, 1) = "U" Then
                    fSql = "SELECT AATIPO FROM RFALPRE "
                    fSql &= " WHERE AID IN('PA', 'LQ') AND AVEND = " & rs("PVEND").Value & " AND APROD = '" & rs("PPROD").Value & "' AND AATIPO <> 'NOR' "
                    rs1.Open(fSql, DB_DB2)
                    If Not rs1.EOF Then
                        qArchivo = "WMAFTRPEN"
                        tipPreliq = rs1("AATIPO").Value
                    Else
                        qArchivo = "ITP500"
                        qValor = Cantidad * rs("PECST").Value
                    End If
                    rs1.Close()
                Else
                    qArchivo = "ITP500"
                End If
            Else
                wrtLog("1", "[ No encontró orden " & aReg(6) & " Linea " & QueLinea() & " Producto " & aReg(7) & " ] [" & NumeroT & "]")
            End If
        Else
            qArchivo = "ITP500"
        End If
        rs.Close()

        If qArchivo.Trim <> "" Then
            fSql = " INSERT INTO " & qArchivo & "("
            vSql = " VALUES( "

            If qArchivo = "ITP500" Then
                mkSql(tDato.tStr, "MPPRID", "MP2")
                mkSql(tDato.tStr, "MPACTL", "Post")
                mkSql(tDato.tStr, "MPUSR", "MP2")
                mkSql(tDato.tNum, "MPDTNV", Format(Now(), "yyyyMMdd"))
                mkSql(tDato.tNum, "MPTMNV", Format(Now(), "HHmmss"))
                mkSql(tDato.tStr, "THMLOT", NumeroT)
                mkSql(tDato.tNum, "TVAL", qValor)
            Else
                mkSql(tDato.tNum, " NUMREG    ", CalcConsec("WMAFTRPEN", False, "99999999"), 0)        '//8P0   Numero Registro
                mkSql(tDato.tStr, " WATIPO    ", tipPreliq, 0)         '//3A    Tipo
                mkSql(tDato.tStr, " WAESTAD   ", "01", 0)
            End If

            mkSql(tDato.tStr, "TPROD", aReg(7))
            mkSql(tDato.tStr, "TTYPE", TipoTransacc)
            mkSql(tDato.tNum, "TQTY", Cantidad)

            mkSql(tDato.tStr, "TWHS", aReg(9))
            mkSql(tDato.tStr, "TLOCT", aReg(10))
            mkSql(tDato.tStr, "TCOM", aReg(11))
            If aReg(8) = "S1" Or aReg(8) = "S2" Or aReg(8) = "S4" Then
                If Left(aReg(11), 1) = "A" Or Left(aReg(11), 1) = "D" Then
                    mkSql(tDato.tStr, "TRES", "02")
                Else
                    mkSql(tDato.tStr, "TRES", "01")
                End If
            End If

            If UBound(aReg) >= 20 Then
                mkSql(tDato.tStr, "THTUM", aReg(20))
            End If
            'mkSql(tDato.tStr, "THADVN", NumeroT)
            mkSql(tDato.tStr, "THADVN", CalcConsec("TRANSEC", False, "99999999"))

            mkSql(tDato.tNum, "TTDTE", "20" + Replace(aReg(4), ".", ""))
            If IsNumeric(aReg(6)) Then
                mkSql(tDato.tNum, "TREF", CStr(CLng(aReg(6))))
            End If
            mkSql(tDato.tNum, "THLIN", QueLinea(), True)

            DB_DB2_Execute(fSql + vSql, r)
        Else
            r = 0
        End If

        Return (r <> 0)

    End Function

    '' ''Function RecibeOC(ByVal NumeroT As Double) As Boolean
    '' ''    Dim r As Integer = 0
    '' ''    Dim rs As New ADODB.Recordset
    '' ''    Dim rs1 As New ADODB.Recordset
    '' ''    Dim rs2 As New ADODB.Recordset
    '' ''    Dim bCantCost As Boolean = False

    '' ''    fSql = "SELECT * FROM HPOL01 WHERE PORD = " & CStr(CLng(aReg(6))) & " AND PPROD = '" & aReg(7) & "'"
    '' ''    fSql += " AND (PODEST = '" + aReg(19) + "')"
    '' ''   rs =  rs_Open(fSql, DB_DB2)
    '' ''    If rs.EOF Then
    '' ''        rs.Close()
    '' ''        Return False
    '' ''    End If

    '' ''    Cantidad = TraerCantidad()
    '' ''    'If Cantidad > rs("PQORD").Value - rs("PQREC").Value Then
    '' ''    '    rs.Close()
    '' ''    '    Return False
    '' ''    'End If

    '' ''    Dim qArchivo As String = ""
    '' ''    Dim qValor As Double = 0
    '' ''    Dim tipPreliq As String = ""

    '' ''    fSql = "SELECT AATIPO FROM RFALPRE "
    '' ''    fSql &= " WHERE AID IN('PA', 'LQ') AND AVEND = " & rs("PVEND").Value & " AND APROD = '" & rs("PPROD").Value & "' AND AATIPO <> 'NOR' "
    '' ''    rs1.Open(fSql, DB_DB2)
    '' ''    If Not rs1.EOF Then
    '' ''        qArchivo = "WMAFTRPEN"
    '' ''        tipPreliq = rs1("AATIPO").Value
    '' ''        If tipPreliq = "PRN" Or tipPreliq = "PRI" Then
    '' ''            TipoTransacc = "UI"
    '' ''        End If
    '' ''    Else
    '' ''        qArchivo = "PRDFOCREC"
    '' ''        qValor = Cantidad * rs("PECST").Value
    '' ''    End If
    '' ''    rs1.Close()

    '' ''    fSql = "SELECT * FROM ITEL01 WHERE TTYPE='" + TipoTransacc + "' AND TCENT='Y'"
    '' ''    rs2.Open(fSql, DB_DB2)
    '' ''    If Not rs2.EOF Then
    '' ''        If Left(TipoTransacc, 1) = "U" Then
    '' ''            If rs2("TAPC").Value = "Y" Then
    '' ''                bCantCost = True
    '' ''            End If
    '' ''        Else
    '' ''            qArchivo = "PRDFOCREC"
    '' ''        End If
    '' ''    Else
    '' ''        qArchivo = "PRDFOCREC"
    '' ''    End If
    '' ''    rs2.Close()

    '' ''    fSql = " INSERT INTO " & qArchivo & "("
    '' ''    vSql = " VALUES( "
    '' ''    If qArchivo = "PRDFOCREC" Then
    '' ''        mkSql(tDato.tStr, "MPPRID", "MP2")
    '' ''        mkSql(tDato.tStr, "MPACTL", "Recibe")
    '' ''        mkSql(tDato.tStr, "MPUSR", "MP2")
    '' ''        mkSql(tDato.tNum, "MPDTNV", Format(Now(), "yyyyMMdd"))
    '' ''        mkSql(tDato.tNum, "MPTMNV", Format(Now(), "HHmmss"))
    '' ''        mkSql(tDato.tStr, "THMLOT", NumeroT)
    '' ''    Else
    '' ''        mkSql(tDato.tNum, " NUMREG    ", CalcConsec("WMAFTRPEN", False, "99999999"), 0)        '//8P0   Numero Registro
    '' ''        mkSql(tDato.tStr, " WATIPO    ", tipPreliq, 0)         '//3A    Tipo
    '' ''        mkSql(tDato.tStr, " WAESTAD   ", "01", 0)
    '' ''    End If
    '' ''    mkSql(tDato.tNum, "BATCH", nBatch)    '//Batch
    '' ''    mkSql(tDato.tNum, "TREF", CStr(CLng(aReg(6))))
    '' ''    mkSql(tDato.tStr, "TPROD", aReg(7))
    '' ''    mkSql(tDato.tStr, "TTYPE", TipoTransacc)
    '' ''    mkSql(tDato.tNum, "TVAL", qValor)
    '' ''    mkSql(tDato.tNum, "TQTY", Cantidad)
    '' ''    If bCantCost Then
    '' ''        mkSql(tDato.tNum, "TQTYCO", Cantidad)
    '' ''    End If
    '' ''    mkSql(tDato.tStr, "TWHS", aReg(9))
    '' ''    mkSql(tDato.tStr, "TLOCT", aReg(10))
    '' ''    mkSql(tDato.tStr, "TCOM", aReg(11))
    '' ''    mkSql(tDato.tStr, "THADVN", NumeroT)
    '' ''    mkSql(tDato.tNum, "TTDTE", "20" + Replace(aReg(4), ".", ""))
    '' ''    mkSql(tDato.tNum, "THLIN", rs("PLINE").Value, True)
    '' ''    DB_DB2_Execute(fSql + vSql, r)

    '' ''    Return (r <> 0)

    '' ''End Function

    Function RecibeOC(ByVal NumeroT As Double) As Boolean
        Dim r As Integer = 0
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim rs2 As New ADODB.Recordset
        Dim bCantCost As Boolean = False

        fSql = "SELECT * FROM HPOL01 WHERE PORD = " & CStr(CLng(aReg(6))) & " AND PPROD = '" & aReg(7) & "'"
        fSql += " AND (PODEST = '" + aReg(19) + "')"
        rs = rs_Open(fSql, DB_DB2)
        If rs.EOF Then
            rs.Close()
            Return False
        End If

        Cantidad = TraerCantidad()
        'If Cantidad > rs("PQORD").Value - rs("PQREC").Value Then
        '    rs.Close()
        '    Return False
        'End If

        Dim qArchivo As String = ""
        Dim qValor As Double = 0
        Dim tipPreliq As String = ""

        fSql = "SELECT AATIPO FROM RFALPRE "
        fSql &= " WHERE AID IN('PA', 'LQ') AND AVEND = " & rs("PVEND").Value & " AND APROD = '" & rs("PPROD").Value & "' AND AATIPO <> 'NOR' "
        rs1.Open(fSql, DB_DB2)
        If Not rs1.EOF Then
            qArchivo = "WMAFTRPEN"
            tipPreliq = rs1("AATIPO").Value
        Else
            qArchivo = "PRDFOCREC"
            qValor = Cantidad * rs("PECST").Value
        End If
        rs1.Close()

        fSql = "SELECT * FROM ITEL01 WHERE TTYPE='" + TipoTransacc + "' AND TCENT='Y'"
        rs2.Open(fSql, DB_DB2)
        If Not rs2.EOF Then
            If Left(TipoTransacc, 1) = "U" Then
                If rs2("TAPC").Value = "Y" Then
                    bCantCost = True
                End If
            Else
                qArchivo = "PRDFOCREC"
            End If
        Else
            qArchivo = "PRDFOCREC"
        End If
        rs2.Close()

        fSql = " INSERT INTO " & qArchivo & "("
        vSql = " VALUES( "
        If qArchivo = "PRDFOCREC" Then
            mkSql(tDato.tStr, "MPPRID", "MP2")
            mkSql(tDato.tStr, "MPACTL", "Recibe")
            mkSql(tDato.tStr, "MPUSR", "MP2")
            mkSql(tDato.tNum, "MPDTNV", Format(Now(), "yyyyMMdd"))
            mkSql(tDato.tNum, "MPTMNV", Format(Now(), "HHmmss"))
            mkSql(tDato.tStr, "THMLOT", NumeroT)
        Else
            mkSql(tDato.tNum, " NUMREG    ", CalcConsec("WMAFTRPEN", False, "99999999"), 0)        '//8P0   Numero Registro
            mkSql(tDato.tStr, " WATIPO    ", tipPreliq, 0)         '//3A    Tipo
            mkSql(tDato.tStr, " WAESTAD   ", "01", 0)
        End If
        mkSql(tDato.tNum, "BATCH", nBatch)    '//Batch
        mkSql(tDato.tNum, "TREF", CStr(CLng(aReg(6))))
        mkSql(tDato.tStr, "TPROD", aReg(7))
        mkSql(tDato.tStr, "TTYPE", TipoTransacc)
        mkSql(tDato.tNum, "TVAL", qValor)
        mkSql(tDato.tNum, "TQTY", Cantidad)
        If bCantCost Then
            mkSql(tDato.tNum, "TQTYCO", Cantidad)
        End If
        mkSql(tDato.tStr, "TWHS", aReg(9))
        mkSql(tDato.tStr, "TLOCT", aReg(10))
        mkSql(tDato.tStr, "TCOM", aReg(11))
        mkSql(tDato.tStr, "THADVN", NumeroT)
        mkSql(tDato.tNum, "TTDTE", "20" + Replace(aReg(4), ".", ""))
        mkSql(tDato.tNum, "THLIN", rs("PLINE").Value, True)
        DB_DB2_Execute(fSql + vSql, r)

        Return (r <> 0)

    End Function
    Function QueLinea() As Integer
        Dim Sql As String
        Dim rs As New ADODB.Recordset
        Dim r As Integer = 0

        Sql = "SELECT * FROM ITEL01 WHERE TTYPE='" + TipoTransacc + "'"
        rs = rs_Open(Sql, DB_DB2)
        If rs.Fields("TPO").Value = "Y" And Not Movimiento Then
            rs.Close()
            Sql = "SELECT PLINE FROM HPO "
            Sql += " WHERE (PORD = " + CStr(CLng(aReg(6))) + ") "
            Sql += " AND (PODEST = '" + aReg(19) + "')"
            rs = rs_Open(Sql, DB_DB2)
            If Not rs.EOF Then
                r = rs.Fields(0).Value
            End If
            rs.Close()
        End If

        Return r

    End Function

    Function TraerCantidad() As Double
        Dim Sql As String
        Dim rs As New ADODB.Recordset
        Dim r As Double = 0

        Sql = " SELECT "
        Sql += " 	QTYRECEIVED"
        Sql += " FROM "
        Sql += " 	PORECEIV"
        Sql += " WHERE "
        Sql += " 	(PONUM = '" + CStr(CLng(aReg(6))) + "' ) "
        Sql += " 	AND (RELEASENUM = " + aReg(12) + ") "
        Sql += " 	AND (SEQNUM = " + aReg(13) + ") "
        Sql += " 	AND (RECEIPTNUM = " + aReg(14) + ")"
        rs = rs_Open(Sql, DB_mp2)
        If Not rs.EOF Then
            r = rs.Fields(0).Value
        End If
        rs.Close()

        Return r

    End Function

    Function CantidadTransacc() As TransaccEstado

        If Val(aReg(5)) <> 0 Then
            If aReg(15) = "Recepcion compra" Then
                Cantidad = TraerCantidad()
            Else
                Select Case aReg(16)
                    Case "RV", "IR", "MV", "IT"
                        Cantidad = -Val(aReg(5))
                    Case Else
                        Cantidad = Val(aReg(5))
                End Select
            End If
        Else
            wrtLog("1", "[ Transaccion con cantidad=" + aReg(5) + " ] [5]")
            Return TransaccEstado.EnError
        End If

        Return TransaccEstado.TrasaccOK

    End Function

    Sub BackupInput(ByVal NumeroT As Double, ByVal Mensaje As String)
        Dim Sql As String
        Dim r As Integer

        Sql = " INSERT INTO InputMp2Bck( Numero_t, Fecha_t, Registro_t, Mensaje_t ) "
        Sql += " SELECT Numero_t, Fecha_t, Registro_t, '" + Mensaje + "' FROM InputMp2"
        Sql += " WHERE Numero_t=" + CStr(NumeroT)
        DB_mp2_Execute(Sql, r)
        If r > 0 Then
            Sql = " DELETE FROM InputMp2"
            Sql += " WHERE Numero_t=" + CStr(NumeroT)
            DB_mp2_Execute(Sql)
        End If

    End Sub

    Sub BackupInput(ByVal NumerosT As String, ByVal Mensaje As String)
        Dim Sql As String
        Dim r As Integer

        Sql = " INSERT INTO InputMp2Bck( Numero_t, Fecha_t, Registro_t, Mensaje_t ) "
        Sql += " SELECT Numero_t, Fecha_t, Registro_t, '" + Mensaje + "' FROM InputMp2"
        Sql += " WHERE Numero_t IN(" + NumerosT + " )"
        DB_mp2_Execute(Sql, r)
        If r > 0 Then
            Sql = " DELETE FROM InputMp2"
            Sql += " WHERE Numero_t IN(" + NumerosT + " )"
            DB_mp2_Execute(Sql)
        End If
    End Sub

    Public Function SetTipoTransacc(ByRef chkSaldo As Boolean) As TransaccEstado
        Dim Sql As String
        Dim rs As New ADODB.Recordset
        Dim aSA() As String
        Dim r As TransaccEstado = TransaccEstado.TrasaccOK
        Dim posTransacc As String = "[ 8]"
        Dim Orden As String
        Dim Linea As String

        TipoTransacc = "*"
        Movimiento = False

        If aReg(16) = "MV" Or aReg(16) = "IT" Then
            'Movimiento entre almacenes
            Movimiento = True
            TipoTransacc = aReg(8)
        ElseIf aReg(8) = "UB" And aReg(16) = "SA" Then
            'Ajuste de Inventario
            If InStr(aReg(15), "$") > 0 Then
                aSA = Split(aReg(15), "$")
                TipoTransacc = UCase(Trim(aSA(1)))
                posTransacc = "[15]"
            Else
                wrtLog("1", "[ Falta transaccion para ajuste. " + aReg(15) + " ] [15]")
                Return TransaccEstado.EnError
            End If
        ElseIf aReg(8) = "A" And aReg(16) = "SA" Then
            'Ajuste de Inventario
            If InStr(aReg(15), "$") > 0 Then
                aSA = Split(aReg(15), "$")
                TipoTransacc = UCase(Trim(aSA(1)))
                posTransacc = "[ 15]"
            Else
                wrtLog("1", "[ Falta transaccion para ajuste. " + aReg(15) + " ] [15]")
                Return TransaccEstado.EnError
            End If
        Else
            TipoTransacc = aReg(8)
        End If

        Sql = "SELECT CCDESC, CCNOT1 FROM ZCCL01 WHERE CCTABL='RTRAMP2' AND CCCODE='" + TipoTransacc + "'"
        rs = rs_Open(Sql, DB_DB2)
        If Not rs.EOF Then
            If rs.Fields("CCNOT1").Value = "N" Then 'Marcadas con N no se envian
                rs.Close()
                Return TransaccEstado.NoAplica
            End If
            TipoTransacc = rs.Fields("CCDESC").Value
        Else
            rs.Close()
            wrtLog("1", "[ No se encontro parametrizacion de la Transaccion " + TipoTransacc + " ]" + posTransacc)
            Return TransaccEstado.EnError
        End If
        rs.Close()

        Sql = "SELECT * FROM ITEL01 WHERE TTYPE='" + TipoTransacc + "'"
        rs = rs_Open(Sql, DB_DB2)
        If rs.EOF Then
            wrtLog("1", "[ No se encontro la Transaccion " & TipoTransacc & " en ITEL01 ]")
            rs.Close()
            Return TransaccEstado.EnError
        End If

        'Revisa si la transacciones es de salida para que luego se revise el saldo
        chkSaldo = rs.Fields("TISS").Value = "Y"
        'Si es devolucion no revisa saldo
        If Left(aReg(8), 1) = "S" And aReg(16) = "IR" Then
            chkSaldo = False
        End If

        If Not Movimiento Then
            If IsNumeric(aReg(6)) Then
                Orden = CStr(CLng(aReg(6)))
            Else
                Orden = " -1"
            End If
            If IsNumeric(aReg(19)) Then
                Linea = aReg(19)
            Else
                Linea = " -1"
            End If
            If rs.Fields("TPO").Value = "Y" Then
                Sql = "SELECT * FROM HPO "
                Sql += " WHERE (PORD = " & Orden + ") "
                Sql += " AND (PODEST = '" + Linea + "')"
                If Not Busca(Sql, DB_DB2) Then
                    If Not BuscaAnterior(Orden, Linea) Then
                        wrtLog("1", "[ No se encontro Orden: " & aReg(6) & " Linea: " + aReg(19) + " ] [6 ] [19]")
                        r = TransaccEstado.EnError
                    End If
                End If
            End If

            If rs.Fields("TSO").Value = "Y" Then
                Sql = "SELECT SORD FROM FSO "
                Sql += " WHERE (SORD = " + Orden + ") "
                If Not Busca(Sql, DB_DB2) Then
                    wrtLog("1", "[ No se encontro Orden de Fabricacion ] [6]")
                    r = TransaccEstado.EnError
                End If
            End If

            If rs.Fields("TUM").Value = "Y" Then
                Sql = "SELECT CCCODE FROM ZCC WHERE (CCTABL = 'UNITMEAS') AND (CCCODE = '" + aReg(20) + "')"
                If Not Busca(Sql, DB_DB2) Then
                    wrtLog("1", "[ No se encontro Unidad de Medida ] [20]")
                    r = TransaccEstado.EnError
                End If
            End If

            If rs.Fields("TRO").Value = "Y" Then
                Sql = "SELECT COUNT(*) FROM ECH "
                Sql += " WHERE (HORD = " + Orden + ") "
                If Not Busca(Sql, DB_DB2) Then
                    wrtLog("1", "[ No se encontro Pedido ] [6]")
                    r = TransaccEstado.EnError
                End If
            End If
            rs.Close()
        End If
        Return r

    End Function

    Function BuscaAnterior(ByVal Orden As String, ByVal Linea As String) As Boolean
        Dim rs As New ADODB.Recordset
        Dim Sql As String
        Dim prod As String
        Dim cant As Double

        Sql = "SELECT * FROM purreq WHERE ponum='" & Orden & "' AND linenumber=" & Linea & " ORDER BY linenumber"
        rs = rs_Open(Sql, DB_mp2)
        If Not rs.EOF Then
            Sql = "SELECT PLINE FROM HPO "
            Sql += " WHERE PORD=" & Orden & " AND PPROD='" & rs.Fields("itemnum").Value & "'"
            Sql += " AND PQORD=" & rs.Fields("QTYREQUESTED").Value
            Sql += " AND PODEST = ''"
            Sql += " ORDER BY PLINE"
            prod = rs.Fields("itemnum").Value
            cant = rs.Fields("QTYREQUESTED").Value
            rs.Close()
            rs = rs_Open(Sql, DB_DB2)
            If Not rs.EOF Then
                Sql = "UPDATE HPO SET PODEST='" & Linea & "'"
                Sql += " WHERE PORD=" & Orden & " AND PPROD='" & prod & "'"
                Sql += " AND PQORD=" & cant
                Sql += " AND PODEST = ''"
                DB_DB2_Execute(Sql)
                rs.Close()
                Return True
            End If
        End If

        Return False

    End Function

End Module
