﻿Imports System.IO

Public Class wsScanPlanillas
    Private WithEvents m_timer As New Timers.Timer(60000)
    Private DB As clsConexion
    Public sEstado As String = ""

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Agregue el código aquí para iniciar el servicio. Este método debería poner
        ' en movimiento los elementos para que el servicio pueda funcionar.
        Me.EventLog1.WriteEntry("Inicia")
        m_timer.Enabled = True

    End Sub
    Private Sub m_timer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles m_timer.Elapsed

        Try
            m_timer.Enabled = False
            If Not AbreConexion("") Then
                Exit Sub
            End If
            revisaPlanillas()
            DB.Close()
        Catch ex As Exception
            DB.WrtSqlError(DB.lastSQL, ex.StackTrace & "<br />" & ex.Message)
            Me.EventLog1.WriteEntry(DB.lastSQL & vbCrLf & ex.Message & vbCrLf & ex.StackTrace)
        End Try
        m_timer.Enabled = True

    End Sub

    Sub revisaPlanillas()
        Dim fSql As String
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim rsT As New ADODB.Recordset
        Dim resultado As String = ""
        Dim qArchivo As String
        Dim aPlani() As String
        Dim Planilla As String
        Dim procesoScan As String = "0"
        Dim bHubo As Boolean = False
        Dim carpeta As String


        fSql = " SELECT TPROVE, TMAILCD FROM RTTRA WHERE TFLAG8 = 1 "
        DB.OpenRS(rsT, fSql)
        Do While Not rsT.EOF
            If procesoScan = "0" Then
                procesoScan = DB.CalcConsec("PROCESOSCAN", "99999999")
            End If
            carpeta = My.Settings.PLANILLAS_ORIGEN & rsT("TPROVE").Value & "\"
            qArchivo = Dir(carpeta & "*.*")
            Do While qArchivo <> ""
                aPlani = Split(qArchivo, ".")
                Planilla = CampoNum(aPlani(0))
                If ArchivoCerrado(carpeta & qArchivo) Then
                    If Val(Planilla) > 0 Then
                        fSql = " SELECT EID FROM RECC WHERE EPLANI = " & Planilla
                        If DB.OpenRS(rs, fSql) Then
                            DB.gsKeyWords = carpeta & qArchivo & " " & My.Settings.PLANILLAS_DESTINO & qArchivo
                            FileCopy(carpeta & qArchivo, My.Settings.PLANILLAS_DESTINO & qArchivo)
                            fSql = " UPDATE RECC SET EUDN01 = " & procesoScan & ", ESCAN=2, ESCANF = NOW(),"
                            fSql &= " EPLAJPEG = '" & My.Settings.PLANILLAS_DESTINO & qArchivo & "' "
                            fSql &= " WHERE EPLANI = " & Planilla
                            DB.ExecuteSQL(fSql)
                        Else
                            FileCopy(carpeta & qArchivo, My.Settings.PLANILLAS_NOPROCESADAS & qArchivo)
                            NoProcesado(My.Settings.PLANILLAS_NOPROCESADAS, qArchivo, rsT("TMAILCD").Value)
                        End If
                        rs.Close()
                    Else
                        FileCopy(carpeta & qArchivo, My.Settings.PLANILLAS_NOPROCESADAS & qArchivo)
                        NoProcesado(My.Settings.PLANILLAS_NOPROCESADAS, qArchivo, rsT("TMAILCD").Value)
                    End If
                    Kill(carpeta & qArchivo)
                End If
                qArchivo = Dir()
                bHubo = True
            Loop
            rsT.MoveNext()
        Loop
        rsT.Close()

        If bHubo Then
            fSql = "SELECT ECONSO FROM RECC "
            fSql &= " WHERE EUDN01 = " & procesoScan & " AND ECONSO <> '' AND EPLANI <> 0"
            fSql &= " GROUP BY ECONSO"
            DB.OpenRS(rs, fSql)
            Do While Not rs.EOF
                fSql = " SELECT ECONSO, EPLANI, IFNULL( MAX(DPLANI), -1 ) AS PLANILLA  "
                fSql = fSql & " FROM RECC LEFT JOIN RDCC ON ECONSO = DCONSO AND EPLANI = DPLANI AND  ESCAN = 2 "
                fSql = fSql & " WHERE ECONSO = '" & rs("ECONSO").Value & "'  "
                fSql = fSql & " GROUP BY ECONSO, EPLANI "
                fSql = fSql & " HAVING  IFNULL( MAX(DPLANI), -1 ) = -1 "
                If Not DB.OpenRS(rs1, fSql) Then
                    fSql = "UPDATE RHCCX SET XCSTS05 = 1 WHERE XCCONSO = '" & rs("ECONSO").Value & "' "
                    DB.ExecuteSQL(fSql)
                End If
                rs1.Close()
                rs.MoveNext()
            Loop
            rs.Close()
        End If

    End Sub

    'CampoNum()
    Function CampoNum(Dato) As String
        Dim i As Integer
        Dim Ascii As Integer
        Dim resultado As String = ""
        For i = 1 To Len(Dato)
            Ascii = Asc(Mid(Dato, i, 1))
            If Ascii >= Asc("0") And Ascii <= Asc("9") Then
                resultado &= Chr(Ascii)
            End If
        Next
        Return resultado

    End Function
    Function AbreConexion(ModuloActual As String) As Boolean

        DB = New clsConexion
        DB.gsDatasource = My.Settings.AS400
        DB.gsLib = My.Settings.AMBIENTE
        DB.ModuloActual = ModuloActual
        Return DB.Conexion()

    End Function

    Sub NoProcesado(carpeta As String, archivo As String, correo As String)
        Dim Alerta As New clsCorreoMHT
        With Alerta
            .Inicializa()
            .DB = DB
            .Referencia = "PLANILLAS FTP"
            .Asunto = "Error en Scan Planillas"
            .Modulo = "SCANPLANILLAS"
            .Referencia = "Scan Planillas"
            If correo.Trim <> "" Then
                .Destinatario = correo
            Else
                .Destinatario = .AvisarA()
            End If
            .AddLine("Archivo " & archivo & " scan de planilla ")
            .AddLine("no pudo ser procesado.")
            .AddLine("Renombrelo con el numero de planilla")
            .AddLine("y envielo nuevamente.")
            .Adjuntos = carpeta & archivo
            .EnviaCorreo()
        End With

    End Sub

    Function ArchivoCerrado(qArchivo As String) As Boolean
        Dim fs As FileStream
        Try
            fs = File.Open(qArchivo, FileMode.Open, FileAccess.Read, FileShare.None)
            fs.Close()
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function

    Protected Overrides Sub OnStop()
        ' Agregue el código aquí para realizar cualquier anulación necesaria para detener el servicio.
        m_timer.Stop()
    End Sub

End Class
