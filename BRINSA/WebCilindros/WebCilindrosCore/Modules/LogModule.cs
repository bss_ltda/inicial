﻿namespace WebCilindrosCore.Modules
{
    public class LogModule
    {
        Repository.DB2Repository<Entities.RFLOG> dB2Repository;

        public LogModule()
        {
            this.dB2Repository = new Repository.DB2Repository<Entities.RFLOG>();
        }

        public string AddLog(Entities.RFLOG log)
        {
            if (log is null)
                throw new System.NullReferenceException($"El log especificado es un valor nulo");

            string res = "";

            res = this.dB2Repository.Add(log);

            if (!res.Contains("ERROR"))
            {
                res = "OK";
            }

            return res;
        }

    }
}
