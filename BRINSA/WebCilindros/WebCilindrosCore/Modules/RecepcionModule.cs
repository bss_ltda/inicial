﻿namespace WebCilindrosCore.Modules
{
    public class RecepcionModule
    {
        Repository.DB2Repository<Entities.RFWCYLRECH> dB2Repository;

        public RecepcionModule()
        {
            this.dB2Repository = new Repository.DB2Repository<Entities.RFWCYLRECH>();
        }

        public void Add(Entities.RFWCYLRECH datos)
        {
            if (datos is null)
                throw new System.NullReferenceException($"La recepción especificada es un valor nulo");

            string res = "";

            res = this.dB2Repository.Add(datos);

            if (res.Contains("ERROR"))
            {
                throw new System.Exception($"{{ \"Message\": \"{res}\" }}");
            }
        }

    }
}
