﻿using System.Collections.Generic;
using System.Linq.Expressions;

namespace WebCilindrosCore.Modules
{
    public class RecepcionDetalleModule
    {
        Repository.DB2Repository<Entities.RFWCYLRECL> dB2Repository;

        public RecepcionDetalleModule()
        {
            this.dB2Repository = new Repository.DB2Repository<Entities.RFWCYLRECL>();
        }

        public void Add(Entities.RFWCYLRECL datos)
        {
            if (datos is null)
                throw new System.NullReferenceException($"El detalle especificado es un valor nulo");

            string res = "";

            res = this.dB2Repository.Add(datos);

            if (res.Contains("ERROR"))
            {
                throw new System.Exception($"{{ \"Message\": \"{res}\" }}");
            }
        }

        public void Upd(Entities.RFWCYLRECL datos)
        {
            if (datos is null)
                throw new System.NullReferenceException($"El detalle especificado es un valor nulo");

            string res = "";

            res = this.dB2Repository.Update(datos);

            if (res.Contains("ERROR"))
            {
                throw new System.Exception($"{{ \"Message\": \"{res}\" }}");
            }
        }

        public void Del(string Linea)
        {
            string res = "";

            res = this.dB2Repository.ExecutePlainQuery($"DELETE FROM RFWCYLRECL WHERE RECEPCION_LINEA = '{Linea}'");

            if (res.Contains("ERROR"))
            {
                throw new System.Exception($"{{ \"Message\": \"{res}\" }}");
            }
        }

        public IEnumerable<Entities.RFWCYLRECL> GetByPredicate(Expression<System.Func<Entities.RFWCYLRECL, bool>> predicate)
        {
            return dB2Repository.Find(predicate);
        }
    }
}
