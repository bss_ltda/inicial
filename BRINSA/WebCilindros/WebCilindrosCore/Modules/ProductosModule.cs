﻿using System.Collections.Generic;
using System.Linq.Expressions;

namespace WebCilindrosCore.Modules
{
    public class ProductosModule
    {
        Repository.DB2Repository<Entities.RIIMC> dB2Repository;

        public ProductosModule()
        {
            this.dB2Repository = new Repository.DB2Repository<Entities.RIIMC>();
        }

        public IEnumerable<Entities.RIIMC> GetAll()
        {
            return dB2Repository.GetAll();
        }

        public IEnumerable<Entities.RIIMC> GetByPredicate(Expression<System.Func<Entities.RIIMC, bool>> predicate)
        {
            return dB2Repository.Find(predicate);
        }

        public void DelByCod(string Codigo)
        {           
            string res = "";

            res = this.dB2Repository.ExecutePlainQuery($"DELETE FROM RIIMC WHERE RCPROD = '{Codigo}'");

            if (res.Contains("ERROR"))
            {
                throw new System.Exception($"{{ \"Message\": \"{res}\" }}");
            }
        }

        public void Add(Entities.RIIMC datos)
        {
            if (datos is null)
                throw new System.NullReferenceException($"El producto especificado es un valor nulo");

            string res = "";

            res = this.dB2Repository.Add(datos);

            if (res.Contains("ERROR"))
            {
                throw new System.Exception($"{{ \"Message\": \"{res}\" }}");
            }
        }

        public void Upd(Entities.RIIMC datos)
        {
            if (datos is null)
                throw new System.NullReferenceException($"El producto especificado es un valor nulo");

            string res = "";

            res = this.dB2Repository.Update(datos);

            if (res.Contains("ERROR"))
            {
                throw new System.Exception($"{{ \"Message\": \"{res}\" }}");
            }
        }
    }
}
