﻿using WebCilindrosCore.Repository;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace WebCilindrosCore.Modules
{
    public class TablaParamModule
    {
        DB2Repository<Entities.RFPARAM> dB2Repository;

        public TablaParamModule()
        {
            this.dB2Repository = new DB2Repository<Entities.RFPARAM>();
        }

        public IEnumerable<Entities.RFPARAM> GetByPredicate(Expression<System.Func<Entities.RFPARAM, bool>> predicate)
        {
            return dB2Repository.Find(predicate);
        }

        public void AddCECO(Entities.RFPARAM datos)
        {
            if (datos is null)
                throw new System.NullReferenceException($"El param especificado es un valor nulo");

            string res = "";

            res = this.dB2Repository.Add(datos);

            if (res.Contains("ERROR"))
            {                
                throw new System.Exception($"{{ \"Message\": \"{res}\" }}");
            }
        }

        public void UpdCECO(int Id, string CECO)
        {
            string res = "";

            StringBuilder query = new StringBuilder();

            query.Append(" UPDATE RFPARAM SET");
            query.Append($" CCCODE2  = '{CECO}'");
            query.Append($" WHERE CCNUM = {Id}");

            res = this.dB2Repository.ExecutePlainQuery(query.ToString());

            if (res.Contains("ERROR"))
            {
                throw new System.Exception($"{{ \"Message\": \"{res}\" }}");
            }
        }

        public void DelCECO(int Id)
        {
            string res = "";

            StringBuilder query = new StringBuilder();

            query.Append(" DELETE FROM RFPARAM ");            
            query.Append($" WHERE CCNUM = {Id}");

            res = this.dB2Repository.ExecutePlainQuery(query.ToString());

            if (res.Contains("ERROR"))
            {
                throw new System.Exception($"{{ \"Message\": \"{res}\" }}");
            }
        }

        public void Upd(Entities.RFPARAM datos)
        {
            if (datos is null)
                throw new System.NullReferenceException($"El param especificado es un valor nulo");

            string res = "";

            res = this.dB2Repository.Update(datos);

            if (res.Contains("ERROR"))
            {
                throw new System.Exception($"{{ \"Message\": \"{res}\" }}");
            }
        }

        public void Add(Entities.RFPARAM datos)
        {
            if (datos is null)
                throw new System.NullReferenceException($"El param especificado es un valor nulo");

            string res = "";

            res = this.dB2Repository.Add(datos);

            if (res.Contains("ERROR"))
            {
                throw new System.Exception($"{{ \"Message\": \"{res}\" }}");
            }
        }

    }
}
