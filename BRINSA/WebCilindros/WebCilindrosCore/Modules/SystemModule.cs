﻿using IBM.Data.DB2.iSeries;
using WebCilindrosCore.Repository;
using System.Configuration;
using System.Data;

namespace WebCilindrosCore.Modules
{
    public class SystemModule
    {
        DB2Repository<dynamic> dB2Repository;

        public SystemModule()
        {
            dB2Repository = new DB2Repository<dynamic>();
        }

        public string GetLibrerias()
        {            
            string lib = "";

            using (IDbConnection dCon = new iDB2Connection(ConfigurationManager.ConnectionStrings["ConexionDB2"].ToString()))
            {
                lib = ((iDB2Connection)dCon).LibraryList;
            }

            return lib;
        }
    }
}
