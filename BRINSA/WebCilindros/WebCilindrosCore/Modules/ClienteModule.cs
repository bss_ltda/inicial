﻿using System.Collections.Generic;
using System.Linq.Expressions;

namespace WebCilindrosCore.Modules
{
    public class ClienteModule
    {
        Repository.DB2Repository<Entities.RCM> dB2Repository;

        public ClienteModule()
        {
            this.dB2Repository = new Repository.DB2Repository<Entities.RCM>();
        }

        public IEnumerable<Entities.RCM> GetWhere(string query)
        {
            return dB2Repository.ExecuteQuery(query);
        }
    }
}
