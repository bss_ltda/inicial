﻿using WebCilindrosCore.Repository;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace WebCilindrosCore.Modules
{
    public class ModuloUsuarioModule
    {
        DB2Repository<Entities.RCAA> dB2Repository;

        public ModuloUsuarioModule()
        {
            this.dB2Repository = new DB2Repository<Entities.RCAA>();
        }

        public IEnumerable<Entities.RCAA> GetByPredicate(Expression<System.Func<Entities.RCAA, bool>> predicate)
        {
            return dB2Repository.Find(predicate);
        }

        public string AddUsr(string Usr, bool ad)
        {
            string res = "";
            StringBuilder query = new StringBuilder();

            query.Append(" INSERT INTO RCAA");
            query.Append(" (ADEFAULT, ASTS, AAPP, AMOD, ADESC, AUSR, ACC00)");
            query.Append(" VALUES('1', 'A', 'WebCilindros', 'WebCilindros',");
            query.Append(" 'Manufactura Multimodo',");
            query.Append($" '{Usr}',");

            if (ad)
            {
                query.Append(" 'X')");
            }
            else
            {
                query.Append(" ' ')");
            }
            
            try
            {
                dB2Repository.ExecutePlainQuery(query.ToString());
                res = "OK";
            }           
            catch (System.Exception ex)
            {
                res = ex.Message;
            }
            return res;
        }

        public string UpdUsr(string Usr, bool ad)
        {
            string res = "";
            StringBuilder query = new StringBuilder();

            query.Append(" UPDATE RCAA SET");
            query.Append(" ACC00 = ");
            if (ad)
            {
                query.Append(" 'X'");
            }
            else
            {
                query.Append(" ' '");
            }
            query.Append($" WHERE AUSR = '{Usr}'");
            query.Append(" AND AAPP = 'WebCilindros' AND AMOD = 'WebCilindros'");
            
            try
            {
                dB2Repository.ExecutePlainQuery(query.ToString());
                res = "OK";
            }
            catch (System.Exception ex)
            {
                res = ex.Message;
            }
            return res;
        }
    }
}
