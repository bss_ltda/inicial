﻿using System.ComponentModel.DataAnnotations;

namespace WebCilindrosCore.Entities
{
    public class RFWCYLRECH
    {
        private string _OBSERVACIONES;
        private string _CRTUSR;

        [Display(Name = "FECRECEPCION")]
        [Timestamp]
        public System.DateTime? FECRECEPCION { get; set; }
        [Key]
        [Display(Name = "RECEPCION")]
        public string RECEPCION { get; set; }
        [Display(Name = "CLIENTE")]
        public int CLIENTE { get; set; }
        [Display(Name = "OBSERVACIONES")]
        public string OBSERVACIONES { get => _OBSERVACIONES?.Trim(); set => _OBSERVACIONES = value; }
        [Display(Name = "CRTUSR")]
        public string CRTUSR { get => _CRTUSR?.Trim(); set => _CRTUSR = value; }
    }
}
