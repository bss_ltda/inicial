﻿using System.ComponentModel.DataAnnotations;

namespace WebCilindrosCore.Entities
{
    public class RFWCYLRECL
    {
        private string _ESTADO;
        private string _SUBESTADO;
        private string _CYLNUMERO;
        private string _CYLCODBARRAS;
        private string _TARAUM;
        private string _USR_OTRO_CLIE;
        private string _USR_SIN_CUAR;
        private string _CONSO;
        private string _CUSTNEWUSR;
        private string _CUSTNUEVOOBSERV;
        private string _OBSERVACION;

        [Display(Name = "RECEPCION")]
        public string RECEPCION { get; set; }
        [Display(Name = "REINSPEC")]
        public int REINSPEC { get; set; }
        [Key]
        [Display(Name = "RECEPCION_LINEA")]
        public string RECEPCION_LINEA { get; set; }
        [Display(Name = "FECRECEPCION")]
        [Timestamp]
        public System.DateTime? FECRECEPCION { get; set; }
        [Display(Name = "ESTADO")]
        public string ESTADO { get => _ESTADO?.Trim(); set => _ESTADO = value; }
        [Display(Name = "SUBESTADO")]
        public string SUBESTADO { get => _SUBESTADO?.Trim(); set => _SUBESTADO = value; }
        [Display(Name = "CYLNUMERO")]
        public string CYLNUMERO { get => _CYLNUMERO?.Trim(); set => _CYLNUMERO = value; }
        [Display(Name = "CYLCODBARRAS")]
        public string CYLCODBARRAS { get => _CYLCODBARRAS?.Trim(); set => _CYLCODBARRAS = value; }
        [Display(Name = "TARAACTUAL")]
        public double TARAACTUAL { get; set; }
        [Display(Name = "TARAUM")]
        public string TARAUM { get => _TARAUM?.Trim(); set => _TARAUM = value; }
        [Display(Name = "CAPUCHON")]
        public int CAPUCHON { get; set; }
        [Display(Name = "TAPON")]
        public int TAPON { get; set; }
        [Display(Name = "VALVULA")]
        public int VALVULA { get; set; }
        [Display(Name = "DANOVALVULA")]
        public int DANOVALVULA { get; set; }
        [Display(Name = "CORROSION")]
        public int CORROSION { get; set; }
        [Display(Name = "CORTES")]
        public int CORTES { get; set; }
        [Display(Name = "ABOLLADURAS")]
        public int ABOLLADURAS { get; set; }
        [Display(Name = "PROTUBERANCIAS")]
        public int PROTUBERANCIAS { get; set; }
        [Display(Name = "DANOFUEGO")]
        public int DANOFUEGO { get; set; }
        [Display(Name = "ABRASION")]
        public int ABRASION { get; set; }
        [Display(Name = "DANOROSCA")]
        public int DANOROSCA { get; set; }
        [Display(Name = "F_DESGASE")]
        [Timestamp]
        public System.DateTime? F_DESGASE { get; set; }
        [Display(Name = "F_LLENAR")]
        [Timestamp]
        public System.DateTime? F_LLENAR { get; set; }
        [Display(Name = "PLANILLACARGUE")]
        public int PLANILLACARGUE { get; set; }
        [Display(Name = "F_CARGUE")]
        public System.DateTime? F_CARGUE { get; set; }
        [Display(Name = "DESP_OTRO_CLIE")]
        public int DESP_OTRO_CLIE { get; set; }
        [Display(Name = "USR_OTRO_CLIE")]
        public string USR_OTRO_CLIE { get => _USR_OTRO_CLIE?.Trim(); set => _USR_OTRO_CLIE = value; }
        [Display(Name = "F_OTRO_CLIE")]
        [Timestamp]
        public System.DateTime? F_OTRO_CLIE { get; set; }
        [Display(Name = "CLIENTEDESP")]
        public int CLIENTEDESP { get; set; }
        [Display(Name = "SIN_CUAR")]
        public int SIN_CUAR { get; set; }
        [Display(Name = "USR_SIN_CUAR")]
        public string USR_SIN_CUAR { get => _USR_SIN_CUAR?.Trim(); set => _USR_SIN_CUAR = value; }
        [Display(Name = "F_USR_SIN_CUAR")]
        [Timestamp]
        public System.DateTime? F_USR_SIN_CUAR { get; set; }
        [Display(Name = "F_CUARENTENA")]
        [Timestamp]
        public System.DateTime? F_CUARENTENA { get; set; }
        [Display(Name = "F_PREPDESPACHO")]
        [Timestamp]
        public System.DateTime? F_PREPDESPACHO { get; set; }
        [Display(Name = "F_DESPACHO")]
        [Timestamp]
        public System.DateTime? F_DESPACHO { get; set; }
        [Display(Name = "F_PHD")]
        [Timestamp]
        public System.DateTime? F_PHD { get; set; }
        [Display(Name = "F_LAVADO")]
        [Timestamp]
        public System.DateTime? F_LAVADO { get; set; }
        [Display(Name = "F_PINTADO")]
        [Timestamp]
        public System.DateTime? F_PINTADO { get; set; }
        [Display(Name = "F_BAJA")]
        [Timestamp]
        public System.DateTime? F_BAJA { get; set; }
        [Display(Name = "CUASA_BAJA")]
        public string CUASA_BAJA { get; set; }
        [Display(Name = "F_REINSPEC")]
        [Timestamp]
        public System.DateTime? F_REINSPEC { get; set; }
        [Display(Name = "PEDIDO")]
        public int PEDIDO { get; set; }
        [Display(Name = "CONSO")]
        public string CONSO { get => _CONSO?.Trim(); set => _CONSO = value; }
        [Display(Name = "DESGASE")]
        public int DESGASE { get; set; }
        [Display(Name = "LLENANDO")]
        public int LLENANDO { get; set; }
        [Display(Name = "ENCUARENTENA")]
        public int ENCUARENTENA { get; set; }
        [Display(Name = "PREPDESPACHO")]
        public int PREPDESPACHO { get; set; }
        [Display(Name = "DESPACHADO")]
        public int DESPACHADO { get; set; }
        [Display(Name = "CUSTODIO")]
        public int CUSTODIO { get; set; }
        [Display(Name = "REV_CUSTOD")]
        public int REV_CUSTOD { get; set; }
        [Display(Name = "CUSTOD_NUEVO")]
        public int CUSTOD_NUEVO { get; set; }
        [Display(Name = "CUSTNEWUSR")]
        public string CUSTNEWUSR { get => _CUSTNEWUSR?.Trim(); set => _CUSTNEWUSR = value; }
        [Display(Name = "F_CUSTNEW")]
        [Timestamp]
        public System.DateTime? F_CUSTNEW { get; set; }
        [Display(Name = "CUSTNUEVOOBSERV")]
        public string CUSTNUEVOOBSERV { get => _CUSTNUEVOOBSERV?.Trim(); set => _CUSTNUEVOOBSERV = value; }
        [Display(Name = "OBSERVACION")]
        public string OBSERVACION { get => _OBSERVACION?.Trim(); set => _OBSERVACION = value; }
    }
}
