﻿using System.ComponentModel.DataAnnotations;

namespace WebCilindrosCore.Entities
{
    public class RCM
    {
        private string _CMID;
        private string _CNME;
        private string _CAD1;
        private string _CAD2;
        private string _CAD3;
        private string _CSTE;
        private string _CZIP;
        private string _CCOUN;
        private string _CTYPE;
        private string _CPHON;
        private string _CTERM;
        private string _CDISC;
        private string _CTAX;
        private string _CCOM;
        private string _CLOC;
        private string _CTAXE;
        private string _CREG;
        private string _CPCD;
        private string _CCON;
        private string _COCDG;
        private string _CDEA1;
        private string _CDEA2;
        private string _CWHSE;
        private string _CTXID;
        private string _CCURR;
        private string _CMABC;
        private string _CMSIC;
        private string _CMDUNN;
        private string _CMDUNR;
        private string _CMFTXC;
        private string _CMALPH;
        private string _CROUT;
        private string _CMSTTP;
        private string _CMXNBR;
        private string _CMXCRT;
        private string _CREF01;
        private string _CREF02;
        private string _CREF03;
        private string _CREF04;
        private string _CREF05;
        private string _CMSUBS;
        private string _CMINVC;
        private string _CMCDTP;
        private string _CMCDST;
        private string _CMDLTS;
        private string _CMDPFX;
        private string _CMPRCC;
        private string _CMPRNO;
        private string _CMACKN;
        private string _CMSHIP;
        private string _CMINVP;
        private string _CALR;
        private string _CCARR;
        private string _CMNTR;
        private string _CTRMC;
        private string _CAPPR;
        private string _CASNR;
        private string _CLUSR;
        private string _CMBANK;
        private string _CMFAXN;
        private string _CMDATN;
        private string _CMSOFR;
        private string _CMDPT1;
        private string _CMDPT2;
        private string _CMDPT3;
        private string _CMEXCN;
        private string _CMAD4;
        private string _CMAD5;
        private string _CMAD6;
        private string _CDATN;
        private string _CMDFOT;
        private string _CMFF01;
        private string _CMFF02;
        private string _CMFF03;
        private string _CMFF04;
        private string _CMFF05;
        private string _CMFF06;
        private string _CMHOLD;
        private string _CMLANG;
        private string _CMPREG;
        private string _CMENUS;
        private string _CMPARM;
        private string _CMROUN;
        private string _CMRELH;
        private string _CMDCIH;
        private string _CMRREC;
        private string _CMJREC;
        private string _CMSUPP;
        private string _CMAUTO;
        private string _CMSEQC;
        private string _CMSSTW;
        private string _CMSBCU;
        private string _CMPREQ;
        private string _CMCPAL;
        private string _CMSEQN;
        private string _CMSSCF;
        private string _CMSSFW;
        private string _CMOUTQ;
        private string _CMSBRF;
        private string _CMCSTW;
        private string _CMSFMT;
        private string _CMPBDP;
        private string _CMSBDP;
        private string _CMDOCK;
        private string _CMISSP;
        private string _CMINPK;
        private string _CMCHOR;
        private string _CMRPDO;
        private string _CMLBLD;
        private string _CMASSI;
        private string _CMPABI;
        private string _CMDUN4;
        private string _CMUCC;
        private string _CMAIAG;
        private string _CMEAN;
        private string _CMNXCN;
        private string _CMNXCL;
        private string _CMNXCU;
        private string _CMNXPN;
        private string _CMNXPL;
        private string _CMNXPU;
        private string _CMBSLF;
        private string _CMACPT;
        private string _CMACUR;
        private string _CMRCSR;
        private string _CMCAKG;
        private string _CMINMC;
        private string _CMUF01;
        private string _CMUF02;
        private string _CMUF03;
        private string _CMUF04;
        private string _CMUF05;
        private string _CMUF06;
        private string _CMUF07;
        private string _CMUF08;
        private string _CMUF09;
        private string _CMFU10;
        private string _CMCINF;
        private string _CMOVSD;
        private string _CMURDF;
        private string _CMOLAQ;
        private string _CMGRCD;
        private string _CMBANO;
        private string _CMAGEN;
        private string _CMCCTA;
        private string _CMDGTC;
        private string _CMCRDS;
        private string _CMPRVA;
        private string _CMPGDE;
        private string _CMPRIN;
        private string _CMSPAG;
        private string _CMBBAP;
        private string _CMFDAP;
        private string _CMSTS;
        private string _CMLVL;
        private string _CMDMPR;
        private string _CMPST2;
        private string _CMARCF;
        private string _CMPICF;
        private string _CMRFCF;
        private string _CMSTCF;
        private string _CMPOCF;
        private string _CMPPCF;
        private string _CMINCF;
        private string _CMLBCF;
        private string _CMIXCF;
        private string _CMSACF;
        private string _CMSHCF;
        private string _CMCCCF;
        private string _CMEXAU;
        private string _CMRGCD;

        [Display(Name = "Record ID (CM/CZ)")]
        public string CMID { get => _CMID?.Trim(); set => _CMID = value; }
        [Display(Name = "Customer Number")]
        public int CCUST { get; set; }
        [Display(Name = "Customer Name")]
        public string CNME { get => _CNME?.Trim(); set => _CNME = value; }
        [Display(Name = "Company Address Line 1")]
        public string CAD1 { get => _CAD1?.Trim(); set => _CAD1 = value; }
        [Display(Name = "Company Address Line 2")]
        public string CAD2 { get => _CAD2?.Trim(); set => _CAD2 = value; }
        [Display(Name = "Company Address Line 3")]
        public string CAD3 { get => _CAD3?.Trim(); set => _CAD3 = value; }
        [Display(Name = "State Code")]
        public string CSTE { get => _CSTE?.Trim(); set => _CSTE = value; }
        [Display(Name = "Customer Postal Code")]
        public string CZIP { get => _CZIP?.Trim(); set => _CZIP = value; }
        [Display(Name = "Amount Due")]
        public double CAMTD { get; set; }
        [Display(Name = "Credit Limit Amount")]
        public double CRDOL { get; set; }
        [Display(Name = "Customer Country Code")]
        public string CCOUN { get => _CCOUN?.Trim(); set => _CCOUN = value; }
        [Display(Name = "Customer Type")]
        public string CTYPE { get => _CTYPE?.Trim(); set => _CTYPE = value; }
        [Display(Name = "Open Order Amount")]
        public double COPEN { get; set; }
        [Display(Name = "Year to Date Sales Amount")]
        public double CYSAL { get; set; }
        [Display(Name = "MTD Sales Amount")]
        public double CMSAL { get; set; }
        [Display(Name = "Last YTD Sales Amount")]
        public double CLYTD { get; set; }
        [Display(Name = "Sales Month 01")]
        public double CSL01 { get; set; }
        [Display(Name = "Sales Month 02")]
        public double CSL02 { get; set; }
        [Display(Name = "Sales Month 03")]
        public double CSL03 { get; set; }
        [Display(Name = "Sales Month 04")]
        public double CSL04 { get; set; }
        [Display(Name = "Sales Month 05")]
        public double CSL05 { get; set; }
        [Display(Name = "Sales Month 06")]
        public double CSL06 { get; set; }
        [Display(Name = "Sales Month 07")]
        public double CSL07 { get; set; }
        [Display(Name = "Sales Month 08")]
        public double CSL08 { get; set; }
        [Display(Name = "Sales Month 09")]
        public double CSL09 { get; set; }
        [Display(Name = "Sales Month 10")]
        public double CSL10 { get; set; }
        [Display(Name = "Sales Month 11")]
        public double CSL11 { get; set; }
        [Display(Name = "Sales Month 12")]
        public double CSL12 { get; set; }
        [Display(Name = "Year to Date Cost of Sales")]
        public double CYCOS { get; set; }
        [Display(Name = "Last YTD Cost of Sales")]
        public double CLYCS { get; set; }
        [Display(Name = "MTD Cost")]
        public double CMCOS { get; set; }
        [Display(Name = "Cost of Sales Month 01")]
        public double CCS01 { get; set; }
        [Display(Name = "Cost of Sales Month 02")]
        public double CCS02 { get; set; }
        [Display(Name = "Cost of Sales Month 03")]
        public double CCS03 { get; set; }
        [Display(Name = "Cost of Sales Month 04")]
        public double CCS04 { get; set; }
        [Display(Name = "Cost of Sales Month 05")]
        public double CCS05 { get; set; }
        [Display(Name = "Cost of Sales Month 06")]
        public double CCS06 { get; set; }
        [Display(Name = "Cost of Sales Month 07")]
        public double CCS07 { get; set; }
        [Display(Name = "Cost of Sales Month 08")]
        public double CCS08 { get; set; }
        [Display(Name = "Cost of Sales Month 09")]
        public double CCS09 { get; set; }
        [Display(Name = "Cost of Sales Month 10")]
        public double CCS10 { get; set; }
        [Display(Name = "Cost of Sales Month 11")]
        public double CCS11 { get; set; }
        [Display(Name = "Cost of Sales Month 12")]
        public double CCS12 { get; set; }
        [Display(Name = "Days Credit Limit")]
        public int CDLIM { get; set; }
        [Display(Name = "Phone Number")]
        public string CPHON { get => _CPHON?.Trim(); set => _CPHON = value; }
        [Display(Name = "Customer Salesman")]
        public int CSAL { get; set; }
        [Display(Name = "Terms Code")]
        public string CTERM { get => _CTERM?.Trim(); set => _CTERM = value; }
        [Display(Name = "Average Pay Days")]
        public int CAPD { get; set; }
        [Display(Name = "Average Invoice Size")]
        public double CAIS { get; set; }
        [Display(Name = "Last Transaction Date")]
        public int CLAST { get; set; }
        [Display(Name = "Customer Discount Code")]
        public string CDISC { get => _CDISC?.Trim(); set => _CDISC = value; }
        [Display(Name = "Customer Tax Code")]
        public string CTAX { get => _CTAX?.Trim(); set => _CTAX = value; }
        [Display(Name = "Commission Code")]
        public string CCOM { get => _CCOM?.Trim(); set => _CCOM = value; }
        [Display(Name = "Ship To Key")]
        public int CSHIP { get; set; }
        [Display(Name = "Company Number")]
        public int CCOMP { get; set; }
        [Display(Name = "Profit Center")]
        public string CLOC { get => _CLOC?.Trim(); set => _CLOC = value; }
        [Display(Name = "Corporate Customer Number")]
        public int CCCUS { get; set; }
        [Display(Name = "Tax Reporting Bypass Flag")]
        public string CTAXE { get => _CTAXE?.Trim(); set => _CTAXE = value; }
        [Display(Name = "Promotion Region")]
        public string CREG { get => _CREG?.Trim(); set => _CREG = value; }
        [Display(Name = "Ship Via Key")]
        public int CSHV { get; set; }
        [Display(Name = "Payment Code")]
        public string CPCD { get => _CPCD?.Trim(); set => _CPCD = value; }
        [Display(Name = "Contact Name")]
        public string CCON { get => _CCON?.Trim(); set => _CCON = value; }
        [Display(Name = "Order Special Item")]
        public string COCDG { get => _COCDG?.Trim(); set => _COCDG = value; }
        [Display(Name = "Oldest Invoice Days")]
        public int COID { get; set; }
        [Display(Name = "Group Code 1")]
        public string CDEA1 { get => _CDEA1?.Trim(); set => _CDEA1 = value; }
        [Display(Name = "Group Code 2")]
        public string CDEA2 { get => _CDEA2?.Trim(); set => _CDEA2 = value; }
        [Display(Name = "Default Warehouse")]
        public string CWHSE { get => _CWHSE?.Trim(); set => _CWHSE = value; }
        [Display(Name = "Tax ID Number")]
        public string CTXID { get => _CTXID?.Trim(); set => _CTXID = value; }
        [Display(Name = "Last Payment Date")]
        public int CLPDT { get; set; }
        [Display(Name = "Last Payment Amount")]
        public double CLPAM { get; set; }
        [Display(Name = "Date Account Opened")]
        public int CFRDA { get; set; }
        [Display(Name = "Currency Code")]
        public string CCURR { get => _CCURR?.Trim(); set => _CCURR = value; }
        [Display(Name = "ABC Code")]
        public string CMABC { get => _CMABC?.Trim(); set => _CMABC = value; }
        [Display(Name = "SIC Code")]
        public string CMSIC { get => _CMSIC?.Trim(); set => _CMSIC = value; }
        [Display(Name = "DUN Number")]
        public string CMDUNN { get => _CMDUNN?.Trim(); set => _CMDUNN = value; }
        [Display(Name = "DUN Ranking")]
        public string CMDUNR { get => _CMDUNR?.Trim(); set => _CMDUNR = value; }
        [Display(Name = "Fiscal Tax Code")]
        public string CMFTXC { get => _CMFTXC?.Trim(); set => _CMFTXC = value; }
        [Display(Name = "Alpha Search Key")]
        public string CMALPH { get => _CMALPH?.Trim(); set => _CMALPH = value; }
        [Display(Name = "Route Code")]
        public string CROUT { get => _CROUT?.Trim(); set => _CROUT = value; }
        [Display(Name = "Statement Type")]
        public string CMSTTP { get => _CMSTTP?.Trim(); set => _CMSTTP = value; }
        [Display(Name = "Outstanding Drafts")]
        public double CMOPEN { get; set; }
        [Display(Name = "Created On Date")]
        public int CMDCRT { get; set; }
        [Display(Name = "Tax Exemption Number")]
        public string CMXNBR { get => _CMXNBR?.Trim(); set => _CMXNBR = value; }
        [Display(Name = "Exemption Certificate")]
        public string CMXCRT { get => _CMXCRT?.Trim(); set => _CMXCRT = value; }
        [Display(Name = "Expiration Date")]
        public int CMXDTE { get; set; }
        [Display(Name = "Maximum Tax Exemption Amount")]
        public double CMXMAX { get; set; }
        [Display(Name = "Back Order Code")]
        public int CBO { get; set; }
        [Display(Name = "Priority Code Allocation")]
        public int CALOPR { get; set; }
        [Display(Name = "Group Sales Analysis Field")]
        public string CREF01 { get => _CREF01?.Trim(); set => _CREF01 = value; }
        [Display(Name = "Group Sales Analysis Field")]
        public string CREF02 { get => _CREF02?.Trim(); set => _CREF02 = value; }
        [Display(Name = "Group Sales Analysis Field")]
        public string CREF03 { get => _CREF03?.Trim(); set => _CREF03 = value; }
        [Display(Name = "Group Sales Analysis Field")]
        public string CREF04 { get => _CREF04?.Trim(); set => _CREF04 = value; }
        [Display(Name = "Group Sales Analysis Field")]
        public string CREF05 { get => _CREF05?.Trim(); set => _CREF05 = value; }
        [Display(Name = "Allow Item Substitution")]
        public string CMSUBS { get => _CMSUBS?.Trim(); set => _CMSUBS = value; }
        [Display(Name = "Consolidate Invoices Flag")]
        public string CMINVC { get => _CMINVC?.Trim(); set => _CMINVC = value; }
        [Display(Name = "Customer Dunning Type")]
        public string CMCDTP { get => _CMCDTP?.Trim(); set => _CMCDTP = value; }
        [Display(Name = "Customer Dunning Status")]
        public string CMCDST { get => _CMCDST?.Trim(); set => _CMCDST = value; }
        [Display(Name = "Dunning Letter Set")]
        public string CMDLTS { get => _CMDLTS?.Trim(); set => _CMDLTS = value; }
        [Display(Name = "Last Dunning Date")]
        public int CMDDAT { get; set; }
        [Display(Name = "Prefix Code")]
        public string CMDPFX { get => _CMDPFX?.Trim(); set => _CMDPFX = value; }
        [Display(Name = "Customer Item X-Ref Flag")]
        public int CMITMX { get; set; }
        [Display(Name = "Customer Country Code")]
        public string CMPRCC { get => _CMPRCC?.Trim(); set => _CMPRCC = value; }
        [Display(Name = "Customer Registration Number")]
        public string CMPRNO { get => _CMPRNO?.Trim(); set => _CMPRNO = value; }
        [Display(Name = "Print on Acknowledgement")]
        public string CMACKN { get => _CMACKN?.Trim(); set => _CMACKN = value; }
        [Display(Name = "Print on Shipping Papers")]
        public string CMSHIP { get => _CMSHIP?.Trim(); set => _CMSHIP = value; }
        [Display(Name = "Print on Invoice")]
        public string CMINVP { get => _CMINVP?.Trim(); set => _CMINVP = value; }
        [Display(Name = "Allow Line Add During RMA")]
        public string CALR { get => _CALR?.Trim(); set => _CALR = value; }
        [Display(Name = "Drop Ship Allowed")]
        public int CDSALW { get; set; }
        [Display(Name = "Carrier")]
        public string CCARR { get => _CCARR?.Trim(); set => _CCARR = value; }
        [Display(Name = "Means of Transportation")]
        public string CMNTR { get => _CMNTR?.Trim(); set => _CMNTR = value; }
        [Display(Name = "Freight Terms Code")]
        public string CTRMC { get => _CTRMC?.Trim(); set => _CTRMC = value; }
        [Display(Name = "Freight Markup Percentage")]
        public double CMPCT { get; set; }
        [Display(Name = "Approval Process Required")]
        public string CAPPR { get => _CAPPR?.Trim(); set => _CAPPR = value; }
        [Display(Name = "ASN Required")]
        public string CASNR { get => _CASNR?.Trim(); set => _CASNR = value; }
        [Display(Name = "Last Maintenance User")]
        public string CLUSR { get => _CLUSR?.Trim(); set => _CLUSR = value; }
        [Display(Name = "Last Maintenance Date")]
        public int CLDTE { get; set; }
        [Display(Name = "Last Maintenance Time")]
        public int CLTME { get; set; }
        [Display(Name = "Bank Code")]
        public string CMBANK { get => _CMBANK?.Trim(); set => _CMBANK = value; }
        [Display(Name = "Fax Number")]
        public string CMFAXN { get => _CMFAXN?.Trim(); set => _CMFAXN = value; }
        [Display(Name = "E-mail Address")]
        public string CMDATN { get => _CMDATN?.Trim(); set => _CMDATN = value; }
        [Display(Name = "Shipping Officer")]
        public string CMSOFR { get => _CMSOFR?.Trim(); set => _CMSOFR = value; }
        [Display(Name = "Declaration Part 1")]
        public string CMDPT1 { get => _CMDPT1?.Trim(); set => _CMDPT1 = value; }
        [Display(Name = "Declaration Part 2")]
        public string CMDPT2 { get => _CMDPT2?.Trim(); set => _CMDPT2 = value; }
        [Display(Name = "Declaration Part 3")]
        public string CMDPT3 { get => _CMDPT3?.Trim(); set => _CMDPT3 = value; }
        [Display(Name = "Excise Number")]
        public string CMEXCN { get => _CMEXCN?.Trim(); set => _CMEXCN = value; }
        [Display(Name = "Address Line 4")]
        public string CMAD4 { get => _CMAD4?.Trim(); set => _CMAD4 = value; }
        [Display(Name = "Address Line 5")]
        public string CMAD5 { get => _CMAD5?.Trim(); set => _CMAD5 = value; }
        [Display(Name = "Address Line 6")]
        public string CMAD6 { get => _CMAD6?.Trim(); set => _CMAD6 = value; }
        [Display(Name = "Data Number")]
        public string CDATN { get => _CDATN?.Trim(); set => _CDATN = value; }
        [Display(Name = "Default Order Class/Type 01")]
        public int CM1CLS { get; set; }
        [Display(Name = "Default Order Class/Type 02")]
        public int CM2CLS { get; set; }
        [Display(Name = "Default Order Class/Type 03")]
        public int CM3CLS { get; set; }
        [Display(Name = "Default Order Class/Type 04")]
        public int CM4CLS { get; set; }
        [Display(Name = "Default Order Class/Type 05")]
        public int CM5CLS { get; set; }
        [Display(Name = "Default Order Class/Type 06")]
        public int CM6CLS { get; set; }
        [Display(Name = "Default Order Class/Type 07")]
        public int CM7CLS { get; set; }
        [Display(Name = "Default Order Class/Type 08")]
        public int CM8CLS { get; set; }
        [Display(Name = "Default Order Class/Type 09")]
        public int CM9CLS { get; set; }
        [Display(Name = "Default Order Class/Quote")]
        public int CMQCLS { get; set; }
        [Display(Name = "Default Order Class/RMA")]
        public int CMRCLS { get; set; }
        [Display(Name = "Default Order Type")]
        public string CMDFOT { get => _CMDFOT?.Trim(); set => _CMDFOT = value; }
        [Display(Name = "User Defined Financial Fld 01")]
        public string CMFF01 { get => _CMFF01?.Trim(); set => _CMFF01 = value; }
        [Display(Name = "User Defined Financial Fld 02")]
        public string CMFF02 { get => _CMFF02?.Trim(); set => _CMFF02 = value; }
        [Display(Name = "User Defined Financial Fld 03")]
        public string CMFF03 { get => _CMFF03?.Trim(); set => _CMFF03 = value; }
        [Display(Name = "User Defined Financial Fld 04")]
        public string CMFF04 { get => _CMFF04?.Trim(); set => _CMFF04 = value; }
        [Display(Name = "User Defined Financial Fld 05")]
        public string CMFF05 { get => _CMFF05?.Trim(); set => _CMFF05 = value; }
        [Display(Name = "User Defined Financial Fld 06")]
        public string CMFF06 { get => _CMFF06?.Trim(); set => _CMFF06 = value; }
        [Display(Name = "Days Between Invoice")]
        public int CMBTBL { get; set; }
        [Display(Name = "Day of Month to Invoice")]
        public int CMDYBL { get; set; }
        [Display(Name = "Last Invoice Date")]
        public int CMLSBL { get; set; }
        [Display(Name = "Next Invoice Date")]
        public int CMNXBL { get; set; }
        [Display(Name = "Calculate Margins")]
        public int CMCMRG { get; set; }
        [Display(Name = "Lower Margin Percent")]
        public double CMLMPC { get; set; }
        [Display(Name = "Upper Margin Percent")]
        public double CMUMPC { get; set; }
        [Display(Name = "Allow Promotion Pricing")]
        public int CMAPPR { get; set; }
        [Display(Name = "Customer Hold Code")]
        public string CMHOLD { get => _CMHOLD?.Trim(); set => _CMHOLD = value; }
        [Display(Name = "Credit Check")]
        public int CMCRCK { get; set; }
        [Display(Name = "Require PO Number")]
        public int CMRQCM { get; set; }
        [Display(Name = "Primary Language")]
        public string CMLANG { get => _CMLANG?.Trim(); set => _CMLANG = value; }
        [Display(Name = "Organization Type")]
        public int CMORTP { get; set; }
        [Display(Name = "Pricing Region")]
        public string CMPREG { get => _CMPREG?.Trim(); set => _CMPREG = value; }
        [Display(Name = "Head Corporate Parent Number")]
        public int CMHCOR { get; set; }
        [Display(Name = "Corporate Parent Number")]
        public int CMCPAR { get; set; }
        [Display(Name = "Promotion Customer Number")]
        public int CMDLCU { get; set; }
        [Display(Name = "Head Sales Organization Parent")]
        public int CMHSAL { get; set; }
        [Display(Name = "Sales Organization Parent")]
        public int CMSPAR { get; set; }
        [Display(Name = "Sales History Customer Number")]
        public int CMSLCU { get; set; }
        [Display(Name = "Promotion Payment To Customer")]
        public int CMPPCU { get; set; }
        [Display(Name = "Line Number")]
        public int CMPPNO { get; set; }
        [Display(Name = "Pricing Customer Number")]
        public int CMPRCU { get; set; }
        [Display(Name = "Invoice To Customer Number")]
        public int CMINCU { get; set; }
        [Display(Name = "Invoice To Number")]
        public int CMINNO { get; set; }
        [Display(Name = "Statement To Customer")]
        public int CMSTCU { get; set; }
        [Display(Name = "Remit From Customer Number")]
        public int CMRMCU { get; set; }
        [Display(Name = "Ship To Customer Number")]
        public int CMSHCU { get; set; }
        [Display(Name = "Item X-Reference Customer Nbr")]
        public int CMIXCU { get; set; }
        [Display(Name = "Lock Box To Customer Number")]
        public int CMLBCU { get; set; }
        [Display(Name = "Lock Box To Number")]
        public int CMLBNO { get; set; }
        [Display(Name = "Include/Exclude Rule")]
        public int CMINEX { get; set; }
        [Display(Name = "Entered Date")]
        public int CMENDT { get; set; }
        [Display(Name = "Entered Time")]
        public int CMENTM { get; set; }
        [Display(Name = "Entered By User")]
        public string CMENUS { get => _CMENUS?.Trim(); set => _CMENUS = value; }
        [Display(Name = "Parameters at This Level")]
        public string CMPARM { get => _CMPARM?.Trim(); set => _CMPARM = value; }
        [Display(Name = "Round Req for Cmck Mul")]
        public string CMROUN { get => _CMROUN?.Trim(); set => _CMROUN = value; }
        [Display(Name = "Percent of Whole CMck")]
        public int CMRPCT { get; set; }
        [Display(Name = "Shipping Lead Time")]
        public int CMLEAD { get; set; }
        [Display(Name = "Keep Release History")]
        public string CMRELH { get => _CMRELH?.Trim(); set => _CMRELH = value; }
        [Display(Name = "Keep JIT History")]
        public string CMDCIH { get => _CMDCIH?.Trim(); set => _CMDCIH = value; }
        [Display(Name = "Current Model Year")]
        public int CMMODY { get; set; }
        [Display(Name = "Release Reconciliation Method")]
        public string CMRREC { get => _CMRREC?.Trim(); set => _CMRREC = value; }
        [Display(Name = "JIT Reconciliation Method")]
        public string CMJREC { get => _CMJREC?.Trim(); set => _CMJREC = value; }
        [Display(Name = "Supplier Code")]
        public string CMSUPP { get => _CMSUPP?.Trim(); set => _CMSUPP = value; }
        [Display(Name = "PAB Model Year")]
        public int CMPABM { get; set; }
        [Display(Name = "Auto Create Packaging")]
        public string CMAUTO { get => _CMAUTO?.Trim(); set => _CMAUTO = value; }
        [Display(Name = "Sequenced Shipment Allowed")]
        public string CMSEQC { get => _CMSEQC?.Trim(); set => _CMSEQC = value; }
        [Display(Name = "Pay As Built Warehouse")]
        public string CMSSTW { get => _CMSSTW?.Trim(); set => _CMSSTW = value; }
        [Display(Name = "Self Billing Allowed")]
        public string CMSBCU { get => _CMSBCU?.Trim(); set => _CMSBCU = value; }
        [Display(Name = "PAB Order Class")]
        public int CMPABC { get; set; }
        [Display(Name = "Packing Info Manditory Flag")]
        public string CMPREQ { get => _CMPREQ?.Trim(); set => _CMPREQ = value; }
        [Display(Name = "Check Pallets: 0,1")]
        public string CMCPAL { get => _CMCPAL?.Trim(); set => _CMCPAL = value; }
        [Display(Name = "Type of Sequence Number")]
        public string CMSEQN { get => _CMSEQN?.Trim(); set => _CMSEQN = value; }
        [Display(Name = "Seq Shipping Contract Field")]
        public string CMSSCF { get => _CMSSCF?.Trim(); set => _CMSSCF = value; }
        [Display(Name = "Sequenced Ship From Warehouse")]
        public string CMSSFW { get => _CMSSFW?.Trim(); set => _CMSSFW = value; }
        [Display(Name = "Pack Slip Output Queue")]
        public string CMOUTQ { get => _CMOUTQ?.Trim(); set => _CMOUTQ = value; }
        [Display(Name = "Self Bill Ref Field")]
        public string CMSBRF { get => _CMSBRF?.Trim(); set => _CMSBRF = value; }
        [Display(Name = "Self Billing Warehouse")]
        public string CMCSTW { get => _CMCSTW?.Trim(); set => _CMCSTW = value; }
        [Display(Name = "Nbr of Copies of Signal Label")]
        public int CMLSIG { get; set; }
        [Display(Name = "Label Format - Signal")]
        public string CMSFMT { get => _CMSFMT?.Trim(); set => _CMSFMT = value; }
        [Display(Name = "PAB Document Prefix")]
        public string CMPBDP { get => _CMPBDP?.Trim(); set => _CMPBDP = value; }
        [Display(Name = "Self Billing Document Prefix")]
        public string CMSBDP { get => _CMSBDP?.Trim(); set => _CMSBDP = value; }
        [Display(Name = "JIT Conversion Horizon")]
        public int CMJITH { get; set; }
        [Display(Name = "Receiving Dock")]
        public string CMDOCK { get => _CMDOCK?.Trim(); set => _CMDOCK = value; }
        [Display(Name = "Self Billing Order Class")]
        public int CMSBOC { get; set; }
        [Display(Name = "Issue Packaging?")]
        public string CMISSP { get => _CMISSP?.Trim(); set => _CMISSP = value; }
        [Display(Name = "Invoice Packaging?")]
        public string CMINPK { get => _CMINPK?.Trim(); set => _CMINPK = value; }
        [Display(Name = "Start Date for Conv Horizon")]
        public string CMCHOR { get => _CMCHOR?.Trim(); set => _CMCHOR = value; }
        [Display(Name = "Retain Past Due Order Lines")]
        public string CMRPDO { get => _CMRPDO?.Trim(); set => _CMRPDO = value; }
        [Display(Name = "Load Build Policy")]
        public string CMLBLD { get => _CMLBLD?.Trim(); set => _CMLBLD = value; }
        [Display(Name = "Preassign Invoice Numbers")]
        public string CMASSI { get => _CMASSI?.Trim(); set => _CMASSI = value; }
        [Display(Name = "Pay as Built Allowed")]
        public string CMPABI { get => _CMPABI?.Trim(); set => _CMPABI = value; }
        [Display(Name = "Duns+4")]
        public string CMDUN4 { get => _CMDUN4?.Trim(); set => _CMDUN4 = value; }
        [Display(Name = "UCC")]
        public string CMUCC { get => _CMUCC?.Trim(); set => _CMUCC = value; }
        [Display(Name = "AIAG")]
        public string CMAIAG { get => _CMAIAG?.Trim(); set => _CMAIAG = value; }
        [Display(Name = "EAN")]
        public string CMEAN { get => _CMEAN?.Trim(); set => _CMEAN = value; }
        [Display(Name = "Next Carton Number")]
        public string CMNXCN { get => _CMNXCN?.Trim(); set => _CMNXCN = value; }
        [Display(Name = "Lower Carton Number")]
        public string CMNXCL { get => _CMNXCL?.Trim(); set => _CMNXCL = value; }
        [Display(Name = "Upper Carton Number")]
        public string CMNXCU { get => _CMNXCU?.Trim(); set => _CMNXCU = value; }
        [Display(Name = "Next Pallet Number")]
        public string CMNXPN { get => _CMNXPN?.Trim(); set => _CMNXPN = value; }
        [Display(Name = "Lower Pallet Number")]
        public string CMNXPL { get => _CMNXPL?.Trim(); set => _CMNXPL = value; }
        [Display(Name = "Upper Pallet Number")]
        public string CMNXPU { get => _CMNXPU?.Trim(); set => _CMNXPU = value; }
        [Display(Name = "Consignment Print Validation")]
        public int CMCSGV { get; set; }
        [Display(Name = "Billing Selection Flag")]
        public string CMBSLF { get => _CMBSLF?.Trim(); set => _CMBSLF = value; }
        [Display(Name = "Inv Only Complete Orders")]
        public int CMCORD { get; set; }
        [Display(Name = "Free Goods Invoice Customer")]
        public int CMFGCU { get; set; }
        [Display(Name = "Tax Exemption Flag")]
        public int CMTXEX { get; set; }
        [Display(Name = "Delivery Note Invoice Flag")]
        public int CMDNIF { get; set; }
        [Display(Name = "Invoice Layout Type")]
        public int CMILYT { get; set; }
        [Display(Name = "Print/Display Flag")]
        public string CMACPT { get => _CMACPT?.Trim(); set => _CMACPT = value; }
        [Display(Name = "Alternate Currency")]
        public string CMACUR { get => _CMACUR?.Trim(); set => _CMACUR = value; }
        [Display(Name = "Responsible CSR Code")]
        public string CMRCSR { get => _CMRCSR?.Trim(); set => _CMRCSR = value; }
        [Display(Name = "Minimum Order")]
        public int CMMINC { get; set; }
        [Display(Name = "En Cartons /Kilos")]
        public string CMCAKG { get => _CMCAKG?.Trim(); set => _CMCAKG = value; }
        [Display(Name = "Interest Method Code")]
        public string CMINMC { get => _CMINMC?.Trim(); set => _CMINMC = value; }
        [Display(Name = "Exempt from Interest Invoicing")]
        public int CMEXII { get; set; }
        [Display(Name = "User Defined Field for Bank")]
        public string CMUF01 { get => _CMUF01?.Trim(); set => _CMUF01 = value; }
        [Display(Name = "User Defined Field for Bank")]
        public string CMUF02 { get => _CMUF02?.Trim(); set => _CMUF02 = value; }
        [Display(Name = "User Defined Field for Bank")]
        public string CMUF03 { get => _CMUF03?.Trim(); set => _CMUF03 = value; }
        [Display(Name = "User Defined Field for Bank")]
        public string CMUF04 { get => _CMUF04?.Trim(); set => _CMUF04 = value; }
        [Display(Name = "User Defined Field for Bank")]
        public string CMUF05 { get => _CMUF05?.Trim(); set => _CMUF05 = value; }
        [Display(Name = "User Defined Field for Bank")]
        public string CMUF06 { get => _CMUF06?.Trim(); set => _CMUF06 = value; }
        [Display(Name = "User Defined Field for Bank")]
        public string CMUF07 { get => _CMUF07?.Trim(); set => _CMUF07 = value; }
        [Display(Name = "User Defined Field for Bank")]
        public string CMUF08 { get => _CMUF08?.Trim(); set => _CMUF08 = value; }
        [Display(Name = "User Defined Field for Bank")]
        public string CMUF09 { get => _CMUF09?.Trim(); set => _CMUF09 = value; }
        [Display(Name = "User Defined Field for Bank")]
        public string CMFU10 { get => _CMFU10?.Trim(); set => _CMFU10 = value; }
        [Display(Name = "Shipping Lead Time Hours")]
        public int CMLHRS { get; set; }
        [Display(Name = "Cancellation Tracking Flag")]
        public string CMCINF { get => _CMCINF?.Trim(); set => _CMCINF = value; }
        [Display(Name = "Override Ship Date at Pck Rls")]
        public string CMOVSD { get => _CMOVSD?.Trim(); set => _CMOVSD = value; }
        [Display(Name = "User Defined 10 A")]
        public string CMURDF { get => _CMURDF?.Trim(); set => _CMURDF = value; }
        [Display(Name = "Allow Discounts on Credit Ord")]
        public int CMADCO { get; set; }
        [Display(Name = "CO Lot Alloc Qualification")]
        public string CMOLAQ { get => _CMOLAQ?.Trim(); set => _CMOLAQ = value; }
        [Display(Name = "Quality Group Code")]
        public string CMGRCD { get => _CMGRCD?.Trim(); set => _CMGRCD = value; }
        [Display(Name = "Self Bill Backorder Code")]
        public int CMSBBC { get; set; }
        [Display(Name = "Discount Type Code")]
        public int CMDTC { get; set; }
        [Display(Name = "Order Entry Fin Discount Calc")]
        public int CMCFDO { get; set; }
        [Display(Name = "Reprice RMA Lines")]
        public int CMRRMA { get; set; }
        [Display(Name = "Allow Advanced List Pricing")]
        public int CMAALP { get; set; }
        [Display(Name = "Proof of Delivery Flag")]
        public int CMPODF { get; set; }
        [Display(Name = "Allow Self Billing Credit")]
        public int CMASBC { get; set; }
        [Display(Name = "Promo Qualification Date Type")]
        public int CMPQDT { get; set; }
        [Display(Name = "Allow Charge/Allow Processing")]
        public int CMALCA { get; set; }
        [Display(Name = "Fixed Days of Payment 1")]
        public int CMVTO1 { get; set; }
        [Display(Name = "Fixed Days of Payment 2")]
        public int CMVTO2 { get; set; }
        [Display(Name = "Fixed Days of Payment 3")]
        public int CMVTO3 { get; set; }
        [Display(Name = "Customer Bank")]
        public string CMBANO { get => _CMBANO?.Trim(); set => _CMBANO = value; }
        [Display(Name = "Agency")]
        public string CMAGEN { get => _CMAGEN?.Trim(); set => _CMAGEN = value; }
        [Display(Name = "Bank Account")]
        public string CMCCTA { get => _CMCCTA?.Trim(); set => _CMCCTA = value; }
        [Display(Name = "Check Digit")]
        public string CMDGTC { get => _CMDGTC?.Trim(); set => _CMDGTC = value; }
        [Display(Name = "Discount Grace Days")]
        public int CMDCAB { get; set; }
        [Display(Name = "Vendor Code")]
        public int CMVNDC { get; set; }
        [Display(Name = "Credit Notes Action")]
        public string CMCRDS { get => _CMCRDS?.Trim(); set => _CMCRDS = value; }
        [Display(Name = "Holiday Period Type")]
        public string CMPRVA { get => _CMPRVA?.Trim(); set => _CMPRVA = value; }
        [Display(Name = "Surcharge Percent")]
        public double CMPRRE { get; set; }
        [Display(Name = "Financial Discount Percent")]
        public double CMPRDE { get; set; }
        [Display(Name = "Charge Return Expenses")]
        public string CMPGDE { get => _CMPGDE?.Trim(); set => _CMPGDE = value; }
        [Display(Name = "Holiday Period From (MMDD)")]
        public int CMDVDF { get; set; }
        [Display(Name = "Holiday Period To (MMDD)")]
        public int CMDVHF { get; set; }
        [Display(Name = "Bad Debt Provisions")]
        public string CMPRIN { get => _CMPRIN?.Trim(); set => _CMPRIN = value; }
        [Display(Name = "Stoppage Payment")]
        public string CMSPAG { get => _CMSPAG?.Trim(); set => _CMSPAG = value; }
        [Display(Name = "Unpaid Amount Month 1")]
        public double CMIIM1 { get; set; }
        [Display(Name = "Unpaid Number Month 1")]
        public int CMIN01 { get; set; }
        [Display(Name = "Unpaid Amount Month 2")]
        public double CMIIM2 { get; set; }
        [Display(Name = "Unpaid Number Month 2")]
        public int CMIN02 { get; set; }
        [Display(Name = "Unpaid Amount Month 3")]
        public double CMIIM3 { get; set; }
        [Display(Name = "Unpaid Number Month 3")]
        public int CMIN03 { get; set; }
        [Display(Name = "Unpaid Amount Month 4")]
        public double CMIIM4 { get; set; }
        [Display(Name = "Unpaid Number Month 4")]
        public int CMIN04 { get; set; }
        [Display(Name = "Unpaid Amount Month 5")]
        public double CMIIM5 { get; set; }
        [Display(Name = "Unpaid Number Month 5")]
        public int CMIN05 { get; set; }
        [Display(Name = "Unpaid Amount Month 6")]
        public double CMIIM6 { get; set; }
        [Display(Name = "Unpaid Number Month 6")]
        public int CMIN06 { get; set; }
        [Display(Name = "Unpaid Amount Month 7")]
        public double CMIIM7 { get; set; }
        [Display(Name = "Unpaid Number Month 7")]
        public int CMIN07 { get; set; }
        [Display(Name = "Unpaid Amount Month 8")]
        public double CMIIM8 { get; set; }
        [Display(Name = "Unpaid Number Month 8")]
        public int CMIN08 { get; set; }
        [Display(Name = "Unpaid Amount Month 9")]
        public double CMIIM9 { get; set; }
        [Display(Name = "Unpaid Number Month 9")]
        public int CMIN09 { get; set; }
        [Display(Name = "Unpaid Amount Month 10")]
        public double CMII10 { get; set; }
        [Display(Name = "Unpaid Number Month 10")]
        public int CMIN10 { get; set; }
        [Display(Name = "Unpaid Amount Month 11")]
        public double CMII11 { get; set; }
        [Display(Name = "Unpaid Number Month 11")]
        public int CMIN11 { get; set; }
        [Display(Name = "Unpaid Amount Month 12")]
        public double CMII12 { get; set; }
        [Display(Name = "Unpaid Number Month 12")]
        public int CMIN12 { get; set; }
        [Display(Name = "Unpaid Amount Actual Year")]
        public double CMIIAC { get; set; }
        [Display(Name = "Unpaid Number Actual Year")]
        public int CMINAC { get; set; }
        [Display(Name = "Unpaid Amount Previous Year")]
        public double CMIIAN { get; set; }
        [Display(Name = "Unpaid Number Previous Year")]
        public int CMINAN { get; set; }
        [Display(Name = "Unpaid Amount Actual Month")]
        public double CMUPAM { get; set; }
        [Display(Name = "Unpaid Number Actual Month")]
        public int CMUNAM { get; set; }
        [Display(Name = "Apply Bill Back Promotions")]
        public string CMBBAP { get => _CMBBAP?.Trim(); set => _CMBBAP = value; }
        [Display(Name = "Apply Fin Discount on Remit")]
        public string CMFDAP { get => _CMFDAP?.Trim(); set => _CMFDAP = value; }
        [Display(Name = "Delay Invoice Flag")]
        public int CMDINV { get; set; }
        [Display(Name = "Invoice Delay Days")]
        public int CMDAYS { get; set; }
        [Display(Name = "Customer Credit Status")]
        public string CMSTS { get => _CMSTS?.Trim(); set => _CMSTS = value; }
        [Display(Name = "Level")]
        public string CMLVL { get => _CMLVL?.Trim(); set => _CMLVL = value; }
        [Display(Name = "Shopping List Used Flag")]
        public int CMSLF { get; set; }
        [Display(Name = "Number of Best Sellers")]
        public int CMNBS { get; set; }
        [Display(Name = "Number of Periods")]
        public int CMNPU { get; set; }
        [Display(Name = "Self Bill Reference Used")]
        public int CMSBRU { get; set; }
        [Display(Name = "Manual Price Override R/C Flag")]
        public int CMMNPO { get; set; }
        [Display(Name = "Deflt Man Price Override R/C")]
        public string CMDMPR { get => _CMDMPR?.Trim(); set => _CMDMPR = value; }
        [Display(Name = "Postal Code")]
        public string CMPST2 { get => _CMPST2?.Trim(); set => _CMPST2 = value; }
        [Display(Name = "AR Credit Check Flag")]
        public int CMCRCF { get; set; }
        [Display(Name = "A/R Customer Relationship Flag")]
        public string CMARCF { get => _CMARCF?.Trim(); set => _CMARCF = value; }
        [Display(Name = "Pricing Cust Relationship Flag")]
        public string CMPICF { get => _CMPICF?.Trim(); set => _CMPICF = value; }
        [Display(Name = "Remit Cust Relationship Flag")]
        public string CMRFCF { get => _CMRFCF?.Trim(); set => _CMRFCF = value; }
        [Display(Name = "Statement To Relationship Flag")]
        public string CMSTCF { get => _CMSTCF?.Trim(); set => _CMSTCF = value; }
        [Display(Name = "Promo Cust Relationship Flag")]
        public string CMPOCF { get => _CMPOCF?.Trim(); set => _CMPOCF = value; }
        [Display(Name = "Promo Pay To Relationship Flag")]
        public string CMPPCF { get => _CMPPCF?.Trim(); set => _CMPPCF = value; }
        [Display(Name = "Inventory-To Cust Reltnshp Flg")]
        public string CMINCF { get => _CMINCF?.Trim(); set => _CMINCF = value; }
        [Display(Name = "Lockbox Cust Reltnshp Flag")]
        public string CMLBCF { get => _CMLBCF?.Trim(); set => _CMLBCF = value; }
        [Display(Name = "Item X-Ref Reltnshp Flag")]
        public string CMIXCF { get => _CMIXCF?.Trim(); set => _CMIXCF = value; }
        [Display(Name = "Sales Hist Cust Reltnshp Flag")]
        public string CMSACF { get => _CMSACF?.Trim(); set => _CMSACF = value; }
        [Display(Name = "Ship To Cust Reltnshp Flag")]
        public string CMSHCF { get => _CMSHCF?.Trim(); set => _CMSHCF = value; }
        [Display(Name = "Corp Credit Cust Reltnshp Flag")]
        public string CMCCCF { get => _CMCCCF?.Trim(); set => _CMCCCF = value; }
        [Display(Name = "Corporate Credit Check")]
        public int CMCCCK { get; set; }
        [Display(Name = "Corporate Credit Customer")]
        public int CMCCCU { get; set; }
        [Display(Name = "Corporate Credit Limit")]
        public double CMCCRL { get; set; }
        [Display(Name = "Corporate Credit Days")]
        public int CMCDCL { get; set; }
        [Display(Name = "Corporate Open Order Amount")]
        public double CMCOOA { get; set; }
        [Display(Name = "Corporate Amount Due")]
        public double CMCAMD { get; set; }
        [Display(Name = "CC Ordered Amount")]
        public double CMCAMT { get; set; }
        [Display(Name = "CC Corp Ordered Amount")]
        public double CMCCAM { get; set; }
        [Display(Name = "Print Zero Balance Invoice")]
        public int CMZBAL { get; set; }
        [Display(Name = "External Authorization Allowed")]
        public string CMEXAU { get => _CMEXAU?.Trim(); set => _CMEXAU = value; }
        [Display(Name = "Allocation Fill Percentage")]
        public int CMBAFP { get; set; }
        [Display(Name = "Pick Release Fill Percentage")]
        public int CMPRFP { get; set; }
        [Display(Name = "Region Code")]
        public string CMRGCD { get => _CMRGCD?.Trim(); set => _CMRGCD = value; }
        [Display(Name = "Corp Credit Check Flag")]
        public int CMCRPF { get; set; }
    }
}
