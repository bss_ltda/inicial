﻿using System.ComponentModel.DataAnnotations;

namespace WebCilindrosCore.Entities
{
    public class RCAA
    {   
        //private string _USTS;
        
        private string _ASTS;
        private string _AUSR;
        private string _ACC00;
        private string _ACC01;
        private string _ACC02;
        private string _ACC03;
        private string _ACC04;
        private string _ACC05;
        private string _ACC06;
        private string _ACC07;
        private string _ACC08;
        private string _ACC09;
        private string _ACC10;
        private string _ACC11;
        private string _ACC12;
        private string _ACC13;
        private string _ACC14;
        private string _ACC15;
        private string _ACC16;
        private string _ACC17;
        private string _ACC18;
        private string _ACC19;
        private string _ACC20;
        private string _AAUT;
        private string _ASTYLE;
        private string _ACATEG;
        private string _AAPP;
        private string _AMOD;
        private string _ADESC;
        private string _AAPPMNU;
        private string _AOPCMNU;
        private string _AURL;

        
        //public string USTS { get => _USTS?.Trim(); set => _USTS = value; }
        
      

        [Display(Name = "Estado")]
        public string ASTS { get => _ASTS?.Trim(); set => _ASTS = value; }
        [Display(Name = "AMODUSR")]
        public int AMODUSR { get; set; }
        [Display(Name = "AMODULO")]
        public int AMODULO { get; set; }
        [Display(Name = "Usuario")]
        public string AUSR { get => _AUSR?.Trim(); set => _AUSR = value; }
        [Display(Name = "Acc 00")]
        public string ACC00 { get => _ACC00?.Trim(); set => _ACC00 = value; }
        [Display(Name = "Acc 01")]
        public string ACC01 { get => _ACC01?.Trim(); set => _ACC01 = value; }
        [Display(Name = "Acc 02")]
        public string ACC02 { get => _ACC02?.Trim(); set => _ACC02 = value; }
        [Display(Name = "Acc 03")]
        public string ACC03 { get => _ACC03?.Trim(); set => _ACC03 = value; }
        [Display(Name = "Acc 04")]
        public string ACC04 { get => _ACC04?.Trim(); set => _ACC04 = value; }
        [Display(Name = "Acc 05")]
        public string ACC05 { get => _ACC05?.Trim(); set => _ACC05 = value; }
        [Display(Name = "Acc 06")]
        public string ACC06 { get => _ACC06?.Trim(); set => _ACC06 = value; }
        [Display(Name = "Acc 07")]
        public string ACC07 { get => _ACC07?.Trim(); set => _ACC07 = value; }
        [Display(Name = "Acc 08")]
        public string ACC08 { get => _ACC08?.Trim(); set => _ACC08 = value; }
        [Display(Name = "Acc 09")]
        public string ACC09 { get => _ACC09?.Trim(); set => _ACC09 = value; }
        [Display(Name = "Acc 10")]
        public string ACC10 { get => _ACC10?.Trim(); set => _ACC10 = value; }
        [Display(Name = "Acc 11")]
        public string ACC11 { get => _ACC11?.Trim(); set => _ACC11 = value; }
        [Display(Name = "Acc 12")]
        public string ACC12 { get => _ACC12?.Trim(); set => _ACC12 = value; }
        [Display(Name = "Acc 13")]
        public string ACC13 { get => _ACC13?.Trim(); set => _ACC13 = value; }
        [Display(Name = "Acc 14")]
        public string ACC14 { get => _ACC14?.Trim(); set => _ACC14 = value; }
        [Display(Name = "Acc 15")]
        public string ACC15 { get => _ACC15?.Trim(); set => _ACC15 = value; }
        [Display(Name = "Acc 16")]
        public string ACC16 { get => _ACC16?.Trim(); set => _ACC16 = value; }
        [Display(Name = "Acc 17")]
        public string ACC17 { get => _ACC17?.Trim(); set => _ACC17 = value; }
        [Display(Name = "Acc 18")]
        public string ACC18 { get => _ACC18?.Trim(); set => _ACC18 = value; }
        [Display(Name = "Acc 19")]
        public string ACC19 { get => _ACC19?.Trim(); set => _ACC19 = value; }
        [Display(Name = "Acc 20")]
        public string ACC20 { get => _ACC20?.Trim(); set => _ACC20 = value; }
        [Display(Name = "Autorizó")]
        public string AAUT { get => _AAUT?.Trim(); set => _AAUT = value; }
        [Display(Name = "Fecha")]
        public System.DateTime? AUPD { get; set; }
        [Display(Name = "ASTYLE")]
        public string ASTYLE { get => _ASTYLE?.Trim(); set => _ASTYLE = value; }
        [Display(Name = "ABOL")]
        public int ABOL { get; set; }
        [Display(Name = "Modulo por defecto")]
        public int ADEFAULT { get; set; }
        [Display(Name = "Categoria")]
        public string ACATEG { get => _ACATEG?.Trim(); set => _ACATEG = value; }
        [Display(Name = "Aplicacion")]
        public string AAPP { get => _AAPP?.Trim(); set => _AAPP = value; }
        [Display(Name = "Modulo")]
        public string AMOD { get => _AMOD?.Trim(); set => _AMOD = value; }
        [Display(Name = "Descripcion")]
        public string ADESC { get => _ADESC?.Trim(); set => _ADESC = value; }
        [Display(Name = "Menu")]
        public string AAPPMNU { get => _AAPPMNU?.Trim(); set => _AAPPMNU = value; }
        [Display(Name = "Opc Menu")]
        public string AOPCMNU { get => _AOPCMNU?.Trim(); set => _AOPCMNU = value; }
        [Display(Name = "URL")]
        public string AURL { get => _AURL?.Trim(); set => _AURL = value; }

        public RCAA()
        {

        }

        public RCAA(string JSon)
        {
            var modulousuario = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(JSon);

            if (modulousuario != null)
            {
                if (modulousuario.Type == Newtonsoft.Json.Linq.JTokenType.Array)
                {
                    foreach (var item in modulousuario)
                    {
                        CargaDatos(item);
                    }
                }
                else
                {
                    CargaDatos(modulousuario);
                }
            }
        }

        private void CargaDatos(dynamic modulousuario)
        {
            AAPP = modulousuario.AAPP;
            AUSR = modulousuario.AUSR;
            ADEFAULT = modulousuario.ADEFAULT;
            //USTS = modulousuario.USTS;
            ASTS = modulousuario.ASTS;
            AMOD = modulousuario.AMOD;
            ADESC = modulousuario.ADESC;
            AURL = modulousuario.AURL;
        }
    }
}
