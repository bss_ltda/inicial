﻿using System.Collections.Generic;
using System.Linq.Expressions;

namespace WebCilindrosCore.Repository
{
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity GetOne(string Where);
        IEnumerable<TEntity> GetAll();
        string Add(TEntity entity);
        string Add(List<TEntity> entity);
        string Update(TEntity entity);
        string Delete(string Where);
        IEnumerable<TEntity> ExecuteQuery(string query, bool isolationLevel = false);
        IEnumerable<TResult> ExecuteQuery<TResult, TFirst>(string query, bool fromParent = true);
        IEnumerable<TResult> ExecuteQuery<TResult, TFirst, TSecond>(string query);
        IEnumerable<TResult> ExecuteQuery<TResult, TFirst, TSecond, TThird>(string query);
        IEnumerable<TResult> ExecuteQuery<TResult, TFirst, TSecond, TThird, TFourth>(string query);
        IEnumerable<TResult> ExecuteQuery<TResult, TFirst, TSecond, TThird, TFourth, TFifth>(string query);
        IEnumerable<TResult> ExecuteQuery<TResult, TFirst, TSecond, TThird, TFourth, TFifth, TSixth>(string query);
        IEnumerable<TResult> ExecuteQuery<TResult, TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh>(string query);
        IEnumerable<TEntity> Find(Expression<System.Func<TEntity, bool>> predicate, bool isolationLevel = false);
    }
}
