﻿using System.Collections.Generic;

namespace WebCilindros.Models
{
    public class UserLiteDTO
    {
        public string Estado { get; set; }
        public string Tipo { get; set; }
        public string Usuario { get; set; }
        public string Nombre { get; set; }
        public string Modulo { get; set; }
        public string Planta { get; set; }
        //public List<WebCilindrosCore.Entities.RFWFUSR> Bodega { get; set; }
        public string Area { get; set; }
        public string Cargo { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public string Periodo { get; set; }
        public string Version { get; set; }
        public string UserEMail { get; set; }
        public List<bool> AppAcc { get; set; }
        public bool PasswordVigente { get; set; }
        //public IEnumerable<WebCilindrosCore.Entities.RCAAP> Asignaciones { get; set; }

        public UserLiteDTO()
        {

        }

        public UserLiteDTO(dynamic UserData)
        {
            Modulo = UserData.Modulo;
            Usuario = UserData.Usuario;
            //Asignaciones = UserData.Asignaciones;
        }

        public UserLiteDTO(string JSon)
        {
            var UserData = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(JSon);

            Modulo = UserData.Modulo;
            Usuario = UserData.Usuario;

            //if (Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(UserData.Asignaciones.ToString()).Count > 0)
            //{
            //    List<WebCilindrosCore.Entities.RCAAP> rCAAPs = new List<WebCilindrosCore.Entities.RCAAP>();
            //    WebCilindrosCore.Entities.RCAAP rCAAP = new WebCilindrosCore.Entities.RCAAP(UserData.Asignaciones.ToString());

            //    rCAAPs.Add(rCAAP);

            //    Asignaciones = rCAAPs;
            //}
        }
    }
}