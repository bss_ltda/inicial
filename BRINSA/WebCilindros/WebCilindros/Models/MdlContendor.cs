﻿using System.ComponentModel.DataAnnotations;

namespace WebCilindros.Models
{
    public class MdlContendor
    {
        [Required(ErrorMessage = "El campo contenedor es obligatorio")]
        [Display(Name = "Contenedor")]
        public string contenedor { get; set; }
        public string recepcion { get; set; }
    }
}