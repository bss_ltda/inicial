﻿using System.ComponentModel.DataAnnotations;

namespace WebCilindros.Models
{
    public class MdlSegundoDetalle
    {
        [Required(ErrorMessage = "El campo contenedor es obligatorio")]
        [Display(Name = "Contenedor")]
        public string contenedor { get; set; }
        [Required(ErrorMessage = "El campo Recepción es obligatorio")]
        [Display(Name = "Recepción")]
        public string recepcion { get; set; }
        [Required(ErrorMessage = "El campo Linea es obligatorio")]
        [Display(Name = "Linea")]
        public string linea { get; set; }
        [Required(ErrorMessage = "El campo Capuchón es obligatorio")]
        [Display(Name = "Capuchón")]
        public int? capuchon { get; set; }
        [Required(ErrorMessage = "El campo Tapón es obligatorio")]
        [Display(Name = "Tapón")]
        public int? tapon { get; set; }
        [Required(ErrorMessage = "El campo Valvula es obligatorio")]
        [Display(Name = "Valvula")]
        public int? valvula { get; set; }
        [Required(ErrorMessage = "El campo Daño Valvula es obligatorio")]
        [Display(Name = "Daño Valvula")]
        public int? danovalvula { get; set; }
        [Required(ErrorMessage = "El campo Corrosión es obligatorio")]
        [Display(Name = "Corrosión")]
        public int? corrosion { get; set; }
        [Required(ErrorMessage = "El campo Cortes es obligatorio")]
        [Display(Name = "Cortes")]
        public int? cortes { get; set; }
        [Required(ErrorMessage = "El campo Abolladuras es obligatorio")]
        [Display(Name = "Abolladuras")]
        public int? abolladuras { get; set; }
        [Required(ErrorMessage = "El campo Protuberancias es obligatorio")]
        [Display(Name = "Protuberancias")]
        public int? protuberancias { get; set; }
        [Required(ErrorMessage = "El campo Daño Fuego es obligatorio")]
        [Display(Name = "Daño Fuego")]
        public int? danofuego { get; set; }
        [Required(ErrorMessage = "El campo Abrasión es obligatorio")]
        [Display(Name = "Abrasión")]
        public int? abrasion { get; set; }
        [Required(ErrorMessage = "El campo Daño Rosca es obligatorio")]
        [Display(Name = "Daño Rosca")]
        public int? danorosca { get; set; }        
        public string observacion { get; set; }
    }
}