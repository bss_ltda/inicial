﻿using System.ComponentModel.DataAnnotations;

namespace WebCilindros.Models
{
    public class MdlLogin
    {
        [Required(ErrorMessage = "El campo usuario es obligatorio")]
        [Display(Name = "Usuario")]
        public string User { get; set; }
        [Required(ErrorMessage = "El campo Contraseña es obligatorio")]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }
    }
}