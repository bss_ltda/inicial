﻿using System.ComponentModel.DataAnnotations;

namespace WebCilindros.Models
{
    public class MdlCliente
    {
        [Required(ErrorMessage = "El campo cliente es obligatorio")]
        [Display(Name = "Cliente")]
        public string cliente { get; set; }
        [Required]        
        public int idCliente { get; set; }
    }
}