﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WebCilindros.Models;

namespace WebCilindros.Controllers
{
    public class RecepcionDetalleController : Controller
    {
        WebCilindrosCore.Modules.RecepcionDetalleModule recepcionDetalleModule = new WebCilindrosCore.Modules.RecepcionDetalleModule();

        public ActionResult Index(string recepcion)
        {
            if (WebCilindrosCore.Helpers.SessionManager.Get<UserLiteDTO>("VarUsuario") == null)
            {
                return RedirectToAction("Login", "Account", new { donde = Url.Action("Index", "RecepcionDetalle") });
            }
               
            IEnumerable<WebCilindrosCore.Entities.RFWCYLRECL> detalleRecepcion = recepcionDetalleModule.GetByPredicate(m => m.RECEPCION == recepcion);

            ViewBag.DetalleRecepcion = detalleRecepcion;

            return View(new MdlContendor() { recepcion = recepcion});
        }

        [HttpPost]
        public ActionResult Index(MdlContendor mdlContendor)
        {
            if (!ModelState.IsValid)
            {
                return View(mdlContendor);
            }

            var usuario = WebCilindrosCore.Helpers.SessionManager.Get<UserLiteDTO>("VarUsuario");

            if (usuario != null)
            {
                WebCilindrosCore.Entities.RFWCYLRECL detalleRecepcion = new WebCilindrosCore.Entities.RFWCYLRECL()
                {
                    CYLNUMERO = "3333",
                    RECEPCION = mdlContendor.recepcion,
                    CUSTODIO = 0,
                    REINSPEC = 0,
                    CYLCODBARRAS = mdlContendor.contenedor
                };

                recepcionDetalleModule.Add(detalleRecepcion);

                return RedirectToAction("SegundoDetalle", "RecepcionDetalle", new { linea = detalleRecepcion.RECEPCION_LINEA, contenedor = detalleRecepcion.CYLCODBARRAS, recepcion = detalleRecepcion.RECEPCION });
            }

            return View();
        }

        public ActionResult Borrar(string linea)
        {
            if (WebCilindrosCore.Helpers.SessionManager.Get<UserLiteDTO>("VarUsuario") == null)
            {
                return RedirectToAction("Login", "Account", new { donde = Url.Action("Index", "RecepcionDetalle") });
            }

            WebCilindrosCore.Entities.RFWCYLRECL detalleRecepcion = recepcionDetalleModule.GetByPredicate(m => m.RECEPCION_LINEA == linea).SingleOrDefault();

            if (detalleRecepcion != null)
            {
                recepcionDetalleModule.Del(linea);

                return RedirectToAction("Index", "RecepcionDetalle", new { recepcion = detalleRecepcion.RECEPCION });
            }

            return View();
        }

        public ActionResult SegundoDetalle(string linea, string contenedor, string recepcion)
        {
            if (WebCilindrosCore.Helpers.SessionManager.Get<UserLiteDTO>("VarUsuario") == null)
            {
                return RedirectToAction("Login", "Account", new { donde = Url.Action("Index", "RecepcionDetalle", new { linea, contenedor, recepcion }) });
            }

            return View(new MdlSegundoDetalle() { linea = linea, contenedor = contenedor, recepcion = recepcion});
        }

        [HttpPost]
        public ActionResult SegundoDetalle(MdlSegundoDetalle mdlSegundoDetalle)
        {
            if (!ModelState.IsValid)
            {
                return View(mdlSegundoDetalle);
            }

            WebCilindrosCore.Entities.RFWCYLRECL detalleRecepcion = recepcionDetalleModule.GetByPredicate(m => m.RECEPCION_LINEA == mdlSegundoDetalle.linea).SingleOrDefault();

            if (detalleRecepcion != null)
            {
                detalleRecepcion.CAPUCHON = (int)mdlSegundoDetalle.capuchon;
                detalleRecepcion.TAPON = (int)mdlSegundoDetalle.tapon;
                detalleRecepcion.VALVULA = (int)mdlSegundoDetalle.valvula;
                detalleRecepcion.DANOVALVULA = (int)mdlSegundoDetalle.danovalvula;
                detalleRecepcion.CORROSION = (int)mdlSegundoDetalle.corrosion;
                detalleRecepcion.CORTES = (int)mdlSegundoDetalle.cortes;
                detalleRecepcion.ABOLLADURAS = (int)mdlSegundoDetalle.abolladuras;
                detalleRecepcion.PROTUBERANCIAS = (int)mdlSegundoDetalle.protuberancias;
                detalleRecepcion.DANOFUEGO = (int)mdlSegundoDetalle.danofuego;
                detalleRecepcion.ABRASION = (int)mdlSegundoDetalle.abrasion;
                detalleRecepcion.DANOROSCA = (int)mdlSegundoDetalle.danorosca;
                detalleRecepcion.OBSERVACION = mdlSegundoDetalle.observacion;

                recepcionDetalleModule.Upd(detalleRecepcion);

                return RedirectToAction("Index", "RecepcionDetalle", new { recepcion = detalleRecepcion.RECEPCION });
            }

            return View();
        }
    }
}