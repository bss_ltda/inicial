﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WebCilindros.Models;

namespace WebCilindros.Controllers
{
    public class AccountController : Controller
    {

        Funciones f = new Funciones();
        WebCilindrosCore.Modules.TablaParamModule tablaParamModule = new WebCilindrosCore.Modules.TablaParamModule();
        WebCilindrosCore.Modules.SystemModule systemModule = new WebCilindrosCore.Modules.SystemModule();
        WebCilindrosCore.Modules.ModuloUsuarioModule moduloUsuarioModule = new WebCilindrosCore.Modules.ModuloUsuarioModule();

        [AllowAnonymous]
        public ActionResult Login(string returnUrl, string donde)
        {
            ViewBag.Donde = donde;

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(MdlLogin model, string returnUrl, string donde)
        {
            string res = "", urlService = "";

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {

                WebCilindrosCore.Entities.RFPARAM rFPARAM = tablaParamModule.GetByPredicate(m => m.CCTABL == "LXLONG" && m.CCCODE2 == "LOGIN_USUARIO").SingleOrDefault();

                if (rFPARAM != null)
                {
                    urlService = rFPARAM.CCNOT1;
                }

                string librerias = systemModule.GetLibrerias();

                WebCilindrosCore.Helpers.SessionManager.Set("Librerias", librerias);

                res = f.CallService($"{{ 'Username': '{model.User}', 'Password': '{model.Password}', 'App': 'WebCilindros' }}", urlService);

                if (!res.ToUpper().Contains("MESSAGE"))
                {
                    UserLiteDTO mUserLiteDTO = new UserLiteDTO(res);

                    if (mUserLiteDTO != null)
                    {
                        IEnumerable<WebCilindrosCore.Entities.RFPARAM> lmRFPARAM = tablaParamModule.GetByPredicate(m => m.CCTABL == "WebCilindros" && m.CCCODE == "PARAMG");

                        mUserLiteDTO.Version = lmRFPARAM.Where(m => m.CCCODE2 == "VERSION" && m.CCCODE3 == "").FirstOrDefault()?.CCSDSC;
                        mUserLiteDTO.Periodo = lmRFPARAM.Where(m => m.CCCODE2 == "PERIODO").FirstOrDefault()?.CCUDV1.ToString();

                        //mUserLiteDTO.Bodega = flujosModule.GetByPredicate(m => m.FAPP == "WebCilindros" && m.FMOD == "WebCilindros" && m.FUSRACT == model.User).ToList();

                        WebCilindrosCore.Entities.RCAA mRCAA = moduloUsuarioModule.GetByPredicate(m => m.AAPP == "WebCilindros" && m.AUSR == model.User && m.ADEFAULT == 1).FirstOrDefault();

                        if (mRCAA != null)
                        {
                            mUserLiteDTO.AppAcc = new List<bool>() {
                            (mRCAA.ACC00.Trim() == "") ? false : true,
                            (mRCAA.ACC01.Trim() == "") ? false : true,
                            (mRCAA.ACC02.Trim() == "") ? false : true,
                            (mRCAA.ACC03.Trim() == "") ? false : true,
                            (mRCAA.ACC04.Trim() == "") ? false : true,
                            (mRCAA.ACC05.Trim() == "") ? false : true,
                            (mRCAA.ACC06.Trim() == "") ? false : true,
                            (mRCAA.ACC07.Trim() == "") ? false : true,
                            (mRCAA.ACC08.Trim() == "") ? false : true,
                            (mRCAA.ACC09.Trim() == "") ? false : true,
                            (mRCAA.ACC10.Trim() == "") ? false : true,
                            (mRCAA.ACC11.Trim() == "") ? false : true,
                            (mRCAA.ACC12.Trim() == "") ? false : true,
                            (mRCAA.ACC13.Trim() == "") ? false : true,
                            (mRCAA.ACC14.Trim() == "") ? false : true,
                            (mRCAA.ACC15.Trim() == "") ? false : true,
                            (mRCAA.ACC16.Trim() == "") ? false : true,
                            (mRCAA.ACC17.Trim() == "") ? false : true,
                            (mRCAA.ACC18.Trim() == "") ? false : true,
                            (mRCAA.ACC19.Trim() == "") ? false : true,
                            (mRCAA.ACC20.Trim() == "") ? false : true
                        };
                        }

                        WebCilindrosCore.Helpers.SessionManager.Set("VarUsuario", mUserLiteDTO);

                        res = "OK";

                        return RedirectToAction("Index", "Recepcion");
                    }
                }
                else
                {
                    MdlMessage mdlMessage = Newtonsoft.Json.JsonConvert.DeserializeObject<MdlMessage>(res);
                    res = mdlMessage.Message;
                }
            }
            catch (System.Exception ex)
            {
                res = "ERROR: " + ex.ToString() + ((ex.InnerException != null) ? ex.InnerException.ToString() : "");
            }

            return View();
        }

        [AllowAnonymous]
        public ActionResult CerrarSesion()
        {
            WebCilindrosCore.Helpers.SessionManager.DelAll();

            return Redirect("/");
        }
    }
}