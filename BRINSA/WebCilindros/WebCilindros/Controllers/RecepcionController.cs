﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WebCilindros.Models;

namespace WebCilindros.Controllers
{
    public class RecepcionController : Controller
    {
        WebCilindrosCore.Modules.ClienteModule clienteModule = new WebCilindrosCore.Modules.ClienteModule();
        WebCilindrosCore.Modules.RecepcionModule recepcionModule = new WebCilindrosCore.Modules.RecepcionModule();

        public ActionResult Index()
        {
            if (WebCilindrosCore.Helpers.SessionManager.Get<UserLiteDTO>("VarUsuario") == null)
            {
                return RedirectToAction("Login", "Account", new { donde = Url.Action("Index", "Recepcion") });
            }

            return View();
        }


        [HttpPost]
        public ActionResult Index(MdlCliente mdlCliente)
        {
            if (!ModelState.IsValid)
            {
                return View(mdlCliente);
            }

            var usuario = WebCilindrosCore.Helpers.SessionManager.Get<UserLiteDTO>("VarUsuario");

            if (usuario != null)
            {
                WebCilindrosCore.Entities.RFWCYLRECH recepcion = new WebCilindrosCore.Entities.RFWCYLRECH()
                {
                    CLIENTE = mdlCliente.idCliente,
                    CRTUSR = usuario.Usuario
                };

                recepcionModule.Add(recepcion);

                return RedirectToAction("Index", "RecepcionDetalle", new { recepcion = recepcion.RECEPCION });
            }

            return View();
        }

        [HttpPost]
        public ActionResult ClienteComplete(string Prefix)
        {
            List<WebCilindrosCore.Entities.RCM> ObjList = clienteModule.GetWhere($"SELECT CCUST, CNME FROM RCM WHERE CMID = 'CM' AND (CCUST LIKE '%{Prefix.ToUpper()}%' OR UPPER(CNME) LIKE '%{Prefix.ToUpper()}%') LIMIT 12").ToList();

            return Json(ObjList, JsonRequestBehavior.AllowGet);
        }
    }
}