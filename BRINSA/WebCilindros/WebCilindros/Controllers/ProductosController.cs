﻿using WebCilindros.Models;
using Syncfusion.JavaScript;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;

namespace WebCilindros.Controllers
{
    public class ProductosController : Controller
    {
        Funciones fun = new Funciones();
        WebCilindrosCore.Modules.ProductosModule productosModule = new WebCilindrosCore.Modules.ProductosModule();

        public ActionResult Index()
        {
            IEnumerable<WebCilindrosCore.Entities.RIIMC> productos = productosModule.GetAll();

            WebCilindrosCore.Helpers.SessionManager.Set("DProductos", productos);

            return View();
        }

        public void ExpToExc(string GridModel)
        {
            fun.ExportToExcel(GridModel, WebCilindrosCore.Helpers.SessionManager.Get<List<WebCilindrosCore.Entities.RIIMC>>("DProductos"), "Productos");
        }

        public ActionResult DSProductos(DataManager dm)
        {
            IEnumerable Data = WebCilindrosCore.Helpers.SessionManager.Get<List<WebCilindrosCore.Entities.RIIMC>>("DProductos");

            if (Data == null)
            {
                Data = new List<WebCilindrosCore.Entities.RIIMC>();
            }

            return fun.DSGenerico(dm, Data);
        }

        public ActionResult NuevoEditar(string Codigo, string Descripcion, string Division, string Linea, string Segmento, string Empa, string Tipo, string Clase, string IdRegistro, bool Edita)
        {
            string respuesta = "";
            
            try
            {
                WebCilindrosCore.Entities.RIIMC producto;

                if (Edita)
                {
                    producto = productosModule.GetByPredicate(m => m.RCPROD == IdRegistro).SingleOrDefault();

                    if (producto != null)
                    {
                        producto.RCPROD = Codigo;
                        producto.RCDESC = Descripcion;
                        producto.RCDIVI = Division;
                        producto.RCLINE = Linea;
                        producto.RCSEGM = Segmento;
                        producto.RCEMPA = Empa;
                        producto.RCTIPO = Tipo;
                        producto.CLASDZ = Clase;
                    }

                    productosModule.Upd(producto);
                }
                else
                {
                    producto = new WebCilindrosCore.Entities.RIIMC()
                    {
                        RCPROD = Codigo,
                        RCDESC = Descripcion,
                        RCDIVI = Division,
                        RCLINE = Linea,
                        RCSEGM = Segmento,
                        RCEMPA = Empa,
                        RCTIPO = Tipo,
                        CLASDZ = Clase
                    };

                    productosModule.Add(producto);
                }

                IEnumerable<WebCilindrosCore.Entities.RIIMC> productos = productosModule.GetAll();

                WebCilindrosCore.Helpers.SessionManager.Set("DProductos", productos);

                respuesta = "OK";

            }
            catch (System.Exception ex)
            {
                respuesta = "ERROR: " + ex.ToString() + ((ex.InnerException != null) ? ex.InnerException.ToString() : "");
            }

            return Json(new { result = respuesta });
        }

        public ActionResult Eliminar(string Id)
        {
            string respuesta = "";
            
            try
            {
                productosModule.DelByCod(Id);
                
                IEnumerable<WebCilindrosCore.Entities.RIIMC> productos = productosModule.GetAll();

                WebCilindrosCore.Helpers.SessionManager.Set("DProductos", productos);

                respuesta = "OK";
            }
            catch (System.Exception ex)
            {
                respuesta = "ERROR: " + ex.ToString() + ((ex.InnerException != null) ? ex.InnerException.ToString() : "");
            }

            return Json(new { result = respuesta });
        }

        public DataTable GetDatatable()
        {
            List<WebCilindrosCore.Entities.RIIMC> productos = WebCilindrosCore.Helpers.SessionManager.Get<List<WebCilindrosCore.Entities.RIIMC>>("DProductos");

            DataTable dt = new DataTable($"Producto_{System.DateTime.Now.ToString("yyyy-MM-dd")}");
            
            dt.Columns.AddRange(new DataColumn[] {
                new DataColumn("Código"),
                new DataColumn("Descripción"),
                new DataColumn("División"),
                new DataColumn("Linea"),
                new DataColumn("Segmento"),
                new DataColumn("Empa"),
                new DataColumn("Tipo"),
                new DataColumn("Clase")                
            });

            foreach (var item in productos)
            {
                dt.Rows.Add(
                    item.RCPROD,
                    item.RCDESC,
                    item.RCDIVI,
                    item.RCLINE,
                    item.RCSEGM,
                    item.RCEMPA,
                    item.RCTIPO,
                    item.CLASDZ
                );
            }

            return dt;
        }

        public FileResult GenerateReport()
        {
            DataTable datatable = GetDatatable();

            using (ClosedXML.Excel.XLWorkbook wb = new ClosedXML.Excel.XLWorkbook())
            {
                wb.Worksheets.Add(datatable);
                using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
                {
                    wb.SaveAs(stream);

                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Productos.xlsx");

                    //var result = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.OK)
                    //{
                    //    Content = new System.Net.Http.ByteArrayContent(stream.ToArray())
                    //};
                    //result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
                    //{
                    //    FileName = $"Nomina_{System.DateTime.Now.ToString("yyyy-MM-dd")}.xlsx"
                    //};

                    //result.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

                    //return result;
                }
            }
        }
    }
}