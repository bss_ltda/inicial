﻿Imports System
Imports System.IO
Imports Microsoft.VisualBasic
Imports System.Security.Permissions
Public Class clsInterfaceWMS
    Public Shared Sub Main()

        Run()

    End Sub

    <PermissionSet(SecurityAction.Demand, Name:="FullTrust")>
    Private Shared Sub Run()

        Dim args() As String = System.Environment.GetCommandLineArgs()
        ' If a directory is not specified, exit the program.
        If args.Length <> 2 Then
            ' Display the proper way to call the program.
            Console.WriteLine("Usage: Watcher.exe (directory)")
            Return
        End If


        'Infor\Infor WM Provia\dat\fromhost\BRINSA


        'Dim aCarpetas() As String = {"\\192.168.8.98\Infor\Infor WM Provia\dat\tohost\BRINSA" _
        '                          , "\\192.168.8.98\Infor\Infor WM Almacen\dat\tohost\ALMACEN"}

        Dim aCarpetas() As String = {"\\192.168.0.200\Natura\FTP CSV\Revendedoras" _
                                  , "\\192.168.0.200\Natura\FTP CSV\Pedidos"}

        'D:\Natura\FTP CSV\Pedidos


        For Each Carpeta In aCarpetas
            Console.WriteLine("Revisando: " & Carpeta)
            ' Create a new FileSystemWatcher and set its properties.
            Dim watcher As New FileSystemWatcher()
            watcher.Path = Carpeta 'args(1)
            ' Watch for changes in LastAccess and LastWrite times, and
            ' the renaming of files or directories. 
            watcher.NotifyFilter = (NotifyFilters.LastAccess Or NotifyFilters.LastWrite Or NotifyFilters.FileName Or NotifyFilters.DirectoryName)
            ' Only watch text files.
            watcher.Filter = "*.*"

            ' Add event handlers.
            AddHandler watcher.Changed, AddressOf OnChanged
            AddHandler watcher.Created, AddressOf OnChanged
            AddHandler watcher.Deleted, AddressOf OnChanged
            AddHandler watcher.Renamed, AddressOf OnChanged
            ' AddHandler watcher.Renamed, AddressOf OnRenamed


            ' Begin watching.
            watcher.EnableRaisingEvents = True

            ' Wait for the user to quit the program.
            'Console.WriteLine("Press 'q' to quit the sample.")
            'While Chr(Console.Read()) <> "q"c
            'End While
        Next

        ' Wait for the user to quit the program.
        Console.WriteLine("Press 'q' to quit the sample.")
        While Chr(Console.Read()) <> "q"
        End While

    End Sub

    ' Define the event handlers.
    Private Shared Sub OnChanged(source As Object, e As FileSystemEventArgs)
        Dim wms As String = IIf(e.FullPath.Contains("Almacen"), "Almacen", "Cendis")

        ' Specify what is done when a file is changed, created, or deleted.
        Console.WriteLine("{0} {1} {2}", wms, e.Name, e.ChangeType)
    End Sub

    Private Shared Sub OnRenamed(source As Object, e As RenamedEventArgs)
        ' Specify what is done when a file is renamed.
        'Console.WriteLine("File: {0} renamed to {1}", e.OldFullPath, e.FullPath)
        'Console.WriteLine("Archivo: {0} Listo para la interface. {1}", e.FullPath, e.Name)
        Console.WriteLine("{0} {1}", e.Name, e.ChangeType)
    End Sub
End Class
