﻿Public Class clsApp
    '' Evento para indicar que una propiedad no está disponible
    '' aunque se puede ampliar si se quieren capturar errores
    'Public Event ErrorMsg(ByVal PropertyName As String, ByVal sMessage As String)
    '
    Private mAssembly As System.Reflection.Assembly
    Private mLocation As String
    Private mFileVersionInfo As System.Diagnostics.FileVersionInfo
    '
    ' Al crear la clase hay que pasar forzosamente el tipo del Assembly a usar
    ' por ejemplo: Dim app As New cApp(Me.GetType)
    ' Este constructor es útil para aplicaciones de Windows o librerías (DLL)
    Public Sub New(ByVal theType As Type)
        Me.New(System.Reflection.Assembly.GetAssembly(theType))
    End Sub
    '
    ' Crear una nueva instancia usando un ensamblado.
    ' por ejemplo: Dim App As cApp = New cApp(System.Reflection.Assembly.GetExecutingAssembly)
    Public Sub New(ByVal theAssembly As System.Reflection.Assembly)
        mAssembly = theAssembly
        mLocation = mAssembly.Location
        mFileVersionInfo = System.Diagnostics.FileVersionInfo.GetVersionInfo(mLocation)
    End Sub
    '
    Public ReadOnly Property Comments() As String
        Get
            Return mFileVersionInfo.Comments
        End Get
    End Property
    '
    Public ReadOnly Property CompanyName() As String
        Get
            Return mFileVersionInfo.CompanyName
        End Get
    End Property
    '
    Public ReadOnly Property EXEName() As String
        Get
            Return System.IO.Path.GetFileName(mLocation)
        End Get
    End Property
    '
    Public ReadOnly Property FileDescription() As String
        Get
            Return mFileVersionInfo.FileDescription
        End Get
    End Property
    '
    Public ReadOnly Property HInstance() As Int32
        Get
            Return System.Runtime.InteropServices.Marshal.GetHINSTANCE(mAssembly.GetModules()(0)).ToInt32
        End Get
    End Property
    '
    Public ReadOnly Property LegalCopyright() As String
        Get
            Return mFileVersionInfo.LegalCopyright
        End Get
    End Property
    '
    Public ReadOnly Property LegalTrademarks() As String
        Get
            Return mFileVersionInfo.LegalTrademarks
        End Get
    End Property
    '
    Public ReadOnly Property Major() As Int32
        Get
            Return Me.FileMajorPart
        End Get
    End Property
    '
    Public ReadOnly Property Minor() As Int32
        Get
            Return Me.FileMinorPart
        End Get
    End Property
    '
    Public ReadOnly Property Path(Optional ByVal conBackSlash As Boolean = False) As String
        Get
            If conBackSlash Then
                Return System.IO.Path.GetDirectoryName(mLocation) & "\"
            Else
                Return System.IO.Path.GetDirectoryName(mLocation)
            End If
        End Get
    End Property
    '
    '
    Public ReadOnly Property ProductName() As String
        Get
            Return mFileVersionInfo.ProductName
        End Get
    End Property
    '
    Public ReadOnly Property Revision() As Int32
        Get
            Return Me.FileBuildPart
        End Get
    End Property
    '
    Public ReadOnly Property Title() As String
        Get
            Try
                Return mAssembly.GetName.Name
            Catch
                Return mFileVersionInfo.ProductName
            End Try
        End Get
    End Property
    '
    '--------------------------------------------------------------------------
    ' Estas propiedades no son parte del objeto App.
    '--------------------------------------------------------------------------
    '
    Public ReadOnly Property Build() As Int32
        Get
            Return Me.FileBuildPart
        End Get
    End Property
    Public ReadOnly Property FileBuildPart() As Int32
        Get
            Return mFileVersionInfo.FileBuildPart
        End Get
    End Property
    Public ReadOnly Property FileMajorPart() As Int32
        Get
            Return mFileVersionInfo.FileMajorPart
        End Get
    End Property
    Public ReadOnly Property FileMinorPart() As Int32
        Get
            Return mFileVersionInfo.FileMinorPart
        End Get
    End Property
    Public ReadOnly Property FilePrivatePart() As Int32
        Get
            Return mFileVersionInfo.FilePrivatePart
        End Get
    End Property
    Public ReadOnly Property FileVersion() As String
        Get
            Return mFileVersionInfo.FileVersion
        End Get
    End Property
    '
    ' Devuelve el valor del Location del Assemby usado              (09/Dic/01)
    Public ReadOnly Property Location() As String
        Get
            Return mLocation
        End Get
    End Property

End Class
