﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' La información general sobre un ensamblado se controla mediante el siguiente 
' conjunto de atributos. Cambie estos atributos para modificar la información
' asociada con un ensamblado.

' Revisar los valores de los atributos del ensamblado

<Assembly: AssemblyTitle("ListadoEmpaqueG")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("BSSLTDA")> 
<Assembly: AssemblyProduct("Listado Empaque General")> 
<Assembly: AssemblyCopyright("Copyright © BSSLTDA 2016")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'El siguiente GUID sirve como identificador de typelib si este proyecto se expone a COM
<Assembly: Guid("77b94486-6b0b-4ba9-83fc-4affda45706b")> 

' La información de versión de un ensamblado consta de los cuatro valores siguientes:
'
'      Versión principal
'      Versión secundaria 
'      Número de compilación
'      Revisión
'
' Puede especificar todos los valores o usar los valores predeterminados de número de compilación y de revisión 
' mediante el asterisco ('*'), como se muestra a continuación:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.4")> 
<Assembly: AssemblyFileVersion("1.0.0.4")> 
