﻿Imports ADODB
Imports System.Globalization
Imports System
Imports System.IO

Module BSSCommon
    Public Const tNum As Integer = 1
    Public Const tStr As Integer = 2

    Public DB As New clsConexion
    Public fSql As String
    Public vSql As String

    Public CARPETA_SITIO As String
    Public CARPETA_IMG As String
    Public C_UPLOAD_DIR As String
    Public CARPETA_LOG As String


    Public Enum tDato
        tNum = 1
        tStr = 2
        tXfn = 3
    End Enum


    Function AbreConexion() As Boolean
        Dim forceDotCulture As CultureInfo
        forceDotCulture = Application.CurrentCulture.Clone()
        forceDotCulture.NumberFormat.NumberDecimalSeparator = "."
        forceDotCulture.NumberFormat.NumberGroupSeparator = ","
        Application.CurrentCulture = forceDotCulture

        DB.gsDatasource = My.Settings.AS400
        DB.gsLib = My.Settings.AMBIENTE
        Return DB.Conexion()

    End Function

    Function Ceros(val As Integer, c As Integer)

        Dim result As String
        Dim sCeros As String = New String("0", c)

        result = val.ToString(sCeros)

        Return result

    End Function

    Function Ceros(val As Double, c As Integer)

        Dim result As String
        Dim sCeros As String = New String("0", c)

        result = val.ToString(sCeros)

        Return result

    End Function

    Function Ceros(val As String, c As Integer)

        Dim result As String
        Dim sCeros As String = New String("0", c)

        result = Right(sCeros & val, c)

        Return result

    End Function

    Sub mkSql(ByVal tipoF As Integer, ByVal Campo As String, ByVal valor As Integer, Optional ByVal Fin As Integer = 0)
        mkSql(tipoF, Campo, valor.ToString, Fin)
    End Sub
    Sub mkSql(ByVal tipoF As Integer, ByVal Campo As String, ByVal valor As Double, Optional ByVal Fin As Integer = 0)
        mkSql(tipoF, Campo, valor.ToString, Fin)
    End Sub
    Sub mkSql(ByVal tipoF As Integer, ByVal Campo As String, ByVal valor As Long, Optional ByVal Fin As Integer = 0)
        mkSql(tipoF, Campo, valor.ToString, Fin)
    End Sub
    Sub mkSql(ByVal tipoF As Integer, ByVal Campo As String, ByVal valor As String, Optional ByVal Fin As Integer = 0)
        Dim sep As String

        If Left(Trim(UCase(fSql)), 6) = "INSERT" Then
            sep = IIf(tipoF = tStr, "'", "")
            fSql = fSql & Campo
            Select Case Fin
                Case 0
                    fSql = fSql & " , "
                Case 10, 11, -1, 1
                    fSql = fSql & " ) "
            End Select
            If IsDBNull(valor) Then
                valor = IIf(tipoF = tStr, "", "0")
            End If
            vSql = vSql & sep & CStr(valor) & sep
            Select Case Fin
                Case 10
                    vSql = vSql & "  "
                Case 11, -1, 1
                    vSql = vSql & " )"
                Case 0
                    vSql = vSql & " , "
            End Select
        Else
            sep = IIf(tipoF = tStr, "'", "")
            If IsDBNull(valor) Then
                valor = IIf(tipoF = tStr, "", "0")
            End If
            fSql = fSql & Campo & " = " & sep & CStr(valor) & sep & IIf(Fin = 10 Or Fin = 11, " ", " , ")
        End If

    End Sub

    'timeStampWin()
    Function timeStampWin() As String

        Return Now.Year.ToString & "-" & Now.Month.ToString("00") & "-" & Now.Day.ToString("00") & "-" & _
               Now.Hour.ToString("00") & "." & Now.Minute.ToString("00") & "." & Now.Second.ToString("00") & ".000000"

    End Function


    Function AppEXEName() As String
        Dim aApp() As String = System.Reflection.Assembly.GetExecutingAssembly().Location.Split("\")
        Return aApp(UBound(aApp))
    End Function

    Public Sub WrtTxtError(Archivo As String, ByVal Texto As String)

        Using w As StreamWriter = File.AppendText(Archivo)
            Log(Texto, w)
        End Using

    End Sub
    Public Sub Log(logMessage As String, w As TextWriter)
        w.Write(vbCrLf + "Entrada de Log : ")
        w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString())
        w.WriteLine("  :{0}", logMessage)
        w.WriteLine(" ")
    End Sub

End Module
