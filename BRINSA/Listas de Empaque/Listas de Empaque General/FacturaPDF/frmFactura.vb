﻿Imports PDF_In_The_BoxCtl
Imports ADODB

Public Class frmFactura
    Dim bPaginaNueva As Boolean
    Dim rsTit As New ADODB.Recordset
    Dim rsCab As New ADODB.Recordset
    Dim rsPed As New ADODB.Recordset
    Dim rsDet As New ADODB.Recordset
    Dim rsNotas As New ADODB.Recordset
    Dim DocScanner As Integer, Observ As String, ImprimeIAL As String
    Dim bColor As Boolean
    Dim bQuimicos As Boolean
    Dim bExport As Boolean
    Dim Flag3
    Dim Sellos() As String
    Dim Fila As Integer
    Dim TotNotas As Integer
    Dim TotFilas As Integer
    Dim Ta As TBoxTable

    Const CELDA_NORMAL As String = "BrushColor = 255, 255, 255"
    'Const CELDA_SOMBRA As String = "BrushColor = 215, 215, 215"
    Const CELDA_SOMBRA As String = "BrushColor = 255, 255, 255"
    Const TABLE_HEADER_STYLE As String = "ChildrenStyle=""BorderStyle=rect;FontSize=9;Fontname=Arial;Alignment=Center;VertAlignment=Center"""
    Const ESTILO_TIT As String = "Normal;fontsize=6;Fontname=Corbel;BorderColor=Black;Alignment=Center;VertAlignment=Center"


    Private Sub frmFactura_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim parametro As String
        Dim aSubParam() As String
        Dim qPrinter As String = ""
        Dim rs As New ADODB.Recordset
        Dim bGenerar As Boolean
        Dim campoParam As String = ""
        Flag3 = 1
        Environment.GetCommandLineArgs()
        For Each parametro In Environment.GetCommandLineArgs()
            aSubParam = Split(parametro, "=")
            Select Case UCase(aSubParam(0))
                Case "S"
                    CARPETA_SITIO = Trim(aSubParam(1))
                Case "O"
                    Origen = Trim(aSubParam(1))
                Case "P"
                    Pedido = Ceros(Trim(aSubParam(1)), 8)
                Case "TAREA"
                    TASK_No = aSubParam(1)
            End Select
        Next


        DB.bDateTimeToChar = True
        AbreConexion()
        logDocNum = DB.CalcConsec("RFLOGDOCA", "9999999999")

        If My.Settings.LOCAL = "SI" Then
            campoParam = "CCNOT2"
        End If
        CARPETA_SITIO = DB.getLXParam("SITIO_CARPETA", "TM", campoParam)
        CARPETA_IMG = DB.getLXParam("CARPETA_IMG", "TM", campoParam)
        APP_PATH = DB.getLXParam("LOGOEMPRESA", "TM", campoParam)
        bPRUEBAS = My.Settings.PRUEBAS.ToUpper() = "SI"

        fSql = "SELECT DOCCLI FROM RDCC WHERE DPEDID = " & Pedido & " FETCH FIRST 1 ROWS ONLY "
        If DB.OpenRS(rs, fSql) Then
            Factura_ID = rs("DOCCLI").Value
            Factura_ID = CStr(CInt(Factura_ID.Replace("EXP", "")))
        End If
        rs.Close()

        PDF_Folder = CARPETA_IMG & "pdf\ListadoEmpaque"
        LOG_File = PDF_Folder & "\" & Factura_ID & ".txt"
        WrtTxtError(LOG_File, Factura_ID)
        Dim arguments As String() = Environment.GetCommandLineArgs()
        WrtTxtError(LOG_File, String.Join(", ", arguments))

        If Not DB.CheckVer() Then
            DB.WrtSqlError(DB.lastError, "")
            End
        End If

        PDFEXE = PDFPrtExe()
        bGenerar = True

        If My.Settings.PRUEBAS = "SI" Then
            GeneraPdf(bGenerar)
        Else
            Try
                If GeneraPdf(bGenerar) Then
                    fSql = " UPDATE RFTASK SET STS2 = 1  "
                    fSql = fSql & " WHERE TNUMREG = " & TASK_No
                    DB.ExecuteSQL(fSql)
                End If
            Catch ex As Exception
                Debug.Print(ex.Message)
                WrtTxtError(LOG_File, ex.Message)
                DB.WrtSqlError(ex.Message, "GeneraPDF")
                End
            End Try
        End If

        If qPrinter <> "" Then
            If qPrinter <> "GEN" Then
                Imprimir(qPrinter)
            End If
        Else
            Imprimir("")
        End If

        DB.Close()

Manejo_Error:
        If Err.Number <> 0 Then
            WrtTxtError(LOG_File, "Main")
            End
        End If


        Application.Exit()
    End Sub

    Public Function GeneraPdf(bGenerar As Boolean) As Boolean
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim ArchivoPDF As String
        Dim Resultado As Boolean = False

        Fila = 0

        fSql = "SELECT * FROM RDCCH "
        fSql = fSql & " WHERE CPEDID = " & Pedido
        If Not DB.OpenRS(rsCab, fSql, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly) Then
            rsCab.Close()
            WrtTxtError(LOG_File, "Pedido no se encontro.")
            Return False
        End If

        If Not bGenerar Then
            rsCab.Close()
            rsDet = Nothing
            rsNotas = Nothing
            rsTit = Nothing
            rsCab = Nothing
            Return False
        End If

        fSql = "SELECT * FROM RFPARAM WHERE CCTABL = 'PACKINGLIST' AND CCCODE = 'TIT'"
        DB.OpenRS(rsTit, fSql, 3, 3)

        Idioma = Origen

        '*****************************************************************
        '   DETALLE
        '*****************************************************************
        fSql = " SELECT "
        fSql = fSql & " CASE WHEN XCNTR = '0' THEN '' ELSE XCNTR END XCNTR, "                       '//2A    
        fSql = fSql & " DPROD , DOCCLI, "                       '//8P0   
        fSql = fSql & " DINAME,  "
        fSql = fSql & " DOUBLE(DCANT) DCANT, IUMS,"                       '//35A   
        fSql = fSql & " DOUBLE(DCANT*DEC(IWGHT, 15, 5 )) DPESO , IMWTUM ,"                       '//50A   
        fSql = fSql & " DOUBLE(IVULI*DCANT/1000000) VOLUMEN, 'MT3' MT3,  "                       '//2A    
        fSql = fSql & " DOUBLE(DCANT*DEC(IMNNWU, 15, 5 )) DIPESN   "   '//5P2   Porc de Impu
        fSql = fSql & " FROM RDCC INNER JOIN RHCC ON RDCC.DCONSO = RHCC.HCONSO"
        fSql = fSql & " INNER JOIN IIM ON RDCC.DPROD = IPROD"
        fSql = fSql & " WHERE  DPEDID = " & Pedido
        If Not DB.OpenRS(rsDet, fSql, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly) Then
            rsTit.Close()
            rsDet.Close()
            rsCab.Close()
            WrtTxtError(LOG_File, "Pedido sin detalle 1.")
            Return False
        End If
        '*****************************************************************
        '   FIN DETALLE
        '*****************************************************************

        fSql = " Select  DF.DPREFIJ, "
        fSql = fSql & "  DOUBLE(DF.DFACTUR) DFACTUR, "
        fSql = fSql & "  C.HPRCDVA , "
        fSql = fSql & "  DOUBLE(Sum(DF.DCANTID * DEC(IIM.IWGHT, 15, 5))) As PBRUTO, "
        fSql = fSql & "  Max(IIM.IMWTUM) IMWTUM, "
        fSql = fSql & "  Max(F.FFECFAC) As FECHAFAC, "
        fSql = fSql & "  DOUBLE(Sum(DF.DCANTID * DEC(IIM.IMNNWU, 15, 5))) As PNETO, "
        fSql = fSql & "  DOUBLE(Sum(DF.DVALTOT)) As VALOR, "
        fSql = fSql & "  F.FMONEDA "
        fSql = fSql & " From "
        fSql = fSql & "  RFFACDET DF Inner Join "
        fSql = fSql & "  RFFACCAB F "
        fSql = fSql & "    On DF.DPREFIJ = F.FPREFIJ And DF.DFACTUR = F.FFACTUR Inner Join "
        fSql = fSql & "  IIM "
        fSql = fSql & "    On DF.DCODPRO = IIM.IPROD Inner Join "
        fSql = fSql & "  RHCC C "
        fSql = fSql & "    On DF.DCONSOL = C.HCONSO "
        fSql = fSql & " Where "
        fSql = fSql & "  DF.DPEDIDO = " & Pedido
        fSql = fSql & " Group By "
        fSql = fSql & "  DF.DPREFIJ, DF.DFACTUR, F.FMONEDA, C.HPRCDVA "

        If Not DB.OpenRS(rsNotas, fSql, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly) Then
            rsTit.Close()
            rsDet.Close()
            rsCab.Close()
            WrtTxtError(LOG_File, "Pedido sin detalle.2")
            Return False
        End If

        'Crea temporal de notas y totaliza detalle y numero de notas
        ''''REVINotasFactura()

        ''''REVIfSql = "SELECT CAT, SNSEQ, SNDESC FROM QTEMP." & Pedido & " WHERE SNDESC <> '' ORDER BY CAT, SNSEQ "
        ''''REVIlogDocsAutoSQL(fSql)
        ''''REVIDB.OpenRS(rsNotas, fSql, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly)
        ArchivoPDF = PDF_Folder & "\" & Factura_ID & ".pdf"
        Resultado = ArmaPDF(ArchivoPDF)

        rsDet.Close()
        rsNotas.Close()
        rsTit.Close()
        rsCab.Close()
        Return Resultado

    End Function

    Function ArmaPDF(ArchivoPDF As String) As Boolean
        Dim Ta1 As TBoxTable
        Dim Detail As TBoxBand
        Dim Note As TBoxBand
        Dim Header As TBoxBand
        Dim Footer As TBoxBand
        Dim AfterFooter As TBoxBand
        Dim b2 As TBoxBand
        Dim sError As String
        ArmaPDF = False

        If Dir(ArchivoPDF) <> "" Then
            On Error Resume Next
            Kill(ArchivoPDF)
            If Err.Number <> 0 Then
                sError = Err.Description
                WrtTxtError(LOG_File, "ArmaPDF: Borrar " & ArchivoPDF & " " & sError)
                rsDet.Close()
                rsNotas.Close()
                rsTit.Close()
                rsCab.Close()
                rsDet = Nothing
                rsNotas = Nothing
                rsTit = Nothing
                rsCab = Nothing
                DB.Close()
                End
            End If
            On Error GoTo 0
        End If

        ' Titulos
        With Me.AxPdfBox1
            .FileName = ArchivoPDF
            .Title = "Lista de Empaque General  - " & Factura_ID
            .WantShow = bPRUEBAS
            .WantPageCount = True
            .PaperSizeName = "Letter"
            .BeginDoc()
            '.BottomMargin = 50
            'table
            Fila = 0
            Flag = 0
            Ta = .CreateTable("BorderStyle=None;Margins=130,200,100,150")
            'Set Ta = .CreateTable("BorderStyle=none;Alignment=Center;Margins=113,200,100,150")
            Ta.Assign(rsDet)

            bPaginaNueva = True
            'Tabla del Detalle
            Detail = Ta.CreateBand()
            Detail.Role = TBandRole.rlDetail
            Detail.ChildrenStyle = "FontSize=7;Fontname=Arial;VertAlignment=Center"
            Detail.Height = 45
            Detail.ChildrenStyle = "FontSize=7;Fontname=Arial; VertAlignment=Center;BorderStyle=LeftRight"
            Detail.CreateCell(260, "BorderStyle=Left").CreateText("Alignment=Left;BorderLeftMargin=10;BorderLeftMargin=10").Bind("XCNTR")
            Detail.CreateCell(150, "BorderStyle=none").CreateText("Alignment=Left;BorderLeftMargin=10;BorderLeftMargin=10").Bind("DPROD")
            Detail.CreateCell(660, "BorderStyle=none").CreateText("Alignment=Left;BorderLeftMargin=10;BorderLeftMargin=10").Bind("DINAME")

            With Detail.CreateCell(160, "BorderStyle=none").CreateText
                .Bind("DCANT")
                .Format = "#,##0.00"
                .Alignment = 1 'haRight
            End With

            With Detail.CreateCell(50, "BorderStyle=none").CreateText
                .Bind("IUMS")
                .Alignment = 1 'haRight
                .BorderRightMargin = 10
            End With

            With Detail.CreateCell(150, "BorderStyle=none").CreateText
                .Bind("VOLUMEN")
                .Format = "#,##0.000"
                .Alignment = 1 'haRight
                .BorderRightMargin = 5
            End With

            With Detail.CreateCell(60, "BorderStyle=none").CreateText
                .Bind("MT3")
                .Alignment = 1 'haRight
                .BorderRightMargin = 10
            End With

            With Detail.CreateCell(160, "BorderStyle=none").CreateText
                .Bind("DIPESN")
                .Format = "#,##0.000"
                .Alignment = 1 'haRight
                .BorderRightMargin = 10
                .BorderLeftMargin = 10
            End With

            With Detail.CreateCell(50, "BorderStyle=none").CreateText
                .Bind("IMWTUM")
                .Alignment = 1 'haRight
                .BorderRightMargin = 10
            End With

            With Detail.CreateCell(160, "BorderStyle=none").CreateText
                .Bind("DPESO")
                .Format = "#,##0.000"
                .Alignment = 1 'haRight
                .BorderRightMargin = 10
                .BorderLeftMargin = 10
            End With

            With Detail.CreateCell(50, "BorderStyle=Right").CreateText
                .Bind("IMWTUM")
                .Alignment = 1 'haRight
                .BorderRightMargin = 10
            End With

            'NUEVO
            Detail.Breakable = True

            Ta.Put()

            Flag = 1
            b2 = .CreateBand("BorderStyle=LeftRight;Margins=130,200,100,150")
            b2.CreateCell(1910, "BorderStyle=Top").CreateText().Assign("")
            b2.Put()

            Dim b As TBoxBand
            b = .CreateBand("Margins=130,200,100,150; BorderStyle=Rect")
            b.ChildrenStyle = "Normal;fontsize=7;Fontname=Arial;VertAlignment=Bottom"
            b.BrushColor = RGB(215, 215, 215)

            b.CreateCell(200, "BorderStyle=Rect").CreateText("BorderLeftMargin = 10;BorderLeftMargin = 10").Assign(CStr(Titulo("FACTURA")))                                         'Codigo
            b.CreateCell(300, "BorderStyle=Rect").CreateText("BorderLeftMargin = 10;BorderLeftMargin = 10").Assign("" + Titulo("TOTAL") & " " & rsNotas("FMONEDA").Value)                                         'Descripcion
            b.CreateCell(710, "BorderStyle=Rect").CreateText("Alignment=Center;BorderLeftMargin = 10;BorderLeftMargin = 10").Assign("" + Titulo("EMPAQUE"))                        'UNIDADES
            b.CreateCell(350, "BorderStyle=Rect").CreateText("Alignment=Center;BorderLeftMargin = 10;BorderLeftMargin = 10").Assign("" + Titulo("PESO_NETO"))                        '%IVA
            b.CreateCell(350, "BorderStyle=Rect").CreateText("Alignment=Center;BorderLeftMargin = 10;BorderLeftMargin = 10").Assign("" + Titulo("PESO_BRUTO"))   'CANT
            b.Put()

            Flag3 = 1
            Flag = 0
            Ta1 = .CreateTable("BorderStyle=None;Margins=130,200,100,150")
            'Set Ta = .CreateTable("BorderStyle=none;Alignment=Center;Margins=113,200,100,150")
            Ta1.Assign(rsNotas)

            bPaginaNueva = True
            'Tabla del Detalle
            Detail = Ta1.CreateBand()
            Detail.Role = TBandRole.rlDetail
            Detail.ChildrenStyle = "FontSize=7;Fontname=Arial;VertAlignment=Center"
            Detail.Height = 45
            Detail.ChildrenStyle = "FontSize=7;Fontname=Arial; VertAlignment=Center;BorderStyle=LeftRight"
            Detail.CreateCell(50, "BorderStyle=Left").CreateText("Alignment=Left;BorderLeftMargin = 10;BorderLeftMargin = 10").Bind("DPREFIJ")
            'Detail.CreateCell(200, "BorderStyle=Left").CreateText("Alignment=Left").Bind("DFACTUR")
            With Detail.CreateCell(150, "BorderStyle=none").CreateText
                .Bind("DFACTUR")
                .Format = "###000"
                .Alignment = 2 'haRight
                .BorderRightMargin = 10
                .BorderLeftMargin = 10
            End With
            Detail.CreateCell(100, "BorderStyle=Left").CreateText("Alignment=Right;BorderRightMargin=10").Bind("FMONEDA")
            With Detail.CreateCell(200, "BorderStyle=right").CreateText
                .Bind("VALOR")
                .Format = "#,##0.000"
                .Alignment = 2 'haRight
                .BorderRightMargin = 10
                .BorderLeftMargin = 10
            End With
            Detail.CreateCell(710, "BorderStyle=right").CreateText("Alignment=Center;BorderLeftMargin = 10;BorderLeftMargin = 10").Bind("HPRCDVA")

            'Detail.CreateCell(400, "BorderStyle=none").CreateText("Alignment=Right").Bind("PBRUTO")
            'Detail.CreateCell(400, "BorderStyle=none").CreateText("Alignment=Right").Bind("PNETO")

            With Detail.CreateCell(300, "BorderStyle=none").CreateText
                .Bind("PNETO")
                .Format = "#,##0.000"
                .Alignment = 1 'haRight
                .BorderLeftMargin = 10
            End With

            With Detail.CreateCell(50, "BorderStyle=Right").CreateText
                .Bind("IMWTUM")
                .Alignment = 1 'haRight
                .BorderRightMargin = 10
            End With

            With Detail.CreateCell(300, "BorderStyle=none").CreateText
                .Bind("PBRUTO")
                .Format = "#,##0.000"
                .Alignment = 1 'haRight
                .BorderRightMargin = 10
                .BorderLeftMargin = 10
            End With

            With Detail.CreateCell(50, "BorderStyle=Right").CreateText
                .Bind("IMWTUM")
                .Alignment = 1 'haRight
                .BorderRightMargin = 10
            End With
            'NUEVO
            Detail.Breakable = True

            Ta1.Put()

            .EndDoc()
            Debug.Print(.FileName())
            ArchivoPDF = .FileName

        End With
        Return True

    End Function

 

    Private Sub AxPdfBox1_OnBottomOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnBottomOfPageEvent) Handles AxPdfBox1.OnBottomOfPage
        Dim b As TBoxBand
        Dim rs As New ADODB.Recordset
        Dim aqui As String = ""
        Dim Pie As String
        Dim bSalto As Boolean

        With AxPdfBox1

            b = .CreateBand("BorderStyle=bottom;Margins=130,200,100,150")
            b.Breakable = False
            b.CreateCell(1910, "BorderStyle=None").CreateText("Alignment=left").Assign("")
            b.Put()
            If e.lastPage Then
                FinDocumento(bSalto)
            End If

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,150")
            b.Breakable = False
            b.Height = 25
            b.CreateCell(1910, "BorderStyle=None").CreateText("Alignment=left").Assign("")
            b.Put(130, 2390)

            'Fila = 0
            'Pie = ""
            'Pie = Pie & Titulo("PIE02") & vbCrLf
            'Pie = Pie & Titulo("PIE03") & vbCrLf
            'Pie = Pie & Titulo("PIE04") & vbCrLf
            'Pie = Pie & Titulo("PIE05") & vbCrLf
            'Pie = Pie & Titulo("PIE06") & vbCrLf

            'b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            'b.ChildrenStyle = "BorderStyle=None;Alignment=Center"
            'b.CreateCell(1030).CreateText().Assign("" + Pie)
            'b.CreateCell(400).CreateText().Assign("")
            'b.CreateCell(480).CreateImage("Alignment=Right;VertAlignment=Top").Assign(APP_PATH + "\abajo_dcha.png")
            'b.Put()

            'Select Case Factura_Marca
            '    Case "O"     'Original
            '        Factura_Marca = Titulo("PIE07")
            '    Case "CC"    'Copia Cliente
            '        Factura_Marca = Titulo("PIE08")
            '    Case "C"     'Copia
            '        Factura_Marca = Titulo("PIE09")
            'End Select

            'b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            'b.ChildrenStyle = "BorderStyle=None;Alignment=Center"
            'b.CreateCell(900).CreateText("FontName=Arial;FontSize=6;Alignment=Left").Assign("Pag. {%PageNumber}" & IIf(Idioma = "ESP", " de ", " of ") & "{%PageCount}")
            'b.CreateCell(530).CreateText("FontName=Arial;FontSize=6;Alignment=Left").Assign("v." + APP_VERSION)
            'b.CreateCell(480).CreateText("FontName=Arial;FontSize=6;Alignment=Right").Assign("" + Factura_Marca)
            'b.Put()

            '.Put(Titulo("DER01" & Pedido), 2071, 2100, 3800, 2500, "FontDegreeAngle = 90")

        End With

Manejo_Error:
        If Err.Number <> 0 Then
            WrtTxtError(LOG_File, "OnBottomOfPage " & aqui & vbCrLf & Err.Description)
            End
        End If

    End Sub

    Sub FinDocumento(bSalto As Boolean)
        Dim b As TBoxBand
        Dim txt As String
        Dim rs As New ADODB.Recordset
        Dim mUni As String
        Dim mCent As String

        'Monto escrito
        ' ''fSql = "SELECT * FROM RFPARAM WHERE CCTABL = 'MONEDA' AND CCCODE = '" & rsCab("FMONEDA").Value & "' AND CCCODE2 = '" & Idioma & "'"
        ' ''If DB.OpenRS(rs, fSql) Then
        ' ''    mUni = rs("CCDESC").Value
        ' ''    mCent = rs("CCSDSC").Value
        ' ''End If
        ' ''rs.Close()
        ' ''rs = Nothing

        With Me.AxPdfBox1

            If bSalto Then
                Fila = 0
                LinesAdd(17)
            Else
                LinesAdd(29 - Fila)
                Fila = 0
            End If

            'b = .CreateBand("Margins=130,200,100,150")
            'b.Height = 15
            'b.ChildrenStyle = "Normal;fontsize=9;Fontname=Arial"
            'b.CreateCell(1910, "BorderStyle=None").CreateText("Alignment=LEFT").Assign("")
            'b.CreateCell(1910).CreateText().Assign("")


            b = .CreateBand("BorderStyle=None;Margins=130,200,100,150")
            b.ChildrenStyle = "BorderStyle=None;Alignment=Left"
            b.Height = 15
            b.CreateCell(1910).CreateText().Assign("")
            b.Put(130, 1900)


          
      
        End With

    End Sub


    Function Titulo(Clave As String) As String

        Titulo = Clave & " ??"
        rsTit.Filter = "CCCODE2='" & Clave & "'"
        If Not rsTit.EOF Then
            If Idioma = "ESP" Then
                Titulo = rsTit("CCNOT1").Value
            Else
                Titulo = rsTit("CCNOT2").Value
            End If
        End If

    End Function

    Function TituloListado(Cliente As String)
        Dim rs As New ADODB.Recordset
        Dim fSql As String
        Dim result As String

        fSql = "SELECT RNNUMREG FROM RFBRULES WHERE RNCATEG01 = 'EXPORTACIONES' AND RNCATEG02 = 'LISTADO EMPAQUE' AND RNCATEG03 = 'TITULO' AND RNCUST = " & Cliente
        If DB.OpenRS(rs, fSql) Then
            result = Titulo("TITULO2")
        Else
            result = Titulo("TITULO")
        End If
        rs.Close()
        rs = Nothing
        Return result

    End Function

    Function Emergencia(Pedido As String) As String
        Dim rs As New ADODB.Recordset

        Emergencia = ""
        fSql = " SELECT * FROM ECH INNER JOIN ECL ON HORD=LORD  "
        fSql = fSql & " WHERE LCONT='SALGR'  "
        fSql = fSql & " AND HTAX IN('CGCOE', 'CRCME', 'CRCNE', 'CRSME', 'CLESE')   "
        fSql = fSql & " AND HORD = " & Pedido
        If DB.OpenRS(rs, fSql) Then
            rs.Close()
            fSql = " SELECT SNSEQ, SNDESC   "
            fSql = fSql & " FROM ESNL01 "
            fSql = fSql & " WHERE SNTYPE = 'O' AND SNCUST = 88888888 "
            fSql = fSql & " ORDER BY SNSEQ  "
            DB.OpenRS(rs, fSql)
            Do While Not rs.EOF
                Emergencia = Emergencia & rs("SNDESC").Value & " "
                rs.MoveNext()
            Loop
        End If
        rs.Close()

    End Function


    Sub DetallePedido1(ByVal sqlDeta As String)

        DB.DropTable("QTEMP.FA" & logDocNum)

        fSql = "CREATE TABLE QTEMP.FA" & logDocNum & " (  "
        fSql = fSql & "     XCNTR CHAR(30) CCSID 37 NOT NULL ,  "
        fSql = fSql & "     DPROD CHAR(35) CCSID 37 NOT NULL ,  "
        fSql = fSql & "     DINAME CHAR(50) CCSID 37 NOT NULL ,  "
        fSql = fSql & "     DCANT DECIMAL(11, 3) NOT NULL , "
        fSql = fSql & "     DPESO DECIMAL(11, 3) NOT NULL , "
        fSql = fSql & "     DPESOP DECIMAL(11, 3) NOT NULL , "
        fSql = fSql & "     DIPESN DECIMAL(11, 3) NOT NULL )   "
        DB.ExecuteSQL(fSql)

        fSql = "INSERT INTO QTEMP.FA" & logDocNum & "(XCNTR, DPROD, DINAME, DCANT, DPESO, DPESOP, "
        fSql = fSql & " DIPESN) "
        DB.ExecuteSQL(fSql & sqlDeta)


    End Sub

    Sub DetallePedido2(ByVal sqlDeta As String)

        DB.DropTable("QTEMP.FAC" & logDocNum)

        fSql = "CREATE TABLE QTEMP.FAC" & logDocNum & " (  "
        fSql = fSql & "     DPREFIJ CHAR(2) CCSID 37 NOT NULL ,  "
        fSql = fSql & "     DFACTUR  DECIMAL(8, 0) NOT NULL ,  "
        fSql = fSql & "     HPRCDVA CHAR(50)  NOT NULL ,  "
        fSql = fSql & "     FMONEDA CHAR(3)  NOT NULL ,  "
        fSql = fSql & "     PBRUTO DECIMAL(11, 3) NOT NULL , "
        fSql = fSql & "     IMWTUM CHAR(2) CCSID 37 NOT NULL ,  "
        fSql = fSql & "     PNETO DECIMAL(11, 3) NOT NULL , "
        fSql = fSql & "     VALOR DECIMAL(11, 3) NOT NULL )   "
        DB.ExecuteSQL(fSql)

        fSql = "INSERT INTO QTEMP.FAC" & logDocNum & "(DPREFIJ, DFACTUR, HPRCDVA, FMONEDA, PBRUTO, IMWTUM,  PNETO, VALOR) "
        DB.ExecuteSQL(fSql & sqlDeta)


    End Sub
    Sub LinesAdd(lineas As Integer)

        Exit Sub

    End Sub


    Private Sub AxPdfBox1_OnTopOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnTopOfPageEvent) Handles AxPdfBox1.OnTopOfPage
        On Error GoTo Manejo_Error
        Dim b2 As TBoxBand
        Dim b As TBoxBand
        Dim b4 As TBoxBand
        Dim b5 As TBoxBand
        Dim b6 As TBoxBand
        Dim img As TBoxImage
        Dim AvisosCab As String

        With Me.AxPdfBox1
            .DefineStyle("Normal", "0; Margins=130,200,100,150; fontsize=10;Fontname=Corbel;BorderColor=Black")
            .Style = "Normal"
            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "BorderStyle=None;Alignment=Center"
           
            b.CreateCell(380).CreateImage("Alignment=Right;VertAlignment=Top").Assign(APP_PATH + "\BRINSA-IAL.png")
            b.CreateCell(1030).CreateText().Assign("" + TituloListado(rsCab("CCUST").Value))
            b.CreateCell(500).CreateText().Assign("")
            b.Put()


            b = .CreateBand("Margins=130,200,100,150")
            b.Height = 35
            b.CreateCell(1910, "BorderStyle=None").CreateText().Assign("")
            b.Put()

            b = .CreateBand("Margins=130,200,100,150")
            b.Height = 35
            b.CreateCell(1910, "BorderStyle=None").CreateText().Assign("")
            b.Put()

            'NUMERO FACTURA
            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(330).CreateText("Alignment=Left;FontSize=9").Assign(" " + Titulo("CLIENTE"))
            b.CreateCell(700).CreateText("Alignment=Left;FontSize=9").Assign(" " + rsCab("CHNAME").Value)
            b.CreateCell(480).CreateText("Alignment=Right;FontSize=9").Assign(" ")
            b.CreateCell(400, "BorderStyle=none").CreateText("FontName=Arial; FontSize=12").Assign("" + Titulo("PEDIDO") + " " & Factura_ID)
            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(330).CreateText("Alignment=Left;FontSize=9").Assign(" " + Titulo("FECHA").ToString)
            b.CreateCell(700).CreateText("Alignment=Left;FontSize=9").Assign(Format(rsNotas("FECHAFAC").Value, "####-##-##"))
            b.CreateCell(880).CreateText("Alignment=Right;FontSize=9").Assign(" ")
            b.Put()

            b = .CreateBand("Margins=130,200,100,150")
            b.Height = 35
            b.CreateCell(1910, "BorderStyle=None").CreateText().Assign("")
            b.Put()

            b = .CreateBand("Margins=130,200,100,150")
            b.Height = 35
            b.ChildrenStyle = "Normal;fontsize=9;Fontname=Arial"
            b.CreateCell(1910, "BorderStyle=Top").CreateText("Alignment=LEFT").Assign("")
            b.Put()

            b = .CreateBand("Margins=130,200,100,150; BorderStyle=Rect")
            b.ChildrenStyle = "Normal;fontsize=7;Fontname=Arial;VertAlignment=Bottom"
            b.BrushColor = RGB(215, 215, 215)

            b.CreateCell(260, "BorderStyle=Rect").CreateText("Alignment=Center").Assign(CStr(Titulo("CONTENEDORES")))                                         'Codigo
            b.CreateCell(150, "BorderStyle=Rect").CreateText("Alignment=Center").Assign("" + Titulo("CODIGO"))                                         'Descripcion
            b.CreateCell(660, "BorderStyle=Rect").CreateText("Alignment=Center").Assign("" + Titulo("PRODUCTO"))                        'UNIDADES
            b.CreateCell(210, "BorderStyle=Rect").CreateText("Alignment=Center").Assign("" + Titulo("CANTIDAD"))                        '%IVA
            b.CreateCell(210, "BorderStyle=Rect").CreateText("Alignment=Center;BorderLeftMargin=50").Assign("" + Titulo("VOLUMEN"))   'CANT
            b.CreateCell(210, "BorderStyle=Rect").CreateText("BorderLeftMargin=20").Assign("" + Titulo("PESO_NETO"))                    'U/V
            b.CreateCell(210, "BorderStyle=Rect").CreateText("Alignment=Center;BorderLeftMargin=50").Assign("" + Titulo("PESO_BRUTO"))   'VLR UNITARIO
            b.Put()


        End With

Manejo_Error:
        If Err.Number <> 0 Then
            WrtTxtError(LOG_File, "OnToOfPage")
            End
        End If

    End Sub

  
    Private Sub AxPdfBox1_BeforePutBand4(sender As System.Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_BeforePutBandEvent) Handles AxPdfBox1.BeforePutBand
        If e.aBand.Role = 1 Then ' TBandRole.rlDetail  rlDetail = 1
            If Flag3 = -1 Then
                e.aBand.BrushColor = RGB(215, 215, 215)
            Else
                e.aBand.BrushColor = RGB(255, 255, 255)
            End If
            Flag3 = Flag3 * -1
            bColor = Not bColor
        End If


        Fila = Fila + 1
        If Fila > 38 Then
            AxPdfBox1.NewPage()
            Fila = 0
        End If


    End Sub

    Private Sub AxPdfBox1_Enter(sender As System.Object, e As System.EventArgs) Handles AxPdfBox1.Enter

    End Sub

End Class
