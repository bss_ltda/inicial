﻿Module Aplicacion
    Public Const APP_NAME As String = "ListadoEmpaqueC"
    Public APP_PATH As String = ""
    Public APP_VERSION As String

    Public Origen As String
    Public Pedido As String
    Public Factura_ID As String
    Public logDocNum As String
    Public PDF_Folder As String
    Public LOG_File As String
    Public TASK_No As String

    Public Impresora_PDF As String
    Public PDFEXE As String
    Public Idioma As String
    Public Factura_Marca As String
    Public IAL_Prt As Integer
    Public Flag As Integer
    Public aPermisos() As Boolean
    Public Const ACC_TOTAL As Integer = 21

    Public bPRUEBAS As Boolean

    Public qConso As String
    Public qPedid As String
    Public qPlanilla As String
    Public qCliente As String
    Public qBodega As String
    Public qAPCHK01 As String
    Public qAPCHK02 As String
    Public qAFLAG01 As Integer

    Public aMarca() As String

    Function PDFPrtExe() As String
        PDFPrtExe = """E:\CLPrint\CLPrint.exe""{sp}/print{sp}/copies:{copias}{sp}/printer:""{printer}""{sp}/pdffile:""{doc}"""
    End Function

   

    Sub Imprimir(qImpresora As String)
        Dim rs As New ADODB.Recordset
        Dim i As Integer
        Dim ArchivoPDF As String
        Dim comando As String

        If qImpresora <> "" Then
            fSql = "SELECT CCNOT1, CCUDC1 FROM ZCC WHERE CCTABL ='PRTFACTU' AND CCCODE = '" & qImpresora & "' AND CCUDC2 = 1"
            If DB.OpenRS(rs, fSql) Then
                For i = 0 To 2
                    'If Not bPRUEBAS Then
                    If aMarca(i) <> "" Then
                        If Impresora_PDF = "" Then Impresora_PDF = CStr(rs("CCNOT1").Value)
                        ArchivoPDF = PDF_Folder & "\" & Factura_ID & "_" & aMarca(i) & ".pdf"
                        WrtTxtError(LOG_File, "Imprime " & ArchivoPDF)
                        'LogConso qConso, Now(), "Imprime Factura" & Factura_ID
                        comando = Replace(PDFEXE, "{printer}", Impresora_PDF)
                        comando = Replace(comando, "{copias}", "1")
                        comando = Replace(comando, "{doc}", ArchivoPDF)
                        comando = Replace(comando, "{sp}", " ")
                        WrtTxtError(LOG_File, comando)
                        Shell(comando, AppWinStyle.Hide, True, 300000) '5 minutos
                    End If
                Next
            Else
                WrtTxtError(LOG_File, "No econtró la impresora " & qImpresora)
            End If
            rs.Close()
            rs = Nothing
            Exit Sub
        End If

        
    End Sub

    Sub EnvioDocsFactu(Factura_Pref As String, Factura_Num As String)
        Dim rsCab As New ADODB.Recordset
        Dim rs As New ADODB.Recordset
        Dim rs2 As New ADODB.Recordset
        Dim sBodegas() As String
        Dim Ruta As String
        Dim Bodegas As String = ""

        Ruta = PDF_Folder & "\" & Trim(Factura_Pref) & "" & Trim(Factura_Num) & "_O.pdf"

        fSql = "SELECT * FROM  ZCC WHERE CCCODE  = '" & Factura_Pref & "' AND CCTABL = 'RDOCFAC' "
        If Not DB.OpenRS(rs2, fSql) Then
            rs.Close()
            Exit Sub
        End If

        If qCliente <> "" Then
            fSql = "SELECT * FROM  RMAILL WHERE LAPP = 'DOCS'  AND LMOD ='FACTURAS' AND LCUST = " & qCliente & " AND LTIPPE = 'PE'"
            If DB.OpenRS(rs, fSql) Then
                If bEnviarAviso(rs, Factura_Pref, Factura_Num) Then
                    sBodegas = Split(rs("LBODS").Value, ",")
                    Bodegas = Replace(rs("LBODS").Value, ",", "','")
                    fSql = "INSERT INTO RMAILS( SMOD, SIDH, SFILE, SREF )"
                    fSql = fSql & " SELECT  LMOD, LID, '" & Ruta & "', '" & Trim(Factura_Pref) & Trim(Factura_Num) & "'"
                    fSql = fSql & " FROM RFFACCAB INNER JOIN"
                    fSql = fSql & " EST INNER JOIN RMAILL ON LAPP = 'DOCS' AND LMOD='FACTURAS' AND TCUST = LCUST  AND TSHIP=LSHIP "
                    fSql = fSql & " ON FCLIENT=TCUST AND FPTOEN=TSHIP"
                    fSql = fSql & " WHERE FPREFIJ = '" & Factura_Pref & "' AND FFACTUR=" & Factura_Num
                    If rs("LBODS").Value <> "*ALL" Then
                        fSql = fSql & " AND FBODEGA IN( '" & Bodegas & "' )"
                    End If
                    DB.ExecuteSQL(fSql)
                End If
            End If
            rs.Close()

            fSql = "SELECT * FROM  RMAILL WHERE LAPP = 'DOCS'  AND LMOD ='FACTURAS' AND LCUST = " & qCliente & " AND LTIPPE <> 'PE'"
            If DB.OpenRS(rs, fSql) Then
                If bEnviarAviso(rs, Factura_Pref, Factura_Num) Then
                    sBodegas = Split(rs("LBODS").Value, ",")
                    fSql = "INSERT INTO RMAILS( SMOD, SIDH, SFILE, SREF )"
                    fSql = fSql & " SELECT  LMOD, LID, '" & Ruta & "', '" & Trim(Factura_Pref) & Trim(Factura_Num) & "'"
                    fSql = fSql & " FROM RFFACCAB INNER JOIN"
                    fSql = fSql & " ( EST INNER JOIN RMAILL ON LAPP = 'DOCS' AND LMOD='FACTURAS' AND TCUST = LCUST  AND STDPT1=LTIPPE )"
                    fSql = fSql & " ON FCLIENT=TCUST AND FPTOEN=TSHIP"
                    fSql = fSql & " WHERE FPREFIJ = '" & Factura_Pref & "' AND FFACTUR=" & Factura_Num & ""
                    If rs("LBODS").Value <> "*ALL" Then
                        fSql = fSql & " AND FBODEGA IN( '" & Bodegas & "' )"
                    End If
                    DB.ExecuteSQL(fSql)
                End If
            End If
        End If

    End Sub

    Function bEnviarAviso(rsM As ADODB.Recordset, Factura_Pref As String, Factura_Num As String) As Boolean
        Dim rs As New ADODB.Recordset
        Dim fSql As String

        fSql = "SELECT * FROM  RMAILS WHERE SMOD ='FACTURAS' AND SIDH = " & rsM("LID").Value & " AND SREF = '" & Factura_Pref.Trim & Factura_Num.Trim & "'"
        rs.Open(fSql, DB)
        bEnviarAviso = rs.EOF
        rs.Close()
        rs = Nothing

    End Function


End Module
