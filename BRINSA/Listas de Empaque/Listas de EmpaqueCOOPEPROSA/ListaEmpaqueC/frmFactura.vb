﻿Imports PDF_In_The_BoxCtl
Imports ADODB

Public Class frmFactura
    Dim bPaginaNueva As Boolean
    Dim rsTit As New ADODB.Recordset
    Dim rsCab As New ADODB.Recordset
    Dim rsPed As New ADODB.Recordset
    Dim rsDet As New ADODB.Recordset
    Dim rsDet2 As New ADODB.Recordset
    Dim rsNotas As New ADODB.Recordset
    Dim DocScanner As Integer, Observ As String, ImprimeIAL As String
    Dim bColor As Boolean
    Dim Flag2
    Dim bQuimicos As Boolean
    Dim bExport As Boolean
    Dim Sellos() As String
    Dim Fila As Integer
    Dim TotNotas As Integer
    Dim TotFilas As Integer
    Dim Flag3
    Dim Ta As TBoxTable

    Const CELDA_NORMAL As String = "BrushColor = 255, 255, 255"
    'Const CELDA_SOMBRA As String = "BrushColor = 215, 215, 215"
    Const CELDA_SOMBRA As String = "BrushColor = 255, 255, 255"
    Const TABLE_HEADER_STYLE As String = "ChildrenStyle=""BorderStyle=rect;FontSize=9;Fontname=Arial;Alignment=Center;VertAlignment=Center"""
    Const ESTILO_TIT As String = "Normal;fontsize=6;Fontname=Corbel;BorderColor=Black;Alignment=Center;VertAlignment=Center"


    Private Sub frmFactura_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim parametro As String
        Dim aSubParam() As String
        Dim qPrinter As String = ""
        Dim rs As New ADODB.Recordset
        Dim bGenerar As Boolean
        Dim campoParam As String = ""
        Flag3 = 1
        Environment.GetCommandLineArgs()
        For Each parametro In Environment.GetCommandLineArgs()
            aSubParam = Split(parametro, "=")
            Select Case UCase(aSubParam(0))
                Case "S"
                    CARPETA_SITIO = Trim(aSubParam(1))
                Case "P"
                    Pedido = Ceros(Trim(aSubParam(1)), 8)
                Case "TAREA"
                    TASK_No = aSubParam(1)
            End Select
        Next

        DB.bDateTimeToChar = True
        AbreConexion()
        logDocNum = DB.CalcConsec("RFLOGDOCA", "9999999999")

        fSql = "SELECT DOCCLI FROM RDCC WHERE DPEDID = " & Pedido & " FETCH FIRST 1 ROWS ONLY "
        If DB.OpenRS(rs, fSql) Then
            Factura_ID = rs("DOCCLI").Value
            Factura_ID = CStr(CInt(Factura_ID.Replace("EXP", "")))
        End If
        rs.Close()

        If My.Settings.LOCAL = "SI" Then
            campoParam = "CCNOT2"
        End If
        CARPETA_SITIO = DB.getLXParam("SITIO_CARPETA", "TM", campoParam)
        CARPETA_IMG = DB.getLXParam("CARPETA_IMG", "TM", campoParam)
        APP_PATH = DB.getLXParam("LOGOEMPRESA", "TM", campoParam)
        bPRUEBAS = My.Settings.PRUEBAS.ToUpper() = "SI"

        PDF_Folder = CARPETA_IMG & "pdf\ListadoEmpaque"
        LOG_File = PDF_Folder & "\" & Factura_ID & ".txt"
        WrtTxtError(LOG_File, Factura_ID)
        Dim arguments As String() = Environment.GetCommandLineArgs()
        WrtTxtError(LOG_File, String.Join(", ", arguments))

        If Not DB.CheckVer() Then
            DB.WrtSqlError(DB.lastError, "")
            WrtTxtError(LOG_File, DB.lastError)
            End
        End If
        bGenerar = True
        If My.Settings.PRUEBAS = "SI" Then
            GeneraPdf(bGenerar)
        Else
            Try
                If GeneraPdf(bGenerar) Then
                    fSql = " UPDATE RFTASK SET STS2 = 1  "
                    fSql = fSql & " WHERE TNUMREG = " & TASK_No
                    DB.ExecuteSQL(fSql)
                End If
            Catch ex As Exception
                DB.WrtSqlError(ex.Message, "GeneraPDF")
                WrtTxtError(LOG_File, ex.Message)
                End
            End Try
        End If

        DB.Close()

        Application.Exit()
    End Sub

    Public Function GeneraPdf(bGenerar As Boolean) As Boolean
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim ArchivoPDF As String

        GeneraPdf = False
        Fila = 0

        DB.DropTable("QTEMP.CAB" & logDocNum)

        ' PARA ENCABEZADO Y PIE DE PAGINA
        'fSql = "CREATE TABLE QTEMP.CAB" & logDocNum & " AS ( SELECT * FROM RDCCH "
        'fSql = fSql & " WHERE "
        'fSql = fSql & " CPEDID = '" & Pedido & "'"       '//2A    Prefijo
        'fSql = fSql & " ) WITH DATA "
        'DB.ExecuteSQL(fSql)

        fSql = "SELECT * FROM RDCCH "
        fSql = fSql & " WHERE "
        fSql = fSql & " CPEDID = '" & Pedido & "'"       '//2A    Prefijo
        'fSql = "SELECT * FROM  QTEMP.CAB" & logDocNum
        If Not DB.OpenRS(rsCab, fSql, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly) Then
            rsCab.Close()
            WrtTxtError(LOG_File, "Pedido no se encontro.")
            Exit Function
        End If

        If Not bGenerar Then
            rsCab.Close()
            rsDet = Nothing
            rsNotas = Nothing
            rsTit = Nothing
            rsCab = Nothing
            Exit Function
        End If

        fSql = "SELECT * FROM RFPARAM WHERE CCTABL = 'PACKINGLIST' AND CCCODE = 'TIT'"
        DB.OpenRS(rsTit, fSql, 3, 3)

        Idioma = Origen

        'tabla DETALLE
        fSql = " SELECT "
        fSql = fSql & " XCNTR   , DOCCLI, "                       '//2A    
        fSql = fSql & " DPROD   , "                       '//8P0   
        fSql = fSql & " DINAME , "                        '//4P0   
        fSql = fSql & " DOUBLE(DCANT) DCANT   , IUMS,"                       '//35A   
        fSql = fSql & " DOUBLE(DCANT*DEC(IWGHT, 15, 5 )) DPESO , IMWTUM ,"                       '//50A   
        'fSql = fSql & " DOUBLE(DPESOP) DPESOP  , "                       '//2A    
        fSql = fSql & " DOUBLE(DCANT*DEC(IMNNWU, 15, 5 )) DIPESN   "   '//5P2   Porc de Impu
        fSql = fSql & " FROM RDCC INNER JOIN RHCC ON RDCC.DCONSO = RHCC.HCONSO"
        fSql = fSql & " INNER JOIN IIM ON RDCC.DPROD = IPROD"
        fSql = fSql & " INNER JOIN RFFACDET ON RFFACDET.DPEDIDO = RDCC.DPEDID AND RFFACDET.DLINPED = RDCC.DLINEA"
        fSql = fSql & " WHERE  DPEDID = " & Pedido
        If Not DB.OpenRS(rsDet, fSql, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly) Then
            rsTit.Close()
            rsDet.Close()
            rsCab.Close()
            WrtTxtError(LOG_File, "Pedido sin detalle 1.")
            Exit Function
        End If

        fSql = " Select  DF.DPREFIJ, "
        fSql = fSql & "  DOUBLE(DF.DFACTUR) DFACTUR, "
        fSql = fSql & "  C.HPRCDVA , "
        fSql = fSql & "  DOUBLE(Sum(DF.DCANTID * DEC(IIM.IWGHT, 15, 5))) As PBRUTO, "
        fSql = fSql & "  Max(IIM.IMWTUM) IMWTUM, "
        fSql = fSql & "  Max(F.FFECFAC) As FECHAFAC, "
        fSql = fSql & "  DOUBLE(Sum(DF.DCANTID * DEC(IIM.IMNNWU, 15, 5))) As PNETO, "
        fSql = fSql & "  DOUBLE(Sum(DF.DVALTOT)) As VALOR, "
        fSql = fSql & "  F.FMONEDA "
        fSql = fSql & " From "
        fSql = fSql & "  RFFACDET DF Inner Join "
        fSql = fSql & "  RFFACCAB F "
        fSql = fSql & "    On DF.DPREFIJ = F.FPREFIJ And DF.DFACTUR = F.FFACTUR Inner Join "
        fSql = fSql & "  IIM "
        fSql = fSql & "    On DF.DCODPRO = IIM.IPROD Inner Join "
        fSql = fSql & "  RHCC C "
        fSql = fSql & "    On DF.DCONSOL = C.HCONSO "
        fSql = fSql & " Where "
        fSql = fSql & "  DF.DPEDIDO = " & Pedido
        fSql = fSql & " Group By "
        fSql = fSql & "  DF.DPREFIJ, DF.DFACTUR, F.FMONEDA, C.HPRCDVA "


        'DetallePedido2(fSql)
        'fSql = " SELECT "
        'fSql = fSql & " DPREFIJ   , IMWTUM,"                       '//2A    
        'fSql = fSql & " DOUBLE(DFACTUR) AS  DFACTUR  , "                       '//8P0   
        'fSql = fSql & " HPRCDVA , "                        '//4P0   
        'fSql = fSql & " DOUBLE(PBRUTO) AS PBRUTO  , "                       '//35A   
        'fSql = fSql & " DOUBLE(PNETO) AS PNETO  , "                       '//50A   
        'fSql = fSql & " DOUBLE(VALOR) AS VALOR  "                       '//2A    
        'fSql = fSql & " FROM QTEMP.FAC" & logDocNum

        If Not DB.OpenRS(rsNotas, fSql, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly) Then
            rsTit.Close()
            rsDet.Close()
            rsCab.Close()
            WrtTxtError(LOG_File, "Pedido sin detalle.2")
            Exit Function
        End If
        fSql = " SELECT   SUM(DVALTOT) TOTAL, MAX(FMONEDA) AS FMONEDA, MAX(FFECFAC) AS FECHAFAC   "
        fSql = fSql & " FROM RDCC INNER JOIN RHCC ON RDCC.DCONSO = RHCC.HCONSO "
        fSql = fSql & " INNER JOIN RFFACDET ON RFFACDET.DPEDIDO = RDCC.DPEDID AND RFFACDET.DLINPED = RDCC.DLINEA "
        fSql = fSql & " INNER JOIN RFFACCAB ON DPREFIJ = FPREFIJ AND DFACTUR = FFACTUR "
        fSql = fSql & " WHERE  DPEDID = " & Pedido

        If Not DB.OpenRS(rsDet2, fSql, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly) Then
            rsTit.Close()
            rsDet.Close()
            rsCab.Close()
            WrtTxtError(LOG_File, "Pedido sin detalle.2")
            Exit Function
        End If
        ArchivoPDF = PDF_Folder & "\" & Factura_ID & ".pdf"
        Dim Resultado As Boolean = ArmaPDF(ArchivoPDF)

        rsDet.Close()
        rsNotas.Close()
        rsTit.Close()
        rsCab.Close()

        Return Resultado

    End Function

    Function ArmaPDF(ArchivoPDF As String) As Boolean
        Dim Ta1 As TBoxTable
        Dim Detail As TBoxBand
        Dim Note As TBoxBand
        Dim Header As TBoxBand
        Dim Footer As TBoxBand
        Dim AfterFooter As TBoxBand
        Dim b2 As TBoxBand
        Dim sError As String

        ArmaPDF = False
        If Dir(ArchivoPDF) <> "" Then
            On Error Resume Next
            Kill(ArchivoPDF)
            If Err.Number <> 0 Then
                sError = Err.Description
                WrtTxtError(LOG_File, "ArmaPDF: Borrar " & ArchivoPDF & " " & sError)
                rsDet.Close()
                rsNotas.Close()
                rsTit.Close()
                rsCab.Close()
                rsDet = Nothing
                rsNotas = Nothing
                rsTit = Nothing
                rsCab = Nothing
                DB.Close()
                End
            End If
            On Error GoTo 0
        End If

        ' Titulos
        With Me.AxPdfBox1
            .FileName = ArchivoPDF
            .Title = "Declaración de Exportación General  - " & Factura_ID
            .WantShow = bPRUEBAS
            .WantPageCount = True
            .PaperSizeName = "Letter"
            .BeginDoc()
            '.BottomMargin = 50
            'table
            Fila = 0
            Flag = 0
            Ta = .CreateTable("BorderStyle=None;Margins=130,200,100,150")
            'Set Ta = .CreateTable("BorderStyle=none;Alignment=Center;Margins=113,200,100,150")
            Ta.Assign(rsDet)

            bPaginaNueva = True
            'Tabla del Detalle
            Dim Cont
            Cont = 1
            Flag2 = 1
            Flag = -1
            While Not rsDet.EOF
                b2 = .CreateBand("BorderStyle=LeftRight;Margins=130,200,100,150")
                b2.ChildrenStyle = "FontSize=7;Fontname=Arial;VertAlignment=Center"
                If Flag = 1 Then
                    b2.BrushColor = RGB(215, 215, 215)
                Else
                    b2.BrushColor = RGB(255, 255, 255)
                End If
                b2.Height = 35
                If Cont = 1 Then
                    b2.CreateCell(400, "BorderStyle=Left").CreateText("Alignment=Right;BorderLeftMargin = 10;BorderRightMargin = 10").Assign("" & rsDet2("FMONEDA").Value & " " & Format(rsDet2("TOTAL").Value, "#,##0.000"))
                Else
                    b2.CreateCell(400, "BorderStyle=LeftRight").CreateText("BorderLeftMargin = 10;BorderLeftMargin = 10").Assign("")
                End If
                b2.CreateCell(330, "BorderStyle=LeftRight").CreateText("BorderLeftMargin=10;BorderLeftMargin=10").Assign("" + rsDet("XCNTR").Value)
                b2.CreateCell(200, "BorderStyle=Left").CreateText("Alignment=Right;BorderLeftMargin=10;BorderRightMargin = 10").Assign("" & rsDet("DCANT").Value)
                b2.CreateCell(70, "BorderStyle=Right").CreateText("Alignment=Right;BorderLeftMargin=10;BorderRightMargin = 10").Assign("" & rsDet("IUMS").Value)
                b2.CreateCell(200, "BorderStyle=LeftRight").CreateText("BorderLeftMargin = 10;BorderLeftMargin = 10").Assign("" + rsDet("DPROD").Value)
                b2.CreateCell(710, "BorderStyle=LeftRight").CreateText("Alignment=Left;BorderLeftMargin = 10;BorderLeftMargin = 10").Assign("" + rsDet("DINAME").Value)
                b2.Put()
                Flag = Flag * -1
                rsDet.MoveNext()
                Cont = Cont + 1
            End While
            Flag2 = 0

            Flag = 1
            b2 = .CreateBand("BorderStyle=LeftRight;Margins=130,200,100,150")
            b2.CreateCell(1910, "BorderStyle=Top").CreateText().Assign("")
            b2.Put()

            Dim b As TBoxBand
            b = .CreateBand("Margins=130,200,100,150; BorderStyle=Rect")
            b.ChildrenStyle = "Normal;fontsize=7;Fontname=Arial;VertAlignment=Bottom"
            b.BrushColor = RGB(215, 215, 215)

            b.CreateCell(300, "BorderStyle=Rect").CreateText().Assign("FACTURA")                                         'Codigo                                       'Descripcion
            b.CreateCell(810, "BorderStyle=Rect").CreateText("Alignment=Center").Assign("EMPAQUE")                        'UNIDADES
            b.CreateCell(400, "BorderStyle=Rect").CreateText("Alignment=Center").Assign("PESO NETO")                        '%IVA
            b.CreateCell(400, "BorderStyle=Rect").CreateText("Alignment=Center;BorderLeftMargin=50").Assign("PESO BRUTO")   'CANT
            b.Put()


            Flag = 0
            Ta1 = .CreateTable("BorderStyle=None;Margins=130,200,100,150")
            'Set Ta = .CreateTable("BorderStyle=none;Alignment=Center;Margins=113,200,100,150")
            Ta1.Assign(rsNotas)

            bPaginaNueva = True
            'Tabla del Detalle
            Detail = Ta1.CreateBand()
            Detail.Role = TBandRole.rlDetail
            Detail.ChildrenStyle = "FontSize=7;Fontname=Arial;VertAlignment=Center"
            Detail.Height = 45
            Detail.ChildrenStyle = "FontSize=7;Fontname=Arial; VertAlignment=Center;BorderStyle=LeftRight"
            Detail.CreateCell(50, "BorderStyle=Left").CreateText("Alignment=Left;BorderLeftMargin = 10;BorderLeftMargin = 10").Bind("DPREFIJ")
            'Detail.CreateCell(200, "BorderStyle=Left").CreateText("Alignment=Left").Bind("DFACTUR")
            With Detail.CreateCell(250, "BorderStyle=none").CreateText
                .Bind("DFACTUR")
                .Format = "###000"
                .Alignment = 2 'haRight
                .BorderRightMargin = 10
                .BorderLeftMargin = 10
            End With

            Detail.CreateCell(810, "BorderStyle=none").CreateText("Alignment=Center;BorderLeftMargin = 10;BorderLeftMargin = 10").Bind("HPRCDVA")
            With Detail.CreateCell(350, "BorderStyle=none").CreateText
                .Bind("PNETO")
                .Format = "#,##0.000"
                .Alignment = 1 'haRight
                .BorderRightMargin = 10
                .BorderLeftMargin = 10
            End With
            With Detail.CreateCell(50, "BorderStyle=none").CreateText
                .Bind("IMWTUM")
                .Alignment = 1 'haRight
                .BorderRightMargin = 10
            End With

            With Detail.CreateCell(350, "BorderStyle=Left").CreateText
                .Bind("PBRUTO")
                .Format = "#,##0.000"
                .Alignment = 1 'haRight
                .BorderRightMargin = 10
                .BorderLeftMargin = 10
            End With
            With Detail.CreateCell(50, "BorderStyle=Right").CreateText
                .Bind("IMWTUM")
                .Alignment = 1 'haRight
                .BorderRightMargin = 10
            End With

            'NUEVO
            Detail.Breakable = True

            Ta1.Put()

            .EndDoc()
            Debug.Print(.FileName())
            ArchivoPDF = .FileName

        End With

        Return True

    End Function

    Private Sub AxPdfBox1_BeforePutBand(sender As Object, e As PDF_In_The_BoxCtl.IPdfBoxEvents) Handles AxPdfBox1.BeforePutBand

        If e.aBand.Role = 1 Then ' TBandRole.rlDetail  rlDetail = 1
            If bColor And Flag Then
                e.aBand.BrushColor = RGB(215, 215, 215)
            Else
                e.aBand.BrushColor = RGB(255, 255, 255)
            End If
            bColor = Not bColor
        End If

        If Flag2 = 1 Then ' TBandRole.rlDetail  rlDetail = 1
            If Flag = 1 Then
                e.aBand.BrushColor = RGB(215, 215, 215)
            Else
                e.aBand.BrushColor = RGB(255, 255, 255)
            End If
            bColor = Not bColor
        End If


        Fila = Fila + 1
        If Fila > 28 Then
            AxPdfBox1.NewPage()
            Fila = 0
        End If

    End Sub


    Private Sub AxPdfBox1_OnBottomOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnBottomOfPageEvent) Handles AxPdfBox1.OnBottomOfPage
        Dim b As TBoxBand
        Dim rs As New ADODB.Recordset
        Dim aqui As String = ""
        Dim bSalto As Boolean

        With AxPdfBox1

            b = .CreateBand("BorderStyle=bottom;Margins=130,200,100,150")
            b.Breakable = False
            b.CreateCell(1910, "BorderStyle=None").CreateText("Alignment=left").Assign("")
            b.Put()
            If e.lastPage Then
                If Fila > 16 Then
                    '            If (TotFilas + TotNotas + 2) Mod 29 > 16 Then
                    LinesAdd(29 - Fila)
                    .NewPage()
                    Fila = 0
                    bSalto = True
                End If
                FinDocumento(bSalto)
            End If

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,150")
            b.Breakable = False
            b.Height = 25
            b.CreateCell(1910, "BorderStyle=None").CreateText("Alignment=left").Assign("")
            b.Put(130, 2390)

        End With

Manejo_Error:
        If Err.Number <> 0 Then
            WrtTxtError(LOG_File, "OnBottomOfPage " & aqui & vbCrLf & Err.Description)
            End
        End If

    End Sub

    Sub FinDocumento(bSalto As Boolean)
        Dim b As TBoxBand
        Dim rs As New ADODB.Recordset
    
        With Me.AxPdfBox1

            If bSalto Then
                Fila = 0
                LinesAdd(17)
            Else
                LinesAdd(29 - Fila)
                Fila = 0
            End If

            'b = .CreateBand("Margins=130,200,100,150")
            'b.Height = 15
            'b.ChildrenStyle = "Normal;fontsize=9;Fontname=Arial"
            'b.CreateCell(1910, "BorderStyle=None").CreateText("Alignment=LEFT").Assign("")
            'b.CreateCell(1910).CreateText().Assign("")


            b = .CreateBand("BorderStyle=None;Margins=130,200,100,150")
            b.ChildrenStyle = "BorderStyle=None;Alignment=Left"
            b.Height = 15
            b.CreateCell(1910).CreateText().Assign("")
            b.Put(130, 1900)


          
        End With

    End Sub


    Function Titulo(Clave As String) As String

        Titulo = Clave & " ??"
        rsTit.Filter = "CCCODE2='" & Clave & "'"
        If Not rsTit.EOF Then
            If Idioma = "ENG" Then
                Titulo = rsTit("CCNOT2").Value
            Else
                Titulo = rsTit("CCNOT1").Value
            End If
        End If

    End Function


    Function Emergencia(Pedido As String) As String
        Dim rs As New ADODB.Recordset

        Emergencia = ""
        fSql = " SELECT * FROM ECH INNER JOIN ECL ON HORD=LORD  "
        fSql = fSql & " WHERE LCONT='SALGR'  "
        fSql = fSql & " AND HTAX IN('CGCOE', 'CRCME', 'CRCNE', 'CRSME', 'CLESE')   "
        fSql = fSql & " AND HORD = " & Pedido
        If DB.OpenRS(rs, fSql) Then
            rs.Close()
            fSql = " SELECT SNSEQ, SNDESC   "
            fSql = fSql & " FROM ESNL01 "
            fSql = fSql & " WHERE SNTYPE = 'O' AND SNCUST = 88888888 "
            fSql = fSql & " ORDER BY SNSEQ  "
            DB.OpenRS(rs, fSql)
            Do While Not rs.EOF
                Emergencia = Emergencia & rs("SNDESC").Value & " "
                rs.MoveNext()
            Loop
        End If
        rs.Close()

    End Function

    Sub LinesAdd(lineas As Integer)

        Exit Sub

    End Sub


    Private Sub AxPdfBox1_OnTopOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnTopOfPageEvent) Handles AxPdfBox1.OnTopOfPage
        On Error GoTo Manejo_Error
        Dim b2 As TBoxBand
        Dim b As TBoxBand
        Dim b4 As TBoxBand
        Dim b5 As TBoxBand
        Dim b6 As TBoxBand
        Dim img As TBoxImage
        Dim AvisosCab As String

        With Me.AxPdfBox1
            .DefineStyle("Normal", "0; Margins=130,200,100,150; fontsize=10;Fontname=Corbel;BorderColor=Black")
            .Style = "Normal"
            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "BorderStyle=None;Alignment=Center"
           
            b.CreateCell(380).CreateImage("Alignment=Right;VertAlignment=Top").Assign(APP_PATH + "\BRINSA-IAL.png")
            b.CreateCell(1030).CreateText().Assign("DECLARACIÓN DE EXPORTACIÓN")
            b.CreateCell(500).CreateText().Assign("")
            b.Put()


            b = .CreateBand("Margins=130,200,100,150")
            b.Height = 35
            b.CreateCell(1910, "BorderStyle=None").CreateText().Assign("")
            b.Put()

            b = .CreateBand("Margins=130,200,100,150")
            b.Height = 35
            b.CreateCell(1910, "BorderStyle=None").CreateText().Assign("")
            b.Put()

            'NUMERO FACTURA
            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(330).CreateText("Alignment=Left;FontSize=9").Assign(" " + Titulo("CLIENTE"))
            b.CreateCell(700).CreateText("Alignment=Left;FontSize=9").Assign(" " + rsCab("CHNAME").Value)
            b.CreateCell(480).CreateText("Alignment=Right;FontSize=9").Assign(" ")
            b.CreateCell(400, "BorderStyle=none").CreateText("FontName=Arial; FontSize=12").Assign("" + Titulo("PEDIDO") + " " & Factura_ID)
            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(330).CreateText("Alignment=Left;FontSize=9").Assign(" " + Titulo("FECHA").ToString)
            b.CreateCell(700).CreateText("Alignment=Left;FontSize=9").Assign(Format(rsDet2("FECHAFAC").Value, "####-##-##"))
            b.CreateCell(880).CreateText("Alignment=Right;FontSize=9").Assign(" ")
            b.Put()

            b = .CreateBand("Margins=130,200,100,150")
            b.Height = 35
            b.CreateCell(1910, "BorderStyle=None").CreateText().Assign("")
            b.Put()

            b = .CreateBand("Margins=130,200,100,150")
            b.Height = 35
            b.ChildrenStyle = "Normal;fontsize=9;Fontname=Arial"
            b.CreateCell(1910, "BorderStyle=Top").CreateText("Alignment=LEFT").Assign("")
            b.Put()

            b = .CreateBand("Margins=130,200,100,150; BorderStyle=Rect")
            b.ChildrenStyle = "Normal;fontsize=7;Fontname=Arial;VertAlignment=Bottom"
            b.BrushColor = RGB(215, 215, 215)

            b.CreateCell(400, "BorderStyle=Rect").CreateText("Alignment=Center").Assign("VALOR DE MERCANCIA")   'CANT
            b.CreateCell(330, "BorderStyle=Rect").CreateText("Alignment=Center").Assign("CONTENEDORES")                                         'Codigo
            b.CreateCell(270, "BorderStyle=Rect").CreateText("Alignment=Center").Assign("UNIDADES")                                         'Descripcion
            b.CreateCell(200, "BorderStyle=Rect").CreateText("Alignment=Center").Assign("CODIGO")                        'UNIDADES
            b.CreateCell(710, "BorderStyle=Rect").CreateText("Alignment=Center").Assign("PRODUCTO")                        '%IVA
            b.Put()


        End With

Manejo_Error:
        If Err.Number <> 0 Then
            WrtTxtError(LOG_File, "OnToOfPage")
            End
        End If

    End Sub

    Private Sub AxPdfBox1_BeforePutBand2(sender As System.Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_BeforePutBandEvent) Handles AxPdfBox1.BeforePutBand
        If e.aBand.Role = 1 Then ' TBandRole.rlDetail  rlDetail = 1
            If Flag3 = -1 Then
                e.aBand.BrushColor = RGB(215, 215, 215)
            Else
                e.aBand.BrushColor = RGB(255, 255, 255)
            End If
            Flag3 = Flag3 * -1
            bColor = Not bColor
        End If
    End Sub

    Private Sub AxPdfBox1_Enter(sender As System.Object, e As System.EventArgs) Handles AxPdfBox1.Enter

    End Sub
End Class
