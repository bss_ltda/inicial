﻿'BRINSA-COLTANQUES
Imports System
Imports System.Globalization
Imports System.IO
Imports System.Collections
Imports System.Xml.Serialization
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Xml
Imports System.Net

<System.Web.Services.WebService(Namespace:="http://br.org/")>
Public Class BodegaSatelite
    Public Const DB2_DATEDEC = " ( YEAR( NOW() ) * 10000 + MONTH( NOW()  ) * 100 + DAY( NOW() ) )"
    Public Const DB2_TIMEDEC = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    Public Const DB2_TIMEDEC0 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    Public Const DB2_TIMEDEC1 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 ) "
    Dim xmlRespuesta As String
    Dim Programa As String
    Dim ERRORES As String
    Dim Factura As String
    Dim Pedid As String
    Dim Ial As String
    Dim Oc As String
    Dim Bodega As String
    Dim qBodega As String
    Dim Conso As String
    Dim Factu As String
    Dim gsProvider As String
    Dim gsDatasource As String
    Dim rs As New ADODB.Recordset
    Dim rs1 As New ADODB.Recordset
    Dim mp2Param(5) As String
    Dim AS400Param(5) As String
    Dim Resultado As Boolean = True
    Dim i As Integer
    Dim sLib
    Dim gsConnString
    Dim DB_DB2, DB_MP2
    Dim C_ID As String
    Public Conn As New ADODB.Connection
    'Dim appSettings As NameValueCollection = ConfigurationManager.AppSettings
    Dim Biblioteca As String
    Dim ContadorLineas As Integer

    Structure Cabecera
        Dim Usuario As String
        Dim Password As String
        Dim Consolidado As String
        Dim Pedido As Double
        Dim Fecha As String
        Dim Bodega As String
    End Structure
    Structure CabeceraDespacho
        Dim Usuario As String
        Dim Password As String
        Dim Manifiesto As String
        Dim Bodega As String

        Dim TipoVehiculo As String
        Dim Notas As String
        Dim DescTipoVehiculo As String
        Dim Placa As String
        Dim CedulaConductor As String
        Dim NombreConductor As String
        Dim CelularConductor As String

        Dim Dpto As String
        Dim Ciudad As String
        Dim Destino As String

    End Structure

    Structure DetalleDespacho
        Dim Pedido As String
    End Structure

    Structure CabeceraPedidoDespachado
        Dim Usuario As String
        Dim Password As String
        Dim Pedido As Double

    End Structure


    Structure DetallePedidoDespachado

        Dim LineaPedido As Integer
        Dim Producto As String
        Dim Lote As String
        Dim Cantidad As Double
    End Structure

    Structure DetalleReporteEntrega

        Dim Usuario As String
        Dim Password As String
        Dim Planilla As String
        Dim FechaReporte As String
        Dim EstadoEntrega As Double
        Dim Reporte As String
    End Structure

    Structure DetalleReporteDespacho

        Dim Usuario As String
        Dim Password As String
        Dim NumeroUnico As Double
        Dim Consolidado As String
        Dim EstadoDespacho As Double
        Dim FechaReporte As String
        Dim Manifiesto As Double
        Dim ReporteGeneral As String
    End Structure
    Structure Detalle

        Dim LineaPedido As Integer
        Dim CodigoProducto As String
        Dim Lote As String
        Dim Ubicacion As String
        Dim Cantidad As Double
    End Structure

    Public Class qVersion
        Public Version As String
        Public Fecha As String
        Public CSI As String
        Public Cambio As String
        Public sLib As String
    End Class

    <WebMethod()> _
    Public Function Version() As qVersion
        Dim qVer As New qVersion
        qVer.Version = "1.1.8"
        qVer.Fecha = "2017-07-10"
        qVer.CSI = "28153"
        qVer.Cambio = "Migracion Webservices a Bod Sat II"
        qVer.sLib = My.Settings.AMBIENTE
        Return (qVer)

    End Function

    <WebMethod()> _
    Public Function ConfirmacionEntradaInventario(ByVal XML As String) As XmlDocument


        System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("es-CO")
        System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator = "."
        System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyGroupSeparator = ","
        System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator = "."
        System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberGroupSeparator = ","

        CargaPrm()
        C_ID = CalcConsec("RFLOGWSCDA", False, "99999999")
        wrtLogWsCDA(C_ID, XML, "EnProceso", "", "ConfirmacionEntradaInventario", "wsBrinsa", " ")
        ContadorLineas = 0
        Dim THADNV As String
        Dim nuevaLinea As Detalle
        Dim Cabeceraa As New Cabecera
        Dim Lineas As New List(Of Detalle)
        Dim documento As XDocument = XDocument.Parse(XML)
        Dim qx = From xee In documento.Elements("ConfirmacionEntradaInventario")
       Select New With { _
                .Usuario = xee.Element("Usuario").Value,
                .Password = xee.Element("Password").Value,
                .Pedido = xee.Element("Pedido").Value,
                .Consolidado = xee.Element("Consolidado").Value,
                .Fecha = xee.Element("Fecha").Value,
                .Bodega = xee.Element("Bodega").Value
                  }
        Try
            For Each elements In qx
                Cabeceraa = New Cabecera
                Cabeceraa.Usuario = elements.Usuario
                Cabeceraa.Password = elements.Password
                Cabeceraa.Pedido = elements.Pedido
                Cabeceraa.Consolidado = elements.Consolidado
                Cabeceraa.Fecha = elements.Fecha
                Cabeceraa.Bodega = elements.Bodega
                qBodega = elements.Bodega
            Next


            wrtLogWsCDA(C_ID, XML, "Pedido-" & Cabeceraa.Pedido & "-" & Cabeceraa.Consolidado & "", "", "ConfirmacionEntradaInventario", "wsBrinsa", Cabeceraa.Bodega)
            Conso = Cabeceraa.Consolidado

            fSql = "SELECT UPASS FROM RCAU WHERE UUSR = '" & Cabeceraa.Usuario & "'"
            rs.Open(fSql, DB_DB2)
            If Not rs.EOF Then
                If Not rs("UPASS").Value = Cabeceraa.Password Then
                    Return DevuelveError(Cabeceraa.Pedido, "Password Incorrecto", "5050", "ConfirmacionEntradaInventario", Cabeceraa.Bodega)
                End If
            Else
                Return DevuelveError(Cabeceraa.Pedido, "Usuario Incorrecto ", "5051", "ConfirmacionEntradaInventario", Cabeceraa.Bodega)
            End If
            rs.Close()
            If EsBodSatII(Cabeceraa.Bodega) Then
                Return DevuelveError(Cabeceraa.Pedido, "Bodega sin Webservice", "5060", "ConfirmacionEntradaInventario", Cabeceraa.Bodega)
            End If
        Catch ex As Exception
            Return DevuelveError("000", "Error Estructura  XML " & ex.ToString(), "5011", "ConfirmacionEntradaInventario", Cabeceraa.Bodega)
        End Try

        Try

            Dim qx2 = From xe In documento.Descendants.Elements("Linea")
        Select New With { _
                          .LineaPedido = xe.Element("LineaPedido").Value,
                          .CodigoProducto = xe.Element("CodigoProducto").Value,
                          .Cantidad = xe.Element("Cantidad").Value,
                          .Lote = xe.Element("Lote").Value,
                            .Ubicacion = xe.Element("Ubicacion").Value
                            }
            For Each elementos In qx2
                ContadorLineas = ContadorLineas + 1
                nuevaLinea = New Detalle
                nuevaLinea.Cantidad = elementos.Cantidad
                If nuevaLinea.Cantidad <= 0 Then
                    Return DevuelveError(Cabeceraa.Pedido, "Cantidad no puede ser menor o igual que 0", 5012, "ConfirmacionEntradaInventario", Cabeceraa.Bodega)
                End If
                nuevaLinea.LineaPedido = elementos.LineaPedido
                nuevaLinea.CodigoProducto = elementos.CodigoProducto
                nuevaLinea.Cantidad = elementos.Cantidad
                nuevaLinea.Lote = elementos.Lote
                nuevaLinea.Ubicacion = elementos.Ubicacion
                Lineas.Add(nuevaLinea)

                Try

                    Select Case nuevaLinea.Ubicacion
                        Case "PT", "NC", "SO", "FA"

                        Case Else
                            Return DevuelveError(Cabeceraa.Pedido, "Ubicacion " & nuevaLinea.Ubicacion & " Incorrecta", "5052", "ConfirmacionEntradaInventario", Cabeceraa.Bodega)
                    End Select

                    fSql = "SELECT * FROM RDCC INNER JOIN ECL ON DPEDID=LORD AND DLINEA=LLINE AND DPROD=LPROD"
                    fSql = fSql & " WHERE DPEDID = " & Cabeceraa.Pedido & " AND DLINEA = " & nuevaLinea.LineaPedido
                    fSql = fSql & " AND DPROD = '" & nuevaLinea.CodigoProducto & "' "
                    rs.Open(fSql, DB_DB2)
                    If Not rs.EOF Then
                        If nuevaLinea.Ubicacion = "PT" Then 'Ubicacion PT es producto terminado en Coltanques
                            fSql = "SELECT * FROM ITH "
                            fSql = fSql & " WHERE  TREF = " & Cabeceraa.Pedido & " AND THLIN = " & nuevaLinea.LineaPedido
                            fSql = fSql & " AND TPROD = '" & nuevaLinea.CodigoProducto & "' AND TLOCT = 'TRANSITO' "
                            rs1.Open(fSql, DB_DB2)
                            If rs1.EOF Then
                                rs1.Close()
                                Return DevuelveError(Cabeceraa.Pedido, " La linea No " & nuevaLinea.LineaPedido & _
                                                     " Producto " & nuevaLinea.CodigoProducto & " No tiene ITH", "5001", _
                                                     "ConfirmacionEntradaInventario", Cabeceraa.Bodega)
                            End If
                            rs1.Close()


                            fSql = " SELECT -TQTY, " & nuevaLinea.Cantidad & " FROM ITH  "
                            fSql = fSql & " WHERE  "
                            fSql = fSql & " TREF = " & Cabeceraa.Pedido & "   "
                            fSql = fSql & " AND TTYPE = 'B'  "
                            fSql = fSql & " AND TPROD = '" & nuevaLinea.CodigoProducto & "'  "
                            fSql = fSql & " AND THLIN = " & nuevaLinea.LineaPedido & "  "
                            fSql = fSql & " AND TLOT = '" & nuevaLinea.Lote & "'  "
                            rs1.Open(fSql, DB_DB2)
                            If Not rs1.EOF Then
                                If nuevaLinea.Cantidad = rs1(0).Value Then
                                    rs1.Close()
                                    fSql = " SELECT IFNULL(SUM(TQTY), 0) TOTAL FROM ITH  "
                                    fSql = fSql & " WHERE  "
                                    fSql = fSql & " TREF = " & Cabeceraa.Pedido & "   "
                                    fSql = fSql & " AND TTYPE = 'TP'  "
                                    fSql = fSql & " AND TPROD = '" & nuevaLinea.CodigoProducto & "'  "
                                    fSql = fSql & " AND TCOM = '" & Cabeceraa.Pedido & "." & nuevaLinea.LineaPedido & "'"
                                    fSql = fSql & " AND TQTY > 0"
                                    rs1.Open(fSql, DB_DB2)
                                    If Not rs1.EOF Then
                                        If rs1("TOTAL").Value > 0 Then
                                            Return DevuelveError(Cabeceraa.Pedido, " La linea No " & nuevaLinea.LineaPedido & " Producto " & nuevaLinea.CodigoProducto & " Ya fue recibida", "5001", "ConfirmacionEntradaInventario", Cabeceraa.Bodega)

                                        End If
                                    End If
                                End If

                            End If
                            rs1.Close()
                        End If
                    Else
                        'rs.Close()
                        If nuevaLinea.Ubicacion <> "SO" Then
                            Return DevuelveError(Cabeceraa.Pedido, " Pedido " & Cabeceraa.Pedido & " Linea " & nuevaLinea.LineaPedido & " Producto " & nuevaLinea.CodigoProducto & " No existe ", "5002", "ConfirmacionEntradaInventario", Cabeceraa.Bodega)
                        End If
                    End If
                    rs.Close()
                    'rs1.Close()
                Catch ex As Exception
                    Return DevuelveError("000", "Error No se pudo procesar algunas consultas " & ex.ToString, "5003", "ConfirmacionEntradaInventario", Cabeceraa.Bodega)
                End Try
            Next

            If Not ControlLlave("WDA", "ConfirmacionEntradaInventario." & Conso & "." & Cabeceraa.Pedido.ToString()) Then
                Return DevuelveError("000", "Confirmacion " & Cabeceraa.Pedido & "-" & Cabeceraa.Consolidado & " ya enviada.", "5003", "ConfirmacionEntradaInventario", Cabeceraa.Bodega)
            End If

            Dim qx3 = From xe In documento.Descendants.Elements("Linea")
            Select New With { _
              .LineaPedido = xe.Element("LineaPedido").Value,
              .CodigoProducto = xe.Element("CodigoProducto").Value,
              .Cantidad = xe.Element("Cantidad").Value,
              .Lote = xe.Element("Lote").Value,
              .Ubicacion = xe.Element("Ubicacion").Value
                }
            For Each elementos In qx3
                ContadorLineas = ContadorLineas + 1
                nuevaLinea = New Detalle
                nuevaLinea.Cantidad = elementos.Cantidad
                nuevaLinea.LineaPedido = elementos.LineaPedido
                nuevaLinea.CodigoProducto = elementos.CodigoProducto
                nuevaLinea.Cantidad = elementos.Cantidad
                nuevaLinea.Lote = elementos.Lote
                nuevaLinea.Ubicacion = elementos.Ubicacion
                Lineas.Add(nuevaLinea)

                Try

                    fSql = " UPDATE RFBSREAP SET SCOMPLETA = -1 , SFCONFIRM = NOW()"
                    fSql &= " WHERE  "
                    fSql &= " SCONSO = '" & Cabeceraa.Consolidado & "'  "
                    fSql &= " AND SPEDID = " & Cabeceraa.Pedido
                    DB_DB2.Execute(fSql)

                    If nuevaLinea.Ubicacion = "PT" Then
                        fSql = "SELECT * FROM ITH "
                        fSql = fSql & " WHERE  TREF = " & Cabeceraa.Pedido & " AND THLIN = " & nuevaLinea.LineaPedido
                        fSql = fSql & " AND TPROD = '" & nuevaLinea.CodigoProducto & "' AND TLOCT = 'TRANSITO' "
                        rs.Open(fSql, DB_DB2)

                        If Not rs.EOF Then
                            THADNV = CalcConsec("TRANSEC", False, "99999999")
                            fSql = " INSERT INTO PRDFTRIN( "
                            vSql = " VALUES ( "
                            fSql = fSql & " PRDTNV    , " : vSql = vSql & " " & DB2_DATEDEC & ", "                              '//8S0   Fecha Novedad
                            fSql = fSql & " PRTMNV    , " : vSql = vSql & " " & DB2_TIMEDEC0 & ", "                             '//6S0   Hora Novedad
                            fSql = fSql & " PRUSR     , " : vSql = vSql & "'" & "WSCONFIRM" & "', "                             '//10A   Usuario
                            fSql = fSql & " PRPRID    , " : vSql = vSql & "'" & "SAT" & "', "                                   '//3A    Identificación Producto
                            fSql = fSql & " PRACTL    , " : vSql = vSql & "'" & "Post" & "', "                                  '//10A   Tipo de Actualizacion
                            fSql = fSql & " TTYPE     , " : vSql = vSql & "'" & "TP" & "', "                                    '//2A    Transaction Type
                            fSql = fSql & " TPROD     , " : vSql = vSql & "'" & rs("TPROD").Value.ToString & "', "                          '//35O   Item Number
                            fSql = fSql & " TLOT      , " : vSql = vSql & "'" & rs("TLOT").Value.ToString & "', "               '//25O   Lot Number
                            fSql = fSql & " TWHS      , " : vSql = vSql & "'" & rs("TWHS").Value.ToString & "', "               '//3A    Warehouse
                            fSql = fSql & " TLOCT     , " : vSql = vSql & "'" & "TRANSITO" & "', "
                            fSql = fSql & " TQTY      , " : vSql = vSql & " -" & nuevaLinea.Cantidad & ", "                     '//11P3  Transaction Quantity
                            fSql = fSql & " TREF      , " : vSql = vSql & " " & Cabeceraa.Pedido & ", "                         '//8P0   Order Number
                            fSql = fSql & " TCOM      , " : vSql = vSql & "'" & Cabeceraa.Pedido & "." & Ceros(nuevaLinea.LineaPedido, 4) & "'" & ", "                       '//4P0   Order Line Number
                            fSql = fSql & " TTDTE     , " : vSql = vSql & " " & DB2_DATEDEC & ", "                       '//8P0   Transaction Date
                            fSql = fSql & " THADVN    ) " : vSql = vSql & "'" & THADNV & "' ) "       '//15O   Advice Note Number
                            DB_DB2.Execute(fSql & vSql)

                            THADNV = CalcConsec("TRANSEC", False, "99999999")
                            fSql = " INSERT INTO PRDFTRIN( "
                            vSql = " VALUES ( "
                            fSql = fSql & " PRDTNV    , " : vSql = vSql & " " & DB2_DATEDEC & ", "                           '//8S0   Fecha Novedad
                            fSql = fSql & " PRTMNV    , " : vSql = vSql & " " & DB2_TIMEDEC0 & ", "                          '//6S0   Hora Novedad
                            fSql = fSql & " PRUSR     , " : vSql = vSql & "'" & "WSCONFENT" & "', "                           '//10A   Usuario
                            fSql = fSql & " PRPRID    , " : vSql = vSql & "'" & "SAT" & "', "                                '//3A    Identificación Producto
                            fSql = fSql & " PRACTL    , " : vSql = vSql & "'" & "Post" & "', "                               '//10A   Tipo de Actualizacion
                            fSql = fSql & " TTYPE     , " : vSql = vSql & "'" & "TP" & "', "                                  '//2A    Transaction Type
                            fSql = fSql & " TPROD     , " : vSql = vSql & "'" & rs("TPROD").Value.ToString & "', "                          '//35O   Item Number
                            fSql = fSql & " TLOT      , " : vSql = vSql & "'" & rs("TLOT").Value.ToString & "',"    '//25O   Lot Number
                            fSql = fSql & " TWHS      , " : vSql = vSql & "'" & rs("TWHS").Value.ToString & "', "                          '//3A    Warehouse
                            Select Case nuevaLinea.Ubicacion
                                Case "PT"
                                    fSql = fSql & " TLOCT     , " : vSql = vSql & " ( SELECT ILOC FROM IIM WHERE IPROD = '" & rs("TPROD").Value.ToString & "' ), "  '//10O   Location
                                Case "NC"
                                    fSql = fSql & " TLOCT     , " : vSql = vSql & "'" & "NOCONFORME" & "', "
                                Case "SO"
                                    fSql = fSql & " TLOCT     , " : vSql = vSql & "'" & "SOBRANTE" & "', "
                                Case "FA"
                                    fSql = fSql & " TLOCT     , " : vSql = vSql & "'" & "FALTANTE" & "', "
                            End Select
                            fSql = fSql & " TQTY      , " : vSql = vSql & " " & nuevaLinea.Cantidad & ", "                       '//11P3  Transaction Quantity
                            fSql = fSql & " TREF      , " : vSql = vSql & " " & Cabeceraa.Pedido & ", "                       '//8P0   Order Number
                            fSql = fSql & " TCOM      , " : vSql = vSql & "'" & Cabeceraa.Pedido & "." & Ceros(nuevaLinea.LineaPedido, 4) & "'" & ", "                       '//4P0   Order Line Number
                            fSql = fSql & " TTDTE     , " : vSql = vSql & " " & DB2_DATEDEC & ", "                       '//8P0   Transaction Date
                            fSql = fSql & " THADVN    ) " : vSql = vSql & "'" & THADNV & "' ) "       '//15O   Advice Note Number
                            DB_DB2.Execute(fSql & vSql)
                            rs.Close()
                        Else
                            fSql = "Pedido = " & Cabeceraa.Pedido & " Linea = " & nuevaLinea.LineaPedido
                            fSql = fSql & " Producto '" & nuevaLinea.CodigoProducto & "' Ubicacion = 'TRANSITO' "
                            rs.Close()
                            Return DevuelveError("000", "Error No Econtro H para " & fSql, "5003", "ConfirmacionEntradaInventario", Cabeceraa.Bodega)
                        End If
                    Else
                        fSql = " INSERT INTO PRDFTRINBS( "
                        vSql = " VALUES ( "
                        fSql = fSql & " PRDTNV    , " : vSql = vSql & " " & DB2_DATEDEC & ", "                           '//8S0   Fecha Novedad
                        fSql = fSql & " PRTMNV    , " : vSql = vSql & " " & DB2_TIMEDEC0 & ", "                          '//6S0   Hora Novedad
                        fSql = fSql & " PRUSR     , " : vSql = vSql & "'" & Cabeceraa.Bodega & "', "                     '//10A   Usuario
                        fSql = fSql & " PRPRID    , " : vSql = vSql & "'" & "SAT" & "', "                                '//3A    Identificación Producto
                        fSql = fSql & " PRACTL    , " : vSql = vSql & "'" & "Post" & "', "                               '//10A   Tipo de Actualizacion
                        fSql = fSql & " TTYPE     , " : vSql = vSql & "'" & "TP" & "', "                                 '//2A    Transaction Type
                        fSql = fSql & " TPROD     , " : vSql = vSql & "'" & nuevaLinea.CodigoProducto & "', "                          '//35O   Item Number
                        fSql = fSql & " TLOT      , " : vSql = vSql & "'" & nuevaLinea.Lote & "',"    '//25O   Lot Number
                        fSql = fSql & " TWHS      , " : vSql = vSql & "'" & Cabeceraa.Bodega & "', "                          '//3A    Warehouse
                        Select Case nuevaLinea.Ubicacion
                            Case "NC"
                                fSql = fSql & " TLOCT     , " : vSql = vSql & "'" & "NOCONFORME" & "', "
                            Case "SO"
                                fSql = fSql & " TLOCT     , " : vSql = vSql & "'" & "SOBRANTE" & "', "
                            Case "FA"
                                fSql = fSql & " TLOCT     , " : vSql = vSql & "'" & "FALTANTE" & "', "
                        End Select
                        fSql = fSql & " TQTY      , " : vSql = vSql & " " & nuevaLinea.Cantidad & ", "                       '//11P3  Transaction Quantity
                        fSql = fSql & " TREF      , " : vSql = vSql & " " & Cabeceraa.Pedido & ", "                       '//8P0   Order Number
                        fSql = fSql & " TCOM     , " : vSql = vSql & "'" & Cabeceraa.Pedido & "." & Ceros(nuevaLinea.LineaPedido, 4) & "'" & ", "                       '//4P0   Order Line Number
                        fSql = fSql & " TTDTE     ) " : vSql = vSql & " " & DB2_DATEDEC & ") "                       '//8P0   Transaction Date
                        DB_DB2.Execute(fSql & vSql)
                    End If
                    fSql = " INSERT INTO RFBSTRIN( "
                    vSql = " VALUES ( "
                    fSql = fSql & " TIPOD     , " : vSql = vSql & "'" & "CONFIRM" & "', "     '//20A   
                    fSql = fSql & " DCONSO    , " : vSql = vSql & "'" & Cabeceraa.Consolidado & "', "     '//6A    Consolidado
                    fSql = fSql & " DPEDID    , " : vSql = vSql & " " & Cabeceraa.Pedido & ", "     '//8P0   Pedido
                    fSql = fSql & " DLINEA    , " : vSql = vSql & " " & nuevaLinea.LineaPedido & ", "     '//4P0   Linea
                    fSql = fSql & " DPROD     , " : vSql = vSql & "'" & nuevaLinea.CodigoProducto & "', "     '//35A   Producto
                    fSql = fSql & " TWHS      , " : vSql = vSql & "'" & Cabeceraa.Bodega & "', "     '//3A    Warehouse
                    'fSql = fSql & " ILOC      , " : vSql = vSql & "'TRANSITO', "     '//10O   Default Location
                    Select Case nuevaLinea.Ubicacion
                        Case "PT"
                            fSql = fSql & " ILOC     , " : vSql = vSql & " ( SELECT ILOC FROM IIM WHERE IPROD = '" & nuevaLinea.CodigoProducto & "' ), "  '//10O   Location
                        Case "NC"
                            fSql = fSql & " ILOC     , " : vSql = vSql & "'" & "NOCONFORME" & "', "
                        Case "SO"
                            fSql = fSql & " ILOC     , " : vSql = vSql & "'" & "SOBRANTE" & "', "
                        Case "FA"
                            fSql = fSql & " ILOC      , " : vSql = vSql & "'" & "FALTANTE" & "', "
                    End Select
                    fSql = fSql & " TLOT      , " : vSql = vSql & "'" & nuevaLinea.Lote & "', "     '//25O   Lot Number
                    fSql = fSql & " TQTY      ) " : vSql = vSql & " -" & nuevaLinea.Cantidad & ") "     '//11P3  
                    DB_DB2.Execute(fSql & vSql)

                Catch ex As Exception
                    Return DevuelveError("000", "Error No se pudo procesar algunas consultas consulta. " & ex.ToString, "5003", "ConfirmacionEntradaInventario", Cabeceraa.Bodega)
                End Try
            Next
            fSql = " UPDATE RFBSREAP SET SCOMPLETA = 1 , SFCONFIRM = NOW()"
            fSql &= " WHERE  "
            fSql &= " SCONSO = '" & Cabeceraa.Consolidado & "'  "
            fSql &= " AND SPEDID = " & Cabeceraa.Pedido
            DB_DB2.Execute(fSql)

            Return DevuelveResultado(Cabeceraa.Pedido, "ConfirmacionEntradaInventario", Cabeceraa.Bodega)
        Catch

            'aca
            Try

                Dim qx2 = From xe In documento.Descendants.Elements("Linea")
            Select New With { _
                              .CodigoProducto = xe.Element("CodigoProducto").Value,
                              .Cantidad = xe.Element("Cantidad").Value,
                                .Ubicacion = xe.Element("Ubicacion").Value
                                }



                Dim qx3 = From xe In documento.Descendants.Elements("Linea")
                Select New With { _
                        .CodigoProducto = xe.Element("CodigoProducto").Value,
                        .Cantidad = xe.Element("Cantidad").Value,
                        .Ubicacion = xe.Element("Ubicacion").Value
                    }
                For Each elementos In qx3
                    ContadorLineas = ContadorLineas + 1
                    nuevaLinea = New Detalle
                    nuevaLinea.Cantidad = elementos.Cantidad
                    nuevaLinea.CodigoProducto = elementos.CodigoProducto
                    nuevaLinea.Ubicacion = elementos.Ubicacion
                    Lineas.Add(nuevaLinea)

                    Try

                        Select Case Cabeceraa.Pedido
                            Case 1056042, 1058364, 1058721, 1059437, 1059495, 1059642, 1058365, 1058509, 1052423, 1057776, 1058371, 1059640

                                fSql = "SELECT * FROM FALTAH "
                                fSql = fSql & " WHERE  TREF = " & Cabeceraa.Pedido & " AND THLIN = " & nuevaLinea.LineaPedido
                                fSql = fSql & " AND TPROD = '" & nuevaLinea.CodigoProducto & "' AND TLOCT = 'TRANSITO' "
                                rs.Open(fSql, DB_DB2)

                            Case Else
                                fSql = "SELECT * FROM ITH "
                                fSql = fSql & " WHERE  TREF = " & Cabeceraa.Pedido & " AND THLIN = " & nuevaLinea.LineaPedido
                                fSql = fSql & " AND TPROD = '" & nuevaLinea.CodigoProducto & "' AND TLOCT = 'TRANSITO' "
                                rs.Open(fSql, DB_DB2)

                        End Select



                        If rs.EOF Then
                            Return DevuelveError("000", "Pedido = " & Cabeceraa.Pedido & "  Linea = " & nuevaLinea.LineaPedido & " Producto = '" & nuevaLinea.CodigoProducto & " NO valido", "6000", "ConfirmacionEntradaInventario", Cabeceraa.Bodega)
                        End If

                        THADNV = CalcConsec("TRANSEC", False, "99999999")
                        Select Case nuevaLinea.Ubicacion
                            Case "PT"
                                fSql = " INSERT INTO PRDFTRIN( "
                            Case Else
                                fSql = " INSERT INTO PRDFTRINBS( "
                        End Select
                        'fSql = " INSERT INTO PRDFTRIN( "
                        vSql = " VALUES ( "
                        fSql = fSql & " PRDTNV    , " : vSql = vSql & " " & DB2_DATEDEC & ", "                           '//8S0   Fecha Novedad
                        fSql = fSql & " PRTMNV    , " : vSql = vSql & " " & DB2_TIMEDEC0 & ", "                          '//6S0   Hora Novedad
                        fSql = fSql & " PRUSR     , " : vSql = vSql & "'" & rs("THUSER").Value.ToString & "', "                           '//10A   Usuario
                        fSql = fSql & " PRPRID    , " : vSql = vSql & "'" & "SAT" & "', "                                '//3A    Identificación Producto
                        fSql = fSql & " PRACTL    , " : vSql = vSql & "'" & "Post" & "', "                               '//10A   Tipo de Actualizacion
                        fSql = fSql & " TTYPE     , " : vSql = vSql & "'" & "TP" & "', "                                  '//2A    Transaction Type
                        fSql = fSql & " TPROD     , " : vSql = vSql & "'" & rs("TPROD").Value.ToString & "', "                          '//35O   Item Number
                        fSql = fSql & " TLOT      , " : vSql = vSql & "'" & rs("TLOT").Value.ToString & "', "  '//25O   Lot Number
                        fSql = fSql & " TWHS      , " : vSql = vSql & "'" & rs("TWHS").Value.ToString & "', "                          '//3A    Warehouse
                        fSql = fSql & " TLOCT     , " : vSql = vSql & "'" & "TRANSITO" & "', "
                        fSql = fSql & " TQTY      , " : vSql = vSql & " -" & nuevaLinea.Cantidad & ", "                       '//11P3  Transaction Quantity
                        fSql = fSql & " TREF      , " : vSql = vSql & " " & Cabeceraa.Pedido & ", "                       '//8P0   Order Number
                        fSql = fSql & " TCOM     , " : vSql = vSql & "'" & Cabeceraa.Pedido & "." & Ceros(nuevaLinea.LineaPedido, 4) & "'" & ", "                       '//4P0   Order Line Number
                        fSql = fSql & " TTDTE     , " : vSql = vSql & " " & DB2_DATEDEC & ", "                       '//8P0   Transaction Date
                        fSql = fSql & " THADVN    ) " : vSql = vSql & "'" & THADNV & "' ) "       '//15O   Advice Note Number
                        DB_DB2.Execute(fSql & vSql)
                        THADNV = CalcConsec("TRANSEC", False, "99999999")
                        Select Case nuevaLinea.Ubicacion
                            Case "PT"
                                fSql = " INSERT INTO PRDFTRIN( "
                            Case Else
                                fSql = " INSERT INTO PRDFTRINBS( "
                        End Select
                        'fSql = " INSERT INTO PRDFTRIN( "
                        vSql = " VALUES ( "
                        fSql = fSql & " PRDTNV    , " : vSql = vSql & " " & DB2_DATEDEC & ", "                           '//8S0   Fecha Novedad
                        fSql = fSql & " PRTMNV    , " : vSql = vSql & " " & DB2_TIMEDEC0 & ", "                          '//6S0   Hora Novedad
                        fSql = fSql & " PRUSR     , " : vSql = vSql & "'" & rs("TWHS").Value.ToString & "', "                           '//10A   Usuario
                        fSql = fSql & " PRPRID    , " : vSql = vSql & "'" & "SAT" & "', "                                '//3A    Identificación Producto
                        fSql = fSql & " PRACTL    , " : vSql = vSql & "'" & "Post" & "', "                               '//10A   Tipo de Actualizacion
                        fSql = fSql & " TTYPE     , " : vSql = vSql & "'" & "TP" & "', "                                  '//2A    Transaction Type
                        fSql = fSql & " TPROD     , " : vSql = vSql & "'" & rs("TPROD").Value.ToString & "', "                          '//35O   Item Number
                        fSql = fSql & " TLOT      , " : vSql = vSql & "'" & rs("TLOT").Value.ToString & "',"    '//25O   Lot Number
                        fSql = fSql & " TWHS      , " : vSql = vSql & "'" & rs("TWHS").Value.ToString & "', "                          '//3A    Warehouse
                        Select Case nuevaLinea.Ubicacion
                            Case "PT"
                                fSql = fSql & " TLOCT     , " : vSql = vSql & " ( SELECT ILOC FROM IIM WHERE IPROD = '" & rs("DPROD").Value.ToString & "' ), "  '//10O   Location
                            Case "NC"
                                fSql = fSql & " TLOCT     , " : vSql = vSql & "'" & "NOCONFORME" & "', "
                            Case "SO"
                                fSql = fSql & " TLOCT     , " : vSql = vSql & "'" & "SOBRANTE" & "', "
                            Case "FA"
                                fSql = fSql & " TLOCT     , " : vSql = vSql & "'" & "FALTANTE" & "', "
                        End Select
                        fSql = fSql & " TQTY      , " : vSql = vSql & " " & nuevaLinea.Cantidad & ", "                       '//11P3  Transaction Quantity
                        fSql = fSql & " TREF      , " : vSql = vSql & " " & Cabeceraa.Pedido & ", "                       '//8P0   Order Number
                        fSql = fSql & " TCOM     , " : vSql = vSql & "'" & Cabeceraa.Pedido & "." & Ceros(nuevaLinea.LineaPedido, 4) & "'" & ", "                       '//4P0   Order Line Number
                        fSql = fSql & " TTDTE     , " : vSql = vSql & " " & DB2_DATEDEC & ", "                       '//8P0   Transaction Date
                        fSql = fSql & " THADVN    ) " : vSql = vSql & "'" & THADNV & "' ) "       '//15O   Advice Note Number
                        DB_DB2.Execute(fSql & vSql)

                        fSql = " INSERT INTO RFBSTRIN( "
                        vSql = " VALUES ( "
                        fSql = fSql & " TIPOD     , " : vSql = vSql & "'" & "CONFIRM" & "', "     '//20A   
                        fSql = fSql & " DCONSO    , " : vSql = vSql & "'" & Cabeceraa.Consolidado & "', "     '//6A    Consolidado
                        fSql = fSql & " DPEDID    , " : vSql = vSql & " " & Cabeceraa.Pedido & ", "     '//8P0   Pedido
                        fSql = fSql & " DLINEA    , " : vSql = vSql & " " & nuevaLinea.LineaPedido & ", "     '//4P0   Linea
                        fSql = fSql & " DPROD     , " : vSql = vSql & "'" & rs("TPROD").Value.ToString & "', "     '//35A   Producto
                        fSql = fSql & " TWHS      , " : vSql = vSql & "'" & rs("TWHS").Value.ToString & "', "     '//3A    Warehouse
                        fSql = fSql & " ILOC      , " : vSql = vSql & "'TRANSITO', "     '//10O   Default Location
                        Select Case nuevaLinea.Ubicacion
                            Case "PT"
                                fSql = fSql & " ILOC     , " : vSql = vSql & " ( SELECT ILOC FROM IIM WHERE IPROD = '" & rs("TPROD").Value.ToString & "' ), "  '//10O   Location
                            Case "NC"
                                fSql = fSql & " ILOC     , " : vSql = vSql & "'" & "NOCONFORME" & "', "
                            Case "SO"
                                fSql = fSql & " ILOC     , " : vSql = vSql & "'" & "SOBRANTE" & "', "
                            Case "FA"
                                fSql = fSql & " ILOC      , " : vSql = vSql & "'" & "FALTANTE" & "', "
                        End Select
                        fSql = fSql & " TLOT      , " : vSql = vSql & "'" & rs("TLOT").Value.ToString & "', "     '//25O   Lot Number
                        fSql = fSql & " TQTY      ) " : vSql = vSql & " -" & nuevaLinea.Cantidad & ") "     '//11P3  
                        DB_DB2.Execute(fSql & vSql)
                        rs.Close()


                        'rs.Close()

                    Catch
                        Return DevuelveError("000", "Error No se pudo procesar algunas consultas consulta " & Err.Description, "5003", "ConfirmacionEntradaInventario", Cabeceraa.Bodega)
                    End Try
                Next

                fSql = " UPDATE RFBSREAP SET SCOMPLETA = 1 , SFCONFIRM = NOW()"
                fSql &= " WHERE  "
                fSql &= " SCONSO = '" & Cabeceraa.Consolidado & "'  "
                fSql &= " AND SPEDID = " & Cabeceraa.Pedido
                DB_DB2.Execute(fSql)


                Return DevuelveResultado(Cabeceraa.Pedido, "ConfirmacionEntradaInventario", Cabeceraa.Bodega)

            Catch
                Return DevuelveError("000", "Error Estructura  XML ", "5011", "ConfirmacionEntradaInventario", Cabeceraa.Bodega)
            End Try

            'aca
            Return DevuelveError("000", "Error Estructura  XML ", "5011", "ConfirmacionEntradaInventario", Cabeceraa.Bodega)
        End Try
    End Function
    <WebMethod()> _
    Public Function PedidoDespachado(ByVal XML As String) As XmlDocument
        CargaPrm()
        C_ID = CalcConsec("RFLOGWSCDA", False, "99999999")
        wrtLogWsCDA(C_ID, XML, "EnProceso", "", "PedidoDespachado", "wsBrinsa", "  ")
        ContadorLineas = 0
        Dim nuevaLinea As DetallePedidoDespachado
        Dim Cabeceraa As New CabeceraPedidoDespachado
        Dim Lineas As New List(Of DetallePedidoDespachado)
        Dim documento As XDocument = XDocument.Parse(XML)
        Dim qx = From xee In documento.Elements("PedidoDespachado")
       Select New With { _
                .Usuario = xee.Element("Usuario").Value,
                .Password = xee.Element("Password").Value,
                .Pedido = xee.Element("Pedido").Value
                  }
        Try
            For Each elements In qx
                Cabeceraa = New CabeceraPedidoDespachado
                Cabeceraa.Usuario = elements.Usuario
                Cabeceraa.Password = elements.Password
                Cabeceraa.Pedido = elements.Pedido

            Next


            fSql = "SELECT EBOD  FROM RECC WHERE EPEDID = '" & Cabeceraa.Pedido & "'"
            rs.Open(fSql, DB_DB2)
            Bodega = "  "
            If Not rs.EOF Then
                Bodega = rs("EBOD").Value
            End If
            rs.Close()

            If EsBodSatII(Bodega) Then
                Return DevuelveError(Cabeceraa.Pedido, "Bodega sin Webservice", "5060", "ConfirmacionEntradaInventario", Bodega)
            End If

            fSql = "SELECT UPASS FROM RCAU WHERE UUSR = '" & Cabeceraa.Usuario & "'"
            rs.Open(fSql, DB_DB2)
            If Not rs.EOF Then
                If Not rs("UPASS").Value = Cabeceraa.Password Then
                    Return DevuelveError(Cabeceraa.Pedido, "Password Incorrecto", "5050", "PedidoDespachado", Bodega)
                End If
            Else
                wrtLogWsCDA(C_ID, XML, "Pedido - " & Cabeceraa.Pedido, "", "PedidoDespachado", "wsBrinsa", Bodega)

                Return DevuelveError(Cabeceraa.Pedido, "Usuario Incorrecto ", "5051", "PedidoDespachado", Bodega)
            End If
            rs.Close()
            If EsBodSatII(Bodega) Then
                Return DevuelveError(Cabeceraa.Pedido, "Bodega sin Webservice", "5060", "ConfirmacionEntradaInventario", Bodega)
            End If

        Catch ex As Exception
            Return DevuelveError("000", "Error Estructura  XML " & ex.ToString, "5011", "PedidoDespachado", Bodega)
        End Try

        Try

            Dim qx2 = From xe In documento.Descendants.Elements("Linea")
        Select New With { _
                          .LineaPedido = xe.Element("LineaPedido").Value,
                          .Producto = xe.Element("Producto").Value,
                          .Lote = xe.Element("Producto").Value,
                          .Cantidad = xe.Element("Cantidad").Value
                            }
            For Each elementos In qx2
                ContadorLineas = ContadorLineas + 1
                nuevaLinea = New DetallePedidoDespachado
                nuevaLinea.Cantidad = elementos.Cantidad
                If nuevaLinea.Cantidad <= 0 Then
                    Return DevuelveError("000", "Cantidad no puede ser menor o igual que 0", 106, "PedidoDespachado", Bodega)
                End If
                nuevaLinea.LineaPedido = elementos.LineaPedido
                nuevaLinea.Producto = elementos.Producto
                nuevaLinea.Lote = elementos.Lote
                nuevaLinea.Cantidad = elementos.Cantidad
                Lineas.Add(nuevaLinea)
            Next
            ' Return DevuelveResultado(Cabeceraa.Pedido, "PedidoDespachado")
        Catch ex As Exception
            wrtLogWsCDA(C_ID, XML, "Pedido - " & Cabeceraa.Pedido, "", "PedidoDespachado", "wsBrinsa", Bodega)
            Return DevuelveError("000", "Error Estructura  XML ", "5011", "PedidoDespachado", Bodega)
        End Try



        Try

            Dim qx3 = From xe In documento.Descendants.Elements("Linea")
        Select New With { _
                          .LineaPedido = xe.Element("LineaPedido").Value,
                          .Producto = xe.Element("Producto").Value,
                          .Lote = xe.Element("Producto").Value,
                          .Cantidad = xe.Element("Cantidad").Value
                            }
            For Each elementos In qx3
                ContadorLineas = ContadorLineas + 1
                nuevaLinea = New DetallePedidoDespachado
                nuevaLinea.Cantidad = elementos.Cantidad
                nuevaLinea.LineaPedido = elementos.LineaPedido
                nuevaLinea.Producto = elementos.Producto
                nuevaLinea.Lote = elementos.Lote
                nuevaLinea.Cantidad = elementos.Cantidad
                Lineas.Add(nuevaLinea)

                fSql = "SELECT * FROM RDCC"
                fSql = fSql & " WHERE DPEDID = " & Cabeceraa.Pedido & " AND DLINEA = " & nuevaLinea.LineaPedido & " AND DPROD = '" & nuevaLinea.Producto & "' "
                rs.Open(fSql, DB_DB2)
                If Not rs.EOF Then
                    If rs("DCANTE").Value = 0 Then
                        fSql = "UPDATE RDCC SET DCANTE =" & nuevaLinea.Cantidad
                        fSql = fSql & " WHERE DPEDID = " & Cabeceraa.Pedido & " AND DLINEA = " & nuevaLinea.LineaPedido & " AND DPROD = '" & nuevaLinea.Producto & "' "
                        DB_DB2.Execute(fSql)
                        rs.Close()
                    Else
                        rs.Close()
                        wrtLogWsCDA(C_ID, XML, "Pedido - " & Cabeceraa.Pedido, "", "PedidoDespachado", "wsBrinsa", Bodega)

                        Return DevuelveError(Cabeceraa.Pedido, "Linea No " & nuevaLinea.LineaPedido & " ya Despachada", "5006", "PedidoDespachado", Bodega)
                    End If
                Else
                    rs.Close()
                    wrtLogWsCDA(C_ID, XML, "Pedido - " & Cabeceraa.Pedido, "", "PedidoDespachado", "wsBrinsa", Bodega)

                    Return DevuelveError(Cabeceraa.Pedido, "Linea No " & nuevaLinea.LineaPedido & " no existe", "5007", "PedidoDespachado", Bodega)
                End If
                '                Si(DCANTE = 0)
                'UPDATE RDCC SET DCANTE = <Cantidad> 
                'WHERE DPEDID = <Pedido> AND DLINEA = <LineaPedido> 
                '	Sino
                '		Error
                'Sino esta
                '	Error
            Next
            wrtLogWsCDA(C_ID, XML, "Pedido - " & Cabeceraa.Pedido, "", "PedidoDespachado", "wsBrinsa", Bodega)

            Return DevuelveResultado(Cabeceraa.Pedido, "PedidoDespachado", Bodega)
        Catch
            wrtLogWsCDA(C_ID, XML, "Pedido - " & Cabeceraa.Pedido, "", "PedidoDespachado", "wsBrinsa", Bodega)

            Return DevuelveError("000", "Error Estructura  XML ", "5011", "PedidoDespachado", Bodega)
        End Try

    End Function
    <WebMethod()> _
    Public Function ReporteDespacho(ByVal XML As String) As XmlDocument
        CargaPrm()
        C_ID = CalcConsec("RFLOGWSCDA", False, "99999999")
        wrtLogWsCDA(C_ID, XML, "EnProceso", "", "ReporteDespacho", "wsBrinsa", "  ")
        ContadorLineas = 0
        Dim Cabeceraa As New DetalleReporteDespacho
        Dim documento As XDocument = XDocument.Parse(XML)
        Dim qx = From xee In documento.Elements("ReporteDespacho")
       Select New With { _
                .Usuario = xee.Element("Usuario").Value,
                .Password = xee.Element("Password").Value,
                .NumeroUnico = xee.Element("NumeroUnico").Value,
                .Consolidado = xee.Element("Consolidado").Value,
                .EstadoDespacho = xee.Element("EstadoDespacho").Value,
                .FechaReporte = xee.Element("FechaReporte").Value,
                .Manifiesto = xee.Element("Manifiesto").Value,
                .ReporteGeneral = xee.Element("ReporteGeneral").Value
                  }
        Try
            For Each elements In qx
                Cabeceraa = New DetalleReporteDespacho
                Cabeceraa.Usuario = elements.Usuario
                Cabeceraa.Password = elements.Password
                Cabeceraa.NumeroUnico = elements.NumeroUnico
                Cabeceraa.Consolidado = elements.Consolidado
                Conso = Cabeceraa.Consolidado

                fSql = "SELECT EBOD  FROM RECC WHERE ECONSO = '" & Cabeceraa.Consolidado & "'"
                rs.Open(fSql, DB_DB2)
                Bodega = "  "
                If Not rs.EOF Then
                    Bodega = rs("EBOD").Value
                End If
                rs.Close()

                If elements.EstadoDespacho = 60 Then
                    Cabeceraa.EstadoDespacho = 6

                    fSql = "SELECT HGENERA, HESTAD FROM RHCC WHERE HCONSO = '" & Cabeceraa.Consolidado & "' AND HESTAD = 6"
                    rs.Open(fSql, DB_DB2)

                    If Not rs.EOF Then
                        Return DevuelveError(Cabeceraa.Consolidado, "Estado ya Reportado", "5555", "ReporteDespacho", Bodega)
                    Else

                        fSql = "UPDATE RHCC SET HGENERA ='2' "
                        fSql = fSql & " WHERE HCONSO = '" & Cabeceraa.Consolidado & "' AND HGENERA ='1'"
                        DB_DB2.Execute(fSql)

                    End If
                    rs.Close()
                Else
                    Cabeceraa.EstadoDespacho = elements.EstadoDespacho
                End If


                Cabeceraa.FechaReporte = elements.FechaReporte
                Cabeceraa.Manifiesto = elements.Manifiesto
                Cabeceraa.ReporteGeneral = elements.ReporteGeneral
                wrtLogWsCDA(C_ID, XML, "Consolidado - " & Cabeceraa.Consolidado, "", "ReporteDespacho", "wsBrinsa", Bodega)


                fSql = "SELECT UPASS FROM RCAU WHERE UUSR = '" & Cabeceraa.Usuario & "'"
                rs.Open(fSql, DB_DB2)
                If Not rs.EOF Then
                    If Not rs("UPASS").Value = Cabeceraa.Password Then
                        Return DevuelveError(Cabeceraa.Consolidado, "Password Incorrecto", "5050", "ReporteDespacho", Bodega)
                    End If
                Else
                    Return DevuelveError(Cabeceraa.Consolidado, "Usuario Incorrecto ", "5051", "ReporteDespacho", Bodega)
                End If
                rs.Close()

                Dim Fechas As Date = Convert.ToDateTime(Cabeceraa.FechaReporte)
                Cabeceraa.FechaReporte = Fechas.ToString("yyyy-MM-dd-HH.mm.ss.000000")

                fSql = "SELECT * FROM RHCC WHERE HCONSO ='" & Cabeceraa.Consolidado & "'"
                rs.Open(fSql, DB_DB2)
                If Not rs.EOF Then

                    fSql = "UPDATE RHCC SET HESTAD =" & Cabeceraa.EstadoDespacho & ", HFESTAD='" & Cabeceraa.FechaReporte & "'"
                    fSql = fSql & " WHERE HCONSO = '" & Cabeceraa.Consolidado & "'"
                    DB_DB2.Execute(fSql)
                    rs.Close()
                Else

                    Return DevuelveError(Cabeceraa.Consolidado, "No existe Consolidado", "5009", "ReporteDespacho", Bodega)
                End If

                fSql = " INSERT INTO RRCC( "
                vSql = " VALUES ( "
                fSql = fSql & " RTREP     , " : vSql = vSql & " 1, "     '//2S0   Tipo Rep
                fSql = fSql & " RCONSO    , " : vSql = vSql & "'" & Cabeceraa.Consolidado & "', "     '//6A    Consolidado
                fSql = fSql & " RSTSC     , " : vSql = vSql & " (SELECT HESTAD FROM RHCC WHERE HCONSO = '" & Cabeceraa.Consolidado & "'), "     '//2S0   Est Cons
                fSql = fSql & " RREPORT   , " : vSql = vSql & "'" & Cabeceraa.ReporteGeneral & "', "     '//502A  Reporte
                fSql = fSql & " RCRTUSR   , " : vSql = vSql & "'WSCDA', "     '//10A   Usuario
                fSql = fSql & " RAPP      ) " : vSql = vSql & "'WSCDA') "     '//15A   App
                DB_DB2.Execute(fSql & vSql)

            Next
            Return DevuelveResultado(Cabeceraa.Consolidado, "ReporteDespacho", Bodega)
        Catch
            Return DevuelveError("000", "Error Estructura  XML ", "5011", "ReporteDespacho", Bodega)
        End Try
    End Function

    <WebMethod()> _
    Public Function ReporteEntrega(ByVal XML As String) As XmlDocument
        CargaPrm()
        C_ID = CalcConsec("RFLOGWSCDA", False, "99999999")
        wrtLogWsCDA(C_ID, XML, "EnProceso", "", "ReporteEntrega", "wsBrinsa", "  ")

        ContadorLineas = 0
        Dim Cabeceraa As New DetalleReporteEntrega
        Dim documento As XDocument = XDocument.Parse(XML)
        Dim qx = From xee In documento.Elements("ReporteEntrega")
       Select New With { _
                .Usuario = xee.Element("Usuario").Value,
                .Password = xee.Element("Password").Value,
                .Planilla = xee.Element("Planilla").Value,
                .FechaReporte = xee.Element("FechaReporte").Value,
                .EstadoEntrega = xee.Element("EstadoEntrega").Value,
                .Reporte = xee.Element("Reporte").Value
                   }
        Try
            For Each elements In qx
                Cabeceraa = New DetalleReporteEntrega
                Cabeceraa.Usuario = elements.Usuario
                Cabeceraa.Password = elements.Password
                Cabeceraa.Planilla = elements.Planilla
                Cabeceraa.FechaReporte = elements.FechaReporte
                Cabeceraa.EstadoEntrega = elements.EstadoEntrega
                Cabeceraa.Reporte = elements.Reporte

                fSql = "SELECT * FROM RDCC WHERE DPLANI = " & Cabeceraa.Planilla & ""
                rs1.Open(fSql, DB_DB2)
                Bodega = "  "
                If Not rs1.EOF Then
                    Bodega = rs1("DIWHR").Value
                    Conso = rs1("DCONSO").Value
                    If Conso = "" Then
                        wrtLogWsCDA(C_ID, XML, "Planilla - " & Cabeceraa.Planilla, "", "ReporteEntrega", "wsBrinsa", Bodega)
                        Return DevuelveError(Cabeceraa.Planilla, "Planilla Sin Consolidado", "5051", "ReporteEntrega", Bodega)
                    End If
                Else
                    Return DevuelveError(Cabeceraa.Planilla, "Planilla no Existe", "5051", "ReporteEntrega", Bodega)
                End If
                rs1.Close()


                fSql = "SELECT EBOD FROM RECC WHERE EPLANI  = '" & Cabeceraa.Planilla & "'"
                rs.Open(fSql, DB_DB2)
                Bodega = "  "
                If Not rs.EOF Then
                    Bodega = rs("EBOD").Value
                Else
                    CreaRECC(Conso)
                    fSql = "SELECT EBOD  FROM RECC WHERE EPLANI  = '" & Cabeceraa.Planilla & "'"
                    rs1.Open(fSql, DB_DB2)
                    Bodega = "  "
                    If Not rs1.EOF Then
                        Bodega = rs1("EBOD").Value
                    End If
                    rs1.Close()
                End If
                rs.Close()

                Dim Fechas As Date = Convert.ToDateTime(Cabeceraa.FechaReporte)
                Cabeceraa.FechaReporte = Fechas.ToString("yyyy-MM-dd-HH.mm.ss.000000")
                wrtLogWsCDA(C_ID, XML, "Planilla - " & Cabeceraa.Planilla, "", "ReporteEntrega", "wsBrinsa", Bodega)

                fSql = "SELECT UPASS FROM RCAU WHERE UUSR = '" & Cabeceraa.Usuario & "'"
                rs.Open(fSql, DB_DB2)
                If Not rs.EOF Then
                    If Not rs("UPASS").Value = Cabeceraa.Password Then
                        Return DevuelveError(Cabeceraa.Planilla, "Password Incorrecto", "5050", "ReporteEntrega", Bodega)
                    End If
                Else
                    Return DevuelveError(Cabeceraa.Planilla, "Usuario Incorrecto ", "5051", "ReporteEntrega", Bodega)
                End If
                rs.Close()
                fSql = "SELECT ECONSO, EUDS02, EUDS03 FROM RECC WHERE EPLANI =" & Cabeceraa.Planilla & " AND ECONSO = ''"
                rs.Open(fSql, DB_DB2)
                If Not rs.EOF Then
                    ActualizaRECC(Conso)
                End If
                rs.Close()

                fSql = "SELECT ECONSO, EUDS02, EUDS03 FROM RECC WHERE EPLANI =" & Cabeceraa.Planilla
                rs.Open(fSql, DB_DB2)
                wrtLogWsCDA(C_ID, XML, "Planilla - " & Cabeceraa.Planilla & " - " & Conso, "", "ReporteEntrega", "wsBrinsa", Bodega)
                If Not rs.EOF Then
                    Select Case Cabeceraa.EstadoEntrega
                        Case 35 'Llegada al Cliente
                            If rs("EUDS02").Value = 0 Then
                                fSql = "UPDATE RECC SET ESTS =" & Cabeceraa.EstadoEntrega & ", ELLECLI = '" & Cabeceraa.FechaReporte & "', EUDS02 = 1 "
                                fSql = fSql & " WHERE EPLANI = " & Cabeceraa.Planilla
                                DB_DB2.Execute(fSql)
                                rs.Close()
                            End If
                        Case 50 'Fin de Entrega
                            If rs("EUDS03").Value = 0 Then
                                fSql = "UPDATE RECC SET ESTS =" & Cabeceraa.EstadoEntrega & ", EFINENT = '" & Cabeceraa.FechaReporte & "', EUDS03 = 1 "
                                fSql = fSql & " WHERE EPLANI = " & Cabeceraa.Planilla
                                DB_DB2.Execute(fSql)
                                rs.Close()
                            End If
                        Case Else
                            Return DevuelveError(Cabeceraa.Planilla, "Estado no valido [" & Cabeceraa.EstadoEntrega & "]", "5054", "ReporteEntrega", Bodega)
                    End Select
                Else
                    rs.Close()
                    Return DevuelveError(Cabeceraa.Planilla, "No existe planilla", "5008", "ReporteEntrega", Bodega)
                End If

                fSql = " INSERT INTO RRCC( "
                vSql = " VALUES ( "
                fSql = fSql & " RTREP     , " : vSql = vSql & " 1, "     '//2S0   Tipo Rep
                fSql = fSql & " RCONSO    , " : vSql = vSql & "'" & Conso & "', "     '//6A    Consolidado
                fSql = fSql & " RPLANI    , " : vSql = vSql & " " & Cabeceraa.Planilla & ", "     '//6S0   Planilla
                fSql = fSql & " RSTSC     , " : vSql = vSql & " (SELECT HESTAD FROM RHCC WHERE HCONSO = '" & Conso & "'), "     '//2S0   Est Cons
                fSql = fSql & " RREPORT   , " : vSql = vSql & "'" & Cabeceraa.Reporte & "', "     '//502A  Reporte
                fSql = fSql & " RCRTUSR   , " : vSql = vSql & "'WSCDA', "     '//10A   Usuario
                fSql = fSql & " RAPP      ) " : vSql = vSql & "'WSCDA') "     '//15A   App
                DB_DB2.Execute(fSql & vSql)

            Next
        Catch
            Return DevuelveError("000", "Error Estructura  XML ", "5011", "ReporteEntrega", Bodega)
        End Try
        Return DevuelveResultado(Cabeceraa.Planilla, "ReporteEntrega", Bodega)
    End Function

    <WebMethod()> _
    Public Function Despacho(ByVal XML As String) As XmlDocument
        CargaPrm()
        C_ID = CalcConsec("RFLOGWSCDA", False, "99999999")
        wrtLogWsCDA(C_ID, XML, "Inicio", "", "Despacho", "wsBrinsa", "  ")
        ContadorLineas = 0
        Dim nuevaLinea As DetalleDespacho
        Dim Cabeceraa As New CabeceraDespacho
        Dim Lineas As New List(Of DetalleDespacho)
        Dim documento As XDocument = XDocument.Parse(XML)
        Dim qx = From xee In documento.Elements("Despacho")
       Select New With { _
        .Usuario = xee.Element("Usuario").Value,
        .Password = xee.Element("Password").Value,
        .Manifiesto = xee.Element("Manifiesto").Value,
        .Bodega = xee.Element("Bodega").Value,
        .TipoVehiculo = xee.Element("TipoVehiculo").Value,
        .Notas = xee.Element("Notas").Value,
        .DescTipoVehiculo = xee.Element("DescTipoVehiculo").Value,
        .Placa = xee.Element("Placa").Value,
        .CedulaConductor = xee.Element("CedulaConductor").Value,
        .NombreConductor = xee.Element("NombreConductor").Value,
        .CelularConductor = xee.Element("CelularConductor").Value
                  }
        Try
            For Each elements In qx
                Cabeceraa = New CabeceraDespacho
                If Len(elements.Usuario) > 10 Then
                    Return (DevuelveError("000", "Error Estructura  XML  - USUARIO", "5011", "Despacho", Cabeceraa.Bodega))
                End If
                Cabeceraa.Usuario = elements.Usuario
                If Len(elements.Password) > 10 Then
                    Return (DevuelveError("000", "Error Estructura  XML  - PASSWORD", "5011", "Despacho", Cabeceraa.Bodega))
                End If
                Cabeceraa.Password = elements.Password

                If Len(elements.Manifiesto) > 10 Then
                    Return (DevuelveError("000", "Error Estructura  XML  - MANIFIESTO", "5011", "Despacho", Cabeceraa.Bodega))
                End If
                Cabeceraa.Manifiesto = elements.Manifiesto
                Cabeceraa.Bodega = elements.Bodega
                Cabeceraa.TipoVehiculo = elements.TipoVehiculo
                Cabeceraa.Notas = elements.Notas
                Cabeceraa.DescTipoVehiculo = elements.DescTipoVehiculo
                Cabeceraa.Placa = elements.Placa
                Cabeceraa.CedulaConductor = elements.CedulaConductor
                Cabeceraa.NombreConductor = elements.NombreConductor
                Cabeceraa.CelularConductor = elements.CelularConductor
            Next
            fSql = "SELECT UPASS FROM RCAU WHERE UUSR = '" & Cabeceraa.Usuario & "'"
            rs.Open(fSql, DB_DB2)
            If Not rs.EOF Then
                If Not rs("UPASS").Value = Cabeceraa.Password Then
                    Return DevuelveError(Cabeceraa.Manifiesto, "Password Incorrecto", "5050", "Despacho", Cabeceraa.Bodega)
                End If
            Else
                Return DevuelveError(Cabeceraa.Manifiesto, "Usuario Incorrecto ", "5051", "Despacho", Cabeceraa.Bodega)
            End If
            rs.Close()
            If EsBodSatII(Cabeceraa.Bodega) Then
                Return DevuelveError(Cabeceraa.Manifiesto, "Bodega sin Webservice", "5060", "ConfirmacionEntradaInventario", Cabeceraa.Bodega)
            End If

        Catch
            Return DevuelveError("000", "Error Estructura  XML ", "5011", "Despacho", Cabeceraa.Bodega)
        End Try

        wrtLogWsCDA(C_ID, XML, "Manifiesto - " & Cabeceraa.Manifiesto, "", "Despacho", "wsBrinsa", Cabeceraa.Bodega)
        Conso = ""
        Try
            Dim qx2 = From xe In documento.Descendants.Elements("PedidoDespachado")
            Select New With { _
                          .Pedido = xe.Element("Pedido").Value
                            }
            For Each elementos In qx2
                ContadorLineas = ContadorLineas + 1
                nuevaLinea = New DetalleDespacho
                nuevaLinea.Pedido = elementos.Pedido
                Lineas.Add(nuevaLinea)

                fSql = " INSERT INTO RFBSPEDDES( "
                vSql = " VALUES ( "
                fSql = fSql & " LPEDID   , " : vSql = vSql & " " & nuevaLinea.Pedido & ", "     '//8S0   Pedido
                fSql = fSql & " LBOD      ) " : vSql = vSql & "'" & Cabeceraa.Bodega & "') "     '//3A    Bodega
                Try
                    DB_DB2.Execute(fSql & vSql)
                Catch ex As Exception
                    Return DevuelveError(Cabeceraa.Manifiesto, "Pedido " & nuevaLinea.Pedido & " ya Consolidado o en proceso", "5004", "Despacho", Cabeceraa.Bodega)
                End Try

                fSql = "SELECT * FROM RDCC WHERE DPEDID = " & nuevaLinea.Pedido & ""
                rs.Open(fSql, DB_DB2)
                If Not rs.EOF Then
                    If rs("DCONSO").Value <> "" Then
                        rs.Close()
                        Return DevuelveError(Cabeceraa.Manifiesto, "Pedido ya Consolidado", "5004", "Despacho", Cabeceraa.Bodega)
                    Else
                    End If
                    rs.Close()
                Else
                    rs.Close()
                    Return DevuelveError(Cabeceraa.Manifiesto, "Pedido no Existe", "5005", "Despacho", Cabeceraa.Bodega)
                End If
            Next

            fSql = "SELECT CCCODE FROM ZCC WHERE CCTABL = 'SPTIPVEH' AND CCENUS = '" & Cabeceraa.TipoVehiculo & "'"
            rs.Open(fSql, DB_DB2)
            If rs.EOF Then
                Return DevuelveError(Cabeceraa.Manifiesto, "Tipo de Vehiculo erroneo", "5010", "Despacho", Cabeceraa.Bodega)
            End If
            rs.Close()


        Catch

            Return DevuelveError("000", "Error Estructura  XML ", "5011", "Despacho", Cabeceraa.Bodega)
        End Try


        Try
            Dim qx3 = From xe In documento.Descendants.Elements("PedidoDespachado")
        Select New With { _
                          .Pedido = xe.Element("Pedido").Value
                        }

            Conso = CalcConsec("SPCONSO", False, "999999")
            'consolidado = "379880"
            wrtLogWsCDA(C_ID, XML, "Manifiesto - " & Cabeceraa.Manifiesto & " - " & Conso, "", "Despacho", "wsBrinsa", Cabeceraa.Bodega)

            For Each elementos In qx3
                ContadorLineas = ContadorLineas + 1
                nuevaLinea = New DetalleDespacho
                nuevaLinea.Pedido = elementos.Pedido
                Lineas.Add(nuevaLinea)

                fSql = "SELECT * FROM RECC WHERE ECONSO = '" & Conso & "' AND ESTS NOT IN( 50, 60 )"
                rs.Open(fSql, DB_DB2)

                If Not rs.EOF Then

                    Return DevuelveError(Cabeceraa.Manifiesto, "Consolidado no ha sido totalmente despachado", "5013", "Despacho", Cabeceraa.Bodega)

                End If

                rs.Close()
                fSql = "SELECT * FROM RDCC WHERE DPEDID = " & nuevaLinea.Pedido & ""
                rs.Open(fSql, DB_DB2)
                fSql = "UPDATE RDCC SET DCONSO = '" & Conso & "' WHERE DPEDID = " & nuevaLinea.Pedido & " AND UPRCON = 0 AND DPLANI > 0 "
                DB_DB2.Execute(fSql)
                rs.Close()
            Next

            Dim Sql As String
            Dim Division As String
            Division = " "
            Sql = " SELECT DISTINCT"
            Sql = Sql & " RREF02 "
            Sql = Sql & " FROM RDCC "
            Sql = Sql & " INNER JOIN RIIM ON DPROD=RIPROD"
            Sql = Sql & " WHERE DCONSO ='" & Conso & "'"
            rs.Open(Sql, DB_DB2)
            Do While Not rs.EOF
                Select Case Trim(rs("RREF02").Value)
                    Case "SAL"
                        Division = "CSECA"
                    Case "ASEO"
                        Division = "CSECA"
                    Case "QSE"
                        Division = "CSECA"
                    Case "QLA", "QLF"
                        Division = rs("RREF02").Value
                End Select
                rs.MoveNext()
            Loop
            rs.Close()


            fSql = " SELECT  "
            fSql = fSql & "  P.DSPOST, "
            fSql = fSql & "  P.DSHDEP, "
            fSql = fSql & "  IFNULL( AVG(K.FTDIST), 0 ) AS KM "
            fSql = fSql & " FROM "
            fSql = fSql & "  RDCC P "
            fSql = fSql & "  LEFT JOIN RFFLTRF K ON P.DIWHR = K.FBOD AND P.DSPOST = K.FCIUD AND P.DSHDEP = K.FDPTO "
            fSql = fSql & " WHERE "
            fSql = fSql & "  P.DCONSO = '" & Conso & "' "
            fSql = fSql & " GROUP BY "
            fSql = fSql & "  P.DSPOST, "
            fSql = fSql & "  P.DSHDEP "
            fSql = fSql & " ORDER BY 3  "
            rs.Open(fSql, DB_DB2)
            Do While Not rs.EOF
                Cabeceraa.Dpto = rs("DSHDEP").Value
                Cabeceraa.Ciudad = rs("DSPOST").Value
                Cabeceraa.Destino &= rs("DSHDEP").Value & "/" & rs("DSPOST").Value & " "
                rs.MoveNext()
            Loop
            rs.Close()

            fSql = " INSERT INTO RHCC( "
            vSql = " VALUES ( "
            fSql = fSql & " HSFCNS    , " : vSql = vSql & "'SAT', "     '//3A    Tipo Consolidado
            fSql = fSql & " HCONSO    , " : vSql = vSql & "'" & Conso & "', "     '//6A    Consolidado
            fSql = fSql & " HPLACA    , " : vSql = vSql & "'" & Cabeceraa.Placa & "', "     '//6A    Placa
            fSql = fSql & " HPROVE    , " : vSql = vSql & "5013" & ", "     '//8P0   Transp
            fSql = fSql & " HPLTA     , " : vSql = vSql & "'10', "     '//Planta
            fSql = fSql & " HNPROVE   , " : vSql = vSql & "'COLTANQUES', "     '//50A   Transportador
            fSql = fSql & " HFERECA   , " : vSql = vSql & " NOW()   , "     '//26Z   F.Reque Vehiculo
            fSql = fSql & " HCEDUL    , " : vSql = vSql & " " & Cabeceraa.CedulaConductor & ", "     '//18P0  Cedula Conductor
            fSql = fSql & " HCHOFE    , " : vSql = vSql & "'" & Cabeceraa.NombreConductor & "', "     '//30A   Conductor
            fSql = fSql & " HCELU     , " : vSql = vSql & "'" & Left(Cabeceraa.CelularConductor, 20) & "', "     '//20A   Celular
            fSql = fSql & " HGENERA   , " : vSql = vSql & "'1', "     '//1A    Generada OC S/N
            fSql = fSql & " HMANIF    , " : vSql = vSql & "'" & Cabeceraa.Manifiesto & "', "     '//15A   Manifiesto del Transp
            fSql = fSql & " HTIPO     , " : vSql = vSql & " (SELECT CCCODE FROM ZCC WHERE CCTABL = 'SPTIPVEH' AND CCENUS = '" & Cabeceraa.TipoVehiculo & "')" & ", "     '//2P0   Tipo Vehiculo

            fSql = fSql & " HDESTI    , " : vSql = vSql & "'" & Left(Cabeceraa.Destino, 50) & "', "     '//50A   Destino
            fSql = fSql & " HCIUDAD   , " : vSql = vSql & "'" & Cabeceraa.Ciudad & "', "     '//9A    Ciudad
            fSql = fSql & " HDEPTO    , " : vSql = vSql & "'" & Cabeceraa.Dpto & "', "     '//3A    Depto

            fSql = fSql & " HOBSER    , " : vSql = vSql & "'" & Cabeceraa.Notas & "', "     '//120A  Observaciones
            fSql = fSql & " HDIVIS    , " : vSql = vSql & "'" & Division & "', "     '//120A  Observaciones
            fSql = fSql & " HPER      , " : vSql = vSql & Now().ToString("yyyyMM") & ", "      '
            fSql = fSql & " HBOD      , " : vSql = vSql & "'" & Cabeceraa.Bodega & "', "     '//3A    Bodega
            fSql = fSql & " HREF05    ) " : vSql = vSql & "'" & Cabeceraa.Bodega & "' )"     '//15A   Bodega

            DB_DB2.Execute(fSql & vSql)
            CreaRECC(Conso)

            fSql = "SELECT HESTAD FROM RHCC WHERE HCONSO = '" & Conso & "'"
            rs.Open(fSql, DB_DB2)
            If rs.EOF Then
                Return DevuelveError(Cabeceraa.Manifiesto, "Consolidado No existe", "5013", "Despacho", Cabeceraa.Bodega)
            End If
            rs.Close()

            fSql = " INSERT INTO RRCC( "
            vSql = " VALUES ( "
            fSql = fSql & " RTREP     , " : vSql = vSql & " 2, "     '//2S0   Tipo Rep
            fSql = fSql & " RCONSO    , " : vSql = vSql & "'" & Conso & "', "     '//6A    Consolidado
            fSql = fSql & " RMANIF    , " : vSql = vSql & "'" & Cabeceraa.Manifiesto & "', "     '//15A   Manifiesto del Transp
            fSql = fSql & " RPLACA    , " : vSql = vSql & "'" & Cabeceraa.Placa & "', "     '//6A    Placa
            fSql = fSql & " RSTSC     , " : vSql = vSql & " (SELECT HESTAD FROM RHCC WHERE HCONSO = '" & Conso & "'), "     '//2S0   Est Cons
            fSql = fSql & " RREPORT   , " : vSql = vSql & "'Consolidado generado.', "     '//502A  Reporte
            fSql = fSql & " RCRTUSR   , " : vSql = vSql & "'WSCDA', "     '//10A   Usuario
            fSql = fSql & " RAPP      ) " : vSql = vSql & "'WSCDA') "     '//15A   App
            DB_DB2.Execute(fSql & vSql)

            Return DevuelveResultado(Cabeceraa.Manifiesto, "Despacho", Cabeceraa.Bodega)
        Catch ex As Exception
            Return DevuelveError("000", "Error Estructura  XML " & ex.ToString, "5011", "Despacho", Cabeceraa.Bodega)
        End Try


    End Function
    Function DevuelveError(ByVal NumeroDocumento As String, ByVal ERROOR As String, ByVal CODIGO As String, ByVal proceso As String, ByVal Bodega As String) As XmlDocument
        Dim xmlstr As String = ""
        Dim sw As New StringWriter()
        Dim writer As New XmlTextWriter(sw)
        Dim flag As Integer = 0
        Dim flag2 As Integer = 0

        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        'writer.WriteStartDocument(True)
        writer.Formatting = Formatting.Indented
        writer.Indentation = 2
        writer.WriteStartElement("Respuesta")
        writer.WriteStartElement("Operacion")
        writer.WriteString(proceso)
        writer.WriteEndElement()
        If (proceso = "ConfirmacionEntradaInventario") Then
            writer.WriteStartElement("Consolidado")
            writer.WriteString(Conso)
            writer.WriteEndElement()
        End If
        If (proceso = "Despacho") Then
            writer.WriteStartElement("Consolidado")
            writer.WriteString(Conso)
            writer.WriteEndElement()
            writer.WriteStartElement("Manifiesto")
        Else
            If (proceso = "ReporteDespacho") Then
                writer.WriteStartElement("Consolidado")
            Else
                If (proceso <> "ReporteEntrega") Then
                    writer.WriteStartElement("Pedido")
                Else
                    writer.WriteStartElement("Planilla")
                End If
            End If
        End If
        writer.WriteString(NumeroDocumento)
        writer.WriteEndElement()
        If (proceso <> "ConfirmacionEntradaInventario" And proceso <> "PedidoDespachado" And proceso <> "ReporteDespacho" And proceso <> "ReporteEntrega" And proceso <> "Despacho") Then
            writer.WriteStartElement("TotalLineas")
            writer.WriteString(ContadorLineas)
            writer.WriteEndElement()
        End If
        writer.WriteStartElement("Mensaje")
        writer.WriteString(CODIGO)
        writer.WriteEndElement()

        writer.WriteStartElement("Mensajetexto")
        writer.WriteString(soloLetras(ERROOR))
        writer.WriteEndElement()

        writer.WriteEndElement()

        writer.Flush()
        xmlstr = sw.ToString
        writer.Close()
        Dim pDoc As New XmlDocument
        pDoc.LoadXml(xmlstr)
        wrtLogWsCDA(C_ID, sw.ToString, "", "Fail", proceso, "wsBrinsa", Bodega)
        AvisaProblema(C_ID, ERROOR, proceso, "", qBodega)
        Return pDoc



    End Function

    Function CrearNodoError(ByVal ErrorXML As String, ByVal codigo As String, ByVal NumeroDocumento As String, ByVal proceso As String, ByVal writer As XmlTextWriter)


        writer.WriteStartElement(proceso) ' ENVELOPE
        writer.WriteStartElement("Documento")  ' PEDIDO
        writer.WriteString(NumeroDocumento)
        writer.WriteStartElement("Mensaje")     'MENSAJE
        writer.WriteStartElement("Error")       'ERROR
        writer.WriteStartElement("MensajeID")   'ID
        writer.WriteString(codigo)
        writer.WriteEndElement() ' MensajeId
        writer.WriteStartElement("MensajeDES")
        writer.WriteString(ErrorXML)
        writer.WriteEndElement() ' DES
        writer.WriteEndElement() ' ERROR
        writer.WriteEndElement() ' MENSAJE
        writer.WriteEndElement() ' PEDIDO
        writer.WriteEndElement() ' ENVELOPE
        Return 0
    End Function

    Function DevuelveResultado(ByVal NumeroDocumento As String, ByVal proceso As String, ByVal Bodega As String) As XmlDocument
        Dim xmlstr As String = ""
        Dim sw As New StringWriter()
        Dim writer As New XmlTextWriter(sw)
        Dim flag As Integer = 0
        Dim flag2 As Integer = 0

        Dim rs As New ADODB.Recordset

        'writer.WriteStartDocument(True)
        writer.Formatting = Formatting.Indented
        writer.Indentation = 2
        writer.WriteStartElement("Respuesta")
        writer.WriteStartElement("Operacion")
        writer.WriteString(proceso)
        writer.WriteEndElement()
        If (proceso = "Despacho") Then
            writer.WriteStartElement("Consolidado")
            writer.WriteString(Conso)
            writer.WriteEndElement()
            writer.WriteStartElement("Manifiesto")
        Else
            If (proceso = "ReporteDespacho") Then
                writer.WriteStartElement("Consolidado")
            Else
                If (proceso <> "ReporteEntrega") Then
                    writer.WriteStartElement("Pedido")
                Else
                    writer.WriteStartElement("Planilla")
                End If
            End If
        End If
        writer.WriteString(NumeroDocumento)
        writer.WriteEndElement()
        If (proceso <> "ConfirmacionEntradaInventario" And proceso <> "PedidoDespachado" And proceso <> "ReporteDespacho" And proceso <> "ReporteEntrega" And proceso <> "Despacho") Then
            writer.WriteStartElement("TotalLineas")
            writer.WriteString(ContadorLineas)
            writer.WriteEndElement()
        End If
        writer.WriteStartElement("Mensaje")
        writer.WriteString("0000")
        writer.WriteEndElement()
        writer.WriteStartElement("Mensajetexto")
        writer.WriteString("Operación Exitosa")
        writer.WriteEndElement()
        writer.WriteEndElement()

        writer.Flush()
        xmlstr = sw.ToString
        writer.Close()
        Dim pDoc As New XmlDocument
        pDoc.LoadXml(xmlstr)
        wrtLogWsCDA(C_ID, sw.ToString, "", "Pass", proceso, "wsBrinsa", Bodega)

        Return pDoc

    End Function



    Private Sub InitializeComponent()

    End Sub

    Function CargaPrm() As Boolean


        gsProvider = "IBMDA400"
        gsDatasource = My.Settings.AS400
        sLib = My.Settings.AMBIENTE

        gsConnString = "Provider=" & gsProvider & ";Data Source=" & gsDatasource & ";"
        gsConnString = gsConnString & "Persist Security Info=False;Default Collection=" & sLib & ";"
        gsConnString = gsConnString & "Password=LXAPL;User ID=APLLX;"

        DB_DB2 = New ADODB.Connection
        DB_MP2 = New ADODB.Connection
        DB_DB2.Open(gsConnString)

        rs.Open(" SELECT * FROM " & sLib & ".ZCC WHERE CCTABL = 'LXLONG'", DB_DB2)
        Do While Not rs.EOF
            Select Case rs("CCCODE").Value.ToString
                Case "LXUSER"
                    AS400Param(0) = rs("CCDESC").Value.ToString
                Case "LXPASS"
                    AS400Param(1) = rs("CCDESC").Value.ToString
            End Select
            rs.MoveNext()
        Loop
        rs.Close()
        Dim i As Integer
        For i = 0 To 1
            If AS400Param(i) = "" Then
                Resultado = False
            End If
        Next

        gsConnString = "Provider=" & gsProvider & ";Data Source=" & gsDatasource & ";"
        gsConnString = gsConnString & "Persist Security Info=False;Default Collection=" & sLib & ";"
        gsConnString = gsConnString & "Password=" & AS400Param(1) & ";User ID=" & AS400Param(0) & ";"
        gsConnString = gsConnString & "Force Translate=0;"
        DB_DB2.Close()
        If Resultado Then
            DB_DB2.Open(gsConnString)
        Else
            'Error Conexion
        End If

        Return Resultado

    End Function
    Public Function CalcConsec(ByRef sID As String, ByRef bLee As Boolean, ByRef TopeMax As String) As Double
        Dim rs As ADODB.Recordset
        Dim locID As Double
        Dim sql As String
        Dim sConsec As String
        Dim tMax As Double

        sql = "SELECT CCDESC FROM ZCCL01 WHERE CCTABL = 'SECUENCE' AND CCCODE='" & sID & "'"
        rs = New ADODB.Recordset
        tMax = Val(TopeMax)

        With rs
            .CursorLocation = ADODB.CursorLocationEnum.adUseServer
            .Open(sql, DB_DB2, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)
            If Not (.BOF And .EOF) Then
                locID = Val(.Fields("CCDESC").Value) + 1
                If locID > tMax Then
                    locID = 1
                End If
                sConsec = Format(locID, New String("0", Len(TopeMax)))
                If Not bLee Then
                    .Fields("CCDESC").Value = sConsec
                    .Update()
                End If
                .Close()
            Else
                locID = 1
                .Close()
                sConsec = Format(1, New String("0", Len(8)))
                DB_DB2.Execute(" INSERT INTO ZCC (CCID, CCTABL, CCCODE, CCDESC ) " & " VALUES( 'CC', 'SECUENCE', '" & sID & "', '" & sConsec & "' )")
            End If
        End With

        CalcConsec = locID

    End Function

    Sub wrtLogWsCDA(ByVal logId As String, ByVal xml As String, ByVal id_msg As String, ByVal estado As String, ByVal operacion As String, ByVal fuente As String, ByVal usuario As String)
        Dim rs As New ADODB.Recordset
        Dim dato() As Byte

        dato = System.Text.Encoding.UTF8.GetBytes("<?xml version=""1.0""?>" & xml)
        With rs
            .CursorLocation = ADODB.CursorLocationEnum.adUseServer
            .Open("SELECT * FROM RFLOGWSCDA WHERE C_ID = " & logId, DB_DB2, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)
            If .EOF Then
                .AddNew()
                .Fields("C_ID").Value = logId
                .Fields("C_XML").Value = dato
                .Fields("C_NOUN_ID").Value = operacion
                .Fields("C_MESSAGEID").Value = id_msg
                .Fields("C_SOURCE_NAME").Value = fuente
                .Fields("C_USER").Value = usuario
                .Fields("C_BODID").Value = ""
            Else
                .Fields("C_USER").Value = usuario
                If id_msg = "" Then
                    .Fields("C_WAS_PROCESSED").Value = 1
                    .Fields("C_REPLY").Value = dato
                    .Fields("C_PASS_FAIL").Value = estado
                Else
                    .Fields("C_MESSAGEID").Value = id_msg
                End If
            End If
            .Update()
            .Close()
        End With
        rs = Nothing
    End Sub

    Public Sub AvisaProblema(ByVal logId As String, ByVal id_msg As String, ByVal operacion As String, ByVal fuente As String, ByVal usuario As String)
        Dim url As String
        url = "<a href=""https://transportes.brinsa.com.co/tm/common/lxconn/cda_inbox.asp?s_C_ID=" & logId & """>Log WS-CDA " & logId & "</a>"
        fSql = " INSERT INTO RFLOG( "
        vSql = " VALUES ( "
        fSql = fSql & " USUARIO   , " : vSql = vSql & "'" & usuario & "', "      '//502A
        fSql = fSql & " PROGRAMA  , " : vSql = vSql & "'" & "WSCDA" & "', "      '//502A
        'fSql = fSql & " ALERT     , " : vSql = vSql & "1, "      '//1P0
        fSql = fSql & " EVENTO    , " : vSql = vSql & "'" & Replace(id_msg, "'", "''") & "<br>" & url & "', "      '//20002A
        fSql = fSql & " FECHAWIN  , " : vSql = vSql & "'" & timeStampWin() & "', "      '//26Z
        fSql = fSql & " TXTSQL    ) " : vSql = vSql & "'" & Replace(operacion, "'", "''") & "' ) "      '//5002A
        DB_DB2.Execute(fSql & vSql)
    End Sub

    Function EsBodSatII(Bod As String) As Boolean
        Dim fSql As String
        Dim rs As New ADODB.Recordset
        Dim resultado As Boolean = False

        fSql = " SELECT CCUDC1 as BodSatII FROM ZCC WHERE CCTABL = 'BODSAT' AND CCCODE = '" & Bod & "' "
        rs.Open(fSql, DB_DB2)
        If Not rs.EOF Then
            If rs("BodSatII").Value = 1 Then
                resultado = True
            End If
        End If
        rs.Close()
        rs = Nothing
        Return resultado

    End Function

    Public Sub CreaRECC(ByVal Consolidado As String)

        ActualizaRECC(Consolidado)

    End Sub

    Public Sub ActualizaRECC(ByVal Conso As String)
        Dim numTarea As String = CalcConsec("RFTASKBATCH", False, "9999999")

        fSql = " INSERT INTO RFTASK( "
        vSql = " VALUES ( "
        fSql = fSql & " TCATEG    , " : vSql = vSql & "'RECC', "                '//20A   Categoria
        fSql = fSql & " TNUMTAREA , " : vSql = vSql & " " & numTarea & ", "                  '//7P0   Numero de Tarea
        fSql = fSql & " TFPROXIMA , " : vSql = vSql & " NOW() , "                '//26Z   Proxima Ejecucion
        fSql = fSql & " TTIPO     , " : vSql = vSql & "'Rutina', "                     '//22A   Tipo
        fSql = fSql & " TKEY      , " : vSql = vSql & "'" & Conso & "', "
        fSql = fSql & " TKEYWORD  , " : vSql = vSql & "'" & Conso & "', "
        fSql = fSql & " STS1      , " : vSql = vSql & " 0, "                   '//1S0   Sts 1
        fSql = fSql & " TCRTUSR   , " : vSql = vSql & "'WSCDA', "     '//10A   Usuario
        fSql = fSql & " TPGM      ) " : vSql = vSql & "'E:\Despachos_DB2\rutina.bat') "                  '//502A  Programa	
        DB_DB2.execute(fSql & vSql)

    End Sub

    'ControLlave()
    Function ControlLlave(Categ As String, Llave As String) As Boolean
        Dim fSql, vSql As String

        fSql = " INSERT INTO RFCTLKEY( "
        vSql = " VALUES ( "
        fSql = fSql & " KCATEG    , " : vSql = vSql & "'" & Categ & "', "
        fSql = fSql & " KLLAVE    ) " : vSql = vSql & "'" & Llave & "') "     '//120A  
        Try
            DB_DB2.execute(fSql & vSql)
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function


End Class