﻿using MSXML2;
using Syncfusion.EJ.Export;
using Syncfusion.JavaScript;
using Syncfusion.JavaScript.DataSources;
using Syncfusion.JavaScript.Models;
using Syncfusion.Linq;
using Syncfusion.XlsIO;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Requisiciones.Models
{
    public class Funciones
    {
        public void ExportToExcel(string GridModel, dynamic datos, string Nombre)
        {
            ExcelExport exp = new ExcelExport();
            var DataSource = datos;
            var count = DataSource.Count;
            GridProperties gridProperty = (GridProperties)Utils.DeserializeToModel(typeof(GridProperties), GridModel);
            GridExtensions ext = new GridExtensions();
            AutoFormat auto = new AutoFormat();
            ext.SetTheme(auto, "flat-saffron");
            auto.FontFamily = "Roboto";
            auto.ContentFontSize = 9;
            auto.GCaptionBorderColor = Color.FromArgb(219, 231, 245);
            auto.GContentFontColor = Color.Black;
            auto.HeaderFontSize = 10;
            auto.AltRowBgColor = Color.FromArgb(219, 231, 245);
            auto.ContentBgColor = Color.FromArgb(255, 255, 255);
            gridProperty.AutoFormat = auto;
            IWorkbook book = exp.Export(gridProperty, DataSource, $"{Nombre}.xlsx", ExcelVersion.Excel2010, false, false, "flat-saffron", true);
            IWorksheet worksheet = book.Worksheets[0];
            IStyle headingStyle = book.Styles.Add("HeadingStyle");
            headingStyle.Font.Bold = true;
            headingStyle.Font.Color = ExcelKnownColors.White;
            headingStyle.HorizontalAlignment = ExcelHAlign.HAlignCenter;
            headingStyle.Color = Color.FromArgb(4, 60, 125);

            worksheet.Range[1, 1, 1, gridProperty.Columns.Count].CellStyle = headingStyle;

            worksheet.UsedRange.WrapText = false;

            //worksheet.AutoFilters.FilterRange = worksheet.Range[$"A1:J{count}"];
            worksheet.AutoFilters.FilterRange = worksheet.Range[1, 1, count, gridProperty.Columns.Count];
            worksheet.Range[1, 1, count, gridProperty.Columns.Count].AutofitColumns();
            //worksheet.UsedRange.AutofitColumns();

            book.Author = "BSS Ltda";
            //book.ActiveSheet.Range[1, 4, count, 4].NumberFormat = "$             #,##0.00"; //Customize the formatting for account formatting 
            book.SaveAs($"{Nombre}.xlsx", ExcelSaveType.SaveAsXLS, System.Web.HttpContext.Current.Response, ExcelDownloadType.Open);

        }

        public class DataResult
        {
            public IEnumerable result { get; set; }
            public int count { get; set; }
            public IEnumerable aggregate { get; set; }
            public IEnumerable groupDs { get; set; }
        }

        public JsonResult DSGenerico(DataManager dm, IEnumerable Data)
        {
            int count = 0;
            DataResult result = new DataResult();
            List<string> str = new List<string>();
            DataOperations operation = new DataOperations();

            if (dm.Sorted != null && dm.Sorted.Count > 0) //Sorting
            {
                Data = operation.PerformSorting(Data, dm.Sorted);
            }
            if (dm.Where != null && dm.Where.Count > 0) //Filtering
            {
                Data = operation.PerformWhereFilter(Data, dm.Where, dm.Where[0].Condition);
            }
            if (dm.Aggregates != null)
            {
                for (var i = 0; i < dm.Aggregates.Count; i++)
                    str.Add(dm.Aggregates[i].Field);
                result.aggregate = operation.PerformSelect(Data, str);
            }
            try
            {
                count = Data.AsQueryable().Count();
            }
            catch (System.Exception ex)
            {
                count = 0;
            }
            if (dm.Skip != 0)
            {
                Data = operation.PerformSkip(Data, dm.Skip);
            }
            if (dm.Take == 0)
            {
                result.result = operation.PerformTake(Data, count);
            }
            else
            {
                result.result = operation.PerformTake(Data, dm.Take);
            }

            result.count = count;
            Data = operation.Execute(Data, dm);

            JsonResult jr = new JsonResult()
            {
                MaxJsonLength = int.MaxValue,
                Data = result,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

            return jr;
        }

        public JsonResult DSGenericoLazy(DataManager dm, IEnumerable Data, int TotalRegistros)
        {
            int count = 0;
            DataResult result = new DataResult();
            List<string> str = new List<string>();
            DataOperations operation = new DataOperations();

            if (dm.Sorted != null && dm.Sorted.Count > 0) //Sorting
            {
                Data = operation.PerformSorting(Data, dm.Sorted);
            }
            if (dm.Where != null && dm.Where.Count > 0) //Filtering
            {
                Data = operation.PerformWhereFilter(Data, dm.Where, dm.Where[0].Condition);
            }
            if (dm.Aggregates != null)
            {
                for (var i = 0; i < dm.Aggregates.Count; i++)
                    str.Add(dm.Aggregates[i].Field);
                result.aggregate = operation.PerformSelect(Data, str);
            }
            try
            {
                count = TotalRegistros;
            }
            catch (System.Exception ex)
            {
                count = 0;
            }
            if (dm.Skip != 0)
            {
                Data = operation.PerformSkip(Data, dm.Skip);
            }
            if (dm.Take == 0)
            {
                result.result = operation.PerformTake(Data, count);
            }
            else
            {
                result.result = operation.PerformTake(Data, dm.Take);
            }

            result.count = count;
            Data = operation.Execute(Data, dm);

            JsonResult jr = new JsonResult()
            {
                MaxJsonLength = int.MaxValue,
                Data = result,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

            return jr;
        }

        public string CallService(string data, string urlService)
        {
            string res = "";
            try
            {
                if (!string.IsNullOrEmpty(urlService))
                {
                    //urlService = "http://192.168.8.84:8089/api/auth/login";

                    ServerXMLHTTP srv = new ServerXMLHTTP();
                    srv.open("POST", urlService, false, null, null);
                    srv.setRequestHeader("Content-Type", "application/json");
                    srv.send(data);

                    res = srv.responseText;
                }
            }
            catch (ProtocolViolationException ex)
            {
                res = "ERROR: " + ex.ToString() + ((ex.InnerException != null) ? ex.InnerException.ToString() : "");
            }
            catch (WebException ex)
            {
                res = "ERROR: " + ex.ToString() + ((ex.InnerException != null) ? ex.InnerException.ToString() : "");
                //string strReturn = string.Empty;
                switch (ex.Status)
                {
                    case WebExceptionStatus.ProtocolError:
                        if (ex is WebException && ((WebException)ex).Status == WebExceptionStatus.ProtocolError)
                        {
                            var resp = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();

                        }
                        else
                            res = "Protocol error";
                        break;
                    case WebExceptionStatus.ConnectFailure:
                        res = "Connect Failure";
                        break;
                    default:
                        res = new StreamReader(ex.Response.GetResponseStream())
                    .ReadToEnd();
                        break;
                }
                //pResponse = ex.Message.ToString();
            }
            catch (System.Exception ex)
            {
                res = "ERROR: " + ex.ToString() + ((ex.InnerException != null) ? ex.InnerException.ToString() : "");
            }

            return res;
        }
    }
}