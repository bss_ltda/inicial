﻿using System.ComponentModel.DataAnnotations;

namespace RequisicionesCore.Entities
{
    public class RIIMC
    {
        private string _RCPROD;
        private string _RCDESC;
        private string _RCDIVI;
        private string _RCLINE;
        private string _RCSEGM;
        private string _RCEMPA;
        private string _RCTIPO;
        private string _CLASDZ;

        [Display(Name = "RCPROD")]
        public string RCPROD { get => _RCPROD?.Trim(); set => _RCPROD = value; }
        [Display(Name = "RCDESC")]
        public string RCDESC { get => _RCDESC?.Trim(); set => _RCDESC = value; }
        [Display(Name = "RCDIVI")]
        public string RCDIVI { get => _RCDIVI?.Trim(); set => _RCDIVI = value; }
        [Display(Name = "RCLINE")]
        public string RCLINE { get => _RCLINE?.Trim(); set => _RCLINE = value; }
        [Display(Name = "RCSEGM")]
        public string RCSEGM { get => _RCSEGM?.Trim(); set => _RCSEGM = value; }
        [Display(Name = "RCEMPA")]
        public string RCEMPA { get => _RCEMPA?.Trim(); set => _RCEMPA = value; }
        [Display(Name = "RCTIPO")]
        public string RCTIPO { get => _RCTIPO?.Trim(); set => _RCTIPO = value; }
        [Display(Name = "CLASDZ")]
        public string CLASDZ { get => _CLASDZ?.Trim(); set => _CLASDZ = value; }
    }
}
