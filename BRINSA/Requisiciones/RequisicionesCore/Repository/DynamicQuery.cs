﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace RequisicionesCore.Repository
{
    public class MyExpressionVisitor : ExpressionVisitor
    {
        protected override Expression VisitMember(MemberExpression node)
        {
            switch (node.Expression.NodeType)
            {
                case ExpressionType.Constant:
                case ExpressionType.MemberAccess:
                    {
                        var cleanNode = GetMemberConstant(node);

                        //Test
                        //Assert.AreEqual(1L, cleanNode.Value);

                        return cleanNode;
                    }
                default:
                    {
                        return base.VisitMember(node);
                    }
            }
        }

        private static ConstantExpression GetMemberConstant(MemberExpression node)
        {
            object value;

            if (node.Member.MemberType == MemberTypes.Field)
            {
                value = GetFieldValue(node);
            }
            else if (node.Member.MemberType == MemberTypes.Property)
            {
                value = GetPropertyValue(node);
            }
            else
            {
                throw new System.NotSupportedException();
            }

            return Expression.Constant(value, node.Type);
        }

        private static object GetFieldValue(MemberExpression node)
        {
            var fieldInfo = (FieldInfo)node.Member;

            var instance = (node.Expression == null) ? null : TryEvaluate(node.Expression).Value;

            return fieldInfo.GetValue(instance);
        }

        private static object GetPropertyValue(MemberExpression node)
        {
            var propertyInfo = (PropertyInfo)node.Member;

            var instance = (node.Expression == null) ? null : TryEvaluate(node.Expression).Value;

            return propertyInfo.GetValue(instance, null);
        }

        private static ConstantExpression TryEvaluate(Expression expression)
        {
            if (expression.NodeType == ExpressionType.Constant)
            {
                return (ConstantExpression)expression;
            }
            else
            {
                return GetMemberConstant((MemberExpression)expression);
            }

            throw new System.NotSupportedException();
        }
    }

    //// Base Visitor class:
    //public abstract class Visitor
    //{
    //    private readonly Expression node;

    //    protected Visitor(Expression node)
    //    {
    //        this.node = node;
    //    }

    //    public abstract void Visit(string prefix);

    //    public ExpressionType NodeType => this.node.NodeType;
    //    //public static Visitor CreateFromExpression(Expression node)
    //    //{
    //    //    switch (node.NodeType)
    //    //    {
    //    //        case ExpressionType.Constant:
    //    //            return new ConstantVisitor((ConstantExpression)node);
    //    //        case ExpressionType.Lambda:
    //    //            return new LambdaVisitor((LambdaExpression)node);
    //    //        case ExpressionType.Parameter:
    //    //            return new ParameterVisitor((ParameterExpression)node);
    //    //        case ExpressionType.Add:
    //    //            return new BinaryVisitor((BinaryExpression)node);
    //    //        default:
    //    //            System.Console.Error.WriteLine($"Node not processed yet: {node.NodeType}");
    //    //            return default(Visitor);
    //    //    }
    //    //}
    //    public static Visitor CreateFromExpression(Expression node)
    //    {
    //        switch (node.NodeType)
    //        {
    //            case ExpressionType.Constant:
    //                return new ConstantVisitor((ConstantExpression)node);
    //            case ExpressionType.Lambda:
    //                return new LambdaVisitor((LambdaExpression)node);
    //            case ExpressionType.Parameter:
    //                return new ParameterVisitor((ParameterExpression)node);
    //            case ExpressionType.Add:
    //            case ExpressionType.Equal:
    //            case ExpressionType.Multiply:
    //                return new BinaryVisitor((BinaryExpression)node);
    //            case ExpressionType.Conditional:
    //                return new ConditionalVisitor((ConditionalExpression)node);
    //            case ExpressionType.Call:
    //                return new MethodCallVisitor((MethodCallExpression)node);
    //            default:
    //                System.Console.Error.WriteLine($"Node not processed yet: {node.NodeType}");
    //                return default(Visitor);
    //        }
    //    }
    //}

    // Lambda Visitor
    //public class LambdaVisitor : Visitor
    //{
    //    private readonly LambdaExpression node;
    //    public LambdaVisitor(LambdaExpression node) : base(node)
    //    {
    //        this.node = node;
    //    }

    //    public override void Visit(string prefix)
    //    {
    //        System.Console.WriteLine($"{prefix}This expression is a {NodeType} expression type");
    //        System.Console.WriteLine($"{prefix}The name of the lambda is {((node.Name == null) ? "<null>" : node.Name)}");
    //        System.Console.WriteLine($"{prefix}The return type is {node.ReturnType.ToString()}");
    //        System.Console.WriteLine($"{prefix}The expression has {node.Parameters.Count} argument(s). They are:");
    //        // Visit each parameter:
    //        foreach (var argumentExpression in node.Parameters)
    //        {
    //            var argumentVisitor = Visitor.CreateFromExpression(argumentExpression);
    //            argumentVisitor.Visit(prefix + "\t");
    //        }
    //        System.Console.WriteLine($"{prefix}The expression body is:");
    //        // Visit the body:
    //        var bodyVisitor = Visitor.CreateFromExpression(node.Body);
    //        bodyVisitor.Visit(prefix + "\t");
    //    }
    //}

    // Binary Expression Visitor:
    //public class BinaryVisitor : Visitor
    //{
    //    private readonly BinaryExpression node;
    //    public BinaryVisitor(BinaryExpression node) : base(node)
    //    {
    //        this.node = node;
    //    }

    //    public override void Visit(string prefix)
    //    {
    //        System.Console.WriteLine($"{prefix}This binary expression is a {NodeType} expression");
    //        var left = Visitor.CreateFromExpression(node.Left);
    //        System.Console.WriteLine($"{prefix}The Left argument is:");
    //        left.Visit(prefix + "\t");
    //        var right = Visitor.CreateFromExpression(node.Right);
    //        System.Console.WriteLine($"{prefix}The Right argument is:");
    //        right.Visit(prefix + "\t");
    //    }
    //}

    // Parameter visitor:
    //public class ParameterVisitor : Visitor
    //{
    //    private readonly ParameterExpression node;
    //    public ParameterVisitor(ParameterExpression node) : base(node)
    //    {
    //        this.node = node;
    //    }

    //    public override void Visit(string prefix)
    //    {
    //        System.Console.WriteLine($"{prefix}This is an {NodeType} expression type");
    //        System.Console.WriteLine($"{prefix}Type: {node.Type.ToString()}, Name: {node.Name}, ByRef: {node.IsByRef}");
    //    }
    //}

    // Constant visitor:
    //public class ConstantVisitor : Visitor
    //{
    //    private readonly ConstantExpression node;
    //    public ConstantVisitor(ConstantExpression node) : base(node)
    //    {
    //        this.node = node;
    //    }

    //    public override void Visit(string prefix)
    //    {
    //        System.Console.WriteLine($"{prefix}This is an {NodeType} expression type");
    //        System.Console.WriteLine($"{prefix}The type of the constant value is {node.Type}");
    //        System.Console.WriteLine($"{prefix}The value of the constant value is {node.Value}");
    //    }
    //}

    //public class MethodCallVisitor : Visitor
    //{
    //    private readonly MethodCallExpression node;
    //    public MethodCallVisitor(MethodCallExpression node) : base(node)
    //    {
    //        this.node = node;
    //    }

    //    public override void Visit(string prefix)
    //    {
    //        System.Console.WriteLine($"{prefix}This expression is a {NodeType} expression");
    //        if (node.Object == null)
    //            System.Console.WriteLine($"{prefix}This is a static method call");
    //        else
    //        {
    //            System.Console.WriteLine($"{prefix}The receiver (this) is:");
    //            var receiverVisitor = Visitor.CreateFromExpression(node.Object);
    //            receiverVisitor.Visit(prefix + "\t");
    //        }

    //        var methodInfo = node.Method;
    //        System.Console.WriteLine($"{prefix}The method name is {methodInfo.DeclaringType}.{methodInfo.Name}");
    //        // There is more here, like generic arguments, and so on.
    //        System.Console.WriteLine($"{prefix}The Arguments are:");
    //        foreach (var arg in node.Arguments)
    //        {
    //            var argVisitor = Visitor.CreateFromExpression(arg);
    //            argVisitor.Visit(prefix + "\t");
    //        }
    //    }
    //}

    //public class ConditionalVisitor : Visitor
    //{
    //    private readonly ConditionalExpression node;
    //    public ConditionalVisitor(ConditionalExpression node) : base(node)
    //    {
    //        this.node = node;
    //    }

    //    public override void Visit(string prefix)
    //    {
    //        System.Console.WriteLine($"{prefix}This expression is a {NodeType} expression");
    //        var testVisitor = Visitor.CreateFromExpression(node.Test);
    //        System.Console.WriteLine($"{prefix}The Test for this expression is:");
    //        testVisitor.Visit(prefix + "\t");
    //        var trueVisitor = Visitor.CreateFromExpression(node.IfTrue);
    //        System.Console.WriteLine($"{prefix}The True clause for this expression is:");
    //        trueVisitor.Visit(prefix + "\t");
    //        var falseVisitor = Visitor.CreateFromExpression(node.IfFalse);
    //        System.Console.WriteLine($"{prefix}The False clause for this expression is:");
    //        falseVisitor.Visit(prefix + "\t");
    //    }
    //}

    internal sealed class DynamicQuery
    {
        /// <summary>
        /// Gets the insert query.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="item">The item.</param>
        /// <returns>
        /// The Sql query based on the item properties of the anonymous type.
        /// </returns>
        internal static string GetInsertQuery(string tableName, dynamic item)
        {
            PropertyInfo[] props = item.GetType().GetProperties();
            PropertyInfo ChildKey = props.Where(s => s.GetCustomAttributes(false)
                                    .FirstOrDefault(p => p.GetType().Name == "KeyAttribute") != null).FirstOrDefault();
            string[] columns = props.Select(p => p.Name).Where(s => s != ChildKey.Name).ToArray();

            return string.Format("INSERT INTO {0} ({1}) OUTPUT inserted.ID VALUES (@{2})",
                                    tableName,
                                    string.Join(",", columns),
                                    string.Join(",@", columns));
        }

        /// <summary>
        /// Gets the update query.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="item">The item.</param>
        /// <returns>
        /// The Sql query based on the item properties of the anonymous type.
        /// </returns>
        internal static string GetUpdateQuery(string tableName, dynamic item)
        {
            PropertyInfo[] props = item.GetType().GetProperties();
            string[] columns = props.Select(p => p.Name).ToArray();
            PropertyInfo ChildKey = props.Where(s => s.GetCustomAttributes(false)
                                    .FirstOrDefault(p => p.GetType().Name == "KeyAttribute") != null).FirstOrDefault();

            var parameters = columns.Select(name => name + "=:'" + name + "'").ToList();

            var query = string.Format("UPDATE {0} SET {1} WHERE " + ChildKey.Name + "=:" + ChildKey.Name, tableName, string.Join(", ", parameters));
            System.Diagnostics.Debug.Print("QUERY: " + query);
            return query;
        }

        private static dynamic GetConvertedValue(MethodCallExpression methodCallExpression)
        {
            object result = Expression.Lambda(methodCallExpression).Compile().DynamicInvoke();

            return Expression.Constant((object)Expression.Lambda(methodCallExpression).Compile().DynamicInvoke(), methodCallExpression.Type);
        }

        /// <summary>
        /// Gets the dynamic query.
        /// </summary>
        /// <param name="tableName">Name of the table.</param>
        /// <param name="expression">The expression.</param>
        /// <returns>A result object with the generated sql and dynamic params.</returns>
        public static QueryResult GetDynamicQuery<T>(string tableName, Expression<System.Func<T, bool>> expression)
        {
            var queryProperties = new List<QueryParameter>();
            var visitor = new MyExpressionVisitor();
            var body = (BinaryExpression)visitor.Visit(expression.Body);
            IDictionary<string, System.Object> expando = new ExpandoObject();
            var builder = new StringBuilder();

            // walk the tree and build up a list of query parameter objects
            // from the left and right branches of the expression tree
            WalkTree(body, ExpressionType.Default, ref queryProperties);

            // convert the query parms into a SQL string and dynamic property object
            builder.Append("SELECT * FROM ");
            builder.Append(tableName);
            builder.Append(" WHERE ");

            for (int i = 0; i < queryProperties.Count(); i++)
            {
                QueryParameter item = queryProperties[i];

                if (!string.IsNullOrEmpty(item.LinkingOperator) && i > 0)
                {
                    builder.Append($"{item.LinkingOperator} {item.PropertyName} {item.QueryOperator} @{item.PropertyName} ");
                }
                else
                {
                    builder.Append($"{item.PropertyName} {item.QueryOperator} @{item.PropertyName} ");
                }

                expando[item.PropertyName] = item.PropertyValue;
            }

            return new QueryResult(builder.ToString().TrimEnd(), expando);
        }

        /// <summary>
        /// Walks the tree.
        /// </summary>
        /// <param name="body">The body.</param>
        /// <param name="linkingType">Type of the linking.</param>
        /// <param name="queryProperties">The query properties.</param>
        private static void WalkTree(BinaryExpression body, ExpressionType linkingType, ref List<QueryParameter> queryProperties)
        {
            try
            {
                if (body.NodeType != ExpressionType.AndAlso && body.NodeType != ExpressionType.OrElse)
                {
                    string propertyName = GetPropertyName(body);
                    dynamic propertyValue = body.Right;
                    string opr = GetOperator(body.NodeType);
                    string link = GetOperator(linkingType);


                    if (body.Right.NodeType == ExpressionType.Call)
                    {
                        propertyValue = GetConvertedValue((MethodCallExpression)body.Right);
                    }

                    queryProperties.Add(new QueryParameter(link, propertyName, propertyValue.Value, opr));
                }
                else
                {
                    WalkTree((BinaryExpression)body.Left, body.NodeType, ref queryProperties);

                    if (body.Right.NodeType == ExpressionType.Call)
                    {
                        object result = Expression.Lambda((MethodCallExpression)body.Right).Compile().DynamicInvoke();
                        //propertyValue = GetConvertedValue((MethodCallExpression)body.Right);
                    }


                    WalkTree((BinaryExpression)body.Right, body.NodeType, ref queryProperties);
                }
            }
            catch (System.Exception ex)
            {
                string err = ex.ToString();
            }
        }

        /// <summary>
        /// Gets the name of the property.
        /// </summary>
        /// <param name="body">The body.</param>
        /// <returns>The property name for the property expression.</returns>
        private static string GetPropertyName(BinaryExpression body)
        {
            string propertyName = body.Left.ToString().Split(new char[] { '.' })[1];

            if (body.Left.NodeType == ExpressionType.Convert)
            {
                // hack to remove the trailing ) when convering.
                propertyName = propertyName.Replace(")", string.Empty);
            }

            return propertyName;
        }

        /// <summary>
        /// Gets the operator.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>
        /// The expression types SQL server equivalent operator.
        /// </returns>
        /// <exception cref="System.NotImplementedException"></exception>
        private static string GetOperator(ExpressionType type)
        {
            switch (type)
            {
                case ExpressionType.Equal:
                    return "=";
                case ExpressionType.NotEqual:
                    return "!=";
                case ExpressionType.LessThan:
                    return "<";
                case ExpressionType.GreaterThan:
                    return ">";
                case ExpressionType.AndAlso:
                case ExpressionType.And:
                    return "AND";
                case ExpressionType.Or:
                case ExpressionType.OrElse:
                    return "OR";
                case ExpressionType.Default:
                    return string.Empty;
                default:
                    throw new System.NotImplementedException();
            }
        }

        /// <summary>
        /// Gets the where query.
        /// </summary>
        /// <param name="item">The item.</param>
        /// <returns>The where clause of a query from an anonymous type.</returns>
        internal static string GetWhereQuery(dynamic item)
        {
            PropertyInfo[] props = item.GetType().GetProperties();
            string[] columns = props.Select(p => p.Name).ToArray();

            var builder = new StringBuilder();

            for (int i = 0; i < columns.Count(); i++)
            {
                string col = columns[i];
                builder.Append(col);
                builder.Append("=@");
                builder.Append(col);

                if (i < columns.Count() - 1)
                {
                    builder.Append(" AND ");
                }
            }

            return builder.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Entity"></param>
        /// <param name="Property"></param>
        /// <returns></returns>
        public dynamic GetPropertyValue(dynamic Entity, string Property)
        {
            dynamic value = Entity.GetType().GetProperty(Property).GetValue(Entity);

            if (Entity.GetType().GetProperty(Property).PropertyType.Name.ToLower() == "string")
            {
                value = value?.Trim();
            }

            return value;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Entity"></param>
        /// <param name="ParentKeyValue"></param>
        /// <param name="Dictionary"></param>
        public void MapEntityToDictionary(dynamic Entity, dynamic ParentKeyValue, ref Dictionary<string, dynamic> Dictionary)
        {
            System.Type type = Entity.GetType();

            string keyProperty = type.GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute))).Name;

            string keyDictionary = (ParentKeyValue != null ? ParentKeyValue + "-" : null) + GetPropertyValue(Entity, keyProperty).ToString();

            if (!Dictionary.TryGetValue(keyDictionary, out dynamic tResultQuery))
            {
                Dictionary.Add(keyDictionary, Entity);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="Entity"></param>
        /// <param name="ResultList"></param>
        public void MapEntityToList<TResult>(dynamic Entity, ref List<TResult> ResultList)
        {
            try
            {
                System.Type type = Entity.GetType();
                string keyProperty = "";

                if (type == typeof(TResult))
                {
                    keyProperty = type.GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute)))?.Name;

                    if (!ResultList.Any(m => m.GetType().GetProperty(keyProperty).GetValue(m).ToString().Trim() == GetPropertyValue(Entity, keyProperty).ToString().Trim()))
                    {
                        ResultList.Add(Entity);
                    }
                }
                else
                {
                    keyProperty = type.GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(ForeignKeyAttribute))).Name;

                    var keyValue = GetPropertyValue(Entity, keyProperty);

                    if (keyValue != null)
                    {
                        keyValue = keyValue.ToString().Trim();
                        var parent = ResultList.FirstOrDefault(m => m.GetType().GetProperty(keyProperty).GetValue(m).ToString().Trim() == GetPropertyValue(Entity, keyProperty).ToString().Trim());
                        var propertyChild = parent.GetType().GetProperty(type.Name);
                        dynamic childValue = propertyChild.GetValue(parent);

                        if (childValue == null)
                        {
                            var collectionType = typeof(List<>);
                            var constructedCollectionType = collectionType.MakeGenericType(type);
                            childValue = System.Activator.CreateInstance(constructedCollectionType);
                            childValue.Add(Entity);

                            parent.GetType().GetProperty(type.Name).SetValue(parent, childValue, null);
                        }
                        else
                        {
                            keyProperty = type.GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute))).Name;
                            bool agregar = true;

                            foreach (var item in childValue)
                            {
                                if (item.GetType().GetProperty(keyProperty).GetValue(item).ToString().Trim() == GetPropertyValue(Entity, keyProperty).ToString().Trim())
                                {
                                    agregar = false;
                                    break;
                                }
                            }
                            if (agregar)
                            {
                                childValue.Add(Entity);
                                parent.GetType().GetProperty(type.Name).SetValue(parent, childValue, null);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                string err = ex.ToString();
            }
        }

        private bool prueba(dynamic tipo)
        {
            bool res = true;

            return res;
        }

        public IEnumerable<dynamic> GetInstanceOf(IEnumerable<KeyValuePair<string, dynamic>> Data)
        {
            dynamic tResultList = null;
            dynamic tResultEntry;
            PropertyInfo[] propertiesList;
            dynamic dato;
            dynamic PK = "";
            try
            {
                if (Data.Any())
                {
                    //tResultList = Activator.CreateInstance(Data.FirstOrDefault().Value.GetType());
                    var collectionType = typeof(List<>);
                    var constructedCollectionType = collectionType.MakeGenericType(Data.FirstOrDefault().Value.GetType());
                    tResultList = System.Activator.CreateInstance(constructedCollectionType);

                    foreach (KeyValuePair<string, dynamic> dataElement in Data)
                    {

                        //tResultList = instance;


                        //tResultEntry = Activator.CreateInstanceFrom(Assembly.GetExecutingAssembly().CodeBase, dataElement.Value.GetType().Name);
                        tResultEntry = System.Activator.CreateInstance(dataElement.Value.GetType());
                        //tResultEntry = dataElement.Value;
                        propertiesList = dataElement.Value.GetType().GetProperties();

                        foreach (PropertyInfo property in propertiesList)
                        {
                            if (property.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute)))
                            {
                                //Soy llave primaria                        
                                PK = property.GetValue(dataElement.Value);
                                if (PK.GetType().Name.ToLower() == "string")
                                {
                                    PK = PK.Trim();
                                }
                            }
                            // Valida si la propiedad es virtual 
                            // (Si es virtual indica que esa propiedad hace referencia a una Entidad relacionada)
                            if (!property.GetMethod.IsVirtual)
                            {
                                // Obtiene información alojada en la propiedad
                                // (Si es string hace TRIM debido a que muchas veces los datos vienen con espacios innecesarios)
                                dato = dataElement.Value.GetType().GetProperty(property.Name).GetValue(dataElement.Value);

                                if (property.PropertyType.Name.ToLower() == "string")
                                {
                                    dato = dato?.Trim();
                                }

                                //Asigna el valor obtenido a la propiedad relacionada
                                tResultEntry.GetType().GetProperty(property.Name).SetValue(tResultEntry, dato, null);
                            }
                            else
                            {
                                //string PKParent = property.PropertyType.GetGenericArguments().FirstOrDefault().GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute))).Name;
                                //string PKChild = property.PropertyType.GetGenericArguments().FirstOrDefault().GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(ForeignKeyAttribute))).Name;

                                var re = Data.Where(m => m.Key.Contains(PK + "-"));
                                if (re != null && re.Any())
                                {
                                    var tFirstList = GetInstanceOf(re);
                                    if (tFirstList != null && tFirstList.Any())
                                    {
                                        Data = Data.Where(m => !re.Contains(m));
                                        tResultEntry.GetType().GetProperty(property.Name).SetValue(tResultEntry, tFirstList, null);
                                    }
                                }
                            }
                        }

                        tResultList.Add(tResultEntry);
                    }
                }
            }
            catch (System.Exception ex)
            {
                string err = ex.ToString();
            }

            return tResultList;
        }

        public IEnumerable<dynamic> GetInstanceOf2(IEnumerable<KeyValuePair<string, dynamic>> Data)
        {
            dynamic tResultList = null;
            dynamic tResultEntry;
            PropertyInfo[] propertiesList;
            dynamic dato;
            dynamic PK = "";
            try
            {
                if (Data.Any())
                {
                    //tResultList = Activator.CreateInstance(Data.FirstOrDefault().Value.GetType());
                    var collectionType = typeof(List<>);
                    var constructedCollectionType = collectionType.MakeGenericType(Data.FirstOrDefault().Value.GetType());
                    tResultList = System.Activator.CreateInstance(constructedCollectionType);

                    foreach (KeyValuePair<string, dynamic> dataElement in Data)
                    {

                        //tResultList = instance;


                        //tResultEntry = Activator.CreateInstanceFrom(Assembly.GetExecutingAssembly().CodeBase, dataElement.Value.GetType().Name);
                        tResultEntry = System.Activator.CreateInstance(dataElement.Value.GetType());
                        //tResultEntry = dataElement.Value;
                        propertiesList = dataElement.Value.GetType().GetProperties();

                        foreach (PropertyInfo property in propertiesList)
                        {
                            if (property.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute)))
                            {
                                //Soy llave primaria                        
                                PK = property.GetValue(dataElement.Value);
                                if (PK.GetType().Name.ToLower() == "string")
                                {
                                    PK = PK.Trim();
                                }
                            }
                            // Valida si la propiedad es virtual 
                            // (Si es virtual indica que esa propiedad hace referencia a una Entidad relacionada)
                            if (!property.GetMethod.IsVirtual)
                            {
                                // Obtiene información alojada en la propiedad
                                // (Si es string hace TRIM debido a que muchas veces los datos vienen con espacios innecesarios)
                                dato = dataElement.Value.GetType().GetProperty(property.Name).GetValue(dataElement.Value);

                                if (property.PropertyType.Name.ToLower() == "string")
                                {
                                    dato = dato?.Trim();
                                }

                                //Asigna el valor obtenido a la propiedad relacionada
                                tResultEntry.GetType().GetProperty(property.Name).SetValue(tResultEntry, dato, null);
                            }
                            else
                            {
                                //string PKParent = property.PropertyType.GetGenericArguments().FirstOrDefault().GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute))).Name;
                                //string PKChild = property.PropertyType.GetGenericArguments().FirstOrDefault().GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(ForeignKeyAttribute))).Name;

                                var re = Data.Where(m => m.Key.Contains(PK + "-"));
                                if (re != null && re.Any())
                                {
                                    var tFirstList = GetInstanceOf(re);
                                    if (tFirstList != null && tFirstList.Any())
                                    {
                                        Data = Data.Where(m => !re.Contains(m));
                                        tResultEntry.GetType().GetProperty(property.Name).SetValue(tResultEntry, tFirstList, null);
                                    }
                                }
                            }
                        }

                        tResultList.Add(tResultEntry);
                    }
                }
            }
            catch (System.Exception ex)
            {
                string err = ex.ToString();
            }

            return tResultList;
        }
    }

    /// <summary>
    /// Class that models the data structure in coverting the expression tree into SQL and Params.
    /// </summary>
    internal class QueryParameter
    {
        public string LinkingOperator { get; set; }
        public string PropertyName { get; set; }
        public object PropertyValue { get; set; }
        public string QueryOperator { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryParameter" /> class.
        /// </summary>
        /// <param name="linkingOperator">The linking operator.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="propertyValue">The property value.</param>
        /// <param name="queryOperator">The query operator.</param>
        internal QueryParameter(string linkingOperator, string propertyName, object propertyValue, string queryOperator)
        {
            this.LinkingOperator = linkingOperator;
            this.PropertyName = propertyName;
            this.PropertyValue = propertyValue;
            this.QueryOperator = queryOperator;
        }
    }

    /// <summary>
    /// A result object with the generated sql and dynamic params.
    /// </summary>
    public class QueryResult
    {
        /// <summary>
        /// The _result
        /// </summary>
        private readonly System.Tuple<string, dynamic> _result;

        /// <summary>
        /// Gets the SQL.
        /// </summary>
        /// <value>
        /// The SQL.
        /// </value>
        public string Sql
        {
            get
            {
                return _result.Item1;
            }
        }

        /// <summary>
        /// Gets the param.
        /// </summary>
        /// <value>
        /// The param.
        /// </value>
        public dynamic Param
        {
            get
            {
                return _result.Item2;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="QueryResult" /> class.
        /// </summary>
        /// <param name="sql">The SQL.</param>
        /// <param name="param">The param.</param>
        public QueryResult(string sql, dynamic param)
        {
            _result = new System.Tuple<string, dynamic>(sql, param);
        }
    }
}
