﻿using Dapper;
using IBM.Data.DB2.iSeries;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace RequisicionesCore.Repository
{
    public class DB2Repository<T> : System.IDisposable, IRepository<T> where T : class
    {
        private readonly IDbConnection dCon = new iDB2Connection(ConfigurationManager.ConnectionStrings["ConexionDB2"].ToString());
        private StringBuilder query;
        private DynamicQuery dynamicQuery;
        private IDbTransaction idbt = null;
        private string library;

        /// <summary>
        /// Instancia el datacontext a partir de una conexión IDbConnection
        /// </summary>
        /// <param name="_dCon">Sobreescribe la conexión por defecto</param>
        public DB2Repository(iDB2Connection _dCon = null)
        {
            this.dCon = _dCon ?? dCon;
            dynamicQuery = new DynamicQuery();
        }

        public string Add(List<T> entity)
        {
            string res = "";
            //string Sql = DynamicQuery.GetInsertQuery(typeof(T).Name, entity);
            query = new StringBuilder();
            query.AppendFormat("INSERT INTO {0}", typeof(T).Name);
            query.Append(" (");
            string sep = "";
            PropertyInfo ChildKey = entity.First().GetType().GetProperties().Where(s => s.GetCustomAttributes(false)
                                    .FirstOrDefault(p => p.GetType().Name == "KeyAttribute") != null).FirstOrDefault();
            foreach (PropertyInfo item in entity.First().GetType().GetProperties())
            {
                if (item.Name != ChildKey.Name)
                {
                    query.AppendFormat("{0} {1}", sep, item.Name);
                    if (sep == "")
                    {
                        sep = ",";
                    }
                }
            }
            query.Append(")");
            query.Append(" Values(");
            sep = "";
            foreach (var item in entity)
            {
                //if (item.GetType() != ChildKey.Name)
                //{
                query.AppendFormat("{0} {1}", sep, item);
                if (sep == "")
                {
                    sep = ",";
                }
                //}
            }
            query.Append(" )");
            try
            {
                res = dCon.Execute(query.ToString(), entity).ToString();
            }
            catch (System.Exception ex)
            {
                res = "ERROR: " + ex.ToString();
            }

            return res;
        }

        public string Add(T entity)
        {
            //string Sql = DynamicQuery.GetInsertQuery(typeof(T).Name, entity);
            //var affectedRows = dCon.Execute(Sql, (object)entity);
            string res = "";

            try
            {

                System.Globalization.CultureInfo r = new System.Globalization.CultureInfo("es-CO");
                r.NumberFormat.CurrencyDecimalSeparator = ".";
                r.NumberFormat.CurrencyGroupSeparator = ",";
                r.NumberFormat.NumberDecimalSeparator = ".";
                r.NumberFormat.NumberGroupSeparator = ",";
                r.DateTimeFormat.ShortDatePattern = "yyyy-MM-dd";
                r.DateTimeFormat.ShortTimePattern = "HH:mm:ss";
                System.Threading.Thread.CurrentThread.CurrentCulture = r;

                query = new StringBuilder();
                string entityName = !string.IsNullOrEmpty(this.library) ? this.library + "." + typeof(T).Name : typeof(T).Name;

                PropertyInfo primaryKey = entity.GetType().GetProperties().Where(s => s.GetCustomAttributes(false)
                                        .FirstOrDefault(p => p.GetType().Name == "KeyAttribute") != null).FirstOrDefault();

                var valorQuery = "*";

                if (primaryKey != null)
                {
                    valorQuery = primaryKey.Name;
                }

                query.Append($"SELECT {valorQuery} FROM FINAL TABLE(INSERT INTO { entityName }");
                string sep = "";
                string propsClause = " (";
                string valuesClause = " Values(";



                foreach (PropertyInfo item in entity.GetType().GetProperties())
                {

                    bool isTimestamp = item.GetCustomAttributes(false).Any(ca => ca.GetType().Name == "TimestampAttribute");

                    //!isTimestamp && 

                    if (item.GetValue(entity) != null && !item.GetMethod.IsVirtual)
                    {
                        if (item.Name != primaryKey?.Name ||
                            (primaryKey?.GetValue(entity) != null && primaryKey?.GetValue(entity).ToString() != "0"))
                        {
                            propsClause += $"{sep} {item.Name}";
                            if (item.Name == "C_XML")
                            {
                                valuesClause += $"{sep} BINARY(X'{item.GetValue(entity)}')";
                            }
                            else
                            {
                                if (item.PropertyType.Name == "String")
                                {
                                    valuesClause += $"{sep} '{item.GetValue(entity)}'";
                                }
                                else
                                {
                                    if (item.PropertyType.GenericTypeArguments.Length > 0)
                                    {
                                        if (item.PropertyType.GenericTypeArguments[0].Name == "DateTime")
                                        {
                                            if (isTimestamp)
                                            {
                                                valuesClause += $"{sep} '{System.DateTime.Parse(item.GetValue(entity).ToString()).ToString("yyyy-MM-dd-HH.mm.ss.ffffff")}'";
                                            }
                                            else
                                            {
                                                valuesClause += $"{sep} '{System.DateTime.Parse(item.GetValue(entity).ToString()).ToString("yyyy-MM-dd")}'";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        valuesClause += $"{sep} {item.GetValue(entity)}";
                                    }
                                }
                            }

                            if (sep == "")
                            {
                                sep = ",";
                            }
                        }
                    }
                }

                propsClause += ")";
                valuesClause += ")";

                query.Append(propsClause);
                query.Append(valuesClause);
                query.Append(")"); // Cierra el select final

                var freshEntity = dCon.Query<T>(query.ToString()).FirstOrDefault();

                // Realiza el intercambio de datos con los datos frescos
                var freshPropertiesDic = freshEntity.GetType().GetProperties().AsList().ToDictionary(p => p.Name, p => p.GetValue(freshEntity));

                if (primaryKey != null)
                {
                    System.Diagnostics.Debug.Print($"Nuevo ID: {freshEntity.GetType().GetProperty(primaryKey.Name).GetValue(freshEntity)}");
                    primaryKey?.SetValue(entity, freshPropertiesDic[primaryKey.Name]);
                }

                res = "OK";
            }
            catch (iDB2Exception ex)
            {
                // throw new Exception($"Error al guardar la entidad de tipo { typeof(T).Name }", ex);
                // throw ex;
                res = "ERROR: " + ex.ToString();
            }

            return res;
        }

        //public string Add(T entity)
        //{            
        //    string res = "";

        //    try
        //    {
        //        query = new StringBuilder();
        //        query.AppendFormat("INSERT INTO {0}", typeof(T).Name);
        //        string sep = "";
        //        string propsClause = " (";
        //        string valuesClause = " Values(";

        //        PropertyInfo ChildKey = entity.GetType().GetProperties().Where(s => s.GetCustomAttributes(false)
        //                                .FirstOrDefault(p => p.GetType().Name == "KeyAttribute") != null).FirstOrDefault();

        //        foreach (PropertyInfo item in entity.GetType().GetProperties())
        //        {
        //            bool isTimestamp = item.GetCustomAttributes(false).Any(ca => ca.GetType().Name == "TimestampAttribute");

        //            if (!isTimestamp && item.GetValue(entity) != null && !item.GetMethod.IsVirtual)
        //            {
        //                if (item.Name != ChildKey?.Name ||
        //                    (ChildKey?.GetValue(entity) != null && ChildKey?.GetValue(entity).ToString() != "0"))
        //                {
        //                    propsClause += $"{sep} {item.Name}";
        //                    if (item.Name == "C_XML")
        //                    {
        //                        valuesClause += $"{sep} BINARY(X'{item.GetValue(entity)}')";
        //                    }
        //                    else
        //                    {
        //                        valuesClause += $"{sep} '{item.GetValue(entity)}'";
        //                    }

        //                    if (sep == "")
        //                    {
        //                        sep = ",";
        //                    }
        //                }
        //            }
        //        }

        //        propsClause += ")";
        //        valuesClause += ")";

        //        query.Append(propsClause);
        //        query.Append(valuesClause);

        //        res = dCon.Execute(query.ToString(), entity).ToString();
        //        dynamic id = null;

        //        if (ChildKey?.PropertyType.Name == "Int32")
        //        {
        //            id = dCon.Query<int>("SELECT IDENTITY_VAL_LOCAL() FROM SYSIBM.SYSDUMMY1").FirstOrDefault();
        //        }
        //        else
        //        {
        //            id = dCon.Query<string>("SELECT IDENTITY_VAL_LOCAL() FROM SYSIBM.SYSDUMMY1").FirstOrDefault();
        //        }

        //        ChildKey?.SetValue(entity, id);

        //        res = "OK";
        //    }
        //    catch (System.Exception ex)
        //    {
        //        res = "ERROR: " + ex.ToString();
        //    }

        //    return res;
        //}

        public string Update(T entity)
        {
            //string Sql = DynamicQuery.GetInsertQuery(typeof(T).Name, entity);
            //var affectedRows = dCon.Execute(Sql, (object)entity);
            string res = "";

            try
            {
                query = new StringBuilder();
                query.AppendFormat("UPDATE {0} SET ", typeof(T).Name);
                string sep = "";
                string propsClause = "";

                PropertyInfo primaryKey = entity.GetType().GetProperties().Where(s => s.GetCustomAttributes(false)
                                        .FirstOrDefault(p => p.GetType().Name == "KeyAttribute") != null).FirstOrDefault();

                foreach (PropertyInfo item in entity.GetType().GetProperties())
                {
                    bool isTimestamp = item.GetCustomAttributes(false).Any(ca => ca.GetType().Name == "TimestampAttribute");
                    if (!isTimestamp && item.GetValue(entity) != null
                        && !item.GetMethod.IsVirtual
                        && (!item.GetCustomAttributes(false).Any(ca => ca.GetType().Name == "EditableAttribute")
                        || item.GetCustomAttributes(false).Any(ca => ca.GetType().Name == "EditableAttribute" && (bool)ca.GetType().GetProperty("AllowEdit").GetValue(ca))))

                    //if (!isTimestamp && item.GetValue(entity) != null && !item.GetMethod.IsVirtual)
                    {
                        if (item.Name != primaryKey?.Name)
                        {
                            if (item.Name == "FECHA_INGRESO")
                            {
                                string err = "";
                            }
                            if (item.GetType().Name == "DateTime")
                            {

                            }


                            propsClause += $"{sep} {item.Name}='{item.GetValue(entity)}'";

                            if (sep == "")
                            {
                                sep = ",";
                            }
                        }
                    }
                }

                query.Append(propsClause);
                query.Append($" WHERE {primaryKey?.Name}='{primaryKey.GetValue(entity)}'");
                System.Diagnostics.Debug.Print(query.ToString());
                res = dCon.Execute(query.ToString(), entity).ToString();
                res = "OK";
            }
            catch (System.Exception ex)
            {
                res = "ERROR: " + ex.ToString() + ((ex.InnerException != null) ? ex.InnerException.ToString() : "");
            }

            return res;
        }

        public string Delete(string Where = null)
        {
            string res = "";
            query = new StringBuilder();
            query.AppendFormat("DELETE FROM {0}", typeof(T).Name);

            if (Where != null)
            {
                if (Where.Trim() != "")
                {
                    query.Append(" WHERE ");
                    query.Append(Where);
                }
            }

            try
            {
                dCon.Execute(query.ToString());
                res = "OK";
            }
            catch (System.Exception ex)
            {
                res = "ERROR: " + ex.ToString();
            }

            return res;
            //db.GetTable<T>().DeleteOnSubmit(entity);
            //db.SubmitChanges();
        }

        public IEnumerable<T> ExecuteQuery(string query, bool isolationLevel = false)
        {
            if (isolationLevel)
            {
                dCon.Open();
                idbt = dCon.BeginTransaction(IsolationLevel.RepeatableRead);
            }

            return dCon.Query<T>(query, idbt);
        }

        public void CommitQuery()
        {
            idbt.Commit();
            dCon.Close();
        }

        public string ExecutePlainQuery(string query)
        {
            string result = "OK";

            try
            {
                dCon.Execute(query);
            }
            catch (iDB2Exception ex)
            {
                result = ex.ToString();
            }
            catch (System.Exception ex)
            {
                result = ex.ToString();
            }

            return result;
        }

        public IEnumerable<TResult> ExecuteQuery<TResult, TFirst>(string query, bool fromParent = true)
        {
            //query = "SELECT * FROM RCAAPP A JOIN RCAAP M ON M.AAPP = A.AAPP WHERE A.AAPP = 'BRINSAAP'";
            System.Diagnostics.Debug.WriteLine(query);

            List<TResult> listaTResult = new List<TResult>();

            var PKTResult = typeof(TResult).GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute)))?.Name;
            var PKTFirst = typeof(TFirst).GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute)))?.Name;

            if (PKTResult == null || PKTFirst == null)
                throw new System.Exception("No se han podido obtener las llaves primarias de las entidades especificadas");

            string splitOn = fromParent ? PKTResult : $"{PKTFirst}";

            System.Diagnostics.Debug.Print("splitOn: " + splitOn);

            dCon.Query<TResult, TFirst, TResult>(
                query, (tResult, tFirst) =>
                {

                    dynamicQuery.MapEntityToList<TResult>(tResult, ref listaTResult);
                    dynamicQuery.MapEntityToList<TResult>(tFirst, ref listaTResult);

                    return tResult;
                },
                splitOn: splitOn);

            return listaTResult;
        }

        //public IEnumerable<TResult> ExecuteQuery<TResult, TFirst, TSecond>(string query)
        //{
        //    //query = "SELECT * FROM RCAAPP A JOIN RCAAP M ON M.AAPP = A.AAPP WHERE A.AAPP = 'BRINSAAP'";

        //    List<TResult> listaTResult = new List<TResult>();
        //    List<TFirst> listaTFirst = new List<TFirst>();

        //    var PKTResult = typeof(TResult).GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute)))?.Name;
        //    var PKTFirst = typeof(TFirst).GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute)))?.Name;
        //    var PKTSecond = typeof(TSecond).GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute)))?.Name;

        //    System.Diagnostics.Debug.Print(PKTResult);
        //    System.Diagnostics.Debug.Print(PKTFirst);

        //    if (PKTResult == null || PKTFirst == null)
        //        throw new Exception("No se han podido obtener las llaves primarias de las entidades especificadas");

        //    dCon.Query<TResult, TFirst, TSecond, TResult>(
        //        query, (tResult, tFirst, tSecond) =>
        //        {

        //            dynamicQuery.MapEntityToList<TResult>(tResult, ref listaTResult);
        //            dynamicQuery.MapEntityToList<TResult>(tFirst, ref listaTResult);
        //            dynamicQuery.MapEntityToList<TFirst>(tSecond, ref listaTFirst);

        //            return tResult;
        //        },
        //        splitOn: $"{PKTResult}");

        //    return listaTResult;
        //}






























        //public IEnumerable<Entidades.RCAAPP> ExecuteQuery(string query)
        //{
        //    string sql = "SELECT * FROM RCAAPP A JOIN RCAAP M ON M.AAPP = A.AAPP WHERE A.AAPP = 'BRINSAAP'";

        //    var orderDictionary = new Dictionary<string, Entidades.RCAAPP>();
        //    Entidades.RCAAP rCAAP = new Entidades.RCAAP();
        //    PropertyInfo ChildKey = rCAAP.GetType().GetProperties()
        //        .Where(s => s.GetCustomAttributes(false).FirstOrDefault(p => p.GetType().Name == "KeyAttribute") != null).FirstOrDefault();

        //    return dCon.Query<Entidades.RCAAPP, Entidades.RCAAP, Entidades.RCAAPP>(
        //        sql, (rcaapp, rcaap) =>
        //        {
        //            if (!orderDictionary.TryGetValue(rcaapp.AAPP, out Entidades.RCAAPP rcaappEntry))
        //            {
        //                rcaappEntry = rcaapp;
        //                rcaappEntry.RCAAP = new List<Entidades.RCAAP>();
        //                orderDictionary.Add(rcaappEntry.AAPP, rcaappEntry);
        //            }
        //            rcaappEntry.RCAAP.Add(rcaap);
        //            return rcaappEntry;
        //        },
        //        splitOn: ChildKey.Name).Distinct();
        //}



        public IEnumerable<TResult> ExecuteQuery2<TResult, TFirst>(string query)
        {
            query = "SELECT * FROM RCAAPP A JOIN RCAAP M ON M.AAPP = A.AAPP WHERE A.AAPP = 'BRINSAAP'";

            var dictionary = new Dictionary<string, dynamic>();

            var PKTResult = typeof(TResult).GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute))).Name;
            var PKTFirst = typeof(TFirst).GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute))).Name;

            dCon.Query<TResult, TFirst, TResult>(
                query, (tResult, tFirst) =>
                {
                    dynamicQuery.MapEntityToDictionary(tResult, null, ref dictionary);
                    dynamicQuery.MapEntityToDictionary(tFirst, dynamicQuery.GetPropertyValue(tResult, PKTResult).ToString(), ref dictionary);

                    return tResult;
                },
                splitOn: PKTFirst);
            var respuesta = dynamicQuery.GetInstanceOf(dictionary.Where(s => s.Value != null)) as List<TResult>;
            return respuesta;
        }



        public IEnumerable<TResult> ExecuteQuery2<TResult, TFirst, TSecond>(string query)
        {
            query = "SELECT * FROM RCAAPP A " +
                "JOIN RCAAP M ON M.AAPP = A.AAPP " +
                "JOIN RCAA AU ON M.AMODULO = AU.AMODULO " +
                "WHERE A.AAPP = 'BRINSAAP'";

            var dictionary = new Dictionary<string, dynamic>();
            //var tuple = new Tuple<>();


            var PKTResult = typeof(TResult).GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute))).Name;
            var PKTFirst = typeof(TFirst).GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute))).Name;
            var PKTSecond = typeof(TSecond).GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute))).Name;

            dCon.Query<TResult, TFirst, TSecond, TResult>(
                query, (tResult, tFirst, tSecond) =>
                {
                    dynamicQuery.MapEntityToDictionary(tResult, null, ref dictionary);
                    dynamicQuery.MapEntityToDictionary(tFirst, dynamicQuery.GetPropertyValue(tResult, PKTResult).ToString(), ref dictionary);
                    dynamicQuery.MapEntityToDictionary(tSecond, dynamicQuery.GetPropertyValue(tFirst, PKTFirst).ToString(), ref dictionary);

                    return tResult;
                },
                splitOn: PKTFirst + "," + PKTSecond);
            var respuesta = dynamicQuery.GetInstanceOf(dictionary.Where(s => s.Value != null)) as List<TResult>;
            return respuesta;
        }

        public IEnumerable<TResult> ExecuteQuery<TResult, TFirst, TSecond>(string query)
        {
            query = "SELECT * FROM RCAAPP A " +
                "INNER JOIN RCAAP M ON M.AAPP = A.AAPP " +
                "INNER JOIN RCAA AU ON M.AMODULO = AU.AMODULO " +
                "WHERE A.AAPP = 'BRINSAAP'";



            query = "SELECT                                     " +
"  MENU.AMENU,                                                  " +
"  MODUSR.AUSR,                                                 " +
"  MODULO.AAPP,                                                 " +
"  MODULO.AMOD                                                  " +
"FROM                                                           " +
"  RCAAP MODULO                                                 " +
"  INNER JOIN RCAM MENU ON MENU.AMODULO = MODULO.AMODULO        " +
"  INNER JOIN RCAA MODUSR ON MODULO.AMODULO = MODUSR.AMODULO    " +
"WHERE                                                          " +
"  MODUSR.AUSR = 'JROBAYO'                                      ";


            List<TResult> listaTResult = new List<TResult>();
            List<TFirst> listaTFirst = new List<TFirst>();

            var PKTResult = typeof(TResult).GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute))).Name;
            var PKTFirst = typeof(TFirst).GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute))).Name;
            var PKTSecond = typeof(TSecond).GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute))).Name;

            dCon.Query<TResult, TFirst, TSecond, TResult>(
                query, (tResult, tFirst, tSecond) =>
                {
                    dynamicQuery.MapEntityToList<TResult>(tResult, ref listaTResult);
                    dynamicQuery.MapEntityToList<TResult>(tFirst, ref listaTResult);

                    var parent = listaTResult.FirstOrDefault(m => m.GetType().GetProperty(PKTResult).GetValue(m).ToString().Trim() == dynamicQuery.GetPropertyValue(tResult, PKTResult));

                    listaTFirst = parent.GetType().GetProperty(typeof(TFirst).Name).GetValue(parent) as List<TFirst>;

                    dynamicQuery.MapEntityToList<TFirst>(tSecond, ref listaTFirst);

                    parent.GetType().GetProperty(typeof(TFirst).Name).SetValue(parent, listaTFirst, null);

                    return tResult;
                },
                splitOn: PKTResult + "," + PKTFirst + "," + PKTResult + "," + PKTSecond);

            return listaTResult;
        }

        public IEnumerable<TResult> ExecuteQuery<TResult, TFirst, TSecond, TThird>(string query)
        {
            query = "SELECT * FROM RCAAPP A " +
               "INNER JOIN RCAAP M ON M.AAPP = A.AAPP " +
               "INNER JOIN RCAA AU ON M.AMODULO = AU.AMODULO " +
               "INNER JOIN RCAM MNU ON M.AMODULO = MNU.AMODULO " +
               "WHERE A.AAPP = 'BRINSAAP'";

            List<TResult> listaTResult = new List<TResult>();
            List<TFirst> listaTFirst = new List<TFirst>();
            List<TSecond> listaTSecond = new List<TSecond>();
            List<TThird> listaTThird = new List<TThird>();

            TResult tResultEntry;
            TFirst tFirstEntry;

            var PKTResult = typeof(TResult).GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute))).Name;
            var PKTFirst = typeof(TFirst).GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute))).Name;
            var PKTSecond = typeof(TSecond).GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute))).Name;
            var PKTThird = typeof(TThird).GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute))).Name;

            dCon.Query<TResult, TFirst, TSecond, TThird, TResult>(
                query, (tResult, tFirst, tSecond, tThird) =>
                {
                    dynamicQuery.MapEntityToList<TResult>(tResult, ref listaTResult);





                    dynamicQuery.MapEntityToList<TFirst>(tFirst, ref listaTFirst);
                    dynamicQuery.MapEntityToList<TSecond>(tSecond, ref listaTSecond);
                    dynamicQuery.MapEntityToList<TThird>(tThird, ref listaTThird);











                    dynamicQuery.MapEntityToList<TResult>(tFirst, ref listaTResult);

                    tResultEntry = listaTResult.FirstOrDefault(m => m.GetType().GetProperty(PKTResult).GetValue(m).ToString().Trim() == dynamicQuery.GetPropertyValue(tResult, PKTResult));
                    listaTFirst = tResultEntry.GetType().GetProperty(typeof(TFirst).Name).GetValue(tResultEntry) as List<TFirst>;
                    dynamicQuery.MapEntityToList<TFirst>(tSecond, ref listaTFirst);
                    tResultEntry.GetType().GetProperty(typeof(TFirst).Name).SetValue(tResultEntry, listaTFirst, null);

                    tFirstEntry = listaTFirst.FirstOrDefault(m => m.GetType().GetProperty(PKTFirst).GetValue(m).ToString().Trim() == dynamicQuery.GetPropertyValue(tFirst, PKTFirst));
                    listaTSecond = tFirstEntry.GetType().GetProperty(typeof(TSecond).Name).GetValue(tFirstEntry) as List<TSecond>;
                    dynamicQuery.MapEntityToList<TSecond>(tThird, ref listaTSecond);
                    tFirstEntry.GetType().GetProperty(typeof(TSecond).Name).SetValue(tFirstEntry, listaTSecond, null);


                    return tResult;
                },
                splitOn: PKTResult + "," + PKTFirst + "," + PKTResult + "," + PKTSecond + "," + PKTThird + "," + PKTFirst);









            return listaTResult;
        }

        public IEnumerable<TResult> ExecuteQuery<TResult, TFirst, TSecond, TThird, TFourth>(string query)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<TResult> ExecuteQuery<TResult, TFirst, TSecond, TThird, TFourth, TFifth>(string query)
        {
            throw new System.NotImplementedException();
        }

        /// <summary>
        /// Devuelve IEnumerable de objeto TResult
        /// </summary>
        /// <typeparam name="TResult">Entidad</typeparam>
        /// <param name="Data"></param>
        /// <returns></returns>
        private IEnumerable<TResult> GetInstanceOf<TResult>(IEnumerable<KeyValuePair<string, dynamic>> Data)
        {
            List<TResult> tResultList = new List<TResult>();
            TResult tResultEntry;
            PropertyInfo[] propertiesList;
            dynamic dato;

            foreach (KeyValuePair<string, dynamic> dataElement in Data)
            {
                tResultEntry = System.Activator.CreateInstance<TResult>();
                propertiesList = dataElement.Value.GetType().GetProperties();

                foreach (PropertyInfo property in propertiesList)
                {
                    // Valida si la propiedad es virtual 
                    // (Si es virtual indica que esa propiedad hace referencia a una Entidad relacionada)
                    if (!property.GetMethod.IsVirtual)
                    {
                        // Obtiene información alojada en la propiedad
                        // (Si es string hace TRIM debido a que muchas veces los datos vienen con espacios innecesarios)
                        dato = dataElement.Value.GetType().GetProperty(property.Name).GetValue(dataElement.Value);

                        if (property.PropertyType.Name.ToLower() == "string")
                        {
                            dato = dato?.Trim();
                        }

                        //Asigna el valor obtenido a la propiedad relacionada
                        tResultEntry.GetType().GetProperty(property.Name).SetValue(tResultEntry, dato, null);
                    }
                    else
                    {
                        //this.GetInstanceOf<property.PropertyType>()
                    }
                }

                tResultList.Add(tResultEntry);
            }

            return tResultList;
        }

        public IEnumerable<T> GetAll()
        {
            query = new StringBuilder();
            query.Append("SELECT *");
            query.AppendFormat(" FROM {0}", typeof(T).Name);

            return dCon.Query<T>(query.ToString());
        }

        public T GetOne(string Where)
        {
            query = new StringBuilder();
            query.Append("SELECT *");
            query.AppendFormat(" FROM {0}", typeof(T).Name);
            query.Append(" WHERE ");
            query.Append(Where);
            return dCon.Query<T>(query.ToString()).SingleOrDefault();
        }

        public IEnumerable<T> Find(Expression<System.Func<T, bool>> predicate, bool isolationLevel = false)
        {
            IDbTransaction idbt = null;
            if (isolationLevel)
            {
                idbt = dCon.BeginTransaction(IsolationLevel.RepeatableRead);
            }

            QueryResult result = DynamicQuery.GetDynamicQuery(typeof(T).Name, predicate);

            return dCon.Query<T>(result.Sql, (object)result.Param, idbt);
            //throw new NotImplementedException();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dCon.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }

        public IEnumerable<TResult> ExecuteQuery<TResult, TFirst, TSecond, TThird, TFourth, TFifth, TSixth>(string query)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<TResult> ExecuteQuery<TResult, TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh>(string query)
        {
            //throw new System.NotImplementedException();

            var dictionary = new Dictionary<string, dynamic>();
            //var tuple = new Tuple<>();


            var PKTResult = typeof(TResult).GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute))).Name;
            var PKTFirst = typeof(TFirst).GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute))).Name;
            var PKTSecond = typeof(TSecond).GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute))).Name;
            var PKTThird = typeof(TThird).GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute))).Name;
            var PKTFourth = typeof(TFourth).GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute))).Name;
            var PKTFifth = typeof(TFifth).GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute))).Name;
            var PKTSixth = typeof(TSixth).GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute))).Name;
            var PKTSeventh = typeof(TSeventh).GetProperties().FirstOrDefault(s => s.GetCustomAttributes(false).Any(p => p.GetType() == typeof(KeyAttribute))).Name;

            dCon.Query<TResult, TFirst, TSecond, TResult>(
                query, (tResult, tFirst, tSecond) =>
                {
                    dynamicQuery.MapEntityToDictionary(tResult, null, ref dictionary);
                    dynamicQuery.MapEntityToDictionary(tFirst, dynamicQuery.GetPropertyValue(tResult, PKTResult).ToString(), ref dictionary);
                    dynamicQuery.MapEntityToDictionary(tSecond, dynamicQuery.GetPropertyValue(tFirst, PKTFirst).ToString(), ref dictionary);

                    return tResult;
                },
                splitOn: PKTFirst + "," + PKTSecond);
            var respuesta = dynamicQuery.GetInstanceOf2(dictionary.Where(s => s.Value != null)) as List<TResult>;
            return respuesta;
        }
    }
}
