﻿Module lib_login

    Public IPServer
    Public AppServer
    Public AppMsg
    Public bRevisaPwd As Boolean
    Public pagLogin
    Public bImgLX
    Public SitioActual
    Public xls_XID

    Public CarpetaSitio

    Public CARPETA_SITE As String



    'BSSLoginUser()
    Function BSSLoginUser(Login, Password, msgErr)
        Dim Ingreso As bssControlIngreso

        Ingreso = New bssControlIngreso
        With Ingreso
            .userLogin = Login
            .userPassword = Password
            BSSLoginUser = .BSSLoginUser()
        End With
        Ingreso = Nothing

    End Function

    'Inicio_lib_login
    Sub Inicio_lib_login()
        Dim rs As ADODB.Recordset
        Dim fSql As String

        'CarpetaSitio = Request.ServerVariables("APPL_PHYSICAL_PATH")
        bImgLX = True
        bRevisaPwd = True

        'LeeParametrosWebConfig()
        'gsConnectionString = mkConnectionString(gsAS400, gsUSER, gsPASS, gsLIB)

        gsLXLONG_CAMPO = "CCNOT1"
        gsLXLONG = "LXLONG"

        fSql = "SELECT CCCODE, " & gsLXLONG_CAMPO & " AS DATO FROM RFPARAM WHERE CCTABL='" & gsLXLONG & "' AND CCSDSC = 'SITIO'"
        rs = ExecuteSQL(fSql)
        Do While Not rs.EOF
            dictSitio.Add(rs("CCCODE").Value, rs("DATO").Value)
            rs.MoveNext()
        Loop
        rs.Close()

        ServerRaiz = dictSitio.Item("SERVER_RAIZ")
        IPServer = dictSitio.Item("SITIO_IP")
        AppServer = dictSitio.Item("SITIO_APPSERVER")
        SitioActual = dictSitio.Item("SITIO_ACTUAL")
        pagLogin = dictSitio.Item("SITIO_PAGLOGIN")

        ' ''If Session("style") = "" Then
        ' ''    Session("style") = getLXParamIni("HOJA_ESTILO", rs)
        ' ''    HOJA_ESTILO = getLXParamIni("HOJA_ESTILO", rs)
        ' ''Else
        ' ''    HOJA_ESTILO = Session("style")
        ' ''End If




    End Sub

    'getLXParamini()
    Function getLXParamIni(prm, rs) As String
        rs.Filter = "CCCODE='" & prm & "'"
        If Not rs.EOF Then
            getLXParamIni = rs("DATO")
        Else
            getLXParamIni = ""
        End If
    End Function


    'setAmbiente()
    Function setAmbiente(Ambiente) As String
        If Session("AMBIENTE") <> "" Then
            Ambiente = Session("AMBIENTE")
        End If
        If Ambiente <> "" Then
            gsLIB = Ambiente
            setSession("LIB", gsLIB)
        End If
        Return gsLIB

    End Function


End Module
