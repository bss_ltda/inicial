﻿Imports System.Web.SessionState

Public Class Global_asax
    Inherits System.Web.HttpApplication

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Se desencadena al iniciar la aplicación
        gsConnectionString = "Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=APLLX;Password=LXAPL;Persist Security Info=True;Convert Date Time To Char=TRUE;Application Name=DFU001;Default Collection=ERPLX834F;Force Translate=0;"
        DB.Open(gsConnectionString)

    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)

        Inicio_lib_login()
        leeSession()

        Dim pair As KeyValuePair(Of String, String)
        For Each pair In dictSession
            Session(pair.Key) = pair.Value
        Next

        CARPETA_SITE = Request.ServerVariables("APPL_PHYSICAL_PATH")
        Session("AMBIENTE") = setAmbiente(Session("AMBIENTE"))
        ' Se desencadena al iniciar la sesión
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Se desencadena al comienzo de cada solicitud

    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Se desencadena al intentar autenticar el uso
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Se desencadena cuando se produce un error
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Se desencadena cuando finaliza la sesión
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Se desencadena cuando finaliza la aplicación
    End Sub

End Class