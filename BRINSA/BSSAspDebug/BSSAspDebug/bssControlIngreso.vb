﻿Public Class bssControlIngreso
    Public userLogin
    Public userPassword
    Public msgErr
    Public userURL

    Private tipoMenu

    'BSSLoginUser()
    Function BSSLoginUser()
        Dim Result, stsUser
        Dim sql, bPerfil400
        Dim rs, rsA As ADODB.Recordset
        Dim i, aCS, aDato, gsConnString, db, verificarPass
        Dim AppDefault

        userPassword = UCase(userPassword)

        If AppServer = "" Then
            rw__("Falta AppServer")
            response_End()
        End If
        userLogin = UCase(TRIM(userLogin))
        gsUserLogin = userLogin


        verificarPass = True
        Result = True

        sql = "SELECT * FROM RCAU "
        sql = sql & " WHERE USTS = 'A' AND UTIPO = 'TM' "
        sql = sql & " AND UUSR = '" & userLogin & "' "
        rs = ExecuteSql(sql)
        If rs.EOF Then
            Result = False
            BSSLoginUser = False
            rs.Close()
            rs = Nothing
            rw__(EnRojo("El usuario no existe"))
            response_End()
            Exit Function
        Else
            bPerfil400 = CInt(rs("UPRF400").Value) = 1
            tipoMenu = rs("UINGR").Value
            If CInt(rs("UTU").Value) >= 100 And CInt(rs("UTU").Value) < 200 Then
                setSession("DesdeBOL", "1")
            End If
        End If
        rs.Close()
        If getSession("DesdeBOL") = "1" Then
            sql = "SELECT * FROM ENLACEUSR INNER JOIN RCAU ON USTS = 'A' AND UTIPO = 'TM' AND UUSRBOL = UPPER(F5USER) "
            sql = sql & " WHERE "
            sql = sql & " UCASE(F5USER) = '" & userLogin & "' "
            If userPassword = "CDA14" Or (gsLIB = "DESLX834F" And userPassword = "PRUEBAS") Then
            Else
                sql = sql & " AND UCASE(F5PASW) = '" & userPassword & "'"
            End If
            'dsp sql
            rs = ExecuteSQL(sql)
            If Not rs.EOF Then
                verificarPass = False
                Result = True
                userLogin = rs("UUSR").Value
                VendedorIndustria(rs("UVEND").Value)
            Else
                msgErr = "Usuario ENLACE incorrecto."
                rs.Close()
                rs = Nothing
                BSSLoginUser = False
                Exit Function
            End If
            rs.Close()
            rs = Nothing
        Else
            If userPassword = "CDA14" Or (gsLIB = "DESLX834F" And userPassword = "PRUEBAS") Then
                verificarPass = False
                Result = True
            Else
                If bPerfil400 Then
                    stsUser = Usuario400(userLogin)
                Else
                    stsUser = "*NO400"
                End If
                Select Case stsUser
                    Case "*NO400"
                        msgErr = "No es usuario 400"
                        Result = True
                    Case "*DISABLED"
                        msgErr = "El usuario esta deshabilitado."
                        Result = False
                    Case "*ENABLED"
                        db = Server_CreateObject("ADODB.Connection")
                        On Error Resume Next
                        db.Open(mkConnectionString(gsAS400, userLogin, userPassword, gsLIB))
                        'dspT err.Description
                        If Err.Number <> 0 Then
                            rw__("Error Conexion")
                            If InStr(1, Err.Description, "CWBSY0001") > 0 Then
                                msgErr = "El usuario del servidor no existe"
                                Result = True
                            ElseIf InStr(1, Err.Description, "CWBSY0002") > 0 Then
                                msgErr = "Password incorrecto.(1)"
                                Result = False
                            Else
                                msgErr = "Error no controlado<br />" & Err.Description
                                Result = False
                            End If
                        Else
                            verificarPass = False
                            Result = True
                        End If
                        On Error GoTo 0
                        If db.State = 1 Then
                            db.Close()
                        End If
                        db = Nothing
                    Case Else
                        msgErr = stsUser
                        Result = False
                End Select
            End If
        End If
        If Not Result Then
            rw__(EnRojo(msgErr))
            response_End()
        Else
            sql = " SELECT * "
            If getSession("DesdeBOL") = "1" Then

                sql = sql & " FROM RCAUEB "
            Else
                sql = sql & " FROM RCAU "
            End If
            sql = sql & " WHERE UTIPO='TM' AND UCASE(UUSR)= '" & userLogin & "' "
            rs = ExecuteSQL(sql)
            'dsp sql
            If Not rs.EOF Then
                sql = " SELECT "
                sql = sql & " ASTS, AAPP, AMOD, ADESC, AUSR, AAPPMNU, AOPCMNU, AURL, "
                sql = sql & " ACC00, ACC01, ACC02, ACC03, ACC04, ACC05, ACC06, ACC07, ACC08, "
                sql = sql & " ACC09, ACC10, ACC11, ACC12, ACC13, ACC14, ACC15, ACC16, ACC17, "
                sql = sql & " ACC18, ACC19, ACC20, AAUT, AUPD "
                sql = sql & " FROM RCAA "
                sql = sql & " WHERE UCASE(AAPP) = '" & UCase(AppServer) & "' "
                sql = sql & " AND AUSR = '000000'"
                rsA = ExecuteSQL(sql & " AND AMOD='" & ModuloPorDefecto() & "' ")
                If rsA.EOF Then
                    rsA.Close()
                    rsA = ExecuteSQL(sql)
                End If
                If rsA.EOF Then
                    msgErr = "Usuario no asignado a la aplicacion. [" & AppServer & "][" & rs("UMOD").Value & "]"
                    Result = False
                ElseIf CStr(rs.Fields("USTS").Value) <> "A" Then
                    msgErr = "Usuario inactivo."
                    Result = False
                ElseIf rsA("ASTS").Value <> "A" Then
                    msgErr = "Aplicacion inactiva."
                    Result = False
                Else
                    If verificarPass Then
                        Result = UCase(rs("UPASS").Value) = userPassword
                        If Not Result Then
                            msgErr = "Password incorrecto.(2)"
                        End If
                    End If
                End If
            Else
                msgErr = "Usuario no existe..."
                Result = False
            End If

            If Result Then
                setSession("UserLogin", userLogin)
                setSession("UserID", rs("UNOM").Value)
                setSession("UserShort", NombreAbr(userLogin, rs("UUSRABR").Value))
                setSession("UserType", rsA("AMOD").Value)
                setSession("GroupID", CInt(rs("UGRP").Value))
                setSession("GroupLogin", rsA("AAPP").Value)
                setSession("GroupName", rsA("ADESC").Value)
                setSession("Perfil", rs("UPERFIL").Value)
                'dspT "utra", rs("UTRA")
                setSession("Transport", IIf(CDbl(rs("UTRA").Value) = 0, "", rs("UTRA").Value))
                setSession("UserEMail", rs("UEMAIL").Value)
                setSession("Planta", Trim(rs("UPLANT").Value))
                setSession("UEN", IIf(rs("UUEN").Value = "", "", rs("UUEN").Value))
                setSession("Bodega", Trim(rs("UWHS").Value))
                UsrQueBods(rs("UBODS").Value)    'Session("Bodegas")
                setSession("SufijoConso", Trim(rs("USFCNS").Value))
                setSession("url_home", Replace(Trim(rsA("AURL").Value), "//", "/"))
                userURL = ServerRaiz & Replace(Trim(rsA("AURL").Value), "//", "/")
                setSession("ServerRaiz", ServerRaiz)
                setSession("AppDefault", Trim(rsA("AURL").Value))
                setSession("IdSesion", CalcConsec("RTNOW", 999999999))
                setSession("LIB", QueLIB(gsConnectionString))
                If Not rs.EOF Then
                    VendedorIndustria(CInt(rs("UVEND").Value))
                End If
                rsA.Close()
                rsA = Nothing
                'Buscar configuracion particular del usuario
                PerfilUsuario()
            End If
            If rs.State = 1 Then 'adStateOpen
                rs.Close()
            End If
            rs = Nothing
        End If
        If Result Then
            sql = "SELECT * FROM ZXUL01 WHERE XUUSER='" & Replace(userLogin, "'", "''") & "'"
            rs = ExecuteSql(sql)
            If Not rs.EOF Then
                setSession("lxGroupID", CStr(rs("XUGRUP").Value))
                setSession("lxTypeID", CStr(rs("XUTYPE").Value))
            End If
            If rs.State = 1 Then  'adStateOpen
                rs.Close()
            End If
            rs = Nothing
        End If

        BSSLoginUser = Result

    End Function


    'Usuario400()
    Function Usuario400(Usr)
        Dim db, CMD, rs, txtCmd, stsUser, fSql

        Usuario400 = ""

        stsUser = chkNombreUsr(Usr)
        If stsUser <> "" Then
            Usuario400 = stsUser
            Exit Function
        End If

        db = Server_CreateObject("ADODB.Connection")
        db.Open(mkConnectionString(gsAS400, gsUSER, gsPASS, gsLIB))
        If db.State = 1 Then
            CMD = Server_CreateObject("ADODB.Command")
            CMD.ActiveConnection = db
            CMD.CommandType = 1 'adCmdText
            txtCmd = "DSPUSRPRF USRPRF(" & Usr & ") OUTPUT(*OUTFILE) OUTFILE(QTEMP/USRPRF)"
            CMD.CommandText = Run4Cmd(txtCmd)
            On Error Resume Next
            CMD.Execute()
            If Err.Number <> 0 Then
                If InStr(Err.Description, "CPF2204") > 0 Then
                    Usuario400 = "*NO400"
                Else
                    rw__(EnRojo(txtCmd))
                    rw__(EnRojo(Err.Description))
                    response_End()
                End If
            End If
            On Error GoTo 0
            CMD = Nothing

            If Usuario400 = "*NO400" Then
                db.Close()
                db = Nothing
                Exit Function
            End If

            fSql = "DELETE FROM RCAU400  "
            fSql = fSql & " WHERE UPUPRF = '" & Usr & "' "
            db.Execute(fSql)

            fSql = "INSERT INTO RCAU400  "
            fSql = fSql & "SELECT * FROM QTEMP.USRPRF "
            db.Execute(fSql)

            fSql = " SELECT UPSTAT FROM RCAU400  "
            fSql = fSql & " WHERE UPUPRF = '" & Usr & "' "
            rs = db.Execute(fSql)
            If Not rs.EOF Then
                Usuario400 = rs("UPSTAT")
            Else
                Usuario400 = "*NO400"
            End If
            rs.Close()
            rs = Nothing
            db.Close()
        End If

        db = Nothing
    End Function

    'chkNombreUsr()
    Function chkNombreUsr(nUser)
        Dim i, Ascii, resultado

        resultado = ""
        Ascii = Asc(Mid(nUser, 1, 1))
        If Ascii = Asc("_") Then
            resultado = "Login No pueden empezar con guion al piso(_)"
        ElseIf Ascii >= Asc("0") And Ascii <= Asc("9") Then
            resultado = "Login No pueden empezar con numero"
        End If

        For i = 1 To Len(nUser)
            Ascii = Asc(Mid(nUser, i, 1))
            If Ascii = Asc("_") Then
            ElseIf Ascii >= Asc("0") And Ascii <= Asc("9") Then
            ElseIf Ascii >= Asc("A") And Ascii <= Asc("Z") Then
            ElseIf Ascii >= Asc("a") And Ascii <= Asc("z") Then
            Else
                resultado = "Login Contiene caracteres no validos. Solo puede contener A-Z, 0-9 y guion al piso(_)"
                Exit For
            End If
        Next

        chkNombreUsr = resultado

    End Function

    'Run4Cmd()
    Public Function Run4Cmd(ctxt)
        On Error Resume Next
        ctxt = "call qsys.qcmdexc('" & ctxt & "'," & _
           Rpad(CStr(Len(ctxt)), 10, "0") & ".00000)"
        Run4Cmd = ctxt
        On Error GoTo 0
    End Function

    'Rpad()
    Function Rpad(X, L, c)
        Dim sl

        sl = Len(TRIM(X))
        If sl < L Then
            Rpad = Repite(L - sl, c) & Trim(X)
        Else
            Rpad = X
        End If
    End Function


    'UsrQueBods()
    Function UsrQueBods(Bods)
        Dim i

        If TRIM(Bods) = "" Then
            Exit Function
        End If

        ' ''Session("Bodegas") = Split(Trim(Replace(Bods, " ", "")), ",")
        ' ''For i = 0 To UBound(Session("Bodegas"))
        ' ''    Session("Bodegas")(i) = TRIM(Session("Bodegas")(i))
        ' ''Next

    End Function


    'PerfilUsuario()
    Sub PerfilUsuario()
        Dim rsA, sql

        sql = "SELECT * FROM RCAA "
        sql = sql & " WHERE AAPP='" & AppServer & "'  "
        sql = sql & " AND AMOD='" & getSession("UserType") & "' "
        sql = sql & " AND AUSR='" & getSession("UserLogin") & "'"
        rsA = ExecuteSql(sql)
        If Not rsA.EOF Then
            If TRIM(rsA("AURL")) <> "" Then
                setSession("url_home", Replace(ServerRaiz & Trim(rsA("AURL")), "//", "/"))
                setSession("AppDefault", Trim(rsA("AURL").Value))
            End If
        End If
        rsA.Close()
        rsA = Nothing

    End Sub

    'BSSChkPwd()
    Function BSSChkPwd(url)
        Dim rs, bChg, s

        bChg = 0
        rs = ExecuteSQL("SELECT CASE WHEN UFECCAD < DATE( NOW())  THEN 'CADUCADA' ELSE 'ACTIVA' END AS ESTADO, RCAU.* FROM RCAU WHERE UUSR='" & getSession("UserLogin") & "'")
        'dsp "SELECT * FROM RCAU WHERE UUSR='" &  Session("UserLogin") & "'"
        If CInt(rs("UDIAS")) <> 0 Then
            If rs("ESTADO") = "CADUCADA" Then
                bChg = 1
            End If
        ElseIf rs("UEMAIL") = "" Then
            bChg = 2
        End If
        rs.Close()
        rs = Nothing

        s = IIf(getSession("style") <> "", "&style=" & getSession("style"), "")

        Select Case bChg
            Case 0
                'response.Redirect( url )
                BSSChkPwd = url
            Case 1
                If bRevisaPwd Then
                    'response.Redirect( ServerRaiz & "/tm/chgpwd.asp?UUSR=" & Session("UserLogin") & "&ret_link=" & url &  s  )
                    BSSChkPwd = (ServerRaiz & "/tm/chgpwd.asp?UUSR=" & getSession("UserLogin") & "&ret_link=" & url & s)
                Else
                    'response.Redirect( ServerRaiz & "/tm/chgmail.asp?UUSR=" & Session("UserLogin") & "&ret_link=" & url & s)
                    BSSChkPwd = (ServerRaiz & "/tm/chgmail.asp?UUSR=" & getSession("UserLogin") & "&ret_link=" & url & s)
                End If
            Case 2
                'response.Redirect( ServerRaiz & "/tm/chgmail.asp?UUSR=" & Session("UserLogin") & "&ret_link=" & url & s)
                BSSChkPwd = (ServerRaiz & "/tm/chgmail.asp?UUSR=" & getSession("UserLogin") & "&ret_link=" & url & s)
        End Select

    End Function

    'ModuloPorDefecto()
    Function ModuloPorDefecto()
        Dim rs, rs1, fSql

        fSql = " UPDATE RCAA SET  ADEFAULT = 0 "
        fSql = fSql & " WHERE  "
        fSql = fSql & " AAPP = '" & UCase(AppServer) & "'  "
        fSql = fSql & " AND AUSR = '" & userLogin & "' "
        ExecuteSql(fSql)

        fSql = " SELECT IFNULL(AMOD, '') AMOD,  UMOD"
        fSql = fSql & " FROM   RCAU LEFT OUTER JOIN RCAA ON "
        fSql = fSql & "              UUSR = AUSR "
        fSql = fSql & "          AND UCASE(AAPP) = '" & UCase(AppServer) & "' "
        fSql = fSql & "          AND AUSR = '" & userLogin & "' "
        fSql = fSql & "          AND ADEFAULT = 1 "
        fSql = fSql & " WHERE  UUSR = '" & userLogin & "' "
        ExecuteSql(fSql)
        rs = ExecuteSql(fSql)
        If Not rs.EOF Then
            If rs("AMOD") <> "" Then
                ModuloPorDefecto = rs("AMOD").Value
            ElseIf TRIM(rs("UMOD")) <> "" Then
                fSql = " UPDATE RCAA SET  ADEFAULT = 1 "
                fSql = fSql & " WHERE  "
                fSql = fSql & " AAPP = '" & UCase(AppServer) & "'  "
                fSql = fSql & " AND AUSR = '" & userLogin & "' "
                fSql = fSql & " AND AMOD = '" & TRIM(rs("UMOD")) & "'"
                ExecuteSql(fSql)
                fSql = "SELECT * FROM RCAA "
                fSql = fSql & " WHERE  "
                fSql = fSql & " AAPP = '" & UCase(AppServer) & "'  "
                fSql = fSql & " AND AUSR = '" & userLogin & "' "
                fSql = fSql & " AND ADEFAULT = 1 "
                rs1 = ExecuteSql(fSql)
                If rs1.EOF Then
                    rs1.Close()
                    fSql = "SELECT * FROM RCAA "
                    fSql = fSql & " WHERE  "
                    fSql = fSql & " AAPP = '" & UCase(AppServer) & "'  "
                    fSql = fSql & " AND AUSR = '" & userLogin & "' "
                    rs1 = ExecuteSql(fSql)
                    If Not rs1.EOF Then
                        fSql = " UPDATE RCAA SET  ADEFAULT = 1 "
                        fSql = fSql & " WHERE  "
                        fSql = fSql & " AAPP = '" & UCase(AppServer) & "'  "
                        fSql = fSql & " AND AUSR = '" & userLogin & "' "
                        fSql = fSql & " AND AMOD = '" & TRIM(rs1("AMOD")) & "'"
                        ExecuteSql(fSql)
                        ModuloPorDefecto = rs1("AMOD").Value
                    End If
                    rs1.Close()
                Else
                    ModuloPorDefecto = rs("UMOD").Value
                End If


            Else
                rw__(EnRojo("Falta modulo por defecto de " & userLogin))
                'response_End
            End If
        End If
        rs.Close()
        rs = Nothing

    End Function

    Function getTipoMenu()
        getTipoMenu = tipoMenu
    End Function

End Class
