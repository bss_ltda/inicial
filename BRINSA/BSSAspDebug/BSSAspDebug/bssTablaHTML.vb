﻿Public Class bssTablaHTML
    Public Datos

    Dim Fila

    Sub AddRow(Titulo, Descripcion)

        Datos(Fila, 0) = Trim(Titulo)
        Datos(Fila, 1) = Trim(Descripcion)
        Fila = Fila + 1

    End Sub

    Function getTabla()
        Dim i, s

        s = ""
        s = s & vbLf & "<table width=""100%"" cellspacing=""0"" cellpadding=""0"">"
        s = s & vbLf & "<tbody>"
        For i = vbLf & 0 To UBound(Datos)
            s = s & vbLf & "<tr><th>" & Datos(i, 0) & "</th><td>" & Datos(i, 1) & "</td></tr>"
        Next
        s = s & vbLf & "</tbody>"
        s = s & vbLf & "</table>"

        getTabla = s

    End Function

    Sub New()
        ReDim Datos(13, 2)
        Fila = 0
    End Sub

End Class
