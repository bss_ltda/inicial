﻿'+-----------------------------------------------------------------------------------------------+'
'|                   CLASE PARA CONSTRUCCION DE MENU                                             |'
'|                   v5.0.1 | 2016-09-07 09:27:04                                                |'
'+-----------------------------------------------------------------------------------------------+'

Public Class bssMenuMaker5

    Public mmUser
    Public mmApp
    Public mmModulo
    Public mmTitulo
    Public userModulo
    Public mnu5
    Public bPrimero
    Public bVerificar
    Public mmAppMsg

    Function Inicializa()

        bVerificar = True
        bPrimero = True
        Inicializa = True
        If Session("UserLogin") = "" Then
            If mmAppMsg = "" Then
                mmAppMsg = "ca.Sesion expiro."
            End If
            Inicializa = False
            Reingresa()
        End If

    End Function


    'CreaMenu()
    Function CreaMenu(Parent)
        Dim sql, rsA
        Dim a, m, i

        setSession("MenuActual", mmModulo)
        Parent = CInt(Parent)

        sql = "SELECT * FROM RCAA "
        sql = sql & " WHERE ASTS = 'A' "
        sql = sql & " AND AAPP='" & mmApp & "'  "
        sql = sql & " AND AMOD='" & mmModulo & "' "
        sql = sql & " AND AUSR = '" & mmUser & "'"
        rsA = ExecuteSQL(sql)
        If rsA.EOF Then
            rsA.Close()
            rsA = Nothing
            mmAppMsg = "1. Usuario no asignado a la aplicaci&oacute;n. [" & mmApp & "][" & mmModulo & "][" & mmUser & "]"
            re__(EnRojo(mmAppMsg))
            Reingresa()
        End If

        If Not IsDBNull(rsA("ASTYLE").Value) Then
            If rsA("ASTYLE").Value <> "" Then
                setSession("Stlye", rsA("ASTYLE").Value)
            End If
        End If

        If Parent = 0 Then
            RegistraAcceso(rsA)
        End If

        For i = 0 To 20
            a = rsA("ACC" & Ceros(i, 2)).Value
            m = m & IIf(a = "", "0", "1")
        Next

        mmTitulo = Trim(rsA("ADESC").Value) & mmTitulo

        rsA.Close()
        rsA = Nothing
        setSession("AppAcc", m)

        mnu5 = ""
        Add_mnu5(vbLf & "<script src=""/Common/menumaker/js/jquery-latest.min.js"" type=""text/javascript""></script>")
        Add_mnu5(vbLf & "<script src=""/Common/menumaker/js/menumaker.min.js"" type=""text/javascript""></script>")
        Add_mnu5(vbLf & "<script src=""/Common/menumaker/js/script.js""></script>")
        Add_mnu5(vbLf & "<link rel=""stylesheet"" href=""/Common/menumaker/css/font-awesome.min.css"">")
        Add_mnu5(vbLf & "<link rel=""stylesheet"" href=""/Common/menumaker/css/bssMenu.css"">" & vbLf)

        Add_mnu5(vbLf & "<div id=""tituloMenu"">" & mmTitulo & "</div>")
        Add_mnu5(vbLf & "<div id=""cssmenu"">")
        Add_mnu5(vbLf & "<ul id=""menu"">" & vbLf)
        Add_mnu5(vbLf & "<li>" & mmTitulo & "</li>")

        CreaMenuModulos()
        CreaMenuHorizontal()

        Add_mnu5(vbLf & "            <li id=""infosesion"">")
        Add_mnu5(vbLf & "                <a href=""#"">")
        Add_mnu5(vbLf & "                    <span>" & MuestraPerfil() & "</span>")
        Add_mnu5(vbLf & "                </a>")
        Add_mnu5(vbLf & "                <ul>")
        Add_mnu5(vbLf & "                    <li>")
        Add_mnu5(vbLf & "                        <a href=""#"">")
        Add_mnu5(vbLf & "                            <strong>" & Session("UserID") & "</strong>")
        Add_mnu5(vbLf & "                        </a>")
        Add_mnu5(vbLf & "                        <div>" & MuestraSesion() & "</div>")
        Add_mnu5(vbLf & "                        <a href=""/logout.asp"">")
        Add_mnu5(vbLf & "                            <strong>Salir</strong>")
        Add_mnu5(vbLf & "                        </a>")
        Add_mnu5(vbLf & "                    </li>")
        Add_mnu5(vbLf & "                </ul>")
        Add_mnu5(vbLf & "            </li>")

        Add_mnu5(vbLf & "</ul>")
        Add_mnu5(vbLf & "</div>")

    End Function


    Sub CreaMenuHorizontal()
        Dim sql, rs, rs1

        sql = " SELECT M.* FROM  RCAM M  "
        sql = sql & " Inner Join RCAAU U On M.AAPP = U.AUAPP And M.AMOD = U.AUMOD AND M.AID = U.AUID"
        sql = sql & " WHERE "
        sql = sql & "      M.AAPP   = '" & mmApp & "' "
        sql = sql & "  AND M.AMOD   = '" & mmModulo & "' "
        sql = sql & "  AND U.AUUSER = '" & mmUser & "' "
        sql = sql & "  AND M.APARENT = M.AID "
        sql = sql & "  ORDER BY M.AORD, M.AID"
        rs = ExecuteSQL(sql)
        Do While Not rs.EOF
            sql = " SELECT COUNT(*) FROM RCAM M"
            sql = sql & " WHERE "
            sql = sql & "      M.AAPP   = '" & mmApp & "' "
            sql = sql & "  AND M.AMOD   = '" & mmModulo & "' "
            sql = sql & "  AND APARENT  = " & rs("AID").Value
            rs1 = ExecuteSQL(sql)
            If rs1(0).Value > 1 Then
                Add_mnu5(vbLf & "<li>")
                Add_mnu5(vbLf & mnu5_li(rs("ALINK").Value, Trim(rs("ATIT").Value), rs("AISET").Value))
                Add_mnu5(vbLf & "<ul>")
                CreaMenuVertical(rs("AID").Value)
                Add_mnu5(vbLf & "</ul>")
                Add_mnu5(vbLf & "</li>")
            Else
                Add_mnu5(vbLf & "<li>" & mnu5_li(rs("ALINK").Value, Trim(rs("ATIT").Value), rs("AISET").Value) & "</li>")
            End If
            rs1.Close()
            rs.MoveNext()
        Loop
        rs.Close()
        rs = Nothing
        rs1 = Nothing

    End Sub

    'CreaMenuVertical()
    Sub CreaMenuVertical(Parent)
        Dim sql, rs, rs1

        sql = " SELECT M.* FROM  RCAM M  "
        sql = sql & " Inner Join RCAAU U On M.AAPP = U.AUAPP And M.AMOD = U.AUMOD AND M.AID = U.AUID"
        sql = sql & " WHERE "
        sql = sql & "      M.AAPP   = '" & mmApp & "' "
        sql = sql & "  AND M.AMOD   = '" & mmModulo & "' "
        sql = sql & "  AND U.AUUSER = '" & mmUser & "' "
        sql = sql & "  AND M.APARENT = " & Parent
        sql = sql & "  AND M.APARENT <> M.AID "
        sql = sql & "  ORDER BY M.APARENT, M.AORD, M.AID"
        rs = ExecuteSQL(sql)
        Do While Not rs.EOF
            'Por ejemplo, si APARM1 = '1,3' quiere decir que los usuario con acceso 1,3 pueden tener la opcion de menu
            If AppPermisoOpc(rs("AID").Value, rs("APARM1").Value) Then
                sql = " SELECT COUNT(*) FROM RCAM M"
                sql = sql & " WHERE "
                sql = sql & "      M.AAPP   = '" & mmApp & "' "
                sql = sql & "  AND M.AMOD   = '" & mmModulo & "' "
                sql = sql & "  AND APARENT  = " & rs("AID").Value
                rs1 = ExecuteSQL(sql)
                If rs1(0).Value > 1 Then
                    Add_mnu5(vbLf & "<li>")
                    Add_mnu5(vbLf & mnu5_li(rs("ALINK").Value, Trim(rs("ATIT").Value), rs("AISET").Value))
                    Add_mnu5(vbLf & "<ul>")
                    CreaMenuVertical(rs("AID").Value)
                    Add_mnu5(vbLf & "</ul>")
                    Add_mnu5(vbLf & "</li>")
                Else
                    Add_mnu5(vbLf & "<li>" & mnu5_li(rs("ALINK").Value, Trim(rs("ATIT").Value), rs("AISET").Value) & "</li>")
                End If
                rs1.Close()
            End If
            rs.MoveNext()
        Loop
        rs.Close()
        rs = Nothing

    End Sub


    'CreaMenuModulos()
    Function CreaMenuModulos()
        Dim sql, rs, rs1

        Add_mnu5(vbLf & "<li id=""Empresa""><a href=""" & Session("url_home") & """><img id=""logoEmpresa""" & _
                                     " alt=""" & getLXParam("SITIO_NOMBRE", "") & """" & _
                                     " src=""" & getLXParam("SITIO_LOGO", "") & """ border=""0"">" & _
                                     "<label id=""inicioEmpresa"">" & getLXParam("SITIO_NOMBRE", "") & "</label></a>")
        Add_mnu5(vbLf & "<ul>")

        sql = "SELECT A.AAPP, A.AMOD, A.ADESC "
        sql = sql & " FROM RCAA A  "
        sql = sql & " WHERE A.ASTS='A' "
        sql = sql & " AND A.AAPP =  '" & mmApp & "'  "
        'sql = sql & " AND A.AUSR =  '000000' "
        sql = sql & " AND A.AUSR =  '" & mmUser & "' "
        sql = sql & " AND A.AMOD <> '" & mmModulo & "'"
        sql = sql & " ORDER BY A.ADESC"
        rs = ExecuteSQL(sql)
        Do While Not rs.EOF
            sql = "SELECT A.AAPP, A.AMOD, A.ADESC, A.AURL, IFNULL(P.ATEXT, '') ATEXT "
            sql = sql & " FROM RCAA A INNER JOIN RCAAP P ON A.AAPP = P.AAPP AND A.AMOD = P.AMOD"
            sql = sql & " WHERE A.ASTS = 'A' "
            sql = sql & " AND A.AAPP = '" & mmApp & "'  "
            sql = sql & " AND A.AMOD = '" & rs("AMOD").Value & "' "
            sql = sql & " AND A.AUSR = '000000'"
            sql = sql & " ORDER BY A.ADESC "
            rs1 = ExecuteSQL(sql)
            If Not rs1.EOF Then
                Add_mnu5(vbLf & "<li>")
                Add_mnu5(vbLf & mnu5_li(ServerRaiz & sinDiagonal(rs1("AURL").Value), Trim(rs1("ADESC").Value), ""))
                Add_mnu5(vbLf & "<div>" & rs1("ATEXT").Value & "</div>")
                Add_mnu5(vbLf & "</li>")
            End If
            rs.MoveNext()
        Loop
        rs.Close()
        rs = Nothing
        rs1 = Nothing

        Add_mnu5(vbLf & "</ul>")
        Add_mnu5(vbLf & "</li>")


    End Function

    ' CreaMenuURL()
    Function CreaMenuURL(link)
        Dim u

        If Trim(link) = "" Then
            u = "#"
        Else
            u = Replace(link, "{ServerRaiz}", ServerRaiz)
            u = Replace(u, "{Logout}", ServerRaiz & "logout.asp")
        End If
        CreaMenuURL = u

    End Function


    'mnu5_li()
    Function mnu5_li(pag, tit, targ)
        Dim s

        s = "<a href=""#""{target}><span>{tit}</span></a>"
        If pag <> "" Then
            s = Replace(s, "#", CreaMenuURL(pag))
        End If
        s = Replace(s, "{tit}", tit)
        If Trim(targ) <> "" Then
            s = Replace(s, "{target}", " target=""_blank""")
        Else
            s = Replace(s, "{target}", "")
        End If
        mnu5_li = s

    End Function


    Private Sub RegistraAcceso(rs)
        Dim fSql, vSql, url

        url = Request_ServerVariables("URL")
        url = Replace(url, "'", "''")
        url = Left(url, 2000)

        fSql = " INSERT INTO RCAASTAT( "
        vSql = " VALUES ( "
        fSql = fSql & " AAPP      , " : vSql = vSql & "'" & rs("AAPP").Value & "', "      '//15A   Aplicacion
        fSql = fSql & " AMOD      , " : vSql = vSql & "'" & rs("AMOD").Value & "', "      '//15A   Modulo
        fSql = fSql & " ADESC     , " : vSql = vSql & "'" & rs("ADESC").Value & "', "      '//60A   Descripcion
        fSql = fSql & " AUSR      , " : vSql = vSql & "'" & mmUser & "', "      '//10A   Usuario
        fSql = fSql & " AIPADDR   , " : vSql = vSql & "'" & Request_ServerVariables("REMOTE_ADDR") & "', "      '//25A   Direccion IP
        fSql = fSql & " AURL      ) " : vSql = vSql & "'" & url & "' ) "      '//2002A URL
        ExecuteSQL(fSql & vSql)

    End Sub


    'AppPermisoOpc()
    Private Function AppPermisoOpc(aId, OpcMnu)
        Dim Result, aPerm, i, n

        Result = True

        If Trim(OpcMnu) <> "" Then
            aPerm = Split(OpcMnu, ",")
            If UBound(aPerm) >= 0 Then
                Result = False
                For i = 0 To UBound(aPerm)
                    n = CInt(Trim(aPerm(i)))
                    If AppAccess(n) Then
                        Result = True
                    End If
                Next
            End If
        End If


        AppPermisoOpc = Result

    End Function

    Sub Add_mnu5(addUrl)
        mnu5 = mnu5 & addUrl
    End Sub

End Class
'|                   FIN CLASE PARA CONSTRUCCION DE MENU                                         |'
'+-----------------------------------------------------------------------------------------------+'
