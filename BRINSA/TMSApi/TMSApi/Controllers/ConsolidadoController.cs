﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TMSApi.Controllers
{
    [RoutePrefix("api/consolidado")]
    public class ConsolidadoController : ApiController
    {
        [HttpGet]
        [Route("")]
        public IHttpActionResult GetConsolidado(string conso) {
            TMSCore.Entities.DTO.ConsolidadoDTO consolidadoDTO = new TMSCore.Entities.DTO.ConsolidadoDTO(conso);
            return Ok(consolidadoDTO);
        }
    }
}
