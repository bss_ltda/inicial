﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace TMSApi.Controllers
{
    [RoutePrefix("api/flujo-consolidado")]
    public class FlujoConsolidadoController : ApiController
    {
        [HttpPut]
        [Route("peso-entrada")]
        public IHttpActionResult UpdatePesoEntrada(TMSCore.Entities.DTO.FlujoConsolidadoRequestDTO flujoConsolidadoRequestDTO)
        {
            if (!ModelState.IsValid){
                return BadRequest(ModelState);
            }
            TMSCore.Helpers.LogHelper logHelper = new TMSCore.Helpers.LogHelper();
            logHelper.GuardaLog("CapturaJson", $"UpdatePesoEntrada", "",0, flujoConsolidadoRequestDTO.Consolidado, flujoConsolidadoRequestDTO);

        }
    }
}
