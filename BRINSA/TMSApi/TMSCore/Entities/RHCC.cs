﻿using System.ComponentModel.DataAnnotations;

namespace TMSCore.Entities
{
    public class RHCC
    {
        private string _HSFCNS;
        private string _HCONSO;
        private string _HPLACA;
        private string _XCNTR;
        private string _HREMOLQ;
        private string _HNPROVE;
        private string _HCHOFE;
        private string _HCELU;
        private string _HORDTRA;
        private string _HMANIF;
        private string _HCONPA;
        private string _HSECLD;
        private string _HPROPI;
        private string _HCENTR;
        private string _HCTRAN;
        private string _HCAUBO;
        private string _HOBSEE;
        private string _HOBSES;
        private string _HOBSER;
        private string _HREPORT;
        private string _HCARG;
        private string _XISOT;
        private string _XSELLV;
        private string _XSELLV2;
        private string _XCI1CON;
        private string _XCI2CON;
        private string _XCI3CON;
        private string _XSECON;
        private string _XS1CAR;
        private string _XS2CAR;
        private string _XS3CAR;
        private string _XS4CAR;
        private string _XS5CAR;
        private string _XS6CAR;
        private string _XS7CAR;
        private string _XS8CAR;
        private string _XS9CAR;
        private string _XS10CAR;
        private string _XS11CAR;
        private string _XS12CAR;
        private string _XCAUDCA;
        private string _XCAUDTR;
        private string _XFLANTI;
        private string _XSEANTI;
        private string _HPLTA;
        private string _HDESTI;
        private string _HMERCA;
        private string _HBOD;
        private string _HCODPOSTAL;
        private string _HCIUDAD;
        private string _HDEPTO;
        private string _HTIPCARG;
        private string _HDIVIS;
        private string _HSEGME;
        private string _HGENERA;
        private string _HPROFLE;
        private string _HPRFLVA;
        private string _HPRCDVA;
        private string _HPRODUCCD;
        private string _HREF01;
        private string _HREF02;
        private string _HREF03;
        private string _HREF04;
        private string _HREF05;
        private string _HREF06;
        private string _HREF07;
        private string _HREF08;
        private string _HREF09;
        private string _HREF10;
        private string _HACTUSR;
        private string _HACTACC;
        private string _HTRAUSR;
        private string _HOWNER;
        private string _HOCCON;
        private string _HMXCLDES;

        [Display(Name = "Tipo Consolidado")]
        public string HSFCNS { get => _HSFCNS?.Trim(); set => _HSFCNS = value; }
        [Display(Name = "Consolidado")]
        public string HCONSO { get => _HCONSO?.Trim(); set => _HCONSO = value; }
        [Display(Name = "Num Consolidado")]
        public double HNUMCONSO { get; set; }
        [Display(Name = "Estado")]
        public double HESTAD { get; set; }
        [Display(Name = "Estado")]
        public double HSTS { get; set; }
        [Display(Name = "F.Estado")]
        [Timestamp]
        public System.DateTime? HFESTAD { get; set; }
        [Display(Name = "Placa")]
        public string HPLACA { get => _HPLACA?.Trim(); set => _HPLACA = value; }
        [Display(Name = "Contenedor")]
        public string XCNTR { get => _XCNTR?.Trim(); set => _XCNTR = value; }
        [Display(Name = "Remolque")]
        public string HREMOLQ { get => _HREMOLQ?.Trim(); set => _HREMOLQ = value; }
        [Display(Name = "Transp")]
        public double HPROVE { get; set; }
        [Display(Name = "Transportador")]
        public string HNPROVE { get => _HNPROVE?.Trim(); set => _HNPROVE = value; }
        [Display(Name = "Transp Programado")]
        public double HTRAPR { get; set; }
        [Display(Name = "Cedula Conductor")]
        public double HCEDUL { get; set; }
        [Display(Name = "Conductor")]
        public string HCHOFE { get => _HCHOFE?.Trim(); set => _HCHOFE = value; }
        [Display(Name = "Celular")]
        public string HCELU { get => _HCELU?.Trim(); set => _HCELU = value; }
        [Display(Name = "Orden Cargue del Transp")]
        public string HORDTRA { get => _HORDTRA?.Trim(); set => _HORDTRA = value; }
        [Display(Name = "Manifiesto del Transp")]
        public string HMANIF { get => _HMANIF?.Trim(); set => _HMANIF = value; }
        [Display(Name = "Preconsolid")]
        public double HPRECO { get; set; }
        [Display(Name = "Orden Preco")]
        public double HORDPR { get; set; }
        [Display(Name = "Consolidado Padre")]
        public string HCONPA { get => _HCONPA?.Trim(); set => _HCONPA = value; }
        [Display(Name = "List Desp")]
        public string HSECLD { get => _HSECLD?.Trim(); set => _HSECLD = value; }
        [Display(Name = "Tipo Vehiculo")]
        public double HTIPO { get; set; }
        [Display(Name = "Cap Veh")]
        public double HCMAXI { get; set; }
        [Display(Name = "Veh Propio")]
        public string HPROPI { get => _HPROPI?.Trim(); set => _HPROPI = value; }
        [Display(Name = "Causal Incump entrega")]
        public string HCENTR { get => _HCENTR?.Trim(); set => _HCENTR = value; }
        [Display(Name = "Causal Incump Trans")]
        public string HCTRAN { get => _HCTRAN?.Trim(); set => _HCTRAN = value; }
        [Display(Name = "Causal Borrado Consol")]
        public string HCAUBO { get => _HCAUBO?.Trim(); set => _HCAUBO = value; }
        [Display(Name = "Nro Entregas")]
        public double HENTRE { get; set; }
        [Display(Name = "Peso Vehiculo Vacio")]
        public double HPEVAC { get; set; }
        [Display(Name = "Peso Veh Salida")]
        public double HTIQUT { get; set; }
        [Display(Name = "Peso Consolidado")]
        public double HPETOT { get; set; }
        [Display(Name = "Observ EnTrada")]
        public string HOBSEE { get => _HOBSEE?.Trim(); set => _HOBSEE = value; }
        [Display(Name = "Observ Salida")]
        public string HOBSES { get => _HOBSES?.Trim(); set => _HOBSES = value; }
        [Display(Name = "Observaciones")]
        public string HOBSER { get => _HOBSER?.Trim(); set => _HOBSER = value; }
        [Display(Name = "Num Reporte Viaje")]
        public double HNUMREP { get; set; }
        [Display(Name = "F.Reporte")]
        [Timestamp]
        public System.DateTime? HFECREP { get; set; }
        [Display(Name = "Ultimo Reporte")]
        public string HREPORT { get => _HREPORT?.Trim(); set => _HREPORT = value; }
        [Display(Name = "Trans Acepta Flete")]
        public string HCARG { get => _HCARG?.Trim(); set => _HCARG = value; }
        [Display(Name = "F.Consolidado")]
        [Timestamp]
        public System.DateTime? HFPROG { get; set; }
        [Display(Name = "F.Cita")]
        [Timestamp]
        public System.DateTime? HCITA { get; set; }
        [Display(Name = "F.Reque Vehiculo")]
        [Timestamp]
        public System.DateTime? HFERECA { get; set; }
        [Display(Name = "F.Estim Llegada")]
        [Timestamp]
        public System.DateTime? HESLLE { get; set; }
        [Display(Name = "F.Llegada")]
        [Timestamp]
        public System.DateTime? HLLEGA { get; set; }
        [Display(Name = "F.Basc Entrada")]
        [Timestamp]
        public System.DateTime? HBASCUI { get; set; }
        [Display(Name = "F.Inicio Cargue")]
        [Timestamp]
        public System.DateTime? HCARGI { get; set; }
        [Display(Name = "F.Fin Cargue")]
        [Timestamp]
        public System.DateTime? HCARGA { get; set; }
        [Display(Name = "F.Docs")]
        [Timestamp]
        public System.DateTime? HFDOCS { get; set; }
        [Display(Name = "F.Basc Salida")]
        [Timestamp]
        public System.DateTime? HBASCUS { get; set; }
        [Display(Name = "F.Cargado en Planta")]
        [Timestamp]
        public System.DateTime? HCARGADO { get; set; }
        [Display(Name = "F.Vigencia Reserva")]
        [Timestamp]
        public System.DateTime? HFEVIRE { get; set; }
        [Display(Name = "F.Enturnado")]
        [Timestamp]
        public System.DateTime? HENTUR { get; set; }
        [Display(Name = "F.Asignacion Transportad")]
        [Timestamp]
        public System.DateTime? HFASIG { get; set; }
        [Display(Name = "F.Salida")]
        [Timestamp]
        public System.DateTime? HSALE { get; set; }
        [Display(Name = "F.Fin Entrega")]
        [Timestamp]
        public System.DateTime? HFINENTREG { get; set; }
        [Display(Name = "F.Salida con Cab")]
        [Timestamp]
        public System.DateTime? HSALECAB { get; set; }
        [Display(Name = "F.Retorno a Planta")]
        [Timestamp]
        public System.DateTime? HRETORNO { get; set; }
        [Display(Name = "Isotanque")]
        public string XISOT { get => _XISOT?.Trim(); set => _XISOT = value; }
        [Display(Name = "SelloSeg Cont Vacio")]
        public string XSELLV { get => _XSELLV?.Trim(); set => _XSELLV = value; }
        [Display(Name = "SelloSeg 2 Cont Vacio")]
        public string XSELLV2 { get => _XSELLV2?.Trim(); set => _XSELLV2 = value; }
        [Display(Name = "Cinta1 ContLleno")]
        public string XCI1CON { get => _XCI1CON?.Trim(); set => _XCI1CON = value; }
        [Display(Name = "Cinta2 ContLleno")]
        public string XCI2CON { get => _XCI2CON?.Trim(); set => _XCI2CON = value; }
        [Display(Name = "Cinta3 ContLleno")]
        public string XCI3CON { get => _XCI3CON?.Trim(); set => _XCI3CON = value; }
        [Display(Name = "Sello ContLleno")]
        public string XSECON { get => _XSECON?.Trim(); set => _XSECON = value; }
        [Display(Name = "Sello1 VehiCarp")]
        public string XS1CAR { get => _XS1CAR?.Trim(); set => _XS1CAR = value; }
        [Display(Name = "Sello2 VehiCarp")]
        public string XS2CAR { get => _XS2CAR?.Trim(); set => _XS2CAR = value; }
        [Display(Name = "Sello3 VCarp")]
        public string XS3CAR { get => _XS3CAR?.Trim(); set => _XS3CAR = value; }
        [Display(Name = "Sello4 VCarp")]
        public string XS4CAR { get => _XS4CAR?.Trim(); set => _XS4CAR = value; }
        [Display(Name = "Sello5 VCarp")]
        public string XS5CAR { get => _XS5CAR?.Trim(); set => _XS5CAR = value; }
        [Display(Name = "Sello6 VCarp")]
        public string XS6CAR { get => _XS6CAR?.Trim(); set => _XS6CAR = value; }
        [Display(Name = "Sello7 VCarp")]
        public string XS7CAR { get => _XS7CAR?.Trim(); set => _XS7CAR = value; }
        [Display(Name = "Sello8 VCarp")]
        public string XS8CAR { get => _XS8CAR?.Trim(); set => _XS8CAR = value; }
        [Display(Name = "Sello9 VCarp")]
        public string XS9CAR { get => _XS9CAR?.Trim(); set => _XS9CAR = value; }
        [Display(Name = "Sello10 VCarp")]
        public string XS10CAR { get => _XS10CAR?.Trim(); set => _XS10CAR = value; }
        [Display(Name = "Sello11 VCarp")]
        public string XS11CAR { get => _XS11CAR?.Trim(); set => _XS11CAR = value; }
        [Display(Name = "Sello12 VCarp")]
        public string XS12CAR { get => _XS12CAR?.Trim(); set => _XS12CAR = value; }
        [Display(Name = "F.Entrega Puerto")]
        [Timestamp]
        public System.DateTime? XFEENP { get; set; }
        [Display(Name = "F.Recogida Contenedor")]
        [Timestamp]
        public System.DateTime? XFERCON { get; set; }
        [Display(Name = "F.Ingreso Puerto/Bode")]
        [Timestamp]
        public System.DateTime? XFEINPU { get; set; }
        [Display(Name = "Causal Demora Cargue")]
        public string XCAUDCA { get => _XCAUDCA?.Trim(); set => _XCAUDCA = value; }
        [Display(Name = "Causal Demora Transi")]
        public string XCAUDTR { get => _XCAUDTR?.Trim(); set => _XCAUDTR = value; }
        [Display(Name = "F.Antinarcoticos")]
        public string XFLANTI { get => _XFLANTI?.Trim(); set => _XFLANTI = value; }
        [Display(Name = "F.Antinarcoticos")]
        [Timestamp]
        public System.DateTime? XFEANTI { get; set; }
        [Display(Name = "Sello Antinarcoticos")]
        public string XSEANTI { get => _XSEANTI?.Trim(); set => _XSEANTI = value; }
        [Display(Name = "ID. Demoras")]
        public double HIDDEMO { get; set; }
        [Display(Name = "Planta")]
        public string HPLTA { get => _HPLTA?.Trim(); set => _HPLTA = value; }
        [Display(Name = "Zona")]
        public double HZONA { get; set; }
        [Display(Name = "Destino")]
        public string HDESTI { get => _HDESTI?.Trim(); set => _HDESTI = value; }
        [Display(Name = "Mercado")]
        public string HMERCA { get => _HMERCA?.Trim(); set => _HMERCA = value; }
        [Display(Name = "Bodega")]
        public string HBOD { get => _HBOD?.Trim(); set => _HBOD = value; }
        [Display(Name = "Cod Postal")]
        public string HCODPOSTAL { get => _HCODPOSTAL?.Trim(); set => _HCODPOSTAL = value; }
        [Display(Name = "Ciudad")]
        public string HCIUDAD { get => _HCIUDAD?.Trim(); set => _HCIUDAD = value; }
        [Display(Name = "Depto")]
        public string HDEPTO { get => _HDEPTO?.Trim(); set => _HDEPTO = value; }
        [Display(Name = "Con Mix")]
        public double HMIX { get; set; }
        [Display(Name = "Tipo Carga")]
        public string HTIPCARG { get => _HTIPCARG?.Trim(); set => _HTIPCARG = value; }
        [Display(Name = "Tipo Carga")]
        public string HDIVIS { get => _HDIVIS?.Trim(); set => _HDIVIS = value; }
        [Display(Name = "Segmento")]
        public string HSEGME { get => _HSEGME?.Trim(); set => _HSEGME = value; }
        [Display(Name = "A Granel")]
        public double HGRANEL { get; set; }
        [Display(Name = "Orden Compra Fletes")]
        public double HOCFLE { get; set; }
        [Display(Name = "Periodo Fletes")]
        public double HPER { get; set; }
        [Display(Name = "Generada OC S/N")]
        public string HGENERA { get => _HGENERA?.Trim(); set => _HGENERA = value; }
        [Display(Name = "Num Tarifa Flete")]
        public double HNUMTRF { get; set; }
        [Display(Name = "Num Tarifa Flete")]
        public double HTARDSCM { get; set; }
        [Display(Name = "Tarifa Resolucion")]
        public double HPEEST { get; set; }
        [Display(Name = "Toneladas a Pagar")]
        public double HTONPAG { get; set; }
        [Display(Name = "Toneladas Orden")]
        public double HTONVAC { get; set; }
        [Display(Name = "Tarifa Salida")]
        public double HVALMAN { get; set; }
        [Display(Name = "Tarifa Orden")]
        public double HVALVAC { get; set; }
        [Display(Name = "Vlr Adicional")]
        public double HVALVAM { get; set; }
        [Display(Name = "Periodo")]
        public double HVALREP { get; set; }
        [Display(Name = "Trasbordo")]
        public double HVALREM { get; set; }
        [Display(Name = "Valor Total Flete")]
        public double HVALTOF { get; set; }
        [Display(Name = "Quinta Entrega")]
        public double HVALTOV { get; set; }
        [Display(Name = "Tipo Final Cargue CENDIS")]
        public string HPROFLE { get => _HPROFLE?.Trim(); set => _HPROFLE = value; }
        [Display(Name = "Flag de Cargue Cendis")]
        public string HPRFLVA { get => _HPRFLVA?.Trim(); set => _HPRFLVA = value; }
        [Display(Name = "Tip Cargue CENDIS")]
        public string HPRCDVA { get => _HPRCDVA?.Trim(); set => _HPRCDVA = value; }
        [Display(Name = "Valor CrossDocking")]
        public double HVALORCD { get; set; }
        [Display(Name = "Valor Final Cargue CENDIS")]
        public double HVALVACCD { get; set; }
        [Display(Name = "Indicador de Flete Negociado")]
        public string HPRODUCCD { get => _HPRODUCCD?.Trim(); set => _HPRODUCCD = value; }
        [Display(Name = "Muelle")]
        public string HREF01 { get => _HREF01?.Trim(); set => _HREF01 = value; }
        [Display(Name = "Tipo Carroceria")]
        public string HREF02 { get => _HREF02?.Trim(); set => _HREF02 = value; }
        [Display(Name = "Num CrossD")]
        public string HREF03 { get => _HREF03?.Trim(); set => _HREF03 = value; }
        [Display(Name = "Docs Generados")]
        public string HREF04 { get => _HREF04?.Trim(); set => _HREF04 = value; }
        [Display(Name = "Bodega")]
        public string HREF05 { get => _HREF05?.Trim(); set => _HREF05 = value; }
        [Display(Name = "Campo 06")]
        public string HREF06 { get => _HREF06?.Trim(); set => _HREF06 = value; }
        [Display(Name = "Campo 07")]
        public string HREF07 { get => _HREF07?.Trim(); set => _HREF07 = value; }
        [Display(Name = "Campo 08")]
        public string HREF08 { get => _HREF08?.Trim(); set => _HREF08 = value; }
        [Display(Name = "Campo 09")]
        public string HREF09 { get => _HREF09?.Trim(); set => _HREF09 = value; }
        [Display(Name = "Campo 10")]
        public string HREF10 { get => _HREF10?.Trim(); set => _HREF10 = value; }
        [Display(Name = "Doc Ref 01")]
        public double HDOCREF01 { get; set; }
        [Display(Name = "Doc Ref 02")]
        public double HDOCREF02 { get; set; }
        [Display(Name = "Doc Ref 03")]
        public double HDOCREF03 { get; set; }
        [Display(Name = "Fecha 01")]
        [Timestamp]
        public System.DateTime? HFEC01 { get; set; }
        [Display(Name = "F.Confirm")]
        [Timestamp]
        public System.DateTime? HFEC02 { get; set; }
        [Display(Name = "F.Factura")]
        [Timestamp]
        public System.DateTime? HFEC03 { get; set; }
        [Display(Name = "Fecha 04")]
        [Timestamp]
        public System.DateTime? HFEC04 { get; set; }
        [Display(Name = "Fecha 05")]
        [Timestamp]
        public System.DateTime? HFEC05 { get; set; }
        [Display(Name = "Fecha 06")]
        [Timestamp]
        public System.DateTime? HFEC06 { get; set; }
        [Display(Name = "Fecha 07")]
        [Timestamp]
        public System.DateTime? HFEC07 { get; set; }
        [Display(Name = "Fecha 08")]
        [Timestamp]
        public System.DateTime? HFEC08 { get; set; }
        [Display(Name = "Fecha 09")]
        [Timestamp]
        public System.DateTime? HFEC09 { get; set; }
        [Display(Name = "Fecha 10")]
        [Timestamp]
        public System.DateTime? HFEC10 { get; set; }
        [Display(Name = "Fec/Hor Act")]
        [Timestamp]
        public System.DateTime? HACTDAT { get; set; }
        [Display(Name = "User UPD")]
        public string HACTUSR { get => _HACTUSR?.Trim(); set => _HACTUSR = value; }
        [Display(Name = "Ultima Acc")]
        public string HACTACC { get => _HACTACC?.Trim(); set => _HACTACC = value; }
        [Display(Name = "User Transp")]
        public string HTRAUSR { get => _HTRAUSR?.Trim(); set => _HTRAUSR = value; }
        [Display(Name = "Actualiz Transp")]
        [Timestamp]
        public System.DateTime? HTRAUPD { get; set; }
        [Display(Name = "Num Trabajo")]
        public double HWRKID { get; set; }
        [Display(Name = "Wrk ID 2")]
        public double HWRKID2 { get; set; }
        [Display(Name = "Flag")]
        public double HFLAG1 { get; set; }
        [Display(Name = "Flag")]
        public double HFLAG2 { get; set; }
        [Display(Name = "Sts 01")]
        public double HSTS1 { get; set; }
        [Display(Name = "Sts 02")]
        public double HSTS2 { get; set; }
        [Display(Name = "Sts 03")]
        public double HSTS3 { get; set; }
        [Display(Name = "Sts 04")]
        public double HSTS4 { get; set; }
        [Display(Name = "Sts 05")]
        public double HSTS5 { get; set; }
        [Display(Name = "Propietario Doc")]
        public string HOWNER { get => _HOWNER?.Trim(); set => _HOWNER = value; }
        [Display(Name = "Transp 2")]
        public double HPROVE2 { get; set; }
        [Display(Name = "Transp 3")]
        public double HPROVE3 { get; set; }
        [Display(Name = "Guia Despacho Trans")]
        public string HOCCON { get => _HOCCON?.Trim(); set => _HOCCON = value; }
        [Display(Name = "Max.Pto Envio")]
        public double HMAXPED { get; set; }
        [Display(Name = "Max.Cliente")]
        public double HMAXCLI { get; set; }
        [Display(Name = "Max.Cliente Descrip")]
        public string HMXCLDES { get => _HMXCLDES?.Trim(); set => _HMXCLDES = value; }
        [Display(Name = "Total Clientes")]
        public double HTOTCLI { get; set; }
        [Display(Name = "Total Pedidos")]
        public double HTOTPED { get; set; }
        [Display(Name = "Valor 1")]
        public double HVAL01 { get; set; }
        [Display(Name = "Valor 2")]
        public double HVAL02 { get; set; }
        [Display(Name = "Valor 3")]
        public double HVAL03 { get; set; }
        [Display(Name = "Valor 4")]
        public double HVAL04 { get; set; }
        [Display(Name = "Valor 5")]
        public double HVAL05 { get; set; }
        [Display(Name = "UDF01")]
        public double HUDF01 { get; set; }
        [Display(Name = "UDF02")]
        public double HUDF02 { get; set; }
        [Display(Name = "UDF03")]
        public double HUDF03 { get; set; }
        [Display(Name = "UDF04")]
        public double HUDF04 { get; set; }
        [Display(Name = "UDF05")]
        public double HUDF05 { get; set; }
        [Display(Name = "UDS01")]
        public double HUDS01 { get; set; }
        [Display(Name = "UDS02")]
        public double HUDS02 { get; set; }
        [Display(Name = "UDS03")]
        public double HUDS03 { get; set; }
        [Display(Name = "UDN01")]
        public double HUDN01 { get; set; }
        [Display(Name = "UDN02")]
        public double HUDN02 { get; set; }
        [Display(Name = "UDN03")]
        public double HUDN03 { get; set; }
        [Display(Name = "UDP01")]
        public double HUDP01 { get; set; }
        [Display(Name = "UDP02")]
        public double HUDP02 { get; set; }
        [Display(Name = "UDP03")]
        public double HUDP03 { get; set; }
        [Display(Name = "Numero Unico")]
        public double HNUMREG { get; set; }
    }
}
