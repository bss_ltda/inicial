﻿using System.ComponentModel.DataAnnotations;
using TMSCore.Modules;

namespace TMSCore.Entities.DTO
{
    public class BodegaDTO
    {
        [Display(Name = "BOD")]
        public string BOD { get; set; }
        [Display(Name = "BODFLETES")]
        public string BODFLETES { get; set; }
        [Display(Name = "CATEG01")]
        public string CATEG01 { get; set; }
        [Display(Name = "GRUPO01")]
        public string GRUPO01 { get; set; }
        [Display(Name = "GRUPO02")]
        public string GRUPO02 { get; set; }
        [Display(Name = "GRUPO03")]
        public string GRUPO03 { get; set; }
        [Display(Name = "OPERACION")]
        public string OPERACION { get; set; }
        [Display(Name = "OP_BOD_SAT")]
        public string OP_BOD_SAT { get; set; }
        [Display(Name = "OP_ODT")]
        public string OP_ODT { get; set; }
        [Display(Name = "CONBASCULA")]
        public string CONBASCULA { get; set; }
        [Display(Name = "EXTERNA")]
        public string EXTERNA { get; set; }
        [Display(Name = "TRANSITO")]
        public string TRANSITO { get; set; }
        [Display(Name = "CARGA_BETANIA")]
        public string CARGA_BETANIA { get; set; }
        [Display(Name = "Traslados entre Bod")]
        public string TRASLADOS { get; set; }
        [Display(Name = "Description")]
        public string LDESC { get; set; }
        [Display(Name = "Address Line 1")]
        public string LADD1 { get; set; }
        [Display(Name = "Warehouse Postal Code")]
        public string LPOAS { get; set; }
    }
}