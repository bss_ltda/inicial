﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMSCore.Entities.DTO
{
    public class FlujoConsolidadoRequestDTO
    {
        [Required]
        public string Consolidado { get; set; }
        [Required]
        public string Evento { get; set; }
        public double PesoEntrada { get; set; }
        public double PesoSalida { get; set; }
        public short Muelle { get; set; }
    }
}
