﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMSCore.Modules;

namespace TMSCore.Entities.DTO
{
    public class ConsolidadoDTO
    {
        public BodegaDTO Bodega;
        public string TipoConsolidado { get; set; }
        public ConsolidadoDTO(String conso)
        {
            ConsolidadoModule consolidadoModule = new ConsolidadoModule();
            BodegaModule bodegaModule = new BodegaModule();
            var RHCC = consolidadoModule.GetByPredicate(m => m.HCONSO == conso).SingleOrDefault();
            Bodega = bodegaModule.GetBodega(RHCC.HBOD);
        }
    }
}
