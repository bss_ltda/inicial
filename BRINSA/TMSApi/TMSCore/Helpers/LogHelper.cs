﻿using System.IO;
using System.Reflection;
using System.Text;
using System.Web.Script.Serialization;

namespace TMSCore.Helpers
{
    public class LogHelper
    {
        private readonly string appPath = System.Web.Hosting.HostingEnvironment.MapPath("~/") + "\\Log\\";
        Modules.LogModule logModule = new Modules.LogModule();
        private StreamWriter sw;
        private StringBuilder sb;
        private StringBuilder Error = new StringBuilder();

        /// <summary>
        /// directory exists
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="create">if set to <c>true</c> [create].</param>
        /// <returns>
        /// System.Boolean
        /// </returns>
        public static bool DirectoryExists(string name, bool create = false)
        {
            if (!Directory.Exists(name))
            {
                if (create)
                    Directory.CreateDirectory(name);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Guarda txt en carpeta log de instalación
        /// </summary>
        /// <param name="err">Descripción del error</param>
        /// <param name="proceso">Proceso en el que ocurrio el error</param>
        /// <remarks>Autor: JASC Fecha: 2018-06-06</remarks>
        public void CapturaLog(string err, string proceso)
        {
            DirectoryExists(appPath, true);
            sb = new StringBuilder();
            sw = new StreamWriter(appPath + "\\" + proceso + System.DateTime.Now.ToString("ddMMyyyyHHmmss") + ".txt");
            sb.AppendLine(proceso);
            sb.AppendLine(err);
            sb.AppendLine(System.DateTime.Now.ToString());
            sw.WriteLine(sb.ToString());
            sw.Close();
        }

        public void GuardaLog(string OPERACION, string EVENTO, string TXTSQL, int ALERT, string KEY, dynamic request = null)
        {
            try
            {
                Encoding encoding = Encoding.UTF8;
                string json = "";

                if (request != null)
                {
                    json = new JavaScriptSerializer().Serialize(request);
                }

                var logBytes = encoding.GetBytes(json);

                var ensamblado = Assembly.GetExecutingAssembly();
                System.Console.WriteLine("USUARIO: " + System.Net.Dns.GetHostName());
                System.Console.WriteLine("OPERACION: " + OPERACION);
                System.Console.WriteLine("PROGRAMA: " + ensamblado.GetName().Name + "- V" + ensamblado.GetName().Version.ToString());
                System.Console.WriteLine("EVENTO: " + EVENTO);
                System.Console.WriteLine("TXTSQL: " + TXTSQL);
                System.Console.WriteLine("ALERT: " + ALERT);
                System.Console.WriteLine("LKEY: " + KEY);

                Entities.RFLOG nRFLOG = new Entities.RFLOG()
                {
                    USUARIO = System.Net.Dns.GetHostName(),
                    OPERACION = OPERACION,
                    PROGRAMA = ensamblado.GetName().Name + "- V" + ensamblado.GetName().Version.ToString(),
                    EVENTO = EVENTO.Replace("'", "''"),
                    TXTSQL = TXTSQL.Replace("'", "''"),
                    ALERT = ALERT,
                    LKEY = KEY.ToUpper(),
                    OBJETOENTRA = request != null ? System.BitConverter.ToString(logBytes).Replace("-", "") : null,
                    FECHA = System.DateTime.Now
                };

                var logbytesDictionary = new System.Collections.Generic.Dictionary<string, byte[]>
                {
                    { "OBJETOENTRA", logBytes }
                };

                logModule.Add(nRFLOG, logbytesDictionary);
            }
            catch (System.Exception ex)
            {
                Error.AppendLine(ex.ToString());
                System.Console.WriteLine("ERROR: " + Error);
                CapturaLog(Error.ToString(), "GuardaLog " + KEY);
            }
        }
    }
}
