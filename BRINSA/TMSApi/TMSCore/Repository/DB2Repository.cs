﻿using Dapper;
using IBM.Data.DB2.iSeries;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace TMSCore.Repository
{
    public class DB2Repository<T> : System.IDisposable, IRepository<T> where T : class
    {
        private readonly IDbConnection dCon = new iDB2Connection(ConfigurationManager.ConnectionStrings["ConexionDB2"].ToString());
        private StringBuilder query;
        private DynamicQuery dynamicQuery;
        private IDbTransaction idbt = null;
        private string library;
        List<Task<string>> tasks = new List<Task<string>>();
        Task task;

        /// <summary>
        /// Instancia el datacontext a partir de una conexión IDbConnection
        /// </summary>
        /// <param name="_dCon">Sobreescribe la conexión por defecto</param>
        public DB2Repository(iDB2Connection _dCon = null)
        {
            this.dCon = _dCon ?? dCon;
            dynamicQuery = new DynamicQuery();
        }

        private int ExecuteListQueryInsert(List<string> listQuery)
        {
            if (this.dCon.State == ConnectionState.Closed)
            {
                this.dCon.Open();
            }

            IDbTransaction idbt = this.dCon.BeginTransaction(IsolationLevel.ReadCommitted);
            IDbCommand cmm = new iDB2Command();
            cmm.Connection = this.dCon;
            cmm.Transaction = idbt;

            //TMSCore.Modules.TablaParamModule tablaParam = new TMSCore.Modules.TablaParamModule();
            //Entities.RFPARAM paramNomina = tablaParam.GetByPredicate(m => m.CCTABL == "PPTOMAKER" && m.CCCODE == "NOMINA" && m.CCCODE2 == "GUARDAR").FirstOrDefault();
            task = new Task(() =>
            {
                Task<string>.Run(() =>
                {
                    foreach (var sql in listQuery)
                    {
                        Parallel.Invoke(
                                () =>
                                {
                                    cmm.CommandText = sql;
                                    cmm.ExecuteNonQuery();

                                    //if (paramNomina != null)
                                    //{
                                    //    paramNomina.CCCODEN += 1;
                                    //    paramNomina.CCNOT1 = "Guardando...";
                                    //    paramNomina.CCUDTS = System.DateTime.Now;

                                    //    tablaParam.Upd(paramNomina);
                                    //}
                                }
                        );
                    }
                }).Wait();

                idbt.Commit();
                if (this.dCon.State == ConnectionState.Open)
                {
                    this.dCon.Close();
                }

                //if (paramNomina != null)
                //{
                //    paramNomina.CCNOT1 = "OK";

                //    tablaParam.Upd(paramNomina);
                //}
            });

            task.Start();
            return task.Id;
        }

        public void Add(List<T> entities)
        {
            string res = "";
            query = new StringBuilder();
            List<string> sqlList = new List<string>();

            string sep = "";
            PropertyInfo primaryKey = entities.First().GetType().GetProperties().Where(s => s.GetCustomAttributes(false)
                                    .FirstOrDefault(p => p.GetType().Name == "KeyAttribute") != null).FirstOrDefault();

            string propsClause;
            string valuesClause;

            foreach (var entity in entities)
            {
                query.Clear();
                sep = "";
                propsClause = "";
                valuesClause = "";

                query.AppendFormat("INSERT INTO {0}", typeof(T).Name);
                propsClause = " (";
                valuesClause = " Values(";

                foreach (PropertyInfo item in entity.GetType().GetProperties())
                {
                    bool isTimestamp = item.GetCustomAttributes(false).Any(ca => ca.GetType().Name == "TimestampAttribute");

                    if (item.GetValue(entity) != null && !item.GetMethod.IsVirtual)
                    {
                        if (item.Name != primaryKey?.Name ||
                            (primaryKey?.GetValue(entity) != null && primaryKey?.GetValue(entity).ToString() != "0"))
                        {
                            propsClause += $"{sep} {item.Name}";

                            if (item.Name == "C_XML")
                            {
                                valuesClause += $"{sep} @C_XML";
                            }
                            else
                            {
                                if (item.PropertyType.Name == "String")
                                {
                                    valuesClause += $"{sep} '{item.GetValue(entity)}'";
                                }
                                else
                                {
                                    if (item.PropertyType.GenericTypeArguments.Length > 0)
                                    {
                                        if (item.PropertyType.GenericTypeArguments[0].Name == "DateTime")
                                        {
                                            if (isTimestamp)
                                            {
                                                valuesClause += $"{sep} '{System.DateTime.Parse(item.GetValue(entity).ToString()).ToString("yyyy-MM-dd-HH.mm.ss.ffffff")}'";
                                            }
                                            else
                                            {
                                                valuesClause += $"{sep} '{System.DateTime.Parse(item.GetValue(entity).ToString()).ToString("yyyy-MM-dd")}'";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        valuesClause += $"{sep} {item.GetValue(entity)}";
                                    }
                                }
                            }

                            if (sep == "")
                            {
                                sep = ",";
                            }
                        }
                    }
                }

                propsClause += ")";
                valuesClause += ")";

                query.Append(propsClause);
                query.Append(valuesClause);

                sqlList.Add(query.ToString());
            }

            res = ExecuteListQueryInsert(sqlList).ToString();
        }

        public string Update2(List<T> entities)
        {
            string res = "";
            query = new StringBuilder();
            List<string> sqlList = new List<string>();

            string sep = "";
            PropertyInfo primaryKey = entities.First().GetType().GetProperties().Where(s => s.GetCustomAttributes(false)
                                    .FirstOrDefault(p => p.GetType().Name == "KeyAttribute") != null).FirstOrDefault();

            string propsClause;

            foreach (var entity in entities)
            {
                query.Clear();
                sep = "";
                propsClause = "";


                //query.Append($"UPDATE {typeof(T).Name}");
                propsClause = $" UPDATE {typeof(T).Name} SET ";
                //valuesClause = " Values(";

                foreach (PropertyInfo item in entity.GetType().GetProperties())
                {
                    bool isTimestamp = item.GetCustomAttributes(false).Any(ca => ca.GetType().Name == "TimestampAttribute");

                    if (item.GetValue(entity) != null && !item.GetMethod.IsVirtual)
                    {
                        if (item.Name != primaryKey?.Name)
                        {
                            if (item.PropertyType.Name == "String")
                            {
                                propsClause += $"{sep} {item.Name} = '{item.GetValue(entity)}'";
                            }
                            else
                            {
                                if (item.PropertyType.GenericTypeArguments.Length > 0)
                                {
                                    if (item.PropertyType.GenericTypeArguments[0].Name == "DateTime")
                                    {
                                        if (isTimestamp)
                                        {
                                            propsClause += $"{sep} {item.Name} = '{System.DateTime.Parse(item.GetValue(entity).ToString()).ToString("yyyy-MM-dd-HH.mm.ss.ffffff")}'";
                                        }
                                        else
                                        {
                                            propsClause += $"{sep} {item.Name} = '{System.DateTime.Parse(item.GetValue(entity).ToString()).ToString("yyyy-MM-dd")}'";
                                        }
                                    }
                                }
                                else
                                {
                                    if (item.PropertyType.Name == "Double")
                                    {
                                        if (double.Parse(item.GetValue(entity).ToString()) != 0)
                                        {
                                            propsClause += $"{sep} {item.Name} = {item.GetValue(entity)}";
                                        }
                                    }
                                }
                            }

                            if (!propsClause.Trim().EndsWith("SET"))
                            {
                                if (sep == "")
                                {
                                    sep = ",";
                                }
                            }
                        }
                    }
                }

                if (!propsClause.Trim().EndsWith("SET"))
                {
                    propsClause += $" WHERE {primaryKey.Name} = {primaryKey.GetValue(entity)} ";

                    query.Append(propsClause);

                    sqlList.Add(query.ToString());
                }
            }
            //TMSCore.Modules.TablaParamModule tablaParam = new TMSCore.Modules.TablaParamModule();
            //Entities.RFPARAM paramNomina = tablaParam.GetByPredicate(m => m.CCTABL == "PPTOMAKER" && m.CCCODE == "NOMINA" && m.CCCODE2 == "GUARDAR").FirstOrDefault();

            //if (paramNomina != null)
            //{
            //    paramNomina.CCCODEN2 = sqlList.Count();

            //    tablaParam.Upd(paramNomina);
            //}

            res = ExecuteListQueryInsert(sqlList).ToString();

            return res;
        }

        public void Add(T entity, Dictionary<string, byte[]> bytesDictionary = null)
        {
            //string res = "";

            //try
            //{
            query = new StringBuilder();
            string entityName = !string.IsNullOrEmpty(this.library) ? this.library + "." + typeof(T).Name : typeof(T).Name;

            PropertyInfo primaryKey = entity.GetType().GetProperties().Where(s => s.GetCustomAttributes(false)
                                    .FirstOrDefault(p => p.GetType().Name == "KeyAttribute") != null).FirstOrDefault();

            var valorQuery = "*";

            if (primaryKey != null)
            {
                valorQuery = primaryKey.Name;
            }

            query.Append($"SELECT {valorQuery} FROM FINAL TABLE(INSERT INTO { entityName }");

            string sep = "";
            string propsClause = " (";
            string valuesClause = " Values(";
            bool isBlob = false;
            bool isTimestamp = false;

            foreach (PropertyInfo item in entity.GetType().GetProperties())
            {
                isTimestamp = item.GetCustomAttributes(false).Any(ca => ca.GetType().Name == "TimestampAttribute");
                isBlob = item.GetCustomAttributes(false).Where(ca => ca.GetType().Name == "DisplayAttribute").Any(d => ((DisplayAttribute)d).GroupName == "BLOB");

                if (item.GetValue(entity) != null && !item.GetMethod.IsVirtual)
                {
                    if (item.Name != primaryKey?.Name ||
                        (primaryKey?.GetValue(entity) != null && primaryKey?.GetValue(entity).ToString() != "0"))
                    {
                        propsClause += $"{sep} {item.Name}";

                        if (isBlob)
                        {
                            if (bytesDictionary.TryGetValue(item.Name, out byte[] bytesItem))
                            {
                                valuesClause += $"{sep} @{item.Name}";
                            }
                        }
                        else
                        {
                            if (item.PropertyType.Name == "String")
                            {
                                valuesClause += $"{sep} '{item.GetValue(entity)}'";
                            }
                            else
                            {
                                if (item.PropertyType.GenericTypeArguments.Length > 0)
                                {
                                    if (item.PropertyType.GenericTypeArguments[0].Name == "DateTime")
                                    {
                                        if (isTimestamp)
                                        {
                                            valuesClause += $"{sep} '{System.DateTime.Parse(item.GetValue(entity).ToString()).ToString("yyyy-MM-dd-HH.mm.ss.ffffff")}'";
                                        }
                                        else
                                        {
                                            valuesClause += $"{sep} '{System.DateTime.Parse(item.GetValue(entity).ToString()).ToString("yyyy-MM-dd")}'";
                                        }
                                    }
                                }
                                else
                                {
                                    valuesClause += $"{sep} {item.GetValue(entity)}";
                                }
                            }
                        }

                        if (sep == "")
                        {
                            sep = ",";
                        }
                    }
                }
            }

            propsClause += ")";
            valuesClause += ")";

            query.Append(propsClause);
            query.Append(valuesClause);
            query.Append(")"); // Cierra el select final

            iDB2Command DB2Command = new iDB2Command(query.ToString(), dCon);

            if (bytesDictionary != null)
            {
                foreach (var item in bytesDictionary)
                {
                    DB2Command.Parameters.Add($"@{item.Key}", iDB2DbType.iDB2Blob, item.Value.Length).Value = item.Value;
                }
            }

            dCon.Open();

            iDB2DataReader DB2DataReader = DB2Command.ExecuteReader();

            dynamic valorId = null;

            if (primaryKey != null)
            {
                while (DB2DataReader.Read())
                {
                    valorId = DB2DataReader.GetFieldValue<dynamic>(0);
                }

                DB2DataReader.Close();

                primaryKey?.SetValue(entity, valorId);
            }

            dCon.Close();
        }

        public void Update(T entity)
        {
            query = new StringBuilder();
            query.AppendFormat("UPDATE {0} SET ", typeof(T).Name);
            string sep = "";
            string propsClause = "";

            PropertyInfo primaryKey = entity.GetType().GetProperties().Where(s => s.GetCustomAttributes(false)
                                    .FirstOrDefault(p => p.GetType().Name == "KeyAttribute") != null).FirstOrDefault();

            foreach (PropertyInfo item in entity.GetType().GetProperties())
            {
                bool isTimestamp = item.GetCustomAttributes(false).Any(ca => ca.GetType().Name == "TimestampAttribute");
                if (!isTimestamp && item.GetValue(entity) != null
                    && !item.GetMethod.IsVirtual
                    && (!item.GetCustomAttributes(false).Any(ca => ca.GetType().Name == "EditableAttribute")
                    || item.GetCustomAttributes(false).Any(ca => ca.GetType().Name == "EditableAttribute" && (bool)ca.GetType().GetProperty("AllowEdit").GetValue(ca))))

                {
                    if (item.Name != primaryKey?.Name)
                    {
                        if (item.Name == "FECHA_INGRESO")
                        {
                            string err = "";
                        }
                        if (item.GetType().Name == "DateTime")
                        {

                        }


                        propsClause += $"{sep} {item.Name}='{item.GetValue(entity)}'";

                        if (sep == "")
                        {
                            sep = ",";
                        }
                    }
                }
            }

            query.Append(propsClause);
            query.Append($" WHERE {primaryKey?.Name}='{primaryKey.GetValue(entity)}'");
            System.Diagnostics.Debug.Print(query.ToString());
            dCon.Execute(query.ToString(), entity).ToString();

        }

        public void Update(List<T> entities)
        {

            query = new StringBuilder();
            query.AppendFormat("UPDATE {0} SET ", typeof(T).Name);
            string sep = "";
            string propsClause = "";

            PropertyInfo primaryKey = entities.First().GetType().GetProperties().Where(s => s.GetCustomAttributes(false)
                                    .FirstOrDefault(p => p.GetType().Name == "KeyAttribute") != null).FirstOrDefault();


        }

        public void Delete(string Where = null)
        {
            query = new StringBuilder();
            query.AppendFormat("DELETE FROM {0}", typeof(T).Name);

            if (Where != null)
            {
                if (Where.Trim() != "")
                {
                    query.Append(" WHERE ");
                    query.Append(Where);
                }
            }

            dCon.Execute(query.ToString());
        }

        public IEnumerable<T> ExecuteQuery(string query)
        {            
            return dCon.Query<T>(query);
        }

        public void ExecutePlainQuery(string query)
        {
            dCon.Execute(query);
        }

        /// <summary>
        /// Devuelve IEnumerable de objeto TResult
        /// </summary>
        /// <typeparam name="TResult">Entidad</typeparam>
        /// <param name="Data"></param>
        /// <returns></returns>
        private IEnumerable<TResult> GetInstanceOf<TResult>(IEnumerable<KeyValuePair<string, dynamic>> Data)
        {
            List<TResult> tResultList = new List<TResult>();
            TResult tResultEntry;
            PropertyInfo[] propertiesList;
            dynamic dato;

            foreach (KeyValuePair<string, dynamic> dataElement in Data)
            {
                tResultEntry = System.Activator.CreateInstance<TResult>();
                propertiesList = dataElement.Value.GetType().GetProperties();

                foreach (PropertyInfo property in propertiesList)
                {
                    // Valida si la propiedad es virtual 
                    // (Si es virtual indica que esa propiedad hace referencia a una Entidad relacionada)
                    if (!property.GetMethod.IsVirtual)
                    {
                        // Obtiene información alojada en la propiedad
                        // (Si es string hace TRIM debido a que muchas veces los datos vienen con espacios innecesarios)
                        dato = dataElement.Value.GetType().GetProperty(property.Name).GetValue(dataElement.Value);

                        if (property.PropertyType.Name.ToLower() == "string")
                        {
                            dato = dato?.Trim();
                        }

                        //Asigna el valor obtenido a la propiedad relacionada
                        tResultEntry.GetType().GetProperty(property.Name).SetValue(tResultEntry, dato, null);
                    }
                    else
                    {
                        //this.GetInstanceOf<property.PropertyType>()
                    }
                }

                tResultList.Add(tResultEntry);
            }

            return tResultList;
        }

        public IEnumerable<T> GetAll()
        {
            query = new StringBuilder();
            query.Append("SELECT *");
            query.AppendFormat(" FROM {0}", typeof(T).Name);

            return dCon.Query<T>(query.ToString());
        }

        public T GetOne(string Where)
        {
            query = new StringBuilder();
            query.Append("SELECT *");
            query.AppendFormat(" FROM {0}", typeof(T).Name);
            query.Append(" WHERE ");
            query.Append(Where);
            return dCon.Query<T>(query.ToString()).SingleOrDefault();
        }

        public IEnumerable<T> GetByPredicate(Expression<System.Func<T, bool>> predicate)
        {
            QueryResult result = DynamicQuery.GetDynamicQuery(typeof(T).Name, predicate);

            return dCon.Query<T>(result.Sql, (object)result.Param);
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dCon.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }

        public IEnumerable<dynamic> DynamicFromSql(string Sql, params object[] parameters)
        {
            using (var cmd = this.dCon.CreateCommand())
            {
                cmd.CommandText = Sql;
                if (cmd.Connection.State != ConnectionState.Open) { cmd.Connection.Open(); }

                foreach (var param in parameters)
                {
                    cmd.Parameters.Add(param);
                }

                using (var dataReader = cmd.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        var row = new ExpandoObject() as IDictionary<string, object>;

                        for (var fieldCount = 0; fieldCount < dataReader.FieldCount; fieldCount++)
                        {
                            row.Add(dataReader.GetName(fieldCount), dataReader[fieldCount]);
                        }

                        if (cmd.Connection.State != ConnectionState.Closed) { cmd.Connection.Close(); }

                        yield return row;
                    }
                }
            }
        }

        public IEnumerable<dynamic> DynamicSqlQuery(string sql)
        {
            //Database database = this.context.Database;

            TypeBuilder builder = CreateTypeBuilder(
                    "MyDynamicAssembly", "MyDynamicModule", "MyDynamicType");

            using (System.Data.IDbCommand command = dCon.CreateCommand())
            {
                try
                {
                    dCon.Open();
                    //database.Connection.Open();
                    command.CommandText = sql;
                    command.CommandTimeout = command.Connection.ConnectionTimeout;

                    using (System.Data.IDataReader reader = command.ExecuteReader())
                    {
                        var schema = reader.GetSchemaTable();

                        //var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

                        foreach (System.Data.DataRow row in schema.Rows)
                        {
                            string name = (string)row["ColumnName"];
                            //var a=row.ItemArray.Select(d=>d.)
                            System.Type type = (System.Type)row["DataType"];

                            if (type != typeof(string) && (bool)row.ItemArray[schema.Columns.IndexOf("AllowDbNull")])
                            {
                                type = typeof(System.Nullable<>).MakeGenericType(type);
                            }

                            CreateAutoImplementedProperty(builder, name, type);
                        }
                    }
                }
                finally
                {
                    dCon.Close();
                    command.Parameters.Clear();
                }
            }

            System.Type resultType = builder.CreateType();

            return dCon.Query(resultType, sql).Cast<dynamic>();
        }

        private static TypeBuilder CreateTypeBuilder(
            string assemblyName, string moduleName, string typeName)
        {
            TypeBuilder typeBuilder = System.AppDomain
                .CurrentDomain
                .DefineDynamicAssembly(new AssemblyName(assemblyName),
                                       AssemblyBuilderAccess.Run)
                .DefineDynamicModule(moduleName)
                .DefineType(typeName, TypeAttributes.Public);
            typeBuilder.DefineDefaultConstructor(MethodAttributes.Public);
            return typeBuilder;
        }

        private static void CreateAutoImplementedProperty(
            TypeBuilder builder, string propertyName, System.Type propertyType)
        {
            const string PrivateFieldPrefix = "m_";
            const string GetterPrefix = "get_";
            const string SetterPrefix = "set_";

            // Generate the field.
            FieldBuilder fieldBuilder = builder.DefineField(
                string.Concat(PrivateFieldPrefix, propertyName),
                              propertyType, FieldAttributes.Private);

            // Generate the property
            PropertyBuilder propertyBuilder = builder.DefineProperty(
                propertyName, System.Reflection.PropertyAttributes.HasDefault, propertyType, null);

            // Property getter and setter attributes.
            MethodAttributes propertyMethodAttributes =
                MethodAttributes.Public | MethodAttributes.SpecialName |
                MethodAttributes.HideBySig;

            // Define the getter method.
            MethodBuilder getterMethod = builder.DefineMethod(
                string.Concat(GetterPrefix, propertyName),
                propertyMethodAttributes, propertyType, System.Type.EmptyTypes);

            // Emit the IL code.
            // ldarg.0
            // ldfld,_field
            // ret
            ILGenerator getterILCode = getterMethod.GetILGenerator();
            getterILCode.Emit(OpCodes.Ldarg_0);
            getterILCode.Emit(OpCodes.Ldfld, fieldBuilder);
            getterILCode.Emit(OpCodes.Ret);

            // Define the setter method.
            MethodBuilder setterMethod = builder.DefineMethod(
                string.Concat(SetterPrefix, propertyName),
                propertyMethodAttributes, null, new System.Type[] { propertyType });

            // Emit the IL code.
            // ldarg.0
            // ldarg.1
            // stfld,_field
            // ret
            ILGenerator setterILCode = setterMethod.GetILGenerator();
            setterILCode.Emit(OpCodes.Ldarg_0);
            setterILCode.Emit(OpCodes.Ldarg_1);
            setterILCode.Emit(OpCodes.Stfld, fieldBuilder);
            setterILCode.Emit(OpCodes.Ret);

            propertyBuilder.SetGetMethod(getterMethod);
            propertyBuilder.SetSetMethod(setterMethod);
        }
    }
}
