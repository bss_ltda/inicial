﻿using System.Collections.Generic;
using System.Linq.Expressions;

namespace TMSCore.Repository
{
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity GetOne(string Where);
        IEnumerable<TEntity> GetAll();
        void Add(TEntity entity, Dictionary<string, byte[]> bytesDictionary = null);
        void Add(List<TEntity> entity);
        void Update(TEntity entity);
        void Update(List<TEntity> entities);
        void Delete(string Where);
        IEnumerable<TEntity> ExecuteQuery(string query);        
        IEnumerable<TEntity> GetByPredicate(Expression<System.Func<TEntity, bool>> predicate);
        IEnumerable<dynamic> DynamicSqlQuery(string sql);
        IEnumerable<dynamic> DynamicFromSql(string Sql, params object[] parameters);
        void ExecutePlainQuery(string query);
    }
}
