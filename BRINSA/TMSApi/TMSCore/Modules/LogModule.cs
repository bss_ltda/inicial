﻿using System.Collections.Generic;

namespace TMSCore.Modules
{
    public class LogModule
    {
        Repository.DB2Repository<Entities.RFLOG> dB2Repository;

        public LogModule()
        {
            this.dB2Repository = new Repository.DB2Repository<Entities.RFLOG>();
        }

        public void Add(Entities.RFLOG log, Dictionary<string, byte[]> bytesDictionary = null)
        {
            if (log is null)
                throw new System.NullReferenceException($"El log especificado es un valor nulo");

            this.dB2Repository.Add(log, bytesDictionary);
        }
    }
}
