﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TMSCore.Modules
{
    class ConsolidadoModule
    {
        Repository.DB2Repository<Entities.RHCC> dB2Repository;
        public ConsolidadoModule()
        {
            this.dB2Repository = new Repository.DB2Repository<Entities.RHCC>();
        }
        public IEnumerable<Entities.RHCC> GetByPredicate(Expression<System.Func<Entities.RHCC, bool>> predicate)
        {
            return dB2Repository.GetByPredicate(predicate);
        }
        public void UpdatePesoEntrada(string conso, double peso)
        {
            StringBuilder query = new StringBuilder();
            query.Append($" UPDATE RHCC SET HPEVAC = {peso} WHERE HCONSO  = '{conso}' ");
            this.dB2Repository.ExecutePlainQuery(query.ToString());

        }
    }
}
