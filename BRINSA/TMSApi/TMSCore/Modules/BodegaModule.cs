﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TMSCore.Modules
{
    class BodegaModule
    {
        Repository.DB2Repository<Entities.DTO.BodegaDTO> dB2Repository;
        public BodegaModule()
        {
            this.dB2Repository = new Repository.DB2Repository<Entities.DTO.BodegaDTO>();
        }
        public Entities.DTO.BodegaDTO GetBodega(String Bodega)
        {
            StringBuilder qwery = new StringBuilder();
            qwery.Append(" Select");
            qwery.Append("     B.BOD,");
            qwery.Append("     B.BODFLETES,");
            qwery.Append("     B.CATEG01,");
            qwery.Append("     B.GRUPO01,");
            qwery.Append("     B.GRUPO02,");
            qwery.Append("     B.GRUPO03,");
            qwery.Append("     B.OPERACION,");
            qwery.Append("     B.OP_BOD_SAT,");
            qwery.Append("     B.OP_ODT,");
            qwery.Append("     B.CONBASCULA,");
            qwery.Append("     B.EXTERNA,");
            qwery.Append("     B.TRANSITO,");
            qwery.Append("     B.CARGA_BETANIA,");
            qwery.Append("     B.TRASLADOS,");
            qwery.Append("     W.LDESC,");
            qwery.Append("     W.LADD1,");
            qwery.Append("     W.LPOAS");
            qwery.Append(" From");
            qwery.Append("     RFFLBODORI B Inner Join");
            qwery.Append("     IWM W On W.LWHS = B.BOD");
            qwery.Append(" Where");
            qwery.Append($"     B.BOD = '{Bodega}'");
            return this.dB2Repository.ExecuteQuery(qwery.ToString()).FirstOrDefault();
        }
    }
}
