﻿Public Class wsCorreoMHT
    Private WithEvents m_timer As New Timers.Timer(30000)

    Public sEstado As String = ""

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Agregue el código aquí para iniciar el servicio. Este método debería poner
        ' en movimiento los elementos para que el servicio pueda funcionar.
        Me.EventLog1.WriteEntry("Inicia")
        m_timer.Enabled = True

    End Sub
    Private Sub m_timer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles m_timer.Elapsed

        Try
            m_timer.Enabled = False
            If Not AbreConexion("") Then
                Exit Sub
            End If
            If RevisaArchivo("RFLOG") Then
                AvisoLog()
            End If
            If RevisaArchivo("RMAILX") Then
                revisaTareasMHT()
            End If
            DB.Close()
        Catch ex As Exception
            DB.WrtSqlError(DB.lastSQL, ex.StackTrace & "<br />" & ex.Message)
            Me.EventLog1.WriteEntry(DB.lastSQL & vbCrLf & ex.Message & vbCrLf & ex.StackTrace)
        End Try
        m_timer.Enabled = True

    End Sub

    Sub Inicializador()
        If Not AbreConexion("") Then
            Exit Sub
        End If
        If RevisaArchivo("RFLOG") Then
            AvisoLog()
        End If
        If RevisaArchivo("RMAILX") Then
            revisaTareasMHT()
        End If

        DB.Close()
    End Sub

    Sub revisaTareasMHT()
        Dim fSql As String
        Dim rs As New ADODB.Recordset
        Dim serverMHT As String
        Dim local As String = IIf(My.Settings.LOCAL.ToUpper = "SI", "CCNOT1", "")
        Dim resultado As String = ""
        Dim Correo As New clsMHT

        DB.gsKeyWords = "revisaTareasMHT (1)"
        serverMHT = DB.getLXParam("SMTPMHTSERVER", local)
        fSql = " SELECT * FROM  RMAILX WHERE LENVIADO = 0"
        DB.OpenRS(rs, fSql)
        Do While Not rs.EOF
            DB.gsKeyWords = "revisaTareasMHT (5)"
            DB.gsKeyWords = "revisaTareasMHT, LID = " & rs("LID").Value
            fSql = " UPDATE RMAILX SET  "
            fSql = fSql & " LUPDDAT = NOW() , "
            fSql = fSql & " LENVIADO = -1,  "
            fSql = fSql & " LMSGERR = 'En proceso'  "
            fSql = fSql & " WHERE LID = " & rs("LID").Value
            DB.ExecuteSQL(fSql)
            Correo = Nothing
            Correo = New clsMHT
            With Correo
                .Inicializa()
                .Modulo = rs("LMOD").Value
                .Asunto = rs("LASUNTO").Value
                .De = IIf(rs("LENVIA").Value = "", rs("LREF").Value, rs("LENVIA").Value)
                .Para = rs("LPARA").Value
                .CC = rs("LCOPIA").Value
                .BCC = rs("LBCC").Value

                .url = rs("LCUERPO").Value
                If InStr(.url.ToUpper, "HREF") <> 0 Then
                    .url = serverMHT & DB.getLXParam("SMTPAVISOGRAL", local) & "?LID=" & rs("LID").Value
                Else
                    If InStr(.url.ToUpper, "HTTP") = 0 Then
                        If InStr(.url.ToUpper, ".ASP") = 0 Then
                            .url = serverMHT & DB.getLXParam("SMTPAVISOGRAL", local) & "?LID=" & rs("LID").Value
                        Else
                            .url = serverMHT & .url
                        End If
                    End If
                End If

                .Adjuntos = rs("LADJUNTOS").Value
                .local = My.Settings.LOCAL.ToUpper
                resultado = .enviaMail()
                If (resultado = "OK") Then
                    fSql = " UPDATE RMAILX SET  "
                    fSql = fSql & " LUPDDAT = NOW() , "
                    fSql = fSql & " LENVIADO = 1,  "
                    fSql = fSql & " LMSGERR = 'OK'  "
                    fSql = fSql & " WHERE LID = " & rs("LID").Value
                Else
                    fSql = " UPDATE RMAILX SET  "
                    fSql = fSql & " LUPDDAT = NOW() , "
                    fSql = fSql & " LENVIADO = -1,  "
                    fSql = fSql & " LMSGERR = '" & resultado & "'  "
                    fSql = fSql & " WHERE LID = " & rs("LID").Value
                End If
                DB.ExecuteSQL(fSql)
            End With
            rs.MoveNext()
        Loop
        rs.Close()

    End Sub
    Function AvisoLog() As Boolean
        Dim fSql As String
        Dim rs As New ADODB.Recordset
        Dim serverMHT As String
        Dim local As String = IIf(My.Settings.LOCAL.ToUpper = "SI", "CCNOT1", "")
        Dim resultado As String = ""
        Dim Correo As New clsMHT

        DB.gsKeyWords = "AvisoLog"
        serverMHT = DB.getLXParam("SMTPMHTSERVER", local)

        fSql = " SELECT * FROM  RFLOG WHERE ALERT = 1 "
        DB.OpenRS(rs, fSql)
        Do While Not rs.EOF
            With Correo
                .Inicializa()
                .Modulo = "PGMERR"
                .Asunto = rs("PROGRAMA").Value
                .De = "Error en Programa"
                .Para = "BSS" & IIf(rs("ALERTTO").Value <> "", "," & rs("ALERTTO").Value, "")
                .url = serverMHT & DB.getLXParam("SMTPAVISOERROR", "") & "?ID=" & rs("ID").Value
                .local = My.Settings.LOCAL.ToUpper
                resultado = .enviaMail()
                If (resultado = "OK") Then
                    fSql = " UPDATE RFLOG SET ALERT = 2"
                    fSql = fSql & " WHERE ID = " & rs("ID").Value
                Else
                    fSql = " UPDATE RFLOG SET ALERT = -1"
                    fSql = fSql & " WHERE ID = " & rs("ID").Value
                End If
                DB.ExecuteSQL(fSql)
            End With
            rs.MoveNext()
        Loop
        rs.Close()
        Return True

    End Function

    Function AbreConexion(ModuloActual As String) As Boolean

        DB = New clsConexion
        DB.gsDatasource = My.Settings.AS400
        DB.gsLib = My.Settings.AMBIENTE
        DB.ModuloActual = ModuloActual
        Return DB.Conexion()

    End Function

    Function RevisaArchivo(Archivo As String) As Boolean
        Dim Correo As New clsMHT
        With Correo
            .Inicializa()
            Return .avisoDesborde(Archivo)
        End With

    End Function

    Protected Overrides Sub OnStop()
        ' Agregue el código aquí para realizar cualquier anulación necesaria para detener el servicio.
        m_timer.Stop()
    End Sub


 
End Class
