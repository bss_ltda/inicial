﻿Imports System.IO

Public Class wsTareas

    Private WithEvents tim As New Timers.Timer(1000)

    Private DB As ADODB.Connection
    Dim AppTarea As String
    Dim curDia As Integer

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Agregue el código aquí para iniciar el servicio. Este método debería poner
        ' en movimiento los elementos para que el servicio pueda funcionar.
        tim.Enabled = True
        curDia = Day(Now())
    End Sub

    Protected Overrides Sub OnStop()
        ' Agregue el código aquí para realizar cualquier anulación necesaria para detener el servicio.
    End Sub

    Private Sub tim_Elapsed(sender As Object, e As System.Timers.ElapsedEventArgs) Handles tim.Elapsed

        revisaTareas()

    End Sub

    Sub revisaTareas()
        Dim fSql As String
        Dim pgm, param, exe As String
        Dim rs As New ADODB.Recordset
        Dim sts As Integer
        Dim msgT As String
        Dim tLXmsg As String
        Dim Accion As String

        DB = New ADODB.Connection
        'DB.Open("Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=APLLX;Password=LXAPL;Persist Security Info=True;Convert Date Time To Char=TRUE;Application Name=DFU001;Default Collection=ERPLX834F;Force Translate=0;")
        'DB.Open("Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=DESLXSSA;Password=DIC2013;Persist Security Info=True;Convert Date Time To Char=TRUE;Application Name=TAREAS;Default Collection=DESLX834F;Force Translate=0;")
        DB.Open(My.Settings.SERVIDOR)
        If DB.State = 0 Then
            DB.Close()
            Return
        End If
        Try

            AppTarea = My.Settings.TAPP
            If curDia <> Day(Now()) Then
                curDia = Day(Now())
                fSql = " DELETE FROM RFTASK  "
                fSql &= " WHERE "
                fSql &= "     TAPP = '" & AppTarea & "' "
                fSql &= " AND STS1 <> 0   "
                fSql &= " AND TFRECUENCIA = 0  "
                fSql &= " AND DATE(TFPROXIMA) <= DATE(NOW()) - 45 DAYS "
                DB.Execute(fSql)
            End If

            If AppTarea = "WMS" Then
                WMSInterfaces("WMS INTERFACE")
                WMSInterfaces("WMA INTERFACE")
            End If

            fSql = " SELECT * FROM  RFTASK "
            fSql &= " WHERE "
            fSql &= "     TAPP = '" & AppTarea & "' "
            fSql &= " AND STS1 = 0 "
            fSql &= " AND THLD = 0 "
            fSql &= " AND TFPROXIMA <= NOW() "
            rs.Open(fSql, DB)
            Do While Not rs.EOF
                Accion = UCase(rs("TTIPO").Value)
                If rs("FWRKID").Value <> 0 Then
                    If Accion <> "ULTIMO" Then
                        Accion = "PAQUETE"
                    End If
                Else
                    Accion = UCase(rs("TTIPO").Value)
                End If

                Select Case Accion
                    Case UCase("Shell")
                        pgm = rs("TPGM").Value
                        param = rs("TPRM").Value
                        exe = pgm & " " & param.Replace("|", " ")
                        If rs("TNUMTAREA").Value = 1 Then
                            exe &= " TAREA=" & rs("TNUMREG").Value
                        End If
                        sts = 1
                        msgT = "Comando Enviado."
                        tLXmsg = "EJECUTADA"
                        Try
                            Shell(exe, AppWinStyle.Hide, False, -1)
                        Catch ex As Exception
                            sts = -1
                            msgT = ex.Message.Replace("'", "''")
                            tLXmsg = "ERROR"
                        End Try
                        fSql = " UPDATE RFTASK SET "
                        fSql &= "  TLXMSG = '" & tLXmsg & "' "
                        fSql &= ", STS1 = " & sts & " "
                        fSql &= ", TUPDDAT = NOW()  "
                        fSql &= ", TMSG = '" & msgT & "'"
                        fSql &= ", TCMD = '" & Replace(exe, "'", "''") & "'"
                        fSql &= " WHERE TNUMREG = " & rs("TNUMREG").Value
                        DB.Execute(fSql)
                    Case UCase("Ultimo")
                        exe = "ImprimeDocumentos.exe " & rs("FWRKID").Value
                        sts = 1
                        msgT = "Paquete Enviado."
                        tLXmsg = "ENVIADO"
                        fSql = " UPDATE RFTASK SET "
                        fSql &= " TLXMSG = '" & tLXmsg & "', "
                        fSql &= " STS1 = " & sts & ", "
                        fSql &= " TUPDDAT = NOW() , "
                        fSql &= " TMSG = '" & msgT & "'"
                        fSql &= " WHERE TNUMREG = " & rs("TNUMREG").Value
                        DB.Execute(fSql)
                        Try
                            Shell(exe, AppWinStyle.Hide, False, -1)
                        Catch ex As Exception
                            msgT = ex.Message.Replace("'", "''")
                            WrtSqlError(exe, msgT)
                        End Try
                    Case UCase("Rutina")
                        pgm = Trim(rs("TPGM").Value)
                        If Left(pgm, 1) <> """" Then
                            pgm = """" & pgm & """"
                        End If
                        'Ejecutable de rutinas con parametro TNUMREG de RFTASK
                        exe = pgm & " " & rs("TNUMREG").Value
                        sts = 1
                        msgT = "Comando Enviado."
                        tLXmsg = "EJECUTADA"
                        Try
                            Shell(exe, AppWinStyle.Hide, False, -1)
                        Catch ex As Exception
                            sts = -1
                            msgT = ex.Message.Replace("'", "''")
                            tLXmsg = "ERROR"
                        End Try
                        fSql = " UPDATE RFTASK SET "
                        fSql &= " TLXMSG = '" & tLXmsg & "' "
                        fSql &= ", STS1 = " & sts
                        fSql &= ", TUPDDAT = NOW() "
                        fSql &= ", TMSG = '" & msgT & "'"
                        fSql &= ", TCMD = '" & exe.Replace("'", "''") & "'"
                        fSql &= " WHERE TNUMREG = " & rs("TNUMREG").Value
                        DB.Execute(fSql)
                    Case UCase("Java")

                End Select
                rs.MoveNext()
            Loop
            rs.Close()
            DB.Close()
        Catch ex As Exception
            sts = -1
            msgT = ex.Message.Replace("'", "''")
            tLXmsg = "ERROR"
            fSql = " UPDATE RFTASK SET "
            fSql &= " TLXMSG = '" & tLXmsg & "', "
            fSql &= " STS1 = " & sts & ", "
            fSql &= " TUPDDAT = NOW() , "
            fSql &= " TMSG = '" & msgT & "'"
            fSql &= " WHERE TNUMREG = " & rs("TNUMREG").Value
            DB.Execute(fSql)
        End Try

    End Sub

    Sub WMSInterfaces(qInterface As String)
        Dim rs As New ADODB.Recordset
        Dim fSql, vSql As String

        fSql = " SELECT TNUMREG, STS1, STS4 FROM RFTASK WHERE TAPP = 'WMS' AND TCATEG = '" & qInterface & "' AND NOW() + 2 MINUTES > TFPROXIMA "
        rs = DB.Execute(fSql)
        If Not rs.EOF Then
            If rs("STS4").Value = 0 Then
                If rs("STS1").Value = 1 Then
                    fSql = "UPDATE RFTASK SET"
                    fSql &= "  STS1 = 0 "
                    Select Case qInterface
                        Case "WMS INTERFACE"
                            fSql &= ", TFPROXIMA = NOW() + " & getLXParam("WMS_FRECUENCIA")
                        Case "WMA INTERFACE"
                            fSql &= ", TFPROXIMA = NOW() + " & getLXParam("WMA_FRECUENCIA")
                    End Select
                    fSql &= " WHERE TNUMREG = " & rs("TNUMREG").Value
                    DB.Execute(fSql)
                End If
            End If
        Else
            fSql = " INSERT INTO RFTASK( "
            vSql = " VALUES ( "
            fSql &= " TAPP      , " : vSql &= "'WMS', "     '//15A   
            fSql &= " TCATEG    , " : vSql &= "'" & qInterface & "', "     '//25A   Categoria
            fSql &= " TTIPO     , " : vSql &= "'Rutina', "     '//22A   Tipo
            fSql &= " TPGM      , " : vSql &= "'" & getLXParam("RUTINAS_WMS") & "', "     '//502A  Programa
            fSql &= " TCRTUSR   ) " : vSql &= "'SISTEMA' ) "     '//10A   Usuario
            DB.Execute(fSql & vSql)
        End If
        rs.Close()
        rs = Nothing

    End Sub


    Sub WrtSqlError(sql As String, Descrip As String)
        Dim conn As New ADODB.Connection
        Dim fSql As String
        Dim vSql As String

        Dim gsConnString As String = "Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=APLLX;Password=LXAPL;Persist Security Info=True;Default Collection=ERPLX834F;Force Translate=0;"

        Try
            conn.Open(gsConnString)
            fSql = " INSERT INTO RFLOG( "
            vSql = " VALUES ( "
            fSql &= " USUARIO   , " : vSql &= "'" & System.Net.Dns.GetHostName() & "', "      '//502A
            fSql &= " PROGRAMA  , " : vSql &= "'" & System.Reflection.Assembly.GetExecutingAssembly.FullName & "', "      '//502A
            fSql &= " ALERT     , " : vSql &= "1, "      '//1P0
            fSql &= " EVENTO    , " : vSql &= "'" & Replace(Descrip, "'", "''") & "', "      '//20002A
            fSql &= " TXTSQL    ) " : vSql &= "'" & Replace(sql, "'", "''") & "' ) "      '//5002A
            conn.Execute(fSql & vSql)
            conn.Close()
        Catch ex As Exception

        End Try

    End Sub

    Function getLXParam(prm As String) As String
        Return getLXParam(prm, "")
    End Function

    Function getLXParam(prm As String, Campo As String) As String
        Dim rs As New ADODB.Recordset
        Dim resultado As String = ""
        Dim fSql As String

        If Campo = "" Then
            Campo = "CCNOT1"
        End If
        fSql = "SELECT " & Campo & " AS DATO FROM RFPARAM WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER('" & prm & "')"
        rs.Open(fSql, Me.DB)
        If Not rs.EOF Then
            resultado = rs("DATO").Value
        End If
        rs.Close()
        Return resultado.Trim

    End Function
End Class
