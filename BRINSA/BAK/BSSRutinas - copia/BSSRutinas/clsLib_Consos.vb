﻿Public Class clsLib_Consos
    Public Conso As String
    Public HSFCNS As String

    Dim fSql, vSql As String

    'WMSCOPED()
    Sub WMSCOPED()
        Dim rs As New ADODB.Recordset
        Dim rs0 As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim wmsDoc As String = ""
        Dim wmsPed, wmsOrdEnt As String
        Dim wmsPedLin As Integer = 0
        Dim wmsLine As Integer = 0
        Dim wmsPEnv As String = ""
        Dim recNum As String
        Dim totPeds As Double
        Dim i As Integer
        Dim bWms, bEnviar As Boolean
        Dim Sql As String
        Dim Pedid As Double = 0

        bEnviar = True

        fSql = "SELECT * FROM RDCCEW WHERE VCONSO = '" & Conso & "'"
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            LogConso("Reenvio a WMS.")
        Else
            Select Case HSFCNS
                Case "BS2"
                    LogConso("Enviado a Asignacion BodSat II")
                Case "SAT"
                    LogConso("Enviado a Webservices")
                Case Else
                    LogConso("Enviado a WMS. Portal CDA. v2.")
            End Select

        End If
        rs.Close()

        If bEnviar Then
            fSql = " DELETE  FROM WMSFDNL  "
            fSql &= " WHERE DPRE = 'PNO' AND DID = " & Conso
            DB.ExecuteSQL(fSql)

            fSql = " DELETE  FROM WMSFSEND  "
            fSql &= " WHERE PRE = 'PNO' AND ID = " & Conso
            DB.ExecuteSQL(fSql)
        End If

        fSql = "DELETE FROM RDCCEW WHERE VCONSO = '" & Conso & "'"
        DB.ExecuteSQL(fSql)

        fSql = "UPDATE RDCC SET DNUMCD = RIGHT(DIGITS( DPEDID ), 6 ) "
        fSql &= " Where DCONSO = '" & Conso & "' "
        DB.ExecuteSQL(fSql)

        fSql = " UPDATE RDCCAD SET ESUBCON = 'PD' || RIGHT( VARCHAR( EPEDID  ), 6 )  "
        fSql &= " WHERE ECONSO = '" & Conso & "' "
        DB.ExecuteSQL(fSql)

        fSql = " SELECT DISTINCT  "
        fSql &= " DNUMCD, DIGITS(DCUST) || '.' || DIGITS(DSHIP)  AS PENVIO, DPEDID, "
        fSql &= " CASE WHEN ECPO IS NULL THEN 'PEDIDO' ELSE 'PREDIST' END AS TIPO "
        fSql &= " FROM RDCC LEFT JOIN RDCCAD ON ECONSO='" & Conso & "' AND DPEDID=EPEDID AND DLINEA=ELINEA "
        fSql &= " WHERE DCONSO = '" & Conso & "' "
        fSql &= " ORDER BY TIPO "
        rs = DB.ExecuteSQL(fSql)
        Do While Not rs.EOF
            If rs("TIPO").Value = "PREDIST" Then
                If wmsPEnv = rs("PENVIO").Value Then
                    If wmsDoc <> rs("DNUMCD").Value Then
                        fSql = "UPDATE RDCC SET DNUMCD = '" & Right(wmsDoc, 6) & "'"
                        fSql &= " Where DCONSO = '" & Conso & "' "
                        fSql &= " AND DNUMCD = '" & rs("DNUMCD").Value & "'"
                        DB.ExecuteSQL(fSql)
                        fSql = "UPDATE RDCCAD SET ESUBCON = 'PD" & Right(wmsDoc, 6) & "'"
                        fSql &= " Where ECONSO = '" & Conso & "' "
                        fSql &= " AND EPEDID = " & rs("DPEDID").Value
                        DB.ExecuteSQL(fSql)
                    End If
                Else
                    wmsPEnv = rs("PENVIO").Value
                    wmsDoc = rs("DNUMCD").Value
                End If
            Else
                wmsPEnv = ""
                wmsDoc = ""
            End If
            rs.MoveNext()
        Loop
        rs.Close()

        'ORDEN DE ENTREGA
        fSql = " Select "
        fSql &= " DNUMCD, MAX(DORDENT) AS DORDENT "
        fSql &= " From RDCC "
        fSql &= " Where DCONSO = '" & Conso & "' "
        fSql &= " Group By DNUMCD "
        fSql &= " Order By DORDENT"
        rs0 = DB.ExecuteSQL(fSql)
        i = 1
        Do While Not rs0.EOF
            fSql = "UPDATE RDCC SET DORDENT = " & (CDbl(rs0("DORDENT").Value) + i)
            fSql &= " Where DCONSO = '" & Conso & "' "
            fSql &= " AND DNUMCD='" & rs0("DNUMCD").Value & "'"
            DB.ExecuteSQL(fSql)
            rs0.MoveNext()
            i = i + 1
        Loop
        rs0.Close()


        fSql = "SELECT COUNT(*) FROM ( SELECT DISTINCT DNUMCD FROM RDCC WHERE DCONSO = '" & Conso & "' ) PEDS "
        rs = DB.ExecuteSQL(fSql)
        totPeds = CDbl(rs(0).Value)
        rs.Close()

        fSql = "SELECT COUNT(*) FROM ( SELECT DISTINCT ETIENDA FROM RDCCAD WHERE ECONSO = '" & Conso & "' ) PEDS "
        rs = DB.ExecuteSQL(fSql)
        totPeds = totPeds + CDbl(rs(0).Value) + 1
        rs.Close()

        wmsPed = 0

        fSql = " Select "
        fSql &= " DNUMCD, MAX(DORDENT) AS DORDENT "
        fSql &= " From RDCC "
        fSql &= " Where DCONSO = '" & Conso & "' "
        fSql &= " Group By DNUMCD "
        fSql &= " Order By DORDENT DESC"
        rs0 = DB.ExecuteSQL(fSql)
        wmsOrdEnt = 0
        Do While Not rs0.EOF
            wmsPed = wmsPed + 1
            wmsDoc = Conso & ceros(wmsPed, 3)
            totPeds = totPeds - 1
            wmsOrdEnt = wmsOrdEnt + 1
            'Revisa si es predistribuido
            fSql = "SELECT * FROM RDCCAD WHERE ECONSO = '" & Conso & "' AND ESUBCON = 'PD" & rs0("DNUMCD").Value & "'"
            rs = DB.ExecuteSQL(fSql)
            'PROCESO NORMAL
            If rs.EOF Then
                rs.Close()
                fSql = " Select "
                fSql &= "  DCONSO, "
                fSql &= "  DPEDID, "
                fSql &= "  DLINEA, "
                fSql &= "  DORDENT, "
                fSql &= "  DSHIP, "
                fSql &= "  DNSHIP, "
                fSql &= "  DPROD, "
                fSql &= "  DIWHR, "
                fSql &= "  DCANT, "
                fSql &= "  DPESO "
                fSql &= " From "
                fSql &= "  RDCC RDCC "
                fSql &= " Where "
                fSql &= "  RDCC.DCONSO = '" & Conso & "' "
                fSql &= "  AND RDCC.DNUMCD = '" & rs0("DNUMCD").Value & "'"
                fSql &= " Order By "
                fSql &= "  DORDENT DESC, DPROD, DCANT "
                rs = DB.ExecuteSQL(fSql)
                i = 1
                Do While Not rs.EOF
                    'wmsLine = 100 + wmsPed & Ceros(rs("DLINEA"), 3)
                    wmsLine = 100 + wmsPed & ceros(i, 3)
                    fSql = " INSERT INTO RDCCEW( "
                    vSql = " VALUES ( "
                    mkSql(" ", " VNUMREG   ", " ( NEXT VALUE FOR RDCCEW_SEQ )", 0)                 '//8P0   Num Reg
                    mkSql("'", " VTIPO     ", "PED", 0)                         '//3A    Tipo
                    mkSql("'", " VWMSDOCU  ", wmsDoc, 0)                        '//9A    Documento WMS
                    mkSql("'", " VWMSLINEA ", wmsLine, 0)                       '//6A    Linea Consolidado
                    mkSql(" ", " VORDENT   ", wmsOrdEnt, 0)                     '//3P0   Orden Entrega
                    mkSql("'", " VCONSO    ", rs("DCONSO").Value, 0)            '//6A    Consolidado
                    mkSql("'", " VSUBCON   ", rs("DPEDID").Value, 0)            '//8A    Subconso
                    mkSql(" ", " VWMSPED   ", rs("DPEDID").Value, 0)            '//83P0  Pedido Calc
                    mkSql(" ", " VWMSLIN   ", i, 0)                             '//4P0   Linea Pedido Calc
                    mkSql(" ", " VPEDID    ", rs("DPEDID").Value, 0)            '//8P0   Pedido BPCS
                    mkSql(" ", " VLINEA    ", rs("DLINEA").Value, 0)            '//4P0   Linea  BPCS
                    mkSql(" ", " VCONSEC   ", wmsPed, 0)                        '//3P0   Consecutivo o Tienda
                    mkSql(" ", " VTIENDA   ", rs("DSHIP").Value, 0)             '//4P0   Tienda
                    mkSql("'", " VTIENDAD  ", ceros(rs("DSHIP").Value, 4) & " " & campoAlfa(CStr(rs("DNSHIP").Value), 45), 0)            '//50A   Nombre ShipTo
                    mkSql(" ", " VTLINEA   ", rs("DLINEA").Value, 0)            '//4P0   Linea Tienda
                    mkSql("'", " VPROD     ", rs("DPROD").Value, 0)             '//35A   Producto
                    mkSql(" ", " VCANT     ", rs("DCANT").Value, 0)             '//11P3  Cantidad
                    mkSql(" ", " VPESO     ", rs("DPESO").Value, 0)             '//11P3  Cantidad
                    mkSql("'", " VBOD      ", rs("DIWHR").Value, 1)             '//3A    Bodega
                    DB.ExecuteSQL(fSql & vSql)
                    i = i + 1
                    rs.MoveNext()
                Loop
                rs.Close()
                'PROCESO PREDISTRIBUIDO
            Else
                rs.Close()
                fSql = " SELECT DNUMCD, ETIENDA, SUM(ECANT) AS ECANT"
                fSql &= " FROM RDCC INNER JOIN RDCCAD ON DPEDID=EPEDID AND DLINEA=ELINEA  "
                fSql &= " WHERE DCONSO = '" & Conso & "'"
                fSql &= " AND EQCON <> 0 "
                fSql &= " GROUP BY DNUMCD, ETIENDA "
                fSql &= " ORDER BY ECANT "
                rs = DB.ExecuteSQL(fSql)
                i = wmsOrdEnt
                Do While Not rs.EOF
                    fSql = " SELECT DISTINCT EPEDID "
                    fSql &= " FROM RDCC INNER JOIN RDCCAD ON DPEDID=EPEDID AND DLINEA=ELINEA  "
                    fSql &= " WHERE DCONSO = '" & Conso & "'"
                    fSql &= " AND DNUMCD = '" & rs("DNUMCD").Value & "'"
                    rs1 = DB.ExecuteSQL(fSql)
                    Do While Not rs1.EOF
                        fSql = "UPDATE RDCCAD SET EORDENT = " & i
                        fSql &= " WHERE ECONSO = '" & Conso & "'"
                        fSql &= " AND EPEDID = " & rs1("EPEDID").Value
                        fSql &= " AND ETIENDA = " & rs("ETIENDA").Value
                        DB.ExecuteSQL(fSql)
                        rs1.MoveNext()
                    Loop
                    rs1.Close()
                    rs.MoveNext()
                    i = i + 1
                Loop
                rs.Close()
                wmsOrdEnt = i

                fSql = " Select "
                fSql &= "  ECPO, ETIENDA, STAD4, ETLINEA, DCONSO, ESUBCON, "
                fSql &= "  DPEDID, "
                fSql &= "  DLINEA, "
                fSql &= "  EORDENT, "
                fSql &= "  ETIENDAD, "
                fSql &= "  DPROD, "
                fSql &= "  DIWHR, "
                fSql &= "  EQCON, EPESO "
                fSql &= " From "
                fSql &= "  RDCC RDCC INNER JOIN "
                fSql &= "  ( RDCCAD INNER JOIN EST ON EPCUST=TCUST AND ETIENDA=TSHIP )"
                fSql &= "  ON DCONSO=ECONSO AND DPEDID=EPEDID AND DLINEA=ELINEA "
                fSql &= " Where "
                fSql &= "  RDCC.DCONSO = '" & Conso & "' "
                fSql &= "  AND RDCC.DNUMCD = " & rs0("DNUMCD").Value
                fSql &= "  AND EQCON > 0"
                fSql &= " Order By "
                fSql &= "  EORDENT DESC, DPROD, DCANT "
                rs = DB.ExecuteSQL(fSql)
                Do While Not rs.EOF

                    If Pedid <> CDbl(rs("ETIENDA").Value) Then
                        wmsLine = 200000
                        wmsPed = wmsPed + 1
                        wmsDoc = Conso & ceros(wmsPed, 3)
                        Pedid = CDbl(rs("ETIENDA").Value)
                        totPeds = totPeds - 1
                        wmsPedLin = 1
                    End If

                    wmsLine = wmsLine + 1
                    fSql = " INSERT INTO RDCCEW( "
                    vSql = " VALUES ( "
                    mkSql(" ", " VNUMREG   ", " ( NEXT VALUE FOR RDCCEW_SEQ )", 0)                 '//8P0   Num Reg
                    mkSql("'", " VTIPO     ", "PRE", 0)                 '//3A    Tipo
                    mkSql("'", " VWMSDOCU  ", wmsDoc, 0)                '//9A    Documento WMS
                    mkSql("'", " VWMSLINEA ", wmsLine, 0)               '//6A    Linea Consolidado
                    mkSql(" ", " VORDENT   ", rs("EORDENT").Value, 0)         '//3P0   Orden Entrega
                    mkSql("'", " VCONSO    ", rs("DCONSO").Value, 0)          '//6A    Consolidado
                    mkSql("'", " VSUBCON   ", "PD" & Right(rs("ESUBCON").Value, 4), 0)            '//6A    Consolidado
                    mkSql(" ", " VWMSPED   ", wmsPed, 0)                '//83P0  Pedido Calc
                    mkSql(" ", " VWMSLIN   ", wmsPedLin, 0)             '//4P0   Linea Pedido Calc
                    mkSql(" ", " VPEDID    ", rs("DPEDID").Value, 0)          '//8P0   Pedido BPCS
                    mkSql(" ", " VLINEA    ", rs("DLINEA").Value, 0)          '//4P0   Linea  BPCS
                    mkSql(" ", " VCONSEC   ", wmsPed, 0)                '//3P0   Consecutivo o Tienda
                    mkSql("'", " VCPO      ", rs("ECPO").Value, 0)          '//23A   Orden de Compra
                    mkSql(" ", " VTIENDA   ", rs("ETIENDA").Value, 0)         '//4P0   Tienda
                    mkSql("'", " VTIENDAD  ", Left(rs("STAD4").Value, 3) & " " & campoAlfa(CStr(rs("ETIENDAD").Value), 48), 0)            '//50A   Nombre ShipTo
                    mkSql(" ", " VTLINEA   ", rs("ETLINEA").Value, 0)         '//4P0   Linea Tienda
                    mkSql("'", " VPROD     ", rs("DPROD").Value, 0)           '//35A   Producto
                    mkSql(" ", " VCANT     ", rs("EQCON").Value, 0)          '//11P3  Cantidad
                    mkSql(" ", " VPESO     ", rs("EPESO").Value, 0)           '//11P3  Cantidad
                    mkSql("'", " VBOD      ", rs("DIWHR").Value, 1)           '//3A    Bodega
                    DB.ExecuteSQL(fSql & vSql)
                    fSql = "UPDATE RDCCAD SET "
                    fSql &= " EDOCLIN = " & wmsLine & ", "
                    fSql &= " EWMSDOCU  ='" & wmsDoc & "', "      '//9A    Documento WMS
                    fSql &= " EWMSLINEA ='" & wmsLine & "' "      '//6A    Linea Consolidado
                    fSql &= " WHERE ECPO = '" & rs("ECPO").Value & "'"
                    fSql &= " AND ETIENDA = " & rs("ETIENDA").Value
                    fSql &= " AND ETLINEA = " & rs("ETLINEA").Value
                    DB.ExecuteSQL(fSql)
                    rs.MoveNext()
                    wmsPedLin = wmsPedLin + 1
                Loop
                rs.Close()
            End If
            rs0.MoveNext()
        Loop
        rs0.Close()

        Select Case HSFCNS
            Case "MDA", "ODT", "BS2"
                fSql = " SELECT HBOD FROM RHCC WHERE HCONSO = '" & Conso & "' "
                rs0 = DB.ExecuteSQL(fSql)
                'LA BODEGA DEBE EXISTIR EN SELECT * FROM  RFPARAM WHERE CCALTC = 'ODT'
                fSql = "CALL PGM(RRMXDESP) PARM('" & Conso & "' 'ODT_" & rs0("HBOD").Value & "')"
                DB.ExecuteSQL("{{ " & fSql & " }}")
                rs0.Close()

                fSql = "SELECT VNUMREG FROM RDCCEW WHERE VCONSO = '" & Conso & "'"
                fSql &= " Order By "
                fSql &= "  VORDENT DESC, VPEDID, VPROD, VLOTE "
                rs0 = DB.ExecuteSQL(fSql)
                Dim lineaConso As Integer = 1
                Do While Not rs0.EOF
                    fSql = "UPDATE RDCCEW SET VLINCONSO = " & lineaConso & " WHERE VNUMREG = " & rs0("VNUMREG").Value
                    DB.ExecuteSQL(fSql)
                    lineaConso += 1
                    rs0.MoveNext()
                Loop
                rs0.Close()

                Select Case HSFCNS
                    Case "MDA"
                        fSql = DB.getLXParam("SITIO_CARPETA") & "\LDmda.bat " & DB.getLXParam("SITIO_CARPETA") & " " & Conso
                        DB.gsKeyWords &= fSql
                        Shell(fSql, AppWinStyle.Hide, False)
                    Case "ODT", "BS2"
                        bEnviar = False
                        fSql = DB.getLXParam("SITIO_CARPETA") & "\LD_ODT.bat " & DB.getLXParam("SITIO_CARPETA") & " " & Conso
                        DB.gsKeyWords &= fSql
                        Shell(fSql, AppWinStyle.Hide, False)
                End Select

                fSql = " SELECT VPROD, IDESC FROM RDCCEW INNER JOIN IIM ON VPROD = IPROD WHERE VCONSO = '" & Conso & "' AND VLOTE = 'NOINV' "
                rs0 = DB.ExecuteSQL(fSql)
                If Not rs0.EOF Then
                    'Marca el consolidado con problema de inventarios
                    fSql = " UPDATE RHCCX SET XCSTS01 = 1 "
                    fSql &= " WHERE XCCONSO = '" & Conso & "' "
                    DB.ExecuteSQL(fSql)

                    Dim Alerta As New clsCorreoMHT
                    With Alerta
                        .Inicializa()
                        .DB = DB
                        .Referencia = "INVENTARIO " & HSFCNS
                        .Asunto = "PRODUCTO SIN INVENTARIO"
                        .Modulo = HSFCNS
                        .Referencia = "Operacion " & HSFCNS
                        .Destinatario = "BSS"
                        'DEBE EXISTIR EN SELECT * FROM ZCCC WHERE CCTABL = 'FLAVISOS' AND CCCODE = HSFCNS
                        .Destinatario = .AvisarA()
                        .AddLine("Consolidado: " & Conso)
                        .AddLine("<HR>")
                    End With
                    Do While Not rs0.EOF
                        Alerta.AddLine(rs0("VPROD").Value & " " & rs0("IDESC").Value)
                        rs0.MoveNext()
                    Loop
                    If Alerta.Destinatario <> "" Then
                        Alerta.EnviaCorreo()
                    End If
                End If
                rs0.Close()
        End Select

        If bEnviar Then
            'dsp "Envio a wms    "
            fSql = " DELETE FROM WMSFOD WHERE V4CONSO = '" & Conso & "' "
            DB.ExecuteSQL(fSql)

            fSql = " DELETE FROM WMSFOM WHERE V3CONSO = '" & Conso & "' "
            DB.ExecuteSQL(fSql)

            fSql = " SELECT W.*, IUMS FROM RDCCEW W INNER JOIN IIM ON VPROD=IPROD WHERE VCONSO = '" & Conso & "' AND VBOD IN( 'PT', 'TC' ) "
            rs = DB.ExecuteSQL(fSql)
            recNum = 1
            bWms = False
            Do While Not rs.EOF
                recNum = recNum + 1
                bWms = True
                fSql = " INSERT INTO WMSFOD( "
                vSql = " VALUES ( "
                mkSql(" ", " V4CONS    ", recNum, 0)                        '//6S0   recordNumber
                mkSql("'", " V4TIPO    ", "OD", 0)                          '//2A    recordName
                mkSql("'", " V4ACTI    ", "A", 0)                           '//1A    recordAction
                mkSql(" ", " V4OLIN    ", rs("VWMSLINEA").Value, 0)         '//6S0   orderline
                mkSql("'", " V4INID    ", rs("VWMSDOCU").Value, 0)          '//15A   orderId
                mkSql("'", " V4INTP    ", "PNO", 0)                         '//4A    orderIdType
                mkSql("'", " V4LNST    ", "HOLD", 0)                        '//4A    lineStatus
                mkSql("'", " V4PART    ", rs("VPROD").Value, 0)             '//20A   part
                If rs("VBOD").Value = "TC" Then
                    mkSql("'", " V4PACK    ", rs("VBOD").Value, 0)          '//6A    package
                End If
                mkSql("'", " V4LOTE    ", "*", 0)                           '//15A   lot
                mkSql("'", " V4USR2    ", "*", 0)                           '//15A   usercode2
                mkSql("'", " V4USR3    ", "*", 0)                           '//15A   usercode3
                mkSql(" ", " V4QORI    ", rs("VCANT").Value, 0)             '//13S3  Quantity,original
                mkSql(" ", " V4QORD    ", rs("VCANT").Value, 0)             '//13S3  Quantity,ordered
                mkSql(" ", " V4QPLA    ", rs("VCANT").Value, 0)             '//13S3  Quantity,planned
                mkSql("'", " V4ORUM    ", rs("IUMS").Value, 0)              '//6A    orderUOM
                mkSql("'", " V4ALFS    ", "Y", 0)                           '//1A    allowFillShort
                mkSql("'", " V4HBCK    ", "N", 0)                           '//1A    hostBackordered
                mkSql("'", " V4SNUM    ", "N", 0)                           '//1A    serialNumberRequired
                mkSql("'", " V4OWNE    ", "BRINSA", 0)                      '//20A   owner
                mkSql("'", " V4HOUS    ", rs("VBOD").Value, 0)              '//10A   warehouse
                mkSql("'", " V4CONSO   ", rs("VCONSO").Value, 0)            '//6A    Consolidado
                mkSql("'", " V4FLAG    ", " ", 1)                           '//1A    Flag
                DB.ExecuteSQL(fSql & vSql)
                rs.MoveNext()
            Loop
            rs.Close()
        End If

        If bWms And bEnviar Then
            fSql = " INSERT INTO WMSFOM( "
            vSql = " SELECT DISTINCT "
            mkSql(" ", " V3CONS    ", 1, 0)                     '//6S0   recordNumber
            mkSql("'", " V3TIPO    ", "OM", 0)                  '//2A    recordName
            mkSql("'", " V3ACTI    ", "A", 0)                   '//1A    recordAction
            mkSql(" ", " V3INID    ", "VWMSDOCU    ", 0)        '//15A   orderId
            mkSql("'", " V3INTP    ", "PNO", 0)                 '//4A    orderIdType
            mkSql(" ", " V3FEEX    ", "VCONSO    ", 0)          '//15A   shipment
            Sql = "'' || MOD( YEAR(NOW()), 100 ) || RIGHT( '0' || MONTH( NOW() )  , 2 ) || RIGHT( '0' || DAY( NOW() )  , 2 ) || RIGHT( '0' || HOUR( NOW() )  , 2 )"
            mkSql(" ", " V3GRP1    ", Sql, 0)                   '//20A   Group1
            mkSql(" ", " V3GRP2    ", "DIGITS(VPEDID)    ", 0)  '//20A   Group2
            mkSql(" ", " V3GRP3    ", "HPLACA", 0)              '//20A   Group2
            mkSql("'", " V3ORST    ", "HOLD", 0)                '//4A    orderStatus
            mkSql("'", " V3PDST    ", "SALIDA", 0)              '//10A   destinationPickingLocation

            Sql = " CASE WHEN DIWHR = 'TC' THEN 'RNAL'"
            Sql = Sql & "      WHEN DSLMER = 'NACIONAL' THEN 'RNAL' "
            Sql = Sql & "      ELSE 'REXP' END"

            mkSql(" ", " V3MATE    ", "( " & Sql & " )", 0)          '//8A    materialSearchCode
            mkSql("'", " V3PAMS    ", "N", 0)                   '//1A    packAndMarkSeperate
            mkSql(" ", " V3PRIO    ", "VORDENT   ", 0)          '//3S0   priority
            mkSql(" ", " V3CHDT    ", DB2_DATEDEC, 0)           '//8A    scheduledShippingDate
            mkSql(" ", " V3CARR    ", "DIGITS(HPROVE)", 0)      '//15A   carrier
            mkSql("'", " V3FRTE    ", "01", 0)                  '//2A    freightTerms
            mkSql(" ", " V3BLD1    ", "TRIM(DSLGER) || '/' || TRIM(DIDIVI) || '[' || DIGITS(DPEDID) || ']' ", 0)          '//40A   BillTo,Address,name1
            mkSql(" ", " V3SHN1    ", "LEFT(DNSHIP, 40 ) ", 0)  '//40A   ShipTo,Address,name1
            mkSql(" ", " V3SHL1    ", "LEFT(DHAD1, 40 )  ", 0)  '//40A   ShipTo,Address,LINE1
            mkSql(" ", " V3SHL2    ", "LEFT(DHAD2, 40 )  ", 0)  '//40A   ShipTo,Address,Line2
            mkSql(" ", " V3SHTO    ", "DSPOST    ", 0)          '//35A   ShipTo,Address,city
            mkSql(" ", " V3SHAD    ", "DSHDEP    ", 0)          '//10A   ShipTo,Address,state
            mkSql(" ", " V3SHCN    ", "DIGITS(DCUST)", 0)       '//20A   ShipTo,customerNumber
            mkSql(" ", " V3SAD1    ", "LEFT(DHAD1, 40 ) ", 0)   '//40A   ShipFrom,Address,name1
            mkSql(" ", " V3SAD2    ", "LEFT(DHAD1, 40 ) ", 0)   '//40A   ShipFrom,Address,name2
            mkSql(" ", " V3BUMA    ", "LEFT(HOBSER, 100 ) ", 0) '//100A  BillTo,Address,emailAddress
            mkSql(" ", " V3WHOU    ", "DIWHR    ", 0)           '//4A    Warehouse
            mkSql(" ", " V3CONSO   ", "VCONSO   ", 0)           '//6A    Consolidado
            mkSql("'", " V3FLAG    ", " ", 10)                  '//1A    Flag
            vSql &= " From "
            vSql &= "  RDCC RDCC Inner Join "
            vSql &= "  RDCCEW RDCCEW On RDCC.DCONSO = RDCCEW.VCONSO And RDCC.DPEDID = RDCCEW.VPEDID "
            vSql &= "  And RDCC.DLINEA = RDCCEW.VLINEA Inner Join "
            vSql &= "  RHCC RHCC On RDCC.DCONSO = RHCC.HCONSO "
            vSql &= "  WHERE VCONSO='" & Conso & "'"
            DB.ExecuteSQL(fSql & vSql)

            fSql = " DELETE FROM WMSFDNL WHERE DPRE = 'PNO' AND DID = " & Conso
            DB.ExecuteSQL(fSql)

            fSql = " INSERT INTO WMSFDNL( "
            vSql = " VALUES ( "
            mkSql("'", " DPRE      ", "PNO", 0)          '//3A    Prefijo
            mkSql(" ", " DID       ", Conso, 1)         '//8P0   Numero
            DB.ExecuteSQL(fSql & vSql)

            fSql = "CALL PGM(WMSCONSO) PARM('" & Conso & "')"
            DB.ExecuteSQL("{{ " & fSql & " }} ")
        End If

    End Sub

    'LogConso()
    Sub LogConso(Reporte As String)
        Dim fSql, vSql As String
        Dim rs As New ADODB.Recordset

        fSql = "INSERT INTO RRCC("
        vSql = "VALUES( "
        fSql &= "RCONSO, " : vSql &= "'" & Conso & "'" & ", "
        fSql &= "RSTSC, "
        vSql &= "( SELECT HESTAD FROM RHCC WHERE HCONSO='" & Conso & "' )" & ", "
        fSql &= "RTREP,   " : vSql &= " " & "2" & " " & ", "
        fSql &= "RFECREP, " : vSql &= " NOW(), "
        fSql &= "RREPORT, " : vSql &= "'" & campoAlfa(Reporte, 500) & "'" & ", "
        fSql &= "RWINDAT, " : vSql &= "'" & Format(Now(), "yyyy-MM-dd-HH.mm.ss.000000") & "', "
        fSql &= "RAPP,    " : vSql &= "'PORTALTM'" & ", "
        fSql &= "RCRTUSR) " : vSql &= "'SISTEMA'" & ") "
        ''dsp vSql
        If Conso <> "" Then
            DB.ExecuteSQL(fSql & vSql)
            fSql = "SELECT RCONSO FROM RRCCT WHERE RCONSO='" & Conso & "'"
            rs = DB.ExecuteSQL(fSql)
            If rs.EOF Then
                DB.ExecuteSQL("INSERT INTO RRCCT( RCONSO ) VALUES('" & Conso & "')")
            End If
            rs.Close()

            fSql = "SELECT CCALTC, HREF03, HESTAD FROM RHCC, VSPESTAD WHERE HESTAD=CCCODE AND HCONSO='" & Conso & "'"
            rs = DB.ExecuteSQL(fSql)
            fSql = "UPDATE RRCCT SET "
            fSql &= "RFEC" & rs("CCALTC").Value & "= NOW()"
            fSql &= " WHERE RCONSO='" & Conso & "' AND YEAR(RFEC" & rs("CCALTC").Value & ") = 1"
            DB.ExecuteSQL(fSql)
            rs.Close()
            rs = Nothing
        End If

    End Sub

    Sub timerConsoCargado()
        Dim rs As New ADODB.Recordset
        fSql = "SELECT DISTINCT HESTAD, HACTUSR, "
        fSql &= " CHAR( VARCHAR_FORMAT( HBASCUI,'yyyy-MM-dd HH24:MI'), 16 ) HBASCUI, DACTUSR "
        fSql &= "FROM RHCC INNER JOIN RDCC ON HCONSO = DCONSO WHERE HCONSO = '" & Conso & "'"
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            If rs("HESTAD").Value = 43 Then
                Dim Alerta As New clsCorreoMHT
                With Alerta
                    .Inicializa()
                    .DB = DB
                    .Asunto = "Consolidado aun no despachado " & Conso
                    .Modulo = "MDA"
                    .Referencia = "Operacion MDA"
                    .Destinatario = rs("HACTUSR").Value
                    .AddLine("Consolidado: " & Conso)
                    .AddLine("Fecha Documentos: " & rs("HBASCUI").Value)
                    Do While Not rs.EOF
                        If rs("HACTUSR").Value <> rs("DACTUSR").Value Then
                            .AddTO(rs("DACTUSR").Value)
                        End If
                        rs.MoveNext()
                    Loop
                    .EnviaCorreo()
                End With
            End If
        End If
        rs.Close()

    End Sub

    Sub LiberaConso()
        Dim rs As New ADODB.Recordset
        Dim fSql As String

        fSql = " SELECT HCONSO FROM RHCC WHERE HCONSO = '" & Conso & "' AND HESTAD = 0 "
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            fSql = " UPDATE RHCC SET  "
            fSql &= "  HPROVE2 = 88888 "
            fSql &= " WHERE HCONSO = '" & Conso & "'  "
            DB.ExecuteSQL(fSql)

            LogConso("Liberado a las demas trasnportadoras.")

        End If
        rs.Close()
        rs = Nothing

    End Sub

    Sub debeGenerarIAL()
        Dim rs As New ADODB.Recordset

        fSql = "UPDATE RDCC SET DUDS02 = 0 "
        fSql &= " WHERE  "
        fSql &= "    DCONSO    = '" & Conso & "' "
        db.ExecuteSQL(fSql)

        fSql = " SELECT DISTINCT DCONSO, DPROD FROM RDCC  "
        fSql &= " INNER JOIN RCQCOND ON DPROD = KPROD  "
        fSql &= " WHERE  "
        fSql &= "    DCONSO    = '" & Conso & "' "
        fSql &= " AND DUDS02   = 0 "
        fSql &= " AND KSLMER   = '-- TODOS --' "
        fSql &= " AND KDIVI    = '-- TODOS --' "
        fSql &= " AND KLINE    = '-- TODOS --' "
        fSql &= " AND KSEGM    = '-- TODOS --' "
        fSql &= " AND KMARCA   = '-- TODOS --' "
        fSql &= " AND KCUSTSEL = '-- TODOS --' "
        fSql &= " AND KSHIPSEL = '-- TODOS --' "
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            fSql = "UPDATE RDCC SET DUDS02 = 1 "
            fSql &= " WHERE  "
            fSql &= "    DCONSO    = '" & rs("DCONSO").Value & "' "
            fSql &= " AND DPROD    = '" & rs("DPROD").Value & "' "
            db.ExecuteSQL(fSql)
        End If
        rs.Close()

        fSql = " SELECT DISTINCT DCONSO, DIDIVI FROM RDCC  "
        fSql &= " INNER JOIN RCQCOND ON DIDIVI = KDIVI  "
        fSql &= " WHERE  "
        fSql &= "    DCONSO    = '" & Conso & "' "
        fSql &= " AND DUDS02   = 0 "
        fSql &= " AND KSLMER   = '-- TODOS --' "
        fSql &= " AND KLINE    = '-- TODOS --' "
        fSql &= " AND KSEGM    = '-- TODOS --' "
        fSql &= " AND KMARCA   = '-- TODOS --' "
        fSql &= " AND KPROD    = '-- TODOS --' "
        fSql &= " AND KCUSTSEL = '-- TODOS --' "
        fSql &= " AND KSHIPSEL = '-- TODOS --' "
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            fSql = "UPDATE RDCC SET DUDS02 = 1 "
            fSql &= " WHERE  "
            fSql &= "    DCONSO    = '" & rs("DCONSO").Value & "' "
            fSql &= "    AND DIDIVI = '" & rs("DIDIVI").Value & "' "
            db.ExecuteSQL(fSql)
        End If
        rs.Close()

        fSql = " SELECT DISTINCT DCONSO, DSLMER FROM RDCC  "
        fSql &= " INNER JOIN RCQCOND ON DSLMER = KSLMER  "
        fSql &= " WHERE  "
        fSql &= "    DCONSO    = '" & Conso & "' "
        fSql &= " AND DUDS02   = 0 "
        fSql &= " AND KDIVI    = '-- TODOS --' "
        fSql &= " AND KLINE    = '-- TODOS --' "
        fSql &= " AND KSEGM    = '-- TODOS --' "
        fSql &= " AND KMARCA   = '-- TODOS --' "
        fSql &= " AND KPROD    = '-- TODOS --' "
        fSql &= " AND KCUSTSEL = '-- TODOS --' "
        fSql &= " AND KSHIPSEL = '-- TODOS --' "
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            fSql = "UPDATE RDCC SET DUDS02 = 1 "
            fSql &= " WHERE  "
            fSql &= "    DCONSO    = '" & rs("DCONSO").Value & "' "
            fSql &= "    AND DSLMER = '" & rs("DSLMER").Value & "' "
            db.ExecuteSQL(fSql)
        End If
        rs.Close()

        fSql = " SELECT DISTINCT DCONSO, DIDIVI, DLINEAP, DSEGM FROM RDCC  "
        fSql &= " INNER JOIN RCQCOND  ON DIDIVI = KDIVI "
        fSql &= "                   AND DLINEAP = KLINE "
        fSql &= "                   AND DSEGM   = KSEGM "
        fSql &= " WHERE  "
        fSql &= "    DCONSO    = '" & Conso & "' "
        fSql &= " AND DUDS02   = 0 "
        fSql &= " AND KSLMER   = '-- TODOS --' "
        fSql &= " AND KMARCA   = '-- TODOS --' "
        fSql &= " AND KPROD    = '-- TODOS --' "
        fSql &= " AND KCUSTSEL = '-- TODOS --' "
        fSql &= " AND KSHIPSEL = '-- TODOS --' "
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            fSql = "UPDATE RDCC SET DUDS02 = 1 "
            fSql &= " WHERE  "
            fSql &= "    DCONSO    = '" & rs("DCONSO").Value & "' "
            fSql &= "    AND DLINEAP = '" & rs("DLINEAP").Value & "' "
            fSql &= "    AND DSEGM = '" & rs("DSEGM").Value & "' "
            db.ExecuteSQL(fSql)
        End If
        rs.Close()

        fSql = " SELECT DISTINCT DCONSO, DMARCA, DCUST FROM RDCC  "
        fSql &= " INNER JOIN RCQCOND  ON DMARCA = KMARCA "
        fSql &= "                   AND DCUST  = KCUST "
        fSql &= " WHERE  "
        fSql &= "    DCONSO    = '" & Conso & "' "
        fSql &= " AND DUDS02   = 0 "
        fSql &= " AND KSLMER   = '-- TODOS --' "
        fSql &= " AND KDIVI    = '-- TODOS --' "
        fSql &= " AND KLINE    = '-- TODOS --' "
        fSql &= " AND KSEGM    = '-- TODOS --' "
        fSql &= " AND KPROD    = '-- TODOS --' "
        fSql &= " AND KSHIPSEL = '-- TODOS --' "
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            fSql = "UPDATE RDCC SET DUDS02 = 1 "
            fSql &= " WHERE  "
            fSql &= "    DCONSO    = '" & rs("DCONSO").Value & "' "
            fSql &= "    AND DMARCA = '" & rs("DMARCA").Value & "' "
            fSql &= "    AND DCUST = " & rs("DCUST").Value
            db.ExecuteSQL(fSql)
        End If
        rs.Close()

        fSql = " SELECT DCONSO, DCUST FROM RDCC  "
        fSql &= " INNER JOIN RCQCOND  ON DCUST = KCUST "
        fSql &= " WHERE  "
        fSql &= "    DCONSO    = '" & Conso & "' "
        fSql &= " AND DUDS02   = 0 "
        fSql &= " AND KSLMER   = '-- TODOS --' "
        fSql &= " AND KDIVI    = '-- TODOS --' "
        fSql &= " AND KLINE    = '-- TODOS --' "
        fSql &= " AND KSEGM    = '-- TODOS --' "
        fSql &= " AND KMARCA   = '-- TODOS --' "
        fSql &= " AND KPROD    = '-- TODOS --' "
        fSql &= " AND KSHIPSEL = '-- TODOS --' "
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            fSql = "UPDATE RDCC SET DUDS02 = 1 "
            fSql &= " WHERE  "
            fSql &= "    DCONSO    = '" & rs("DCONSO").Value & "' "
            fSql &= "    AND DCUST = " & rs("DCUST").Value
            db.ExecuteSQL(fSql)
        End If
        rs.Close()

        fSql = " SELECT DCONSO, DCUST, DPROD FROM RDCC  "
        fSql &= " INNER JOIN RCQCOND  ON DCUST = KCUST "
        fSql &= "                   AND DPROD = KPROD "
        fSql &= " WHERE  "
        fSql &= "    DCONSO    = '" & Conso & "' "
        fSql &= " AND DUDS02   = 0 "
        fSql &= " AND KSLMER   = '-- TODOS --' "
        fSql &= " AND KDIVI    = '-- TODOS --' "
        fSql &= " AND KLINE    = '-- TODOS --' "
        fSql &= " AND KSEGM    = '-- TODOS --' "
        fSql &= " AND KMARCA   = '-- TODOS --' "
        fSql &= " AND KSHIPSEL = '-- TODOS --' "
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            fSql = "UPDATE RDCC SET DUDS02 = 1 "
            fSql &= " WHERE  "
            fSql &= "    DCONSO    = '" & rs("DCONSO").Value & "' "
            fSql &= "    AND DCUST = " & rs("DCUST").Value
            fSql &= "    AND DPROD = '" & rs("DPROD").Value & "' "
            db.ExecuteSQL(fSql)
        End If
        rs.Close()

        fSql = " SELECT DCONSO, DCUST, DSHIP FROM RDCC  "
        fSql &= " INNER JOIN RCQCOND  ON DCUST = KCUST "
        fSql &= "                   AND DSHIP = KSHIP "
        fSql &= " WHERE  "
        fSql &= "    DCONSO    = '" & Conso & "' "
        fSql &= " AND DUDS02   = 0 "
        fSql &= " AND KSLMER   = '-- TODOS --' "
        fSql &= " AND KDIVI    = '-- TODOS --' "
        fSql &= " AND KLINE    = '-- TODOS --' "
        fSql &= " AND KSEGM    = '-- TODOS --' "
        fSql &= " AND KMARCA   = '-- TODOS --' "
        fSql &= " AND KPROD    = '-- TODOS --' "
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            fSql = "UPDATE RDCC SET DUDS02 = 1 "
            fSql &= " WHERE  "
            fSql &= "    DCONSO    = '" & rs("DCONSO").Value & "' "
            fSql &= "    AND DCUST = " & rs("DCUST").Value
            fSql &= "    AND DSHIP = " & rs("DSHIP").Value
            db.ExecuteSQL(fSql)
        End If
        rs.Close()

        fSql = " SELECT DCONSO, DCUST, DSHIP, DPROD FROM RDCC  "
        fSql &= " INNER JOIN RCQCOND  ON DCUST = KCUST "
        fSql &= "                   AND DSHIP = KSHIP "
        fSql &= "                   AND DPROD = KPROD "
        fSql &= " WHERE  "
        fSql &= "    DCONSO    = '" & Conso & "' "
        fSql &= " AND DUDS02   = 0 "
        fSql &= " AND KSLMER   = '-- TODOS --' "
        fSql &= " AND KDIVI    = '-- TODOS --' "
        fSql &= " AND KLINE    = '-- TODOS --' "
        fSql &= " AND KSEGM    = '-- TODOS --' "
        fSql &= " AND KMARCA   = '-- TODOS --' "
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            fSql = "UPDATE RDCC SET DUDS02 = 1 "
            fSql &= " WHERE  "
            fSql &= "    DCONSO    = '" & rs("DCONSO").Value & "' "
            fSql &= "    AND DCUST = " & rs("DCUST").Value
            fSql &= "    AND DSHIP = " & rs("DSHIP").Value
            fSql &= "    AND DPROD = '" & rs("DPROD").Value & "' "
            db.ExecuteSQL(fSql)
        End If
        rs.Close()
        rs = Nothing

    End Sub

    Sub mkSql(tipoF As String, Campo As String, Valor As String, Optional fin As Single = 0)
        mkSqlG(fSql, vSql, tipoF, Campo, Valor, fin)
    End Sub
    Sub mkSql(tipoF As String, Campo As String, Valor As Double, Optional fin As Single = 0)
        mkSqlG(fSql, vSql, tipoF, Campo, Valor, fin)
    End Sub

End Class
