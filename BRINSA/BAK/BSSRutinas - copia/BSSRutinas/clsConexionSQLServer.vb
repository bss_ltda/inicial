﻿Option Explicit On

Imports ADODB
Imports BSSRutinas.modConstantes

Public Class clsConexionSQLServer
    Public DB As ADODB.Connection
    Public gsDatasource As String
    Public gsLib As String
    Public gsConnstring As String
    Public gsUser As String
    Public gsPassword As String
    Public gsAppName As String
    Public gsAppPath
    Public gsAppVersion As String
    Public gsKeyWords As String

    Public ModuloActual As String = ""
    Public lastError As String = ""
    Public lastSQL As String = ""
    Public bDateTimeToChar As Boolean = False
    Dim regA As Double


    Function Conexion() As Boolean
        Dim gsProvider As String = "SQLOLEDB"
        Dim rs As New ADODB.Recordset

        gsKeyWords = ""
        gsConnstring = "Provider={PROVIDER};Password={PASSWORD};Persist Security Info=True;User ID={USER};Initial Catalog={CATALOG};Data Source={DATASOURCE}"

        gsConnstring = gsConnstring.Replace("{PROVIDER}", gsProvider)
        gsConnstring = gsConnstring.Replace("{PASSWORD}", gsPassword)
        gsConnstring = gsConnstring.Replace("{USER}", gsUser)
        gsConnstring = gsConnstring.Replace("{CATALOG}", gsLib)
        gsConnstring = gsConnstring.Replace("{DATASOURCE}", gsDatasource)

        DB = New ADODB.Connection
        DB.Open(gsConnstring)
        Return DB.State = 1

    End Function

    Public Sub DropTable(Table As String)
        Try
            DB.Execute("DROP TABLE " & Table)
        Catch ex As Exception

        End Try
    End Sub

    Public Function ExecuteSQL(ByVal sql As String) As ADODB.Recordset
        Dim txtErr As String
        sql = CStr(sql)
        lastSQL = sql
        Try
            ExecuteSQL = DB.Execute(sql, regA)
        Catch ex As Exception
            wc(sql)
            If InStr(sql, "{{") = 0 And InStr(UCase(sql), "DROP TABLE") = 0 Then
                txtErr = Err.Number & " " & Err.Description
                WrtSqlError(CStr(sql), txtErr)
            End If
            ExecuteSQL = Nothing
        End Try

    End Function

    Public Function OpenRS(ByRef rs As ADODB.Recordset, fSql As String, CursorType As CursorTypeEnum, LockType As LockTypeEnum) As Boolean
        lastSQL = fSql
        rs.Open(fSql, DB, CursorType, LockType)
        Return Not rs.EOF

    End Function

    Public Function OpenRS(ByRef rs As ADODB.Recordset, fSql As String) As Boolean
        lastSQL = fSql
        rs.Open(fSql, DB)
        Return Not rs.EOF

    End Function

    Sub WrtSqlError(sql As String, ByVal Descrip As String)
        WrtSqlError(sql, Descrip, "")
    End Sub

    Sub WrtSqlError(sql As String, ByVal Descrip As String, ByVal EnviarA As String)
        Dim conn As New ADODB.Connection
        Dim fSql As String
        Dim vSql As String
        Dim aApl() As String
        Dim sApp As String = "VB.NET"

        Dim gsConnString As String = "Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=APLLX;Password=LXAPL;Persist Security Info=True;Default Collection=ERPLX834F;Force Translate=0;"
        If gsKeyWords.Trim <> "" Then
            Descrip = gsKeyWords & "<br>" & Descrip
        End If
        Descrip = Left(Descrip, 20000)

        Try
            aApl = Split(System.Reflection.Assembly.GetExecutingAssembly.FullName, ", ")
            If UBound(aApl) > 0 Then
                sApp = aApl(0) & IIf(ModuloActual <> "", "." & ModuloActual, "")
            End If

            conn.Open(gsConnString)
            fSql = " INSERT INTO RFLOG( "
            vSql = " VALUES ( "
            fSql &= " USUARIO   , " : vSql &= "'" & System.Net.Dns.GetHostName() & "', "      '//502A
            fSql &= " PROGRAMA  , " : vSql &= "'" & "" & sApp & "', "      '//502A
            fSql &= " ALERT     , " : vSql &= "1, "      '//1P0
            If EnviarA <> "" Then
                fSql &= " ALERTTO   , " : vSql &= "'" & EnviarA & "', "     '//152A  
            End If
            fSql &= " EVENTO    , " : vSql &= "'" & Replace(Descrip, "'", "''") & "', "      '//20002A
            fSql &= " LKEY      , " : vSql &= "'" & gsKeyWords & "', "     '//30A   Clave         
            fSql &= " TXTSQL    ) " : vSql &= "'" & Replace(sql, "'", "''") & "' ) "      '//5002A
            conn.Execute(fSql & vSql)
            conn.Close()
        Catch ex As Exception

        End Try

    End Sub


    Sub Bitacora(Programa As String, ByVal Descrip As String, Clave As String)
        Dim fSql As String
        Dim vSql As String
        Dim aApl() As String
        Dim sApp As String = "VB.NET"

        Dim gsConnString As String = "Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=APLLX;Password=LXAPL;Persist Security Info=True;Default Collection=ERPLX834F;Force Translate=0;"
        If gsKeyWords.Trim <> "" Then
            Descrip = gsKeyWords & "<br>" & Descrip
        End If
        Descrip = Left(Descrip, 20000)

        Try
            aApl = Split(System.Reflection.Assembly.GetExecutingAssembly.FullName, ", ")
            If UBound(aApl) > 0 Then
                sApp = aApl(0) & IIf(ModuloActual <> "", "." & ModuloActual, "")
            End If
            fSql = " INSERT INTO RFLOG( "
            vSql = " VALUES ( "
            fSql &= " USUARIO   , " : vSql &= "'" & System.Net.Dns.GetHostName() & "', "    '//502A
            fSql &= " LKEY      , " : vSql &= "'" & "" & Clave & "', "                      '
            fSql &= " PROGRAMA  , " : vSql &= "'" & "" & sApp & "', "                       '//502A
            fSql &= " EVENTO    , " : vSql &= "'" & Replace(Descrip, "'", "''") & "', "     '//20002A
            fSql &= " TXTSQL    ) " : vSql &= "'" & Replace(Programa, "'", "''") & "' ) "   '//5002A
            DB.Execute(fSql & vSql)
        Catch ex As Exception

        End Try

    End Sub


    Public Function getLXParam(prm As String) As String
        Return getLXParam(prm, "")
    End Function


    Public Function getLXParam(prm As String, Campo As String) As String
        Dim rs As New ADODB.Recordset
        Dim resultado As String = ""

        If Campo = "" Then
            Campo = "CCDESC"
        End If
        lastSQL = "SELECT " & Campo & " AS DATO FROM RFPARAM WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER('" & prm & "')"
        rs.Open(lastSQL, Me.DB)
        If Not rs.EOF Then
            resultado = rs("DATO").Value
        End If
        rs.Close()
        Return resultado.Trim

    End Function

    Public Sub setLXParam(codigo As String, Campo As String, Valor As String)
        Dim fSql As String

        If Campo = "" Then
            Campo = "CCDESC"
        End If
        fSql = "UPDATE RFPARAM SET  " & Campo & " = '" & Valor & "'"
        fSql &= " FROM RFPARAM "
        fSql &= " WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER('" & codigo & "')"
        ExecuteSQL(fSql)

    End Sub

    Public Sub setLXParamNum(codigo As String, Campo As String, Valor As String)
        Dim fSql As String

        If Campo = "" Then
            Campo = "CCDESC"
        End If
        fSql = "UPDATE RFPARAM SET  " & Campo & " = " & CDbl(Valor)
        fSql &= " WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER('" & codigo & "')"
        ExecuteSQL(fSql)

    End Sub


    Public Function regActualizados() As Long
        Return regA
    End Function

    Public Function Login(Usuario As String, Password As String) As Boolean
        Dim rs As New ADODB.Recordset
        Dim resultado As Boolean = False
        lastSQL = "SELECT UUSR FROM RCAU WHERE UUSR='" & Usuario & "' AND UPASS='" & Password & "'"
        rs.Open(lastSQL, DB)
        If Not rs.EOF Then
            resultado = True
        End If
        rs.Close()
        Return resultado

    End Function


    Public Function CalcConsec(ByVal sID As String, ByVal TopeMax As String) As String
        Dim rs As New ADODB.Recordset
        Dim locID As String
        Dim sql As String = ""
        Dim sConsec As String = ""
        Dim tMax As Double
        Dim sFmt As String
        Dim nDig As Integer

        Try
            nDig = Len(TopeMax)
            sFmt = New String("0", nDig)

            sql = "SELECT CCDESC FROM ZCCL01 WHERE CCTABL = 'SECUENCE' AND CCCODE='" & sID & "'"
            lastSQL = sql
            tMax = Val(TopeMax)

            With rs
                .CursorLocation = ADODB.CursorLocationEnum.adUseServer
                .Open(sql, DB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)
                If Not (.BOF And .EOF) Then
                    locID = Val(.Fields("CCDESC").Value) + 1
                    If locID > tMax Then
                        locID = 1
                    End If
                    sConsec = Right(sFmt & locID, nDig)
                    .Fields("CCDESC").Value = sConsec
                    .Update()
                    .Close()
                Else
                    locID = 1
                    .Close()
                    sConsec = Right(sFmt & locID, nDig)
                    sql = " INSERT INTO ZCC (CCID, CCTABL, CCCODE, CCDESC ) " & " VALUES( 'CC', 'SECUENCE', '" & sID & "', '" & sConsec & "' )"
                    DB.Execute(sql)
                End If
            End With
        Catch ex As Exception
            WrtSqlError(sql, Err.Description)
        End Try
        Return sConsec

    End Function

    Public Function CheckVer() As Boolean
        Dim rs As New ADODB.Recordset
        Dim lVer As String
        Dim msg As String
        Dim NomApp As String
        Dim App As clsApp = New clsApp(System.Reflection.Assembly.GetExecutingAssembly)
        Dim Resultado As Boolean = True

        gsAppPath = App.Path
        gsAppName = App.EXEName
        gsAppVersion = App.Major.ToString & "." & App.Minor.ToString & "." & App.Revision.ToString

        lVer = "No hay registro"

        lastSQL = "SELECT CCSDSC, CCUDC1, CCNOT2, CCDESC  FROM ZCCL01 WHERE CCTABL='RFVBVER' AND UPPER(CCCODE) = '" & gsAppName.ToUpper & "'"
        rs.Open(lastSQL, DB)
        If Not (rs.EOF) Then
            lVer = Trim(rs("CCSDSC").Value)
            NomApp = rs("CCDESC").Value
            If InStr(lVer, "*NOCHK") > 0 Then
                Resultado = True
            ElseIf InStr(lVer, "(" & gsAppVersion & ")") > 0 Then
                Resultado = True
            End If
            If Resultado And rs("CCUDC1").Value = 0 Then
                msg = "La aplicacion " & NomApp & " no se puede usar en este momento." & vbCr
                msg = msg & "Razon: " & vbCr
                msg = msg & "=========================================" & vbCr
                If Trim(rs("CCNOT2").Value) = "" Then
                    msg = msg & "Aplicacion en Mantenimiento." & vbCr
                Else
                    msg = msg & rs("CCNOT2").Value & vbCr
                End If
                msg = msg & "=========================================" & vbCr
                rs.Close()
                Return False
            End If
            rs.Close()
        End If

        If Not Resultado Then
            msg = "Aplicacion " & App.EXEName.ToUpper & vbCr & _
                   "=========================================" & vbCr & _
                   "Versión incorrecta del programa." & vbCr & _
                   "Versión Registrada: " & lVer & vbCr & _
                   "Versión Actual: " & "(" & gsAppVersion & ")"
            lastError = msg
        End If

        Return Resultado

    End Function

    Sub Close()
        DB.Close()
    End Sub

End Class
