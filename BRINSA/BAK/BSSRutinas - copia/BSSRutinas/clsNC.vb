﻿Public Class clsNC
    Public Periodo As String
    Public Usuario As String

    Dim NCCod As Int16
    Dim Planta As String = "10"
    Dim NumeroProceso As String
    Dim Paquete As String
    Dim PEnvio As String
    Dim ZonaVentas As String
    Dim rsNC As ADODB.Recordset

    Sub LiquidaNC57()  'Actas de destruccion
        Dim rs As New ADODB.Recordset, fSql As String
        Dim datos As New clsDatosNegocio
        Dim Precio As Double
        Dim Margen As Double
        Dim Costo As Double
        Dim i As Int16
        Dim Usuario As String = ""

        'Revisar si NCSTS0 esta en uno para pasar la solicitud al siguiente aprobador.

        'SELECT NCCUST, NCSHIP FROM RFNCH WHERE NCDOCNR = 11000890
        fSql = " SELECT AIDNUM, NCCUST, NCSHIP, APROD, ALOTE, (AUORD / IVULP) AS AQORD, IDESC, NCSTS0, NCSOLICT "
        fSql &= " FROM RFNCH INNER JOIN RFNCL ON NCDOCNR = ADOCNR "
        fSql &= " INNER JOIN IIM ON APROD = IPROD "
        fSql &= " WHERE NCDOCNR = " & NumeroProceso
        rs = DB.ExecuteSQL(fSql)
        i = 1
        If Not rs.EOF Then
            'Ya se liquido, pasar al siguiente nivel
            If rs("NCSTS0").Value = 1 Then
                fSql = " UPDATE RFNCH SET "
                fSql &= "   NCSTS1 = 1 "
                fSql &= " , FESTADO = 'En Revision'"
                fSql &= " WHERE NCDOCNR = " & NumeroProceso
                DB.ExecuteSQL(fSql)
                rs.Close()
                Return
            End If
            Usuario = rs("NCSOLICT").Value
        End If
        Do While Not rs.EOF
            With datos
                .Cliente = rs("NCCUST").Value
                .PtoEnvio = rs("NCSHIP").Value
                .Producto = rs("APROD").Value
                Precio = .getPrecio()
                Margen = .getMargen()
                Costo = .getCostoProducto()

                fSql = " UPDATE RFNCL SET  "
                fSql &= "   ALINE   = " & i
                fSql &= " , APRODD  = '" & rs("IDESC").Value & "'"
                fSql &= " , AQORD   = " & rs("AQORD").Value
                fSql &= " , AVALOR  = " & rs("AQORD").Value * (Precio * Margen / 100)
                fSql &= " , ACOSTO  = " & Costo
                fSql &= " , APRECIO = " & Precio
                fSql &= " , AMARGEN = " & (100 - Margen)
                fSql &= " WHERE "
                fSql &= "    AIDNUM = " & rs("AIDNUM").Value
                DB.ExecuteSQL(fSql)
                i += 1
            End With

            rs.MoveNext()
        Loop
        rs.Close()

        'fSql = DB.getLXParam("SITIO_CARPETA") & "\LDmda.bat " & DB.getLXParam("SITIO_CARPETA") & " " & Conso
        fSql = """C:\Program Files (x86)\BSSLTDA\BSSRutinas2.0\BSSRutinas2.0.exe"" SITIO=LXLONG PROCESO=PRAD NUMPRAD=" & NumeroProceso
        Shell(fSql, AppWinStyle.Hide, True)


        fSql = " UPDATE RFNCH SET "
        fSql &= "   NCSTS0 = 1"
        fSql &= " , FESTADO = 'Firmar Planilla'"
        fSql &= " , NCVALOR = (SELECT SUM(AVALOR) FROM RFNCL WHERE ADOCNR = " & NumeroProceso & ") "
        fSql &= " WHERE NCDOCNR = " & NumeroProceso
        DB.ExecuteSQL(fSql)

        Dim Aviso As New clsCorreoMHT
        With Aviso
            .Inicializa()
            .DB = DB
            .Referencia = "Acta de Destruccion"
            .Asunto = "Planilla de Reporte de Acta de Destruccion"
            .Modulo = "NC"
            .Destinatario = Usuario
            .AddMHT(DB.getLXParam("SERVER_RAIZ") & "tm/E2/nc57_acta.asp?ADOCNR=" & NumeroProceso)
            .Adjuntar(DB.getLXParam("C_UPLOAD_DIR") & "pdf\ActaDestruccion\PRAD" & NumeroProceso & ".pdf")
            .EnviaCorreo()
        End With


    End Sub

    Sub AcuerdoComercialNC22()
        Dim rs As New ADODB.Recordset
        Dim fSql, wSql As String
        Dim setDatos As String

        Console.WriteLine(Paquete)
        NCCod = 22

        fSql = " SELECT  "
        fSql &= "   ACNUMERO   AS No_Acuerdo "
        fSql &= " , ACCUST     AS Cliente "
        fSql &= " , ACCAUSAL   AS Causal_NC "
        fSql &= " , ACSUBCAU   AS Concepto "
        fSql &= " , ACZONAVEND AS Zona "
        fSql &= " , ACNEG01P   AS Porc_Ciudado_Hogar "
        fSql &= " , ACNEG01V   AS Valor_Cuidado_Hogar "
        fSql &= " , ACNEG02P   AS Porc_Alimentos "
        fSql &= " , ACNEG02V   AS Valor_Alimentos "
        fSql &= " , ACSOLICT   AS Solicitante"
        fSql &= "  FROM RFNCAC "
        fSql &= "  WHERE ACCAUSAL = 22 AND ACID = 'AC' AND ACSTS1 = 1 "
        rs = DB.ExecuteSQL(fSql)
        Do While Not rs.EOF
            Usuario = rs("Solicitante").Value
            wSql = "DMPER = " & Periodo & " AND DMNEGOCIO = 'CUIDADO DEL HOGAR' AND DMCLI = " & rs("Cliente").Value
            wSql &= " AND DMZONAVEND = '" & rs("Zona").Value & "' "

            If setZonaVentas(wSql) Then
                setDatos = DB.CalcConsec("NCSETDATOS", "99999999")
                fSql = "UPDATE RFNCDM SET DMSET = " & setDatos
                DB.ExecuteSQL(fSql & " WHERE " & wSql)

                If rs("Porc_Ciudado_Hogar").Value > 0 Then
                    fSql = " UPDATE RFNCDM SET  "
                    fSql &= " DMNC22P = DMNETO * " & rs("Porc_Ciudado_Hogar").Value & " / 100 "
                    fSql &= " WHERE DMSET = " & setDatos
                    DB.ExecuteSQL(fSql)
                    preNC_AC(rs, setDatos, "Porcentaje")
                End If
                If rs("Valor_Cuidado_Hogar").Value > 0 Then
                    'Factor prorrateo
                    fSql = " UPDATE RFNCDM  DM SET DMFACT01 = DMNETO / (  "
                    fSql &= " 	SELECT SUM(DMNETO) FROM RFNCDM WHERE DMSET = " & setDatos
                    fSql &= " )  "
                    fSql &= " WHERE DMSET = " & setDatos
                    DB.ExecuteSQL(fSql)

                    'Distribuir valor
                    fSql = "UPDATE RFNCDM SET  "
                    fSql &= " DMNC22V = DMFACT01 * " & rs("Valor_Cuidado_Hogar").Value
                    fSql &= " WHERE DMSET = " & setDatos
                    DB.ExecuteSQL(fSql)

                    preNC_AC(rs, setDatos, "Valor")

                End If
            End If

            wSql = "DMPER = " & Periodo & " AND DMNEGOCIO = 'ALIMENTOS' AND DMDONABLAN = 0 AND DMCLI = " & rs("Cliente").Value
            wSql &= " AND DMZONAVEND = '" & rs("Zona").Value & "' "
            If setZonaVentas(wSql) Then
                setDatos = DB.CalcConsec("NCSETDATOS", "99999999")
                fSql = "UPDATE RFNCDM SET DMSET = " & setDatos
                DB.ExecuteSQL(fSql & " WHERE " & wSql)

                If rs("Porc_Alimentos").Value > 0 Then
                    fSql = " UPDATE RFNCDM SET  "
                    fSql &= " DMNC22P = DMNETO * " & rs("Porc_Alimentos").Value & " / 100 "
                    fSql &= " WHERE DMSET = " & setDatos
                    DB.ExecuteSQL(fSql)
                    preNC_AC(rs, setDatos, "Porcentaje")
                End If
                If rs("Valor_Alimentos").Value > 0 Then
                    fSql = " UPDATE RFNCDM  DM SET DMFACT01 = DMNETO / (  "
                    fSql &= " 	SELECT SUM(DMNETO) FROM RFNCDM WHERE DMSET = " & setDatos
                    fSql &= " )  "
                    fSql &= " WHERE DMSET = " & setDatos
                    DB.ExecuteSQL(fSql)

                    fSql = "UPDATE RFNCDM SET  "
                    fSql &= " DMNC22V = DMFACT01 * " & rs("Valor_Alimentos").Value
                    fSql &= " WHERE " & wSql
                    DB.ExecuteSQL(fSql)
                    preNC_AC(rs, setDatos, "Valor")
                End If
            End If
            rs.MoveNext()
        Loop
        rs.Close()
        rs = Nothing

    End Sub

    Sub preNC_AC(rs As ADODB.Recordset, setDatos As String, tipoValor As String)
        Dim fSql, vSql As String

        fSql = " INSERT INTO RFNCH( "
        vSql = " SELECT "
        fSql &= " NCCAUSAL  , " : vSql &= " " & rs("Causal_NC").Value & "  , "      '//2P0   Causal
        fSql &= " NCSUBCAU  , " : vSql &= " " & rs("Concepto").Value & "  , "       '//3P0   Subcausal
        fSql &= " NCCUST    , " : vSql &= " " & rs("Cliente").Value & "    , "      '//8P0   Cliente
        fSql &= " NCVALOR   , " : vSql &= "     SUM(DMNC22P)   , "                  '//19P7  Valor NC
        fSql &= " NCPAQ     , " : vSql &= " " & Paquete & "     , "                 '//8P0   Paquete NCs
        fSql &= " NCSOLICT  , " : vSql &= "'" & rs("Solicitante").Value & "', "     '//10A   Solicitante
        fSql &= " NCACUERDO ) " : vSql &= " " & rs("No_Acuerdo").Value & "  "       '//8P0   No. Acuerdo
        vSql &= " FROM RFNCDM"
        vSql &= " WHERE DMSET = " & setDatos
        DB.ExecuteSQL(fSql & vSql)
        NumeroProceso = DB.ultimoRegNum()
        NC_Deta(NumeroProceso, setDatos, tipoValor)

    End Sub

    Sub NC_Deta(NumeroProceso As String, setDatos As String, tipoValor As String)
        Dim fSql, vSql As String
        Dim Planta As String = "10"

        fSql = " INSERT INTO RFNCL( "
        vSql = " SELECT "
        fSql &= " ADOCTP    , " : vSql &= "'N', "                             '//1A    Tipo Inter/acci
        fSql &= " ADOCNR    , " : vSql &= " " & NumeroProceso & "    , "      '//8P0   Documen
        fSql &= " ACUST     , " : vSql &= " DMCLI    , "      '//8P0   Cliente
        fSql &= " ASHIP     , " : vSql &= " " & PEnvio & "     , "      '//4P0   Punto de Envio
        fSql &= " ACZONVEND , " : vSql &= "'" & ZonaVentas & "', "     '//30A   Zona Ventas
        fSql &= " ACPAQ     , " : vSql &= " " & Paquete & "     , "                 '//8P0   Paquete NCs

        Select Case Planta
            Case "10"
                fSql &= " APREF     , " : vSql &= " MAX(NCPFX)     , "      '//2A    Prefijo
                fSql &= " AREAS     , " : vSql &= " MAX(NCCODFIN)  , "      '//5A    Codigo Razon Contable
                fSql &= " ATIPPE    , " : vSql &= " MAX(NCTIPPE)   , "      '//1A    Tipo de Pedido
            Case "20"
                fSql &= " APREF     , " : vSql &= " NCPFX20    , "      '//2A    Prefijo
                fSql &= " AREAS     , " : vSql &= " NCCODFIN20 , "      '//5A    Codigo Razon Contable
                fSql &= " ATIPPE    , " : vSql &= " NCTIPPE20  , "      '//1A    Tipo de Pedido
            Case Else

        End Select
        fSql &= " ACLAPE    , " : vSql &= " MAX(NCCLAPE) , "      '//3P0   Clase de pedido
        'fSql &=  " ANOTE     , " : vSql &=  "'" & Nota & "', "   '//256A  Nota al pedido
        fSql &= " ALINE     , " : vSql &= " ROW_NUMBER() OVER(ORDER BY DMPROD, CCCODEN ), "      '//4P0   Linea del pedido
        fSql &= " APROD     , " : vSql &= " DMPROD       , "     '//35A   Producto
        fSql &= " APRODD    , " : vSql &= " MAX(IDESC)   , "     '//35A   Producto
        fSql &= " ACNEGOCIO , " : vSql &= " MAX(DMNEGOCIO)   , "     '//35A   Producto
        fSql &= " AQORD     , " : vSql &= " CCCODEN      , "     '//11P3  Cantidad
        fSql &= " AVALOR0   , " : vSql &= " SUM(DMNETO * CCCODEN2)  , "     '//19P7  Valor
        Select Case tipoValor
            Case "Porcentaje"
                fSql &= " AVALOR    , " : vSql &= " ( SUM(DMNC22P * CCCODEN2) - 1 )   , "       '//19P7  Valor
            Case "Valor"
                fSql &= " AVALOR    , " : vSql &= " ( SUM(DMNC22V * CCCODEN2) - 1 )   , "       '//19P7  Valor
        End Select

        fSql &= " ACORAIN   , " : vSql &= "  '01', "             '//2A Codigo Razon Inventarios
        fSql &= " ACRTUSR   ) " : vSql &= " '" & Usuario & "'"   '//10A   Usuario
        vSql &= " FROM RFNCDM "
        vSql &= " INNER JOIN RFPARAM ON CCTABL = 'NCCOMPENSADETA'"
        vSql &= " INNER JOIN IIM ON DMPROD = IPROD "
        vSql &= " INNER JOIN RFNCPRM ON NCCOD = " & NCCod
        vSql &= " WHERE DMSET = " & setDatos
        vSql &= " Group By "
        vSql &= "   DMPER, DMCLI, DMPROD, CCCODEN "

        DB.ExecuteSQL(fSql & vSql)

    End Sub

    Sub AcuerdoComercialNC23(wrkId As String)
        Dim wrkIdNew, fSql, vSql As String
        Dim rs As New ADODB.Recordset
        Dim Precio As Double

        fSql = " UPDATE RFNCAC U SET  ( ACPRECIO  )  = "
        fSql &= "    ( "
        fSql &= "        SELECT "
        fSql &= "            IFNULL( MAX(LPRETO) , 0 ) "
        fSql &= "        FROM "
        fSql &= "            RLISPRE "
        fSql &= "        WHERE "
        fSql &= "            LESTADO = 'LA' "
        fSql &= "            AND DATE(NOW()) BETWEEN LFECIN AND LFECFI "
        fSql &= "            AND U.ACCUST = LCLIEN "
        fSql &= "            AND U.ACSHIP = LPTOEN "
        fSql &= "            AND U.ACPROD = LPROD "
        fSql &= "        GROUP BY "
        fSql &= "            LCLIEN "
        fSql &= "          , LPTOEN "
        fSql &= "          , LPROD "
        fSql &= "    ) "
        fSql &= " WHERE "
        fSql &= "    ACCAUSAL = 23 AND ACID = 'AC' "
        DB.ExecuteSQL(fSql)

        wrkIdNew = DB.CalcConsec("RFWRKGRL", "999999999")
        fSql = " INSERT INTO RFWRKGRL( " : vSql = " SELECT "
        fSql &= " WRKID     ,    " : vSql &= wrkIdNew
        fSql &= " WCATEG01  ,    " : vSql &= "  , 'NC' "
        fSql &= " WCATEG02  ,    " : vSql &= "  , '23' "
        fSql &= " WCUST     ,    " : vSql &= "  , A.ACCUST "
        fSql &= " WPROD     ,    " : vSql &= "  , A.ACPROD "
        fSql &= " WVAL05    ,    " : vSql &= "  , MAX(A.ACPRECIO) "
        fSql &= " WVAL01    ,    " : vSql &= "  , MAX(A.ACTON) "
        fSql &= " WVAL02    ,    " : vSql &= "  , MAX(A.ACNEG04P) "
        fSql &= " WVAL03    )    " : vSql &= "  , MAX(TONS) "
        vSql &= " FROM "
        vSql &= "    ( "
        vSql &= "        SELECT "
        vSql &= "            A.ACCUST "
        vSql &= "          , A.ACPROD "
        vSql &= "          , A.ACTON "
        vSql &= "          , A.ACNEG04P "
        vSql &= "          , A.ACPRECIO "
        vSql &= "          , W.WVAL03 - W.WVAL04 AS TONS "
        vSql &= "        FROM "
        vSql &= "            RFNCAC A "
        vSql &= "            INNER JOIN "
        vSql &= "                RFWRKGRL W "
        vSql &= "                ON "
        vSql &= "                    A.ACCUST     = W.WCUST "
        vSql &= "                    AND A.ACPROD = W.WPROD "
        vSql &= "        WHERE "
        vSql &= "            A.ACCAUSAL  = 23 "
        vSql &= "            AND W.WRKID = " & wrkId
        vSql &= "            AND ACTON   < ( W.WVAL03 - W.WVAL04 ) "
        vSql &= "    ) "
        vSql &= "    A "
        vSql &= " GROUP BY "
        vSql &= "    A.ACCUST "
        vSql &= "  , A.ACPROD "
        DB.ExecuteSQL(fSql & vSql)

        fSql = "SELECT "
        fSql &= "   WCUST  Cliente  "
        fSql &= " , WPROD  Producto "
        fSql &= " , IDESC  ProductoDesc "
        fSql &= " , WVAL01 TonBase "
        fSql &= " , WVAL02 Porcentaje "
        fSql &= " , WVAL03 TonModif "
        fSql &= " , WVAL05 Precio"
        fSql &= " FROM RFWRKGRL INNER JOIN IIM ON WPROD = IPROD"
        fSql &= " WHERE "
        fSql &= "   WRKID = " & wrkIdNew
        rs = DB.ExecuteSQL(fSql)
        Do While Not rs.EOF
            fSql = " INSERT INTO RFNCH( "
            vSql = " VALUES("
            fSql &= " NCCAUSAL  , " : vSql &= " 23  , " '//2P0   Causal
            fSql &= " NCSUBCAU  , " : vSql &= " 23  , " '//3P0   Subcausal
            fSql &= " NCCUST    , " : vSql &= " " & rs("Cliente").Value & "    , "      '//8P0   Cliente
            fSql &= " NCVALOR   , " : vSql &= rs("TonModif").Value * Precio & "  , "                  '//19P7  Valor NC
            fSql &= " NCPAQ     , " : vSql &= " " & Paquete & "     , "                 '//8P0   Paquete NCs
            fSql &= " NCSOLICT  ) " : vSql &= "'" & Usuario & "' ) "     '//10A   Solicitante
            DB.ExecuteSQL(fSql & vSql)
            NumeroProceso = DB.ultimoRegNum()

            fSql = " INSERT INTO RFNCL( "
            vSql = " VALUES( "
            fSql &= " ADOCTP    , " : vSql &= "'N', "                                  '//1A    Tipo Inter/acci
            fSql &= " ADOCNR    , " : vSql &= "  " & NumeroProceso & "    , "          '//8P0   Documen
            fSql &= " ACUST     , " : vSql &= "  " & rs("Cliente").Value & " , "       '//8P0   Cliente
            fSql &= " ACPAQ     , " : vSql &= "  " & Paquete & "     , "               '//8P0   Paquete NCs
            fSql &= " ATIPPE    , " : vSql &= " '" & rsNC("NCTIPPE").Value & "' , "      '//1A    Tipo de Pedido
            fSql &= " ACLAPE    , " : vSql &= "  " & rsNC("NCCLAPE").Value & " , "       '//3P0   Clase de pedido

            Select Case Planta
                Case "10"
                    fSql &= " APREF     , " : vSql &= " '" & rsNC("NCPFX").Value & "' , "      '//2A    Prefijo
                    fSql &= " AREAS     , " : vSql &= " '" & rsNC("NCCODFIN").Value & "'  , "      '//5A    Codigo Razon Contable
                    fSql &= " ACORAIN   , " : vSql &= "  '" & rsNC("NCCODINV").Value & "', "             '//2A Codigo Razon Inventarios
                Case "20"
                    fSql &= " APREF     , " : vSql &= " '" & rsNC("NCPFX20").Value & "' , "      '//2A    Prefijo
                    fSql &= " AREAS     , " : vSql &= " '" & rsNC("NCCODFIN20").Value & "'  , "      '//5A    Codigo Razon Contable
                    fSql &= " ACORAIN   , " : vSql &= "  '" & rsNC("NCCODINV20").Value & "', "             '//2A Codigo Razon Inventarios
                Case Else

            End Select
            fSql &= " ALINE     , " : vSql &= " 1, "      '//4P0   Linea del pedido
            fSql &= " APROD     , " : vSql &= " '" & rs("Producto").Value & "'       , "     '//35A   Producto
            fSql &= " APRODD    , " : vSql &= " '" & rs("ProductoDesc").Value & "' , "     '//35A   Producto
            fSql &= " ACNEGOCIO , " : vSql &= " 'Industria' , "     '//35A   Producto
            fSql &= " AQORD     , " : vSql &= " " & rs("TonModif").Value & "      , "     '//11P3  Cantidad
            fSql &= " AVALOR    , " : vSql &= " " & rs("Precio").Value & "  , "       '//19P7  Valor
            fSql &= " ACRTUSR   ) " : vSql &= " '" & Usuario & "' )"   '//10A   Usuario
            DB.ExecuteSQL(fSql & vSql)

            rs.MoveNext()
        Loop


    End Sub

    Sub ParametrosNC(codNC As String)
        Dim fSql As String

        rsNC = New ADODB.Recordset
        fSql = " SELECT "
        fSql &= "    NCTIPPE "
        fSql &= "  , NCCLAPE "
        fSql &= "  , NCPFX "
        fSql &= "  , NCCODFIN "
        fSql &= "  , NCCODINV "
        fSql &= "  , NCPFX20 "
        fSql &= "  , NCCODFIN20 "
        fSql &= "  , NCCODINV20 "
        fSql &= "  , NCTIPPE2 "
        fSql &= "  , NCCLAPE2 "
        fSql &= "  , NCPFX2 "
        fSql &= "  , NCCODFIN2 "
        fSql &= "  , NCCODINV2 "
        fSql &= " FROM "
        fSql &= "    RFNCPRM "
        fSql &= " WHERE "
        fSql &= "    NCCOD = " & codNC
        rsNC = DB.ExecuteSQL(fSql)

    End Sub

    Sub Cerrar()
        rsNC.Close()

    End Sub
    Function setZonaVentas(wSql As String) As Boolean
        Dim rs As New ADODB.Recordset
        Dim fSql As String
        Dim result As Boolean = False
        fSql = " SELECT  "
        fSql &= "  DMPENV, DMZONAVEND "
        fSql &= " FROM RFNCDM "
        fSql &= " WHERE " & wSql
        fSql &= " FETCH FIRST 1 ROWS ONLY "
        rs = DB.ExecuteSQL(fSql)
        If Not rs.Eof Then
            PEnvio = rs("DMPENV").Value
            ZonaVentas = rs("DMZONAVEND").Value
            result = True
        End If
        rs.Close()
        rs = Nothing
        Return result
    End Function

    Sub setPaquete(Paquete As String)
        Me.Paquete = Paquete
    End Sub

    Function getPaquete() As String
        Return Me.Paquete
    End Function

    Sub setNumeroProceso(NumeroProceso As String)
        Me.NumeroProceso = NumeroProceso
    End Sub

    Function getNumeroProceso() As String
        Return Me.NumeroProceso
    End Function

End Class
