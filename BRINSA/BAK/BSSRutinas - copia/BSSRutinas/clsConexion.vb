﻿Option Explicit On

Imports ADODB
Imports BSSRutinas.modConstantes

Public Class clsConexion
    Public DB As ADODB.Connection
    Public gsDatasource As String
    Public gsLib As String
    Public gsConnstring As String
    Public gsUser As String
    Public gsUserOP As String
    Public gsAppName As String
    Public gsAppPath
    Public gsAppVersion As String
    Public gsKeyWords As String
    Public appUser As String
    Public LXLONG As String
    Public LXLONG_CAMPO As String

    Public ModuloActual As String
    Public lastError As String
    Public lastSQL As String
    Public bDateTimeToChar As Boolean
    Public bEnDebug As Boolean

    Dim regA As Double


    Function Conexion() As Boolean
        Dim gsProvider As String = "IBMDA400"
        Dim Resultado As Boolean
        Dim rs As New ADODB.Recordset

        gsKeyWords = ""
        gsConnstring = "Provider=" & gsProvider & ";Data Source=" & gsDatasource & ";"
        gsConnstring = gsConnstring & "Persist Security Info=False;Default Collection=" & gsLib & ";"
        gsConnstring = gsConnstring & "Password=LXAPL;User ID=APLLX;"

        DB = New ADODB.Connection

        'DB.Open("Provider=IBMDA400.DataSource;Data Source=213.62.216.85;User ID=COSIS001;Password=ZXC14ASD;Persist Security Info=True;Convert Date Time To Char=TRUE;Application Name=DFU001;Default Collection=CP61BPCSF;Force Translate=0;")
        'Return True

        DB.Open(gsConnstring)

        Dim gsPass As String = getLXParam("LXPASS")
        gsUser = getLXParam("LXUSER")

        If gsUser <> "" And gsPass <> "" Then
            gsConnstring = "Provider=" & gsProvider & ";Data Source=" & gsDatasource & ";"
            gsConnstring = gsConnstring & "Persist Security Info=False;Default Collection=" & gsLib & ";"
            gsConnstring = gsConnstring & "Password=" & gsPass & ";User ID=" & gsUser & ";"
            gsConnstring = gsConnstring & "Force Translate=0;"
            If bDateTimeToChar Then
                gsConnstring &= "Convert Date Time To Char=FALSE;"
            End If
            DB.Close()
            DB.Open(gsConnstring)
            Resultado = True
        Else
            Resultado = False
        End If

        Return Resultado

    End Function

    Public Sub DropTable(Table As String)
        Try
            DB.Execute("DROP TABLE " & Table)
        Catch ex As Exception

        End Try
    End Sub

    Public Function ExisteTabla(Tabla As String) As Boolean
        Dim rs As New ADODB.Recordset
        Dim resultado As Boolean
        Dim fSql As String

        fSql = " SELECT TABLE_NAME "
        fSql &= " FROM   QSYS2.SYSTABLES "
        fSql &= " WHERE "
        fSql &= "    TABLE_NAME       = '" & Tabla & "' "
        If InStr(Tabla, ".") = 0 Then
            fSql &= "  AND TABLE_SCHEMA = '" & Me.gsLib & "' "
        End If
        rs.Open(fSql, DB)
        resultado = Not rs.EOF
        rs.Close()
        Return resultado

    End Function

    Public Function ExecuteSQL(ByVal sql As String) As ADODB.Recordset
        Dim txtError As String
        sql = CStr(sql)
        lastSQL = sql
        If Me.bEnDebug Then
            ExecuteSQL = DB.Execute(sql, regA)
        Else
            Try
                ExecuteSQL = DB.Execute(sql, regA)
            Catch ex As Exception
                If InStr(sql, "{{") = 0 And InStr(UCase(sql), "DROP TABLE") = 0 Then
                    txtError = ex.ToString.Replace("'", "''")
                    Tarea.setMsgError(txtError)
                    Tarea.GuardaError()
                    Console.WriteLine(ex.ToString)
                    WrtSqlError(CStr(sql), txtError)
                    lastError = ex.ToString
                End If
                ExecuteSQL = Nothing
            End Try
        End If

    End Function

    Sub ExecuteSQLError(sql As String)
        Try
            DB.Execute(sql)
        Catch ex As Exception
            wc(sql)
            wc(ex.ToString)
        End Try
    End Sub

    'ultimoRegNum()
    Function ultimoRegNum() As String
        Dim rs As New ADODB.Recordset, fSql As String
        Dim result As String = ""

        fSql = "SELECT IDENTITY_VAL_LOCAL() FROM SYSIBM.SYSDUMMY1"
        rs = ExecuteSQL(fSql)
        If Not rs.EOF Then
            result = rs(0).Value
        End If
        rs.Close()
        rs = Nothing
        Return result

    End Function

    Public Function rsOpen(ByRef rs As ADODB.Recordset, fSql As String, CursorType As CursorTypeEnum, LockType As LockTypeEnum) As Boolean
        lastSQL = fSql
        rs.Open(fSql, DB, CursorType, LockType)
        Return Not rs.EOF
    End Function
    Public Function OpenRS(ByRef rs As ADODB.Recordset, fSql As String, CursorType As CursorTypeEnum, LockType As LockTypeEnum) As Boolean
        lastSQL = fSql
        rs.Open(fSql, DB, CursorType, LockType)
        Return Not rs.EOF
    End Function

    Public Function rsOpen(ByRef rs As ADODB.Recordset, fSql As String) As Boolean
        lastSQL = fSql
        rs.Open(fSql, DB)
        Return Not rs.EOF
    End Function

    Public Function OpenRS(ByRef rs As ADODB.Recordset, fSql As String) As Boolean
        lastSQL = fSql
        rs.Open(fSql, DB)
        Return Not rs.EOF

    End Function

    Public Function SiExiste(fSql As String) As Boolean
        Dim rs As New ADODB.Recordset
        Dim resultado As Boolean
        lastSQL = fSql
        rs.Open(fSql & " FETCH FIRST  1 ROWS ONLY ", DB)
        resultado = Not rs.EOF
        rs.Close()
        Return resultado
    End Function


    Sub WrtSqlError(sql As String, ByVal Descrip As String)
        WrtSqlError(sql, Descrip, "")
    End Sub

    Sub WrtSqlError(sql As String, ByVal Descrip As String, ByVal EnviarA As String)
        Dim conn As New ADODB.Connection
        Dim fSql As String
        Dim vSql As String
        Dim aApl() As String
        Dim sApp As String = "VB.NET"

        Dim gsConnString As String = "Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=APLLX;Password=LXAPL;Persist Security Info=True;Default Collection=ERPLX834F;Force Translate=0;"
        If gsKeyWords.Trim <> "" Then
            Descrip = gsKeyWords & "<br>" & Descrip
        End If
        Descrip = Left(Descrip, 20000)

        Try
            aApl = Split(System.Reflection.Assembly.GetExecutingAssembly.FullName, ", ")
            If UBound(aApl) > 0 Then
                sApp = aApl(0) & IIf(ModuloActual <> "", "." & ModuloActual, "")
            End If

            conn.Open(gsConnString)
            fSql = " INSERT INTO RFLOG( "
            vSql = " VALUES ( "
            fSql &= " USUARIO   , " : vSql &= "'" & System.Net.Dns.GetHostName() & "', "      '//502A
            fSql &= " PROGRAMA  , " : vSql &= "'" & "" & sApp & "', "      '//502A
            fSql &= " ALERT     , " : vSql &= "1, "      '//1P0
            If EnviarA <> "" Then
                fSql &= " ALERTTO   , " : vSql &= "'" & EnviarA & "', "     '//152A  
            End If
            fSql &= " EVENTO    , " : vSql &= "'" & Replace(Descrip, "'", "''") & "', "      '//20002A
            fSql &= " LKEY      , " : vSql &= "'" & Left(gsKeyWords, 30) & "', "     '//30A   Clave         
            fSql &= " TXTSQL    ) " : vSql &= "'" & Left(Replace(sql, "'", "''"), 5000) & "' ) "      '//5002A
            conn.Execute(fSql & vSql)
            conn.Close()
        Catch ex As Exception
            lastError = ex.ToString()
        End Try

    End Sub


    Sub Bitacora(Programa As String, ByVal Descrip As String, Clave As String)
        Dim fSql As String
        Dim vSql As String
        Dim aApl() As String
        Dim sApp As String = "VB.NET"

        Dim gsConnString As String = "Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=APLLX;Password=LXAPL;Persist Security Info=True;Default Collection=ERPLX834F;Force Translate=0;"
        If gsKeyWords.Trim <> "" Then
            Descrip = gsKeyWords & "<br>" & Descrip
        End If
        Descrip = Left(Descrip, 20000)

        Try
            aApl = Split(System.Reflection.Assembly.GetExecutingAssembly.FullName, ", ")
            If UBound(aApl) > 0 Then
                sApp = aApl(0) & IIf(ModuloActual <> "", "." & ModuloActual, "")
            End If
            fSql = " INSERT INTO RFLOG( "
            vSql = " VALUES ( "
            fSql &= " USUARIO   , " : vSql &= "'" & System.Net.Dns.GetHostName() & "', "    '//502A
            fSql &= " LKEY      , " : vSql &= "'" & "" & Left(Clave, 30) & "', "                      '
            fSql &= " PROGRAMA  , " : vSql &= "'" & "" & sApp & "', "                       '//502A
            fSql &= " EVENTO    , " : vSql &= "'" & Replace(Descrip, "'", "''") & "', "     '//20002A
            fSql &= " TXTSQL    ) " : vSql &= "'" & Replace(Programa, "'", "''") & "' ) "   '//5002A
            DB.Execute(fSql & vSql)
        Catch ex As Exception
            lastError = ex.ToString()
        End Try

    End Sub


    Public Function getLXParam(prm As String) As String
        Return getLXParam(prm, "")
    End Function


    Public Function getLXParam(prm As String, Campo As String) As String
        Dim rs As New ADODB.Recordset
        Dim resultado As String = ""

        If Campo = "" Then
            Campo = LXLONG_CAMPO
        End If
        lastSQL = "SELECT " & Campo & " AS DATO FROM RFPARAM WHERE CCTABL='" & LXLONG & "' AND UPPER(CCCODE) = UPPER('" & prm & "')"
        rs.Open(lastSQL, Me.DB)
        If Not rs.EOF Then
            resultado = rs("DATO").Value
        End If
        rs.Close()
        Return resultado.Trim

    End Function

    Function getLXJobQ(Jobq As String) As String
        Dim jq As String

        jq = getLXParam(Jobq, "")
        getLXJobQ = IIf(jq <> "", jq, "QBATCH")

    End Function


    Public Sub setLXParam(codigo As String, Campo As String, Valor As String)
        Dim fSql As String

        If Campo = "" Then
            Campo = LXLONG_CAMPO
        End If
        fSql = "UPDATE RFPARAM SET  " & Campo & " = '" & Valor & "'"
        fSql &= " FROM RFPARAM "
        fSql &= " WHERE CCTABL='" & LXLONG & "' AND UPPER(CCCODE) = UPPER('" & codigo & "')"
        ExecuteSQL(fSql)

    End Sub

    Public Sub setLXParamNum(codigo As String, Campo As String, Valor As String)
        Dim fSql As String

        If Campo = "" Then
            Campo = LXLONG_CAMPO
        End If
        fSql = "UPDATE RFPARAM SET  " & Campo & " = " & CDbl(Valor)
        fSql &= " WHERE CCTABL='" & LXLONG & "' AND UPPER(CCCODE) = UPPER('" & codigo & "')"
        ExecuteSQL(fSql)

    End Sub


    Public Function regActualizados() As Long
        Return regA
    End Function

    Public Function Login(Usuario As String, Password As String) As Boolean
        Dim rs As New ADODB.Recordset
        Dim resultado As Boolean = False
        lastSQL = "SELECT UUSR FROM RCAU WHERE UUSR='" & Usuario & "' AND UPASS='" & Password & "'"
        rs.Open(lastSQL, DB)
        If Not rs.EOF Then
            resultado = True
        End If
        rs.Close()
        Return resultado

    End Function


    Public Function CalcConsec(ByVal sID As String, ByVal TopeMax As String) As String
        Dim rs As New ADODB.Recordset
        Dim locID As String
        Dim sql As String = ""
        Dim sConsec As String = ""
        Dim tMax As Double
        Dim sFmt As String
        Dim nDig As Integer
        Dim aTabla() As String
        Dim sTabla As String

        Try

            If sID.Contains(".") Then
                aTabla = sID.Split(".")
                sTabla = aTabla(0) & "." & "ZCCL01"
                sID = aTabla(1)
            Else
                sTabla = "ZCCL01"
            End If
            nDig = Len(TopeMax)
            sFmt = New String("0", nDig)

            sql = "SELECT CCDESC FROM " & sTabla & " WHERE CCTABL = 'SECUENCE' AND CCCODE='" & sID & "'"
            lastSQL = sql
            tMax = Val(TopeMax)

            With rs
                .CursorLocation = ADODB.CursorLocationEnum.adUseServer
                .Open(sql, DB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)
                If Not (.BOF And .EOF) Then
                    locID = Val(.Fields("CCDESC").Value) + 1
                    If locID > tMax Then
                        locID = 1
                    End If
                    sConsec = Right(sFmt & locID, nDig)
                    .Fields("CCDESC").Value = sConsec
                    .Update()
                    .Close()
                Else
                    locID = 1
                    .Close()
                    sConsec = Right(sFmt & locID, nDig)
                    sql = " INSERT INTO " & sTabla & " (CCID, CCTABL, CCCODE, CCDESC ) " & " VALUES( 'CC', 'SECUENCE', '" & sID & "', '" & sConsec & "' )"
                    DB.Execute(sql)
                End If
            End With
        Catch ex As Exception
            WrtSqlError(sql, Err.Description)
        End Try
        Return sConsec

    End Function

    Public Function CheckVer() As Boolean
        Dim rs As New ADODB.Recordset
        Dim lVer As String
        Dim msg As String
        Dim NomApp As String
        Dim App As clsApp = New clsApp(System.Reflection.Assembly.GetExecutingAssembly)
        Dim Resultado As Boolean = True

        gsAppPath = App.Path
        If gsAppName = "" Then
            gsAppName = App.EXEName
        End If
        gsAppVersion = App.Major.ToString & "." & App.Minor.ToString & "." & App.Revision.ToString

        lVer = "No hay registro"

        lastSQL = "SELECT CCSDSC, CCUDC1, CCNOT2, CCDESC  FROM ZCCL01 WHERE CCTABL='RFVBVER' AND UPPER(CCCODE) = '" & gsAppName.ToUpper & "'"
        rs.Open(lastSQL, DB)
        If Not (rs.EOF) Then
            lVer = Trim(rs("CCSDSC").Value)
            NomApp = rs("CCDESC").Value
            If InStr(lVer, "*NOCHK") > 0 Then
                Resultado = True
            ElseIf InStr(lVer, "(" & gsAppVersion & ")") > 0 Then
                Resultado = True
            End If
            If Resultado And rs("CCUDC1").Value = 0 Then
                msg = "La aplicacion " & NomApp & " no se puede usar en este momento." & vbCr
                msg = msg & "Razon: " & vbCr
                msg = msg & "=========================================" & vbCr
                If Trim(rs("CCNOT2").Value) = "" Then
                    msg = msg & "Aplicacion en Mantenimiento." & vbCr
                Else
                    msg = msg & rs("CCNOT2").Value & vbCr
                End If
                msg = msg & "=========================================" & vbCr
                rs.Close()
                Return False
            End If
            rs.Close()
        End If

        If Not Resultado Then
            msg = "Aplicacion " & App.EXEName.ToUpper & vbCr & _
                   "=========================================" & vbCr & _
                   "Versión incorrecta del programa." & vbCr & _
                   "Versión Registrada: " & lVer & vbCr & _
                   "Versión Actual: " & "(" & gsAppVersion & ")"
            lastError = msg
        End If

        Return Resultado

    End Function

    Sub Close()
        DB.Close()
    End Sub

    Public Sub New()

        Me.appUser = "RUTINAS"
        Me.ModuloActual = ""
        Me.lastError = ""
        Me.lastSQL = ""
        Me.bDateTimeToChar = False
        Me.bEnDebug = False
        Me.LXLONG = "LXLONG"
        Me.LXLONG_CAMPO = "CCNOT1"

    End Sub

    Function ExecuteCmd() As Boolean
        On Error GoTo SqlErr
        Dim txtErr As String
        Dim idErr As String
        Dim CMD As New ADODB.Command
        Dim txtCmd As String = "SBMJOB CMD( RUNJVA CLASS(com.bss.main.Productos) PARM('-idnum' '13' )  CLASSPATH('/BSSLtda/:/BSSLtda/RJAPIPROD.jar:/BSSLtda/lib/commons-cli-1.2.jar:/BSSLtda/lib/jt400.jar:/BSSLtda/lib/mailapi.jar:/BSSLTDA/lib/mysql-connector-java-5.1.39-bin.jar:/BSSLtda/lib/smtp.jar') ) JOB(APIPROD2) JOBD(CP61BPCUSR/BPCS) JOBQ(QBATCH) INLLIBL(*JOBD)"
        ExecuteCmd = True
        CMD = CreateObject("ADODB.Command")
        CMD.ActiveConnection = DB
        CMD.CommandType = 1
        CMD.CommandText = "call qsys.qcmdexc('" & Replace(txtCmd, "'", "''") & "', " & txtCmd.Length.ToString("0000000000.00000") & ")"
        CMD.Execute()
        CMD = Nothing

SqlErr:
        If Err.Number <> 0 Then
            Debug.Print(Err.Number & " " & Err.Description)
            ExecuteCmd = False
        End If

    End Function


End Class
