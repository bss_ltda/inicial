﻿Public Class clsErrores

    Private Structure DetalleError
        Dim DescripcionError As String
    End Structure

    Private Errors As List(Of DetalleError)

    Public Sub New()
        Errors = New List(Of DetalleError)
    End Sub

    Public Sub AddError(e1 As String, Optional e2 As String = "", Optional e3 As String = "", Optional e4 As String = "", Optional e5 As String = "", Optional e6 As String = "", Optional e7 As String = "", Optional e8 As String = "")
        Dim e As New DetalleError
        e.DescripcionError = e1
        Errors.Add(e)
    End Sub

    Public Function Count() As Integer
        Return Errors.Count()
    End Function

    Public Sub Clear()
        Errors.Clear()
    End Sub

    Public Sub Show()
        For Each item In Errors
            Console.WriteLine(item.DescripcionError)
        Next
    End Sub

    Public Sub getErrors(ByRef e() As String)
        Dim i As Int16 = 0

        ReDim e(Errors.Count)
        For Each item In Errors
            e(i) = item.DescripcionError
            i += 1
        Next

    End Sub

End Class
