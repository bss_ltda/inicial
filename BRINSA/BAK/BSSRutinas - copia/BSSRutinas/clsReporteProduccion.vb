﻿Public Class clsReporteProduccion
    Public Fecha As Date
    Public Turno As String
    Public bHora As Boolean
    Public bSoloRep As Boolean
    Public Prod As String

    Dim totalRep As Double
    Dim DB_WMS As clsConexionSQLServer
    Dim fSql As String
    Dim vSql As String
    Dim bSoloSQL As Boolean
    Dim SoloPlanta As Int16
    Dim sCRLF As String

    Public Sub ReportesProduccion()
        Dim rs As ADODB.Recordset
        Dim bOK As Boolean

        '===============================================================================================
        '    PARA UN SOLO PRODUCTO
        '===============================================================================================
        If Prod <> "" Then
            bOK = ActualizaHora(False)
            bOK = ActualizaHora(True)
            If bOK Then
                ReportaProduccion("P")       'Produccion Normal      (P)
                ReportaProduccion("R")       'Producto Recuperado    (R)
            End If
            Exit Sub
        End If
        '===============================================================================================
        '    FIN PARA UN SOLO PRODUCTO
        '===============================================================================================


        '===============================================================================================
        '    PARA EL TURNO
        '===============================================================================================
        If Not bSoloRep Then
            fSql = "SELECT * FROM WMSFTREP "
            fSql &= " WHERE "
            fSql &= "     FECHA = '" & fecha.ToString("yyyy-MM-dd") & "' "
            fSql &= " AND TURNO = '" & Turno & "' "
            fSql &= " AND PROD  = 'P' "
            fSql &= " AND CODE  = '00'"
            rs = DB.ExecuteSQL(fSql)
            If rs.EOF Then
                wc("Actualiza Hora")
                bOK = ActualizaHora(False)
                rs.Close()
                '==============
                '    Ajustes
                '==============
                wc("Hora Ajustes")
                fSql = "SELECT * FROM WMSFTREP "
                fSql &= " WHERE "
                fSql &= "     FECHA = '" & fecha.ToString("yyyy-MM-dd") & "' "
                fSql &= " AND TURNO = '" & Turno & "' "
                fSql &= " AND PROD  = 'A' "
                fSql &= " AND CODE  = '00'"
                rs = DB.ExecuteSQL(fSql)
                If rs.EOF Then
                    bOK = bOK And ActualizaHora(True)
                End If
            Else
                bOK = True
            End If
            rs.Close()
        Else
            bOK = True
        End If

        'Si solo actualiza hora => retorna
        If bHora Then
            Exit Sub
        End If

        'Si el turno ya se reporto => retorna
        fSql = "SELECT * FROM WMSFTREP "
        fSql &= " WHERE "
        fSql &= "     FECHA = '" & fecha.ToString("yyyy-MM-dd") & "' "
        fSql &= " AND TURNO='" & Turno & "' "
        fSql &= " AND PROD='A' "
        fSql &= " AND CODE='OK'"
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            rs.Close()
            rs = Nothing
            Exit Sub
        End If
        rs.Close()

        'Si ActualizaHora retorna Visto Bueno
        If bOK Then
            totalRep = 0
            wc("Reporte")

            bOK = ReportaProduccion("P")               'Produccion Normal      (P)
            bOK = bOK And ReportaProduccion("R")       'Producto Recuperado    (R)

            fSql = " INSERT INTO WMSFTREP( FECHA, TURNO, CODE, DESC, EVENTO ) "
            fSql &= " VALUES( '" & Format(fecha, "yyyy-MM-dd") & "', '" & Turno & "', "
            If bOK Then
                fSql &= "'OK', 'OK', "
            Else
                fSql &= "'02', 'Cargado pero con errores.', "
            End If
            fSql &= "'Reporte Cargado. Total: " & totalRep & "' )"
            DB.ExecuteSQL(fSql)

        End If

    End Sub

    Function ActualizaHora(bAjustes As Boolean) As Boolean
        Dim rs As ADODB.Recordset
        Dim rs1 As ADODB.Recordset
        Dim sql As String
        Dim r As Integer
        Dim totReg As Long
        Dim badReg As Long
        Dim bContinueReporte As Boolean

        bSoloSQL = False
        sql = sqlTurnoWMS(bAjustes)
        DB_WMS.Conexion()


        rs = DB_WMS.ExecuteSQL(sql)
        If DB_WMS.lastError <> "" Then
            fSql = " INSERT INTO WMSFTREP( FECHA, TURNO, CODE, DESC, EVENTO ) "
            fSql &= " VALUES( '" & Format(fecha, "yyyy-MM-dd") & "', '" & Turno & "', "
            fSql &= "'92', 'Error Conexion Viaware', "
            fSql &= "'" & Left(DB_WMS.lastError, 400) & "' )"
            DB.ExecuteSQL(fSql)
            DB_WMS.Close()
            Return False
        End If

        totReg = 0
        badReg = 0
        totalRep = 0
        bContinueReporte = True

        Do While Not rs.EOF
            totalRep = totalRep + rs("act_qty").Value
            'Si se esta ejecutando el turno C del ultimo dia, pondra las transacciones
            'del primer dia del siguiente mes con fecha ultimo dia del mes que se esta ejecutando
            'y hora 23:59:59
            fSql = "UPDATE ITH SET THCTM = THTIME, "
            If Month(fecha) < 12 And Month(fecha) <> Month(DateAdd("d", 1, fecha)) And Turno = "C" _
               And Month(fecha) <> Month(rs(1).Value) And Hour(rs(1).Value) <= 6 Then
                fSql &= " THTIME = 235959, TTDTE = " & fecha.ToString("yyyyMMdd") & ", "
            Else
                fSql &= " THTIME = " & rs(1).Value.ToString("hhmmss") & ", "
            End If
            fSql &= " THLBY  = 'WMS', "
            fSql &= " THLDT  = " & fecha.ToString("yyyyMMdd") & ", "
            fSql &= " THLTM  = " & Now().ToString("hhmmss") & ", "
            fSql &= " THMACH = '" & Turno & "'"
            fSql &= " WHERE TCOM='" & Right("0000000000" & rs(0).Value, 10) & "' " 'AND THMACH=''"
            DB.ExecuteSQL(fSql)
            If r = 0 Then
                fSql = "SELECT TCOM, THLDT, THMACH FROM ITH "
                fSql &= " WHERE TCOM='" & Right("0000000000" & rs(0).Value, 10) & "'"
                rs1 = DB.ExecuteSQL(fSql)
                If rs1.EOF Then
                    rs1.Close()
                    If DateDiff("n", rs(1), Now()) > 75 Then
                        rs1.Open("SELECT * FROM WMSFTREP WHERE TCOM='" & Right("0000000000" & rs(0).Value, 10) & "'", DB)
                        If rs1.EOF Then
                            sql = " INSERT INTO WMSFTREP( FECHA, TURNO, PROD, CODE, DESC, EVENTO, TCOM ) "
                            sql &= " VALUES( '" & fecha.ToString("yyyy-MM-dd") & "', '" & Turno & "','" & rs("sku").Value & "', '99', "
                            sql &= " 'No encontro en ITH', 'Transaccion no encontrada', '" & Right("0000000000" & rs(0).Value, 10) & "' )"
                            DB.ExecuteSQL(sql)
                        End If
                        rs1.Close()
                    Else
                        bContinueReporte = False
                    End If
                    badReg = badReg + 1
                Else
                    If Not (CStr(rs1("THLDT").Value) = fecha.ToString("yyyyMMdd") And rs1("THMACH").Value = Turno) Then
                        sql = " INSERT INTO WMSFTREP( FECHA, TURNO, PROD, CODE, DESC, EVENTO, TCOM ) "
                        sql &= " VALUES( '" & fecha.ToString("yyyy-MM-dd") & "', '" & Turno & "','" & rs("sku").Value & "', '80', "
                        sql &= " 'Ya estaba marcada en ITH', 'Transaccion Marcada ', '" & Right("0000000000" & rs(0).Value, 10) & "' )"
                        DB.ExecuteSQL(sql)
                    End If
                    rs1.Close()
                End If
            End If
            totReg = totReg + 1
            rs.MoveNext()
        Loop
        rs.Close()
        rs = Nothing
        rs1 = Nothing

        'Si no encontro alguna transaccion, pero todavia no ha pasado 1 hora y 15 minutos de la transaccion
        'entonces no hace reporte.
        If Not bContinueReporte Then
            ActualizaHora = False
            Exit Function
        End If

        If totReg < 30 Or (badReg / (totReg + 0.00001) < 0.2) Then
            ActualizaHora = True
            fSql = " INSERT INTO WMSFTREP( FECHA, TURNO, PROD, CODE, DESC, EVENTO ) "
            fSql &= " VALUES( '" & fecha.ToString("yyyy-MM-dd") & "', '" & Turno & "', "
            fSql &= IIf(bAjustes, "'A', ", "'P', ")
            If badReg = 0 Then
                fSql &= "'00', 'OK', "
                If Prod <> "" Then
                    sql = "UPDATE WMSFTREP SET CODE='00' "
                    sql &= " WHERE FECHA = '" & fecha.ToString("yyyy-MM-dd") & "'"
                    sql &= " AND TURO ='" & Turno & "' AND PROD IN(" & Prod & ")"
                    DB.ExecuteSQL(sql)
                End If
            Else
                fSql &= "'01', 'Actualizado incompleto.', "
                ActualizaHora = True
            End If
            fSql &= "'Comparacion Terminada. Total " & IIf(bAjustes, "Ajustes ", "Producto") & ": " & totalRep & "' )"
        Else
            ActualizaHora = False
            fSql = " INSERT INTO WMSFTREP( FECHA, TURNO, CODE, DESC, EVENTO ) "
            fSql &= " VALUES( '" & Format(fecha, "yyyy-MM-dd") & "', '" & Turno & "', "
            fSql &= "'98', 'Demasiadas transacciones no encontradas.', "
            fSql &= "'Comparacion Terminada.' )"

            sql = "UPDATE WMSFTREP SET UPDDAT=NOW() "
            sql &= " WHERE FECHA = '" & Format(fecha, "yyyy-MM-dd") & "'"
            sql &= " AND TURNO = '" & Turno & "' AND CODE='98'"
            DB.ExecuteSQL(sql)
            bContinueReporte = (DB.regActualizados = 0)
        End If

        If Prod = "" And bContinueReporte Then
            DB.ExecuteSQL(fSql)
        End If

    End Function

    Public Function ReportaProduccion(TipoR As String) As Boolean
        Dim sql As String
        Dim rs As ADODB.Recordset
        Dim rs1 As ADODB.Recordset
        Dim bOK As Boolean

        bOK = True
        bSoloSQL = False
        sql = sqlTurnoITH(fecha, Turno, TipoR, Prod, False)
        rs = DB.ExecuteSQL(sql)
        Do While Not rs.EOF
            fSql = "SELECT REPORD FROM RFREP WHERE "
            fSql &= " REPORD=" & rs("TREF").Value
            fSql &= " AND REPFEC = '" & fecha.ToString("yyyy-MM-dd") & "'"
            fSql &= " AND REPTUR='" & Turno & "' "
            fSql &= " AND REPPRO='" & rs("TPROD").Value & "' "
            fSql &= " AND REPTREP='" & TipoR & "' "
            fSql &= " AND REPPLA=" & rs("PLACOD").Value & " "
            rs1 = DB.ExecuteSQL(fSql)
            If rs1.EOF Then
                totalRep = totalRep + rs("TQTY").Value

                fSql = " INSERT INTO RFREP("
                vSql = " VALUES("
                fSql &= " REPORD, " : vSql &= " " & rs("TREF").Value & " ,"
                fSql &= " REPFEC, " : vSql &= "'" & fecha.ToString("yyyy-MM-dd") & "' ,"
                fSql &= " REPHOR, " : vSql &= "'" & Now().ToString("hh:mm:ss") & "', "
                fSql &= " REPTUR, " : vSql &= "'" & Turno & "' ,"
                fSql &= " REPPRO, " : vSql &= "'" & rs("TPROD").Value & "' ,"
                fSql &= " REPTREP, " : vSql &= "'" & TipoR & "' ,"
                fSql &= " REPQENT, " : vSql &= " " & rs("TQTY").Value & ", "
                fSql &= " REPBOD, " : vSql &= "'" & "PT" & "' ,"
                fSql &= " REPTRA, " : vSql &= "'" & "R" & "' ,"
                fSql &= " REPPLA, " : vSql &= " " & rs("PLACOD").Value & " ,"
                fSql &= " REPSTS, " : vSql &= "'" & "OK" & "' ,"
                fSql &= " REPCON, " : vSql &= " " & DB.CalcConsec("RFPSEC", "9999999999") & ", "
                fSql &= " REPUSR) " : vSql &= "'" & DB.gsUserOP & "' )"
                DB.ExecuteSQL(fSql & vSql)
            Else
                sql = " INSERT INTO WMSFTREP( FECHA, TURNO, PLANTA, PROD, CODE, DESC, EVENTO ) "
                sql &= " VALUES( '" & fecha.ToString("yyyy-MM-dd") & "', '" & Turno & "', "
                sql &= rs("PLACOD").Value & ", '" & rs("TPROD").Value & "', '98', 'Ya existe registro', "
                sql &= "'Orden: " & rs("TREF").Value & "' )"
                DB.ExecuteSQL(sql)
                bOK = False
            End If
            rs1.Close()
            rs.MoveNext()
        Loop
        rs.Close()
        rs = Nothing

        bOK = ExistenProductos(TipoR)
        ReportaProduccion = bOK

    End Function


    Function sqlTurnoWMS(bAjustes As Boolean) As String
        Dim sql As String
        Dim DiaSte As Date

        sql = ""
        sql &= sCRLF & "  SELECT "
        If bSoloSQL Then
            sql &= sCRLF & "   " & IIf(bAjustes, "'ADJ'", "'REP'") & " as tipo, whse_id, "
            sql &= sCRLF & "   " & Year(fecha) & " as año, "
            sql &= sCRLF & "   " & Month(fecha) & " as mes, "
            sql &= sCRLF & "   " & Day(fecha) & " as dia, "
            sql &= sCRLF & "   '" & Turno & "' as turno, "
        End If
        sql &= sCRLF & "  a.trans_seq_num, a.dtimecre, a.sku,"
        sql &= sCRLF & "  " & IIf(bAjustes, " -a.orig_qty + act_qty as act_qty ", " a.act_qty ")
        sql &= sCRLF & "  FROM history_master a"
        sql &= sCRLF & "  WHERE (a.trans_class='INVEQTY')"

        If Prod <> "" Then
            sql &= sCRLF & " AND (a.sku IN(" & Prod & ") )"
        End If

        If bAjustes Then
            sql &= sCRLF & "  AND (a.trans_obj='MANUAL') "
            sql &= sCRLF & "  AND (a.trans_act='ADJUST') "
            sql &= sCRLF & "  AND (a.rsn_code IN( 'ERRORECI', 'CAMBPROD' ) ) "
            sql &= sCRLF & "  AND (a.whse_id IN( 'PT', 'TC' ) ) "
        Else
            sql &= sCRLF & "  AND (a.trans_obj='IBO') "
            sql &= sCRLF & "  AND (a.trans_act='RECEIVE') "
            sql &= sCRLF & "  AND (a.oid<>'') "
            sql &= sCRLF & "  AND (a.whse_id IN( 'PT', 'TC' ) ) "
            sql &= sCRLF & "  AND (a.oid_type In ('R1','R2','PR') ) "
        End If

        Select Case Turno
            Case "A"
                sql &= sCRLF & "  AND (year(dtimecre)=" & Year(fecha) & ") "
                sql &= sCRLF & "  AND (month(dtimecre)=" & Month(fecha) & ") "
                sql &= sCRLF & "  AND (day(dtimecre)=" & Day(fecha) & ")"
                'sql &=  sCRLF & "  AND (datepart(hh,dtimecre) Between 0 And 13)"
                sql &= sCRLF & "  AND (datepart(hh,dtimecre) Between 6 And 13)"
            Case "B"
                sql &= sCRLF & "  AND (year(dtimecre)=" & Year(fecha) & ") "
                sql &= sCRLF & "  AND (month(dtimecre)=" & Month(fecha) & ") "
                sql &= sCRLF & "  AND (day(dtimecre)=" & Day(fecha) & ")"
                sql &= sCRLF & "  AND (datepart(hh,a.dtimecre) Between 14 And 21) "
            Case "C"
                DiaSte = DateAdd("d", 1, fecha)
                sql &= sCRLF & "  AND "
                sql &= sCRLF & "("
                sql &= sCRLF & "  (       (year(dtimecre)=" & Year(fecha) & ") "
                sql &= sCRLF & "     AND (month(dtimecre)=" & Month(fecha) & ") "
                sql &= sCRLF & "     AND (day(dtimecre)=" & Day(fecha) & ")"
                sql &= sCRLF & "     AND (datepart(hh,dtimecre)>21 ) "
                sql &= sCRLF & "  )"
                sql &= sCRLF & "   OR "
                sql &= sCRLF & "  (      (year(dtimecre)=" & Year(DiaSte) & ") "
                sql &= sCRLF & "     AND (month(dtimecre)=" & Month(DiaSte) & ") "
                sql &= sCRLF & "     AND (day(dtimecre)=" & Day(DiaSte) & ")"
                sql &= sCRLF & "     AND (datepart(hh,dtimecre)<6 )  "
                sql &= sCRLF & "  )"
                sql &= sCRLF & ")"
        End Select

        sqlTurnoWMS = sql

    End Function


    Public Function sqlTurnoITH(fecha As Date, Turno As String, TipoR As String, Prod As String, bChkProd As Boolean) As String
        Dim sql As String

        sql = ""
        sql &= "  SELECT "

        If bChkProd Then
            sql &= sCRLF & " DISTINCT "
        End If
        sCRLF = vbCrLf
        sql &= sCRLF & " ITH.TPROD "
        If Not bChkProd Then
            sql &= sCRLF & ",    SUM( TQTY ) AS TQTY, TREF, PLACOD, "
            sql &= sCRLF & "     ITH.THLDT, ITH.THMACH AS TURNO, ITH.TRES  "
            sql &= sCRLF & "  FROM "
            sql &= sCRLF & "      ITH "
            sql &= sCRLF & "      INNER JOIN FSOL01 FSO ON ITH.TREF = FSO.SORD "
            sql &= sCRLF & "      INNER JOIN RFPROD ON  ITH.TPROD = RFPROD.PROCOD"
            sql &= sCRLF & "      INNER JOIN RFPLA  ON  ITH.TPROD = RFPROD.PROCOD AND RFPROD.PROPLA = RFPLA.PLACOD"
            sql &= sCRLF & "  WHERE    "
            sql &= sCRLF & "     (PLAWMS = 'S'"
            If SoloPlanta <> 0 Then
                sql &= sCRLF & "     AND PLACOD = " & SoloPlanta
            End If
            sql &= sCRLF & "     ) AND ( "
            sql &= sCRLF & "               ( TWHS ='PT' AND TTYPE = 'R' )"
            sql &= sCRLF & "                 OR "
            sql &= sCRLF & "               ( TWHS ='TC' AND TTYPE = 'RT' )"
            sql &= sCRLF & "           )"
        Else
            sql &= sCRLF & "  FROM "
            sql &= sCRLF & "      ITH, FSOL01 FSO"
            sql &= sCRLF & "  WHERE    "
            sql &= sCRLF & "     ITH.TREF = FSO.SORD"
            sql &= sCRLF & "     AND  ( "
            sql &= sCRLF & "               ( TWHS ='PT' AND TTYPE = 'R' )"
            sql &= sCRLF & "                 OR "
            sql &= sCRLF & "               ( TWHS ='TC' AND TTYPE = 'RT' )"
            sql &= sCRLF & "           )"
        End If

        If Prod <> "" Then
            sql &= sCRLF & "     AND TPROD IN( " & Prod & ")"
        End If

        If Not bSoloSQL Then
            If TipoR = "R" Then
                sql &= sCRLF & "     AND TRES = '02'"
            Else
                sql &= sCRLF & "     AND TRES <> '02'"
            End If
        End If

        sql &= sCRLF & "     AND ( THLDT = " & fecha.ToString("yyyyMMdd")
        If Not bSoloSQL Then
            sql &= sCRLF & "     AND   THMACH = '" & Turno & "' "
        Else
            sql &= sCRLF & "     AND   THMACH <> '' "
        End If
        sql &= sCRLF & ")"

        If Not bChkProd Then
            sql &= sCRLF & "  GROUP BY    "
            sql &= sCRLF & "     ITH.THLDT,"
            sql &= sCRLF & "     ITH.THMACH,"
            sql &= sCRLF & "     ITH.TPROD,"
            sql &= sCRLF & "     ITH.TRES,"
            sql &= sCRLF & "     TREF,"
            sql &= sCRLF & "     PLACOD "
        End If

        sqlTurnoITH = sql

    End Function

    Public Function ExistenProductos(TipoR As String) As Boolean
        Dim sql As String
        Dim rs As ADODB.Recordset
        Dim rs1 As ADODB.Recordset
        Dim bOK As Boolean

        bOK = True
        sql = sqlTurnoITH(fecha, Turno, TipoR, Prod, True)
        rs = DB.ExecuteSQL(sql)
        Do While Not rs.EOF
            sql = ""
            rs1 = DB.ExecuteSQL("SELECT PROCOD From RFPROD, RFPLA Where PROPLA = PLACOD AND( PLAWMS = 'S' AND PROCOD='" & rs("TPROD").Value & "')")
            If rs1.EOF Then
                sql = " INSERT INTO WMSFTREP( FECHA, TURNO, PROD, CODE, DESC, EVENTO ) "
                sql &= " VALUES( '" & fecha.ToString("yyyy-MM-dd") & ", '" & Turno & "', "
                sql &= "'" & rs("TPROD").Value & "', '98', 'Producto sin parametrizar.', 'Producto sin parametrizar.' )"
                DB.ExecuteSQL(sql)
                bOK = False
            End If
            rs1.Close()
            rs.MoveNext()
        Loop
        rs.Close()
        rs = Nothing
        rs1 = Nothing
        ExistenProductos = bOK

    End Function

    Public Sub RepiteTRIN(actTCOM As String)
        Dim fSql As String
        Dim vSql As String
        Dim sql As String
        Dim rs As ADODB.Recordset
        Dim newTCOM As String = ""
        Dim Hora As String = ""

        'Se requiere colocar espera y ver el manejo del nuevo TCOM entre Viaware e ITH

        actTCOM = Right("0000000000" & actTCOM, 10)

        sql = "SELECT "
        sql &= "  a.trans_seq_num, a.dtimecre"
        sql &= "  FROM history_master a"
        sql &= "  WHERE (a.trans_seq_num=" & actTCOM & " )"
        rs = DB_WMS.ExecuteSQL(sql)
        If Not rs.EOF Then
            Hora = rs("dtimecre").Value.ToString("hhmmss")
        End If
        rs.Close()

        fSql = "SELECT COUNT(*) AS TOT FROM WMSFTRIN WHERE TCOM='" & actTCOM & "'"
        rs.Open(fSql, DB)
        If DB.rsOpen(rs, fSql) Then
            newTCOM = actTCOM & "-" & rs("TOT").Value.ToString("000")
        End If
        rs.Close()

        fSql = "SELECT * FROM WMSFTRIN WHERE TCOM='" & actTCOM & "'"
        If DB.rsOpen(rs, fSql) Then
            fSql = " INSERT INTO WMSFTRIN("
            vSql = " VALUES( "
            fSql &= "WMDTNV,  " : vSql &= " " & rs("WMDTNV").Value & " " & ", "
            fSql &= "WMTMNV,  " : vSql &= " " & rs("WMTMNV").Value & " " & ", "
            fSql &= "WMUSR,   " : vSql &= "'" & rs("WMUSR").Value & "'" & ", "
            fSql &= "WMPRID,  " : vSql &= "'" & rs("WMPRID").Value & "'" & ", "
            fSql &= "WMACTL,  " : vSql &= "'" & rs("WMACTL").Value & "'" & ", "
            fSql &= "TTYPE,   " : vSql &= "'" & rs("TTYPE").Value & "'" & ", "
            fSql &= "TPROD,   " : vSql &= "'" & rs("TPROD").Value & "'" & ", "
            fSql &= "TLOT,    " : vSql &= "'" & rs("TLOT").Value & "'" & ", "
            fSql &= "TWHS,    " : vSql &= "'" & rs("TWHS").Value & "'" & ", "
            fSql &= "TLOCT,   " : vSql &= "'" & rs("TLOCT").Value & "'" & ", "
            fSql &= "THCNTR,  " : vSql &= "'" & rs("THCNTR").Value & "'" & ", "
            fSql &= "TQTY,    " : vSql &= " " & rs("TQTY").Value & " " & ", "
            fSql &= "TCOM,    " : vSql &= "'" & rs("TCOM").Value & "'" & ", "
            fSql &= "TREF,    " : vSql &= " " & rs("TREF").Value & " " & ", "
            fSql &= "THLIN,   " : vSql &= " " & rs("THLIN").Value & " " & ", "
            fSql &= "TRES,    " : vSql &= "'" & rs("TRES").Value & "'" & ", "
            fSql &= "TTDTE,   " : vSql &= " " & rs("TTDTE").Value & " " & ", "
            fSql &= "TVAL,    " : vSql &= " " & rs("TVAL").Value & " " & ", "
            fSql &= "THTUM,   " : vSql &= "'" & rs("THTUM").Value & "'" & ", "
            fSql &= "THUPI,   " : vSql &= " " & rs("THUPI").Value & " " & ", "
            fSql &= "THADVN,  " : vSql &= "'" & rs("THADVN").Value & "'" & ", "
            fSql &= "RPROD,   " : vSql &= "'" & rs("RPROD").Value & "'" & ", "
            fSql &= "THMFGR,  " : vSql &= "'" & rs("THMFGR").Value & "'" & ", "
            fSql &= "THMLOT,  " : vSql &= "'" & rs("THMLOT").Value & "'" & ", "
            fSql &= "THGLRT,  " : vSql &= " " & rs("THGLRT").Value & " " & ", "
            fSql &= "TMDTE,   " : vSql &= " " & rs("TMDTE").Value & " " & ", "
            fSql &= "XNCTR,   " : vSql &= " " & rs("XNCTR").Value & " " & ", "
            fSql &= "XREF2,   " : vSql &= " " & rs("XREF2").Value & " " & ", "
            fSql &= "XMREL2)  " : vSql &= " " & rs("XMREL2").Value & " " & ") "
            DB.ExecuteSQL(fSql & vSql)

            'colocar espera para darle tiempo al SMG
            fSql = "UPDATE ITH SET THTIME=" & Hora
            fSql &= " WHERE TCOM='" & newTCOM & "'"
            DB.ExecuteSQL(fSql)

        End If

    End Sub

    Sub RepasaHora(bAjustes As Boolean)
        Dim rs As ADODB.Recordset
        Dim sql As String
        Dim totReg As Long
        Dim badReg As Long
        Dim bContinueReporte As Boolean


        bSoloSQL = False
        sql = sqlTurnoWMS(bAjustes)
        DB_WMS.Conexion()
        totReg = 0
        badReg = 0
        totalRep = 0
        bContinueReporte = True
        rs = DB_WMS.ExecuteSQL(sql)
        Do While Not rs.EOF
            totalRep = totalRep + rs("act_qty").Value
            'Si se esta ejecutando el turno C del ultimo dia, pondra las transacciones
            'del primer dia del siguiente mes con fecha ultimo dia del mes que se esta ejecutando
            'y hora 23:59:59
            fSql = "UPDATE ITH SET THCTM = THTIME, "
            If Month(fecha) <> Month(DateAdd("d", 1, fecha)) And Turno = "C" _
               And Month(fecha) <> Month(rs(1).Value) And Hour(rs(1).Value) <= 6 Then
                fSql &= " THTIME = 235959, TTDTE = " & fecha.ToString("yyyyMMdd") & ", "
            Else
                fSql &= " THTIME = " & rs(1).Value.ToString("hhmmss") & ", "
            End If
            fSql &= " THLBY  = 'WMS', "
            fSql &= " THLDT  = " & Format(fecha, "yyyyMMdd") & ", "
            fSql &= " THLTM  = " & Format(Now(), "hhmmss") & ", "
            fSql &= " THMACH = '" & Turno & "'"
            fSql &= " WHERE TCOM='" & Right("0000000000" & rs(0).Value, 10) & "'"
            fSql &= " AND THMACH=''"
            DB.ExecuteSQL(fSql)
            If DB.regActualizados = 0 Then
                DB.Bitacora("REPITEHORA", Right("0000000000" & rs(0).Value, 10), "REPROD")
            End If
            totReg = totReg + 1
            rs.MoveNext()
        Loop
        rs.Close()
        rs = Nothing

    End Sub


    Function ListaProd(txt As String) As String
        Dim aProds() As String
        Dim i As Integer
        Dim sep As String = ""

        txt = Replace(txt, "'", "")
        aProds = Split(txt, ",")
        txt = ""
        For i = 0 To UBound(aProds)
            txt = txt & sep & "'" & Trim(aProds(i)) & "'"
            sep = ", "
        Next

        ListaProd = txt

    End Function

    Public Sub New()
        DB_WMS = New clsConexionSQLServer
        With DB_WMS
            .gsDatasource = DB.getLXParam("WMSSERV")
            .gsUser = DB.getLXParam("WMSUSER")
            .gsPassword = DB.getLXParam("WMSPASS")
            .gsLib = DB.getLXParam("WMSCATALOG")
        End With

        sCRLF = vbLf
        SoloPlanta = 0
        Turno = ""
        bHora = False
        bSoloRep = False
        Prod = ""

    End Sub


End Class
