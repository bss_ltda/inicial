﻿Public Class clsEntregasConso
    Public Consolidado As String

    'ActualizaRECC()
    Public Sub ActualizaRECC()
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim bCompleto As Boolean
        Dim fSql As String
        Dim bPlanillas As Boolean = False
        Dim bConDespachos As Boolean = False
        Dim numEntrega As Integer = -1

        'Consolidado = "511252"

        'Si el consolidado es de crossdocking 
        If DB.SiExiste("SELECT HSFCNS FROM RHCC WHERE HCONSO = '" & Consolidado & "' AND HSFCNS = 'CXD'") Then
            CreaRECC_Crossdocking()
            Return
        End If

        fSql = " SELECT DISTINCT "
        fSql &= "  d.DCONSO "
        fSql &= " FROM "
        fSql &= "  RDCC d "
        fSql &= "  INNER JOIN ECL e ON d.DPEDID = e.LORD AND d.DLINEA = e.LLINE "
        fSql &= " WHERE "
        fSql &= "  d.DCONSO = '" & Consolidado & "' "
        fSql &= "  AND e.LQSHP > 0 "
        rs = ExecuteSQL(fSql)
        If Not rs.EOF Then
            bConDespachos = True
        End If
        rs.Close()

        If bConDespachos Then
            'Actualiza número de Planilla
            fSql = " SELECT DPEDID, DLINEA, IFNULL(LLLOAD, 0 ) LLLOAD, DPLANI, DNUMENT  "
            fSql &= " FROM RDCC LEFT JOIN LLL ON DPEDID = LLORDN AND DLINEA=LLOLIN "
            fSql &= " WHERE DCONSO = '" & Consolidado & "'"
            fSql &= " AND ( DPLANI <> LLLOAD OR LLLOAD IS NULL ) "
            fSql &= " ORDER BY DNUMENT "
            rs = ExecuteSQL(fSql)
            Do While Not rs.EOF
                fSql = "UPDATE RDCC SET DPLANI = " & rs("LLLOAD").Value
                fSql &= " WHERE "
                fSql &= " DPEDID = " & rs("DPEDID").Value
                fSql &= " AND DLINEA = " & rs("DLINEA").Value
                ExecuteSQL(fSql)
                If numEntrega <> rs("DNUMENT").Value Then
                    fSql = " UPDATE RECC SET  "
                    fSql &= "  EPLANI = " & rs("LLLOAD").Value
                    fSql &= " WHERE EID =" & rs("DNUMENT").Value
                    ExecuteSQL(fSql)
                    numEntrega = rs("DNUMENT").Value
                End If
                rs.MoveNext()
            Loop
            rs.Close()

            bPlanillas = True
            fSql = " SELECT DISTINCT "
            fSql &= "  d.DPEDID, "
            fSql &= "  IFNULL( p.LLLOAD, -1 ) LLLOAD, "
            fSql &= "  d.DPLANI "
            fSql &= " FROM "
            fSql &= "  RDCC d "
            fSql &= "  LEFT JOIN LLL p ON d.DPEDID = p.LLORDN AND d.DLINEA = p.LLOLIN "
            fSql &= " WHERE "
            fSql &= "  d.DCONSO = '" & Consolidado & "' "
            rs = ExecuteSQL(fSql)
            Do While Not rs.EOF
                fSql = " SELECT EID FROM RECC WHERE EPLANI = " & rs("LLLOAD").Value
                rs1 = ExecuteSQL(fSql)
                If rs1.EOF Then
                    bPlanillas = False
                End If
                rs1.Close()
                rs.MoveNext()
            Loop
            rs.Close()

            'Si todas las planillas están en el RECC, borrar los registros con planilla 0 en el RECC
            If bPlanillas Then
                fSql = " DELETE FROM RECC  "
                fSql &= " WHERE ECONSO = '" & Consolidado & "'  "
                fSql &= " AND EPLANI = 0 "
                ExecuteSQL(fSql)
            Else
                fSql = "UPDATE RDCC SET DNUMENT = 0 WHERE DNUMENT IN(  "
                fSql &= "Select "
                fSql &= "  d.DNUMENT "
                fSql &= " FROM "
                fSql &= "  RDCC d "
                fSql &= "  LEFT JOIN RECC e ON d.DNUMENT = e.EID "
                fSql &= " WHERE "
                fSql &= "  d.DCONSO = '" & Consolidado & "' AND "
                fSql &= "  e.EID IS NULL "
                fSql &= " GROUP BY "
                fSql &= "  d.DCONSO, "
                fSql &= "  d.DNUMENT, "
                fSql &= "  e.EID )"
                ExecuteSQL(fSql)

                fSql = "UPDATE RDCC R SET ( DNUMENT ) = ("
                fSql &= " SELECT  IFNULL( MAX( EID  ) , 0 ) FROM RECC WHERE R.DCONSO = ECONSO AND R.DPEDID = EPEDID "
                fSql &= " AND R.DIDIVI=EIDIVI AND R.DCUST=ECUST AND R.DSHIP=ESHIP AND R.DPLANI = EPLANI)"
                fSql &= " WHERE "
                fSql &= " DCONSO='" & Consolidado & "' AND DNUMENT=0"
                ExecuteSQL(fSql)
            End If
        Else
            fSql = "SELECT EPLANI FROM RECC "
            fSql &= " WHERE ECONSO = '" & Consolidado & "'"
            rs = ExecuteSQL(fSql)
            If rs.EOF Then
                CreaRECC(Consolidado)
                bCompleto = False
            End If
            rs.Close()
        End If

        'Actualiza numero de factura
        fSql = " SELECT DPEDID, DLINEA, IFNULL(ILDPFX, '') ILDPFX, IFNULL(ILDOCN, 0 ) ILDOCN, DLDPFX, DLDOCN  "
        fSql &= " FROM RDCC LEFT JOIN SIL ON DPEDID = ILORD AND DLINEA=ILSEQ "
        fSql &= " WHERE DCONSO = '" & Consolidado & "'"
        fSql &= " AND  DLDOCN <> ILDOCN  "
        ExecuteSQL(fSql)
        rs = ExecuteSQL(fSql)
        Do While Not rs.EOF
            fSql = "UPDATE RDCC SET DLDPFX = '" & rs("ILDPFX").Value & "', DLDOCN = " & rs("ILDOCN").Value
            fSql &= " WHERE "
            fSql &= " DPEDID = " & rs("DPEDID").Value
            fSql &= " AND DLINEA = " & rs("DLINEA").Value
            ExecuteSQL(fSql)
            rs.MoveNext()
        Loop
        rs.Close()

        fSql = "SELECT DPLANI FROM RDCC LEFT OUTER JOIN RECC ON DNUMENT = EID AND DPLANI = EPLANI "
        fSql &= " WHERE DCONSO = '" & Consolidado & "' AND EID IS NULL "
        rs = ExecuteSQL(fSql)
        If Not rs.EOF Then
            bCompleto = False
            CompletaRECC(Consolidado)
        Else
            bCompleto = True
        End If
        rs.Close()

    End Sub

    Public Sub CreaRECC(Consolidado As String)
        Dim rs As New ADODB.Recordset
        Dim fSql, vSql As String

        fSql = "UPDATE RDCC SET DNUMENT=0 "
        fSql &= " WHERE "
        fSql &= " DCONSO='" & Consolidado & "' "
        ExecuteSQL(fSql)

        fSql = " INSERT INTO RECC("
        vSql = " SELECT"
        fSql &= " ECONSO,  " : vSql &= " DCONSO,"
        fSql &= " EPEDID,  " : vSql &= " DPEDID,"
        fSql &= " EPLANI,  " : vSql &= " DPLANI,"
        fSql &= " EFREQCIT," : vSql &= " MAX(DFEC04),"
        fSql &= " EORDENT, " : vSql &= " MAX(DORDENT),"
        fSql &= " EIDIVI,  " : vSql &= " DIDIVI,"
        fSql &= " ECUST,   " : vSql &= " DCUST,"
        fSql &= " ESHIP,   " : vSql &= " DSHIP,"
        fSql &= " EPESOD,  " : vSql &= " SUM( DPESO ), "
        fSql &= " ECANTD ) " : vSql &= " SUM( DCANT )"
        vSql &= " FROM RDCC "
        vSql &= " WHERE "
        vSql &= " DCONSO='" & Consolidado & "'"
        vSql &= " GROUP BY DCONSO, DPEDID, DPLANI, DIDIVI, "
        vSql &= "          DCUST, DSHIP "
        ExecuteSQL(fSql & vSql)

        fSql = "        UPDATE RECC E SET ( EHNAME, ENSHIP, ESPOST, ESHDEP  ) = ("
        fSql &= "    SELECT TCUSTNAME, TNAME, TPOST, TSTE "
        fSql &= "    FROM RVESTTOT WHERE E.ECUST=TCUST AND E.ESHIP=TSHIP )"
        fSql &= " WHERE "
        fSql &= " ECONSO='" & Consolidado & "'"
        ExecuteSQL(fSql)

        fSql = "UPDATE RDCC R SET ( DNUMENT ) = ("
        fSql &= " SELECT  IFNULL( MAX( EID  ) , 0 ) FROM RECC WHERE R.DCONSO = ECONSO AND R.DPEDID = EPEDID "
        fSql &= " AND R.DIDIVI=EIDIVI AND R.DCUST=ECUST AND R.DSHIP=ESHIP AND R.DPLANI = EPLANI)"
        fSql &= " WHERE "
        fSql &= " DCONSO='" & Consolidado & "' AND DNUMENT=0"
        ExecuteSQL(fSql)

        fSql = "UPDATE RHCC SET HCITA = ( SELECT MIN(EFREQCIT) FROM RECC WHERE ECONSO = HCONSO ) "
        fSql &= " WHERE "
        fSql &= " HCONSO='" & Consolidado & "' "
        ExecuteSQL(fSql)


    End Sub

    Public Sub CompletaRECC(Consolidado As String)
        Dim rs As New ADODB.Recordset
        Dim fSql, vSql As String

        fSql = " INSERT INTO RECC("
        vSql = " SELECT"
        fSql &= " ECONSO,  " : vSql &= " DCONSO,"
        fSql &= " EPEDID,  " : vSql &= " DPEDID,"
        fSql &= " EPLANI,  " : vSql &= " DPLANI,"
        fSql &= " EBOD  ,  " : vSql &= " MAX(DIWHR),"
        fSql &= " ESTSCON, " : vSql &= " MAX(DCONSTS),"
        fSql &= " EFREQCIT," : vSql &= " MAX(DFEC04),"
        fSql &= " EORDENT, " : vSql &= " MAX(DORDENT),"
        fSql &= " EIDIVI,  " : vSql &= " DIDIVI,"
        fSql &= " ECUST,   " : vSql &= " DCUST,"
        fSql &= " ESHIP,   " : vSql &= " DSHIP,"
        fSql &= " EPESOD,  " : vSql &= " SUM( DPESO ), "
        fSql &= " ECANTD ) " : vSql &= " SUM( DCANT )"
        vSql &= " FROM RDCC LEFT JOIN RECC ON DCONSO = ECONSO AND  DPLANI = EPLANI "
        vSql &= " WHERE "
        vSql &= " DCONSO='" & Consolidado & "' AND DESTPE <> '98'"
        vSql &= " AND EPLANI IS NULL"
        vSql &= " GROUP BY DCONSO, DPEDID, DPLANI, DIDIVI, "
        vSql &= "          DCUST, DSHIP "
        ExecuteSQL(fSql & vSql)

        If DB.regActualizados > 0 Then
            fSql = " UPDATE RECC SET  "
            fSql &= "  ESTS = 50 "
            fSql &= " WHERE ECONSO =  '" & Consolidado & "'   "
            fSql &= " AND ESTSCON = 6 "
            ExecuteSQL(fSql)

            fSql = "        UPDATE RECC E SET ( EHNAME, ENSHIP, ESPOST, ESHDEP  ) = ("
            fSql &= "    SELECT TCUSTNAME, TNAME, TPOST, TSTE "
            fSql &= "    FROM RVESTTOT WHERE E.ECUST=TCUST AND E.ESHIP=TSHIP )"
            fSql &= " WHERE "
            fSql &= " ECONSO='" & Consolidado & "'"
            ExecuteSQL(fSql)

            fSql = "UPDATE RDCC R SET ( DNUMENT ) = ("
            fSql &= " SELECT EID FROM RECC WHERE R.DCONSO = ECONSO AND R.DPEDID = EPEDID "
            fSql &= " AND R.DIDIVI=EIDIVI AND R.DCUST=ECUST AND R.DSHIP=ESHIP AND R.DPLANI = EPLANI)"
            fSql &= " WHERE "
            fSql &= " DCONSO='" & Consolidado & "' AND DNUMENT=0"
            ExecuteSQL(fSql)

            fSql = "UPDATE RHCC SET HENTRE = ( SELECT COUNT(*) FROM (	SELECT DISTINCT DSPOST, DHAD1 FROM RDCC	WHERE DCONSO = '" & Consolidado & "') T ) "
            fSql &= " WHERE "
            fSql &= " HCONSO='" & Consolidado & "' "
            ExecuteSQL(fSql)
        End If


    End Sub

    Private Sub LogConso(reporte As String)
        Dim fSql, vSql As String

        fSql = "INSERT INTO RRCC("
        vSql = "VALUES( "
        fSql &= "RCONSO, " : vSql &= "'" & Consolidado & "'" & ", "
        fSql &= "RSTSC, "
        vSql &= "( SELECT IFNULL(SUM(HESTAD), 9) FROM RHCC WHERE HCONSO='" & Consolidado & "' )" & ", "
        fSql &= "RTREP  , " : vSql &= " " & "3" & " " & ", "
        fSql &= "RFECREP, " : vSql &= " NOW(), "
        fSql &= "RREPORT, " : vSql &= "'" & Left(Trim(reporte), 15000) & "'" & ", "
        fSql &= "RAPP   , " : vSql &= "'RECC'" & ", "
        fSql &= "RCRTUSR) " : vSql &= "'SISTEMA'" & ") "
        ExecuteSQL(fSql & vSql)

    End Sub


    Public Sub CreaRECC_Crossdocking()
        Dim rs As New ADODB.Recordset
        Dim fSql, vSql As String
        Dim sPlanis As String = ""
        Dim sep As String = ""

        fSql = " SELECT SPLANIS FROM RHCD WHERE SCONSO2 = '" & Consolidado & "' "
        rs = ExecuteSQL(fSql)
        Do While Not rs.EOF
            sPlanis &= sep & rs("SPLANIS").Value
            sep = ", "
            rs.MoveNext()
        Loop
        rs.Close()

        fSql = " INSERT INTO RECC("
        vSql = " SELECT"
        fSql &= " ESFCNS,  " : vSql &= " 'CXD',"
        fSql &= " ECONSO,  " : vSql &= " '" & Consolidado & "',"
        fSql &= " EPEDID,  " : vSql &= " DPEDID,"
        fSql &= " EPLANI,  " : vSql &= " LLLOAD,"
        fSql &= " EFREQCIT," : vSql &= " MAX(DFEC04),"
        fSql &= " EORDENT, " : vSql &= " MAX(DORDENT),"
        fSql &= " EIDIVI,  " : vSql &= " DIDIVI,"
        fSql &= " ECUST,   " : vSql &= " DCUST,"
        fSql &= " ESHIP,   " : vSql &= " DSHIP,"
        fSql &= " EPESOD,  " : vSql &= " SUM( DPESO ), "
        fSql &= " ECANTD ) " : vSql &= " SUM( DCANT )"
        vSql &= " FROM RDCC d"
        vSql &= " INNER JOIN LLL p ON d.DPEDID = p.LLORDN AND d.DLINEA = p.LLOLIN "
        vSql &= " WHERE "
        vSql &= " LLLOAD IN( " & sPlanis & ")"
        vSql &= " GROUP BY DCONSO, DPEDID, LLLOAD, DIDIVI, "
        vSql &= "          DCUST, DSHIP "
        ExecuteSQL(fSql & vSql)

        fSql = "  UPDATE RECC E SET ( EHNAME, ENSHIP, ESPOST, ESHDEP  ) = ("
        fSql &= "    SELECT TCUSTNAME, TNAME, TPOST, TSTE "
        fSql &= "    FROM RVESTTOT WHERE E.ECUST=TCUST AND E.ESHIP=TSHIP )"
        fSql &= " WHERE "
        fSql &= " ECONSO='" & Consolidado & "'"
        ExecuteSQL(fSql)

    End Sub


    'Private Sub EnCuarentena()

    '    fSql = " SELECT "
    '    fSql &=  "  RECC.EPLANI, "
    '    fSql &=  "  RHCC.HSTS, "
    '    fSql &=  "  RDCC.DPEDID, "
    '    fSql &=  "  RDCC.DLINEA, "
    '    fSql &=  "  RECC.EID "
    '    fSql &=  " FROM "
    '    fSql &=  "  RECC "
    '    fSql &=  "  LEFT OUTER JOIN RDCC ON RECC.EID = RDCC.DNUMENT AND RECC.EPLANI = RDCC.DPLANI "
    '    fSql &=  "  INNER JOIN RHCC ON RECC.ECONSO = RHCC.HCONSO "
    '    fSql &=  " WHERE "
    '    fSql &=  "  RECC.ECONSO = '" & Consolidado & "' AND "
    '    fSql &=  "  RDCC.DPLANI IS NULL "


    '    rs = DB.ExecuteSQL(fSql)
    '    If Not rs.EOF Then
    '        If rs("HSTS").Value = 60 Then
    '            bCompleto = True
    '        Else
    '            bCompleto = False
    '        End If
    '    End If
    '    rs.Close()
    '    If bCompleto And bConDespachos Then
    '        rs = Nothing
    '        fSql = "DELETE FROM RECC "
    '        fSql &=  " WHERE ECONSO='" & Consolidado & "' AND EPLANI = 0"
    '        DB.ExecuteSQL(fSql)
    '        LogConso("Entrega Completa. Planillas Actualizadas")
    '        Exit Sub
    '    End If

    '    DB.Bitacora("ActualizaRECC", "Paso Validacion Info", Consolidado)

    '    fSql = "UPDATE RDCC SET DNUMENT=0 "
    '    fSql &=  " WHERE "
    '    fSql &=  " DCONSO='" & Consolidado & "' "
    '    DB.ExecuteSQL(fSql)

    '    fSql = "UPDATE RECC SET EPLANI = 0 WHERE ECONSO = '" & Consolidado & "'"
    '    DB.ExecuteSQL(fSql)

    '    fSql = "SELECT DPEDID, DPLANI, MAX(DORDENT) AS DORDENT, DIDIVI, DCUST, DSHIP  "
    '    fSql &=  " FROM RDCC "
    '    fSql &=  " WHERE DCONSO='" & Consolidado & "' AND DPLANI > 0 "
    '    fSql &=  " GROUP BY DPEDID, DPLANI, DIDIVI, DCUST, DSHIP "
    '    fSql &=  " ORDER BY DPEDID, DPLANI "
    '    rs = DB.ExecuteSQL(fSql)
    '    Do While Not rs.EOF
    '        fSql = "SELECT EID FROM RECC WHERE "
    '        fSql &=  "     ECONSO = '" & Consolidado & "'"
    '        fSql &=  " AND EPEDID = " & rs("DPEDID").Value
    '        fSql &=  " AND EIDIVI = '" & rs("DIDIVI").Value & "'"
    '        fSql &=  " AND ECUST  = " & rs("DCUST").Value
    '        fSql &=  " AND ESHIP  = " & rs("DSHIP").Value
    '        fSql &=  " AND EPLANI = 0"
    '        rs1 = DB.ExecuteSQL(fSql)
    '        If Not rs1.EOF Then
    '            fSql = "UPDATE RECC SET EPLANI = " & rs("DPLANI").Value
    '            fSql &=  " WHERE EID = " & rs1("EID").Value
    '            DB.ExecuteSQL(fSql)
    '        Else
    '            fSql = " INSERT INTO RECC("
    '            vSql = " SELECT"
    '            fSql &=  " ECONSO," : vSql &=  " DCONSO,"
    '            fSql &=  " EPEDID," : vSql &=  " DPEDID,"
    '            fSql &=  " EPLANI," : vSql &=  " DPLANI,"
    '            fSql &=  " EORDENT," : vSql &=  " MAX(DORDENT),"
    '            fSql &=  " EFREQCIT," : vSql &=  " MAX(DFEC02),"
    '            fSql &=  " EIDIVI," : vSql &=  " DIDIVI,"
    '            fSql &=  " ECUST," : vSql &=  " DCUST,"
    '            fSql &=  " ESHIP," : vSql &=  " DSHIP,"
    '            fSql &=  " EPESOD," : vSql &=  " SUM( DPESO ), "
    '            fSql &=  " ECANTD )" : vSql &=  " SUM( DCANT )"
    '            vSql &=  " FROM RDCC "
    '            vSql &=  " WHERE DPLANI = " & rs("DPLANI").Value
    '            vSql &=  " GROUP BY DCONSO, DPEDID, DPLANI, DIDIVI, "
    '            vSql &=  "          DCUST, DSHIP "
    '            DB.ExecuteSQL(fSql & vSql)
    '        End If
    '        rs.MoveNext()
    '    Loop
    '    rs.Close()

    '    fSql = "        UPDATE RECC E SET ( EHNAME, ENSHIP, ESPOST, ESHDEP  ) = ("
    '    fSql &=  "    SELECT TCUSTNAME, TNAME, TPOST, TSTE "
    '    fSql &=  "    FROM RVESTTOT00 WHERE E.ECUST=TCUST AND E.ESHIP=TSHIP )"
    '    fSql &=  " WHERE "
    '    fSql &=  " ECONSO='" & Consolidado & "'"
    '    DB.ExecuteSQL(fSql)

    '    If bPlanillas And bConDespachos Then
    '        fSql = "DELETE FROM RECC "
    '        fSql &=  " WHERE ECONSO='" & Consolidado & "' AND EPLANI = 0"
    '        DB.ExecuteSQL(fSql)
    '    End If

    '    fSql = "UPDATE RDCC D SET ( DNUMENT ) = ("
    '    fSql &=  " SELECT EID FROM RECC WHERE D.DCONSO = ECONSO AND D.DPEDID = EPEDID "
    '    fSql &=  " AND D.DIDIVI=EIDIVI AND D.DCUST=ECUST AND D.DSHIP=ESHIP AND D.DPLANI = EPLANI)"
    '    fSql &=  " WHERE "
    '    fSql &=  " DCONSO='" & Consolidado & "' AND DPLANI > 0"
    '    DB.ExecuteSQL(fSql)

    'End Sub


    Private Function ExecuteSQL(ByVal sql As String) As ADODB.Recordset
        Return DB.ExecuteSQL(sql)
    End Function

End Class
