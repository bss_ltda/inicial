﻿'*************************************************************************************************'
'|                   CLASES PARA OPERACIONES SOBRE PEDIDOS                                       |'
'|                   v1.0.0 |  2017-06-15 21:16:09 |                                             |'
'*************************************************************************************************'
'|     PedidoCierraLinea                                                                         |'
'|     PedidoNota                                                                                |'
'|     PedidoCambioBodega                                                                        |'
'*************************************************************************************************'

Public Class PedidoCerrarLinea
    Public Pedido As String
    Public Linea As String
    Public CausalCierre As String

    Dim sMsg As String
    Dim fSql As String
    Dim vSql As String
    Dim sUser As String
    Dim r As Integer
    Dim CausalCodigo As String

    Dim iLogTXT As Integer
    Dim sArchivo As String

    Function CierraLineaPed() As Boolean
        Dim rs As ADODB.Recordset
        Dim estado As String
        Dim Descontar As Double = 0
        Dim Cliente As String = ""
        Dim Conso As String = ""
        Dim aCausal() As String

        If Not puedeBorrar(Pedido, Linea) Then
            Return False
        End If

        estado = "'98'"
        aCausal = Split(CausalCierre, " ")
        CausalCodigo = aCausal(0)

        fSql = "SELECT CLBOCL, IFNULL( SUM(LQSHP), 0 ) AS SCANT FROM ECL "
        fSql &= " WHERE LORD = " & Pedido
        If Linea <> "" Then
            fSql &= " AND LLINE = " & Linea
        End If
        fSql &= " GROUP BY CLBOCL "
        rs = DB.ExecuteSQL(fSql)
        If rs("SCANT").Value <> 0 Then
            Errors.AddError("Cantidad despachada diferente de 0")
            rs.Close()
        End If
        rs.Close()

        If Linea = 0 Then
            fSql = "SELECT HCUST AS CLI, HDTOT AS VALOR FROM ECH WHERE HORD = " & Pedido
        Else
            fSql = "SELECT LCUST AS CLI, LNET*LQORD AS VALOR "
            fSql &= " FROM ECL "
            fSql &= " WHERE LORD = " & Pedido
            If Linea > 0 Then
                fSql &= " AND LLINE = " & Linea
            End If
        End If
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            Descontar = rs("VALOR").Value
            Cliente = rs("cli").Value
        End If
        rs.Close()

        'LQORD = 0 porque BPCSF lo hace
        fSql = " UPDATE ECL SET"
        fSql &= " LID = 'ZL', LQORD = 0, "
        fSql &= " CLSTS1 = 0, CLSTS2 = 0, CLSTS3 = 0, CLSTS4 = 0, CLSTS5 = 1, "
        fSql &= " CLMNUS='" & sUser & "', CLMNDT=" & Now().ToString("yyyyMMdd") & ", CLMNTM=999999 "
        fSql &= " WHERE LORD = " & Pedido
        If Linea > 0 Then
            fSql &= " AND LLINE = " & Linea
        End If
        DB.ExecuteSQL(fSql)

        Dim nota As New PedidoNota
        nota.Pedido = Pedido
        nota.Linea = Linea
        nota.Causal = CausalCodigo
        nota.CrearNota("BORRADO")

        fSql = "SELECT DPEDID, DLINEA FROM RDCC "
        fSql &= " WHERE DPEDID = " & Pedido
        If Linea > 0 Then
            fSql &= " AND DLINEA = " & Linea
        End If
        rs = DB.ExecuteSQL(fSql)
        Do While Not rs.EOF
            fSql = "UPDATE RDCCAD SET "
            fSql &= " ECONSO = '" & Conso & "', ESTS01 = -1"
            fSql &= " WHERE EPEDID = " & rs("DPEDID").Value
            fSql &= " AND ELINEA = " & rs("DLINEA").Value
            DB.ExecuteSQL(fSql)
            rs.MoveNext()
        Loop
        rs.Close()

        fSql = " UPDATE RDCC SET DCONSO = '', DCONSOC = '', DESTPE = " & estado & ", USTSTR = 0, "
        fSql &= " DCAUBOR = '" & CausalCodigo & "'"
        fSql &= " WHERE DPEDID = " & Pedido
        If Linea > 0 Then
            fSql &= " AND DLINEA = " & Linea
        End If
        DB.ExecuteSQL(fSql)

        fSql = " UPDATE RCM SET COPEN = COPEN - " & Descontar
        fSql &= " WHERE CCUST = " & Cliente
        DB.ExecuteSQL(fSql)

        If Linea = 0 Then
            fSql = " UPDATE ECH SET "
            fSql &= " HID = 'CZ', CHSTS1=0, CHSTS2=0, CHSTS3=0, CHSTS4=0, CHSTS5=1, "
            fSql &= " HDTOT = 0, "
            fSql &= " HCNDT = " & Now().ToString("yyyyMMdd")
            fSql &= " WHERE HORD = " & Pedido
            DB.ExecuteSQL(fSql)
        Else
            fSql = "SELECT IFNULL( SUM( LQORD ), 0 ) AS CANTIDAD,  IFNULL( SUM(LQORD * LNET), 0 ) AS VALOR FROM ECLL01 WHERE LORD = " & Pedido
            If DB.OpenRS(rs, fSql) Then
                fSql = " UPDATE ECH SET "
                fSql &= " CHSTS5     = 1, "
                fSql &= " HDTOT      = " & rs("VALOR").Value & ", "
                fSql &= " CHTOEA     = " & rs("VALOR").Value & ", "
                fSql &= " HPTOT      = " & rs("Cantidad").Value
                fSql &= " WHERE HORD = " & Pedido
                DB.ExecuteSQL(fSql)
            End If
            rs.Close()

        End If

        'Bitacora "Cierre Pedido", "Pedido=" & Pedido & IIf(Linea > 0, " Linea=" & Linea, "") & "[" & r & "]"

        rs = Nothing
        CierraLineaPed = True

    End Function

    Private Function puedeBorrar(Pedido, Linea) As Boolean
        Dim rs As ADODB.Recordset
        puedeBorrar = False
        fSql = " SELECT "
        fSql &= "  r.DCONSO, "
        fSql &= "  r.DPEDID, "
        fSql &= "  r.DLINEA, "
        fSql &= "  IFNULL( c.CUDS05, 0 ) CUDS05, "
        fSql &= "  e.LID, "
        fSql &= "  CASE WHEN l.LLLOAD IS NULL THEN '' ELSE 'PLANILLA' END PLANILLA, "
        fSql &= "  CASE WHEN s.ILDOCN IS NULL THEN '' ELSE 'FACTURA' END FACTURA, "
        fSql &= "  CASE WHEN a.AORD   IS NULL THEN '' ELSE 'ELA' END ELA, "
        fSql &= "  RHCC.HCONSO, "
        fSql &= "  RHCC.HESTAD, "
        fSql &= "  e.LQORD, "
        fSql &= "  CASE WHEN e.LQALL = 0 THEN '' ELSE 'ASIGNADO' END ASIGNADO, "
        fSql &= "  CASE WHEN e.LQSHP = 0 THEN '' ELSE 'DESPACHADO' END DESPACHADO"
        fSql &= " FROM "
        fSql &= "  RDCC r "
        fSql &= "  LEFT JOIN ECL e ON r.DPEDID = e.LORD AND r.DLINEA = e.LLINE "
        fSql &= "  LEFT JOIN LLL l ON r.DPEDID = l.LLORDN AND r.DLINEA = l.LLOLIN "
        fSql &= "  LEFT JOIN SIL s ON r.DPEDID = s.ILORD AND r.DLINEA = s.ILSEQ "
        fSql &= "  LEFT JOIN ELA a ON r.DPEDID = a.AORD AND r.DLINEA = a.ALINE"
        fSql &= "  LEFT JOIN RHCC ON r.DCONSO = RHCC.HCONSO "
        fSql &= "  LEFT JOIN RDCCH c ON r.DPEDID = c.CPEDID "
        fSql &= " WHERE "
        fSql &= "  r.DPEDID = " & Pedido & " AND "
        fSql &= "  r.DLINEA = " & Linea & " "

        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            If rs("CUDS05").Value = 1 Then
                puedeBorrar = False
            ElseIf rs("PLANILLA").Value & rs("FACTURA").Value & rs("ELA").Value & rs("ASIGNADO").Value & rs("DESPACHADO").Value <> "" Then
                If rs("LID").Value = "CL" Then
                    If rs("ELA").Value = "" And rs("ASIGNADO").Value = "ASIGNADO" Then
                        fSql = "UPDATE ECL SET LQALL = 0 WHERE LORD = " & Pedido & " AND LLINE = " & Linea
                        DB.ExecuteSQL(fSql)
                    Else
                        Errors.AddError(Pedido, Linea, rs("LID").Value, rs("PLANILLA").Value, rs("FACTURA").Value, rs("ELA").Value, rs("ASIGNADO").Value, rs("DESPACHADO").Value)
                    End If
                End If
            Else
                If rs("LID").Value = "CL" Then
                    puedeBorrar = True
                End If
            End If
        Else
            Errors.AddError("No encontro pedido/linea")
        End If
            rs.Close()
            rs = Nothing

    End Function

    Public Sub New()
        Pedido = ""
        Linea = ""
    End Sub
End Class
'*************************************************************************************************'
'|                  PedidoNota                                                                   |'
'|                   v1.0.0 |  2017-06-15 21:16:09 |                                             |'
'*************************************************************************************************'
Public Class PedidoNota
    Public CategNota As String
    Public Pedido As String
    Public Linea As String
    Public Causal As String
    Public Nota As String

    Dim fSql, vSql As String

    Public Sub CrearNota(sTipo)
        Dim rs As ADODB.Recordset
        Dim sqlConso As String

        fSql = "SELECT CCDESC FROM ZCCL01 WHERE CCTABL='SPBORPED' AND CCCODE = '" & Causal & "'"
        rs = DB.ExecuteSQL(fSql)
        Nota = Causal & " " & rs("CCDESC").Value
        rs.Close()

        If sTipo = "CAUSAL" Then
            fSql = "UPDATE RDCC SET "
            fSql &= " DREF01= '" & Causal & "' "
            fSql &= " WHERE DPEDID = " & Pedido
            DB.ExecuteSQL(fSql)
        End If

        sqlConso = "SELECT IFNULL(MAX(DCONSO), '' ) "
        sqlConso &= " FROM RDCC WHERE "
        sqlConso &= " DPEDID = " & Pedido
        If Linea <> "" Then
            sqlConso &= " AND DLINEA = " & Linea
        End If

        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            Do While Nota <> ""
                fSql = " INSERT INTO RNCC( "
                vSql = " VALUES ( "
                fSql &= " NID       ," : vSql &= "'" & CategNota & "', "
                fSql &= " NTIPO     ," : vSql &= "'" & sTipo & "', "
                fSql &= " NPEDID    ," : vSql &= " " & Pedido & " , "
                If Linea <> "" Then
                    fSql &= " NLINEA    ," : vSql &= " " & Linea & ", "
                End If
                fSql &= " NCONSO    ," : vSql &= "(" & sqlConso & "), "
                fSql &= " NCAUSAL   ," : vSql &= "'" & Causal & "', "
                fSql &= " NNOTA     ," : vSql &= "'" & Left(Nota, 500) & "', "
                fSql &= " NFECH1    ," : vSql &= " " & "NOW()" & " , "
                fSql &= " NCRTUSR   )" : vSql &= "'" & DB.gsUserOP & "' )"
                DB.ExecuteSQL(fSql & vSql)
                Nota = Mid(Nota, 501)
            Loop
        End If

    End Sub

    Public Sub New()
        Pedido = ""
        Linea = ""
    End Sub
End Class

'*************************************************************************************************'
'|  PedidoCambioBodega                                                                           |'
'|                   v1.0.0 |  2017-06-15 21:16:09 |                                             |'
'*************************************************************************************************'
Public Class PedidoCambioBodega
    Public Pedido As String
    Public Linea As String
    Public Bodega As String


    Dim fSql, vSql As String
    Dim Errors As New clsErrores

    Function CambiarBodega() As Boolean
        Dim Facility As String
        Dim rs As ADODB.Recordset

        fSql = "SELECT DCONSO FROM RDCC "
        fSql &= "WHERE DPEDID = " & Pedido & ""
        If Linea <> "" Then
            fSql &= " AND DLINEA = " & Linea
        End If
        rs = DB.ExecuteSQL(fSql)
        If rs.EOF Then
            Errors.AddError("No Existe Pedido/Linea")
        ElseIf rs("DCONSO").Value <> "" Then
            Errors.AddError("Pedido/Linea Consolidado")
        End If
        rs.Close()

        fSql = " SELECT DISTINCT LPROD FROM ECHL01, ECL, ELAL01 "
        fSql &= " WHERE "
        fSql &= " AORD = LORD"
        fSql &= " AND ALINE=LLINE"
        fSql &= " AND HORD=AORD"
        fSql &= " AND AORD = " & Pedido
        If Linea <> "" Then
            fSql &= " AND ALINE = " & Linea
        End If
        fSql &= " AND ATYPE='C' "
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            Errors.AddError("Pedido/Linea Con Asignacion")
        End If
        rs.Close()

        fSql = "SELECT WMFAC FROM IWM WHERE LWHS='" & Bodega & "'"
        rs = DB.ExecuteSQL(fSql)
        If rs.EOF Then
            Errors.AddError("Bodega No Existe o no despachable")
        End If
        Facility = rs("WMFAC").Value
        rs.Close()

        If Errors.Count = 0 Then
            fSql = "UPDATE ECL SET LWHS='" & Bodega & "', LICFAC='" & Facility & "'"
            fSql &= " WHERE LORD = " & Pedido
            If Linea <> "" Then
                fSql &= " AND LLINE = " & Linea
            End If
            DB.ExecuteSQL(fSql)
            fSql = "UPDATE ECH SET HWHSE='" & Bodega & "' WHERE HORD = " & Pedido
            DB.ExecuteSQL(fSql)

            fSql = "UPDATE RDCC SET "
            fSql &= "  DIWHR='" & Bodega & "'"
            fSql &= ", DPLANT=" & Facility
            fSql &= " WHERE DPEDID = " & Pedido
            If Linea <> "" Then
                fSql &= " AND DLINEA = " & Linea
            End If
            DB.ExecuteSQL(fSql)

            fSql = "UPDATE RDCCH SET "
            fSql &= " CIWHR = '" & Bodega & "', "
            fSql &= " CPLANT = " & Facility & " "
            fSql &= " WHERE CPEDID = " & Pedido
            DB.ExecuteSQL(fSql)

            If Linea = "" Then
                DB.Bitacora("Cambio Bodega", Pedido, Pedido)
            Else
                DB.Bitacora("Cambio Bodega", String.Format("Pedido {0} Linea {1}", Pedido, Linea), Pedido)
            End If
        Else
            Return False
        End If
        Return True

    End Function
End Class

'+-----------------------------------------------------------------------------------------------+'
'|                   CLASE PARA ABRIR LINEAS DE PEDIDOS LX                                       |'
'|                   v1.0.0 | 2016-05-31 19:29:32                                                |
'+-----------------------------------------------------------------------------------------------+'
Class PedidoAbrirLinea

    Public Pedido As String
    Public LineaPedido As String
    Public CantidadActual As String
    Public CantidadNueva As String

    Dim CantActual As Double
    Dim CantNuevaLinActual As Double
    Dim CantLinNueva As Double
    Dim NuevaLinea As Int32

    Dim fSql, vSql As String

    Sub Inicializa()
        Pedido = ""
        LineaPedido = ""
    End Sub

    Sub AbrirLineaPedido()
        Dim rs As ADODB.Recordset

        CantActual = Val(CantidadActual)
        CantNuevaLinActual = Val(CantidadNueva)

        If CantNuevaLinActual > CantActual Then
            Exit Sub
        End If

        CantLinNueva = CantActual - CantNuevaLinActual

        fSql = "SELECT IFNULL( SUM(DVOBO), 0 ) FROM RDCC WHERE DPEDID=" & Pedido
        rs = DB.ExecuteSQL(fSql)
        If CDbl(rs(0).Value) > 0 Then
            DB.ExecuteSQL("INSERT INTO CONSOS( CPEDID, CESTPE ) VALUES( " & Pedido & ", 'AL' )")
        End If
        rs.Close()


        'Obtener el N° de lineas del pedido
        fSql = "UPDATE ECH SET HLINS = HLINS + 1 WHERE HORD = " & Pedido
        DB.ExecuteSQL(fSql)

        'Primero actualiza el ECH para que no falle el trigger de promociones
        fSql = "SELECT HLINS FROM ECH WHERE HORD = " & Pedido
        rs = DB.ExecuteSQL(fSql)
        NuevaLinea = CInt(rs("HLINS").Value)
        rs.Close()

        '************************************************
        '* Inserta la linea en Registro de Promociones
        '************************************************
        AbrirLineaPedidoRPROFRE()

        'Obtener la línea actual para duplicarla

        fSql = " DELETE FROM RFECLW "
        fSql &= " WHERE LORD = " & Pedido & " AND  LLINE IN( " & LineaPedido & " , " & NuevaLinea & " )"
        DB.ExecuteSQL(fSql)

        fSql = " INSERT INTO RFECLW "
        fSql &= " SELECT * FROM ECL WHERE LORD = " & Pedido & " AND  LLINE = " & LineaPedido
        DB.ExecuteSQL(fSql)

        fSql = "UPDATE RFECLW SET "
        fSql &= "  LLINE = " & NuevaLinea     'Nueva Linea
        fSql &= ", LQORD = " & CantLinNueva      'Cantidad Pendiente
        fSql &= ", LQALL = " & 0          'cero
        fSql &= ", LQSHP = " & 0          'Cero por ser nacional

        'Usario y fecha de creacion
        fSql &= ", CLMNDT = " & DB2_DATEDEC
        fSql &= ", CLMNTM = " & DB2_TIMEDEC0
        fSql &= ", CLENUS = '" & DB.gsUserOP & "'"
        fSql &= ", CLMNUS = '" & DB.gsUserOP & "'"

        fSql &= ", CLNUWS = 'SEGPEDNAL'"
        fSql &= ", CLLPGM = 'SEGPEDNAL'"
        fSql &= ", CLNUUS = '" & DB.gsUserOP & "'"

        fSql &= ", LPLIN = " & LineaPedido       'Purchase Order Line Number / Se usa para hacer referencia a linea origen

        fSql &= ", LQSALL = " & CantLinNueva          'Cantidad Pendiente
        fSql &= ", LQPCK  = " & CantLinNueva           'Cantidad Pendiente

        fSql &= ", CLLPSC = '12'"
        fSql &= ", CLNPSC = '06'"

        fSql &= ", CLORQT = " & CantLinNueva          'Cantidad Pendiente
        fSql &= ", CLOSQT = " & CantLinNueva          'Cantidad Pendiente
        fSql &= ", CLPSQT = " & CantLinNueva          'Cantidad Pendiente
        fSql &= ", CLSSQT = 0"                 'Cantidad Pendiente

        fSql &= ", CLQPRC = " & CantLinNueva          'Cantidad Pendiente

        fSql &= " WHERE LORD = " & Pedido & " AND  LLINE = " & LineaPedido
        DB.ExecuteSQL(fSql)

        fSql = " INSERT INTO ECL "
        fSql &= " SELECT * FROM RFECLW WHERE LORD = " & Pedido & " AND  LLINE = " & NuevaLinea
        DB.ExecuteSQL(fSql)

        fSql = "SELECT YEAR(DFECR)*10000+MONTH(DFECR)*100+DAY(DFECR) AS FCITA, "
        fSql &= " HOUR(DFECR)*10000 + MINUTE(DFECR) AS HCITA "
        fSql &= " FROM RDCC "
        fSql &= " WHERE DPEDID=" & Pedido & " AND DLINEA = " & LineaPedido
        rs = DB.ExecuteSQL(fSql)
        fSql = "  UPDATE ECL SET "
        fSql &= "  LSCDT = " & rs("FCITA").Value & ", "
        fSql &= "  LRDTE = " & rs("FCITA").Value & ", "
        fSql &= "  CLSRTM = " & rs("HCITA").Value
        fSql &= "  WHERE LORD = " & Pedido & " AND  LLINE = " & NuevaLinea
        DB.ExecuteSQL(fSql)
        rs.Close()

        '************************************************
        '* Inserta la linea en WMSFAD
        '************************************************
        InsertLineWMSFAD()

        '************************************************
        '* Actualia ECL Registro Actual
        '* (Cambia cantidades si lo despachado es menor)
        '************************************************

        fSql = "UPDATE ECL SET"
        fSql &= "  LQORD  = " & CantNuevaLinActual
        fSql &= ", LQSALL = " & CantNuevaLinActual
        fSql &= ", CLOSQT = " & CantNuevaLinActual
        fSql &= ", CLQPRC = " & CantNuevaLinActual
        fSql &= ", LQPCK  = " & CantNuevaLinActual
        fSql &= ", CLPSQT = " & CantNuevaLinActual
        fSql &= ", CLTAXA =  CASE WHEN CLBTYP = '9' THEN 0"
        fSql &= "                 WHEN LNET = 0 THEN 0"
        fSql &= "            ELSE " & CantNuevaLinActual & "* LNET * (CLTAXA / (LQORD * LNET)) END"
        fSql &= ", CLMNUS ='" & DB.gsUserOP & "'"
        fSql &= ", CLMNDT = " & DB2_DATEDEC
        fSql &= ", CLMNTM = " & DB2_TIMEDEC0
        fSql &= " WHERE LORD = " & Pedido & " AND  LLINE = " & LineaPedido
        DB.ExecuteSQL(fSql)

        rs = Nothing

    End Sub

    Private Sub AbrirLineaPedidoRPROFRE()
        Dim fSql, rs

        fSql = "SELECT FPEDID FROM  RPROFRE "
        fSql &= " WHERE FPEDID=" & Pedido
        fSql &= " AND FLINEA=" & LineaPedido
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            fSql = "UPDATE RPROFRE SET FCANT=" & CantNuevaLinActual
            fSql &= " WHERE FPEDID=" & Pedido
            fSql &= " AND FLINEA=" & LineaPedido
            DB.ExecuteSQL(fSql)
        Else
            fSql = "INSERT INTO RPROFRE( FPEDID, FLINEA, FPROD, FGRUP, FCANT, FODTE, FCUST, FSHIP, FVALOR, FFLAG ) "
            fSql &= " SELECT FPEDID, "
            fSql &= " CAST( " & LineaPedido & " AS DECIMAL( 3, 0 ) ), "
            fSql &= " FPROD, FGRUP, "
            fSql &= " CAST(" & CantNuevaLinActual & " AS DECIMAL( 11, 3 ) ), "
            fSql &= " FODTE, FCUST, FSHIP,"
            fSql &= " FVALOR, FFLAG FROM RPROFRE"
            fSql &= " WHERE FPEDID=" & Pedido
            fSql &= " AND FLINEA=" & LineaPedido
            DB.ExecuteSQL(fSql)
        End If
        rs.Close()
        rs = Nothing

    End Sub

    Private Sub InsertLineWMSFAD()
        Dim fSql, vSql

        fSql = " INSERT INTO WMSFAD( "
        vSql = " SELECT "
        fSql &= "EBCON,   " : vSql &= "EBCON  , "                  '       Documento
        fSql &= "EPEAN,   " : vSql &= "EPEAN  , "                  '       Codigo Producto EAN
        fSql &= "EPROD,   " : vSql &= "EPROD  , "                  '       Producto BPCS
        fSql &= "ECONSO,  " : vSql &= "ECONSO , "                  '       Consolidado
        fSql &= "EPLANI,  " : vSql &= "EPLANI , "                  '       Planilla
        fSql &= "EHEAN,   " : vSql &= "EHEAN  , "                  '       Punto de Envio EAN Hijos
        '=============================================================================
        fSql &= "EQPEDI,  " : vSql &= CantNuevaLinActual & ", "    '       Cantidad Pedido
        '=============================================================================
        fSql &= "EDISTR,  " : vSql &= "EDISTR , "                  '       Se distribuye S/N
        fSql &= "EDOWMS,  " : vSql &= "EDOWMS , "                  '       Nro documento WMS
        fSql &= "EDHPO,   " : vSql &= "EDHPO  , "                  '       Orden de Compra EDI
        fSql &= "ECONS,   " : vSql &= "ECONS  , "                  '       Conse por Punto Env
        fSql &= "EADNUM,  " : vSql &= "EADNUM , "                  '       Numero Aviso EDI
        fSql &= "EEDIUNH, " : vSql &= "EEDIUNH, "                  '       UNH Mensaje
        fSql &= "EEDIBGM, " : vSql &= "EEDIBGM, "                  '       Id Documento
        fSql &= "ELINEA,  " : vSql &= "ELINEA , "                  '       Linea CHBCON
        fSql &= "EPEDBP,  " : vSql &= "EPEDBP , "                  '       Pedido BPCS
        '=============================================================================
        fSql &= "ELINBP,  " : vSql &= NuevaLinea & ", "                '       Linea  BPCS
        '=============================================================================
        fSql &= "EDESPA, " : vSql &= "EDESPA , "                  '       Despachado S/N
        fSql &= "EPCUST, " : vSql &= "EPCUST , "                  '       Cliente BPCS
        fSql &= "EPSHIP, " : vSql &= "EPSHIP , "                  '       Ship to BPCS
        fSql &= "ETNAME, " : vSql &= "ETNAME , "                  '       Nombre Punto Envio
        fSql &= "EUSER  )" : vSql &= "'" & DB.gsUserOP & "'"            '       Usuario

        vSql &= " FROM WMSFAD "
        vSql &= " WHERE "
        vSql &= " EPEDBP=" & Pedido
        vSql &= " AND ELINBP=" & LineaPedido
        DB.ExecuteSQL(fSql & vSql)

        'Actualiza linea anterior
        fSql = "UPDATE WMSFAD SET "
        fSql &= " EQPEDI = EQPEDI - " & CantNuevaLinActual
        fSql &= " WHERE "
        fSql &= " EPEDBP = " & Pedido
        fSql &= " AND ELINBP = " & LineaPedido
        DB.ExecuteSQL(fSql)

    End Sub


    Public Sub New()
        Inicializa()
    End Sub

End Class
'+-----------------------------------------------------------------------------------------------+'
'|                   FIN CLASE PARA ABRIR LINEAS DE PEDIDOS LX                                   |'
'+-----------------------------------------------------------------------------------------------+'

