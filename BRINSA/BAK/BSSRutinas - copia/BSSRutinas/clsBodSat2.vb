﻿Public Class clsBodSat2
    Public WrkID As String

    Dim Transportadora As String
    Dim Password As String
    Dim Manifiesto As String
    Dim Bodega As String

    Dim TipoVehiculo As String
    Dim Notas As String
    Dim DescTipoVehiculo As String
    Dim Placa As String
    Dim CedulaConductor As String
    Dim NombreConductor As String
    Dim CelularConductor As String
    
    Structure DetalleDespacho
        Dim Pedido As String
    End Structure

    Dim Pedidos As New List(Of DetalleDespacho)

    Sub Despacho()
        Dim fSql As String
        Dim rs As New ADODB.Recordset
        Dim xmlPedido As String = ""
        Dim Pedido As DetalleDespacho

        fSql = " Select "
        fSql &= "  NOW(), "
        fSql &= "  CBUSY, "
        fSql &= "  W.WRKID, "
        fSql &= "  V.VPLACA, "
        fSql &= "  V.VCAPTON, "
        fSql &= "  TV.VCODIGO, "
        fSql &= "  TV.VTIPVEHD, "
        fSql &= "  V.CCEDULA, "
        fSql &= "  V.CNOMBRE, "
        fSql &= "  V.CMOVIL, "
        fSql &= "  RDCCH.CPEDID, "
        fSql &= "  RDCCH.CIWHR, "
        fSql &= "  W.WPEDID AS TRANSP "
        fSql &= " From "
        fSql &= "  RDCCH Inner Join "
        fSql &= "  RFWRKGRL W On RDCCH.CUDN01 = W.WRKID And RDCCH.CUDS06 = W.WLINEA Inner Join "
        fSql &= "  RTVEH V On W.WCATEG01 = V.VPLACA And W.WCATEG02 = V.VBOD And "
        fSql &= "    W.WPEDID = V.VPROV Inner Join "
        fSql &= "  RFFLTIPVEH TV On V.VTIPVEH = DIGITS(TV.VTIPVEH) "
        fSql &= " Where "
        fSql &= "  W.WRKID = " & WrkID & " AND CBUSY = 1"
        fSql &= " Order By "
        fSql &= "  V.VPLACA, "
        fSql &= "  RDCCH.CPEDID "
        DB.ExecuteSQL("INSERT INTO RFBS2CONSO " & fSql)
        rs = DB.ExecuteSQL(fSql)

        If rs.EOF Then
            DB.WrtSqlError(fSql, "BodSat II No encotro pedidos")
        End If
        Do While Not rs.EOF
            If Placa <> rs("VPLACA").Value Then
                If Placa <> "" Then
                    creaDespacho()
                End If
                Transportadora = rs("TRANSP").Value
                Placa = rs("VPLACA").Value
                Bodega = rs("CIWHR").Value
                TipoVehiculo = rs("VCODIGO").Value
                DescTipoVehiculo = rs("VTIPVEHD").Value
                CedulaConductor = rs("CCEDULA").Value
                NombreConductor = Left(rs("CNOMBRE").Value, 30)
                CelularConductor = rs("CMOVIL").Value
                Manifiesto = ""
            End If
            Pedido = New DetalleDespacho
            Pedido.Pedido = rs("CPEDID").Value
            Pedidos.Add(Pedido)
            rs.MoveNext()
        Loop

        fSql = " UPDATE RDCCH SET CBUSY = 2 "
        fSql &= " WHERE CUDN01 = " & WrkID
        DB.ExecuteSQL(fSql)

        creaDespacho()
        rs.Close()
        rs = Nothing

    End Sub


    Sub creaDespacho()
        Dim fSql, vSql As String
        Dim Pedido As DetalleDespacho
        Dim rs As New ADODB.Recordset
        Dim C_ID As String = DB.CalcConsec("RFLOGWSCDA", "99999999")
        Dim Conso As String
        Dim Division As String
        Dim Segmento As String = ""
        Dim lstPedidos As String = ""
        Dim sep As String = ""

        For Each Pedido In Pedidos

            fSql = "SELECT DISTINCT DPEDID FROM RDCC WHERE DPEDID = " & Pedido.Pedido & " AND DCONSO = ''"
            rs = DB.ExecuteSQL(fSql)
            If Not rs.EOF Then
                lstPedidos &= sep & Pedido.Pedido
                sep = ", "
            Else
                'Pedido consolidado
            End If
            rs.Close()
        Next

        If lstPedidos = "" Then
            Console.WriteLine("Sin pedidos.")
            Exit Sub
        End If

        Conso = DB.CalcConsec("SPCONSO", "999999")
        Console.WriteLine(Conso)
        Division = " "

        fSql = "UPDATE RDCC SET DCONSO = '" & Conso & "' WHERE DPEDID IN( " & lstPedidos & " ) AND DESTPE = '01' AND DCONSO = ''"
        DB.ExecuteSQL(fSql)

        fSql = " SELECT DISTINCT"
        fSql &= " RREF02 "
        fSql &= " FROM RDCC "
        fSql &= " INNER JOIN RIIM ON DPROD=RIPROD"
        fSql &= " WHERE DCONSO ='" & Conso & "'"
        rs = DB.ExecuteSQL(fSql)
        Do While Not rs.EOF
            Select Case Trim(rs("RREF02").Value)
                Case "SAL"
                    Division = "CSECA"
                    Segmento = "SAL"
                Case "ASEO"
                    Division = "CSECA"
                    Segmento = "ASEO"
                Case "QSE"
                    Division = "CSECA"
                    Segmento = "QUIMI"
                Case "QLA", "QLF"
                    Division = rs("RREF02").Value
                    Segmento = "QUIMI"
            End Select
            rs.MoveNext()
        Loop
        rs.Close()

        fSql = " INSERT INTO RHCC( "
        vSql = " VALUES ( "
        fSql &= " HSFCNS    , " : vSql &= "'BS2', "                         '//3A    Tipo Consolidado
        fSql &= " HCONSO    , " : vSql &= "'" & Conso & "', "               '//6A    Consolidado
        fSql &= " HPLACA    , " : vSql &= "'" & Placa & "', "               '//6A    Placa
        fSql &= " HPROVE    , " : vSql &= Transportadora & ", "             '//8P0   Transp
        fSql &= " HPLTA     , " : vSql &= "'10', "                          '//Planta
        fSql &= " HNPROVE   , " : vSql &= "(SELECT TNOMBRE FROM RTTRA WHERE TPROVE = " & Transportadora & "), "     '//50A   Transportador
        fSql &= " HFERECA   , " : vSql &= " NOW()   , "                     '//26Z   F.Reque Vehiculo
        fSql &= " HCEDUL    , " : vSql &= " " & CedulaConductor & ", "      '//18P0  Cedula Conductor
        fSql &= " HCHOFE    , " : vSql &= "'" & NombreConductor & "', "     '//30A   Conductor
        fSql &= " HCELU     , " : vSql &= "'" & Left(CelularConductor, 20) & "', "     '//20A   Celular
        fSql &= " HGENERA   , " : vSql &= "'1', "                           '//1A    Generada OC S/N
        fSql &= " HMANIF    , " : vSql &= "'" & Manifiesto & "', "          '//15A   Manifiesto del Transp
        fSql &= " HTIPO     , " : vSql &= "(SELECT VTIPVEH FROM RFFLTIPVEH WHERE VCODIGO = '" & TipoVehiculo & "')" & ", "     '//2P0   Tipo Vehiculo
        fSql &= " HACTUSR   , " : vSql &= "'" & DB.appUser & "', "     '//10A   User UPD
        fSql &= " HOBSER    , " : vSql &= "'" & Notas & "', "               '//120A  Observaciones
        fSql &= " HMERCA    , " : vSql &= "'NACIONAL', "
        fSql &= " HDIVIS    , " : vSql &= "'" & Division & "', "
        fSql &= " HSEGME    , " : vSql &= "'" & Segmento & "', "
        fSql &= " HPER      , " : vSql &= Now().ToString("yyyyMM") & ", "      '
        fSql &= " HBOD      , " : vSql &= "'" & Bodega & "', "              '//3A    Bodega
        fSql &= " HREF05    ) " : vSql &= "'" & Bodega & "' )"              '//15A   Bodega
        DB.ExecuteSQL(fSql & vSql)

        DestinoConso(Conso)
        CreaRECC(Conso)

        fSql = " INSERT INTO RRCC( "
        vSql = " VALUES ( "
        fSql &= " RTREP     , " : vSql &= " 2, "                            '//2S0   Tipo Rep
        fSql &= " RCONSO    , " : vSql &= "'" & Conso & "', "               '//6A    Consolidado
        fSql &= " RMANIF    , " : vSql &= "'" & Manifiesto & "', "          '//15A   Manifiesto del Transp
        fSql &= " RPLACA    , " : vSql &= "'" & Placa & "', "               '//6A    Placa
        fSql &= " RSTSC     , " : vSql &= " (SELECT HESTAD FROM RHCC WHERE HCONSO = '" & Conso & "'), "     '//2S0   Est Cons
        fSql &= " RREPORT   , " : vSql &= "'Consolidado generado. Pedidos: " & lstPedidos & " Placa: " & Placa & " Tip Veh " & DescTipoVehiculo & "', "     '//502A  Reporte
        fSql &= " RCRTUSR   , " : vSql &= "'" & DB.appUser & "', "                     '//10A   Usuario
        fSql &= " RAPP      ) " : vSql &= "'BODSAT2') "                     '//15A   App
        DB.ExecuteSQL(fSql & vSql)

        fSql = "UPDATE RDCCH SET CBUSY = 3 , CREF07 = '" & Conso & "' "
        fSql &= " WHERE CPEDID IN( " & lstPedidos & " ) "
        DB.ExecuteSQL(fSql)

        AsignaInventario(Conso)

        fSql = " UPDATE RDCCH SET CBUSY = 5, CUDS05 = 2 "
        fSql &= " WHERE CPEDID IN( " & lstPedidos & " )"
        DB.ExecuteSQL(fSql)

        fSql = " UPDATE RHCC SET  HESTAD = 36  "
        fSql &= " WHERE HCONSO = '" & Conso & "' "
        DB.ExecuteSQL(fSql)

        Tarea.setResultado(String.Format("Consolidado {0} generado.", Conso))


    End Sub

    Private Sub DestinoConso(Conso)
        Dim fSql As String
        Dim rs As New ADODB.Recordset
        Dim Recorrido As String = ""
        Dim Dpto As String = ""
        Dim Ciudad As String = ""

        fSql = " SELECT DISTINCT HCONSO, LPOAS, DSPOST, DSHDEP , IFNULL( FTDIST, -1 ) AS DISTAN "
        fSql &= " FROM RDCC INNER JOIN RHCC ON DCONSO = HCONSO "
        fSql &= " INNER JOIN IWM ON DIWHR = LWHS"
        fSql &= " LEFT JOIN RFFLDISTKM ON DIWHR = FBOD AND DSHDEP = FDPTO AND DSPOST = FCIUD "
        fSql &= " WHERE "
        fSql &= " HCONSO = '" & Conso & "' "
        fSql &= " ORDER BY DISTAN "
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            Recorrido = rs("LPOAS").Value
        End If
        Do While Not rs.EOF
            If CDbl(rs("DISTAN").Value) = -1 Then
                DB.WrtSqlError(rs("HCONSO").Value, "Ciudad sin distancia. " & rs("DSPOST").Value)
            End If
            Recorrido = Recorrido & "/" & rs("DSPOST").Value
            Dpto = rs("DSHDEP").Value
            Ciudad = rs("DSPOST").Value
            rs.MoveNext()
        Loop
        rs.Close()
        rs = Nothing

        fSql = "UPDATE RHCC SET "
        fSql &= "   HCIUDAD = '" & Ciudad & "'"
        fSql &= " , HDEPTO  = '" & Dpto & "'"
        fSql &= " , HDESTI  = '" & Left(Recorrido, 50) & "'"
        fSql &= " WHERE  "
        fSql &= " HCONSO = '" & Conso & "' "
        DB.ExecuteSQL(fSql)


    End Sub


    Private Sub CreaRECC(Conso As String)
        Dim RECC As New clsEntregasConso

        DB.gsKeyWords = Conso
        RECC.Consolidado = Conso
        RECC.ActualizaRECC()

    End Sub

    Private Sub AsignaInventario(Conso As String)
        Dim AsignaInv As New clsLib_Consos
        DB.gsKeyWords = Conso
        With AsignaInv
            .Conso = Conso
            .HSFCNS = "BS2"
            .WMSCOPED()
        End With
    End Sub
    Function xmlDespacho(PedidoDespachado)
        Dim s, br

        br = vbCrLf
        If PedidoDespachado = "" Then
            s = ""
            s = s & br & "<Despacho>"
            's = s & br & Replace("   <Usuario>{USUARIO}</Usuario>", "{USUARIO}", Usuario)
            s = s & br & Replace("   <Password>{PASSWORD}</Password>", "{PASSWORD}", Password)
            s = s & br & Replace("   <Manifiesto>{MANIFIESTO}</Manifiesto>", "{MANIFIESTO}", Manifiesto)
            s = s & br & Replace("   <Bodega>{BOD}</Bodega>", "{BOD}", Bodega)
            s = s & br & Replace("   <TipoVehiculo>{TIPVEH}</TipoVehiculo>", "{TIPVEH}", TipoVehiculo)
            s = s & br & Replace("   <DescTipoVehiculo>{VEHICULO}</DescTipoVehiculo>", "{VEHICULO}", DescTipoVehiculo)
            s = s & br & Replace("   <Placa>{PLACA}</Placa>", "{PLACA}", Placa)
            s = s & br & Replace("   <CedulaConductor>{CEDULA}</CedulaConductor>", "{CEDULA}", CedulaConductor)
            s = s & br & Replace("   <NombreConductor>{CONDUCTOR}</NombreConductor>", "{CONDUCTOR}", NombreConductor)
            s = s & br & Replace("   <CelularConductor>{CELULAR}</CelularConductor>", "{CELULAR}", CelularConductor)
            s = s & br & Replace("   <Notas>{NOTAS}</Notas>", "{NOTAS}", Notas)
            s = s & br & "{PEDIDOS}"
            s = s & br & "</Despacho>"
        Else
            s = br & Replace("<PedidoDespachado><Pedido>{PEDIDO}</Pedido></PedidoDespachado>", "{PEDIDO}", PedidoDespachado)
        End If

        xmlDespacho = s


    End Function

End Class
