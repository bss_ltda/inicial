﻿Imports System.IO

Public Class clsFacturas
    Public Prefijo As String
    Public Factura As String


    Dim fSql, vSql As String
    Dim numeroAviso As String

    Function GeneraAvisoDespacho() As String
        Dim bGenera As Boolean
        Dim rs As New ADODB.Recordset

        fSql = " SELECT CCNUM FROM RFFACCAB INNER JOIN RFPARAM ON CCTABL = 'EDIAVISODESP' AND CCCODEN = FCLIENT "
        fSql &= " WHERE FPREFIJ = '" & Prefijo & "' AND FFACTUR = " & Factura
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            bGenera = True
        End If
        rs.Close()
        rs = Nothing

        If Not bGenera Then
            Return "No genera."
        End If

        numeroAviso = "AD" & DB.CalcConsec("RFAVDES", "99999999")
        fSql = " DELETE FROM RFAVDES  "
        fSql &= " WHERE NUM_AVISO = '" & numeroAviso & "'"
        DB.ExecuteSQL(fSql)

        fSql = " INSERT INTO RFAVDES( "
        vSql = " SELECT "
        fSql &= " CRTUSR    , " : vSql &= "  'AUTOMATICO' AS CRTUSR, "
        fSql &= " AVDPFX    , " : vSql &= "  FC.FPREFIJ, "
        fSql &= " AVDOCN    , " : vSql &= "  FC.FFACTUR, "

        fSql &= " AVFACLIN  , " : vSql &= "  FD.DLINEA, "
        fSql &= " AVPEDID   , " : vSql &= "  FD.DPEDIDO, "
        fSql &= " AVPEDLIN  , " : vSql &= "  FD.DLINPED, "

        fSql &= " EAN_PROV  , " : vSql &= "  CHAR( '7703812000017', 14 ) As EAN_PROV, "
        fSql &= " EAN_CLI   , " : vSql &= "  CLI.CMFF06 EAN_CLI, "
        fSql &= " NUM_AVISO , " : vSql &= "  '" & numeroAviso & "' As NUM_AVISO, "
        fSql &= " FECHA_DOC , " : vSql &= "  DEC( Year(NOW()) * 10000 + Month(NOW()) * 100 + Day(NOW()), 8, 0 ) As FECHA_DOC, "
        fSql &= " FECHA_DES , " : vSql &= fecCampo("FACRTDAT") & ", "
        fSql &= " FECHA_ENT , " : vSql &= fecCampo("DFEC02") & ", "
        fSql &= " ORD_COMPRA, " : vSql &= "  FC.FORDCOM ORD_COMPRA, "
        fSql &= " FACTURA   , " : vSql &= "  CHAR( TRIM(FC.FPREFIJ) || DIGITS(FC.FFACTUR), 15 ) AS FACTURA, "
        fSql &= " EAN_TRANSP, " : vSql &= "  IFNULL( AVM.VMEAN, '' ) AS EAN_TRANSP, "
        fSql &= " EAN_CLICORP , " : vSql &= "  CORP.CMFF06 AS EAN_CLICORP, "
        fSql &= " EAN_CORPROV , " : vSql &= "  CHAR( '7703812000017', 14 ) As EAN_CORPROV, "
        fSql &= " NIT_PROV  , " : vSql &= "  CHAR( '8002217892', 20 ) As NIT_PROV, "
        fSql &= " NIT_CLI   , " : vSql &= "  FC.FNIT AS NIT_CLI, "
        fSql &= " EAN_PENVIO, " : vSql &= "  EST.STDOCK EAN_PENVIO, "
        fSql &= " PLACA_VEH , " : vSql &= "  IFNULL(RHCC.HPLACA, '' ) AS PLACA_VEH, "
        fSql &= " EMPAQUE   , " : vSql &= "  RIIM.RTPAQUL AS EMPAQUE, "
        fSql &= " CANT      , " : vSql &= "  DIGITS( DEC( RDCC.DCANT, 8, 0 )) AS CANT, "
        fSql &= " PESO_NETO , " : vSql &= Dec2Char("RDCC.DIPESN", 15, 3) & " AS PESO_NETO, "
        fSql &= " VOLUMEN   , " : vSql &= "  '' AS VOLUMEN, "
        fSql &= " SERIAL_ETIQUETA , " : vSql &= "  '', "
        fSql &= " EAN_PRODUCTO , " : vSql &= "  RIIM.REANP EAN_PRODUCTO, "

        fSql &= " CANT_PEDIDA , " : vSql &= Dec2Char("RDCC.DCANT * IIM.IVULP", 12, 3) & " As CANT_PEDIDA, "
        fSql &= " CANT_DESP , " : vSql &= Dec2Char("RDCC.DCANT * IIM.IVULP", 12, 3) & " As CANT_DESP, "

        fSql &= " EAN_ENVIO , " : vSql &= "  EST.STDOCK EAN_ENVIO, "
        fSql &= " AVCUST    , " : vSql &= "  CLI.CCUST, "
        fSql &= " AVSHIP    , " : vSql &= "  RDCC.DSHIP, "
        fSql &= " AVPROD    , " : vSql &= "  FD.DCODPRO, "
        fSql &= " AVCONSO   ) " : vSql &= "  RDCC.DCONSO "
        vSql &= " From "

        vSql &= "    RFFACCAB FC "
        vSql &= "    INNER JOIN  RFFACDET FD  ON  FC.FPREFIJ = FD.DPREFIJ  AND FC.FFACTUR = FD.DFACTUR "
        vSql &= "    INNER JOIN  RCML01 CLI   ON  FC.FCLIENT = CLI.CCUST "
        vSql &= "    LEFT JOIN   RCML01 CORP  ON  CLI.CCCUS = CORP.CCUST "
        vSql &= "    LEFT JOIN  (RHCC INNER JOIN AVM ON RHCC.HPROVE = AVM.VENDOR ) ON FC.FCONSOL = RHCC.HCONSO "
        vSql &= "    INNER JOIN  RIIM         ON  FD.DCODPRO = RIIM.RIPROD "
        vSql &= "    INNER JOIN  RDCC         ON  FD.DPEDIDO = RDCC.DPEDID AND FD.DLINPED = RDCC.DLINEA "
        vSql &= "    INNER JOIN  EST          ON  RDCC.DCUST = EST.TCUST   AND RDCC.DSHIP = EST.TSHIP "
        vSql &= "    INNER JOIN  IIM          ON  FD.DCODPRO = IIM.IPROD "

        vSql &= " Where "
        vSql &= "  FC.FPREFIJ = '" & Prefijo & "' And "
        vSql &= "  FC.FFACTUR = " & Factura
        DB.ExecuteSQL(fSql & vSql)
        Return txt_Aviso(numeroAviso)

    End Function

    Function txt_Aviso(numAviso As String) As String
        Dim rs As New ADODB.Recordset
        Dim f As System.IO.StreamWriter
        Dim Archivo As String


        Archivo = DB.getLXParam("AVISODESPACHO") & numAviso & "-" & Prefijo.Trim() & Factura & ".txt" '"\\192.168.8.70\publico\Mapa_Aviso_Despacho\" & numAviso

        f = My.Computer.FileSystem.OpenTextFileWriter(Archivo, True)

        fSql = " SELECT "
        fSql &= " EAN_PROV        ||  '{TAB}' || "
        fSql &= " EAN_CLI         ||  '{TAB}' || "
        fSql &= " NUM_AVISO       ||  '{TAB}' || "
        fSql &= " FECHA_DOC       ||  '{TAB}' || "
        fSql &= " FECHA_DES       ||  '{TAB}' || "
        fSql &= " FECHA_ENT       ||  '{TAB}' || "
        fSql &= " ORD_COMPRA      ||  '{TAB}' || "
        fSql &= " FACTURA         ||  '{TAB}' || "
        fSql &= " EAN_TRANSP      ||  '{TAB}' || "
        fSql &= " EAN_CLICORP     ||  '{TAB}' || "
        fSql &= " EAN_CORPROV     ||  '{TAB}' || "
        fSql &= " NIT_PROV        ||  '{TAB}' || "
        fSql &= " NIT_CLI         ||  '{TAB}' || "
        fSql &= " EAN_PENVIO      ||  '{TAB}' || "
        fSql &= " PLACA_VEH       ||  '{TAB}' || "
        fSql &= " EMPAQUE         ||  '{TAB}' || "
        fSql &= " CANT            ||  '{TAB}' || "
        fSql &= " PESO_NETO       ||  '{TAB}' || "
        fSql &= " VOLUMEN         ||  '{TAB}' || "
        fSql &= " SERIAL_ETIQUETA ||  '{TAB}' || "
        fSql &= " EAN_PRODUCTO    ||  '{TAB}' || "
        fSql &= " CANT_PEDIDA     ||  '{TAB}' || "
        fSql &= " CANT_DESP       ||  '{TAB}' || "
        fSql &= " EAN_ENVIO  "
        fSql &= " FROM RFAVDES "
        fSql &= " WHERE NUM_AVISO = '" & numAviso & "' "
        rs = DB.ExecuteSQL(fSql)
        Do While Not rs.EOF
            f.WriteLine(Replace(rs(0).Value, "{TAB}", vbTab))
            rs.MoveNext()
        Loop
        rs.Close()
        rs = Nothing
        f.Close()

        Return If(File.Exists(Archivo), "Archivo Generado.", "El Archivo no se Creó")

    End Function


    Private Function Dec2Char(campo, longitud, decimales)

        Dec2Char = "LEFT( DIGITS( DEC( {CAMPO}, {LONG}, {DEC} )), {ENTEROS} ) || '.' || RIGHT(DIGITS( DEC( {CAMPO}, {LONG}, {DEC} )), {DEC})"
        Dec2Char = Replace(Dec2Char, "{CAMPO}", campo)
        Dec2Char = Replace(Dec2Char, "{LONG}", longitud)
        Dec2Char = Replace(Dec2Char, "{DEC}", decimales)
        Dec2Char = Replace(Dec2Char, "{ENTEROS}", longitud - decimales)

    End Function

    Private Function fecCampo(campo)
        fecCampo = "CHAR( ( Year({CAMPO}) * 10000 + Month({CAMPO}) * 100 + Day({CAMPO}) ) || DIGITS( DEC( HOUR({CAMPO}) * 100 + MINUTE({CAMPO}), 4, 0 ) ), 12 )"
        fecCampo = Replace(fecCampo, "{CAMPO}", campo)
    End Function


    Function getNumeroAviso() As String
        Return Me.numeroAviso
    End Function

End Class
