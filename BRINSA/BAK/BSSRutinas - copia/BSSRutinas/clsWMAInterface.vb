﻿Imports System.IO

Public Class clsWMAInterface
    Dim qEjecuta As Integer
    Dim sFolder_Data As String
    Dim sFolder_fromHost As String
    Dim sFolder_toHost As String
    Dim sFolder_toHost_BAK As String
    Dim sFolder_LOG As String
    Dim sFolder_rpt As String

    Dim fSql As String
    Dim vSql As String
    Dim ftp As Chilkat.Ftp2

    Public Sub EjecutaInterface()
        Dim rs As New ADODB.Recordset

        DB.gsAppName = "WMACARGUE"
        If DB.rsOpen(rs, "SELECT * FROM ZCCL01 WHERE CCTABL='RFVBVER' AND CCCODE = '" & DB.gsAppName & "'") Then
            qEjecuta = rs("CCUDC1").Value
            If qEjecuta = 9 Then
                rs.Close()
                Return
            End If
        End If
        rs.Close()

        'Si ZCC.CCUDC1 = 1 para la aplicacion Ejecuta
        If qEjecuta <> 0 Then
            If Dir(sFolder_rpt & "\*.log") <> "" Then
                CpyLog()
            End If

            wc("Revisando LX->WMA", 1)

            'LX a WMA
            If qEjecuta = 1 Or qEjecuta = 2 Then
                RevisaHPO_CUR("WMAFTRIN")    'Revisa su hubo cambio de moneda temporal y devuelve la original
                RevisaHPO_CUR("ITP500")      'Revisa su hubo cambio de moneda temporal y devuelve la original
                BajarArchivos()
            End If
            wc("Revisando WMA->LX", 1)

            If qEjecuta = 1 Or qEjecuta = 3 Then
                'SubirRetenidos
                If Dir(sFolder_toHost & "\*.dat.xfr") <> "" Then
                    'WMA a LX
                    SubirArchivo()
                End If
            End If
        Else
            wc("No Ejecuta", 1)
        End If


    End Sub

    '*******************************************************************
    '*                 Bajar Archivos de LX a WMA
    '*******************************************************************
    Sub BajarArchivos()
        Dim rs As ADODB.Recordset

        fSql = "SELECT DPRE, DID, TRIM(DPRE) || DIGITS( DID ) AS DARCH FROM WMAFDNL WHERE DSTS=''"
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            Do While Not rs.EOF
                wc("Recibiendo " & rs("DARCH").Value & ".DAT ...")
                If DB.ExisteTabla(rs("DARCH").Value) Then
                    DB.ExecuteSQL("UPDATE " & rs("DARCH").Value & " SET FIN = '#'")
                    'Actualizar campo TEXTO para no enviar comillas, ni tildes
                    If wrtDownload(rs("DARCH").Value) Then
                        fSql = "UPDATE WMAFDNL SET "
                        fSql &= " DSTS='2', "
                        fSql &= " DDNDT = NOW()  "
                        fSql &= " WHERE "
                        fSql &= " DPRE = '" & rs("DPRE").Value & "' "
                        fSql &= " AND DID = " & rs("DID").Value
                        DB.ExecuteSQL(fSql)

                        fSql = "UPDATE WMAFDOCS SET "
                        fSql &= " OSTS = 'ENVIADO A WMA', "
                        fSql &= " UPDDAT = NOW()  "
                        fSql &= " WHERE "
                        fSql &= " OTIPO = '" & rs("DPRE").Value & "' "
                        fSql &= " AND OCONS = " & rs("DID").Value
                        DB.ExecuteSQL(fSql)

                    Else
                        fSql = "UPDATE WMAFDNL SET "
                        fSql &= " DSTS = '1', "
                        fSql &= " DDNDT = NOW() "
                        fSql &= " WHERE "
                        fSql &= " DPRE = '" & rs("DPRE").Value & "' "
                        fSql &= " AND DID = " & rs("DID").Value
                        DB.ExecuteSQL(fSql)

                        fSql = "UPDATE WMAFDOCS SET "
                        fSql &= " OSTS = 'ERROR ENVIANDO', "
                        fSql &= " UPDDAT = NOW()  "
                        fSql &= " WHERE "
                        fSql &= " OTIPO = '" & rs("DPRE").Value & "' "
                        fSql &= " AND OCONS = " & rs("DID").Value
                        DB.ExecuteSQL(fSql)

                    End If
                End If
                rs.MoveNext()
            Loop
        Else
            wc("Sin archivos para enviar.")
        End If
        rs.Close()
        rs = Nothing
        wc(StrDup(50, "-"))

    End Sub

    '*******************************************************************
    '*                 Suber Archivos de WMA al AS400
    '*******************************************************************
    Sub SubirArchivo()
        Dim rs As ADODB.Recordset
        Dim rs1 As ADODB.Recordset
        Dim qArchivo As String
        Dim fRemoto As String
        Dim Procesar As Integer

        DB.ExecuteSQL("DELETE FROM WMAFTOHOST")
        qArchivo = Dir(sFolder_toHost & "\*.dat.xfr")
        Do While qArchivo <> ""
            DB.ExecuteSQL("INSERT INTO WMAFTOHOST VALUES('" & qArchivo & "')")
            qArchivo = Dir()
        Loop

        rs1 = DB.ExecuteSQL("SELECT ARCHIVO FROM WMAFTOHOST ORDER BY ARCHIVO")
        Do While Not rs1.EOF
            Procesar = 0
            qArchivo = rs1("Archivo").Value
            fSql = "SELECT * FROM WMAFDNL "
            fSql &= " WHERE DPRE='UPL' AND DID=" & Left(qArchivo, 8)
            rs = DB.ExecuteSQL(fSql)
            If rs.EOF Then
                fRemoto = "UP" & Left(qArchivo, 8)
                DB.DropTable(fRemoto)

                fSql = "CREATE TABLE " & fRemoto & " ( " & fRemoto & " CHARACTER (1584) NOT NULL ) "
                DB.ExecuteSQL(fSql)

                Try
                    Using sr As New StreamReader(sFolder_toHost & "\" & qArchivo)
                        Do While Not sr.EndOfStream
                            fSql = "INSERT INTO " & fRemoto & "( " & fRemoto & " ) VALUES('" & sr.ReadLine & "')"
                            DB.ExecuteSQL(fSql)
                        Loop
                    End Using
                Catch e As Exception
                    Console.WriteLine("The file could not be read:")
                    Console.WriteLine(e.Message)
                End Try
                fSql = " INSERT INTO WMAFDNL ("
                vSql = " VALUES( "
                fSql &= " DPRE, " : vSql &= "'UPL', "
                fSql &= " DID, " : vSql &= Left(qArchivo, 8) & ", "
                fSql &= " DSTS) " : vSql &= "'0' ) "
                DB.ExecuteSQL(fSql & vSql)

                fSql = ""
                fSql &= " UPDATE " & fRemoto & " SET " & fRemoto & "="
                fSql &= " SUBSTR(" & fRemoto & ", 1, 90) ||  '00000000' ||"
                fSql &= " SUBSTR( " & fRemoto & ", 99 ) "
                fSql &= " WHERE "
                fSql &= " TRIM(SUBSTR( " & fRemoto & ", 91, 8 )) = ''"
                DB.ExecuteSQL(fSql)

                DB.ExecuteSQL("DELETE FROM WMAFUPL WHERE UNFILE=''") 'UNFILE='ARCHIVO'"
                DB.ExecuteSQL("DELETE FROM WMAFUPL WHERE UNFILE='" & fRemoto & "'")

                fSql = ""
                fSql &= " CPYF FROMFILE(" & DB.gsLib & "/" & fRemoto & ")"
                fSql &= "      TOFILE(" & DB.gsLib & "/WMAFUPL)"
                fSql &= "      MBROPT(*ADD)"
                fSql &= "      FMTOPT(*NOCHK)"
                DB.ExecuteSQL(" {{ " & fSql & " }} ")

                'Borra el archivo ya copiado
                DB.DropTable(fRemoto)

                'Para los que NO son UREC=TR
                fSql = "UPDATE WMAFUPL SET "
                fSql &= " UQTYO=UQTYOC, "
                fSql &= " UQTY=UQTYC, "
                fSql &= " UPESO=UPESOC, "
                fSql &= " UCARFLE=UCARFLEC, "
                fSql &= " UALTSOCO=UALTSOCOC, "
                fSql &= " UALTSODI=UALTSODIC, "
                fSql &= " UCONSO =  '', UPEDID = 0, ULINEA = 0, "
                fSql &= " UNFILE='" & fRemoto & "'"
                fSql &= " WHERE UNFILE='' AND UREC NOT LIKE '%TR%'"
                DB.ExecuteSQL(fSql)

                'Para los que SI son UREC=TR
                fSql = "UPDATE WMAFUPL SET "
                fSql &= " UNFILE='" & fRemoto & "', UREC='TR', "
                fSql &= " UFECHA     = 0, UHORA      = 0, ULINE      = 0, UORCLIL    = 0, "
                fSql &= " UQTYO      = 0, UQTY       = 0, UPESO      = 0, UCARFLE    = 0, "
                fSql &= " UALTSOCO   = 0, UALTSODI   = 0, UCONSO = '', UPEDID = 0, ULINEA = 0"
                fSql &= " WHERE UNFILE='' AND UREC LIKE '%TR%'"
                DB.ExecuteSQL(fSql)

                ActualizaNumPedido(fRemoto)

                FileCopy(sFolder_toHost & "\" & qArchivo, sFolder_toHost_BAK & "\" & qArchivo)
                Kill(sFolder_toHost & "\" & qArchivo)

                'EjecutaUpload fRemoto, val(qArchivo)
                DB.ExecuteSQL("UPDATE WMAFDNL SET DCTL=1, DSTS='1' WHERE DPRE='UPL' AND DID=" & Val(qArchivo))
                fSql = "CALL PGM(WMACUPL) PARM( '" & fRemoto & "' ) "
                DB.ExecuteSQL("{{ " & fSql & " }}")
                'Bitacora("WMASubeArchivo", fSql)

                fSql = " UPDATE WMAFDNL SET"
                fSql &= " DSTS='2', DCTL=2 "
                fSql &= " WHERE "
                fSql &= " DPRE='UPL' "
                fSql &= " AND DID=" & Val(qArchivo)
                DB.ExecuteSQL(fSql)

            End If
            rs.Close()
            rs1.MoveNext()
        Loop
        rs1.Close()

        rs = Nothing
        rs1 = Nothing

    End Sub

    'Devuelve moneda de pesos a dolares
    Sub RevisaHPO_CUR(qArchivo As String)
        Dim rs As ADODB.Recordset
        Dim rs1 As ADODB.Recordset

        fSql = " SELECT DISTINCT HPO.*, CCCODE FROM " & qArchivo & " INNER JOIN HPO ON TREF=PORD AND THLIN = PLINE "
        fSql &= " INNER JOIN ZCC ON CCTABL = 'PPPCURR'  AND CCUDC1 = POKAN"
        fSql &= " WHERE POKAN > 0 AND LXMSG <> '' "
        rs = DB.ExecuteSQL(fSql)
        Do While Not rs.EOF
            fSql = " SELECT *  "
            fSql &= " FROM " & qArchivo & "  "
            fSql &= " WHERE TREF = " & rs("PORD").Value & " AND LXMSG = '' "
            rs1 = DB.ExecuteSQL(fSql)
            If rs1.EOF Then
                fSql = "UPDATE HPO SET POCUR = '" & rs("CCCODE").Value & "', POKAN = 0 "
                fSql &= " WHERE PORD = " & rs("PORD").Value
                DB.ExecuteSQL(fSql)
                DB.Bitacora("MONEDA", "Retorno de Moneda OC[" & CStr(rs("PORD").Value) & "] Moneda[" & rs("POCUR").Value & "] Tasa[" & rs("PIEXRT").Value & "]", fSql)
            End If
            rs1.Close()
            rs.MoveNext()
        Loop
        rs.Close()
        rs = Nothing
        rs1 = Nothing

    End Sub

    Sub CpyLog()
        Dim qArchivo As String
        Dim mystream As New ADODB.Stream
        Dim rstUpload As New ADODB.Recordset

        mystream.Type = ADODB.StreamTypeEnum.adTypeBinary

        qArchivo = Dir(sFolder_rpt & "\*.log")

        Do While qArchivo <> ""
            Debug.Print(qArchivo)
            mystream.Open()
            mystream.LoadFromFile(sFolder_rpt & "\" & qArchivo)

            fSql = "SELECT * FROM WMAFDNLR "
            fSql &= " WHERE "
            fSql &= " RPRE    ='" & UCase(Mid(qArchivo, 11, 3)) & "' "
            fSql &= " AND RID = " & Val(Mid(qArchivo, 14, 7))

            DB.rsOpen(rstUpload, fSql, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)
            With rstUpload
                If .EOF Then
                    .AddNew()
                    .Fields("RPRE").Value = UCase(Mid(qArchivo, 11, 3))
                    .Fields("RID").Value = Val(Mid(qArchivo, 14, 7))
                    .Fields("R_LOG").Value = mystream.Read
                    .Update()
                End If
                .Close()
            End With
            qArchivo = Dir()
            mystream.Close()
        Loop
        rstUpload = Nothing
        mystream = Nothing

    End Sub

    Public Sub ActualizaNumPedido(fRemoto As String)
        Dim rs As ADODB.Recordset
        Dim bUpd As Boolean

        rs = DB.ExecuteSQL("SELECT * FROM WMAFUPL WHERE UNFILE='" & fRemoto & "' AND UREC NOT LIKE '%TR%' ")
        Do While Not rs.EOF
            bUpd = False
            fSql = "UPDATE WMAFUPL SET "
            If rs("UCLAS").Value = "INVEQTY" And rs("UOBJ").Value = "MANUAL" And rs("UACC").Value = "ADJUST" Then
                fSql &= " UORDTY= '' "
                bUpd = True
            End If
            If rs("UCLAS").Value = "INVE" And rs("UOBJ").Value = "MANUAL" And rs("UACC").Value = "CHANGE" Then
                fSql &= " UORDTY= '' "
                bUpd = True
            End If
            fSql &= " WHERE UNFILE='" & fRemoto & "' AND UREC='" & rs("UREC").Value & "' "
            If bUpd Then
                DB.ExecuteSQL(fSql)
            End If
            rs.MoveNext()
        Loop
        rs.Close()
        rs = Nothing

    End Sub

    Function ftpOpen() As Boolean
        Dim success As Integer

        ftp = New Chilkat.Ftp2

        ' Any string unlocks the component for the 1st 30-days.
        success = ftp.UnlockComponent("SMANRIFTP_keysbDt3oHn3")
        If (success <> 1) Then
            ShowLog(ftp.LastErrorText, True)
            Return False
        End If

        ftp.Hostname = DB.gsDatasource
        ftp.Username = "APLLX"
        ftp.Password = "LXAPL"

        ' Connect and login to the FTP server.
        success = ftp.Connect()
        If (success <> 1) Then
            ftpOpen = False
            ShowLog(ftp.LastErrorText, True)
            Exit Function
        End If

        ' Change to the remote directory where the file will be uploaded.
        success = ftp.ChangeRemoteDir(DB.gsLib)
        If (success <> 1) Then
            ftpOpen = False
            ShowLog(ftp.LastErrorText, True)
        Else
            ftpOpen = True
        End If

    End Function

    Function ftpDownload(localFilename As String, remoteFilename As String, Tipo As String) As Boolean
        Dim success As Integer

        If Tipo = "ASCII" Then
            Me.ftp.SetTypeAscii()
        Else
            Me.ftp.SetTypeBinary()
        End If

        success = ftp.GetFile(remoteFilename, localFilename)
        If (success <> 1) Then
            ShowLog(ftp.LastErrorText, True)

            ftpDownload = False
        Else
            ShowLog(ftp.LastErrorText, False)
            ftpDownload = True
        End If

    End Function

    Function wrtDownload(remoteFilename As String) As Boolean
        Dim rs As ADODB.Recordset

        rs = DB.ExecuteSQL("SELECT TEXTO || '#' AS DATO FROM " & remoteFilename)

        Do While Not rs.EOF
            Using outputFile As New StreamWriter(sFolder_fromHost & Convert.ToString("\" & remoteFilename & ".DAT"), True)
                outputFile.WriteLine(rs("DATO").Value)
            End Using
            rs.MoveNext()
        Loop
        rs.Close()

        rs = Nothing
        wrtDownload = True

        fSql = " INSERT INTO WMAFSEND( PRE, ID, TEXTO, FIN, ARCHIVO )"
        fSql &= " SELECT '" & Left(remoteFilename, 3) & "', " & Val(Right(remoteFilename, 7)) & ", "
        fSql &= " A.*, '" & sFolder_fromHost & Convert.ToString("\" & remoteFilename & ".DAT") & "' "
        fSql &= "FROM " & remoteFilename & " A"
        DB.ExecuteSQL(fSql)

        DB.DropTable(remoteFilename)

    End Function


    Function ftpUpload(localFilename As String, remoteFilename As String) As Boolean
        Dim success As Integer

        Me.ftp.SetTypeAscii()
        success = ftp.PutFile(localFilename, remoteFilename)
        If (success <> 1) Then
            ftpUpload = False
        Else
            ShowLog(ftp.LastErrorText, False)
            ftpUpload = True
        End If

    End Function




    Sub ShowLog(txt As String, bErr As Boolean)


        wc("toBPCS=" & sFolder_toHost & vbCr & "fromBPCS=" & sFolder_fromHost, 1)

    End Sub


    Public Sub New()
        qEjecuta = 0

        sFolder_toHost = DB.getLXParam("WMA_TO_AS400")
        sFolder_fromHost = DB.getLXParam("WMA_FROM_AS400")
        sFolder_toHost_BAK = sFolder_toHost & ".save"
        sFolder_LOG = DB.getLXParam("WMA_LOG")
        sFolder_rpt = DB.getLXParam("WMA_RPT")

    End Sub

End Class
