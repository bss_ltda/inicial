﻿Imports System.Xml.Serialization
Imports System.IO
Imports BSSRutinas.modConstantes

Module Principal

    Sub Main(ByVal Parametros As String())

        If My.Settings.LOCAL = "SI" Then
            Principal(Parametros)
        Else
            Try
                Principal(Parametros)
            Catch ex As Exception
                DB.WrtSqlError(DB.lastSQL, ex.ToString)
                Tarea.setMsgError(ex.ToString)
                Tarea.GuardaError()
            End Try
        End If

    End Sub

    Sub Principal(ByVal Parametros As String())
        Dim i As Integer
        Dim resultado As String = ""
        Dim msgTask As String = ""

        If Not AbreConexion(Parametros(0)) Then
            wc("No se pudo conectar")
            Exit Sub
        Else
            wc("Conexion OK")
        End If

        If My.Settings.LOCAL = "SI" Then
            DB.LXLONG_CAMPO = "CCNOT2"
        End If
        Tarea.idTarea = Parametros(0)

        If Tarea.LeeTarea() Then
            Tarea.Sts1 = "1"
            Console.WriteLine(Tarea.Categoria)
            DB.gsUserOP = Tarea.Usuario
            For i = 0 To UBound(Parametros)
                DB.gsKeyWords &= Parametros(i) & " "
            Next

            DB.gsKeyWords &= " " & Tarea.Categoria & " " & Tarea.Programa
            DB.appUser = Tarea.Usuario
            DB.ModuloActual &= "." & Tarea.Categoria

            Select Case Tarea.Categoria
                Case "WMSCOPED"
                    WMSCOPED(Tarea.Claves, Tarea.Parametros)
                Case "WMS INTERFACE"
                    WMSInterface()
                Case "WMA INTERFACE"
                    WMAInterface()
                Case "WMS REPPRODUCCION"
                    ReporteProduccion()
                Case "WMS ARTICULO"
                    WMSNuevoArticulo()
                Case "RECC", "SMSSALIDA"
                    ActualizaRECC(Tarea.Claves)
                Case "CROSSDOCKING"
                    Crossdocking()
                Case "LIBCONSO"
                    LiberaConso(Tarea.Claves)
                Case "INFOSEGUIMIENTO"
                    InfoSeguimiento()
                Case "PEDIDOS"
                    OperacionesPedidos()
                Case "NOCONSOLIDAR"
                    NoConsolidar()
                Case "INDPROD"
                    IndicesProduccion(Parametros(1), Parametros(2))
                Case "REP_DESTINOSEGURO"
                    DestinoSeguro()
                Case "BODSAT2"
                    BodSat2_Consos(Tarea.Claves)
                Case "BODSAT_SALDO"
                    DB.ModuloActual &= ".BODSAT_SALDO"
                Case "MDA"
                    mdaTimer(Tarea.Claves, Tarea.Parametros)
                Case "WMSLOTE"
                    WMSLote(Tarea.Claves, Tarea.Parametros)
                Case "NCAC"
                    'Notas Credito Acuerdos Comerciales
                    NCAcuerdosComerciales(Tarea.idTarea, Tarea.Parametros)
                Case "AVISODESPACHO"
                    'Aviso de Despacho EDI
                    AvisoDespacho(Tarea.idTarea, Tarea.Parametros)
                Case Else
                    resultado = "Rutina no parametrizada"
            End Select
        End If
        Tarea.ActualizaTarea()
        DB.Close()

    End Sub

    Sub ActualizaRECC(Conso As String)
        Dim RECC As New clsEntregasConso

        DB.ModuloActual &= "." & Tarea.Categoria
        DB.gsKeyWords = Conso
        RECC.Consolidado = Conso
        RECC.ActualizaRECC()
        Tarea.setResultado("RUTINA EJECUTADA")

    End Sub

    Sub WMSInterface()

        Dim wms As New clsWMSInterface
        wms.EjecutaInterface()

        If Tarea.Sts1 = "1" Then
            Tarea.Sts1 = "0"
        End If

    End Sub

    Sub WMAInterface()

        Dim wma As New clsWMAInterface
        wma.EjecutaInterface()
        If Tarea.Sts1 = "1" Then
            Tarea.Sts1 = "0"
        End If

    End Sub

    Sub WMSNuevoArticulo()
        Dim sku As New clsWMS
        With sku
            .Producto = Tarea.getParametro("Producto")
            .WMS_EnviaArticulo()

        End With
    End Sub

    Sub ReporteProduccion()

        Dim rep As New clsReporteProduccion
        With rep
            .Fecha = DateTime.ParseExact(Tarea.getParametro("FECHA"), "yyyy-MM-dd", Nothing)
            .Turno = Tarea.getParametro("TURNO")
            .Prod = Tarea.getParametro("PRODUCTO")
            .ReportesProduccion()
        End With

    End Sub

    Sub Crossdocking()
        Dim cxd As New clsCrossdocking

        DB.ModuloActual &= "." & Tarea.Categoria

        Select Case Tarea.Subcategoria
            Case "MARCAR"
                DB.gsKeyWords = Tarea.getParametro("CONSO")
                cxd.Conso = Tarea.getParametro("CONSO")
                cxd.PreparaCrossdocking()
            Case "CONSOLIDAR"
                DB.gsKeyWords = Tarea.getParametro("WRKID")
                cxd.WrkID = Tarea.getParametro("WRKID")
                cxd.Despacho()

        End Select
        Tarea.setResultado("RUTINA EJECUTADA")

    End Sub
    Sub LiberaConso(Conso As String)

        Dim xConso As New clsLib_Consos
        DB.gsKeyWords = Conso
        xConso.Conso = Conso
        xConso.LiberaConso()
        Tarea.setResultado("RUTINA EJECUTADA")

    End Sub

    Sub BodSat2_Consos(WrkID As String)
        Dim bs2 As New clsBodSat2

        DB.gsKeyWords = WrkID
        With bs2
            .WrkID = WrkID
            .Despacho()
        End With
        Tarea.setResultado("RUTINA EJECUTADA")

    End Sub

    Sub WMSCOPED(Conso As String, HSFCNS As String)

        DB.ModuloActual &= ".WMSCOPED"
        If Conso = "" Then
            Conso = Tarea.getParametro("CONSO")
            If Conso = "" Then
                Tarea.Sts1 = "-9"
                Tarea.setResultado("Consolidado en blanco")
                Return
            End If
        End If

        Dim RECC As New clsLib_Consos
        DB.gsKeyWords = Conso
        RECC.Conso = Conso
        RECC.HSFCNS = Tarea.getParametro("HSFCNS")
        RECC.WMSCOPED()
        Tarea.setResultado("RUTINA EJECUTADA")

    End Sub

    Sub mdaTimer(Conso As String, HSFCNS As String)

        If Conso = "" Then
            Tarea.Sts1 = "-9"
            Tarea.setResultado("Consolidado en blanco")
        End If

        Dim xConso As New clsLib_Consos
        DB.gsKeyWords = Conso
        With xConso
            .Conso = Conso
            .HSFCNS = Tarea.getParametro("HSFCNS")
            .timerConsoCargado()
        End With
        Tarea.setResultado("RUTINA EJECUTADA")

    End Sub

    Sub IndicesProduccion(Desde As String, Hasta As String)
        Dim rs As New ADODB.Recordset
        Dim fDesde As Date = dateDecToDate(Desde)
        Dim fHasta As Date = dateDecToDate(Hasta)

        Do While fDesde <= fHasta
            Console.WriteLine(fDesde.ToString("yyyyMMdd"))
            DB.ExecuteSQL("{{ CALL RRPRIND '" & fDesde.ToString("yyyyMMdd") & "' }}")
            fDesde = DateAdd("d", 1, fDesde)
        Loop

        comandoEjecutado("INDPROD", "OK")

    End Sub

    Sub NCAcuerdosComerciales(numTarea As Integer, parametro As String)
        Dim nc As New clsNC
        With nc
            .Periodo = Tarea.getParametro("PERIODO")
            .Usuario = Tarea.getParametro("USUARIO")
            .setPaquete(Tarea.getParametro("PAQUETE"))
            .ParametrosNC(Tarea.getParametro("ACUERDO"))
            Select Case Tarea.getParametro("ACUERDO")
                Case "22"
                    .AcuerdoComercialNC22()
                    Tarea.Job400 = .getPaquete()
                Case "23"
                    .AcuerdoComercialNC23(Tarea.getParametro("WRKID"))
                    Tarea.Job400 = .getPaquete()
                Case "57"
                    .setNumeroProceso(Tarea.getParametro("PROCESO"))
                    .LiquidaNC57()
                    Tarea.Job400 = .getNumeroProceso()
            End Select
            .Cerrar()
            Tarea.setResultado("RUTINA EJECUTADA")
        End With

    End Sub

    Sub AvisoDespacho(parametro As String, ByRef resultado As String)
        Dim ad As New clsFacturas
        Dim fSql As String

        With ad
            .Prefijo = Tarea.getParametro("PREF")
            .Factura = Tarea.getParametro("FACTURA")
            Tarea.setResultado(.GeneraAvisoDespacho())
            fSql = " UPDATE RFTASK SET  "
            fSql &= "   TJOB400 = '" & .getNumeroAviso() & "' "
            fSql &= " , TUPDDAT = NOW()"
            fSql &= " WHERE TNUMREG = " & Tarea.idTarea
            DB.ExecuteSQL(fSql)
        End With

    End Sub

    Sub OperacionesPedidos()
        Dim ped As New PedidoAbrirLinea

        DB.ModuloActual &= "." & Tarea.Categoria

        Select Case Tarea.Subcategoria
            Case "ABRIR LINEA"
                DB.gsKeyWords = Tarea.getParametro("Pedido")
                With ped
                    .Pedido = Tarea.getParametro("Pedido")
                    .LineaPedido = Tarea.getParametro("LineaPedido")
                    .CantidadActual = Tarea.getParametro("CantidadActual")
                    .CantidadNueva = Tarea.getParametro("CantidadNueva")
                    .AbrirLineaPedido()
                End With

        End Select
        Tarea.setResultado("RUTINA EJECUTADA")

    End Sub
    Sub InfoSeguimiento()
        Dim Info As New clsInfoSeguimiento

        DB.ModuloActual &= ".INFOSEGUIMIENTO"
        With Info
            .C_XID = CInt(Tarea.getParametro("XID")) ' Tarea.PalabraClave
            .qInforme = CInt(Tarea.getParametro("PERFIL"))
            .UPDDatos()
        End With

    End Sub

    Sub NoConsolidar()
        Dim Info As New clsInfoSeguimiento

        DB.ModuloActual &= ".INFOSEGUIMIENTO"
        With Info
            .ExcepcionesNoConsolidar()
        End With

    End Sub

    Sub DestinoSeguro()
        Dim rep As New ReporteDestinoSeguro
        rep.Consolidado = Tarea.Parametros
        Tarea.setResultado(rep.reporte())
    End Sub


    Function AbreConexion(ModuloActual As String) As Boolean
        wc("Abriendo Conexion")
        wc(My.Settings.AMBIENTE)
        DB = New clsConexion
        DB.bEnDebug = (My.Settings.LOCAL = "SI")
        DB.gsDatasource = My.Settings.AS400
        DB.gsLib = My.Settings.AMBIENTE
        DB.ModuloActual = ModuloActual
        DB.bDateTimeToChar = True

        Return DB.Conexion()

    End Function

    Sub WMSLote(ModuloActual As String, Parametros As String)
        Dim WMS As New clsWMS
        Dim regs As Integer
        With WMS
            .ModuloActual = ModuloActual
            .Producto = Tarea.getParametro("PRODUCTO")
            .Lote = Tarea.getParametro("LOTE")
        End With

        If WMS.Conecta() Then
            regs = WMS.venceLote()
            If regs <> 1 Then
                Tarea.setResultado("Registros actualizados: " & regs)
            Else
                Tarea.setResultado("Registro actualizado: " & regs)
            End If
            WMS.Desconecta()
        Else
            Tarea.setResultado("No conecta WMS")
        End If

    End Sub
    Sub comandoEjecutado(ByVal rutina As String, ByVal msg As String)
        Dim fSql As String

        fSql = " UPDATE ZCC SET CCUDC1 = 0"
        fSql &= ", CCNOT1 = 'Finalizó " & Now.ToString("yyyy-MM-dd HH:mm:ss") & "' "
        fSql &= ", CCNOT2 = '" & Left(msg, 30) & "'  "
        fSql &= " WHERE CCTABL = 'RUTINAS'  AND CCCODE = '" & UCase(rutina) & "' "
        DB.ExecuteSQL(fSql)

    End Sub

    <Serializable()> _
    Public Class SaldoInventario
        Public Usuario As String
        Public Password As String
        Public Bodega As String
        Public CodigoProducto As String

        'Public Sub New(Usuario As String, Password As String, Bodega As String, CodigoProducto As String)
        '    Me.Usuario = Usuario
        '    Me.Password = Password
        '    Me.Bodega = Bodega
        '    Me.CodigoProducto = CodigoProducto
        'End Sub

    End Class

    Public Sub saldito()

        Dim si As New SaldoInventario '("u", "p", "b", "p")

        With si
            .Usuario = "u"
            .Password = "p"
            .Bodega = "EM"
            .CodigoProducto = "p"
        End With

        Dim xml_serializer As New XmlSerializer(GetType(SaldoInventario))
        Dim string_writer As New StringWriter
        xml_serializer.Serialize(string_writer, si)
        Console.WriteLine(string_writer.ToString())
        string_writer.Close()



    End Sub

End Module


