﻿Imports System.IO

Public Class clsWMSInterface
    Dim qEjecuta As Integer
    Dim sFolder_fromHost As String
    Dim sFolder_toHost As String
    Dim sFolder_toHost_BAK As String
    Dim sFolder_LOG As String
    Dim sFolder_rpt As String

    Dim fSql As String
    Dim vSql As String
    Dim dictConsos As New Dictionary(Of String, String)


    Public Sub EjecutaInterface()
        Dim rs As New ADODB.Recordset

        DB.gsAppName = "WMSCARGUE"
        If DB.OpenRS(rs, "SELECT CCUDC1 FROM ZCCL01 WHERE CCTABL = 'RFVBVER' AND CCCODE = '" & DB.gsAppName & "'") Then
            qEjecuta = rs("CCUDC1").Value
            If qEjecuta = 9 Then
                rs.Close()
                Return
            End If
        End If
        rs.Close()

        'Si ZCC.CCUDC1 = 1 para la aplicacion Ejecuta
        If qEjecuta <> 0 Then
            wc("Revisando LX->WMS", 1)
            'BPCS a WMS
            If qEjecuta = 1 Or qEjecuta = 2 Then
                BajarArchivos()
            End If

            wc("Revisando WMS->LX", 1)
            If qEjecuta = 1 Or qEjecuta = 3 Then
                'SubirRetenidos    12:45 p.m. 2015-01-17 Ya no es necesario porque ya está en el EjecutaUpload
                If Dir(sFolder_toHost & "\*.dat.xfr") <> "" Then
                    'WMS a BPCS
                    SubirArchivo()
                End If
            End If
        Else
            wc("No Ejecuta", 1)
        End If


    End Sub

    '*******************************************************************
    '*                 Bajar Archivos de LX a WMS
    '*******************************************************************
    Sub BajarArchivos()
        Dim rs As ADODB.Recordset
        fSql = "SELECT DPRE, DID, TRIM(DPRE) || DIGITS( DEC( DID, 7, 0 ) ) AS DARCH FROM WMSFDNL WHERE DSTS=''"
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            Do While Not rs.EOF
                wc("Recibiendo " & rs("DARCH").Value & ".DAT ...")
                DB.gsKeyWords = rs("DARCH").Value & ".DAT"
                'Actualizar campo TEXTO para no enviar comillas, ni tildes
                If wrtDownload(rs("DARCH").Value) Then
                    fSql = "UPDATE WMSFDNL SET "
                    fSql &= " DSTS='2', "
                    fSql &= " DDNDT=NOW()  "
                    fSql &= " WHERE "
                    fSql &= " DPRE='" & rs("DPRE").Value & "' "
                    fSql &= " AND DID=" & rs("DID").Value
                Else
                    fSql = "UPDATE WMSFDNL SET "
                    fSql &= " DSTS='1', "
                    fSql &= " DDNDT=NOW() "
                    fSql &= " WHERE "
                    fSql &= " DPRE='" & rs("DPRE").Value & "' "
                    fSql &= " AND DID=" & rs("DID").Value
                End If
                DB.ExecuteSQL(fSql)
                rs.MoveNext()
            Loop
        Else
            wc("Sin archivos para enviar.")
        End If
        rs.Close()
        rs = Nothing
        wc(StrDup(50, "-"))
    End Sub

    '*******************************************************************
    '*                 Suber Archivos de WMS a AS400
    '*******************************************************************
    Sub SubirArchivo()
        Dim rs As ADODB.Recordset
        Dim rs1 As ADODB.Recordset
        Dim qArchivo As String
        Dim fRemoto As String
        Dim Procesar As Integer

        qArchivo = Dir(sFolder_toHost & "\*.dat.xfr")
        DB.ExecuteSQL("DELETE FROM WMSFTOHOST")
        Do While qArchivo <> ""
            DB.ExecuteSQL("INSERT INTO WMSFTOHOST VALUES('" & qArchivo & "')")
            qArchivo = Dir()
        Loop

        rs1 = DB.ExecuteSQL("SELECT ARCHIVO FROM WMSFTOHOST ORDER BY ARCHIVO")
        Do While Not rs1.EOF
            Procesar = 0
            qArchivo = rs1("ARCHIVO").Value
            DB.gsKeyWords = qArchivo

            If False Then
                fSql = "DELETE FROM WMSFDNL "
                fSql &= " WHERE DPRE='UPL' AND DID=" & Left(qArchivo, 8)
                DB.ExecuteSQL(fSql)
            End If

            fSql = "SELECT * FROM WMSFDNL "
            fSql &= " WHERE DPRE='UPL' AND DID=" & Left(qArchivo, 8)
            rs = DB.ExecuteSQL(fSql)
            If rs.EOF Then
                fRemoto = "UP" & Left(qArchivo, 8)
                DB.dropTable(fRemoto)

                fSql = "CREATE TABLE " & fRemoto & " ( " & fRemoto & " CHARACTER (1584) NOT NULL ) "
                DB.ExecuteSQL(fSql)

                Try
                    Using sr As New StreamReader(sFolder_toHost & "\" & qArchivo)
                        Do While Not sr.EndOfStream
                            fSql = "INSERT INTO " & fRemoto & "( " & fRemoto & " ) VALUES('" & sr.ReadLine & "')"
                            DB.ExecuteSQL(fSql)
                        Loop
                    End Using
                Catch e As Exception
                    Console.WriteLine("The file could not be read:")
                    Console.WriteLine(e.Message)
                End Try


                fSql = " INSERT INTO WMSFDNL ("
                vSql = " VALUES( "
                fSql &= " DPRE, " : vSql &= "'UPL', "
                fSql &= " DID, " : vSql &= Left(qArchivo, 8) & ", "
                fSql &= " DSTS) " : vSql &= "'0' ) "
                DB.ExecuteSQL(fSql & vSql)

                fSql = ""
                fSql &= " UPDATE " & fRemoto & " SET " & fRemoto & "="
                fSql &= " SUBSTR(" & fRemoto & ", 1, 90) ||  '00000000' ||"
                fSql &= " SUBSTR( " & fRemoto & ", 99 ) "
                fSql &= " WHERE "
                fSql &= " TRIM(SUBSTR( " & fRemoto & ", 91, 8 )) = ''"
                DB.ExecuteSQL(fSql)

                DB.ExecuteSQL("DELETE FROM WMSFUPL WHERE UNFILE=''") 'UNFILE='ARCHIVO'"
                DB.ExecuteSQL("DELETE FROM WMSFUPL WHERE UNFILE='" & fRemoto & "'")

                fSql = ""
                fSql &= " CPYF FROMFILE(" & DB.gsLib & "/" & fRemoto & ")"
                fSql &= "      TOFILE(" & DB.gsLib & "/WMSFUPL)"
                fSql &= "      MBROPT(*ADD)"
                fSql &= "      FMTOPT(*NOCHK)"
                DB.ExecuteSQL(" {{ " & fSql & " }} ")

                'Borra el archivo ya copiado
                DB.DropTable(fRemoto)

                'Para los que NO son UREC=TR
                fSql = "UPDATE WMSFUPL SET "
                fSql &= " UQTYO=UQTYOC, "
                fSql &= " UQTY=UQTYC, "
                fSql &= " UPESO=UPESOC, "
                fSql &= " UCARFLE=UCARFLEC, "
                fSql &= " UALTSOCO=UALTSOCOC, "
                fSql &= " UALTSODI=UALTSODIC, "
                fSql &= " UNFILE='" & fRemoto & "'"
                fSql &= " WHERE UNFILE='' AND UREC NOT LIKE '%TR%'"
                DB.ExecuteSQL(fSql)

                'Para los que SI son UREC=TR
                fSql = "UPDATE WMSFUPL SET "
                fSql &= " UNFILE='" & fRemoto & "', UREC='TR', "
                fSql &= " UFECHA     = 0, UHORA      = 0, ULINE      = 0, UORCLIL    = 0, "
                fSql &= " UQTYO      = 0, UQTY       = 0, UPESO      = 0, UCARFLE    = 0, "
                fSql &= " UALTSOCO   = 0, UALTSODI   = 0, UPEDID     = 0, ULINEA     = 0"
                fSql &= " WHERE UNFILE='' AND UREC LIKE '%TR%'"
                DB.ExecuteSQL(fSql)

                ActualizaNumPedido(fRemoto)

                FileCopy(sFolder_toHost & "\" & qArchivo, sFolder_toHost_BAK & "\" & qArchivo)
                Kill(sFolder_toHost & "\" & qArchivo)

                EjecutaUpload(fRemoto, Val(qArchivo))


            End If
            rs.Close()
            rs1.MoveNext()
        Loop
        rs1.Close()
        rs = Nothing
        rs1 = Nothing

    End Sub

    Sub CpyLog()
        Dim qArchivo As String
        Dim mystream As New ADODB.Stream
        Dim rstUpload As New ADODB.Recordset

        mystream.Type = ADODB.StreamTypeEnum.adTypeBinary
        qArchivo = Dir(sFolder_rpt & "\*.log")

        Do While qArchivo <> ""
            Debug.Print(qArchivo)
            mystream.Open()
            mystream.LoadFromFile(sFolder_rpt & "\" & qArchivo)

            fSql = "SELECT * FROM WMSFDNLR "
            fSql &= " WHERE "
            fSql &= " RPRE    ='" & UCase(Mid(qArchivo, 11, 3)) & "' "
            fSql &= " AND RID = " & Val(Mid(qArchivo, 14, 7))

            DB.rsOpen(rstUpload, fSql)
            With rstUpload
                If .EOF Then
                    .AddNew()
                    .Fields("RPRE").Value = UCase(Mid(qArchivo, 11, 3))
                    .Fields("RID").Value = Val(Mid(qArchivo, 14, 7))
                    .Fields("R_LOG").Value = mystream.Read
                    .Update()
                End If
                .Close()
            End With
            qArchivo = Dir()
            mystream.Close()
        Loop
        rstUpload = Nothing
        mystream = Nothing

    End Sub

    Sub EjecutaUpload(fRemoto As String, IdUpload As Double)
        Dim bEjecutar As Boolean
        Dim rs As ADODB.Recordset
        Dim rs1 As ADODB.Recordset
        Dim rs2 As ADODB.Recordset
        Dim result As Integer
        Dim s As String
        Dim hora As Integer
        result = 0
        bEjecutar = True

        DB.gsKeyWords = fRemoto & "." & IdUpload.ToString()

        Dim pair As KeyValuePair(Of String, String)
        For Each pair In dictConsos
            DB.gsKeyWords &= "." & pair.Value
        Next

        fSql = "INSERT INTO WMSFUPLP( UNFILE, UPEDID, UCONSO ) "
        fSql &= " SELECT DISTINCT U.UNFILE, U.UPEDID, U.UCONSO   "
        fSql &= " FROM WMSFUPL U INNER JOIN WMSFPARM ON U.UCLAS=V5CLAS AND U.UOBJ=V5OBJ "
        fSql &= " AND U.UACC=V5ACC AND U.UORDTY=V5ORTY AND V5DESP= 'Y'  "
        fSql &= " LEFT OUTER JOIN WMSFUPLP P ON U.UPEDID=P.UPEDID AND U.UCONSO = P.UCONSO "
        fSql &= " WHERE U.UNFILE='" & fRemoto & "' "
        fSql &= " AND P.UPEDID IS NULL "
        rs = DB.ExecuteSQL(fSql)

        'Actualiza fecha/hora fin de cargue
        fSql = " SELECT U.UCONSO, MAX(U.UFECHA) FECHA, MAX(U.UHORA) HORA "
        fSql &= " FROM WMSFUPL U INNER JOIN WMSFPARM ON U.UCLAS=V5CLAS AND U.UOBJ=V5OBJ  "
        fSql &= " AND U.UACC=V5ACC AND U.UORDTY=V5ORTY AND V5DESP= 'Y'   "
        fSql &= " WHERE U.UNFILE='" & fRemoto & "'  "
        fSql &= " GROUP BY U.UCONSO  "
        rs = DB.ExecuteSQL(fSql)
        Do While Not rs.EOF
            fSql = "UPDATE RHCC SET "
            hora = rs("FECHA").Value
            s = hora.ToString("0000-00-00")
            fSql &= "HCARGA = '" & s
            hora = rs("HORA").Value
            s = hora.ToString("00:00:00")
            s = s.Replace(":", ".")
            fSql &= "-" & s & ".000000' "
            fSql &= "WHERE HCONSO = '" & rs("UCONSO").Value & "'"
            DB.ExecuteSQL(fSql)
            rs.MoveNext()
        Loop
        rs.Close()

        'Busca si han desconsolidado los pendientes
        fSql = "SELECT DISTINCT UCONSO, UPEDID FROM WMSFUPLP LEFT JOIN RDCC ON UCONSO = DCONSO AND UPEDID = DPEDID"
        fSql &= " WHERE USTS = 1 AND DCONSO IS NULL " 'En ejecucion
        rs = DB.ExecuteSQL(fSql)
        Do While Not rs.EOF
            fSql = "SELECT DCONSO, DPEDID FROM RDCC "
            fSql &= " WHERE DCONSO='" & rs("UCONSO").Value & "' AND DPEDID=" & rs("UPEDID").Value
            rs1 = DB.ExecuteSQL(fSql)
            If rs1.EOF Then
                fSql = "DELETE FROM WMSFUPLP "
                fSql &= " WHERE UCONSO='" & rs("UCONSO").Value & "' AND UPEDID=" & rs("UPEDID").Value
                DB.ExecuteSQL(fSql)
            End If
            rs1.Close()
            rs.MoveNext()
        Loop
        rs.Close()

        fSql = "SELECT DISTINCT UPEDID, UCRTDAT FROM WMSFUPLP WHERE USTS = 1 " 'En ejecucion
        fSql &= " ORDER BY UCRTDAT"
        rs = DB.ExecuteSQL(fSql)
        Do While Not rs.EOF
            'Comprueba que el Consolidado/Pedido ya tiene factura
            fSql = " SELECT UPEDID , UCONSO "
            fSql &= " FROM  WMSFUPLP INNER JOIN ( RDCC INNER JOIN LLL ON DPEDID =  LLORDN AND DLINEA = LLOLIN ) "
            fSql &= " ON DPEDID=UPEDID AND DCONSO=UCONSO "
            fSql &= " WHERE UPEDID = " & rs("UPEDID").Value
            rs1 = DB.ExecuteSQL(fSql)
            Do While Not rs1.EOF
                fSql = "DELETE FROM WMSFUPLP "
                fSql &= " WHERE UCONSO='" & rs1("UCONSO").Value & "' AND UPEDID=" & rs1("UPEDID").Value
                DB.ExecuteSQL(fSql)
                rs1.MoveNext()
            Loop
            rs1.Close()
            rs.MoveNext()
        Loop
        rs.Close()

        fSql = "SELECT * FROM WMSFUPLP WHERE USTS = 0 " 'En Cola
        fSql &= " ORDER BY UCRTDAT"
        rs = DB.ExecuteSQL(fSql)
        Do While Not rs.EOF
            'Marca Consolidado/Pedido para que lo ejecute en WMSRUPL
            fSql = " SELECT UNFILE, UPEDID , UCONSO FROM WMSFUPLP "
            fSql &= " WHERE UPEDID = " & rs("UPEDID").Value
            fSql &= " AND USTS = 1 " 'En ejecucuion
            rs1 = DB.ExecuteSQL(fSql)
            If rs1.EOF Then 'No lo encontro
                fSql = "UPDATE WMSFUPL SET USTS1=1 " 'Lo pone en ejecucion
                fSql &= " WHERE UCONSO='" & rs("UCONSO").Value & "' AND UPEDID=" & rs("UPEDID").Value & " AND USTS1 = 0 "
                DB.ExecuteSQL(fSql)
                fSql = "UPDATE WMSFUPLP SET USTS=1 " 'Lo pone en ejecucion
                fSql &= " WHERE UCONSO='" & rs("UCONSO").Value & "' AND UPEDID=" & rs("UPEDID").Value
                DB.ExecuteSQL(fSql)
                '===>>>quitar
                DB.Bitacora("WMSUPLOAD", "1. CALL PGM(WMSCUPL) PARM( '" & Fijo(rs("UNFILE").Value, 15) & "' 'P' )<br>" & DB.gsKeyWords, rs("UNFILE").Value)
                DB.ExecuteSQL(" {{ CALL PGM(WMSCUPL) PARM( '" & Fijo(rs("UNFILE").Value, 15) & "' 'P' ) }}")

            End If
            rs1.Close()
            rs.MoveNext()
        Loop
        rs.Close()

        rs = Nothing
        rs1 = Nothing
        rs2 = Nothing

        DB.ExecuteSQL("UPDATE WMSFDNL SET DCTL=1, DSTS='1' WHERE DPRE='UPL' AND DID=" & IdUpload)
        '===>>>quitar
        DB.Bitacora("WMSUPLOAD", "2. CALL PGM(WMSCUPL) PARM( '" & Fijo(fRemoto, 15) & "' '' )<br>" & DB.gsKeyWords, "")
        DB.ExecuteSQL(" {{ CALL PGM(WMSCUPL) PARM( '" & Fijo(fRemoto, 15) & "' '' ) }}")

        fSql = " UPDATE WMSFDNL SET"
        fSql &= " DSTS='2', DCTL=2, DFILE = '" & fRemoto & "' "
        fSql &= " WHERE "
        fSql &= " DPRE='UPL' "
        fSql &= " AND DID=" & IdUpload
        DB.ExecuteSQL(fSql)

    End Sub


    Public Sub ActualizaNumPedido(fRemoto As String)
        Dim rs As ADODB.Recordset
        Dim numLinea As Integer
        Dim numPedido As String
        Dim fSql As String

        DB.gsKeyWords = fRemoto
        rs = DB.ExecuteSQL("SELECT * FROM WMSFUPL WHERE UNFILE='" & fRemoto & "' AND UREC NOT LIKE '%TR%'")
        Do While Not rs.EOF
            fSql = "UPDATE WMSFUPL SET "
            fSql &= " UCONSO = '" & Left(Trim(rs("USHIPID").Value), 6) & "', "

            '2011-09-20 07:42:54 La nueva version de viaware no trae el campo en blanco.
            If rs("UCLAS").Value = "INVEQTY" And rs("UOBJ").Value = "MANUAL" And rs("UACC").Value = "ADJUST" Then
                fSql &= " UORDTY= '', "
            End If

            '2011-10-12 20:16:15 Se encontro otra transaccion con el mismo problema
            If rs("UCLAS").Value = "INVE" And rs("UOBJ").Value = "MANUAL" And rs("UACC").Value = "CHANGE" Then
                fSql &= " UORDTY= '', "
            End If


            numPedido = ""
            If rs("ULINE").Value > 99999 Then
                numLinea = getNumLinea(rs, numPedido)
            Else
                numPedido = getNumPedido(rs)
                numLinea = rs("ULINE").Value
            End If


            'UREC     CLAS   UOBJ    UACC    UORDID  UORDTY  USHIPID
            '0011964327 OBO OBORD   DELETE  333037002   PNO 333037
            '0011964332 OBO OBORD   DELETE  333037003   PNO 333037


            If UCase(Trim(rs("UORDTY").Value)) = "-B" Or UCase(Trim(rs("UORDTY").Value)) = "EM" Then
                fSql &= " UPEDID=" & Val(Trim(rs("UORDID").Value)) & ", "
                fSql &= " ULINEA=" & numLinea
            ElseIf UCase(Trim(rs("UORDTY").Value)) = "T" And Val(Mid(rs("UORDID").Value, 9, 6)) = 0 Then               '         1
                fSql &= " UPEDID=" & Val(Trim(rs("UORDID").Value)) & ", "                                   '1234567890123456789
                fSql &= " ULINEA=" & numLinea                                                         '817722               Pedido 6
            ElseIf UCase(Trim(rs("UORDTY").Value)) = "T" And Val(Mid(rs("UORDID").Value, 9, 6)) <> 0 Then              '00817722             Pedido 8
                fSql &= " UPEDID=" & Val(Mid(Trim(rs("UORDID").Value), 7)) & ", "                           '311618818524         Conso + Pedido 6
                fSql &= " ULINEA=" & Val(rs("UUSRC3").Value)                                                '31161800818524       Conso + Pedido 8
            Else
                If numPedido <> "" Then
                    fSql &= " UPEDID=" & numPedido & ", "
                Else
                    Try
                        numPedido = Val(Mid(rs("UORDID").Value, 7))
                    Catch ex As Exception
                        numPedido = "0"
                    End Try
                    fSql &= " UPEDID=" & numPedido & ", "
                End If
                fSql &= " ULINEA=" & numLinea
            End If
            fSql &= " WHERE UNFILE='" & fRemoto & "' AND UREC='" & rs("UREC").Value & "' "
            DB.ExecuteSQL(fSql)

            If rs("UCLAS").Value = "OBO" And rs("UOBJ").Value = "OBORD" And rs("UACC").Value = "DELETE" And numPedido <> "" Then
                dictConsos.Add(Left(Trim(rs("USHIPID").Value), 6) & "|" & numPedido, Left(Trim(rs("USHIPID").Value), 6) & "|" & numPedido)
            End If

            rs.MoveNext()
        Loop
        rs.Close()
        rs = Nothing

    End Sub

    Function getNumLinea(rsUpl As ADODB.Recordset, ByRef numPedido As String) As Integer
        Dim rs As ADODB.Recordset
        Dim fSql As String

        fSql = "SELECT * FROM RDCCEW INNER JOIN IIM ON VPROD=IPROD "
        fSql &= " WHERE  VWMSDOCU='" & rsUpl("UORDID").Value & "'"
        fSql &= " AND VWMSLINEA='" & rsUpl("ULINE").Value & "'"
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            numPedido = rs("VPEDID").Value
            getNumLinea = rs("VLINEA").Value
            If rsUpl("ULINE").Value > 200000 Then
                If rsUpl("UCLAS").Value = "OBO" And rsUpl("UOBJ").Value = "OBORDLINE" And rsUpl("UACC").Value = "SHIP" Then
                    fSql = "UPDATE RDCCAD SET ESTS02=1, EQDES = " & rsUpl("UQTY").Value & ", EPDES = " & rsUpl("UQTY").Value * rs("IWGHT").Value / 1000
                    fSql &= " WHERE ECPO = '" & rs("VCPO").Value & "'"
                    fSql &= " AND ETIENDA = " & rs("VTIENDA").Value         'Tienda
                    fSql &= " AND ETLINEA = " & rs("VTLINEA").Value         'Linea
                    DB.ExecuteSQL(fSql)
                End If
            End If
        Else
            getNumLinea = 0
        End If
        rs.Close()
        rs = Nothing

    End Function

    Function getNumPedido(rsUpl As ADODB.Recordset) As String
        Dim rs As ADODB.Recordset
        Dim fSql As String

        getNumPedido = ""
        fSql = "SELECT * FROM RDCCEW INNER JOIN IIM ON VPROD=IPROD "
        fSql &= " WHERE  VWMSDOCU='" & rsUpl("UORDID").Value & "'"
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            getNumPedido = rs("VPEDID").Value
        Else
            getNumPedido = "0"
        End If
        rs.Close()
        rs = Nothing

    End Function


    Sub ActualizaCabPed(Ped As String, Bodega As String)
        Dim fSql As String

        fSql = "UPDATE ECH SET HWHSE = '" & Bodega & "'"
        fSql &= " WHERE HORD = " & Ped
        DB.ExecuteSQL(fSql)

    End Sub

    Private Sub Salir(Cancel As Integer)
        Dim sql As String

        sql = "UPDATE ZCC SET CCMNDT=" & Now().ToString("yyyyMMdd")
        sql = sql & ", CCMNTM=" & Now().ToString("hhmmss")
        sql = sql & ", CCNOT1='" & Now().ToString("hhmmss") & "'"
        sql = sql & ", CCNOT2=''"
        sql = sql & ", CCALTC='00'"
        sql = sql & " WHERE CCTABL='RFVBVER' AND CCCODE = '" & DB.gsAppName & "'"
        DB.ExecuteSQL(sql)

    End Sub


    Function wrtDownload(remoteFilename As String) As Boolean
        Dim rs As ADODB.Recordset

        rs = DB.ExecuteSQL("SELECT TEXTO || '#' AS DATO FROM " & remoteFilename)
        Do While Not rs.EOF
            Using outputFile As New StreamWriter(sFolder_fromHost & Convert.ToString("\" & remoteFilename & ".DAT"), True)
                outputFile.WriteLine(rs("DATO").Value)
            End Using
            rs.MoveNext()
        Loop
        rs.Close()

        wrtDownload = True

        fSql = " INSERT INTO WMSFSEND( PRE, ID, TEXTO, FIN, ARCHIVO )"
        fSql &= " SELECT '" & Left(remoteFilename, 3) & "', " & Val(Right(remoteFilename, 7)) & ", "
        fSql &= " A.*, '" & sFolder_fromHost & Convert.ToString("\" & remoteFilename & ".DAT") & "' "
        fSql &= "FROM " & remoteFilename & " A"
        DB.ExecuteSQL(fSql)

        DB.DropTable(remoteFilename)


    End Function


    Sub ShowLog(txt As String, bErr As Boolean)

        wc("toBPCS=" & sFolder_toHost & vbCr & "fromBPCS=" & sFolder_fromHost, 1)
    End Sub



    Public Sub New()

        sFolder_toHost = DB.getLXParam("WMS_TO_AS400")
        sFolder_fromHost = DB.getLXParam("WMS_FROM_AS400")
        sFolder_toHost_BAK = sFolder_toHost & ".save"
        sFolder_LOG = DB.getLXParam("WMS_LOG")
        sFolder_rpt = DB.getLXParam("WMS_RPT")

    End Sub
End Class
