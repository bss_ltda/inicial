﻿Public Class clsJVAPGM
    Public IdJob As String
    Public Job400 As String
    Public JobKeyWords As String
    Public Modulo As String
    Public Clase As String
    Public Jar As String
    Public Jobq As String
    Public Archivo As String
    Public Fuente As String
    Public BatchNo As String
    Public ParamJVA As String
    Public ParamPGM As String

    Dim fSql, vSql As String

    Sub Parametro(prm As String, vlr As String)
        Dim p As String

        p = "'-{P}' '{V}'"
        p = Replace(p, "{P}", prm)
        p = Replace(p, "{V}", vlr)
        If ParamJVA = "" Then
            ParamJVA = p
        Else
            ParamJVA = ParamJVA & " " & p
        End If

        p = "{P}={V}"
        p = Replace(p, "{P}", prm)
        p = Replace(p, "{V}", vlr)
        If ParamPGM = "" Then
            ParamPGM = p
        Else
            ParamPGM = ParamPGM & ";" & p
        End If

    End Sub

    Function ExecutePGM() As String
        Dim sCmd As String
        Dim sSbm As String

        Me.Job400 = Left(Me.Job400, 10)

        If JobKeyWords = "" Then
            JobKeyWords = DB.gsKeyWords
        End If

        sCmd = "RUNJVA CLASS(" & Me.Clase & ") PARM( " & Me.ParamJVA & ") " & jvaClassPath(Me.Jar)

        If Jobq = "" Then
            Jobq = "QBATCH"
        End If
        sSbm = "SBMJOB CMD( " & sCmd & " )"
        sSbm = sSbm & " JOB({JOB}) JOBD({JOBD}) JOBQ({JOBQ}) "
        sSbm = Replace(sSbm, "{JOB}", Job400)
        sSbm = Replace(sSbm, "{JOBD}", DB.getLXParam("LXJOBD", ""))
        sSbm = Replace(sSbm, "{JOBQ}", Jobq)

        fSql = "DELETE FROM RFPIBATCH WHERE BJOB = '" & Job400 & "'"
        DB.ExecuteSQL(fSql)

        fSql = " INSERT INTO RFPIBATCH( "
        vSql = " VALUES ( "
        mkSql("'", " BJOB      ", Job400, 0)                            '//10A   Trabajo
        mkSql("'", " BJOB400   ", Job400, 0)                            '//10A   Trabajo
        mkSql("'", " BMODULO   ", Modulo, 0)                            '//10  Categoria
        mkSql("'", " BPGM      ", Jar, 0)                               '//50  Paquete jar
        mkSql("'", " BDESC     ", Clase, 0)                             '//10  Clase
        mkSql("'", " BBATCH   ", JobKeyWords, 0)                        '//100  Batch
        mkSql("'", " BCRTUSR   ", DB.appUser, 0)                        '//10A   Usuario
        mkSql("'", " BPARAM    ", Replace(ParamPGM, "'", "''"), 1)      '//5002A Parametros
        DB.ExecuteSQL(fSql & vSql)

        IdJob = DB.ultimoRegNum()
        'Me.DesdeVerde

        DB.ExecuteSQL("{{" & sSbm & "}}")
        Return IdJob.ToString()


    End Function

    Sub DesdeVerde()

        Debug.Print("Desde linea de comando:")
        Debug.Print("ADDENVVAR CLASSPATH")
        Debug.Print("Luego F4 y pegar:")
        Debug.Print(jvaClassPath(Me.Jar))
        Debug.Print("Ahora Ejecutar:")
        Debug.Print("RUNJVA CLASS(" & Me.Clase & ") PARM( " & Me.ParamJVA & ") ")
        Debug.Print("Para Eclipse")
        Debug.Print(Replace(Me.ParamJVA, "'", ""))

    End Sub

    Sub Initialize()

        IdJob = ""
        Modulo = ""
        Clase = ""
        Jar = ""
        Jobq = ""
        Archivo = ""
        Fuente = ""
        BatchNo = ""
        ParamPGM = ""
        ParamJVA = ""
        JobKeyWords = ""

    End Sub

    Function jvaClassPath(Jar)
        Dim s As String = ""
        Dim rs As ADODB.Recordset
        Dim fSql As String

        s = " CLASSPATH('/{LIB}/:/{LIB}/" & Jar & ".jar:"
        fSql = " SELECT CCNOT1 FROM RFPARAM WHERE CCTABL = 'LXLONG' AND CCCODE = 'CLASSPATH' ORDER BY CCCODEN"
        rs = DB.ExecuteSQL(fSql)
        Do While Not rs.EOF
            s = s & rs("CCNOT1").Value
            rs.MoveNext()
        Loop
        s = s & "')"
        rs.Close()
        rs = Nothing
        jvaClassPath = Replace(s, "{LIB}", DB.gsLib)

    End Function


    Sub mkSql(tipoF As String, Campo As String, Valor As String, Optional fin As Single = 0)
        mkSqlG(fSql, vSql, tipoF, Campo, Valor, fin)
    End Sub
    Sub mkSql(tipoF As String, Campo As String, Valor As Double, Optional fin As Single = 0)
        mkSqlG(fSql, vSql, tipoF, Campo, Valor, fin)
    End Sub

    Public Sub New()
        Initialize()
    End Sub
End Class
