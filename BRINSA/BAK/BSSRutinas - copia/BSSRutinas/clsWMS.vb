﻿Imports System.IO

Public Class clsWMS

    Public ModuloActual As String
    Public Producto As String
    Public Lote As String
    Public sFolder_fromHost As String

    Const WMS_CAMPOS As Integer = 39

    Dim Sku As String
    Dim Bodega As String
    Dim DBWMS As New clsConexionSQLServer
    Dim aWMSCampos() As String

    Function Conecta() As Boolean
        DBWMS.gsDatasource = DB.getLXParam("WMSSERV")
        DBWMS.gsLib = DB.getLXParam("WMSCATALOG")
        DBWMS.gsUser = DB.getLXParam("WMSUSER")
        DBWMS.gsPassword = DB.getLXParam("WMSPASS")
        DBWMS.ModuloActual = ModuloActual
        Return DBWMS.Conexion()
    End Function

    Sub WMS_EnviaArticulo()
        Dim rs As ADODB.Recordset
        Dim rsIIM As ADODB.Recordset
        Dim fSql As String
        Dim remoteFilename As String


        Sku = Left(Producto, 20)

        fSql = "SELECT p.IWHS, p.IDESC, div.CCDESC AS DIVISION, ex.RTPAQSE "
        fSql &= " FROM "
        fSql &= "  IIM p "
        fSql &= "  INNER JOIN RIIM ex ON p.IPROD = ex.RIPROD"
        fSql &= "  INNER JOIN ZCCL01 div ON p.IREF01 = div.CCCODE AND div.CCTABL = 'SIRF1' "
        fSql &= " WHERE IPROD = '" + Producto + "'"
        rsIIM = DB.ExecuteSQL(fSql)
        If rsIIM.EOF Then
            rsIIM.Close()
            Return
        End If

        If rsIIM("IWHS").Value = "PT" Then
            Bodega = " "
        Else
            Bodega = "TC"
        End If

        fSql = "UPDATE RIIMWMS SET "
        fSql &= " SKU_DESC = '" & Left(Trim(rsIIM("IDESC").Value), 40) & "'"
        Select Case rsIIM("DIVISION").Value
            Case "SAL"
                fSql &= ", PROD_CLASS = 'SA' "
            Case "ASEO"
                fSql &= ", PROD_CLASS = 'AS' "
            Case "QUIMICOS"
                fSql &= ", PROD_CLASS = 'QM' "
            Case "OTRAS DIVISIONES"
                fSql &= ", PROD_CLASS = 'OD' "
        End Select
        fSql &= ", CRTN_UOM = '" & rsIIM("RTPAQSE").Value & "'"
        fSql &= ", PCONS = '000001'"
        fSql &= ", PTIPO = 'PM'"
        fSql &= ", PACTI = 'A'"
        fSql &= ", TAG_TRACK = 'Y'"
        fSql &= ", FR_CLASS = '000.00'"
        fSql &= ", LOAD_UOM = UOM2"
        fSql &= ", BRK_UOM = UOM1"
        fSql &= ", BACK_UOM = UOM1"
        fSql &= ", CYCC_TRIG_QTY = '0000000000.00'"
        fSql &= ", ALLOW_REC_RET = 'Y'"
        fSql &= ", OVER_PICK = 'N'"
        fSql &= ", UPLOAD = 'Y'"
        fSql &= ", ENTRY_LOT = 'R'"
        fSql &= ", ENTRY_UC1 = 'O'"
        fSql &= ", ENTRY_UC2 = 'O'"
        fSql &= ", ENTRY_UC3 = 'O'"
        fSql &= ", ENTRY_TAG = 'R'"
        fSql &= ", CONS_LOT = 'Y'"
        fSql &= ", CONS_UC1 = 'N'"
        fSql &= ", CONS_UC2 = 'N'"
        fSql &= ", CONS_UC3 = 'N'"
        fSql &= ", CONS_HOLD = 'Y'"
        fSql &= ", DEF_REL_PUT = 'Y'"
        fSql &= ", TOL_ITEM = 'N'"
        fSql &= ", TOL_RULE = 'O'"
        fSql &= ", TOL_PERCENT = '0'"
        fSql &= ", FIFO_WIN = '000'"
        fSql &= ", CWGT_REQ = 'N'"
        fSql &= ", CWGT_TOL_ITEM = 'N'"
        fSql &= ", CWGT_TOL_PERCENT = '0'"
        fSql &= ", SER_REQ = 'N'"
        fSql &= ", SCHED_DELETE = 'N'"
        fSql &= ", ALTSRC_PART = 'N'"
        fSql &= ", CONS_DATE_TYPE = 'R'"
        fSql &= ", ENTRY_MFG = 'O'"
        fSql &= ", ENTRY_DELIVER_BY = 'N'"
        fSql &= ", ENTRY_BEST_BY = 'N'"
        fSql &= ", ENTRY_EXPIRE = 'O'"
        fSql &= ", ENTRY_RECV = 'N'"
        fSql &= ", IMEI_REQ = 'N'"
        fSql &= ", EID_REQ = 'N'"
        fSql &= ", NEI_REQ = 'N'"
        fSql &= ", SAMP_UOM = 'ESTIBA'"
        fSql &= ", SAFETY_STOCK = '000000.00'"
        fSql &= ", OWNER = 'BRINSA'"
        fSql &= ", WHSE_GROUP_ID = 'GENERAL'"
        fSql &= ", PAL_REQ = 'N' "
        fSql &= " WHERE SKU='" & Sku & "' AND PKG='" & Bodega & "'"
        DB.ExecuteSQL(fSql)
        rsIIM.Close()


        fSql = " SELECT "
        fSql &= "    PCONS     "
        fSql &= " || PTIPO     "
        fSql &= " || PACTI     "
        fSql &= " || SKU       "
        fSql &= " || PKG       "
        fSql &= " || SKU_DESC  "
        fSql &= " || PKG_DESC  "
        fSql &= " || DEDICATE_LOC "
        fSql &= " || LOC_SIZE  "
        fSql &= " || TAG_TRACK "
        fSql &= " || STORE_RULE "
        fSql &= " || PROD_CLASS "
        fSql &= " || LEFT( DIGITS( FR_CLASS  ), 4  ) || '.' || RIGHT( DIGITS( FR_CLASS  ), 1  )"
        fSql &= " || LOAD_UOM  "
        fSql &= " || LAB_UOM   "
        fSql &= " || CMD_UOM   "
        fSql &= " || CRTN_UOM  "
        fSql &= " || BRK_UOM   "
        fSql &= " || BACK_UOM  "
        fSql &= " || LEFT( DIGITS( LOAD_MAX_QTY ), 10  ) || '.' || RIGHT( DIGITS( LOAD_MAX_QTY ), 2  )"
        fSql &= " || LEFT( DIGITS( LOAD_MIN_QTY ), 10  ) || '.' || RIGHT( DIGITS( LOAD_MIN_QTY ), 2  )"
        fSql &= " || DIGITS( CYCC_FREQ )"
        fSql &= " || LEFT( DIGITS( CYCC_TRIG_QTY ), 10  ) || '.' || RIGHT( DIGITS( CYCC_TRIG_QTY ), 2  )"
        fSql &= " || CYCC_TRIG_UOM "
        fSql &= " || CHES_NUM  "
        fSql &= " || HAZ_ID_CODE "
        fSql &= " || HAZ_CLASS "
        fSql &= " || HAZ_PLC_REQ "
        fSql &= " || HAZ_PACK_GRP "
        fSql &= " || HAZ_STK_DESC "
        fSql &= " || HAZ_LAB_DESC "
        fSql &= " || ALLOW_REC_RET "
        fSql &= " || OVER_PICK "
        fSql &= " || UPLOAD    "
        fSql &= " || TAG_FORMAT "
        fSql &= " || DEF_HOLD  "
        fSql &= " || LEFT( DIGITS( CONS_AGE  ), 4  ) || '.' || RIGHT( DIGITS( CONS_AGE  ), 5  )"
        fSql &= " || DIGITS( SHELF_LIFE )"
        fSql &= " || DIGITS( MAX_STACK )"
        fSql &= " || UOM1      "
        fSql &= " || UOM2      "
        fSql &= " || UOM3      "
        fSql &= " || UOM4      "
        fSql &= " || LEFT( DIGITS( QTY1IN2   ), 10  ) || '.' || RIGHT( DIGITS( QTY1IN2   ), 2  )"
        fSql &= " || LEFT( DIGITS( QTY2IN3   ), 10  ) || '.' || RIGHT( DIGITS( QTY2IN3   ), 2  )"
        fSql &= " || LEFT( DIGITS( QTY3IN4   ), 10  ) || '.' || RIGHT( DIGITS( QTY3IN4   ), 2  )"
        fSql &= " || LEFT( DIGITS( WGT1      ), 9  ) || '.' || RIGHT( DIGITS( WGT1      ), 3  )"
        fSql &= " || LEFT( DIGITS( WGT2      ), 9  ) || '.' || RIGHT( DIGITS( WGT2      ), 3  )"
        fSql &= " || LEFT( DIGITS( WGT3      ), 9  ) || '.' || RIGHT( DIGITS( WGT3      ), 3  )"
        fSql &= " || LEFT( DIGITS( WGT4      ), 9  ) || '.' || RIGHT( DIGITS( WGT4      ), 3  )"
        fSql &= " || LEFT( DIGITS( HGT1      ), 11  ) || '.' || RIGHT( DIGITS( HGT1      ), 1  )"
        fSql &= " || LEFT( DIGITS( HGT2      ), 11  ) || '.' || RIGHT( DIGITS( HGT2      ), 1  )"
        fSql &= " || LEFT( DIGITS( HGT3      ), 11  ) || '.' || RIGHT( DIGITS( HGT3      ), 1  )"
        fSql &= " || LEFT( DIGITS( HGT4      ), 11  ) || '.' || RIGHT( DIGITS( HGT4      ), 1  )"
        fSql &= " || LEFT( DIGITS( WID1      ), 11  ) || '.' || RIGHT( DIGITS( WID1      ), 1  )"
        fSql &= " || LEFT( DIGITS( WID2      ), 11  ) || '.' || RIGHT( DIGITS( WID2      ), 1  )"
        fSql &= " || LEFT( DIGITS( WID3      ), 11  ) || '.' || RIGHT( DIGITS( WID3      ), 1  )"
        fSql &= " || LEFT( DIGITS( WID4      ), 11  ) || '.' || RIGHT( DIGITS( WID4      ), 1  )"
        fSql &= " || LEFT( DIGITS( DPTH1     ), 11  ) || '.' || RIGHT( DIGITS( DPTH1     ), 1  )"
        fSql &= " || LEFT( DIGITS( DPTH2     ), 11  ) || '.' || RIGHT( DIGITS( DPTH2     ), 1  )"
        fSql &= " || LEFT( DIGITS( DPTH3     ), 11  ) || '.' || RIGHT( DIGITS( DPTH3     ), 1  )"
        fSql &= " || LEFT( DIGITS( DPTH4     ), 11  ) || '.' || RIGHT( DIGITS( DPTH4     ), 1  )"
        fSql &= " || ENTRY_LOT "
        fSql &= " || ENTRY_UC1 "
        fSql &= " || ENTRY_UC2 "
        fSql &= " || ENTRY_UC3 "
        fSql &= " || ENTRY_TAG "
        fSql &= " || CONS_LOT  "
        fSql &= " || CONS_UC1  "
        fSql &= " || CONS_UC2  "
        fSql &= " || CONS_UC3  "
        fSql &= " || CONS_HOLD "
        fSql &= " || DEF_REL_PUT "
        fSql &= " || INV_CLASS "
        fSql &= " || TOL_ITEM  "
        fSql &= " || TOL_RULE  "
        fSql &= " || DIGITS( TOL_PERCENT )"
        fSql &= " || DEF_UC1   "
        fSql &= " || DEF_UC2   "
        fSql &= " || DEF_UC3   "
        fSql &= " || DEF_LOT   "
        fSql &= " || DIGITS( FIFO_WIN  )"
        fSql &= " || CWGT_REQ  "
        fSql &= " || CWGT_UOM  "
        fSql &= " || CWGT_TOL_ITEM "
        fSql &= " || DIGITS( CWGT_TOL_PERCENT )"
        fSql &= " || NO_CONT_UOM "
        fSql &= " || REQ_CNTYPE "
        fSql &= " || SER_REQ   "
        fSql &= " || SER_UOM   "
        fSql &= " || DIGITS( MAX_PICK_QTY )"
        fSql &= " || MAX_PICK_UOM "
        fSql &= " || ALLOW_VOL_UPD "
        fSql &= " || SCHED_DELETE "
        fSql &= " || DATE_DELETE "
        fSql &= " || CRTN_CNTYPE "
        fSql &= " || LOAD_CNTYPE "
        fSql &= " || ALTSRC_PART "
        fSql &= " || TARIFF_CODE "
        fSql &= " || LEFT( DIGITS( CURE_TIME ), 7  ) || '.' || RIGHT( DIGITS( CURE_TIME ), 5  )"
        fSql &= " || SKU_TYPE  "
        fSql &= " || LEFT( DIGITS( UNIT_COST ), 10  ) || '.' || RIGHT( DIGITS( UNIT_COST ), 2  )"
        fSql &= " || DIGITS( MFG_TO_DELIVER_BY )"
        fSql &= " || DIGITS( MFG_TO_BEST_BY )"
        fSql &= " || CONS_DATE_TYPE "
        fSql &= " || ENTRY_MFG "
        fSql &= " || ENTRY_DELIVER_BY "
        fSql &= " || ENTRY_BEST_BY "
        fSql &= " || ENTRY_EXPIRE "
        fSql &= " || ENTRY_RECV "
        fSql &= " || SVCC_CTRL "
        fSql &= " || MASK_TYPE "
        fSql &= " || IMEI_REQ  "
        fSql &= " || EID_REQ   "
        fSql &= " || NEI_REQ   "
        fSql &= " || IMEI_MASK_TYPE "
        fSql &= " || SAMP_UOM  "
        fSql &= " || LEFT( DIGITS( SAFETY_STOCK ), 6  ) || '.' || RIGHT( DIGITS( SAFETY_STOCK ), 2  )"
        fSql &= " || NMFC_CODE "
        fSql &= " || OWNER     "
        fSql &= " || WHSE_GROUP_ID "
        fSql &= " || EPC_ITEM_REF "
        fSql &= " || EPC_CASE_IMAGE "
        fSql &= " || TRADE_PRTNR_RULE "
        fSql &= " || PAL_REQ   "
        fSql &= " || STORE_RULE_UOM3 "
        fSql &= " || STORE_RULE_UOM2 "
        fSql &= " || STORE_RULE_UOM1 "
        fSql &= " || VALIDATE_UC1 "
        fSql &= " || VALIDATE_UC2 "
        fSql &= " || VALIDATE_UC3 "
        fSql &= " || RFID_ENABLED "
        fSql &= " || UCC_VALUE "
        fSql &= " || '#' AS DATO "
        fSql &= " FROM RIIMWMS "
        fSql &= " WHERE SKU='" & Sku & "' AND PKG='" & Bodega & "'"
        fSql &= " UNION "
        fSql &= " SELECT '000001TR" & Now().ToString("hhmmssyyyyMMdd") & "' AS DATO"
        fSql &= " FROM SYSIBM.SYSDUMMY1"
        rs = DB.ExecuteSQL(fSql)

        remoteFilename = "ART" & DB.CalcConsec("RIIMWMS", "9999999")

        Tarea.setResultado(sFolder_fromHost & Convert.ToString("\" & remoteFilename & ".DAT"))

        Do While Not rs.EOF
            Using outputFile As New StreamWriter(sFolder_fromHost & Convert.ToString("\" & remoteFilename & ".DAT"), True)
                outputFile.WriteLine(rs("DATO").Value)
            End Using
            rs.MoveNext()
        Loop
        rs.Close()
        rs = Nothing
        DB.ExecuteSQL("INSERT INTO WMSFDNL (DPRE, DSTS,  DID) VALUES( 'ART', '2', " & remoteFilename.Replace("ART", "") & " )")

    End Sub


    Function venceLote() As Integer
        Dim fSql As String
        Dim rs As New ADODB.Recordset
        Dim resultado As Integer = 0

        fSql = " Select year ( iv_f.dt_expire )  * 10000 +  month( iv_f.dt_expire ) * 100 + day( iv_f.dt_expire ) as vence "
        fSql &= " From "
        fSql &= "  iv_f "
        fSql &= " Where "
        fSql &= "  iv_f.sku = '" & Sku & "' And "
        fSql &= "  iv_f.lot = '" & Lote & "' "
        If DBWMS.OpenRS(rs, fSql) Then
            fSql = "UPDATE ILN SET LEXDT = " & rs("vence").Value
            fSql &= " WHERE LPROD = '" & Sku & "' AND LLOT = '" & Lote & "' "
            DB.ExecuteSQL(fSql)
            resultado = DB.regActualizados
        End If
        rs.Close()
        Return resultado
    End Function

    Sub Desconecta()
        DBWMS.Close()
    End Sub

    Public Sub New()
        sFolder_fromHost = DB.getLXParam("WMS_FROM_AS400")
    End Sub
End Class
