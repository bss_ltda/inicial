﻿Public Class clsCrossdocking
    Public Conso As String
    Public WrkID As String


    Dim fSql, vSql As String

    Sub PreparaCrossdocking()
        Dim fWrkID As String
        Dim rs As ADODB.Recordset

        If DB.SiExiste("SELECT SCONSO FROM RHCD WHERE SCONSO = '" & Conso & "'") Then
            Return
        End If

        fWrkID = DB.CalcConsec("RHCD", "99999999")

        fSql = " INSERT INTO RHCD( "
        vSql = " SELECT "
        fSql &= " FWRKID    , " : vSql &= fWrkID & ",  "                      '//8S0   FWRKID
        fSql &= " SPER      , " : vSql &= " h.HPER, "                         '//Periodo
        fSql &= " SCONSO    , " : vSql &= " h.HCONSO, "                       '//6A    Consolidado
        fSql &= " SPROVE    , " : vSql &= " Max(p1.CCCODEN2), "               '//8S0   Transportadora
        fSql &= " SPROVECXD , " : vSql &= " Max(p1.CCCODEN), "                '//4S0   Plataforma Crossdocking
        fSql &= " SDIVISION , " : vSql &= " Max(TRIM(ZCC.CCNOT2)), "          '//30A   Division Generica
        fSql &= " SIDIVI    , " : vSql &= " d.DIDIVI, "                       '//30A   Division Producto
        fSql &= " SBOD      , " : vSql &= " p2.TWHSE, "                       '//6A    Consolidado
        fSql &= " SPEDID    , " : vSql &= " d.DPEDID, "                       '//8S0   Pedido
        fSql &= " SFCITA    , " : vSql &= " Max(d.DFEC04), "                  '//Fecha de Cita
        fSql &= " SCUST     , " : vSql &= " d.DCUST, "                        '//8S0   Cliente
        fSql &= " SHNAME    , " : vSql &= " d.DHNAME, "                       '//50A   Nombre Cliente
        fSql &= " SSHIP     , " : vSql &= " d.DSHIP, "                        '//4S0   ShipTo
        fSql &= " SNSHIP    , " : vSql &= " d.DNSHIP, "                       '//50A   Nombre ShipTo
        fSql &= " SCIUD     , " : vSql &= " d.DSPOST, "                       '//10A   Ciudad Punto Envio
        fSql &= " SPOBLADO  , " : vSql &= " c.CPOBLADO, "                     '//10A   Ciudad DANE
        fSql &= " SDPTO     , " : vSql &= " d.DSHDEP, "                       '//3A    Depto Punto Envio
        fSql &= " SAD1      , " : vSql &= " Max(d.DHAD1), "                   '//50A   
        fSql &= " SAD2      , " : vSql &= " Max(d.DHAD2), "                   '//50A   
        fSql &= " SCANT     , " : vSql &= " Sum(p.LLOQTY) AS CANT, "          '//11P3  Cajas
        fSql &= " SPESO     ) " : vSql &= " Sum(p.LLLWGT / 1000) AS PESO "    '//11P3  Peso Bruto

        vSql &= " FROM "
        vSql &= "  RHCC h "
        vSql &= "  INNER JOIN RDCC d ON h.HCONSO = d.DCONSO "
        vSql &= "  INNER JOIN RDCCH cp ON d.DPEDID = cp.CPEDID "
        vSql &= "  INNER JOIN LLL p ON d.DPEDID = p.LLORDN AND d.DLINEA = p.LLOLIN "
        vSql &= "  INNER JOIN (IIM "
        vSql &= "  INNER JOIN ZCC ON ZCC.CCCODE = IIM.IREF01 AND ZCC.CCTABL = 'SIRF1') ON d.DPROD = IIM.IPROD "
        vSql &= "  INNER JOIN RFDANE c ON d.DSPOST = c.CLXCIUD AND d.DSHDEP = c.CLXDPTO AND c.CTIPO = 'CM' AND c.CLXPAIS = 'COL' "
        vSql &= "  INNER JOIN (RFPARAM p1 "
        vSql &= "  INNER JOIN ESTL01 p2 ON p1.CCCODEN = p2.TSHIP AND p2.TCUST = 1) ON h.HPROVE3 = p1.CCNUM "

        vSql &= " WHERE "
        vSql &= "  h.HCONSO = '" & Conso & "'  "
        vSql &= "  AND h.HPRCDVA = 'CROSSDOCKING' "
        vSql &= "  AND cp.CUDS07 = 0"

        vSql &= " GROUP BY "
        vSql &= "  h.HPER, "
        vSql &= "  h.HCONSO, "
        vSql &= "  d.DIDIVI, "
        vSql &= "  p2.TWHSE, "
        vSql &= "  d.DPEDID, "
        vSql &= "  d.DCUST, "
        vSql &= "  d.DHNAME, "
        vSql &= "  d.DSHIP, "
        vSql &= "  d.DNSHIP, "
        vSql &= "  d.DSPOST, "
        vSql &= "  c.CPOBLADO, "
        vSql &= "  d.DSHDEP "

        DB.ExecuteSQL(fSql & vSql)

        fSql = " SELECT DISTINCT "
        fSql &= "  c.SCONSO, "
        fSql &= "  c.SPEDID, "
        fSql &= "  p.LLLOAD "
        fSql &= " FROM "
        fSql &= "  RHCD c "
        fSql &= "  INNER JOIN RDCC d ON c.SCONSO = d.DCONSO AND c.SPEDID = d.DPEDID "
        fSql &= "  INNER JOIN LLL p ON d.DPEDID = p.LLORDN AND d.DLINEA = p.LLOLIN "
        fSql &= " WHERE "
        fSql &= "  c.FWRKID = " & fWrkID
        rs = DB.ExecuteSQL(fSql)
        Do While Not rs.Eof
            fSql = " UPDATE RHCD SET  "
            fSql &= "  SPLANIS = IFNULL(SPLANIS, '') || CASE WHEN SPLANIS IS NULL THEN '' ELSE ', ' END || '" & rs("LLLOAD").Value & "'"
            fSql &= ", FWRKID = 0 "
            fSql &= " WHERE FWRKID = " & fWrkID
            fSql &= " AND SCONSO = '" & rs("SCONSO").Value & "'  "
            fSql &= " AND SPEDID = " & rs("SPEDID").Value
            DB.ExecuteSQL(fSql)
            rs.MoveNext()
        Loop
        rs.Close()

    End Sub

    Dim Transportadora As String
    Dim Password As String
    Dim Manifiesto As String
    Dim Bodega As String

    Dim TipoVehiculo As String
    Dim Notas As String
    Dim DescTipoVehiculo As String
    Dim Placa As String
    Dim CedulaConductor As String
    Dim NombreConductor As String
    Dim CelularConductor As String

    Structure DetalleDespacho
        Dim Planilla As String
    End Structure

    Dim Planillas As New List(Of DetalleDespacho)

    Sub Despacho()
        Dim fSql As String
        Dim rs As New ADODB.Recordset
        Dim xmlPedido As String = ""
        Dim Planilla As DetalleDespacho

        fSql = " Select "
        fSql &= "  NOW(), "
        fSql &= "  SBUSY, "
        fSql &= "  W.WRKID, "
        fSql &= "  V.VPLACA, "
        fSql &= "  V.VCAPTON, "
        fSql &= "  TV.VCODIGO, "
        fSql &= "  TV.VTIPVEHD, "
        fSql &= "  V.CCEDULA, "
        fSql &= "  V.CNOMBRE, "
        fSql &= "  V.CMOVIL, "
        fSql &= "  S.SPLANIS, "
        fSql &= "  S.SBOD, "
        fSql &= "  W.WPEDID AS TRANSP "
        fSql &= " From "
        fSql &= "  RHCD S Inner Join "
        fSql &= "  RFWRKGRL W On S.FWRKID = W.WRKID And S.SVEH = W.WLINEA Inner Join "
        fSql &= "  RTVEH V On W.WCATEG01 = V.VPLACA And W.WCATEG02 = V.VBOD And "
        fSql &= "    W.WPEDID = V.VPROV Inner Join "
        fSql &= "  RFFLTIPVEH TV On V.VTIPVEH = DIGITS(TV.VTIPVEH) "
        fSql &= " Where "
        fSql &= "  W.WRKID = " & WrkID & " AND S.SBUSY = 1"
        fSql &= " Order By "
        fSql &= "  V.VPLACA, "
        fSql &= "  S.SPLANIS "

        rs = DB.ExecuteSQL(fSql)

        If rs.EOF Then
            DB.WrtSqlError(fSql, "Crossdocking No encotro pedidos")
        End If
        Do While Not rs.EOF
            If Placa <> rs("VPLACA").Value Then
                If Placa <> "" Then
                    creaDespacho()
                End If
                Transportadora = rs("TRANSP").Value
                Placa = rs("VPLACA").Value
                Bodega = rs("SBOD").Value
                TipoVehiculo = rs("VCODIGO").Value
                DescTipoVehiculo = rs("VTIPVEHD").Value
                CedulaConductor = rs("CCEDULA").Value
                NombreConductor = Left(rs("CNOMBRE").Value, 30)
                CelularConductor = rs("CMOVIL").Value
                Manifiesto = ""
            End If
            Planilla = New DetalleDespacho
            Planilla.Planilla = rs("SPLANIS").Value
            Planillas.Add(Planilla)
            rs.MoveNext()
        Loop

        creaDespacho()

        fSql = " UPDATE RHCD SET SBUSY = 2 "
        fSql &= " WHERE FWRKID = " & WrkID
        DB.ExecuteSQL(fSql)

        rs.Close()
        rs = Nothing

    End Sub

    Sub creaDespacho()
        Dim fSql, vSql As String
        Dim Planilla As DetalleDespacho
        Dim rs As New ADODB.Recordset
        Dim C_ID As String = DB.CalcConsec("RFLOGWSCDA", "99999999")
        Dim Conso As String
        Dim Division As String
        Dim Segmento As String = ""
        Dim lstPlanis As String = ""
        Dim sep As String = ""

        For Each Planilla In Planillas
            fSql = "SELECT EPLANI FROM RECC WHERE EPLANI IN( " & Planilla.Planilla & ") AND ESFCNS = 'CXD'"
            rs = DB.ExecuteSQL(fSql)
            If rs.EOF Then
                lstPlanis &= sep & "'" & Planilla.Planilla & "'"
                sep = ", "
            Else
                'Pedido consolidado
            End If
            rs.Close()
        Next

        If lstPlanis = "" Then
            Console.WriteLine("Sin pedidos.")
            Exit Sub
        End If

        Conso = DB.CalcConsec("SPCONSO", "999999")
        Console.WriteLine(Conso)
        Division = " "

        fSql = "UPDATE RHCD SET SCONSO2 = '" & Conso & "' WHERE SPLANIS IN( " & lstPlanis & " ) AND SCONSO2 = ''"
        DB.ExecuteSQL(fSql)

        fSql = " SELECT DISTINCT"
        fSql &= " SIDIVI "
        fSql &= " FROM RHCD "
        fSql &= " WHERE SCONSO2 ='" & Conso & "'"
        rs = DB.ExecuteSQL(fSql)
        Do While Not rs.EOF
            Select Case Trim(rs("SIDIVI").Value)
                Case "SAL"
                    Division = "CSECA"
                    Segmento = "SAL"
                Case "ASEO"
                    Division = "CSECA"
                    Segmento = "ASEO"
                Case "QSE"
                    Division = "CSECA"
                    Segmento = "QUIMI"
                Case "QLA", "QLF"
                    Division = rs("RREF02").Value
                    Segmento = "QUIMI"
            End Select
            rs.MoveNext()
        Loop
        rs.Close()

        fSql = " INSERT INTO RHCC( "
        vSql = " VALUES ( "
        fSql &= " HSFCNS    , " : vSql &= "'CXD', "                         '//3A    Tipo Consolidado
        fSql &= " HCONSO    , " : vSql &= "'" & Conso & "', "               '//6A    Consolidado
        fSql &= " HESTAD    , " : vSql &= "45, "                            '//Fin de Cargue
        fSql &= " HPLACA    , " : vSql &= "'" & Placa & "', "               '//6A    Placa
        fSql &= " HPROVE    , " : vSql &= Transportadora & ", "             '//8P0   Transp
        fSql &= " HPLTA     , " : vSql &= "'10', "                          '//Planta
        fSql &= " HNPROVE   , " : vSql &= "(SELECT TNOMBRE FROM RTTRA WHERE TPROVE = " & Transportadora & "), "     '//50A   Transportador
        fSql &= " HFERECA   , " : vSql &= " NOW()   , "                     '//26Z   F.Reque Vehiculo
        fSql &= " HCEDUL    , " : vSql &= " " & CedulaConductor & ", "      '//18P0  Cedula Conductor
        fSql &= " HCHOFE    , " : vSql &= "'" & NombreConductor & "', "     '//30A   Conductor
        fSql &= " HCELU     , " : vSql &= "'" & Left(CelularConductor, 20) & "', "     '//20A   Celular
        fSql &= " HGENERA   , " : vSql &= "'1', "                           '//1A    Generada OC S/N
        fSql &= " HMANIF    , " : vSql &= "'" & Manifiesto & "', "          '//15A   Manifiesto del Transp
        fSql &= " HTIPO     , " : vSql &= "(SELECT VTIPVEH FROM RFFLTIPVEH WHERE VCODIGO = '" & TipoVehiculo & "')" & ", "     '//2P0   Tipo Vehiculo
        fSql &= " HACTUSR   , " : vSql &= "'" & DB.appUser & "', "     '//10A   User UPD
        fSql &= " HOBSER    , " : vSql &= "'" & Notas & "', "               '//120A  Observaciones
        fSql &= " HMERCA    , " : vSql &= "'NACIONAL', "
        fSql &= " HDIVIS    , " : vSql &= "'" & Division & "', "
        fSql &= " HSEGME    , " : vSql &= "'" & Segmento & "', "
        fSql &= " HPER      , " : vSql &= Now().ToString("yyyyMM") & ", "      '
        fSql &= " HBOD      , " : vSql &= "'" & Bodega & "', "              '//3A    Bodega
        fSql &= " HREF05    ) " : vSql &= "'" & Bodega & "' )"              '//15A   Bodega
        DB.ExecuteSQL(fSql & vSql)

        DestinoConso(Conso)
        CreaRECC(Conso)

        fSql = " INSERT INTO RRCC( "
        vSql = " VALUES ( "
        fSql &= " RTREP     , " : vSql &= " 2, "                            '//2S0   Tipo Rep
        fSql &= " RCONSO    , " : vSql &= "'" & Conso & "', "               '//6A    Consolidado
        fSql &= " RMANIF    , " : vSql &= "'" & Manifiesto & "', "          '//15A   Manifiesto del Transp
        fSql &= " RPLACA    , " : vSql &= "'" & Placa & "', "               '//6A    Placa
        fSql &= " RSTSC     , " : vSql &= " (SELECT HESTAD FROM RHCC WHERE HCONSO = '" & Conso & "'), "     '//2S0   Est Cons
        fSql &= " RREPORT   , " : vSql &= "'Consolidado Crossdocking generado. Planillas: " & lstPlanis & " Placa: " & Placa & " Tip Veh " & DescTipoVehiculo & "', "     '//502A  Reporte
        fSql &= " RCRTUSR   , " : vSql &= "'" & DB.appUser & "', "                     '//10A   Usuario
        fSql &= " RAPP      ) " : vSql &= "'CXD') "                     '//15A   App
        DB.ExecuteSQL(fSql & vSql)

        fSql = "UPDATE RHCD SET SBUSY = 3 "
        fSql &= " WHERE CPEDID IN( " & lstPlanis & " ) "
        DB.ExecuteSQL(fSql)

        Tarea.setResultado(String.Format("Consolidado {0} generado.", Conso))


    End Sub

    Private Sub DestinoConso(Conso)
        Dim fSql As String
        Dim rs As ADODB.Recordset
        Dim Recorrido As String = ""
        Dim Dpto As String = ""
        Dim Ciudad As String = ""

        fSql = " SELECT DISTINCT HCONSO, SCIUD, SDPTO " ' , IFNULL( FTDIST, -1 ) AS DISTAN "
        fSql &= " FROM RHCD INNER JOIN RHCC ON SCONSO2 = HCONSO "
        '        fSql &= " INNER JOIN IWM ON DIWHR = LWHS"
        '       fSql &= " LEFT JOIN RFFLDISTKM ON DIWHR = FBOD AND DSHDEP = FDPTO AND DSPOST = FCIUD "
        fSql &= " WHERE "
        fSql &= " HCONSO = '" & Conso & "' "
        '      fSql &= " ORDER BY DISTAN "
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            Recorrido = rs("SCIUD").Value
        End If
        Do While Not rs.EOF
            'If CDbl(rs("DISTAN").Value) = -1 Then
            '    DB.WrtSqlError(rs("HCONSO").Value, "Ciudad sin distancia. " & rs("DSPOST").Value)
            'End If
            Recorrido = Recorrido & "/" & rs("SCIUD").Value
            Dpto = rs("SDPTO").Value
            Ciudad = rs("SCIUD").Value
            rs.MoveNext()
        Loop
        rs.Close()
        rs = Nothing

        fSql = "UPDATE RHCC SET "
        fSql &= "   HCIUDAD = '" & Ciudad & "'"
        fSql &= " , HDEPTO  = '" & Dpto & "'"
        fSql &= " , HDESTI  = '" & Left(Recorrido, 50) & "'"
        fSql &= " WHERE  "
        fSql &= " HCONSO = '" & Conso & "' "
        DB.ExecuteSQL(fSql)


    End Sub


    Private Sub CreaRECC(Conso As String)
        Dim RECC As New clsEntregasConso

        DB.gsKeyWords = Conso
        RECC.Consolidado = Conso
        RECC.ActualizaRECC()

    End Sub

End Class
