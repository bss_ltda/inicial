﻿
Public Class clsInfoSeguimiento
    Public C_XID As String
    Public qInforme As Integer

    Dim fSql As String
    Dim vSql As String

    Dim numID As String

    Public Sub UPDDatos()
        Dim j As Long
        Dim kCau As Long
        Dim kFreq As Long
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim bUpd As Boolean
        Dim FechaCita As Date
        Dim Inicia As Date = Now()
        Dim msgEstado As String = ""
        Dim lleva As String

        Dim qPedido As Long
        Dim regActualizados As Integer = 0
        Dim regCausalIncumplimiento As Integer = 0
        Dim regCausalDefinitiva As Integer = 0
        Dim regFechaRequerido As Integer = 0
        Dim regLineaCerrada As Integer = 0
        Dim regBod As Integer = 0
        Dim regBorr As Integer = 0

        msgEstado &= "Registros Procesados: {1}{0}"
        msgEstado &= "Causal Incumplimiento: {2}{0}"
        msgEstado &= "Causal Definitiva: {3}{0}"
        msgEstado &= "Fecha de Requerido: {4}{0}"
        msgEstado &= "Bodega Cambiada: {5}{0}"
        msgEstado &= "Lineas Cerradas: {6}{0}"
        msgEstado &= "Tiempo transcurrido: {7} min"

        j = 0
        kCau = 0
        kFreq = 0

        numID = DB.CalcConsec("RFFLWRK02", "99999999")
        fSql = "DELETE FROM RFFLWRK02 WHERE WVAL05 = " & numID
        DB.ExecuteSQL(fSql)

        fSql = " SELECT XLREGNUM, XLPEDID, XLLINEA, XLCAUINCUW, XLCAUINCU, XLCAUDEFW, XLSTS7, "
        fSql &= " XLCAUDEF, XFEC03W, XLVACNU1W, XLWHR, XLWHRW, XLBORRARW, LQALL, LQSHP "
        fSql &= " FROM  RDXLS INNER JOIN ECL ON XLPEDID = LORD AND XLLINEA = LLINE"
        fSql &= " WHERE XLID = " & C_XID   '& " AND XLMSG <> 'ACTUALIZADO'"
        rs = DB.ExecuteSQL(fSql)
        Do While Not rs.EOF
            bUpd = False
            If qInforme = 1 Then
                If qPedido <> rs("XLPEDID").Value Then
                    If qPedido <> 0 Then

                    Else
                        qPedido = rs("XLPEDID").Value
                    End If
                End If

                If rs("XLCAUINCUW").Value <> "" And rs("XLCAUINCUW").Value <> rs("XLCAUINCU").Value Then
                    fSql = "SELECT NID FROM RNCC "
                    fSql &= " WHERE NID='TRACKINGORDER' "
                    fSql &= " AND NTIPO='INCUMPLIMIENTO' "
                    fSql &= " AND NPEDID=" & rs("XLPEDID").Value
                    fSql &= " AND NLINEA=" & rs("XLLINEA").Value
                    rs1 = DB.ExecuteSQL(fSql)
                    If rs1.EOF Then
                        fSql = " INSERT INTO RNCC("
                        vSql = " VALUES("
                        fSql &= "NID,      " : vSql &= "'TRACKINGORDER', "
                        fSql &= "NTIPO,    " : vSql &= "'INCUMPLIMIENTO', "
                        fSql &= "NPEDID,   " : vSql &= "" & rs("XLPEDID").Value & ", "
                        fSql &= "NLINEA,   " : vSql &= "" & rs("XLLINEA").Value & ", "
                        fSql &= "NCAUSAL,  " : vSql &= "'" & Left(rs("XLCAUINCUW").Value, 2) & "', "
                        fSql &= "NNOTA,    " : vSql &= "'" & rs("XLCAUINCUW").Value & "', "
                        fSql &= "NCRTUSR ) " : vSql &= "'" & DB.gsUserOP & "')"
                        DB.ExecuteSQL(fSql & vSql)

                        fSql = "UPDATE RDCC SET "
                        fSql &= " DCAUINCU = '" & Left(rs("XLCAUINCUW").Value, 35) & "' "
                        fSql &= " WHERE DPEDID=" & rs("XLPEDID").Value
                        fSql &= " AND DLINEA = " & rs("XLLINEA").Value & " "
                        DB.ExecuteSQL(fSql)
                        regCausalIncumplimiento += 1
                    End If
                    rs1.Close()
                End If
            End If
            If rs("XLCAUDEFW").Value <> "" And rs("XLSTS7").Value = 0 Then
                If rs("XLCAUDEFW").Value <> rs("XLCAUDEF").Value Then
                    fSql = " INSERT INTO SPDHCS ("
                    vSql = " VALUES( "
                    fSql &= " SPID,   " : vSql &= "'SP', "
                    fSql &= " SPORD,  " : vSql &= rs("XLPEDID").Value & ", "
                    fSql &= " SPLIN,  " : vSql &= rs("XLLINEA").Value & ", "
                    fSql &= " SPUSR,  " : vSql &= "'" & DB.gsUserOP & "', "      '//10A   Usuario
                    fSql &= " SPCAU ) " : vSql &= "'" & Left(rs("XLCAUDEFW").Value, 2) & "') "
                    DB.ExecuteSQL(fSql & vSql)

                    fSql = "DELETE FROM RNCC "
                    fSql &= " WHERE NID='TRACKINGORDER' "
                    fSql &= " AND NTIPO='DEFINITIVA' "
                    fSql &= " AND NPEDID=" & rs("XLPEDID").Value
                    fSql &= " AND NLINEA=" & rs("XLLINEA").Value
                    DB.ExecuteSQL(fSql)

                    fSql = " INSERT INTO RNCC("
                    vSql = " VALUES("
                    fSql &= "NID,     " : vSql &= "'TRACKINGORDER', "
                    fSql &= "NTIPO,   " : vSql &= "'DEFINITIVA', "
                    fSql &= "NPEDID,  " : vSql &= "" & rs("XLPEDID").Value & ", "
                    fSql &= "NLINEA,  " : vSql &= "" & rs("XLLINEA").Value & ", "
                    fSql &= "NCAUSAL, " : vSql &= "'" & Left(rs("XLCAUDEFW").Value, 2) & "', "   'Causal
                    fSql &= "NNOTA,   " : vSql &= "'" & rs("XLCAUDEFW").Value & "', "            'Causal
                    fSql &= "NCRTUSR )" : vSql &= "'" & DB.gsUserOP & "')"     'Usuario
                    DB.ExecuteSQL(fSql & vSql)

                    fSql = "UPDATE RDCC SET "
                    fSql &= " DCAUDEF = '" & Left(rs("XLCAUDEFW").Value, 35) & "' "
                    fSql &= " WHERE DPEDID=" & rs("XLPEDID").Value
                    fSql &= " AND DLINEA = " & rs("XLLINEA").Value & " "
                    DB.ExecuteSQL(fSql)

                    regCausalDefinitiva += 1
                End If
            End If
            If rs("LQALL").Value = 0 And rs("LQSHP").Value = 0 Then
                If IsDate(rs("XFEC03W").Value) Then
                    If Year(rs("XFEC03W").Value) >= Year(Now()) - 1 Then
                        FechaCita = rs("XFEC03W").Value

                        UpdFechaCita(rs("XLPEDID").Value, rs("XLLINEA").Value, FechaCita, (rs("XLVACNU1W").Value = 1))

                        fSql = " UPDATE ECL SET "
                        fSql &= "  LSCDT  = " & FechaCita.ToString("yyyyMMdd")    'Schedule Receive Date
                        fSql &= ", CLSRTM = " & FechaCita.ToString("HHmmss")      'Schedule Receive Time
                        fSql &= " WHERE "
                        fSql &= " LORD  = " & rs("XLPEDID").Value
                        fSql &= " AND LLINE=" & rs("XLLINEA").Value
                        DB.ExecuteSQL(fSql)

                        fSql = " INSERT INTO RNCC("
                        vSql = " VALUES("
                        fSql &= "NID,     " : vSql &= "'" & "FR" & "', "
                        fSql &= "NTIPO,   " : vSql &= "'XLS', "
                        fSql &= "NPEDID,  " : vSql &= "" & rs("XLPEDID").Value & ", "
                        fSql &= "NLINEA,  " : vSql &= "" & rs("XLLINEA").Value & ", "
                        fSql &= "NNOTA,   " : vSql &= "'Cambio desde Excel.', "
                        fSql &= "NFECH2,  " : vSql &= "'" & FechaCita.ToString("yyyy-MM-dd-HH.mm.ss.000000") & "' ,"
                        fSql &= "NCRTUSR )" : vSql &= "'" & DB.gsUserOP & "')"     'Usuario
                        DB.ExecuteSQL(fSql & vSql)
                        regFechaRequerido += 1
                    End If
                End If

                If rs("XLWHR").Value <> "" And rs("XLWHRW").Value <> rs("XLCAUINCU").Value Then
                    Dim bodPedid As New PedidoCambioBodega
                    With bodPedid
                        .Bodega = rs("XLWHRW").Value
                        .Pedido = rs("XLPEDID").Value
                        .Linea = rs("XLLINEA").Value
                        .CambiarBodega()
                        regBod += 1
                    End With

                End If

                If rs("XLBORRARW").Value <> "" Then
                    Dim borraPedid As New PedidoCerrarLinea
                    With borraPedid
                        .Pedido = rs("XLPEDID").Value
                        .Linea = rs("XLLINEA").Value
                        .CausalCierre = rs("XLBORRARW").Value
                        If .CierraLineaPed() Then
                            regBorr += 1
                        End If
                    End With
                End If
            End If

            regActualizados += 1
            If regActualizados Mod 500 = 0 Then
                lleva = (DateDiff(DateInterval.Second, Inicia, Now()) / 60).ToString("00.00")
                Tarea.ActualizaEstadoTarea(String.Format(msgEstado, vbCrLf, fn(regActualizados), fn(regCausalIncumplimiento), fn(regCausalDefinitiva), fn(regFechaRequerido), fn(regBod), fn(regLineaCerrada), lleva))
                Console.WriteLine(String.Format(msgEstado, vbCrLf, fn(regActualizados), fn(regCausalIncumplimiento), fn(regCausalDefinitiva), fn(regFechaRequerido), fn(regBod), fn(regLineaCerrada), lleva))

            End If

            fSql = " UPDATE RDXLS SET "
            fSql &= "  XLMSG = 'ACTUALIZADO' "
            fSql &= ", XLACTDAT = NOW() "
            fSql &= " WHERE XLID = " & C_XID & " AND XLREGNUM = " & rs("XLREGNUM").Value
            DB.ExecuteSQL(fSql)

            rs.MoveNext()
        Loop

        lleva = (DateDiff(DateInterval.Second, Inicia, Now()) / 60).ToString("00.00")
        Tarea.setResultado(String.Format(msgEstado, "<br>" & vbCrLf, fn(regActualizados), fn(regCausalIncumplimiento), fn(regCausalDefinitiva), fn(regFechaRequerido), fn(regBod), fn(regLineaCerrada), lleva))
        Tarea.Sts1 = "1"
        Dim Alerta As New clsCorreoMHT
        With Alerta
            .Inicializa()
            .DB = DB
            .Referencia = "INFOSEGUIMIENTO"
            .Asunto = "INFOSEGUIMIENTO " & C_XID
            .Modulo = "INFOSEGUIMIENTO"
            .Referencia = "InfoSeguimiento"
            .Destinatario = DB.gsUserOP
            .AddBCC("BSS")
            .AddLine(String.Format(msgEstado, "<br>", regActualizados, regCausalIncumplimiento, regCausalDefinitiva, regFechaRequerido, regBod, regLineaCerrada, lleva))
            .AddLine("<HR>")
            .EnviaCorreo()
        End With
    End Sub

    Sub UpdFechaCita(Pedido As String, Linea As String, FechaCita As Date, Prorroga As Boolean)
        Dim FechaCitaTS As String
        Dim rs As New ADODB.Recordset

        FechaCitaTS = "'" & FechaCita.ToString("yyyy-MM-dd-HH.mm.ss.000000") & "'"
        fSql = "UPDATE RDCC SET "
        fSql &= " DFLAG = 'D', "
        fSql &= " DPER = " & FechaCita.ToString("yyyymm") & ", "

        fSql &= " DFECR = '" & FechaCita.ToString("yyyy-MM-dd-HH.mm.ss.000000") & "', "

        'Fecha de requerido real, solo se actualiza una vez
        fSql &= " DFEC02 = CASE "
        If Prorroga Then
            fSql &= " WHEN DSLGER='CADENAS' AND DUDP02 < 3 THEN " & FechaCitaTS
        End If
        fSql &= " WHEN DUDP01=0 THEN " & FechaCitaTS & " ELSE DFEC02 END, "
        fSql &= " DFEC04 = " & FechaCitaTS & ", "
        If Prorroga Then
            fSql &= " DUDP02 = DUDP02 + 1 , "
        End If
        fSql &= " DUDP01 = DUDP01 + 1"
        fSql &= " WHERE DPEDID=" & Pedido
        If Linea <> "" Then
            fSql &= " AND DLINEA = " & Linea & " "
        End If
        fSql &= " AND USTSTR = 1 AND DSLMER = 'NACIONAL'"
        DB.ExecuteSQL(fSql)

        fSql = "UPDATE RDCC SET "
        fSql &= " DPER=" & FechaCita.ToString("yyyymm") & ", "
        fSql &= " DFLAG = 'D', "
        fSql &= " DRDTE  = '" & FechaCita.ToString("yyyy-MM-dd-HH.mm.ss.000000") & "', "
        fSql &= " DFEC02 = '" & FechaCita.ToString("yyyy-MM-dd-HH.mm.ss.000000") & "'  "
        fSql &= " WHERE DPEDID = " & Pedido
        If Linea <> "" Then
            fSql &= " AND DLINEA = " & Linea & " "
        End If
        fSql &= " AND USTSTR = 1 AND DSLMER ='EXPORTACIONES'"
        DB.ExecuteSQL(fSql)

        RegPedido(Pedido)

        If Linea = "" Then
            rs.Open("SELECT DIGITS(DPEDID) AS PEDIDO, DIGITS(DLINEA) AS LINEA FROM RDCC WHERE DPEDID=" & Pedido & " AND USTSTR = 1", DB)
            Do While Not rs.EOF
                fSql = "{{ CALL PGM(MPRIBP0) "
                fSql &= " PARM('" & rs("PEDIDO").Value & "'"
                fSql &= " '" & rs("LINEA").Value & "' 'N' 'S' 'I') }}"
                DB.ExecuteSQL(fSql)
                rs.MoveNext()
            Loop
            rs.Close()
        Else
            fSql = "{{ CALL PGM(MPRIBP0) "
            fSql &= " PARM('" & ceros(Pedido, 8) & "'"
            fSql &= " '" & ceros(Linea, 4) & "' 'N' 'S' 'I') }}"
            DB.ExecuteSQL(fSql)
        End If
        rs = Nothing

    End Sub


    Sub RegPedido(Pedido As String)
        Dim fSql As String
        Dim vSql As String

        fSql = " INSERT INTO RFFLWRK02( "
        vSql = " VALUES ( "
        fSql &= " WPEDID    , " : vSql &= " " & Pedido & ", "      '//8S0   Pedido
        fSql &= " WVAL05    ) " : vSql &= " " & numID & ") "      '//15P5  Valor 01
        DB.ExecuteSQL(fSql & vSql)

    End Sub

    Sub FecCitaRECC()
        Dim rs As New ADODB.Recordset

        fSql = "SELECT DISTINCT EID, DFEC02 "
        fSql &= " FROM RDCC INNER JOIN RECC ON DNUMENT = EID "
        fSql &= " INNER JOIN RFFLWRK02 ON WVAL05 = " & numID & " AND DPEDID = WPEDID "
        rs = DB.ExecuteSQL(fSql)
        Do While Not rs.EOF
            fSql = "UPDATE RECC SET EFREQCIT = '" & rs("DFEC02").Value.ToString("yyyy-MM-dd-HH.mm.ss.000000") & "'"
            fSql &= " WHERE EID = " & rs("EID").Value
            DB.ExecuteSQL(fSql)
            rs.MoveNext()
        Loop
        rs.Close()

        fSql = "SELECT ECONSO, MIN(EFREQCIT) AS FCITA "
        fSql &= " FROM RDCC INNER JOIN RECC ON DNUMENT = EID "
        fSql &= " INNER JOIN RFFLWRK02 ON WVAL05 = " & numID & " AND DPEDID = WPEDID "
        fSql &= " WHERE ECONSO <> ''"
        fSql &= " GROUP BY ECONSO"
        rs = DB.ExecuteSQL(fSql)
        Do While Not rs.EOF
            fSql = "UPDATE RHCC SET HCITA = '" & rs("FCITA").Value.ToString("yyyy-MM-dd-HH.mm.ss.000000") & "'"
            fSql &= " WHERE HCONSO = '" & rs("ECONSO").Value & "'"
            DB.ExecuteSQL(fSql)
            rs.MoveNext()
        Loop
        rs.Close()

        rs = Nothing

        fSql = "DELETE FROM RFFLWRK02 WHERE WVAL05 = " & numID
        DB.ExecuteSQL(fSql)

    End Sub

    Sub ExcepcionesNoConsolidar()
        Dim ProcesoJAVA As New clsJVAPGM
        Dim jobNum1 As String

        jobNum1 = "NOCONSO" & DB.CalcConsec("QNOCONSO", "999")

        With ProcesoJAVA
            .Initialize()
            .Jar = "RJNOCONSO"
            .Jobq = DB.getLXJobQ("JOBQ-INFOSEG")
            .BatchNo = jobNum1
            .Job400 = jobNum1
            .Clase = "NoConsolidar"
            .Parametro("lib", DB.gsLib)
            .Parametro("periodo", Now.ToString("yyyymm"))
            .Parametro("job", jobNum1)
            .ExecutePGM()
        End With

    End Sub

    Function fn(vlr As Integer) As String
        Return vlr.ToString("#,##0")
    End Function

    Function PruebaJSON() As String        
        Dim json As New Chilkat.JsonObject

        Dim success As Boolean

        '  The only reason for failure in the following lines of code would be an out-of-memory condition..

        '  An index value of -1 is used to append at the end.
        Dim index As Integer = -1


        success = json.AddStringAt(-1, "Title", "The Cuckoo's Calling")
        success = json.AddStringAt(-1, "Author", "Robert Galbraith")
        success = json.AddStringAt(-1, "Genre", "classic crime novel")

        '  Let's create the Detail JSON object:
        success = json.AddObjectAt(-1, "Detail")
        Dim detail As Chilkat.JsonObject = json.ObjectAt(json.Size - 1)
        success = detail.AddStringAt(-1, "Publisher", "Little Brown")
        success = detail.AddIntAt(-1, "Publication_Year", 2013)
        success = detail.AddNumberAt(-1, "ISBN-13", "9781408704004")
        success = detail.AddStringAt(-1, "Language", "English")
        success = detail.AddIntAt(-1, "Pages", 494)


        '  Add the array for Price
        success = json.AddArrayAt(-1, "Price")
        Dim aPrice As Chilkat.JsonArray = json.ArrayAt(json.Size - 1)

        '  Entry entry in aPrice will be a JSON object.

        '  Append a new/empty ojbect to the end of the aPrice array.
        success = aPrice.AddObjectAt(-1)
        '  Get the object that was just appended.
        Dim priceObj As Chilkat.JsonObject = aPrice.ObjectAt(aPrice.Size - 1)
        success = priceObj.AddStringAt(-1, "type", "Hardcover")
        success = priceObj.AddNumberAt(-1, "price", "16.65")


        success = aPrice.AddObjectAt(-1)
        priceObj = aPrice.ObjectAt(aPrice.Size - 1)
        success = priceObj.AddStringAt(-1, "type", "Kindle Edition")
        success = priceObj.AddNumberAt(-1, "price", "7.00")




        json.EmitCompact = False
        Return json.Emit()

    End Function

End Class
