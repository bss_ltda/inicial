﻿Public Class clsDatosNegocio
    Public Cliente As String
    Public PtoEnvio As String
    Public Producto As String
    Public Facility As String

    Public Sub New()
        Facility = "10"
    End Sub

    Function getPrecio() As Double
        Dim rs As ADODB.Recordset
        Dim fSql As String
        Dim result As Double = 0

        fSql = " SELECT "
        fSql &= "    DMDSPR "
        fSql &= " FROM "
        fSql &= "    PDM "
        fSql &= " WHERE "
        fSql &= "    (DMID = 'DM') "
        fSql &= "    AND (YEAR( NOW() ) * 10000 + MONTH( NOW() ) * 100 + DAY( NOW() )) BETWEEN DMBGDT AND DMENDT "
        fSql &= "    AND (DMDESC LIKE '%PREC%' OR DMDESC = 'LP INDU') "
        fSql &= "    AND DMPROD = '" & Producto & "' "
        fSql &= "    AND DMCUST = " & Cliente
        fSql &= "    AND DMSHIP = " & PtoEnvio
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            result = rs("DMDSPR").Value
        Else
            rs.Close()
            fSql = " SELECT "
            fSql &= "    PFCT1 "
            fSql &= "  , PSDTE "
            fSql &= " FROM "
            fSql &= "    ESP "
            fSql &= " WHERE "
            fSql &= "    PRKEY = CHAR( '" & Producto & "', 35 ) || DIGITS( DEC( " & Cliente & " , 8, 0 ) ) "
            fSql &= "    AND ( YEAR( NOW() ) * 10000 + MONTH( NOW() ) * 100 + DAY( NOW() )) BETWEEN PSDTE AND PSEDT "
            fSql &= "    AND ( SPID = 'SP') "
            fSql &= "    AND ( PMETH ='A' ) "
            fSql &= " ORDER BY "
            fSql &= "    PSDTE DESC "
            rs = DB.ExecuteSQL(fSql)
            If Not rs.EOF Then
                result = rs("PFCT1").Value
            Else
                result = 0
            End If
        End If
        rs.Close()
        rs = Nothing

        Return result

    End Function

    Function getMargen() As Double
        Dim rs As ADODB.Recordset
        Dim fSql As String
        Dim result As Double = 100

        fSql = " SELECT "
        fSql &= "     DMDPCT "
        fSql &= " FROM "
        fSql &= "    (PDM "
        fSql &= "    INNER JOIN "
        fSql &= "        ESTL01 "
        fSql &= "        ON "
        fSql &= "            PDM.DMCUST     = ESTL01.TCUST "
        fSql &= "            AND PDM.DMSHIP = ESTL01.TSHIP) "
        fSql &= "    INNER JOIN "
        fSql &= "        IIM "
        fSql &= "        ON "
        fSql &= "            PDM.DMIDSC = IIM.IDISC "
        fSql &= " WHERE "
        fSql &= "    (YEAR( NOW() ) * 10000 + MONTH( NOW() ) * 100 + DAY( NOW() )) BETWEEN DMBGDT AND DMENDT "
        fSql &= "    AND (DMID = 'DM') "
        fSql &= "    AND (DMDESC LIKE '%MARG%') "
        fSql &= "    AND PDM.DMCUST = " & Cliente
        fSql &= "    AND PDM.DMSHIP = " & PtoEnvio
        fSql &= "    AND IIM.IPROD  = '" & Producto & "' "
        fSql &= " ORDER BY "
        fSql &= "    ESTL01.STDPT1 "
        fSql &= "  , PDM.DMSHIP    "
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            result = rs("DMDPCT").Value
        End If
        rs.Close()
        rs = Nothing

        Return result

    End Function

    Function getCostoProducto() As Double
        Dim rs As ADODB.Recordset
        Dim fSql As String
        Dim result As Double = 0

        fSql = " SELECT "
        fSql &= " CFPLVL "
        fSql &= " FROM "
        fSql &= "    CMF INNER JOIN IIM ON CFPROD=IPROD AND CFCBKT=IUMAT "
        fSql &= " WHERE "
        fSql &= "    CFPROD    = '" & Producto & "' "
        fSql &= "    AND CFFAC ='" & Facility & "' "
        fSql &= "    AND CFCSET=1 "
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            result = rs("CFPLVL").Value
        End If
        rs.Close()
        rs = Nothing
        Return result

    End Function


End Class
