﻿Public Class clsTarea
    Public idTarea As String
    Public Categoria As String
    Public Subcategoria As String
    Public Claves As String
    Public Parametros As String
    Public Usuario As String
    Public Resultado As String
    Public Programa As String
    Public PalabraClave As String
    Public Sts1 As String
    Public Job400 As String

    Dim dictParam As New Dictionary(Of String, String)

    Dim Descripcion As String
    Dim MensajeEstado As String
    Dim Tipo As String
    Dim Mensaje As String
    Dim WrkID As String
    Dim Sts2 As String
    Dim Sts3 As String
    Dim Sts4 As String
    Dim Sts5 As String
    Dim Creacion As String
    Dim Actualizado As String
    Dim Enlace As String
    Dim NumeroTarea As String
    Dim Subcat As String
    Dim Frecuencia As String
    Dim ProximaEjecucion As String
    Dim Retenida As String
    Dim TituloURL As String
    Dim Comando As String

    Function LeeTarea() As Boolean
        Dim rs As ADODB.Recordset
        Dim fSql As String

        fSql = " SELECT * FROM  RFTASK WHERE TNUMREG = " & idTarea
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            idTarea = v1(rs("TNUMREG").Value)
            Categoria = v1(rs("TCATEG").Value)
            Subcategoria = v1(rs("TSUBCAT").Value)
            Descripcion = v1(rs("TDESC").Value)
            MensajeEstado = v1(rs("TLXMSG").Value)
            Tipo = v1(rs("TTIPO").Value)
            Claves = v1(rs("TKEY").Value)
            Programa = v1(rs("TPGM").Value)
            Parametros = v1(rs("TPRM").Value)
            Mensaje = v1(rs("TMSG").Value)
            WrkID = v1(rs("FWRKID").Value)
            Sts1 = v1(rs("STS1").Value)
            Sts2 = v1(rs("STS2").Value)
            Sts3 = v1(rs("STS3").Value)
            Sts4 = v1(rs("STS4").Value)
            Sts5 = v1(rs("STS5").Value)
            Creacion = v1(rs("TCRTDAT").Value)
            Usuario = v1(rs("TCRTUSR").Value)
            Actualizado = v1(rs("TUPDDAT").Value)
            Enlace = v1(rs("TURL").Value)
            NumeroTarea = v1(rs("TNUMTAREA").Value)
            Subcat = v1(rs("TSUBCAT").Value)
            Frecuencia = v1(rs("TFRECUENCIA").Value)
            ProximaEjecucion = v1(rs("TFPROXIMA").Value)
            Retenida = v1(rs("THLD").Value)
            PalabraClave = v1(rs("TKEYWORD").Value)
            Job400 = v1(rs("TJOB400").Value)
            TituloURL = v1(rs("TURLTIT").Value)
            Comando = v1(rs("TCMD").Value)
            LeeTarea = True

            If Sts4 = "1" Then
                Return False
            End If
            'Tarea en Ejecucion
            fSql = "UPDATE RFTASK SET STS4 = 1 WHERE TNUMREG = " & idTarea
            DB.ExecuteSQL(fSql)

            leeParametros()
        Else
            LeeTarea = False
        End If
        rs.Close()

    End Function

    Sub ActualizaTarea()
        Dim fSql As String
        If Sts4 = "0" Then
            fSql = " UPDATE RFTASK SET "
            fSql &= "  STS1    = " & Me.Sts1 & ""
            fSql &= ", STS2    = 1 " 'Termino
            fSql &= ", STS4    = 0 " 'Ya no esta en Ejecucion
            fSql &= ", TUPDDAT = NOW() "
            fSql &= ", TMSG    = '" & Me.Resultado.Trim().Replace("'", "''") & "' "
            fSql &= ", TJOB400 = '" & Me.Job400 & "' "
            fSql &= " WHERE TNUMREG = " & Me.idTarea
            DB.ExecuteSQL(fSql)
            If Me.Sts1 = "1" Then
                If CInt(Frecuencia) > 0 Then
                    ProgramaSiguiente()
                End If
            End If
        End If


    End Sub


    Sub ActualizaEstadoTarea(Estado As String)
        Dim fSql As String

        fSql = " UPDATE RFTASK SET "
        fSql &= " TMSG = '" & Estado & "' "
        fSql &= " WHERE TNUMREG = " & Me.idTarea
        DB.ExecuteSQL(fSql)

    End Sub

    Sub GuardaError()
        Dim fSql As String
        Me.Resultado = Me.Resultado.Trim().Replace("'", "''")
        Dim sql As String = DB.lastSQL
        fSql = " UPDATE RFTASK SET "
        fSql &= "  STS1 = -9, STS2 = -9 "
        fSql &= ", TUPDDAT = NOW() "
        fSql &= ", TMSG = '" & Left(Trim(Me.Resultado), 15000) & "' "
        fSql &= " WHERE TNUMREG = " & Me.idTarea
        DB.ExecuteSQLError(fSql)
        DB.WrtSqlError(sql, Left(Trim(Me.Resultado), 20000))

    End Sub

    Sub ProgramaSiguiente()
        Dim fSql, vSql As String
        Dim Proxima As String

        Proxima = tsAS400(DateAdd(DateInterval.Minute, CInt(Me.Frecuencia), Now()))

        fSql = " INSERT INTO RFTASK( "
        vSql = " VALUES ( "
        fSql &= " TCATEG    , " : vSql &= "'" & Me.Categoria & "', "            '//20A   Categoria
        fSql &= " TSUBCAT   , " : vSql &= "'" & Me.Subcategoria & "', "         '//20A   Categoria
        fSql &= " TDESC     , " : vSql &= "'" & Me.Descripcion & "', "          '//100A  Descripcion
        fSql &= " TFPROXIMA , " : vSql &= "'" & Proxima & "' ,  "               '//26Z   Proxima Ejecucion"
        fSql &= " TTIPO     , " : vSql &= "'" & Me.Tipo & "', "                 '//22A   Tipo
        fSql &= " THLD      , " : vSql &= " " & Me.Retenida & ", "              '//1P0   Retenida
        fSql &= " TKEY      , " : vSql &= "'" & Me.Claves & "', "
        fSql &= " TKEYWORD  , " : vSql &= "'" & Me.PalabraClave & "', "
        fSql &= " TCRTUSR   , " : vSql &= "'" & Me.Usuario & "', "              '//10A   Usuario
        fSql &= " TPGM      ) " : vSql &= "'" & Me.Comando & "') "              '//502A  Programa
        DB.ExecuteSQL(fSql & vSql)

    End Sub

    Function getParametro(prm As String) As String
        Try
            Return dictParam.Item(prm.ToUpper())
        Catch ex As Exception
            Return ""
        End Try

    End Function

    Sub setResultado(txt As String)
        If Resultado <> "" Then
            Resultado &= "<br>" & vbCrLf
        End If
        Resultado &= txt
    End Sub

    Sub setMsgError(txt As String)
        Resultado &= txt
    End Sub

    Sub leeParametros()
        Dim aPrms() As String
        Dim aPrm() As String
        Dim item As String


        aPrms = Parametros.Split("|")
        For Each item In aPrms
            aPrm = item.Split("=")
            If UBound(aPrm) = 1 Then
                dictParam.Add(aPrm(0).ToUpper(), aPrm(1))
            End If
        Next

    End Sub

    Function v1(d) As String
        If IsDBNull(d) Then
            Return ""
        Else
            Return CStr(d)
        End If
    End Function

    Public Sub New()
        idTarea = ""
        Categoria = ""
        Subcategoria = ""
        Claves = ""
        Parametros = ""
        Usuario = ""
        Resultado = ""
        Programa = ""
        PalabraClave = ""
        Sts1 = ""
        Job400 = ""
    End Sub
End Class
