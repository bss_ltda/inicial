﻿Public Class clsCorreoMHT
    Public Modulo As String
    Public Referencia As String
    Public Asunto As String
    Public Remitente As String
    Public Destinatario As String
    Public ConCopiaA As String
    Public Mensaje As String
    Public Adjuntos As String
    Public Enviado As String
    Public Creado As String
    Public Actualizado As String
    Public DB As clsConexion

    Dim CopiaOculta As String

    Function EnviaCorreo()
        Dim IdMail
        Dim fSql, vSql As String

        IdMail = DB.CalcConsec("ERPLX834F.RMAILX", "99999999")
        fSql = " INSERT INTO ERPLX834F.RMAILX( "
        vSql = " VALUES ( "
        fSql &= " LID       , " : vSql &= " " & IdMail & ", "         '//8P0    ID
        fSql &= " LMOD      , " : vSql &= "'" & Modulo & "', "        '//25A   Modulo
        fSql &= " LREF      , " : vSql &= "'" & Left(Referencia, 25) & "', "    '//25A   Referencia
        fSql &= " LASUNTO   , " : vSql &= "'" & Asunto & "', "        '//102A  Asunto
        fSql &= " LENVIA    , " : vSql &= "'" & Remitente & "', "     '//152A  Remitente
        fSql &= " LPARA     , " : vSql &= "'" & Destinatario & "', "  '//2002A Destinatario
        fSql &= " LCOPIA    , " : vSql &= "'" & ConCopiaA & "', "     '//2002A Con Copia A
        fSql &= " LBCC      , " : vSql &= "'" & CopiaOculta & "', "     '//152A  BCC
        fSql &= " LCUERPO   , " : vSql &= "'" & Mensaje & "', "       '//10002AMensaje
        fSql &= " LADJUNTOS ) " : vSql &= "'" & Adjuntos & "') "      '//10002AAdjuntos
        DB.ExecuteSQL(fSql & vSql)
        EnviaCorreo = IdMail

    End Function

    Sub AddMHT(txt)
        Mensaje = CStr(txt)
    End Sub

    Sub AddLine(txt)

        If txt = "" Then
            Mensaje = ""
        Else
            Mensaje = Mensaje & CStr(txt) & "<br/>"
        End If

    End Sub

    Sub AddTO(dest As String)
        If Destinatario = "" Then
            Destinatario = dest
        Else
            Destinatario = Destinatario & ", " & dest
        End If
    End Sub

    Sub AddCC(dest As String)
        If ConCopiaA = "" Then
            ConCopiaA = dest
        Else
            ConCopiaA &= ", " & dest
        End If
    End Sub

    Sub AddBCC(dest As String)
        If CopiaOculta = "" Then
            CopiaOculta = dest
        Else
            CopiaOculta &= ", " & dest
        End If
    End Sub


    Function AvisarA()
        Dim rs As New ADODB.RecordSet
        Dim fSql As String
        Dim resultado As String = ""
        fSql = " SELECT CCNOT1 FROM ZCC WHERE CCTABL = 'FLAVISOS' AND CCCODE = '" & Modulo & "' "
        If DB.OpenRS(rs, fSql) Then
            resultado = rs("CCNOT1").Value
        End If
        rs.Close()
        rs = Nothing
        Return resultado

    End Function


    Sub Adjuntar(archivo As String)

        If Adjuntos = "" Then
            Adjuntos = archivo
        Else
            Adjuntos &= "," & archivo
        End If

    End Sub

    Sub Inicializa()
        Modulo = ""
        Referencia = ""
        Asunto = ""
        Remitente = ""
        Destinatario = ""
        ConCopiaA = ""
        Mensaje = ""
        Adjuntos = ""
        Enviado = ""
        Creado = ""
        Actualizado = ""
        CopiaOculta = ""
    End Sub

    Public Sub New()
        Inicializa()
    End Sub
End Class
