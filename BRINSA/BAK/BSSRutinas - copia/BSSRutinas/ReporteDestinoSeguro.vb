﻿Imports System.IO
Imports System.Xml.Serialization

Public Class ReporteDestinoSeguro
    Public Consolidado As String

    Private fSql, vSql As String

    Function reporte() As String
        Dim rsRHCC As New ADODB.Recordset
        Dim rs As New ADODB.Recordset
        Dim errProceso As String = ""
        Dim ruta As String = ""

        Dim DestinoSeguro As New net.destinoseguro.www.WSdestino
        Dim DespachoTransp As New ServiceReferenceDestinoSeguro.setDespachoTranspRequest
        DB.gsKeyWords = Consolidado

        With DespachoTransp
            .usr = " "
            .pwd = " "
            .salida = " "
            .nittra = " "
            .manifi = " "
            .corigen = " "
            .cdestino = " "
            .via = " "
            .rut = " "
            .placa = " "
            .cmarca = " "
            .ccarroceria = " "
            .ccolor = " "
            .modelo = " "
            .cedula = " "
            .nombres = " "
            .apellidos = " "
            .dir = " "
            .tels = " "
            .cel = " "
            .nitgen = " "
            .nomgen = " "
            .sucursal = " "
            .obs = " "
            .it1 = " "
            .it2 = " "
            .it3 = Now().ToString("yyyy-MM-dd HH:mm")
        End With

        fSql = "SELECT CHAR( VARCHAR_FORMAT( HSALE,'yyyy-MM-dd HH24:MI:SS'), 19 ) AS SALIDA  "
        fSql &= ", HBOD, HPROVE, HMANIF, HCIUDAD, HDEPTO, HDIVIS, HSEGME, HREF06 "
        fSql &= ", HPLACA, HCEDUL, HCELU, HCHOFE"
        fSql &= " FROM RHCC H WHERE HCONSO = '" & Consolidado & "'"
        If DB.OpenRS(rsRHCC, fSql) Then
            errProceso = datosOK(rsRHCC)
            If errProceso = "" Then
                DespachoTransp.usr = "WSBRINSA"
                DespachoTransp.pwd = "123456"
                'salida
                DespachoTransp.salida = Replace(rsRHCC("SALIDA").Value, " ", "T")
                'nittra
                fSql = "SELECT VMFSCD FROM AVM WHERE VENDOR = " & rsRHCC("HPROVE").Value
                If DB.OpenRS(rs, fSql) Then
                    Dim aNit() As String = Split(rs(0).Value, "-")
                    DespachoTransp.nittra = CampoNum(aNit(0))
                Else
                    errProceso &= "NIT Transportadora|"
                End If
                rs.Close()

                'manifi
                DespachoTransp.manifi = rsRHCC("HMANIF").Value
                DespachoTransp.it2 = rsRHCC("HDIVIS").Value & "/" & rsRHCC("HSEGME").Value & "/" & rsRHCC("HREF06").Value
                'corigen
                fSql = "SELECT WMSLOC, CDPTOD, CCIUDAD FROM IWM INNER JOIN RFDANE ON WMSLOC = CPOBLADO "
                fSql &= " WHERE LWHS = '" & rsRHCC("HBOD").Value & "'"
                If DB.OpenRS(rs, fSql) Then
                    DespachoTransp.corigen = rs("WMSLOC").Value
                    ruta = "<br/>Origen: " & rs("WMSLOC").Value & " " & rs("CDPTOD").Value & "/" & rs("CCIUDAD").Value
                Else
                    errProceso &= "Codigo dane ciudad origen|"
                End If
                rs.Close()

                'cdestino
                fSql = " SELECT CPOBLADO, CDPTOD, CCIUDAD FROM RFDANE "
                fSql &= " WHERE CLXDPTO = '" & IIf(rsRHCC("HDEPTO").Value = "D.C", "CUN", rsRHCC("HDEPTO").Value) & "' "
                fSql &= " AND CLXCIUD = '" & rsRHCC("HCIUDAD").Value & "' "
                fSql &= " AND CTIPO = 'CM' "
                If DB.OpenRS(rs, fSql) Then
                    DespachoTransp.cdestino = rs(0).Value
                    ruta &= "<br/>Destino: " & rs("CPOBLADO").Value & " " & rs("CDPTOD").Value & "/" & rs("CCIUDAD").Value
                Else
                    rs.Close()
                    fSql = " SELECT PCENUS  "
                    fSql &= " FROM LPCL01  "
                    fSql &= " WHERE PCSTCD = '" & IIf(rsRHCC("HDEPTO").Value = "D.C", "CUN", rsRHCC("HDEPTO").Value) & "'  "
                    fSql &= " AND PCPSCD = '" & rsRHCC("HCIUDAD").Value & "'  "
                    fSql &= " AND PCENUS <> '' "
                    If DB.OpenRS(rs, fSql) Then
                        DespachoTransp.cdestino = rs(0).Value & "000"
                        rs.Close()
                        fSql = " SELECT CPOBLADO, CDPTOD, CCIUDAD FROM RFDANE "
                        fSql &= " WHERE CPOBLADO = '" & DespachoTransp.cdestino & "' "
                        fSql &= " AND CTIPO = 'CM' "
                        If DB.OpenRS(rs, fSql) Then
                            ruta &= "<br/>Destino: " & rs("CPOBLADO").Value & " " & rs("CDPTOD").Value & "/" & rs("CCIUDAD").Value
                        End If
                    Else
                        errProceso &= "Codigo dane ciudad destino|"
                    End If
                End If
                rs.Close()

                DespachoTransp.placa = rsRHCC("HPLACA").Value
                DespachoTransp.cedula = rsRHCC("HCEDUL").Value
                DespachoTransp.nombres = campoAlfa(rsRHCC("HCHOFE").Value)
                DespachoTransp.cel = Replace(rsRHCC("HCELU").Value, " ", "")
                DespachoTransp.nitgen = "800221789"
                DespachoTransp.nomgen = "BRINSA S.A."
                DespachoTransp.sucursal = "1"
                DespachoTransp.it1 = Consolidado
                DespachoTransp.rut = "0"
            End If
        End If

        If errProceso <> "" Then
            'avisa()
            Return errProceso
        End If
        Dim msg As String = ""
        msg &= "<br />usr      = " & DespachoTransp.usr
        msg &= "<br />pwd      = " & DespachoTransp.pwd
        msg &= "<br />salida   = " & DespachoTransp.salida
        msg &= "<br />nittra   = " & DespachoTransp.nittra
        msg &= "<br />manifi   = " & DespachoTransp.manifi
        msg &= "<br />corigen  = " & DespachoTransp.corigen
        msg &= "<br />cdestino = " & DespachoTransp.cdestino
        msg &= "<br />placa    = " & DespachoTransp.placa
        msg &= "<br />cedula   = " & DespachoTransp.cedula
        msg &= "<br />nombres  = " & DespachoTransp.nombres
        msg &= "<br />cel      = " & DespachoTransp.cel
        msg &= "<br />nitgen   = " & DespachoTransp.nitgen
        msg &= "<br />nomgen   = " & DespachoTransp.nomgen
        msg &= "<br />sucursal = " & DespachoTransp.sucursal
        msg &= "<br />it1      = " & DespachoTransp.it1
        msg &= "<br />rut      = " & DespachoTransp.rut
        With (DespachoTransp)
            Try
                errProceso = DestinoSeguro.setDespachoTransp( _
                                .usr, _
                                .pwd, _
                                .salida, _
                                .nittra, _
                                .manifi, _
                                .corigen, _
                                .cdestino, _
                                .via, _
                                .rut, _
                                .placa, _
                                .cmarca, _
                                .ccarroceria, _
                                .ccolor, _
                                .modelo, _
                                .cedula, _
                                .nombres, _
                                .apellidos, _
                                .dir, _
                                .tels, _
                                .cel, _
                                .nitgen, _
                                .nomgen, _
                                .sucursal, _
                                .obs, _
                                .it1, _
                                .it2, _
                                .it3, _
                                .itemadicional, _
                                .usrinterno)
            Catch ex As Exception
                Console.WriteLine(ex.Message)
                DB.WrtSqlError("", ex.ToString)
            End Try

        End With
        Console.WriteLine(errProceso)
        DB.Bitacora("DESTINOSEGURO", msg & "<br>Respuesta webservice: " & procesaError(errProceso, ruta), Consolidado)


        Return errProceso
    End Function

    Private Function revisaRuta(msg As String) As String
        Dim aMsg() As String = Split(msg, "|")
        Do While InStr(aMsg(1), " ") > 0
            aMsg(1) = aMsg(1).Replace(" ", "")
        Loop
        If aMsg(0) = "0" Then
            DB.WrtSqlError("", aMsg(1) & " " & DB.gsKeyWords)
        End If
        Return aMsg(1)
    End Function

    Private Function procesaError(msg As String, ruta As String) As String
        Dim aMsg() As String = Split(msg, "|")

        If aMsg(0) = "0" Then
            If InStr(UCase(aMsg(1).Replace(" ", "")), "LARUTANOEXISTEONOESTAHOMOLOGADA") > 0 Then
                aMsg(1) &= ruta & "<br/>"
                rutaNoExiste(ruta, "")
                'rutaNoExiste(ruta, "BSS,us01@bssltda.com,BSS2")
                'DB.WrtSqlError("", aMsg(1) & " " & DB.gsKeyWords, "us01@bssltda.com,BSS2")
            Else
                DB.WrtSqlError("", aMsg(1) & " " & DB.gsKeyWords)
            End If

        End If
        LogConso("DSEGURO " & aMsg(1))
        Return aMsg(1)
    End Function

    Sub rutaNoExiste(ruta As String, correo As String)
        Dim Alerta As New clsCorreoMHT
        With Alerta
            .Inicializa()
            .DB = DB
            .Asunto = "LA RUTA NO EXISTE O NO ESTA HOMOLOGADA"
            .Modulo = "DESTINOSEGURO"
            .Referencia = "Ruta No Existe (BRINSA)"
            If correo.Trim <> "" Then
                .Destinatario = correo
            Else
                .Destinatario = .AvisarA()
            End If

            .AddLine(ruta)
            .AddLine("<hr />")
            .AddLine("El presente origen y destino no está homologado en su sistema.")
            .AddLine("Por favor incluirlo.")
            .AddLine("<hr />")
            .AddLine("Este es un aviso automático desde el sistema de BRINSA.")
            .EnviaCorreo()
        End With

    End Sub

    'CampoNum()
    Private Function CampoNum(Dato) As String
        Dim i As Integer
        Dim Ascii As Integer
        Dim resultado As String = ""
        For i = 1 To Len(Dato)
            Ascii = Asc(Mid(Dato, i, 1))
            If Ascii >= Asc("0") And Ascii <= Asc("9") Then
                resultado &= Chr(Ascii)
            End If
        Next
        Return resultado

    End Function


    Private Sub LogConso(reporte As String)
        fSql = "UPDATE RHCC SET HREPORT = '" & Left(reporte, 50) & "', "
        fSql &= " HFECREP = NOW()"
        fSql &= " WHERE HCONSO = '" & Consolidado & "'"
        DB.ExecuteSQL(fSql)

        fSql = "INSERT INTO RRCC("
        vSql = "VALUES( "
        fSql &= "RCONSO, " : vSql &= "'" & Consolidado & "'" & ", "
        fSql &= "RSTSC, "
        vSql &= "( SELECT IFNULL(SUM(HESTAD), 9) FROM RHCC WHERE HCONSO='" & Consolidado & "' )" & ", "
        fSql &= "RTREP, " : vSql &= " " & "3" & " " & ", "
        fSql &= "RFECREP, " : vSql &= " NOW(), "
        fSql &= "RREPORT, " : vSql &= "'" & Left(Trim(reporte), 15000) & "'" & ", "
        fSql &= "RAPP, " : vSql &= "'PCDS'" & ", "
        fSql &= "RCRTUSR) " : vSql &= "'SISTEMA'" & ") "
        DB.ExecuteSQL(fSql & vSql)

    End Sub

    Private Function datosOK(rs As ADODB.Recordset) As String
        If Not PlacaOK(rs("HPLACA").Value) Then
            Return "Error en PLACA " & rs("HPLACA").Value
        ElseIf CDbl(rs("HMANIF").Value) = 0 Then
            Return "Error manifiesto " & rs("HMANIF").Value
        End If
        Return ""
    End Function

    Private Function PlacaOK(placa As String) As Boolean
        Dim i As Integer
        Dim Ascii As Integer

        If Len(placa) <> 6 Then
            Return False
        End If

        For i = 1 To 3
            Ascii = Asc(Mid(placa, i, 1))
            If Ascii < Asc("A") Or Ascii > Asc("Z") Then
                Return False
            End If
        Next
        For i = 4 To 6
            Ascii = Asc(Mid(placa, i, 1))
            If Ascii < Asc("0") Or Ascii > Asc("9") Then
                Return False
            End If
        Next

        Return True

    End Function

End Class
