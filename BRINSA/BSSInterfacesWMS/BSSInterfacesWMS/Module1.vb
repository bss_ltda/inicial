﻿Imports System.Threading

Module Module1
    Dim p() As Process

    Sub Main()

        p = Process.GetProcessesByName("BSSInterfacesWMS")
        If p.Count = 1 Then
            Hilos()
        End If

    End Sub

    Sub Hilos()
        Dim wms_segundos As Int16
        Dim wma_segundos As Int16

        wms_segundos = CInt(My.Settings.WMS_SEGUNDOS) * 1000
        wma_segundos = CInt(My.Settings.WMA_SEGUNDOS) * 1000

        Dim wms = New Thread(Sub()
                                 Do
                                     Console.WriteLine("Enviando " & My.Settings.RUTINAS & " WMS")
                                     Shell(My.Settings.RUTINAS & " WMS", AppWinStyle.NormalFocus, True, -1)
                                     Console.WriteLine("WMS Esperando ...")
                                     Thread.Sleep(wms_segundos)
                                 Loop
                             End Sub)
        'wms.IsBackground = True
        wms.Start()

        Dim wma = New Thread(Sub()
                                 Do
                                     Console.WriteLine("Enviando " & My.Settings.RUTINAS & " WMS")
                                     Shell(My.Settings.RUTINAS & " WMA", AppWinStyle.NormalFocus, True, -1)
                                     Console.WriteLine("WMA Esperando ...")
                                     Thread.Sleep(wma_segundos)
                                 Loop
                             End Sub)
        'wma.IsBackground = True
        wma.Start()


    End Sub


End Module
