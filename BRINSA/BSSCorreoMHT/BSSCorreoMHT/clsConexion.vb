﻿Option Explicit On

Imports ADODB

Public Class clsConexion
    Public DB As ADODB.Connection
    Public gsDatasource As String
    Public gsLib As String
    Public gsConnstring As String
    Public gsUser As String
    Public gsAppName As String
    Public gsAppPath
    Public gsAppVersion As String
    Public gsKeyWords As String
    Friend Property EjecucionConsola As Boolean = False

    Public ModuloActual As String = ""
    Public lastError As String = ""
    Public lastSQL As String = ""
    Public bDateTimeToChar As Boolean = False
    Dim regA As Double

    Public Const DB2_DATEDEC = " ( YEAR( NOW() ) * 10000 + MONTH( NOW()  ) * 100 + DAY( NOW() ) )"
    Public Const DB2_TIMEDEC0 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    Public Const DB2_TIMEDEC1 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 ) "

    Function Conexion() As Boolean
        Dim gsProvider As String = "IBMDA400"
        Dim Resultado As Boolean
        Dim rs As New ADODB.Recordset

        gsKeyWords = ""
        gsConnstring = "Provider=" & gsProvider & ";Data Source=" & gsDatasource & ";"
        gsConnstring = gsConnstring & "Persist Security Info=False;Default Collection=" & gsLib & ";"
        gsConnstring = gsConnstring & "Password=LXAPL;User ID=APLLX;"
        gsConnstring = gsConnstring & "Application Name = BSSCORREOS;"
        'gsConnstring = gsConnstring & "Password=ABCD1234;User ID=DEFAULT;"

        DB = New ADODB.Connection
        DB.Open(gsConnstring)

        Dim gsPass As String = getLXParam("LXPASS")
        gsUser = getLXParam("LXUSER")

        If gsUser <> "" And gsPass <> "" Then
            gsConnstring = "Provider=" & gsProvider & ";Data Source=" & gsDatasource & ";"
            gsConnstring = gsConnstring & "Persist Security Info=False;Default Collection=" & gsLib & ";"
            gsConnstring = gsConnstring & "Password=" & gsPass & ";User ID=" & gsUser & ";"
            gsConnstring = gsConnstring & "Application Name = BSSCORREOS;"
            gsConnstring = gsConnstring & "Force Translate=0;"
            If bDateTimeToChar Then
                gsConnstring &= "Convert Date Time To Char=FALSE;"
            End If
            DB.Close()
            DB.Open(gsConnstring)
            Resultado = True
        Else
            Resultado = False
        End If

        Return Resultado

    End Function

    Public Function ExecuteSQL(ByVal sql As String) As ADODB.Recordset
        Dim txtErr As String
        sql = CStr(sql)
        lastSQL = sql
        Try
            ExecuteSQL = DB.Execute(sql, regA)
        Catch ex As Exception
            If EjecucionConsola Then
                Console.WriteLine(ex.Message)
            End If
            If InStr(sql, "{{") = 0 And InStr(UCase(sql), "DROP TABLE") = 0 Then
                txtErr = Err.Number & " " & Err.Description
                With New RegistroEvento
                    .IdMessage = "0"
                    .IdMessage = txtErr
                    .LastSQL = sql
                    .EjecucionConsola = EjecucionConsola
                    .RegistrarEvento()
                End With
            End If
            ExecuteSQL = Nothing
        End Try
    End Function

    Public Function OpenRS(ByRef rs As ADODB.Recordset, fSql As String, CursorType As CursorTypeEnum, LockType As LockTypeEnum) As Boolean
        lastSQL = fSql
        rs.Open(fSql, DB, CursorType, LockType)
        Return Not rs.EOF

    End Function

    Public Function OpenRS(ByRef rs As ADODB.Recordset, fSql As String) As Boolean
        lastSQL = fSql
        rs.Open(fSql, DB)
        Return Not rs.EOF
    End Function

    Function LookUp(ColumnName As String, TableName As String, Where As String) As String
        Dim rs As New ADODB.Recordset
        Dim fSql As String

        If Left(ColumnName, 6) = "SELECT" Then
            fSql = ColumnName
        Else
            fSql = "SELECT " & ColumnName & " FROM " & TableName & " WHERE " & Where
        End If

        rs.Open(fSql, DB)
        If Not rs.EOF Then
            LookUp = CStr(rs(0).Value)
        Else
            LookUp = ""
        End If
        rs.Close()

    End Function
    Public Function getLXParam(prm As String) As String
        Return getLXParam(prm, "")
    End Function

    Public Function getLXParam(prm As String, Campo As String) As String
        Dim rs As New ADODB.Recordset
        Dim resultado As String = ""
        If Campo = "" Then
            Campo = "CCNOT1"
        End If
        lastSQL = "SELECT " & Campo & " AS DATO FROM RFPARAM WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER('" & prm & "')"
        gsKeyWords &= " " & lastSQL & " [" & Me.DB.State & "]" & " [" & DB.State & "]"
        rs.Open(lastSQL, Me.DB)
        If Not rs.EOF Then
            resultado = rs("DATO").Value
        End If
        rs.Close()
        gsKeyWords = "[ok]"
        Return resultado.Trim

    End Function

    Public Sub setLXParam(codigo As String, Campo As String, Valor As String)
        Dim fSql As String

        If Campo = "" Then
            Campo = "CCDESC"
        End If
        fSql = "UPDATE RFPARAM SET  " & Campo & " = '" & Valor & "'"
        fSql &= " FROM RFPARAM "
        fSql &= " WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER('" & codigo & "')"
        ExecuteSQL(fSql)

    End Sub

    Public Sub setLXParamNum(codigo As String, Campo As String, Valor As String)
        Dim fSql As String

        If Campo = "" Then
            Campo = "CCDESC"
        End If
        fSql = "UPDATE RFPARAM SET  " & Campo & " = " & CDbl(Valor)
        fSql &= " WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER('" & codigo & "')"
        ExecuteSQL(fSql)
    End Sub

    Public Function regActualizados() As Long
        Return regA
    End Function

    Sub Close()
        DB.Close()
    End Sub

    Function Estado() As Integer
        Return DB.State
    End Function

End Class
