﻿Public Class wsCorreoMHT
    Private WithEvents M_timer As New Timers.Timer(30000)
    Friend Property ServicioCorreo As ServicioCorreo

    Public sEstado As String = ""
    Public sAmbiente As String = ""
    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Agregue el código aquí para iniciar el servicio. Este método debería poner
        ' en movimiento los elementos para que el servicio pueda funcionar.
        Me.EventLog1.WriteEntry("Inicia")
        M_timer.Enabled = True

    End Sub
    'aqui ingresa como Servicio Windows
    Private Sub m_timer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles M_timer.Elapsed
        Try
            M_timer.Enabled = False
            If Not AbreConexion("") Then
                Exit Sub
            End If
            ServicioCorreo = New ServicioCorreo
            ServicioCorreo.DB = DB
            ServicioCorreo.SetParametros()
            ServicioCorreo.EjecucionConsola = False
            If RevisaArchivo("RFLOG") Then
                AvisoLog()
            End If
            If RevisaArchivo("RMAILX") Then
                revisaTareasMHT()
            End If
            DB.Close()
        Catch ex As Exception
            With New RegistroEvento
                .IdMessage = ServicioCorreo.IdMensaje
                .Message = ex.Message
                .EjecucionConsola = ServicioCorreo.EjecucionConsola
                .RegistrarEvento()
            End With
        End Try
        M_timer.Enabled = True
    End Sub
    'Aqui ingresa como aplicacion de consola
    Sub Inicializador()
        If Not AbreConexion("") Then
            Exit Sub
        End If
        Try
            ServicioCorreo = New ServicioCorreo
            ServicioCorreo.DB = DB
            ServicioCorreo.SetParametros()
            ServicioCorreo.EjecucionConsola = True
            ServicioCorreo.EscribirEnConsola(ServicioCorreo.VerParametros)
            DB.EjecucionConsola = True
            If ServicioCorreo.Ejecutar Then
                If RevisaArchivo("RFLOG") Then
                    AvisoLog()
                End If
                If RevisaArchivo("RMAILX") Then
                    revisaTareasMHT()
                End If
            End If
            DB.Close()
        Catch ex As Exception
            ServicioCorreo.EscribirEnConsola(ex.Message)
            With New RegistroEvento
                .IdMessage = ServicioCorreo.IdMensaje
                .Message = ex.Message
                .EjecucionConsola = ServicioCorreo.EjecucionConsola
                .RegistrarEvento()
            End With
        End Try
        ServicioCorreo.EscribirEnConsola("Presione una tecla para continuar.")
        If ServicioCorreo.EjecucionConsola Then
            Console.ReadKey()
        End If
    End Sub

    Sub revisaTareasMHT()
        Dim fSql As String
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim resultado As String = ""
        Dim Correo As New clsMHT

        DB.gsKeyWords = "revisaTareasMHT (1)"

        fSql = " SELECT LNUMREG, CASE WHEN LID = 0 THEN LNUMREG ELSE LID END LID, LMOD, LREF, LASUNTO, LENVIA, "
        fSql &= "LPARA, LCOPIA, LCUERPO, LADJUNTOS, LENVIADO, LCRTDAT, LUPDDAT, IFNULL( LMSGERR, '' ) AS LMSGERR, LKEY, LBCC, LAMBIENTE "
        fSql &= "  FROM  RMAILX "
        fSql &= "  WHERE "
        'Para pruebas. Se debe poner en configuracion del proyecto MyProject LID=64955
        If unLID <> "" Then
            fSql &= "LID = " & unLID
        Else
            fSql &= "  LENVIADO = 0 "
        End If
        DB.OpenRS(rs, fSql)
        Do While Not rs.EOF
            sAmbiente = "ERPLX834F."
            If rs("LAMBIENTE").Value <> "" Then
                sAmbiente = rs("LAMBIENTE").Value & "."
            End If
            DB.gsKeyWords = rs("LNUMREG").Value
            fSql = " UPDATE RMAILX SET  "
            fSql &= " LUPDDAT = NOW() , "
            fSql &= " LENVIADO = -1,  "
            fSql &= " LMSGERR = '" & System.Net.Dns.GetHostName() & vbCr & "Inicia proceso' "
            fSql &= " WHERE LNUMREG = " & rs("LNUMREG").Value
            DB.ExecuteSQL(fSql)
            ServicioCorreo.IdMensaje = rs("LNUMREG").Value
            With New clsMHT
                .Inicializa()
                .ServicioCorreo = ServicioCorreo
                .sAmbiente = sAmbiente
                .IdMsg = rs("LNUMREG").Value
                .Modulo = rs("LMOD").Value
                .Asunto = Trim(rs("LASUNTO").Value) & " (" & rs("LNUMREG").Value & ")"
                .De = IIf(rs("LENVIA").Value = "", rs("LREF").Value, rs("LENVIA").Value)
                .Para = rs("LPARA").Value
                .CC = rs("LCOPIA").Value
                .BCC = rs("LBCC").Value
                If .BCC = "" Then
                    .BCC = DB.LookUp("CCNOT1", "RFPARAM", "CCTABL = 'CORREOMHT' AND CCCODE = '" & rs("LMOD").Value & "' AND CCUDC2 = 1")
                Else
                    .BCC &= "," & DB.LookUp("CCNOT1", "RFPARAM", "CCTABL = 'CORREOMHT' AND CCCODE = '" & rs("LMOD").Value & "' AND CCUDC2 = 1")
                End If
                .url = rs("LCUERPO").Value
                If InStr(.url.ToUpper, "HREF") <> 0 Then
                    .url = ServicioCorreo.MHTServer & DB.getLXParam("SMTPAVISOGRAL", campoMAILMAN) & "?LID=" & rs("LID").Value
                Else
                    If InStr(.url.ToUpper, "HTTP") = 0 Then
                        If InStr(.url.ToUpper, ".ASP") = 0 Then
                            .url = ServicioCorreo.MHTServer & DB.getLXParam("SMTPAVISOGRAL", campoMAILMAN) & "?LID=" & rs("LID").Value
                        Else
                            .url = ServicioCorreo.MHTServer & .url
                        End If
                    End If
                End If
                .Adjuntos = rs("LADJUNTOS").Value
                Try
                    ServicioCorreo.EscribirEnConsola("Enviar " & rs("LNUMREG").Value)
                    resultado = .enviaMail()
                    If (resultado = "OK") Then
                        fSql = " UPDATE RMAILX SET  "
                        fSql &= " LUPDDAT = NOW() , "
                        fSql &= " LENVIADO = 1,  "
                        fSql &= " LMSGERR = '" & Left(System.Net.Dns.GetHostName() & vbCr & "OK" & vbCr & .getEnviadoA(), 7000) & "' "
                        fSql &= " WHERE LNUMREG = " & rs("LNUMREG").Value
                        DB.ExecuteSQL(fSql)
                        ServicioCorreo.EscribirEnConsola(" Enviado")
                    End If
                    DB.ExecuteSQL(fSql)
                Catch ex As Exception
                    ServicioCorreo.EscribirEnConsola(ex.Message)
                    With New RegistroEvento
                        .IdMessage = rs("LNUMREG").Value
                        .Message = ex.Message
                        .EjecucionConsola = ServicioCorreo.EjecucionConsola
                        .RegistrarEvento()
                    End With
                End Try
            End With
            rs.MoveNext()
        Loop
        rs.Close()

    End Sub
    Function AvisoLog() As Boolean
        Dim fSql As String
        Dim rs As ADODB.Recordset
        Dim resultado As String = ""
        Dim Correo As New clsMHT With {
            .ServicioCorreo = ServicioCorreo
        }
        DB.gsKeyWords = "AvisoLog"

        fSql = " SELECT * FROM  RFLOG WHERE ALERT = 1 "
        rs = DB.ExecuteSQL(fSql)
        Do While Not rs.EOF
            With Correo
                .IdMsg = "0"
                .Inicializa()
                .Modulo = "PGMERR"
                .Asunto = rs("PROGRAMA").Value
                .De = "Error en Programa"
                .Para = "BSS" & IIf(rs("ALERTTO").Value <> "", "," & rs("ALERTTO").Value, "")
                .url = ServicioCorreo.MHTServer & DB.getLXParam("SMTPAVISOERROR", "") & "?ID=" & rs("ID").Value
                resultado = .enviaMail()
                If (resultado = "OK") Then
                    fSql = " UPDATE RFLOG SET ALERT = 2"
                    fSql &= " WHERE ID = " & rs("ID").Value
                Else
                    fSql = " UPDATE RFLOG SET ALERT = -1"
                    fSql &= " WHERE ID = " & rs("ID").Value
                End If
                DB.ExecuteSQL(fSql)
            End With
            rs.MoveNext()
        Loop
        rs.Close()
        Return True

    End Function

    Function AbreConexion(ModuloActual As String) As Boolean
        Dim aPrm() As String
        Dim item As String
        Dim aValor() As String
        sParamLocal = My.Settings.LOCAL
        aPrm = sParamLocal.Split(";")
        For Each item In aPrm
            aValor = item.Split("=")
            If UBound(aValor) = 1 Then
                Select Case UCase(Trim(aValor(0)))
                    Case "CAMPO_MAILMAN"
                        campoMAILMAN = Trim(aValor(1))
                    Case "LID"
                        unLID = Trim(aValor(1))
                    Case "SERVER_MAILMAN"
                        serverMAILMAN = Trim(aValor(1))
                End Select
            End If
        Next
        DB = New clsConexion
        DB.gsDatasource = My.Settings.AS400
        DB.gsLib = My.Settings.AMBIENTE
        DB.ModuloActual = ModuloActual
        Return DB.Conexion()
    End Function

    Function RevisaArchivo(Archivo As String) As Boolean
        With New clsMHT
            .Inicializa()
            .ServicioCorreo = ServicioCorreo
            Return .avisoDesborde(Archivo)
        End With

    End Function

    Protected Overrides Sub OnStop()
        ' Agregue el código aquí para realizar cualquier anulación necesaria para detener el servicio.
        m_timer.Stop()
    End Sub


 
End Class
