﻿Imports Chilkat

Public Class ServicioCorreo
    Friend Property DB As clsConexion
    Friend Property SmtpHost As String
    Friend Property SmtpPort As String
    Friend Property StartTLS As String
    Friend Property SmtpUsername As String
    Friend Property FromAddress As String
    Friend Property SmtpPassword As String
    Friend Property MHTServer As String
    Friend Property serverMAILMAN As String
    Friend Property EjecucionConsola As Boolean = False
    Friend Property Ejecutar As Boolean
    Friend Property UsarCredenciales As Boolean
    Friend Property Version As String
    Friend Property IdMensaje As String

    Sub SetParametros()
        SetVersion()
        If Version <> DB.LookUp("CCSDSC", "ZCC", "CCTABL = 'RFVBVER' AND CCCODE = 'BSSCorreoMHT'") Then
            Console.WriteLine("Version Incorrecta del Programa")
            Ejecutar = False
        Else
            Ejecutar = True
        End If
        If DB.getLXParam("SMTPAUTH", campoMAILMAN) <> "DIRECTO" Then
            UsarCredenciales = True
            SmtpUsername = DB.getLXParam("SMTPUSERNAME", campoMAILMAN)
            SmtpPassword = DB.getLXParam("SMTPPASSWORD", campoMAILMAN)
        Else
            UsarCredenciales = False
        End If
        SmtpHost = DB.getLXParam("SMTPHOST", campoMAILMAN)
        StartTLS = DB.getLXParam("SMTP_TLS", campoMAILMAN) = "1"  'True
        SmtpPort = DB.getLXParam("SMTP_PUERTO", campoMAILMAN)  '587
        MHTServer = DB.getLXParam("SMTPMHTSERVER", campoMAILMAN)
        FromAddress = DB.getLXParam("SMTPUSERMAIL", "CCNOT1") '"admin.logistica@brinsa.com.co"

    End Sub
    Private Sub SetVersion()
        Dim aVersion() As String
        aVersion = Split(System.Reflection.Assembly.GetExecutingAssembly.FullName, ",")
        For Each sVersion In aVersion
            If InStr(UCase(sVersion), "VERSION") > 0 Then
                Version = sVersion.Trim()
                Exit For
            End If
        Next
    End Sub
    Sub EscribirEnConsola(Texto As String)
        If EjecucionConsola Then
            Console.WriteLine(Texto)
        End If
    End Sub

    ReadOnly Property VerParametros As String
        Get
            Dim s As String = ""
            s &= vbCrLf & " SmtpHost       = " & SmtpHost
            s &= vbCrLf & " SmtpPort       = " & SmtpPort
            s &= vbCrLf & " StartTLS       = " & StartTLS
            s &= vbCrLf & " SmtpUsername   = " & SmtpUsername
            s &= vbCrLf & " SmtpPassword   = " & SmtpPassword
            s &= vbCrLf & " MHTServer      = " & MHTServer
            s &= vbCrLf & " serverMAILMAN  = " & serverMAILMAN
            s &= vbCrLf & " Version        = " & Version
            Return s
        End Get
    End Property
End Class
