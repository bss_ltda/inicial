﻿Public Class clsMHT

    Friend Property ServicioCorreo As ServicioCorreo
    Public Modulo As String
    Public IdMsg As String
    Public Adjuntos As String
    Public Asunto As String
    Public Cuerpo As String
    Public De As String
    Public Para As String
    Public CC As String
    Public BCC As String
    Public url As String
    Public sAmbiente As String

    Private campoMail As String = "UEMAIL"
    Private mht As Chilkat.Mht
    Private mailman As Chilkat.MailMan
    Private email As Chilkat.Email
    Private EnviadoA As String

    Public Sub New()
        Inicializa()
    End Sub

    Function enviaMail() As String
        Dim resultado As String = ""
        'Dim destBCC As String        

        mht = New Chilkat.Mht
        mailman = New Chilkat.MailMan
        email = New Chilkat.Email

        mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ") 'SMANRIMAILQ_ZEKrtWHSpOpZ
        mht.UnlockComponent("SMANRIMHT_dEnsKQ0g3Rue")

        mailman.SmtpHost = ServicioCorreo.SmtpHost
        mailman.StartTLS = ServicioCorreo.StartTLS
        mailman.SmtpPort = ServicioCorreo.SmtpPort
        If ServicioCorreo.UsarCredenciales Then
            mailman.SmtpUsername = ServicioCorreo.SmtpUsername
            mailman.SmtpPassword = ServicioCorreo.SmtpPassword
        End If

        resultado = correoMHT()
        If resultado = "OK" Then
            If Adjuntos.Trim <> "" Then
                If Not Adjuntar() Then
                    Return "Error adjuntando archivos"
                End If
            End If
            email.FromName = IIf(De <> "", De, "Alertas BRINSA")
            email.FromAddress = ServicioCorreo.FromAddress
            Destinatarios("TO", Para)
            If CC.Trim() <> "" Then
                Destinatarios("CC", CC)
            End If
            If BCC.Trim() <> "" Then
                Destinatarios("BCC", BCC)
            End If
            email.Subject = Asunto
            If mailman.SendEmail(email) Then
                resultado = "OK"
            Else
                ServicioCorreo.EscribirEnConsola(mailman.LastErrorText)
                With New RegistroEvento
                    .IdMessage = IdMsg
                    .Message = mailman.LastErrorHtml
                    .EjecucionConsola = ServicioCorreo.EjecucionConsola
                    .RegistrarEvento()
                End With
                resultado = "Error en Envio"
            End If
            mailman.CloseSmtpConnection()
        End If
        Return resultado

    End Function

    Function enviaMailSimple() As Boolean
        Dim success As Boolean

        mht = New Chilkat.Mht
        mailman = New Chilkat.MailMan
        email = New Chilkat.Email


        mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
        mailman.SmtpHost = DB.getLXParam("SMTPHOST", campoMAILMAN)
        If DB.getLXParam("SMTPAUTH", campoMAILMAN) <> "DIRECTO" Then
            mailman.SmtpUsername = DB.getLXParam("SMTPUSERNAME", campoMAILMAN)
            mailman.SmtpPassword = DB.getLXParam("SMTPPASSWORD", campoMAILMAN)
        End If

        email.Subject = Asunto
        email.SetHtmlBody("<html><body>" & Cuerpo & "</body></html>")
        email.From = De
        Destinatarios("TO", Para)

        success = mailman.SendEmail(email)
        mailman.CloseSmtpConnection()

        Return success

    End Function

    Sub Destinatarios(Tipo As String, ByVal Correos As String)
        Dim aDest() As String
        Dim rs As New ADODB.Recordset
        Dim fSql As String

        If Correos.Trim() = "" Then
            Exit Sub
        End If

        Correos = Correos.Replace(vbCrLf, vbLf)
        Correos = Correos.Replace(vbCr, vbLf)
        Correos = Correos.Replace(vbLf & vbLf, vbLf)
        Correos = Correos.Replace(vbTab, ",")
        Correos = Correos.Replace(vbLf, ",")
        Correos = Correos.Replace(";", ",")

        aDest = Split(Correos, ",")

        For Each dest In aDest
            If dest.Trim() <> "" Then
                If InStr(dest, "@") = 0 Then
                    If InStr(dest.ToUpper.Trim, "SELECT ") = 0 Then
                        fSql = "SELECT UNOM, " & campoMail & " AS UEMAIL FROM " & sAmbiente & "RCAU WHERE UUSR = '" & dest.ToUpper.Trim & "'"
                    Else
                        fSql = dest.ToUpper.Trim
                    End If
                    rs = DB.ExecuteSQL(fSql)
                    If Not rs.EOF Then
                        Select Case Tipo
                            Case "TO"
                                email.AddTo(rs("UNOM").Value.ToString, rs("UEMAIL").Value.ToString)
                                EnviadoA &= vbCr & "To: " & rs("UEMAIL").Value
                            Case "CC"
                                email.AddCC(rs("UNOM").Value, rs("UEMAIL").Value)
                                EnviadoA &= vbCr & "CC: " & rs("UEMAIL").Value
                            Case "BCC"
                                email.AddBcc(rs("UNOM").Value.ToString, rs("UEMAIL").Value.ToString)
                                EnviadoA &= vbCr & "BCC: " & rs("UEMAIL").Value
                        End Select
                    End If
                    rs.Close()
                Else
                    Select Case Tipo
                        Case "TO"
                            email.AddTo("", dest)
                            EnviadoA &= vbCr & "To: " & dest
                        Case "CC"
                            email.AddCC("", dest)
                            EnviadoA &= vbCr & "CC: " & dest
                        Case "BCC"
                            email.AddBcc("", dest)
                            EnviadoA &= vbCr & "BCC: " & dest
                    End Select
                End If
            End If
        Next

    End Sub

    Function Adjuntar() As Boolean
        Dim aAdjuntos() As String = Split(Adjuntos, ",")
        For Each adjunto In aAdjuntos
            If Dir(adjunto) <> "" Then
                If email.AddFileAttachment(adjunto) = vbNullString Then
                    ServicioCorreo.EscribirEnConsola(email.LastErrorText)
                    With New RegistroEvento
                        .IdMessage = IdMsg
                        .Message = email.LastErrorHtml
                        .EjecucionConsola = ServicioCorreo.EjecucionConsola
                        .RegistrarEvento()
                    End With
                    Return False
                End If
            Else
                With New RegistroEvento
                    .IdMessage = IdMsg
                    .Message = "No encontró " & adjunto
                    .EjecucionConsola = ServicioCorreo.EjecucionConsola
                    .RegistrarEvento()
                End With
                Return False
            End If
        Next
        Return True

    End Function

    'correoMHT()	
    Function correoMHT() As String
        Dim emlStr As String

        mht.UseCids = 1
        emlStr = mht.GetEML(url)
        If (emlStr = vbNullString) Then
            ServicioCorreo.EscribirEnConsola(mht.LastErrorText)
            With New RegistroEvento
                .IdMessage = IdMsg
                .Message = mht.LastErrorHtml
                .EjecucionConsola = ServicioCorreo.EjecucionConsola
                .RegistrarEvento()
            End With
            Return "Error en MHT."
        End If
        If Not (email.SetFromMimeText(emlStr)) Then
            ServicioCorreo.EscribirEnConsola(mailman.LastErrorText)
            With New RegistroEvento
                .IdMessage = IdMsg
                .Message = mailman.LastErrorHtml
                .EjecucionConsola = ServicioCorreo.EjecucionConsola
                .RegistrarEvento()
            End With
            Return "Error en MimeText"
        End If
        Return "OK"



    End Function

    Public Function getBCC() As String
        Dim rs As New ADODB.Recordset
        Dim resultado As String = ""

        campoMail = "UEMAIL"
        If DB.OpenRS(rs, "SELECT CCNOT1 FROM RFPARAM WHERE CCTABL='CORREOMHT' AND UPPER(CCCODE) = UPPER('" & Modulo & "') AND CCUDC2 = 1") Then
            resultado = rs("CCNOT1").Value
        End If
        rs.Close()
        Return resultado.Trim
    End Function
    Function avisoDesborde(Archivo As String) As Boolean
        Dim rs As New ADODB.Recordset
        Dim fSql As String
        If ServicioCorreo.EjecucionConsola Then
            ServicioCorreo.EscribirEnConsola("Aviso de Desborde")
        End If
        fSql = " UPDATE RFPARAM SET  "
        fSql = fSql & "  CCMNDT = NOW() "
        fSql = fSql & " WHERE CCTABL = 'LXLONG'  AND CCCODE = '" & Archivo & "' "
        DB.ExecuteSQL(fSql)
        Select Case Archivo
            Case "RMAILX"
                Dim MaximoPend As Integer = DB.LookUp("CCUDC4", "RFPARAM", "CCTABL = 'LXLONG' AND CCCODE = '" & Archivo & "'")
                Dim PendEnviar As Integer = DB.LookUp("IFNULL( COUNT(*), 0 )", "RMAILX", "LENVIADO = 0 AND LMOD NOT IN ( SELECT CCCODE2 FROM RFPARAM WHERE CCTABL = 'LXLONG'  AND CCCODE = 'CORREOMASIVO' )")
                fSql = "UPDATE RFPARAM SET CCUDC1 = " & IIf(PendEnviar > MaximoPend, 1, 0)
                fSql &= " WHERE CCTABL = 'LXLONG'   AND CCCODE = '" & Archivo & "'  "
                DB.ExecuteSQL(fSql)
            Case "RFLOG"
                fSql = "UPDATE RFPARAM SET CCUDC1 = ( SELECT CASE WHEN COUNT(*) > CCUDC4 THEN 1 ELSE 0 END FROM RFLOG WHERE ALERT = 1 )"
                fSql &= " WHERE CCTABL = 'LXLONG' AND CCCODE = '" & Archivo & "'  "
                DB.ExecuteSQL(fSql)
        End Select

        fSql = " SELECT CCCODE2 AS EMPRESA, CCCODE3 AS SOPORTE, HOUR(CCUDTS) AS HORA  "
        fSql = fSql & " FROM RFPARAM  "
        fSql = fSql & " WHERE CCTABL = 'LXLONG'   AND CCCODE = '" & Archivo & "'  "
        fSql = fSql & " AND CCUDC1 = 1"
        If Not DB.OpenRS(rs, fSql) Then
            rs.Close()
            Return True
        End If

        If rs("HORA").Value <> Hour(Now()) Then
            fSql = "UPDATE RFPARAM Set CCNOT1 ='Alerta', CCUDTS = NOW() WHERE CCTABL = 'LXLONG' AND CCCODE = '" & Archivo & "'"
            DB.ExecuteSQL(fSql)

            DB.gsKeyWords &= " avisoDesborde (1) " & Archivo & " " & DB.Estado()

            Inicializa()

            Modulo = Archivo
            Asunto = "Desbordamiento " & Archivo
            De = String.Format("Correo {0}<correo{0}@bssltda.com>", rs("EMPRESA").Value)
            Para = rs("SOPORTE").Value
            Cuerpo = "Desbordamiento de archivo " & Archivo & vbCrLf
            Cuerpo &= "<br>Reviso y devuelva el parametro con<br>UPDATE RFPARAM SET CCUDC1 = 0 WHERE CCTABL = 'LXLONG'  AND CCCODE = '" & Archivo & "'"
            If enviaMailSimple() Then
                fSql = "UPDATE RFPARAM SET CCNOT1 = 'Desbordamiento de archivo' WHERE CCTABL = 'LXLONG' AND CCCODE = '" & Archivo & "'"
                DB.ExecuteSQL(fSql)
            Else
                fSql = "UPDATE RFPARAM SET CCNOT1 = 'No pudo enviar aviso' WHERE CCTABL = 'LXLONG' AND CCCODE = '" & Archivo & "'"
                DB.ExecuteSQL(fSql)
            End If
        End If
        rs.Close()


        Return False

    End Function

    Function getEnviadoA() As String
        Return EnviadoA
    End Function

    Sub Inicializa()
        Modulo = ""
        IdMsg = ""
        Adjuntos = ""
        Asunto = ""
        De = ""
        Para = ""
        CC = ""
        BCC = ""
        url = ""
        sAmbiente = ""
        EnviadoA = ""
    End Sub


End Class
