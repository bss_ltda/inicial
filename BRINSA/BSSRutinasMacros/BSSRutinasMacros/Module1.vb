﻿Imports System.Runtime.InteropServices
Imports Microsoft.Office.Interop.Excel

Module Module1
    Dim Workbook As Workbook
    Dim Excel As Application

    Sub Main()
        Try
            Console.WriteLine("Inicio")
            Console.WriteLine("Modificando seguridad")

            ModifyExcelSecuritySettings()

            Excel = New Application With {
                .DisplayAlerts = False,
                .ScreenUpdating = False,
                .Visible = False
            }

            Console.WriteLine("Abriendo Excel " & My.Settings.RutaArchivo & "...")

            Workbook = Excel.Workbooks.Open(My.Settings.RutaArchivo)

            Console.WriteLine("Refrescando")

            Workbook.RefreshAll()

            Console.WriteLine("Guardando... ")

            Workbook.Save()

            Console.WriteLine("Cerrando")

            QuitExcel()

        Catch ex As Exception
            Console.WriteLine(ex.ToString())
            Console.ReadLine()

        End Try

    End Sub

    Private Sub QuitExcel()
        If Workbook IsNot Nothing Then
            Workbook.Close(False)

            Marshal.ReleaseComObject(Workbook)
        End If

        If Excel IsNot Nothing Then
            Excel.Quit()

            Marshal.ReleaseComObject(Excel)
        End If
    End Sub

    Private Sub ModifyExcelSecuritySettings()
        Using key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("Software\Microsoft\Office\16.0\Excel\Security", True)

            If key IsNot Nothing Then

                If CInt(key.GetValue("AccessVBOM", 0)) <> 1 Then
                    key.SetValue("AccessVBOM", 1)
                End If

                key.Close()
            End If
        End Using
    End Sub

End Module
