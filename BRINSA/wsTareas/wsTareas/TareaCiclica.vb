﻿Public Class TareaCiclica
    Property TareaCiclica As Integer
    Sub TareasFijas(qInterface As String, qTipo As String)
        Dim rs As ADODB.Recordset
        Dim fSql, vSql As String
        Dim Ahora As Date
        Dim ProximaCalc As Date
        Dim ProximaProg As Date
        Dim bCreaRegistro As Boolean = False
        Dim Diferencia As Double
        fSql = " Select TNUMREG, STSERR, STS1, STS4, TFPROXIMA, NOW() As AHORA "
        Select Case qInterface
            Case "WMS Interface - CENDIS"
                fSql &= ", NOW() + " & DB.getLXParam("WMS_FRECUENCIA", "CCNOT1") & " As PROXIMA "
            Case "WMA Interface - ALMACEN"
                fSql &= ", NOW() + " & DB.getLXParam("WMA_FRECUENCIA", "CCNOT1") & " As PROXIMA "
            Case "REPORTE CSV"
                fSql &= ", NOW() + " & DB.getLXParam("MMM_CSV_FRECUEN", "CCNOT1") & " As PROXIMA "
            Case "REENVIA PENDIENTES"
                fSql &= ", NOW() + " & DB.getLXParam("MMM_PEND_FRECUEN", "CCNOT1") & " As PROXIMA "
            Case "TPS"
                fSql &= ", NOW() + " & DB.getLXParam("MMM_TPS_FRECUEN", "CCNOT1") & " As PROXIMA "
        End Select
        fSql &= " FROM RFTASK "
        fSql &= " WHERE TAPP = 'WMS' "
        Select Case qTipo
            Case "WMS"
                fSql &= " And TCATEG = '" & qInterface & "' "
                fSql &= " AND TSUBCAT = 'ACTUAL' "
            Case "MMM"
                fSql &= " And TCATEG = 'MMM' "
                fSql &= " And TSUBCAT = '" & qInterface & "' "
        End Select
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            Ahora = rs("AHORA").Value
            ProximaCalc = rs("PROXIMA").Value
            ProximaProg = rs("TFPROXIMA").Value
            Diferencia = Math.Abs(DateDiff(DateInterval.Second, Ahora, ProximaProg) / (DateDiff(DateInterval.Second, Ahora, ProximaCalc) + 0.00001))
            If Diferencia > 2 Then
                If rs("STSERR").Value = 1 Then
                    fSql = "UPDATE RFTASK SET"
                    fSql &= " THLD = 1, STS4 = 1, STS1 = 1 "
                    Select Case qTipo
                        Case "WMS"
                            fSql &= ", TSUBCAT = 'ANTERIOR'"
                        Case "MMM"
                            fSql &= ", TSUBCAT = 'MMM-ANTERIOR'"
                    End Select
                    fSql &= " WHERE TNUMREG = " & rs("TNUMREG").Value
                    DB.ExecuteSQL(fSql)
                    bCreaRegistro = True
                ElseIf rs("STS4").Value = 1 Then
                    fSql = "UPDATE RFTASK SET"
                    fSql &= "  STS1 = 0, STS4 = 0 "
                    Select Case qInterface
                        Case "WMS INTERFACE - CENDIS"
                            fSql &= ", TFPROXIMA = NOW() + " & DB.getLXParam("WMS_FRECUENCIA", "CCNOT1")
                        Case "WMA INTERFACE - ALMACEN"
                            fSql &= ", TFPROXIMA = NOW() + " & DB.getLXParam("WMA_FRECUENCIA", "CCNOT1")
                        Case "REPORTE CSV"
                            fSql &= ", TFPROXIMA = NOW() + " & DB.getLXParam("MMM_CSV_FRECUEN", "CCNOT1")
                        Case "REENVIA PENDIENTES"
                            fSql &= ", TFPROXIMA = NOW() + " & DB.getLXParam("MMM_PEND_FRECUEN", "CCNOT1")
                        Case "TPS"
                            fSql &= ", NOW() + " & DB.getLXParam("MMM_TPS_FRECUEN", "CCNOT1") & " AS PROXIMA "
                    End Select
                    fSql &= " WHERE TNUMREG = " & rs("TNUMREG").Value
                    DB.ExecuteSQL(fSql)
                ElseIf rs("STS4").Value = 0 Then
                    fSql = "UPDATE RFTASK SET"
                    fSql &= "  STS1 = 0, STS4 = 0 "
                    fSql &= ", TFPROXIMA = NOW() + 10 SECONDS "
                    fSql &= " WHERE TNUMREG = " & rs("TNUMREG").Value
                    DB.ExecuteSQL(fSql)
                End If
            End If
        Else
            bCreaRegistro = True
        End If

        If bCreaRegistro Then
            fSql = " INSERT INTO RFTASK( "
            vSql = " VALUES ( "
            fSql &= " TAPP      , " : vSql &= "'WMS', "     '//15A   

            Select Case qTipo
                Case "WMS"
                    fSql &= " TCATEG    , " : vSql &= "'" & qInterface & "', "     '//25A   Categoria
                    fSql &= " TSUBCAT   , " : vSql &= "'ACTUAL', "     '//20A   Categoria
                Case "MMM"
                    fSql &= " TCATEG    , " : vSql &= "'MMM', "     '//20A   Categoria
                    fSql &= " TSUBCAT   , " : vSql &= "'" & qInterface & "', "     '//25A   Categoria
            End Select

            fSql &= " TTIPO     , " : vSql &= "'Rutina', "     '//22A   Tipo
            fSql &= " TPGM      , " : vSql &= "'" & DB.getLXParam("RUTINAS_WMS") & "', "     '//502A  Programa
            fSql &= " STS1      , " : vSql &= "1, "
            fSql &= " TCICLICA  , " : vSql &= "1, "
            fSql &= " TCRTUSR   ) " : vSql &= "'SISTEMA' ) "     '//10A   Usuario
            DB.ExecuteSQL(fSql & vSql)

            rs.Requery()
            TareaCiclica = rs("TNUMREG").Value

            fSql = "UPDATE RFTASK SET"
            fSql &= "  STS1 = 0, STS4 = 0 "
            fSql &= ", TFPROXIMA = NOW() + 10 SECONDS "
            Select Case qInterface
                Case "WMS INTERFACE - CENDIS"
                    fSql &= ", TFRECUENTXT = '" & DB.getLXParam("WMS_FRECUENCIA", "CCNOT1") & "'"
                Case "WMA INTERFACE - ALMACEN"
                    fSql &= ", TFRECUENTXT = '" & DB.getLXParam("WMA_FRECUENCIA", "CCNOT1") & "'"
                Case "REPORTE CSV"
                    fSql &= ", TFRECUENTXT = '" & DB.getLXParam("MMM_CSV_FRECUEN", "CCNOT1") & "'"
                Case "REENVIA PENDIENTES"
                    fSql &= ", TFRECUENTXT = '" & DB.getLXParam("MMM_PEND_FRECUEN", "CCNOT1") & "'"
                Case "TPS"
                    fSql &= ", TFRECUENTXT = '" & DB.getLXParam("MMM_TPS_FRECUEN", "CCNOT1") & "'"
            End Select
            fSql &= " WHERE TNUMREG = " & TareaCiclica
            DB.ExecuteSQL(fSql)
        End If
        rs.Close()

    End Sub

End Class
