﻿Public Class Conexion
    Public Property LastSQL As String
    Public Property ConnectionString As String
    Public Property RegistrosActualizados As Integer
    Public Property TareaNumero As Integer

    Dim DB As ADODB.Connection

    Public Sub Open()
        DB = New ADODB.Connection
        DB.Open(ConnectionString)
    End Sub

    Public Function ExecuteSQL(ByVal sql As String) As ADODB.Recordset
        LastSQL = sql
        ExecuteSQL = DB.Execute(sql, RegistrosActualizados)
    End Function

    Function getLXParam(prm As String) As String
        Return getLXParam(prm, "")
    End Function

    Function getLXParam(prm As String, Campo As String) As String
        Dim rs As New ADODB.Recordset
        Dim resultado As String = ""
        Dim fSql As String

        If Campo = "" Then
            Campo = "CCNOT1"
        End If
        fSql = "SELECT " & Campo & " AS DATO FROM RFPARAM WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER('" & prm & "')"
        rs.Open(fSql, Me.DB)
        If Not rs.EOF Then
            resultado = rs("DATO").Value
        End If
        rs.Close()
        Return resultado.Trim
    End Function
    Function LookUp(ColumnName As String, TableName As String, Where As String) As String
        Dim rs As New ADODB.Recordset
        Dim fSql As String
        If Left(ColumnName.Trim, 6) = "SELECT" Then
            fSql = ColumnName
        Else
            fSql = "SELECT IFNULL(" & ColumnName & ", '' ) FROM " & TableName & " WHERE " & Where
        End If
        rs.Open(fSql, DB)
        If Not rs.EOF Then
            LookUp = CStr(rs(0).Value)
        Else
            LookUp = ""
        End If
        rs.Close()
    End Function

    Function Estado() As Integer
        Return DB.State
    End Function

    Friend Function IsActiva() As Boolean
        Return DB.State = 1
    End Function

    Friend Sub Close()
        DB.Close()
    End Sub
End Class
