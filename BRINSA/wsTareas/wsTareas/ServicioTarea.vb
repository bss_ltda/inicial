﻿Friend Class ServicioTarea
    Public Property Servidor As String
    Public Property EjecutableRutina As String
    Public Property DiaActual As Int16
    Public Property PrimeraEjecucion As Boolean
    Public Property TareasPendientes As Integer
    Public Property TareaNumero As Integer
    Public Property Accion As String
    Public Property Ejecutable As String
    Public Property Parametros As String
    Public Property AgregarParametroTarea As Boolean
    Public Property MensajeTarea As String
    Public Property EstadoTarea As Integer
    Public Property CicloTerminado As Boolean = False
    Public Property Probando As Boolean = False


    Dim fSql As String
    Public Sub SetEjecutableRutina()
        If EjecutableRutina = "" Then
            EjecutableRutina = DB.LookUp("CCNOT1", "RFPARAM", "CCTABL = 'LXLONG' AND  CCCODE = 'RUTINAS_" & Servidor & "'")
            If Not EjecutableRutina.Contains("""") Then
                EjecutableRutina = """" & EjecutableRutina & """"
            End If
        End If
    End Sub

    Public Sub SetEjecutable(Ejecutable As String)
        Me.Ejecutable = Ejecutable
    End Sub
    Friend Sub SetContarTareasPendientes()
        Dim rs As ADODB.Recordset
        fSql = " Select IFNULL( COUNT(*), 0) TOT_TAREAS FROM  RFTASK "
        fSql &= " WHERE "
        fSql &= "     TAPP = '" & Servidor & "'"
        fSql &= " AND STS1 = 0 "
        fSql &= " AND THLD = 0 "
        fSql &= " AND TFPROXIMA <= NOW() "
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            TareasPendientes = rs("TOT_TAREAS").Value
        End If
        rs.Close()
    End Sub
    Friend Function GetParametros() As String
        If AgregarParametroTarea Then
            Return Parametros & " TAREA=" & TareaNumero
        Else
            Return Parametros
        End If
    End Function
    Friend Function GetComandoCompleto() As String
        Dim Comando As String = Ejecutable & " " & Parametros.Replace("|", " ")
        If AgregarParametroTarea Then
            Return Comando & " TAREA=" & TareaNumero
        Else
            Return Comando
        End If
    End Function
    Friend Function GetComando() As String
        Return Ejecutable
    End Function

    Public Sub ActualizarComando(Comando As String)
        Dim fSql As String
        fSql = " UPDATE RFTASK SET "
        fSql &= "  STS1 = 1"
        fSql &= ", TUPDDAT = NOW()  "
        fSql &= ", TCMD = '" & Comando.Replace("'", "''") & "'"
        fSql &= " WHERE TNUMREG = " & TareaNumero
        DB.ExecuteSQL(fSql)
    End Sub

    Sub DepurarTareasAntiguas()
        If DiaActual <> Day(Now()) Then
            DiaActual = Day(Now())
            fSql = " DELETE FROM RFTASK  "
            fSql &= " WHERE "
            fSql &= "     TAPP = '" & Servidor & "' "
            fSql &= " AND STS1 <> 0 "
            fSql &= " AND THLD = 0"
            fSql &= " And TCICLICA = 0  "
            fSql &= " And DATE(TFPROXIMA) <= DATE(NOW()) - 60 DAYS"
            DB.ExecuteSQL(fSql)
            fSql = " DELETE  "
            fSql &= " FROM RFLOG  "
            fSql &= " WHERE DATE(FECHA) <= DATE(NOW()) - 60 DAYS"
            DB.ExecuteSQL(fSql)
        End If
    End Sub

End Class
