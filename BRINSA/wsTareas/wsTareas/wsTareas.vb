﻿Imports System.IO
Imports System.Threading

Public Class wsTareas

    Private WithEvents tim As New Timers.Timer(5000)
    Private ServicioTarea As ServicioTarea

    Dim bPrimerVez As Boolean = True
    Dim fecHoraMM1 As Date
    Dim fecHoraMM2 As Date
    Dim regA As Integer
    Dim lastSQL As String
    Dim tareaNumero As String
    Dim qVersion As String

    Dim bEnPruebas As Boolean
    Dim tareasPendientes As Int16 = 0
    Dim idLog As String = ""
    Dim bEjecutando As Boolean = False
    Protected Overrides Sub OnStart(ByVal args() As String)
        ServicioTarea = New ServicioTarea With {
            .DiaActual = 0,
            .Servidor = My.Settings.TAPP
            }
        tim.Enabled = True
    End Sub
    Protected Overrides Sub OnStop()
        ' Agregue el código aquí para realizar cualquier anulación necesaria para detener el servicio.
    End Sub
    Private Sub tim_Elapsed(sender As Object, e As System.Timers.ElapsedEventArgs) Handles tim.Elapsed
        If Not ServicioTarea.CicloTerminado Then
            ServicioTarea.CicloTerminado = True
            Try
                RevisarTareas()
            Catch ex As Exception
                With New ReporteDeError
                    .ServicioTarea = ServicioTarea
                    .ConnectionString = My.Settings.CENTRAL
                    .LastSQL = DB.LastSQL
                    .WrtSqlError(ex.Message)
                End With
            End Try
            ServicioTarea.CicloTerminado = False
        End If
    End Sub
    Public Sub EjecutarComoConsola()
        ServicioTarea = New ServicioTarea With {
            .DiaActual = 0,
            .Servidor = My.Settings.TAPP,
            .Probando = True
            }
        ServicioTarea.CicloTerminado = True
        Try
            RevisarTareas()
        Catch ex As Exception
            With New ReporteDeError
                .ServicioTarea = ServicioTarea
                .ConnectionString = My.Settings.CENTRAL
                .LastSQL = DB.LastSQL
                .WrtSqlError(ex.Message)
            End With
        End Try
        ServicioTarea.CicloTerminado = False
    End Sub
    Public Sub RevisarTareas()
        Me.bEnPruebas = bEnPruebas
        DB = New Conexion With {
            .ConnectionString = My.Settings.SERVIDOR
        }
        DB.Open()
        If Not DB.IsActiva() Then
            Return
        End If
        ServicioTarea.SetEjecutableRutina()
        With New TareaProgramada
            .ServicioTarea = ServicioTarea
            .RevisarTareasProgramadas()
        End With
        DB.Close()
    End Sub
    Sub SetExe(exe As String, ByRef aExe() As String)
        Dim i As Int16

        aExe(0) = ""
        aExe(1) = ""

        Dim a As Int16 = InStr(exe.ToUpper, ".EXE")
        Dim b As Int16 = InStr(exe.ToUpper, ".BAT")

        If a > 0 Then
            i = InStr(exe.Substring(a), " ")
            aExe(0) = exe.Substring(0, a + i)
            aExe(1) = exe.Substring(a + i)
        ElseIf b > 0 Then
            i = InStr(exe.Substring(b), " ")
            aExe(0) = exe.Substring(0, b + i).Trim
            aExe(1) = exe.Substring(b + i).Trim
        Else
            'error
        End If
        aExe(0) = aExe(0).Replace("""", "")
        If UCase(aExe(0)).Trim = UCase("ImprimeDocumentos.exe") Then
            aExe(0) = My.Application.Info.DirectoryPath & "\" & aExe(0)
        End If

    End Sub

    Sub Hilos()
        Dim wms = New Thread(Sub()
                                 Do
                                     Shell("notepad", AppWinStyle.NormalFocus, True, -1)
                                     Thread.Sleep(5000)
                                     Debug.Print(Now())
                                 Loop
                             End Sub)
        'wms.IsBackground = True
        wms.Start()

        Dim wma = New Thread(Sub()
                                 Do
                                     Shell("calc", AppWinStyle.NormalFocus, True, -1)
                                     Thread.Sleep(5000)
                                     Debug.Print(Now())
                                 Loop
                             End Sub)
        'wma.IsBackground = True
        wma.Start()

        Debug.Print("")

    End Sub

End Class

