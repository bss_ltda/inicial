﻿Friend Class ReporteDeError
    Friend Property ServicioTarea As ServicioTarea
    Friend Property ConnectionString As String
    Friend Property LastSQL As String
    Sub WrtSqlError(Descrip As String)
        Dim conn As New ADODB.Connection
        Dim fSql As String
        Dim vSql As String
        Try
            conn.Open(ConnectionString)
            fSql = " UPDATE RFTASK SET "
            fSql &= "  TLXMSG = 'ERROR'  "
            fSql &= ", STSERR = 1  "
            fSql &= ", STS1 = 1  "
            fSql &= ", TUPDDAT = NOW() "
            fSql &= ", TMSG = '" & Descrip & "'"
            fSql &= " WHERE TNUMREG = " & ServicioTarea.TareaNumero
            conn.Execute(fSql)

            fSql = " INSERT INTO RFLOG( "
            vSql = " VALUES ( "
            fSql &= " LKEY      , " : vSql &= "'TAREAS." & My.Settings.TAPP & "', "
            fSql &= " TAREA     , " : vSql &= " " & ServicioTarea.TareaNumero & ", "
            fSql &= " USUARIO   , " : vSql &= "'" & System.Net.Dns.GetHostName() & "', "      '//502A
            fSql &= " PROGRAMA  , " : vSql &= "'" & System.Reflection.Assembly.GetExecutingAssembly.FullName.Replace("'", "''") & "', "      '//502A
            fSql &= " ALERT     , " : vSql &= "1, "      '//1P0
            fSql &= " EVENTO    , " : vSql &= "'" & Descrip.Replace("'", "''") & "', "      '//20002A
            fSql &= " TXTSQL    ) " : vSql &= "'" & LastSQL.Replace("'", "''") & "' )"
            conn.Execute(fSql & vSql)
            conn.Close()
        Catch ex As Exception
            Debug.Print(ex.Message)
        End Try

    End Sub

End Class
