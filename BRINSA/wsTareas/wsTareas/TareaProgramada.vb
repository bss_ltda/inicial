﻿Public Class TareaProgramada
    Property Aplicacion As String
    Friend Property ServicioTarea As ServicioTarea

    Dim fSql, vSql As String
    Dim DiaActual As Int32 = 0
    Sub RevisarTareasProgramadas()
        Dim fSql As String
        Dim rs As New ADODB.Recordset
        Dim sts As Integer
        Dim msgT As String
        Dim tLXmsg As String
        Dim ejecutar As String = ""
        Dim bRevisando As Boolean = False
        Dim i As Integer = 1
        Dim iTareas As Int16 = 0
        Dim bEjecutar As Boolean = True

        System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("es-CO")
        With System.Threading.Thread.CurrentThread.CurrentCulture
            .NumberFormat.CurrencyDecimalSeparator = "."
            .NumberFormat.CurrencyGroupSeparator = ","
            .NumberFormat.NumberDecimalSeparator = "."
            .NumberFormat.NumberGroupSeparator = ","
            .DateTimeFormat.ShortDatePattern = "yyyy-MM-dd"
            .DateTimeFormat.ShortTimePattern = "HH:mm:ss"
        End With
        If ServicioTarea.Servidor = "WMS" Then
            'TareasFijas("WMS INTERFACE - CENDIS", "WMS")
            'TareasFijas("WMA INTERFACE - ALMACEN", "WMS")
            'TareasFijas("REPORTE CSV", "MMM")
            ServicioTarea.PrimeraEjecucion = False
            With New TareaCiclica
                .TareasFijas("REENVIA PENDIENTES", "MMM")
                .TareasFijas("TPS", "MMM")
            End With
        End If
        ServicioTarea.SetContarTareasPendientes()
        '========================================================================================
        '===                               CICLO PRINCIPAL                                    ===
        '========================================================================================
        fSql = " Select * FROM  RFTASK "
        fSql &= " WHERE "
        fSql &= "     TAPP = '" & ServicioTarea.Servidor & "' "
        fSql &= " And STS1 = 0 "
        fSql &= " And THLD = 0 "
        fSql &= " And TFPROXIMA <= NOW() "
        If ServicioTarea.Probando Then
            fSql = " Select * FROM  RFTASK WHERE TNUMREG = 6557547"
        End If
        rs = DB.ExecuteSQL(fSql)
        Do While Not rs.EOF
            ServicioTarea.TareaNumero = rs("TNUMREG").Value
            ServicioTarea.Accion = UCase(rs("TTIPO").Value)
            If rs("FWRKID").Value <> 0 Then
                If ServicioTarea.Accion <> "ULTIMO" Then
                    ServicioTarea.Accion = "PAQUETE"
                End If
            Else
                ServicioTarea.Accion = UCase(rs("TTIPO").Value)
            End If
            Select Case ServicioTarea.Accion
                Case UCase("Shell")
                    With ServicioTarea
                        .SetEjecutable(rs("TPGM").Value)
                        .Parametros = rs("TPRM").Value
                        .AgregarParametroTarea = rs("TNUMTAREA").Value = 1
                        .EstadoTarea = 1
                        .MensajeTarea = "Comando Enviado."
                        tLXmsg = "ENVIADA"
                    End With
                Case UCase("Ultimo")
                    With ServicioTarea
                        .Ejecutable = .EjecutableRutina
                        .Parametros = .TareaNumero
                        .AgregarParametroTarea = False
                        .EstadoTarea = 1
                        .MensajeTarea = "Paquete Enviado."
                        tLXmsg = "ENVIADA"
                    End With
                Case UCase("Rutina")
                    With ServicioTarea
                        .Ejecutable = .EjecutableRutina
                        .Parametros = ServicioTarea.TareaNumero
                        .AgregarParametroTarea = False
                        .EstadoTarea = 1
                        .MensajeTarea = "Rutina Enviado."
                        tLXmsg = "ENVIADA"
                    End With
            End Select
            With New ComandoShell
                .ServicioTarea = ServicioTarea
                .EjecutarComando()
            End With
            rs.MoveNext()
            i += 1
        Loop
        rs.Close()
    End Sub

    Private Function Version() As String
        Dim aVersion() As String
        Dim sVersion As String = ""
        aVersion = Split(System.Reflection.Assembly.GetExecutingAssembly.FullName, ",")
        For Each sVersion In aVersion
            If InStr(UCase(sVersion), "VERSION") > 0 Then
                sVersion = sVersion.Trim()
            End If
        Next
        Return sVersion
    End Function
End Class
