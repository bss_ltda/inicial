﻿Friend Class ComandoShell
    Public Property Tarea As String
    Public Property Comando As String
    Public Property MensajeLog As String
    Public Property ServicioTarea As ServicioTarea

    Dim IdProceso As Integer

    Sub EjecutarComando()
        If ServicioTarea.TareasPendientes > 50 Then
            Enviar_y_Esperar()
        Else
            Enviar()
        End If
    End Sub
    Private Sub Enviar_y_Esperar()
        Dim newProc As Diagnostics.Process
        Dim Comando As String = ServicioTarea.GetComandoCompleto()
        Dim Parametros As String
        If ServicioTarea.Parametros <> "" Then
            Parametros = ServicioTarea.GetParametros()
            newProc = Diagnostics.Process.Start(Comando, Parametros)
        Else
            newProc = Diagnostics.Process.Start(Comando)
        End If
        IdProceso = newProc.Id
        newProc.WaitForExit()
        Dim procEC As Integer = -1
        If newProc.HasExited Then
            procEC = newProc.ExitCode
        End If
    End Sub
    Private Sub Enviar()
        Dim Comando As String = ServicioTarea.GetComandoCompleto()
        ServicioTarea.ActualizarComando(Comando)
        DB.LastSQL = Comando
        IdProceso = Shell(Comando, AppWinStyle.Hide, False)
    End Sub

End Class
