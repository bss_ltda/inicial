﻿using BSSReportesPDF.Models;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace BSSReportesPDF.Controllers
{
    public class PruebaRFPARAMController : Controller
    {
        BSSReportesPDFCore.Modules.DynamicModule dynamicModule = new BSSReportesPDFCore.Modules.DynamicModule();
        BSSReportesPDFCore.Modules.ParamModule paramModule = new BSSReportesPDFCore.Modules.ParamModule();

        public ActionResult Index()
        {
            //var otraPrueba = paramModule.GetByPredicate(m => m.CCTABL != "xxLXLONG").ToList();

            //var otraPrueba = paramModule.GetByPredicate(m => m.CCTABL == "LXLONG" && m.CCCODE == "APIBRINSA").ToList();

            StringBuilder query = new StringBuilder();

            query.Append(" SELECT ");
            query.Append("   CCTABL AS \"Nombre Tabla\" ");
            query.Append(" , CCCODE ");
            query.Append(" , CCNOT1 ");
            query.Append(" FROM ");
            query.Append("  RFPARAM ");
            query.Append(" WHERE ");
            query.Append("  CCTABL = 'LXLONG' ");

            var dosQueries = new DosQueries() {
                parametro1 = dynamicModule.GetByQuery(query.ToString()),
                parametro2 = dynamicModule.GetByQuery(query.ToString())
            };

            return View(dosQueries);
        }
    }
}