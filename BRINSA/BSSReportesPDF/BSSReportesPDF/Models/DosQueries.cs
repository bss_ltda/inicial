﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BSSReportesPDF.Models
{
    public class DosQueries
    {
        public IEnumerable<dynamic> parametro1 { get; set; }
        public IEnumerable<dynamic> parametro2 { get; set; }
    }
}