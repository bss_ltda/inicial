﻿using System.Collections.Generic;
using System.Linq.Expressions;

namespace BSSReportesPDFCore.Modules
{
    public class ParamModule
    {
        BSSReportesPDFCore.Repository.DB2Repository<Entities.RFPARAM> repository;

        public ParamModule()
        {
            this.repository = new BSSReportesPDFCore.Repository.DB2Repository<Entities.RFPARAM>();
        }

        public void Add(Entities.RFPARAM data, Dictionary<string, byte[]> bytesDictionary = null)
        {
            if (data is null)
                throw new System.NullReferenceException($"El param especificado es un valor nulo");

            this.repository.Add(data, bytesDictionary);
        }

        public IEnumerable<Entities.RFPARAM> GetByPredicate(Expression<System.Func<Entities.RFPARAM, bool>> predicate)
        {
            return repository.GetByPredicate(predicate);
        }
    }
}
