﻿using System.Collections.Generic;
using System.Linq;

namespace BSSReportesPDFCore.Modules
{
    public class DynamicModule
    {
        BSSReportesPDFCore.Repository.DB2Repository<dynamic> repository;

        public DynamicModule()
        {
            this.repository = new BSSReportesPDFCore.Repository.DB2Repository<dynamic>();
        }

        public List<dynamic> GetByQuery(string query)
        {
            return this.repository.DynamicSqlQuery(query).ToList();
        }
        public void UpdateByQuery(string query)
        {
            this.repository.ExecutePlainQuery(query);
        }
    }
}
