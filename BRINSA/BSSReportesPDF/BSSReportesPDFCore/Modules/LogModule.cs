﻿using System.Collections.Generic;

namespace BSSReportesPDFCore.Modules
{
    public class LogModule
    {
        BSSReportesPDFCore.Repository.DB2Repository<Entities.RFLOG> repository;

        public LogModule()
        {
            this.repository = new BSSReportesPDFCore.Repository.DB2Repository<Entities.RFLOG>();
        }

        public void Add(Entities.RFLOG data, Dictionary<string, byte[]> bytesDictionary = null)
        {
            if (data is null)
                throw new System.NullReferenceException($"El log especificado es un valor nulo");

            this.repository.Add(data, bytesDictionary);
        }
        public void Update(Entities.RFLOG data, Dictionary<string, byte[]> bytesDictionary = null)
        {
            if (data is null)
                throw new System.NullReferenceException($"El log especificado es un valor nulo");

            this.repository.Update(data);
        }
    }
}
