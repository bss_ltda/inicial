﻿using Dapper;
using IBM.Data.DB2.iSeries;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace BSSReportesPDFCore.Repository
{
    public class DB2Repository<T> : System.IDisposable, IRepository<T> where T : class
    {
        private readonly IDbConnection dCon = new iDB2Connection(ConfigurationManager.ConnectionStrings["ConexionDB2"].ToString());
        private StringBuilder query;
        private DynamicQuery dynamicQuery;
        private IDbTransaction idbt = null;
        private string library;

        /// <summary>
        /// Instancia el datacontext a partir de una conexión IDbConnection
        /// </summary>
        /// <param name="_dCon">Sobreescribe la conexión por defecto</param>
        public DB2Repository(iDB2Connection _dCon = null)
        {
            this.dCon = _dCon ?? dCon;
            dynamicQuery = new DynamicQuery();
        }

        public void Add(List<T> entity)
        {

            //string Sql = DynamicQuery.GetInsertQuery(typeof(T).Name, entity);
            query = new StringBuilder();
            query.AppendFormat("INSERT INTO {0}", typeof(T).Name);
            query.Append(" (");
            string sep = "";
            PropertyInfo ChildKey = entity.First().GetType().GetProperties().Where(s => s.GetCustomAttributes(false)
                                    .FirstOrDefault(p => p.GetType().Name == "KeyAttribute") != null).FirstOrDefault();
            foreach (PropertyInfo item in entity.First().GetType().GetProperties())
            {
                if (item.Name != ChildKey.Name)
                {
                    query.AppendFormat("{0} {1}", sep, item.Name);
                    if (sep == "")
                    {
                        sep = ",";
                    }
                }
            }
            query.Append(")");
            query.Append(" Values(");
            sep = "";
            foreach (var item in entity)
            {
                //if (item.GetType() != ChildKey.Name)
                //{
                query.AppendFormat("{0} {1}", sep, item);
                if (sep == "")
                {
                    sep = ",";
                }
                //}
            }
            query.Append(" )");

            dCon.Execute(query.ToString(), entity).ToString();

        }

        public void Add(T entity, Dictionary<string, byte[]> bytesDictionary = null)
        {
            query = new StringBuilder();
            string entityName = !string.IsNullOrEmpty(this.library) ? this.library + "." + typeof(T).Name : typeof(T).Name;

            PropertyInfo primaryKey = entity.GetType().GetProperties().Where(s => s.GetCustomAttributes(false)
                                    .FirstOrDefault(p => p.GetType().Name == "KeyAttribute") != null).FirstOrDefault();

            var valorQuery = "*";

            if (primaryKey != null)
            {
                valorQuery = primaryKey.Name;
            }

            query.Append($"SELECT {valorQuery} FROM FINAL TABLE(INSERT INTO { entityName }");

            string sep = "";
            string propsClause = " (";
            string valuesClause = " Values(";
            bool isBlob = false;
            bool isTimestamp = false;

            foreach (PropertyInfo item in entity.GetType().GetProperties())
            {
                isTimestamp = item.GetCustomAttributes(false).Any(ca => ca.GetType().Name == "TimestampAttribute");
                isBlob = item.GetCustomAttributes(false).Where(ca => ca.GetType().Name == "DisplayAttribute").Any(d => ((DisplayAttribute)d).GroupName == "BLOB");

                if (item.GetValue(entity) != null && !item.GetMethod.IsVirtual)
                {
                    if (item.Name != primaryKey?.Name ||
                        (primaryKey?.GetValue(entity) != null && primaryKey?.GetValue(entity).ToString() != "0"))
                    {
                        propsClause += $"{sep} {item.Name}";

                        if (isBlob)
                        {
                            if (bytesDictionary.TryGetValue(item.Name, out byte[] bytesItem))
                            {
                                valuesClause += $"{sep} @{item.Name}";
                            }
                        }
                        else
                        {
                            if (item.PropertyType.Name == "String")
                            {
                                valuesClause += $"{sep} '{item.GetValue(entity)}'";
                            }
                            else
                            {
                                if (item.PropertyType.GenericTypeArguments.Length > 0)
                                {
                                    if (item.PropertyType.GenericTypeArguments[0].Name == "DateTime")
                                    {
                                        if (isTimestamp)
                                        {
                                            valuesClause += $"{sep} '{System.DateTime.Parse(item.GetValue(entity).ToString()).ToString("yyyy-MM-dd-HH.mm.ss.ffffff")}'";
                                        }
                                        else
                                        {
                                            valuesClause += $"{sep} '{System.DateTime.Parse(item.GetValue(entity).ToString()).ToString("yyyy-MM-dd")}'";
                                        }
                                    }
                                }
                                else
                                {
                                    valuesClause += $"{sep} {item.GetValue(entity)}";
                                }
                            }
                        }

                        if (sep == "")
                        {
                            sep = ",";
                        }
                    }
                }
            }

            propsClause += ")";
            valuesClause += ")";

            query.Append(propsClause);
            query.Append(valuesClause);
            query.Append(")"); // Cierra el select final

            iDB2Command DB2Command = new iDB2Command(query.ToString(), dCon);

            if (bytesDictionary != null)
            {
                foreach (var item in bytesDictionary)
                {
                    DB2Command.Parameters.Add($"@{item.Key}", iDB2DbType.iDB2Blob, item.Value.Length).Value = item.Value;
                }
            }

            dCon.Open();

            iDB2DataReader DB2DataReader = DB2Command.ExecuteReader();

            dynamic valorId = null;

            if (primaryKey != null)
            {
                while (DB2DataReader.Read())
                {
                    valorId = DB2DataReader.GetFieldValue<dynamic>(0);
                }

                DB2DataReader.Close();

                primaryKey?.SetValue(entity, valorId);
            }

            dCon.Close();
        }

        public void Update(T entity)
        {
            query = new StringBuilder();

            query.Append($"UPDATE {typeof(T).Name} SET ");

            string sep = "";
            string propsClause = "";

            PropertyInfo primaryKey = entity.GetType().GetProperties().Where(s => s.GetCustomAttributes(false)
                                    .FirstOrDefault(p => p.GetType().Name == "KeyAttribute") != null).FirstOrDefault();

            foreach (PropertyInfo item in entity.GetType().GetProperties())
            {
                bool isTimestamp = item.GetCustomAttributes(false).Any(ca => ca.GetType().Name == "TimestampAttribute");

                if (item.GetValue(entity) != null && !item.GetMethod.IsVirtual)
                {
                    if (item.Name != primaryKey?.Name)
                    {
                        propsClause += $"{sep} {item.Name}";

                        if (item.Name == "C_XML")
                        {
                            propsClause += $" = @C_XML";
                        }
                        else
                        {
                            if (item.PropertyType.Name == "String")
                            {
                                propsClause += $" = '{item.GetValue(entity)}'";
                            }
                            else
                            {
                                if (item.PropertyType.GenericTypeArguments.Length > 0)
                                {
                                    if (item.PropertyType.GenericTypeArguments[0].Name == "DateTime")
                                    {
                                        if (isTimestamp)
                                        {
                                            propsClause += $" = '{System.DateTime.Parse(item.GetValue(entity).ToString()).ToString("yyyy-MM-dd-HH.mm.ss.ffffff")}'";
                                        }
                                        else
                                        {
                                            propsClause += $" = '{System.DateTime.Parse(item.GetValue(entity).ToString()).ToString("yyyy-MM-dd")}'";
                                        }
                                    }
                                }
                                else
                                {
                                    propsClause += $" = {item.GetValue(entity)}";
                                }
                            }
                        }

                        if (sep == "")
                        {
                            sep = ",";
                        }
                    }
                }
            }

            query.Append(propsClause);
            query.Append($" WHERE {primaryKey?.Name} = '{primaryKey.GetValue(entity)}'");

            System.Diagnostics.Debug.Print(query.ToString());

            dCon.Execute(query.ToString(), entity).ToString();
        }

        public void Delete(string Where = null)
        {
            query = new StringBuilder();
            query.AppendFormat("DELETE FROM {0}", typeof(T).Name);

            if (Where != null)
            {
                if (Where.Trim() != "")
                {
                    query.Append(" WHERE ");
                    query.Append(Where);
                }
            }

            dCon.Execute(query.ToString());
        }

        public IEnumerable<T> ExecuteQuery(string query)
        {
            return dCon.Query<T>(query, commandTimeout: 200);
        }

        public int ExecutePlainQuery(string query)
        {
            return dCon.Execute(query);
        }

        public IEnumerable<T> GetAll()
        {
            query = new StringBuilder();
            query.Append("SELECT *");
            query.AppendFormat(" FROM {0}", typeof(T).Name);

            return dCon.Query<T>(query.ToString());
        }

        public T GetOne(string Where)
        {
            query = new StringBuilder();
            query.Append("SELECT *");
            query.AppendFormat(" FROM {0}", typeof(T).Name);
            query.Append(" WHERE ");
            query.Append(Where);
            return dCon.Query<T>(query.ToString()).SingleOrDefault();
        }

        public IEnumerable<T> GetByPredicate(Expression<System.Func<T, bool>> predicate)
        {            
            QueryResult result = DynamicQuery.GetDynamicQuery(typeof(T).Name, predicate);

            return dCon.Query<T>(result.Sql, (object)result.Param);            
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dCon.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }

        public IEnumerable<dynamic> DynamicSqlQuery(string sql)
        {
            //Database database = this.context.Database;

            TypeBuilder builder = CreateTypeBuilder(
                    "MyDynamicAssembly", "MyDynamicModule", "MyDynamicType");

            using (System.Data.IDbCommand command = dCon.CreateCommand())
            {
                try
                {
                    dCon.Open();
                    //database.Connection.Open();
                    command.CommandText = sql;
                    command.CommandTimeout = command.Connection.ConnectionTimeout;

                    using (System.Data.IDataReader reader = command.ExecuteReader())
                    {
                        var schema = reader.GetSchemaTable();

                        //var columns = Enumerable.Range(0, reader.FieldCount).Select(reader.GetName).ToList();

                        foreach (System.Data.DataRow row in schema.Rows)
                        {
                            string name = (string)row["ColumnName"];
                            //var a=row.ItemArray.Select(d=>d.)
                            System.Type type = (System.Type)row["DataType"];

                            if (type != typeof(string) && (bool)row.ItemArray[schema.Columns.IndexOf("AllowDbNull")])
                            {
                                type = typeof(System.Nullable<>).MakeGenericType(type);
                            }

                            CreateAutoImplementedProperty(builder, name, type);
                        }
                    }
                }
                finally
                {
                    dCon.Close();
                    command.Parameters.Clear();
                }
            }

            System.Type resultType = builder.CreateType();

            return dCon.Query(resultType, sql).Cast<dynamic>();
        }

        private static TypeBuilder CreateTypeBuilder(
            string assemblyName, string moduleName, string typeName)
        {
            TypeBuilder typeBuilder = System.AppDomain
                .CurrentDomain
                .DefineDynamicAssembly(new AssemblyName(assemblyName),
                                       AssemblyBuilderAccess.Run)
                .DefineDynamicModule(moduleName)
                .DefineType(typeName, TypeAttributes.Public);
            typeBuilder.DefineDefaultConstructor(MethodAttributes.Public);
            return typeBuilder;
        }

        private static void CreateAutoImplementedProperty(
            TypeBuilder builder, string propertyName, System.Type propertyType)
        {
            const string PrivateFieldPrefix = "m_";
            const string GetterPrefix = "get_";
            const string SetterPrefix = "set_";

            // Generate the field.
            FieldBuilder fieldBuilder = builder.DefineField(
                string.Concat(PrivateFieldPrefix, propertyName),
                              propertyType, FieldAttributes.Private);

            // Generate the property
            PropertyBuilder propertyBuilder = builder.DefineProperty(
                propertyName, System.Reflection.PropertyAttributes.HasDefault, propertyType, null);

            // Property getter and setter attributes.
            MethodAttributes propertyMethodAttributes =
                MethodAttributes.Public | MethodAttributes.SpecialName |
                MethodAttributes.HideBySig;

            // Define the getter method.
            MethodBuilder getterMethod = builder.DefineMethod(
                string.Concat(GetterPrefix, propertyName),
                propertyMethodAttributes, propertyType, System.Type.EmptyTypes);

            // Emit the IL code.
            // ldarg.0
            // ldfld,_field
            // ret
            ILGenerator getterILCode = getterMethod.GetILGenerator();
            getterILCode.Emit(OpCodes.Ldarg_0);
            getterILCode.Emit(OpCodes.Ldfld, fieldBuilder);
            getterILCode.Emit(OpCodes.Ret);

            // Define the setter method.
            MethodBuilder setterMethod = builder.DefineMethod(
                string.Concat(SetterPrefix, propertyName),
                propertyMethodAttributes, null, new System.Type[] { propertyType });

            // Emit the IL code.
            // ldarg.0
            // ldarg.1
            // stfld,_field
            // ret
            ILGenerator setterILCode = setterMethod.GetILGenerator();
            setterILCode.Emit(OpCodes.Ldarg_0);
            setterILCode.Emit(OpCodes.Ldarg_1);
            setterILCode.Emit(OpCodes.Stfld, fieldBuilder);
            setterILCode.Emit(OpCodes.Ret);

            propertyBuilder.SetGetMethod(getterMethod);
            propertyBuilder.SetSetMethod(setterMethod);
        }

        public IEnumerable<dynamic> DynamicFromSql(string Sql, params object[] parameters)
        {
            using (var cmd = dCon.CreateCommand())
            {
                cmd.CommandText = Sql;
                if (cmd.Connection.State != ConnectionState.Open) { cmd.Connection.Open(); }

                foreach (var param in parameters)
                {
                    cmd.Parameters.Add(param);
                }

                //foreach (KeyValuePair<string, object> p in Params)
                //{
                //    DbParameter dbParameter = cmd.CreateParameter();

                //    dbParameter.ParameterName = p.Key;
                //    dbParameter.Value = p.Value;
                //    cmd.Parameters.Add(dbParameter);
                //}

                using (var dataReader = cmd.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        var row = new ExpandoObject() as IDictionary<string, object>;

                        for (var fieldCount = 0; fieldCount < dataReader.FieldCount; fieldCount++)
                        {
                            row.Add(dataReader.GetName(fieldCount), dataReader[fieldCount]);
                        }

                        if (cmd.Connection.State != ConnectionState.Closed) { cmd.Connection.Close(); }

                        yield return row;
                    }
                }
            }
        }
    }
}
