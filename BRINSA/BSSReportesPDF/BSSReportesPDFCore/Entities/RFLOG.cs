﻿using System.ComponentModel.DataAnnotations;

namespace BSSReportesPDFCore.Entities
{
    public class RFLOG
    {
        private string _OPERACION;
        private string _USUARIO;
        private string _PROGRAMA;
        private string _EVENTO;
        private string _TXTSQL;
        private string _ALERTTO;
        private string _LKEY;
        private string _ERRORCODE;
        private string _AMBIENTE;

        [Key]
        [Display(Name = "ID")]
        public long ID { get; set; }
        [Display(Name = "OPERACION")]
        public string OPERACION { get => _OPERACION?.Trim(); set => _OPERACION = value; }
        [Display(Name = "FECHA")]
        [Timestamp]
        public System.DateTime? FECHA { get; set; }
        [Display(Name = "USUARIO")]
        public string USUARIO { get => _USUARIO?.Trim(); set => _USUARIO = value; }
        [Display(Name = "PROGRAMA")]
        public string PROGRAMA { get => _PROGRAMA?.Trim(); set => _PROGRAMA = value; }
        [Display(Name = "EVENTO")]
        public string EVENTO { get => _EVENTO?.Trim(); set => _EVENTO = value; }
        [Display(Name = "TXTSQL")]
        public string TXTSQL { get => _TXTSQL?.Trim(); set => _TXTSQL = value; }
        [Display(Name = "ALERT")]
        public int ALERT { get; set; }
        [Display(Name = "IDSESION")]
        public int IDSESION { get; set; }
        [Display(Name = "FECHAWIN")]
        [Timestamp]
        public System.DateTime? FECHAWIN { get; set; }
        [Display(Name = "ALERTTO")]
        public string ALERTTO { get => _ALERTTO?.Trim(); set => _ALERTTO = value; }
        [Display(Name = "Clave", GroupName = "BLOB")]
        public string LKEY { get => _LKEY?.Trim(); set => _LKEY = value; }
        [Display(Name = "Codigo de Error")]
        public string ERRORCODE { get => _ERRORCODE?.Trim(); set => _ERRORCODE = value; }
        [Display(Name = "Tarea")]
        public string TAREA { get; set; }
        [Display(Name = "Ambiente")]
        public string AMBIENTE { get => _AMBIENTE?.Trim(); set => _AMBIENTE = value; }
    }


}
