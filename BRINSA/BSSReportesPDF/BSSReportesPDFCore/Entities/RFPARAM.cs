﻿using System.ComponentModel.DataAnnotations;

namespace BSSReportesPDFCore.Entities
{
    public class RFPARAM
    {
        private string _CCID;
        private string _CCTABL;
        private string _CCCODE;
        private string _CCCODE2;
        private string _CCCODE3;
        private string _CCCODE4;
        private string _CCCODE5;
        private string _CCLANG;
        private string _CCALTC;
        private string _CCDESC;
        private string _CCSDSC;
        private string _CCNOT1;
        private string _CCNOT2;
        private string _CCENUS;
        private string _CCMNUS;
        private string _CCNOTE;

        [Display(Name = "Numero Unico")]
        public int CCNUM { get; set; }
        [Display(Name = "Record ID (CC/CZ)")]
        public string CCID { get => _CCID?.Trim(); set => _CCID = value; }
        [Display(Name = "Table ID")]
        public string CCTABL { get => _CCTABL?.Trim(); set => _CCTABL = value; }
        [Display(Name = "Primary Code 1")]
        public string CCCODE { get => _CCCODE?.Trim(); set => _CCCODE = value; }
        [Display(Name = "Primary Code 2")]
        public string CCCODE2 { get => _CCCODE2?.Trim(); set => _CCCODE2 = value; }
        [Display(Name = "Primary Code 3")]
        public string CCCODE3 { get => _CCCODE3?.Trim(); set => _CCCODE3 = value; }
        [Display(Name = "Primary Code 4")]
        public string CCCODE4 { get => _CCCODE4?.Trim(); set => _CCCODE4 = value; }
        [Display(Name = "Primary Code 5")]
        public string CCCODE5 { get => _CCCODE5?.Trim(); set => _CCCODE5 = value; }
        [Display(Name = "Primary Code 6")]
        public int CCCODEN { get; set; }
        [Display(Name = "Primary Code 7")]
        public int CCCODEN2 { get; set; }
        [Display(Name = "CCLANG")]
        public string CCLANG { get => _CCLANG?.Trim(); set => _CCLANG = value; }
        [Display(Name = "Alternate Code")]
        public string CCALTC { get => _CCALTC?.Trim(); set => _CCALTC = value; }
        [Display(Name = "Description")]
        public string CCDESC { get => _CCDESC?.Trim(); set => _CCDESC = value; }
        [Display(Name = "Short Description")]
        public string CCSDSC { get => _CCSDSC?.Trim(); set => _CCSDSC = value; }
        [Display(Name = "Note 1")]
        public string CCNOT1 { get => _CCNOT1?.Trim(); set => _CCNOT1 = value; }
        [Display(Name = "Note 2")]
        public string CCNOT2 { get => _CCNOT2?.Trim(); set => _CCNOT2 = value; }
        [Display(Name = "User Code 0")]
        public int CCUDC0 { get; set; }
        [Display(Name = "User Code 1")]
        public int CCUDC1 { get; set; }
        [Display(Name = "User Code 2")]
        public int CCUDC2 { get; set; }
        [Display(Name = "User Code 3")]
        public int CCUDC3 { get; set; }
        [Display(Name = "User Code 4")]
        public int CCUDC4 { get; set; }
        [Display(Name = "User Code 5")]
        public int CCUDC5 { get; set; }
        [Display(Name = "User Code 6")]
        public int CCUDC6 { get; set; }
        [Display(Name = "User Code 7")]
        public int CCUDC7 { get; set; }
        [Display(Name = "User Code 8")]
        public int CCUDC8 { get; set; }
        [Display(Name = "User Code 9")]
        public int CCUDC9 { get; set; }
        [Display(Name = "User Val 1")]
        public double CCUDV1 { get; set; }
        [Display(Name = "User Val 2")]
        public double CCUDV2 { get; set; }
        [Display(Name = "User Val 3")]
        public double CCUDV3 { get; set; }
        [Display(Name = "User Val 4")]
        public double CCUDV4 { get; set; }
        [Display(Name = "User Timestamp")]
        [Timestamp]
        public System.DateTime? CCUDTS { get; set; }
        [Display(Name = "User Date")]
        public System.DateTime? CCUDDT { get; set; }
        [Display(Name = "Reservation")]
        public int CCRESV { get; set; }
        [Display(Name = "Entered Date")]
        [Timestamp]
        public System.DateTime? CCENDT { get; set; }
        [Display(Name = "Entered By User")]
        public string CCENUS { get => _CCENUS?.Trim(); set => _CCENUS = value; }
        [Display(Name = "Last Maintenance Date")]
        [Timestamp]
        public System.DateTime? CCMNDT { get; set; }
        [Display(Name = "Last Maintenance User")]
        public string CCMNUS { get => _CCMNUS?.Trim(); set => _CCMNUS = value; }
        [Display(Name = "Note")]
        public string CCNOTE { get => _CCNOTE?.Trim(); set => _CCNOTE = value; }
    }


}
