﻿using System.Collections.Generic;

namespace BSSRutinasCSharpCore.Repositorio
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IEnumerable<TEntity> GetData(string query);
        string ExecuteQuery(string query);
    }
}
