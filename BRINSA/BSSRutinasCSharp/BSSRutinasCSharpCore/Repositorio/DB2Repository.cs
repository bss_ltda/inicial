﻿using Dapper;
using IBM.Data.DB2.iSeries;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;

namespace BSSRutinasCSharpCore.Repositorio
{
    public class DB2Repository<T> : IDisposable, IRepository<T> where T : class
    {
        private readonly IDbConnection dCon = new iDB2Connection(ConfigurationManager.ConnectionStrings["ConexionDB2"].ToString());

        public string ExecuteQuery(string query)
        {
            string res = "";
            try
            {
                var affectedRows = dCon.Execute(query);
                res = affectedRows.ToString();
            }
            catch (Exception ex)
            {
                res = "ERROR: " + ex.ToString() + ((ex.InnerException != null) ? ex.InnerException.ToString() : "");
            }

            return res;
        }

        public IEnumerable<T> GetData(string query)
        {
            return dCon.Query<T>(query);
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    dCon.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
