﻿using BSSRutinasCSharpCore.Repositorio;
using System.Collections.Generic;
using System.Linq;

namespace BSSRutinasCSharpCore.Modulos
{
    public class EjemploModulo
    {
        DB2Repository<Entidades.Ejemplo> dB2Repository;

        public EjemploModulo()
        {
            this.dB2Repository = new DB2Repository<Entidades.Ejemplo>();
        }

        public List<Entidades.Ejemplo> GetDataEjemplo(string campo2)
        {
            IEnumerable<Entidades.Ejemplo> ejemplos = dB2Repository.GetData(string.Format("SELECT Campo1, Campo4 FROM TABLA WHERE Campo2 = '{0}'", campo2));
            return ejemplos.ToList();
        }

        public string ExecuteEjemplo(string campo2)
        {
            string res = dB2Repository.ExecuteQuery(string.Format("UPDATE TABLA SET Campo1 = '2' WHERE Campo2 = '{0}'", campo2));
            return res;
        }
    }
}
