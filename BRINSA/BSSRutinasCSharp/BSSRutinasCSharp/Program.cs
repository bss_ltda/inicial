﻿using System.Collections.Generic;

namespace BSSRutinasCSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            BSSRutinasCSharpCore.Modulos.EjemploModulo ejemploModulo = new BSSRutinasCSharpCore.Modulos.EjemploModulo();
            List<BSSRutinasCSharpCore.Entidades.Ejemplo> ejemplos = ejemploModulo.GetDataEjemplo("Prueba");
            string res = ejemploModulo.ExecuteEjemplo("Prueba");
        }
    }
}
