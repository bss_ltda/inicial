﻿Imports System.Data.OleDb
Imports System.Text

Public Class Form1
    Dim db As New OleDbConnection
    Dim bCarga As Boolean

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim gsConn As String = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source=C:\Dropbox\AppBSS\dfu.mdb"

        Try
            db = New OleDbConnection(gsConn)
        Catch ex As Exception
            MessageBox.Show("Error al crear la conexión:" & vbCrLf & ex.Message)
            Exit Sub
        End Try
        db.Open()
        bCarga = False

        LlenaGrupos()
        Me.ListBoxGrupos.SelectedIndex = -1
        bCarga = True
        Me.ListBoxGrupos.SelectedIndex = CInt(GetSetting(My.Application.Info.AssemblyName, "Sitios", "Grupo", "-1"))
        If Me.ListBoxGrupos.SelectedIndex <> -1 Then
            LlenaGrid(Me.ListBoxGrupos.Text)
        End If
        RBtnSitios.Checked = True



    End Sub

    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        With Me.DataGridView1
            Try
                Dim url As String = "" '.Rows.Item(e.RowIndex).Cells(2).Value
                Dim fila As Integer = e.RowIndex
                url = Me.DataGridView1.Rows(fila).Cells(e.ColumnIndex).Value
                If InStr(url, "http") > 0 Then
                    System.Diagnostics.Process.Start(url)
                Else
                    Shell("mstsc /v:" & url & "  /f /Admin", vbNormalFocus)
                End If

            Catch ex As ArgumentOutOfRangeException
                MsgBox(ex.Message)
            End Try

        End With

    End Sub

    Sub LlenaGrid(Grupo As String)
        Dim fSql As String = ""
        If Not bCarga Then Exit Sub

        If RBtnSitios.Checked = True Then
            fSql = "SELECT NOMBRE, URL, DEBUG FROM PERFIL_WEB"
        Else
            fSql = "SELECT NOMBRE, DIRECCION_IP, USUARIO, PASSWORD FROM ESCRITORIO_REMOTO"
        End If
        'Dim fSql As String = "SELECT NOMBRE, URL, DEBUG FROM PERFIL_WEB"
        If Grupo <> "" Then
            fSql &= String.Format(" WHERE GRUPO = '{0}'", Grupo)
        End If
        Dim da = New OleDbDataAdapter(fSql, db)
        Dim ds = New Data.DataSet()
        da.Fill(ds)
        Me.DataGridView1.DataSource = ds.Tables(0)
        With Me.DataGridView1
            .Columns.Item(0).Width = 260
            .Columns.Item(1).Width = 300
        End With
        SaveSetting(My.Application.Info.AssemblyName, "Sitios", "GRUPO", Me.ListBoxGrupos.SelectedIndex)


    End Sub
    Sub LlenaGrupos()
        Dim cm As OleDbCommand = New OleDbCommand
        Dim da As OleDbDataAdapter
        Dim ds As DataSet
        Dim fSql As String

        If RBtnSitios.Checked = True Then
            fSql = "SELECT GRUPO FROM PERFIL_WEB GROUP BY GRUPO ORDER BY GRUPO"
        Else : RBtnRemoto.Checked = True
            fSql = "SELECT GRUPO FROM ESCRITORIO_REMOTO GROUP BY GRUPO ORDER BY GRUPO"
        End If
        cm.CommandText = fSql
        cm.CommandType = CommandType.Text
        cm.Connection = db

        da = New OleDbDataAdapter(cm)
        ds = New DataSet()
        da.Fill(ds)

        'Me.ComboBoxGrupos.DataSource = ds.Tables(0)
        'Me.ComboBoxGrupos.DisplayMember = "GRUPO"
        'Me.ComboBoxGrupos.ValueMember = "GRUPO"

        Me.ListBoxGrupos.DataSource = ds.Tables(0)
        Me.ListBoxGrupos.DisplayMember = "GRUPO"
        Me.ListBoxGrupos.ValueMember = "GRUPO"
        


    End Sub

    'Private Sub ComboBoxGrupos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxGrupos.SelectedIndexChanged

    '    LlenaGrid(Me.ComboBoxGrupos.Text)

    'End Sub

    'Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
    '    Dim wCols As New StringBuilder
    '    Dim i As Integer = 0
    '    For Each col In Me.DataGridView1.Columns
    '        wCols.AppendFormat("Columna {0} Ancho {1}", i, col.Width)
    '        wCols.AppendLine()
    '        i += 1
    '    Next
    '    MsgBox(wCols.ToString)
    'End Sub

    Private Sub DataGridView1_Click(sender As Object, e As EventArgs) Handles DataGridView1.Click

    End Sub

    Private Sub ListBoxGrupos_Click(sender As Object, e As EventArgs) Handles ListBoxGrupos.Click
        LlenaGrid(Me.ListBoxGrupos.Text)
    End Sub


   
    Private Sub ListBoxGrupos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListBoxGrupos.SelectedIndexChanged

    End Sub

    Private Sub RBtnRemoto_CheckedChanged(sender As Object, e As EventArgs) Handles RBtnRemoto.CheckedChanged
        If RBtnRemoto.Checked = True Then
            RBtnSitios.Checked = False
            LlenaGrupos()
            Me.ListBoxGrupos.SelectedIndex = -1
            bCarga = True
            Me.ListBoxGrupos.SelectedIndex = CInt(GetSetting(My.Application.Info.AssemblyName, "Sitios", "Grupo", "-1"))
            If Me.ListBoxGrupos.SelectedIndex <> -1 Then
                LlenaGrid(Me.ListBoxGrupos.Text)
            End If
        End If

    End Sub

    Private Sub RBtnSitios_CheckedChanged(sender As Object, e As EventArgs) Handles RBtnSitios.CheckedChanged
        If RBtnSitios.Checked = True Then
            RBtnRemoto.Checked = False
           
            LlenaGrupos()
            Me.ListBoxGrupos.SelectedIndex = -1
            bCarga = True
            Me.ListBoxGrupos.SelectedIndex = CInt(GetSetting(My.Application.Info.AssemblyName, "Sitios", "Grupo", "-1"))
            If Me.ListBoxGrupos.SelectedIndex <> -1 Then
                LlenaGrid(Me.ListBoxGrupos.Text)
            End If
        End If

    End Sub

    
End Class
