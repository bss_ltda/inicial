﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.ListBoxGrupos = New System.Windows.Forms.ListBox()
        Me.RBtnRemoto = New System.Windows.Forms.RadioButton()
        Me.RBtnSitios = New System.Windows.Forms.RadioButton()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(105, 40)
        Me.DataGridView1.MultiSelect = False
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.Size = New System.Drawing.Size(803, 290)
        Me.DataGridView1.TabIndex = 0
        '
        'ListBoxGrupos
        '
        Me.ListBoxGrupos.FormattingEnabled = True
        Me.ListBoxGrupos.Location = New System.Drawing.Point(3, 40)
        Me.ListBoxGrupos.Name = "ListBoxGrupos"
        Me.ListBoxGrupos.Size = New System.Drawing.Size(96, 290)
        Me.ListBoxGrupos.TabIndex = 2
        '
        'RBtnRemoto
        '
        Me.RBtnRemoto.AutoSize = True
        Me.RBtnRemoto.Location = New System.Drawing.Point(124, 12)
        Me.RBtnRemoto.Name = "RBtnRemoto"
        Me.RBtnRemoto.Size = New System.Drawing.Size(108, 17)
        Me.RBtnRemoto.TabIndex = 3
        Me.RBtnRemoto.Text = "Escritorio Remoto"
        Me.RBtnRemoto.UseVisualStyleBackColor = True
        '
        'RBtnSitios
        '
        Me.RBtnSitios.AutoSize = True
        Me.RBtnSitios.Location = New System.Drawing.Point(12, 12)
        Me.RBtnSitios.Name = "RBtnSitios"
        Me.RBtnSitios.Size = New System.Drawing.Size(76, 17)
        Me.RBtnSitios.TabIndex = 4
        Me.RBtnSitios.Text = "Sitios Web"
        Me.RBtnSitios.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(911, 335)
        Me.Controls.Add(Me.RBtnSitios)
        Me.Controls.Add(Me.RBtnRemoto)
        Me.Controls.Add(Me.ListBoxGrupos)
        Me.Controls.Add(Me.DataGridView1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.Text = "Sitios Web"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents ListBoxGrupos As System.Windows.Forms.ListBox
    Friend WithEvents RBtnRemoto As System.Windows.Forms.RadioButton
    Friend WithEvents RBtnSitios As System.Windows.Forms.RadioButton

End Class
