﻿Public Class Form1
    Private Sub CmdBusca_Click(sender As Object, e As EventArgs) Handles cmdBusca.Click
        If Me.Label2.Text = "" Then
            MsgBox("Falta el archivo de datos", MsgBoxStyle.Exclamation, "Busqueda Dinámica de Datos")
            Return
        End If
        With New clsBuscaTexto
            .Archivo = OpenFileDialog1.FileName
            .Carpeta = OpenFileDialog1.FileName.Replace(OpenFileDialog1.SafeFileName, "")
            .Entrada = Me.txtEntrada.Text
            .Buscar()
            MsgBox("Proceso Terminado" & .ArchivosGenerados, MsgBoxStyle.Information, "Busqueda Dinámica de Datos")
        End With
    End Sub

    Private Sub CmdArchivo_Click(sender As Object, e As EventArgs) Handles cmdArchivo.Click
        With OpenFileDialog1
            .FileName = ""
            .InitialDirectory = Environment.SpecialFolder.MyDocuments.ToString
            .ShowDialog()
            Me.Label2.Text = .FileName
        End With
    End Sub
End Class
