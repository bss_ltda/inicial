﻿Imports System.IO
Public Class clsBuscaTexto

    Dim _Archivo As String
    Dim _Carpeta As String
    Dim _Entrada As String
    Dim _ArchivosGenerados As String
    Dim Salida As String

    Const c1 As String = "ç,Ç,á,é,í,ó,ú,ý,Á,É,Í,Ó,Ú,Ý,à,è,ì,ò,ù,À,È,Ì,Ò,Ù,ã,õ,ñ,ä,ë,ï,ö,ü,ÿ,Ä,Ë,Ï,Ö,Ü,Ã,Õ,Ñ,â,ê,î,ô,û,Â,Ê,Î,Ô,Û"
    Const c2 As String = "c,C,a,e,i,o,u,y,A,E,I,O,U,Y,a,e,i,o,u,A,E,I,O,U,a,o,n,a,e,i,o,u,y,A,E,I,O,U,A,O,N,a,e,i,o,u,A,E,I,O,U"

    Dim a1() As String = c1.Split(",")
    Dim a2() As String = c2.Split(",")
    Dim aBusca() As String


    Public Property Entrada As String
        Get
            Return _Entrada
        End Get
        Set(value As String)
            _Entrada = value.Replace(vbCrLf, vbCr).Replace(vbCr & vbCr, vbCr)
        End Set
    End Property

    Public Property Archivo As String
        Get
            Return _Archivo
        End Get
        Set(value As String)
            _Archivo = value
        End Set
    End Property

    Public Property Carpeta As String
        Get
            Return _Carpeta
        End Get
        Set(value As String)
            _Carpeta = value
        End Set
    End Property

    Public Property ArchivosGenerados As String
        Get
            Return _ArchivosGenerados
        End Get
        Set(value As String)
            _ArchivosGenerados = value
        End Set
    End Property

    Sub Buscar()
        Dim linea, campo As String
        Dim aLineas() As String = Entrada.Split(vbCr)
        Dim sep As String
        ArchivosGenerados = vbCr & vbCr & "Archivos Generados:"

        For Each linea In aLineas
            linea = SinTildes(linea)
            aBusca = linea.Replace(",", vbTab).Replace(";", vbTab).Replace(" ", vbTab).Split(vbTab)
            Salida = ""
            sep = ""
            For Each campo In aBusca
                Salida &= sep & SinTildes(campo.Trim)
                sep = "_"
            Next
            ArchivosGenerados &= vbCr & Salida
            BuscarCoincidencias()
        Next

    End Sub

    Sub BuscarCoincidencias()

        Dim datoB As String
        Dim bPrimero As Boolean = True
        Dim dato As String
        Dim busq As String
        Dim bEsta As Boolean

        Using outputFile As New StreamWriter(Carpeta & Salida & ".csv", True)
            Using sr As New StreamReader(Archivo)
                Do While Not sr.EndOfStream
                    If bPrimero Then
                        outputFile.WriteLine(sr.ReadLine)
                        bPrimero = False
                    Else
                        bEsta = True
                        dato = sr.ReadLine
                        datoB = SinTildes(dato)
                        For Each busq In aBusca
                            If Not datoB.ToUpper.Contains(busq.ToUpper) Then
                                bEsta = False
                                Exit For
                            End If
                        Next
                        If bEsta Then
                            outputFile.WriteLine(dato)
                        End If
                    End If
                Loop
            End Using
        End Using

    End Sub

    Function SinTildes(txt As String) As String
        Dim s As String = txt
        For i = 0 To UBound(a1)
            s = s.Replace(a1(i), a2(i))
        Next
        Return s
    End Function

End Class
