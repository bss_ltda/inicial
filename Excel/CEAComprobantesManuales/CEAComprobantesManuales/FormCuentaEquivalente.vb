﻿Imports System.Collections.Specialized
Imports System.Data.OleDb

Public Class FormCuentaEquivalente
    Public Parametros As String
    Dim DB As New ADODB.Connection
    Dim bCargando As Boolean
    Dim ws As Excel.Worksheet
    Dim gsLib As String
    Dim SegmentosCEAColeccion As Collection
    Dim CuentasEquivalentesDic As StringDictionary
    Dim qRow As Integer
    Dim bErrores As Boolean

    Private Sub FormCuentaEquivalente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ws = Globals.ThisWorkbook.ActiveSheet
        Me.AS400.Text = My.Settings.AS400
        Me.TextBoxUser.Text = GetSetting(My.Application.Info.AssemblyName, "Ingreso", "Usuario", "")

        With My.Application.Info.Version
            Me.Text &= String.Format(" ({0}.{1}.{2})", .Major, .Minor, .Revision)
        End With
        Globals.ThisWorkbook.libroAmbiente = CInt(GetSetting(My.Application.Info.AssemblyName, "Ingreso", "Ambiente", "-1"))
        If Not RestauraConexion() Then
            Me.Close()
        End If
        Me.GroupBoxExe.Enabled = True
        Me.TextBoxClave.Focus()

    End Sub

    Function valorParametro(param As String) As String
        Dim aParam() As String = Split(Parametros, vbCrLf)
        Dim p, a

        valorParametro = ""
        For Each p In aParam
            a = Split(p, "=")
            If a(0) = param Then
                valorParametro = a(1)
            End If
        Next

    End Function
    Private Function RestauraConexion() As Boolean

        With Globals.ThisWorkbook
            Me.TextBoxEvento.Text = valorParametro("TextBoxEvento")
            Me.ComboBoxAmbiente.SelectedIndex = CInt(GetSetting(My.Application.Info.AssemblyName, "Ingreso", "Ambiente", "-1"))
            If .libroConexionOK Then
                Me.TextBoxUser.Text = .libroUsuario
                Me.TextBoxClave.Text = valorParametro("TextBoxClave")
                'gsLib = valorParametro("gsLib")
                If Not EjecutaLogin() Then
                    Return False
                End If
            Else
                Return True
            End If
        End With
        Return True

    End Function
    Private Sub ComboBoxActividad_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxActividad.SelectedIndexChanged
        Dim fSql As String
        Dim da As New OleDbDataAdapter()
        Dim ds As New DataSet
        Dim rs As New ADODB.Recordset

        If bCargando Then
            Exit Sub
        End If

        fSql = "SELECT TRIM(TDJNMD) || '.' || TRIM(TDLDGR) || '.' || TRIM(TDBOOK) TDLDGR, TDBOOK FROM gtD WHERE TDTRMD = '" & sender.SelectedValue & "'"
        rs.Open(fSql, DB)

        da.Fill(ds, rs, "TABLA")
        rs.ActiveConnection = Nothing

        Me.CheckedListBoxLibros.DataSource = ds.Tables("TABLA")
        Me.CheckedListBoxLibros.DisplayMember = "TDBOOK"
        Me.CheckedListBoxLibros.ValueMember = "TDLDGR"
    End Sub

    Private Function comparaTitulo(ByRef qCol As Integer, qTit As String) As String
        If qCol = -1 Then Return ""
        Dim a, b As String
        a = ws.Cells(1, qCol).text
        a = a.Replace(" ", "").Replace(vbCr, "").Replace(vbLf, "")
        b = qTit.Replace(" ", "").Replace(vbCr, "").Replace(vbLf, "")

        If a <> b Then
            Return "Columna " & qCol & " No corresponde. Debe ser " & qTit & vbCr
            qCol = -1
        Else
            qCol += 1
            Return ""
        End If

    End Function

    Private Sub RevisaSegmento(ByRef msg As String, NumeroColumna As Integer, NumeroSegmento As Integer)
        Dim Codigo As String = ws.Cells(qRow, NumeroColumna).text
        If Codigo.Trim() = "" Then
            Exit Sub
        End If
        Dim texto As String = ws.Cells(1, NumeroColumna).text
        Dim fSql As String
        fSql = " SELECT SVLDES FROM GSVL01  "
        fSql &= " WHERE "
        fSql &= String.Format("SVSGMN = '{0}'", SegmentosCEAColeccion.Item(NumeroSegmento).ToString())
        fSql &= String.Format(" AND SVSGVL = '{0}'", Codigo.Trim())
        If Not SiExiste(fSql) Then
            bErrores = True
            msg &= IIf(msg = "", "", vbLf) & "No encontró " & texto
        End If
    End Sub
    Private Sub SegmentosCEA()
        Dim fSql As String
        fSql = " SELECT TRIM(CCCODE) FROM RFPARAM  "
        fSql &= " WHERE CCTABL = 'FNZSEGMENTS'  AND CCCODE4 <> '' "
        fSql &= " ORDER BY CCCODEN ASC "
        SegmentosCEAColeccion = New Collection
        SegmentosCEAColeccion.Clear()
        Dim rs As New ADODB.Recordset
        rs.Open(fSql, DB)
        Do While Not rs.EOF
            SegmentosCEAColeccion.Add(rs(0).Value)
            rs.MoveNext()
        Loop
        rs.Close()
    End Sub
    Private Function CodigoSegmento(NumeroSegmento As Integer) As String
        Dim fSql As String
        fSql = " SELECT CCCODE FROM RFPARAM  "
        fSql &= " WHERE CCTABL = 'FNZSEGMENTS'  "
        fSql &= String.Format(" AND CCCODEN = {0}", NumeroSegmento)
        Return LookUp01(fSql)
    End Function
    Private Function LookUp01(Sql As String) As String
        Dim rs As New ADODB.Recordset
        Dim s As String = ""
        rs.Open(Sql, DB)
        If Not rs.EOF() Then
            s = rs(0).Value
        End If
        rs.Close()
        Return s
    End Function
    Private Sub Inserta(Sql As String)
        DB.Execute(Sql)
    End Sub
    Private Function RevisaColumnas() As Boolean
        Dim qColumna As Integer = 1
        Dim m As String = ""
        m += comparaTitulo(qColumna, "RESULTADO")                            '01
        m += comparaTitulo(qColumna, "LINEA")                                '02
        m += comparaTitulo(qColumna, "COMPAÑÍA")                             '03
        m += comparaTitulo(qColumna, "CENTRO DE COSTO")                      '04
        m += comparaTitulo(qColumna, "Cuenta BPEFINCOP")                     '05
        m += comparaTitulo(qColumna, "Cuenta NIIF")                          '06
        m += comparaTitulo(qColumna, "PRODUCTO")                             '07
        m += comparaTitulo(qColumna, "APA")                                  '08
        m += comparaTitulo(qColumna, "LINEA PRODUCTO")                       '09
        m += comparaTitulo(qColumna, "CADENA")                               '10
        m += comparaTitulo(qColumna, "Transacciones Debito")                 '11
        m += comparaTitulo(qColumna, "Transacciones Credito")                '12
        m += comparaTitulo(qColumna, "Libro Debito")                         '13
        m += comparaTitulo(qColumna, "Libro Credito")                        '14
        m += comparaTitulo(qColumna, "Estadistica")                          '15
        m += comparaTitulo(qColumna, "Descripcion Linea de Actividad")       '16
        m += comparaTitulo(qColumna, "Referencia 1")                         '17
        m += comparaTitulo(qColumna, "Referencia 2")                         '18
        m += comparaTitulo(qColumna, "Código de Razón")                      '19
        m += comparaTitulo(qColumna, "Documento de Referencia")              '20
        If m <> "" Then
            MsgBox(m, MsgBoxStyle.Exclamation, "Columnas")
        End If
        Return m = ""
    End Function

    Private Sub ButtonCrearComprobante_Click(sender As Object, e As EventArgs) Handles ButtonCrearComprobante.Click
        Me.Enabled = False

        GuardaParametros()

        If Not RevisaColumnas() Then
            Me.Enabled = True
            Exit Sub
        End If

        Dim msg As String = ""
        Dim sep As String
        Dim qColumna As Integer = 1
        Dim Debitos, Creditos, LineaDebito, LineaCredito As Double
        Dim totLibros As Integer
        Dim SegmentoCIA As String

        '' '' ''With Globals.ThisWorkbook
        '' '' ''    .libroAmbiente = Me.ComboBoxAmbiente.SelectedIndex
        '' '' ''    .libroActividad = Me.ComboBoxActividad.SelectedIndex
        '' '' ''End With

        If Not Me.FechaDocumento.Checked Then
            MsgBox("Seleccione la Fecha del documento.", MsgBoxStyle.Exclamation, "Ingreso")
            Me.Enabled = True
            Exit Sub
        End If

        If Me.ComboBoxMoneda.SelectedIndex = -1 Then
            MsgBox("Seleccione Moneda del documento.", MsgBoxStyle.Exclamation, "Ingreso")
            Me.Enabled = True
            Exit Sub

        End If

        totLibros = Me.CheckedListBoxLibros.CheckedItems.Count
        If totLibros = 0 Then
            MsgBox("Seleccione libro o libros.", MsgBoxStyle.Exclamation, "Validacion")
            Me.Enabled = True
            Exit Sub
        End If

        Globals.ThisWorkbook.libroFecha = Me.FechaDocumento.Value
        Globals.ThisWorkbook.libroMoneda = Me.ComboBoxMoneda.SelectedIndex

        'SEGMENTOS BRINSA
        '				1	Compania
        '				2	Centro de Costo
        '				3	Cuenta
        '				4	Producto
        '				5	APA
        '				6	Linea Producto
        '				7	Cadena
        SegmentosCEA()

        qRow = 2
        sep = ""
        Debitos = 0
        Creditos = 0
        LineaDebito = 0
        LineaCredito = 0
        bErrores = False

        SegmentoCIA = ws.Cells(2, 3).Text                                                           '	COMPAÑÍA
        Do While ws.Cells(qRow, 3).text <> ""
            msg = ""
            ws.Cells(qRow, 2).value = qRow - 1                                                      '	LINEA
            ws.Cells(qRow, 3).value = SegmentoCIA
            RevisaSegmento(msg, 3, 1)                                                               '	COMPAÑÍA (1)
            RevisaSegmento(msg, 4, 2)                                                               '	centro de costo (2)
            'ws.Cells(qRow, 21).value = String.Format("{0:yyyyMMdd}", Me.FechaDocumento.Value)       '	Fecha

            RevisaSegmento(msg, 5, 3)                                                               '	Cuenta BPEFINCOP (3)
            If ws.Cells(qRow, 6).text <> "" Then
                RevisaSegmento(msg, 6, 3)                                                           '	Cuenta NIIF (3)
            End If
            RevisaSegmento(msg, 7, 4)                                                               '	PRODUCTO (4)
            RevisaSegmento(msg, 8, 5)                                                               '	APA (5)
            RevisaSegmento(msg, 9, 6)                                                               '	LINEA PRODUCTO (6)
            RevisaSegmento(msg, 10, 7)                                                              '	CADENA (7)
            If msg <> "" Then
                ws.Cells(qRow, 1).value = msg
            Else
                ws.Cells(qRow, 1).value = "OK"
            End If
            Debitos += ws.Cells(qRow, 11).value                                                     '	11	TransaccionesDebito
            Creditos += ws.Cells(qRow, 12).value                                                    '	12	Transacciones Credito
            LineaDebito += ws.Cells(qRow, 13).value                                                 '	13	Libro Debito
            LineaCredito += ws.Cells(qRow, 14).value                                                '	14	Libro Credito
            qRow += 1
        Loop
        If Debitos <> Creditos Then
            msg = "Debitos y Creditos diferentes"
            bErrores = True
        End If
        If LineaDebito <> LineaCredito Then
            msg = "Linea Debito y Linea Credito diferentes"
            bErrores = True
        End If
        If bErrores Then
            MsgBox("Revise Inconsistencias." & vbCr & msg, MsgBoxStyle.Exclamation, "Validacion Datos")
            Me.Enabled = True
            Me.Close()
            Exit Sub
        End If

        'Try
        EnviaComprobante()
        'Catch ex As Exception
        'MsgBox(ex.ToString)
        'End Try


        ws.Cells(1, 1).Select()
        Me.Enabled = True

    End Sub

    Private Sub ButtonCuentasEquivalentes_Click(sender As Object, e As EventArgs) Handles ButtonCuentasEquivalentes.Click
        Me.Enabled = False

        GuardaParametros()

        If Not RevisaColumnas() Then
            Me.Enabled = True
            Exit Sub
        End If

        CuentasEquivalentes()
        qRow = 2
        Do While ws.Cells(qRow, 2).text <> ""
            If CuentasEquivalentesDic(ws.Cells(qRow, 5).value) <> Nothing Then
                ws.Cells(qRow, 6).value = CuentasEquivalentesDic(ws.Cells(qRow, 5).value)
            Else
                ws.Cells(qRow, 6).value = ws.Cells(qRow, 5).value
            End If
            qRow += 1
        Loop

        Me.Enabled = True
        Me.Close()

    End Sub

    Private Sub GuardaParametros()
        '' '' ''With Globals.ThisWorkbook
        '' '' ''    .libroAmbiente = Me.ComboBoxAmbiente.SelectedIndex
        '' '' ''    .libroActividad = Me.ComboBoxActividad.SelectedIndex
        '' '' ''End With

        '' '' ''Globals.ThisWorkbook.libroLibros.Clear()
        '' '' ''For Each i In Me.CheckedListBoxLibros.CheckedIndices
        '' '' ''    Globals.ThisWorkbook.libroLibros.Add(i)
        '' '' ''Next
    End Sub

    Private Sub CuentasEquivalentes()
        Dim fSql As String
        CuentasEquivalentesDic = New StringDictionary
        fSql = " SELECT CCUEORI, CCUEEQU FROM BSSFFNCUE ORDER BY 1 "
        Try
            Dim rs As New ADODB.Recordset
            rs.Open(fSql, DB)
            Do While Not rs.EOF
                CuentasEquivalentesDic.Add(rs("CCUEORI").Value.ToString.Trim, rs("CCUEEQU").Value.ToString.Trim)
                rs.MoveNext()
            Loop
            rs.Close()
            rs = Nothing
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
        End Try

    End Sub

    Sub EnviaComprobante()
        Dim itemChecked As DataRowView
        Dim i As Integer
        Dim aModelo() As String
        Dim Batch As String = CalcConsec("BSSFFNUP", "99999999")

        For i = 0 To (CheckedListBoxLibros.Items.Count - 1)
            If CheckedListBoxLibros.GetItemChecked(i) = True Then
                itemChecked = CheckedListBoxLibros.Items.Item(i)
                aModelo = Split(itemChecked.Row.ItemArray(0), ".")
                qRow = 2
                Do While ws.Cells(qRow, 2).text <> ""
                    InsertaRegistro(Batch, aModelo(0), aModelo(1), aModelo(2))
                    qRow += 1
                Loop
            End If
        Next

        LlamaPrograma(Batch, gsLib)

        Me.TextBoxBatch.Text = Batch
        Me.TextBoxEvento.Text = BuscarValor("SELECT UEVENTO FROM BSSFFNUP WHERE UBATCH = " & Batch)
        Globals.ThisWorkbook.libroEvento = Me.TextBoxEvento.Text
        MsgBox("Se generó el evento: " & Me.TextBoxEvento.Text, vbInformation, "Upload")


    End Sub

    Sub InsertaRegistro(Batch As String, Modelo As String, Ledger As String, Libro As String)
        Dim fSql, vSql As New StringBuilder()
        Dim nota As String = Me.TextNota.Text

        If Trim(nota) = "" Then
            nota = "Batch enviado sin Nota."
        End If

        fSql.Clear() : vSql.Clear()
        fSql.Append(" INSERT INTO BSSFFNUP( ")
        vSql.Append(" VALUES( ")
        fSql.Append("UBATCH    , ") : vSql.AppendFormat(" {0} , ", Batch)         '8P0   Batch
        fSql.Append("UACTIVI   , ") : vSql.AppendFormat("'{0}', ", Me.ComboBoxActividad.SelectedItem(0).ToString)         '10A   Actividad
        fSql.Append("UMODELO   , ") : vSql.AppendFormat("'{0}', ", Modelo)         '10A   Modelo
        fSql.Append("ULEDGER   , ") : vSql.AppendFormat("'{0}', ", Ledger)         '8A    Ledger
        fSql.Append("UBOOK     , ") : vSql.AppendFormat("'{0}', ", Libro)         '10A   Book
        fSql.Append("UUYEAR    , ") : vSql.AppendFormat(" {0:yyyy}, ", Me.FechaDocumento.Value)         '4P0   Año
        fSql.Append("UCURR     , ") : vSql.AppendFormat("'{0}', ", Me.ComboBoxMoneda.SelectedItem(0).ToString)         '3A    Moneda

        fSql.Append("UPERIO    , ") : vSql.AppendFormat(" {0:MM}, ", Me.FechaDocumento.Value)         '2P0   Periodo
        fSql.Append("ULINCOM   , ") : vSql.AppendFormat(" {0} , ", ws.Cells(qRow, 2).Value)         '7P0   Linea Comprob
        fSql.Append("UDOCUM    , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 20).Value)         '30A   Documento
        fSql.Append("UFECDOC   , ") : vSql.AppendFormat(" {0:yyyyMMdd} , ", Me.FechaDocumento.Value)         '8P0   Fecha Documento
        fSql.Append("USEGM01   , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 3).Value)         '16A   Segmento 1
        fSql.Append("USEGM02   , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 4).Value)         '16A   Segmento 2
        'UCURR

        If Libro = "BPENFINCOP" Then
            fSql.Append("USEGM03   , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 5).Value)         '16A   Segmento 3
        Else
            fSql.Append("USEGM03   , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 6).Value)         '16A   Segmento 3
        End If

        fSql.Append("USEGM04   , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 7).Value)         '16A   Segmento 4
        fSql.Append("USEGM05   , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 8).Value)         '16A   Segmento 5
        fSql.Append("USEGM06   , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 9).Value)         '16A   Segmento 6
        fSql.Append("USEGM07   , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 10).Value)         '16A   Segmento 7
        fSql.Append("UCONCEP   , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 16).Value)         '30A   Concepto
        fSql.Append("UREFER1   , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 17).Value)         '35A   Referencial 1
        fSql.Append("UREFER2   , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 18).Value)         '35A   Referencial 2
        fSql.Append("UESTADI   , ") : vSql.AppendFormat(" {0} , ", ValorNumericoCelda(15))         '15P5  Estadistica
        fSql.Append("UREAS     , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 19).Value)         '5A    Codigo Razón
        fSql.Append("UDEBTRA   , ") : vSql.AppendFormat(" {0} , ", ValorNumericoCelda(11))         '15P2  Débito Transacción
        fSql.Append("UCRETRA   , ") : vSql.AppendFormat(" {0} , ", ValorNumericoCelda(12))         '15P2  Crédito Transac
        fSql.Append("UDEBBAS   , ") : vSql.AppendFormat(" {0} , ", ValorNumericoCelda(13))         '15P2  Débito Base
        fSql.Append("UCREBAS   , ") : vSql.AppendFormat(" {0} , ", ValorNumericoCelda(14))         '15P2  Crédito Base
        If qRow = 2 Then
            fSql.Append("UNOTAS    , ") : vSql.AppendFormat("'{0}', ", nota.Replace("'", ""))         '1002A Usuario
        End If
        fSql.Append("UUSER     ) ") : vSql.AppendFormat("'{0}') ", Me.TextBoxUser.Text)         '10A   Usuario
        Try
            Inserta(fSql.Append(vSql.ToString()).ToString())
        Catch ex As Exception
            MsgBox(ex.Message & vbCr & fSql.ToString & vSql.ToString)
            Me.Close()
        End Try

    End Sub


    Private Function ValorNumericoCelda(Columna As Integer) As Double
        If CStr(ws.Cells(qRow, Columna).Value) <> "" Then
            Return CDbl(ws.Cells(qRow, Columna).Value)
        Else
            Return 0
        End If
    End Function

    Function CalcConsec(sId As String, topeMax As String) As String
        Dim fSql As New StringBuilder
        Dim vSql As New StringBuilder
        Dim rs As New ADODB.Recordset
        Dim Consecutivo As String = ""
        Dim Inicial As String = New String("0"c, topeMax.Length() - 1)

        fSql.Clear()
        fSql.Append(" SELECT CCDESC FROM ZCC ")
        fSql.AppendFormat(" WHERE CCTABL = 'SECUENCE'  AND CCCODE = '{0}' ", sId)
        rs.CursorLocation = 2 'adUseServer
        rs.CursorType = 2     'adOpenDynamic
        rs.LockType = 2       'adLockPessimistic
        rs.Open(fSql.ToString(), DB)
        If Not rs.EOF() Then
            Consecutivo = CDbl(rs(0).Value) + 1
            If CDbl(Consecutivo) > CDbl(topeMax) Then
                Consecutivo = Inicial.ToString() & "1"
            Else
                Consecutivo = Inicial.ToString() & Consecutivo
                Consecutivo = Consecutivo.Substring(Consecutivo.Length - topeMax.Length)
            End If
        Else
            rs.Close()
            Consecutivo = Inicial & "1"
            fSql.Clear() : vSql.Clear()
            fSql.Append(" INSERT INTO ZCC( ")
            vSql.Append(" VALUES( ")
            fSql.Append("CCID      , ") : vSql.AppendFormat("'{0}', ", "CC")          '2A    Record ID (CC/CZ)
            fSql.Append("CCTABL    , ") : vSql.AppendFormat("'{0}', ", "SECUENCE")            '8O    Table ID
            fSql.Append("CCCODE    , ") : vSql.AppendFormat("'{0}', ", sId)            '15O   Primary Code
            fSql.Append("CCDESC    ) ") : vSql.AppendFormat("'{0}') ", Consecutivo)            '60G   Description
            fSql.Append(vSql.ToString()).ToString()
            DB.Execute(fSql.ToString())
            Return Consecutivo
        End If
        rs("CCDESC").Value = Consecutivo
        rs.Update()
        rs.Close()

        Return Consecutivo

    End Function


    Private Function SiExiste(Sql As String) As Boolean
        Dim rs As New ADODB.Recordset
        Dim result As Boolean
        rs.Open(Sql, DB)
        result = Not rs.EOF
        rs.Close()
        rs = Nothing
        Return result
    End Function

    Private Function BuscarValor(Sql As String) As String
        Dim result As String = ""
        Dim rs As New ADODB.Recordset
        rs.Open(Sql, DB)
        If Not rs.EOF Then
            result = rs(0).Value
        End If
        rs.Close()
        Return result
    End Function
    Private Sub LlamaPrograma(Batch As String, Biblioteca As String)
        Dim sCmd As String = String.Format("CALL PGM({0}/BSSCFNUP) PARM('{1}' '{2}')", gsLib, Batch, Biblioteca.PadRight(10))
        DB.Execute("{{ " & sCmd & " }} ")
    End Sub
    Private Sub ButtonIngreso_Click(sender As Object, e As EventArgs) Handles ButtonIngreso.Click
        Dim aLib() As String
        Try
            aLib = Split(Me.ComboBoxAmbiente.Text, "-")
            If UBound(aLib) = 0 Then
                MsgBox("Seleccione un ambiente", MsgBoxStyle.Exclamation, "Ingreso")
                Exit Sub
            Else
                gsLib = aLib(1).Trim
            End If

            If Me.TextBoxUser.Text = "" Then
                MsgBox("Falta el usuario", MsgBoxStyle.Exclamation, "Ingreso")
                Exit Sub
            End If
            If Me.TextBoxClave.Text = "" Then
                MsgBox("Falta la clave", MsgBoxStyle.Exclamation, "Ingreso")
                Exit Sub
            End If
            If EjecutaLogin() Then
                Me.GroupBoxExe.Enabled = True
            End If
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
            Application.Exit()
        End Try
    End Sub
    Private Function EjecutaLogin() As Boolean
        'db.Close()
        Try
            Dim s As New StringBuilder
            Dim aLib() As String

            aLib = Split(Me.ComboBoxAmbiente.Text, "-")
            gsLib = aLib(1).Trim

            s.Append("Provider=IBMDA400.DataSource;Data Source=" & My.Settings.AS400 & ";")
            s.AppendFormat("User ID={0};", Me.TextBoxUser.Text)
            s.AppendFormat("Password={0};", Me.TextBoxClave.Text)
            s.Append("Persist Security Info=True;Convert Date Time To Char=TRUE;")
            s.AppendFormat("Default Collection={0};", gsLib)
            s.Append("Force Translate=0;")
            If DB.State = 1 Then
                DB.Close()
            End If
            DB.ConnectionString = s.ToString
            DB.Open()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Ingreso")
            Return False
        End Try

        SaveSetting(My.Application.Info.AssemblyName, "Ingreso", "Usuario", Me.TextBoxUser.Text)
        SaveSetting(My.Application.Info.AssemblyName, "Ingreso", "Ambiente", Me.ComboBoxAmbiente.SelectedIndex)

        If Not VersionApp() Then
            MsgBox("Version Incorrecta del Programa", MsgBoxStyle.Exclamation, My.Application.Info.AssemblyName)
            DB.Close()
            Return False
        End If

        With Globals.ThisWorkbook
            .libroConexionOK = True
            .libroBiblioteca = gsLib
            .libroUsuario = Me.TextBoxUser.Text
            .libroPassword = Me.TextBoxClave.Text
        End With

        bCargando = True
        CargaActividades()
        CargaMonedas()
        bCargando = False

        Return True

    End Function

    Private Function VersionApp() As Boolean
        Dim fSql As String = ""
        Dim locVer As String = ""

        With My.Application.Info.Version
            locVer = String.Format("({0}.{1}.{2})", .Major, .Minor, .Revision)
        End With

        Dim result As Boolean = True
        Dim rs As New ADODB.Recordset
        fSql = "SELECT CCSDSC FROM RFPARAM WHERE CCTABL = 'FINANZASDM' AND CCCODE = 'INSTALL' AND CCCODE2 = '" & My.Application.Info.AssemblyName & "'"
        rs.Open(fSql, DB)
        If Not rs.EOF Then
            If locVer <> rs("CCSDSC").Value Then
                result = False
            End If
        Else
            result = False
        End If
        rs.Close()
        rs = Nothing
        Return result

    End Function
    Private Sub CargaActividades()
        Dim fSql As String
        Dim da As New OleDbDataAdapter()
        Dim ds As New DataSet
        Dim rs As New ADODB.Recordset

        fSql = "SELECT TNTRMD, TNTRMD || ' ' || TNLDES AS DESCRIPCION FROM GTNL01 WHERE TNTRTP = 0 ORDER BY 1 "
        rs.Open(fSql, DB)

        da.Fill(ds, rs, "TABLA")
        rs.ActiveConnection = Nothing
        rs = Nothing

        Me.ComboBoxActividad.DataSource = ds.Tables("TABLA")
        Me.ComboBoxActividad.DisplayMember = "DESCRIPCION"
        Me.ComboBoxActividad.ValueMember = "TNTRMD"

        If valorParametro("ComboBoxActividad") <> "" Then
            If valorParametro("ComboBoxActividad") <> "-1" Then
                bCargando = False
                Me.ComboBoxActividad.SelectedIndex = CInt(valorParametro("ComboBoxActividad"))
                Dim aLibros() As String
                Dim book
                If valorParametro("CheckedListBoxLibros") <> "" Then
                    aLibros = Split(valorParametro("CheckedListBoxLibros"), ",")
                    For Each book In aLibros
                        If book <> "" Then
                            CheckedListBoxLibros.SetItemChecked(CInt(book), True)
                        End If
                    Next
                End If
            End If
        End If

    End Sub

    Private Sub CargaMonedas()
        Dim fSql As String
        Dim da As New OleDbDataAdapter()
        Dim ds As New DataSet
        Dim rs As New ADODB.Recordset

        fSql = "SELECT CURCDE, GCMNY FROM GCM ORDER BY 1 "

        rs.Open(fSql, DB)

        da.Fill(ds, rs, "TABLA")
        rs.ActiveConnection = Nothing
        rs = Nothing

        Me.ComboBoxMoneda.DataSource = ds.Tables(0)
        Me.ComboBoxMoneda.DisplayMember = "GCMNY"
        Me.ComboBoxMoneda.ValueMember = "CURCDE"
        Me.ComboBoxMoneda.SelectedIndex = -1

    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBoxBatch.TextChanged

    End Sub
    Private Sub Label9_Click(sender As Object, e As EventArgs) Handles Label9.Click

    End Sub

    Private Sub ButtonEvento_Click(sender As Object, e As EventArgs) Handles ButtonEvento.Click
        If IsNumeric(Me.TextBoxBatch.Text) Then
            Me.TextBoxEvento.Text = BuscarValor("SELECT UEVENTO FROM BSSFFNUP WHERE UBATCH = " & Me.TextBoxBatch.Text)
        End If
    End Sub



End Class

