﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormCuentaEquivalente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ButtonIngreso = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBoxClave = New System.Windows.Forms.TextBox()
        Me.TextBoxUser = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ComboBoxAmbiente = New System.Windows.Forms.ComboBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.ButtonCuentasEquivalentes = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.CheckedListBoxLibros = New System.Windows.Forms.CheckedListBox()
        Me.ComboBoxActividad = New System.Windows.Forms.ComboBox()
        Me.ButtonCrearComprobante = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.TextNota = New System.Windows.Forms.TextBox()
        Me.FechaDocumento = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TextBoxEvento = New System.Windows.Forms.TextBox()
        Me.GroupBoxExe = New System.Windows.Forms.GroupBox()
        Me.ButtonEvento = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TextBoxBatch = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ComboBoxMoneda = New System.Windows.Forms.ComboBox()
        Me.AS400 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBoxExe.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.AS400)
        Me.GroupBox1.Controls.Add(Me.ButtonIngreso)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TextBoxClave)
        Me.GroupBox1.Controls.Add(Me.TextBoxUser)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.ComboBoxAmbiente)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(526, 105)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Ingreso"
        '
        'ButtonIngreso
        '
        Me.ButtonIngreso.Location = New System.Drawing.Point(442, 76)
        Me.ButtonIngreso.Name = "ButtonIngreso"
        Me.ButtonIngreso.Size = New System.Drawing.Size(75, 23)
        Me.ButtonIngreso.TabIndex = 11
        Me.ButtonIngreso.Text = "Aceptar"
        Me.ButtonIngreso.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(36, 79)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(34, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Clave"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(36, 53)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(43, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Usuario"
        '
        'TextBoxClave
        '
        Me.TextBoxClave.Location = New System.Drawing.Point(111, 76)
        Me.TextBoxClave.Name = "TextBoxClave"
        Me.TextBoxClave.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TextBoxClave.Size = New System.Drawing.Size(118, 20)
        Me.TextBoxClave.TabIndex = 8
        '
        'TextBoxUser
        '
        Me.TextBoxUser.Location = New System.Drawing.Point(112, 50)
        Me.TextBoxUser.Name = "TextBoxUser"
        Me.TextBoxUser.Size = New System.Drawing.Size(118, 20)
        Me.TextBoxUser.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(36, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Ambiente"
        '
        'ComboBoxAmbiente
        '
        Me.ComboBoxAmbiente.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxAmbiente.FormattingEnabled = True
        Me.ComboBoxAmbiente.Items.AddRange(New Object() {"REAL        - ERPLX834F", "PILOTO      - PROLX834F", "GALERAZAMBA - GZPLX834F"})
        Me.ComboBoxAmbiente.Location = New System.Drawing.Point(111, 19)
        Me.ComboBoxAmbiente.Name = "ComboBoxAmbiente"
        Me.ComboBoxAmbiente.Size = New System.Drawing.Size(245, 23)
        Me.ComboBoxAmbiente.TabIndex = 3
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ButtonCuentasEquivalentes)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.CheckedListBoxLibros)
        Me.GroupBox2.Controls.Add(Me.ComboBoxActividad)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 123)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(526, 138)
        Me.GroupBox2.TabIndex = 4
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Actividad"
        '
        'ButtonCuentasEquivalentes
        '
        Me.ButtonCuentasEquivalentes.Location = New System.Drawing.Point(381, 109)
        Me.ButtonCuentasEquivalentes.Name = "ButtonCuentasEquivalentes"
        Me.ButtonCuentasEquivalentes.Size = New System.Drawing.Size(136, 23)
        Me.ButtonCuentasEquivalentes.TabIndex = 10
        Me.ButtonCuentasEquivalentes.Text = "Cuentas Equivalentes"
        Me.ButtonCuentasEquivalentes.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(36, 46)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(35, 13)
        Me.Label5.TabIndex = 8
        Me.Label5.Text = "Libros"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(36, 27)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Actividad"
        '
        'CheckedListBoxLibros
        '
        Me.CheckedListBoxLibros.FormattingEnabled = True
        Me.CheckedListBoxLibros.Location = New System.Drawing.Point(114, 46)
        Me.CheckedListBoxLibros.Name = "CheckedListBoxLibros"
        Me.CheckedListBoxLibros.Size = New System.Drawing.Size(406, 49)
        Me.CheckedListBoxLibros.TabIndex = 6
        '
        'ComboBoxActividad
        '
        Me.ComboBoxActividad.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBoxActividad.FormattingEnabled = True
        Me.ComboBoxActividad.Location = New System.Drawing.Point(113, 19)
        Me.ComboBoxActividad.Name = "ComboBoxActividad"
        Me.ComboBoxActividad.Size = New System.Drawing.Size(407, 23)
        Me.ComboBoxActividad.TabIndex = 5
        '
        'ButtonCrearComprobante
        '
        Me.ButtonCrearComprobante.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonCrearComprobante.Location = New System.Drawing.Point(361, 40)
        Me.ButtonCrearComprobante.Name = "ButtonCrearComprobante"
        Me.ButtonCrearComprobante.Size = New System.Drawing.Size(156, 29)
        Me.ButtonCrearComprobante.TabIndex = 5
        Me.ButtonCrearComprobante.Text = "Crear Comprobante"
        Me.ButtonCrearComprobante.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.TextNota)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 267)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(524, 162)
        Me.GroupBox3.TabIndex = 6
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Nota"
        '
        'TextNota
        '
        Me.TextNota.Location = New System.Drawing.Point(6, 19)
        Me.TextNota.MaxLength = 1000
        Me.TextNota.Multiline = True
        Me.TextNota.Name = "TextNota"
        Me.TextNota.Size = New System.Drawing.Size(511, 136)
        Me.TextNota.TabIndex = 0
        '
        'FechaDocumento
        '
        Me.FechaDocumento.Checked = False
        Me.FechaDocumento.CustomFormat = "yyyy-MM-dd"
        Me.FechaDocumento.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.FechaDocumento.Location = New System.Drawing.Point(61, 14)
        Me.FechaDocumento.MaxDate = New Date(2030, 12, 31, 0, 0, 0, 0)
        Me.FechaDocumento.MinDate = New Date(2010, 1, 1, 0, 0, 0, 0)
        Me.FechaDocumento.Name = "FechaDocumento"
        Me.FechaDocumento.Size = New System.Drawing.Size(94, 20)
        Me.FechaDocumento.TabIndex = 7
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(6, 16)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(37, 13)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Fecha"
        '
        'TextBoxEvento
        '
        Me.TextBoxEvento.Location = New System.Drawing.Point(359, 14)
        Me.TextBoxEvento.Name = "TextBoxEvento"
        Me.TextBoxEvento.Size = New System.Drawing.Size(158, 20)
        Me.TextBoxEvento.TabIndex = 0
        '
        'GroupBoxExe
        '
        Me.GroupBoxExe.Controls.Add(Me.ButtonEvento)
        Me.GroupBoxExe.Controls.Add(Me.Label9)
        Me.GroupBoxExe.Controls.Add(Me.TextBoxBatch)
        Me.GroupBoxExe.Controls.Add(Me.Label8)
        Me.GroupBoxExe.Controls.Add(Me.TextBoxEvento)
        Me.GroupBoxExe.Controls.Add(Me.Label7)
        Me.GroupBoxExe.Controls.Add(Me.ComboBoxMoneda)
        Me.GroupBoxExe.Controls.Add(Me.Label6)
        Me.GroupBoxExe.Controls.Add(Me.FechaDocumento)
        Me.GroupBoxExe.Controls.Add(Me.ButtonCrearComprobante)
        Me.GroupBoxExe.Enabled = False
        Me.GroupBoxExe.Location = New System.Drawing.Point(12, 435)
        Me.GroupBoxExe.Name = "GroupBoxExe"
        Me.GroupBoxExe.Size = New System.Drawing.Size(526, 77)
        Me.GroupBoxExe.TabIndex = 10
        Me.GroupBoxExe.TabStop = False
        Me.GroupBoxExe.Text = "Generar Upload"
        '
        'ButtonEvento
        '
        Me.ButtonEvento.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonEvento.Location = New System.Drawing.Point(216, 40)
        Me.ButtonEvento.Name = "ButtonEvento"
        Me.ButtonEvento.Size = New System.Drawing.Size(90, 29)
        Me.ButtonEvento.TabIndex = 14
        Me.ButtonEvento.Text = "Evento"
        Me.ButtonEvento.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(169, 19)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(35, 13)
        Me.Label9.TabIndex = 13
        Me.Label9.Text = "Batch"
        '
        'TextBoxBatch
        '
        Me.TextBoxBatch.Location = New System.Drawing.Point(216, 14)
        Me.TextBoxBatch.Name = "TextBoxBatch"
        Me.TextBoxBatch.Size = New System.Drawing.Size(90, 20)
        Me.TextBoxBatch.TabIndex = 12
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(312, 19)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(41, 13)
        Me.Label8.TabIndex = 11
        Me.Label8.Text = "Evento"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(9, 45)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(46, 13)
        Me.Label7.TabIndex = 10
        Me.Label7.Text = "Moneda"
        '
        'ComboBoxMoneda
        '
        Me.ComboBoxMoneda.FormattingEnabled = True
        Me.ComboBoxMoneda.Location = New System.Drawing.Point(61, 45)
        Me.ComboBoxMoneda.Name = "ComboBoxMoneda"
        Me.ComboBoxMoneda.Size = New System.Drawing.Size(94, 21)
        Me.ComboBoxMoneda.TabIndex = 9
        '
        'AS400
        '
        Me.AS400.AutoSize = True
        Me.AS400.Location = New System.Drawing.Point(372, 21)
        Me.AS400.Name = "AS400"
        Me.AS400.Size = New System.Drawing.Size(45, 13)
        Me.AS400.TabIndex = 12
        Me.AS400.Text = "Label10"
        '
        'FormCuentaEquivalente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(549, 557)
        Me.Controls.Add(Me.GroupBoxExe)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "FormCuentaEquivalente"
        Me.Text = "Comprobantes Manuales"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBoxExe.ResumeLayout(False)
        Me.GroupBoxExe.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TextBoxClave As System.Windows.Forms.TextBox
    Friend WithEvents TextBoxUser As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxAmbiente As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents CheckedListBoxLibros As System.Windows.Forms.CheckedListBox
    Friend WithEvents ComboBoxActividad As System.Windows.Forms.ComboBox
    Friend WithEvents ButtonCrearComprobante As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents TextNota As System.Windows.Forms.TextBox
    Friend WithEvents ButtonCuentasEquivalentes As System.Windows.Forms.Button
    Friend WithEvents FechaDocumento As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TextBoxEvento As System.Windows.Forms.TextBox
    Friend WithEvents ButtonIngreso As System.Windows.Forms.Button
    Friend WithEvents GroupBoxExe As System.Windows.Forms.GroupBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents ComboBoxMoneda As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents TextBoxBatch As System.Windows.Forms.TextBox
    Friend WithEvents ButtonEvento As System.Windows.Forms.Button
    Friend WithEvents AS400 As Label
End Class
