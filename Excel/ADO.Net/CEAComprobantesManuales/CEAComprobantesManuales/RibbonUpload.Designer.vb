﻿Partial Class RibbonUpload
    Inherits Microsoft.Office.Tools.Ribbon.RibbonBase

    <System.Diagnostics.DebuggerNonUserCode()> _
   Public Sub New(ByVal container As System.ComponentModel.IContainer)
        MyClass.New()

        'Requerido para la compatibilidad con el Diseñador de composiciones de clases Windows.Forms
        If (container IsNot Nothing) Then
            container.Add(Me)
        End If

    End Sub

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New()
        MyBase.New(Globals.Factory.GetRibbonFactory())

        'El Diseñador de componentes requiere esta llamada.
        InitializeComponent()

    End Sub

    'Component invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de componentes
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de componentes requiere el siguiente procedimiento
    'Se puede modificar usando el Diseñador de componentes.
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Tab1 = Me.Factory.CreateRibbonTab
        Me.Group1 = Me.Factory.CreateRibbonGroup
        Me.ButtonCuentasEquivalentes = Me.Factory.CreateRibbonButton
        Me.LabelEvento = Me.Factory.CreateRibbonLabel
        Me.ButtonAcercaDe = Me.Factory.CreateRibbonButton
        Me.Tab1.SuspendLayout()
        Me.Group1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Tab1
        '
        Me.Tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office
        Me.Tab1.Groups.Add(Me.Group1)
        Me.Tab1.Label = "TabAddIns"
        Me.Tab1.Name = "Tab1"
        '
        'Group1
        '
        Me.Group1.Items.Add(Me.ButtonCuentasEquivalentes)
        Me.Group1.Items.Add(Me.LabelEvento)
        Me.Group1.Items.Add(Me.ButtonAcercaDe)
        Me.Group1.Label = "Upload CEA"
        Me.Group1.Name = "Group1"
        '
        'ButtonCuentasEquivalentes
        '
        Me.ButtonCuentasEquivalentes.Label = "Upload CEA"
        Me.ButtonCuentasEquivalentes.Name = "ButtonCuentasEquivalentes"
        '
        'LabelEvento
        '
        Me.LabelEvento.Label = " "
        Me.LabelEvento.Name = "LabelEvento"
        '
        'ButtonAcercaDe
        '
        Me.ButtonAcercaDe.Label = "Acerca De"
        Me.ButtonAcercaDe.Name = "ButtonAcercaDe"
        '
        'RibbonUpload
        '
        Me.Name = "RibbonUpload"
        Me.RibbonType = "Microsoft.Excel.Workbook"
        Me.Tabs.Add(Me.Tab1)
        Me.Tab1.ResumeLayout(False)
        Me.Tab1.PerformLayout()
        Me.Group1.ResumeLayout(False)
        Me.Group1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Tab1 As Microsoft.Office.Tools.Ribbon.RibbonTab
    Friend WithEvents Group1 As Microsoft.Office.Tools.Ribbon.RibbonGroup
    Friend WithEvents ButtonCuentasEquivalentes As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents ButtonAcercaDe As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents LabelEvento As Microsoft.Office.Tools.Ribbon.RibbonLabel
End Class

Partial Class ThisRibbonCollection

    <System.Diagnostics.DebuggerNonUserCode()> _
    Friend ReadOnly Property RibbonUpload() As RibbonUpload
        Get
            Return Me.GetRibbon(Of RibbonUpload)()
        End Get
    End Property
End Class
