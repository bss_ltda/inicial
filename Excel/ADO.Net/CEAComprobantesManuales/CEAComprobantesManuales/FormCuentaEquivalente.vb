﻿Imports IBM.Data.DB2.iSeries
Imports System.Collections.Specialized
Public Class FormCuentaEquivalente
    Dim db As iDB2Connection = New iDB2Connection
    Dim bCargando As Boolean
    Dim ws As Excel.Worksheet
    Dim gsLib As String
    Dim SegmentosCEAColeccion As Collection
    Dim CuentasEquivalentesDic As StringDictionary
    Dim qRow As Integer
    Dim bErrores As Boolean
    Private Sub FormCuentaEquivalente_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ws = Globals.ThisWorkbook.ActiveSheet
        Me.TextBoxUser.Text = GetSetting(My.Application.Info.AssemblyName, "Ingreso", "Usuario", "")
        Globals.ThisWorkbook.libroAmbiente = CInt(GetSetting(My.Application.Info.AssemblyName, "Ingreso", "Ambiente", "-1"))
        If Not RestauraConexion() Then
            Me.Close()
        End If
        Me.TextBoxClave.Focus()
    End Sub
    Private Function RestauraConexion() As Boolean
        With Globals.ThisWorkbook
            If .libroAmbiente <> -1 Then
                Me.ComboBoxAmbiente.SelectedIndex = .libroAmbiente
            End If
            If .libroConexionOK Then
                Me.TextBoxUser.Text = .libroUsuario
                Me.TextBoxClave.Text = .libroPassword
                gsLib = .libroBiblioteca
                If Not EjecutaLogin() Then
                    Return False
                End If
            Else
                Return True
            End If
            If .libroActividad <> -1 Then
                Me.ComboBoxActividad.SelectedIndex = .libroActividad
                For Each i In .libroLibros
                    Me.CheckedListBoxLibros.SetItemCheckState(i, CheckState.Checked)
                Next
            End If
        End With
        Return True
    End Function
    Private Sub ComboBoxActividad_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBoxActividad.SelectedIndexChanged
        If bCargando Then
            Exit Sub
        End If
        Dim cm As iDB2Command = New iDB2Command
        Dim da As iDB2DataAdapter
        Dim ds As DataSet
        Dim fSql As String
        fSql = "SELECT TRIM(TDJNMD) || '.' || TRIM(TDLDGR) || '.' || TRIM(TDBOOK) TDLDGR, TDBOOK FROM gtD WHERE TDTRMD = '" & sender.SelectedValue & "'"
        cm.CommandText = fSql
        cm.CommandType = CommandType.Text
        cm.Connection = db
        da = New iDB2DataAdapter(cm)
        ds = New DataSet()
        da.Fill(ds)
        Me.CheckedListBoxLibros.DataSource = ds.Tables(0)
        Me.CheckedListBoxLibros.DisplayMember = "TDBOOK"
        Me.CheckedListBoxLibros.ValueMember = "TDLDGR"
    End Sub
    Private Function comparaTitulo(ByRef qCol As Integer, qTit As String) As String
        If qCol = -1 Then Return ""
        Dim a, b As String
        a = ws.Cells(1, qCol).text
        a = a.Replace(" ", "").Replace(vbCr, "").Replace(vbLf, "")
        b = qTit.Replace(" ", "").Replace(vbCr, "").Replace(vbLf, "")

        If a <> b Then
            qCol = -1
            Return "Columna " & qCol & " No corresponde. Debe ser " & qTit & vbCr
        Else
            qCol += 1
            Return ""
        End If
    End Function
    Private Sub RevisaSegmento(ByRef msg As String, NumeroColumna As Integer, NumeroSegmento As Integer)
        Dim Codigo As String = ws.Cells(qRow, NumeroColumna).text
        If Codigo.Trim() = "" Then
            Exit Sub
        End If
        Dim texto As String = ws.Cells(1, NumeroColumna).text
        Dim fSql As String
        fSql = " SELECT SVLDES FROM GSVL01  "
        fSql &= " WHERE "
        fSql &= String.Format("SVSGMN = '{0}'", SegmentosCEAColeccion.Item(NumeroSegmento).ToString())
        fSql &= String.Format(" AND SVSGVL = '{0}'", Codigo.Trim())
        If Not SiExiste(fSql) Then
            bErrores = True
            msg &= IIf(msg = "", "", vbLf) & "No encontró " & texto
        End If
    End Sub
    Private Sub SegmentosCEA()
        Dim fSql As String
        fSql = " SELECT TRIM(CCCODE) FROM RFPARAM  "
        fSql &= " WHERE CCTABL = 'FNZSEGMENTS'  AND CCCODE4 <> '' "
        fSql &= " ORDER BY CCCODEN ASC "
        SegmentosCEAColeccion = New Collection
        SegmentosCEAColeccion.Clear()
        Dim cm As iDB2Command = New iDB2Command(fSql, db)
        Using rs As iDB2DataReader = cm.ExecuteReader()
            Do While rs.Read()
                SegmentosCEAColeccion.Add(rs(0))
            Loop
            rs.Close()
        End Using
    End Sub
    Private Function CodigoSegmento(NumeroSegmento As Integer) As String
        Dim fSql As String
        fSql = " SELECT CCCODE FROM RFPARAM  "
        fSql &= " WHERE CCTABL = 'FNZSEGMENTS'  "
        fSql &= String.Format(" AND CCCODEN = {0}", NumeroSegmento)
        Return LookUp01(fSql)
    End Function
    Private Function LookUp01(Sql As String) As String
        Dim cm As iDB2Command = New iDB2Command(Sql, db)
        Dim s As String = ""
        Using rs As iDB2DataReader = cm.ExecuteReader()
            If rs.Read() Then
                s = rs(0)
            End If
            rs.Close()
        End Using
        Return s
    End Function
    Private Sub Inserta(Sql As String)
        '"Insert into [ordering].[dbo].[Customer] ([FirstName],[LastName]) values ('" + TextBox1.Text + "','" + TextBox2.Text + "')"
        Dim cmd As iDB2Command = New iDB2Command(Sql, db)
        cmd.ExecuteNonQuery()
    End Sub
    Private Sub ComoRecordSet(Sql As String)
        Dim cm As iDB2Command = New iDB2Command(Sql, db)
        Using rs As iDB2DataReader = cm.ExecuteReader()
            Do While rs.Read()
                Dim a As String = rs(0)
            Loop
            rs.Close()
        End Using
    End Sub
    Private Function RevisaColumnas() As Boolean
        Dim qColumna As Integer = 1
        Dim m As String = ""
        m += comparaTitulo(qColumna, "RESULTADO")                            '01
        m += comparaTitulo(qColumna, "LINEA")                                '02
        m += comparaTitulo(qColumna, "COMPAÑÍA")                             '03
        m += comparaTitulo(qColumna, "CENTRO DE COSTO")                      '04
        m += comparaTitulo(qColumna, "Cuenta BPEFINCOP")                     '05
        m += comparaTitulo(qColumna, "Cuenta NIIF")                          '06
        m += comparaTitulo(qColumna, "PRODUCTO")                             '07
        m += comparaTitulo(qColumna, "APA")                                  '08
        m += comparaTitulo(qColumna, "LINEA PRODUCTO")                       '09
        m += comparaTitulo(qColumna, "CADENA")                               '10
        m += comparaTitulo(qColumna, "Transacciones Debito")                 '11
        m += comparaTitulo(qColumna, "Transacciones Credito")                '12
        m += comparaTitulo(qColumna, "Libro Debito")                         '13
        m += comparaTitulo(qColumna, "Libro Credito")                        '14
        m += comparaTitulo(qColumna, "Estadistica")                          '15
        m += comparaTitulo(qColumna, "Descripcion Linea de Actividad")       '16
        m += comparaTitulo(qColumna, "Referencia 1")                         '17
        m += comparaTitulo(qColumna, "Referencia 2")                         '18
        m += comparaTitulo(qColumna, "Código de Razón")                      '19
        m += comparaTitulo(qColumna, "Documento de Referencia")              '20
        m += comparaTitulo(qColumna, "Fecha")                                '21
        If m <> "" Then
            MsgBox(m, MsgBoxStyle.Exclamation, "Columnas")
        End If
        Return m = ""
    End Function
    Private Sub ButtonCrearComprobante_Click(sender As Object, e As EventArgs) Handles ButtonCrearComprobante.Click
        Me.Enabled = False
        GuardaParametros()
        If Not RevisaColumnas() Then
            Me.Enabled = True
            Exit Sub
        End If
        Dim msg, sep As String
        Dim qColumna As Integer = 1
        Dim Debitos, Creditos, LineaDebito, LineaCredito As Double
        Dim totLibros As Integer
        Dim SegmentoCIA As String
        With Globals.ThisWorkbook
            .libroAmbiente = Me.ComboBoxAmbiente.SelectedIndex
            .libroActividad = Me.ComboBoxActividad.SelectedIndex
        End With
        If Not Me.FechaDocumento.Checked Then
            MsgBox("Seleccione la Fecha del documento.", MsgBoxStyle.Exclamation, "Ingreso")
            Me.Enabled = True
            Exit Sub
        End If
        If Me.ComboBoxMoneda.SelectedIndex = -1 Then
            MsgBox("Seleccione Monea del documento.", MsgBoxStyle.Exclamation, "Ingreso")
            Me.Enabled = True
            Exit Sub
        End If
        totLibros = Me.CheckedListBoxLibros.CheckedItems.Count
        If totLibros = 0 Then
            MsgBox("Seleccione libro o libros.", MsgBoxStyle.Exclamation, "Validacion")
            Me.Enabled = True
            Exit Sub
        End If
        'SEGMENTOS BRINSA
        '				1	Compania
        '				2	Centro de Costo
        '				3	Cuenta
        '				4	Producto
        '				5	APA
        '				6	Linea Producto
        '				7	Cadena
        SegmentosCEA()
        qRow = 2
        sep = ""
        Debitos = 0
        Creditos = 0
        LineaDebito = 0
        LineaCredito = 0
        bErrores = False
        SegmentoCIA = ws.Cells(2, 3).Text                                                           '	COMPAÑÍA
        Do While ws.Cells(qRow, 3).text <> ""
            msg = ""
            ws.Cells(qRow, 2).value = qRow - 1                                                      '	LINEA
            ws.Cells(qRow, 3).value = SegmentoCIA
            RevisaSegmento(msg, 3, 1)                                                               '	COMPAÑÍA (1)
            RevisaSegmento(msg, 4, 2)                                                               '	centro de costo (2)
            ws.Cells(qRow, 21).value = String.Format("{0:yyyyMMdd}", Me.FechaDocumento.Value)       '	Fecha

            RevisaSegmento(msg, 5, 3)                                                               '	Cuenta BPEFINCOP (3)
            If ws.Cells(qRow, 6).text <> "" Then
                RevisaSegmento(msg, 6, 3)                                                           '	Cuenta NIIF (3)
            End If
            RevisaSegmento(msg, 7, 4)                                                               '	PRODUCTO (4)
            RevisaSegmento(msg, 8, 5)                                                               '	APA (5)
            RevisaSegmento(msg, 9, 6)                                                               '	LINEA PRODUCTO (6)
            RevisaSegmento(msg, 10, 7)                                                              '	CADENA (7)
            If msg <> "" Then
                ws.Cells(qRow, 1).value = msg
            Else
                ws.Cells(qRow, 1).value = "OK"
            End If
            Debitos += ws.Cells(qRow, 11).value                                                     '	11	TransaccionesDebito
            Creditos += ws.Cells(qRow, 12).value                                                    '	12	Transacciones Credito
            LineaDebito += ws.Cells(qRow, 13).value                                                 '	13	Libro Debito
            LineaCredito += ws.Cells(qRow, 14).value                                                '	14	Libro Credito
            qRow += 1
        Loop
        If Debitos <> Creditos Then
            msg = "Debitos y Creditos diferentes"
        End If
        If LineaDebito <> LineaCredito Then
            msg = "Linea Debito y Linea Credito diferentes"
        End If
        If bErrores Then
            MsgBox("Revise Inconsistencias.", MsgBoxStyle.Exclamation, "Validacion Datos")
            Me.Enabled = True
            Me.Close()
            Exit Sub
        End If
        EnviaComprobante()
        ws.Cells(1, 1).Select()
        Me.Enabled = True
    End Sub
    Private Sub ButtonCuentasEquivalentes_Click(sender As Object, e As EventArgs) Handles ButtonCuentasEquivalentes.Click
        Me.Enabled = False
        GuardaParametros()
        If Not RevisaColumnas() Then
            Me.Enabled = True
            Exit Sub
        End If
        CuentasEquivalentes()
        qRow = 2
        Do While ws.Cells(qRow, 2).text <> ""
            If CuentasEquivalentesDic(ws.Cells(qRow, 5).value) <> Nothing Then
                ws.Cells(qRow, 6).value = CuentasEquivalentesDic(ws.Cells(qRow, 5).value)
            Else
                ws.Cells(qRow, 6).value = ws.Cells(qRow, 5).value
            End If
            qRow += 1
        Loop
        Me.Enabled = True
        Me.Close()
    End Sub
    Private Sub GuardaParametros()
        With Globals.ThisWorkbook
            .libroAmbiente = Me.ComboBoxAmbiente.SelectedIndex
            .libroActividad = Me.ComboBoxActividad.SelectedIndex
        End With
        Globals.ThisWorkbook.libroLibros.Clear()
        For Each i In Me.CheckedListBoxLibros.CheckedIndices
            Globals.ThisWorkbook.libroLibros.Add(i)
        Next
    End Sub
    Private Sub CuentasEquivalentes()
        Dim fSql As String
        CuentasEquivalentesDic = New StringDictionary
        fSql = " SELECT CCUEORI, CCUEEQU FROM BSSFFNCUE ORDER BY 1 "
        Try
            Dim cm As iDB2Command = New iDB2Command(fSql, db)
            Using rs As iDB2DataReader = cm.ExecuteReader()
                Do While rs.Read()
                    CuentasEquivalentesDic.Add(rs("CCUEORI").ToString.Trim, rs("CCUEEQU").ToString.Trim)
                Loop
                rs.Close()
            End Using
        Catch ex As iDB2Exception
            MsgBox(ex.MessageDetails.ToString)
        End Try
    End Sub
    Sub EnviaComprobante()
        Dim itemChecked As DataRowView
        Dim i As Integer
        Dim aModelo() As String
        Dim Batch As String = CalcConsec("BSSFFNUP", "99999999")
        For i = 0 To (CheckedListBoxLibros.Items.Count - 1)
            If CheckedListBoxLibros.GetItemChecked(i) = True Then
                itemChecked = CheckedListBoxLibros.Items.Item(i)
                aModelo = Split(itemChecked.Row.ItemArray(0), ".")
                qRow = 2
                Do While ws.Cells(qRow, 2).text <> ""
                    InsertaRegistro(Batch, aModelo(0), aModelo(1), aModelo(2))
                    qRow += 1
                Loop
            End If
        Next
        LlamaPrograma(Batch, gsLib)
        Me.TextBoxEvento.Text = BuscarValor("SELECT UEVENTO FROM BSSFFNUP WHERE UBATCH = " & Batch)
        MsgBox("Se generó el evento: " & Me.TextBoxEvento.Text, vbInformation, "Upload")
    End Sub
    Sub InsertaRegistro(Batch As String, Modelo As String, Ledger As String, Libro As String)
        Dim cmd As iDB2Command = New iDB2Command()
        Dim fSql, vSql As New StringBuilder()
        Dim periodo As String
        Dim nota As String = Me.TextNota.Text
        periodo = ws.Cells(qRow, 21).Value
        fSql.Clear() : vSql.Clear()
        fSql.Append(" INSERT INTO BSSFFNUP( ")
        vSql.Append(" VALUES( ")
        fSql.Append("UBATCH    , ") : vSql.AppendFormat(" {0} , ", Batch)         '8P0   Batch
        fSql.Append("UACTIVI   , ") : vSql.AppendFormat("'{0}', ", Me.ComboBoxActividad.SelectedItem(0).ToString)         '10A   Actividad
        fSql.Append("UMODELO   , ") : vSql.AppendFormat("'{0}', ", Modelo)         '10A   Modelo
        fSql.Append("ULEDGER   , ") : vSql.AppendFormat("'{0}', ", Ledger)         '8A    Ledger
        fSql.Append("UBOOK     , ") : vSql.AppendFormat("'{0}', ", Libro)         '10A   Book
        fSql.Append("UUYEAR    , ") : vSql.AppendFormat(" {0:yyyy}, ", Me.FechaDocumento.Value)         '4P0   Año
        fSql.Append("UCURR     , ") : vSql.AppendFormat("'{0}', ", Me.ComboBoxMoneda.SelectedItem(0).ToString)         '3A    Moneda
        fSql.Append("UPERIO    , ") : vSql.AppendFormat(" {0:MM}, ", Me.FechaDocumento.Value)         '2P0   Periodo
        fSql.Append("ULINCOM   , ") : vSql.AppendFormat(" {0} , ", ws.Cells(qRow, 2).Value)         '7P0   Linea Comprob
        fSql.Append("UDOCUM    , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 20).Value)         '30A   Documento
        fSql.Append("UFECDOC   , ") : vSql.AppendFormat(" {0} , ", ws.Cells(qRow, 21).Value)         '8P0   Fecha Documento
        fSql.Append("USEGM01   , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 3).Value)         '16A   Segmento 1
        fSql.Append("USEGM02   , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 4).Value)         '16A   Segmento 2
        'UCURR
        If Libro = "BPENFINCOP" Then
            fSql.Append("USEGM03   , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 5).Value)         '16A   Segmento 3
        Else
            fSql.Append("USEGM03   , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 6).Value)         '16A   Segmento 3
        End If
        fSql.Append("USEGM04   , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 7).Value)         '16A   Segmento 4
        fSql.Append("USEGM05   , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 8).Value)         '16A   Segmento 5
        fSql.Append("USEGM06   , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 9).Value)         '16A   Segmento 6
        fSql.Append("USEGM07   , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 10).Value)         '16A   Segmento 7
        fSql.Append("UCONCEP   , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 16).Value)         '30A   Concepto
        fSql.Append("UREFER1   , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 17).Value)         '35A   Referencial 1
        fSql.Append("UREFER2   , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 18).Value)         '35A   Referencial 2
        fSql.Append("UESTADI   , ") : vSql.AppendFormat(" {0} , ", ValorNumericoCelda(15))         '15P5  Estadistica
        fSql.Append("UREAS     , ") : vSql.AppendFormat("'{0}', ", ws.Cells(qRow, 19).Value)         '5A    Codigo Razón
        fSql.Append("UDEBTRA   , ") : vSql.AppendFormat(" {0} , ", ValorNumericoCelda(11))         '15P2  Débito Transacción
        fSql.Append("UCRETRA   , ") : vSql.AppendFormat(" {0} , ", ValorNumericoCelda(12))         '15P2  Crédito Transac
        fSql.Append("UDEBBAS   , ") : vSql.AppendFormat(" {0} , ", ValorNumericoCelda(13))         '15P2  Débito Base
        fSql.Append("UCREBAS   , ") : vSql.AppendFormat(" {0} , ", ValorNumericoCelda(14))         '15P2  Crédito Base
        If qRow = 2 Then
            fSql.Append("UNOTAS    , ") : vSql.AppendFormat("'{0}', ", nota.Replace("'", ""))         '1002A Usuario
        End If
        fSql.Append("UUSER     ) ") : vSql.AppendFormat("'{0}') ", Me.TextBoxUser.Text)         '10A   Usuario
        Try
            cmd.CommandText = fSql.Append(vSql.ToString()).ToString()
            cmd.Connection = db
            cmd.ExecuteNonQuery()
        Catch ex As iDB2Exception
            MsgBox(ex.MessageDetails & vbCr & fSql.ToString & vSql.ToString)
            Me.Close()
        End Try
    End Sub
    Private Function ValorNumericoCelda(Columna As Integer) As Double
        If CStr(ws.Cells(qRow, Columna).Value) <> "" Then
            Return CDbl(ws.Cells(qRow, Columna).Value)
        Else
            Return 0
        End If
    End Function

    Function CalcConsec(sId As String, topeMax As String) As String
        Dim fSql As New StringBuilder
        Dim vSql As New StringBuilder
        Dim cmd As iDB2Command = New iDB2Command()
        Dim Consecutivo As String = ""
        Dim Inicial As String = New String("0"c, topeMax.Length() - 1)
        fSql.Append("SET TRANSACTION ISOLATION LEVEL REPEATABLE READ ")
        cmd.CommandText = fSql.ToString()
        cmd.Connection = db
        cmd.ExecuteNonQuery()
        fSql.Clear()
        fSql.Append(" SELECT DIGITS( DEC( TRIM(CCDESC), 10, 0 ) + 1 ) AS CONSEC FROM ZCC ")
        fSql.AppendFormat(" WHERE CCTABL = 'SECUENCE'  AND CCCODE = '{0}' ", sId)
        cmd.CommandText = fSql.ToString()
        Using rs As iDB2DataReader = cmd.ExecuteReader()
            If rs.Read() Then
                Consecutivo = rs(0)
            End If
        End Using
        If Consecutivo = "" Then
            Consecutivo = Inicial & "1"
            fSql.Clear() : vSql.Clear()
            fSql.Append(" INSERT INTO ZCC( ")
            vSql.Append(" VALUES( ")
            fSql.Append("CCID      , ") : vSql.AppendFormat("'{0}', ", "CC")          '2A    Record ID (CC/CZ)
            fSql.Append("CCTABL    , ") : vSql.AppendFormat("'{0}', ", "SECUENCE")            '8O    Table ID
            fSql.Append("CCCODE    , ") : vSql.AppendFormat("'{0}', ", sId)            '15O   Primary Code
            fSql.Append("CCDESC    ) ") : vSql.AppendFormat("'{0}') ", Consecutivo)            '60G   Description
            cmd.CommandText = fSql.Append(vSql.ToString()).ToString()
            cmd.ExecuteNonQuery()
            fSql.Clear()
            fSql.Append("COMMIT")
            cmd.CommandText = fSql.ToString()
            cmd.ExecuteNonQuery()
            Return Consecutivo
        End If
        If CDbl(Consecutivo) > CDbl(topeMax) Then
            Consecutivo = Inicial & "1"
        Else
            Consecutivo = Consecutivo.Substring(Consecutivo.Length - topeMax.Length)
        End If
        fSql.Clear()
        fSql.AppendFormat(" UPDATE ZCC SET CCDESC = '{0}' ", Consecutivo)
        fSql.AppendFormat(" WHERE CCTABL = 'SECUENCE'  AND CCCODE = '{0}'  ", sId)
        cmd.CommandText = fSql.ToString()
        cmd.ExecuteNonQuery()
        fSql.Clear()
        fSql.Append("COMMIT")
        cmd.CommandText = fSql.ToString()
        cmd.ExecuteNonQuery()
        Return Consecutivo
    End Function
    Function ExecuteSQL(Sql As String) As Integer
        Dim cmd As iDB2Command = New iDB2Command()
        Try
            cmd.CommandText = Sql
            cmd.Connection = db
            Return cmd.ExecuteNonQuery()
        Catch ex As iDB2Exception
        End Try
    End Function
    Private Function SiExiste(Sql As String) As Boolean
        Dim cm As iDB2Command = New iDB2Command(Sql, db)
        Using rs As iDB2DataReader = cm.ExecuteReader()
            If rs.HasRows Then
                Return True
            End If
        End Using
        Return False
    End Function
    Private Function BuscarValor(Sql As String)
        Dim cm As iDB2Command = New iDB2Command(Sql, db)
        Using rs As iDB2DataReader = cm.ExecuteReader()
            If rs.Read() Then
                Return rs(0)
            End If
        End Using
        Return ""
    End Function
    Private Sub LlamaPrograma(Batch As String, Biblioteca As String)
        Dim cmd As iDB2Command = New iDB2Command()
        cmd.Connection = db
        cmd.CommandText = String.Format("Call BSSCFNUP('{0}', '{1}')", Batch, Biblioteca.PadRight(10))
        cmd.CommandType = CommandType.Text
        cmd.ExecuteReader()
    End Sub
    Private Sub ButtonIngreso_Click(sender As Object, e As EventArgs) Handles ButtonIngreso.Click
        'Para generar el instalador, en My Project en Firma se genero un certificado de prueba con clave VSTO
        'Luego desde ahí se puede Publicar para que genere el instalador.
        Dim aLib() As String
        Try
            aLib = Split(Me.ComboBoxAmbiente.Text, "-")
            If UBound(aLib) = 0 Then
                MsgBox("Seleccione un ambiente", MsgBoxStyle.Exclamation, "Ingreso")
                Exit Sub
            Else
                gsLib = aLib(1).Trim
            End If

            If Me.TextBoxUser.Text = "" Then
                MsgBox("Falta el usuario", MsgBoxStyle.Exclamation, "Ingreso")
                Exit Sub
            End If
            If Me.TextBoxClave.Text = "" Then
                MsgBox("Falta la clave", MsgBoxStyle.Exclamation, "Ingreso")
                Exit Sub
            End If
            'MsgBox("Va a hacer login")
            EjecutaLogin()
        Catch ex As Exception
            MsgBox(ex.Message.ToString)
            Application.Exit()
        End Try
    End Sub
    Private Function EjecutaLogin() As Boolean
        'db.Close()
        Try
            Dim CONN As New ADODB.Connection
            CONN.Open("Provider=IBMDA400.DataSource;Data Source=192.168.21.135;User ID=APLLX;Password=LXAPL;Persist Security Info=True;Convert Date Time To Char=TRUE;Application Name=DFU001;Default Collection=ERPLX834F;Force Translate=0;")
            'MsgBox(CONN.State)
            CONN.Close()
            Dim s As String = "Data Source=192.168.21.135;Default Collection={0};Persist Security Info=True;User ID={1};Password={2}"
            s = String.Format(s, gsLib, Me.TextBoxUser.Text, Me.TextBoxClave.Text)
            'MsgBox(s)
            Try
                db.ConnectionString = s
            Catch ex As iDB2InvalidConnectionStringException
                MsgBox(ex.Message.ToString)
            End Try
            'MsgBox(db.ConnectionString)
            db.Open()
        Catch ex As iDB2CommErrorException
            MsgBox(ex.MessageDetails.ToString, MsgBoxStyle.Critical, "Ingreso")
            Return False
        End Try
        SaveSetting(My.Application.Info.AssemblyName, "Ingreso", "Usuario", Me.TextBoxUser.Text)
        SaveSetting(My.Application.Info.AssemblyName, "Ingreso", "Ambiente", Me.ComboBoxAmbiente.SelectedIndex)
        With Globals.ThisWorkbook
            .libroConexionOK = True
            .libroBiblioteca = gsLib
            .libroUsuario = Me.TextBoxUser.Text
            .libroPassword = Me.TextBoxClave.Text
        End With
        bCargando = True
        CargaActividades()
        CargaMonedas()
        bCargando = False
        Return True
    End Function
    Private Sub CargaActividades()
        Dim fSql As String
        Dim cm As iDB2Command = New iDB2Command
        Dim da As iDB2DataAdapter
        Dim ds As DataSet
        fSql = "SELECT TNTRMD, TNTRMD || ' ' || TNLDES AS DESCRIPCION FROM GTNL01 WHERE TNTRTP = 0 ORDER BY 1 "
        cm.CommandText = fSql
        cm.CommandType = CommandType.Text
        cm.Connection = db
        da = New iDB2DataAdapter(cm)
        ds = New DataSet()
        da.Fill(ds)
        Me.ComboBoxActividad.DataSource = ds.Tables(0)
        Me.ComboBoxActividad.DisplayMember = "DESCRIPCION"
        Me.ComboBoxActividad.ValueMember = "TNTRMD"
    End Sub
    Private Sub CargaMonedas()
        Dim fSql As String
        Dim cm As iDB2Command = New iDB2Command
        Dim da As iDB2DataAdapter
        Dim ds As DataSet
        fSql = "SELECT CURCDE, GCMNY FROM GCM ORDER BY 1 "
        cm.CommandText = fSql
        cm.CommandType = CommandType.Text
        cm.Connection = db
        da = New iDB2DataAdapter(cm)
        ds = New DataSet()
        da.Fill(ds)
        Me.ComboBoxMoneda.DataSource = ds.Tables(0)
        Me.ComboBoxMoneda.DisplayMember = "GCMNY"
        Me.ComboBoxMoneda.ValueMember = "CURCDE"
        Me.ComboBoxMoneda.SelectedIndex = -1
    End Sub
    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim con As ADODB.Connection
        Dim rs As ADODB.Recordset
        Dim tb As DataTable
        con = New ADODB.Connection
        rs = New ADODB.Recordset
        tb = New DataTable
        con.ConnectionString = "Provider=IBMDA400.DataSource;Data Source=192.168.21.135;User ID=APLLX;Password=LXAPL;Persist Security Info=True;Convert Date Time To Char=TRUE;Application Name=DFU001;Default Collection=PROLX834F;Force Translate=0;"
        con.CursorLocation = ADODB.CursorLocationEnum.adUseClient
        con.Open()
        Dim fSql As String
        fSql = " SELECT ccdesc FROM RFPARAM  "
        fSql &= " WHERE CCTABL = 'FNZSEGMENTS'  AND CCCODE4 <> '' "
        fSql &= " ORDER BY CCCODEN ASC "
        ' rs.Open(fSql, con, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic, 2)
        rs.Open(fSql, con)
        Dim da As New System.Data.OleDb.OleDbDataAdapter()
        Dim ds As New DataSet()
        da.Fill(ds, rs, "BSSFFNCUE")
        'rs.Close()  'Cannot close in this method
        rs.ActiveConnection = Nothing
        con.Close()
        rs = Nothing
        con = Nothing
        grid.DataSource = (ds.Tables("BSSFFNCUE"))
    End Sub
End Class

