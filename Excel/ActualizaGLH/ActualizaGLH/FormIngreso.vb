﻿Imports Microsoft.Office.Tools.Ribbon
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Xml
Imports System.IO


Public Class FormIngreso
    Dim ws As Excel.Worksheet

    Private Sub FormIngreso_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.txtBiblioteca.Text = My.Settings.Biblioteca
        Me.txtUsuario.Text = My.Settings.Usuario
        Me.txtSistema.Text = My.Settings.Sistema
        Me.labelMensaje.Text = ""

    End Sub

    Private Sub botonEnvia_Click(sender As Object, e As EventArgs) Handles botonEnvia.Click
        ws = Globals.ThisWorkbook.ActiveSheet
        Dim aTit() As String
        Dim datos
        Dim i, qRow As Integer
        Dim msg As String

        aTit = Split(",RESULTADO,LEDGER,LIBRO,AÑO,PERIODO,COMPROBANTE,LINEA COMPROBANTE,PROVEEDOR O CLIENTE,TASA", ",")


        For i = 1 To UBound(aTit)
            If Not compTit(ws.Cells(1, i).text, aTit(i)) Then
                MsgBox("Columna " & i & " No corresponde." & vbCr & "Debe ser " & aTit(i))
                Exit Sub
            End If
        Next

        labelMensaje.Text = "Espere ..."
        Me.botonEnvia.Visible = False
        Application.DoEvents()

        datos = xmlHoja()

        Dim Resultado As New ActualizaBSSLX.Respuesta
        Dim glh As New ActualizaBSSLX.ServicioDatosSoapClient

        Resultado = glh.NumComprobantesGLH(datos)
        For i = 0 To Resultado.lineaRespuesta.Count - 1
            qRow = Resultado.lineaRespuesta(i).linea
            msg = Resultado.lineaRespuesta(i).Descripcion
            If qRow = 0 Then
                labelMensaje.Text = msg
            Else
                ws.Cells(qRow + 1, 1) = msg
            End If
        Next

        My.Settings.Biblioteca = Me.txtBiblioteca.Text
        My.Settings.Usuario = Me.txtUsuario.Text
        My.Settings.Sistema = Me.txtSistema.Text

    End Sub

    Function compTit(ByVal a As String, ByVal b As String) As Boolean
        a = a.Replace(" ", "").Replace(vbCr, "").Replace(vbLf, "")
        b = b.Replace(" ", "").Replace(vbCr, "").Replace(vbLf, "")
        Return a = b
    End Function

    Public Function xmlHoja() As String
        Dim po1
        Dim sw = New StringWriter()
        Dim settings = New XmlWriterSettings()
        settings.Indent = True
        Dim writer = XmlWriter.Create(sw, settings)
        writer.WriteStartElement("GLH")
        po1 = <DataSource><%= Me.txtSistema.Text %></DataSource>
        po1.WriteTo(writer)
        po1 = <UserID><%= Me.txtUsuario.Text %></UserID>
        po1.WriteTo(writer)
        po1 = <Password><%= Me.txtPassword.Text %></Password>
        po1.WriteTo(writer)
        po1 = <DefaultCollection><%= Me.txtBiblioteca.Text %></DefaultCollection>
        po1.WriteTo(writer)


        Dim qRow As Integer = 2
        Do While ws.Cells(qRow, 2).text <> ""
            ws.Cells(qRow, 1) = ""
            po1 = det(qRow)
            po1.WriteTo(writer)
            qRow += 1
        Loop

        writer.WriteEndElement()
        writer.Close()
        Return sw.ToString()

    End Function

    Function cab()
        Dim linDet = _
            <Cabecera>
                <DataSource><%= Me.txtSistema.Text %></DataSource>
                <UserID><%= Me.txtUsuario.Text %></UserID>
                <Password><%= Me.txtPassword.Text %></Password>
                <DefaultCollection><%= Me.txtBiblioteca.Text %></DefaultCollection>
            </Cabecera>
        Return linDet
    End Function
    Function det(qRow As Integer)
        Dim linDet = _
            <Linea>
                <LHLDGR><%= ws.Cells(qRow, 2).text %></LHLDGR>
                <LHBOOK><%= ws.Cells(qRow, 3).text %></LHBOOK>
                <LHYEAR><%= ws.Cells(qRow, 4).text %></LHYEAR>
                <LHPERD><%= ws.Cells(qRow, 5).text %></LHPERD>
                <LHJNEN><%= ws.Cells(qRow, 6).text %></LHJNEN>
                <LHJNLN><%= ws.Cells(qRow, 7).text %></LHJNLN>
                <LHJRF1><%= ws.Cells(qRow, 8).text %></LHJRF1>
                <LHJRF2><%= ws.Cells(qRow, 9).text %></LHJRF2>
            </Linea>
        Return linDet
    End Function

End Class