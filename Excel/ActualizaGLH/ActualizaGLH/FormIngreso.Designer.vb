﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormIngreso
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtSistema = New System.Windows.Forms.TextBox()
        Me.txtBiblioteca = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtUsuario = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.botonEnvia = New System.Windows.Forms.Button()
        Me.labelMensaje = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(25, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Sistema"
        '
        'txtSistema
        '
        Me.txtSistema.Location = New System.Drawing.Point(86, 21)
        Me.txtSistema.Name = "txtSistema"
        Me.txtSistema.Size = New System.Drawing.Size(95, 20)
        Me.txtSistema.TabIndex = 1
        '
        'txtBiblioteca
        '
        Me.txtBiblioteca.Location = New System.Drawing.Point(86, 47)
        Me.txtBiblioteca.Name = "txtBiblioteca"
        Me.txtBiblioteca.Size = New System.Drawing.Size(95, 20)
        Me.txtBiblioteca.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(25, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Biblioteca"
        '
        'txtUsuario
        '
        Me.txtUsuario.Location = New System.Drawing.Point(86, 73)
        Me.txtUsuario.Name = "txtUsuario"
        Me.txtUsuario.Size = New System.Drawing.Size(95, 20)
        Me.txtUsuario.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(25, 76)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(43, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Usuario"
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(86, 99)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.Size = New System.Drawing.Size(95, 20)
        Me.txtPassword.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(25, 102)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(53, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Password"
        '
        'botonEnvia
        '
        Me.botonEnvia.Location = New System.Drawing.Point(86, 126)
        Me.botonEnvia.Name = "botonEnvia"
        Me.botonEnvia.Size = New System.Drawing.Size(75, 23)
        Me.botonEnvia.TabIndex = 8
        Me.botonEnvia.Text = "Enviar"
        Me.botonEnvia.UseVisualStyleBackColor = True
        '
        'labelMensaje
        '
        Me.labelMensaje.AutoSize = True
        Me.labelMensaje.Location = New System.Drawing.Point(27, 171)
        Me.labelMensaje.MaximumSize = New System.Drawing.Size(450, 300)
        Me.labelMensaje.Name = "labelMensaje"
        Me.labelMensaje.Size = New System.Drawing.Size(64, 13)
        Me.labelMensaje.TabIndex = 9
        Me.labelMensaje.Text = "MENSAJES"
        '
        'FormIngreso
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(492, 464)
        Me.Controls.Add(Me.labelMensaje)
        Me.Controls.Add(Me.botonEnvia)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtUsuario)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtBiblioteca)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtSistema)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FormIngreso"
        Me.Text = "Actualizacion Comrobantes Manuales"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtSistema As System.Windows.Forms.TextBox
    Friend WithEvents txtBiblioteca As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtUsuario As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents botonEnvia As System.Windows.Forms.Button
    Friend WithEvents labelMensaje As System.Windows.Forms.Label
End Class
