﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Xml
Imports System.IO

' Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class ServicioDatos
    Inherits System.Web.Services.WebService
    Dim c_ID As String
    Dim resultConn As String

    Structure ActualizaGLH
        Dim DataSource As String
        Dim UserID As String
        Dim Password As String
        Dim DefaultCollection As String
        Dim lineasGLH As List(Of lineaActualizaGLH)
    End Structure

    Structure lineaActualizaGLH
        Dim Ledger As String
        Dim Book As String
        Dim Year As Integer
        Dim Period As Integer
        Dim JournalEntryNumber As String
        Dim JournalEntryLine As Integer
        Dim Reference1 As String
        Dim Reference2 As String
    End Structure

    Structure Respuesta
        Dim lineaRespuesta As List(Of lineaRespuesta)
    End Structure

    Structure lineaRespuesta
        Dim linea As Integer
        Dim Descripcion As String
    End Structure


    <WebMethod()> _
    Public Function NumComprobantesGLH(ByVal xml As String) As Respuesta
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim docEntrada As XDocument = XDocument.Parse(xml)
        Dim msgLin As String = ""

        Dim qx = From xe In docEntrada.Elements("GLH") Select New With { _
            .DataSource = xe.Element("DataSource").Value,
            .UserID = xe.Element("UserID").Value,
            .Password = xe.Element("Password").Value,
            .DefaultCollection = xe.Element("DefaultCollection").Value
        }

        Dim Resultado As New Respuesta
        Resultado.lineaRespuesta = New List(Of lineaRespuesta)
        Dim linResultado As New lineaRespuesta

        Dim GLH As New ActualizaGLH
        With GLH
            .DataSource = qx.First.DataSource
            .UserID = qx.First.UserID
            .Password = qx.First.Password
            .DefaultCollection = qx.First.DefaultCollection
            Try
                If Not DB.Conexion(.DataSource, .UserID, .Password, .DefaultCollection) Then
                    linResultado.linea = 0
                    linResultado.Descripcion = "No se pudo conectar."
                    Resultado.lineaRespuesta.Add(linResultado)
                    Return Resultado
                End If
            Catch ex As Exception
                linResultado.linea = 0
                linResultado.Descripcion = ex.ToString
                Resultado.lineaRespuesta.Add(linResultado)
                Return Resultado
            End Try

        End With
        Dim lineaGLH As lineaActualizaGLH
        GLH.lineasGLH = New List(Of lineaActualizaGLH)

        Dim qd = From xe In docEntrada.Descendants.Elements("Linea") Select New With { _
            .Ledger = xe.Element("LHLDGR").Value,
            .Book = xe.Element("LHBOOK").Value,
            .Year = xe.Element("LHYEAR").Value,
            .Period = xe.Element("LHPERD").Value,
            .JournalEntryNumber = xe.Element("LHJNEN").Value,
            .JournalEntryLine = xe.Element("LHJNLN").Value,
            .Reference1 = xe.Element("LHJRF1").Value,
            .Reference2 = xe.Element("LHJRF2").Value
        }

        For Each e In qd
            lineaGLH = New lineaActualizaGLH
            With lineaGLH
                .Ledger = e.Ledger
                .Book = e.Book
                .Year = e.Year
                .Period = e.Period
                .JournalEntryNumber = e.JournalEntryNumber
                .JournalEntryLine = e.JournalEntryLine
                .Reference1 = e.Reference1
                .Reference2 = e.Reference2
            End With
            GLH.lineasGLH.Add(lineaGLH)
        Next
        Dim i As Integer = 1
        Dim iOK As Integer = 0
        Dim iBAD As Integer = 0
        Try

            For Each e In GLH.lineasGLH
                With e
                    linResultado.linea = i
                    linResultado.Descripcion = ""
                    fSql = "SELECT PREF1, PREF2 FROM BSSLX.BSSFFNPAR WHERE PTIPCOM = '" & Left(.JournalEntryNumber, 1) & "'"
                    If DB.RS(rs, fSql) Then
                        Select Case rs("PREF1").Value
                            Case "PR"
                                If UCase(Left(.Reference1, 1)) = "C" Then
                                    If IsNumeric(.Reference1.Substring(2)) Then
                                        fSql = "SELECT CCUST FROM RCM WHERE CCUST = " & .Reference1.Substring(2)
                                        If Not DB.RS(rs1, fSql) Then
                                            linResultado.Descripcion &= "No encontró Cliente. "
                                        End If
                                        rs1.Close()
                                    Else
                                        linResultado.Descripcion &= "Error en codigo del cliente. "
                                    End If
                                ElseIf IsNumeric(.Reference1) Then
                                    fSql = "SELECT VENDOR FROM AVM WHERE VENDOR = " & .Reference1
                                    If Not DB.RS(rs1, fSql) Then
                                        linResultado.Descripcion &= "No encontró Proveedor. "
                                    End If
                                    rs1.Close()
                                Else
                                    linResultado.Descripcion &= "Error en codigo de Proveedor. "
                                End If
                            Case "CL"
                                If IsNumeric(.Reference1) Then
                                    fSql = "SELECT CCUST FROM RCM WHERE CCUST = " & .Reference1
                                    If Not DB.RS(rs1, fSql) Then
                                        linResultado.Descripcion &= "No encontró Cliente. "
                                    End If
                                    rs1.Close()
                                Else
                                    linResultado.Descripcion &= "Error en codigo del cliente. "
                                End If
                        End Select
                        If rs("PREF2").Value = "TA" Then
                            fSql = "SELECT RCRTCD FROM ZRC WHERE RCRTCD = '" & .Reference2 & "'"
                            If Not DB.RS(rs1, fSql) Then
                                linResultado.Descripcion &= "No encontró Tasa. "
                            End If
                            rs1.Close()
                        End If
                    End If
                    rs.Close()
                    If linResultado.Descripcion = "" Then
                        fSql = " UPDATE GLH SET  "
                        fSql = fSql & " LHJRF1 = '" & .Reference1 & "', "
                        fSql = fSql & " LHJRF2 = '" & .Reference2 & "' "
                        fSql = fSql & " WHERE LHLDGR = '" & .Ledger & "' "
                        fSql = fSql & " AND LHBOOK = '" & .Book & "' "
                        fSql = fSql & " AND LHYEAR = " & .Year
                        fSql = fSql & " AND LHPERD = " & .Period
                        fSql = fSql & " AND LHJNEN = '" & .JournalEntryNumber & "' "
                        fSql = fSql & " AND LHJNLN = " & .JournalEntryLine
                        DB.ExecuteSQL(fSql)
                        If DB.regActualizados = 0 Then
                            linResultado.Descripcion = "NO ACTUALIZADO"
                            iBAD += 1
                        Else
                            linResultado.Descripcion = "OK"
                            iOK += 1
                        End If
                    End If
                    Resultado.lineaRespuesta.Add(linResultado)
                    i += 1
                End With
            Next
            linResultado.linea = 0
            linResultado.Descripcion = String.Format("Registros recibidos {1}{0}Registros Actualizados {2}{0}Registros no actualizados {3}{0}", vbCrLf, i - 1, iOK, iBAD)
            Resultado.lineaRespuesta.Add(linResultado)
        Catch ex As Exception
            linResultado.linea = 0
            linResultado.Descripcion = ex.ToString
            Resultado.lineaRespuesta.Add(linResultado)
            Return Resultado
        End Try


        DB.Close()
        Return Resultado

    End Function

    Function AbreConexion(docEntrada As String) As Boolean
        resultConn = DB.Conexion()

        If resultConn = "" Then
            c_ID = DBMySql.CalcConsec("BFWSINBOX", "99999999")
            fSql = " INSERT INTO bfwsinbox(C_ID, C_XML)  "
            fSql = fSql & " VALUES ('" & c_ID & "', '" & docEntrada & "') "
            DBMySql.ExecuteSQL(fSql)
            Return True
        Else
            Return False
        End If
    End Function

End Class