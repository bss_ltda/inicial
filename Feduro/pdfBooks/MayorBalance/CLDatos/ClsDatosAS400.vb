﻿Imports System.IO
Imports System.Text
Imports IBM.Data.DB2.iSeries

Public Class ClsDatosAS400
    Public ReadOnly conexionIBM As New iDB2Connection()
    Private adapterIBM As iDB2DataAdapter = New iDB2DataAdapter()
    Private transIBM As iDB2Transaction
    Private builderIBM As iDB2CommandBuilder
    Private commandIBM As iDB2Command
    Private numFilas As Integer
    Public dt As DataTable
    Private WithEvents ds As DataSet
    Private continuarguardando As Boolean
    Public appPath As String = Path.GetFullPath(My.Application.Info.DirectoryPath & "\Log\")
    Public mensaje As String
    Private data
    Public fSql As String
    Private sApp As String
    Public gsKeyWords As String = ""
    Public Piloto As String = My.Settings.PILOTO

#Region "ACTUALIZAR"

    ''' <summary>
    ''' Actualiza la tabla RFTASK 
    ''' </summary>
    ''' <param name="datos">RFTASK</param>
    ''' <param name="tipo">0- SET TLXMSG = @TLXMSG, TMSG = @TMSG WHERE TNUMREG = @TNUMREG,
    ''' 1- SET TLXMSG = @TLXMSG, TMSG = @TMSG, STS2 = @STS2 WHERE TNUMREG = @TNUMREG,
    ''' 2- SET TLXMSG = @TLXMSG, TMSG = @TMSG, TURL = @TURL, STS2 = @STS2 WHERE TNUMREG = @TNUMREG</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 24/05/2016</remarks>
    Public Function ActualizarRFTASKRow(ByVal datos As RFTASK, ByVal tipo As Integer) As Boolean
        Select Case tipo
            Case 0
                fSql = " UPDATE RFTASK SET "
                fSql &= " TLXMSG = '" & datos.TLXMSG & "',  "
                fSql &= " TMSG = '" & datos.TMSG & "' "
                fSql &= " WHERE TNUMREG = '" & datos.TNUMREG & "'"
            Case 1
                fSql = " UPDATE RFTASK SET "
                fSql &= " TLXMSG = '" & datos.TLXMSG & "',  "
                fSql &= " TMSG = '" & datos.TMSG & "',  "
                fSql &= " STS2 = " & datos.STS2 & " "
                fSql &= " WHERE TNUMREG = '" & datos.TNUMREG & "'"
            Case 2
                fSql = " UPDATE RFTASK SET "
                fSql = fSql & " TLXMSG = '" & datos.TLXMSG & "',  "
                fSql = fSql & " TMSG = '" & datos.TMSG & "',  "
                fSql = fSql & " TURL = '" & datos.TURL & "',  "
                fSql = fSql & " STS2 = " & datos.STS2 & " "
                fSql = fSql & " WHERE TNUMREG = '" & datos.TNUMREG & "' "
        End Select

        commandIBM = New iDB2Command
        commandIBM.Connection = conexionIBM
        Try
            commandIBM.CommandText = fSql
            commandIBM.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As Data.SqlClient.SqlException
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFTASKError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla RFTASK.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFTASKError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla RFTASK.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                data = Nothing
                data = New RFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .OPERACION = "Actualizar RFLOG"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarRFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

#End Region

#Region "CONSULTAR"

    ''' <summary>
    ''' Consulta la tabla RFPARAM con un determinado parametro de busqueda.Retorna un String
    ''' </summary>
    ''' <param name="prm">parametro de Busqueda</param>
    ''' <param name="Campo"></param>
    ''' <returns>Un String</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 24/05/2016</remarks>
    Public Function ConsultarRFPARAMString(ByVal prm As String, ByVal Campo As String, ByVal cctabl As String) As String
        dt = Nothing

        If Campo = "" Then
            Campo = "CCNOT1"
        End If
        If cctabl = "" Then
            cctabl = "LXLONG"
        End If
        fSql = "SELECT " & Campo & " AS DATO "
        fSql &= " FROM RFPARAM "
        fSql &= " WHERE CCTABL='" & cctabl & "' AND UPPER(CCCODE) = UPPER('" & prm & "')"

        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)

        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt.Rows(0).Item("DATO")
    End Function

    ''' <summary>
    ''' Consulta la tabla BSSFFNBAL con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- </param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 23/05/2016</remarks>
    Public Function ConsultarBSSFFNBALdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing

        Select Case tipoparametro
            Case 0
                fSql = " SELECT "
                fSql &= " DIGITS(BYEAR) BYEAR,   "
                fSql &= " DIGITS(BPERIO) BPERIO,  "
                fSql &= " BCUENTA, "
                fSql &= " BDESCTA, "
                fSql &= " DOUBLE(BSALANT) BSALANT, "
                fSql &= " DOUBLE(BDEBITO) BDEBITO, "
                fSql &= " DOUBLE(BCREDITO) BCREDITO, "
                fSql &= " DOUBLE(BSALFIN) BSALFIN"
                fSql &= " FROM BSSFFNBAL "
                fSql &= " WHERE "
                fSql &= " BCONTROL = '" & parametroBusqueda(0) & "'"
                fSql &= " ORDER BY BCUENTA"
            Case 1
                fSql = " SELECT "
                fSql = fSql & " DIGITS(BYEAR) BYEAR,   "
                fSql = fSql & " DIGITS(BPERIO) BPERIO  "
                fSql = fSql & " FROM BSSFFNBAL "
                fSql = fSql & " WHERE "
                fSql = fSql & " BCONTROL = '" & parametroBusqueda(0) & "'"
        End Select
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)
        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

#End Region

#Region "FUNCIONES"

    Public Function AbrirConexion() As Boolean
        Dim abierta As Boolean = False
        Try
            If Not Directory.Exists(appPath) Then
                Directory.CreateDirectory(appPath)
            End If
            If conexionIBM.State = ConnectionState.Open Then
                abierta = True
            Else
                If Piloto = "NO" Then
                    conexionIBM.ConnectionString = My.Settings.BASEDATOSAS400
                Else
                    conexionIBM.ConnectionString = My.Settings.BASEDATOSAS400Piloto
                End If
                conexionIBM.Open()
                abierta = True
            End If
        Catch ex1 As iDB2Exception
            abierta = False
            mensaje = ex1.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo establecer Conexion." & vbCrLf & ex1.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Catch ex As Exception
            abierta = False
            mensaje = ex.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo establecer Conexion." & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try
        Return abierta
    End Function

    Public Function CerrarConexion() As Boolean
        Dim cerrada As Boolean = False
        Try
            conexionIBM.Close()
            cerrada = True
        Catch ex1 As iDB2Exception
            cerrada = False
            mensaje = ex1.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo finalizar Conexion." & vbCrLf & ex1.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Catch ex As Exception
            cerrada = False
            mensaje = ex.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo finalizar Conexion." & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try
        Return cerrada
    End Function

    Public Sub wrtLogHtml(sql As String, ByVal Descrip As String)
        Dim fEntrada As String = ""
        Dim linea As String = Format(Now(), "yyyy-MM-dd HH:mm:ss") & "|"
        Dim strStreamW As Stream = Nothing
        Dim sApp As String = My.Application.Info.ProductName & "." & _
                                   My.Application.Info.Version.Major & "." & _
                                   My.Application.Info.Version.Minor & "." & _
                                   My.Application.Info.Version.Revision

        Descrip = Descrip.Replace(vbCrLf, vbCr)
        Descrip = Descrip.Replace(vbLf, vbCr)
        Descrip = Descrip.Replace(vbCr, "<BR/>")

        If gsKeyWords.Trim <> "" Then
            Descrip = gsKeyWords & "<br>" & Descrip
        End If

        linea &= System.Net.Dns.GetHostName() & "|" & sApp & "|" & Descrip.Replace("|", "?") & "|" & sql.Replace("|", "?")

        fEntrada = Replace(appPath & "\" & Format(Now(), "yyyyMM") & ".csv", "\\", "\")
        If Not Directory.Exists(appPath) Then
            Directory.CreateDirectory(appPath)
        End If
        If File.Exists(fEntrada) Then
            strStreamW = File.Open(fEntrada, FileMode.Open) 'Abrimos el archivo
        Else
            strStreamW = File.Create(fEntrada) ' lo creamos
        End If
        strStreamW.Close()
        Try
            ' Create an instance of StreamReader to read from a file.
            Dim sr As StreamReader = New StreamReader(fEntrada)
            Dim sb As New StringBuilder()
            sb.Append(sr.ReadToEnd())
            sb.Append(linea)
            sr.Close()
            Dim sw As StreamWriter = New StreamWriter(fEntrada)
            sw.WriteLine(sb.ToString())
            sw.Close()

        Catch E As Exception

        End Try
        'Application.Exit()
    End Sub

    Public Sub WrtTxtError(ByVal Archivo As String, ByVal Texto As String)
        wrtLogHtml("", "Archivo:  " & Archivo & " Texto: " & Texto)
    End Sub

    Public Sub LlamaPrograma(ByVal PGM As String, ByVal Batch As String)
        Dim cmd As iDB2Command = New iDB2Command()
        cmd.Connection = conexionIBM
        cmd.CommandText = String.Format("Call " & PGM & "('{0}')", Batch)
        cmd.CommandType = CommandType.Text
        Try
            cmd.ExecuteReader()
        Catch ex As iDB2Exception
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ProgramaError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo llamar al programa." & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Catch ex As Exception
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ProgramaError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo llamar al programa." & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try
    End Sub

#End Region

#Region "GUARDAR"

    ''' <summary>
    ''' Guarda RFLOG
    ''' </summary>
    ''' <param name="datos">RFLOG</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 24/05/2016</remarks>
    Public Function GuardarRFLOGRow(ByVal datos As RFLOG) As Boolean

        fSql = "INSERT INTO RFLOG (USUARIO, OPERACION, PROGRAMA, EVENTO, TXTSQL, ALERT)"
        fSql &= " VALUES ('" & datos.USUARIO & "', '" & datos.OPERACION & "', '" & datos.PROGRAMA & "', '" & datos.EVENTO.Replace("'", "''") & "', '" & datos.TXTSQL.Replace("'", "''") & "', " & datos.ALERT & ")"

        commandIBM = New iDB2Command
        commandIBM.Connection = conexionIBM
        Try
            commandIBM.CommandText = fSql
            commandIBM.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As Data.SqlClient.SqlException
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo guardar en la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Catch ex As Exception
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo guardar en la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Finally
        End Try
        Return continuarguardando
    End Function

#End Region

End Class

Public Class RFLOG
    Public OPERACION As String
    Public PROGRAMA As String
    Public EVENTO As String
    Public TXTSQL As String
    Public ALERT As String
    Public ID As String
    Public USUARIO As String
End Class

Public Class RFTASK
    Public TNUMREG As String
    Public TLXMSG As String
    Public TMSG As String
    Public STS2 As String
    Public TURL As String
End Class