﻿Imports iTextSharp.text
Imports CLDatos
Imports System.Text
Imports System.IO
Imports iTextSharp.text.pdf

Module MB
    Public datos As New ClsDatosAS400
    Private dtBSSFFNBAL As DataTable
    Public FechaImpresion
    Public Referencia As String
    Private data
    Private CARPETA_SITIO As String
    Private CARPETA_IMG As String
    Public Control As String
    Public Consecutivo As String
    Private tarea As String
    Private bPRUEBAS As Boolean
    Private PDF_Folder As String
    Private LOG_File As String
    Public LOGO As String
    Public NOMEMP As String
    Public NIT As String
    Private PGM400 As String
    Public AAAA As String
    Public PERIODO As String
    Private LIBRO As String
    Private AMBIENTE As String
    Private ArchivoPDF As String
    Private document As Document
    Private PageCount As Integer

    Sub Main()
        Dim campoParam As String = ""
        Dim aSubParam() As String
        Dim continuar As Boolean

        Try

            Environment.GetCommandLineArgs()
            For Each parametro In Environment.GetCommandLineArgs()
                aSubParam = Split(parametro, "=")
                Select Case UCase(aSubParam(0))
                    Case "TAREA"
                        tarea = aSubParam(1)
                    Case "IDREPORTE"
                        Control = aSubParam(1)
                    Case "CONSECUTIVO"
                        Consecutivo = aSubParam(1)
                    Case "REFERENCIA"
                        Referencia = aSubParam(1)
                    Case "FECHA"
                        FechaImpresion = aSubParam(1)
                    Case "PGM400"
                        PGM400 = aSubParam(1)
                    Case "AAAA"
                        AAAA = aSubParam(1)
                    Case "PERIODO"
                        PERIODO = aSubParam(1)
                    Case "LIBRO"
                        LIBRO = aSubParam(1)
                    Case "AMBIENTE"
                        AMBIENTE = aSubParam(1)
                End Select
            Next

            If Not datos.AbrirConexion() Then
                Return
            End If

            Try
                'LLAMA PROGRAMA AS400
                datos.LlamaPrograma(PGM400, AAAA & "', '" & PERIODO & "', '" & Control & "', '" & LIBRO & "', '" & AMBIENTE)
            Catch ex As Exception
                data = Nothing
                data = New RFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .OPERACION = "Llamar Programa"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = ex.Message
                    .TXTSQL = datos.fSql
                    .ALERT = 1
                End With
                continuar = datos.GuardarRFLOGRow(data)
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\ProgramaError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo llamar al programa." & vbCrLf & ex.Message)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try

            If datos.Piloto = "SI" Then
                campoParam = "CCNOT2"
            End If
            CARPETA_SITIO = datos.ConsultarRFPARAMString("SITIO_CARPETA", campoParam, "")
            CARPETA_IMG = datos.ConsultarRFPARAMString("CARPETA_IMG", campoParam, "")
            LOGO = datos.ConsultarRFPARAMString("LOGO", campoParam, "")
            NOMEMP = datos.ConsultarRFPARAMString("NOMBRE", campoParam, "")
            NIT = datos.ConsultarRFPARAMString("NIT", campoParam, "")
            bPRUEBAS = datos.Piloto.ToUpper() = "SI"
            PDF_Folder = CARPETA_IMG & "pdf\Libros\"
            Try
                If Not Directory.Exists(PDF_Folder) Then
                    Directory.CreateDirectory(PDF_Folder)
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\DirectorioError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo crear Directorio." & vbCrLf & ex.Message)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            LOG_File = PDF_Folder & "LD" & Control & ".log"

            Balancee()
            datos.CerrarConexion()
            Return
        Catch ex As Exception
            datos.wrtLogHtml("", ex.Message.ToString)
            datos.CerrarConexion()
            Return
        End Try
    End Sub

    Sub Balancee()
        Dim continuar As Boolean
        Console.WriteLine("Generando documento...")
        data = Nothing
        data = New RFTASK
        With data
            .TLXMSG = "EJECUTANDO"
            .TMSG = "Generando Libro Mayor y Balances"
            .TNUMREG = tarea
        End With
        If datos.Piloto = "NO" Then
            continuar = datos.ActualizarRFTASKRow(data, 0)
        Else
            continuar = True
        End If
        If continuar = True Then
            ArchivoPDF = PDF_Folder & "MyB" & Control & ".pdf"
            'fileName = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() & ".pdf"
            pdf()

            If bPRUEBAS Then
                'EJECUTA
                Dim prc As Process = New System.Diagnostics.Process()
                prc.StartInfo.FileName = ArchivoPDF
                prc.Start()
            End If
        End If
    End Sub

    Sub pdf()
        Dim parametro(0) As String
        Dim continuar As Boolean

        document = New Document(New Rectangle(288.0F, 144.0F), 36, 36, 140, 40)
        document.SetPageSize(iTextSharp.text.PageSize.LETTER.Rotate())
        Console.WriteLine(document.PageSize.Width.ToString)
        document.AddTitle("Libro MyB - ")
        Dim es As eventos = New eventos()
        Dim pw As PdfWriter = PdfWriter.GetInstance(document, New FileStream(ArchivoPDF, FileMode.Create))
        pw.PageEvent = es
        document.Open()

        parametro(0) = Control
        dtBSSFFNBAL = datos.ConsultarBSSFFNBALdt(parametro, 0)
        If dtBSSFFNBAL IsNot Nothing Then
            Dim unaTabla As PdfPTable = GenerarTabla()
            document.Add(unaTabla)
            PageCount = pw.PageNumber
            data = Nothing
            data = New RFTASK
            With data
                .TLXMSG = "EJECUTADA"
                .TMSG = "Libro Mayor y Banances Generado N° Paginas: " & PageCount
                .TURL = Replace(ArchivoPDF, CARPETA_SITIO, "\")
                .STS2 = "1"
                .TNUMREG = tarea
            End With
            If datos.Piloto = "NO" Then
                continuar = datos.ActualizarRFTASKRow(data, 2)
            End If
            If continuar = False Then
                'ERROR DE ACTUALIZADO
            End If
        Else
            datos.WrtTxtError(LOG_File, "Factura no se encontro.")
            data = Nothing
            data = New RFTASK
            With data
                .TLXMSG = "EJECUTADA"
                .TMSG = "Libro Mayor y Banances no Existe"
                .STS2 = "1"
                .TNUMREG = tarea
            End With
            If datos.Piloto = "NO" Then
                continuar = datos.ActualizarRFTASKRow(data, 1)
            End If
            If continuar = False Then
                'ERROR DE ACTUALIZADO
            End If
        End If

            Dim tablafin As PdfPTable = fintabla(document)
            document.Add(tablafin)
            FinDocumento()

            document.Close()
    End Sub

    Public Function GenerarTabla() As PdfPTable

        Dim flag As Integer = 0

        Console.WriteLine("Generando una tabla...")
        Dim tablaPadre As New PdfPTable(1)
        tablaPadre.SetWidthPercentage(New Single() {615}, PageSize.LETTER)
        tablaPadre.HorizontalAlignment = Element.ALIGN_CENTER
        tablaPadre.DefaultCell.BorderWidth = 0.5

        Dim unaTabla As New PdfPTable(6)
        unaTabla.HorizontalAlignment = Element.ALIGN_CENTER
        unaTabla.DefaultCell.BorderWidth = 0

        unaTabla.SetWidthPercentage(New Single() {50, 205, 90, 90, 90, 90}, PageSize.LETTER)
        'Headers
        
            Dim header1 As New PdfPCell(New Paragraph("CUENTA", FontFactory.GetFont("Arial", 8, Font.BOLD)))
            header1.PaddingBottom = 5
            header1.HorizontalAlignment = 1
            header1.VerticalAlignment = Element.ALIGN_MIDDLE
            Dim header2 As New PdfPCell(New Paragraph("DESCRIPCIÓN", FontFactory.GetFont("Arial", 8, Font.BOLD)))
            header2.PaddingBottom = 5
            header2.HorizontalAlignment = 1
            header2.VerticalAlignment = Element.ALIGN_MIDDLE
            Dim header3 As New PdfPCell(New Paragraph("INICIAL", FontFactory.GetFont("Arial", 8, Font.BOLD)))
            header3.PaddingBottom = 5
            header3.HorizontalAlignment = 1
            header3.VerticalAlignment = Element.ALIGN_MIDDLE
            Dim header4 As New PdfPCell(New Paragraph("DÉBITO", FontFactory.GetFont("Arial", 8, Font.BOLD)))
            header4.PaddingBottom = 5
            header4.HorizontalAlignment = 1
            header4.VerticalAlignment = Element.ALIGN_MIDDLE
            Dim header5 As New PdfPCell(New Paragraph("CRÉDITO", FontFactory.GetFont("Arial", 8, Font.BOLD)))
            header5.PaddingBottom = 5
            header5.HorizontalAlignment = 1
            header5.VerticalAlignment = Element.ALIGN_MIDDLE
            Dim header6 As New PdfPCell(New Paragraph("FINAL", FontFactory.GetFont("Arial", 8, Font.BOLD)))
            header6.PaddingBottom = 5
            header6.HorizontalAlignment = 1
            header6.VerticalAlignment = Element.ALIGN_MIDDLE

            header1.BorderWidth = 0
            header2.BorderWidth = 0
            header3.BorderWidth = 0
            header4.BorderWidth = 0
            header5.BorderWidth = 0
            header6.BorderWidth = 0

            header2.BorderWidthLeft = 0.5
            header3.BorderWidthLeft = 0.5
            header4.BorderWidthLeft = 0.5
            header5.BorderWidthLeft = 0.5
            header6.BorderWidthLeft = 0.5

            header1.BorderWidthBottom = 0.5
            header2.BorderWidthBottom = 0.5
            header3.BorderWidthBottom = 0.5
            header4.BorderWidthBottom = 0.5
            header5.BorderWidthBottom = 0.5
            header6.BorderWidthBottom = 0.5

            unaTabla.AddCell(header1)
            unaTabla.AddCell(header2)
            unaTabla.AddCell(header3)
            unaTabla.AddCell(header4)
            unaTabla.AddCell(header5)
            unaTabla.AddCell(header6)
            unaTabla.HeaderRows = 1

            Dim celda As PdfPCell

            For i = 0 To dtBSSFFNBAL.Rows.Count - 1
                celda = New PdfPCell(New Paragraph(dtBSSFFNBAL.Rows(i).Item("BCUENTA"), FontFactory.GetFont("Arial", 8)))
                celda.BorderWidth = 0

                If flag = 1 Then
                    celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                End If

                unaTabla.AddCell(celda)
                celda = New PdfPCell(New Paragraph(dtBSSFFNBAL.Rows(i).Item("BDESCTA"), FontFactory.GetFont("Arial", 8)))
                celda.BorderWidth = 0
                celda.BorderWidthLeft = 0.5

                If flag = 1 Then
                    celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                End If

                unaTabla.AddCell(celda)
                celda = New PdfPCell(New Paragraph(FormatNumber(dtBSSFFNBAL.Rows(i).Item("BSALANT"), 2), FontFactory.GetFont("Arial", 8)))
                celda.BorderWidth = 0
                celda.BorderWidthLeft = 0.5

                celda.HorizontalAlignment = 2
                If flag = 1 Then
                    celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                End If

                unaTabla.AddCell(celda)
                celda = New PdfPCell(New Paragraph(FormatNumber(dtBSSFFNBAL.Rows(i).Item("BDEBITO"), 2), FontFactory.GetFont("Arial", 8)))
                celda.BorderWidth = 0
                celda.BorderWidthLeft = 0.5

                celda.HorizontalAlignment = 2
                If flag = 1 Then
                    celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                End If

                unaTabla.AddCell(celda)
                celda = New PdfPCell(New Paragraph(FormatNumber(dtBSSFFNBAL.Rows(i).Item("BCREDITO"), 2), FontFactory.GetFont("Arial", 8)))
                celda.BorderWidth = 0
                celda.BorderWidthLeft = 0.5

                celda.HorizontalAlignment = 2
                If flag = 1 Then
                    celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                End If

                unaTabla.AddCell(celda)
                celda = New PdfPCell(New Paragraph(FormatNumber(dtBSSFFNBAL.Rows(i).Item("BSALFIN"), 2), FontFactory.GetFont("Arial", 8)))
                celda.BorderWidth = 0
                celda.BorderWidthLeft = 0.5

                celda.HorizontalAlignment = 2
                If flag = 1 Then
                    celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                    flag = 0
                Else
                    flag += 1
                End If
                unaTabla.AddCell(celda)
            Next
           
        Dim celdapapa As New PdfPCell(unaTabla)
        celdapapa.BorderWidth = 0.5
        tablaPadre.AddCell(celdapapa)

        Return tablaPadre
    End Function

    Private Function fintabla(doc As Document) As PdfPTable

        Dim unaTabla As New PdfPTable(1)
        unaTabla.SetWidthPercentage(New Single() {625}, PageSize.LETTER)
        unaTabla.DefaultCell.BorderWidth = 0
        Dim fin As New PdfPCell(New Paragraph("Fin del Informe", FontFactory.GetFont("Arial", 12, Font.BOLD)))
        fin.BorderWidth = 0
        fin.HorizontalAlignment = Element.ALIGN_CENTER
        unaTabla.AddCell(fin)
        Return unaTabla
    End Function

    Sub FinDocumento()
        If PageCount Mod 2 = 0 Then
            document.NewPage()
            Dim unaTabla As New PdfPTable(1)
            unaTabla.SetWidthPercentage(New Single() {625}, PageSize.LETTER)
            unaTabla.DefaultCell.BorderWidth = 0
            Dim fin As New PdfPCell(New Paragraph("Fin del Informe", FontFactory.GetFont("Arial", 12, Font.BOLD)))
            fin.BorderWidth = 0.5
            fin.HorizontalAlignment = Element.ALIGN_CENTER
            unaTabla.AddCell(fin)

            document.Add(unaTabla)
        End If
    End Sub

End Module
