﻿Imports iTextSharp.text
Imports iTextSharp.text.pdf

Public Class eventos
    Inherits PdfPageEventHelper

    Public Overrides Sub OnEndPage(writer As PdfWriter, document As Document)
        Try
            Dim pdfTab As New PdfPTable(5)
            Dim Titulo As New PdfPCell(New Paragraph("LIBRO MAYOR Y BALANCES", FontFactory.GetFont("Arial", 11, Font.BOLD)))
            Dim Ref As New PdfPCell(New Paragraph(Referencia.ToString, FontFactory.GetFont("Arial", 8, BaseColor.RED)))
            Dim Cons As New PdfPCell(New Paragraph(Consecutivo + writer.PageNumber, FontFactory.GetFont("Arial", 8, BaseColor.BLUE)))
            Dim Fech As New PdfPCell(New Paragraph(FechaImpresion.ToString, FontFactory.GetFont("Arial", 8)))
            Dim Nombre As New PdfPCell(New Paragraph(NOMEMP.ToString, FontFactory.GetFont("Arial", 8, Font.BOLD)))
            Dim Nitt As New PdfPCell(New Paragraph(NIT.ToString, FontFactory.GetFont("Arial", 8)))
            Dim periodot As New PdfPCell(New Paragraph("PERIODO", FontFactory.GetFont("Arial", 8)))
            Dim aniomes As New PdfPCell(New Paragraph(AAAA & PERIODO, FontFactory.GetFont("Arial", 8, Font.BOLD)))
            Dim imagen As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(LOGO)
            Dim percentage As Single = 0.0F
            Dim imageCell As PdfPCell = New PdfPCell(imagen)

            '===== TITULO =========
            Titulo.Colspan = 2
            Titulo.PaddingBottom = 5.0F
            Titulo.BorderWidth = 0
            Titulo.HorizontalAlignment = Element.ALIGN_LEFT

            '===== REFERENCIA =========
            Ref.BorderWidth = 0
            Ref.HorizontalAlignment = Element.ALIGN_RIGHT

            '===== CONSECUTIVO =========
            Cons.BorderWidth = 0

            '===== FECHA =========
            Fech.BorderWidth = 0
            Fech.HorizontalAlignment = 2

            '===== NOMBRE EMP =========
            Nombre.Colspan = 5
            Nombre.BorderWidth = 0
            Nombre.HorizontalAlignment = 0

            '===== NIT =========
            Nitt.Colspan = 5
            Nitt.BorderWidth = 0
            Nitt.HorizontalAlignment = 0

            '===== PERIODO =========
            periodot.BorderWidth = 0
            periodot.HorizontalAlignment = 0
            periodot.VerticalAlignment = Element.ALIGN_MIDDLE

            '===== PERIODO AÑO MES =========
            aniomes.BorderWidth = 0

            aniomes.HorizontalAlignment = 0
            aniomes.VerticalAlignment = Element.ALIGN_MIDDLE

            '===== IMAGEN LOGO =========
            imagen.BorderWidth = 0
            imagen.Alignment = Element.ALIGN_RIGHT

            '===== ESCALANDO IMAGEN =========
            percentage = 78 / imagen.Width
            imagen.ScalePercent(percentage * 100)

            imageCell.BorderWidth = 0
            imageCell.Colspan = 3
            imageCell.HorizontalAlignment = 2

            pdfTab.AddCell(Titulo)
            pdfTab.AddCell(Ref)
            pdfTab.AddCell(Cons)
            pdfTab.AddCell(Fech)
            pdfTab.AddCell(Nombre)
            pdfTab.AddCell(Nitt)
            pdfTab.AddCell(periodot)
            pdfTab.AddCell(aniomes)
            pdfTab.AddCell(imageCell)

            pdfTab.SetWidthPercentage(New Single() {50, 510, 65, 45, 50}, PageSize.LETTER)
            pdfTab.WriteSelectedRows(0, -1, 34.8, document.PageSize.Height - 42, writer.DirectContent)
        Catch ex As System.StackOverflowException
            Console.WriteLine(ex.StackTrace & vbCrLf & ex.Message)
        End Try
    End Sub

End Class
