﻿
Public Module modLogica
    Private datos As New ClsDatosAS400
    Private data

    Sub revisaTareas()
        Dim dtRFTASK As DataTable
        Dim parametro(0) As String
        Dim pgm, param, exe, msgT, tLXmsg, Accion As String
        Dim sts As Integer
        Dim continuar As Boolean
        Dim librerias() As String

        librerias = My.Settings.LIBS.Split(",")
        Console.WriteLine("Recorriendo Librerias...")
        For i = 0 To librerias.Length - 1
            Console.WriteLine("Abriendo Conexión...")
            If Not datos.AbrirConexion(librerias(i)) Then
                Console.WriteLine("Error Abriendo Conexión " & datos.mensaje & vbCrLf & datos.conexionIBM.ConnectionString)
                Exit Sub
            End If

            Console.WriteLine("Consultando Tareas...")
            dtRFTASK = datos.ConsultarRFTASKdt(parametro, 0)
            If dtRFTASK IsNot Nothing Then
                Console.WriteLine("Recorriendo Tareas...")
                For Each row As DataRow In dtRFTASK.Rows
                    Accion = UCase(row("TTIPO"))
                    If row("FWRKID") <> 0 Then
                        If Accion <> "ULTIMO" Then
                            Accion = "PAQUETE"
                        End If
                    Else
                        Accion = UCase(row("TTIPO"))
                    End If
                    Console.WriteLine(Accion)
                    'Try
                    Select Case Accion
                        Case UCase("Shell")
                            Console.WriteLine("Programa: " & row("TPGM"))
                            Console.WriteLine("Parametros: " & row("TPRM"))
                            pgm = row("TPGM")
                            param = row("TPRM")
                            If row("TNUMTAREA") = 1 Then
                                exe = pgm & param.Replace("|", " ") & " TAREA=" & row("TNUMREG")
                            Else
                                exe = pgm & param.Replace("|", " ")
                            End If

                            Console.WriteLine("Comando: " & exe)
                            sts = 1
                            msgT = "Comando Enviado."
                            tLXmsg = "EJECUTADA"
                            Try
                                Console.WriteLine("Ejecutando: " & exe)
                                Shell(exe, AppWinStyle.Hide, False, -1)
                            Catch ex As Exception
                                Console.WriteLine("Error Ejecutando: " & exe & vbCrLf & ex.Message)
                                sts = -1
                                msgT = ex.Message.Replace("'", "''")
                                tLXmsg = "ERROR"
                            End Try
                            data = Nothing
                            data = New RFTASK
                            With data
                                .TLXMSG = tLXmsg
                                .STS1 = sts
                                .TMSG = msgT
                                .TCMD = "" & exe & ""
                                .TNUMREG = row("TNUMREG")
                            End With
                            Console.WriteLine("Actualizando RFTASK...")
                            continuar = datos.ActualizarRFTASKRow(data, 0)
                            If continuar = False Then
                                'ERROR DE ACTUALIZADO
                                Console.WriteLine("Error Actualizando RFTASK " & vbCrLf & datos.mensaje)
                            Else
                                Console.WriteLine("RFTASK Actualizado")
                            End If
                        Case UCase("Ultimo")
                            exe = "ImprimeDocumentos.exe " & row("FWRKID").Value
                            Console.WriteLine("Comando: " & exe)
                            sts = 1
                            msgT = "Paquete Enviado."
                            tLXmsg = "ENVIADO"
                            data = Nothing
                            data = New RFTASK
                            With data
                                .TLXMSG = tLXmsg
                                .STS1 = sts
                                .TMSG = msgT
                                .TCMD = "" & exe & ""
                                .TNUMREG = row("TNUMREG")
                            End With
                            Console.WriteLine("Actualizando RFTASK...")
                            continuar = datos.ActualizarRFTASKRow(data, 0)
                            If continuar = False Then
                                'ERROR DE ACTUALIZADO
                                Console.WriteLine("Error Actualizando RFTASK " & vbCrLf & datos.mensaje)
                            Else
                                Console.WriteLine("RFTASK Actualizado")
                            End If
                            Try
                                Console.WriteLine("Ejecutando: " & exe)
                                Shell(exe, AppWinStyle.Hide, False, -1)
                            Catch ex As Exception
                                Console.WriteLine("Error Ejecutando: " & exe & vbCrLf & ex.Message)
                                msgT = ex.Message.Replace("'", "''")
                                datos.WrtSqlError(exe, msgT)
                            End Try
                        Case UCase("Rutina")
                            pgm = row("TPGM").Value
                            'Ejecutable de rutinas con parametro TNUMREG de RFTASK
                            exe = pgm & " " & row("TNUMREG").Value
                            sts = 1
                            msgT = "Comando Enviado."
                            tLXmsg = "EJECUTADA"
                            Try
                                Console.WriteLine("Ejecutando: " & exe)
                                Shell(exe, AppWinStyle.Hide, False, -1)
                            Catch ex As Exception
                                Console.WriteLine("Error Ejecutando: " & exe & vbCrLf & ex.Message)
                                sts = -1
                                msgT = ex.Message.Replace("'", "''")
                                tLXmsg = "ERROR"
                            End Try
                            data = Nothing
                            data = New RFTASK
                            With data
                                .TLXMSG = tLXmsg
                                .STS1 = sts
                                .TMSG = msgT
                                .TCMD = "" & exe & ""
                                .TNUMREG = row("TNUMREG")
                            End With
                            Console.WriteLine("Actualizando RFTASK...")
                            continuar = datos.ActualizarRFTASKRow(data, 0)
                            If continuar = False Then
                                'ERROR DE ACTUALIZADO
                                Console.WriteLine("Error Actualizando RFTASK " & vbCrLf & datos.mensaje)
                            Else
                                Console.WriteLine("RFTASK Actualizado")
                            End If
                        Case UCase("Java")
                    End Select
                    'Catch ex As Exception
                    '    Console.WriteLine("Error en: " & Accion & vbCrLf & ex.Message)
                    '    sts = -1
                    '    msgT = ex.Message.Replace("'", "''")
                    '    tLXmsg = "ERROR"

                    'End Try
                Next
            Else
                Console.WriteLine("No hay Tareas para ejecutar")
            End If
            Console.WriteLine("FIN Libreria " & librerias(i))
            datos.CerrarConexion()
        Next
        Console.WriteLine("FIN")
    End Sub

End Module
