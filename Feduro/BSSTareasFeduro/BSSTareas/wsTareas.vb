﻿Imports System.IO
Imports CLDatos
Imports IBM.Data.DB2.iSeries

Public Class wsTareas
    Private WithEvents m_timer As New Timers.Timer(30000)
    Private datos As New ClsDatosAS400

    Protected Overrides Sub OnStart(ByVal args() As String)
        Me.EventLog1.WriteEntry("Inicia")
        m_timer.Enabled = True
    End Sub

    Private Sub m_timer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles m_timer.Elapsed
        'revisaTareas()
        Try
            m_timer.Enabled = False
            revisaTareas()
        Catch ex4 As iDB2NullValueException
            Me.EventLog1.WriteEntry(datos.fSql & vbCrLf & ex4.InnerException.ToString & vbCrLf & ex4.Message & vbCrLf & ex4.StackTrace, EventLogEntryType.Error)
        Catch ex3 As iDB2ConnectionTimeoutException
            Me.EventLog1.WriteEntry(datos.fSql & vbCrLf & ex3.InnerException.ToString & vbCrLf & ex3.Message & vbCrLf & ex3.StackTrace, EventLogEntryType.Error)
        Catch ex2 As iDB2DCFunctionErrorException
            Me.EventLog1.WriteEntry(datos.fSql & vbCrLf & ex2.InnerException.ToString & vbCrLf & ex2.Message & vbCrLf & ex2.StackTrace, EventLogEntryType.Error)
        Catch ex1 As iDB2Exception
            Me.EventLog1.WriteEntry(datos.fSql & vbCrLf & ex1.InnerException.ToString & vbCrLf & ex1.Message & vbCrLf & ex1.StackTrace, EventLogEntryType.Error)
        Catch ex As Exception
            Me.EventLog1.WriteEntry(datos.fSql & vbCrLf & ex.Message & vbCrLf & ex.StackTrace, EventLogEntryType.Error)
        End Try
        m_timer.Enabled = True
    End Sub

    Protected Overrides Sub OnStop()
        m_timer.Stop()
    End Sub

End Class
