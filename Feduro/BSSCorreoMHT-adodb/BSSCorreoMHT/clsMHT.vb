﻿Public Class clsMHT

    Public Modulo, IdMsg, Adjuntos, Asunto, Cuerpo, De, Para, CC, url, local As String
    Public DB As clsConexion
    Private campoMail As String = "UEMAIL"
    Private mht As New Chilkat.Mht
    Private mailman As New Chilkat.MailMan
    Private email As New Chilkat.Email

    Function enviaMail() As String
        Dim resultado As String = ""
        Dim destBCC As String
        Dim campoParam As String = IIf(local = "SI", "CCNOT1", "")

        mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ") 'SMANRIMAILQ_ZEKrtWHSpOpZ
        mht.UnlockComponent("SMANRIMHT_dEnsKQ0g3Rue")

        mailman.SmtpHost = DB.getLXParam("SMTPHOST", campoParam)
        If DB.getLXParam("SMTPAUTH", campoParam) <> "DIRECTO" Then
            mailman.SmtpUsername = DB.getLXParam("SMTPUSERNAME", campoParam)
            mailman.SmtpPassword = DB.getLXParam("SMTPPASSWORD", campoParam)
        End If


        'mailman.SmtpHost = "mail.bssltda.com"
        'mailman.SmtpUsername = "soporte@bssltda.com"
        'mailman.SmtpPassword = "Oficina2016"

        resultado = correoMHT()
        If resultado = "OK" Then
            If Adjuntos.Trim <> "" Then
                If Not Adjuntar() Then
                    Return "Error adjuntando archivos."
                End If
            End If
            email.FromName = IIf(De <> "", De, "Alertas BRINSA")
            email.FromAddress = DB.getLXParam("SMTPUSERMAIL", "CCNOT1") '"admin.logistica@brinsa.com.co"
            destBCC = getBCC()
            Destinatarios("TO", Para)
            If CC.Trim <> "" Then
                Destinatarios("CC", CC)
            End If
            Destinatarios("BCC", destBCC)
            email.Subject = Asunto
            If mailman.SendEmail(email) Then
                resultado = "OK"
            Else
                DB.WrtSqlError(IdMsg, mailman.LastErrorHtml)
                DB.setLXParamNum("SMTPAUTH", "CCUDC2", -1)
                resultado = "Error en Envio"
            End If
            mailman.CloseSmtpConnection()
        End If
        Return resultado

    End Function

    Function enviaMailSimple() As Boolean
        Dim campoParam As String = IIf(local = "SI", "CCNOT1", "")
        Dim success As Boolean

        mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
        mailman.SmtpHost = DB.getLXParam("SMTPHOST", campoParam)
        If DB.getLXParam("SMTPAUTH", campoParam) <> "DIRECTO" Then
            mailman.SmtpUsername = DB.getLXParam("SMTPUSERNAME", campoParam)
            mailman.SmtpPassword = DB.getLXParam("SMTPPASSWORD", campoParam)
        End If

        email.Subject = Asunto
        email.SetHtmlBody("<html><body>" & Cuerpo & "</body></html>")
        email.From = De
        Destinatarios("TO", Para)

        success = mailman.SendEmail(email)
        mailman.CloseSmtpConnection()

        Return success

    End Function

    Sub Destinatarios(Tipo As String, Correos As String)
        Dim aDest() As String = Split(Correos, ",")
        Dim rs As New ADODB.Recordset
        Dim fSql As String

        For Each dest In aDest

            If InStr(dest, "@") = 0 Then
                If InStr(dest.ToUpper.Trim, "SELECT ") = 0 Then
                    fSql = "SELECT UNOM, " & campoMail & " AS UEMAIL FROM RCAU WHERE UUSR = '" & dest.ToUpper.Trim & "'"
                Else
                    fSql = dest.ToUpper.Trim
                End If
                rs = DB.ExecuteSQL(fSql)
                If Not rs.EOF Then
                    Select Case Tipo
                        Case "TO"
                            email.AddTo(rs("UNOM").Value, rs("UEMAIL").Value)
                        Case "CC"
                            email.AddCC(rs("UNOM").Value, rs("UEMAIL").Value)
                        Case "BCC"
                            email.AddBcc(rs("UNOM").Value, rs("UEMAIL").Value)
                    End Select
                End If
                rs.Close()
            Else
                Select Case Tipo
                    Case "TO"
                        email.AddTo("", dest)
                    Case "CC"
                        email.AddCC("", dest)
                    Case "BCC"
                        email.AddBcc("", dest)
                End Select
            End If
        Next

    End Sub

    Function Adjuntar() As Boolean

        Dim aAdjuntos() As String = Split(Adjuntos, ",")
        For Each adjunto In aAdjuntos
            If email.AddFileAttachment(adjunto) = vbNullString Then
                DB.WrtSqlError(IdMsg, email.LastErrorHtml)
                Return False
            End If
        Next
        Return True

    End Function

    'correoMHT()	
    Function correoMHT() As String
        Dim emlStr As String

        mht.UseCids = 1
        emlStr = mht.GetEML(url)
        If (emlStr = vbNullString) Then
            DB.WrtSqlError(IdMsg, mht.LastErrorHtml)
            Return "Error en MHT."
        End If
        If Not (email.SetFromMimeText(emlStr)) Then
            DB.WrtSqlError(IdMsg, email.LastErrorHtml)
            Return "Error en MimeText"
        End If
        Return "OK"

    End Function

    Public Function getBCC() As String
        Dim rs As New ADODB.Recordset
        Dim resultado As String = ""

        campoMail = "UEMAIL"
        If DB.OpenRS(rs, "SELECT CCDESC, CCSDSC FROM RFPARAM WHERE CCTABL='CORREOMHT' AND UPPER(CCCODE) = UPPER('" & Modulo & "') AND CCUDC1 = 1") Then
            resultado = rs("CCDESC").Value
            If rs("CCSDSC").Value <> "" Then
                campoMail = rs("CCSDSC").Value
            End If
        End If
        rs.Close()
        Return resultado.Trim

    End Function

    Sub avisoDesborde(Archivo As String)
        Dim rs As New ADODB.Recordset
        Dim fSql As String

        fSql = " SELECT CCCODE2 AS EMPRESA, CCCODE3 AS SOPORTE  "
        fSql = fSql & " FROM RFPARAM  "
        fSql = fSql & " WHERE CCTABL = 'LXLONG'   AND CCCODE = '" & Archivo & "'  "
        fSql = fSql & " AND CCUDC1 = 0 OR ( "
        fSql = fSql & "     CCUDC1 = 1 AND CCUDTS >= NOW() - 2 HOURS  ) "
        If Not DB.OpenRS(rs, fSql) Then
            rs.Close()
            Return
        End If

        fSql = "UPDATE RFPARAM SET CCUDC1 = 1, CCNOT1 ='Alerta', CCUDTS = NOW() WHERE CCTABL = 'LXLONG' AND CCCODE = '" & Archivo & "'"
        DB.ExecuteSQL(fSql)

        DB.gsKeyWords &= " avisoDesborde (1) " & Archivo & " " & DB.Estado()

        Modulo = Archivo
        Asunto = "Desbordamiento " & Archivo
        De = String.Format("Correo {0}<correo{0}@bssltda.com>", rs("EMPRESA").Value)
        Para = rs("SOPORTE").Value
        local = My.Settings.LOCAL.ToUpper
        Cuerpo = "Desbordamiento de archivo " & Archivo & vbCrLf
        Cuerpo &= "<br>Reviso y devuelva el parametro con<br>UPDATE RFPARAM SET CCUDC1 = 0 WHERE CCTABL = 'LXLONG'  AND CCCODE = '" & Archivo & "'"
        If enviaMailSimple() Then
            fSql = "UPDATE RFPARAM SET CCNOT1 = 'Desbordamiento de archivo' WHERE CCTABL = 'LXLONG' AND CCCODE = '" & Archivo & "'"
            DB.ExecuteSQL(fSql)
        Else
            fSql = "UPDATE RFPARAM SET CCNOT1 = 'No pudo enviar aviso' WHERE CCTABL = 'LXLONG' AND CCCODE = '" & Archivo & "'"
            DB.ExecuteSQL(fSql)
        End If
    End Sub

    Sub Inicializa()
        Modulo = ""
        IdMsg = ""
        Adjuntos = ""
        Asunto = ""
        De = ""
        Para = ""
        CC = ""
        url = ""
        local = ""
    End Sub


End Class
