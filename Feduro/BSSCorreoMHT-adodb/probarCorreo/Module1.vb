﻿Module Module1
    Private DB As clsConexion
    Private Correo As New clsMHT
    Public sEstado As String = ""

    Sub Main()
        Try
            If Not AbreConexion("") Then
                Exit Sub
            End If
            If AvisoLog() Then
                revisaTareasMHT()
            End If
            DB.Close()
        Catch ex As Exception
            DB.WrtSqlError(DB.lastSQL, ex.StackTrace & "<br />" & ex.Message)
        End Try
    End Sub

    Sub revisaTareasMHT()
        Dim fSql As String
        Dim rs As New ADODB.Recordset
        Dim serverMHT As String
        Dim local As String = IIf(My.Settings.LOCAL.ToUpper = "SI", "CCNOT1", "")
        Dim resultado As String = ""

        DB.gsKeyWords = "revisaTareasMHT (1)"
        serverMHT = DB.getLXParam("SMTPMHTSERVER", local)

        fSql = " SELECT COUNT(*) AS TOT FROM  RMAILX WHERE LENVIADO = 0"
        If DB.OpenRS(rs, fSql) Then
            DB.gsKeyWords = "revisaTareasMHT (2)"
            If rs("TOT").Value > 50 Then
                DB.gsKeyWords = "revisaTareasMHT (3)"
                With Correo
                    .Inicializa()
                    .DB = DB
                    DB.gsKeyWords = "revisaTareasMHT (4)"
                    .avisoDesborde("RMAILX")
                    DB.gsKeyWords = "Sale de avisoDesborde"
                End With
                rs.Close()
                Return
            End If
        End If
        rs.Close()

        fSql = " SELECT * FROM  RMAILX WHERE LENVIADO = 0"
        DB.OpenRS(rs, fSql)
        Do While Not rs.EOF
            DB.gsKeyWords = "revisaTareasMHT (5)"
            DB.gsKeyWords = "revisaTareasMHT, LID = " & rs("LID").Value
            fSql = " UPDATE RMAILX SET  "
            fSql = fSql & " LUPDDAT = NOW() , "
            fSql = fSql & " LENVIADO = -1,  "
            fSql = fSql & " LMSGERR = 'En proceso'  "
            fSql = fSql & " WHERE LID = " & rs("LID").Value
            DB.ExecuteSQL(fSql)

            With Correo
                .Inicializa()
                .DB = DB
                .Modulo = rs("LMOD").Value
                .Asunto = rs("LASUNTO").Value
                .De = IIf(rs("LENVIA").Value = "", rs("LREF").Value, rs("LENVIA").Value)
                .Para = rs("LPARA").Value
                .CC = rs("LCOPIA").Value
                .url = rs("LCUERPO").Value

                If InStr(.url.ToUpper, "HREF") <> 0 Then
                    .url = serverMHT & DB.getLXParam("SMTPAVISOGRAL", local) & "?LID=" & rs("LID").Value
                Else
                    If InStr(.url.ToUpper, "HTTP") = 0 Then
                        If InStr(.url.ToUpper, ".ASP") = 0 Then
                            .url = serverMHT & DB.getLXParam("SMTPAVISOGRAL", local) & "?LID=" & rs("LID").Value
                        Else
                            .url = serverMHT & .url
                        End If
                    End If
                End If

                .Adjuntos = rs("LADJUNTOS").Value
                .local = My.Settings.LOCAL.ToUpper
                resultado = .enviaMail()
                If (resultado = "OK") Then
                    fSql = " UPDATE RMAILX SET  "
                    fSql = fSql & " LUPDDAT = NOW() , "
                    fSql = fSql & " LENVIADO = 1,  "
                    fSql = fSql & " LMSGERR = 'OK'  "
                    fSql = fSql & " WHERE LID = " & rs("LID").Value
                Else
                    fSql = " UPDATE RMAILX SET  "
                    fSql = fSql & " LUPDDAT = NOW() , "
                    fSql = fSql & " LENVIADO = -1,  "
                    fSql = fSql & " LMSGERR = '" & resultado & "'  "
                    fSql = fSql & " WHERE LID = " & rs("LID").Value
                End If
                DB.ExecuteSQL(fSql)
            End With
            rs.MoveNext()
        Loop
        rs.Close()

    End Sub
    Function AvisoLog() As Boolean
        Dim fSql As String
        Dim rs As New ADODB.Recordset
        Dim serverMHT As String
        Dim local As String = IIf(My.Settings.LOCAL.ToUpper = "SI", "CCNOT1", "")
        Dim resultado As String = ""

        DB.gsKeyWords = "AvisoLog"
        serverMHT = DB.getLXParam("SMTPMHTSERVER", local)
        fSql = " SELECT COUNT(*) AS TOT FROM RFLOG WHERE ALERT = 1 "
        If DB.OpenRS(rs, fSql) Then
            If rs("TOT").Value > 50 Then
                With Correo
                    .Inicializa()
                    .DB = DB
                    .avisoDesborde("RFLOG")
                End With
                rs.Close()
                Return False
            End If
        End If
        rs.Close()

        fSql = " SELECT * FROM  RFLOG WHERE ALERT = 1 "
        DB.OpenRS(rs, fSql)
        Do While Not rs.EOF
            With Correo
                .Inicializa()
                .DB = DB
                .Modulo = "PGMERR"
                .Asunto = rs("PROGRAMA").Value
                .De = "Error en Programa"
                .Para = "BSS" & IIf(rs("ALERTTO").Value <> "", "," & rs("ALERTTO").Value, "")
                .url = serverMHT & DB.getLXParam("SMTPAVISOERROR", "") & "?ID=" & rs("ID").Value
                .local = My.Settings.LOCAL.ToUpper
                resultado = .enviaMail()
                If (resultado = "OK") Then
                    fSql = " UPDATE RFLOG SET ALERT = 2"
                    fSql = fSql & " WHERE ID = " & rs("ID").Value
                Else
                    fSql = " UPDATE RFLOG SET ALERT = -1"
                    fSql = fSql & " WHERE ID = " & rs("ID").Value
                End If
                DB.ExecuteSQL(fSql)
            End With
            rs.MoveNext()
        Loop
        rs.Close()
        Return True

    End Function

    Function AbreConexion(ModuloActual As String) As Boolean

        DB = New clsConexion
        DB.gsDatasource = My.Settings.AS400
        DB.gsLib = My.Settings.AMBIENTE
        DB.ModuloActual = ModuloActual
        Return DB.Conexion()

    End Function

End Module
