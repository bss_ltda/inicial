﻿using BSSFileUpload.Common;
using Dapper;
using IBM.Data.DB2.iSeries;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;

namespace BSSFileUpload.DB2
{
    public class RFADJUNTORepository : IRFADJUNTORepository
    {
        private IDbConnection db = new iDB2Connection(ConfigurationManager.ConnectionStrings["ConexionDB2"].ConnectionString);

        public List<RFADJUNTO> GetAll()
        {
            List<RFADJUNTO> result = this.db.Query<RFADJUNTO>("SELECT * FROM RFADJUNTO").ToList();
            return result;
        }

        public RFADJUNTO Find(int id)
        {
            string query = string.Format("SELECT * FROM RFADJUNTO WHERE (FANUMREG = {0})", id);
            return this.db.Query<RFADJUNTO>(query).SingleOrDefault();
        }

        public RFADJUNTO Add(RFADJUNTO RFADJUNTO)
        {            
            StringBuilder query = new StringBuilder();            
            query.Append("INSERT INTO RFADJUNTO ");
            query.Append("(FAPROCNUM, FAPROCLIN, FACATEG01, FACATEG02, FACATEG03, FADESCRIP, FACRTUSR, FACRTDAT, FAFILE, FATITURL, FAURL) ");            
            query.Append("VALUES (" + RFADJUNTO.FAPROCNUM + ", " + RFADJUNTO.FAPROCLIN + ", '" + RFADJUNTO.FACATEG01 + "', '" + RFADJUNTO.FACATEG02 + "', '" + RFADJUNTO.FACATEG03 + "', '" + RFADJUNTO.FADESCRIP + "', '" + RFADJUNTO.FACRTUSR + "', NOW(), '" + RFADJUNTO.FAFILE + "', '" + RFADJUNTO.FATITURL + "', '" + RFADJUNTO.FAURL + "')");
            
            try            
            {
                this.db.Execute(query.ToString());
                query.Clear();
                query.Append("SELECT IDENTITY_VAL_LOCAL() FROM SYSDUMMY1");
                var id = this.db.Query<int>(query.ToString()).Single();                
                RFADJUNTO.FANUMREG = id;
            }catch (iDB2SQLErrorException ex){
                string err = ex.Message;
            }
            catch (Exception ex)
            {
                string err = ex.Message;
            }               
            return RFADJUNTO;
        }

        public RFADJUNTO Update(RFADJUNTO RFADJUNTO)
        {
            StringBuilder query = new StringBuilder();
            query.Append("UPDATE RFADJUNTO ");
            query.Append("SET ");
            //query.Append("FAPROCNUM = @FAPROCNUM");
            query.Append("FAPROCLIN = @FAPROCLIN,");
            query.Append("FACATEG01 = @FACATEG01,");
            query.Append("FACATEG02 = @FACATEG02,");
            query.Append("FACATEG03 = @FACATEG03,");
            query.Append("FADESCRIP = @FADESCRIP,");
            query.Append("FACRTUSR = @FACRTUSR,");
            query.Append("FACRTDAT = @FACRTDAT,");
            query.Append("FAFILE = @FAFILE,");
            query.Append("FATITURL = @FATITURL,");
            query.Append("FAURL = @FAURL");
            query.Append("WHERE (FAPROCNUM = @FAPROCNUM)");
            this.db.Execute(query.ToString(), RFADJUNTO);
            return RFADJUNTO;
        }

        public void Remove(int id)
        {
            string query = string.Format("DELETE FROM RFADJUNTO WHERE (FANUMREG = {0})", id);
            this.db.Execute(query);
        }
    }
}
