﻿using BSSFileUpload.Common;
using Dapper;
using IBM.Data.DB2.iSeries;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;

namespace BSSFileUpload.DB2
{
    public class RFPARAMRepository : IRFPARAMRepository
    {
        private IDbConnection db = new iDB2Connection(ConfigurationManager.ConnectionStrings["ConexionDB2"].ConnectionString);

        public string GetSize(string CCCODE2)
        {
            string query = string.Format("SELECT CCCODEN FROM RFPARAM WHERE CCTABL = 'LXLONG' AND CCCODE = 'SIZE' AND CCCODE2 = UPPER('{0}')", CCCODE2);                        
            return this.db.Query<string>(query).SingleOrDefault();
        }

        public string GetPgm()
        {
            string query = "SELECT CCNOT1 FROM RFPARAM WHERE CCTABL = 'LXLONG' AND CCCODE = 'PROCEDURE' AND CCCODE2 = 'PGM'";
            return this.db.Query<string>(query).SingleOrDefault();
        }

        public string GetProc(string CCCODE2)
        {
            string query = "SELECT CCNOT1 FROM RFPARAM WHERE CCTABL = 'LXLONG' AND CCCODE = 'PROCEDURE' AND CCCODE2 = '" + CCCODE2 + "'";
            return (this.db.Query<string>(query).SingleOrDefault() == null) ? "" : this.db.Query<string>(query).SingleOrDefault();
        }
    }
}
