﻿using BSSFileUpload.Common;
using Dapper;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace BSSFileUpload.SQLServer
{
    public class RFTASKRepository : IRFTASKRepository
    {
        private IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionSQLServer"].ConnectionString);

        public RFTASK Add(RFTASK RFTASK)
        {
            db.ConnectionString += ";Initial Catalog=InfoNatura";
            StringBuilder query = new StringBuilder();
            query.Append("INSERT INTO RFTASK ");
            query.Append("(TKEYWORD, TCATEG, TLXMSG, TTIPO, TKEY, TPGM, TPRM) ");
            query.Append("VALUES (@TKEYWORD, @TCATEG, @TLXMSG, @TTIPO, @TKEY, @TPGM, @TPRM); ");
            query.Append("SELECT CAST(SCOPE_IDENTITY() AS INT)");
            var id = this.db.Query<int>(query.ToString(), RFTASK).Single();
            RFTASK.TNUMREG = id;
            return RFTASK;
        }
    }
}
