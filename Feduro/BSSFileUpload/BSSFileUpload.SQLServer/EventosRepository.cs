﻿using BSSFileUpload.Common;
using Dapper;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace BSSFileUpload.SQLServer
{
    public class EventosRepository : IEventosRepository
    {
        private IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionSQLServer"].ConnectionString);

        public Eventos Add(Eventos Eventos)
        {
            db.ConnectionString += ";Initial Catalog=InfoNatura";
            StringBuilder query = new StringBuilder();
            query.Append("INSERT INTO Eventos ");
            query.Append("(Macro, Estado) ");
            query.Append("VALUES (@Macro, @Estado); ");
            query.Append("SELECT CAST(SCOPE_IDENTITY() AS INT)");
            var id = this.db.Query<int>(query.ToString(), Eventos).Single();
            Eventos.id = id;
            return Eventos;
        }
    }
}
