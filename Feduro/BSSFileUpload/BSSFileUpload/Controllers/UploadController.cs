﻿using BSSFileUpload.Common;
using BSSFileUpload.Models;
using BSSFileUpload.DB2;
using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Text;

namespace BSSFileUpload.Controllers
{

    /// <summary>
    /// Clase BSSFileUpload.Controllers.UploadController
    /// </summary>
    public class UploadController : Controller
    {
        #region Acciones
        /// <summary>
        /// index
        /// </summary>
        /// <returns>
        /// ActionResult
        /// </returns>
        public ActionResult Index(int proc, int lin, string cat01, string cat02, string usr, string bd)
        {
            string fs = string.Empty;
            IRFPARAMRepository data = new RFPARAMRepository();
            fs = data.GetSize(cat01);
            var model = new UploadModel
            {
                Proc = (proc == null) ? 0 : proc,
                Lin = lin,
                Cat01 = cat01,
                Cat02 = cat02,
                Usr = usr,
                BD = bd,
                FileSize = fs
            };
            SessionManager.Set<UploadModel>("UploadModel", model);

            return View(model);
        }
                
        /// <summary>
        /// save file
        /// </summary>
        /// <returns>
        /// ActionResult
        /// </returns>
        public ActionResult SaveFile()
        {
            bool estaGuardado = true;
            bool result = false;
            string mensaje = string.Empty;
            //string pgm = string.Empty;
            //string proc = string.Empty;
            //Eventos Evento;
            //string idTarea = string.Empty;

            //Toma la sesion
            var model = SessionManager.Get<UploadModel>("UploadModel");

            //Toma la carpeta de archivos
            string targetFolder = Server.MapPath(Config.CarpetaArchivos);
            //Valida que exista sino se crea
            result = Utils.DirectoryExists(targetFolder, true);

            //Toma la carpeta de categoria 01
            string cat01Folder = Path.Combine(targetFolder, model.Cat01);
            //Valida que exista sino se crea
            result = Utils.DirectoryExists(cat01Folder, true);

            //Toma la carpeta de categoria 02
            string cat02Folder = Path.Combine(cat01Folder, model.Cat02);
            //Valida que exista sino se crea
            result = Utils.DirectoryExists(cat02Folder,  true);

            //Toma la carpeta de categoria 02
            string procFolder = Path.Combine(cat02Folder, string.Format("{0:00000000}", int.Parse(model.Proc.ToString())));
            //Valida que exista sino se crea
            result = Utils.DirectoryExists(procFolder, true);

            string fName = string.Empty;
            try
            {
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase file = Request.Files[fileName];
                    //Save file content goes here
                    fName = file.FileName;
                    if (file != null && file.ContentLength > 0)
                    {
                        string pathFile = Path.Combine(procFolder, file.FileName);
                        try
                        {
                            //file.SaveAs(pathFile);
                            WriteFileFromStream(file.InputStream, pathFile);
                        }
                        catch (Exception ex)
                        {
                            StringBuilder sb = new StringBuilder();
                            StreamWriter sw = new StreamWriter(Path.Combine(procFolder , "SaveASError" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".txt"));
                            sb.AppendLine(ex.Message);
                            sb.AppendLine(DateTime.Now.ToString());
                            sw.WriteLine(sb.ToString());
                            sw.Close();
                        }

                        //Guarda en base de datos
                        var item = new RFADJUNTO
                        {
                            FAPROCNUM = model.Proc,
                            FAPROCLIN = model.Lin,
                            FACATEG01 = model.Cat01,
                            FACATEG02 = model.Cat02,
                            FACATEG03 = model.Cat02,
                            FADESCRIP = pathFile,
                            FACRTUSR = model.Usr,
                            FABD = model.BD,
                            FACRTDAT = DateTime.Now,
                            FAFILE = Path.GetFileNameWithoutExtension(file.FileName),
                            FATITURL = Path.GetFileNameWithoutExtension(file.FileName),
                            FAURL = string.Format("{0}/{1}/{2}/{3:00000000}/{4}", Config.UrlWeb, model.Cat01, model.Cat02, model.Proc, file.FileName)
                        };
                        IRFADJUNTORepository data = new RFADJUNTORepository();
                        data.Add(item);

                        //IRFPARAMRepository datapgm = new RFPARAMRepository();
                        //pgm = datapgm.GetPgm();
                        //proc = datapgm.GetProc(model.Cat01);
                        //if (proc != "")
                        //{
                        //    var evento = new Eventos
                        //    {
                        //        Macro = model.Cat01,
                        //        Estado = "Creado"
                        //    };
                        //    IEventosRepository dataEventos = new EventosRepository();
                        //    Evento = dataEventos.Add(evento);
                        //    var tarea = new RFTASK
                        //    {
                        //        TKEYWORD = Convert.ToString(Evento.id),
                        //        TCATEG = model.Cat01,
                        //        TLXMSG = "PROGRAMADA",
                        //        TTIPO = "Procedimiento",
                        //        TKEY = Convert.ToString(Evento.id),
                        //        TPGM = pgm,
                        //        TPRM = "Proc=" + proc + " Params=@Id:" + Convert.ToString(Evento.id)
                        //    };
                        //    IRFTASKRepository dataRFTASK = new RFTASKRepository();
                        //    dataRFTASK.Add(tarea);
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                mensaje = ex.Message;
                estaGuardado = false;
            }

            if (estaGuardado)
            {
                return Json(new { Message = fName });
            }
            else
            {
                return Json(new { Message = "Error guardando el archivo: " + mensaje });
            }
        }

        public static void WriteFileFromStream(Stream stream, string toFile)
        {
            using (FileStream fileToSave = new FileStream(toFile, FileMode.Create))
            {
                stream.CopyTo(fileToSave);
            }
        }

        #endregion
    }
}
