﻿
using System.Web;
namespace BSSFileUpload.Models
{
    /// <summary>
    /// Clase BSSFileUpload.Models.UploadModel
    /// </summary>
    public class UploadModel
    {
        /// <summary>
        /// Obtiene o establece proc
        /// </summary>
        public int Proc { get; set; }
        /// <summary>
        /// Obtiene o establece lin
        /// </summary>
        public int Lin { get; set; }
        /// <summary>
        /// Obtiene o establece cat01
        /// </summary>
        public string Cat01 { get; set; }
        /// <summary>
        /// Obtiene o establece cat02
        /// </summary>
        public string Cat02 { get; set; }
        /// <summary>
        /// Obtiene o establece usuario
        /// </summary>
        public string Usr { get; set; }
        /// <summary>
        /// Obtiene o establece Base de Datos
        /// </summary>
        public string BD { get; set; }
        /// <summary>
        /// Obtiene o establece tamaño permitido
        /// </summary>
        public string FileSize { get; set; }
        /// <summary>
        /// Obtiene o establece archivo a subir
        /// </summary>
        public HttpPostedFileBase File { get; set; }
    }
}
