﻿using System.Web;

namespace BSSFileUpload
{
    /// <summary>
    /// Clase SessionManager
    /// </summary>
    public class SessionManager
    {
        /// <summary>
        /// Sets the specified key.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <param name="data">The data.</param>
        public static void Set<T>(string key, T data)
        {
            HttpContext context = HttpContext.Current;
            context.Session[key] = data;
        }

        /// <summary>
        /// Gets the specified key.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public static T Get<T>(string key)
        {
            HttpContext context = HttpContext.Current;
            return (T)context.Session[key];
        }
    }
}
