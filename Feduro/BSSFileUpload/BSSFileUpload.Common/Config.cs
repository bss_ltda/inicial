﻿using System.Configuration;

namespace BSSFileUpload.Common
{
    /// <summary>
    /// Clase BSSFileUpload.Config
    /// </summary>
    public static class Config
    {
        /// <summary>
        /// Obtiene o establece carpeta archivos
        /// </summary>
        public static string CarpetaArchivos { get { return ConfigurationManager.AppSettings["CarpetaArchivos"]; } }

        /// <summary>
        /// Obtiene o establece URL web
        /// </summary>
        public static string UrlWeb { get { return ConfigurationManager.AppSettings["UrlWeb"]; } }

        public static string MaxFiles
        {
            get
            {
                //int maxFiles = 10; //Default // maximo de archivos
                //int.TryParse(ConfigurationManager.AppSettings["MaxFiles"], out maxFiles);

                string maxFiles = ConfigurationManager.AppSettings["MaxFiles"];
                return maxFiles;
            }
        }

        public static string MaxFilesize
        {
            get
            {
                //int maxFilesize = 2; //Default
                //int.TryParse(ConfigurationManager.AppSettings["MaxFilesize"], out maxFilesize);


                string maxFilesize = ConfigurationManager.AppSettings["MaxFilesize"];
                return maxFilesize;
            }
        }
    }
}
