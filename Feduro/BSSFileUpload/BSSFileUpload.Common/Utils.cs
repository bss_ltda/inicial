﻿using System.IO;

namespace BSSFileUpload.Common
{
    /// <summary>
    /// Clase BSSFileUpload.Utils
    /// </summary>
    public static class Utils
    {
        /// <summary>
        /// directory exists
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="create">if set to <c>true</c> [create].</param>
        /// <returns>
        /// System.Boolean
        /// </returns>
        public static bool DirectoryExists(string name, bool create = false)
        {
            if (!Directory.Exists(name))
            {
                if (create)
                    Directory.CreateDirectory(name);

                return true;
            }
            return false;
        }
    }
}
