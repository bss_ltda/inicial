﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

namespace BSSFileUpload.Common
{
    public class RFPARAM
    {
        string fSql;
        SqlDataAdapter adapter = new SqlDataAdapter();
        public string mensaje;
        int numFilas;
        //public string appPath = System.IO.Directory.GetParent(Application.ExecutablePath) ;
        SqlConnection conexion = new SqlConnection(ConfigurationManager.ConnectionStrings["ConexionSQLServer"].ConnectionString);
        SqlCommandBuilder builder;

        /// <summary>
        /// Consulta la tabla RFPARAM con un determinado parametro de busqueda.Retorna un DataTable
        /// </summary>
        /// <param name="parametroBusqueda">parametro de Busqueda</param>
        /// <param name="tipoparametro">0- Busca por CCTABL(0) y CCCODE(1),
        /// 1- Busca por CCTABL(0) - CCCODE(1) y CCODE2(2)</param>
        /// <returns>Un DataTable</returns>
        /// <remarks>Autor: Jesus Santamaria Fecha: 20/04/2016</remarks>
        public string ConsultarRFPARAMstring(string CCCODE2, int tipoparametro)
        {
            DataTable dt = null;
            DataSet ds = null;

            switch (tipoparametro)
            {
                case 0:
                    fSql = "SELECT CCCODEN FROM RFPARAM WHERE CCTABL = 'LXLONG' AND CCCODE = 'SIZE' AND CCCODE2 = UPPER('" + CCCODE2 + "')";
                    break;
                case 1:
                    fSql = "SELECT CCNOT1 FROM RFPARAM WHERE CCTABL = 'LXLONG' AND CCCODE = 'PROCEDURE'";
                    break;
            }
            adapter = new SqlDataAdapter(fSql, conexion);

            adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;

            builder = new SqlCommandBuilder(adapter);
            dt = new DataTable();
            ds = new DataSet();
            numFilas = adapter.Fill(ds);
            if (numFilas > 0)
            {
                dt = ds.Tables[0];
            }
            else
            {
                dt = null;
            }
            adapter = null;
            builder = null;

            return Convert.ToString(dt.Rows[0].ItemArray[0]);
        }

        ///// <summary>
        ///// Guarda RFTASK
        ///// </summary>
        ///// <param name="datos">RFTASK</param>
        ///// <returns>true si se guarda con exito</returns>
        ///// <remarks> Autor: Jesús Santamaria Fecha: 27/07/2016</remarks>
        //public Boolean GuardarRFTASKRow(RFTASK datos)
        //{

        //    Boolean continuarguardando = false;
        //    using (SqlCommand Comm = new SqlCommand())
        //    {

        //        fSql = "INSERT INTO RFTASK (TKEYWORD, TCATEG, TLXMSG, TIPO, TKEY, TPGM, TPRM) VALUES (";
        //        fSql += "'" + datos.TKEYWORD + "', ";
        //        fSql += "'" + datos.TCATEG + "', ";
        //        fSql += "'" + datos.TLXMSG + "', ";
        //        fSql += "'" + datos.TIPO + "', ";
        //        fSql += datos.TKEY + ", ";
        //        fSql += "'" + datos.TPGM + "', ";
        //        fSql += "'" + datos.TPRM + "')";
        //        Comm.CommandText = fSql;
        //        Comm.Connection = conexion;
        //        try
        //        {
        //            Comm.ExecuteNonQuery();
        //            continuarguardando = true;
        //        }
        //        catch (SqlException ex)
        //        {
        //            continuarguardando = false;
        //            mensaje = ex.Message;
        //        }
        //        catch (Exception ex)
        //        {
        //            continuarguardando = false;
        //            mensaje = ex.Message;
        //        }
        //        finally
        //        {
        //            if (continuarguardando == false)
        //            {  
        //                var data = new RFLOG
        //                {
        //                //RFLOG data;
        //                //data = null;
        //                //data = new RFLOG();
        //                USUARIO = System.Net.Dns.GetHostName(),
        //                OPERACION = "Guardar RFTASK",
        //                PROGRAMA = Convert.ToString(this.GetType().Assembly.FullName) + "- V" + this.GetType().Assembly.ImageRuntimeVersion.ToString(),
        //                EVENTO = mensaje,
        //                TXTSQL = fSql,
        //                ALERT = "1",
        //                LKEY = ""
        //            };
        //                GuardarBFLOGRow(data);
        //            }
        //        }
        //    }
        //    return continuarguardando;
        //}

        ///// <summary>
        ///// Guarda RFLOG
        ///// </summary>
        ///// <param name="datos">RFLOG</param>
        ///// <returns>true si se guarda con exito</returns>
        ///// <remarks> Autor: Jesús Santamaria Fecha: 27/07/2016</remarks>
        //public Boolean GuardarBFLOGRow(RFLOG datos)
        //{
        //    Boolean Cont = false;
        //    using (SqlCommand Comm = new SqlCommand())
        //    {
        //        fSql = "INSERT INTO RFLOG (USUARIO, OPERACION, PROGRAMA, ALERT, EVENTO, TXTSQL) VALUES (";
        //        fSql += "'" + datos.USUARIO + "', ";
        //        fSql += "'" + datos.OPERACION + "', ";
        //        fSql += "'" + datos.PROGRAMA + "', ";
        //        fSql += datos.ALERT + ", ";
        //        fSql += "'" + datos.EVENTO.Replace("'", "''") + "', ";
        //        fSql += "'" + datos.TXTSQL.Replace("'", "''") + "')";
        //        Comm.CommandText = fSql;
        //        Comm.Connection = conexion;
        //        try
        //        {
        //            Comm.ExecuteNonQuery();
        //            Cont = true;
        //        }
        //        catch (SqlException ex)
        //        {
        //            Cont = false;
        //            mensaje = ex.Message;
        //        }
        //        catch (Exception ex)
        //        {
        //            Cont = false;
        //            mensaje = ex.Message;
        //        }
        //        finally
        //        {

        //        }
        //    }
        //    return Cont;
        //}

        ///// <summary>
        ///// Guarda Eventos
        ///// </summary>
        ///// <param name="datos">Eventos</param>
        ///// <returns>true si se guarda con exito</returns>
        ///// <remarks> Autor: Jesús Santamaria Fecha: 27/07/2016</remarks>
        //public string GuardarEventosRow(Eventos datos)
        //{
        //    string id;
        //    using (SqlCommand Comm = new SqlCommand())
        //    {
        //        fSql = "INSERT INTO Eventos (Macro, Estado) VALUES (";
        //        fSql += "'" + datos.Macro + "', ";
        //        fSql += "'" + datos.Estado + "')";
        //        Comm.CommandText = fSql;
        //        Comm.Connection = conexion;
        //        try
        //        {
        //            Comm.ExecuteNonQuery();
        //            Comm.CommandText = "SELECT @@IDENTITY";
        //            id = Comm.ExecuteScalar().ToString();
        //        }
        //        catch (SqlException ex)
        //        {
        //            id = "-1";
        //            mensaje = ex.Message;
        //        }
        //        catch (Exception ex)
        //        {
        //            id = "-1";
        //            mensaje = ex.Message;
        //        }
        //        finally
        //        {

        //        }
        //    }
        //    return id;
        //}

        public bool AbrirConexion(string bd)
        {
            bool abierta = false;
            try
            {
                conexion.ConnectionString += ";Initial Catalog=" + bd;
                if (conexion.State == ConnectionState.Open)
                {
                    abierta = true;
                }
                else
                {
                    conexion.Open();
                    abierta = true;
                }
            }
            catch (SqlException ex1)
            {
                abierta = false;
                mensaje = ex1.ToString();
                //StringBuilder sb = new StringBuilder();
                //StreamWriter sw = new StreamWriter(appPath + @"\ConexionError" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".txt");
                //sb.AppendLine("No se pudo realizar la conexión.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " + "\r\n" + ex1.InnerException.ToString() + "\r\n" + "Conexion: " + conexion.ConnectionString + "\r\n" + ex1.Message);
                //sb.AppendLine(DateTime.Now.ToString());
                //sw.WriteLine(sb.ToString());
                //sw.Close();
            }
            catch (Exception ex)
            {
                abierta = false;
                mensaje = ex.ToString();
                //StringBuilder sb = new StringBuilder();
                //StreamWriter sw = new StreamWriter(appPath + @"\ConexionError" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".txt");
                //sb.AppendLine("No se pudo realizar la conexión.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " + "\r\n" + ex.InnerException.ToString() + "\r\n" + "Conexion: " + conexion.ConnectionString + "\r\n" + ex.Message);
                //sb.AppendLine(DateTime.Now.ToString());
                //sw.WriteLine(sb.ToString());
                //sw.Close();
            }
            return abierta;
        }

        public bool CerrarConexion()
        {
            bool cerrada = false;
            try
            {
                conexion.Close();
                cerrada = true;
            }
            catch (SqlException ex1)
            {
                cerrada = false;
                mensaje = ex1.ToString();
                //StringBuilder sb = new StringBuilder();
                //StreamWriter sw = new StreamWriter(appPath + @"\ConexionError" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".txt");
                //sb.AppendLine("No se pudo Cerrar la conexión.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " + "\r\n" + ex1.InnerException.ToString() + "\r\n" + "Conexion: " + conexion.ConnectionString + "\r\n" + ex1.Message);
                //sb.AppendLine(DateTime.Now.ToString());
                //sw.WriteLine(sb.ToString());
                //sw.Close();
            }
            catch (Exception ex)
            {
                cerrada = false;
                mensaje = ex.ToString();
                //StringBuilder sb = new StringBuilder();
                //StreamWriter sw = new StreamWriter(appPath + @"\ConexionError" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".txt");
                //sb.AppendLine("No se pudo Cerrar la conexión.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " + "\r\n" + ex.InnerException.ToString() + "\r\n" + "Conexion: " + conexion.ConnectionString + "\r\n" + ex.Message);
                //sb.AppendLine(DateTime.Now.ToString());
                //sw.WriteLine(sb.ToString());
                //sw.Close();
            }
            return cerrada;
        }

    }
}

//public class RFLOG
//{
//    public String USUARIO;
//    public String PROGRAMA;
//    public String ALERT;
//    public String EVENTO;
//    public String LKEY;
//    public String TXTSQL;
//    public String OPERACION;
//}

//public class RFTASK
//{
//    public String TKEYWORD;
//    public String TCATEG;
//    public String TLXMSG;
//    public String TIPO;
//    public String TKEY;
//    public String TPGM;
//    public String TPRM;
//}

//public class Eventos
//{
//    public String Macro;
//    public String Estado;
//}
