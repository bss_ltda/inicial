﻿using System.Collections.Generic;

namespace BSSFileUpload.Common
{
    /// <summary>
    /// 
    /// </summary>
    public interface IRFADJUNTORepository
    {
        List<RFADJUNTO> GetAll();
        RFADJUNTO Find(int id);
        RFADJUNTO Add(RFADJUNTO RFADJUNTO);
        RFADJUNTO Update(RFADJUNTO RFADJUNTO);
        void Remove(int id);
    }
}
