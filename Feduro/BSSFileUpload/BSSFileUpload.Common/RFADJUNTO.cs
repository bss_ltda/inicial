﻿using System;

namespace BSSFileUpload.Common
{
    public class RFADJUNTO
    {
        public int FANUMREG { get; set; }
        public int FAPROCNUM { get; set; }
        public int FAPROCLIN { get; set; }
        public string FACATEG01 { get; set; }
        public string FACATEG02 { get; set; }
        public string FACATEG03 { get; set; }
        public string FADESCRIP { get; set; }
        public string FACRTUSR { get; set; }
        public DateTime FACRTDAT { get; set; }
        public string FAFILE { get; set; }
        public string FATITURL { get; set; }
        public string FAURL { get; set; }
        public string FABD { get; set; }
    }
}