﻿using System.Collections.Generic;

namespace BSSFileUpload.Common
{
    public interface IRFPARAMRepository
    {
        string GetSize(string CCCODE2);
        string GetPgm();
        string GetProc(string CCCODE2);
        //RFPARAMM Find(int id);
        //RFPARAMM Add(RFADJUNTO RFADJUNTO);
        //RFPARAMM Update(RFADJUNTO RFADJUNTO);
        //void Remove(int id);
    }
}
