﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSSFileUpload.Common
{
    public class RFTASK
    {
        public int TNUMREG { get; set; }
        public string TKEYWORD { get; set; }
        public string TCATEG { get; set; }
        public string TLXMSG { get; set; }
        public string TTIPO { get; set; }
        public string TKEY { get; set; }
        public string TPGM { get; set; }
        public string TPRM { get; set; }
    }
}
