﻿Imports CLDatos
Imports System.IO

Module EC
    Private datos As New ClsDatosAS400
    Public PeriodoAAAA As String
    Public PeriodoMM As String
    Public TASK_No As String
    Public NumeroDocumento As String = ""
    Private CARPETA_SITIO As String

    Sub Main()
        Dim parametro As String
        Dim aSubParam() As String

        Environment.GetCommandLineArgs()
        For Each parametro In Environment.GetCommandLineArgs()
            aSubParam = Split(parametro, "=")
            Select Case UCase(aSubParam(0))
                Case "A"
                    PeriodoAAAA = aSubParam(1)
                Case "M"
                    PeriodoMM = aSubParam(1)
                Case "TAREA"
                    TASK_No = aSubParam(1)
                Case "C"
                    NumeroDocumento = Trim(aSubParam(1))
            End Select
        Next

        GeneraCertificados()

    End Sub

    Sub GeneraCertificados()
        Dim campoParam As String = ""
        Dim dtBSSFFNCER As DataTable
        Dim parametro(2) As String
        Dim sCmd As String
        Dim exeCert As String = Path.GetFullPath(My.Application.Info.DirectoryPath & "\BSSCertificadoRetencion.exe")
        Dim msgErr As String
        Dim data
        Dim secuencia As String = ""

        datos.AbrirConexion()
        If datos.Piloto = "SI" Then
            campoParam = "CCNOT2"
        End If
        CARPETA_SITIO = datos.ConsultarRFPARAMString("SITIO_CARPETA", campoParam, "", 0)

        data = Nothing
        data = New RFTASK
        With data
            .STS2 = 1
            .TMSG = "Enviando..."
            .TNUMREG = TASK_No
        End With
        datos.ActualizarRFTASKRow(data, 0)

        parametro(0) = PeriodoAAAA
        parametro(1) = PeriodoMM
        parametro(2) = NumeroDocumento
        dtBSSFFNCER = datos.ConsultarBSSFFNCERdt(parametro, 2)
        If dtBSSFFNCER IsNot Nothing Then
            For Each row As DataRow In dtBSSFFNCER.Rows
                sCmd = String.Format("{0} P=01195872 C={1} A={2} M={3} TAREA={4}", exeCert, row("CSECUEN"), PeriodoAAAA, PeriodoMM, TASK_No)
                Shell(sCmd, AppWinStyle.NormalNoFocus, True)
                If InStr(row("VMDATN"), "@") = 0 Or InStr(row("VMDATN"), ".") = 0 Then
                    datos.wrtLog("Error en direccion de correo", row("CSECUEN"), "")
                End If
                msgErr = EnviarCorreo(String.Format("{0} {1}", row("CCNOT2"), PeriodoAAAA), row("CSECUEN"), row("VNDNAM"), row("VMDATN"), "")
                If msgErr <> "" Then
                    datos.wrtLog(msgErr, row("CSECUEN"), "")
                Else
                    datos.wrtLog(String.Format("Certificado {0:00000000}.pdf enviado a {1} de {2}", row("CSECUEN"), row("VMDATN"), row("VNDNAM")), row("CSECUEN"), "")
                End If
                If row("CSECUEN").ToString() <> secuencia Then
                    secuencia = row("CSECUEN")
                    data = Nothing
                    data = New BSSFFNCER
                    With data
                        .CSECUEN = row("CSECUEN")
                    End With
                    datos.ActualizarBSSFFNCERRow(data, 0)
                End If
            Next
            data = Nothing
            data = New RFTASK
            With data
                .STS2 = 1
                .TMSG = "Correos Enviados"
                .TNUMREG = TASK_No
            End With
            datos.ActualizarRFTASKRow(data, 0)

            data = Nothing
            data = New RFPARAM
            With data
                .CCUDC2 = 2
                .CCTABL = "PGMCTL"
                .CCCODE = "BSSCFNCER"
            End With
            datos.ActualizarRFPARAMRow(data, 0)
        End If
        datos.CerrarConexion()
    End Sub

    Function EnviarCorreo(per As String, No As String, Nombre As String, cuentaCorreo As String, PDFCertificado As String) As String
        Dim Correo As New class_EnviarCorreo
        With Correo
            .datos = datos
            .Asunto = "CERTIFICADO MENSUAL DE RETENCION"
            .url = String.Format("http://192.168.2.22/Common/mht/certif_rete.asp?per={0}&No={1}", per, No)
            If .InicializaMHT() Then
                .Destinatario(Nombre, cuentaCorreo)
                '.Destinatario("", "")
                .Archivo(String.Format(CARPETA_SITIO & "xm\pdf\CertifRet\{0}{1:00}\{2:00000000}.pdf", CInt(PeriodoAAAA), CInt(PeriodoMM), CLng(No)))
                If .Envia() Then
                    Return ""
                Else
                    Return .ErrorTXT()
                End If
            Else
                Return "Error " & .ErrorTXT()
            End If
        End With

    End Function

End Module
