﻿Imports CLDatos
Imports iTextSharp.text
Imports System.IO
Imports System.Text
Imports iTextSharp.text.pdf

Module CR
    Private datos As New ClsDatosAS400
    Private dtCab As DataTable
    Private dtDet As DataTable
    Private dtNotas As DataTable
    Dim totalvalor As Double
    Private data
    Private CARPETA_SITIO As String
    Private CARPETA_IMG As String
    Private TASK_No As String
    Private FDESDE As String
    Private FHASTA As String
    Private FTERNIT As String
    Private PDF_Folder As String
    Private bPRUEBAS As Boolean
    Public LOGO As String
    Public NOMBRE As String
    Public NIT As String
    Private LOG_File As String
    Private ArchivoPDF As String
    Private document As Document
    Private PeriodoAAAA As String
    Private PeriodoMM As String
    Private NumeroDocumento As String
    Public RENGLON3 As String
    Public RENGLON4 As String
    Public RENGLON5 As String
    Public RENGLON6 As String

    Sub Main()
        Dim parametro As String
        Dim aSubParam() As String
        Dim campoParam As String = ""
        Try
            Environment.GetCommandLineArgs()
            For Each parametro In Environment.GetCommandLineArgs()
                aSubParam = Split(parametro, "=")
                Select Case UCase(aSubParam(0))
                    Case "C"
                        NumeroDocumento = datos.Ceros(Trim(aSubParam(1)), 8)
                    Case "A"
                        PeriodoAAAA = aSubParam(1)
                    Case "M"
                        PeriodoMM = datos.Ceros(Trim(aSubParam(1)), 2)
                    Case "TAREA"
                        TASK_No = aSubParam(1)
                End Select
            Next

            If Not datos.AbrirConexion() Then
                Return
            End If

            If datos.Piloto = "SI" Then
                campoParam = "CCNOT2"
            End If
            CARPETA_SITIO = datos.ConsultarRFPARAMString("SITIO_CARPETA", campoParam, "", 0)
            CARPETA_IMG = datos.ConsultarRFPARAMString("CARPETA_IMG", campoParam, "", 0)
            LOGO = datos.ConsultarRFPARAMString("LOGO", campoParam, "", 0)
            NOMBRE = datos.ConsultarRFPARAMString("NOMBRE", campoParam, "", 0)
            NIT = datos.ConsultarRFPARAMString("NIT", campoParam, "", 0)
            RENGLON3 = datos.ConsultarRFPARAMString("RENGLON3", campoParam, "", 0)
            RENGLON4 = datos.ConsultarRFPARAMString("RENGLON4", campoParam, "", 0)
            RENGLON5 = datos.ConsultarRFPARAMString("RENGLON5", campoParam, "", 0)
            RENGLON6 = datos.ConsultarRFPARAMString("RENGLON6", campoParam, "", 0)
            bPRUEBAS = datos.Piloto.ToUpper() = "SI"
            PDF_Folder = CARPETA_SITIO & "xm\pdf\CertifRet\" & PeriodoAAAA & PeriodoMM
            Try
                If Not Directory.Exists(PDF_Folder) Then
                    Directory.CreateDirectory(PDF_Folder)
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\DirectorioError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo crear Directorio." & vbCrLf & ex.Message)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            LOG_File = PDF_Folder & "\" & NumeroDocumento & ".txt"
            datos.WrtTxtError(LOG_File, FTERNIT)

            Dim arguments As String() = Environment.GetCommandLineArgs()
            datos.WrtTxtError(LOG_File, String.Join(", ", arguments))

            If Not datos.CheckVer() Then
                datos.WrtSqlError(datos.lastError, "")
                End
            End If

            GenerarPDF()
            data = Nothing
            data = New RFTASK
            With data
                .STS2 = 1
                .TMSG = NumeroDocumento
                .TNUMREG = TASK_No
            End With
            datos.ActualizarRFTASKRow(data, 0)
            datos.CerrarConexion()
            Return
        Catch ex As Exception
            datos.wrtLogHtml("", ex.Message.ToString)
            datos.CerrarConexion()
            Return
        End Try
    End Sub

    Sub GenerarPDF()
        Dim Resultado As Boolean = False
        Dim parametro(2) As String

        parametro(0) = PeriodoAAAA
        parametro(1) = PeriodoMM
        parametro(2) = NumeroDocumento
        dtCab = datos.ConsultarBSSFFNCERdt(parametro, 0)
        If dtCab Is Nothing Then
            datos.WrtTxtError(LOG_File, "Certificado no se encontro.")
            Return
        End If

        dtDet = datos.ConsultarBSSFFNCERdt(parametro, 1)
        If dtDet Is Nothing Then
            datos.WrtTxtError(LOG_File, "Certificado sin detalle 1.")
            Return
        End If

        parametro(0) = PeriodoAAAA
        parametro(1) = PeriodoMM
        dtNotas = datos.ConsultarRMMTPIdt(parametro, 0)
        If dtNotas Is Nothing Then
            datos.WrtTxtError(LOG_File, "Sin declaracion.")
            Return
        End If

        ArchivoPDF = PDF_Folder & "\" & NumeroDocumento & ".pdf"
        'ArchivoPDF = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() & ".pdf"
        ArmaPDF()

        If bPRUEBAS Then
            'EJECUTA
            Dim prc As Process = New System.Diagnostics.Process()
            prc.StartInfo.FileName = ArchivoPDF
            prc.Start()
        End If
    End Sub

    Sub ArmaPDF()
        document = New Document(New Rectangle(288.0F, 144.0F), 36, 36, 140, 40)
        document.SetPageSize(iTextSharp.text.PageSize.LETTER)
        Console.WriteLine(document.PageSize.Width.ToString)
        document.AddTitle("Certificado de Retencion - " & NumeroDocumento)
        Dim es As eventos = New eventos()
        Dim pw As PdfWriter = PdfWriter.GetInstance(document, New FileStream(ArchivoPDF, FileMode.Create))
        pw.PageEvent = es
        document.Open()


        Dim unaTabla As PdfPTable = GenerarTabla()
        document.Add(unaTabla)

        Dim unaTabla2 As PdfPTable = detalleCerti()
        document.Add(unaTabla2)

        Dim unaTabla3 As PdfPTable = detalleCerti2()
        document.Add(unaTabla3)

        Dim unaTabla4 As PdfPTable = FinTabla()
        document.Add(unaTabla4)

        document.Close()
    End Sub

    Public Function GenerarTabla() As PdfPTable
        Dim flag As Integer = 0
        Console.WriteLine("Generando una tabla...")
        Dim tablaPadre As New PdfPTable(1)
        tablaPadre.SetWidthPercentage(New Single() {615}, PageSize.LETTER)
        tablaPadre.HorizontalAlignment = Element.ALIGN_CENTER
        tablaPadre.DefaultCell.BorderWidth = 0.5

        Dim unaTabla As New PdfPTable(1)
        unaTabla.HorizontalAlignment = Element.ALIGN_CENTER
        unaTabla.DefaultCell.BorderWidth = 0
        unaTabla.SetWidthPercentage(New Single() {615}, PageSize.LETTER)
        unaTabla.DefaultCell.FixedHeight = 100.0F

        Dim celda As PdfPCell

        celda = New PdfPCell(New Paragraph("Certificado N° " & NumeroDocumento, FontFactory.GetFont("Arial", 11, Font.BOLD)))
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("CERTIFICADO MENSUAL DE RETENCION I.T.B.M.S RETENIDO Y PAGADO", FontFactory.GetFont("Arial", 9)))
        celda.HorizontalAlignment = Element.ALIGN_CENTER
        celda.PaddingTop = 10
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("CERTIFICAMOS", FontFactory.GetFont("Arial", 9)))
        celda.HorizontalAlignment = Element.ALIGN_CENTER
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("QUE DURANTE EL PERIODO: " & PeriodoCertif() & " PRACTICAMOS RETENCION ITBMS A:", FontFactory.GetFont("Arial", 9)))
        celda.HorizontalAlignment = Element.ALIGN_CENTER
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(Trim(dtCab.Rows(0).Item("VNDNAM")), FontFactory.GetFont("Arial", 10, Font.BOLD)))
        celda.HorizontalAlignment = Element.ALIGN_CENTER
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("CON RUC N°: " & CStr(Trim(dtCab.Rows(0).Item("CTERNIT"))) & " DV " & CStr(Trim(dtCab.Rows(0).Item("VMUF02"))) & " POR LOS SIGUIENTES CONCEPTOS Y VALORES:", FontFactory.GetFont("Arial", 9)))
        celda.HorizontalAlignment = Element.ALIGN_CENTER
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(""))
        celda.BorderWidth = 0
        celda.FixedHeight = 18
        unaTabla.AddCell(celda)

        Dim celdapapa As New PdfPCell(unaTabla)
        celdapapa.BorderWidth = 0
        tablaPadre.AddCell(celdapapa)

        Return tablaPadre
    End Function

    Public Function detalleCerti() As PdfPTable
        Dim flag As Integer = 0
        Console.WriteLine("Generando detalleCerti...")
        Dim tablaPadre As New PdfPTable(1)
        tablaPadre.SetWidthPercentage(New Single() {500}, PageSize.LETTER)
        tablaPadre.HorizontalAlignment = Element.ALIGN_CENTER
        tablaPadre.DefaultCell.BorderWidth = 0.5

        Dim unaTabla As New PdfPTable(6)
        unaTabla.HorizontalAlignment = Element.ALIGN_CENTER
        unaTabla.DefaultCell.BorderWidth = 0
        unaTabla.SetWidthPercentage(New Single() {70, 119, 60, 83, 73, 93}, PageSize.LETTER)
        unaTabla.DefaultCell.FixedHeight = 100.0F

        Dim celda As PdfPCell

        Dim header1 As New PdfPCell(New Paragraph("CONCEPTO", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header1.PaddingBottom = 3
        header1.HorizontalAlignment = 1
        header1.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header2 As New PdfPCell(New Paragraph("BASE DE OPERACIÓN", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header2.PaddingBottom = 3
        header2.HorizontalAlignment = 1
        header2.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header3 As New PdfPCell(New Paragraph("% ITBMS", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header3.PaddingBottom = 3
        header3.HorizontalAlignment = 1
        header3.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header4 As New PdfPCell(New Paragraph("VALOR ITBMS", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header4.PaddingBottom = 3
        header4.HorizontalAlignment = 1
        header4.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header5 As New PdfPCell(New Paragraph("% RET", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header5.PaddingBottom = 3
        header5.HorizontalAlignment = 1
        header5.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header6 As New PdfPCell(New Paragraph("VLR. RETENCIÓN", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header6.PaddingBottom = 3
        header6.HorizontalAlignment = 1
        header6.VerticalAlignment = Element.ALIGN_MIDDLE

        header1.BorderWidth = 0
        header2.BorderWidth = 0
        header3.BorderWidth = 0
        header4.BorderWidth = 0
        header5.BorderWidth = 0
        header6.BorderWidth = 0

        header2.BorderWidthLeft = 0.5
        header3.BorderWidthLeft = 0.5
        header4.BorderWidthLeft = 0.5
        header5.BorderWidthLeft = 0.5
        header6.BorderWidthLeft = 0.5

        header1.BorderWidthBottom = 0.5
        header2.BorderWidthBottom = 0.5
        header3.BorderWidthBottom = 0.5
        header4.BorderWidthBottom = 0.5
        header5.BorderWidthBottom = 0.5
        header6.BorderWidthBottom = 0.5

        unaTabla.AddCell(header1)
        unaTabla.AddCell(header2)
        unaTabla.AddCell(header3)
        unaTabla.AddCell(header4)
        unaTabla.AddCell(header5)
        unaTabla.AddCell(header6)
        unaTabla.HeaderRows = 1

        If dtDet IsNot Nothing Then
            For Each row As DataRow In dtDet.Rows
                celda = New PdfPCell(New Paragraph(row("CTASIVA"), FontFactory.GetFont("Arial", 8)))
                celda.HorizontalAlignment = Element.ALIGN_CENTER
                celda.BorderWidth = 0
                If flag = 1 Then
                    celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                End If
                unaTabla.AddCell(celda)

                celda = New PdfPCell(New Paragraph(FormatNumber(row("BASE"), 2), FontFactory.GetFont("Arial", 8)))
                celda.BorderWidth = 0
                celda.BorderWidthLeft = 0.5
                celda.HorizontalAlignment = Element.ALIGN_RIGHT
                If flag = 1 Then
                    celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                End If
                unaTabla.AddCell(celda)

                celda = New PdfPCell(New Paragraph(FormatNumber(row("CPORIVA"), 2), FontFactory.GetFont("Arial", 8)))
                celda.BorderWidth = 0
                celda.BorderWidthLeft = 0.5
                celda.HorizontalAlignment = Element.ALIGN_RIGHT
                If flag = 1 Then
                    celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                End If
                unaTabla.AddCell(celda)

                celda = New PdfPCell(New Paragraph(FormatNumber(row("IVA"), 2), FontFactory.GetFont("Arial", 8)))
                celda.BorderWidth = 0
                celda.BorderWidthLeft = 0.5
                celda.HorizontalAlignment = Element.ALIGN_RIGHT
                If flag = 1 Then
                    celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                End If
                unaTabla.AddCell(celda)

                celda = New PdfPCell(New Paragraph(FormatNumber(row("CPORRTI"), 2), FontFactory.GetFont("Arial", 8)))
                celda.BorderWidth = 0
                celda.BorderWidthLeft = 0.5
                celda.HorizontalAlignment = Element.ALIGN_RIGHT
                If flag = 1 Then
                    celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                End If
                unaTabla.AddCell(celda)

                celda = New PdfPCell(New Paragraph(FormatNumber(row("RTI"), 2), FontFactory.GetFont("Arial", 8)))
                celda.BorderWidth = 0
                celda.BorderWidthLeft = 0.5
                celda.HorizontalAlignment = Element.ALIGN_RIGHT
                If flag = 1 Then
                    celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                    flag = 0
                Else
                    flag += 1
                End If
                unaTabla.AddCell(celda)
            Next
        End If

        Dim celdapapa As New PdfPCell(unaTabla)
        celdapapa.BorderWidth = 0.5
        tablaPadre.AddCell(celdapapa)

        celda = New PdfPCell(New Paragraph(""))
        celda.FixedHeight = 16
        celda.BorderWidth = 0
        tablaPadre.AddCell(celda)

        Return tablaPadre
    End Function

    Public Function detalleCerti2() As PdfPTable
        Dim flag As Integer = 0
        Console.WriteLine("Generando detalleCerti2...")
        Dim tablaPadre As New PdfPTable(1)
        tablaPadre.SetWidthPercentage(New Single() {360}, PageSize.LETTER)
        tablaPadre.HorizontalAlignment = Element.ALIGN_CENTER
        tablaPadre.DefaultCell.BorderWidth = 0.5

        Dim unaTabla As New PdfPTable(4)
        unaTabla.HorizontalAlignment = Element.ALIGN_CENTER
        unaTabla.DefaultCell.BorderWidth = 0
        unaTabla.SetWidthPercentage(New Single() {100, 80, 100, 80}, PageSize.LETTER)
        unaTabla.DefaultCell.FixedHeight = 100.0F

        Dim celda As PdfPCell

        Dim header1 As New PdfPCell(New Paragraph("Declaración", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header1.PaddingBottom = 3
        header1.HorizontalAlignment = 1
        header1.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header2 As New PdfPCell(New Paragraph("Fecha", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header2.PaddingBottom = 3
        header2.HorizontalAlignment = 1
        header2.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header3 As New PdfPCell(New Paragraph("Código de Pago", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header3.PaddingBottom = 3
        header3.HorizontalAlignment = 1
        header3.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header4 As New PdfPCell(New Paragraph("Fecha", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header4.PaddingBottom = 3
        header4.HorizontalAlignment = 1
        header4.VerticalAlignment = Element.ALIGN_MIDDLE

        header1.BorderWidth = 0
        header2.BorderWidth = 0
        header3.BorderWidth = 0
        header4.BorderWidth = 0

        header2.BorderWidthLeft = 0.5
        header3.BorderWidthLeft = 0.5
        header4.BorderWidthLeft = 0.5

        header1.BorderWidthBottom = 0.5
        header2.BorderWidthBottom = 0.5
        header3.BorderWidthBottom = 0.5
        header4.BorderWidthBottom = 0.5

        unaTabla.AddCell(header1)
        unaTabla.AddCell(header2)
        unaTabla.AddCell(header3)
        unaTabla.AddCell(header4)
        unaTabla.HeaderRows = 1

        If dtNotas IsNot Nothing Then
            For Each row As DataRow In dtNotas.Rows
                celda = New PdfPCell(New Paragraph(row("RPNDEC"), FontFactory.GetFont("Arial", 8)))
                celda.HorizontalAlignment = Element.ALIGN_CENTER
                celda.BorderWidth = 0
                If flag = 1 Then
                    celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                End If
                unaTabla.AddCell(celda)

                celda = New PdfPCell(New Paragraph(CDate(row("RPFDEC")).ToString("yyyy-MM-dd"), FontFactory.GetFont("Arial", 8)))
                celda.BorderWidth = 0
                celda.BorderWidthLeft = 0.5
                celda.HorizontalAlignment = Element.ALIGN_CENTER
                If flag = 1 Then
                    celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                End If
                unaTabla.AddCell(celda)

                celda = New PdfPCell(New Paragraph(row("RPNSTI"), FontFactory.GetFont("Arial", 8)))
                celda.BorderWidth = 0
                celda.BorderWidthLeft = 0.5
                celda.HorizontalAlignment = Element.ALIGN_CENTER
                If flag = 1 Then
                    celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                End If
                unaTabla.AddCell(celda)

                celda = New PdfPCell(New Paragraph(CDate(row("RPFSTI")).ToString("yyyy-MM-dd"), FontFactory.GetFont("Arial", 8)))
                celda.BorderWidth = 0
                celda.BorderWidthLeft = 0.5
                celda.HorizontalAlignment = Element.ALIGN_CENTER
                If flag = 1 Then
                    celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                    flag = 0
                Else
                    flag += 1
                End If
                unaTabla.AddCell(celda)
            Next
        End If



        Dim celdapapa As New PdfPCell(unaTabla)
        celdapapa.BorderWidth = 0.5
        tablaPadre.AddCell(celdapapa)

        celda = New PdfPCell(New Paragraph(""))
        celda.FixedHeight = 16
        celda.BorderWidth = 0
        tablaPadre.AddCell(celda)

        Return tablaPadre
    End Function

    Public Function FinTabla() As PdfPTable
        Dim flag As Integer = 0
        Console.WriteLine("Generando FinTabla...")
        Dim tablaPadre As New PdfPTable(1)
        tablaPadre.SetWidthPercentage(New Single() {500}, PageSize.LETTER)
        tablaPadre.HorizontalAlignment = Element.ALIGN_CENTER
        tablaPadre.DefaultCell.BorderWidth = 0.5

        Dim unaTabla As New PdfPTable(1)
        unaTabla.HorizontalAlignment = Element.ALIGN_CENTER
        unaTabla.DefaultCell.BorderWidth = 0
        unaTabla.SetWidthPercentage(New Single() {500}, PageSize.LETTER)
        unaTabla.DefaultCell.FixedHeight = 100.0F

        Dim celda As PdfPCell

        Dim texto As String = "LAS RETENCIONES FUERON PRACTICADAS EN LA CIUDAD DE PANAMA CONSIGNADAS OPORTUNAMENTE"
        texto &= " A LA ORDEN DE LA ADMINISTRACIÓN DE IMPUESTOS NACIONALES ""DGI"", AGENTE RETENEDOR"
        texto &= " SIN FIRMA AUTÓGRAFA - Resolucion No.201-17679 del 19 de Octubre de 2015"
        texto &= " SE EXPIDE LA PRESENTE CERTIFICACIÓN A: " & ExpedicionCertif()
        texto &= " ESTE CERTIFICADO REEMPLAZA TODOS EMITIDOS ANTERIORMENTE PARA EL MISMO PERIODO"

        celda = New PdfPCell(New Paragraph(texto, FontFactory.GetFont("Arial", 9)))
        celda.BorderWidth = 0
        celda.HorizontalAlignment = Element.ALIGN_CENTER
        unaTabla.AddCell(celda)

        Dim celdapapa As New PdfPCell(unaTabla)
        celdapapa.BorderWidth = 0
        tablaPadre.AddCell(celdapapa)

        Return tablaPadre
    End Function

    Function PeriodoCertif() As String
        Dim result As String = ""
        result = Trim(datos.ConsultarRFPARAMString(PeriodoMM, "", "MESES", 1)) & " DE: " & PeriodoAAAA

        'fSql = " SELECT CCNOT2 FROM RFPARAM WHERE CCTABL = 'MESES' AND CCCODEN = " & PeriodoMM
        'If DB.OpenRS(rs, fSql) Then
        '    result = rs("CCNOT2").Value & " DE: " & PeriodoAAAA
        'End If
        'rs.Close()
        'rs = Nothing
        Return result
    End Function

    Function ExpedicionCertif() As String
        Dim result As String = ""
        result = Trim(datos.ConsultarRFPARAMString(Month(Now()), "", "MESES", 1)) & ", " & String.Format("{0:dd yyyy}", Now())
        'fSql = " SELECT CCNOT2 FROM RFPARAM WHERE CCTABL = 'MESES' AND CCCODEN = " & Month(Now())
        'If DB.OpenRS(rs, fSql) Then
        '    result = rs("CCNOT2").Value & ", " & String.Format("{0:dd yyyy}", Now())
        'End If
        'rs.Close()
        'rs = Nothing
        Return result
    End Function

End Module
