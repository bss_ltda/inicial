﻿Imports iTextSharp.text.pdf
Imports iTextSharp.text

Public Class eventos
    Inherits PdfPageEventHelper

    Public Overrides Sub OnEndPage(writer As PdfWriter, document As Document)
        Try
            Dim pdfTab As New PdfPTable(2)
            Dim NomEmp As New PdfPCell(New Paragraph(NOMBRE, FontFactory.GetFont("Arial", 11, Font.BOLD)))
            Dim NitEmp As New PdfPCell(New Paragraph(NIT, FontFactory.GetFont("Arial", 9)))
            Dim parrafo1 As New PdfPCell(New Paragraph(RENGLON3, FontFactory.GetFont("Arial", 9)))
            Dim parrafo2 As New PdfPCell(New Paragraph(RENGLON4, FontFactory.GetFont("Arial", 9)))
            Dim parrafo3 As New PdfPCell(New Paragraph(RENGLON5, FontFactory.GetFont("Arial", 9)))
            Dim parrafo4 As New PdfPCell(New Paragraph(RENGLON6, FontFactory.GetFont("Arial", 9)))
            Dim logoemp As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(LOGO)
            Dim logoimgCell As PdfPCell = New PdfPCell(logoemp)

            NomEmp.BorderWidth = 0
            NitEmp.BorderWidth = 0
            parrafo1.BorderWidth = 0
            parrafo2.BorderWidth = 0
            parrafo3.BorderWidth = 0
            parrafo4.BorderWidth = 0
            logoimgCell.BorderWidth = 0

            logoemp.ScalePercent(65)

            logoimgCell.HorizontalAlignment = Element.ALIGN_LEFT
            logoimgCell.VerticalAlignment = Element.ALIGN_MIDDLE
            NomEmp.HorizontalAlignment = Element.ALIGN_CENTER
            NitEmp.HorizontalAlignment = Element.ALIGN_CENTER
            parrafo1.HorizontalAlignment = Element.ALIGN_CENTER
            parrafo2.HorizontalAlignment = Element.ALIGN_CENTER
            parrafo3.HorizontalAlignment = Element.ALIGN_CENTER
            parrafo4.HorizontalAlignment = Element.ALIGN_CENTER

            logoimgCell.Rowspan = 6

            pdfTab.AddCell(logoimgCell)
            pdfTab.AddCell(NomEmp)
            pdfTab.AddCell(NitEmp)
            pdfTab.AddCell(parrafo1)
            pdfTab.AddCell(parrafo2)
            pdfTab.AddCell(parrafo3)
            pdfTab.AddCell(parrafo4)

            pdfTab.SetWidthPercentage(New Single() {100, 440}, PageSize.LETTER)
            pdfTab.WriteSelectedRows(0, -1, 34.8, document.PageSize.Height - 35, writer.DirectContent)
        Catch ex As System.StackOverflowException
            Console.WriteLine(ex.StackTrace & vbCrLf & ex.Message)
        End Try
    End Sub

End Class
