﻿Imports CLDatos
Imports System.Text
Imports System.IO
Imports iTextSharp.text
Imports iTextSharp.text.pdf

Module DF
    Private datos As New ClsDatosAS400
    Private dtCab As DataTable
    Private dtDet As DataTable
    Dim totalvalor As Double
    Private data
    Private CARPETA_SITIO As String
    Private CARPETA_IMG As String
    Private TASK_No As String
    Private FDESDE As String
    Private FHASTA As String
    Private FTERNIT As String
    Private PDF_Folder As String
    Private bPRUEBAS As Boolean
    Public LOGO As String
    Public NOMBRE As String
    Public NIT As String
    Public RENGLON3 As String
    Public RENGLON4 As String
    Public RENGLON5 As String
    Public RENGLON6 As String
    Private LOG_File As String
    Private ArchivoPDF As String
    Private document As Document

    Sub Main()
        Dim parametro As String
        Dim aSubParam() As String
        Dim campoParam As String = ""
        Try
            Environment.GetCommandLineArgs()
            For Each parametro In Environment.GetCommandLineArgs()
                aSubParam = Split(parametro, "=")
                Select Case UCase(aSubParam(0))
                    Case "TAREA"
                        TASK_No = aSubParam(1)
                    Case "FDESDE"
                        FDESDE = aSubParam(1)
                    Case "FHASTA"
                        FHASTA = aSubParam(1)
                    Case "FTERNIT"
                        FTERNIT = aSubParam(1)
                End Select
            Next

            If Not datos.AbrirConexion() Then
                Return
            End If

            If datos.Piloto = "SI" Then
                campoParam = "CCNOT2"
            End If
            CARPETA_SITIO = datos.ConsultarRFPARAMString("SITIO_CARPETA", campoParam)
            CARPETA_IMG = datos.ConsultarRFPARAMString("CARPETA_IMG", campoParam)
            LOGO = datos.ConsultarRFPARAMString("LOGO", campoParam)
            NOMBRE = datos.ConsultarRFPARAMString("NOMBRE", campoParam)
            NIT = datos.ConsultarRFPARAMString("NIT", campoParam)
            RENGLON3 = datos.ConsultarRFPARAMString("RENGLON3", campoParam)
            RENGLON4 = datos.ConsultarRFPARAMString("RENGLON4", campoParam)
            RENGLON5 = datos.ConsultarRFPARAMString("RENGLON5", campoParam)
            RENGLON6 = datos.ConsultarRFPARAMString("RENGLON6", campoParam)
            bPRUEBAS = datos.Piloto.ToUpper() = "SI"
            PDF_Folder = CARPETA_SITIO & "xm\pdf\DetaFact"
            Try
                If Not Directory.Exists(PDF_Folder) Then
                    Directory.CreateDirectory(PDF_Folder)
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\DirectorioError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo crear Directorio." & vbCrLf & ex.Message)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            LOG_File = PDF_Folder & "\" & FTERNIT & ".txt"
            datos.WrtTxtError(LOG_File, FTERNIT)

            Dim arguments As String() = Environment.GetCommandLineArgs()
            datos.WrtTxtError(LOG_File, String.Join(", ", arguments))

            If Not datos.CheckVer() Then
                datos.WrtSqlError(datos.lastError, "")
                End
            End If

            GenerarPDF()
            datos.CerrarConexion()
            Return
        Catch ex As Exception
            datos.wrtLogHtml("", ex.Message.ToString)
            datos.CerrarConexion()
            Return
        End Try
    End Sub

    Sub GenerarPDF()
        Dim Resultado As Boolean = False
        Dim parametro(2) As String

        parametro(0) = FTERNIT
        parametro(1) = FDESDE
        parametro(2) = FHASTA
        dtCab = datos.ConsultarBSSFFNFACdt(parametro, 0)
        If dtCab Is Nothing Then
            datos.WrtTxtError(LOG_File, "Factura no encontrada.")
            Return
        End If

        parametro(0) = FDESDE
        parametro(1) = FHASTA
        parametro(2) = FTERNIT
        dtDet = datos.ConsultarBSSFFNFACdt(parametro, 1)
        If dtDet Is Nothing Then
            datos.WrtTxtError(LOG_File, "Factura sin detalle 1.")
            Return
        End If

        ArchivoPDF = PDF_Folder & "\" & FTERNIT & ".pdf"
        'ArchivoPDF = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() & ".pdf"
        ArmaPDF()

        If bPRUEBAS Then
            'EJECUTA
            Dim prc As Process = New System.Diagnostics.Process()
            prc.StartInfo.FileName = ArchivoPDF
            prc.Start()
        End If
    End Sub

    Sub ArmaPDF()
        document = New Document(New Rectangle(288.0F, 144.0F), 36, 36, 140, 40)
        document.SetPageSize(iTextSharp.text.PageSize.LETTER)
        Console.WriteLine(document.PageSize.Width.ToString)
        document.AddTitle("Detalle de Factura - " & FTERNIT)
        Dim es As eventos = New eventos()
        Dim pw As PdfWriter = PdfWriter.GetInstance(document, New FileStream(ArchivoPDF, FileMode.Create))
        pw.PageEvent = es
        document.Open()

        Dim unaTabla As PdfPTable = GenerarTabla()
        document.Add(unaTabla)

        Dim unaTabla2 As PdfPTable = detalleFact()
        document.Add(unaTabla2)

        document.Close()
    End Sub

    Public Function GenerarTabla() As PdfPTable
        Dim flag As Integer = 0
        Console.WriteLine("Generando una tabla...")
        Dim tablaPadre As New PdfPTable(1)
        tablaPadre.SetWidthPercentage(New Single() {615}, PageSize.LETTER)
        tablaPadre.HorizontalAlignment = Element.ALIGN_CENTER
        tablaPadre.DefaultCell.BorderWidth = 0.5

        Dim unaTabla As New PdfPTable(4)
        unaTabla.HorizontalAlignment = Element.ALIGN_CENTER
        unaTabla.DefaultCell.BorderWidth = 0
        unaTabla.SetWidthPercentage(New Single() {90, 355, 90, 80}, PageSize.LETTER)
        unaTabla.DefaultCell.FixedHeight = 100.0F

        Dim celda As PdfPCell

        Dim fechad As String = FDESDE.Substring(0, 4) & "-" & FDESDE.Substring(4, 2) & "-" & FDESDE.Substring(6, 2)
        Dim fechah As String = FHASTA.Substring(0, 4) & "-" & FHASTA.Substring(4, 2) & "-" & FHASTA.Substring(6, 2)

        celda = New PdfPCell(New Paragraph("FECHA DESDE:", FontFactory.GetFont("Arial", 9, Font.BOLD)))
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(CDate(fechad).ToString("yyyy-MM-dd"), FontFactory.GetFont("Arial", 9)))
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("FECHA HASTA:", FontFactory.GetFont("Arial", 9, Font.BOLD)))
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(CDate(fechah).ToString("yyyy-MM-dd"), FontFactory.GetFont("Arial", 9)))
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("PROVEEDOR:", FontFactory.GetFont("Arial", 9, Font.BOLD)))
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(CStr(Trim(dtCab.Rows(0).Item("VNDNAM"))), FontFactory.GetFont("Arial", 9)))
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("RUC: " & CStr(Trim(FTERNIT.ToString)) & " DV " & CStr(Trim(dtCab.Rows(0).Item("VMUF02"))), FontFactory.GetFont("Arial", 9, Font.BOLD)))
        celda.Colspan = 2
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("PAGAR A:", FontFactory.GetFont("Arial", 9, Font.BOLD)))
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(CStr(Trim(dtCab.Rows(0).Item("VNDNAM"))), FontFactory.GetFont("Arial", 9)))
        celda.Colspan = 3
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("DIRECCIÓN:", FontFactory.GetFont("Arial", 9, Font.BOLD)))
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(CStr(Trim(dtCab.Rows(0).Item("VNDAD1"))), FontFactory.GetFont("Arial", 9)))
        celda.Colspan = 3
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(""))
        celda.Colspan = 7
        celda.BorderWidth = 0
        celda.FixedHeight = 15
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("DETALLE DE FACTURAS", FontFactory.GetFont("Arial", 10, Font.BOLD)))
        celda.BorderWidth = 0
        celda.Colspan = 7
        celda.HorizontalAlignment = Element.ALIGN_CENTER
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(""))
        celda.Colspan = 7
        celda.BorderWidth = 0
        celda.FixedHeight = 10
        unaTabla.AddCell(celda)

        Dim celdapapa As New PdfPCell(unaTabla)
        celdapapa.BorderWidth = 0
        tablaPadre.AddCell(celdapapa)

        Return tablaPadre
    End Function

    Public Function detalleFact() As PdfPTable
        Dim flag As Integer = 0
        Console.WriteLine("Generando una tabla...")
        Dim tablaPadre As New PdfPTable(1)
        tablaPadre.SetWidthPercentage(New Single() {615}, PageSize.LETTER)
        tablaPadre.HorizontalAlignment = Element.ALIGN_CENTER
        tablaPadre.DefaultCell.BorderWidth = 0.5

        Dim unaTabla As New PdfPTable(7)
        unaTabla.HorizontalAlignment = Element.ALIGN_CENTER
        unaTabla.DefaultCell.BorderWidth = 0
        unaTabla.SetWidthPercentage(New Single() {60, 104, 87, 87, 87, 87, 97}, PageSize.LETTER)
        unaTabla.DefaultCell.FixedHeight = 100.0F

        Dim celda As PdfPCell

        Dim header1 As New PdfPCell(New Paragraph("FATURA", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header1.PaddingBottom = 3
        header1.HorizontalAlignment = 1
        header1.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header2 As New PdfPCell(New Paragraph("VLR. FACTURA", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header2.PaddingBottom = 3
        header2.HorizontalAlignment = 1
        header2.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header3 As New PdfPCell(New Paragraph("%ITBMS", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header3.PaddingBottom = 3
        header3.HorizontalAlignment = 1
        header3.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header4 As New PdfPCell(New Paragraph("VALOR ITBMS", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header4.PaddingBottom = 3
        header4.HorizontalAlignment = 1
        header4.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header5 As New PdfPCell(New Paragraph("% RET", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header5.PaddingBottom = 3
        header5.HorizontalAlignment = 1
        header5.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header6 As New PdfPCell(New Paragraph("VLR. RETENCIÓN", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header6.PaddingBottom = 3
        header6.HorizontalAlignment = 1
        header6.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header7 As New PdfPCell(New Paragraph("NETO A PAGAR", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header7.PaddingBottom = 3
        header7.HorizontalAlignment = 1
        header7.VerticalAlignment = Element.ALIGN_MIDDLE

        header1.BorderWidth = 0
        header2.BorderWidth = 0
        header3.BorderWidth = 0
        header4.BorderWidth = 0
        header5.BorderWidth = 0
        header6.BorderWidth = 0
        header7.BorderWidth = 0

        header2.BorderWidthLeft = 0.5
        header3.BorderWidthLeft = 0.5
        header4.BorderWidthLeft = 0.5
        header5.BorderWidthLeft = 0.5
        header6.BorderWidthLeft = 0.5
        header7.BorderWidthLeft = 0.5

        header1.BorderWidthBottom = 0.5
        header2.BorderWidthBottom = 0.5
        header3.BorderWidthBottom = 0.5
        header4.BorderWidthBottom = 0.5
        header5.BorderWidthBottom = 0.5
        header6.BorderWidthBottom = 0.5
        header7.BorderWidthBottom = 0.5

        unaTabla.AddCell(header1)
        unaTabla.AddCell(header2)
        unaTabla.AddCell(header3)
        unaTabla.AddCell(header4)
        unaTabla.AddCell(header5)
        unaTabla.AddCell(header6)
        unaTabla.AddCell(header7)
        unaTabla.HeaderRows = 1

        totalvalor = 0

        For Each row As DataRow In dtDet.Rows
            totalvalor += CDbl(row("NETO"))

            celda = New PdfPCell(New Paragraph(row("FDOCUM"), FontFactory.GetFont("Arial", 8)))
            celda.BorderWidth = 0
            celda.HorizontalAlignment = Element.ALIGN_CENTER
            If flag = 1 Then
                celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
            End If
            unaTabla.AddCell(celda)

            celda = New PdfPCell(New Paragraph(FormatNumber(row("VALOR"), 2), FontFactory.GetFont("Arial", 8)))
            celda.BorderWidth = 0
            celda.BorderWidthLeft = 0.5
            celda.HorizontalAlignment = Element.ALIGN_RIGHT
            If flag = 1 Then
                celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
            End If
            unaTabla.AddCell(celda)

            celda = New PdfPCell(New Paragraph(FormatNumber(row("FPORIVA"), 2), FontFactory.GetFont("Arial", 8)))
            celda.BorderWidth = 0
            celda.BorderWidthLeft = 0.5
            celda.HorizontalAlignment = Element.ALIGN_RIGHT
            If flag = 1 Then
                celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
            End If
            unaTabla.AddCell(celda)

            celda = New PdfPCell(New Paragraph(FormatNumber(row("IVA"), 2), FontFactory.GetFont("Arial", 8)))
            celda.BorderWidth = 0
            celda.BorderWidthLeft = 0.5
            celda.HorizontalAlignment = Element.ALIGN_RIGHT
            If flag = 1 Then
                celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
            End If
            unaTabla.AddCell(celda)

            celda = New PdfPCell(New Paragraph(FormatNumber(row("FPORRTI"), 2), FontFactory.GetFont("Arial", 8)))
            celda.BorderWidth = 0
            celda.BorderWidthLeft = 0.5
            celda.HorizontalAlignment = Element.ALIGN_RIGHT
            If flag = 1 Then
                celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
            End If
            unaTabla.AddCell(celda)

            celda = New PdfPCell(New Paragraph(FormatNumber(row("RETENCION"), 2), FontFactory.GetFont("Arial", 8)))
            celda.BorderWidth = 0
            celda.BorderWidthLeft = 0.5
            celda.HorizontalAlignment = Element.ALIGN_RIGHT
            If flag = 1 Then
                celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
            End If
            unaTabla.AddCell(celda)

            celda = New PdfPCell(New Paragraph(FormatNumber(row("NETO"), 2), FontFactory.GetFont("Arial", 8)))
            celda.BorderWidth = 0
            celda.BorderWidthLeft = 0.5
            celda.HorizontalAlignment = Element.ALIGN_RIGHT
            If flag = 1 Then
                celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                flag = 0
            Else
                flag += 1
            End If
            unaTabla.AddCell(celda)
        Next

        celda = New PdfPCell(New Paragraph("TOTAL", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        celda.BorderWidth = 0
        celda.BorderWidthTop = 0.5
        celda.Colspan = 6
        celda.HorizontalAlignment = Element.ALIGN_RIGHT
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(FormatNumber(totalvalor.ToString, 2), FontFactory.GetFont("Arial", 8)))
        celda.BorderWidth = 0
        celda.BorderWidthLeft = 0.5
        celda.BorderWidthTop = 0.5
        celda.HorizontalAlignment = Element.ALIGN_RIGHT
        unaTabla.AddCell(celda)

        Dim celdapapa As New PdfPCell(unaTabla)
        celdapapa.BorderWidth = 0.5
        tablaPadre.AddCell(celdapapa)

        Return tablaPadre
    End Function

End Module
