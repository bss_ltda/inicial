﻿Imports CLDatos
Imports System.IO

Module ED
    Private TASK_No As String
    Private FDESDE As String
    Private FHASTA As String
    Private FTERNIT As String
    Private NumeroDocumento As String = ""
    Private datos As New ClsDatosAS400
    Private CARPETA_SITIO As String

    Sub Main()
        GeneraCertificados()
    End Sub

    Sub GeneraCertificados()
        Dim campoParam As String = ""
        Dim sCmd As String
        'Dim exeCert As String = "C:\Feduro\DetalleFactura\DetalleFactura.exe"
        Dim exeCert As String = Path.GetFullPath(My.Application.Info.DirectoryPath & "\BSSDetalleFactura.exe")
        Dim msgErr As String
        Dim dtBSSFFNFAC As DataTable
        Dim parametro(0) As String

        datos.AbrirConexion()


        If datos.Piloto = "SI" Then
            campoParam = "CCNOT2"
        End If
        CARPETA_SITIO = datos.ConsultarRFPARAMString("SITIO_CARPETA", campoParam)

        dtBSSFFNFAC = datos.ConsultarBSSFFNFAC2dt(parametro, 0)
        If dtBSSFFNFAC IsNot Nothing Then
            For Each row As DataRow In dtBSSFFNFAC.Rows
                Console.WriteLine(datos.fSql)
                Console.WriteLine(String.Format("{0} TAREA=203737 FDESDE={1} FHASTA={2} FTERNIT={3}", exeCert, Trim(row("FDESDE")), Trim(row("FHASTA")), Trim(row("FTERNIT"))))
                sCmd = String.Format("{0} TAREA=203737 FDESDE={1} FHASTA={2} FTERNIT={3}", exeCert, Trim(row("FDESDE")), Trim(row("FHASTA")), Trim(row("FTERNIT")))
                Shell(sCmd, AppWinStyle.NormalNoFocus, True)
                If InStr(row("VMDATN"), "@") = 0 Or InStr(row("VMDATN"), ".") = 0 Then
                    datos.wrtLog("Error en direccion de correo", row("FTERNIT"), "")
                End If
                msgErr = EnviarCorreo(Trim(row("FDESDE")), Trim(row("FHASTA")), Trim(row("FTERNIT")), Trim(row("VNDNAM")), Trim(row("VMDATN")), "")
                If msgErr <> "" Then
                    datos.wrtLog(msgErr, row("FTERNIT"), "")
                Else
                    datos.wrtLog(String.Format("Detalle {0:00000000}.pdf enviado a {1} de {2}", Trim(row("FTERNIT")), Trim(row("VMDATN")), Trim(row("VNDNAM"))), Trim(row("FTERNIT")), "")
                End If
            Next
        End If
        datos.CerrarConexion()
    End Sub

    Function EnviarCorreo(desde As String, hasta As String, No As String, Nombre As String, cuentaCorreo As String, PDFCertificado As String) As String
        Console.WriteLine("Enviando Correo")
        Dim Correo As New class_EnviarCorreo
        With Correo
            .datos = datos
            .Asunto = "DETALLE DE FACTURACIÓN"
            .url = String.Format("http://192.168.2.22/Common/mht/deta_fac.asp?FDESDE={0}&FHASTA={1}&NO={2}.pdf", desde, hasta, No)
            If .InicializaMHT() Then
                .Destinatario(Nombre, cuentaCorreo)
                '.Destinatario("", "")
                .Archivo(String.Format(CARPETA_SITIO & "xm\pdf\DetaFact\{0}.pdf", No))
                If .Envia() Then
                    Return ""
                Else
                    Return .ErrorTXT()
                End If
            Else
                Return "Error " & .ErrorTXT()
            End If
        End With
    End Function

End Module
