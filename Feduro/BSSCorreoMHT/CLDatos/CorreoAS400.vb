﻿Public Class CorreoAS400
    Private datosas400 As New ClsDatosAS400
    Private Correo As New clsMHTAS400
    Private data

    Public Sub Inicializador()
        Try
            'If Not datosas400.AbrirConexion("") Then
            '    Exit Sub
            'End If
            If AvisoLog() Then
                revisaTareasMHTAS400()
            End If
            datosas400.CerrarConexion()
        Catch ex As Exception
            datosas400.WrtSqlError(datosas400.fSql, ex.Message & "<br />" & ex.Message)
        End Try
    End Sub

    Sub revisaTareasMHTAS400()
        Dim serverMHT As String
        Dim local As String = datosas400.local
        Dim resultado As String = ""
        Dim parametro(1) As String
        Dim dtRMAILX As DataTable
        Dim continuar As Boolean

        Console.WriteLine("Abriendo Conexión...")
        If Not datosas400.AbrirConexion("") Then
            Console.WriteLine("Error Abriendo Conexión " & datosas400.mensaje & vbCrLf & datosas400.conexionIBM.ConnectionString)
            Exit Sub
        End If
        Console.WriteLine("Conexión..." & datosas400.conexionibm.State)
        datosas400.gsKeyWords = "revisaTareasMHT (1)"
        parametro(0) = local
        parametro(1) = "SMTPMHTSERVER"
        serverMHT = datosas400.ConsultarRFPARAMdt(parametro, 0)

        Console.WriteLine("SMTPMHTSERVER: " & serverMHT)
        Console.WriteLine("Consulta RMAILX...")
        dtRMAILX = datosas400.ConsultarRMAILXdt(parametro, 0)
        If dtRMAILX IsNot Nothing Then
            datosas400.gsKeyWords = "revisaTareasMHT (2)"
            If dtRMAILX.Rows(0).Item("TOT") > 50 Then
                Console.WriteLine("Es Mayor a 50...")
                datosas400.gsKeyWords = "revisaTareasMHT (3)"
                With Correo
                    .Inicializa()
                    datosas400.gsKeyWords = "revisaTareasMHT (4)"
                    Console.WriteLine("Aviso Desborde")
                    .avisoDesborde("RMAILX")
                    datosas400.gsKeyWords = "Sale de avisoDesborde"
                End With

                'Me.EventLog1.WriteEntry("Mas de 50 Correos Pendientes.")
                Return
            End If
        End If
        dtRMAILX = datosas400.ConsultarRMAILXdt(parametro, 1)
        If dtRMAILX IsNot Nothing Then
            Console.WriteLine("Recorriendo RMAILX...")
            For Each row As DataRow In dtRMAILX.Rows
                datosas400.gsKeyWords = "revisaTareasMHT (5)"
                datosas400.gsKeyWords = "revisaTareasMHT, LID = " & row("LID")
                Data = Nothing
                Data = New RMAILX
                With Data
                    '.LUPDDAT = Now().ToString("yyyy-MM-dd HH:mm:ss")
                    .LENVIADO = "-1"
                    .LMSGERR = "En proceso"
                    .LID = row("LID")
                End With
                Console.WriteLine("Actualiza RMAILX -1 En Proceso...")
                continuar = datosas400.ActualizarRMAILXRow(Data, 0)
                If continuar = False Then
                    Console.WriteLine("Error Actualizando " & datosas400.mensaje)
                Else
                    Console.WriteLine("RMAILX -1 Actualizado")
                End If
                Console.WriteLine("Inicializando envio...")
                With Correo
                    .Inicializa()
                    If Not row("LMOD") Is DBNull.Value Then
                        .Modulo = row("LMOD")
                    Else
                        .Modulo = ""
                    End If
                    .Asunto = row("LASUNTO")
                    If Not row("LENVIA") Is DBNull.Value Then
                        If Not row("LREF") Is DBNull.Value Then
                            .De = IIf(row("LENVIA") = "", row("LREF"), row("LENVIA"))
                        Else
                            .De = row("LENVIA")
                        End If
                    Else
                        If Not row("LREF") Is DBNull.Value Then
                            .De = row("LREF")
                        Else
                            .De = ""
                        End If
                    End If
                    Console.WriteLine("Para: " & row("LPARA"))
                    .Para = row("LPARA")
                    If Not row("LCOPIA") Is DBNull.Value Then
                        .CC = row("LCOPIA")
                    Else
                        .CC = ""
                    End If

                    .url = row("LCUERPO")

                    If InStr(.url.ToUpper, "HREF") <> 0 Then
                        parametro(0) = local
                        parametro(1) = "SMTPAVISOGRAL"

                        .url = serverMHT & datosas400.ConsultarRFPARAMdt(parametro, 0) & "?LID=" & row("LID")
                    Else
                        If InStr(.url.ToUpper, "HTTP") = 0 Then
                            If InStr(.url.ToUpper, ".ASP") = 0 Then
                                parametro(0) = local
                                parametro(1) = "SMTPAVISOGRAL"
                                .url = serverMHT & datosas400.ConsultarRFPARAMdt(parametro, 0) & "?LID=" & row("LID")
                            Else
                                .url = serverMHT & .url
                            End If
                        End If
                    End If

                    If Not row("LADJUNTOS") Is DBNull.Value Then
                        .Adjuntos = row("LADJUNTOS")
                    Else
                        .Adjuntos = ""
                    End If

                    '.local = local
                    resultado = .enviaMail()
                    If (resultado = "OK") Then
                        Data = Nothing
                        Data = New RMAILX
                        With Data
                            '.LUPDDAT = Now().ToString("yyyy-MM-dd HH:mm:ss")
                            .LENVIADO = "1"
                            .LMSGERR = "OK"
                            .LID = row("LID")
                        End With
                        Console.WriteLine("Actualizando RMAILX 1 OK...")
                        continuar = datosas400.ActualizarRMAILXRow(Data, 0)
                        If continuar = False Then
                            Console.WriteLine("Error Actualizando RMAILX 1 OK..." & datosas400.mensaje)
                        End If
                    Else
                        Data = Nothing
                        Data = New RMAILX
                        With Data
                            '.LUPDDAT = Now().ToString("yyyy-MM-dd HH:mm:ss")
                            .LENVIADO = "-1"
                            .LMSGERR = resultado
                            .LID = row("LID")
                        End With
                        Console.WriteLine("Actualizando RMAILX -1 " & resultado & "...")
                        continuar = datosas400.ActualizarRMAILXRow(Data, 0)
                        If continuar = False Then
                            Console.WriteLine("Error Actualizando RMAILX -1 " & resultado & "..." & datosas400.mensaje)
                        End If
                    End If
                End With
            Next
        End If
        datosas400.CerrarConexion()
    End Sub

    Function AvisoLog() As Boolean
        Dim serverMHT As String
        Dim resultado As String = ""
        Dim parametro(1) As String
        Dim dtRFLOG As DataTable
        Dim continuar As Boolean
        Dim librerias() As String

        'datosas400.CerrarConexion()

        librerias = Split(My.Settings.LIBRERIAS, ",")
        For i = 0 To librerias.Length - 1
            Console.WriteLine("Abriendo Conexión...")
            If Not datosas400.AbrirConexion(librerias(i)) Then
                Console.WriteLine("Error Abriendo Conexión " & datosas400.mensaje & vbCrLf & datosas400.conexionIBM.ConnectionString)
                Return False
            End If

            datosas400.gsKeyWords = "AvisoLog"
            parametro(0) = datosas400.local
            parametro(1) = "SMTPMHTSERVER"
            serverMHT = datosas400.ConsultarRFPARAMdt(parametro, 0)
            Console.WriteLine("SMTPMHTSERVER: " & serverMHT)
            Console.WriteLine("Consultando RFLOG...")
            dtRFLOG = datosas400.ConsultarRFLOGdt(parametro, 0)
            If dtRFLOG IsNot Nothing Then
                If dtRFLOG.Rows(0).Item("TOT") > 50 Then
                    Console.WriteLine("RFLOG Mayor a 50")
                    With Correo
                        .Inicializa()
                        Console.WriteLine("Aviso Desborde RFLOG Mayor a 50")
                        .avisoDesborde("RFLOG")
                    End With
                    'Me.EventLog1.WriteEntry("Mas de 50 Correos Pendientes.")
                    Return False
                End If
            End If

            dtRFLOG = datosas400.ConsultarRFLOGdt(parametro, 1)
            If dtRFLOG IsNot Nothing Then
                Console.WriteLine("Recorriendo RFLOG...")
                For Each row As DataRow In dtRFLOG.Rows
                    With Correo
                        .Inicializa()
                        .Modulo = "PGMERR"
                        .Asunto = row("PROGRAMA")
                        .De = "Error en Programa"
                        If Not row("ALERTTO") Is DBNull.Value Then
                            .Para = "BSS" & IIf(row("ALERTTO") <> "", "," & row("ALERTTO"), "")
                        Else
                            .Para = "BSS"
                        End If
                        parametro(0) = ""
                        parametro(1) = "SMTPAVISOERROR"
                        .url = serverMHT & datosas400.ConsultarRFPARAMdt(parametro, 0) & "?ID=" & row("ID")
                        .libreria = librerias(i)
                        resultado = .enviaMail()
                        Console.WriteLine("Enviando Correo... " & resultado)
                        If (resultado = "OK") Then
                            data = Nothing
                            data = New RFLOG
                            With data
                                .ALERT = "2"
                                .ID = row("ID")
                            End With
                            Console.WriteLine("Actualizando RFLOG Alert 2")
                            continuar = datosas400.ActualizarRFLOGRow(data, 0)
                            If continuar = False Then
                                Console.WriteLine("Error Actualizando RFLOG " & datosas400.mensaje)
                            End If
                        Else
                            data = Nothing
                            data = New RFLOG
                            With data
                                .ALERT = "-1"
                                .ID = row("ID")
                            End With
                            Console.WriteLine("Actualizando RFLOG Alert -1")
                            continuar = datosas400.ActualizarRFLOGRow(data, 0)
                            If continuar = False Then
                                Console.WriteLine("Error Actualizando RFLOG " & datosas400.mensaje)
                            End If
                        End If
                    End With
                Next
            End If
            datosas400.CerrarConexion()
        Next
        Return True
    End Function

End Class
