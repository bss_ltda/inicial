﻿Imports IBM.Data.DB2.iSeries
Imports System.Text
Imports System.IO

Public Class ClsDatosAS400
    Public ReadOnly conexionIBM As New iDB2Connection()
    Private adapterIBM As iDB2DataAdapter = New iDB2DataAdapter()
    Private transIBM As iDB2Transaction
    Private builderIBM As iDB2CommandBuilder
    Private commandIBM As iDB2Command
    Private numFilas As Integer
    Private dt As DataTable
    Private WithEvents ds As DataSet
    Private continuarguardando As Boolean
    Public appPath As String = Path.GetFullPath(My.Application.Info.DirectoryPath & "\Log\")
    'Private appPath As String = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath & "\Log\"
    Public mensaje As String
    Private data
    Private dat As New ClsDatos
    Public fSql As String
    Public gsKeyWords As String
    Public local As String = IIf(My.Settings.LOCAL.ToUpper = "SI", "CCNOT1", "")

#Region "ACTUALIZAR"

    ''' <summary>
    ''' Actualiza la tabla RFLOG 
    ''' </summary>
    ''' <param name="datos">RFLOG</param>
    ''' <param name="tipo">0- SET ALERT = @ALERT WHERE ID = @ID</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 18/05/2016</remarks>
    Public Function ActualizarRFLOGRow(ByVal datos As RFLOG, ByVal tipo As Integer) As Boolean
        Select Case tipo
            Case 0
                fSql = " UPDATE RFLOG SET ALERT = " & datos.ALERT
                fSql &= " WHERE ID = " & datos.ID
        End Select

        commandIBM = New iDB2Command
        commandIBM.Connection = conexionIBM
        Try
            commandIBM.CommandText = fSql
            commandIBM.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As Data.SqlClient.SqlException
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                data = Nothing
                data = New RFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .OPERACION = "Actualizar RFLOG"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarRFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla RFPARAM 
    ''' </summary>
    ''' <param name="datos">RFPARAM</param>
    ''' <param name="tipo">0- SET @Campo = @Valor WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = @CCCODE</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 18/05/2016</remarks>
    Public Function ActualizarRFPARAMRow(ByVal datos As RFPARAM, ByVal tipo As Integer) As Boolean
        Select Case tipo
            Case 0
                If datos.Campo = "" Then
                    datos.Campo = "CCDESC"
                End If
                fSql = "UPDATE RFPARAM SET  " & datos.Campo & " = " & CDbl(datos.Valor)
                fSql &= " WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER('" & datos.CCCODE & "')"
        End Select

        commandIBM = New iDB2Command
        commandIBM.Connection = conexionIBM
        Try
            commandIBM.CommandText = fSql
            commandIBM.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As Data.SqlClient.SqlException
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFPARAMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla RFPARAM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFPARAMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla RFPARAM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                data = Nothing
                data = New RFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .OPERACION = "Actualizar RFPARAM"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarRFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla RMAILX 
    ''' </summary>
    ''' <param name="datos">RMAILX</param>
    ''' <param name="tipo">0- SET LUPDDAT = NOW(), LENVIADO = @LENVIADO, LMSGERR = @LMSGERR WHERE LID = @LID</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 18/05/2016</remarks>
    Public Function ActualizarRMAILXRow(ByVal datos As RMAILX, ByVal tipo As Integer) As Boolean
        Select Case tipo
            Case 0
                fSql = " UPDATE RMAILX SET "
                fSql &= " LUPDDAT = Now(), "
                fSql &= " LENVIADO = " & datos.LENVIADO & ", "
                fSql &= " LMSGERR = '" & datos.LMSGERR & "' "
                fSql &= " WHERE LID = " & datos.LID
        End Select

        commandIBM = New iDB2Command
        commandIBM.Connection = conexionIBM
        Try
            commandIBM.CommandText = fSql
            commandIBM.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As Data.SqlClient.SqlException
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RMAILXError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla RMAILX.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RMAILXError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla RMAILX.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                data = Nothing
                data = New RFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .OPERACION = "Actualizar RMAILX"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarRFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

#End Region

#Region "CONSULTAR"

    ''' <summary>
    ''' Consulta la tabla RFPARAM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por CCCODE(1) devuelve (0)</param>
    ''' <returns>Un String</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 18/05/2016</remarks>
    Public Function ConsultarRFPARAMdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As String
        dt = Nothing

        Console.WriteLine(conexionIBM.ConnectionString)
        Select Case tipoparametro
            Case 0
                If parametroBusqueda(0) = "" Then
                    parametroBusqueda(0) = "CCDESC"
                End If
                fSql = "SELECT " & parametroBusqueda(0) & " AS DATO FROM RFPARAM WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER('" & parametroBusqueda(1) & "')"
                Console.WriteLine(fSql)
        End Select
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)

        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        Try
            numFilas = adapterIBM.Fill(ds)
        Catch ex As Exception
            Console.WriteLine(ex.ToString)
        End Try

        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing

        Return dt.Rows(0).Item(0)
    End Function

    ''' <summary>
    ''' Consulta la tabla RFPARAM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por CCCODE(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 04/05/2016</remarks>
    Public Function ConsultarRFPARAMdt2(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT CCDESC, CCSDSC FROM RFPARAM WHERE CCTABL='CORREOMHT' AND UPPER(CCCODE) = UPPER('" & parametroBusqueda(0) & "') AND CCUDC1 = 1"
        End Select
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)

        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla RCAU con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por UUSR devuelve UNOM y (0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 18/05/2016</remarks>
    Public Function ConsultarRCAUdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT UNOM, " & parametroBusqueda(0) & " AS UEMAIL FROM RCAU WHERE UUSR = '" & parametroBusqueda(1) & "'"
        End Select
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)

        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla RFLOG con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por ALERT=1 devuelve COUNT,
    ''' 1- Busca por ALERT=1</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 18/05/2016</remarks>
    Public Function ConsultarRFLOGdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        Select Case tipoparametro
            Case 0
                fSql = " SELECT COUNT(*) AS TOT FROM RFLOG WHERE ALERT = 1"
            Case 1
                fSql = " SELECT * FROM  RFLOG WHERE ALERT = 1"
        End Select
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)

        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla RMAILX con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por LENVIADO = 0 retorna COUNT,
    ''' 1- Busca por LENVIADO = 0</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 18/05/2016</remarks>
    Public Function ConsultarRMAILXdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        Select Case tipoparametro
            Case 0
                fSql = " SELECT COUNT(*) AS TOT FROM  RMAILX WHERE LENVIADO = 0"
            Case 1
                fSql = " SELECT * FROM  RMAILX WHERE LENVIADO = 0"
        End Select
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)

        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

#End Region

#Region "FUNCIONES"

    Public Function AbrirConexion(ByVal libreria As String) As Boolean
        Dim abierta As Boolean = False
        Try
            If conexionIBM.State = ConnectionState.Open Then
                abierta = True
            Else
                If libreria <> "" Then
                    conexionIBM.ConnectionString = My.Settings.BASEDATOSAS400 & ";DefaultCollection=" & libreria
                Else
                    conexionIBM.ConnectionString = My.Settings.BASEDATOSAS400
                End If
                conexionIBM.Open()
                abierta = True
            End If
        Catch ex1 As iDB2Exception
            abierta = False
            mensaje = ex1.ToString
        Catch ex As Exception
            abierta = False
            mensaje = ex.ToString
        End Try
        Return abierta
    End Function

    Public Function CerrarConexion() As Boolean
        Dim cerrada As Boolean = False
        Try
            conexionIBM.Close()
            cerrada = True
        Catch ex1 As iDB2Exception
            cerrada = False
            mensaje = ex1.ToString
        Catch ex As Exception
            cerrada = False
            mensaje = ex.ToString
        End Try
        Return cerrada
    End Function

    Sub WrtSqlError(sql As String, ByVal Descrip As String)
        Dim continuar As Boolean
        Dim application As String

        AbrirConexion("")

        If gsKeyWords.Trim <> "" Then
            Descrip = gsKeyWords & "<br>" & Descrip
        End If
        Descrip = Left(Descrip, 20000)

        Try
            application = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
            data = Nothing
            data = New RFLOG
            With data
                .USUARIO = System.Net.Dns.GetHostName()
                .PROGRAMA = application
                .ALERT = "1"
                .EVENTO = Replace(Descrip, "'", "''")
                .TXTSQL = Replace(sql, "'", "''")
            End With
            continuar = GuardarRFLOGRow(data)
            If continuar = False Then
                'ERROR DE GUARDADO
            End If
        Catch ex As Exception

        End Try
    End Sub

#End Region

#Region "GUARDAR"

    ''' <summary>
    ''' Guarda RFLOG
    ''' </summary>
    ''' <param name="datos">RFLOG</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 18/05/2016</remarks>
    Public Function GuardarRFLOGRow(ByVal datos As RFLOG) As Boolean

        fSql = "INSERT INTO RFLOG (USUARIO, OPERACION, PROGRAMA, EVENTO, TXTSQL, ALERT)"
        fSql &= " VALUES ('" & datos.USUARIO & "', '" & datos.OPERACION & "', '" & datos.PROGRAMA & "', '" & datos.EVENTO.Replace("'", "''") & "', '" & datos.TXTSQL.Replace("'", "''") & "', " & datos.ALERT & ")"

        commandIBM = New iDB2Command
        commandIBM.Connection = conexionIBM
        Try
            commandIBM.CommandText = fSql
            commandIBM.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As Data.SqlClient.SqlException
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo guardar en la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Catch ex As Exception
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo guardar en la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Finally

        End Try
        Return continuarguardando
    End Function

#End Region

End Class
