﻿Imports System.ServiceProcess
Imports CLDatos
Imports System.Text
Imports System.IO

Public Class wsCorreoMHT
    Private WithEvents m_timer As New Timers.Timer(30000)
    Private correo As New Correo
    Private as400correo As New CorreoAS400

    Protected Overrides Sub OnStart(ByVal args() As String)
        Me.EventLog1.WriteEntry("Inicia")
        m_timer.Enabled = True
    End Sub

    Protected Overrides Sub OnStop()
        m_timer.Stop()
    End Sub

    Private Sub m_timer_Elapsed(sender As Object, e As Timers.ElapsedEventArgs) Handles m_timer.Elapsed
        Try
            m_timer.Enabled = False
            If My.Settings.AS400CORREO = "SI" Then
                as400correo.Inicializador()
            Else
                correo.Inicializador()
            End If
        Catch ex As Exception
            Me.EventLog1.WriteEntry(ex.Message & vbCrLf & ex.StackTrace)
        End Try
        m_timer.Enabled = True
    End Sub

    

End Class
