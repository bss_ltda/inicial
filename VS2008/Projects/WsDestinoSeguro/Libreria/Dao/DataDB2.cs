﻿using System;
using IBM.Data.DB2.iSeries;

namespace BSS.Brinsa.Ws.DestinoSeguro.Libreria.Dao
{
    /// <summary>
    /// Clase BSS.Brinsa.Ws.DestinoSeguro.Libreria.Dao.DataDB2
    /// </summary>
    public class DataDB2
    {
        #region Miembros
        /// <summary>
        /// Objeto que contiene la base de datos
        /// </summary>
        private DB2Database bd;
        /// <summary>
        /// Objeto de comando
        /// </summary>
        private iDB2Command comando;
        /// <summary>
        /// Variable que almacena la consulta a la base de datos
        /// </summary>
        private String consulta;
        /// <summary>
        /// Numero de registros insertados
        /// </summary>
        private int registro;
        #endregion

        #region Propiedades
        /// <summary>
        /// Devuelve o establece la base de datos
        /// </summary>
        /// <value></value>
        protected DB2Database BD
        {
            get { return bd; }
            set { bd = value; }
        }

        /// <summary>
        /// Devuelve o establece el comando
        /// </summary>
        /// <value></value>
        protected iDB2Command Comando
        {
            get { return comando; }
            set { comando = value; }
        }

        /// <summary>
        /// Devuelve o establece el query
        /// </summary>
        protected string Query
        {
            get { return this.consulta; }
            set { this.consulta = value; }
        }

        /// <summary>
        /// Devuelve o establece el procedimiento
        /// </summary>
        protected string Procedimiento
        {
            get { return this.consulta; }
            set { this.consulta = value; }
        }

        /// <summary>
        /// Devuelve o establece el <see cref="registro"/>
        /// </summary>
        public int Registro
        {
            get { return registro; }
            set { registro = value; }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="BSS.Brinsa.Ws.DestinoSeguro.Libreria.Dao.DataDB2"/>
        /// </summary>
        public DataDB2()
        {
            BD = new DB2Database(Utilidades.BaseDatos.ConexionDB2.ToString());
        }
        #endregion

        #region Metodos publicos
        #endregion

        #region Metodos privados
        #endregion

    }
}
