﻿using System;
using log4net;

namespace BSS.Brinsa.Ws.DestinoSeguro.Libreria
{
    /// <summary>
    /// Clase BSS.Brinsa.Ws.DestinoSeguro.Libreria.Log
    /// </summary>
    public class Log
    {
        #region Miembros
        #endregion

        #region Propiedades
        #endregion

        #region Metodos
        /// <summary>
        /// for
        /// </summary>
        /// <param name="objeto">The objeto.</param>
        /// <returns>log4net.ILog</returns>
        public static ILog For(Object objeto)
        {
            if (objeto != null)
                return For(objeto.GetType());
            else
                return For(null);
        }

        /// <summary>
        /// for
        /// </summary>
        /// <param name="tipo">The tipo.</param>
        /// <returns>log4net.ILog</returns>
        public static ILog For(Type tipo)
        {
            if (tipo != null)
                return LogManager.GetLogger(tipo.Name);
            else
                return LogManager.GetLogger(string.Empty);
        }
        #endregion
    }
}
