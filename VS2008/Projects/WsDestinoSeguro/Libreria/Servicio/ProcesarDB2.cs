﻿using System;
using System.Collections.Generic;
using BSS.Brinsa.Ws.DestinoSeguro.Libreria.Dao;
using IBM.Data.DB2.iSeries;
using System.Text;

namespace BSS.Brinsa.Ws.DestinoSeguro.Libreria.Servicio
{
    /// <summary>
    /// Clase BSS.Brinsa.Ws.DestinoSeguro.Libreria.Servicio.ProcesarDB2
    /// </summary>
    public class ProcesarDB2 : DataDB2
    {
        #region Miembros
        #endregion

        #region Propiedades
        #endregion

        #region Constructor
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="BSS.Brinsa.Ws.DestinoSeguro.Libreria.Servicio.ProcesarDB2"/>
        /// </summary>
        public ProcesarDB2() { }
        #endregion

        #region Metodos Protegidos
        /// <summary>
        /// inserta el query o los query de inserccion
        /// </summary>
        /// <param name="sql">The SQL.</param>
        /// <returns>System.Boolean</returns>
        protected bool Insertar(string sql)
        {
            Query = sql;
            Comando = BD.GetSqlStringCommand(Query);
            Comando.CommandTimeout = Utilidades.CommandTimeout;

            try
            {
                Registro = BD.ExecuteNonQuery(Comando);
                Log.For(this).Info(Query);
                return true;
            }
            catch (Exception ex)
            {
                Log.For(this).Error(string.Format("Insertando en AS400 ({0})", sql), ex);
                throw;
            }
        }

        /// <summary>
        /// existe dato
        /// </summary>
        /// <param name="query">el/la query</param>
        /// <returns>System.Boolean</returns>
        protected bool ExisteDato(string query)
        {
            Query = query;
            Comando = BD.GetSqlStringCommand(Query);
            Comando.CommandTimeout = Utilidades.CommandTimeout;

            try
            {
                Registro = (int)BD.ExecuteScalar(Comando);

                if (Registro != 0)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                Log.For(this).Error(string.Format("Validando si existe el dato en AS400 ({0})", Query), ex);
                throw;
            }
        }
        #endregion

        #region Metodos Publicos
        /// <summary>
        /// consultar estado
        /// </summary>
        /// <param name="consolidado">The consolidado.</param>
        /// <returns>System.Int32</returns>
        public int ConsultarEstado(string consolidado)
        {
            Query = string.Format("SELECT HESTAD FROM RHCC WHERE (HCONSO = '{0}')", consolidado);
            Comando = BD.GetSqlStringCommand(Query);
            Comando.CommandTimeout = Utilidades.CommandTimeout;


                      System.Text.StringBuilder sql = new System.Text.StringBuilder();

                      sql.Append(" INSERT INTO SITMOV                                               ");
                      sql.Append(" SELECT                                                           ");
                      sql.Append(" VARCHAR(GLH.LHYEAR) || ',' ||                                    ");
                      sql.Append(" VARCHAR(GLH.LHPERD)  || ',' ||                                   ");
                      sql.Append(" VARCHAR(GLH.LHJNLN) || ',' ||                                    ");
                      sql.Append(" '\"' || TRIM(GLH.LHJNEN) || '\",' ||                             ");
                      sql.Append(" '\"' || SUBSTRING(GLH.LHJNEN, 1, 2) || ',' ||                    ");
                      sql.Append(" VARCHAR(GLH.LHDATE) || ',' ||                                    ");
                      sql.Append(" '\"' || TRIM(GLH.LHDREF) || '\",' ||                             ");
                      sql.Append(" VARCHAR(GLH.LHDDAT) || ',' ||                                    ");
                      sql.Append(" '\"' || TRIM(GLH.LHLDES) || '\",' ||                             ");
                      sql.Append(" '\"' || TRIM(GLH.LHREAS) || '\",' ||                             ");
                      sql.Append(" REPLACE( VARCHAR(GLH.LHDRAM - GLH.LHCRAM), ',', '.') || ',' ||   ");
                      sql.Append(" REPLACE( VARCHAR(GLH.LHDRAM), ',', '.') || ',' ||                ");
                      sql.Append(" REPLACE( VARCHAR(GLH.LHCRAM), ',', '.') || ',' ||                ");
                      sql.Append(" '\"' || TRIM(GCR.CRSG01) || '\",' ||                             ");
                      sql.Append(" '\"' || TRIM(GCR.CRSG02)  || '\",' ||                            ");
                      sql.Append(" '\"' || TRIM(GCR.CRSG03) || '\",' ||                             ");
                      sql.Append(" '\"' || TRIM(GLH.LHJRF1)   || '\",' ||                           ");
                      sql.Append(" '\"' || TRIM(GLH.LHJRF2)  || '\",' ||                            ");
                      sql.Append(" VARCHAR(GHH.HHBEOR) || ',' ||                                    ");
                      sql.Append(" VARCHAR(GHH.HHJDAT)                                              ");
                      sql.Append(" FROM                                                             ");
                      sql.Append(" GLH                                                              ");
                      sql.Append(" INNER JOIN                                                       ");
                      sql.Append(" GHH                                                              ");
                      sql.Append(" ON GLH.LHLDGR = GHH.HHLDGR                                       ");
                      sql.Append(" AND GLH.LHBOOK = GHH.HHBOOK                                      ");
                      sql.Append(" AND GLH.LHYEAR = GHH.HHYEAR                                      ");
                      sql.Append(" AND GLH.LHPERD = GHH.HHPERD                                      ");
                      sql.Append(" AND GLH.LHJNEN = GHH.HHJNEN                                      ");
                      sql.Append(" INNER JOIN                                                       ");
                      sql.Append(" GCR GCR                                                          ");
                      sql.Append(" ON GLH.LHIAN = GCR.CRIAN                                         ");
                      sql.Append(" WHERE                                                            ");
                      sql.Append(" (GLH.LHLDGR = 'PIMPOLLO')                                        ");
                      sql.Append(" AND (GLH.LHBOOK = 'FISCAL')                                      ");
                      sql.Append(" AND GLH.LHYEAR = 2012 AND GLH.LHPERD=3                           ");

            try
            {
                object resultado = BD.ExecuteScalar(Comando);
                if (resultado != null)
                    return Convert.ToInt32(resultado);
                return 0;
            }
            catch (Exception ex)
            {
                Log.For(this).Error(string.Format("Consultando el estado ({0})", Query), ex);
                throw;
            }
        }

        /// <summary>
        /// Existencia de Consolidado
        /// </summary>
        /// <param name="consolidado">The consolidado.</param>
        /// <returns>System.Int32</returns>
        public bool ConsultarConsolidado(string consolidado)
        {
            Query = string.Format("SELECT HCONSO FROM RHCC WHERE (HCONSO = '{0}')", consolidado);
            Comando = BD.GetSqlStringCommand(Query);
            Comando.CommandTimeout = Utilidades.CommandTimeout;

            try
            {
                object resultado = BD.ExecuteScalar(Comando);
                if (resultado != null)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                Log.For(this).Error(string.Format("Consultando ({0})", Query), ex);
                throw;
            }
        }


        /// <summary>
        /// validar usuario
        /// </summary>
        /// <param name="usuario">The usuario.</param>
        /// <param name="password">The password.</param>
        /// <returns>System.Boolean</returns>
        public bool ValidarUsuario(string usuario, string password)
        {
            try
            {
                Query = string.Format("SELECT COUNT(*) FROM RCAU WHERE (UUSR = '{0}') AND (UPASS = '{1}')", usuario, password);
                if (ExisteDato(Query))
                    return true;
                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// insert RRCC
        /// </summary>
        /// <param name="consolidado">The consolidado.</param>
        /// <param name="manifiesto">The manifiesto.</param>
        /// <param name="placa">The placa.</param>
        /// <param name="estadoEntrega">The estado entrega.</param>
        /// <param name="fechaReporte">The fecha reporte.</param>
        /// <param name="codigoUbicacion">The codigo ubicacion.</param>
        /// <param name="ubicacion">The ubicacion.</param>
        /// <param name="tipoOrigen">The tipo origen.</param>
        /// <param name="tipoNovedad">The tipo novedad.</param>
        /// <param name="reporte">The reporte.</param>
        /// <param name="usuario">The usuario.</param>
        public void ReporteRRCC( int NumReporte, string consolidado, string manifiesto, string placa, int estadoEntrega, DateTime fechaReporte, 
                                string codigoUbicacion, string ubicacion, string tipoOrigen, string tipoNovedad, string reporte, string usuario)
        {
            try
            {
                //Crea el Query
                StringBuilder fSql = new StringBuilder();
                StringBuilder vSql = new StringBuilder();

                fSql.Append("INSERT INTO RRCC( "); 
                vSql.Append(" VALUES ( ");
                fSql.Append( " RTREP     , ");     vSql.AppendFormat("{0}, ", 4 );                                                //Tipo Rep
                fSql.Append( " RCONSO    , ");     vSql.AppendFormat("'{0}', ", Utilidades.ValidarTamano(consolidado, 6) );       //Consolidado
                fSql.Append( " RMANIF    , ");     vSql.AppendFormat("'{0}', ",  Utilidades.ValidarTamano(manifiesto, 15) );      //Manifiesto del Transp
                fSql.Append( " RPLACA    , ");     vSql.AppendFormat("'{0}', ", Utilidades.ValidarTamano(placa, 6) );             //Placa
                fSql.Append( " RSTSC     , ");     vSql.AppendFormat("{0}, ", ConsultarEstado(consolidado) );                     //Est Cons
                fSql.Append( " RSTSE     , ");     vSql.AppendFormat("{0}, ", estadoEntrega );                                    //Est Entrega
                fSql.Append( " RFECREP   , ");     vSql.AppendFormat("'{0}', ", Utilidades.ArmarFechaHora(fechaReporte) );        //F.Reporte
                fSql.Append( " RCODUBI   , ");     vSql.AppendFormat("'{0}', ", Utilidades.ValidarTamano(codigoUbicacion, 4) );   //Codigo Ubicacion
                fSql.Append( " RUBIC     , ");     vSql.AppendFormat("'{0}', ", Utilidades.ValidarTamano(ubicacion, 60) );        //Ubicacion
                fSql.Append( " RTIPORI   , ");     vSql.AppendFormat("'{0}', ", Utilidades.ValidarTamano(tipoOrigen, 3) );        //Tipo de Origen de Nov
                fSql.Append( " RTIPNOV   , ");     vSql.AppendFormat("'{0}', ", Utilidades.ValidarTamano(tipoNovedad, 3) );       //Tipo de Novedad
                fSql.Append( " RREPORT   , ");     vSql.AppendFormat("'{0}', ", Utilidades.ValidarTamano(reporte, 500) );         //Reporte
                fSql.Append("  RNUM      , ");     vSql.AppendFormat("{0}, ", NumReporte);                                        //Numero unico
                fSql.Append( " RCRTUSR   , ");     vSql.AppendFormat("'{0}', ", usuario);                                         //Usuario
                fSql.Append( " RAPP      ) ");     vSql.AppendFormat("'{0}') ", "WSREPORTET" );                                   //App
                fSql.Append( vSql.ToString());

                Query = fSql.ToString();

                //Ejecuta
                this.Insertar(Query);
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Numero del Reporte
        /// </summary>
        /// <returns>System.Int32</returns>
        public int NumeroReporte()
        {
            Query = "SELECT ( NEXT VALUE  FOR RRCC_SEQ  ) AS NUMREP FROM RFUNO";
            Comando = BD.GetSqlStringCommand(Query);
            Comando.CommandTimeout = Utilidades.CommandTimeout;

            try
            {
                object resultado = BD.ExecuteScalar(Comando);
                if (resultado != null)
                    return Convert.ToInt32(resultado);
                return 0;
            }
            catch (Exception ex)
            {
                Log.For(this).Error(string.Format("Numero del Reporte ({0})", Query), ex);
                throw;
            }
        }
        
        #endregion
    }
}
