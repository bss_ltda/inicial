﻿using System.Web.Services.Protocols;

namespace BSS.Brinsa.Ws.DestinoSeguro.Servicios
{
    /// <summary>
    /// Clase BSS.Brinsa.Ws.DestinoSeguro.Servicios.Authentication
    /// </summary>
    public class Authentication : SoapHeader
    {
        /// <summary>
        /// Estable el usuario
        /// </summary>
        public string User;
        /// <summary>
        /// Establece la contraseña
        /// </summary>
        public string Password;
    }
}
