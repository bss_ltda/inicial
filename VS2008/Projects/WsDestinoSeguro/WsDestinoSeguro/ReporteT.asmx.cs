﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using BSS.Brinsa.Ws.DestinoSeguro.Libreria.Servicio;

namespace BSS.Brinsa.Ws.DestinoSeguro.Servicios
{
    /// <summary>
    /// Inserta registro de novedad de consolidado en el RRCC
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]

    public class ReporteT : System.Web.Services.WebService
    {
        #region Miembros
        private ProcesarDB2 proceso = new ProcesarDB2();
        #endregion

        #region Propiedades
        /// <summary>
        /// Obtiene o establece proceso
        /// </summary>
        /// <value></value>
        internal ProcesarDB2 Proceso
        {
            get
            {
                if (proceso == null)
                    proceso = new ProcesarDB2();
                return proceso;
            }
        }
        #endregion
        [WebMethod]
        public string InsertRRCC( string usuario, string password, string consolidado, string manifiesto, string placa, int estadoEntrega, DateTime fechaReporte,
                                  string codigoUbicacion, string ubicacion, string tipoOrigen, string tipoNovedad, string reporte)
        {
            string result ="";
            int NumRep = -1;
            if (Proceso.ValidarUsuario(usuario, password))
                if (Proceso.ConsultarConsolidado(consolidado))
                {
                    NumRep = proceso.NumeroReporte();
                    Proceso.ReporteRRCC(NumRep, consolidado, manifiesto, placa, estadoEntrega, fechaReporte, codigoUbicacion, ubicacion, tipoOrigen, tipoNovedad, reporte, usuario);
                    result = NumRep.ToString() + ": Reporte Registrado.";
                }
                else result = "-2: Consolidado Incorrecto.";
            else result = "-1: Usuario No Válido";
                //"Usuario Invalido.<br>[" + usuario + "]<br>[*******" + "]<br>[" + consolidado + "]<br>[" + manifiesto + "]<br>[" + placa + "]<br>[" + estadoEntrega + "]<br>[" +
                //           fechaReporte + "]<br>[" + codigoUbicacion + "]<br>[" + ubicacion + "]<br>[" + tipoOrigen + "]<br>[" + tipoNovedad + "]<br>[" + reporte + "]";

            return result;
        }

    }
}
