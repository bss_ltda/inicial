﻿using System;
using System.Collections.Generic;
using System.Text;
//using ConsoleTest.ServicioWeb;
using ConsoleTest.co.com.brinsa.transportes;

namespace ConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Servicio servicio = new Servicio();

                Authentication auth = new Authentication();
                auth.User = "DSEGURO";
                auth.Password = "PRUEBAS";
                servicio.AuthenticationValue = auth;
                string resultado = servicio.Test();
                Console.Out.WriteLine(resultado);

                servicio.InsertRRCC("258575", "8017318", "USA848", 3, DateTime.Now, "44", "OC", "1", "OK", "MONDOÑEDO");
                Console.Out.WriteLine("Paso");
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine(ex.Message);
            }

            Console.Out.WriteLine("Oprima una tecla para continuar...");
            Console.In.Read();
        }
    }
}
