﻿Imports System.IO
Imports System.Data.SqlClient
Module Module1

    Sub Main()
        Dim conn As New SqlConnection(My.Settings.InfoNaturaConnectionString)
        conn.Open()

        Dim adaptador As SqlDataAdapter = New SqlDataAdapter()
        Dim sql As SqlCommand = New SqlCommand("select * from Eventos where Estado='Enviado'", conn)
        adaptador.SelectCommand = sql
        Dim tareas As DataSet = New DataSet
        adaptador.Fill(tareas, "Eventos")
        Dim dr As DataRow
        Dim fSql As String
        For Each dr In tareas.Tables("Eventos").Rows
            fSql = "UPDATE Eventos SET Estado=@Estado Where id=@Trabajo"
            sql = New SqlCommand(fSql, conn)
            sql.Parameters.AddWithValue("@Estado", "Iniciado")
            sql.Parameters.AddWithValue("@Trabajo", dr("ID").ToString())
            Dim r As Integer = sql.ExecuteNonQuery()
            Dim aParams() As String = Split(dr("Parametros").ToString.Trim(), ",")
            Select Case dr("Macro").ToString().Trim()
                Case "Cartera"
                    sql = New SqlCommand("SP_CarteraSubirArchivos", conn)
                    sql.CommandTimeout = 0
                    sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
                    sql.CommandType = CommandType.StoredProcedure
                    sql.ExecuteNonQuery()
                Case "Datacredito"
                    sql = New SqlCommand("SP_DataCreditoSubirArchivos", conn)
                    sql.CommandTimeout = 0
                    sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
                    sql.CommandType = CommandType.StoredProcedure
                    sql.ExecuteNonQuery()
                Case "DatacreditoHistorico"
                    sql = New SqlCommand("SP_DataCreditoSubirArchivos2", conn)
                    sql.CommandTimeout = 0
                    sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
                    sql.CommandType = CommandType.StoredProcedure
                    sql.ExecuteNonQuery()
                Case "Logistica1"
                    sql = New SqlCommand("SP_LogisticaCargueArchivo1", conn)
                    sql.CommandTimeout = 0
                    sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
                    sql.CommandType = CommandType.StoredProcedure
                    sql.ExecuteNonQuery()
                Case "Logistica2"
                    sql = New SqlCommand("SP_LogisticaCargueArchivo2", conn)
                    sql.CommandTimeout = 0
                    sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
                    sql.CommandType = CommandType.StoredProcedure
                    sql.ExecuteNonQuery()
                Case "Logistica2"
                    sql = New SqlCommand("SP_LogisticaCargueArchivo2", conn)
                    sql.CommandTimeout = 0
                    sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
                    sql.CommandType = CommandType.StoredProcedure
                    sql.ExecuteNonQuery()
                Case "Logistica2"
                    sql = New SqlCommand("SP_LogisticaCargueArchivo2", conn)
                    sql.CommandTimeout = 0
                    sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
                    sql.CommandType = CommandType.StoredProcedure
                    sql.ExecuteNonQuery()
                Case "Logistica3"
                    sql = New SqlCommand("SP_LogisticaCargueArchivo3", conn)
                    sql.CommandTimeout = 0
                    sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
                    sql.CommandType = CommandType.StoredProcedure
                    sql.ExecuteNonQuery()

            End Select
            sql = New SqlCommand(fSql, conn)
            sql.Parameters.AddWithValue("@Estado", "Terminado")
            sql.Parameters.AddWithValue("@Trabajo", dr("ID").ToString())
            r = sql.ExecuteNonQuery()
        Next
    End Sub

End Module
