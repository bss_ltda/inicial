﻿Imports System.IO
Imports System.Data.SqlClient
Module InfoNatura
    Public fSql As String
    Sub Main()

        Dim conn As New SqlConnection(My.Settings.InfoNaturaConnectionString)
        conn.Open()


   
        Dim adaptador As SqlDataAdapter = New SqlDataAdapter()
        Dim sql As SqlCommand = New SqlCommand("SELECT * FROM EVENTOS WHERE ESTADO <> 'Terminado' AND MACRO = 'Cifin' ", conn)
        adaptador.SelectCommand = sql
        Dim tareas As DataSet = New DataSet
        adaptador.Fill(tareas, "Eventos")
        Dim dr As DataRow
        For Each dr In tareas.Tables("Eventos").Rows
            fSql = "UPDATE EVENTOS SET ESTADO=@Estado WHERE id=@Trabajo"
            sql = New SqlCommand(fSql, conn)
            sql.Parameters.AddWithValue("@Estado", "Iniciado")
            sql.Parameters.AddWithValue("@Trabajo", dr("ID").ToString())
            Dim r As Integer = sql.ExecuteNonQuery()
            Dim aParams() As String = Split(dr("Parametros").ToString.Trim(), ",")
            Select Case dr("Macro").ToString().Trim()
                Case "Cifin"
                    sql = New SqlCommand("SP_SubirArchivosCifin", conn)
                    sql.CommandTimeout = 0
                    sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
                    sql.CommandType = CommandType.StoredProcedure
                    sql.ExecuteNonQuery()
            End Select
            sql = New SqlCommand(fSql, conn)
            sql.Parameters.AddWithValue("@Estado", "Terminado")
            sql.Parameters.AddWithValue("@Trabajo", dr("ID").ToString())
            r = sql.ExecuteNonQuery()
        Next

    End Sub

End Module
