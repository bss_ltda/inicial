﻿Imports System.IO
Imports System.Data.SqlClient
Imports System.Text

Module InfoNatura
    Public fSql As String
    Sub Main()

        revisaArchivos()


        Dim conn = New SqlConnection(My.Settings.InfoNaturaConnectionString)
        conn.Open()

        Dim adaptador As SqlDataAdapter = New SqlDataAdapter()
        Dim sql As SqlCommand = New SqlCommand("SELECT * FROM EVENTOS WHERE ESTADO <> 'Terminado' AND MACRO = 'C42Dias' ", conn)
        adaptador.SelectCommand = sql
        Dim tareas As DataSet = New DataSet
        adaptador.Fill(tareas, "Eventos")
        Dim dr As DataRow
        For Each dr In tareas.Tables("Eventos").Rows
            fSql = "UPDATE EVENTOS SET ESTADO=@Estado WHERE id=@Trabajo"
            sql = New SqlCommand(fSql, conn)
            sql.Parameters.AddWithValue("@Estado", "Iniciado")
            sql.Parameters.AddWithValue("@Trabajo", dr("ID").ToString())
            Dim r As Integer = sql.ExecuteNonQuery()
            Dim aParams() As String = Split(dr("Parametros").ToString.Trim(), ",")
            Select Case dr("Macro").ToString().Trim()
                Case "C42Dias"
                    sql = New SqlCommand("SP_SubirArchivos42Dias", conn)
                    sql.CommandTimeout = 0
                    sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
                    sql.CommandType = CommandType.StoredProcedure
                    sql.ExecuteNonQuery()
            End Select
            sql = New SqlCommand(fSql, conn)
            sql.Parameters.AddWithValue("@Estado", "Terminado")
            sql.Parameters.AddWithValue("@Trabajo", dr("ID").ToString())
            r = sql.ExecuteNonQuery()
        Next

    End Sub

    Sub revisaArchivos()
        Dim DB As New ADODB.Connection
        Dim rs As New ADODB.Recordset
        Dim f As ADODB.Field
        Dim i As Integer = 0
        DB.Open("Provider=SQLOLEDB.1.DataSource;Data Source=DELL-PC\BSSLTDA;User ID=Info;Password=InfoNatura;Persist Security Info=True;Initial Catalog=InfoNatura;CharacterSet=UTF-8")


        Dim objReader As New StreamReader("C:\AppData\Natura\Cartera\PrimerPedido.csv", Encoding.UTF7)
        Dim sLine As String = ""
        Dim aCampos() As String


        sLine = objReader.ReadLine()
        aCampos = Split(sLine, ";")
        objReader.Close()

        fSql = "SELECT TOP 1 * FROM C42PrimerPedido"
        rs.Open(fSql, DB)
        For Each f In rs.Fields
            Debug.Print(i & " " & f.Name & " " & tipoDatoCampo(f.Type) & " " & aCampos(i) & " " & IIf(sinTildes(f.Name.Trim()) = sinTildes(aCampos(i).Trim()), "OK", "MAL"))
            i += 1
        Next
        rs.Close()
        DB.Close()

    End Sub

    Function tipoDatoCampo(tipo As Integer) As String

        Select Case tipo
            'numerico
            Case 2, 3, 4, 5, 6, 14, 16, 17, 18, 19, 20, 21, 131, 139
                Return "numerico"
            Case 7, 133, 135
                Return "fecha"
            Case 129, 130, 200, 201, 202, 203
                Return "alfa"
            Case Else
                Return "sin definir"
        End Select

    End Function

    Sub CargaArchivo()
        Dim objReader As New StreamReader("C:\AppData\Natura\Cartera\PrimerPedido.csv")
        Dim sLine As String = ""
        Dim arrText As New ArrayList()

        objReader.ReadLine()
        objReader.Close()

        Console.ReadLine()
    End Sub

    Function sinTildes(ByRef dato As String) As String
        dato.Replace("á", "a")
        dato.Replace("é", "e")
        dato.Replace("í", "i")
        dato.Replace("ó", "o")
        dato.Replace("ú", "u")
        dato.Replace("ñ", "n")

        dato.Replace("Á", "A")
        dato.Replace("É", "E")
        dato.Replace("Í", "I")
        dato.Replace("Ó", "O")
        dato.Replace("Ú", "U")
        dato.Replace("Ñ", "N")
        Return dato
    End Function

End Module
