﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports System.Threading

Module Module1
    Dim DB As New ADODB.Connection

    Sub Main()
        Dim oExcel As Excel.Application
        Dim oBook As Excel.Workbook
        Dim oBooks As Excel.Workbooks
        Dim bPruebas As Boolean = true
        Dim id As String = Command()
        Dim r As Integer = 0
        Dim fSql As String

        Try

            'FileCopy("C:\AppData\Natura\CDias\C42BaseIni.mdb", "C:\AppData\Natura\CDias\C42Base.mdb")

            DB.Open("Provider=SQLOLEDB.1.DataSource;Data Source=10.21.1.16\INFONATURA;User ID=Info;Password=InfoNatura;Persist Security Info=True;Initial Catalog=InfoNatura;")

            fsql = "UPDATE Eventos SET  "
            fSql = fSql & " Estado = 'Actualizando Excel', FechaEjecucion = getDate(), Sts2 = 1  "
            fSql = fSql & " WHERE id = " & id & " And Sts2 = 0 "
            DB.Execute(fSql, r)

            If r = 1 Then

                Actualizando(id, My.Settings.Archivo1)
                oExcel = CreateObject("Excel.Application")
                oExcel.Visible = bPruebas
                oBooks = oExcel.Workbooks
                oBook = oBooks.Open(My.Settings.Archivo1)
                oExcel.Run(My.Settings.Macro)
                Thread.Sleep(2000)
                oExcel.Run(My.Settings.Macro)
                Thread.Sleep(2000)
                oExcel.DisplayAlerts = False
                Thread.Sleep(2000)
                oBook.Save()
                oBook.Close(True)
                oExcel.Quit()

                Actualizando(id, My.Settings.Archivo2)
                oExcel = CreateObject("Excel.Application")
                oExcel.Visible = bPruebas
                oBooks = oExcel.Workbooks
                oBook = oBooks.Open(My.Settings.Archivo2)
                oExcel.Run(My.Settings.Macro)
                Thread.Sleep(2000)
                oExcel.Run(My.Settings.Macro)
                Thread.Sleep(2000)
                oExcel.DisplayAlerts = False
                Thread.Sleep(2000)
                oBook.Save()
                oBook.Close(True)
                oExcel.Quit()

                Actualizando(id, My.Settings.Archivo3)
                oExcel = CreateObject("Excel.Application")
                oExcel.Visible = bPruebas
                oBooks = oExcel.Workbooks
                oBook = oBooks.Open(My.Settings.Archivo3)
                oExcel.Run(My.Settings.Macro)
                Thread.Sleep(2000)
                oExcel.Run(My.Settings.Macro)
                Thread.Sleep(2000)
                oExcel.DisplayAlerts = False
                Thread.Sleep(2000)
                oBook.Save()
                oBook.Close(True)
                oExcel.Quit()

                Actualizando(id, My.Settings.Archivo4)
                oExcel = CreateObject("Excel.Application")
                oExcel.Visible = bPruebas
                oBooks = oExcel.Workbooks
                oBook = oBooks.Open(My.Settings.Archivo4)
                oExcel.Run(My.Settings.Macro)
                Thread.Sleep(2000)
                oExcel.Run(My.Settings.Macro)
                Thread.Sleep(2000)
                oExcel.DisplayAlerts = False
                Thread.Sleep(2000)
                oBook.Save()
                oBook.Close(True)
                oExcel.Quit()

                Actualizando(id, My.Settings.Archivo5)
                oExcel = CreateObject("Excel.Application")
                oExcel.Visible = bPruebas
                oBooks = oExcel.Workbooks
                oBook = oBooks.Open(My.Settings.Archivo5)
                oExcel.Run(My.Settings.Macro)
                Thread.Sleep(2000)
                oExcel.Run(My.Settings.Macro)
                Thread.Sleep(2000)
                oExcel.DisplayAlerts = False
                Thread.Sleep(2000)
                oBook.Save()
                oBook.Close(True)
                oExcel.Quit()

                Actualizando(id, My.Settings.Archivo6)
                oExcel = CreateObject("Excel.Application")
                oExcel.Visible = bPruebas
                oBooks = oExcel.Workbooks
                oBook = oBooks.Open(My.Settings.Archivo6)
                oExcel.Run(My.Settings.Macro)
                Thread.Sleep(2000)
                oExcel.Run(My.Settings.Macro)
                Thread.Sleep(2000)
                oExcel.DisplayAlerts = False
                Thread.Sleep(2000)
                oBook.Save()
                oBook.Close(True)
                oExcel.Quit()

                Actualizando(id, My.Settings.Archivo7)
                oExcel = CreateObject("Excel.Application")
                oExcel.Visible = bPruebas
                oBooks = oExcel.Workbooks
                oBook = oBooks.Open(My.Settings.Archivo7)
                oExcel.Run(My.Settings.Macro)
                Thread.Sleep(2000)
                oExcel.Run(My.Settings.Macro)
                Thread.Sleep(2000)
                oExcel.DisplayAlerts = False
                Thread.Sleep(2000)
                oBook.Save()
                oBook.Close(True)
                oExcel.Quit()

                Actualizando(id, My.Settings.Archivo8)
                oExcel = CreateObject("Excel.Application")
                oExcel.Visible = bPruebas
                oBooks = oExcel.Workbooks
                oBook = oBooks.Open(My.Settings.Archivo8)
                oExcel.Run(My.Settings.Macro)
                Thread.Sleep(2000)
                oExcel.Run(My.Settings.Macro)
                Thread.Sleep(2000)
                oExcel.DisplayAlerts = False
                Thread.Sleep(2000)
                oBook.Save()
                oBook.Close(True)
                oExcel.Quit()

                Actualizando(id, My.Settings.Archivo9)
                oExcel = CreateObject("Excel.Application")
                oExcel.Visible = bPruebas
                oBooks = oExcel.Workbooks
                oBook = oBooks.Open(My.Settings.Archivo9)
                oExcel.Run(My.Settings.Macro)
                Thread.Sleep(2000)
                oExcel.Run(My.Settings.Macro)
                Thread.Sleep(2000)
                oExcel.DisplayAlerts = False
                Thread.Sleep(2000)
                oBook.Save()
                oBook.Close(True)
                oExcel.Quit()

                fSql = " UPDATE Eventos SET  "
                fSql = fSql & " Sts3 = 1, Estado = 'Excel Actualizado', FechaEjecucion = getDate(), Mensajes = 'Terminado'  "
                fSql = fSql & " WHERE id = " & id
                DB.Execute(fSql)

            End If

            DB.Close()
        Catch ex As Exception
            Console.WriteLine(ex.Message)
            Try
                fSql = " UPDATE Eventos SET  "
                fSql = fSql & " Sts3 = 1, Estado = 'Error Actualizando', FechaEjecucion = getDate(), Mensajes = '" & ex.Message & "'  "
                fSql = fSql & " WHERE id = " & id
                DB.Execute(fSql)
            Catch ex2 As Exception

            End Try

        End Try


    End Sub

    Sub Actualizando(Id As String, Archivo As String)
        Dim fSql As String
        Dim sArch() As String = Split(Archivo, "\")

        Console.WriteLine(sArch(UBound(sArch)))

        fSql = " UPDATE Eventos SET  "
        fSql = fSql & " Mensajes = '" & sArch(UBound(sArch)) & "'  "
        fSql = fSql & " WHERE id = " & Id
        DB.Execute(fSql)

    End Sub

End Module
