﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.OpenFileDialog2 = New System.Windows.Forms.OpenFileDialog()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Inter = New System.Windows.Forms.TextBox()
        Me.Servientrega = New System.Windows.Forms.TextBox()
        Me.Envia = New System.Windows.Forms.TextBox()
        Me.Gera = New System.Windows.Forms.TextBox()
        Me.B_Inter = New System.Windows.Forms.Button()
        Me.B_Servientrega = New System.Windows.Forms.Button()
        Me.B_Envia = New System.Windows.Forms.Button()
        Me.B_Gera = New System.Windows.Forms.Button()
        Me.B_Validar = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.InterHoja = New System.Windows.Forms.TextBox()
        Me.ServientregaHoja = New System.Windows.Forms.TextBox()
        Me.EnviaHoja = New System.Windows.Forms.TextBox()
        Me.GeraHoja = New System.Windows.Forms.TextBox()
        Me.Generar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'FolderBrowserDialog1
        '
        Me.FolderBrowserDialog1.RootFolder = System.Environment.SpecialFolder.MyDocuments
        Me.FolderBrowserDialog1.SelectedPath = "C:\"
        Me.FolderBrowserDialog1.ShowNewFolderButton = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'OpenFileDialog2
        '
        Me.OpenFileDialog2.FileName = "OpenFileDialog2"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Andalus", 16.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(3, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(175, 34)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Validar Archivos :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Andalus", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(81, 119)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(126, 26)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "* InterRapidisimo"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Andalus", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(84, 165)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(103, 26)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "* Servientrega"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Andalus", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(84, 208)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(54, 26)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "*Envia"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Andalus", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(84, 258)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(105, 26)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "*Archivo Gera"
        '
        'Inter
        '
        Me.Inter.Location = New System.Drawing.Point(237, 119)
        Me.Inter.Name = "Inter"
        Me.Inter.Size = New System.Drawing.Size(224, 20)
        Me.Inter.TabIndex = 8
        '
        'Servientrega
        '
        Me.Servientrega.Location = New System.Drawing.Point(237, 165)
        Me.Servientrega.Name = "Servientrega"
        Me.Servientrega.Size = New System.Drawing.Size(224, 20)
        Me.Servientrega.TabIndex = 9
        '
        'Envia
        '
        Me.Envia.Location = New System.Drawing.Point(237, 214)
        Me.Envia.Name = "Envia"
        Me.Envia.Size = New System.Drawing.Size(224, 20)
        Me.Envia.TabIndex = 10
        '
        'Gera
        '
        Me.Gera.Location = New System.Drawing.Point(237, 264)
        Me.Gera.Name = "Gera"
        Me.Gera.Size = New System.Drawing.Size(224, 20)
        Me.Gera.TabIndex = 11
        '
        'B_Inter
        '
        Me.B_Inter.Location = New System.Drawing.Point(491, 122)
        Me.B_Inter.Name = "B_Inter"
        Me.B_Inter.Size = New System.Drawing.Size(123, 23)
        Me.B_Inter.TabIndex = 13
        Me.B_Inter.Text = "Subir Archivo"
        Me.B_Inter.UseVisualStyleBackColor = True
        '
        'B_Servientrega
        '
        Me.B_Servientrega.Location = New System.Drawing.Point(491, 168)
        Me.B_Servientrega.Name = "B_Servientrega"
        Me.B_Servientrega.Size = New System.Drawing.Size(123, 23)
        Me.B_Servientrega.TabIndex = 14
        Me.B_Servientrega.Text = "Subir Archivo"
        Me.B_Servientrega.UseVisualStyleBackColor = True
        '
        'B_Envia
        '
        Me.B_Envia.Location = New System.Drawing.Point(491, 215)
        Me.B_Envia.Name = "B_Envia"
        Me.B_Envia.Size = New System.Drawing.Size(123, 23)
        Me.B_Envia.TabIndex = 15
        Me.B_Envia.Text = "Subir Archivo"
        Me.B_Envia.UseVisualStyleBackColor = True
        '
        'B_Gera
        '
        Me.B_Gera.Location = New System.Drawing.Point(491, 262)
        Me.B_Gera.Name = "B_Gera"
        Me.B_Gera.Size = New System.Drawing.Size(123, 23)
        Me.B_Gera.TabIndex = 16
        Me.B_Gera.Text = "Subir Archivo"
        Me.B_Gera.UseVisualStyleBackColor = True
        '
        'B_Validar
        '
        Me.B_Validar.Location = New System.Drawing.Point(273, 338)
        Me.B_Validar.Name = "B_Validar"
        Me.B_Validar.Size = New System.Drawing.Size(126, 23)
        Me.B_Validar.TabIndex = 18
        Me.B_Validar.Text = "Validar Archivos"
        Me.B_Validar.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Andalus", 16.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(231, 72)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(91, 34)
        Me.Label7.TabIndex = 19
        Me.Label7.Text = "Archivo:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Andalus", 16.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(683, 72)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(61, 34)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "Hoja:"
        '
        'InterHoja
        '
        Me.InterHoja.Location = New System.Drawing.Point(689, 125)
        Me.InterHoja.Name = "InterHoja"
        Me.InterHoja.Size = New System.Drawing.Size(146, 20)
        Me.InterHoja.TabIndex = 21
        Me.InterHoja.Text = "Pedidos"
        Me.InterHoja.Visible = False
        '
        'ServientregaHoja
        '
        Me.ServientregaHoja.Location = New System.Drawing.Point(689, 170)
        Me.ServientregaHoja.Name = "ServientregaHoja"
        Me.ServientregaHoja.Size = New System.Drawing.Size(146, 20)
        Me.ServientregaHoja.TabIndex = 22
        Me.ServientregaHoja.Text = "BASE"
        Me.ServientregaHoja.Visible = False
        '
        'EnviaHoja
        '
        Me.EnviaHoja.Location = New System.Drawing.Point(689, 218)
        Me.EnviaHoja.Name = "EnviaHoja"
        Me.EnviaHoja.Size = New System.Drawing.Size(146, 20)
        Me.EnviaHoja.TabIndex = 23
        Me.EnviaHoja.Text = "Hoja1"
        Me.EnviaHoja.Visible = False
        '
        'GeraHoja
        '
        Me.GeraHoja.Location = New System.Drawing.Point(689, 265)
        Me.GeraHoja.Name = "GeraHoja"
        Me.GeraHoja.Size = New System.Drawing.Size(146, 20)
        Me.GeraHoja.TabIndex = 24
        Me.GeraHoja.Text = "Pag_1"
        Me.GeraHoja.Visible = False
        '
        'Generar
        '
        Me.Generar.Location = New System.Drawing.Point(505, 338)
        Me.Generar.Name = "Generar"
        Me.Generar.Size = New System.Drawing.Size(141, 23)
        Me.Generar.TabIndex = 25
        Me.Generar.Text = "Generar Informes"
        Me.Generar.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(942, 402)
        Me.Controls.Add(Me.Generar)
        Me.Controls.Add(Me.GeraHoja)
        Me.Controls.Add(Me.EnviaHoja)
        Me.Controls.Add(Me.ServientregaHoja)
        Me.Controls.Add(Me.InterHoja)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.B_Validar)
        Me.Controls.Add(Me.B_Gera)
        Me.Controls.Add(Me.B_Envia)
        Me.Controls.Add(Me.B_Servientrega)
        Me.Controls.Add(Me.B_Inter)
        Me.Controls.Add(Me.Gera)
        Me.Controls.Add(Me.Envia)
        Me.Controls.Add(Me.Servientrega)
        Me.Controls.Add(Me.Inter)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents OpenFileDialog2 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Inter As System.Windows.Forms.TextBox
    Friend WithEvents Servientrega As System.Windows.Forms.TextBox
    Friend WithEvents Envia As System.Windows.Forms.TextBox
    Friend WithEvents Gera As System.Windows.Forms.TextBox
    Friend WithEvents B_Inter As System.Windows.Forms.Button
    Friend WithEvents B_Servientrega As System.Windows.Forms.Button
    Friend WithEvents B_Envia As System.Windows.Forms.Button
    Friend WithEvents B_Gera As System.Windows.Forms.Button
    Friend WithEvents B_Validar As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents InterHoja As System.Windows.Forms.TextBox
    Friend WithEvents ServientregaHoja As System.Windows.Forms.TextBox
    Friend WithEvents EnviaHoja As System.Windows.Forms.TextBox
    Friend WithEvents GeraHoja As System.Windows.Forms.TextBox
    Friend WithEvents Generar As System.Windows.Forms.Button

End Class
