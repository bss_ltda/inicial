﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports System.Data.OleDb
Imports System.Text.RegularExpressions
Imports Microsoft.Office.Interop.Excel

Public Class Form1
    Dim Tenvia As New DataSet
    Dim Tinter As New DataSet
    Dim Tservi As New DataSet
    Dim TGera As New DataSet
    Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
    Dim MyConnection As System.Data.OleDb.OleDbConnection

    Sub ImportadelExcel(ByVal sFichero As String, ByVal DS As String, ByVal sTablaDestino As String)

        Dim sTablaOrigen As String
        Dim sConnect As String, sSQL As String
        Dim cnnActiva As ADODB.Connection

        cnnActiva = New ADODB.Connection
        cnnActiva.Open("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & DS & ";User Id=admin;Password=;")
        sTablaOrigen = "[Cobro$]"
        sConnect = "‘" & sFichero & "‘ ‘Excel 8.0;HDR=Yes;’"
        'sSQL = "SELECT * INTO " & sTablaDestino & " FROM " & sTablaOrigen & " IN " & sConnect
        sSQL = "INSERT INTO " & sTablaDestino & "  SELECT * FROM " & sTablaOrigen & " IN " & sConnect
        cnnActiva.Execute(sSQL)
        cnnActiva.Close()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim folderDlg As New OpenFileDialog
        'folderDlg.s = True
        If (folderDlg.ShowDialog() = DialogResult.OK) Then
            Inter.Text = folderDlg.FileName
            'Dim root As Environment.SpecialFolder = folderDlg.
        End If

    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub LeerXls()

        'ImportadelExcel(Envia.Text.ToString, "C:\Users\BssLtda\Desktop\ArchivosMacrosNatura\42 Dias\42Dias.mdb", "Cartera")


        Dim conexion As New System.Data.OleDb.OleDbConnection
        Dim comando As New OleDbCommand
        Dim adaptador As New OleDbDataAdapter


        '/////////////////////////////////////////////////////// INTERRAPIDISIMO /////////////////////////////////////////////
        Dim hoja As String = InterHoja.Text.Trim

        Dim num As Integer
        Dim extension As String = ""

        Dim ExcelPath As String = Inter.Text.ToLower()

        num = Trim(ExcelPath).Length
        extension = Trim(ExcelPath).Substring(num - 4, 4)
        If extension = ".xls" Then
            conexion.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & ExcelPath & "; Extended Properties= ""Excel 8.0;HDR=YES; IMEX=1"""
        Else
            conexion.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & ExcelPath & ";Extended Properties=" & Chr(34) & "Excel 12.0 Xml;HDR=YES;IMEX=1" & Chr(34)
        End If

        conexion.Open()
        comando.CommandText = "SELECT * FROM [" & hoja & "$]"
        comando.Connection = conexion
        adaptador.SelectCommand = comando
        conexion.Close()
        adaptador.Fill(Tinter, "excel")

        ' Quitafilasvacias(dsexcel)
        ' DataGridView1.DataSource = dsexcel.Tables(0)

        SubirInter(My.Settings.BD)


        '///////////////////////////////////////////////// ENVIA  //////////////////////////////////////////////////////////////
        hoja = EnviaHoja.Text.Trim
        extension = ""
        ExcelPath = Envia.Text.ToLower()

        num = Trim(ExcelPath).Length
        extension = Trim(ExcelPath).Substring(num - 4, 4)
        If extension = ".xls" Then
            conexion.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & ExcelPath & "; Extended Properties= ""Excel 8.0;HDR=YES; IMEX=1"""
        Else
            conexion.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & ExcelPath & ";Extended Properties=" & Chr(34) & "Excel 12.0 Xml;HDR=YES;IMEX=1" & Chr(34)
        End If

        conexion.Open()
        comando.CommandText = "SELECT * FROM [" & hoja & "$]"
        comando.Connection = conexion
        adaptador.SelectCommand = comando
        conexion.Close()
        adaptador.Fill(Tenvia, "excel")

        'quitafilasvacias(dsexcel)

        'DataGridView1.DataSource = dsexcel.Tables(0)

        SubirEnvia(My.Settings.BD)


        '///////////////////////////////////////////////// SERVIENTREGA  //////////////////////////////////////////////////////////////
        hoja = ServientregaHoja.Text.Trim
        extension = ""
        ExcelPath = Servientrega.Text.ToLower()

        num = Trim(ExcelPath).Length
        extension = Trim(ExcelPath).Substring(num - 4, 4)
        If extension = ".xls" Then
            conexion.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & ExcelPath & "; Extended Properties= ""Excel 8.0;HDR=YES; IMEX=1"""
        Else
            conexion.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & ExcelPath & ";Extended Properties=" & Chr(34) & "Excel 12.0 Xml;HDR=YES;IMEX=1" & Chr(34)
        End If

        conexion.Open()
        comando.CommandText = "SELECT * FROM [" & hoja & "$]"
        comando.Connection = conexion
        adaptador.SelectCommand = comando
        conexion.Close()
        adaptador.Fill(Tservi, "excel")

        'quitafilasvacias(dsexcel)

        'DataGridView1.DataSource = dsexcel.Tables(0)

        SubirServientrega(My.Settings.BD)


        '///////////////////////////////////////////////// GERA  //////////////////////////////////////////////////////////////
        hoja = GeraHoja.Text.Trim
        extension = ""
        ExcelPath = Gera.Text.ToLower()

        num = Trim(ExcelPath).Length
        extension = Trim(ExcelPath).Substring(num - 4, 4)
        If extension = ".xls" Then
            conexion.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & ExcelPath & "; Extended Properties= ""Excel 8.0;HDR=YES; IMEX=1"""
        Else
            conexion.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & ExcelPath & ";Extended Properties=" & Chr(34) & "Excel 12.0 Xml;HDR=YES;IMEX=1" & Chr(34)
        End If

        conexion.Open()
        comando.CommandText = "SELECT * FROM [" & hoja & "$]"
        comando.Connection = conexion
        adaptador.SelectCommand = comando
        conexion.Close()
        adaptador.Fill(TGera, "excel")

        'quitafilasvacias(dsexcel)

        'DataGridView1.DataSource = dsexcel.Tables(0)

        SubirGera(My.Settings.BD)
        Dim A As Integer
        A = 0
    End Sub

    Private Sub subirxls(ByVal NombreDe_MiTabla As String)

        Dim cn As System.Data.OleDb.OleDbConnection
        'Construimos y abrimos la conexión con la base de datos
        cn = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & NombreDe_MiTabla & ";User Id=admin;Password=;")
        cn.Open()

        Dim fSql As String
        Dim vsql As String

        Try
            Dim com As OleDbCommand
            com = New OleDbCommand("DELETE FROM BASE", cn)
            com.ExecuteNonQuery()


            Dim cmAlta As OleDbCommand
            fSql = " INSERT INTO BASE( "
            vsql = " SELECT "
            fSql = fSql & " Transportadora       , " : vsql = vsql & " Transportadora       , "     '//8P0   ID
            fSql = fSql & " CodigoPedido    , " : vsql = vsql & " CodigoPedido    , "     '//4P0   Hijo
            fSql = fSql & " Persona      , " : vsql = vsql & " Persona      , "     '//2P0   Estado
            fSql = fSql & " NombrePersona     , " : vsql = vsql & " NombrePersona     , "     '//2P0   Tipo
            fSql = fSql & " Fechas     , " : vsql = vsql & " Fecha     , "     '//2P0   Causal Cierre
            fSql = fSql & " PrevisiónEntrega   , " : vsql = vsql & " PrevisiónEntrega   , "     '//3A    Subcausal
            fSql = fSql & " CicloCaptación    , " : vsql = vsql & " CicloCaptación    , "     '//6P0   Planilla
            fSql = fSql & " DirecciónEntrega    , " : vsql = vsql & " VIAPRINCIPALEntrega     , "     '//30A   Gerencia
            fSql = fSql & " MUNICIPIOEntrega    , " : vsql = vsql & " MUNICIPIOEntrega    , "     '//3A    Plta
            fSql = fSql & " DEPARTAMIENTOEntrega    , " : vsql = vsql & " DEPARTAMIENTOEntrega    , "     '//30A   Planta
            fSql = fSql & " EstructuraPadre    , " : vsql = vsql & " EstructuraPadre    , "     '//6A    Consolid
            fSql = fSql & " Estructura      ) " : vsql = vsql & " Estructura       "     '//3A    Bodega
            vsql = vsql & " FROM Gera"
            cmAlta = New OleDbCommand(fSql & vsql, cn)
            cmAlta.ExecuteNonQuery()

            'update PremiosPlanilla set Onda = ONDAS FROM PremiosPlanilla as R INNER JOIN PremiosOndas AS P ON p.[Codigo Sector]=r.[Cód Estructura];
            fSql = "UPDATE DISTINCTROW envia t1 set Numero_Guia = replace(Numero_Guia, ""'"", '')"
            com = New OleDbCommand(fSql, cn)
            com.ExecuteNonQuery()



            fSql = "UPDATE DISTINCTROW base t1 INNER JOIN envia t2 ON t1.CodigoPedido=t2.Numero_Pedido SET t1.Guía=T2.Numero_Guia where Transportadora = 'ENVÍA'"
            com = New OleDbCommand(fSql, cn)
            com.ExecuteNonQuery()


            fSql = "UPDATE DISTINCTROW base t1 INNER JOIN envia t2 ON t1.CodigoPedido=t2.Numero_Pedido SET t1.Estado=T2.Estado_Actual where Transportadora = 'ENVÍA'"
            com = New OleDbCommand(fSql, cn)
            com.ExecuteNonQuery()

            fSql = "UPDATE DISTINCTROW base t1 INNER JOIN envia t2 ON t1.CodigoPedido=t2.Numero_Pedido SET t1.Fecha=T2.Fecha_Novedad where Transportadora = 'ENVÍA'"
            com = New OleDbCommand(fSql, cn)
            com.ExecuteNonQuery()

            fSql = "UPDATE DISTINCTROW base t1 INNER JOIN envia t2 ON t1.CodigoPedido=t2.Numero_Pedido SET t1.Novedad=T2.Descripcion_novedad where Transportadora = 'ENVÍA'"
            com = New OleDbCommand(fSql, cn)
            com.ExecuteNonQuery()



            fSql = "UPDATE DISTINCTROW base t1 INNER JOIN InterRapidisimo t2 ON t1.CodigoPedido=t2.Contenido SET t1.Guía=T2.Guia where Transportadora = 'INTER RAPIDISIMO S.A'"
            com = New OleDbCommand(fSql, cn)
            com.ExecuteNonQuery()
            fSql = "UPDATE DISTINCTROW base t1 INNER JOIN InterRapidisimo t2 ON t1.CodigoPedido=t2.Contenido SET t1.Estado=T2.Ultimo_Estado where Transportadora = 'INTER RAPIDISIMO S.A'"
            com = New OleDbCommand(fSql, cn)
            com.ExecuteNonQuery()
            fSql = "UPDATE DISTINCTROW base t1 INNER JOIN InterRapidisimo t2 ON t1.CodigoPedido=t2.Contenido SET t1.Fecha=T2.Fecha_de_entrega where Transportadora = 'INTER RAPIDISIMO S.A'"
            com = New OleDbCommand(fSql, cn)
            com.ExecuteNonQuery()
            fSql = "UPDATE DISTINCTROW base t1 INNER JOIN InterRapidisimo t2 ON t1.CodigoPedido=t2.Contenido SET t1.Novedad=T2.Estado_Gestion where Transportadora = 'INTER RAPIDISIMO S.A'"
            com = New OleDbCommand(fSql, cn)
            com.ExecuteNonQuery()


            fSql = "UPDATE DISTINCTROW base t1 INNER JOIN Servientrega t2 ON t1.CodigoPedido=t2.PEDIDO SET t1.Guía=T2.GUIA  "
            com = New OleDbCommand(fSql, cn)
            com.ExecuteNonQuery()
            fSql = "UPDATE DISTINCTROW base t1 INNER JOIN Servientrega t2 ON t1.CodigoPedido=t2.PEDIDO SET t1.Estado=T2.ESTADO_ENVIO "
            com = New OleDbCommand(fSql, cn)
            com.ExecuteNonQuery()
            fSql = "UPDATE DISTINCTROW base t1 INNER JOIN Servientrega t2 ON t1.CodigoPedido=t2.PEDIDO SET t1.Fecha=T2.FECHA_DE_ENTREGA "
            com = New OleDbCommand(fSql, cn)
            com.ExecuteNonQuery()
            fSql = "UPDATE DISTINCTROW base t1 INNER JOIN Servientrega t2 ON t1.CodigoPedido=t2.PEDIDO SET t1.Novedad=T2.NOVEDAD_1"

            'fSql = "UPDATE BASE SET Novedad = NOVEDAD_1 from BASE as R INNER JOIN Servientrega AS P ON P.PEDIDO = R.CodigoPedido"
            com = New OleDbCommand(fSql, cn)
            com.ExecuteNonQuery()


            fSql = "UPDATE base SET base.DíasDeRetraso = DateDiff('d',PrevisiónEntrega,Now());"
            com = New OleDbCommand(fSql, cn)
            com.ExecuteNonQuery()

        Catch
            cn.Close()
        End Try


        cn.Close()
    End Sub
    Private Sub SubirInter(ByVal NombreDe_MiTabla As String)


        Dim cmAlta As OleDbCommand
        Dim com As OleDbCommand

        Dim i As Integer
        Dim cn As System.Data.OleDb.OleDbConnection
        'Construimos y abrimos la conexión con la base de datos
        cn = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & NombreDe_MiTabla & ";User Id=admin;Password=;")
        cn.Open()

        Try
            com = New OleDbCommand("DELETE FROM InterRapidisimo", cn)
            com.ExecuteNonQuery()
            'Preparamos el comando
            ') VALUES(@nombre,@apellidos)"
            Dim fSql As String
            fSql = "INSERT INTO InterRapidisimo ( "                          '1
            fSql = fSql & " Guia,"                          '2
            fSql = fSql & " Fecha,"                       '3
            fSql = fSql & " Destino,"                               '4
            fSql = fSql & " ID_Destinatario,"                               '5
            fSql = fSql & " Destinatario,"                         '6
            fSql = fSql & " Telefono,"                           '7
            fSql = fSql & " Direccion," ' orden del Pedido      8
            fSql = fSql & " Contenido,"                                '9
            fSql = fSql & " Tiempo_de_entrega,"                         '10
            fSql = fSql & " Ultimo_Estado,"                            '11
            fSql = fSql & " Estado_Gestion,"                            '12
            fSql = fSql & " Fecha_de_entrega,"                       '13
            fSql = fSql & " Días_no_hábiles,"                          '14
            fSql = fSql & " Tiempo_de_entrega1,"               '15
            fSql = fSql & " Tiempo_real_de_entrega,"                        '16
            fSql = fSql & " Cumple)"                    '17
            ' fSql = fSql & " Estado1,"             '18
            'fSql = fSql & " Estado2)"                     '69

            fSql = fSql & " VALUES("                                '1

            fSql = fSql & " @Guia,"                          '2
            fSql = fSql & " @Fecha,"                       '3
            fSql = fSql & " @Destino,"                               '4
            fSql = fSql & " @ID_Destinatario,"                               '5
            fSql = fSql & " @Destinatario,"                         '6
            fSql = fSql & " @Telefono,"                           '7
            fSql = fSql & " @Direccion," ' orden del Pedido      8
            fSql = fSql & " @Contenido,"                                '9
            fSql = fSql & " @Tiempo_de_entrega,"                         '10
            fSql = fSql & " @Ultimo_Estado,"                            '11
            fSql = fSql & " @Estado_Gestion,"                            '12
            fSql = fSql & " @Fecha_de_entrega,"                       '13
            fSql = fSql & " @Días_no_hábiles,"                          '14
            fSql = fSql & " @Tiempo_de_entrega1,"               '15
            fSql = fSql & " @Tiempo_real_de_entrega,"                        '16
            fSql = fSql & " @Cumple)"                    '17
            '            fSql = fSql & " @Estado1,"             '18
            '           fSql = fSql & " @Estado2)"
            cmAlta = New OleDbCommand(fSql)


            cmAlta.Parameters.Add("@Guia", OleDbType.VarChar)                               '2
            cmAlta.Parameters.Add("@Fecha", OleDbType.VarChar)                            '3
            cmAlta.Parameters.Add("@Destino", OleDbType.VarChar)                                    '4
            cmAlta.Parameters.Add("@ID_Destinatario", OleDbType.VarChar)                                    '5
            cmAlta.Parameters.Add("@Destinatario", OleDbType.VarChar)                              '6
            cmAlta.Parameters.Add("@Telefono", OleDbType.VarChar)                                '7
            cmAlta.Parameters.Add("@Direccion", OleDbType.VarChar)      ' orden del Pedido      8
            cmAlta.Parameters.Add("@Contenido", OleDbType.VarChar)                                     '9
            cmAlta.Parameters.Add("@Tiempo_de_entrega", OleDbType.VarChar)                              '10
            cmAlta.Parameters.Add("@Ultimo_Estado", OleDbType.VarChar)                                 '11
            cmAlta.Parameters.Add("@Estado_Gestion", OleDbType.VarChar)                                 '12
            cmAlta.Parameters.Add("@Fecha_de_entrega", OleDbType.VarChar)                            '13
            cmAlta.Parameters.Add("@Días_no_hábiles", OleDbType.VarChar)                               '14
            cmAlta.Parameters.Add("@Tiempo_de_entrega1", OleDbType.VarChar)                    '15
            cmAlta.Parameters.Add("@Tiempo_real_de_entrega", OleDbType.VarChar)                             '16
            cmAlta.Parameters.Add("@Cumple", OleDbType.VarChar)                         '17
            '            cmAlta.Parameters.Add("@Estado1", OleDbType.VarChar)                  '18
            '           cmAlta.Parameters.Add("@Estado2", OleDbType.VarChar)
            cmAlta.Connection = cn

            For i = 0 To Tinter.Tables(0).Rows.Count - 1
                cmAlta.Parameters("@Guia").Value = Tinter.Tables(0).Rows(i)(0).ToString()                             '2
                cmAlta.Parameters("@Fecha").Value = Tinter.Tables(0).Rows(i)(1).ToString()                          '3
                cmAlta.Parameters("@Destino").Value = Tinter.Tables(0).Rows(i)(2).ToString()                                  '4
                cmAlta.Parameters("@ID_Destinatario").Value = Tinter.Tables(0).Rows(i)(3).ToString()                                  '5
                cmAlta.Parameters("@Destinatario").Value = Tinter.Tables(0).Rows(i)(4).ToString()                            '6
                cmAlta.Parameters("@Telefono").Value = Tinter.Tables(0).Rows(i)(5).ToString()                              '7
                cmAlta.Parameters("@Direccion").Value = Tinter.Tables(0).Rows(i)(6).ToString()    ' orden del Pedido      8
                cmAlta.Parameters("@Contenido").Value = Tinter.Tables(0).Rows(i)(7).ToString()                                   '9
                cmAlta.Parameters("@Tiempo_de_entrega").Value = Tinter.Tables(0).Rows(i)(8).ToString()                            '10
                cmAlta.Parameters("@Ultimo_Estado").Value = Tinter.Tables(0).Rows(i)(9).ToString()                               '11
                cmAlta.Parameters("@Estado_Gestion").Value = Tinter.Tables(0).Rows(i)(10).ToString()                               '12
                cmAlta.Parameters("@Fecha_de_entrega").Value = Tinter.Tables(0).Rows(i)(11).ToString()                          '13
                cmAlta.Parameters("@Días_no_hábiles").Value = Tinter.Tables(0).Rows(i)(12).ToString()                             '14
                cmAlta.Parameters("@Tiempo_de_entrega1").Value = Tinter.Tables(0).Rows(i)(13).ToString()                  '15
                cmAlta.Parameters("@Tiempo_real_de_entrega").Value = Tinter.Tables(0).Rows(i)(14).ToString()                           '16
                cmAlta.Parameters("@Cumple").Value = Tinter.Tables(0).Rows(i)(15).ToString()                       '17
                '             cmAlta.Parameters("@Estado1").Value = Tinter.Tables(0).Rows(i)(16).ToString()                '18
                '              cmAlta.Parameters("@Estado2").Value = Tinter.Tables(0).Rows(i)(17).ToString()
                'realizamos el alta
                cmAlta.ExecuteNonQuery()
            Next

            'Cerramos la conexion con la base de datos
            cn.Close()
        Catch
            cn.Close()
            MsgBox(Err.Description)
        End Try

    End Sub


    Private Sub SubirGera(ByVal NombreDe_MiTabla As String)

        Dim cmAlta As OleDbCommand
        Dim com As OleDbCommand
        Dim i As Integer
        Dim cn As System.Data.OleDb.OleDbConnection

        'Construimos y abrimos la conexión con la base de datos
        cn = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & NombreDe_MiTabla & ";User Id=admin;Password=;")
        cn.Open()
        Try
            com = New OleDbCommand("DELETE FROM Gera", cn)
            com.ExecuteNonQuery()
            'Preparamos el comando
            ') VALUES(@nombre,@apellidos)"
            Dim fSql As String
            fSql = "INSERT INTO Gera ( "                               '1
            fSql = fSql & " CodigoPedido,"                             '2
            fSql = fSql & " SituaciónFiscal,"                       '3
            fSql = fSql & " Factura,"                               '4
            fSql = fSql & " Persona,"                               '5
            fSql = fSql & " NombrePersona,"                         '6
            fSql = fSql & " CodigoGrupo,"                           '7
            fSql = fSql & " OrdenDelPedido," ' orden del Pedido      8
            fSql = fSql & " Puntos,"                                '9
            fSql = fSql & " CantidadÍtems,"                         '10
            fSql = fSql & " ValorTotal,"                            '11
            fSql = fSql & " ValorTabla,"                            '12
            fSql = fSql & " ValorPracticado,"                       '13
            fSql = fSql & " ValorLíquido,"                          '14
            fSql = fSql & " ValorProductosRegulares,"               '15
            fSql = fSql & " MedioCaptación,"                        '16
            fSql = fSql & " SituaciónComercial,"                    '17
            fSql = fSql & " DetalleSituaciónComercial,"             '18
            fSql = fSql & " SituacionIntegracionExterna,"           '19
            fSql = fSql & " Fecha,"                                 '20
            fSql = fSql & " FechaMarketing,"                        '21
            fSql = fSql & " PrevisiónEntrega,"                      '22
            fSql = fSql & " FechaEntrega,"                          '23
            fSql = fSql & " FechaAutorizaciónFacturación,"          '24
            fSql = fSql & " CicloCaptación,"                          '23
            fSql = fSql & " SubCiclo,"                          '23
            fSql = fSql & " CicloMarketing,"                          '23
            fSql = fSql & " CicloIndicador,"                          '23
            fSql = fSql & " CicloAnulación,"                          '23
            fSql = fSql & " CaptacionRestrita,"                          '23
            fSql = fSql & " DíaDelCiclo,"                          '23
            fSql = fSql & " PlanPago,"                          '23
            fSql = fSql & " VIAPRINCIPAL,"                          '23
            fSql = fSql & " Complemento,"                          '23
            fSql = fSql & " BARRIO,"                          '23
            fSql = fSql & " MUNICIPIO,"                          '23
            fSql = fSql & " DEPARTAMIENTO,"                          '23
            fSql = fSql & " CódigoPostal,"                          '23
            fSql = fSql & " Referencia,"                          '23
            fSql = fSql & " VIAPRINCIPALEntrega,"                          '23
            fSql = fSql & " ComplementoEntrega,"                          '23
            fSql = fSql & " BARRIOEntrega,"                          '23
            fSql = fSql & " MUNICIPIOEntrega,"                          '23
            fSql = fSql & " DEPARTAMIENTOEntrega,"                          '23
            fSql = fSql & " CódigoPostalEntrega,"                          '23
            fSql = fSql & " ReferenciaEntrega,"                          '23
            fSql = fSql & " Teléfono,"                          '23
            fSql = fSql & " CódModeloComercial,"                          '23
            fSql = fSql & " ModeloComercial,"                          '23
            fSql = fSql & " CódEstructuraPadre,"                          '23
            fSql = fSql & " EstructuraPadre,"                          '23
            fSql = fSql & " CódEstructura,"                          '23
            fSql = fSql & " Estructura,"                          '23
            fSql = fSql & " ResponsableEstructura,"                          '23
            fSql = fSql & " TeléfonoResponsable,"                          '23
            fSql = fSql & " CódUsuarioCreación,"                          '23
            fSql = fSql & " UsuarioDeCreación,"                          '23
            fSql = fSql & " CódUsuarioDeFinalización,"                          '23
            fSql = fSql & " UsuarioDeFinalización,"                          '23
            fSql = fSql & " CodTransportadora,"                          '23
            fSql = fSql & " Transportadora,"                          '23
            fSql = fSql & " CódigoAgencia,"                          '23
            fSql = fSql & " Agencia,"                          '23
            fSql = fSql & " Volumen,"                          '23
            fSql = fSql & " PesoEstimado,"                          '23
            fSql = fSql & " PesoReal,"                          '23
            fSql = fSql & " LoteSeparación,"                          '23
            fSql = fSql & " Minuta,"                          '23
            fSql = fSql & " PedidoConsolidado)"                            '39


            fSql = fSql & " VALUES("                                '1

            fSql = fSql & " @CodigoPedido,"                             '2
            fSql = fSql & " @SituaciónFiscal,"                       '3
            fSql = fSql & " @Factura,"                               '4
            fSql = fSql & " @Persona,"                               '5
            fSql = fSql & " @NombrePersona,"                         '6
            fSql = fSql & " @CodigoGrupo,"                           '7
            fSql = fSql & " @OrdenDelPedido," ' orden del Pedido      8
            fSql = fSql & " @Puntos,"                                '9
            fSql = fSql & " @CantidadÍtems,"                         '10
            fSql = fSql & " @ValorTotal,"                            '11
            fSql = fSql & " @ValorTabla,"                            '12
            fSql = fSql & " @ValorPracticado,"                       '13
            fSql = fSql & " @ValorLíquido,"                          '14
            fSql = fSql & " @ValorProductosRegulares,"               '15
            fSql = fSql & " @MedioCaptación,"                        '16
            fSql = fSql & " @SituaciónComercial,"                    '17
            fSql = fSql & " @DetalleSituaciónComercial,"             '18
            fSql = fSql & " @SituacionIntegracionExterna,"           '19
            fSql = fSql & " @Fecha,"                                 '20
            fSql = fSql & " @FechaMarketing,"                        '21
            fSql = fSql & " @PrevisiónEntrega,"                      '22
            fSql = fSql & " @FechaEntrega,"                          '23
            fSql = fSql & " @FechaAutorizaciónFacturación,"          '24
            fSql = fSql & " @CicloCaptación,"                          '23
            fSql = fSql & " @SubCiclo,"                          '23
            fSql = fSql & " @CicloMarketing,"                          '23
            fSql = fSql & " @CicloIndicador,"                          '23
            fSql = fSql & " @CicloAnulación,"                          '23
            fSql = fSql & " @CaptacionRestrita,"                          '23
            fSql = fSql & " @DíaDelCiclo,"                          '23
            fSql = fSql & " @PlanPago,"                          '23
            fSql = fSql & " @VIAPRINCIPAL,"                          '23
            fSql = fSql & " @Complemento,"                          '23
            fSql = fSql & " @BARRIO,"                          '23
            fSql = fSql & " @MUNICIPIO,"                          '23
            fSql = fSql & " @DEPARTAMIENTO,"                          '23
            fSql = fSql & " @CódigoPostal,"                          '23
            fSql = fSql & " @Referencia,"                          '23
            fSql = fSql & " @VIAPRINCIPALEntrega,"                          '23
            fSql = fSql & " @ComplementoEntrega,"                          '23
            fSql = fSql & " @BARRIOEntrega,"                          '23
            fSql = fSql & " @MUNICIPIOEntrega,"                          '23
            fSql = fSql & " @DEPARTAMIENTOEntrega,"                          '23
            fSql = fSql & " @CódigoPostalEntrega,"                          '23
            fSql = fSql & " @ReferenciaEntrega,"                          '23
            fSql = fSql & " @Teléfono,"                          '23
            fSql = fSql & " @CódModeloComercial,"                          '23
            fSql = fSql & " @ModeloComercial,"                          '23
            fSql = fSql & " @CódEstructuraPadre,"                          '23
            fSql = fSql & " @EstructuraPadre,"                          '23
            fSql = fSql & " @CódEstructura,"                          '23
            fSql = fSql & " @Estructura,"                          '23
            fSql = fSql & " @ResponsableEstructura,"                          '23
            fSql = fSql & " @TeléfonoResponsable,"                          '23
            fSql = fSql & " @CódUsuarioCreación,"                          '23
            fSql = fSql & " @UsuarioDeCreación,"                          '23
            fSql = fSql & " @CódUsuarioDeFinalización,"                          '23
            fSql = fSql & " @UsuarioDeFinalización,"                          '23
            fSql = fSql & " @CodTransportadora,"                          '23
            fSql = fSql & " @Transportadora,"                          '23
            fSql = fSql & " @CódigoAgencia,"                          '23
            fSql = fSql & " @Agencia,"                          '23
            fSql = fSql & " @Volumen,"                          '23
            fSql = fSql & " @PesoEstimado,"                          '23
            fSql = fSql & " @PesoReal,"                          '23
            fSql = fSql & " @LoteSeparación,"                          '23
            fSql = fSql & " @Minuta,"                          '23
            fSql = fSql & " @PedidoConsolidado)"                            '39
            cmAlta = New OleDbCommand(fSql)




            cmAlta.Parameters.Add("@CodigoPedido", OleDbType.VarChar)                            '2
            cmAlta.Parameters.Add("@SituaciónFiscal", OleDbType.VarChar)                      '3
            cmAlta.Parameters.Add("@Factura", OleDbType.VarChar)                              '4
            cmAlta.Parameters.Add("@Persona", OleDbType.VarChar)                              '5
            cmAlta.Parameters.Add("@NombrePersona", OleDbType.VarChar)                        '6
            cmAlta.Parameters.Add("@CodigoGrupo", OleDbType.VarChar)                          '7
            cmAlta.Parameters.Add("@OrdenDelPedido", OleDbType.VarChar) ' orden del Pedido      8
            cmAlta.Parameters.Add("@Puntos", OleDbType.VarChar)                               '9
            cmAlta.Parameters.Add("@CantidadÍtems", OleDbType.VarChar)                        '10
            cmAlta.Parameters.Add("@ValorTotal", OleDbType.VarChar)                           '11
            cmAlta.Parameters.Add("@ValorTabla", OleDbType.VarChar)                           '12
            cmAlta.Parameters.Add("@ValorPracticado", OleDbType.VarChar)                      '13
            cmAlta.Parameters.Add("@ValorLíquido", OleDbType.VarChar)                         '14
            cmAlta.Parameters.Add("@ValorProductosRegulares", OleDbType.VarChar)              '15
            cmAlta.Parameters.Add("@MedioCaptación", OleDbType.VarChar)                       '16
            cmAlta.Parameters.Add("@SituaciónComercial", OleDbType.VarChar)                   '17
            cmAlta.Parameters.Add("@DetalleSituaciónComercial", OleDbType.VarChar)            '18
            cmAlta.Parameters.Add("@SituacionIntegracionExterna", OleDbType.VarChar)          '19
            cmAlta.Parameters.Add("@Fecha", OleDbType.VarChar)                                '20
            cmAlta.Parameters.Add("@FechaMarketing", OleDbType.VarChar)                       '21
            cmAlta.Parameters.Add("@PrevisiónEntrega", OleDbType.VarChar)                     '22
            cmAlta.Parameters.Add("@FechaEntrega", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@FechaAutorizaciónFacturación", OleDbType.VarChar)         '24
            cmAlta.Parameters.Add("@CicloCaptación", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@SubCiclo", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@CicloMarketing", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@CicloIndicador", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@CicloAnulación", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@CaptacionRestrita", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@DíaDelCiclo", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@PlanPago", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@VIAPRINCIPAL", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@Complemento", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@BARRIO", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@MUNICIPIO", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@DEPARTAMIENTO", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@CódigoPostal", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@Referencia", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@VIAPRINCIPALEntrega", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@ComplementoEntrega", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@BARRIOEntrega", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@MUNICIPIOEntrega", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@DEPARTAMIENTOEntrega", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@CódigoPostalEntrega", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@ReferenciaEntrega", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@Teléfono", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@CódModeloComercial", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@ModeloComercial", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@CódEstructuraPadre", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@EstructuraPadre", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@CódEstructura", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@Estructura", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@ResponsableEstructura", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@TeléfonoResponsable", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@CódUsuarioCreación", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@UsuarioDeCreación", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@CódUsuarioDeFinalización", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@UsuarioDeFinalización", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@CodTransportadora", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@Transportadora", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@CódigoAgencia", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@Agencia", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@Volumen", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@PesoEstimado", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@PesoReal", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@LoteSeparación", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@Minuta", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add("@PedidoConsolidado", OleDbType.VarChar)                           '39       
            cmAlta.Connection = cn


            For i = 0 To TGera.Tables(0).Rows.Count - 1
                cmAlta.Parameters("@CodigoPedido").Value = TGera.Tables(0).Rows(i)(0).ToString()                           '1
                cmAlta.Parameters("@SituaciónFiscal").Value = TGera.Tables(0).Rows(i)(1).ToString()                     '2
                cmAlta.Parameters("@Factura").Value = TGera.Tables(0).Rows(i)(2).ToString()                             '3
                cmAlta.Parameters("@Persona").Value = TGera.Tables(0).Rows(i)(3).ToString()                             '4
                cmAlta.Parameters("@NombrePersona").Value = TGera.Tables(0).Rows(i)(4).ToString()                       '5
                cmAlta.Parameters("@CodigoGrupo").Value = TGera.Tables(0).Rows(i)(5).ToString()                         '6
                cmAlta.Parameters("@OrdenDelPedido").Value = TGera.Tables(0).Rows(i)(6).ToString() ' orden del Pedido     7
                cmAlta.Parameters("@Puntos").Value = TGera.Tables(0).Rows(i)(7).ToString()                              '8
                cmAlta.Parameters("@CantidadÍtems").Value = TGera.Tables(0).Rows(i)(8).ToString()                       '9
                cmAlta.Parameters("@ValorTotal").Value = TGera.Tables(0).Rows(i)(9).ToString()                          '10
                cmAlta.Parameters("@ValorTabla").Value = TGera.Tables(0).Rows(i)(10).ToString()                          '11
                cmAlta.Parameters("@ValorPracticado").Value = TGera.Tables(0).Rows(i)(11).ToString()                     '12
                cmAlta.Parameters("@ValorLíquido").Value = TGera.Tables(0).Rows(i)(12).ToString()                        '13
                cmAlta.Parameters("@ValorProductosRegulares").Value = TGera.Tables(0).Rows(i)(13).ToString()             '14
                cmAlta.Parameters("@MedioCaptación").Value = TGera.Tables(0).Rows(i)(14).ToString()                      '15
                cmAlta.Parameters("@SituaciónComercial").Value = TGera.Tables(0).Rows(i)(15).ToString()                  '16
                cmAlta.Parameters("@DetalleSituaciónComercial").Value = TGera.Tables(0).Rows(i)(16).ToString()           '17
                cmAlta.Parameters("@SituacionIntegracionExterna").Value = TGera.Tables(0).Rows(i)(17).ToString()         '18
                cmAlta.Parameters("@Fecha").Value = TGera.Tables(0).Rows(i)(18).ToString()                               '19
                cmAlta.Parameters("@FechaMarketing").Value = TGera.Tables(0).Rows(i)(19).ToString()                      '20
                cmAlta.Parameters("@PrevisiónEntrega").Value = TGera.Tables(0).Rows(i)(20).ToString()                    '21
                cmAlta.Parameters("@FechaEntrega").Value = TGera.Tables(0).Rows(i)(21).ToString()                        '22
                cmAlta.Parameters("@FechaAutorizaciónFacturación").Value = TGera.Tables(0).Rows(i)(22).ToString()        '23
                cmAlta.Parameters("@CicloCaptación").Value = TGera.Tables(0).Rows(i)(23).ToString()                      '24
                cmAlta.Parameters("@SubCiclo").Value = TGera.Tables(0).Rows(i)(24).ToString()                            '25
                cmAlta.Parameters("@CicloMarketing").Value = TGera.Tables(0).Rows(i)(25).ToString()                      '26
                cmAlta.Parameters("@CicloIndicador").Value = TGera.Tables(0).Rows(i)(26).ToString()                      '27
                cmAlta.Parameters("@CicloAnulación").Value = TGera.Tables(0).Rows(i)(27).ToString()                      '28
                cmAlta.Parameters("@CaptacionRestrita").Value = TGera.Tables(0).Rows(i)(28).ToString()                   '29
                cmAlta.Parameters("@DíaDelCiclo").Value = TGera.Tables(0).Rows(i)(29).ToString()                         '30
                cmAlta.Parameters("@PlanPago").Value = TGera.Tables(0).Rows(i)(30).ToString()                            '31
                cmAlta.Parameters("@VIAPRINCIPAL").Value = TGera.Tables(0).Rows(i)(31).ToString()                       '32
                cmAlta.Parameters("@Complemento").Value = TGera.Tables(0).Rows(i)(32).ToString()                         '33
                cmAlta.Parameters("@BARRIO").Value = TGera.Tables(0).Rows(i)(33).ToString()                              '34
                cmAlta.Parameters("@MUNICIPIO").Value = TGera.Tables(0).Rows(i)(34).ToString()                           '35
                cmAlta.Parameters("@DEPARTAMIENTO").Value = TGera.Tables(0).Rows(i)(35).ToString()                       '36
                cmAlta.Parameters("@CódigoPostal").Value = TGera.Tables(0).Rows(i)(36).ToString()                          '37
                cmAlta.Parameters("@Referencia").Value = TGera.Tables(0).Rows(i)(37).ToString()                          '37
                cmAlta.Parameters("@VIAPRINCIPALEntrega").Value = TGera.Tables(0).Rows(i)(38).ToString()                 '38
                cmAlta.Parameters("@ComplementoEntrega").Value = TGera.Tables(0).Rows(i)(39).ToString()                  '39
                cmAlta.Parameters("@BARRIOEntrega").Value = TGera.Tables(0).Rows(i)(40).ToString()                       '40
                cmAlta.Parameters("@MUNICIPIOEntrega").Value = TGera.Tables(0).Rows(i)(41).ToString()                    '41
                cmAlta.Parameters("@DEPARTAMIENTOEntrega").Value = TGera.Tables(0).Rows(i)(42).ToString()                '42
                cmAlta.Parameters("@CódigoPostalEntrega").Value = TGera.Tables(0).Rows(i)(43).ToString()                 '43
                cmAlta.Parameters("@ReferenciaEntrega").Value = TGera.Tables(0).Rows(i)(44).ToString()                   '44
                cmAlta.Parameters("@Teléfono").Value = TGera.Tables(0).Rows(i)(45).ToString()                            '45
                cmAlta.Parameters("@CódModeloComercial").Value = TGera.Tables(0).Rows(i)(46).ToString()                  '46
                cmAlta.Parameters("@ModeloComercial").Value = TGera.Tables(0).Rows(i)(47).ToString()                     '47
                cmAlta.Parameters("@CódEstructuraPadre").Value = TGera.Tables(0).Rows(i)(48).ToString()                  '48
                cmAlta.Parameters("@EstructuraPadre").Value = TGera.Tables(0).Rows(i)(49).ToString()                     '49
                cmAlta.Parameters("@CódEstructura").Value = TGera.Tables(0).Rows(i)(50).ToString()                       '50
                cmAlta.Parameters("@Estructura").Value = TGera.Tables(0).Rows(i)(51).ToString()                          '51
                cmAlta.Parameters("@ResponsableEstructura").Value = TGera.Tables(0).Rows(i)(52).ToString()               '52
                cmAlta.Parameters("@TeléfonoResponsable").Value = TGera.Tables(0).Rows(i)(53).ToString()                 '53
                cmAlta.Parameters("@CódUsuarioCreación").Value = TGera.Tables(0).Rows(i)(54).ToString()                  '54
                cmAlta.Parameters("@UsuarioDeCreación").Value = TGera.Tables(0).Rows(i)(55).ToString()                   '55
                cmAlta.Parameters("@CódUsuarioDeFinalización").Value = TGera.Tables(0).Rows(i)(56).ToString()            '56
                cmAlta.Parameters("@UsuarioDeFinalización").Value = TGera.Tables(0).Rows(i)(57).ToString()               '57
                cmAlta.Parameters("@CodTransportadora").Value = TGera.Tables(0).Rows(i)(58).ToString()                   '58
                cmAlta.Parameters("@Transportadora").Value = TGera.Tables(0).Rows(i)(59).ToString()                      '59
                cmAlta.Parameters("@CódigoAgencia").Value = TGera.Tables(0).Rows(i)(60).ToString()                       '60
                cmAlta.Parameters("@Agencia").Value = TGera.Tables(0).Rows(i)(61).ToString()                             '61
                cmAlta.Parameters("@Volumen").Value = TGera.Tables(0).Rows(i)(62).ToString()                             '62
                cmAlta.Parameters("@PesoEstimado").Value = TGera.Tables(0).Rows(i)(63).ToString()                        '63
                cmAlta.Parameters("@PesoReal").Value = TGera.Tables(0).Rows(i)(64).ToString()                            '64
                cmAlta.Parameters("@LoteSeparación").Value = TGera.Tables(0).Rows(i)(65).ToString()                      '65
                cmAlta.Parameters("@Minuta").Value = TGera.Tables(0).Rows(i)(66).ToString()                              '66
                cmAlta.Parameters("@PedidoConsolidado").Value = TGera.Tables(0).Rows(i)(67).ToString()                   '67
                cmAlta.ExecuteNonQuery()
            Next

            'Cerramos la conexion con la base de datos
            cn.Close()
        Catch
            MsgBox(Err.Description)
            cn.Close()
        End Try

    End Sub
    Private Sub SubirServientrega(ByVal NombreDe_MiTabla As String)

        Dim cmAlta As OleDbCommand
        Dim com As OleDbCommand
        Dim i As Integer
        Dim cn As System.Data.OleDb.OleDbConnection

        'Construimos y abrimos la conexión con la base de datos
        cn = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & NombreDe_MiTabla & ";User Id=admin;Password=;")
        cn.Open()

        Try
            com = New OleDbCommand("DELETE FROM Servientrega", cn)
            com.ExecuteNonQuery()
            'Preparamos el comando
            ') VALUES(@nombre,@apellidos)"
            Dim fSql As String
            fSql = "INSERT INTO Servientrega ( "                               '1
            fSql = fSql & " RECOLECCION,"                             '2
            fSql = fSql & " FECHA_ENVIO,"                       '3
            fSql = fSql & " PEDIDO,"                               '4
            fSql = fSql & " GUIA,"                               '5
            fSql = fSql & " DESTINATARIO,"                         '6
            fSql = fSql & " DIRECCION,"                           '7
            fSql = fSql & " CIUDAD_DESTINO," ' orden del Pedido      8
            fSql = fSql & " REGIONAL,"                                '9
            fSql = fSql & " GERENCIA,"                         '10
            fSql = fSql & " TELEFONO,"                            '11
            fSql = fSql & " TIEMPO_RED_OPERATIVA,"                            '12
            fSql = fSql & " ESTADO_ENVIO,"                       '13
            fSql = fSql & " FECHA_DE_ENTREGA,"                          '14
            fSql = fSql & " DIAS_DE_ENTREGADO,"               '15
            fSql = fSql & " DIFERENCIA,"                        '16
            fSql = fSql & " EFICIENCIA,"                    '17
            fSql = fSql & " NOVEDAD_1,"             '18
            fSql = fSql & " NOVEDAD_2,"           '19
            fSql = fSql & " OBSERVACION,"                                 '20
            fSql = fSql & " FECHA_NOVEDAD,"                        '21
            fSql = fSql & " PIEZAS,"                      '22
            fSql = fSql & " ESTADO_SISM,"                          '23
            fSql = fSql & " AREA,"          '24
            fSql = fSql & " ESTADO_EN_EL_SISTEMA)"                            '39


            fSql = fSql & " VALUES("                                '1

            fSql = fSql & " @RECOLECCION,"                             '2
            fSql = fSql & " @FECHA_ENVIO,"                       '3
            fSql = fSql & " @PEDIDO,"                               '4
            fSql = fSql & " @GUIA,"                               '5
            fSql = fSql & " @DESTINATARIO,"                         '6
            fSql = fSql & " @DIRECCION,"                           '7
            fSql = fSql & " @CIUDAD_DESTINO," ' orden del Pedido      8
            fSql = fSql & " @REGIONAL,"                                '9
            fSql = fSql & " @GERENCIA,"                         '10
            fSql = fSql & " @TELEFONO,"                            '11
            fSql = fSql & " @TIEMPO_RED_OPERATIVA,"                            '12
            fSql = fSql & " @ESTADO_ENVIO,"                       '13
            fSql = fSql & " @FECHA_DE_ENTREGA,"                          '14
            fSql = fSql & " @DIAS_DE_ENTREGADO,"               '15
            fSql = fSql & " @DIFERENCIA,"                        '16
            fSql = fSql & " @EFICIENCIA,"                    '17
            fSql = fSql & " @NOVEDAD_1,"             '18
            fSql = fSql & " @NOVEDAD_2,"           '19
            fSql = fSql & " @OBSERVACION,"                                 '20
            fSql = fSql & " @FECHA_NOVEDAD,"                        '21
            fSql = fSql & " @PIEZAS,"                      '22
            fSql = fSql & " @ESTADO_SISM,"                          '23
            fSql = fSql & " @AREA,"          '24
            fSql = fSql & " @ESTADO_EN_EL_SISTEMA)"                            '39
            cmAlta = New OleDbCommand(fSql)




            cmAlta.Parameters.Add("  @RECOLECCION", OleDbType.VarChar)                             '2
            cmAlta.Parameters.Add("  @FECHA_ENVIO", OleDbType.VarChar)                       '3
            cmAlta.Parameters.Add("  @PEDIDO", OleDbType.VarChar)                               '4
            cmAlta.Parameters.Add("  @GUIA", OleDbType.VarChar)                               '5
            cmAlta.Parameters.Add("  @DESTINATARIO", OleDbType.VarChar)                         '6
            cmAlta.Parameters.Add("  @DIRECCION", OleDbType.VarChar)                           '7
            cmAlta.Parameters.Add("  @CIUDAD_DESTINO", OleDbType.VarChar) ' orden del Pedido      8
            cmAlta.Parameters.Add("  @REGIONAL", OleDbType.VarChar)                                '9
            cmAlta.Parameters.Add("  @GERENCIA", OleDbType.VarChar)                         '10
            cmAlta.Parameters.Add("  @TELEFONO", OleDbType.VarChar)                            '11
            cmAlta.Parameters.Add("  @TIEMPO_RED_OPERATIVA", OleDbType.VarChar)                            '12
            cmAlta.Parameters.Add("  @ESTADO_ENVIO", OleDbType.VarChar)                       '13
            cmAlta.Parameters.Add("  @FECHA_DE_ENTREGA", OleDbType.VarChar)                          '14
            cmAlta.Parameters.Add("  @DIAS_DE_ENTREGADO", OleDbType.VarChar)               '15
            cmAlta.Parameters.Add("  @DIFERENCIA", OleDbType.VarChar)                        '16
            cmAlta.Parameters.Add("  @EFICIENCIA", OleDbType.VarChar)                    '17
            cmAlta.Parameters.Add("  @NOVEDAD_1", OleDbType.VarChar)             '18
            cmAlta.Parameters.Add("  @NOVEDAD_2", OleDbType.VarChar)           '19
            cmAlta.Parameters.Add("  @OBSERVACION", OleDbType.VarChar)                                 '20
            cmAlta.Parameters.Add("  @FECHA_NOVEDAD", OleDbType.VarChar)                        '21
            cmAlta.Parameters.Add("  @PIEZAS", OleDbType.VarChar)                      '22
            cmAlta.Parameters.Add("  @ESTADO_SISM", OleDbType.VarChar)                          '23
            cmAlta.Parameters.Add("  @AREA", OleDbType.VarChar)          '24
            cmAlta.Parameters.Add("  @ESTADO_EN_EL_SISTEMA", OleDbType.VarChar)                           '39      
            cmAlta.Connection = cn


            For i = 0 To Tservi.Tables(0).Rows.Count - 1
                cmAlta.Parameters("  @RECOLECCION").Value = Tservi.Tables(0).Rows(i)(0).ToString()                            '2
                cmAlta.Parameters("  @FECHA_ENVIO").Value = Tservi.Tables(0).Rows(i)(1).ToString()                      '3
                cmAlta.Parameters("  @PEDIDO").Value = Tservi.Tables(0).Rows(i)(2).ToString()                              '4
                cmAlta.Parameters("  @GUIA").Value = Tservi.Tables(0).Rows(i)(3).ToString()                              '5
                cmAlta.Parameters("  @DESTINATARIO").Value = Tservi.Tables(0).Rows(i)(4).ToString()                        '6
                cmAlta.Parameters("  @DIRECCION").Value = Tservi.Tables(0).Rows(i)(5).ToString()                          '7
                cmAlta.Parameters("  @CIUDAD_DESTINO").Value = Tservi.Tables(0).Rows(i)(6).ToString() ' orden del Pedido      8
                cmAlta.Parameters("  @REGIONAL").Value = Tservi.Tables(0).Rows(i)(7).ToString()                               '9
                cmAlta.Parameters("  @GERENCIA").Value = Tservi.Tables(0).Rows(i)(8).ToString()                        '10
                cmAlta.Parameters("  @TELEFONO").Value = Tservi.Tables(0).Rows(i)(9).ToString()                           '11
                cmAlta.Parameters("  @TIEMPO_RED_OPERATIVA").Value = Tservi.Tables(0).Rows(i)(10).ToString()                           '12
                cmAlta.Parameters("  @ESTADO_ENVIO").Value = Tservi.Tables(0).Rows(i)(11).ToString()                      '13
                cmAlta.Parameters("  @FECHA_DE_ENTREGA").Value = Tservi.Tables(0).Rows(i)(12).ToString()                         '14
                cmAlta.Parameters("  @DIAS_DE_ENTREGADO").Value = Tservi.Tables(0).Rows(i)(13).ToString()              '15
                cmAlta.Parameters("  @DIFERENCIA").Value = Tservi.Tables(0).Rows(i)(14).ToString()                       '16
                cmAlta.Parameters("  @EFICIENCIA").Value = Tservi.Tables(0).Rows(i)(15).ToString()                   '17
                cmAlta.Parameters("  @NOVEDAD_1").Value = Tservi.Tables(0).Rows(i)(16).ToString()            '18
                cmAlta.Parameters("  @NOVEDAD_2").Value = Tservi.Tables(0).Rows(i)(17).ToString()          '19
                cmAlta.Parameters("  @OBSERVACION").Value = Tservi.Tables(0).Rows(i)(18).ToString()                                '20
                cmAlta.Parameters("  @FECHA_NOVEDAD").Value = Tservi.Tables(0).Rows(i)(19).ToString()                       '21
                cmAlta.Parameters("  @PIEZAS").Value = Tservi.Tables(0).Rows(i)(20).ToString()                     '22
                cmAlta.Parameters("  @ESTADO_SISM").Value = Tservi.Tables(0).Rows(i)(21).ToString()                         '23
                cmAlta.Parameters("  @AREA").Value = Tservi.Tables(0).Rows(i)(22).ToString()         '24
                cmAlta.Parameters("  @ESTADO_EN_EL_SISTEMA").Value = Tservi.Tables(0).Rows(i)(23).ToString()                          '39
                cmAlta.ExecuteNonQuery()
            Next

            'Cerramos la conexion con la base de datos
            cn.Close()

        Catch
            cn.Close()
            MsgBox(Err.Description)
        End Try

    End Sub


    Private Sub SubirEnvia(ByVal NombreDe_MiTabla As String)


        Dim cmAlta As OleDbCommand
        Dim com As OleDbCommand

        Dim i As Integer
        Dim cn As System.Data.OleDb.OleDbConnection
        'Construimos y abrimos la conexión con la base de datos
        cn = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & NombreDe_MiTabla & ";User Id=admin;Password=;")
        cn.Open()

        Try
            com = New OleDbCommand("DELETE FROM Envia", cn)
            com.ExecuteNonQuery()

            ') VALUES(@nombre,@apellidos)"
            Dim fSql As String
            fSql = "INSERT INTO Envia ( "                          '1
            fSql = fSql & " Numero_Pedido,"                          '2
            fSql = fSql & " Numero_Guia,"                       '3
            fSql = fSql & " Codigo_Consultora,"                               '4
            fSql = fSql & " Nombre_Destinatario,"                               '5
            fSql = fSql & " Direccion_Destinatario,"                         '6
            fSql = fSql & " Sector,"                           '7
            fSql = fSql & " Gerencia_en_ventas," ' orden del Pedido      8
            fSql = fSql & " Estado_Actual,"                                '9
            fSql = fSql & " Fec_despacho,"                         '10
            fSql = fSql & " Fec_Entrega,"                            '11
            fSql = fSql & " Promesa_Entrega,"                            '12
            fSql = fSql & " Dias_Entrega,"                       '13
            fSql = fSql & " Dif_Dias,"                          '14
            fSql = fSql & " Eficiencia,"               '15
            fSql = fSql & " Responsable,"                        '16
            fSql = fSql & " Regional_destino,"                    '17
            fSql = fSql & " Ciudad_Destino,"             '18
            fSql = fSql & " Tel_Destinatario,"           '19
            fSql = fSql & " Num_Unidades,"                                 '20
            fSql = fSql & " Valor_Flete,"                        '21
            fSql = fSql & " Fecha_Novedad,"                      '22
            fSql = fSql & " Descripcion_novedad,"                          '23
            fSql = fSql & " Fecha_Novedad1,"          '24
            fSql = fSql & " Descripcion_novedad1," ' Ciclo Captación      '25
            fSql = fSql & " Fecha_Solucion,"                              '26
            fSql = fSql & " Observaciones_Novedades,"                        '27
            fSql = fSql & " Confirmacion_de_direcciones_Por_Envia)"                        '28


            fSql = fSql & " VALUES("                                '1

            fSql = fSql & " @Numero_Pedido,"                          '2
            fSql = fSql & " @Numero_Guia,"                       '3
            fSql = fSql & " @Codigo_Consultora,"                               '4
            fSql = fSql & " @Nombre_Destinatario,"                               '5
            fSql = fSql & " @Direccion_Destinatario,"                         '6
            fSql = fSql & " @Sector,"                           '7
            fSql = fSql & " @Gerencia_en_ventas," ' orden del Pedido      8
            fSql = fSql & " @Estado_Actual,"                                '9
            fSql = fSql & " @Fec_despacho,"                         '10
            fSql = fSql & " @Fec_Entrega,"                            '11
            fSql = fSql & " @Promesa_Entrega,"                            '12
            fSql = fSql & " @Dias_Entrega,"                       '13
            fSql = fSql & " @Dif_Dias,"                          '14
            fSql = fSql & " @Eficiencia,"               '15
            fSql = fSql & " @Responsable,"                        '16
            fSql = fSql & " @Regional_destino,"                    '17
            fSql = fSql & " @Ciudad_Destino,"             '18
            fSql = fSql & " @Tel_Destinatario,"           '19
            fSql = fSql & " @Num_Unidades,"                                 '20
            fSql = fSql & " @Valor_Flete,"                        '21
            fSql = fSql & " @Fecha_Novedad,"                      '22
            fSql = fSql & " @Descripcion_novedad,"                          '23
            fSql = fSql & " @Fecha_Novedad1,"          '24
            fSql = fSql & " @Descripcion_novedad1," ' Ciclo Captación      '25
            fSql = fSql & " @Fecha_Solucion,"                              '26
            fSql = fSql & " @Observaciones_Novedades,"                        '27
            fSql = fSql & " @Confirmacion_de_direcciones_Por_Envia)"
            cmAlta = New OleDbCommand(fSql)




            cmAlta.Parameters.Add(" @Numero_Pedido", OleDbType.VarChar)                         '2
            cmAlta.Parameters.Add(" @Numero_Guia", OleDbType.VarChar)                      '3
            cmAlta.Parameters.Add(" @Codigo_Consultora", OleDbType.VarChar)                              '4
            cmAlta.Parameters.Add(" @Nombre_Destinatario", OleDbType.VarChar)                              '5
            cmAlta.Parameters.Add(" @Direccion_Destinatario", OleDbType.VarChar)                        '6
            cmAlta.Parameters.Add(" @Sector", OleDbType.VarChar)                          '7
            cmAlta.Parameters.Add(" @Gerencia_en_ventas", OleDbType.VarChar) ' orden del Pedido      8
            cmAlta.Parameters.Add(" @Estado_Actual", OleDbType.VarChar)                               '9
            cmAlta.Parameters.Add(" @Fec_despacho", OleDbType.VarChar)                        '10
            cmAlta.Parameters.Add(" @Fec_Entrega", OleDbType.VarChar)                           '11
            cmAlta.Parameters.Add(" @Promesa_Entrega", OleDbType.VarChar)                           '12
            cmAlta.Parameters.Add(" @Dias_Entrega", OleDbType.VarChar)                      '13
            cmAlta.Parameters.Add(" @Dif_Dias", OleDbType.VarChar)                         '14
            cmAlta.Parameters.Add(" @Eficiencia", OleDbType.VarChar)              '15
            cmAlta.Parameters.Add(" @Responsable", OleDbType.VarChar)                       '16
            cmAlta.Parameters.Add(" @Regional_destino", OleDbType.VarChar)                   '17
            cmAlta.Parameters.Add(" @Ciudad_Destino", OleDbType.VarChar)            '18
            cmAlta.Parameters.Add(" @Tel_Destinatario", OleDbType.VarChar)          '19
            cmAlta.Parameters.Add(" @Num_Unidades", OleDbType.VarChar)                                '20
            cmAlta.Parameters.Add(" @Valor_Flete", OleDbType.VarChar)                       '21
            cmAlta.Parameters.Add(" @Fecha_Novedad", OleDbType.VarChar)                     '22
            cmAlta.Parameters.Add(" @Descripcion_novedad", OleDbType.VarChar)                         '23
            cmAlta.Parameters.Add(" @Fecha_Novedad1", OleDbType.VarChar)         '24
            cmAlta.Parameters.Add(" @Descripcion_novedad1", OleDbType.VarChar) ' Ciclo Captación      '25
            cmAlta.Parameters.Add(" @Fecha_Solucion", OleDbType.VarChar)                             '26
            cmAlta.Parameters.Add(" @Observaciones_Novedades", OleDbType.VarChar)                       '27
            cmAlta.Parameters.Add(" @Confirmacion_de_direcciones_Por_Envias", OleDbType.VarChar)
            cmAlta.Connection = cn
            For i = 0 To Tenvia.Tables(0).Rows.Count - 1
                cmAlta.Parameters(" @Numero_Pedido").Value = Tenvia.Tables(0).Rows(i)(0).ToString()                           '2
                cmAlta.Parameters(" @Numero_Guia").Value = Tenvia.Tables(0).Rows(i)(1).ToString()                        '3
                cmAlta.Parameters(" @Codigo_Consultora").Value = Tenvia.Tables(0).Rows(i)(2).ToString()                                '4
                cmAlta.Parameters(" @Nombre_Destinatario").Value = Tenvia.Tables(0).Rows(i)(3).ToString()                                '5
                cmAlta.Parameters(" @Direccion_Destinatario").Value = Tenvia.Tables(0).Rows(i)(4).ToString()                          '6
                cmAlta.Parameters(" @Sector").Value = Tenvia.Tables(0).Rows(i)(5).ToString()                            '7
                cmAlta.Parameters(" @Gerencia_en_ventas").Value = Tenvia.Tables(0).Rows(i)(6).ToString()
                cmAlta.Parameters(" @Estado_Actual").Value = Tenvia.Tables(0).Rows(i)(7).ToString()                                 '9
                cmAlta.Parameters(" @Fec_despacho").Value = Tenvia.Tables(0).Rows(i)(8).ToString()                          '10
                cmAlta.Parameters(" @Fec_Entrega").Value = Tenvia.Tables(0).Rows(i)(9).ToString()                             '11
                cmAlta.Parameters(" @Promesa_Entrega").Value = Tenvia.Tables(0).Rows(i)(10).ToString()                             '12
                cmAlta.Parameters(" @Dias_Entrega").Value = Tenvia.Tables(0).Rows(i)(11).ToString()                        '13
                cmAlta.Parameters(" @Dif_Dias").Value = Tenvia.Tables(0).Rows(i)(12).ToString()                           '14
                cmAlta.Parameters(" @Eficiencia").Value = Tenvia.Tables(0).Rows(i)(13).ToString()                '15
                cmAlta.Parameters(" @Responsable").Value = Tenvia.Tables(0).Rows(i)(14).ToString()                         '16
                cmAlta.Parameters(" @Regional_destino").Value = Tenvia.Tables(0).Rows(i)(15).ToString()                     '17
                cmAlta.Parameters(" @Ciudad_Destino").Value = Tenvia.Tables(0).Rows(i)(16).ToString()              '18
                cmAlta.Parameters(" @Tel_Destinatario").Value = Tenvia.Tables(0).Rows(i)(17).ToString()            '19
                cmAlta.Parameters(" @Num_Unidades").Value = Tenvia.Tables(0).Rows(i)(18).ToString()                                  '20
                cmAlta.Parameters(" @Valor_Flete").Value = Tenvia.Tables(0).Rows(i)(19).ToString()                         '21
                cmAlta.Parameters(" @Fecha_Novedad").Value = Tenvia.Tables(0).Rows(i)(20).ToString()                       '22
                cmAlta.Parameters(" @Descripcion_novedad").Value = Tenvia.Tables(0).Rows(i)(21).ToString()                           '23
                cmAlta.Parameters(" @Fecha_Novedad1").Value = Tenvia.Tables(0).Rows(i)(22).ToString()           '24
                cmAlta.Parameters(" @Descripcion_novedad1").Value = Tenvia.Tables(0).Rows(i)(23).ToString()
                cmAlta.Parameters(" @Fecha_Solucion").Value = Tenvia.Tables(0).Rows(i)(24).ToString()                               '26
                cmAlta.Parameters(" @Observaciones_Novedades").Value = Tenvia.Tables(0).Rows(i)(25).ToString()                         '27
                cmAlta.Parameters(" @Confirmacion_de_direcciones_Por_Envias").Value = Tenvia.Tables(0).Rows(i)(26).ToString()                    'realizamos el alta
                cmAlta.ExecuteNonQuery()
            Next

            'Cerramos la conexion con la base de datos
            cn.Close()
        Catch
            MsgBox(Err.Description)
            cn.Close()
        End Try

    End Sub
    Private Sub B_Cobro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B_Inter.Click
        Dim folderDlg As New OpenFileDialog
        folderDlg.Filter = "(*.xls)|*.xls|(*.xlsx)|*.xlsx"
        If (folderDlg.ShowDialog() = DialogResult.OK) Then
            Inter.Text = folderDlg.FileName
            InterHoja.Visible = True
        End If
    End Sub

    Private Sub B_Ventas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B_Servientrega.Click
        Dim folderDlg As New OpenFileDialog
        folderDlg.Filter = "(*.xls)|*.xls|(*.xlsx)|*.xlsx"
        If (folderDlg.ShowDialog() = DialogResult.OK) Then
            Servientrega.Text = folderDlg.FileName
            ServientregaHoja.Visible = True
        End If
    End Sub

    Private Sub B_Cartera_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B_Envia.Click
        Dim folderDlg As New OpenFileDialog
        folderDlg.Filter = "(*.xls)|*.xls|(*.xlsx)|*.xl"
        If (folderDlg.ShowDialog() = DialogResult.OK) Then
            Envia.Text = folderDlg.FileName
            EnviaHoja.Visible = True
        End If
    End Sub

    Private Sub B_Revendedores_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B_Gera.Click
        Dim folderDlg As New OpenFileDialog
        folderDlg.Filter = "(*.xls)|*.xls|(*.xlsx)|*.xlsx"
        If (folderDlg.ShowDialog() = DialogResult.OK) Then
            Gera.Text = folderDlg.FileName
            GeraHoja.Visible = True
        End If
    End Sub
    Public Function correrxls(ByVal carpetaObjetivo As String, ByVal Titulo As String, ByVal macros As String) As Integer
        Try
            Dim oExcel As Excel.ApplicationClass
            Dim oBook As Excel.WorkbookClass
            Dim oBooks As Excel.Workbooks

            'inicia excel y el libro de trabajo
            oExcel = CreateObject("Excel.Application")
            oExcel.Visible = False
            oBooks = oExcel.Workbooks
            oBook = oBooks.Open("C:\Envios\EnviosReporte.xlsm")
            'Corre la macro
            'oExcel.Application.Run("!Actualizar")
            oExcel.Run("Actualizar", )
            'oBook.RefreshAll()
            System.Threading.Thread.Sleep(5000)
            'gaurda el archivo con nombre y fecha
            Dim fecha As Date
            fecha = Date.Now
            Dim dia As String
            Dim nombre As String
            dia = fecha.Day & "-" & fecha.Month & "-" & fecha.Year & "-" & fecha.Hour & "-" & fecha.Minute & "-" & fecha.Second
            nombre = carpetaObjetivo & "\" & Titulo & dia & ".xlsm"
            oBook.Save()
            oBook.Close()
            oBook = Nothing
            oExcel.Quit()
            oExcel = Nothing
            'oBook.SaveAs(nombre)
            'cierra excel

            Return 0
        Catch ex As Exception
            Return 1
        End Try
    End Function


    Private Sub B_Validar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B_Validar.Click
        Try

            If Inter.Text = "" Or Servientrega.Text = "" Or Envia.Text = "" Or Gera.Text = "" Or ServientregaHoja.Text = "" Or EnviaHoja.Text = "" Or InterHoja.Text = "" Or GeraHoja.Text = "" Then
                MsgBox("Datos Incompletos")
                Exit Sub

            End If
            LeerXls()
            subirxls(My.Settings.BD)
            Generar.Visible = True
            MsgBox("Archivos Validados Correctamente")
        Catch ex As Exception
            MsgBox(Err.Description)
        End Try

    End Sub

    Private Sub Ventas_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Servientrega.TextChanged

    End Sub

    Private Sub FolderBrowserDialog1_HelpRequest(sender As System.Object, e As System.EventArgs) Handles FolderBrowserDialog1.HelpRequest

    End Sub

    Private Sub Generar_Click(sender As System.Object, e As System.EventArgs) Handles Generar.Click
        Try
            ' subirxls(My.Settings.BD)
            Dim folderDlg As New FolderBrowserDialog
            Dim destino As String
            folderDlg.Description = "En que Carpeta desea guardar su archivo?"
            If (folderDlg.ShowDialog() = DialogResult.OK) Then
                destino = folderDlg.SelectedPath
                'Dim root As Environment.SpecialFolder = folderDlg.

            else
                Exit Sub
            End if
            correrxls(destino, My.Settings.EXCEL, "Actualizar")
            'Try
            Dim dia As String
            Dim nombre As String
            Dim fecha As Date
            fecha = Date.Now
            dia = fecha.Day & "-" & fecha.Month & "-" & fecha.Year & "-" & fecha.Hour & "-" & fecha.Minute & "-" & fecha.Second
            nombre = destino & "\" & My.Settings.EXCEL & dia & ".xlsm"

            File.Copy(My.Settings.CARPETA & My.Settings.EXCEL & ".xlsm", nombre, True)
            'Catch
            '  MsgBox(Err.Description)
            'End Try
            MsgBox("Archivo guardado en: " & destino & "\" & My.Settings.EXCEL & ".xlsm")
        Catch ex As Exception
            MsgBox(Err.Description)

        End Try
        Me.Close
    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs)
        Dim folderDlg As New OpenFileDialog
        folderDlg.Filter = "(*.xls)|*.xls|(*.xlsx)|*.xlsx"
        If (folderDlg.ShowDialog() = DialogResult.OK) Then
            Inter.Text = folderDlg.FileName
            InterHoja.Visible = True
        End If
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs)
        Dim folderDlg As New OpenFileDialog
        folderDlg.Filter = "(*.xls)|*.xls|(*.xlsx)|*.xlsx"
        If (folderDlg.ShowDialog() = DialogResult.OK) Then
            Servientrega.Text = folderDlg.FileName
            ServientregaHoja.Visible = True
        End If
    End Sub

    Private Sub Button1_Click_1(sender As System.Object, e As System.EventArgs)

    End Sub
End Class
