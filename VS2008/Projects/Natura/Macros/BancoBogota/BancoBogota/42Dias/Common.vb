﻿Module Common
    Function Ceros(ByVal val As Object, ByVal C As Integer)
        Dim sCeros As String = New String("0", C)
        If CInt(C) > Len(Trim(val)) Then
            Ceros = Right(sCeros & Trim(val), C)
        Else
            Ceros = val
        End If

    End Function

    Function Espacios(ByVal val As Object, ByVal C As Integer)
        Dim sCeros As String = New String(" ", C)

        If CInt(C) > Len(Trim(val)) Then
            Espacios = Left(Trim(val) & sCeros, CInt(C))
        Else
            Espacios = val
        End If

    End Function
End Module
