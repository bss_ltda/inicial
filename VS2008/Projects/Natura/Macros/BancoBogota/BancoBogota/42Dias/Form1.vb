﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports System
Imports System.Data
Imports System.IO
Imports System.Data.OleDb
Imports System.Globalization
Imports System.Windows.Forms
Imports vb = Microsoft.VisualBasic
Imports Microsoft.Office.Interop.Excel

Public Class Form1
    Dim fila1 As String
    Dim fila2 As String
    Dim dt As New DataSet
    
    Private Sub LeerXls()


        Dim folder = Inter.Text
        Dim archivo As String
        Dim destino As String
        Dim ruta() As String
        ruta = Split(folder, "\")
        Dim total As Integer
        total = ruta.Length
        archivo = ruta(total - 1)
        Dim contador As Integer
        contador = 0
        folder = ""
        While contador < total - 1
            folder = folder & ruta(contador) & "\"
            contador = contador + 1
        End While
        Try
            Dim hoja As String = "PAGOS_ON_LINE"

            Dim num As Integer
            Dim extension As String = ""
            Dim conexion As New System.Data.OleDb.OleDbConnection
            Dim comando As New OleDbCommand
            Dim adaptador As New OleDbDataAdapter

            Dim ExcelPath As String = folder

            num = Trim(Inter.Text).Length
            extension = Trim(Inter.Text).Substring(num - 4, 4)
            If extension = ".xls" Then
                conexion.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Inter.Text & "; Extended Properties= ""Excel 8.0;HDR=YES; IMEX=1"""
            Else
                conexion.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & Inter.Text & ";Extended Properties=" & Chr(34) & "Excel 12.0 Xml;HDR=YES;IMEX=1" & Chr(34)
            End If

            conexion.Open()
            comando.CommandText = "SELECT * FROM [" & hoja & "$]"
            comando.Connection = conexion
            adaptador.SelectCommand = comando
            conexion.Close()
            adaptador.Fill(dt, "excel")

            Dim Source As String = My.Settings.BSE
            Dim Destination As String = My.Settings.BD
            System.IO.File.Copy(Source, Destination, True)


            SubirInter(My.Settings.BD)


        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try

        destino = ""

        Dim folderDlg As New FolderBrowserDialog
        folderDlg.Description = "En que Carpeta desea guardar su archivo?"
         If (folderDlg.ShowDialog() = DialogResult.OK) Then
            destino = folderDlg.SelectedPath
            'Dim root As Environment.SpecialFolder = folderDlg.
        End If

        If destino <> "" Then
            Dim strStreamW As Stream
            Dim strStreamWriter As StreamWriter

            Dim FilePath As String = destino & "\Validación Datos Natura.txt"
            strStreamW = File.OpenWrite(FilePath)
            strStreamWriter = New StreamWriter(strStreamW, _
                                System.Text.Encoding.Default)


            fila1 = "0108300249740000000000001" & Date.Now.Year & Ceros(Date.Now.Month, 2) & Ceros(Date.Now.Day, 2) & Ceros(Date.Now.Hour, 2) & Ceros(Date.Now.Minute, 2) & "A                                                                                                                                                                                      "
            fila2 = "0577099980100790001RECAUDONATURA                                                                                                                                                                                            "
            strStreamWriter.WriteLine(fila1)
            strStreamWriter.WriteLine(fila2)



            Dim cmd As OleDbCommand
            Dim cmd1 As OleDbCommand
            Dim reader As OleDbDataReader = Nothing
            Dim reader0 As OleDbDataReader = Nothing
            Dim valor As String
            Dim TOTALV As Double
            Dim TOTALR As Double
            Dim cn As System.Data.OleDb.OleDbConnection
            'Construimos y abrimos la conexión con la base de datos
            cn = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & My.Settings.BD & ";User Id=admin;Password=;")
            cn.Open()

            Try
                Dim sql As String = "SELECT * from Detalle"
                cmd = New OleDbCommand(sql, cn)
                reader = cmd.ExecuteReader()

                While reader.Read


                    valor = ""
                    valor = "06" & Ceros(reader.GetString(0).ToString, 48)
                    valor = valor & Ceros(reader.GetString(1).ToString, 30) & "00   "
                    valor = valor & Ceros(CInt(reader.GetString(3).ToString), 14) & "000000000000000000000000000"
                    valor = valor & reader.GetString(2).ToString & "00000000                 000000000000"
                    valor = valor & Microsoft.VisualBasic.Left(Espacios(reader.GetString(4).ToString, 22), 22) ' PASAR POR FUNCION QUE DEJE O COMPLETE 22
                    valor = valor & "000"
                    valor = Espacios(valor, 220)

                    strStreamWriter.WriteLine(valor)
                    TOTALR = TOTALR + 1
                    TOTALV = TOTALV + CInt(reader.GetString(3).ToString)

                End While

                reader.Close()
                fila1 = "08" & Ceros(TOTALR, 9) & Ceros(TOTALV, 18) & "0000000000000000000001"
                fila1 = Espacios(fila1, 220)



                fila2 = "09" & Ceros(TOTALR, 9) & Ceros(TOTALV, 18) & "000000000000000000"
                fila2 = Espacios(fila2, 220)
                strStreamWriter.WriteLine(fila1)
                strStreamWriter.WriteLine(fila2)
                strStreamWriter.Close()
                MsgBox("El archivo se generó con éxito, ruta destino:" & FilePath)

            Catch ex As Exception
                Try
                    strStreamWriter.Close()

                Catch

                End Try

                MsgBox(ex.Message)
            End Try
            cn.Close()
        End If

    End Sub


    Private Sub B_Validar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B_Validar.Click
        LeerXls()
    End Sub

    Private Sub Ventas_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub B_Inter_Click(sender As System.Object, e As System.EventArgs) Handles B_Inter.Click

        Dim folderDlg As New OpenFileDialog
        'folderDlg.s = True
        'folderDlg.Filter = "(*.csv)|*.csv"
        If (folderDlg.ShowDialog() = DialogResult.OK) Then
            Inter.Text = folderDlg.FileName
            'Dim root As Environment.SpecialFolder = folderDlg.
        End If
    End Sub

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub SubirInter(ByVal NombreDe_MiTabla As String)


        Dim cmAlta As OleDbCommand
        Dim com As OleDbCommand

        Dim i As Integer
        Dim cn As System.Data.OleDb.OleDbConnection
        'Construimos y abrimos la conexión con la base de datos
        cn = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & NombreDe_MiTabla & ";User Id=admin;Password=;")
        cn.Open()

        Try
            com = New OleDbCommand("DELETE FROM PagosOnline", cn)
            com.ExecuteNonQuery()
            'Preparamos el comando
            ') VALUES(@nombre,@apellidos)"
            Dim fSql As String
            fSql = "INSERT INTO PagosOnline ( "                          '1
            fSql = fSql & " ReferenciaPrincipal,"                          '2
            fSql = fSql & " ReferenciaSecundaria,"                       '3
            fSql = fSql & " Fecha,"                               '4
            fSql = fSql & " Valortotal,"                               '5
            fSql = fSql & " NombreDelCliente)"                         '6

            fSql = fSql & " VALUES("                                '1

            fSql = fSql & " @ReferenciaPrincipal,"                          '2
            fSql = fSql & " @ReferenciaSecundaria,"                       '3
            fSql = fSql & " @Fecha,"                               '4
            fSql = fSql & " @Valortotal,"                               '5
            fSql = fSql & " @NombreDelCliente)"                    '17
            '            fSql = fSql & " @Estado1,"             '18
            '           fSql = fSql & " @Estado2)"
            cmAlta = New OleDbCommand(fSql)


            cmAlta.Parameters.Add("@ReferenciaPrincipal", OleDbType.VarChar)                               '2
            cmAlta.Parameters.Add("@ReferenciaSecundaria", OleDbType.VarChar)                            '3
            cmAlta.Parameters.Add("@Fecha", OleDbType.VarChar)                                    '4
            cmAlta.Parameters.Add("@Valortotal", OleDbType.VarChar)                                    '5
            cmAlta.Parameters.Add("@NombreDelCliente", OleDbType.VarChar)                         '17
            cmAlta.Connection = cn

            For i = 0 To dt.Tables(0).Rows.Count - 1
                cmAlta.Parameters("@ReferenciaPrincipal").Value = dt.Tables(0).Rows(i)(1).ToString()                             '2
                cmAlta.Parameters("@ReferenciaSecundaria").Value = dt.Tables(0).Rows(i)(2).ToString()                          '3
                cmAlta.Parameters("@Fecha").Value = dt.Tables(0).Rows(i)(3).ToString()                                  '4
                cmAlta.Parameters("@Valortotal").Value = dt.Tables(0).Rows(i)(4).ToString()                                  '5
                cmAlta.Parameters("@NombreDelCliente").Value = dt.Tables(0).Rows(i)(21).ToString()                       '17
                cmAlta.ExecuteNonQuery()
            Next

            'Cerramos la conexion con la base de datos
            cn.Close()
        Catch
            cn.Close()
            MsgBox(Err.Description)
        End Try

    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click

        Dim strStreamW As Stream
        Dim strStreamWriter As StreamWriter

        Dim FilePath As String = "E:\" & Date.Now.Day & Date.Now.Month & Date.Now.Year & Date.Now.Hour & Date.Now.Minute & Date.Now.Second & ".txt"
        strStreamW = File.OpenWrite(FilePath)
        strStreamWriter = New StreamWriter(strStreamW, _
                            System.Text.Encoding.Default)


        fila1 = "0108300249740000000000002" & Date.Now.Year & Ceros(Date.Now.Month, 2) & Ceros(Date.Now.Day, 2) & "    A                                                                                                                                                                                      "
        fila2 = "0500008300249740001000000000000001                                                                                                                                                                                          "
        strStreamWriter.WriteLine(fila1)
        strStreamWriter.WriteLine(fila2)

        Dim cmd As OleDbCommand
        Dim reader As OleDbDataReader = Nothing
        Dim valor As String
        Dim TOTALV As Double
        Dim TOTALR As Double
        Dim cn As System.Data.OleDb.OleDbConnection
        cn = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & My.Settings.BD & ";User Id=admin;Password=;")
        cn.Open()


        Try

            Dim sql As String = "SELECT * from Detalle"
            cmd = New OleDbCommand(sql, cn)
            reader = cmd.ExecuteReader()
            While reader.Read


                valor = ""
                valor = "06" & Ceros(reader.GetString(0).ToString, 48)
                valor = valor & Ceros(reader.GetString(1).ToString, 30) & "00   "
                valor = valor & Ceros(CInt(reader.GetString(3).ToString), 14) & "000000000000000000000000000"
                valor = valor & reader.GetString(2).ToString & "00000000                 000000000000"
                valor = valor & Microsoft.VisualBasic.Left(Espacios(reader.GetString(4).ToString, 22), 22) ' PASAR POR FUNCION QUE DEJE O COMPLETE 22
                valor = valor & "000"
                valor = Espacios(valor, 220)

                strStreamWriter.WriteLine(valor)
                TOTALR = TOTALR + 1
                TOTALV = TOTALV + CInt(reader.GetString(3).ToString)

            End While

            reader.Close()
            fila1 = "08" & Ceros(TOTALR, 9) & Ceros(TOTALV, 18) & "0000000000000000000001"
            fila1 = Espacios(fila1, 220)



            fila2 = "09" & Ceros(TOTALR, 9) & Ceros(TOTALV, 18) & "000000000000000000"
            fila2 = Espacios(fila2, 220)
            strStreamWriter.WriteLine(fila1)
            strStreamWriter.WriteLine(fila2)
            strStreamWriter.Close()
            MsgBox("El archivo se generó con éxito, ruta destino:" & FilePath)
            cn.Close()
            Me.Close()
        Catch ex As Exception
            Try
                strStreamWriter.Close()

            Catch

            End Try

            MsgBox(ex.Message)
        End Try

    End Sub
End Class
' 