﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.OpenFileDialog2 = New System.Windows.Forms.OpenFileDialog()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Cobro = New System.Windows.Forms.TextBox()
        Me.Ventas = New System.Windows.Forms.TextBox()
        Me.Cartera = New System.Windows.Forms.TextBox()
        Me.Revendedores = New System.Windows.Forms.TextBox()
        Me.Agencias = New System.Windows.Forms.TextBox()
        Me.B_Cobro = New System.Windows.Forms.Button()
        Me.B_Ventas = New System.Windows.Forms.Button()
        Me.B_Cartera = New System.Windows.Forms.Button()
        Me.B_Revendedores = New System.Windows.Forms.Button()
        Me.B_Agencias = New System.Windows.Forms.Button()
        Me.B_Validar = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.CobroHoja = New System.Windows.Forms.TextBox()
        Me.VentasHoja = New System.Windows.Forms.TextBox()
        Me.HojaCartera = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.TextBox5 = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'FolderBrowserDialog1
        '
        Me.FolderBrowserDialog1.RootFolder = System.Environment.SpecialFolder.MyDocuments
        Me.FolderBrowserDialog1.SelectedPath = "C:\"
        Me.FolderBrowserDialog1.ShowNewFolderButton = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'OpenFileDialog2
        '
        Me.OpenFileDialog2.FileName = "OpenFileDialog2"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Andalus", 16.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(3, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(175, 34)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Validar Archivos :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Andalus", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(81, 133)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(114, 26)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "* Cobro Interno"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Andalus", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(84, 173)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(66, 26)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "* Ventas"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Andalus", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(84, 211)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(147, 26)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "* Cartera no vencida"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Andalus", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(84, 250)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(111, 26)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "* Revendedores"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Andalus", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(84, 286)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(80, 26)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "* Agencias"
        '
        'Cobro
        '
        Me.Cobro.Location = New System.Drawing.Point(237, 133)
        Me.Cobro.Name = "Cobro"
        Me.Cobro.Size = New System.Drawing.Size(224, 20)
        Me.Cobro.TabIndex = 8
        '
        'Ventas
        '
        Me.Ventas.Location = New System.Drawing.Point(237, 173)
        Me.Ventas.Name = "Ventas"
        Me.Ventas.Size = New System.Drawing.Size(224, 20)
        Me.Ventas.TabIndex = 9
        '
        'Cartera
        '
        Me.Cartera.Location = New System.Drawing.Point(237, 217)
        Me.Cartera.Name = "Cartera"
        Me.Cartera.Size = New System.Drawing.Size(224, 20)
        Me.Cartera.TabIndex = 10
        '
        'Revendedores
        '
        Me.Revendedores.Location = New System.Drawing.Point(237, 256)
        Me.Revendedores.Name = "Revendedores"
        Me.Revendedores.Size = New System.Drawing.Size(224, 20)
        Me.Revendedores.TabIndex = 11
        '
        'Agencias
        '
        Me.Agencias.Location = New System.Drawing.Point(237, 292)
        Me.Agencias.Name = "Agencias"
        Me.Agencias.Size = New System.Drawing.Size(224, 20)
        Me.Agencias.TabIndex = 12
        '
        'B_Cobro
        '
        Me.B_Cobro.Location = New System.Drawing.Point(491, 136)
        Me.B_Cobro.Name = "B_Cobro"
        Me.B_Cobro.Size = New System.Drawing.Size(123, 23)
        Me.B_Cobro.TabIndex = 13
        Me.B_Cobro.Text = "Subir Archivo"
        Me.B_Cobro.UseVisualStyleBackColor = True
        '
        'B_Ventas
        '
        Me.B_Ventas.Location = New System.Drawing.Point(491, 176)
        Me.B_Ventas.Name = "B_Ventas"
        Me.B_Ventas.Size = New System.Drawing.Size(123, 23)
        Me.B_Ventas.TabIndex = 14
        Me.B_Ventas.Text = "Subir Archivo"
        Me.B_Ventas.UseVisualStyleBackColor = True
        '
        'B_Cartera
        '
        Me.B_Cartera.Location = New System.Drawing.Point(491, 218)
        Me.B_Cartera.Name = "B_Cartera"
        Me.B_Cartera.Size = New System.Drawing.Size(123, 23)
        Me.B_Cartera.TabIndex = 15
        Me.B_Cartera.Text = "Subir Archivo"
        Me.B_Cartera.UseVisualStyleBackColor = True
        '
        'B_Revendedores
        '
        Me.B_Revendedores.Location = New System.Drawing.Point(491, 254)
        Me.B_Revendedores.Name = "B_Revendedores"
        Me.B_Revendedores.Size = New System.Drawing.Size(123, 23)
        Me.B_Revendedores.TabIndex = 16
        Me.B_Revendedores.Text = "Subir Archivo"
        Me.B_Revendedores.UseVisualStyleBackColor = True
        '
        'B_Agencias
        '
        Me.B_Agencias.Location = New System.Drawing.Point(491, 290)
        Me.B_Agencias.Name = "B_Agencias"
        Me.B_Agencias.Size = New System.Drawing.Size(123, 23)
        Me.B_Agencias.TabIndex = 17
        Me.B_Agencias.Text = "Subir Archivo"
        Me.B_Agencias.UseVisualStyleBackColor = True
        '
        'B_Validar
        '
        Me.B_Validar.Location = New System.Drawing.Point(405, 361)
        Me.B_Validar.Name = "B_Validar"
        Me.B_Validar.Size = New System.Drawing.Size(126, 23)
        Me.B_Validar.TabIndex = 18
        Me.B_Validar.Text = "Validar Archivos"
        Me.B_Validar.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Andalus", 16.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(231, 72)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(91, 34)
        Me.Label7.TabIndex = 19
        Me.Label7.Text = "Archivo:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Andalus", 16.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(683, 72)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(61, 34)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "Hoja:"
        '
        'CobroHoja
        '
        Me.CobroHoja.Location = New System.Drawing.Point(689, 139)
        Me.CobroHoja.Name = "CobroHoja"
        Me.CobroHoja.Size = New System.Drawing.Size(146, 20)
        Me.CobroHoja.TabIndex = 21
        Me.CobroHoja.Text = "Hoja1"
        '
        'VentasHoja
        '
        Me.VentasHoja.Location = New System.Drawing.Point(689, 179)
        Me.VentasHoja.Name = "VentasHoja"
        Me.VentasHoja.Size = New System.Drawing.Size(146, 20)
        Me.VentasHoja.TabIndex = 22
        Me.VentasHoja.Text = "Hoja1"
        '
        'HojaCartera
        '
        Me.HojaCartera.Location = New System.Drawing.Point(689, 221)
        Me.HojaCartera.Name = "HojaCartera"
        Me.HojaCartera.Size = New System.Drawing.Size(146, 20)
        Me.HojaCartera.TabIndex = 23
        Me.HojaCartera.Text = "Hoja1"
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(689, 257)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(146, 20)
        Me.TextBox4.TabIndex = 24
        Me.TextBox4.Text = "Hoja1"
        '
        'TextBox5
        '
        Me.TextBox5.Location = New System.Drawing.Point(689, 293)
        Me.TextBox5.Name = "TextBox5"
        Me.TextBox5.Size = New System.Drawing.Size(146, 20)
        Me.TextBox5.TabIndex = 25
        Me.TextBox5.Text = "Hoja1"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(942, 411)
        Me.Controls.Add(Me.TextBox5)
        Me.Controls.Add(Me.TextBox4)
        Me.Controls.Add(Me.HojaCartera)
        Me.Controls.Add(Me.VentasHoja)
        Me.Controls.Add(Me.CobroHoja)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.B_Validar)
        Me.Controls.Add(Me.B_Agencias)
        Me.Controls.Add(Me.B_Revendedores)
        Me.Controls.Add(Me.B_Cartera)
        Me.Controls.Add(Me.B_Ventas)
        Me.Controls.Add(Me.B_Cobro)
        Me.Controls.Add(Me.Agencias)
        Me.Controls.Add(Me.Revendedores)
        Me.Controls.Add(Me.Cartera)
        Me.Controls.Add(Me.Ventas)
        Me.Controls.Add(Me.Cobro)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents OpenFileDialog2 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Cobro As System.Windows.Forms.TextBox
    Friend WithEvents Ventas As System.Windows.Forms.TextBox
    Friend WithEvents Cartera As System.Windows.Forms.TextBox
    Friend WithEvents Revendedores As System.Windows.Forms.TextBox
    Friend WithEvents Agencias As System.Windows.Forms.TextBox
    Friend WithEvents B_Cobro As System.Windows.Forms.Button
    Friend WithEvents B_Ventas As System.Windows.Forms.Button
    Friend WithEvents B_Cartera As System.Windows.Forms.Button
    Friend WithEvents B_Revendedores As System.Windows.Forms.Button
    Friend WithEvents B_Agencias As System.Windows.Forms.Button
    Friend WithEvents B_Validar As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents CobroHoja As System.Windows.Forms.TextBox
    Friend WithEvents VentasHoja As System.Windows.Forms.TextBox
    Friend WithEvents HojaCartera As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox5 As System.Windows.Forms.TextBox

End Class
