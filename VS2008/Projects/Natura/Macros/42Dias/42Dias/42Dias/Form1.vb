﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports System.Data.OleDb
Imports System.Text.RegularExpressions
Imports Microsoft.Office.Interop.Excel

Public Class Form1
    Dim dsexcel As New DataSet
    Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
    Dim MyConnection As System.Data.OleDb.OleDbConnection

    Sub ImportadelExcel(ByVal sFichero As String, ByVal DS As String, ByVal sTablaDestino As String)

        Dim sTablaOrigen As String
        Dim sConnect As String, sSQL As String
        Dim cnnActiva As ADODB.Connection

        cnnActiva = New ADODB.Connection
        cnnActiva.Open("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & DS & ";User Id=admin;Password=;")
        sTablaOrigen = "[Cobro$]"
        sConnect = "‘" & sFichero & "‘ ‘Excel 8.0;HDR=Yes;’"
        'sSQL = "SELECT * INTO " & sTablaDestino & " FROM " & sTablaOrigen & " IN " & sConnect
        sSQL = "INSERT INTO " & sTablaDestino & "  SELECT * FROM " & sTablaOrigen & " IN " & sConnect
        cnnActiva.Execute(sSQL)
        cnnActiva.Close()

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Dim folderDlg As New OpenFileDialog
        'folderDlg.s = True
        If (folderDlg.ShowDialog() = DialogResult.OK) Then
            Cobro.Text = folderDlg.FileName
            'Dim root As Environment.SpecialFolder = folderDlg.
        End If

    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub LeerXls()

        ImportadelExcel(Cartera.Text.ToString, "C:\Users\BssLtda\Desktop\ArchivosMacrosNatura\42 Dias\42Dias.mdb", "Cartera")


        Dim conexion As New System.Data.OleDb.OleDbConnection
        Dim comando As New OleDbCommand
        Dim adaptador As New OleDbDataAdapter

        '/////////////////////////////////////////////////////// VENTAS /////////////////////////////////////////////
        Dim hoja As String = VentasHoja.Text.Trim

        Dim num As Integer
        Dim extension As String = ""

        Dim ExcelPath As String = Ventas.Text.ToLower()

        num = Trim(ExcelPath).Length
        extension = Trim(ExcelPath).Substring(num - 4, 4)
        If extension = ".xls" Then
            conexion.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & ExcelPath & "; Extended Properties= ""Excel 8.0;HDR=YES; IMEX=1"""
        Else
            conexion.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & ExcelPath & ";Extended Properties=" & Chr(34) & "Excel 12.0 Xml;HDR=YES;IMEX=1" & Chr(34)
        End If

        conexion.Open()
        comando.CommandText = "SELECT * FROM [" & hoja & "$]"
        comando.Connection = conexion
        adaptador.SelectCommand = comando
        conexion.Close()
        adaptador.Fill(dsexcel, "excel")

        'quitafilasvacias(dsexcel)

        ' DataGridView1.DataSource = dsexcel.Tables(0)

        'SubirVentas("C:\Users\BssLtda\Desktop\ArchivosMacrosNatura\42 Dias\42Dias1.accdb")


        '///////////////////////////////////////////////// COBRO INTERO //////////////////////////////////////////////////////////////
        hoja = CobroHoja.Text.Trim
        extension = ""
        ExcelPath = Cobro.Text.ToLower()

        num = Trim(ExcelPath).Length
        extension = Trim(ExcelPath).Substring(num - 4, 4)
        If extension = ".xls" Then
            conexion.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & ExcelPath & "; Extended Properties= ""Excel 8.0;HDR=YES; IMEX=1"""
        Else
            conexion.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & ExcelPath & ";Extended Properties=" & Chr(34) & "Excel 12.0 Xml;HDR=YES;IMEX=1" & Chr(34)
        End If

        conexion.Open()
        comando.CommandText = "SELECT * FROM [" & hoja & "$]"
        comando.Connection = conexion
        adaptador.SelectCommand = comando
        conexion.Close()
        adaptador.Fill(dsexcel, "excel")

        'quitafilasvacias(dsexcel)

        'DataGridView1.DataSource = dsexcel.Tables(0)

        'SubirCobro("C:\Users\BssLtda\Desktop\ArchivosMacrosNatura\42 Dias\42Dias1.accdb")


        '///////////////////////////////////////////////// CARTERA NO VENCIDA  //////////////////////////////////////////////////////////////
        hoja = HojaCartera.Text.Trim
        extension = ""
        ExcelPath = Cartera.Text.ToLower()

        num = Trim(ExcelPath).Length
        extension = Trim(ExcelPath).Substring(num - 4, 4)
        If extension = ".xls" Then
            conexion.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & ExcelPath & "; Extended Properties= ""Excel 8.0;HDR=YES; IMEX=1"""
        Else
            conexion.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & ExcelPath & ";Extended Properties=" & Chr(34) & "Excel 12.0 Xml;HDR=YES;IMEX=1" & Chr(34)
        End If

        conexion.Open()
        comando.CommandText = "SELECT * FROM [" & hoja & "$]"
        comando.Connection = conexion
        adaptador.SelectCommand = comando
        conexion.Close()
        adaptador.Fill(dsexcel, "excel")

        'quitafilasvacias(dsexcel)

        'DataGridView1.DataSource = dsexcel.Tables(0)

        SubirCartera("C:\Users\BssLtda\Desktop\ArchivosMacrosNatura\42 Dias\42Dias1.accdb")

    End Sub

    Private Sub SubirVentas(ByVal NombreDe_MiTabla As String)


        Dim cmAlta As OleDbCommand
        Dim com As OleDbCommand

        Dim i As Integer
        Dim cn As System.Data.OleDb.OleDbConnection
        'Construimos y abrimos la conexión con la base de datos
        cn = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & NombreDe_MiTabla & ";User Id=admin;Password=;")
        cn.Open()


        com = New OleDbCommand("DELETE FROM VENTAS", cn)
        com.ExecuteNonQuery()
        'Preparamos el comando
        ') VALUES(@nombre,@apellidos)"
        Dim fSql As String
        fSql = "INSERT INTO Ventas ( "                          '1
        fSql = fSql & " CodigoPedido,"                          '2
        fSql = fSql & " SituaciónFiscal,"                       '3
        fSql = fSql & " Factura,"                               '4
        fSql = fSql & " Persona,"                               '5
        fSql = fSql & " NombrePersona,"                         '6
        fSql = fSql & " CodigoGrupo,"                           '7
        fSql = fSql & " OrdenDelPedido," ' orden del Pedido      8
        fSql = fSql & " Puntos,"                                '9
        fSql = fSql & " CantidadÍtems,"                         '10
        fSql = fSql & " ValorTotal,"                            '11
        fSql = fSql & " ValorTabla,"                            '12
        fSql = fSql & " ValorPracticado,"                       '13
        fSql = fSql & " ValorLíquido,"                          '14
        fSql = fSql & " ValorProductosRegulares,"               '15
        fSql = fSql & " MedioCaptación,"                        '16
        fSql = fSql & " SituaciónComercial,"                    '17
        fSql = fSql & " DetalleSituaciónComercial,"             '18
        fSql = fSql & " SituacionIntegracionExterna,"           '19
        fSql = fSql & " Fecha,"                                 '20
        fSql = fSql & " FechaMarketing,"                        '21
        fSql = fSql & " PrevisiónEntrega,"                      '22
        fSql = fSql & " FechaEntrega,"                          '23
        fSql = fSql & " FechaAutorizaciónFacturación,"          '24
        fSql = fSql & " CicloCaptación," ' Ciclo Captación      '25
        fSql = fSql & " SubCiclo,"                              '26
        fSql = fSql & " CicloMarketing,"                        '27
        fSql = fSql & " CicloIndicador,"                        '28
        fSql = fSql & " CicloAnulación,"                        '29
        fSql = fSql & " CaptacionRestrita,"                     '30
        fSql = fSql & " DíadelCiclo," 'Día del Ciclo             31
        fSql = fSql & " PlanPago,"                              '32
        fSql = fSql & " VIAPRINCIPAL, " 'VIA PRINCIPAL           33   
        fSql = fSql & " Complemento,"                           '34
        fSql = fSql & " BARRIO,"                                '35
        fSql = fSql & " MUNICIPIO,"                             '36
        fSql = fSql & " DEPARTAMIENTO,"                         '37
        fSql = fSql & " CódigoPostal,"                          '38
        fSql = fSql & " Referencia,"                            '39
        fSql = fSql & " VIAPRINCIPALEntrega,"                   '40
        fSql = fSql & " ComplementoEntrega,"                    '41
        fSql = fSql & " BARRIOEntrega,"                         '42
        fSql = fSql & " MUNICIPIOEntrega,"                      '43
        fSql = fSql & " DEPARTAMIENTOEntrega,"                  '44
        fSql = fSql & " CódigoPostalEntrega,"                   '45
        fSql = fSql & " ReferenciaEntrega,"                     '46
        fSql = fSql & " Teléfono,"                              '47
        fSql = fSql & " CódModeloComercial,"                    '48
        fSql = fSql & " ModeloComercial,"                       '49
        fSql = fSql & " CódEstructuraPadre," 'Cód EstructuraPadre50
        fSql = fSql & " EstructuraPadre,"                       '51
        fSql = fSql & " CódEstructura,"                         '52
        fSql = fSql & " Estructura,"                            '53
        fSql = fSql & " ResponsableEstructura,"                '54
        fSql = fSql & " TeléfonoResponsable,"                   '55
        fSql = fSql & " CódUsuarioCreación,"                    '56 
        fSql = fSql & " UsuariodeCreación," 'Usuario de Creación 57   
        fSql = fSql & " CódUsuariodeFinalización,"              '58    
        fSql = fSql & " UsuariodeFinalización,"                 '59
        fSql = fSql & " CodTransportadora,"                     '60
        fSql = fSql & " Transportadora,"                        '61
        fSql = fSql & " CódigoAgencia,"                         '62
        fSql = fSql & " Agencia,"                               '63
        fSql = fSql & " Volumen,"                               '64
        fSql = fSql & " PesoEstimado,"                          '65
        fSql = fSql & " PesoReal,"                              '66
        fSql = fSql & " LoteSeparación,"                        '67
        fSql = fSql & " Minuta,"                                '68
        fSql = fSql & " PedidoConsolidado)"                     '69

        fSql = fSql & " VALUES("                                '1

        fSql = fSql & " @CodigoPedido,"                         '2
        fSql = fSql & " @SituaciónFiscal,"                      '3
        fSql = fSql & " @Factura,"                              '4
        fSql = fSql & " @Persona,"                              '5
        fSql = fSql & " @NombrePersona,"                        '6
        fSql = fSql & " @CodigoGrupo,"                          '7
        fSql = fSql & " @OrdenDelPedido," ' orden del Pedido    '8
        fSql = fSql & " @Puntos,"                               '9
        fSql = fSql & " @CantidadÍtems,"                        '10
        fSql = fSql & " @ValorTotal,"                           '11
        fSql = fSql & " @ValorTabla,"                           '12
        fSql = fSql & " @ValorPracticado,"                      '13
        fSql = fSql & " @ValorLíquido,"                         '14
        fSql = fSql & " @ValorProductosRegulares,"              '15
        fSql = fSql & " @MedioCaptación,"                       '16
        fSql = fSql & " @SituaciónComercial,"                   '17
        fSql = fSql & " @DetalleSituaciónComercial,"            '18
        fSql = fSql & " @SituacionIntegracionExterna,"          '19
        fSql = fSql & " @Fecha,"                                '20
        fSql = fSql & " @FechaMarketing,"                       '21
        fSql = fSql & " @PrevisiónEntrega,"                     '22
        fSql = fSql & " @FechaEntrega,"                         '23
        fSql = fSql & " @FechaAutorizaciónFacturación,"         '24
        fSql = fSql & " @CicloCaptación," ' Ciclo Captación     '25
        fSql = fSql & " @SubCiclo,"                             '26
        fSql = fSql & " @CicloMarketing,"                       '27
        fSql = fSql & " @CicloIndicador,"                       '28
        fSql = fSql & " @CicloAnulación,"                       '29
        fSql = fSql & " @CaptacionRestrita,"                    '30
        fSql = fSql & " @DíadelCiclo," 'Día del Ciclo           '31
        fSql = fSql & " @PlanPago,"                             '32
        fSql = fSql & " @VIAPRINCIPAL, " 'VIA PRINCIPAL         '33
        fSql = fSql & " @Complemento,"                          '34
        fSql = fSql & " @BARRIO,"                               '35
        fSql = fSql & " @MUNICIPIO,"                            '36
        fSql = fSql & " @DEPARTAMIENTO,"                        '37
        fSql = fSql & " @CódigoPostal,"                         '38
        fSql = fSql & " @Referencia,"                           '39
        fSql = fSql & " @VIAPRINCIPALEntrega,"                  '40 
        fSql = fSql & " @ComplementoEntrega,"                   '41
        fSql = fSql & " @BARRIOEntrega,"                        '42
        fSql = fSql & " @MUNICIPIOEntrega,"                     '43
        fSql = fSql & " @DEPARTAMIENTOEntrega,"                 '44
        fSql = fSql & " @CódigoPostalEntrega,"                  '45
        fSql = fSql & " @ReferenciaEntrega,"                    '46
        fSql = fSql & " @Teléfono,"                             '47
        fSql = fSql & " @CódModeloComercial,"                   '48
        fSql = fSql & " @ModeloComercial,"                      '49
        fSql = fSql & " @CódEstructuraPadre,"                   '50
        fSql = fSql & " @EstructuraPadre,"                      '51
        fSql = fSql & " @CódEstructura,"                        '52
        fSql = fSql & " @Estructura,"                           '53
        fSql = fSql & " @ResponsableEstructura, "               '54
        fSql = fSql & " @TeléfonoResponsable,"                  '55
        fSql = fSql & " @CódUsuarioCreación,"                   '56
        fSql = fSql & " @UsuariodeCreación,"                    '57
        fSql = fSql & " @CódUsuariodeFinalización,"             '58
        fSql = fSql & " @UsuariodeFinalización,"                '59
        fSql = fSql & " @CodTransportadora,"                    '60
        fSql = fSql & " @Transportadora,"                       '61
        fSql = fSql & " @CódigoAgencia,"                        '62
        fSql = fSql & " @Agencia,"                              '63
        fSql = fSql & " @Volumen,"                              '64
        fSql = fSql & " @PesoEstimado,"                         '65
        fSql = fSql & " @PesoReal,"                             '66
        fSql = fSql & " @LoteSeparación,"                       '67
        fSql = fSql & " @Minuta,"                               '68
        fSql = fSql & " @PedidoConsolidado )"                    '69
        cmAlta = New OleDbCommand(fSql)


        'cmAlta.Parameters.Add("@nombre", OleDbType.VarChar)
        'cmAlta.Parameters.Add("@apellidos", OleDbType.VarChar)
        cmAlta.Parameters.Add("@CodigoPedido", OleDbType.Numeric)                                   '2
        cmAlta.Parameters.Add("@SituaciónFiscal", OleDbType.VarChar)                                '3
        cmAlta.Parameters.Add("@Factura", OleDbType.VarChar)                                        '4
        cmAlta.Parameters.Add("@Persona", OleDbType.Numeric)                                        '5
        cmAlta.Parameters.Add("@NombrePersona", OleDbType.VarChar)                                  '6
        cmAlta.Parameters.Add("@CodigoGrupo", OleDbType.VarChar)                                    '7
        cmAlta.Parameters.Add("@OrdenDelPedido", OleDbType.VarChar) ' orden del Pedido              '8
        cmAlta.Parameters.Add("@Puntos", OleDbType.Numeric)                                         '9
        cmAlta.Parameters.Add("@CantidadÍtems", OleDbType.Numeric)                                  '10 
        cmAlta.Parameters.Add("@ValorTotal", OleDbType.Numeric)                                     '11
        cmAlta.Parameters.Add("@ValorTabla", OleDbType.Numeric)                                     '12
        cmAlta.Parameters.Add("@ValorPracticado", OleDbType.Numeric)                                '13
        cmAlta.Parameters.Add("@ValorLíquido", OleDbType.Numeric)                                   '14
        cmAlta.Parameters.Add("@ValorProductosRegulares", OleDbType.Numeric)                        '15
        cmAlta.Parameters.Add("@MedioCaptación", OleDbType.VarChar)                                 '16
        cmAlta.Parameters.Add("@SituaciónComercial", OleDbType.VarChar)                             '17
        cmAlta.Parameters.Add("@DetalleSituaciónComercial", OleDbType.VarChar)                      '18
        cmAlta.Parameters.Add("@SituacionIntegracionExterna", OleDbType.VarChar)                    '19
        cmAlta.Parameters.Add("@Fecha", OleDbType.VarChar)                                          '20
        cmAlta.Parameters.Add("@FechaMarketing", OleDbType.VarChar)                                 '21
        cmAlta.Parameters.Add("@PrevisiónEntrega", OleDbType.VarChar)                               '22
        cmAlta.Parameters.Add("@FechaEntrega", OleDbType.VarChar)                                   '23
        cmAlta.Parameters.Add("@FechaAutorizaciónFacturación", OleDbType.VarChar)                   '24
        cmAlta.Parameters.Add("@CicloCaptación", OleDbType.VarChar) ' Ciclo Captación               '25
        cmAlta.Parameters.Add("@SubCiclo", OleDbType.VarChar)                                       '26
        cmAlta.Parameters.Add("@CicloMarketing", OleDbType.VarChar)                                 '27
        cmAlta.Parameters.Add("@CicloIndicador", OleDbType.VarChar)                                 '28
        cmAlta.Parameters.Add("@CicloAnulación", OleDbType.VarChar)                                 '29
        cmAlta.Parameters.Add("@CaptacionRestrita", OleDbType.VarChar)                              '30
        cmAlta.Parameters.Add("@DíadelCiclo", OleDbType.VarChar) 'Día del Ciclo                     '31
        cmAlta.Parameters.Add("@PlanPago", OleDbType.VarChar)                                       '32
        cmAlta.Parameters.Add("@VIAPRINCIPAL", OleDbType.VarChar) 'VIA PRINCIPAL                    '33
        cmAlta.Parameters.Add("@Complemento", OleDbType.VarChar)                                    '34
        cmAlta.Parameters.Add("@BARRIO", OleDbType.VarChar)                                         '35
        cmAlta.Parameters.Add("@MUNICIPIO", OleDbType.VarChar)                                      '36
        cmAlta.Parameters.Add("@DEPARTAMIENTO", OleDbType.VarChar)                                  '37
        cmAlta.Parameters.Add("@CódigoPostal", OleDbType.VarChar)                                   '38
        cmAlta.Parameters.Add("@Referencia", OleDbType.VarChar)                                     '39
        cmAlta.Parameters.Add("@VIAPRINCIPALEntrega", OleDbType.VarChar)                            '40
        cmAlta.Parameters.Add("@ComplementoEntrega", OleDbType.VarChar)                             '41
        cmAlta.Parameters.Add("@BARRIOEntrega", OleDbType.VarChar)                                  '42
        cmAlta.Parameters.Add("@MUNICIPIOEntrega", OleDbType.VarChar)                               '43
        cmAlta.Parameters.Add("@DEPARTAMIENTOEntrega", OleDbType.VarChar)                           '44
        cmAlta.Parameters.Add("@CódigoPostalEntrega", OleDbType.VarChar)                            '45
        cmAlta.Parameters.Add("@ReferenciaEntrega", OleDbType.VarChar)                              '46 
        cmAlta.Parameters.Add("@Teléfono", OleDbType.VarChar)                                       '47
        cmAlta.Parameters.Add("@CódModeloComercial", OleDbType.VarChar)                             '48
        cmAlta.Parameters.Add("@ModeloComercial", OleDbType.VarChar)                                '49
        cmAlta.Parameters.Add("@CódEstructuraPadre", OleDbType.VarChar) 'Cód EstructuraPadre        '50
        cmAlta.Parameters.Add("@EstructuraPadre", OleDbType.VarChar)                                '51
        cmAlta.Parameters.Add("@CódEstructura", OleDbType.Numeric)                                  '52
        cmAlta.Parameters.Add("@Estructura", OleDbType.VarChar)                                     '53
        cmAlta.Parameters.Add("@ResponsableEstructura", OleDbType.VarChar)                          '54
        cmAlta.Parameters.Add("@TeléfonoResponsable", OleDbType.VarChar)                            '55
        cmAlta.Parameters.Add("@CódUsuarioCreación", OleDbType.VarChar) ' Cód Usuario Creación      '56
        cmAlta.Parameters.Add("@UsuariodeCreación", OleDbType.VarChar) 'Usuario de Creación         '57
        cmAlta.Parameters.Add("@CódUsuariodeFinalización", OleDbType.VarChar)                       '58
        cmAlta.Parameters.Add("@UsuariodeFinalización", OleDbType.VarChar)                          '59
        cmAlta.Parameters.Add("@CodTransportadora", OleDbType.Numeric)                              '60
        cmAlta.Parameters.Add("@Transportadora", OleDbType.VarChar)                                 '61
        cmAlta.Parameters.Add("@CódigoAgencia", OleDbType.VarChar)                                  '62
        cmAlta.Parameters.Add("@Agencia", OleDbType.VarChar)                                        '63
        cmAlta.Parameters.Add("@Volumen", OleDbType.Numeric)                                        '64
        cmAlta.Parameters.Add("@PesoEstimado", OleDbType.VarChar)                                   '65
        cmAlta.Parameters.Add("@PesoReal", OleDbType.VarChar)                                       '66
        cmAlta.Parameters.Add("@LoteSeparación", OleDbType.VarChar)                                 '67
        cmAlta.Parameters.Add("@Minuta", OleDbType.VarChar)                                         '68
        cmAlta.Parameters.Add("@PedidoConsolidado", OleDbType.VarChar)                              '69

        cmAlta.Connection = cn
        For i = 0 To dsexcel.Tables(0).Rows.Count - 1
            'rellenamos el valor de los parámetros
            'cmAlta.Parameters("@nombre").Value = dsexcel.Tables(0).Rows(i)("nombre").ToString()
            'cmAlta.Parameters("@apellidos").Value = dsexcel.Tables(0).Rows(i)("apellidos").ToString()
            cmAlta.Parameters("@CodigoPedido").Value = System.Convert.ToDecimal(dsexcel.Tables(0).Rows(i)(0).ToString())
            cmAlta.Parameters("@SituaciónFiscal").Value = dsexcel.Tables(0).Rows(i)(1).ToString()
            cmAlta.Parameters("@Factura").Value = dsexcel.Tables(0).Rows(i)(2).ToString()
            cmAlta.Parameters("@Persona").Value = System.Convert.ToDecimal(dsexcel.Tables(0).Rows(i)(3).ToString())
            cmAlta.Parameters("@NombrePersona").Value = dsexcel.Tables(0).Rows(i)(4).ToString()
            cmAlta.Parameters("@CodigoGrupo").Value = dsexcel.Tables(0).Rows(i)(5).ToString()
            cmAlta.Parameters("@OrdenDelPedido").Value = dsexcel.Tables(0).Rows(i)(6).ToString() ' orden del Pedido
            cmAlta.Parameters("@Puntos").Value = System.Convert.ToDecimal(dsexcel.Tables(0).Rows(i)(7).ToString())
            cmAlta.Parameters("@CantidadÍtems").Value = System.Convert.ToDecimal(dsexcel.Tables(0).Rows(i)(8).ToString())
            cmAlta.Parameters("@ValorTotal").Value = System.Convert.ToDecimal(dsexcel.Tables(0).Rows(i)(9).ToString())
            cmAlta.Parameters("@ValorTabla").Value = System.Convert.ToDecimal(dsexcel.Tables(0).Rows(i)(10).ToString())
            cmAlta.Parameters("@ValorPracticado").Value = System.Convert.ToDecimal(dsexcel.Tables(0).Rows(i)(11).ToString())
            cmAlta.Parameters("@ValorLíquido").Value = System.Convert.ToDecimal(dsexcel.Tables(0).Rows(i)(12).ToString())
            cmAlta.Parameters("@ValorProductosRegulares").Value = System.Convert.ToDecimal(dsexcel.Tables(0).Rows(i)(13).ToString())
            cmAlta.Parameters("@MedioCaptación").Value = dsexcel.Tables(0).Rows(i)(14).ToString()
            cmAlta.Parameters("@SituaciónComercial").Value = dsexcel.Tables(0).Rows(i)(15).ToString()
            cmAlta.Parameters("@DetalleSituaciónComercial").Value = dsexcel.Tables(0).Rows(i)(16).ToString()
            cmAlta.Parameters("@SituacionIntegracionExterna").Value = dsexcel.Tables(0).Rows(i)(17).ToString()
            cmAlta.Parameters("@Fecha").Value = dsexcel.Tables(0).Rows(i)(18).ToString()
            cmAlta.Parameters("@FechaMarketing").Value = dsexcel.Tables(0).Rows(i)(19).ToString()
            cmAlta.Parameters("@PrevisiónEntrega").Value = dsexcel.Tables(0).Rows(i)(20).ToString()
            cmAlta.Parameters("@FechaEntrega").Value = dsexcel.Tables(0).Rows(i)(21).ToString()
            cmAlta.Parameters("@FechaAutorizaciónFacturación").Value = dsexcel.Tables(0).Rows(i)(22).ToString()
            cmAlta.Parameters("@CicloCaptación").Value = dsexcel.Tables(0).Rows(i)(23).ToString() ' Ciclo Captación
            cmAlta.Parameters("@SubCiclo").Value = dsexcel.Tables(0).Rows(i)(24).ToString()
            cmAlta.Parameters("@CicloMarketing").Value = dsexcel.Tables(0).Rows(i)(25).ToString()
            cmAlta.Parameters("@CicloIndicador").Value = dsexcel.Tables(0).Rows(i)(26).ToString()
            cmAlta.Parameters("@CicloAnulación").Value = dsexcel.Tables(0).Rows(i)(27).ToString()
            cmAlta.Parameters("@CaptacionRestrita").Value = dsexcel.Tables(0).Rows(i)(28).ToString()
            cmAlta.Parameters("@DíadelCiclo").Value = dsexcel.Tables(0).Rows(i)(29).ToString() 'Día del Ciclo
            cmAlta.Parameters("@PlanPago").Value = dsexcel.Tables(0).Rows(i)(30).ToString()
            cmAlta.Parameters("@VIAPRINCIPAL").Value = dsexcel.Tables(0).Rows(i)(31).ToString() 'VIA PRINCIPAL
            cmAlta.Parameters("@Complemento").Value = dsexcel.Tables(0).Rows(i)(32).ToString()
            cmAlta.Parameters("@BARRIO").Value = dsexcel.Tables(0).Rows(i)(33).ToString()
            cmAlta.Parameters("@MUNICIPIO").Value = dsexcel.Tables(0).Rows(i)(34).ToString()
            cmAlta.Parameters("@DEPARTAMIENTO").Value = dsexcel.Tables(0).Rows(i)(35).ToString()
            cmAlta.Parameters("@CódigoPostal").Value = dsexcel.Tables(0).Rows(i)(36).ToString()
            cmAlta.Parameters("@Referencia").Value = dsexcel.Tables(0).Rows(i)(37).ToString()
            cmAlta.Parameters("@VIAPRINCIPALEntrega").Value = dsexcel.Tables(0).Rows(i)(38).ToString()
            cmAlta.Parameters("@ComplementoEntrega").Value = dsexcel.Tables(0).Rows(i)(39).ToString()
            cmAlta.Parameters("@BARRIOEntrega").Value = dsexcel.Tables(0).Rows(i)(40).ToString()
            cmAlta.Parameters("@MUNICIPIOEntrega").Value = dsexcel.Tables(0).Rows(i)(41).ToString()
            cmAlta.Parameters("@DEPARTAMIENTOEntrega").Value = dsexcel.Tables(0).Rows(i)(42).ToString()
            cmAlta.Parameters("@CódigoPostalEntrega").Value = dsexcel.Tables(0).Rows(i)(43).ToString()
            cmAlta.Parameters("@ReferenciaEntrega").Value = dsexcel.Tables(0).Rows(i)(44).ToString()

            cmAlta.Parameters("@Teléfono").Value = dsexcel.Tables(0).Rows(i)(45).ToString()
            cmAlta.Parameters("@CódModeloComercial").Value = dsexcel.Tables(0).Rows(i)(46).ToString()
            cmAlta.Parameters("@ModeloComercial").Value = dsexcel.Tables(0).Rows(i)(47).ToString()
            cmAlta.Parameters("@CódEstructuraPadre").Value = dsexcel.Tables(0).Rows(i)(48).ToString() 'Cód EstructuraPadre
            cmAlta.Parameters("@EstructuraPadre").Value = dsexcel.Tables(0).Rows(i)(49).ToString()
            cmAlta.Parameters("@CódEstructura").Value = System.Convert.ToDecimal(dsexcel.Tables(0).Rows(i)(50).ToString())
            cmAlta.Parameters("@Estructura").Value = dsexcel.Tables(0).Rows(i)(51).ToString()
            cmAlta.Parameters("@ResponsableEstructura").Value = dsexcel.Tables(0).Rows(i)(52).ToString()
            cmAlta.Parameters("@TeléfonoResponsable").Value = dsexcel.Tables(0).Rows(i)(53).ToString()
            cmAlta.Parameters("@CódUsuarioCreación").Value = dsexcel.Tables(0).Rows(i)(54).ToString() ' Cód Usuario Creación
            cmAlta.Parameters("@UsuariodeCreación").Value = dsexcel.Tables(0).Rows(i)(55).ToString() 'Usuario de Creación
            cmAlta.Parameters("@CódUsuariodeFinalización").Value = dsexcel.Tables(0).Rows(i)(56).ToString()
            cmAlta.Parameters("@UsuariodeFinalización").Value = dsexcel.Tables(0).Rows(i)(57).ToString()
            cmAlta.Parameters("@CodTransportadora").Value = System.Convert.ToDecimal(dsexcel.Tables(0).Rows(i)(58).ToString())
            cmAlta.Parameters("@Transportadora").Value = dsexcel.Tables(0).Rows(i)(59).ToString()
            cmAlta.Parameters("@CódigoAgencia").Value = dsexcel.Tables(0).Rows(i)(60).ToString()
            cmAlta.Parameters("@Agencia").Value = dsexcel.Tables(0).Rows(i)(61).ToString()
            cmAlta.Parameters("@Volumen").Value = System.Convert.ToDecimal(dsexcel.Tables(0).Rows(i)(62).ToString())
            cmAlta.Parameters("@PesoEstimado").Value = dsexcel.Tables(0).Rows(i)(63).ToString()
            cmAlta.Parameters("@PesoReal").Value = dsexcel.Tables(0).Rows(i)(64).ToString()
            cmAlta.Parameters("@LoteSeparación").Value = dsexcel.Tables(0).Rows(i)(65).ToString()
            cmAlta.Parameters("@Minuta").Value = dsexcel.Tables(0).Rows(i)(66).ToString()
            cmAlta.Parameters("@PedidoConsolidado").Value = dsexcel.Tables(0).Rows(i)(67).ToString()
            'realizamos el alta
            cmAlta.ExecuteNonQuery()
        Next

        'Cerramos la conexion con la base de datos
        cn.Close()

    End Sub

    Private Sub SubirCartera(ByVal NombreDe_MiTabla As String)

        Dim cmAlta As OleDbCommand
        Dim com As OleDbCommand
        Dim i As Integer
        Dim cn As System.Data.OleDb.OleDbConnection

        'Construimos y abrimos la conexión con la base de datos
        cn = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & NombreDe_MiTabla & ";User Id=admin;Password=;")
        cn.Open()

        com = New OleDbCommand("DELETE FROM CARTERA", cn)
        com.ExecuteNonQuery()
        'Preparamos el comando
        ') VALUES(@nombre,@apellidos)"
        Dim fSql As String
        fSql = "INSERT INTO Cartera ( "                               '1
        fSql = fSql & " Título,"                             '2
        fSql = fSql & " NúmeroPedido,"                       '3
        fSql = fSql & " CicloCaptacion,"                               '4
        fSql = fSql & " CicloIndicador,"                               '5
        fSql = fSql & " Grupo,"                         '6
        fSql = fSql & " NúmeroFactura,"                           '7
        fSql = fSql & " FechaPedido," ' orden del Pedido      8
        fSql = fSql & " FechaVencimientoOriginal,"                                '9
        fSql = fSql & " FechaVencimiento,"                         '10
        fSql = fSql & " ValorTítulo,"                            '11
        fSql = fSql & " Saldo_Principal,"                            '12
        fSql = fSql & " Saldo_Financiero,"                       '13
        fSql = fSql & " Saldo_Total,"                          '14
        fSql = fSql & " Saldo_actualizado,"               '15
        fSql = fSql & " Situación,"                        '16
        fSql = fSql & " Control_de_Envío_la_Pérdida,"                    '17
        fSql = fSql & " Fecha_de_Envío_la_Pérdida,"             '18
        fSql = fSql & " FechaSaldo,"           '19
        fSql = fSql & " Data_Saldo_Corregido,"                                 '20
        fSql = fSql & " FechaLiquidación,"                        '21
        fSql = fSql & " EntidadConciliación,"                      '22
        fSql = fSql & " Negociación,"                          '23
        fSql = fSql & " Fase_del_Cobro,"          '24
        fSql = fSql & " Cód_Estructura_Padre," ' Ciclo Captación      '25
        fSql = fSql & " Estructura_Padre,"                              '26
        fSql = fSql & " Cód_Estructura,"                        '27
        fSql = fSql & " Estructura,"                        '28
        fSql = fSql & " CódigoPersona,"                        '29
        fSql = fSql & " Nombre,"                     '30
        fSql = fSql & " Dirección," 'Día del Ciclo             31
        fSql = fSql & " Teléfono_de_contacto,"                              '32
        fSql = fSql & " Entidad_de_cobro, " 'VIA PRINCIPAL           33   
        fSql = fSql & " CodPlanRecibimiento,"                           '34
        fSql = fSql & " PlanRecibimiento,"                                '35
        fSql = fSql & " CodModeloComercial,"                             '36
        fSql = fSql & " ModeloComercial,"                         '37
        fSql = fSql & " Contestado,"                          '38
        fSql = fSql & " NSU)"                            '39


        fSql = fSql & " VALUES("                                '1

        fSql = fSql & " @Título,"                             '2
        fSql = fSql & " @NúmeroPedido,"                       '3
        fSql = fSql & " @CicloCaptacion,"                               '4
        fSql = fSql & " @CicloIndicador,"                               '5
        fSql = fSql & " @Grupo,"                         '6
        fSql = fSql & " @NúmeroFactura,"                           '7
        fSql = fSql & " @FechaPedido," ' orden del Pedido      8
        fSql = fSql & " @FechaVencimientoOriginal,"                                '9
        fSql = fSql & " @FechaVencimiento,"                         '10
        fSql = fSql & " @ValorTítulo,"                            '11
        fSql = fSql & " @Saldo_Principal,"                            '12
        fSql = fSql & " @Saldo_Financiero,"                       '13
        fSql = fSql & " @Saldo_Total,"                          '14
        fSql = fSql & " @Saldo_actualizado,"               '15
        fSql = fSql & " @Situación,"                        '16
        fSql = fSql & " @Control_de_Envío_la_Pérdida,"                    '17
        fSql = fSql & " @Fecha_de_Envío_la_Pérdida,"             '18
        fSql = fSql & " @FechaSaldo,"           '19
        fSql = fSql & " @Data_Saldo_Corregido,"                                 '20
        fSql = fSql & " @FechaLiquidación,"                        '21
        fSql = fSql & " @EntidadConciliación,"                      '22
        fSql = fSql & " @Negociación,"                          '23
        fSql = fSql & " @Fase_del_Cobro,"          '24
        fSql = fSql & " @Cód_Estructura_Padre," ' Ciclo Captación      '25
        fSql = fSql & " @Estructura_Padre,"                              '26
        fSql = fSql & " @Cód_Estructura,"                        '27
        fSql = fSql & " @Estructura,"                        '28
        fSql = fSql & " @CódigoPersona,"                        '29
        fSql = fSql & " @Nombre,"                     '30
        fSql = fSql & " @Dirección," 'Día del Ciclo             31
        fSql = fSql & " @Teléfono_de_contacto,"                              '32
        fSql = fSql & " @Entidad_de_cobro, " 'VIA PRINCIPAL           33   
        fSql = fSql & " @CodPlanRecibimiento,"                           '34
        fSql = fSql & " @PlanRecibimiento,"                                '35
        fSql = fSql & " @CodModeloComercial,"                             '36
        fSql = fSql & " @ModeloComercial,"                         '37
        fSql = fSql & " @Contestado,"                          '38
        fSql = fSql & " @NSU)"                            '39
        cmAlta = New OleDbCommand(fSql)




        cmAlta.Parameters.Add("  @Título", OleDbType.VarChar)                             '2
        cmAlta.Parameters.Add("  @NúmeroPedido", OleDbType.VarChar)                       '3
        cmAlta.Parameters.Add("  @CicloCaptacion", OleDbType.VarChar)                               '4
        cmAlta.Parameters.Add("  @CicloIndicador", OleDbType.VarChar)                               '5
        cmAlta.Parameters.Add("  @Grupo", OleDbType.VarChar)                         '6
        cmAlta.Parameters.Add("  @NúmeroFactura", OleDbType.VarChar)                           '7
        cmAlta.Parameters.Add("  @FechaPedido", OleDbType.VarChar) ' orden del Pedido      8
        cmAlta.Parameters.Add("  @FechaVencimientoOriginal", OleDbType.VarChar)                                '9
        cmAlta.Parameters.Add("  @FechaVencimiento", OleDbType.VarChar)                         '10
        cmAlta.Parameters.Add("  @ValorTítulo", OleDbType.VarChar)                            '11
        cmAlta.Parameters.Add("  @Saldo_Principal", OleDbType.VarChar)                            '12
        cmAlta.Parameters.Add("  @Saldo_Financiero", OleDbType.VarChar)                       '13
        cmAlta.Parameters.Add("  @Saldo_Total", OleDbType.VarChar)                          '14
        cmAlta.Parameters.Add("  @Saldo_actualizado", OleDbType.VarChar)               '15
        cmAlta.Parameters.Add("  @Situación", OleDbType.VarChar)                        '16
        cmAlta.Parameters.Add("  @Control_de_Envío_la_Pérdida", OleDbType.VarChar)                    '17
        cmAlta.Parameters.Add("  @Fecha_de_Envío_la_Pérdida", OleDbType.VarChar)             '18
        cmAlta.Parameters.Add("  @FechaSaldo", OleDbType.VarChar)           '19
        cmAlta.Parameters.Add("  @Data_Saldo_Corregido", OleDbType.VarChar)                                 '20
        cmAlta.Parameters.Add("  @FechaLiquidación", OleDbType.VarChar)                        '21
        cmAlta.Parameters.Add("  @EntidadConciliación", OleDbType.VarChar)                      '22
        cmAlta.Parameters.Add("  @Negociación", OleDbType.VarChar)                          '23
        cmAlta.Parameters.Add("  @Fase_del_Cobro", OleDbType.VarChar)          '24
        cmAlta.Parameters.Add("  @Cód_Estructura_Padre", OleDbType.VarChar) ' Ciclo Captación      '25
        cmAlta.Parameters.Add("  @Estructura_Padre", OleDbType.VarChar)                              '26
        cmAlta.Parameters.Add("  @Cód_Estructura", OleDbType.VarChar)                        '27
        cmAlta.Parameters.Add("  @Estructura", OleDbType.VarChar)                        '28
        cmAlta.Parameters.Add("  @CódigoPersona", OleDbType.VarChar)                        '29
        cmAlta.Parameters.Add("  @Nombre", OleDbType.VarChar)                     '30
        cmAlta.Parameters.Add("  @Dirección", OleDbType.VarChar) 'Día del Ciclo             31
        cmAlta.Parameters.Add("  @Teléfono_de_contacto", OleDbType.VarChar)                              '32
        cmAlta.Parameters.Add("  @Entidad_de_cobro", OleDbType.VarChar)
        cmAlta.Parameters.Add("  @CodPlanRecibimiento", OleDbType.VarChar)                           '34
        cmAlta.Parameters.Add("  @PlanRecibimiento", OleDbType.VarChar)                                '35
        cmAlta.Parameters.Add("  @CodModeloComercial", OleDbType.VarChar)                             '36
        cmAlta.Parameters.Add("  @ModeloComercial", OleDbType.VarChar)                         '37
        cmAlta.Parameters.Add("  @Contestado", OleDbType.VarChar)                          '38
        cmAlta.Parameters.Add("  @NSU", OleDbType.VarChar)                             '39
        cmAlta.Connection = cn


        For i = 0 To dsexcel.Tables(0).Rows.Count - 1
            cmAlta.Parameters("  @Título").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                           '2
            cmAlta.Parameters("  @NúmeroPedido").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                     '3
            cmAlta.Parameters("  @CicloCaptacion").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                             '4
            cmAlta.Parameters("  @CicloIndicador").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                             '5
            cmAlta.Parameters("  @Grupo").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                       '6
            cmAlta.Parameters("  @NúmeroFactura").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                         '7
            cmAlta.Parameters("  @FechaPedido").Value = dsexcel.Tables(0).Rows(i)(0).ToString()
            cmAlta.Parameters("  @FechaVencimientoOriginal").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                              '9
            cmAlta.Parameters("  @FechaVencimiento").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                       '10
            cmAlta.Parameters("  @ValorTítulo").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                          '11
            cmAlta.Parameters("  @Saldo_Principal").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                          '12
            cmAlta.Parameters("  @Saldo_Financiero").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                     '13
            cmAlta.Parameters("  @Saldo_Total").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                        '14
            cmAlta.Parameters("  @Saldo_actualizado").Value = dsexcel.Tables(0).Rows(i)(0).ToString()             '15
            cmAlta.Parameters("  @Situación").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                      '16
            cmAlta.Parameters("  @Control_de_Envío_la_Pérdida").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                  '17
            cmAlta.Parameters("  @Fecha_de_Envío_la_Pérdida").Value = dsexcel.Tables(0).Rows(i)(0).ToString()           '18
            cmAlta.Parameters("  @FechaSaldo").Value = dsexcel.Tables(0).Rows(i)(0).ToString()         '19
            cmAlta.Parameters("  @Data_Saldo_Corregido").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                               '20
            cmAlta.Parameters("  @FechaLiquidación").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                      '21
            cmAlta.Parameters("  @EntidadConciliación").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                    '22
            cmAlta.Parameters("  @Negociación").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                        '23
            cmAlta.Parameters("  @Fase_del_Cobro").Value = dsexcel.Tables(0).Rows(i)(0).ToString()        '24
            cmAlta.Parameters("  @Cód_Estructura_Padre").Value = dsexcel.Tables(0).Rows(i)(0).ToString() ' Ciclo Captación      '25
            cmAlta.Parameters("  @Estructura_Padre").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                            '26
            cmAlta.Parameters("  @Cód_Estructura").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                      '27
            cmAlta.Parameters("  @Estructura").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                      '28
            cmAlta.Parameters("  @CódigoPersona").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                      '29
            cmAlta.Parameters("  @Nombre").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                   '30
            cmAlta.Parameters("  @Dirección").Value = dsexcel.Tables(0).Rows(i)(0).ToString() 'Día del Ciclo             31
            cmAlta.Parameters("  @Teléfono_de_contacto").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                            '32
            cmAlta.Parameters("  @Entidad_de_cobro").Value = dsexcel.Tables(0).Rows(i)(0).ToString()
            cmAlta.Parameters("  @CodPlanRecibimiento").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                         '34
            cmAlta.Parameters("  @PlanRecibimiento").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                              '35
            cmAlta.Parameters("  @CodModeloComercial").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                           '36
            cmAlta.Parameters("  @ModeloComercial").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                       '37
            cmAlta.Parameters("  @Contestado").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                        '38
            cmAlta.Parameters("  @NSU").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                            '39            'realizamos el alta
            cmAlta.ExecuteNonQuery()
        Next

        'Cerramos la conexion con la base de datos
        cn.Close()
    End Sub

    Private Sub SubirCobro(ByVal NombreDe_MiTabla As String)


        Dim cmAlta As OleDbCommand
        Dim com As OleDbCommand

        Dim i As Integer
        Dim cn As System.Data.OleDb.OleDbConnection
        'Construimos y abrimos la conexión con la base de datos
        cn = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & NombreDe_MiTabla & ";User Id=admin;Password=;")
        cn.Open()


        com = New OleDbCommand("DELETE FROM Cobro", cn)
        com.ExecuteNonQuery()
        'Preparamos el comando
        ') VALUES(@nombre,@apellidos)"
        Dim fSql As String
        fSql = "INSERT INTO COBRO ( "                          '1
        fSql = fSql & " PRIMER_PEDIDO,"                          '2
        fSql = fSql & " FECHA_PRIMER_PEDIDO,"                       '3
        fSql = fSql & " CODIGO_PEDIDO,"                               '4
        fSql = fSql & " FACTURA,"                               '5
        fSql = fSql & " PERFIL_CONSULTOR_CARTERA,"                         '6
        fSql = fSql & " CODIGO,"                           '7
        fSql = fSql & " NOMBRE_PERSONA," ' orden del Pedido      8
        fSql = fSql & " CEDULA,"                                '9
        fSql = fSql & " PUNTAJE_CREDITO,"                         '10
        fSql = fSql & " VALOR_ORIGINAL,"                            '11
        fSql = fSql & " INTERESES,"                            '12
        fSql = fSql & " CUOTA_ADMINISTRATIVA,"                       '13
        fSql = fSql & " ABONO_PAGO,"                          '14
        fSql = fSql & " VALOR_ACTUALIZADO_DEL_PEDIDO,"               '15
        fSql = fSql & " VALOR_ACTUALIZADO_TOTAL,"                        '16
        fSql = fSql & " SITUACION_COMERCIAL,"                    '17
        fSql = fSql & " FECHA_AUT_FACTURACION,"             '18
        fSql = fSql & " FECHA_ENTREGA,"           '19
        fSql = fSql & " FECHA_VENCIMIENTO,"                                 '20
        fSql = fSql & " DIA_MORA,"                        '21
        fSql = fSql & " RANGO_MORA,"                      '22
        fSql = fSql & " FECHA_PAGO,"                          '23
        fSql = fSql & " ENTIDAD_DE_PAGO,"          '24
        fSql = fSql & " CICLO," ' Ciclo Captación      '25
        fSql = fSql & " CICLO_IND,"                              '26
        fSql = fSql & " CICLO_DE_FACTURACION_DEL_PEDIDO,"                        '27
        fSql = fSql & " PLAN_PAGO,"                        '28
        fSql = fSql & " VIA_PRINCIPAL,"                        '29
        fSql = fSql & " COMPLEMENTO,"                     '30
        fSql = fSql & " BARRIO," 'Día del Ciclo             31
        fSql = fSql & " MUNICIPIO,"                              '32
        fSql = fSql & " DEPARTAMIENTO, " 'VIA PRINCIPAL           33   
        fSql = fSql & " TELEFONO_RESIDENCIAL,"                           '34
        fSql = fSql & " CELULAR,"                                '35
        fSql = fSql & " DESCRIPCION_MODELO_COMERCIAL,"                             '36
        fSql = fSql & " ESTRUCTURA_PADRE,"                         '37
        fSql = fSql & " ESTRUCTURA,"                          '38
        fSql = fSql & " USUARIO_DE_CREACION,"                            '39
        fSql = fSql & " USUARIO_DE_FINALIZACION,"                   '40
        fSql = fSql & " NUMERO_DIAS,"                    '41
        fSql = fSql & " CAPTACION_RESTRITA,"                         '42
        fSql = fSql & " CORREO_ELECTRONICO,"                      '43
        fSql = fSql & " REFERENCIAS_PERSONALES_1,"                  '44
        fSql = fSql & " REFERENCIA_TELEFONO_1,"                   '45
        fSql = fSql & " REFERENCIAS_PERSONALES_2,"                     '46
        fSql = fSql & " REFERENCIA_TELEFONO_2)"                              '47


        fSql = fSql & " VALUES("                                '1

        fSql = fSql & " @PRIMER_PEDIDO,"                          '2
        fSql = fSql & " @FECHA_PRIMER_PEDIDO,"                       '3
        fSql = fSql & " @CODIGO_PEDIDO,"                               '4
        fSql = fSql & " @FACTURA,"                               '5
        fSql = fSql & " @PERFIL_CONSULTOR_CARTERA,"                         '6
        fSql = fSql & " @CODIGO,"                           '7
        fSql = fSql & " @NOMBRE_PERSONA," ' orden del Pedido      8
        fSql = fSql & " @CEDULA,"                                '9
        fSql = fSql & " @PUNTAJE_CREDITO,"                         '10
        fSql = fSql & " @VALOR_ORIGINAL,"                            '11
        fSql = fSql & " @INTERESES,"                            '12
        fSql = fSql & " @CUOTA_ADMINISTRATIVA,"                       '13
        fSql = fSql & " @ABONO_PAGO,"                          '14
        fSql = fSql & " @VALOR_ACTUALIZADO_DEL_PEDIDO,"               '15
        fSql = fSql & " @VALOR_ACTUALIZADO_TOTAL,"                        '16
        fSql = fSql & " @SITUACION_COMERCIAL,"                    '17
        fSql = fSql & " @FECHA_AUT_FACTURACION,"             '18
        fSql = fSql & " @FECHA_ENTREGA,"           '19
        fSql = fSql & " @FECHA_VENCIMIENTO,"                                 '20
        fSql = fSql & " @DIA_MORA,"                        '21
        fSql = fSql & " @RANGO_MORA,"                      '22
        fSql = fSql & " @FECHA_PAGO,"                          '23
        fSql = fSql & " @ENTIDAD_DE_PAGO,"          '24
        fSql = fSql & " @CICLO," ' Ciclo Captación      '25
        fSql = fSql & " @CICLO_IND,"                              '26
        fSql = fSql & " @CICLO_DE_FACTURACION_DEL_PEDIDO,"                        '27
        fSql = fSql & " @PLAN_PAGO,"                        '28
        fSql = fSql & " @VIA_PRINCIPAL,"                        '29
        fSql = fSql & " @COMPLEMENTO,"                     '30
        fSql = fSql & " @BARRIO," 'Día del Ciclo             31
        fSql = fSql & " @MUNICIPIO,"                              '32
        fSql = fSql & " @DEPARTAMIENTO, " 'VIA PRINCIPAL           33   
        fSql = fSql & " @TELEFONO_RESIDENCIAL,"                           '34
        fSql = fSql & " @CELULAR,"                                '35
        fSql = fSql & " @DESCRIPCION_MODELO_COMERCIAL,"                             '36
        fSql = fSql & " @ESTRUCTURA_PADRE,"                         '37
        fSql = fSql & " @ESTRUCTURA,"                          '38
        fSql = fSql & " @USUARIO_DE_CREACION,"                            '39
        fSql = fSql & " @USUARIO_DE_FINALIZACION,"                   '40
        fSql = fSql & " @NUMERO_DIAS,"                    '41
        fSql = fSql & " @CAPTACION_RESTRITA,"                         '42
        fSql = fSql & " @CORREO_ELECTRONICO,"                      '43
        fSql = fSql & " @REFERENCIAS_PERSONALES_1,"                  '44
        fSql = fSql & " @REFERENCIA_TELEFONO_1,"                   '45
        fSql = fSql & " @REFERENCIAS_PERSONALES_2,"                     '46
        fSql = fSql & " @REFERENCIA_TELEFONO_2)"
        cmAlta = New OleDbCommand(fSql)




        cmAlta.Parameters.Add(" @PRIMER_PEDIDO", OleDbType.VarChar)                          '2
        cmAlta.Parameters.Add(" @FECHA_PRIMER_PEDIDO", OleDbType.VarChar)                       '3
        cmAlta.Parameters.Add(" @CODIGO_PEDIDO", OleDbType.VarChar)                               '4
        cmAlta.Parameters.Add(" @FACTURA", OleDbType.VarChar) '5
        cmAlta.Parameters.Add(" @PERFIL_CONSULTOR_CARTERA", OleDbType.VarChar)                         '6
        cmAlta.Parameters.Add(" @CODIGO", OleDbType.VarChar)                           '7
        cmAlta.Parameters.Add(" @NOMBRE_PERSONA", OleDbType.VarChar) ' orden del Pedido      8
        cmAlta.Parameters.Add(" @CEDULA", OleDbType.VarChar)                                '9
        cmAlta.Parameters.Add(" @PUNTAJE_CREDITO", OleDbType.VarChar)                         '10
        cmAlta.Parameters.Add(" @VALOR_ORIGINAL", OleDbType.VarChar)                            '11
        cmAlta.Parameters.Add(" @INTERESES", OleDbType.VarChar)                            '12
        cmAlta.Parameters.Add(" @CUOTA_ADMINISTRATIVA", OleDbType.VarChar)                       '13
        cmAlta.Parameters.Add(" @ABONO_PAGO", OleDbType.VarChar)                          '14
        cmAlta.Parameters.Add(" @VALOR_ACTUALIZADO_DEL_PEDIDO", OleDbType.VarChar)               '15
        cmAlta.Parameters.Add(" @VALOR_ACTUALIZADO_TOTAL", OleDbType.VarChar)                        '16
        cmAlta.Parameters.Add(" @SITUACION_COMERCIAL", OleDbType.VarChar)                    '17
        cmAlta.Parameters.Add(" @FECHA_AUT_FACTURACION", OleDbType.VarChar)             '18
        cmAlta.Parameters.Add(" @FECHA_ENTREGA", OleDbType.VarChar)           '19
        cmAlta.Parameters.Add(" @FECHA_VENCIMIENTO", OleDbType.VarChar)                                 '20
        cmAlta.Parameters.Add(" @DIA_MORA", OleDbType.VarChar)                        '21
        cmAlta.Parameters.Add(" @RANGO_MORA", OleDbType.VarChar)                      '22
        cmAlta.Parameters.Add(" @FECHA_PAGO", OleDbType.VarChar)                          '23
        cmAlta.Parameters.Add(" @ENTIDAD_DE_PAGO", OleDbType.VarChar)          '24
        cmAlta.Parameters.Add(" @CICLO", OleDbType.VarChar) ' Ciclo Captación      '25
        cmAlta.Parameters.Add(" @CICLO_IND", OleDbType.VarChar)                              '26
        cmAlta.Parameters.Add(" @CICLO_DE_FACTURACION_DEL_PEDIDO", OleDbType.VarChar)                        '27
        cmAlta.Parameters.Add(" @PLAN_PAGO", OleDbType.VarChar)                        '28
        cmAlta.Parameters.Add(" @VIA_PRINCIPAL", OleDbType.VarChar)                        '29
        cmAlta.Parameters.Add(" @COMPLEMENTO", OleDbType.VarChar)                     '30
        cmAlta.Parameters.Add(" @BARRIO", OleDbType.VarChar) 'Día del Ciclo             31
        cmAlta.Parameters.Add(" @MUNICIPIO", OleDbType.VarChar)                              '32
        cmAlta.Parameters.Add(" @DEPARTAMIENTO", OleDbType.VarChar) 'VIA PRINCIPAL           33   
        cmAlta.Parameters.Add(" @TELEFONO_RESIDENCIAL", OleDbType.VarChar)                           '34
        cmAlta.Parameters.Add(" @CELULAR", OleDbType.VarChar)                                '35
        cmAlta.Parameters.Add(" @DESCRIPCION_MODELO_COMERCIAL", OleDbType.VarChar)                             '36
        cmAlta.Parameters.Add(" @ESTRUCTURA_PADRE", OleDbType.VarChar)                         '37
        cmAlta.Parameters.Add(" @ESTRUCTURA", OleDbType.VarChar)                          '38
        cmAlta.Parameters.Add(" @USUARIO_DE_CREACION", OleDbType.VarChar)                            '39
        cmAlta.Parameters.Add(" @USUARIO_DE_FINALIZACION", OleDbType.VarChar)                   '40
        cmAlta.Parameters.Add(" @NUMERO_DIAS", OleDbType.VarChar)                    '41
        cmAlta.Parameters.Add(" @CAPTACION_RESTRITA", OleDbType.VarChar)                         '42
        cmAlta.Parameters.Add(" @CORREO_ELECTRONICO", OleDbType.VarChar)                      '43
        cmAlta.Parameters.Add(" @REFERENCIAS_PERSONALES_1", OleDbType.VarChar)                  '44
        cmAlta.Parameters.Add(" @REFERENCIA_TELEFONO_1", OleDbType.VarChar)                   '45
        cmAlta.Parameters.Add(" @REFERENCIAS_PERSONALES_2", OleDbType.VarChar)                     '46
        cmAlta.Parameters.Add(" @REFERENCIA_TELEFONO_2", OleDbType.VarChar)

        cmAlta.Connection = cn
        For i = 0 To dsexcel.Tables(0).Rows.Count - 1
            cmAlta.Parameters(" @PRIMER_PEDIDO").Value = dsexcel.Tables(0).Rows(i)(0).ToString()                      '2
            cmAlta.Parameters(" @FECHA_PRIMER_PEDIDO").Value = dsexcel.Tables(0).Rows(i)(1).ToString()                   '3
            cmAlta.Parameters(" @CODIGO_PEDIDO").Value = dsexcel.Tables(0).Rows(i)(2).ToString()                           '4
            cmAlta.Parameters(" @FACTURA").Value = dsexcel.Tables(0).Rows(i)(3).ToString()                           '5
            cmAlta.Parameters(" @PERFIL_CONSULTOR_CARTERA").Value = dsexcel.Tables(0).Rows(i)(4).ToString()                     '6
            cmAlta.Parameters(" @CODIGO").Value = dsexcel.Tables(0).Rows(i)(5).ToString()                       '7
            cmAlta.Parameters(" @NOMBRE_PERSONA").Value = dsexcel.Tables(0).Rows(i)(6).ToString()
            cmAlta.Parameters(" @CEDULA").Value = dsexcel.Tables(0).Rows(i)(7).ToString()                            '9
            cmAlta.Parameters(" @PUNTAJE_CREDITO").Value = dsexcel.Tables(0).Rows(i)(8).ToString()                     '10
            cmAlta.Parameters(" @VALOR_ORIGINAL").Value = dsexcel.Tables(0).Rows(i)(9).ToString()                        '11
            cmAlta.Parameters(" @INTERESES").Value = dsexcel.Tables(0).Rows(i)(10).ToString()                        '12
            cmAlta.Parameters(" @CUOTA_ADMINISTRATIVA").Value = dsexcel.Tables(0).Rows(i)(11).ToString()                   '13
            cmAlta.Parameters(" @ABONO_PAGO").Value = dsexcel.Tables(0).Rows(i)(12).ToString()                      '14
            cmAlta.Parameters(" @VALOR_ACTUALIZADO_DEL_PEDIDO").Value = dsexcel.Tables(0).Rows(i)(12).ToString()           '15
            cmAlta.Parameters(" @VALOR_ACTUALIZADO_TOTAL").Value = dsexcel.Tables(0).Rows(i)(14).ToString()                    '16
            cmAlta.Parameters(" @SITUACION_COMERCIAL").Value = dsexcel.Tables(0).Rows(i)(15).ToString()                '17
            cmAlta.Parameters(" @FECHA_AUT_FACTURACION").Value = dsexcel.Tables(0).Rows(i)(16).ToString()         '18
            cmAlta.Parameters(" @FECHA_ENTREGA").Value = dsexcel.Tables(0).Rows(i)(17).ToString()       '19
            cmAlta.Parameters(" @FECHA_VENCIMIENTO").Value = dsexcel.Tables(0).Rows(i)(18).ToString()                             '20
            cmAlta.Parameters(" @DIA_MORA").Value = dsexcel.Tables(0).Rows(i)(19).ToString()                    '21
            cmAlta.Parameters(" @RANGO_MORA").Value = dsexcel.Tables(0).Rows(i)(20).ToString()                  '22
            cmAlta.Parameters(" @FECHA_PAGO").Value = dsexcel.Tables(0).Rows(i)(21).ToString()                      '23
            cmAlta.Parameters(" @ENTIDAD_DE_PAGO").Value = dsexcel.Tables(0).Rows(i)(22).ToString()      '24
            cmAlta.Parameters(" @CICLO").Value = dsexcel.Tables(0).Rows(i)(23).ToString()
            cmAlta.Parameters(" @CICLO_IND").Value = dsexcel.Tables(0).Rows(i)(24).ToString()                          '26
            cmAlta.Parameters(" @CICLO_DE_FACTURACION_DEL_PEDIDO").Value = dsexcel.Tables(0).Rows(i)(25).ToString()                    '27
            cmAlta.Parameters(" @PLAN_PAGO").Value = dsexcel.Tables(0).Rows(i)(26).ToString()                    '28
            cmAlta.Parameters(" @VIA_PRINCIPAL").Value = dsexcel.Tables(0).Rows(i)(27).ToString()                    '29
            cmAlta.Parameters(" @COMPLEMENTO").Value = dsexcel.Tables(0).Rows(i)(28).ToString()                 '30
            cmAlta.Parameters(" @BARRIO").Value = dsexcel.Tables(0).Rows(i)(29).ToString()
            cmAlta.Parameters(" @MUNICIPIO").Value = dsexcel.Tables(0).Rows(i)(30).ToString()                          '32
            cmAlta.Parameters(" @DEPARTAMIENTO").Value = dsexcel.Tables(0).Rows(i)(31).ToString()
            cmAlta.Parameters(" @TELEFONO_RESIDENCIAL").Value = dsexcel.Tables(0).Rows(i)(33).ToString()                       '34
            cmAlta.Parameters(" @CELULAR").Value = dsexcel.Tables(0).Rows(i)(34).ToString()                            '35
            cmAlta.Parameters(" @DESCRIPCION_MODELO_COMERCIAL").Value = dsexcel.Tables(0).Rows(i)(35).ToString()                         '36
            cmAlta.Parameters(" @ESTRUCTURA_PADRE").Value = dsexcel.Tables(0).Rows(i)(36).ToString()                     '37
            cmAlta.Parameters(" @ESTRUCTURA").Value = dsexcel.Tables(0).Rows(i)(37).ToString()                      '38
            cmAlta.Parameters(" @USUARIO_DE_CREACION").Value = dsexcel.Tables(0).Rows(i)(38).ToString()                        '39
            cmAlta.Parameters(" @USUARIO_DE_FINALIZACION").Value = dsexcel.Tables(0).Rows(i)(39).ToString()               '40
            cmAlta.Parameters(" @NUMERO_DIAS").Value = dsexcel.Tables(0).Rows(i)(40).ToString()                '41
            cmAlta.Parameters(" @CAPTACION_RESTRITA").Value = dsexcel.Tables(0).Rows(i)(41).ToString()                     '42
            cmAlta.Parameters(" @CORREO_ELECTRONICO").Value = dsexcel.Tables(0).Rows(i)(42).ToString()                  '43
            cmAlta.Parameters(" @REFERENCIAS_PERSONALES_1").Value = dsexcel.Tables(0).Rows(i)(43).ToString()              '44
            cmAlta.Parameters(" @REFERENCIA_TELEFONO_1").Value = dsexcel.Tables(0).Rows(i)(44).ToString()               '45
            cmAlta.Parameters(" @REFERENCIAS_PERSONALES_2").Value = dsexcel.Tables(0).Rows(i)(45).ToString()                 '46
            cmAlta.Parameters(" @REFERENCIA_TELEFONO_2").Value = dsexcel.Tables(0).Rows(i)(46).ToString()

            'realizamos el alta
            cmAlta.ExecuteNonQuery()
        Next

        'Cerramos la conexion con la base de datos
        cn.Close()
    End Sub

    Private Sub B_Cobro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B_Cobro.Click
        Dim folderDlg As New OpenFileDialog
        If (folderDlg.ShowDialog() = DialogResult.OK) Then
            Cobro.Text = folderDlg.FileName
        End If
    End Sub

    Private Sub B_Ventas_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B_Ventas.Click
        Dim folderDlg As New OpenFileDialog
        If (folderDlg.ShowDialog() = DialogResult.OK) Then
            Ventas.Text = folderDlg.FileName
        End If
    End Sub

    Private Sub B_Cartera_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B_Cartera.Click
        Dim folderDlg As New OpenFileDialog
        If (folderDlg.ShowDialog() = DialogResult.OK) Then
            Cartera.Text = folderDlg.FileName
        End If
    End Sub

    Private Sub B_Revendedores_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B_Revendedores.Click
        Dim folderDlg As New OpenFileDialog
        If (folderDlg.ShowDialog() = DialogResult.OK) Then
            Revendedores.Text = folderDlg.FileName
        End If
    End Sub

    Private Sub B_Agencias_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B_Agencias.Click
        Dim folderDlg As New OpenFileDialog
        If (folderDlg.ShowDialog() = DialogResult.OK) Then
            Agencias.Text = folderDlg.FileName
        End If
    End Sub

    Private Sub B_Validar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles B_Validar.Click
        LeerXls()
    End Sub

    Private Sub Ventas_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Ventas.TextChanged

    End Sub

End Class
