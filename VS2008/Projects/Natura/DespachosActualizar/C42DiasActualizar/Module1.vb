﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports System.Threading

Module Module1

    Sub Main()
        Dim oExcel As Excel.Application
        Dim oBook As Excel.Workbook
        Dim oBooks As Excel.Workbooks

        oExcel = CreateObject("Excel.Application")
        oExcel.Visible = True
        oBooks = oExcel.Workbooks
        oBook = oBooks.Open(My.Settings.Archivo1)
        oExcel.Run(My.Settings.Macro)
        Thread.Sleep(2000)
        oExcel.Run(My.Settings.Macro)
        Thread.Sleep(2000)
        oExcel.DisplayAlerts = False
        Thread.Sleep(2000)
        oBook.Save()
        oBook.Close(True)
        oExcel.Quit()

        oExcel = CreateObject("Excel.Application")
        oExcel.Visible = True
        oBooks = oExcel.Workbooks
        oBook = oBooks.Open(My.Settings.Archivo2)
        oExcel.Run(My.Settings.Macro)
        Thread.Sleep(2000)
        oExcel.Run(My.Settings.Macro)
        Thread.Sleep(2000)
        oExcel.DisplayAlerts = False
        Thread.Sleep(2000)
        oBook.Save()
        oBook.Close(True)
        oExcel.Quit()

    End Sub

End Module
