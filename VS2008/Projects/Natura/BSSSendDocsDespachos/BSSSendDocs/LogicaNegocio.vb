﻿Imports System
Imports System.Timers
Imports System.Threading
Imports System.IO
Imports System.Data.SqlClient

Module LogicaNegocio
    Public DB_DB2 As ADODB.Connection
    Public DB_DB1 As ADODB.Connection
    Public gsConnString As String
    Public sFolderPDF As String
    Public sExt As String
    Public sLib As String
    Public Timer1 As System.Timers.Timer
    Public SessionId As Double
    Public bChkProceso As Boolean

    Public Sub Proceso()
        SFTP()

    End Sub

    Sub SFTP()

        Dim Transportadora As String
        Dim Archivo As String
        Dim Estiba As String

        Dim aParam() As String
        Dim mailman As New Chilkat_v9_5_0.ChilkatMailMan

        Dim mht As New Chilkat_v9_5_0.ChilkatMht
        aParam = Split(Command(), " ")
        If UBound(aParam) <> 1 Then
            ' End
        End If

        Transportadora = Trim(aParam(0))
        Archivo = Trim(aParam(1))
        Estiba = Trim(aParam(2))
        Dim email As New Chilkat_v9_5_0.ChilkatMailMan
        mailman.SmtpHost = My.Settings.MAILHOST
        mailman.SmtpUsername = My.Settings.MAILUSER
        mailman.SmtpPassword = My.Settings.MAILPASS
        mailman.SmtpPort = 25
        mailman.StartTLS = 1
        mailman.SmtpSsl = False

        Dim success As Boolean
        success = mht.UnlockComponent("SMANRIMHT_dEnsKQ0g3Rue")
        If (success <> True) Then
            MsgBox(mht.LastErrorHtml)
        End If
        success = mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
        If (success <> True) Then
            Console.WriteLine(mailman.LastErrorHtml)

        End If
        email.subject = "Estiba No: " & Estiba & " Transportadora: " & Transportadora
        email.Body = "Estiba No: " & Estiba & " Transportadora: " & Transportadora
        email.From = "support@Natura.Net"

        If Transportadora = "ENVIA" Then
            email.AddTo("Asesor SAC Natura 2", "asesorsacnatura2@enviacolvanes.com.co")
            email.AddTo("Asesor SAC Natura 2", "asesorsacnatura1@enviacolvanes.com.co")
        End If

        If Transportadora = "DEPRISA" Then
            email.AddTo("Transportesnaturacol_6", "Transportesnaturacol_6@natura.net")
            email.AddTo("Transportesnaturacol_7", "Transportesnaturacol_7@natura.net")
        End If


        If Transportadora = "SERVIENTREGAS.A" Then
            email.AddTo("Luis Padilla", "Luis.Padillau@co.servientrega.com")
            email.AddTo("Jhon Camacho", "jhon.camacho@co.servientrega.com")

        End If

        If Transportadora = "INTERRAPIDISIMOS.A" Then
            email.AddTo("Interrapidisimo", "coord.natura@interrapidisimo.com")
        End If

        email.AddTo("Natura", "Transportesnaturacol_4@natura.net")
        email.AddCC("Criss Velandia", "CrisVelandia@natura.net")
        email.AddCC("Ivonne Perez", "IvonnePerez@natura.net")
        email.AddCC("Carol Morales", "carolmorales@natura.net")
        email.AddCC("Eduard Abril", "eduardabril@natura.net")
        email.AddCC("Despachos", "DespachosColombia@natura.net")
        email.AddCC("Alertas", "AlertasNatura@gmail.com")
        email.AddCC("Marlon Hurtado", "marlonhurtado.sonda@natura.net")

        email.AddBcc("Alertas Correo", "alertascorreo2015@gmail.com")
        '  To add file attachments to an email, call AddFileAttachment
        '  once for each file to be attached.  The method returns
        '  the content-type of the attachment if successful, otherwise
        '  returns cknull
        Dim contentType As String

        contentType = email.AddFileAttachment(My.Settings.CrpetRaiz & Archivo)
        If (contentType = vbNullString) Then
            Console.WriteLine(email.LastErrorText)
            Exit Sub
        End If
        success = mailman.SendEmail(email)

        If (success <> True) Then
            Console.WriteLine(mailman.LastErrorHtml)
        End If




    End Sub

    'AppEjecutando("SENDDOCS", 1)

    'SessionId = CalcConsec("SENDDOCS", False, "99999999")
    'Select CheckVer("SENDDOCS")
    '    Case ChkVerApp.AppDeshabilitada
    '        Console.WriteLine("Aplicacion Deshabilitada.")
    '        Thread.Sleep(5000)
    '        Exit Sub
    '    Case ChkVerApp.AppVersionIncorrect
    '        Console.WriteLine("Version Incorrecta.")
    '        Console.ReadKey()
    '        Exit Sub
    '        '    Case http://www.forticlient.com/video/002ChkVerApp.AppEjecutando
    '        '        Console.WriteLine("En Ejecucion.")
    '        '        Thread.Sleep(5000)
    '        '        Exit Sub
    'End Select

    'AppEjecutando("SENDDOCS", 2)

    'Console.WriteLine("Predist")
    'Predist()

    'Console.WriteLine("AvisoRProducto")
    'AvisoRProducto()

    'Console.WriteLine("AvisoPedidoConDecimales")
    'AvisoPedidoConDecimales()

    'Console.WriteLine("AvisoConsumoProducto")
    'AvisoConsumoProducto()

    'Console.WriteLine("Aviso Cliente Nuevo")
    'AvisoClienteNuevo()

    'Console.WriteLine("Aviso Paletizado")
    'AvisoPaletizado()

    'Console.WriteLine("Pedidos Atrasados")
    'PedidosAtrasados()

    'Console.WriteLine("Alerta Pedidos Bandeja")
    'AlertaBandeja()

    'Console.WriteLine("PPP")
    'AprobPreciosProveedor()

    'Console.WriteLine("Envío de Documentos.")
    'EnvioDeDocumentos()

    'PrepaInfoOCFletes()
    'Console.WriteLine("Tiquetes Incompletos.")
    'TiquetesIncompletos()

    'Console.WriteLine("Aprobacion de Ajustes")
    'AprobAjustesFletes()

    'Console.WriteLine("Aprobacion de Tarifas de Fletes")
    'AprobTarifasFletes()

    'AppEjecutando("SENDDOCS", 1)



End Module
