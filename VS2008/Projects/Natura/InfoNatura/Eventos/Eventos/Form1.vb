﻿Imports System.IO
Imports System.Data.SqlClient
Public Class Form1
    Public fSql As String

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub VButton1_Click(sender As System.Object, e As System.EventArgs) Handles VButton1.Click
        '        Dim conn As New SqlConnection("Data Source=FM-WIN7U\BSSLTDA;Initial Catalog=InfoNatura;Persist Security Info=True;User ID=info;Password=InfoNatura")
        Dim conn As New SqlConnection("Data Source=172.27.34.57\SQLEXPRESS;Initial Catalog=InfoNatura;Persist Security Info=True;User ID=info;Password=InfoNatura")
        conn.Open()

        Dim adaptador As SqlDataAdapter = New SqlDataAdapter()
        Dim sql As SqlCommand = New SqlCommand("select * from Eventos where Estado='Enviado'", conn)
        adaptador.SelectCommand = sql
        Dim tareas As DataSet = New DataSet
        adaptador.Fill(tareas, "Eventos")

        Dim dr As DataRow
        For Each dr In tareas.Tables("Eventos").Rows
            fSql = "UPDATE Eventos SET Estado=@Estado Where id=@Trabajo"
            sql = New SqlCommand(fSql, conn)
            sql.Parameters.AddWithValue("@Estado", "Iniciado")
            sql.Parameters.AddWithValue("@Trabajo", dr("ID").ToString())
            Dim r As Integer = sql.ExecuteNonQuery()
            Dim aParams() As String = Split(dr("Parametros").ToString.Trim(), ",")
            Select Case dr("Macro").ToString().Trim()
                Case "Cartera"
                    sql = New SqlCommand("SP_CarteraSubirArchivos", conn)
                    sql.CommandTimeout = 0
                    sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
                    sql.CommandType = CommandType.StoredProcedure
                    sql.ExecuteNonQuery()
                Case "Datacredito"
                    sql = New SqlCommand("SP_DataCreditoDatos", conn)
                    sql.CommandTimeout = 0
                    sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
                    sql.CommandType = CommandType.StoredProcedure
                    sql.ExecuteNonQuery()
            End Select
            sql = New SqlCommand(fSql, conn)
            sql.Parameters.AddWithValue("@Estado", "Terminado")
            sql.Parameters.AddWithValue("@Trabajo", dr("ID").ToString())
            r = sql.ExecuteNonQuery()
        Next

    End Sub

    Public Function CadenaConexion() As String
        Dim csb As New SqlConnectionStringBuilder
        csb.DataSource = "FM-WIN7U\BSSLTDA"
        csb.InitialCatalog = "InfoNatura"
        csb.IntegratedSecurity = True
        csb.UserID = "Info"
        csb.Password = "InfoNatura"
        Return csb.ConnectionString
    End Function

    Public Sub Actualizar(ByVal estado As String)
        Dim sCon As String = CadenaConexion()
        Dim sel As String

        sel = "UPDATE Eventos SET Estado = @Estado "

        Using con As New SqlConnection(sCon)
            Dim cmd As New SqlCommand(sel, con)
            cmd.Parameters.AddWithValue("@Estado", estado)
            con.Open()
            Dim t As Integer = cmd.ExecuteNonQuery()
            con.Close()
        End Using
    End Sub


    Private Sub VButton2_Click(sender As System.Object, e As System.EventArgs) Handles VButton2.Click
        Dim conn As New SqlConnection("Data Source=FM-WIN7U\BSSLTDA;Initial Catalog=InfoNatura;Persist Security Info=True;User ID=info;Password=InfoNatura")
        conn.Open()

        Dim adaptador As SqlDataAdapter = New SqlDataAdapter()
        'Dim sql As SqlCommand = New SqlCommand("select * from Eventos where Estado='Enviado'", conn)
        Dim sql As SqlCommand = New SqlCommand("select * from Eventos", conn)
        adaptador.SelectCommand = sql
        Dim tareas As DataSet = New DataSet
        adaptador.Fill(tareas, "Eventos")

        Dim tbl As DataTable = tareas.Tables("Eventos")

        Me.grid01.DataSource = tbl

    End Sub
End Class
