﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridLocalization1 As VIBlend.WinForms.DataGridView.DataGridLocalization = New VIBlend.WinForms.DataGridView.DataGridLocalization()
        Me.VButton1 = New VIBlend.WinForms.Controls.vButton()
        Me.grid01 = New VIBlend.WinForms.DataGridView.vDataGridView()
        Me.DataSet1 = New System.Data.DataSet()
        Me.VButton2 = New VIBlend.WinForms.Controls.vButton()
        CType(Me.DataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'VButton1
        '
        Me.VButton1.AllowAnimations = True
        Me.VButton1.BackColor = System.Drawing.Color.Transparent
        Me.VButton1.Location = New System.Drawing.Point(12, 12)
        Me.VButton1.Name = "VButton1"
        Me.VButton1.RoundedCornersMask = CType(15, Byte)
        Me.VButton1.Size = New System.Drawing.Size(100, 30)
        Me.VButton1.TabIndex = 0
        Me.VButton1.Text = "Ejecuta Cargue"
        Me.VButton1.UseVisualStyleBackColor = False
        Me.VButton1.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.VISTABLUE
        '
        'grid01
        '
        Me.grid01.AllowAnimations = True
        Me.grid01.AllowCellMerge = True
        Me.grid01.AllowClipDrawing = True
        Me.grid01.AllowContextMenuColumnChooser = True
        Me.grid01.AllowContextMenuFiltering = True
        Me.grid01.AllowContextMenuGrouping = True
        Me.grid01.AllowContextMenuSorting = True
        Me.grid01.AllowCopyPaste = False
        Me.grid01.AllowDefaultContextMenu = True
        Me.grid01.AllowDragDropIndication = True
        Me.grid01.AllowHeaderItemHighlightOnCellSelection = True
        Me.grid01.AutoUpdateOnListChanged = False
        Me.grid01.BackColor = System.Drawing.Color.FromArgb(CType(CType(171, Byte), Integer), CType(CType(171, Byte), Integer), CType(CType(171, Byte), Integer))
        Me.grid01.BindingProgressEnabled = False
        Me.grid01.BindingProgressSampleRate = 20000
        Me.grid01.BorderColor = System.Drawing.Color.Empty
        Me.grid01.CellsArea.AllowCellMerge = True
        Me.grid01.CellsArea.ConditionalFormattingEnabled = False
        Me.grid01.ColumnsHierarchy.AllowDragDrop = False
        Me.grid01.ColumnsHierarchy.AllowResize = True
        Me.grid01.ColumnsHierarchy.AutoStretchColumns = False
        Me.grid01.ColumnsHierarchy.Fixed = False
        Me.grid01.ColumnsHierarchy.ShowExpandCollapseButtons = True
        Me.grid01.EnableColumnChooser = False
        Me.grid01.EnableResizeToolTip = True
        Me.grid01.EnableToolTips = True
        Me.grid01.FilterDisplayMode = VIBlend.WinForms.DataGridView.FilterDisplayMode.[Default]
        Me.grid01.GridLinesDashStyle = System.Drawing.Drawing2D.DashStyle.Solid
        Me.grid01.GridLinesDisplayMode = VIBlend.WinForms.DataGridView.GridLinesDisplayMode.DISPLAY_ALL
        Me.grid01.GroupingEnabled = False
        Me.grid01.HorizontalScroll = 0
        Me.grid01.HorizontalScrollBarLargeChange = 20
        Me.grid01.HorizontalScrollBarSmallChange = 5
        Me.grid01.ImageList = Nothing
        Me.grid01.Localization = DataGridLocalization1
        Me.grid01.Location = New System.Drawing.Point(12, 59)
        Me.grid01.MultipleSelectionEnabled = True
        Me.grid01.Name = "grid01"
        Me.grid01.PivotColumnsTotalsEnabled = False
        Me.grid01.PivotColumnsTotalsMode = VIBlend.WinForms.DataGridView.PivotTotalsMode.DISPLAY_BOTH
        Me.grid01.PivotRowsTotalsEnabled = False
        Me.grid01.PivotRowsTotalsMode = VIBlend.WinForms.DataGridView.PivotTotalsMode.DISPLAY_BOTH
        Me.grid01.RowsHierarchy.AllowDragDrop = False
        Me.grid01.RowsHierarchy.AllowResize = True
        Me.grid01.RowsHierarchy.CompactStyleRenderingEnabled = False
        Me.grid01.RowsHierarchy.CompactStyleRenderingItemsIndent = 15
        Me.grid01.RowsHierarchy.Fixed = False
        Me.grid01.RowsHierarchy.ShowExpandCollapseButtons = True
        Me.grid01.ScrollBarsEnabled = True
        Me.grid01.SelectionBorderDashStyle = System.Drawing.Drawing2D.DashStyle.Solid
        Me.grid01.SelectionBorderEnabled = True
        Me.grid01.SelectionBorderWidth = 2
        Me.grid01.SelectionMode = VIBlend.WinForms.DataGridView.vDataGridView.SELECTION_MODE.CELL_SELECT
        Me.grid01.Size = New System.Drawing.Size(774, 419)
        Me.grid01.TabIndex = 1
        Me.grid01.Text = "VDataGridView1"
        Me.grid01.ToolTipDuration = 5000
        Me.grid01.ToolTipShowDelay = 1500
        Me.grid01.VerticalScroll = 0
        Me.grid01.VerticalScrollBarLargeChange = 20
        Me.grid01.VerticalScrollBarSmallChange = 5
        Me.grid01.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.VISTABLUE
        Me.grid01.VirtualModeCellDefault = False
        '
        'DataSet1
        '
        Me.DataSet1.DataSetName = "NewDataSet"
        '
        'VButton2
        '
        Me.VButton2.AllowAnimations = True
        Me.VButton2.BackColor = System.Drawing.Color.Transparent
        Me.VButton2.Location = New System.Drawing.Point(127, 12)
        Me.VButton2.Name = "VButton2"
        Me.VButton2.RoundedCornersMask = CType(15, Byte)
        Me.VButton2.Size = New System.Drawing.Size(100, 30)
        Me.VButton2.TabIndex = 2
        Me.VButton2.Text = "Carga grid"
        Me.VButton2.UseVisualStyleBackColor = False
        Me.VButton2.VIBlendTheme = VIBlend.Utilities.VIBLEND_THEME.VISTABLUE
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(798, 502)
        Me.Controls.Add(Me.VButton2)
        Me.Controls.Add(Me.grid01)
        Me.Controls.Add(Me.VButton1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        CType(Me.DataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents VButton1 As VIBlend.WinForms.Controls.vButton
    Friend WithEvents grid01 As VIBlend.WinForms.DataGridView.vDataGridView
    Friend WithEvents DataSet1 As System.Data.DataSet
    Friend WithEvents VButton2 As VIBlend.WinForms.Controls.vButton

End Class
