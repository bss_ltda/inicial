﻿Imports System.IO
Imports System.Data.SqlClient
Public Class Service1
    Public fSql As String
    Private WithEvents tim As New Timers.Timer(10000)
    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Agregue el código aquí para iniciar el servicio. Este método debería poner
        ' en movimiento los elementos para que el servicio pueda funcionar.
        tim.Enabled = True
    End Sub

    Protected Overrides Sub OnStop()
        ' Agregue el código aquí para realizar cualquier anulación necesaria para detener el servicio.
    End Sub

    Private Sub tim_Elapsed(sender As Object, e As System.Timers.ElapsedEventArgs) Handles tim.Elapsed
        Dim conn As New SqlConnection(wsInfoNatura.My.Settings.InfoNaturaConnectionString)
        conn.Open()

        Dim adaptador As SqlDataAdapter = New SqlDataAdapter()
        Dim sql As SqlCommand = New SqlCommand("select * from Eventos where Estado='Enviado'", conn)
        adaptador.SelectCommand = sql
        Dim tareas As DataSet = New DataSet
        adaptador.Fill(tareas, "Eventos")
        Dim dr As DataRow
        For Each dr In tareas.Tables("Eventos").Rows
            fSql = "UPDATE Eventos SET Estado=@Estado Where id=@Trabajo"
            sql = New SqlCommand(fSql, conn)
            sql.Parameters.AddWithValue("@Estado", "Iniciado")
            sql.Parameters.AddWithValue("@Trabajo", dr("ID").ToString())
            Dim r As Integer = sql.ExecuteNonQuery()
            Dim aParams() As String = Split(dr("Parametros").ToString.Trim(), ",")
            Select Case dr("Macro").ToString().Trim()
                Case "Cartera"
                    sql = New SqlCommand("SP_CarteraSubirArchivos", conn)
                    sql.CommandTimeout = 0
                    sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
                    sql.CommandType = CommandType.StoredProcedure
                    sql.ExecuteNonQuery()
                Case "Datacredito"
                    sql = New SqlCommand("SP_DataCreditoSubirArchivos", conn)
                    sql.CommandTimeout = 0
                    sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
                    sql.CommandType = CommandType.StoredProcedure
                    sql.ExecuteNonQuery()
                Case "DatacreditoHistorico"
                    sql = New SqlCommand("SP_DataCreditoSubirArchivos2", conn)
                    sql.CommandTimeout = 0
                    sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
                    sql.CommandType = CommandType.StoredProcedure
                    sql.ExecuteNonQuery()
                Case "Logistica1"
                    sql = New SqlCommand("SP_LogisticaCargueArchivo1", conn)
                    sql.CommandTimeout = 0
                    sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
                    sql.CommandType = CommandType.StoredProcedure
                    sql.ExecuteNonQuery()
                Case "Logistica2"
                    sql = New SqlCommand("SP_LogisticaCargueArchivo2", conn)
                    sql.CommandTimeout = 0
                    sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
                    sql.CommandType = CommandType.StoredProcedure
                    sql.ExecuteNonQuery()
                Case "Logistica3"
                    sql = New SqlCommand("SP_LogisticaCargueArchivo3", conn)
                    sql.CommandTimeout = 0
                    sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
                    sql.CommandType = CommandType.StoredProcedure
                    sql.ExecuteNonQuery()
            End Select
            sql = New SqlCommand(fSql, conn)
            sql.Parameters.AddWithValue("@Estado", "Terminado")
            sql.Parameters.AddWithValue("@Trabajo", dr("ID").ToString())
            r = sql.ExecuteNonQuery()
        Next

    End Sub
End Class
