﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Module Module1
    ' Dim WithEvents m_Excel As Excel.Application
    Sub Main()
        Try
            Dim objLibroExcel As Excel.Workbook
            Dim objHojaExcel As Excel.Worksheet
            Dim sConnectionString As String
            sConnectionString = "Password=" & My.Settings.Password & ";User ID=" & My.Settings.Usuario & ";" & _
                                "Initial Catalog=InfoNatura;" & _
                                "Data Source=" & My.Settings.Servidor & ""
            Dim objConn As New SqlConnection(sConnectionString)
            objConn.Open()
            Dim num As Integer
            num = 3
            Dim value As String


            Dim daAuthors1 As _
            New SqlDataAdapter("Select * From RFSTMGC", objConn)
            Dim dsPubs1 As New DataSet("Pubsz1")
            daAuthors1.FillSchema(dsPubs1, SchemaType.Source, "Authors1")
            daAuthors1.Fill(dsPubs1, "Authors1")
            daAuthors1.MissingSchemaAction = MissingSchemaAction.AddWithKey
            daAuthors1.Fill(dsPubs1, "Authors1")
            Dim tblAuthors1 As DataTable
            tblAuthors1 = dsPubs1.Tables("Authors1")
            Dim drCurrent1 As DataRow
            Dim Sentencia(2) As String
            Sentencia(0) = ""
            For Each drCurrent1 In tblAuthors1.Rows
                Sentencia(0) = ""
                Sentencia(0) = drCurrent1("Sentencia")
            Next
            Dim objConn1 As New SqlConnection(sConnectionString)
            objConn1.Open()

            Dim m_Excel As Excel.Application
            m_Excel = New Excel.Application
            Dim oWord As Excel.Application
            m_Excel.Visible = False
            objLibroExcel = m_Excel.Workbooks.Add()
            objHojaExcel = objLibroExcel.Worksheets(1)
            Dim range As Excel.Range
            range = objHojaExcel.Range("A1", Reflection.Missing.Value)
            range.Name = "Hojas"

            'Dim cmd As System.Data.SqlClient.SqlCommand
            'cmd = New System.Data.SqlClient.SqlCommand()
            'cmd.Connection = objConn
            'cmd.CommandText = _
            '    "EXEC Master..xp_cmdshell 'sqlcmd -Q """ & Sentencia(0) & """ -o ""C:\prue01.csv"" -S 10.21.1.16\INFONATURA -U info -P InfoNatura -s"","" -W'"
            'cmd.ExecuteNonQuery()

            With objHojaExcel.ListObjects.Add(SourceType:=0, Source:="ODBC;DSN=" & My.Settings.DSN & ";UID=" & My.Settings.Usuario & ";PWD=" & My.Settings.Password & ";", Destination:=objHojaExcel.Range("$A$1")).QueryTable
                .CommandText = Sentencia(0)
                .RowNumbers = False
                .FillAdjacentFormulas = False
                .PreserveFormatting = True
                .RefreshOnFileOpen = False
                .BackgroundQuery = True
                .RefreshStyle = 1
                .SavePassword = False
                .SaveData = True
                .AdjustColumnWidth = True
                .RefreshPeriod = 0
                .PreserveColumnInfo = True
                .ListObject.DisplayName = "Q_InfoNatura"
                .Refresh(BackgroundQuery:=False)
            End With

            value = "GradeCualitativa"
            If value = "" Then
                value = Date.Today.Month & "" & Date.Today.Day & "" & Date.Today.Year & "" & Date.Today.Hour & "" & Date.Today.Minute & "" & Date.Today.Second
            End If
            m_Excel.Application.DisplayAlerts = False
            m_Excel.ActiveWorkbook.SaveAs(My.Settings.CarpetaDestino & "" & value, CreateBackup:=False)
            m_Excel.ActiveWorkbook.Close()
            'End If
            Console.Write("ok")
            Dim mydocpath As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            Dim sb As StringBuilder = New StringBuilder()

            sb.AppendLine("New User Input")
            sb.AppendLine("= = = = = =")
            sb.Append("001.Text")
            sb.AppendLine()
            sb.AppendLine()

            Using outfile As StreamWriter = New StreamWriter(mydocpath + "\UserInputFile.txt", True)
                outfile.Write("ok")
            End Using
        Catch

            Console.Write(Err.Description)
            Dim mydocpath As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
            Dim sb As StringBuilder = New StringBuilder()

            sb.AppendLine("New User Input")
            sb.AppendLine("= = = = = =")
            sb.Append("001.Text")
            sb.AppendLine()
            sb.AppendLine()

            Using outfile As StreamWriter = New StreamWriter("C:txt", True)
                outfile.Write(Err.Description)
            End Using

        End Try
    End Sub

End Module
