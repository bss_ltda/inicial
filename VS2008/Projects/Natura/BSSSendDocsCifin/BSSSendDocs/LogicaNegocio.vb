﻿Imports System
Imports System.Timers
Imports System.Threading
Imports System.IO
Imports System.Data.SqlClient
Imports CHILKATFTP2Lib

Module LogicaNegocio
    Public DB_DB2 As ADODB.Connection
    Public DB_DB1 As ADODB.Connection
    Public gsConnString As String
    Public sFolderPDF As String
    Public sExt As String
    Public sLib As String
    Public Timer1 As System.Timers.Timer
    Public SessionId As Double
    Public bChkProceso As Boolean

    Public Sub Proceso()
        SFTP()

    End Sub

    Sub SFTP()

        Console.WriteLine("Inicializando el aplicativo")

        Dim ftp As New ChilkatFtp2()

        Dim success As Boolean

        success = ftp.UnlockComponent("SMANRIFTP_keysbDt3oHn3")
        If (success <> True) Then

            MsgBox(ftp.LastErrorText & vbCrLf)
            Exit Sub
        End If


        Dim aHora
        Dim Sql
        Sql = ""
      

        ftp.Hostname = "201.234.71.92"
        ftp.Username = "ftpInfoNatura"
        ftp.Password = "ftpInfo&&"
        success = ftp.Connect()
        If (success <> True) Then
            Console.WriteLine(ftp.LastErrorText)
            Exit Sub
        End If


        '  Change to the remote directory where the file will be uploaded.


        success = ftp.ChangeRemoteDir(My.Settings.CarpetaDestino)
        If (success <> True) Then
            Console.WriteLine(ftp.LastErrorText)
            Exit Sub
        End If


        Dim localFilename As String = My.Settings.ArchivoCopia & "\COBRO.CSV"
        Dim remoteFilename As String = "COBRO.csv"
        Console.WriteLine("Procesando archivo Cobro")
        success = ftp.PutFile(localFilename, remoteFilename)
        If (success <> True) Then
            Console.WriteLine(ftp.LastErrorText)
            Exit Sub
        End If

        localFilename = My.Settings.ArchivoCopia & "\Vencimientos.CSV"
        remoteFilename = "Vencimientos.csv"
        Console.WriteLine("Procesando archivo Vencimientos")
        success = ftp.PutFile(localFilename, remoteFilename)
        If (success <> True) Then
            Console.WriteLine(ftp.LastErrorText)
            Exit Sub
        End If

        localFilename = My.Settings.ArchivoCopia & "\Personas1.CSV"
        remoteFilename = "Personas1.csv"
        Console.WriteLine("Procesando archivo Personas1")
        success = ftp.PutFile(localFilename, remoteFilename)
        If (success <> True) Then
            Console.WriteLine(ftp.LastErrorText)
            Exit Sub
        End If

        localFilename = My.Settings.ArchivoCopia & "\Personas2.CSV"
        remoteFilename = "Personas2.csv"
        Console.WriteLine("Procesando archivo Personas2")
        success = ftp.PutFile(localFilename, remoteFilename)
        If (success <> True) Then
            Console.WriteLine(ftp.LastErrorText)
            Exit Sub
        End If

        localFilename = My.Settings.ArchivoCopia & "\Personas3.CSV"
        remoteFilename = "Personas3.csv"
        Console.WriteLine("Procesando archivo Personas3")
        success = ftp.PutFile(localFilename, remoteFilename)
        If (success <> True) Then
            Console.WriteLine(ftp.LastErrorText)
            Exit Sub
        End If

        localFilename = My.Settings.ArchivoCopia & "\Personas4.CSV"
        remoteFilename = "Personas4.csv"
        Console.WriteLine("Procesando archivo Personas4")
        success = ftp.PutFile(localFilename, remoteFilename)
        If (success <> True) Then
            Console.WriteLine(ftp.LastErrorText)
            Exit Sub
        End If

        localFilename = My.Settings.ArchivoCopia & "\Personas5.CSV"
        remoteFilename = "Personas5.csv"
        Console.WriteLine("Procesando archivo Personas5")
        success = ftp.PutFile(localFilename, remoteFilename)
        If (success <> True) Then
            Console.WriteLine(ftp.LastErrorText)
            Exit Sub
        End If

        localFilename = My.Settings.ArchivoCopia & "\Personas6.CSV"
        remoteFilename = "Personas6.csv"
        Console.WriteLine("Procesando archivo Personas6")
        success = ftp.PutFile(localFilename, remoteFilename)
        If (success <> True) Then
            Console.WriteLine(ftp.LastErrorText)
            Exit Sub
        End If

        ftp.Disconnect()
        Console.WriteLine("Archivos copiados correctamente")
    End Sub


End Module
