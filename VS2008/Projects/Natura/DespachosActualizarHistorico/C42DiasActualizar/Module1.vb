﻿Imports Excel = Microsoft.Office.Interop.Excel

Module Module1

    Sub Main()
        Dim oExcel As Excel.Application
        Dim oBook As Excel.Workbook
        Dim oBooks As Excel.Workbooks

        oExcel = CreateObject("Excel.Application")
        oExcel.Visible = True
        oBooks = oExcel.Workbooks
        oBook = oBooks.Open(My.Settings.Archivo1)
        oExcel.Run(My.Settings.Macro)
        oBook.Close(True)
        oExcel.Quit()
    End Sub

End Module
