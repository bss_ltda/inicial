﻿Imports Microsoft.VisualBasic
Imports System.IO



Namespace WebCallReports

    Public Class ExportarExcel

        Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response

        Dim fs As FileStream
        Dim w As StreamWriter
        Dim Numerotitulo As Integer
        Dim Imagen1 As String = "<td><IMG SRC=" & Chr(34) & "https://reports.callzilla.net/webcallreports/images/Logo_Callzilla.JPG" & Chr(34) & " WIDTH=200 HEIGHT=100 ></td>"
        Dim idrc As String

        Dim Palabras1 As String
        Dim Palabras As String
        Dim Titulo As String
        Dim Titulo1 As String
        Dim columan As String
        Dim html As StringBuilder = New StringBuilder()

        Dim InicioTr As String = "<Table ><tr style=" & Chr(34) & "font-weight:bold;font-size: 14px;color: black;position: fixed;" & Chr(34) & ">"
        Dim InicioTd As String = ""
        Dim FinTd As String = "</td>"
        Dim FinTr As String = "</tr></Table>"
        Dim Frase3 As String = ""
        Dim ResulatdoFrase3 As String
        'Dim columan As String

        Dim InicioTr1 As String = "<Table > <tr style=" & Chr(34) & "font-weight:bold;font-size: 12px;color: black;" & Chr(34) & ">"
        Dim InicioTd1 As String = ""
        Dim FinTd1 As String = "</td>"
        Dim FinTr1 As String = "</tr></Table>"
        Dim Frase1 As String = ""
        Dim ResulatdoFrase1 As String



        Public Function GeneraExcel(ByVal Registros As DataSet, ByVal Archivo As String, ByVal Fec As String, ByVal Fec1 As String,
                            ByVal HorI As String, ByVal HorF As String, ByVal ReporteNumero As String, ByVal Agen As String, ByVal clienteNombre As String,
                            ByVal Opcion1 As String) As String



            fs = New FileStream("C:\PDFs\" & Archivo & ".xls", FileMode.Create, FileAccess.ReadWrite)
            w = New StreamWriter(fs)

            Numerotitulo = Registros.Tables(0).Columns.Count

            'Encabezados
            PublicarEncabezado(Numerotitulo, Imagen1, Fec, Fec1, HorI, HorF, Agen, clienteNombre, Opcion1)

            'Encabezado de la Grilla
            For i = 0 To Registros.Tables(0).Columns.Count - 1

                idrc = Registros.Tables(0).Columns(i).ToString()
                Palabras = Microsoft.VisualBasic.Left(Trim(idrc), 1)
                Palabras1 = Microsoft.VisualBasic.Right(Trim(idrc), 1)

                If Palabras = "<" And Palabras1 <> ">" Then
                    Titulo1 = idrc.Trim(Trim({" "c, "<"c, "."c}))
                    Titulo = Titulo1
                    InicioTd = "<td  bgcolor= " & Chr(34) & "#808080" & Chr(34) & "  align=" & Chr(34) & "center" & Chr(34) & ">"

                ElseIf Palabras = "<" And Palabras1 = ">" Then
                    Titulo = idrc.Trim(Trim({" "c, "<"c, "."c}))
                    Titulo1 = Titulo.Trim(Trim({" "c, ">"c, "."c}))
                    Titulo = Titulo1
                    InicioTd = "<td bgcolor= " & Chr(34) & "#808080" & Chr(34) & "  align=" & Chr(34) & "center" & Chr(34) & ">"

                Else
                    Titulo = idrc
                    InicioTd = "<td bgcolor= " & Chr(34) & "white" & Chr(34) & "  align=" & Chr(34) & "center" & Chr(34) & ">"
                End If
                Frase3 = Frase3 + InicioTd + Titulo + FinTd
            Next
            ResulatdoFrase3 = InicioTr + Frase3 + FinTr
            w.Write(ResulatdoFrase3)



            'Datos de la Grilla
            For j = 0 To Registros.Tables(0).Rows.Count - 1
                ResulatdoFrase1 = ""
                Frase1 = ""
                For i = 0 To Numerotitulo - 1

                    idrc = Registros.Tables(0).Columns(i).ColumnName
                    columan = Registros.Tables(0).Rows(j).Item(i).ToString

                    Palabras = Microsoft.VisualBasic.Left(Trim(idrc), 1)
                    Palabras1 = Microsoft.VisualBasic.Right(Trim(idrc), 1)

                    If Palabras = "<" And Palabras1 <> ">" Then
                        Titulo = idrc.Trim(Trim({" "c, "<"c, "."c}))
                        InicioTd1 = "<td bgcolor= " & Chr(34) & "#808080" & Chr(34) & " align=" & Chr(34) & "center" & Chr(34) & ">"

                    ElseIf Palabras = "<" And Palabras1 = ">" Then
                        Titulo = idrc.Trim(Trim({" "c, "<"c, "."c}))
                        Titulo1 = Titulo.Trim(Trim({" "c, ">"c, "."c}))
                        Titulo = Titulo1
                        InicioTd1 = "<td  bgcolor= " & Chr(34) & "white" & Chr(34) & "  align=" & Chr(34) & "center" & Chr(34) & ">"

                    Else
                        Titulo = idrc
                        InicioTd1 = "<td  bgcolor= " & Chr(34) & "white" & Chr(34) & "  align=" & Chr(34) & "center" & Chr(34) & ">"
                    End If

                    Frase1 = Frase1 + InicioTd1 + columan + FinTd1
                Next
                ResulatdoFrase1 = InicioTr1 + Frase1 + FinTr1
                w.Write(ResulatdoFrase1)
            Next


            publicarPiePagina()


            Dim Ruta_Acceso As String = "C:\PDFs\" & Archivo & ".xls"
            response.Clear()
            response.Buffer = True
            response.ContentType = "application/vnd.ms-excel"
            response.AddHeader("Content-Disposition", "attachment;filename= " & Archivo & ".xls")
            response.Charset = "UTF-8"
            response.WriteFile(Ruta_Acceso)
            response.End()


            Return Archivo

        End Function

        Public Sub publicarPiePagina()
            html.Append("  </table>")
            html.Append("</p>")
            html.Append(" </body>")
            html.Append("</html>")
            w.Close()

        End Sub


        Public Sub PublicarEncabezado(ByVal Numerotitulo As Integer, ByVal Imagen As String, ByVal Fec As String, ByVal Fec1 As String,
                             ByVal HorI As String, ByVal HorF As String, ByVal agen As String, ByVal NombreCliente As String, ByVal Opcion1 As String)





            html.Append("<!DOCTYPE HTML PUBLIC " & Chr(34) & "-//W3C//DTD HTML 4.0 Transitional//EN" & Chr(34) & ">")
            html.Append("<html>")
            html.Append(" <head>")
            html.Append("<title>www.devjoker.com</title>")
            html.Append("<meta http-equiv=" & Chr(34) & "Content-Type" & Chr(34) & " content=" & Chr(34) & "text/html; charset=UTF-8" & Chr(34) & " />")
            html.Append(" </head>")
            html.Append("<body>")
            html.Append("<p>")
            html.Append("<table>")

            'espacio
            html.Append("<tr style=" & Chr(34) & "font-weight:bold;font-size: 12px;color: #0000FF;" & Chr(34) & ">")
            html.Append("<th colspan=" & Chr(34) & "" & Numerotitulo & "" & Chr(34) & "></th>")
            html.Append("</tr>")

            'Reports
            html.Append("<tr style=" & Chr(34) & "font-weight:bold;font-size: 36px;color: #000072;" & Chr(34) & ">")
            html.Append("<td  align=" & Chr(34) & " left " & Chr(34) & " colspan=" & Chr(34) & "" & Numerotitulo - 3 & "" & Chr(34) & "> TM " & agen & "</td>" & Imagen & "")
            html.Append("</tr>")
            'espacio
            html.Append("<tr style=" & Chr(34) & "font-weight:bold;font-size: 12px;color: #0000FF;" & Chr(34) & ">")
            html.Append("<td colspan=" & Chr(34) & "" & Numerotitulo & "" & Chr(34) & "></td>")
            html.Append("</tr>")
            'Campaign
            html.Append("<tr style=" & Chr(34) & "font-weight:bold;font-size: 36px;color: #000072;" & Chr(34) & ">")
            html.Append("<td align=" & Chr(34) & " left " & Chr(34) & " colspan=" & Chr(34) & "" & Numerotitulo & "" & Chr(34) & ">Campaign Name:      " & NombreCliente & "</td>")
            html.Append("</tr>")
            'espacio
            html.Append("<tr style=" & Chr(34) & "font-weight:bold;font-size: 12px;color: #0000FF;" & Chr(34) & ">")
            html.Append("<td colspan=" & Chr(34) & "" & Numerotitulo & "" & Chr(34) & "></td>")
            html.Append("</tr>")
            'FechaImpresion
            html.Append("<tr style=" & Chr(34) & "font-weight:bold;font-size: 15px;color: black;" & Chr(34) & ">")
            html.Append("<td align=" & Chr(34) & " left " & Chr(34) & " colspan=" & Chr(34) & "" & Numerotitulo & "" & Chr(34) & "> Print Date:      " & Now & " </td>")
            html.Append("</tr>")
            'espacio
            html.Append("<tr style=" & Chr(34) & "font-weight:bold;font-size: 12px;color: #0000FF;" & Chr(34) & ">")
            html.Append("<td colspan=" & Chr(34) & "" & Numerotitulo & "" & Chr(34) & "></td>")
            html.Append("</tr>")
            'FECHA iNI FECAH FIN
            html.Append("<tr style=" & Chr(34) & "font-weight:bold;font-size: 15px;color: black;" & Chr(34) & ">")
            html.Append("<td align=" & Chr(34) & " left " & Chr(34) & " colspan=" & Chr(34) & "" & Numerotitulo & "" & Chr(34) & ">Report Date     Initial Date:  " & Fec & " " & HorI & "  And Final Date: " & Fec1 & " " & HorF & "       " & Opcion1 & " </td>")
            html.Append("</tr>")

            'espacio
            html.Append("<tr style=" & Chr(34) & "font-weight:bold;font-size: 12px;color: #0000FF;" & Chr(34) & ">")
            html.Append("<td colspan=" & Chr(34) & "" & Numerotitulo & "" & Chr(34) & "></td>")
            html.Append("</tr>")
            'espacio
            html.Append("<tr style=" & Chr(34) & "font-weight:bold;font-size: 12px;color: #0000FF;" & Chr(34) & ">")
            html.Append("<td colspan=" & Chr(34) & "" & Numerotitulo & "" & Chr(34) & "></td>")
            html.Append("</tr>")
            'espacio
            html.Append("<tr style=" & Chr(34) & "font-weight:bold;font-size: 12px;color: #0000FF;" & Chr(34) & ">")
            html.Append("<td colspan=" & Chr(34) & "" & Numerotitulo & "" & Chr(34) & "></td>")
            html.Append("</tr>")
            html.Append("  </table>")
            w.Write(html.ToString())

        End Sub

    End Class

End Namespace
