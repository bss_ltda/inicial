﻿Imports Microsoft.VisualBasic

Imports WebCallReports
Imports iTextSharp.text.html.simpleparser
Imports iTextSharp.text.pdf
Imports iTextSharp.text
Imports iTextSharp.text.Font.FontFamily

Namespace WebCallReports
    Public Class ExportarPdf

        Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response


        Dim PaginaClave As String
        Dim NumeroColumnas As Integer
        Dim reporte As Document
        Dim htmlparser As HTMLWorker
        Dim paginas As Integer
        Dim DSReportes As DataSet
        Dim Tabla As iTextSharp.text.pdf.PdfPTable
        Dim Tabla1 As iTextSharp.text.pdf.PdfPTable
        Dim Tabla2 As iTextSharp.text.pdf.PdfPTable
        Dim Tabla3 As iTextSharp.text.pdf.PdfPTable
        Dim TablaTotal As String = "NO"
        Dim idrc As String
        Dim Palabras As String
        Dim Palabras1 As String
        Dim Titulo As String
        Dim Titulo1 As String
        Dim columan As String
        Dim h As Integer = 0
        Dim v As Integer = 0
        Dim color1 As iTextSharp.text.BaseColor
        Dim Frase As iTextSharp.text.Phrase
        Dim Celda As iTextSharp.text.pdf.PdfPCell
        Dim ColorOriginal As String
       
      


        Public Function Generapdf(ByVal Registros As DataSet, ByVal Archivo As String, ByVal Fec As String, ByVal Fec1 As String,
                         ByVal HorI As String, ByVal HorF As String, ByVal ModalReports As Integer, ByVal NombreClien As String, ByVal NombreReporte As String,
                         ByVal TipoImagen As String, ByVal Opcion1 As String) As String

            PaginaClave = "No"

            NumeroColumnas = Registros.Tables(0).Columns.Count


            If NumeroColumnas >= "0" And NumeroColumnas <= "15" Then
                PaginaClave = "B4"
            ElseIf NumeroColumnas >= "16" And NumeroColumnas <= "25" Then
                PaginaClave = "B3"
            ElseIf NumeroColumnas >= "26" And NumeroColumnas <= "35" Then
                PaginaClave = "B2"
            ElseIf NumeroColumnas >= "36" And NumeroColumnas <= "55" Then
                PaginaClave = "B1"
            ElseIf NumeroColumnas >= "56" And NumeroColumnas <= "80" Then
                PaginaClave = "B0"
            End If



            response.Clear()
            response.ContentType = "application/pdf"
            response.AddHeader("content-disposition", "attachment;filename=" & Archivo & ".pdf")
            response.Cache.SetCacheability(HttpCacheability.NoCache)

            If PaginaClave = "B0" Then
                reporte = New Document(PageSize.B0.Rotate, 10.0F, 10.0F, 30.0F, 10.0F) 'tipo carta
            ElseIf PaginaClave = "B1" Then
                reporte = New Document(PageSize.B1.Rotate, 10.0F, 10.0F, 30.0F, 10.0F) 'tipo carta
            ElseIf PaginaClave = "B2" Then
                reporte = New Document(PageSize.B2.Rotate, 10.0F, 10.0F, 30.0F, 10.0F) 'tipo carta
            ElseIf PaginaClave = "B3" Then
                reporte = New Document(PageSize.B3.Rotate, 10.0F, 10.0F, 30.0F, 10.0F) 'tipo carta
            ElseIf PaginaClave = "B4" Then
                reporte = New Document(PageSize.B4.Rotate, 10.0F, 10.0F, 30.0F, 10.0F) 'tipo carta

            End If


            htmlparser = New HTMLWorker(reporte)
            PdfWriter.GetInstance(reporte, response.OutputStream)


            reporte.Open()


            EncabezadoPdf(Fec, HorI, Fec1, HorF, PaginaClave, NombreClien, NombreReporte, TipoImagen, Opcion1)
            LlenaTitulos(Registros, NumeroColumnas, PaginaClave)

            LlenaTablas(Registros, NumeroColumnas, PaginaClave, Fec, HorI, Fec1, HorF, NombreClien, NombreReporte, TipoImagen, Opcion1, ModalReports)

            reporte.Close()
            response.Write(reporte)
            response.End()


            Return Archivo

        End Function

        

        Public Sub LlenaTablas(ByVal Registros1 As DataSet, ByVal NCol1 As Integer, ByVal paginaEspecial As String, ByVal Fec As String, ByVal HorI As String,
                                 ByVal Fec1 As String, ByVal HorF As String, ByVal NombreClien As String, ByVal NombreReporte As String, ByVal TipoImagen As String,
                                 ByVal Opcion1 As String, ByVal ReportsModal As Integer)



            paginas = 1
            v = 0

            DSReportes = Registros1


            Tabla1 = New PdfPTable(NCol1)
            Tabla1.HorizontalAlignment = Element.ALIGN_JUSTIFIED_ALL
            Tabla1.WidthPercentage = 100.0!
            NumeroColumnas = Registros1.Tables(0).Columns.Count
            Dim NCol2 As Integer
            NCol2 = NCol1

            For j = 0 To Registros1.Tables(0).Rows.Count - 1



                'If paginaEspecial = "B0" Then
                Dim NumPagina As Integer
                Dim RegistroActual As Integer = j

                NumPagina = RegistroActual + 1

                If NumPagina Mod ReportsModal = 0 Then

                    reporte.Add(Tabla1)

                    NumeroColumnas = Registros1.Tables(0).Columns.Count
                    EncabezadoPdf(Fec, HorI, Fec1, HorF, PaginaClave, NombreClien, NombreReporte, TipoImagen, Opcion1)
                    LlenaTitulos(Registros1, NumeroColumnas, paginaEspecial)

                    Tabla1 = New PdfPTable(NCol1)
                    Tabla1.HorizontalAlignment = Element.ALIGN_JUSTIFIED_ALL
                    Tabla1.WidthPercentage = 100.0!

                End If
               
                For i = 0 To NCol1 - 1


                    idrc = Registros1.Tables(0).Columns(i).ColumnName

                    columan = Registros1.Tables(0).Rows(j).Item(i).ToString
                    Palabras = Microsoft.VisualBasic.Left(Trim(idrc), 1)
                    Palabras1 = Microsoft.VisualBasic.Right(Trim(idrc), 1)

                    If Palabras = "<" And Palabras1 <> ">" Then
                        Titulo1 = idrc.Trim(Trim({" "c, "<"c, "."c}))
                        Titulo = Titulo1
                        color1 = BaseColor.WHITE
                        ColorOriginal = "WHITE"
                    ElseIf Palabras = "<" And Palabras1 = ">" Then
                        Titulo = idrc.Trim(Trim({" "c, "<"c, "."c}))
                        Titulo1 = Titulo.Trim(Trim({" "c, ">"c, "."c}))
                        Titulo = Titulo1
                        color1 = BaseColor.WHITE
                        ColorOriginal = "WHITE"
                    Else
                        Titulo = idrc
                        color1 = BaseColor.WHITE
                        ColorOriginal = "WHITE"
                    End If



                    If columan = "Total:" Then
                        h = j
                        v = i


                        'For h = j To DSReportes.Tables(0).Rows.Count - 1
                        For v = 0 To NCol2 - 1

                            idrc = DSReportes.Tables(0).Columns(v).ColumnName
                            columan = Registros1.Tables(0).Rows(h).Item(v).ToString
                            Palabras = Microsoft.VisualBasic.Left(Trim(idrc), 1)
                            Palabras1 = Microsoft.VisualBasic.Right(Trim(idrc), 1)

                            If Palabras = "<" And Palabras1 <> ">" Then
                                Titulo1 = idrc.Trim(Trim({" "c, "<"c, "."c}))
                                Titulo = Titulo1
                                color1 = BaseColor.WHITE
                            ElseIf Palabras = "<" And Palabras1 = ">" Then
                                Titulo = idrc.Trim(Trim({" "c, "<"c, "."c}))
                                Titulo1 = Titulo.Trim(Trim({" "c, ">"c, "."c}))
                                Titulo = Titulo1
                                color1 = BaseColor.WHITE
                            Else
                                Titulo = idrc
                                color1 = BaseColor.WHITE
                            End If

                           

                            Frase = New Phrase(columan, FontFactory.GetFont("Tahoma", 10, Font.BOLD, BaseColor.BLACK))
                            Celda = New PdfPCell(Frase)
                            Celda.NoWrap = False
                            Celda.HorizontalAlignment = Element.ALIGN_CENTER
                            Celda.VerticalAlignment = Element.ALIGN_MIDDLE
                            Celda.BackgroundColor = color1
                            Celda.Border = Rectangle.TOP_BORDER
                            Celda.BorderWidth = 2
                            Tabla1.AddCell(Celda)

                            ' Next
                        Next

                        j = j + 1
                        If j <= Registros1.Tables(0).Rows.Count - 1 Then

                            i = -1  'Registros1.Tables(0).Columns.Count
                        Else
                            i = NCol1

                        End If
                    Else



                        If j Mod 2 = 0 Then
                            If ColorOriginal = "WHITE" Then
                                color1 = New CMYKColor(100, 50, 0, 0)
                            End If
                        End If




                        Frase = New Phrase(columan, FontFactory.GetFont("Tahoma", 8, BaseColor.BLACK))
                        Celda = New PdfPCell(Frase)
                        Celda.NoWrap = False

                        Celda.HorizontalAlignment = Element.ALIGN_CENTER
                        Celda.VerticalAlignment = Element.ALIGN_MIDDLE
                        Celda.BackgroundColor = color1
                        Celda.Border = 0
                        Tabla1.AddCell(Celda)





                        End If
                Next
            Next

            reporte.Add(Tabla1)

        End Sub

        'EncabezaPdf
        Public Sub EncabezadoPdf(ByVal Fec As String, ByVal HorI As String,
                                 ByVal Fec1 As String, ByVal HorF As String, ByVal paginaEspecial As String, ByVal NombreCliente As String, ByVal NombreReporte As String,
                                 ByVal TipoImagen As String, ByVal Opcion1 As String)


            reporte.NewPage()

            'TABLA DE TITULO 
            Tabla = New PdfPTable(1)
            Tabla.Size.ToString()
            Tabla.HorizontalAlignment = Element.ALIGN_JUSTIFIED_ALL
            Tabla.WidthPercentage = 100.0!



            '  If paginaEspecial = "B4" Then
            Frase = New Phrase("TM " & NombreCliente, FontFactory.GetFont("Tahoma", 15, Font.BOLD, New CMYKColor(10.0F, 60.0F, 0.0F, 0.0F)))
            'Else
            'Frase = New Phrase("TM " & NombreCliente, FontFactory.GetFont(BaseFont.TIMES_BOLD, 20, New CMYKColor(10.0F, 60.0F, 0.0F, 0.0F)))
            'End If
            Celda = New PdfPCell(Frase)
            Celda.Border = 0
            Tabla.AddCell(Celda)
            reporte.Add(Tabla)

            ''imagen

            Dim imagen As iTextSharp.text.Image 'declaración de imagen
            imagen = iTextSharp.text.Image.GetInstance(TipoImagen) 'nombre y ruta de la imagen a insertar

            If paginaEspecial = "B4" Then
                imagen.ScalePercent(45.0F)
                imagen.SetAbsolutePosition(800, 638)

            ElseIf paginaEspecial = "B0" Then
                imagen.ScalePercent(45.0F)
                imagen.SetAbsolutePosition(3750, 2738)

            ElseIf paginaEspecial = "B1" Then
                imagen.ScalePercent(45.0F)
                imagen.SetAbsolutePosition(2600, 1938) 'en la que se inserta. 40 (de izquierda a derecha). 500 (de abajo hacia arriba)

            ElseIf paginaEspecial = "B2" Then
                imagen.ScalePercent(45.0F)
                imagen.SetAbsolutePosition(1700, 1338)

            ElseIf paginaEspecial = "B3" Then
                imagen.ScalePercent(45.0F)
                imagen.SetAbsolutePosition(1200, 938)

            End If

            reporte.Add(imagen)


            'TABLA DE Campaing
            Tabla = New PdfPTable(1)
            Tabla.HorizontalAlignment = Element.ALIGN_JUSTIFIED_ALL
            Tabla.WidthPercentage = 100.0!
            Tabla.SetWidths(New Single() {10.0!})

            '  If paginaEspecial = "B4" Then
            Frase = New Phrase("Campaign Name:  " + NombreReporte + "", FontFactory.GetFont("Tahoma", 15, Font.BOLD, New CMYKColor(10.0F, 60.0F, 0.0F, 0.0F)))
            '    Else
            'Frase = New Phrase("Campaign Name:      " + NombreReporte + "", FontFactory.GetFont("Tahoma", 30, New CMYKColor(10.0F, 60.0F, 0.0F, 0.0F)))
            '    End If
            Celda = New PdfPCell(Frase)
            Celda.Border = 0
            Tabla.AddCell(Celda)
            reporte.Add(Tabla)


            'Espacio 
            Tabla = New PdfPTable(1)
            Tabla.HorizontalAlignment = Element.ALIGN_JUSTIFIED_ALL
            Tabla.WidthPercentage = 100.0!
            Tabla.SetWidths(New Single() {10.0!})

            Frase = New Phrase(" ", FontFactory.GetFont("Tahoma", 10, New CMYKColor(10.0F, 60.0F, 0.0F, 0.0F)))
            Celda = New PdfPCell(Frase)
            Celda.Border = 0
            Tabla.AddCell(Celda)
            reporte.Add(Tabla)

            'Informacion de ejecucion
            Tabla = New PdfPTable(1)
            Tabla.HorizontalAlignment = Element.ALIGN_JUSTIFIED_ALL
            Tabla.WidthPercentage = 100.0!
            Tabla.SetWidths(New Single() {10.0!})

            'If paginaEspecial = "B4" Then
            Frase = New Phrase("Print Date: " + Now + "", FontFactory.GetFont("Tahoma", 10, Font.BOLD, New CMYKColor(0.0F, 0.0F, 0.0F, 10.0F)))
            '    Else
            'Frase = New Phrase("Print Date:      " + Now + "", FontFactory.GetFont("Tahoma", 15, New CMYKColor(0.0F, 0.0F, 0.0F, 10.0F)))
            '    End If
            Celda = New PdfPCell(Frase)
            Celda.Border = 0
            Tabla.AddCell(Celda)
            reporte.Add(Tabla)


            'Informacion de ejecucion
            Tabla = New PdfPTable(1)
            Tabla.HorizontalAlignment = Element.ALIGN_JUSTIFIED_ALL
            Tabla.WidthPercentage = 100.0!
            Tabla.SetWidths(New Single() {10.0!})

            '   If paginaEspecial = "B4" Then
            Frase = New Phrase("Report Date     Initial Date:  " + Fec + " " + HorI + "  And Final Date: " + Fec1 + " " + HorF + "      " + Opcion1 + "", _
                                   FontFactory.GetFont("Tahoma", 10, Font.BOLD, New CMYKColor(0.0F, 0.0F, 0.0F, 10.0F)))
            '    Else
            'Frase = New Phrase("Report Date     Initial Date:  " + Fec + " " + HorI + "  And Final Date: " + Fec1 + " " + HorF + "      " + Opcion1 + "", _
            '                       FontFactory.GetFont("Tahoma", 15, New CMYKColor(0.0F, 0.0F, 0.0F, 10.0F)))
            'End If

            Celda = New PdfPCell(Frase)
            Celda.Border = 0
            Tabla.AddCell(Celda)
            reporte.Add(Tabla)

            'Espacio 
            Tabla = New PdfPTable(1)
            Tabla.HorizontalAlignment = Element.ALIGN_JUSTIFIED_ALL
            Tabla.WidthPercentage = 100.0!
            Tabla.SetWidths(New Single() {10.0!})

            Frase = New Phrase(" ", FontFactory.GetFont("Tahoma", 20, New CMYKColor(10.0F, 60.0F, 0.0F, 0.0F)))
            Celda = New PdfPCell(Frase)
            Celda.Border = 0
            Tabla.AddCell(Celda)
            reporte.Add(Tabla)


        End Sub


        Public Sub LlenaTitulos(ByVal Datos As DataSet, ByVal NCol As Integer, ByVal paginaEspecial As String)


            Tabla = New PdfPTable(NCol)
            Tabla.HorizontalAlignment = Element.ALIGN_JUSTIFIED_ALL
            Tabla.WidthPercentage = 100.0!

            For i = 0 To Datos.Tables(0).Columns.Count - 1

                idrc = Datos.Tables(0).Columns(i).ToString()
                Palabras = Microsoft.VisualBasic.Left(Trim(idrc), 1)
                Palabras1 = Microsoft.VisualBasic.Right(Trim(idrc), 1)

                If Palabras = "<" And Palabras1 <> ">" Then
                    Titulo1 = idrc.Trim(Trim({" "c, "<"c, "."c}))
                    Titulo = Titulo1
                    color1 = BaseColor.LIGHT_GRAY
                ElseIf Palabras1 = ">" And Palabras1 = ">" Then
                    Titulo = idrc.Trim(Trim({" "c, "<"c, "."c}))
                    Titulo1 = Titulo.Trim(Trim({" "c, ">"c, "."c}))
                    Titulo = Titulo1
                    color1 = BaseColor.LIGHT_GRAY
                Else
                    Titulo = idrc
                    color1 = BaseColor.WHITE
                End If


                Frase = New Phrase(Titulo, FontFactory.GetFont("Tahoma", 10, Font.BOLD, BaseColor.BLACK))
                Celda = New PdfPCell(Frase)
                Celda.NoWrap = False
                Celda.HorizontalAlignment = Element.ALIGN_CENTER
                Celda.VerticalAlignment = Element.ALIGN_MIDDLE
                Celda.BackgroundColor = color1 
                Celda.Border = 0
                Tabla.AddCell(Celda)


            Next
            reporte.Add(Tabla)


        End Sub


    End Class

End Namespace
