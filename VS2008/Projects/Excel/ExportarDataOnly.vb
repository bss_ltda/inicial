﻿Imports Microsoft.VisualBasic
Imports System.IO


Namespace WebCallReports
    Public Class ExportarDataOnly

        Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response

        Dim sb As StringBuilder
        Dim sw As StringWriter
        Dim htw As HtmlTextWriter
        Dim pagina As New Page
        Dim grilla As New GridView
        Dim NombreCompleta As String


        Public Function DATA_ONLY(ByVal Registros As DataSet, ByVal Archivo As String, ByVal Fec As String, ByVal Fec1 As String) As String


            Dim form As HtmlForm

            sb = New StringBuilder()
            sw = New StringWriter(sb)
            htw = New HtmlTextWriter(sw)
            form = New HtmlForm
            NombreCompleta = Archivo + "_" + Fec + "_" + Fec1

            ' grilla.EnableViewState = False
            grilla.DataSource = Registros
            grilla.DataBind()
            pagina.EnableEventValidation = False
            pagina.DesignerInitialize()
            pagina.Controls.Add(form)
            form.Controls.Add(grilla)
            pagina.RenderControl(htw)

            response.Clear()
            response.Buffer = True
            response.ContentType = "application/vnd.ms-excel"
            response.AddHeader("Content-Disposition", "attachment;filename= " & NombreCompleta & ".xls")
            response.Charset = "UTF-8"
            response.ContentEncoding = Encoding.Default
            response.Write(sb.ToString())
            response.End()

            '  Return pagina
            Return Archivo
        End Function
    End Class

End Namespace
