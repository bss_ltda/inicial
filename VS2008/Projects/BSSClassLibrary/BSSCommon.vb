﻿Option Strict Off
Option Explicit On

Imports System.Globalization

Module BSSCommon

    Public Const tNum As Integer = 1
    Public Const tStr As Integer = 2
    Public Const t_iF As Integer = 3
    Public Const t_iV As Integer = 4
    Public Const tFil As Integer = 5

    Public Sql As New mk_Sql
    Public gsConnstring As String
    Public gsConnstringWMS As String
    Public gsConnstringMP2 As String
    Public sLIB As String
    Public sUser As String
    Public bBatchApp As Boolean
    Public sVersion As String
    Public Descripcion_Error As String
    Public Error_Tabla As String

    Public Const DB2_DATEDEC = " ( YEAR( NOW() ) * 10000 + MONTH( NOW()  ) * 100 + DAY( NOW() ) )"
    Public Const DB2_TIMEDEC = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    Public Const DB2_TIMEDEC0 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    Public Const DB2_TIMEDEC1 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 ) "

    Public pCultureInfo As New CultureInfo("th-TH")
    Public LibParamCollection As New Dictionary(Of String, String)
    Public Sesiones As Integer

    Function ParametrosConexion(sDataSource As String, sBiblioteca As String, Optional qUser As String = "", Optional qPass As String = "") As Boolean
        Dim gsProvider As String
        Dim gsDatasource As String
        Dim DB_LOC As New ADODB.Connection
        Dim rs As New ADODB.Recordset
        Dim AS400Param(5) As String
        Dim wmsParam(5) As String
        Dim mp2Param(5) As String
        Dim Resultado As Boolean = True
        Dim i As Integer

        gsProvider = "IBMDA400"
        gsDatasource = sDataSource
        sLIB = sBiblioteca

        gsConnstring = "Provider=" & gsProvider & ";"
        gsConnstring &= "Data Source=" & sDataSource & ";"
        gsConnstring &= "User ID=APLLX;"
        gsConnstring &= "Password=LXAPL;"
        gsConnstring &= "Persist Security Info=True;"
        gsConnstring &= "Default Collection=" & sBiblioteca & ";"
        gsConnstring &= "Force Translate=0;"
        gsConnstring &= "Convert Date Time To Char=FALSE;"

        rs.Open(" SELECT * FROM " & sBiblioteca & ".ZCC WHERE CCTABL = 'LXLONG'", DB_LOC)
        Do While Not rs.EOF
            Select Case rs("CCCODE").Value
                Case "LXUSER"
                    AS400Param(0) = rs("CCDESC").Value
                Case "LXPASS"
                    AS400Param(1) = rs("CCDESC").Value
                Case "MP2SERV"
                    mp2Param(0) = rs("CCDESC").Value
                Case "MP2CATALOG"
                    mp2Param(1) = rs("CCDESC").Value
                Case "MP2USER"
                    mp2Param(2) = rs("CCDESC").Value
                Case "MP2PASS"
                    mp2Param(3) = rs("CCDESC").Value
                Case "WMSSERV"
                    wmsParam(0) = rs("CCDESC").Value
                Case "WMSCATALOG"
                    wmsParam(1) = rs("CCDESC").Value
                Case "WMSUSER"
                    wmsParam(2) = rs("CCDESC").Value
                Case "WMSPASS"
                    wmsParam(3) = rs("CCDESC").Value
            End Select
            rs.MoveNext()
        Loop
        rs.Close()

        For i = 0 To 1
            If AS400Param(i) = "" Then
                Resultado = False
            End If
        Next

        If qUser = "" Then
            gsConnstring = "Provider=" & gsProvider & ";"
            gsConnstring &= "Data Source=" & sDataSource & ";"
            gsConnstring &= "User ID=" & AS400Param(0) & ";"
            gsConnstring &= "Password=" & AS400Param(1) & ";"
            gsConnstring &= "Persist Security Info=True;"
            gsConnstring &= "Default Collection=" & sBiblioteca & ";"
            gsConnstring &= "Force Translate=0;"
            gsConnstring &= "Convert Date Time To Char=FALSE;"
        Else
            gsConnstring = "Provider=" & gsProvider & ";"
            gsConnstring &= "Data Source=" & sDataSource & ";"
            gsConnstring &= "User ID=" & qUser & ";"
            gsConnstring &= "Password=" & qPass & ";"
            gsConnstring &= "Persist Security Info=True;"
            gsConnstring &= "Default Collection=" & sBiblioteca & ";"
            gsConnstring &= "Force Translate=0;"
            gsConnstring &= "Convert Date Time To Char=FALSE;"
        End If

        For i = 0 To 3
            If mp2Param(i) = "" Then
                Resultado = False
            End If
        Next

        gsConnstringMP2 = "Provider=SQLOLEDB.1;"
        gsConnstringMP2 &= "Data Source=" & mp2Param(0) & ";"
        gsConnstringMP2 &= "User ID=" & mp2Param(2) & ";"
        gsConnstringMP2 &= "Password=" & mp2Param(3) & ";"
        gsConnstringMP2 &= "Persist Security Info=True;"
        gsConnstringMP2 &= "Initial Catalog=" & mp2Param(1) & ";"

        For i = 0 To 3
            If wmsParam(i) = "" Then
                Resultado = False
            End If
        Next

        gsConnstringWMS = "Provider=SQLOLEDB.1;"
        gsConnstringWMS &= "Data Source=" & wmsParam(0) & ";"
        gsConnstringWMS &= "User ID=" & wmsParam(2) & ";"
        gsConnstringWMS &= "Password=" & wmsParam(3) & ";"
        gsConnstringWMS &= "Persist Security Info=True;"
        gsConnstringWMS &= "Initial Catalog=" & wmsParam(1) & ";"

        DB_LOC.Close()

        Return Resultado

    End Function

    Public Enum ChkVerApp
        AppOK = 1
        AppVersionIncorrect = 2
        AppSeparadorDecimal = 3
        AppDeshabilitada = 4
        AppEjecutando = 5
    End Enum

    Function CodeCB(ByRef valor As String, Optional ByRef sep As String = "") As String
        Dim aCode As Object

        aCode = Split(valor, IIf(sep = "", " ", sep))
        CodeCB = aCode(0)

    End Function

    Function DescCB(ByRef valor As String, Optional ByRef sep As String = "") As String
        Dim aCode As Object

        aCode = Split(valor, IIf(sep = "", " ", sep))
        DescCB = Trim(Mid(valor, Len(aCode(0)) + 1))

    End Function

    Public Function Busca(ByVal Sql As String, ByVal DB As ADODB.Connection, Optional ByRef Retorna As String = "") As Boolean
        Dim rs As New ADODB.Recordset
        Dim r As Boolean
        rs.Open(Sql, DB)
        If Not rs.EOF Then
            If Retorna <> "" Then
                Retorna = CStr(rs.Fields(0).Value)
            End If
        End If
        r = Not rs.EOF
        rs.Close()
        Return r

    End Function

    Function DateCharDec(ByVal Fecha As Date) As String
        Return Fecha.ToString("yyyyMMdd")
    End Function

    Function TimeCharDec(ByVal Fecha As Date) As String
        Return Fecha.ToString("HHnnss")
    End Function

    Function dtAS400(ByVal fecha As Date) As String
        Return fecha.ToString("yyyy-MM-dd")
    End Function

    Public Function CheckVer(ByVal Aplicacion As String, DB As ExecuteSQL) As Boolean
        Dim rs As New ADODB.Recordset
        Dim lVer, locVer As String
        Dim msg As String
        Dim NomApp As String
        Dim Result As Boolean = False

        rs = New ADODB.Recordset

        lVer = "No hay registro"

        locVer = "(" & My.Application.Info.Version.Major & "." & _
         My.Application.Info.Version.Minor & "." & _
         My.Application.Info.Version.Revision & ")"

        If DB.Cursor(rs, "SELECT CCSDSC, CCUDC1, CCNOT2, CCDESC  FROM ZCCL01 WHERE CCTABL='RFVBVER' AND CCCODE = '" & Aplicacion & "'") Then
            lVer = rs.Fields("CCSDSC").Value
            NomApp = rs.Fields("CCDESC").Value
            If InStr(lVer, "*NOCHK") > 0 Then
                Result = True
            ElseIf InStr(lVer, locVer) > 0 Then
                Result = True
            End If
            If Result And rs.Fields("CCUDC1").Value = 0 Then
                msg = "La aplicacion " & Aplicacion & " no se puede usar en este momento." & vbCr
                msg = msg & "Razon: " & vbCr
                msg = msg & "=========================================" & vbCr
                If rs.Fields("CCNOT2").Value = "" Then
                    msg = msg & "Aplicacion en Mantenimiento." & vbCr
                Else
                    msg = msg & rs.Fields("CCNOT2").Value & vbCr
                End If
                msg = msg & "=========================================" & vbCr
                If Not bBatchApp Then
                    MsgBox(msg, vbExclamation, "Seguimiento a Pedidos")
                Else
                    Console.Write(msg)
                    rs.Close()
                    Return False
                End If
                rs.Close()
            End If

            If Not Result Then
                If Not bBatchApp Then
                    MsgBox("Aplicacion " & Aplicacion & vbCr & _
                           "=========================================" & vbCr & _
                           "Versión incorrecta del programa." & vbCr & vbCr & _
                           "Versión Registrada:" & vbTab & lVer & vbCr & vbCr & _
                           "Versión Actual:" & vbTab & locVer & vbCr & vbCr & _
                           "Comuníquese con Tecnología.")
                Else
                    Console.Write("Aplicacion " & Aplicacion & vbCr & _
                           "=========================================" & vbCr & _
                           "Versión incorrecta del programa." & vbCr & vbCr & _
                           "Versión Registrada:" & vbTab & lVer & vbCr & vbCr & _
                           "Versión Actual:" & vbTab & locVer & vbCr & vbCr & _
                           "Comuníquese con Tecnología.")
                End If
            End If
        End If

        Return Result

    End Function

    Function Acceso(ByVal App As String, ByVal Usuario As String, DB As ExecuteSQL) As Boolean
        Dim rs As New ADODB.Recordset
        Dim Result As Boolean

        Result = DB.Cursor(rs, "SELECT CCCODE FROM ZCCL01 WHERE CCTABL='" & App & "' AND CCCODE='" & UCase(Usuario) & "'")
        rs.Close()
        Return Result

    End Function

    Function ParmItem(ByRef arreglo() As String, ByVal tag As String, ByVal valor As String) As String
        Dim i As Integer = UBound(arreglo)
        ReDim Preserve arreglo(i + 1)
        arreglo(i) = tag & ": " & valor & vbCrLf
        Return tag & ": " & valor & vbCrLf
    End Function

    Function LibParam(prm As String, sep As String) As String
        Dim Result As String = ""

        If sep = "" Then
            Result = LibParamCollection(prm)
        Else
            Result = LibParamCollection(prm) & sep & prm
        End If
        Return Result

    End Function

    Function LibParam(prm) As String
        Dim Result As String

        If prm <> "PGMS" Then
            Result = LibParamCollection(prm) & "." & prm
        Else
            Result = LibParamCollection(prm) & "/"
        End If
        Return Result

    End Function

    Function LibParamLoad(DB As ExecuteSQL) As String
        Dim rs As New ADODB.Recordset
        Dim k As String = ""
        Dim result As String = ""

        If DB.Cursor(rs, "SELECT DISTINCT CCCODE, CCDESC FROM ZCCL01 WHERE CCTABL = 'WMSPARM'") = -99 Then
            Return Descripcion_Error
        End If
        Do While Not rs.EOF
            Try
                k = ""
                k = LibParamCollection(rs("CCCODE").Value)
            Catch ex As Exception
                Debug.Print(Err.Number)
                Debug.Print(Err.Description)
                result += Err.Description & "|"
                If Err.Number = 5 Then
                    k = ""
                End If
            End Try

            If k = "" Then
                LibParamCollection.Add(rs("CCCODE").Value, rs("CCDESC").Value)
            End If
            rs.MoveNext()
        Loop
        rs.Close()
        Return result

    End Function

    
    Sub SetConexion(s As String, u As String, p As String, b As String, ByRef DB As ExecuteSQL)
        DB.setConnectionString("IBMDA400.DataSource", s, u, p, b, "")
    End Sub

    Sub SetConexion(ByRef DB As ExecuteSQL)
        DB.setConnectionString("IBMDA400.DataSource", My.Settings.SERVER, My.Settings.USER, My.Settings.PASSWORD, My.Settings.LIBRARY, "")
        'DB.setConnectionString("IBMDA400.DataSource", "192.168.10.1", "APLDES", "OKMNJI9", "DESBPCSF", "")
    End Sub

    Function Valor(txt As String) As Double
        If pCultureInfo.NumberFormat.NumberDecimalSeparator = "." Then
            Valor = CDbl(Replace(txt, ",", "."))
        Else
            Valor = CDbl(Replace(txt, ".", ","))
        End If
    End Function
    Sub Delay(ByVal dblSecs As Double)
        Dim CurrentTime As DateTime = Now
        Do While DateDiff("s", Now, CurrentTime) < dblSecs
        Loop
    End Sub

    Function Ceros(val As Object, C As Integer)
        Dim sCeros As String = New String("0", C)

        If CInt(C) > Len(Trim(val)) Then
            Ceros = Right(sCeros & Trim(val), C)
        Else
            Ceros = val
        End If

    End Function

End Module
