﻿
Public Class ExecuteSQL
    Dim Conn As New ADODB.Connection
    Dim Sentencia As String
    Dim bLog As Boolean = False
    Dim iNumLog As Integer
    Dim regAfectados As Integer
    Dim gsConnString As String

    Public Function Open() As Boolean
        Try
            If (Conn.State = 0) Then
                Conn.Open(gsConnString)
                Sesiones = 1
            Else
                Sesiones += 1
            End If
            SetNumLog()
            crtLog(iNumLog.ToString, iNumLog.ToString)
            Return (Conn.State = 1)
        Catch
            Descripcion_Error = Err.Description
            wrtLog(Err.Description, iNumLog)
            Return False
        End Try

    End Function

    Public Sub Close()
        If Sesiones = 1 Then
            Sesiones = 0
            Conn.Close()
        Else
            Sesiones -= 1
        End If
    End Sub

    Public Function State() As Integer
        Return Conn.State
    End Function

    Public Function DspErr() As String
        Return Error_Tabla & " " & Sentencia & " " & vbCrLf & Descripcion_Error
    End Function

    Public Function ExecuteSQL(ByVal sql As String) As Integer

        Try
            Sentencia = sql
            wrtLog(sql, iNumLog)
            Conn.Execute(sql, regAfectados)
        Catch
            'MsgBox(Err.Description)
            'Debug.Print(sql)
            If InStr(sql, "QTEMP.CIMWMS") = 0 Then
                Descripcion_Error = Err.Description
                wrtLog(Err.Description, iNumLog)
                Return -99
            ElseIf InStr(Err.Description, "SQL0601") = 0 Then
                Descripcion_Error = Err.Description
                wrtLog(Err.Description, iNumLog)
                Return -99
            End If
        End Try

        Return regAfectados

    End Function

    Public Function LastSQL() As String
        Return Sentencia
    End Function

    Public Function Actualizados() As Integer
        Return regAfectados
    End Function

    Public Function Cursor(ByRef rs As ADODB.Recordset, ByVal sql As String) As Integer

        Dim Result As Integer
        Sentencia = sql
        Try
            rs.Open(sql, Conn)
        Catch
            Descripcion_Error = Sentencia & " " & Err.Description
            wrtLog(Err.Description, iNumLog)
            Return -99
        End Try
        Result = IIf(rs.EOF, 0, 1)
        Return Result

    End Function

    Public Function Cursor(ByRef rs As ADODB.Recordset) As Integer
        Dim Result As Integer
        Try
            rs.Open(Sentencia, Conn)
        Catch
            Descripcion_Error = Sentencia & " " & Err.Description
            wrtLog(Err.Description, iNumLog)
            Return -99
        End Try
        Result = IIf(rs.EOF, 0, 1)
        Return Result
    End Function

    Public Function CalcConsec(ByRef sID As String, ByRef TopeMax As String) As String
        Dim rs As New ADODB.Recordset
        Dim locID As String
        Dim sql As String
        Dim sConsec As String = ""
        Dim tMax As Double
        Dim sFmt As String
        Dim nDig As Integer

        Try

            nDig = Len(TopeMax)
            sFmt = New String("0", nDig)

            sql = "SELECT CCDESC FROM ZCCL01 WHERE CCTABL = 'SECUENCE' AND CCCODE='" & sID & "'"
            tMax = Val(TopeMax)

            With rs
                .CursorLocation = ADODB.CursorLocationEnum.adUseServer
                .Open(sql, Conn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)
                If Not (.BOF And .EOF) Then
                    locID = Val(.Fields("CCDESC").Value) + 1
                    If locID > tMax Then
                        locID = 1
                    End If
                    sConsec = Right(sFmt & locID, nDig)
                    .Fields("CCDESC").Value = sConsec
                    .Update()
                    .Close()
                Else
                    locID = 1
                    .Close()
                    sConsec = Right(sFmt & locID, nDig)
                    Conn.Execute(" INSERT INTO ZCC (CCID, CCTABL, CCCODE, CCDESC ) " & " VALUES( 'CC', 'SECUENCE', '" & sID & "', '" & sConsec & "' )")
                End If
            End With

        Catch ex As Exception
            Descripcion_Error = Sentencia & " " & Err.Description
            wrtLog(Err.Description, iNumLog)
            Return -99
        End Try

        Return sConsec


    End Function

    Sub setConnectionString(g As String)
        gsConnString = g
    End Sub

    Public Function CalcConsecV(ByRef sID As String, ByRef TopeMax As String) As Double
        Return CDbl(CalcConsec(sID, TopeMax))
    End Function

    Public Function Call_PGM(ByVal Pgm As String) As String
        Dim r As Integer = 0

        Try
            Sentencia = Pgm
            wrtLog(Pgm, iNumLog)
            Conn.Execute(" {{ " & Pgm & " }} ", r)
        Catch
            'MsgBox(Err.Description)
            'Debug.Print(sql)
            Return Err.Description
        End Try

        Return "OK"

    End Function

    'Sub WrtLog(sql As String)
    '    Dim fSql, vSql As String

    '    If bLog Then
    '        fSql = " INSERT INTO BFLOG( "
    '        vSql = " VALUES ( "
    '        fSql = fSql & " LNUM      , " : vSql = vSql & " " & iNumLog & ", "
    '        fSql = fSql & " LLOG      ) " : vSql = vSql & "'" & Replace(sql, "'", "''") & "' )"
    '        DB.Execute(fSql & vSql)
    '    End If

    'End Sub

    Function SetNumLog() As Integer
        iNumLog = CInt(CalcConsec("WMSLOG", "99"))
        Return iNumLog
    End Function

    Public Function GetNumLog() As Integer
        Return iNumLog
    End Function


End Class

