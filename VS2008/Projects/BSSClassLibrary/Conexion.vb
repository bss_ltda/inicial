﻿Public Class Conexion

    Public Const DB2_DATEDEC = " ( YEAR( NOW() ) * 10000 + MONTH( NOW()  ) * 100 + DAY( NOW() ) )"
    Public Const DB2_TIMEDEC = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    Public Const DB2_TIMEDEC0 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    Public Const DB2_TIMEDEC1 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 ) "

    Private local As String
    Private gsAS400 As String = "192.168.10.1"
    Private gsLib As String
    Private sUser As String
    Private sUserApl As String
    Private sUserPass As String
    Private conn As New ADODB.Connection

    Public Sentencia As String
    Public bLog As Boolean = False
    Public iNumLog As Integer
    Dim msgErr As String
    Dim gsConnString As String
    Dim rAfectados As Integer

    Public Function Open() As Boolean

        Try
            Conn.CommandTimeout = 120
            Conn.Open(gsConnString)
        Catch
            Descripcion_Error = Err.Description
            msgErr = Err.Description
            wrtLog(Err.Description, iNumLog)
            Return False
        End Try
        Return True
    End Function

    Public Sub Close()
        Conn.Close()
    End Sub

    Public Function State() As Integer
        ' Return Conn.State
        Return 0
    End Function

    Public Function DspErr() As String
        Return Error_Tabla & " " & Sentencia & " " & vbCrLf & Descripcion_Error
    End Function

    Public Function ExecuteSQL(ByVal sql As String) As ADODB.Recordset
        Try
            Sentencia = sql
            Return Conn.Execute(sql, rAfectados)
        Catch
            Descripcion_Error = Err.Description
            Return Nothing
        End Try

    End Function

    Public Function rs(ByVal sql As String) As ADODB.Recordset
        Dim rsT As New ADODB.Recordset
        rsT.Open(sql, Conn)
        Return rsT
    End Function

    Public Function Cursor(ByRef rs As ADODB.Recordset, ByVal sql As String) As Integer

        Dim Result As Integer
        Sentencia = sql
        Try
            rs.Open(sql, Conn)
        Catch
            Descripcion_Error = Sentencia & " " & Err.Description
            wrtLog(Err.Description, iNumLog)
            Return -99
        End Try
        Result = IIf(rs.EOF, 0, 1)
        Return Result

    End Function

    Public Function Cursor(ByRef rs As ADODB.Recordset) As Integer
        Dim Result As Integer
        Try
            rs.Open(Sentencia, Conn)
        Catch
            Descripcion_Error = Sentencia & " " & Err.Description
            wrtLog(Err.Description, iNumLog)
            Return -99
        End Try
        Result = IIf(rs.EOF, 0, 1)
        Return Result
    End Function



    Public Function CalcConsec(ByRef sID As String, ByRef TopeMax As String) As String
        Return ""
    End Function
    Public Function Call_PGM(ByVal Pgm As String, ByVal pais As String) As Integer
        Dim r As Integer = 0
        'Dim Conn As 
        Try
            'Conn.Execute(" {{ " & Pgm & " }} ", r)
            Return 0
        Catch
            msgErr = Err.Description
            Return -99
        End Try



    End Function
    Public Function ShowError() As String
        Return msgErr
    End Function
    Sub setConnectionString(ByVal Prov As String, ByVal srv As String, ByVal usr As String, ByVal pass As String, ByVal sLib As String, ByVal sCatalogo As String)
        'Dim g As String
        'g = "Data Source=192.168.10.1;Default Collection=DESLX834F;Persist Security Info=True;User ID=DESLXSSA;Password=ABR2013"

        Dim g As String

        g = ""
        g = g & "DRIVER=Client Access ODBC Driver (32-bit); "
        g = g & "UID=" & usr & ";"
        g = g & "PWD=" & pass & ";"
        g = g & "SYSTEM=" & srv
        gsConnString = g
        wrtLog(g, iNumLog)


    End Sub

    Public Function CalcConsecV(ByRef sID As String, ByRef TopeMax As String) As Double
        Return CDbl(CalcConsec(sID, TopeMax))
    End Function


    'Sub  wrtLog(sql As String)
    '    Dim fSql, vSql As String

    '    If bLog Then
    '        fSql = " INSERT INTO BFLOG( "
    '        vSql = " VALUES ( "
    '        fSql = fSql & " LNUM      , " : vSql = vSql & " " & iNumLog & ", "
    '        fSql = fSql & " LLOG      ) " : vSql = vSql & "'" & Replace(sql, "'", "''") & "' )"
    '        DB.Execute(fSql & vSql)
    '    End If

    'End Sub

    Function SetNumLog() As Integer
        iNumLog = CInt(CalcConsec("WMSLOG", "99"))
        Return iNumLog
    End Function

    Public Function GetNumLog() As Integer
        Return iNumLog
    End Function


End Class

