﻿Option Strict Off
Option Explicit On
Module BSSCommon

    Public Enum tDato
        tNum = 1
        tStr = 2
        tFunc = 3
    End Enum

    Public Enum ChkVerApp
        AppOK = 1
        AppVersionIncorrect = 2
        AppSeparadorDecimal = 3
        AppDeshabilitada = 4
        AppEjecutando = 5
    End Enum
    Public fSql As String
    Public vSql As String


    Public Function CalcConsec(ByRef sID As String, ByRef bLee As Boolean, ByRef TopeMax As String) As String
        Dim rs As ADODB.Recordset
        Dim locID As String
        Dim sql As String
        Dim sConsec As String
        Dim tMax As Double
        Dim sFmt As String
        Dim nDig As Integer


        nDig = Len(TopeMax)
        sFmt = New String("0", nDig)

        sql = "SELECT CCDESC FROM ZCCL01 WHERE CCTABL = 'SECUENCE' AND CCCODE='" & sID & "'"
        rs = New ADODB.Recordset
        tMax = Val(TopeMax)

        With rs
            .CursorLocation = ADODB.CursorLocationEnum.adUseServer
            .Open(sql, DB_DB2, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)
            If Not (.BOF And .EOF) Then
                locID = Val(.Fields("CCDESC").Value) + 1
                If locID > tMax Then
                    locID = 1
                End If
                sConsec = Right(sFmt & locID, nDig)
                If Not bLee Then
                    .Fields("CCDESC").Value = sConsec
                    .Update()
                End If
                .Close()
            Else
                locID = 1
                .Close()
                sConsec = Right(sFmt & locID, nDig)
                DB_DB2.Execute(" INSERT INTO ZCC (CCID, CCTABL, CCCODE, CCDESC ) " & " VALUES( 'CC', 'SECUENCE', '" & sID & "', '" & sConsec & "' )")
            End If
        End With

        CalcConsec = sConsec

    End Function

    Public Function CheckVer(ByRef Aplicacion As String) As ChkVerApp
        Dim rs As ADODB.Recordset
        Dim lVer As String = "No hay registro"
        Dim result As ChkVerApp = ChkVerApp.AppOK
        Dim locVer As String

        locVer = "(" & My.Application.Info.Version.Major & "." & _
                 My.Application.Info.Version.Minor & "." & _
                 My.Application.Info.Version.Revision & ")"

        rs = New ADODB.Recordset
        With rs
            .Open("SELECT * FROM ZCCL01 WHERE CCTABL='RFVBVER' AND CCCODE = '" & Aplicacion & "'", DB_DB2)
            If Not (.EOF And .BOF) Then
                lVer = Trim(.Fields("CCSDSC").Value)
                If InStr(lVer, "*NOCHK") > 0 Then
                    result = ChkVerApp.AppOK
                End If

                If result = ChkVerApp.AppOK And InStr(lVer, locVer) > 0 Then
                    result = ChkVerApp.AppOK
                Else
                    result = ChkVerApp.AppVersionIncorrect
                End If
                bNewVersion = (.Fields("CCUDC1").Value = 1)
                bMtech01 = (.Fields("CCUDC2").Value = 1)
                bMtech13 = (.Fields("CCUDC3").Value = 1)
                bExeReal = (.Fields("CCALTC").Value = "1")
                .Close()
            Else
                fSql = " INSERT INTO ZCC ("
                vSql = " VALUES("
                mkSql(tDato.tStr, "CCID", "CC")
                mkSql(tDato.tStr, "CCTABL", "RFVBVER")
                mkSql(tDato.tStr, "CCDESC", "Interface LX PMTS")
                mkSql(tDato.tStr, "CCCODE", Aplicacion)
                mkSql(tDato.tStr, "CCSDSC", locVer, True)
                DB_DB2.Execute(fSql & vSql)
            End If
        End With

        If result = ChkVerApp.AppOK And InStr(1, "" & Val("0.7"), ",") > 0 Then
            result = ChkVerApp.AppSeparadorDecimal
        End If

        Return result

    End Function

    Function CodeCB(ByRef valor As String, Optional ByRef sep As String = "") As String
        Dim aCode As Object

        aCode = Split(valor, IIf(sep = "", " ", sep))
        CodeCB = aCode(0)

    End Function

    Function DescCB(ByRef valor As String, Optional ByRef sep As String = "") As String
        Dim aCode As Object

        aCode = Split(valor, IIf(sep = "", " ", sep))
        DescCB = Trim(Mid(valor, Len(aCode(0)) + 1))

    End Function

    Sub mkSql(ByVal tipoF As tDato, ByVal campo As String, ByVal valor As Object, Optional ByVal fin As Boolean = False)
        fSql += campo + IIf(fin, " )", ", ")
        If IsDBNull(valor) Then
            valor = IIf(tipoF = tDato.tNum, "0", "")
        End If
        Select Case tipoF
            Case tDato.tStr
                vSql += "'" + CStr(valor) + "'"
            Case tDato.tNum, tDato.tFunc
                vSql += CStr(valor)
        End Select
        vSql += IIf(fin, " )", ", ")
    End Sub

    Sub mkUpd(ByVal tipoF As tDato, ByVal campo As String, ByVal valor As Object, Optional ByVal fin As Boolean = False)
        fSql += CStr(campo) + " = " + IIf(tipoF = tDato.tStr, "'", "") + valor + IIf(tipoF = tDato.tStr, "'", "") + IIf(fin, "", ", ")
    End Sub

    Public Function Busca(ByVal Sql As String, ByVal DB As ADODB.Connection, Optional ByRef Retorna As String = "") As Boolean
        Dim rs As New ADODB.Recordset
        Dim r As Boolean
        rs.Open(Sql, DB)
        If Not rs.EOF Then
            If Retorna <> "" Then
                Retorna = CStr(rs.Fields(0).Value)
            End If
        End If
        r = Not rs.EOF
        rs.Close()
        Return r

    End Function

    Public Function QueBodega(ByVal Bod As String) As String

        Select Case Trim(Bod)
            Case "UNI" : Return "AU1"
            Case "BGA" : Return "AB1"
            Case Else : Return Bod
        End Select

    End Function

    Sub LogInterface(ByVal sInt As String, ByVal sOp As String, ByVal sDesde As String, ByVal sHasta As String, ByVal sCode As String, ByVal sUser As String)
        Dim fSql, vSql

        fSql = " INSERT INTO PFINTLOG( "
        vSql = " VALUES( "
        fSql = fSql & "LINTER, " : vSql = vSql & "'" & sInt & "', "
        fSql = fSql & "LOP, " : vSql = vSql & "'" & sOp & "', "
        fSql = fSql & "LDESDE, " : vSql = vSql & "'" & sDesde & "', "
        fSql = fSql & "LHASTA, " : vSql = vSql & "'" & sHasta & "', "
        fSql = fSql & "LCODE, " : vSql = vSql & "'" & sCode & "', "
        'fSql = fSql & "LMSG400, " : vSql = vSql & "'" & gsMsgAS400 & "', "
        fSql = fSql & "LUSER ) " : vSql = vSql & "'" & sUser & "' ) "
        DB_DB2.Execute(fSql & vSql)

    End Sub


End Module
