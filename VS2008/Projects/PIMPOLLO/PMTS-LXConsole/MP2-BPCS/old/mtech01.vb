﻿Module mtech01
    Dim rsPMTS As New ADODB.Recordset
    Sub MovMtech01()
        Dim Sql As String = ""
        Dim rsT01 As New ADODB.Recordset

        SessionId = CalcConsec("LXPMTS", False, "99999999")
        Console.WriteLine("Sesion " & SessionId)

        Sql = " SELECT "
        Sql += "  InfinityRecNo,"
        Sql += "  FarmNo,"
        Sql += "  FeedTypeF, "
        Sql += "  FeedConsumedF,"
        Sql += "  FeedTypeM, "
        Sql += "  FeedConsumedM"
        Sql += " FROM "
        Sql += "  " & My.Settings.PMTS_DB2 & "." & My.Settings.PMTS_SCHEMA2 & ".BimFieldTrans "
        Sql += " WHERE "
        Sql += " CreationDate>=(CAST('2010-06-01 00:00' AS datetime)))"
        rsPMTS.Open(Sql, DB_PMTS)
        Do While Not rsPMTS.EOF
            Sql = " SELECT "
            Sql += "  InfinityRecNo "
            Sql += " FROM "
            Sql += "  " & My.Settings.PMTS_DB1 & "." & My.Settings.PMTS_SCHEMA1 & ".Int_mtech01 "
            Sql += " WHERE "
            Sql += " InfinityRecNo = " + rsPMTS.Fields("InfinityRecNo").Value
            rsT01.Open(Sql, DB_PMTS)
            If rsT01.EOF Then
                InsTransacc(rsPMTS.Fields("Farmno").Value, rsPMTS.Fields("FeedTypeF").Value, rsPMTS.Fields("FeedConsumedF").Value)
                InsTransacc(rsPMTS.Fields("Farmno").Value, rsPMTS.Fields("FeedTypeM").Value, rsPMTS.Fields("FeedConsumedM").Value)
                fSql = "INSERT INTO " + My.Settings.PMTS_DB1 & "." & My.Settings.PMTS_SCHEMA1 & ".Int_mtech01 ("
                vSql = " VALUES( "
                mkSql(tDato.tNum, "InfinityRecNo", rsPMTS.Fields("InfinityRecNo").Value)
                mkSql(tDato.tStr, "CreationDate", rsPMTS.Fields("CreationDate").Value)
                mkSql(tDato.tFunc, "CreationDateIng", "GetDate()")
                mkSql(tDato.tStr, "FlagRecSend", 0, True)
                DB_PMTS.Execute(fSql & vSql)
            End If
            rsT01.Close()
            rsPMTS.MoveNext()
        Loop
        rsPMTS.Close()
        Sql = "{{ CALL PCTRAINV PARM( 'M' '" & SessionId & "' ) }} "
        DB_DB2.Execute(Sql)

    End Sub

    Sub InsTransacc(ByVal Bodega As String, ByVal Prod As String, ByVal Kilos As Double)

        If Kilos > 0 Then
            fSql = "INSERT INTO PFTRAINV("
            vSql = " VALUES( "
            mkSql(tDato.tStr, "TTIPO", "M")
            mkSql(tDato.tNum, "TBATCH", SessionId)
            mkSql(tDato.tStr, "TADVIC", "M" & CalcConsec("LXPMTSID", False, "99999999"))
            mkSql(tDato.tStr, "TBODE", Bodega)                                      '	Bodega
            mkSql(tDato.tStr, "TLOCAL", "ALIM")
            mkSql(tDato.tStr, "TCODRA", "01")

            mkSql(tDato.tStr, "TPRODU", Trim(Prod))     '	Producto                                          
            mkSql(tDato.tStr, "TCOMEN", Trim(rsPMTS.Fields("InfinityRecNo").Value))         '	Comentario   

            mkSql(tDato.tNum, "TKILOS", Kilos)           '	Cantidad        
            mkSql(tDato.tStr, "TRANS", "CG")             '	Transaccion                                       

            mkSql(tDato.tStr, "TFECTR", (Year(Now()) * 10000) + (Month(Now()) * 100) + Day(Now()))
            mkSql(tDato.tStr, "TDTVN", (Year(Now()) * 10000) + (Month(Now()) * 100) + Day(Now()))
            mkSql(tDato.tStr, "TTMNV", (Hour(Now()) * 10000) + (Minute(Now()) * 100) + Second(Now()))
            mkSql(tDato.tStr, "TUSR", "MTECH01", True)
            DB_DB2.Execute(fSql & vSql)
        End If

    End Sub

End Module
