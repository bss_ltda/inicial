﻿Module mtech13
    Dim rsPMTS As New ADODB.Recordset
    Sub MovMtech13()
        Dim Sql As String = ""

        SessionId = CalcConsec("LXPMTS", False, "99999999")
        Console.WriteLine("Sesion " & SessionId)

        Sql = " SELECT "
        Sql += "  RefNo,"
        Sql += "  FarmNo,"
        Sql += "  FeedMillCode,"
        Sql += "  FormulaNo,"
        Sql += "  Units "
        Sql += " FROM "
        Sql += "  " & My.Settings.PMTS_DB1 & "." & My.Settings.PMTS_SCHEMA1 & ".EnviosAlimentoConsumo"
        Sql += " WHERE "
        Sql += "  ImportedFlag = 1 "
        rsPMTS.Open(Sql, DB_PMTS)
        Do While Not rsPMTS.EOF
            InsTransacc(rsPMTS.Fields("FeedMillCode").Value, "-T1") 'Planta
            InsTransacc(rsPMTS.Fields("Farmno").Value, "T1") 'Granja
            'Cod granja > 300
            If CInt(rsPMTS.Fields("Farmno").Value) >= 300 Then
                InsTransacc(rsPMTS.Fields("Farmno").Value, "CG")
            End If
            rsPMTS.MoveNext()
        Loop
        rsPMTS.Close()
        Sql = "{{ CALL PCTRAINV PARM( 'M' '" & SessionId & "' ) }} "
        DB_DB2.Execute(Sql)

    End Sub

    Sub InsTransacc(ByVal Bodega As String, ByVal tip As String)
        fSql = "INSERT INTO PFTRAINV("
        vSql = " VALUES( "
        mkSql(tDato.tStr, "TTIPO", "M")
        mkSql(tDato.tNum, "TBATCH", SessionId)
        mkSql(tDato.tStr, "TADVIC", "M" & CalcConsec("LXPMTSID", False, "99999999"))
        mkSql(tDato.tStr, "TBODE", Bodega)                                      '	Bodega
        mkSql(tDato.tStr, "TLOCAL", "ALIM")
        mkSql(tDato.tStr, "TCODRA", "01")

        mkSql(tDato.tStr, "TPRODU", Trim(rsPMTS.Fields("FormulaNo").Value))     '	Producto                                          
        mkSql(tDato.tStr, "TCOMEN", Trim(rsPMTS.Fields("RefNo").Value))         '	Comentario   
        Select Case tip
            Case "T1"
                mkSql(tDato.tNum, "TKILOS", rsPMTS.Fields("Units").Value)           '	Cantidad        
                mkSql(tDato.tStr, "TRANS", "T1")                                    '	Transaccion                                       
            Case "-T1"
                mkSql(tDato.tNum, "TKILOS", -rsPMTS.Fields("Units").Value)          '  -Cantidad        
                mkSql(tDato.tStr, "TRANS", "T1")                                    '	Transaccion                                       
            Case "CG"
                mkSql(tDato.tNum, "TKILOS", rsPMTS.Fields("Units").Value)           '   Cantidad        
                mkSql(tDato.tStr, "TRANS", tip)                                     '   Transaccion                                       
        End Select

        mkSql(tDato.tStr, "TFECTR", (Year(Now()) * 10000) + (Month(Now()) * 100) + Day(Now()))
        mkSql(tDato.tStr, "TDTVN", (Year(Now()) * 10000) + (Month(Now()) * 100) + Day(Now()))
        mkSql(tDato.tStr, "TTMNV", (Hour(Now()) * 10000) + (Minute(Now()) * 100) + Second(Now()))
        mkSql(tDato.tStr, "TUSR", "MTECH13", True)
        DB_DB2.Execute(fSql & vSql)

    End Sub
End Module