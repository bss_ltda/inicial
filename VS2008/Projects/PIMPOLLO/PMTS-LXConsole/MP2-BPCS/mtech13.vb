﻿Module mtech13
    Dim rsPMTS As New ADODB.Recordset
    Sub MovMtech13()
        Dim Sql As String = ""
        Dim codFarm As Integer
        Dim FTransacc As String
        Dim Localizacion As String

        SessionId = CalcConsec("LXPMTS", False, "99999999")
        Console.WriteLine("Sesion " & SessionId)

        Sql = " SELECT "
        Sql += "  xDate, "
        Sql += "  ImportedFlag, "
        Sql += "  RefNo, "
        Sql += "  FarmNo,"
        Sql += "  FeedMillCode,"
        Sql += "  FormulaNo,"
        Sql += "  Units, "
        Sql += "  Bags "
        Sql += " FROM "
        Sql += "  " & My.Settings.PMTS_DB1 & "." & My.Settings.PMTS_SCHEMA1 & ".EnviosAlimentoConsumo"
        Sql += " WHERE "
        Sql += "  ImportedFlag = 0 "
        rsPMTS.Open(Sql, DB_PMTS, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockBatchOptimistic)
        Do While Not rsPMTS.EOF
            FTransacc = Format(rsPMTS.Fields("xDate").Value, "yyyyMMdd")
            'Salida
            If rsPMTS.Fields("Bags").Value > 0 Then
                Localizacion = "BULTO"
            Else
                Localizacion = "GRANEL"
            End If

            codFarm = CInt(rsPMTS.Fields("Farmno").Value)
            If codFarm < 300 Then
                If bNewVersion Then
                    'Salida
                    InsTransacc(rsPMTS.Fields("FeedMillCode").Value, Localizacion, "-T3", "01", FTransacc) 'Planta
                    'Entrada
                    InsTransacc(rsPMTS.Fields("Farmno").Value, "GRANEL", "T3", "02", FTransacc) 'Granja
                Else
                    'Salida
                    InsTransacc(rsPMTS.Fields("FeedMillCode").Value, Localizacion, "T1", "01", FTransacc) 'Planta
                    'Entrada
                    InsTransacc(rsPMTS.Fields("Farmno").Value, "GRANEL", "T1", "01", FTransacc) 'Granja
                End If
            Else
                If bNewVersion Then
                    'Salida
                    InsTransacc(rsPMTS.Fields("FeedMillCode").Value, Localizacion, "-T1", "01", FTransacc) 'Planta
                    'Entrada
                    InsTransacc(rsPMTS.Fields("Farmno").Value, "GRANEL", "T1", "01", FTransacc) 'Granja
                Else
                    'Salida
                    InsTransacc(rsPMTS.Fields("FeedMillCode").Value, Localizacion, "-T1", "01", FTransacc) 'Planta
                    'Entrada
                    InsTransacc(rsPMTS.Fields("Farmno").Value, "GRANEL", "T1", "01", FTransacc) 'Granja
                End If

                'Granjas de Engorde
                InsTransacc(codFarm, "GRANEL", "CG", "01", FTransacc)
            End If
            If bExeReal Then
                rsPMTS.Fields("ImportedFlag").Value = 5
                rsPMTS.UpdateBatch()
            End If
            rsPMTS.MoveNext()
        Loop
        rsPMTS.Close()
        If bExeReal Then
            Sql = "{{ CALL PCTRAINV PARM( 'M' '" & SessionId & "' ) }} "
            DB_DB2.Execute(Sql)
            LogInterface("PMTECH13", "EXE", "", "", CStr(SessionId), "PMTECH13")
        Else
            Sql = "INSERT INTO PFTMPALIM SELECT 'PMTECH13' AS MTECH, P.* "
            Sql += " FROM PFTRAINV P WHERE TTIPO='M' AND TBATCH=" & SessionId
            DB_DB2.Execute(Sql)

            Sql = "DELETE FROM PFTRAINV WHERE TTIPO='M' AND TBATCH=" & SessionId
            DB_DB2.Execute(Sql)

            LogInterface("PMTECH13", "EXE", "", "", CStr(SessionId), "SIMULACION")
        End If

    End Sub

    Sub InsTransacc(ByVal Bodega As String, ByVal qLoc As String, ByVal tip As String, ByVal qCodRazon As String, ByVal FechaTransacc As String)
        fSql = "INSERT INTO PFTRAINV("
        vSql = " VALUES( "
        mkSql(tDato.tStr, "TTIPO", "M")
        mkSql(tDato.tNum, "TBATCH", SessionId)
        'mkSql(tDato.tStr, "TADVIC", "M" & CalcConsec("LXPMTSID", False, "99999999"))
        mkSql(tDato.tStr, "TBODE", QueBodega(Bodega))                                      '	Bodega
        mkSql(tDato.tStr, "TLOCAL", qLoc)
        mkSql(tDato.tStr, "TCODRA", qCodRazon)

        mkSql(tDato.tStr, "TADVIC", FechaTransacc)             'Fecha Transaccion                                       

        mkSql(tDato.tStr, "TPRODU", Trim(rsPMTS.Fields("FormulaNo").Value))                 'Producto                                          
        mkSql(tDato.tStr, "TCOMEN", "M" & CalcConsec("LXPMTSID", False, "99999999") & Trim(rsPMTS.Fields("RefNo").Value)) 'Trazabilidad y RefNo   
        'mkSql(tDato.tStr, "TCOMEN", Trim(rsPMTS.Fields("RefNo").Value))                    'Comentario   
        Select Case tip
            Case "T1"
                mkSql(tDato.tNum, "TCANTI", rsPMTS.Fields("Units").Value)           '	Cantidad        
                mkSql(tDato.tStr, "TRANS", "T1")                                    '	Transaccion                                       
            Case "-T1"
                mkSql(tDato.tNum, "TCANTI", -rsPMTS.Fields("Units").Value)          '  -Cantidad        
                mkSql(tDato.tStr, "TRANS", "T1")                                    '	Transaccion                                       
            Case "T3"
                mkSql(tDato.tNum, "TCANTI", rsPMTS.Fields("Units").Value)           '	Cantidad        
                mkSql(tDato.tStr, "TRANS", "T3")                                    '	Transaccion                                       
            Case "-T3"
                mkSql(tDato.tNum, "TCANTI", -rsPMTS.Fields("Units").Value)          '  -Cantidad        
                mkSql(tDato.tStr, "TRANS", "T3")                                    '	Transaccion                                       
            Case "CG"
                mkSql(tDato.tNum, "TCANTI", rsPMTS.Fields("Units").Value)           '   Cantidad        
                mkSql(tDato.tStr, "TRANS", tip)                                     '   Transaccion                                       
        End Select

        mkSql(tDato.tStr, "TFECTR", (Year(Now()) * 10000) + (Month(Now()) * 100) + Day(Now()))
        mkSql(tDato.tStr, "TDTVN", (Year(Now()) * 10000) + (Month(Now()) * 100) + Day(Now()))
        mkSql(tDato.tStr, "TTMNV", (Hour(Now()) * 10000) + (Minute(Now()) * 100) + Second(Now()))
        mkSql(tDato.tStr, "TUSR", "MTECH13", True)
        DB_DB2.Execute(fSql & vSql)

    End Sub
End Module