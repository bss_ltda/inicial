﻿
Module InterfacePMTS
    Public DB_DB2 As ADODB.Connection
    Public DB_PMTS As ADODB.Connection
    Public gsConnString As String
    Public SessionId As String
    Public bMtech01 As Boolean
    Public bMtech13 As Boolean
    Public bExeReal As Boolean
    Public bNewVersion As Boolean
    Public Sub Proceso()
        Dim bOK As Boolean = False

        CargaPrm()

        Console.WriteLine("SERVER:     " & My.Settings.LX_SERVER)
        Console.WriteLine("BIBLIOTECA: " & My.Settings.LX_LIB)


        bOK = True

        '****************
        Select Case CheckVer(My.Application.Info.AssemblyName)
            Case ChkVerApp.AppDeshabilitada
            Case ChkVerApp.AppSeparadorDecimal
                Console.WriteLine("Separador decimal incorrecto.")
                bOK = False
            Case ChkVerApp.AppVersionIncorrect
                Console.WriteLine("Version Incorrecta")
                bOK = False
            Case ChkVerApp.AppEjecutando
            Case ChkVerApp.AppOK
        End Select

        If bNewVersion Then
            'Console.WriteLine("VERSION: NUEVA")
        Else
            'Console.WriteLine("VERSION: ANTERIOR")
        End If


        If bOK Then
            'DB_DB2.Execute(" {{ CLRPFM PFTMPALIM }} ")
            If bMtech13 Then
                MovMtech13()
            End If
            If bMtech01 And Not bNewVersion Then
                MovMtech01()
            End If
        End If

        DB_DB2.Close()
        DB_PMTS.Close()

        Console.WriteLine("Presione una tecla.")
        Console.ReadKey()

    End Sub


    Sub CargaPrm()
        Dim gsConnStringPMTS As String

        gsConnString = "Provider=IBMDA400;"
        gsConnString += "Data Source=" & My.Settings.LX_SERVER & ";"
        gsConnString += "Persist Security Info=False;"
        gsConnString += "Default Collection=" & My.Settings.LX_LIB & ";"
        gsConnString += "Password=" & My.Settings.LX_PASS & ";"
        gsConnString += "User ID=" & My.Settings.LX_USER & ";"


        DB_DB2 = New ADODB.Connection
        DB_PMTS = New ADODB.Connection
        DB_DB2.Open(gsConnString)
        Console.WriteLine("Conexion LX OK")

        gsConnStringPMTS = "Provider=SQLOLEDB.1;"
        gsConnStringPMTS += "Data Source=" & My.Settings.PMTS_SERVER & ";"
        'gsConnStringPMTS += "Initial Catalog=mp2;"
        gsConnStringPMTS += "User Id=" & My.Settings.PMTS_USER & ";"
        gsConnStringPMTS += "Password=" & My.Settings.PMTS_PASS & ";"
        DB_PMTS.ConnectionTimeout = 300
        DB_PMTS.Open(gsConnStringPMTS)
        Console.WriteLine("Conexion PMTS OK")

    End Sub

End Module
