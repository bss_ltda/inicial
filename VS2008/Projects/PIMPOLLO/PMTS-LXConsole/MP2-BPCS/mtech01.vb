﻿Module mtech01
    Dim rsPMTS As New ADODB.Recordset
    Sub MovMtech01()
        Dim Sql As String = ""
        Dim rsT01 As New ADODB.Recordset
        Dim i As Integer
        Dim rAnt As String = ""
        Dim rAct As String = ""
        Dim FTransacc As String


        Sql = "SELECT * FROM PFPMTS01 WHERE BSTS=0"
        rsT01.Open(Sql, DB_DB2)
        Do While Not rsT01.EOF
            SessionId = Right("00000000" & rsT01.Fields("BBATCH").Value, 8)
            Console.WriteLine("Sesion " & SessionId)
            Sql = " SELECT "
            'Sql += "  InfinityRecNo,"
            Sql += "  CreationDate,"
            Sql += "  FarmNo,"
            Sql += "  FeedTypeF, "
            Sql += "  FeedConsumedF,"
            Sql += "  FeedTypeM, "
            Sql += "  FeedConsumedM, "
            Sql += "  HouseNo"
            Sql += " FROM "
            Sql += "  " & My.Settings.PMTS_DB2 & "." & My.Settings.PMTS_SCHEMA2 & ".BimFieldTrans "
            Sql += " WHERE "
            Sql += " GrowoutArea='" & rsT01.Fields("BREGION").Value & "'"
            Sql += " AND CreationDate Between '" & rsT01.Fields("BDESDE").Value & " 00:00:00' "
            Sql += " AND '" & rsT01.Fields("BHASTA").Value & " 23:59:59' "
            Sql += " ORDER BY 1, 2, 3, 4, 5, 6, 7  "
            rsPMTS.Open(Sql, DB_PMTS)
            Do While Not rsPMTS.EOF
                rAct = Format(rsPMTS.Fields("CreationDate").Value, "yyyyMMddhhmm")
                For i = 1 To 6
                    rAct = rAct & "." & CStr(rsPMTS.Fields.Item(i).Value)
                Next
                If rAnt <> rAct Then
                    rAnt = rAct
                    FTransacc = Format(rsPMTS.Fields("CreationDate").Value, "yyyyMMdd")
                    InsTransacc(rsPMTS.Fields("Farmno").Value, rsPMTS.Fields("FeedTypeF").Value, rsPMTS.Fields("FeedConsumedF").Value, FTransacc)
                    InsTransacc(rsPMTS.Fields("Farmno").Value, rsPMTS.Fields("FeedTypeM").Value, rsPMTS.Fields("FeedConsumedM").Value, FTransacc)
                End If
                rsPMTS.MoveNext()
            Loop
            rsPMTS.Close()

            If bExeReal Then
                Sql = "UPDATE PFPMTS01 SET BSTS = 1 WHERE BBATCH=" & SessionId & " AND BSTS=0"
                DB_DB2.Execute(Sql)
                Sql = "{{ CALL PCTRAINV PARM( 'M' '" & SessionId & "' ) }} "
                DB_DB2.Execute(Sql)
                Sql = "UPDATE PFPMTS01 SET BSTS = 2 WHERE BBATCH=" & SessionId & " AND BSTS=1"
                DB_DB2.Execute(Sql)
                LogInterface("PMTECH01", "EXE", "", "", SessionId, "PMTECH01")
            Else
                Sql = "UPDATE PFPMTS01 SET BSTS = 1 WHERE BBATCH=" & SessionId & " AND BSTS=0"
                DB_DB2.Execute(Sql)

                Sql = "INSERT INTO PFTMPALIM SELECT 'PMTECH01' AS MTECH, P.* "
                Sql += " FROM PFTRAINV P WHERE TTIPO='M' AND TBATCH=" & SessionId
                DB_DB2.Execute(Sql)

                Sql = "DELETE FROM PFTRAINV WHERE TTIPO='M' AND TBATCH=" & SessionId
                DB_DB2.Execute(Sql)

                Sql = "UPDATE PFPMTS01 SET BSTS = 2 WHERE BBATCH=" & SessionId & " AND BSTS=1"
                DB_DB2.Execute(Sql)

                LogInterface("PMTECH01", "EXE", "", "", SessionId, "SIMULACION")
            End If
            rsT01.MoveNext()
        Loop
        rsT01.Close()

    End Sub

    Sub InsTransacc(ByVal Bodega As String, ByVal Prod As String, ByVal Kilos As Double, ByVal FechaTransacc As String)
        Dim codFarm As Integer

        If Kilos > 0 Then
            fSql = "INSERT INTO PFTRAINV("
            vSql = " VALUES( "
            mkSql(tDato.tStr, "TTIPO", "M")
            mkSql(tDato.tNum, "TBATCH", SessionId)
            mkSql(tDato.tStr, "TADVIC", FechaTransacc)             'Fecha Transaccion                                       

            mkSql(tDato.tStr, "TBODE", QueBodega(Bodega))                                      '	Bodega
            mkSql(tDato.tStr, "TLOCAL", "GRANEL")

            'Consumos
            codFarm = CInt(rsPMTS.Fields("Farmno").Value)
            If codFarm >= 100 And codFarm < 200 Then            'Granjas de Levante
                mkSql(tDato.tStr, "TCODRA", "03")
            ElseIf codFarm >= 200 And codFarm < 300 Then        'Granjas de Postura
                mkSql(tDato.tStr, "TCODRA", "05")
            ElseIf codFarm >= 300 Then                          'Granjas de Engorde
                mkSql(tDato.tStr, "TCODRA", "01")
            End If

            mkSql(tDato.tStr, "TPRODU", Trim(Prod))     '	Producto                                          
            'mkSql(tDato.tStr, "TCOMEN", Trim(rsPMTS.Fields("InfinityRecNo").Value))         '	Comentario   
            mkSql(tDato.tStr, "TCOMEN", "M" & CalcConsec("LXPMTSID", False, "99999999"))     'Trazabilidad

            mkSql(tDato.tNum, "TCANTI", Kilos)           '	Cantidad        
            mkSql(tDato.tStr, "TRANS", "CG")             '	Transaccion                                       

            mkSql(tDato.tStr, "TFECTR", (Year(Now()) * 10000) + (Month(Now()) * 100) + Day(Now()))
            mkSql(tDato.tStr, "TDTVN", (Year(Now()) * 10000) + (Month(Now()) * 100) + Day(Now()))
            mkSql(tDato.tStr, "TTMNV", (Hour(Now()) * 10000) + (Minute(Now()) * 100) + Second(Now()))
            mkSql(tDato.tStr, "TUSR", "MTECH01", True)
            DB_DB2.Execute(fSql & vSql)
        End If

    End Sub

End Module
