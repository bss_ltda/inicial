﻿Imports Microsoft.Office.Interop.Excel

Module ExportExcel

    Public Sub ExportaExcel(ByVal fSql As String)
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim i As Integer

        DB.Cursor(rs, fSql)

        'Create a new workbook in Excel
        Dim oExcel As Object
        Dim oBook As Object
        Dim oSheet As Object
        oExcel = CreateObject("Excel.Application")
        oBook = oExcel.Workbooks.Add
        oSheet = oBook.Worksheets(1)

        'Transfer the data to Excel
        For i = 0 To rs.Fields.Count - 1
            oSheet.Cells(1, i + 1) = Replace(Replace(rs.Fields(i).Name, "_", " "), "MENOS", "-")
            oSheet.Cells(1, i + 1).Font.Bold = True
        Next

        oSheet.Range("A2").CopyFromRecordset(rs)
        oSheet.Cells.EntireColumn.AutoFit()
        oSheet.Name = "FACTURAS"
        rs.Close()

        DB.Cursor(rs, "SELECT DOCU, PEDIDO, STS FROM BORRAR.FACTU WHERE STS = 'NO ESTA EN SIH'")
        oSheet = oBook.Worksheets(2)

        For i = 0 To rs.Fields.Count - 1
            oSheet.Cells(1, i + 1) = Replace(Replace(rs.Fields(i).Name, "_", " "), "MENOS", "-")
            oSheet.Cells(1, i + 1).Font.Bold = True
        Next

        oSheet.Range("A2").CopyFromRecordset(rs)
        oSheet.Cells.EntireColumn.AutoFit()
        oSheet.Name = "NO SIH"
        rs.Close()

        oExcel.Visible = True


    End Sub

    Function xlsTitCol(ByVal campo As String, ByVal tablas As String)
        Dim rs As New ADODB.Recordset
        Dim sql As String
        Dim result As String = campo

        sql = "SELECT * FROM PFFFD WHERE WHFILE IN('" & Replace(tablas, ",", "', '") & "') AND WHFLDI = '" & campo & "'"
        If DB.Cursor(rs, sql) Then
            result &= vbLf & rs.Fields("WHCHD1").Value
            If rs.Fields("WHCHD2").Value <> "" Then
                result &= vbLf & rs.Fields("WHCHD2").Value
            End If
            If rs.Fields("WHCHD3").Value <> "" Then
                result &= vbLf & rs.Fields("WHCHD3").Value
            End If
        Else
            result = StrConv(Replace(campo, "_", " "), VbStrConv.ProperCase)
        End If

        rs.Close()
        Return result

    End Function

End Module
