﻿Public Class LoginForm
    Public Const APP_NAME As String = "FACTUNOCONT"

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        If Me.cbLIB.Visible Then
            If Me.cbLIB.Text = "" Then
                MsgBox("Seleccione el ambiente.", vbInformation, "Saldos Inventario")
                Exit Sub
            End If
        End If
        'gsConnstring = "Provider=IBMDA400.DataSource;Data Source=190.216.198.36;User ID=" & Me.txtUsername.Text & ";"
        'gsConnstring &= "Password=" & Me.txtPassword.Text & ";Persist Security Info=True;"

        gsConnstring = "Provider=IBMDA400.DataSource;Data Source=192.200.110.10;User ID=" & Me.txtUsername.Text & ";"
        gsConnstring &= "Password=" & Me.txtPassword.Text & ";Persist Security Info=True;"
        If Me.cbLIB.Visible Then
            gsConnstring &= "Default Collection=" & Me.cbLIB.Text & ";Force Translate=0;"
            DB.Close()
        Else
            gsConnstring &= "Default Collection=ERPLX833F;Force Translate=0;"
        End If
        If DB.Open(gsConnstring) Then
            If CheckVer(APP_NAME) Then
                sVersion = "v " & My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & "." & My.Application.Info.Version.Revision
                If Me.cbLIB.Visible Then
                    sLIB = Me.cbLIB.Text
                    If Acceso("PACCINV", Me.txtUsername.Text) Then
                        Me.Hide()
                        frmPpal.ShowDialog()
                        Me.Close()
                    End If
                Else
                    CargaCombo(Me.cbLIB, "SELECT CCCODE FROM ZCCL01 WHERE CCTABL='LXLIB' AND CCUDC1=1 ORDER BY CCALTC")
                    Me.cbLIB.SelectedIndex = 0
                    Me.GrupoUsuario.Visible = False
                    Me.GroupAmbiente.Top = Me.GrupoUsuario.Top
                    Me.GroupAmbiente.Left = Me.GrupoUsuario.Left
                    Me.GroupAmbiente.Visible = True
                End If
            Else
                DB.Close()
            End If
        End If

    End Sub

    Private Sub LoginForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class
