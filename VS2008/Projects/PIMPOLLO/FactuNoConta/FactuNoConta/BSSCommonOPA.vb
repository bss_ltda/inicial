﻿Module BSSCommonOPA

    Function TipoUnidad(ByVal prod As String) As String
        Dim rs As New ADODB.Recordset
        Dim Result As String = ""

        If DB.Cursor(rs, "SELECT IMCWUM, IMSCWI FROM IIM WHERE IPROD='" & prod & "'") Then
            If UCase(rs.Fields("IMSCWI").Value) = "N" Then   'No maneja doble unidad de medida.
                Result = "UN"      'Charge Amount - ECS   'Precio * Cantidad
            ElseIf rs.Fields("IMCWUM").Value = "KG" Then
                Result = "KG"      'Charge Amount - ECS   'Precio * Peso
            Else
                Result = "UN"     'Charge Amount - ECS   'Precio * Cantidad
            End If

            rs.Close()
        End If
        Return Result

    End Function

End Module
