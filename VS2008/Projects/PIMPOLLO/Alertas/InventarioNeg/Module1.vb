﻿Imports System.Data.SqlClient

Module Module1
    Dim DB_DB2 As New ADODB.Connection
    Dim email As New Chilkat.Email()

    Sub Main()
        Dim rs As New ADODB.Recordset
        Dim fSql As String
        Dim ts, fec, hora As String


        DB_DB2.Open("Provider=IBMDA400.DataSource;Data Source=192.200.110.10;User ID=APPUSR;Password=SINEGMMX;Persist Security Info=True;Default Collection=ERPLX833F;Force Translate=0;")

        fSql = "SELECT  "
        fSql = fSql & "   YEAR( NOW() - 15 MINUTE ) * 10000000000 +  "
        fSql = fSql & "  MONTH( NOW() - 15 MINUTE ) * 100000000 +  "
        fSql = fSql & "    DAY( NOW() - 15 MINUTE ) * 1000000 +  "
        fSql = fSql & "   HOUR( NOW() - 15 MINUTE ) * 10000 +  "
        fSql = fSql & " MINUTE( NOW() - 15 MINUTE ) * 100, "
        fSql = fSql & " DATE(NOW() - 15 MINUTE ), TIME(NOW() - 15 MINUTE) "
        fSql = fSql & " FROM SYSIBM.SYSDUMMY1"
        rs.Open(fSql, DB_DB2)
        ts = CStr(rs(0).Value)
        fec = CStr(rs(1).Value)
        hora = CStr(rs(2).Value)

        fSql = " SELECT "
        fSql = fSql & " COUNT(*)  "
        fSql = fSql & " FROM ILI INNER JOIN PINFOP ON ILI.LPROD = PINFOP.PPROD"
        fSql = fSql & " WHERE  "
        fSql = fSql & "     ( LILDT*1000000 + LILTM > " & ts & " ) AND (LILPGM = 'NEG01') AND ((LOPB - LISSU + LADJU + LRCT) < -1 OR (LIWOPB - LIWISS + LIWRPT + LIWADJ) < -1 )              "
        rs.Close()
        rs.Open(fSql, DB_DB2)
        If rs(0).Value > 0 Then
            AlertLX_01(ts, fec, hora)
        End If
        rs.Close()

    End Sub

    Sub AlertLX_01(ByVal ts As String, ByVal fec As String, ByVal hora As String)
        Dim mailman As New Chilkat.MailMan()
        Dim mht As New Chilkat.Mht()
        Dim rs As New ADODB.Recordset

        mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")

        mailman.SmtpHost = "correo.opav.co"
        mailman.SmtpUsername = "alarmas.lx@opav.co"
        mailman.SmtpPassword = "pimpollospp"

        If Not mht.UnlockComponent("SMANRIMHT_dEnsKQ0g3Rue") Then
            Console.WriteLine(mailman.LastErrorText)
            'My.Computer.Clipboard.Clear()
            'My.Computer.Clipboard.SetText(mailman.LastErrorText)
        End If

        mht.UseCids = True
        email = mht.GetEmail("http://192.200.110.33/sineg/AlertasLX/alerta01.asp?TS=" & ts & "&fec=" & fec & "&hora=" & hora)
        If (email Is Nothing) Then
            Console.WriteLine(mailman.LastErrorText)
            'My.Computer.Clipboard.Clear()
            'My.Computer.Clipboard.SetText(mailman.LastErrorText)
        End If

        email.Subject = "Alerta-Inventario Negativo"

        rs.Open("SELECT * FROM ZCCL01 WHERE CCTABL ='PPMAIL01'", DB_DB2)
        Do While Not rs.EOF
            Select Case UCase(rs("CCSDSC").Value)
                Case "", "TO"
                    email.AddTo(Trim(rs("CCDESC").Value), rs("CCNOT1").Value)
                Case "BCC", "CCO"
                    email.AddBcc(Trim(rs("CCDESC").Value), rs("CCNOT1").Value)
                Case "CC"
                    email.AddCC(Trim(rs("CCDESC").Value), rs("CCNOT1").Value)
            End Select
            rs.MoveNext()
        Loop
        rs.Close()

        email.FromName = "Alertas LX"
        email.FromAddress = "alarmas.lx@opav.co"

        If (Not mailman.SendEmail(email)) Then
            Console.WriteLine(mailman.LastErrorText)
            'My.Computer.Clipboard.Clear()
            'My.Computer.Clipboard.SetText(mailman.LastErrorText)
        End If

    End Sub

    Sub DireccionesMail(ByVal sDir As String, ByVal sTipDest As String)
        Dim aTmp() As String
        Dim aDirecc() As String
        Dim i As Integer

        If Trim(sDir) = "" Then
            Return
        End If

        sDir = Replace(sDir, vbCrLf, ";")
        sDir = Replace(sDir, vbCr, ";")
        sDir = Replace(sDir, vbLf, ";")

        aTmp = Split(sDir, ";")
        For i = 0 To UBound(aTmp)
            aDirecc = Split(aTmp(i), "<")
            Select Case sTipDest
                Case "TO"
                    email.AddTo(Trim(aDirecc(0)), Replace(aDirecc(1), ">", ""))
                Case "CC"
                    email.AddCC(Trim(aDirecc(0)), Replace(aDirecc(1), ">", ""))
                Case "BCC"
                    email.AddBcc(Trim(aDirecc(0)), Replace(aDirecc(1), ">", ""))
            End Select
        Next
    End Sub

End Module
