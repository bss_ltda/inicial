﻿Public Class frmPpal

    Private Sub frmPpal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.StatusLabel1.Text = sLIB
        Me.StatusLabel2.Text = sVersion
        Me.StatusLabel3.Text = ""
        Me.Label1.Text = ""

    End Sub

    Private Sub LinkRep_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs)

        System.Diagnostics.Process.Start(sender.Tag)

    End Sub

    Private Sub cmdTotales_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTotales.Click
        Dim rs As New ADODB.Recordset
        Dim IdRep As String = ""
        Dim fSql As String

        Me.Cursor = Cursors.WaitCursor

        fSql = " DELETE FROM BORRAR.FACTU"
        DB.ExecuteSQL(fSql)

        fSql = " INSERT INTO BORRAR.FACTU( DOCU, PEDIDO, STS )"
        fSql &= " SELECT  FACTU.DOCU, FACTU.PEDIDO, "
        fSql &= " CASE WHEN Sum(FACTU.VALOR) = -1 "
        fSql &= " THEN 'NO ESTA EN GLH' "
        fSql &= " ELSE 'NO ESTA EN SIH' END AS STS FROM ("
        fSql &= "    SELECT 'GLH' AS FTE, DEC(LHDREF) AS DOCU, "
        fSql &= "    DEC(LHJRF2) AS PEDIDO, 1 AS VALOR"
        fSql &= " FROM GLH "
        fSql &= " WHERE LHPERD=" & Me.desde.Value.Month & " AND LHJNEN LIKE 'BL%'"
        fSql &= " AND LHJRF2 NOT LIKE 'IVA%' AND LHDREF <> ''"
        fSql &= " GROUP BY LHDREF, LHJRF2"
        fSql &= " UNION"
        fSql &= " SELECT 'SIH', IHDOCN AS DOCU, "
        fSql &= " SIORD AS PEDIDO, -1 AS VALOR"
        fSql &= " FROM SIH WHERE SIID='IH' "
        fSql &= " AND SIINVD BETWEEN " & Me.desde.Value.Year * 10000 + Me.desde.Value.Month * 100 + 1
        fSql &= " AND " & Me.desde.Value.Year * 10000 + Me.desde.Value.Month * 100 + 31 & " ) FACTU"
        fSql &= " GROUP BY FACTU.DOCU, FACTU.PEDIDO"
        fSql &= " HAVING (Sum(VALOR)<>0)"
        DB.ExecuteSQL(fSql)

        'Dim tr As New PimpoDB2DataSetTableAdapters.TOTAL
        'Me.Label1.Text = "Total de registros: " & tr.TotalRegs().ToString

        'fSql = "SELECT * FROM BORRAR.FACTU WHERE STS = 'NO ESTA EN SIH'"
        'Me.DataGridView1.DataSource = PimpoDB2DataSet.FACTU
        'With Me.DataGridView1
        '    .Columns(2).Width = 200
        'End With

        fSql = " SELECT  "
        fSql &= "SIL.ILDPFX,  "
        fSql &= "SIL.ILDOCN , "
        fSql &= "SIL.ILORD PEDIDO,  "
        fSql &= "SIL.ILCUST CLIENTE,  "
        fSql &= "SIH.SICTYP TIPO, "
        fSql &= "CTXID NIT,  "
        fSql &= "SIL.ILPROD,  "
        fSql &= "SIL.ILREV,  "
        fSql &= "SIL.ILEXTA, "
        fSql &= "SIL.ILREV - SIL.ILEXTA ILREV_MENOS_ILEXTA, "
        fSql &= "SIL.ILTA01,  "
        fSql &= "SIL.ILCLAS,  "
        fSql &= "SIH.SIOTDS "
        fSql &= "FROM  "
        fSql &= "         BORRAR.FACTU FACTU INNER JOIN SIL ON  ILDOCN=DOCU AND ILORD=PEDIDO "
        fSql &= "         INNER JOIN SIH ON ILDOCN=IHDOCN AND ILDPFX= IHDPFX  "
        fSql &= "                                           AND ILCUST=SICUST  AND ILDTYP=IHDTYP  "
        fSql &= "         INNER JOIN RCM ON ILCUST=CCUST "
        fSql &= "ORDER BY FACTU.PEDIDO  "

        Me.StatusLabel3.Text = "Generando Excel ..."
        ExportaExcel(fSql)
        Me.StatusLabel3.Text = "Excel Generado"
        Me.Cursor = Cursors.Default

    End Sub


End Class
