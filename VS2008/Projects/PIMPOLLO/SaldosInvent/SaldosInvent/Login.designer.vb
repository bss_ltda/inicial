﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
<Global.System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726")> _
Partial Class LoginForm
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub
    Friend WithEvents LogoPictureBox As System.Windows.Forms.PictureBox
    Friend WithEvents OK As System.Windows.Forms.Button

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(LoginForm))
        Me.LogoPictureBox = New System.Windows.Forms.PictureBox()
        Me.OK = New System.Windows.Forms.Button()
        Me.cbLIB = New System.Windows.Forms.ComboBox()
        Me.GrupoUsuario = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.txtUsername = New System.Windows.Forms.TextBox()
        Me.lblP = New System.Windows.Forms.Label()
        Me.lblU = New System.Windows.Forms.Label()
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GrupoUsuario.SuspendLayout()
        Me.SuspendLayout()
        '
        'LogoPictureBox
        '
        Me.LogoPictureBox.Image = CType(resources.GetObject("LogoPictureBox.Image"), System.Drawing.Image)
        Me.LogoPictureBox.Location = New System.Drawing.Point(12, 55)
        Me.LogoPictureBox.Name = "LogoPictureBox"
        Me.LogoPictureBox.Size = New System.Drawing.Size(165, 193)
        Me.LogoPictureBox.TabIndex = 0
        Me.LogoPictureBox.TabStop = False
        '
        'OK
        '
        Me.OK.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.OK.Location = New System.Drawing.Point(207, 239)
        Me.OK.Name = "OK"
        Me.OK.Size = New System.Drawing.Size(94, 23)
        Me.OK.TabIndex = 4
        Me.OK.Text = "&Aceptar"
        '
        'cbLIB
        '
        Me.cbLIB.FormattingEnabled = True
        Me.cbLIB.Location = New System.Drawing.Point(22, 57)
        Me.cbLIB.Name = "cbLIB"
        Me.cbLIB.Size = New System.Drawing.Size(262, 21)
        Me.cbLIB.TabIndex = 1
        '
        'GrupoUsuario
        '
        Me.GrupoUsuario.Controls.Add(Me.Label1)
        Me.GrupoUsuario.Controls.Add(Me.cbLIB)
        Me.GrupoUsuario.Controls.Add(Me.txtPassword)
        Me.GrupoUsuario.Controls.Add(Me.txtUsername)
        Me.GrupoUsuario.Controls.Add(Me.lblP)
        Me.GrupoUsuario.Controls.Add(Me.lblU)
        Me.GrupoUsuario.Location = New System.Drawing.Point(198, 24)
        Me.GrupoUsuario.Name = "GrupoUsuario"
        Me.GrupoUsuario.Size = New System.Drawing.Size(310, 193)
        Me.GrupoUsuario.TabIndex = 8
        Me.GrupoUsuario.TabStop = False
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(19, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(156, 23)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "&Ambiente"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtPassword
        '
        Me.txtPassword.Location = New System.Drawing.Point(21, 150)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPassword.Size = New System.Drawing.Size(129, 20)
        Me.txtPassword.TabIndex = 3
        '
        'txtUsername
        '
        Me.txtUsername.Location = New System.Drawing.Point(21, 107)
        Me.txtUsername.Name = "txtUsername"
        Me.txtUsername.Size = New System.Drawing.Size(103, 20)
        Me.txtUsername.TabIndex = 2
        '
        'lblP
        '
        Me.lblP.Location = New System.Drawing.Point(19, 130)
        Me.lblP.Name = "lblP"
        Me.lblP.Size = New System.Drawing.Size(156, 23)
        Me.lblP.TabIndex = 6
        Me.lblP.Text = "&Contraseña"
        Me.lblP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblU
        '
        Me.lblU.Location = New System.Drawing.Point(19, 81)
        Me.lblU.Name = "lblU"
        Me.lblU.Size = New System.Drawing.Size(156, 23)
        Me.lblU.TabIndex = 4
        Me.lblU.Text = "&Nombre de usuario"
        Me.lblU.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LoginForm
        '
        Me.AcceptButton = Me.OK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(524, 321)
        Me.Controls.Add(Me.GrupoUsuario)
        Me.Controls.Add(Me.OK)
        Me.Controls.Add(Me.LogoPictureBox)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "LoginForm"
        Me.Text = "Login"
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GrupoUsuario.ResumeLayout(False)
        Me.GrupoUsuario.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cbLIB As System.Windows.Forms.ComboBox
    Friend WithEvents GrupoUsuario As System.Windows.Forms.GroupBox
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents txtUsername As System.Windows.Forms.TextBox
    Friend WithEvents lblP As System.Windows.Forms.Label
    Friend WithEvents lblU As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label

End Class
