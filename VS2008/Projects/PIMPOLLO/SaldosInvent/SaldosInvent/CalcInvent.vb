﻿Module CalcInvent

    'CalcSaldosInv()
    Function CalcSaldosInv(ByVal Bod As String, ByVal Prod As String, ByVal dDesde As Date, ByVal dHasta As Date) As String
        Dim rs As New ADODB.Recordset
        Dim sldCant, sldPeso, sldCosto As Double
        Dim IdRep, UOM As String
        Dim desde, hasta As String
        Dim wBod As String = ""
        Dim fecSaldo As String
        Dim fSql As String

        Bod = UCase(Trim(Bod))
        If Bod <> "" Then
            wBod = " TWHS='" & Bod & "' AND "
        End If

        IdRep = DB.CalcConsec("PFITHSLD", "999")
        DB.ExecuteSQL("DELETE FROM PFITHSLD WHERE SIDREP=" & IdRep)
        UOM = TipoUnidad(Prod)

        fecSaldo = DateCharDec(DateAdd("m", -1, dDesde))
        desde = DateCharDec(dDesde)
        hasta = DateCharDec(dHasta)

        fSql = " SELECT IFNULL( SUM(TQTY), 0 ) TQTY, IFNULL( SUM(THTOTW), 0 ) THTOTW"
        fSql = fSql & " FROM ITH "
        fSql = fSql & " WHERE NOT IN('C', '#', 'Ñ') AND " & wBod
        fSql = fSql & " TPROD='" & Prod & "' "
        fSql = fSql & " AND TTDTE < " & desde
        DB.Cursor(rs, fSql)
        sldCant = (rs.Fields("TQTY").Value)
        sldPeso = (rs.Fields("THTOTW").Value)
        rs.Close()

        fSql = " SELECT TPROD, TTYPE, TTDTE, THTIME, TQTY, TWHS, TSCST, THTOTW, THCWUM "
        fSql = fSql & " FROM ITH "
        fSql = fSql & " WHERE NOT IN('C', '#', 'Ñ') AND " & wBod
        fSql = fSql & " TPROD='" & Prod & "' "
        fSql = fSql & " AND TTDTE > " & fecSaldo & " AND TTDTE < " & desde
        fSql = fSql & " ORDER BY TTDTE DESC, THTIME DESC "
        fSql = fSql & " FETCH FIRST 1 ROWS ONLY "
        If Not DB.Cursor(rs, fSql) Then
            rs.Close()
            Return "-1"
        End If
        sldCosto = rs.Fields("TSCST").Value

        Sql.mkSql(t_iF, " INSERT INTO PFITHSLD( ")
        Sql.mkSql(t_iV, " VALUES ( ")
        Sql.mkSql(tNum, " SIDREP    ", IdRep, 0)         '3P0   Id Reporte
        Sql.mkSql(tStr, " SWHS      ", Bod, 0)         '3A    Bodega
        Sql.mkSql(tStr, " SPROD     ", Prod, 0)         '35A   Item Number
        Sql.mkSql(tNum, " STDTE     ", rs.Fields("TTDTE").Value, 0)         '8P0   Fecha
        Sql.mkSql(tNum, " SHTIME    ", rs.Fields("THTIME").Value, 0)         '6P0   Hora
        Sql.mkSql(tNum, " SQTY      ", (sldCant), 0)         '11P3  Cantidad
        Sql.mkSql(tNum, " SSCST     ", (sldCosto), 0)         '15P5  Costo Estandar
        Sql.mkSql(tStr, " SHCWUM    ", UOM, 0)         '2A    UOM
        Sql.mkSql(tNum, " SHTOTW    ", (sldPeso), 0)         '11P4  Peso
        Sql.mkSql(tNum, " SQTYS     ", (sldCant), 0)            '11P3  Saldo Cant
        Sql.mkSql(tNum, " SHTOTWS   ", (sldPeso), 0)         '11P4  Saldo en Peso
        Sql.mkSql(tNum, " SCOSTO    ", (sldCosto * IIf(UOM = "KG", sldPeso, sldCant)), 1)            '15P2  Cost
        Sql.Insert()

        rs.Close()
        fSql = " SELECT TPROD, TTYPE, TTDTE, THTIME, TQTY, TWHS, TSCST, THTOTW "
        fSql = fSql & " FROM ITH "
        fSql = fSql & " WHERE NOT IN('C', '#', 'Ñ') AND " & wBod
        fSql = fSql & " TPROD='" & Prod & "' "
        fSql = fSql & " AND TTDTE BETWEEN " & desde & " AND " & hasta
        fSql = fSql & " ORDER BY TTDTE"
        DB.Cursor(rs, fSql)
        Do While Not rs.EOF
            sldCant += rs.Fields("TQTY").Value
            sldPeso += rs.Fields("THTOTW").Value
            sldCosto = rs.Fields("TSCST").Value
            Sql.inifSql(" INSERT INTO PFITHSLD( ")
            Sql.inivSql(" VALUES ( ")
            Sql.mkSql(tNum, " SIDREP    ", IdRep, 0)         '3P0   Id Reporte
            Sql.mkSql(tStr, " SPROD     ", rs.Fields("TPROD").Value, 0)         '35A   Item Number
            Sql.mkSql(tStr, " STYPE     ", rs.Fields("TTYPE").Value, 0)         '2A    Tipo Transaccion
            Sql.mkSql(tNum, " STDTE     ", rs.Fields("TTDTE").Value, 0)         '8P0   Fecha
            Sql.mkSql(tNum, " SHTIME    ", rs.Fields("THTIME").Value, 0)         '6P0   Hora
            Sql.mkSql(tNum, " SQTY      ", rs.Fields("TQTY").Value, 0)         '11P3  Cantidad
            Sql.mkSql(tStr, " SWHS      ", rs.Fields("TWHS").Value, 0)         '3A    Bodega
            Sql.mkSql(tNum, " SSCST     ", rs.Fields("TSCST").Value, 0)         '15P5  Costo Estandar
            Sql.mkSql(tStr, " SHCWUM    ", UOM, 0)         '2A    UOM
            Sql.mkSql(tNum, " SHTOTW    ", rs.Fields("THTOTW").Value, 0)         '11P4  Peso"
            Sql.mkSql(tNum, " SQTYS     ", (sldCant), 0)            '11P3  Saldo Cant
            Sql.mkSql(tNum, " SHTOTWS   ", (sldPeso), 0)         '11P4  Saldo en Peso
            Sql.mkSql(tNum, " SCOSTO    ", (sldCosto * IIf(UOM = "KG", sldPeso, sldCant)), 1)            '15P2  Cost
            Sql.Insert()
            rs.MoveNext()
        Loop
        rs.Close()

        Return IdRep

    End Function


    'CalcSaldosInvTot()
    Function CalcSaldosInvTot(ByVal Bod As String, ByVal Lineas As String, ByVal Prod As String, ByVal dHasta As Date) As String
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim IdRep, IdCosto As String
        Dim wBod As String = ""
        Dim fecSaldo, fSql As String
        Dim Hasta As String
        Dim locProd As String = ""

        Bod = UCase(Trim(Bod))
        If Bod <> "" Then
            wBod = " TWHS='" & Bod & "' AND "
        End If
        fecSaldo = DateCharDec(DateAdd("m", -1, dHasta))
        Hasta = DateCharDec(dHasta)

        IdCosto = DB.CalcConsec("PFITHSLD", "999")
        DB.ExecuteSQL("DELETE FROM PFITHSLD WHERE SIDREP=" & IdCosto)

        fSql = " SELECT  TPROD, TWHS, TSCST,   "
        fSql &= " ( SELECT "
        fSql &= "   CASE "
        fSql &= "     WHEN IMSCWI = 'N' THEN 'UN' "
        fSql &= "     WHEN IMCWUM = 'KG' THEN 'KG'"
        fSql &= "    ELSE 'UN' END FROM IIML01 WHERE IPROD = TPROD ) THCWUM "
        fSql &= "FROM ITH INNER JOIN IIM ON TPROD=IPROD "
        fSql &= "INNER JOIN ("
        fSql &= "      SELECT  TPROD PROD,  TWHS BOD, MAX( TTDTE * 1000000 + THTIME )  FECHA"
        fSql &= "      FROM ITH INNER JOIN IIM ON TPROD=IPROD "
        fSql &= "      WHERE TTYPE NOT IN('C', '#', 'Ñ') AND "
        If Bod <> "" Then
            fSql &= wBod
        End If
        If Lineas <> "" Then
            fSql &= "         IREF01 " & Lineas & " AND "
        End If
        If Prod <> "" Then
            fSql &= " TPROD='" & Prod & "' AND "
        End If
        fSql &= "      TTDTE BETWEEN " & fecSaldo & " AND " & Hasta
        fSql &= "      GROUP  BY TPROD, TWHS"
        fSql &= ") C ON  BOD=TWHS AND TPROD=PROD AND ( TTDTE * 1000000 + THTIME )=FECHA"
        fSql &= " WHERE TTYPE NOT IN( 'C', '#', 'Ñ' ) "
        fSql &= " ORDER BY TPROD, TTDTE DESC, THTIME DESC"
        'wrtSQ(fSql, "CalcSaldosInvTot 1")
        DB.Cursor(rs, fSql)
        Do While Not rs.EOF
            If locProd <> rs.Fields("TPROD").Value Then
                Sql.mkSql(t_iF, " INSERT INTO PFITHSLD( ")
                Sql.mkSql(t_iV, " VALUES ( ")
                Sql.mkSql(tNum, " SIDREP ", IdCosto, 0)                        '3P0   Id Reporte
                If Bod <> "" Then
                    Sql.mkSql(tStr, " SWHS   ", rs.Fields("TWHS").Value, 0)        '3A    Bodega
                End If
                Sql.mkSql(tStr, " SPROD  ", rs.Fields("TPROD").Value, 0)       '35A   Item Number
                Sql.mkSql(tStr, " SHCWUM  ", rs.Fields("THCWUM").Value, 0)       'UOM

                Sql.mkSql(tNum, " SSCST  ", rs.Fields("TSCST").Value, 1)       '15P5  Costo Estandar
                Sql.Insert()
                'wrtSQ(Sql.dspVSql(), "CalcSaldosInvTot " & locProd)
                locProd = rs.Fields("TPROD").Value
            End If
            rs.MoveNext()
        Loop
        rs.Close()

        IdRep = DB.CalcConsec("PFITHSLD", "999")
        DB.ExecuteSQL("DELETE FROM PFITHSLD WHERE SIDREP=" & IdRep)

        Sql.mkSql(t_iF, " INSERT INTO PFITHSLD( ")
        Sql.mkSql(t_iV, " SELECT ")
        Sql.mkSql(tNum, " SIDREP    ", IdRep, 0)        '3P0   Id Reporte
        Sql.mkSql(tNum, " SPROD     ", "TPROD     ", 0)        '35A   Item Number
        Sql.mkSql(tNum, " STDTE     ", Hasta, 0)        '8P0   Fecha

        fSql = " ( SELECT "
        fSql &= "   CASE "
        fSql &= "     WHEN IMSCWI = 'N' THEN 'UN' "
        fSql &= "     WHEN IMCWUM = 'KG' THEN 'KG'"
        fSql &= "    ELSE 'UN' END FROM IIML01 WHERE IPROD = TPROD ) "
        'Sql.mkSql(tNum, " SHCWUM    ", "SHCWUM", 0)        '2A    UOM

        fSql = " ( SELECT IFNULL( AVG(SSCST), 0 ) FROM PFITHSLD WHERE SIDREP=" & IdCosto
        If Bod <> "" Then
            fSql &= " AND SWHS=TWHS "
        End If
        fSql &= " AND SPROD=TPROD ) "
        Sql.mkSql(tNum, " SSCST     ", fSql, 0)        '15P5  Costo Estandar
        If Bod <> "" Then
            Sql.mkSql(tNum, " SWHS      ", "TWHS      ", 0)        '3A    Bodega
        End If
        Sql.mkSql(tNum, " SQTYS     ", "IFNULL( SUM(TQTY), 0 )", 0)        '11P3  Saldo Cant
        Sql.mkSql(tNum, " SHTOTWS   ", "IFNULL( SUM(THTOTW), 0 )", 10)        '11P4  Saldo en Peso
        Sql.mkSql(tFil, " FROM ITH INNER JOIN IIM ON TPROD=IPROD")
        Sql.mkSql(tFil, " WHERE TTYPE NOT IN('C', '#', 'Ñ') AND " & wBod)
        If Lineas <> "" Then
            Sql.mkSql(tFil, " IREF01 " & Lineas & " AND ")
        End If
        If Prod <> "" Then
            Sql.mkSql(tFil, " TPROD='" & Prod & "' AND ")
        End If
        'Sql.mkSql(tFil, " TTDTE BETWEEN " & Desde & " AND " & Hasta)
        Sql.mkSql(tFil, " TTDTE <= " & Hasta)
        If Prod <> "" Then
            Sql.mkSql(tFil, " AND TPROD='" & Prod & "' ")
        End If
        If Bod <> "" Then
            Sql.mkSql(tFil, " GROUP BY TWHS, TPROD ")
        Else
            Sql.mkSql(tFil, " GROUP BY TPROD ")
        End If
        Sql.Insert()
        'wrtSQ(Sql.dspVSql(), "CalcSaldosInvTot Fin")


        fSql = "UPDATE PFITHSLD SET SCOSTO = SSCST * (CASE WHEN SHCWUM = 'KG' THEN SHTOTWS ELSE SQTYS END ) "
        fSql &= " WHERE SIDREP=" & IdRep
        DB.ExecuteSQL(fSql)
        Return IdRep

    End Function

    
End Module
