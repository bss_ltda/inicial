﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPpal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPpal))
        Me.Status = New System.Windows.Forms.StatusStrip()
        Me.StatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.cbLineaT = New System.Windows.Forms.TabPage()
        Me.cmdTotales = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtProdT = New System.Windows.Forms.TextBox()
        Me.txtBodT = New System.Windows.Forms.TextBox()
        Me.hastaT = New System.Windows.Forms.DateTimePicker()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtProd = New System.Windows.Forms.TextBox()
        Me.txtBod = New System.Windows.Forms.TextBox()
        Me.hasta = New System.Windows.Forms.DateTimePicker()
        Me.desde = New System.Windows.Forms.DateTimePicker()
        Me.cmdInvent = New System.Windows.Forms.Button()
        Me.CheckedListBoxT = New System.Windows.Forms.CheckedListBox()
        Me.Status.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.cbLineaT.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Status
        '
        Me.Status.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StatusLabel1, Me.StatusLabel2})
        Me.Status.Location = New System.Drawing.Point(0, 483)
        Me.Status.Name = "Status"
        Me.Status.Size = New System.Drawing.Size(526, 22)
        Me.Status.TabIndex = 16
        Me.Status.Text = "StatusStrip1"
        '
        'StatusLabel1
        '
        Me.StatusLabel1.Name = "StatusLabel1"
        Me.StatusLabel1.Size = New System.Drawing.Size(121, 17)
        Me.StatusLabel1.Text = "ToolStripStatusLabel1"
        '
        'StatusLabel2
        '
        Me.StatusLabel2.Name = "StatusLabel2"
        Me.StatusLabel2.Size = New System.Drawing.Size(121, 17)
        Me.StatusLabel2.Text = "ToolStripStatusLabel2"
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.cbLineaT)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(433, 454)
        Me.TabControl1.TabIndex = 17
        '
        'cbLineaT
        '
        Me.cbLineaT.BackColor = System.Drawing.SystemColors.Control
        Me.cbLineaT.Controls.Add(Me.CheckedListBoxT)
        Me.cbLineaT.Controls.Add(Me.cmdTotales)
        Me.cbLineaT.Controls.Add(Me.Label6)
        Me.cbLineaT.Controls.Add(Me.Label8)
        Me.cbLineaT.Controls.Add(Me.Label9)
        Me.cbLineaT.Controls.Add(Me.Label10)
        Me.cbLineaT.Controls.Add(Me.txtProdT)
        Me.cbLineaT.Controls.Add(Me.txtBodT)
        Me.cbLineaT.Controls.Add(Me.hastaT)
        Me.cbLineaT.Location = New System.Drawing.Point(4, 22)
        Me.cbLineaT.Name = "cbLineaT"
        Me.cbLineaT.Padding = New System.Windows.Forms.Padding(3)
        Me.cbLineaT.Size = New System.Drawing.Size(425, 428)
        Me.cbLineaT.TabIndex = 0
        Me.cbLineaT.Text = "Totales"
        '
        'cmdTotales
        '
        Me.cmdTotales.Location = New System.Drawing.Point(282, 374)
        Me.cmdTotales.Name = "cmdTotales"
        Me.cmdTotales.Size = New System.Drawing.Size(94, 23)
        Me.cmdTotales.TabIndex = 25
        Me.cmdTotales.Text = "Generar"
        Me.cmdTotales.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(26, 89)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(35, 13)
        Me.Label6.TabIndex = 24
        Me.Label6.Text = "Hasta"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(253, 18)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(35, 13)
        Me.Label8.TabIndex = 22
        Me.Label8.Text = "Línea"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(26, 46)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(50, 13)
        Me.Label9.TabIndex = 21
        Me.Label9.Text = "Producto"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(26, 21)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(44, 13)
        Me.Label10.TabIndex = 20
        Me.Label10.Text = "Bodega"
        '
        'txtProdT
        '
        Me.txtProdT.Location = New System.Drawing.Point(90, 43)
        Me.txtProdT.Name = "txtProdT"
        Me.txtProdT.Size = New System.Drawing.Size(97, 20)
        Me.txtProdT.TabIndex = 16
        '
        'txtBodT
        '
        Me.txtBodT.Location = New System.Drawing.Point(91, 18)
        Me.txtBodT.Name = "txtBodT"
        Me.txtBodT.Size = New System.Drawing.Size(51, 20)
        Me.txtBodT.TabIndex = 15
        '
        'hastaT
        '
        Me.hastaT.CustomFormat = "yyyy-MM-dd"
        Me.hastaT.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.hastaT.Location = New System.Drawing.Point(91, 82)
        Me.hastaT.Name = "hastaT"
        Me.hastaT.Size = New System.Drawing.Size(96, 20)
        Me.hastaT.TabIndex = 19
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Controls.Add(Me.Label4)
        Me.TabPage2.Controls.Add(Me.Label2)
        Me.TabPage2.Controls.Add(Me.Label1)
        Me.TabPage2.Controls.Add(Me.txtProd)
        Me.TabPage2.Controls.Add(Me.txtBod)
        Me.TabPage2.Controls.Add(Me.hasta)
        Me.TabPage2.Controls.Add(Me.desde)
        Me.TabPage2.Controls.Add(Me.cmdInvent)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(425, 255)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Por Producto"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(26, 95)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(35, 13)
        Me.Label5.TabIndex = 35
        Me.Label5.Text = "Hasta"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(26, 69)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(38, 13)
        Me.Label4.TabIndex = 34
        Me.Label4.Text = "Desde"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(26, 46)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 13)
        Me.Label2.TabIndex = 32
        Me.Label2.Text = "Producto"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(26, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(44, 13)
        Me.Label1.TabIndex = 31
        Me.Label1.Text = "Bodega"
        '
        'txtProd
        '
        Me.txtProd.Location = New System.Drawing.Point(90, 43)
        Me.txtProd.Name = "txtProd"
        Me.txtProd.Size = New System.Drawing.Size(97, 20)
        Me.txtProd.TabIndex = 27
        '
        'txtBod
        '
        Me.txtBod.Location = New System.Drawing.Point(91, 18)
        Me.txtBod.Name = "txtBod"
        Me.txtBod.Size = New System.Drawing.Size(51, 20)
        Me.txtBod.TabIndex = 26
        '
        'hasta
        '
        Me.hasta.CustomFormat = "yyyy-MM-dd"
        Me.hasta.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.hasta.Location = New System.Drawing.Point(90, 95)
        Me.hasta.Name = "hasta"
        Me.hasta.Size = New System.Drawing.Size(96, 20)
        Me.hasta.TabIndex = 30
        '
        'desde
        '
        Me.desde.CustomFormat = "yyyy-MM-dd"
        Me.desde.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.desde.Location = New System.Drawing.Point(91, 69)
        Me.desde.Name = "desde"
        Me.desde.Size = New System.Drawing.Size(96, 20)
        Me.desde.TabIndex = 29
        '
        'cmdInvent
        '
        Me.cmdInvent.Location = New System.Drawing.Point(29, 204)
        Me.cmdInvent.Name = "cmdInvent"
        Me.cmdInvent.Size = New System.Drawing.Size(94, 23)
        Me.cmdInvent.TabIndex = 25
        Me.cmdInvent.Text = "Por Producto"
        Me.cmdInvent.UseVisualStyleBackColor = True
        '
        'CheckedListBoxT
        '
        Me.CheckedListBoxT.CheckOnClick = True
        Me.CheckedListBoxT.FormattingEnabled = True
        Me.CheckedListBoxT.Location = New System.Drawing.Point(256, 44)
        Me.CheckedListBoxT.Name = "CheckedListBoxT"
        Me.CheckedListBoxT.Size = New System.Drawing.Size(120, 304)
        Me.CheckedListBoxT.TabIndex = 27
        '
        'frmPpal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(526, 505)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Status)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmPpal"
        Me.Text = "Saldos de Inventario Valorizados"
        Me.Status.ResumeLayout(False)
        Me.Status.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.cbLineaT.ResumeLayout(False)
        Me.cbLineaT.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Status As System.Windows.Forms.StatusStrip
    Friend WithEvents StatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents StatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents cbLineaT As System.Windows.Forms.TabPage
    Friend WithEvents cmdTotales As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtProdT As System.Windows.Forms.TextBox
    Friend WithEvents txtBodT As System.Windows.Forms.TextBox
    Friend WithEvents hastaT As System.Windows.Forms.DateTimePicker
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtProd As System.Windows.Forms.TextBox
    Friend WithEvents txtBod As System.Windows.Forms.TextBox
    Friend WithEvents hasta As System.Windows.Forms.DateTimePicker
    Friend WithEvents desde As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmdInvent As System.Windows.Forms.Button
    Friend WithEvents CheckedListBoxT As System.Windows.Forms.CheckedListBox

End Class
