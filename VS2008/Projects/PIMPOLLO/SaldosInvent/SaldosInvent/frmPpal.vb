﻿Public Class frmPpal

    Private Sub frmPpal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim rs As New ADODB.Recordset

        Me.StatusLabel1.Text = sVersion
        Me.StatusLabel2.Text = ""
        DB.Cursor(rs, "SELECT CCCODE FROM ZCCL01 WHERE CCTABL='SIRF1'")
        Do While Not rs.EOF
            Me.CheckedListBoxT.Items.Add(rs(0).Value)
            rs.MoveNext()
        Loop
        rs.Close()

    End Sub

    Private Sub cmdInvent_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim rs As New ADODB.Recordset
        Dim IdRep As String

        If Me.txtProd.Text = "" Then
            MsgBox("Debe ingresar un producto.", vbExclamation, "Saldos Inventarios")
            Return
        End If

        Me.Cursor = Cursors.WaitCursor
        Me.StatusLabel2.Text = "Generando Excel ..."
        IdRep = CalcSaldosInv(Me.txtBod.Text, Me.txtProd.Text, Me.desde.Value, Me.hasta.Value)
        ExportaExcel(IdRep, "SELECT SWHS AS BOD, SPROD AS PRODUCTO, STYPE AS TIPO, STDTE AS FECHA, SQTY AS CANT, SHTOTW AS PESO, SSCST AS COSTO, SQTYS AS SALDO_CANT, SHTOTWS AS SALDO_PESO, SCOSTO AS COSTO_TOTAL FROM PFITHSLD WHERE SIDREP=" & IdRep)
        Me.StatusLabel2.Text = "Excel Generado."
        Me.Cursor = Cursors.Default

    End Sub

    Private Sub LinkRep_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs)

        System.Diagnostics.Process.Start(sender.Tag)

    End Sub


    Private Sub cmdTotales_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdTotales.Click
        Dim IdRep As String
        Dim Lineas As String = ""
        Dim sep As String = ""

        ' Display in a message box all the items that are checked.
        Dim itemChecked As Object

        ' Next show the object title and check state for each item selected.
        For Each itemChecked In CheckedListBoxT.CheckedItems
            Lineas &= sep & "'" & itemChecked.ToString() & "'"
            sep = ", "
        Next

        If Lineas <> "" Then
            Lineas = " IN( " & Lineas & ")"
        End If

        Me.Cursor = Cursors.WaitCursor
        Me.StatusLabel2.Text = "Calculando Saldo ..."
        Application.DoEvents()

        IdRep = CalcSaldosInvTot(Me.txtBodT.Text, Lineas, Me.txtProdT.Text, Me.hastaT.Value)
        Me.StatusLabel2.Text = "Generando Excel (" & IdRep & ")  ..."
        ExportaExcel(IdRep, "SELECT SWHS AS BOD, SPROD AS PRODUCTO, STDTE AS FECHA, SSCST AS COSTO, SQTYS AS SALDO_CANT, SHTOTWS AS SALDO_PESO, SCOSTO AS COSTO_TOTAL FROM PFITHSLD WHERE SIDREP = " & IdRep)
        Me.StatusLabel2.Text = "Excel Generado (" & IdRep & ")"

        Me.Cursor = Cursors.Default
        Application.DoEvents()

    End Sub

    Private Sub cmdInvent_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdInvent.Click
        Dim rs As New ADODB.Recordset
        Dim IdRep As String

        If Me.txtBod.Text = "" Then
            MsgBox("Digite la Bodega.", vbExclamation, Application.ProductName)
            Return
        End If

        If Me.txtProd.Text = "" Then
            MsgBox("Digite el Producto.", vbExclamation, Application.ProductName)
            Return
        End If

        Me.Cursor = Cursors.WaitCursor
        Me.StatusLabel2.Text = "Calculando Saldo ..."
        Application.DoEvents()

        IdRep = CalcSaldosInv(Me.txtBod.Text, Me.txtProd.Text, Me.desde.Value, Me.hasta.Value)
        Me.StatusLabel2.Text = "Generando Excel (" & IdRep & ")  ..."

        ExportaExcel(IdRep, "SELECT SWHS AS BOD, SPROD AS PRODUCTO, STYPE AS TIPO, STDTE AS FECHA, SQTY AS CANT, SHTOTW AS PESO, SSCST AS COSTO, SQTYS AS SALDO_CANT, SHTOTWS AS SALDO_PESO, SCOSTO AS COSTO_TOTAL FROM PFITHSLD WHERE SIDREP=" & IdRep)

        Me.Cursor = Cursors.Default
        Me.StatusLabel2.Text = "Excel Generado (" & IdRep & ")"
        Application.DoEvents()

    End Sub
End Class
