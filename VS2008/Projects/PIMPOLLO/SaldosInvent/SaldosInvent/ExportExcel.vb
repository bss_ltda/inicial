﻿Imports Microsoft.Office.Interop.Excel

Module ExportExcel


    Public Sub ExportaExcel(ByVal IdRep As String, ByVal fSql As String)
        Dim rs As New ADODB.Recordset
        Dim i As Integer
        DB.Cursor(rs, fSql)

        'Create a new workbook in Excel
        Dim oExcel As Object
        Dim oBook As Object
        Dim oSheet As Object
        oExcel = CreateObject("Excel.Application")
        oBook = oExcel.Workbooks.Add
        oSheet = oBook.Worksheets(1)

        'Transfer the data to Excel
        For i = 0 To rs.Fields.Count - 1
            oSheet.Cells(1, i + 1) = StrConv(Replace(rs.Fields(i).Name, "_", " "), VbStrConv.ProperCase)
            oSheet.Cells(1, i + 1).Font.Bold = True
        Next

        oSheet.Range("A2").CopyFromRecordset(rs)

        oSheet.Cells.EntireColumn.AutoFit()
        'oSheet.ListObjects.Add(XlListObjectSourceType.xlSrcRange, oSheet.Range(oSheet.Cells(1, 1), oSheet.Cells(rs.RecordCount + 1, rs.Fields.Count)), , XlYesNoGuess.xlYes).Name = "Tabla1"
        'oSheet.ListObjects("Tabla1").TableStyle = "TableStyleMedium2"

        'Save the Workbook and Quit Excel
        rs.Close()
        oExcel.Visible = True
        'oBook.SaveAs Replace(sXLS & "\Book" & Format(Now, "hhMMss") & ".xlsx", "\\", "\")
        'oExcel.Quit

        'If Not oBook Is Nothing Then
        '    oBook.Close(False)
        '    oBook = Nothing
        'End If


        'If Not oExcel Is Nothing Then
        '    oExcel.Quit()
        '    oExcel = Nothing
        'End If


    End Sub

End Module
