﻿Public Class LoginForm
    Public Const APP_NAME As String = "SALDINVENT"

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click

        Dim rs As New ADODB.Recordset
        Dim fSql As String

        If Me.cbLIB.SelectedIndex = -1 Then
            MsgBox("Seleccione el Ambiente.", vbExclamation, Application.ProductName)
            Return
        End If

        DB.Open("Provider=IBMDA400.DataSource;Data Source=192.200.110.10;User ID=APPUSR;Password=SINEGMMX;Persist Security Info=True;Default Collection=ERPLX833F;Force Translate=0;")
        If Not CheckVer(APP_NAME) Then
            DB.Close()
            Return
        End If

        sVersion = Me.cbLIB.Text & " v " & My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & "." & My.Application.Info.Version.Revision

        fSql = "  SELECT  * "
        fSql &= " FROM    PFLXENV "
        fSql &= " WHERE   (EID = 'LX') "
        fSql &= " AND TRIM(EENV) || ' [' || ELIB ||  ']'='" & Me.cbLIB.Text & "'"

        DB.Cursor(rs, fSql)

        gsConnstring = DB.mkConnectionString(rs.Fields("ESERVER").Value, Me.txtUsername.Text, Me.txtPassword.Text, rs.Fields("ELIB").Value)

        rs.Close()
        DB.Close()

        If DB.Open(gsConnstring) Then
            If Acceso("PACCINV", Me.txtUsername.Text) Then

                Call SaveSetting(Application.ProductName, Application.ProductName, "txtLIB400", Me.cbLIB.Text)
                Call SaveSetting(Application.ProductName, Application.ProductName, "txtUser400", Me.txtUsername.Text)

                Me.Hide()
                frmPpal.ShowDialog()
                Me.Close()
            End If
        End If

    End Sub

    Private Sub LoginForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim DB As New ExecuteSQL
        Dim rs As New ADODB.Recordset
        Dim fSql As String

        If Not DB.Open("Provider=IBMDA400.DataSource;Data Source=192.200.110.10;User ID=APPUSR;Password=SINEGMMX;Persist Security Info=True;Default Collection=ERPLX833F;Force Translate=0;") Then
            End
        End If

        fSql = " SELECT  TRIM(EENV) || ' [' || ELIB ||  ']' AS AMBIENTE  "
        fSql &= " FROM    PFLXENV "
        fSql &= " WHERE   (EID = 'LX') "


        DB.Cursor(rs, fSql)
        Do While Not rs.EOF
            Me.cbLIB.Items.Add(rs.Fields(0).Value)
            rs.MoveNext()
        Loop
        rs.Close()
        DB.Close()

        Me.cbLIB.Text = GetSetting(Application.ProductName, Application.ProductName, "txtLIB400")
        Me.txtUsername.Text = GetSetting(Application.ProductName, Application.ProductName, "txtUser400")


    End Sub

    Private Sub cbLIB_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbLIB.SelectedIndexChanged

    End Sub
End Class
