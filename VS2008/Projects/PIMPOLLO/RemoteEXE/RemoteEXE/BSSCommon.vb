﻿Option Strict Off
Option Explicit On
Module BSSCommon

    Public Const tNum As Integer = 1
    Public Const tStr As Integer = 2
    Public DB_DB2 As ADODB.Connection
    Public DB As New ExecuteSQL

    Public Enum ChkVerApp
        AppOK = 1
        AppVersionIncorrect = 2
        AppSeparadorDecimal = 3
        AppDeshabilitada = 4
        AppEjecutando = 5
    End Enum

    Function CodeCB(ByRef valor As String, Optional ByRef sep As String = "") As String
        Dim aCode As Object

        aCode = Split(valor, IIf(sep = "", " ", sep))
        CodeCB = aCode(0)

    End Function

    Function DescCB(ByRef valor As String, Optional ByRef sep As String = "") As String
        Dim aCode As Object

        aCode = Split(valor, IIf(sep = "", " ", sep))
        DescCB = Trim(Mid(valor, Len(aCode(0)) + 1))

    End Function

    Public Function Busca(ByVal Sql As String, ByVal DB As ADODB.Connection, Optional ByRef Retorna As String = "") As Boolean
        Dim rs As New ADODB.Recordset
        Dim r As Boolean
        rs.Open(Sql, DB)
        If Not rs.EOF Then
            If Retorna <> "" Then
                Retorna = CStr(rs.Fields(0).Value)
            End If
        End If
        r = Not rs.EOF
        rs.Close()
        Return r

    End Function


    Sub LogInterface(ByVal sInt As String, ByVal sOp As String, ByVal sDesde As String, ByVal sHasta As String, ByVal sCode As String, ByVal sUser As String)
        Dim fSql, vSql

        fSql = " INSERT INTO PFINTLOG( "
        vSql = " VALUES( "
        fSql = fSql & "LINTER, " : vSql = vSql & "'" & sInt & "', "
        fSql = fSql & "LOP, " : vSql = vSql & "'" & sOp & "', "
        fSql = fSql & "LDESDE, " : vSql = vSql & "'" & sDesde & "', "
        fSql = fSql & "LHASTA, " : vSql = vSql & "'" & sHasta & "', "
        fSql = fSql & "LCODE, " : vSql = vSql & "'" & sCode & "', "
        'fSql = fSql & "LMSG400, " : vSql = vSql & "'" & gsMsgAS400 & "', "
        fSql = fSql & "LUSER ) " : vSql = vSql & "'" & sUser & "' ) "
        DB_DB2.Execute(fSql & vSql)

    End Sub


End Module
