﻿Public Class ExecuteSQL
    Public DB As New ADODB.Connection

    Public Sub Open(ByVal gsConnString As String)
        DB.Open(gsConnString)
    End Sub

    Public Sub Close()
        DB.Close()
    End Sub

    Public Function ExecuteSQL(ByVal sql As String) As Integer

        Dim r As Integer = 0

        Try
            DB.Execute(sql, r)
        Catch
            Console.WriteLine(Err.Number)
            Console.WriteLine(Err.Description)
            Console.WriteLine(sql)
            Return -99
        End Try

        Return r

    End Function

    Public Function rs(ByRef rs1 As ADODB.Recordset, ByVal sql As String) As Boolean

        Dim Result As Boolean
        Try
            rs1.Open(sql, DB)
        Catch
            Console.WriteLine(Err.Number)
            Console.WriteLine(Err.Description)
            Console.WriteLine(sql)
            Return False
        End Try
        Result = (Not rs1.EOF)
        rs1.Close()
        Return Result

    End Function

    Public Function CalcConsec(ByRef sID As String, ByRef TopeMax As String) As String
        Dim rs As New ADODB.Recordset
        Dim locID As String
        Dim sql As String
        Dim sConsec As String = ""
        Dim tMax As Double
        Dim sFmt As String
        Dim nDig As Integer


        nDig = Len(TopeMax)
        sFmt = New String("0", nDig)

        sql = "SELECT CCDESC FROM ZCCL01 WHERE CCTABL = 'SECUENCE' AND CCCODE='" & sID & "'"
        tMax = Val(TopeMax)

        With rs
            .CursorLocation = ADODB.CursorLocationEnum.adUseServer
            .Open(sql, DB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)
            If Not (.BOF And .EOF) Then
                locID = Val(.Fields("CCDESC").Value) + 1
                If locID > tMax Then
                    locID = 1
                End If
                sConsec = Right(sFmt & locID, nDig)
                .Fields("CCDESC").Value = sConsec
                .Update()
                .Close()
            Else
            locID = 1
            .Close()
            sConsec = Right(sFmt & locID, nDig)
            DB.Execute(" INSERT INTO ZCC (CCID, CCTABL, CCCODE, CCDESC ) " & " VALUES( 'CC', 'SECUENCE', '" & sID & "', '" & sConsec & "' )")
            End If
        End With

        Return sConsec

    End Function


End Class
