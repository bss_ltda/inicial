﻿Public Class clsConexion
    Public DB As ADODB.Connection
    Public gsDatasource As String
    Public gsConnstring As String
    Public gsLib As String
    Public lastError As String
    Public lastSQL As String

    Dim regA As Double

    Public Const DB2_DATEDEC = " ( YEAR( NOW() ) * 10000 + MONTH( NOW()  ) * 100 + DAY( NOW() ) )"
    Public Const DB2_TIMEDEC0 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    Public Const DB2_TIMEDEC1 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 ) "

    Public Function Conexion() As Boolean
        Dim gsProvider As String = "IBMDA400"
        Dim AS400Param(5) As String
        Dim rs As New ADODB.Recordset
        Dim Resultado As Boolean

        gsConnstring = "Provider=" & gsProvider & ";Data Source=" & gsDatasource & ";"
        gsConnstring = gsConnstring & "Persist Security Info=False;Default Collection=" & gsLib & ";"
        gsConnstring = gsConnstring & "Password=LXAPL;User ID=APLLX;"

        DB = New ADODB.Connection
        Try
            DB.Open(gsConnstring)
        Catch ex As Exception
            lastError = ex.Message
            Return False
        End Try


        Dim gsUser As String = getLXParam("LXUSER")
        Dim gsPass As String = getLXParam("LXPASS")
        If gsUser <> "" And gsPass <> "" Then

            gsConnstring = "Provider=" & gsProvider & ";Data Source=" & gsDatasource & ";"
            gsConnstring = gsConnstring & "Persist Security Info=False;Default Collection=" & gsLib & ";"
            gsConnstring = gsConnstring & "Password=" & gsPass & ";User ID=" & gsUser & ";"
            gsConnstring = gsConnstring & "Force Translate=0;"
            DB.Close()
            DB.Open(gsConnstring)
            Resultado = True
        Else
            Resultado = False
        End If

        Return Resultado

    End Function

    Public Sub Cerrar()
        DB.Close()
    End Sub

    Public Function ExecuteSQL(ByVal sql As String) As ADODB.Recordset
        sql = CStr(sql)
        Try
            lastError = ""
            lastSQL = sql
            ExecuteSQL = DB.Execute(sql, regA)
        Catch ex As Exception
            If InStr(sql, "{{") = 0 And InStr(UCase(sql), "DROP TABLE") = 0 Then
                lastError = Err.Number & " " & Err.Description
                WrtSqlError(CStr(sql), lastError)
            End If
            ExecuteSQL = Nothing
        End Try

    End Function

    Sub WrtSqlError(sql As String, Descrip As String)
        Dim conn As New ADODB.Connection
        Dim fSql As String
        Dim vSql As String

        Dim gsConnString As String = "Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=APLLX;Password=LXAPL;Persist Security Info=True;Default Collection=ERPLX834F;Force Translate=0;"

        Try
            conn.Open(gsConnString)
            fSql = " INSERT INTO RFLOG( "
            vSql = " VALUES ( "
            fSql = fSql & " USUARIO   , " : vSql = vSql & "'" & System.Net.Dns.GetHostName() & "', "      '//502A
            fSql = fSql & " PROGRAMA  , " : vSql = vSql & "'" & System.Reflection.Assembly.GetExecutingAssembly.FullName & "', "      '//502A
            fSql = fSql & " ALERT     , " : vSql = vSql & "1, "      '//1P0
            fSql = fSql & " EVENTO    , " : vSql = vSql & "'" & Replace(Descrip, "'", "''") & "', "      '//20002A
            fSql = fSql & " TXTSQL    ) " : vSql = vSql & "'" & Replace(sql, "'", "''") & "' ) "      '//5002A
            conn.Execute(fSql & vSql)
            conn.Close()
        Catch ex As Exception

        End Try

    End Sub

    Public Function getLXParam(prm As String) As String
        Return getLXParam(prm, "")
    End Function


    Public Function getLXParam(prm As String, Campo As String) As String
        Dim rs As New ADODB.Recordset
        Dim resultado As String = ""

        If Campo = "" Then
            Campo = "CCNOT1"
        End If
        rs.Open("SELECT " & Campo & " AS DATO FROM RFPARAM WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER('" & prm & "')", DB)
        If Not rs.EOF Then
            resultado = rs("DATO").Value
        End If
        rs.Close()
        Return resultado

    End Function

    Public Function regActualizados() As Long
        Return regA
    End Function


End Class

