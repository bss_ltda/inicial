﻿Module DocumentosPDF
    Dim DB As clsConexion
    Dim fWrkID As String

    Private mLocation As String

    Sub Main()



        If Environment.GetCommandLineArgs.Length > 1 Then
            fWrkID = Environment.GetCommandLineArgs(1)
            If AbreConexion() Then
                Console.WriteLine(System.Reflection.Assembly.GetExecutingAssembly.FullName)
                Console.WriteLine("Conectado a " & DB.gsLib)
                RevisaDocs()
            Else
                Console.WriteLine("-----------------------------------------------------")
                Console.WriteLine("ERROR")
                Console.WriteLine(DB.lastError)
                Console.WriteLine("-----------------------------------------------------")
            End If
        Else
            Console.WriteLine("-----------------------------------------------------")
            Console.WriteLine("ERROR")
            Console.WriteLine("No se han indicado parámetros en la línea de comandos")
            Console.WriteLine("-----------------------------------------------------")
            Console.WriteLine("El nombre (y path) del ejecutable es:")
            Console.WriteLine(Environment.GetCommandLineArgs(0))
            Console.WriteLine("-----------------------------------------------------")
        End If

    End Sub


    Sub RevisaDocs()
        Dim rs As New ADODB.Recordset
        Dim pgm, param, exe As String
        Dim sts As Integer
        Dim stsTot As Integer = 1
        Dim msgT As String
        Dim tLXmsg As String

        fSql = " SELECT * FROM  RFTASK WHERE STS1 = 0 AND FWRKID = " & fWrkID & " ORDER BY TNUMREG"
        rs = DB.ExecuteSQL(fSql)
        If DB.lastError <> "" Then
            Console.WriteLine(DB.lastSQL)
            Console.WriteLine(DB.lastError)
            Exit Sub
        End If
        Do While Not rs.EOF
            If rs("TTIPO").Value <> "Ultimo" Then
                pgm = rs("TPGM").Value
                param = rs("TPRM").Value
                exe = pgm & " " & param
                sts = 1
                msgT = "Comando Enviado."
                tLXmsg = "EJECUTADA"
                Console.WriteLine(New String("-"c, 50))
                Console.WriteLine(exe)
                fSql = " UPDATE RFTASK SET "
                fSql = fSql & " TLXMSG = 'Ejecutando...', "
                fSql = fSql & " TFPROXIMA = NOW(), STS3 = 1 "
                fSql = fSql & " WHERE TNUMREG = " & rs("TNUMREG").Value
                DB.ExecuteSQL(fSql)
                Try
                    Shell(exe, AppWinStyle.Hide, True, 30000)
                Catch ex As Exception
                    sts = -1
                    stsTot = -1
                    msgT = ex.Message.Replace("'", "''")
                    Console.WriteLine(msgT)
                    DB.WrtSqlError(exe, msgT)
                    tLXmsg = "ERROR"
                End Try
            Else
                tLXmsg = "Finalizado"
                msgT = "Paquete Finalizado"
                sts = stsTot
            End If
            fSql = " UPDATE RFTASK SET "
            fSql = fSql & " TLXMSG = '" & tLXmsg & "', "
            fSql = fSql & " STS1 = " & sts & ", "
            fSql = fSql & " TUPDDAT = NOW() , STS3 = 2,  "
            fSql = fSql & " TMSG = '" & msgT & "'"
            fSql = fSql & " WHERE TNUMREG = " & rs("TNUMREG").Value
            DB.ExecuteSQL(fSql)
            rs.MoveNext()
        Loop
        rs.Close()

    End Sub
    Function AbreConexion() As Boolean

        DB = New clsConexion
        DB.gsDatasource = My.Settings.AS400
        DB.gsLib = My.Settings.AMBIENTE
        Return DB.Conexion()

    End Function

End Module

