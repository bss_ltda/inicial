﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class ReporteT
    Inherits System.Web.Services.WebService
    Dim DB As New clsConexion

    <WebMethod()> _
    Public Function InsertRRCC(ByVal Usuario As String, ByVal Pass As String, ByVal Conso As String, ByVal Manifiesto As String, _
                               ByVal Placa As String, ByVal EstadEntrega As Integer, ByVal FechaReporte As String, _
                               ByVal codUbicacion As String, ByVal Ubicacion As String, _
                               ByVal tipoOrigen As String, ByVal tipoNovedad As String, ByVal Reporte As String) As String
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim fSql, vSql As String
        Dim Result As String = "OK"
        Dim fReporte As String = ""
        Dim dtReporte As Date
        Dim numReporte As String = ""
        Dim pattern As String = "yyyy-MM-dd-hh.mm.ss"

        Try

            Usuario = Usuario.ToUpper
            Pass = Pass.ToUpper

            DB.gsDatasource = My.Settings.AS400
            DB.gsLib = My.Settings.AMBIENTE
            If Not DB.Conexion() Then
                Return "ERROR"
            End If
            rs = DB.ExecuteSQL("SELECT * FROM RCAU WHERE UUSR='" & Usuario & "' AND UPASS='" & Pass & "'")
            If Not rs.EOF Then
                If IsDate(FechaReporte) Then
                    dtReporte = CDate(FechaReporte)
                    fReporte = dtReporte.ToString("yyyy-MM-dd-HH.mm.ss") & ".000000"
                Else
                    Result = "-003: FECHA INCORRECTA"
                    rs.Close()
                    Return Result
                End If

                Dim stsConso As String = "SELECT * FROM RHCC WHERE HCONSO='" & Conso & "'"
                rs1 = DB.ExecuteSQL(stsConso)
                If Not rs1.EOF Then
                    numReporte = DB.CalcConsec("RRCCREPTR", "99999999")
                    fSql = " INSERT INTO RRCC( "
                    vSql = " VALUES ( "
                    fSql = fSql & " RTREP     , " : vSql = vSql & " " & 3 & ", "                                            '//2S0   Tipo Rep
                    fSql = fSql & " RNUM      , " : vSql = vSql & " " & numReporte & ", "                                   '//11S0   
                    fSql = fSql & " RCONSO    , " : vSql = vSql & "'" & Conso & "', "                                       '//6A    Consolidado
                    fSql = fSql & " RMANIF    , " : vSql = vSql & "'" & Left(Manifiesto, 15) & "', "                        '//15A   Manifiesto del Transp
                    fSql = fSql & " RPLACA    , " : vSql = vSql & "'" & Left(UCase(Trim(Placa)), 6) & "', "                 '//6A    Placa
                    fSql = fSql & " RSTSC     , " : vSql = vSql & " " & rs1("HSTS").Value & ", "                            '//2S0   Est Cons
                    fSql = fSql & " RSTSE     , " : vSql = vSql & " " & Left(Trim(EstadEntrega), 2) & ", "                  '//2S0   Est Entrega
                    fSql = fSql & " RFECREP   , " : vSql = vSql & "'" & fReporte & "', "                                    '//26Z   F.Reporte
                    fSql = fSql & " RCODUBI   , " : vSql = vSql & "'" & Left(Trim(UCase(codUbicacion)), 15) & "', "         '//15A   Codigo Ubicacion
                    fSql = fSql & " RUBIC     , " : vSql = vSql & "'" & Left(Trim(UCase(Ubicacion)), 60) & "', "            '//60A   Ubicacion
                    fSql = fSql & " RTIPORI   , " : vSql = vSql & "'" & Left(Trim(UCase(tipoOrigen)), 15) & "', "           '//15A   Tipo de Origen de Nov
                    fSql = fSql & " RTIPNOV   , " : vSql = vSql & "'" & Left(Trim(UCase(tipoNovedad)), 15) & "', "          '//15A   Tipo de Novedad
                    fSql = fSql & " RREPORT   , " : vSql = vSql & "'" & Left(Reporte, 5000) & "', "                         '//15002A  Reporte
                    fSql = fSql & " RCRTUSR   , " : vSql = vSql & "'" & Usuario & "', "                                     '//10A   Usuario
                    fSql = fSql & " RAPP      ) " : vSql = vSql & "'" & "REPORTET" & "' ) "                                 '//15A   App
                    DB.ExecuteSQL(fSql & vSql)
                    fSql = "UPDATE RHCC SET HREPORT = '" & numReporte & "', "
                    fSql = fSql & " HFECREP = '" & fReporte & "'"
                    fSql = fSql & " WHERE HCONSO = '" & Conso & "'"
                    DB.ExecuteSQL(fSql)
                    Result = numReporte & ": REPORTE GUARDADO"
                    If My.Settings.ENVIAMAIL.ToUpper = "SI" Then
                        EnviaCorreo(Conso, Placa, numReporte, "BSS")
                    End If
                Else
                    Result = "-002: CONSOLIDADO NO EXISTE"
                End If
            Else
                Result = "-001: USUARIO/PASSWORD NO VALIDO"
            End If
            rs.Close()
            DB.Close()
        Catch ex As Exception
            DB.WrtSqlError(Conso & " " & Placa, ex.ToString)
            Debug.Print(ex.ToString)
        End Try
        Return Result

    End Function

    Function EnviaCorreo(Consolidado As String, Placa As String, numReporte As String, usrTransp As String) As String
        Dim eMail As New clsCorreoMHT
        Dim resultado As String = ""
        'Dim rs As New ADODB.Recordset
        'Dim fSql As String
        Dim dest As String = usrTransp

        'fSql = " SELECT RCRTUSR  "
        'fSql = fSql & " FROM RRCC  "
        'fSql = fSql & " WHERE RCONSO = '" & Consolidado & "' AND RREPORT = 'Consolidado.' "
        'If DB.OpenRS(rs, fSql) Then
        '    dest &= "," & rs("RCRTUSR").Value
        'End If
        'rs.Close()
        With eMail
            .Inicializa()
            .DB = DB
            .Modulo = "REPORTETRANSITO"
            .Referencia = "Reporte Transito"
            .Asunto = Consolidado & " " & Placa
            .Remitente = "ReportT"
            .Destinatario = dest
            .Mensaje = DB.getLXParam("SERVERRAIZ") & "tm/avisos/ReportT.asp?RNUM=" & numReporte
            resultado = .EnviaCorreo()
        End With

        Return resultado
    End Function

End Class