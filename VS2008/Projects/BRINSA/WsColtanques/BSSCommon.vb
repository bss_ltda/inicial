﻿Option Strict Off
Option Explicit On

Imports System.Globalization

Module BSSCommon
    Dim appSettings As NameValueCollection = _
        ConfigurationManager.AppSettings
    Public Const tNum As Integer = 1
    Public Const tStr As Integer = 2
    Public Const t_iF As Integer = 3
    Public Const t_iV As Integer = 4
    Public Const tFil As Integer = 5

    Public Sql As New mk_Sql
    Public gsConnstring As String
    Public sLIB As String
    Public sVersion As String
    Public Descripcion_Error As String
    Public Error_Tabla As String
    Public fSql As String
    Public vSql As string


    Public Const DB2_DATEDEC = " ( YEAR( NOW() ) * 10000 + MONTH( NOW()  ) * 100 + DAY( NOW() ) )"
    Public Const DB2_TIMEDEC0 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    Public Const DB2_TIMEDEC1 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 ) "

    Public pCultureInfo As New CultureInfo("th-TH")
    Public LibParamCollection As New Dictionary(Of String, String)
    Public Sesiones As Integer

    Public Enum ChkVerApp
        AppOK = 1
        AppVersionIncorrect = 2
        AppSeparadorDecimal = 3
        AppDeshabilitada = 4
        AppEjecutando = 5
    End Enum

    Function CodeCB(ByRef valor As String, Optional ByRef sep As String = "") As String
        Dim aCode As Object

        aCode = Split(valor, IIf(sep = "", " ", sep))
        CodeCB = aCode(0)

    End Function

    Function DescCB(ByRef valor As String, Optional ByRef sep As String = "") As String
        Dim aCode As Object

        aCode = Split(valor, IIf(sep = "", " ", sep))
        DescCB = Trim(Mid(valor, Len(aCode(0)) + 1))

    End Function

    'Public Function Busca(ByVal Sql As String, ByVal DB As ADODB.Connection, Optional ByRef Retorna As String = "") As Boolean
    '    Dim rs As New ADODB.Recordset
    '    Dim r As Boolean
    '    rs.Open(Sql, DB)
    '    If Not rs.EOF Then
    '        If Retorna <> "" Then
    '            Retorna = CStr(rs.Fields(0).Value)
    '        End If
    '    End If
    '    r = Not rs.EOF
    '    rs.Close()
    '    Return r

    'End Function

    Function DateCharDec(ByVal Fecha As Date) As String
        Return Fecha.ToString("yyyyMMdd")
    End Function

    Function TimeCharDec(ByVal Fecha As Date) As String
        Return Fecha.ToString("HHnnss")
    End Function

    Function dtAS400(ByVal fecha As Date) As String
        Return fecha.ToString("yyyy-MM-dd")
    End Function



    Function ParmItem(ByRef arreglo() As String, ByVal tag As String, ByVal valor As String) As String
        Dim i As Integer = UBound(arreglo)
        ReDim Preserve arreglo(i + 1)
        arreglo(i) = tag & ": " & valor & vbCrLf
        Return tag & ": " & valor & vbCrLf
    End Function

    Function LibParam(ByVal prm As String, ByVal sep As String) As String
        Dim Result As String = ""

        If sep = "" Then
            Result = LibParamCollection(prm)
        Else
            Result = LibParamCollection(prm) & sep & prm
        End If
        Return Result

    End Function

    Function LibParam(ByVal prm) As String
        Dim Result As String

        If prm <> "PGMS" Then
            Result = LibParamCollection(prm) & "." & prm
        Else
            Result = LibParamCollection(prm) & "/"
        End If
        Return Result

    End Function

   


    Sub SetConexion(ByVal s As String, ByVal u As String, ByVal p As String, ByVal b As String, ByRef DB As ExecuteSQL)
        DB.setConnectionString("IBMDA400.DataSource", s, u, p, b, "")
    End Sub

    Function SetConexion(ByRef DB As ExecuteSQL, ByVal pais As String) As Boolean
        If pais = "Colombia" Then
            DB.setConnectionString("IBMDA400.DataSource", appSettings(0), appSettings(1), appSettings(2), appSettings(3), "")
        Else
            DB.setConnectionString("IBMDA400.DataSource", appSettings(0), appSettings(8), appSettings(7), appSettings(5), "")
        End If
       Return  DB.Open()
    End Function

    Function Valor(ByVal txt As String) As Double
        If pCultureInfo.NumberFormat.NumberDecimalSeparator = "." Then
            Valor = CDbl(Replace(txt, ",", "."))
        Else
            Valor = CDbl(Replace(txt, ".", ","))
        End If
    End Function
    Sub Delay(ByVal dblSecs As Double)
        Dim CurrentTime As DateTime = Now
        Do While DateDiff("s", Now, CurrentTime) < dblSecs
        Loop
    End Sub

    Function Ceros(ByVal val As Object, ByVal C As Integer)
        Dim sCeros As String = New String("0", C)

        If CInt(C) > Len(Trim(val)) Then
            Ceros = Right(sCeros & Trim(val), C)
        Else
            Ceros = val
        End If

    End Function


    Function Espacios(ByVal val As Object, ByVal C As Integer)
        Dim sCeros As String = New String(" ", C)

        If CInt(C) > Len(Trim(val)) Then
            Espacios = Left(Trim(val) & sCeros, C)
        Else
            Espacios = val
        End If

    End Function

End Module
