﻿Public Class ServicioReportX
    Public fSql As String
    Public vSql As String
    Private WithEvents tim As New Timers.Timer(10000)

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Agregue el código aquí para iniciar el servicio. Este método debería poner
        ' en movimiento los elementos para que el servicio pueda funcionar.
        tim.Enabled = True

    End Sub

    Protected Overrides Sub OnStop()
        ' Agregue el código aquí para realizar cualquier anulación necesaria para detener el servicio.
    End Sub

    Private Sub tim_Elapsed(sender As Object, e As System.Timers.ElapsedEventArgs) Handles tim.Elapsed

        fSql = " SELECT " : vSql = ""
        fSql = fSql & " ECPO      , " : vSql = vSql & "|Orden de Compra"
        fSql = fSql & " ECPOLIN   , " : vSql = vSql & "|CPO Linea"
        fSql = fSql & " EDOCLIN   , " : vSql = vSql & "|Linea Documento"
        fSql = fSql & " ETLINEA   , " : vSql = vSql & "|Linea Tienda"
        fSql = fSql & " ECONSO    , " : vSql = vSql & "|Consolidado"
        fSql = fSql & " EDOCNR    , " : vSql = vSql & "|Documento"
        fSql = fSql & " EPEDID    , " : vSql = vSql & "|Pedido BPCS"
        fSql = fSql & " ELINEA    , " : vSql = vSql & "|Linea  BPCS"
        fSql = fSql & " EPEAN     , " : vSql = vSql & "|Producto EAN"
        fSql = fSql & " EPROD     , " : vSql = vSql & "|Producto BPCS"
        fSql = fSql & " ECANT     , " : vSql = vSql & "|Cantidad Pedida"
        fSql = fSql & " EQCON     , " : vSql = vSql & "|Cantidad Consolid"
        fSql = fSql & " EQDES     , " : vSql = vSql & "|Cantidad Despachada"
        fSql = fSql & " EPESO     , " : vSql = vSql & "|Peso Bruto"
        fSql = fSql & " EPCON     , " : vSql = vSql & "|Peso Consolidado"
        fSql = fSql & " EPDES     , " : vSql = vSql & "|Peso Despachado"
        fSql = fSql & " EPCUST    , " : vSql = vSql & "|Cliente LX"
        fSql = fSql & " ETEAN     , " : vSql = vSql & "|EAN Tienda"
        fSql = fSql & " EPSHIP    , " : vSql = vSql & "|Ship to LX"
        fSql = fSql & " ETIENDA   , " : vSql = vSql & "|Tienda"
        fSql = fSql & " ETIENDAD    " : vSql = vSql & "|Nombre Tienda"
        fSql = fSql & " FROM RDCCAD"
        fSql = fSql & " WHERE EPEDID = 876088"

    End Sub

    Public Sub ReportXls(Sql As String, sTit As String)
        Dim DB As New ADODB.Connection
        Dim RS As New ADODB.Recordset
        Dim aTit() As String
        Dim i As Integer

        DB.Open("Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=DESLXSSA;Password=ABR2013;Persist Security Info=True;Default Collection=DESLX834F;Force Translate=0;")

        With RS
            .CursorLocation = ADODB.CursorLocationEnum.adUseClient
            On Error Resume Next
            .Open(Sql, DB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)
            If Err.Number <> 0 Then
                Exit Sub
            End If
            On Error GoTo 0
        End With

        'Create a new workbook in Excel
        Dim oExcel As Object
        Dim oBook As Object
        Dim oSheet As Object
        oExcel = CreateObject("Excel.Application")
        oBook = oExcel.Workbooks.Add
        oSheet = oBook.Worksheets(1)

        'Transfer the data to Excel
        aTit = Split(sTit, "|")
        If UBound(aTit) > 0 Then
            For i = 0 To RS.Fields.Count - 1
                oSheet.Cells(1, i + 1) = aTit(i + 1)
                oSheet.Cells(1, i + 1).Font.Bold = True
            Next
        Else
            For i = 0 To RS.Fields.Count - 1
                oSheet.Cells(1, i + 1) = (RS.Fields(i).Name) 'TitCampo(rs.Fields(i).Name)
                oSheet.Cells(1, i + 1).Font.Bold = True
            Next
        End If

        oSheet.Range("A2").CopyFromRecordset(RS)
        oSheet.ListObjects.Add(Microsoft.Office.Interop.Excel.XlListObjectSourceType.xlSrcRange, _
                               oSheet.Range(oSheet.Cells(1, 1), oSheet.Cells(RS.RecordCount + 1, RS.Fields.Count)), , _
                               Microsoft.Office.Interop.Excel.XlYesNoGuess.xlYes).Name = "Tabla1"
        oSheet.ListObjects("Tabla1").TableStyle = "TableStyleMedium2"
        oSheet.Cells.EntireColumn.AutoFit()
        oSheet.Cells.Rows("1:1").VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignTop

        'oExcel.Visible = True
        oBook.SaveAs(Replace("C:\Book" & Format(Now, "hhMMss") & ".xlsx", "\\", "\"))
        oExcel.Quit()
        oExcel = Nothing

        RS.Close()
        RS = Nothing

    End Sub

End Class

