﻿Imports Microsoft.Office.Interop
Imports Microsoft.Office.Interop.Excel

Module Module1

    Sub Main()

        'Create a new workbook in Excel
        Dim oExcel As Excel.Application '  Object
        Dim oBook As Excel.Workbook 'Object
        Dim oSheet As Excel.Worksheet  ' Object
        oExcel = CreateObject("Excel.Application")
        oBook = oExcel.Workbooks.Add
        oSheet = oBook.Worksheets(1)

        'Transfer the data to Excel
        For i = 0 To 10
            oSheet.Cells(1, i + 1) = "col" & i
            oSheet.Cells(1, i + 1).Font.Bold = True
        Next

        'Save the Workbook and Quit Excel
        oExcel.DisplayAlerts = False
        'oBook.SaveAs nomArch, CreateBackup:=False
        Try
            oBook.SaveAs(Filename:="E:\Despachos_DB2\ImagenesTransportes\XLS\xls91.xlsx", CreateBackup:=False, ConflictResolution:=2)
        Catch ex As Exception
            Console.WriteLine(ex.Message)
            oExcel.DisplayAlerts = True
            oBook.Close()
            oExcel.Quit()
            End
        End Try
        oExcel.DisplayAlerts = True
        oBook.Close()
        oExcel.Quit()

        oSheet = Nothing
        oBook = Nothing
        oExcel = Nothing

    End Sub

End Module
