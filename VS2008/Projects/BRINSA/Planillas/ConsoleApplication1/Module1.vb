﻿Imports System.IO
Imports IBM.Data.DB2.iSeries
Module Module1
    Private Sub DirectoryCopy( _
               ByVal sourceDirName As String, _
                ByVal destDirName As String, _
                ByVal copySubDirs As Boolean)

        Dim sourceDir As String = sourceDirName
        Dim backupDir As String = destDirName

        Try

            Dim picList1 As String() = Directory.GetDirectories(sourceDir)
            For Each f As String In picList1
                DirectoryCopy(f.ToString(), "\\brinsaonline\Planillas", True)
            Next
            Dim picList As String() = Directory.GetDirectories(sourceDir, "*.pdf")
            Dim txtList As String() = Directory.GetFiles(sourceDir, "*.pdf")

            ' Copy picture files.

            For Each f As String In picList
                'Remove path from the file name.
                My.Computer.FileSystem.RenameFile(f.ToString, "" & f.ToString)
                Dim fName As String = f.Substring(sourceDir.Length + 1)
                My.Computer.FileSystem.RenameFile(f.ToString, "" & fName)
                fName = "" & fName
                If Not File.Exists(backupDir & "\" & fName) Then
                    File.Copy(Path.Combine(sourceDir, fName), Path.Combine(backupDir, fName), True)
                End If
                File.Delete(sourceDir & "\" & fName)
            Next

            ' Copy text files.
            For Each f As String In txtList

                'Remove path from the file name.
                Dim fName As String = f.Substring(sourceDir.Length + 1)

                Try
                    ' Will not overwrite if the destination file already exists.
                    My.Computer.FileSystem.RenameFile(f.ToString, "" & fName)
                    fName = "" & fName
                    If Not File.Exists(backupDir & "\" & fName) Then
                        File.Copy(Path.Combine(sourceDir, fName), Path.Combine(backupDir, fName))
                    End If
                    File.Delete(sourceDir & "\" & fName)
                    ' Catch exception if the file was already copied.
                Catch copyError As IOException
                    Console.WriteLine(copyError.Message)
                End Try
            Next

            For Each f As String In txtList
                File.Delete(f)
            Next

            For Each f As String In picList
                File.Delete(f)
            Next

        Catch dirNotFound As DirectoryNotFoundException
            Console.WriteLine(dirNotFound.Message)
        End Try

    End Sub
    Sub Main()
        Dim fSql As String
        Dim DB As iDB2Connection = New IBM.Data.DB2.iSeries.iDB2Connection
        Dim flag As Integer
        Dim iDB2 As iDB2Command
        Dim R As Integer
        Dim Total As Integer
        Dim Archivo As String
        Dim Descartados As String

        DB.ConnectionString = "Data Source=192.168.10.1;Default Collection=ERPLX834F;Persist Security Info=True;User ID=APLLX;Password=LXAPL"
        DB.Open()
        Console.WriteLine("Servicio iniciado")


        Dim adaptador As iDB2DataAdapter = New IBM.Data.DB2.iSeries.iDB2DataAdapter

        fSql = "Update ZCC SET CCMNTM = (Hour(Now()) * 10000 + Minute(Now()) * 100 + Second(Now())) WHERE CCTABL = 'RFVBVER' AND CCCODE = 'DOCUMENTOS'"
        iDB2 = New IBM.Data.DB2.iSeries.iDB2Command(fSql, DB)
        iDB2.ExecuteNonQuery()
        adaptador = New IBM.Data.DB2.iSeries.iDB2DataAdapter
        fSql = " SELECT * FROM RECCZ WHERE ZFLAG = 0"
        iDB2 = New IBM.Data.DB2.iSeries.iDB2Command(fSql, DB)
        adaptador.SelectCommand = iDB2
        Dim tareas As DataSet = New DataSet
        adaptador.Fill(tareas, "RECCZ")
        Dim tbl As DataTable = tareas.Tables("RECCZ")
        Dim dr As DataRow
        Dim zip

        Archivo = ""
        Descartados = ""
        Total = 0
        R = 0
        For Each dr In tareas.Tables("RECCZ").Rows
            flag = 1

            Dim aux As String
            aux = dr("ZARCHIVO").ToString
            Dim cantidad As Integer
            Dim flagg As Integer
            flag = 0
            cantidad = Len(aux)

            If (aux(cantidad - 3) = "z") Then
                zip = New Chilkat.Zip
                flagg = 1
                Console.WriteLine("Archivo Zip")

            Else
                zip = New Chilkat.Rar
                flagg = 2
                Console.WriteLine("Archivo Rar")
            End If



            '   zip.UnlockComponent("Anything for 30-day trial")
            Dim success As Boolean
            If flagg = 1 Then
                Dim unlocked As Boolean
                unlocked = zip.UnlockComponent("SMANRIZIP_ZrtwHkdh6IvN")
                If (Not unlocked) Then

                    Console.WriteLine(zip.LastErrorText)
                    Exit Sub
                End If
                '  success = zip.OpenZip("E:\Despachos_DB2\ImagenesTransportes\PLANILLAS\" & dr("ZARCHIVO"))
                success = zip.OpenZip("\\192.168.10.6\Despachos\ImagenesTransportes\PLANILLAS\" & dr("ZARCHIVO"))
            Else
                success = zip.Open("\\192.168.10.6\Despachos\ImagenesTransportes\PLANILLAS\" & dr("ZARCHIVO"))
                If (Not success) Then
                    Console.WriteLine(zip.LastErrorText)

                    Exit Sub
                End If
            End If

            If (success) Then

                Console.WriteLine("El archivo : " & dr("ZARCHIVO") & "abrio correctamente")
            End If
            If (Not success) Then
                Console.WriteLine("El archivo : " & dr("ZARCHIVO") & "no abrio correctamente")
                adaptador = New IBM.Data.DB2.iSeries.iDB2DataAdapter
                fSql = "UPDATE RECCZ SET ZDESCART='" & "\\192.168.10.6\Despachos\ImagenesTransportes\PLANILLAS\" & dr("ZARCHIVO") & "' WHERE ZID =" & dr("ZID")
                iDB2 = New IBM.Data.DB2.iSeries.iDB2Command(fSql, DB)
                iDB2.ExecuteNonQuery()
                flag = flag + 1
            End If

            Dim n As Integer
            Dim i As Integer

            Dim entryZip As Chilkat.ZipEntry
            Dim entryRar As Chilkat.RarEntry



            n = zip.NumEntries
            Console.WriteLine("Numero de archivos que contiene el Zip/Rar: " & n)

            If flagg = 1 Then

                For i = 0 To n - 1
                    entryZip = zip.GetEntryByIndex(i)
                    If (entryZip.FileName.EndsWith(".pdf")) Then
                        '                   entryZip.FileName = "PC-" & entryZip.FileName
                        '                  EventLog1.WriteEntry("Extraido el archivo :" & entry.FileName)
                    End If
                Next

                n = zip.unzip("C:\TestDirectory1")

                DirectoryCopy("C:\TestDirectory1", "\\brinsaonline\Planillas", True)
                'CopiaArchivoConProgreso("C:\TestDirectory1", "\\docushare\Prueba", dr("ZARCHIVO"))
                'My.Computer.FileSystem.CopyDirectory("C:\TestDirectory1", "\\docushare\Prueba", True)
                Dim Nombre As String
                Dim NombreTemp As String
                Dim Ascii As Integer
                If (n < 0) Then
                    Console.WriteLine("No se pudo extraer ningun archivo ERROR :")

                    flag = flag + 1
                End If
                n = zip.NumEntries

                For i = 0 To n - 1

                    entryZip = zip.GetEntryByIndex(i)
                    If (entryZip.IsDirectory = False) Then
                        adaptador = New IBM.Data.DB2.iSeries.iDB2DataAdapter
                        Nombre = ""
                        NombreTemp = "" & entryZip.FileName
                        Dim aNombre() As String
                        aNombre = Split(NombreTemp, "\")
                        If UBound(aNombre) = 1 Then
                            NombreTemp = aNombre(1)
                        Else
                            NombreTemp = aNombre(0)
                        End If
                        NombreTemp = NombreTemp.Replace(".pdf", "")
                        For j = 1 To Len(NombreTemp)
                            Ascii = Asc(Mid(NombreTemp, j, 1))
                            If Ascii >= Asc("0") And Ascii <= Asc("9") Then
                                Nombre = Nombre & NombreTemp(j - 1)
                            End If
                        Next


                        fSql = "UPDATE RECC SET ESCAN=2,EUDS01=2, ESCANF=NOW(), EPLAJPEG = 'ZIP' WHERE EPLANI = '" & Nombre & "' AND ECONSO = (SELECT DISTINCT HCONSO FROM RHCC WHERE HPROVE= " & dr("ZPROVE") & "  AND HCONSO = ECONSO)"
                        iDB2 = New IBM.Data.DB2.iSeries.iDB2Command(fSql, DB)
                        iDB2.CommandTimeout = 0
                        R = iDB2.ExecuteNonQuery()
                        If R = 0 Then
                            Descartados = Descartados & "," & Nombre
                        Else
                            Archivo = Archivo & "," & Nombre
                        End If
                        Total = Total + R
                    End If
                Next
            Else

                '               For i = 0 To n - 1
                ' entryRar = zip.GetEntryByIndex(i)
                ' If (entryRar.Filename.EndsWith(".pdf")) Then
                ' entryRar.Filename = "PC-" & entryRar.Filename
                'End If
                'Next
                Dim Nombre As String
                Dim NombreTemp As String
                Dim Ascii As Integer
                n = zip.Unrar("C:\TestDirectory1")
                DirectoryCopy("C:\TestDirectory1", "\\brinsaonline\Planillas", True)

                'My.Computer.FileSystem.CopyDirectory("C:\TestDirectory1", "\\docushare\Prueba", True)

                If (n < 0) Then
                    Console.WriteLine("No se pudo extraer ningun archivo ERROR :")

                    flag = flag + 1
                End If
                n = zip.NumEntries

                For i = 0 To n - 1

                    entryRar = zip.GetEntryByIndex(i)
                    If (entryRar.IsDirectory = False) Then
                        adaptador = New IBM.Data.DB2.iSeries.iDB2DataAdapter
                        Nombre = ""
                        NombreTemp = "" & entryRar.Filename
                        Dim aNombre() As String
                        aNombre = Split(NombreTemp, "\")
                        If UBound(aNombre) = 1 Then
                            NombreTemp = aNombre(1)
                        Else
                            NombreTemp = aNombre(0)
                        End If
                        NombreTemp = NombreTemp.Replace(".pdf", "")
                        For j = 1 To Len(NombreTemp)
                            Ascii = Asc(Mid(NombreTemp, j, 1))
                            If Ascii >= Asc("0") And Ascii <= Asc("9") Then
                                Nombre = Nombre & NombreTemp(j - 1)
                            End If
                        Next


                        fSql = "UPDATE RECC SET ESCAN=2,EUDS01=2, ESCANF=NOW() , EPLAJPEG = 'ZIP'WHERE EPLANI = '" & Nombre & "' AND ECONSO = (SELECT DISTINCT HCONSO FROM RHCC WHERE HPROVE= " & dr("ZPROVE") & "  AND HCONSO = ECONSO)"
                        iDB2 = New IBM.Data.DB2.iSeries.iDB2Command(fSql, DB)
                        iDB2.CommandTimeout = 0
                        R = iDB2.ExecuteNonQuery()
                        If R = 0 Then
                            Descartados = Descartados & "," & Nombre
                        Else
                            Archivo = Archivo & "," & Nombre
                        End If
                        Total = Total + R
                    End If
                Next


            End If
            Dim diferencia As Integer
            diferencia = 0
            diferencia = n - Total
            If flag <= 1 Then
                adaptador = New IBM.Data.DB2.iSeries.iDB2DataAdapter
                ' fSql = "UPDATE RECCZ SET ZFLAG=1, ZCARGADAS='" & Archivo & "', ZDESCART='" & Descartados & "' WHERE ZID = " & dr("ZID")
                fSql = "UPDATE RECCZ SET ZFLAG=1 WHERE ZID = " & dr("ZID")

                Console.WriteLine("Proceso finalizado exitosamente archivos descartados:" & diferencia)

                iDB2 = New IBM.Data.DB2.iSeries.iDB2Command(fSql, DB)
                iDB2.ExecuteNonQuery()
            End If
            If flag > 1 Then
                adaptador = New IBM.Data.DB2.iSeries.iDB2DataAdapter
                'fSql = "UPDATE RECCZ SET ZFLAG=2, ZCARGADAS='" & Archivo & "', ZDESCART='" & Descartados & "' WHERE ZID =" & dr("ZID")
                fSql = "UPDATE RECCZ SET ZFLAG=2  WHERE ZID =" & dr("ZID")
                Console.WriteLine("Proceso finalizado incorrectamente archivos descartados:" & diferencia)
                iDB2 = New IBM.Data.DB2.iSeries.iDB2Command(fSql, DB)
                iDB2.ExecuteNonQuery()
            End If
        Next
        Console.WriteLine("Servicio Finalizado")


        Console.WriteLine("Proceso iniciado")
        Dim Files As String(), Filess As String
        '       Dim Nombre As String
        '      Dim NombreTemp As String
        '     Dim NombreAux As String
        'Dim Ascii As Integer

        Dim Modificados As Integer
        Modificados = 0
        Archivo = ""
        Descartados = ""
        '   DB.ConnectionString = "Data Source=192.168.10.1;Default Collection=ERPLX834F;Persist Security Info=True;User ID=APLLX;Password=LXAPL"
        '  DB.Open()
        Console.WriteLine("conexion exitosa iniciado")
        Files = IO.Directory.GetFiles("\\brinsaonline\Planillas_Temp", "*.pdf")
        Console.WriteLine("conexion con brinsaonline exitosa")
        Dim prueba As Integer
        prueba = 0

        For prueba = 0 To 5
            For Each Filess In Files
                Dim Nombre As String
                Dim NombreTemp As String
                Dim NombreAux As String
                Dim auxiliar As Integer
                auxiliar = 0
                Nombre = ""
                NombreTemp = ""
                NombreAux = ""
                NombreTemp = IO.Path.GetFileNameWithoutExtension(Filess)
                For j = 1 To Len(NombreTemp)
                    Dim Ascii As Integer
                    Ascii = Asc(Mid(NombreTemp, j, 1))
                    If Ascii >= Asc("0") And Ascii <= Asc("9") Then
                        NombreAux = NombreAux & NombreTemp(j - 1)
                    End If
                Next
                If Len(NombreAux) >= 6 Then
                    auxiliar = 6
                Else
                    auxiliar = Len(NombreAux)
                End If
                For j = 1 To auxiliar
                    Nombre = Nombre & NombreAux(j - 1)
                Next
                If Nombre = "" Then
                    Nombre = "00000"
                End If


                Console.WriteLine("planilla No" & Nombre)
                fSql = "UPDATE RECC SET ESCAN=2,EUDS01=1, ESCANF=NOW(), EPLAJPEG = 'ZIP' WHERE EPLANI = '" & Nombre & "' AND ECONSO = (SELECT DISTINCT HCONSO FROM RHCC WHERE HPROVE= 5115  AND HCONSO = ECONSO)"
                iDB2 = New IBM.Data.DB2.iSeries.iDB2Command(fSql, DB)
                iDB2.CommandTimeout = 0

                R = iDB2.ExecuteNonQuery()
                If R = 0 Then
                    Descartados = Descartados & "," & Nombre
                Else
                    Archivo = Archivo & "," & Nombre
                    If Not File.Exists("\\brinsaonline\Planillas\" & Nombre & ".pdf") Then
                        File.Move("\\brinsaonline\Planillas_Temp\" & NombreTemp & ".pdf", " \\brinsaonline\Planillas\" & Nombre & ".pdf")
                        Console.WriteLine("planilla No" & Nombre & "Nuevo destino")
                        Modificados = Modificados + 1
                    Else
                        Console.WriteLine("planilla No" & Nombre & "ya existe en el Nuevo destino")
                    End If
                End If
                Total = Total + R
            Next
        Next

        iDB2 = New IBM.Data.DB2.iSeries.iDB2Command("UPDATE RFPARAM SET CCUDTS = NOW() WHERE CCTABL='PROGRAMADAS' AND CCCODE = 'PLANILLAS'", DB)
        iDB2.CommandTimeout = 0
        R = iDB2.ExecuteNonQuery()
        Console.WriteLine("Servicio Finalizado Archivos Afectados: " & Modificados)

    End Sub

End Module

