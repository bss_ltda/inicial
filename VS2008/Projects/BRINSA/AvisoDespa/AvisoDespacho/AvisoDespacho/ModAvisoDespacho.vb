﻿Imports BSSClassLibrary
Imports System
Imports System.IO
Imports System.Collections

Module ModAvisoDespacho
    Dim Conso As String
    Dim Pedido As String
    Dim fSql As String
    Dim db As New bssConexion
    Dim sql As New bssSQL
    Dim u As New bssUtil
    Dim nArch As Integer
    Dim sArch As String
    Dim nDoc As Long
    Dim numUNH As Long
    Dim numBGM As Long
    Dim sFolder As String
    Dim Archivo As StreamWriter

    Dim aEtiqueta() As String

    Const TIQ_SSCC As Integer = 14
    Const TIQ_COD_CLIENTE As Integer = 1
    Const TIQ_CLIENTE As Integer = 2
    Const TIQ_PUNTO_ENTREGA As Integer = 3
    Const TIQ_DIR_PUNTOE As Integer = 4
    Const TIQ_TIT_AGRUPACION As Integer = 5
    Const TIQ_NOM_AGRUPACION As Integer = 7
    Const TIQ_EAN_AGRUPACION As Integer = 6
    Const TIQ_NUM_INTERNO_PUNTO As Integer = 8
    Const TIQ_OC As Integer = 10
    Const TIQ_FACTURA As Integer = 11
    Const TIQ_FECHA_ENVIO As Integer = 12
    Const TIQ_CAJAS As Integer = 13
    Const TIQ_UNIDADES As Integer = 15
    Const TIQ_CONSOLID As Integer = 16
    Const TIQ_TOTAL As Integer = 17



    Sub Main(ByVal Parametros As String())
        Conso = Parametros(0)
        Pedido = Parametros(1)
        db.abrirConexion(My.Settings.AMBIENTE, "AS400")
        verAvisoDespacho()
    End Sub
    Public Function verAvisoDespacho() As String
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim msg As String

        ' fSql = "SELECT DPLANI, DPEDID, DLINEA, DCANT FROM RDCC WHERE DCONSO='" & Conso & "' AND DPEDID = " & Pedido
        'rs = db.ExecuteSQL(fSql)

        'Do While Not rs.EOF
        '    fSql = " UPDATE RDCCAD "
        '   'fSql = "SET EPLANI=" & rs("DPLANI").Value.ToString
        '    fSql = fSql & ", ECONSO='" & Conso & "'"
        '    fSql = fSql & ", EQDES  =" & rs("DCANT").Value.ToString
        '    fSql = fSql & " WHERE EPEDID =" & Pedido & " AND ELINEA=" & rs("DLINEA").Value.ToString
        '    fSql = fSql & " AND EPLANI<>0"
        '    db.ExecuteSQL(fSql)
        '    rs.MoveNext()
        'Loop
        'rs.Close()

        ReDim aEtiqueta(TIQ_TOTAL)
        aEtiqueta(TIQ_CONSOLID) = Conso
        'Consolidado/OCompra Cliente/Pedido BPCS
        fSql = "SELECT DISTINCT ECONSO, ECPO , EPCUST, EPEDID, EEDIBGM FROM RDCCAD WHERE ECONSO='" & Conso & "' AND EPEDID =" & Pedido
        rs = db.ExecuteSQL(fSql)
        Do While Not rs.EOF
            rs1 = db.ExecuteSQL("SELECT CMFF05, CMFF06, CNME FROM RCM WHERE CCUST = " & rs("EPCUST").Value.ToString)
            If rs1("CMFF05").Value.ToString <> "" Then
                aEtiqueta(TIQ_OC) = rs("ECPO").Value.ToString
                aEtiqueta(TIQ_COD_CLIENTE) = rs("EPCUST").Value.ToString
                aEtiqueta(TIQ_CLIENTE) = rs1("CNME").Value.ToString
                msg = msg & "OC: " & rs("ECPO").Value.ToString
                msg = msg & vbCr & "Cliente: " & rs("EPCUST").Value.ToString
                '============================================================================================================================
                msg = msg & vbCr & "Mensaje: " & wrtAvisoDespacho(rs("ECPO").Value.ToString, rs("EPEDID").Value.ToString, rs("EPCUST").Value.ToString, rs1("CMFF06").Value.ToString, rs1("CMFF05").Value.ToString, rs("EEDIBGM").Value.ToString)
                '============================================================================================================================
                '        msg = msg & vbCr & Char(47, "=") & vbCr
            End If
            rs1.Close()
            rs.MoveNext()
        Loop
        rs.Close()
        rs = Nothing
        rs1 = Nothing
        verAvisoDespacho = msg
    End Function



    Private Function wrtAvisoDespacho(ByVal OCompraEDI As String, ByVal Pedido As String, ByVal Cliente As String, _
                             ByVal EANCliente As String, ByVal EANAvisoDespa As String, ByVal EANTipMsg As String) As String



        nArch = FreeFile()
        sFolder = "E:\AppData\AvisoDespacho"
        ChkFolder(sFolder)
        nDoc = db.CalcConsec("DESADV", "9999")
        sArch = sFolder & "\" & "AD-" & Pedido & "-" & OCompraEDI & "-" & Format(nDoc, "0000") & ".edi"

        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim segLIN As Integer
        Dim iCPS As Integer
        Dim s As Integer
        Dim PtoEnvio As String

        Try
            Dim ruta As String = sArch
            Archivo = File.AppendText(ruta)
            Archivo.Write("UNB+UNOA:2")
            Archivo.Write("+7703812000017")
            Archivo.Write("+" & EANAvisoDespa)
            Archivo.Write("+" & Format(Now, "yymmdd:hhmm"))
            Archivo.Write("+" & Format(nDoc, "0000") & "++DESADV'")
            numUNH = db.CalcConsec("EDIUNH", "999999")
            s = 1
            Archivo.Write("UNH+BR" & Format(numUNH, "000000") & "+DESADV:D:96A:UN:EAN005'")
            s = s + 1

            numBGM = db.CalcConsec("EDIBGM", "9999")
            Archivo.Write("BGM+351+" & numBGM & "+9'")
            s = s + 1
            Archivo.Write("DTM+137:" & Format(Date.Today, "yyyymmdd") & ":102'")
            s = s + 1
            Archivo.Write("DTM+11:" & Format(Date.Today, "yyyymmdd") & ":102'")
            s = s + 1
            aEtiqueta(TIQ_FECHA_ENVIO) = Format(Date.Today, "yyyy-mm-dd")
            rs1 = db.ExecuteSQL("SELECT DRDTE, DSHIP FROM RDCC WHERE DPEDID=" & Pedido)
            PtoEnvio = rs1("DSHIP").Value.ToString
            Archivo.Write("DTM+17:" & Format(CDate(Left(rs1("DRDTE").Value.ToString, 10)), "yyyymmdd") & ":102'")
            s = s + 1
            rs1.Close()
            Archivo.Write("RFF+ON:" & OCompraEDI & "'")
            s = s + 1
            rs1 = db.ExecuteSQL("SELECT ILDPFX, ILINVN FROM SIL WHERE ILORD = " & Pedido)
            If Not rs1.EOF Then
                aEtiqueta(TIQ_FACTURA) = rs1("ILDPFX").Value.ToString & Format(rs1("ILINVN").Value.ToString, "00000000")
                Archivo.Write("RFF+IV:" & rs1("ILDPFX").Value.ToString & Format(rs1("ILINVN").Value.ToString, "00000000") & "'")
                s = s + 1
            Else
                aEtiqueta(TIQ_FACTURA) = "0"
            End If
            rs1.Close()
            Archivo.Write("NAD+SU+7703812000017::9'")
            s = s + 1
            rs1 = db.ExecuteSQL("SELECT STDOCK, TNAME, TADR1  FROM EST WHERE TCUST=" & Cliente & " AND TSHIP=" & PtoEnvio)
            aEtiqueta(TIQ_PUNTO_ENTREGA) = rs1("TNAME").Value.ToString
            aEtiqueta(TIQ_DIR_PUNTOE) = rs1("TADR1").Value.ToString
            Archivo.Write("NAD+DP+" & rs1("STDOCK").Value.ToString & "::9'")
            s = s + 1
            rs1.Close()
            Archivo.Write("NAD+BY+" & EANCliente & "::9'")
            s = s + 1
            Archivo.Write("TDT+20++30+31'")
            s = s + 1
            Archivo.Write("EQD+PA'")
            s = s + 1
            iCPS = 1
            Archivo.Write("CPS+" & iCPS & "'")
            s = s + 1

            If EANTipMsg = "YB1" Then
                segLIN = AvisoDespPredistrib(nArch, OCompraEDI, s, iCPS)     's se actualiza en la rutina
            End If

            If EANTipMsg = "220" Or EANTipMsg = "YA9" Then
                segLIN = AvisoDespAlmacena(nArch, OCompraEDI, s, iCPS)     's se actualiza en la rutina
            End If

            Archivo.Write("CNT+2:" & segLIN & "'")
            s = s + 1

            'El mensaje tiene s segmentos. Número de referenciadel mensaje. Debe ser igual al usado en el UNH
            Archivo.Write("UNT+" & s & "+BR" & Format(numUNH, "000000") & "'")

            'FINAL DE INTERCAMBIO. Número de mensajes o grupos funcionales en el intercambio.
            'Idéntico al DE 0020 del segmento UNB
            Archivo.Write("UNZ+1+" & Format(nDoc, "0000") & "'")

            Archivo.Flush()
            Archivo.Close()
            Console.WriteLine("Escritura realizada con éxito")
        Catch ex As Exception
            Console.WriteLine("Escritura realizada incorrectamente")
        End Try

        wrtAvisoDespacho = sArch

    End Function


    Public Sub ChkFolder(ByVal sFolder As String)
        Dim fso
        Dim aFolder
        Dim i As Integer
        Dim lFolder As String
        Dim sep As String

        fso = CreateObject("Scripting.FileSystemObject")
        aFolder = Split(sFolder, "\")
        For i = 0 To UBound(aFolder)
            lFolder = lFolder & sep & aFolder(i)
            If Not fso.FolderExists(lFolder) Then
                Call MkDir(lFolder)
            End If
            sep = "\"
        Next

    End Sub


    Private Function AvisoDespPredistrib(ByVal nArch As Integer, ByVal OCompraEDI As String, ByVal s As Integer, ByVal iCPS As Integer) As Integer
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim SSCC As String
        Dim segLIN As Integer

        'El contenido total del envío son n cajas
        fSql = "SELECT SUM(EQDES) AS CAJAS FROM RDCCAD WHERE ECPO='" & OCompraEDI & "'"
        fSql = fSql & " AND ECONSO='" & aEtiqueta(TIQ_CONSOLID) & "'"
        rs = db.ExecuteSQL(fSql)

        Archivo.Write("PAC+" & rs("CAJAS").Value.ToString & "++CS'")
        s = s + 1
        rs.Close()

        'El CPS se coloca antes de iniciar la descripción de la estiba identificada con el SSCC 377024370000000038,
        'además este segmento indica que esa estiba depende el CPS 1, que es el envió total.
        'SSCC Serial Shipping Container Code EAN128

        'Por punto de envio
        fSql = "SELECT ETEAN, ESSCC, SUM(EQDES) AS CAJAS FROM RDCCAD "
        fSql = fSql & " WHERE ECPO='" & OCompraEDI & "' "
        fSql = fSql & " AND ECONSO='" & aEtiqueta(TIQ_CONSOLID) & "'"
        fSql = fSql & " GROUP BY ETEAN, ESSCC  HAVING SUM(EQDES) > 0"  ' POR PRUEBAS
        rs = db.ExecuteSQL(fSql)
        Do While Not rs.EOF
            iCPS = iCPS + 1
            fSql = "SELECT SUM(EQDES*IVULP) FROM RDCCAD INNER JOIN IIM ON IPROD=EPROD "
            fSql = fSql & " WHERE ECPO='" & OCompraEDI & "' AND ETEAN='" & rs("ETEAN").Value.ToString & "'"
            fSql = fSql & " AND ECONSO='" & aEtiqueta(TIQ_CONSOLID) & "'"
            rs1 = db.ExecuteSQL(fSql)
            aEtiqueta(TIQ_UNIDADES) = rs1(0).Value.ToString
            rs1.Close()

            'Numero interno, Nombre
            rs1 = db.ExecuteSQL("SELECT STAD4, TNAME, TSHIP  FROM EST WHERE TCUST=" & aEtiqueta(TIQ_COD_CLIENTE) & " AND STDOCK='" & rs("ETEAN").Value.ToString & "'")
            aEtiqueta(TIQ_CAJAS) = rs("CAJAS").Value.ToString
            aEtiqueta(TIQ_TIT_AGRUPACION) = "PTO. VENTA :"
            aEtiqueta(TIQ_NOM_AGRUPACION) = rs1("TNAME").Value.ToString
            aEtiqueta(TIQ_EAN_AGRUPACION) = rs("ETEAN").Value.ToString
            '      aEtiqueta(TIQ_NUM_INTERNO_PUNTO) = IIf(rs1!STAD4 = "", "**" & rs1!TSHIP, rs1!STAD4)
            aEtiqueta(TIQ_NUM_INTERNO_PUNTO) = Format(Val(rs1("STAD4").Value.ToString), "000")
            rs1.Close()

            'Inicio Agrupacion Logistica
            Archivo.Write("CPS+" & iCPS & "+1'")
            s = s + 1
            Archivo.Write("PAC+" & rs("CAJAS").Value.ToString & "++CS'")
            s = s + 1
            Archivo.Write("PCI+33E'")
            s = s + 1
            'Los embalajes enviados se encuentran marcados con un
            'Código Serial de Unidad de Empaque.

            '(00)(3)770 3812 consecutivo
            If rs("ESSCC").Value.ToString = "" Then
                SSCC = "0037703812" & Format(db.CalcConsec("SSCC", "999999"), "0000000")
                SSCC = SSCC & Digito_de_Control_SSCC(SSCC)
                fSql = "UPDATE RDCCAD SET ESSCC='" & SSCC & "'"
                fSql = fSql & " WHERE ECPO ='" & OCompraEDI & "' AND ETEAN='" & rs("ETEAN").Value.ToString & "'"
                fSql = fSql & " AND ECONSO='" & aEtiqueta(TIQ_CONSOLID) & "'"
                db.ExecuteSQL(fSql)
            Else
                SSCC = rs("ESSCC").Value.ToString
            End If

            aEtiqueta(TIQ_SSCC) = SSCC

            Archivo.Write("GIN+BJ+" & aEtiqueta(TIQ_SSCC) & "'")
            s = s + 1
            'El embalaje en cuestión se encuentra identificado
            'con el SSCC 377024370000000038.
            fSql = "SELECT * FROM RDCCAD WHERE ECPO='" & OCompraEDI & "' AND ETEAN='" & rs("ETEAN").Value.ToString & "'"
            fSql = fSql & " AND ECONSO='" & aEtiqueta(TIQ_CONSOLID) & "'"
            rs1 = db.ExecuteSQL(fSql)
            Do While Not rs1.EOF
                segLIN = segLIN + 1
                'La información corresponde a un artículo identificado con el código EAN.
                Archivo.Write("LIN+" & segLIN & "++" & rs1("EPEAN").Value.ToString & ":EN'")
                s = s + 1
                'La cantidad enviada del producto especificado en el segmento anterior LIN es de 10 y NAR es la medida

                Archivo.Write("QTY+12:" & rs1("EQDES").Value.ToString & ":NAR'")
                s = s + 1 'RS!EQDES
                'NAR = Número de artículos
                'Lugar de entrega
                Archivo.Write("LOC+7+" & rs1("ETEAN").Value.ToString & "::9'")
                s = s + 1
                rs1.MoveNext()
            Loop
            rs1.Close()
            rs.MoveNext()
            '  EscribeEtiqueta()
        Loop
        rs.Close()
        rs = Nothing
        rs1 = Nothing
        AvisoDespPredistrib = segLIN

    End Function


    Private Function AvisoDespAlmacena(ByVal nArch As Integer, ByVal OCompraEDI As String, ByVal s As Integer, ByVal iCPS As Integer) As Integer
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim SSCC As String
        Dim segLIN As Integer


        fSql = "SELECT DISTINCT EPEAN FROM RDCCAD WHERE ECPO='" & OCompraEDI & "'"
        fSql = fSql & " AND ECONSO='" & aEtiqueta(TIQ_CONSOLID) & "'"

        rs = db.ExecuteSQL(fSql)
        Archivo.Write("PAC+" & Format(rs.RecordCount, "000000") & "++CS'")
        s = s + 1
        rs.Close()

        'El CPS se coloca antes de iniciar la descripción de la estiba identificada con el SSCC 377024370000000038,
        'además este segmento indica que esa estiba depende el CPS 1, que es el envió total.
        'SSCC Serial Shipping Container Code EAN128

        'Por producto
        fSql = "SELECT EPEAN, EPROD, ESSCC, SUM(EQDES) AS CAJAS  FROM RDCCAD "
        fSql = fSql & " WHERE ECPO='" & OCompraEDI & "' "
        fSql = fSql & " AND ECONSO='" & aEtiqueta(TIQ_CONSOLID) & "'"
        fSql = fSql & " GROUP BY EPEAN, EPROD, ESSCC HAVING SUM(EQDES) > 0"
        rs = db.ExecuteSQL(fSql)
        Do While Not rs.EOF

            'Numero interno, Nombre
            rs1 = db.ExecuteSQL("SELECT IDESC, IVULP FROM IIM WHERE IPROD='" & rs("EPROD").Value.ToString & "'")
            aEtiqueta(TIQ_CAJAS) = rs("CAJAS").Value.ToString
            aEtiqueta(TIQ_UNIDADES) = rs("CAJAS").Value.ToString * rs1("IVULP").Value.ToString
            aEtiqueta(TIQ_TIT_AGRUPACION) = "PRODUCTO   :"
            aEtiqueta(TIQ_NOM_AGRUPACION) = rs1("IDESC").Value.ToString
            aEtiqueta(TIQ_EAN_AGRUPACION) = rs("EPEAN").Value.ToString
            aEtiqueta(TIQ_NUM_INTERNO_PUNTO) = ""
            rs1.Close()

            iCPS = iCPS + 1
            Archivo.Write("CPS+" & iCPS & "+1'")
            s = s + 1
            Archivo.Write("PAC+" & rs("CAJAS").Value.ToString & "++CS'")
            s = s + 1
            Archivo.Write("PCI+33E'")
            s = s + 1
            'Los embalajes enviados se encuentran marcados con un
            'Código Serial de Unidad de Empaque.

            ' (00)(3)770 3812 consecutivo
            If rs("ESSCC").Value.ToString = "" Then
                SSCC = "0037703812" & Format(db.CalcConsec("SSCC", "999999"), "0000000")
                SSCC = SSCC & Digito_de_Control_SSCC(SSCC)
                fSql = "UPDATE RDCCAD SET ESSCC='" & SSCC & "'"
                fSql = fSql & " WHERE ECPO='" & OCompraEDI & "' AND EPROD='" & rs("EPROD").Value.ToString & "'"
                fSql = fSql & " AND ECONSO='" & aEtiqueta(TIQ_CONSOLID) & "'"
                db.ExecuteSQL(fSql)
            Else
                SSCC = rs("ESSCC").Value.ToString
            End If
            aEtiqueta(TIQ_SSCC) = SSCC

            Archivo.Write("GIN+BJ+" & aEtiqueta(TIQ_SSCC) & "'")
            s = s + 1
            'El embalaje en cuestión se encuentra identificado
            'con el SSCC 377024370000000038.
            segLIN = segLIN + 1
            'La información corresponde a un artículo identificado con el código EAN.
            Archivo.Write("LIN+" & segLIN & "++" & rs("EPEAN").Value.ToString & ":EN'")
            s = s + 1
            'La cantidad enviada del producto especificado en el segmento anterior LIN es de 10 y NAR es la medida
            Archivo.Write("QTY+12:" & rs("CAJAS").Value.ToString & ":NAR'")
            s = s + 1
            rs.MoveNext()
            EscribeEtiqueta()
        Loop
        rs.Close()
        rs = Nothing
        AvisoDespAlmacena = segLIN

    End Function

    Private Sub EscribeEtiqueta()
        Dim fSql As String
        Dim vSql As String

        fSql = "INSERT INTO WMSFADT ("
        vSql = "VALUES( "
        fSql = fSql & "QSSCC, " : vSql = vSql & "'" & aEtiqueta(TIQ_SSCC) & "'" & ", "
        fSql = fSql & "QCUST, " : vSql = vSql & " " & aEtiqueta(TIQ_COD_CLIENTE) & " " & ", "
        fSql = fSql & "QNCLI, " : vSql = vSql & "'" & aEtiqueta(TIQ_CLIENTE) & "'" & ", "
        fSql = fSql & "QNOMPE, " : vSql = vSql & "'" & aEtiqueta(TIQ_PUNTO_ENTREGA) & "'" & ", "
        fSql = fSql & "QDIRPE, " : vSql = vSql & "'" & aEtiqueta(TIQ_DIR_PUNTOE) & "'" & ", "
        fSql = fSql & "QTITAGR, " : vSql = vSql & "'" & aEtiqueta(TIQ_TIT_AGRUPACION) & "'" & ", "
        fSql = fSql & "QAGRUP, " : vSql = vSql & "'" & aEtiqueta(TIQ_NOM_AGRUPACION) & "'" & ", "
        fSql = fSql & "QEANAGR, " : vSql = vSql & "'" & aEtiqueta(TIQ_EAN_AGRUPACION) & "'" & ", "
        fSql = fSql & "QNUMINT, " : vSql = vSql & "'" & aEtiqueta(TIQ_NUM_INTERNO_PUNTO) & "'" & ", "
        fSql = fSql & "QHPO, " : vSql = vSql & "'" & aEtiqueta(TIQ_OC) & "'" & ", "
        fSql = fSql & "QFACTU, " : vSql = vSql & "'" & aEtiqueta(TIQ_FACTURA) & "'" & ", "
        fSql = fSql & "QFECHA, " : vSql = vSql & "'" & aEtiqueta(TIQ_FECHA_ENVIO) & "'" & ", "
        fSql = fSql & "QESTIB, " : vSql = vSql & "'" & aEtiqueta(TIQ_UNIDADES) & "'" & ", "
        fSql = fSql & "QCAJAS )" : vSql = vSql & " " & aEtiqueta(TIQ_CAJAS) & " " & ")"

        db.ExecuteSQL(fSql & vSql)

    End Sub

    Public Function Digito_de_Control_SSCC(ByVal ean As String) As Integer
        Dim i As Integer
        Dim Suma As Integer

        Suma = 0
        For i = 17 To 1 Step -1
            Suma = Suma + (val(Mid(ean, i, 1)) * IIf(i Mod 2 = 0, 1, 3))
        Next
        Digito_de_Control_SSCC = IIf(Suma Mod 10 = 0, 0, 10 - (Suma Mod 10))

    End Function

End Module
