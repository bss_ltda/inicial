﻿Module modPruebaMHT
    Private DB As clsConexion

    Sub revisaTareasMHT()
        Dim fSql As String
        Dim rs As New ADODB.Recordset
        Dim Correo As New clsMHT
        Dim serverMHT As String
        Dim local As String = ""

        If Not AbreConexion("") Then
            Exit Sub
        End If

        With Correo
            .Inicializa()
            .DB = DB
            .avisoDesborde("ARCHIVO")
        End With


        serverMHT = DB.getLXParam("SMTPMHTSERVER", local)

        fSql = " SELECT * FROM  RMAILX WHERE LENVIADO = 0"
        DB.OpenRS(rs, fSql)
        Do While Not rs.EOF
            With Correo
                .Inicializa()
                .DB = DB
                .Asunto = rs("LASUNTO").Value
                .De = IIf(rs("LENVIA").Value = "", rs("LREF").Value, rs("LENVIA").Value)
                .Para = rs("LPARA").Value
                .CC = rs("LCOPIA").Value
                .url = rs("LCUERPO").Value
                If InStr(.url.ToUpper, "HTTP") = 0 Then
                    If InStr(.url.ToUpper, ".ASP") = 0 Then
                        .url = serverMHT & DB.getLXParam("SMTPAVISOGRAL", local) & "?LID=" & rs("LID").Value
                    Else
                        .url = serverMHT & .url
                    End If
                End If
                .Adjuntos = rs("LADJUNTOS").Value
                .local = My.Settings.LOCAL.ToUpper
                .enviaMail()
            End With
            fSql = " UPDATE RMAILX SET  "
            fSql = fSql & " LUPDDAT = NOW() , "
            fSql = fSql & " LENVIADO = 1  "
            fSql = fSql & " WHERE LID = " & rs("LID").Value
            DB.ExecuteSQL(fSql)
            rs.MoveNext()
        Loop
        rs.Close()
        DB.Close()

    End Sub

    Private Function AbreConexion(ModuloActual As String) As Boolean

        DB = New clsConexion
        DB.gsDatasource = My.Settings.AS400
        DB.gsLib = My.Settings.AMBIENTE
        DB.ModuloActual = ModuloActual
        Return DB.Conexion()

    End Function







End Module
