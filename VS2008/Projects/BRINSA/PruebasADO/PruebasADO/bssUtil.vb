﻿Public Class bssUtil

    Function cAlfa(Dato As String, Optional ByVal qLargo As Integer = 0) As String
        Dim i, ascii, Largo As Integer
        Dim result As String = ""

        If qLargo = 0 Then
            Largo = Len(Dato)
        Else
            Largo = qLargo
        End If

        For i = 1 To Len(Dato)
            ascii = Asc(Mid(Dato, i, 1))
            If ascii < 32 Then
                result &= "?"
            ElseIf ascii > 128 Then
                result &= "?"
            Else
                Select Case Chr(ascii)
                    Case "'"
                        result &= "''"
                    Case "á", "Á"
                        result &= "á"
                    Case "é", "É"
                        result &= "é"
                    Case "í", "Í"
                        result &= "í"
                    Case "ó", "Ó"
                        result &= "ó"
                    Case "ú", "Ú"
                        result &= "ú"
                    Case "ñ"
                        result &= "ñ"
                    Case "Ñ", "#"
                        result &= "Ñ"
                    Case Else
                        result &= Chr(ascii)
                End Select
            End If
        Next

        Return Left(result, Largo)

    End Function

    Function campoAlfa(Dato As String, Optional ByVal qLargo As Integer = 0) As String
        Dim i, ascii, Largo As Integer
        Dim result As String = ""

        If qLargo = 0 Then
            Largo = Len(Dato)
        Else
            Largo = qLargo
        End If

        For i = 1 To Len(Dato)
            ascii = Asc(Mid(Dato, i, 1))
            If ascii < 32 Then
                result &= "?"
            ElseIf ascii > 128 Then
                result &= "?"
            Else
                Select Case Chr(ascii)
                    Case "á", "Á"
                        result &= "á"
                    Case "é", "É"
                        result &= "é"
                    Case "í", "Í"
                        result &= "í"
                    Case "ó", "Ó"
                        result &= "ó"
                    Case "ú", "Ú"
                        result &= "ú"
                    Case "ñ"
                        result &= "ñ"
                    Case "Ñ", "#"
                        result &= "Ñ"
                    Case Else
                        result &= Chr(ascii)
                End Select
            End If
        Next

        Return Left(result, Largo)

    End Function


    Function ceros(val As Object, C As Integer) As String
        Dim sCeros As String = New String("0", C)

        If CInt(C) > Len(Trim(val)) Then
            ceros = Right(sCeros & Trim(val), C)
        Else
            ceros = val
        End If
        Return ceros

    End Function


    Public Function tsAS400(Fecha As Date) As String
        Return Fecha.ToString("yyyy-mm-dd-hh.mm.ss.000000")
    End Function

    Public Function dtAS400(Fecha As String) As String
        Return Left(Fecha, 10) & " " & Replace(Mid(Fecha, 12, 5), ".", ":")
    End Function

    Public Function dateDecToDate(qFecha As String) As Date
        Dim Fecha As Date
        Dim iFecha As Integer = CInt(qFecha)
        DateTime.TryParseExact(iFecha.ToString("####-##-##"), "yyyy-MM-dd", Nothing, Globalization.DateTimeStyles.None, Fecha)
        Return Fecha
    End Function
    Public Function dateDecToDate(qFecha As Integer) As Date
        Dim Fecha As Date
        DateTime.TryParseExact(qFecha.ToString("####-##-##"), "yyyy-MM-dd", Nothing, Globalization.DateTimeStyles.None, Fecha)
        Return Fecha
    End Function

    Function DateDec(Fecha)
        Return Fecha.ToString("yyyymmdd")
    End Function

    Function TimeDec(Fecha)
        Return Fecha.ToString("hhnnss")
    End Function

    Function HoraFromDEC(Fecha As String) As Date
        Return DateSerial(Left(Fecha, 4), Mid(Fecha, 5, 2), Mid(Fecha, 7, 2)) & " " & TimeSerial(Mid(Fecha, 9, 2), Mid(Fecha, 11, 2), Mid(Fecha, 13, 2))
    End Function

    Function LastDayMonth(Fecha) As Date

        LastDayMonth = DateSerial(Year(Fecha), Month(Fecha), 1)
        LastDayMonth = DateAdd("m", 1, LastDayMonth)
        LastDayMonth = DateAdd("d", -1, LastDayMonth)
        Return LastDayMonth

    End Function

    Function paramWebConfig(webConfig, param)
        Dim oXML, oNode, oChild, oAttr

        oXML = CreateObject("Microsoft.XMLDOM")
        oXML.Async = "false"
        oXML.Load(webConfig)
        oNode = oXML.GetElementsByTagName("appSettings").Item(0)
        oChild = oNode.GetElementsByTagName("add")
        For Each oAttr In oChild
            If UCase(oAttr.getAttribute("key")) = UCase(param) Then
                Return oAttr.getAttribute("value")
            End If
        Next
        Return ""

    End Function

    'mkSqlG()

    Sub mkSqlG(ByRef fSql As String, ByRef vSql As String, tipoF As String, Campo As String, Valor As Double, Optional fin As Single = 0)
        Dim sep As String

        If Left(Trim(UCase(fSql)), 6) = "INSERT" Then
            If tipoF = "'" Then
                sep = "'"
            Else
                sep = ""
            End If
            fSql = fSql & Campo
            Select Case fin
                Case 0
                    fSql = fSql & " , "
                Case 10, 11, -1, 1
                    fSql = fSql & " ) "
            End Select
            If IsDBNull(Valor) Then
                Valor = IIf(tipoF = "'", "", "0")
            End If
            vSql = vSql & sep & CStr(Valor) & sep
            Select Case fin
                Case 10
                    vSql = vSql & "  "
                Case 11, -1, 1
                    vSql = vSql & " )"
                Case 0
                    vSql = vSql & " , "
            End Select
        Else
            sep = tipoF
            If IsDBNull(Valor) Then
                Valor = IIf(tipoF = "'", "", "0")
            End If
            fSql = fSql & Campo & " = " & sep & CStr(Valor) & sep & IIf(fin = 10 Or fin = 11, " ", " , ")
        End If
    End Sub

    Sub mkSqlG(ByRef fSql As String, ByRef vSql As String, tipoF As String, Campo As String, Valor As String, Optional fin As Single = 0)
        Dim sep As String

        If Left(Trim(UCase(fSql)), 6) = "INSERT" Then
            If tipoF = "'" Then
                sep = "'"
            Else
                sep = ""
            End If
            fSql = fSql & Campo
            Select Case fin
                Case 0
                    fSql = fSql & " , "
                Case 10, 11, -1, 1
                    fSql = fSql & " ) "
            End Select
            If IsDBNull(Valor) Then
                Valor = IIf(tipoF = "'", "", "0")
            End If
            vSql = vSql & sep & CStr(Valor) & sep
            Select Case fin
                Case 10
                    vSql = vSql & "  "
                Case 11, -1, 1
                    vSql = vSql & " )"
                Case 0
                    vSql = vSql & " , "
            End Select
        Else
            sep = tipoF
            If IsDBNull(Valor) Then
                Valor = IIf(tipoF = "'", "", "0")
            End If
            fSql = fSql & Campo & " = " & sep & CStr(Valor) & sep & IIf(fin = 10 Or fin = 11, " ", " , ")
        End If
    End Sub

    Function getPrmTask(txtPrm As String, prm As String)
        Dim aPrms(), aPrm() As String
        Dim i As Integer

        aPrms = Split(txtPrm, "|")
        For i = 0 To UBound(aPrms)
            aPrm = Split(aPrms(i), "=")
            If prm = aPrm(0) Then
                Return aPrm(1)
            End If
        Next

        Return ""

    End Function


    'timeStampTxt()
    Function timeStampTxt(txt) As String
        Dim result

        If Len(txt) = 16 Then
            result = Replace(txt, " ", "-")
            result = Replace(result, ":", ".")
            result = result & ".00.000000"
            timeStampTxt = result
        Else
            timeStampTxt = txt
        End If

    End Function

    'timeStampCalc()
    Function timeStampCalc(horas) As String
        Dim fec As Date, h As Integer, m As Integer
        DatePart(DateInterval.Day, Now)
        h = Fix(CDbl(horas))
        m = CDbl(horas) - Fix(CDbl(horas))
        fec = DateAdd("h", h, Now())
        If m <> 0 Then
            m = CInt(60 * m)
            fec = DateAdd("n", m, fec)
        End If
        timeStampCalc = "" & DatePart(DateInterval.Year, fec)
        timeStampCalc &= "-" & DatePart(DateInterval.Month, fec)
        timeStampCalc &= "-" & DatePart(DateInterval.Day, fec)
        timeStampCalc &= "-" & DatePart(DateInterval.Hour, fec)
        timeStampCalc &= "." & DatePart(DateInterval.Minute, fec)
        timeStampCalc &= "." & DatePart(DateInterval.Second, fec) & ".000000"

    End Function

    'Digito de Verificacion
    Public Function dv(numero As String) As String
        Dim i As Long
        Dim Cod As String
        Dim Impares As Long
        Dim Pares As Long
        Dim Total As Long

        If Not IsNumeric(numero) Then Return ""
        Cod = numero
        Pares = 0 : Impares = 0

        For i = 1 To Len(CStr(numero))
            If i Mod 2 = 0 Then
                Pares = Pares + CLng(Mid(Cod, i, 1))
            Else
                Impares = Impares + CLng(Mid(Cod, i, 1))
            End If
        Next i

        Total = Pares + (3 * Impares)

        dv = 10 - (Total Mod 10)
        If dv = 10 Then dv = 0
    End Function
End Class
