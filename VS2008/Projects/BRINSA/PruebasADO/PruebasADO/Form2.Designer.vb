﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form2
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.InfoNaturaDataSet = New WindowsApplication1.InfoNaturaDataSet()
        Me.RCAMBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.RCAMTableAdapter = New WindowsApplication1.InfoNaturaDataSetTableAdapters.RCAMTableAdapter()
        Me.AIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ALVLDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AORDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AAPPDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AMODDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AGRPDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.APARENTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ATITDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ALINKDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACLASSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AISETDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.APARM1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.APARM2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.APARM3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.APARM4DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.APARM5DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ADESCDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InfoNaturaDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RCAMBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.AIDDataGridViewTextBoxColumn, Me.ALVLDataGridViewTextBoxColumn, Me.AORDDataGridViewTextBoxColumn, Me.AAPPDataGridViewTextBoxColumn, Me.AMODDataGridViewTextBoxColumn, Me.AGRPDataGridViewTextBoxColumn, Me.APARENTDataGridViewTextBoxColumn, Me.ATITDataGridViewTextBoxColumn, Me.ALINKDataGridViewTextBoxColumn, Me.ACLASSDataGridViewTextBoxColumn, Me.AISETDataGridViewTextBoxColumn, Me.APARM1DataGridViewTextBoxColumn, Me.APARM2DataGridViewTextBoxColumn, Me.APARM3DataGridViewTextBoxColumn, Me.APARM4DataGridViewTextBoxColumn, Me.APARM5DataGridViewTextBoxColumn, Me.ADESCDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.RCAMBindingSource
        Me.DataGridView1.Location = New System.Drawing.Point(26, 27)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(989, 523)
        Me.DataGridView1.TabIndex = 0
        '
        'InfoNaturaDataSet
        '
        Me.InfoNaturaDataSet.DataSetName = "InfoNaturaDataSet"
        Me.InfoNaturaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'RCAMBindingSource
        '
        Me.RCAMBindingSource.DataMember = "RCAM"
        Me.RCAMBindingSource.DataSource = Me.InfoNaturaDataSet
        '
        'RCAMTableAdapter
        '
        Me.RCAMTableAdapter.ClearBeforeFill = True
        '
        'AIDDataGridViewTextBoxColumn
        '
        Me.AIDDataGridViewTextBoxColumn.DataPropertyName = "AID"
        Me.AIDDataGridViewTextBoxColumn.HeaderText = "AID"
        Me.AIDDataGridViewTextBoxColumn.Name = "AIDDataGridViewTextBoxColumn"
        '
        'ALVLDataGridViewTextBoxColumn
        '
        Me.ALVLDataGridViewTextBoxColumn.DataPropertyName = "ALVL"
        Me.ALVLDataGridViewTextBoxColumn.HeaderText = "ALVL"
        Me.ALVLDataGridViewTextBoxColumn.Name = "ALVLDataGridViewTextBoxColumn"
        '
        'AORDDataGridViewTextBoxColumn
        '
        Me.AORDDataGridViewTextBoxColumn.DataPropertyName = "AORD"
        Me.AORDDataGridViewTextBoxColumn.HeaderText = "AORD"
        Me.AORDDataGridViewTextBoxColumn.Name = "AORDDataGridViewTextBoxColumn"
        '
        'AAPPDataGridViewTextBoxColumn
        '
        Me.AAPPDataGridViewTextBoxColumn.DataPropertyName = "AAPP"
        Me.AAPPDataGridViewTextBoxColumn.HeaderText = "AAPP"
        Me.AAPPDataGridViewTextBoxColumn.Name = "AAPPDataGridViewTextBoxColumn"
        '
        'AMODDataGridViewTextBoxColumn
        '
        Me.AMODDataGridViewTextBoxColumn.DataPropertyName = "AMOD"
        Me.AMODDataGridViewTextBoxColumn.HeaderText = "AMOD"
        Me.AMODDataGridViewTextBoxColumn.Name = "AMODDataGridViewTextBoxColumn"
        '
        'AGRPDataGridViewTextBoxColumn
        '
        Me.AGRPDataGridViewTextBoxColumn.DataPropertyName = "AGRP"
        Me.AGRPDataGridViewTextBoxColumn.HeaderText = "AGRP"
        Me.AGRPDataGridViewTextBoxColumn.Name = "AGRPDataGridViewTextBoxColumn"
        '
        'APARENTDataGridViewTextBoxColumn
        '
        Me.APARENTDataGridViewTextBoxColumn.DataPropertyName = "APARENT"
        Me.APARENTDataGridViewTextBoxColumn.HeaderText = "APARENT"
        Me.APARENTDataGridViewTextBoxColumn.Name = "APARENTDataGridViewTextBoxColumn"
        '
        'ATITDataGridViewTextBoxColumn
        '
        Me.ATITDataGridViewTextBoxColumn.DataPropertyName = "ATIT"
        Me.ATITDataGridViewTextBoxColumn.HeaderText = "ATIT"
        Me.ATITDataGridViewTextBoxColumn.Name = "ATITDataGridViewTextBoxColumn"
        '
        'ALINKDataGridViewTextBoxColumn
        '
        Me.ALINKDataGridViewTextBoxColumn.DataPropertyName = "ALINK"
        Me.ALINKDataGridViewTextBoxColumn.HeaderText = "ALINK"
        Me.ALINKDataGridViewTextBoxColumn.Name = "ALINKDataGridViewTextBoxColumn"
        '
        'ACLASSDataGridViewTextBoxColumn
        '
        Me.ACLASSDataGridViewTextBoxColumn.DataPropertyName = "ACLASS"
        Me.ACLASSDataGridViewTextBoxColumn.HeaderText = "ACLASS"
        Me.ACLASSDataGridViewTextBoxColumn.Name = "ACLASSDataGridViewTextBoxColumn"
        '
        'AISETDataGridViewTextBoxColumn
        '
        Me.AISETDataGridViewTextBoxColumn.DataPropertyName = "AISET"
        Me.AISETDataGridViewTextBoxColumn.HeaderText = "AISET"
        Me.AISETDataGridViewTextBoxColumn.Name = "AISETDataGridViewTextBoxColumn"
        '
        'APARM1DataGridViewTextBoxColumn
        '
        Me.APARM1DataGridViewTextBoxColumn.DataPropertyName = "APARM1"
        Me.APARM1DataGridViewTextBoxColumn.HeaderText = "APARM1"
        Me.APARM1DataGridViewTextBoxColumn.Name = "APARM1DataGridViewTextBoxColumn"
        '
        'APARM2DataGridViewTextBoxColumn
        '
        Me.APARM2DataGridViewTextBoxColumn.DataPropertyName = "APARM2"
        Me.APARM2DataGridViewTextBoxColumn.HeaderText = "APARM2"
        Me.APARM2DataGridViewTextBoxColumn.Name = "APARM2DataGridViewTextBoxColumn"
        '
        'APARM3DataGridViewTextBoxColumn
        '
        Me.APARM3DataGridViewTextBoxColumn.DataPropertyName = "APARM3"
        Me.APARM3DataGridViewTextBoxColumn.HeaderText = "APARM3"
        Me.APARM3DataGridViewTextBoxColumn.Name = "APARM3DataGridViewTextBoxColumn"
        '
        'APARM4DataGridViewTextBoxColumn
        '
        Me.APARM4DataGridViewTextBoxColumn.DataPropertyName = "APARM4"
        Me.APARM4DataGridViewTextBoxColumn.HeaderText = "APARM4"
        Me.APARM4DataGridViewTextBoxColumn.Name = "APARM4DataGridViewTextBoxColumn"
        '
        'APARM5DataGridViewTextBoxColumn
        '
        Me.APARM5DataGridViewTextBoxColumn.DataPropertyName = "APARM5"
        Me.APARM5DataGridViewTextBoxColumn.HeaderText = "APARM5"
        Me.APARM5DataGridViewTextBoxColumn.Name = "APARM5DataGridViewTextBoxColumn"
        '
        'ADESCDataGridViewTextBoxColumn
        '
        Me.ADESCDataGridViewTextBoxColumn.DataPropertyName = "ADESC"
        Me.ADESCDataGridViewTextBoxColumn.HeaderText = "ADESC"
        Me.ADESCDataGridViewTextBoxColumn.Name = "ADESCDataGridViewTextBoxColumn"
        '
        'Form2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1055, 614)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "Form2"
        Me.Text = "Form2"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InfoNaturaDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RCAMBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents InfoNaturaDataSet As WindowsApplication1.InfoNaturaDataSet
    Friend WithEvents RCAMBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents RCAMTableAdapter As WindowsApplication1.InfoNaturaDataSetTableAdapters.RCAMTableAdapter
    Friend WithEvents AIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ALVLDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AORDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AAPPDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AMODDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AGRPDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents APARENTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ATITDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ALINKDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACLASSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AISETDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents APARM1DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents APARM2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents APARM3DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents APARM4DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents APARM5DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ADESCDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
