﻿Imports BSSClassLibrary

Public Class Consolidado
    Dim Conso As String
    Dim Pedidos As String = ""
    Dim Bodega As String
    Dim TipoVeh As String
    Dim Observaciones As String
    Dim msg As String
    Dim Proveedor As String
    Dim fSql As String
    Dim vSql As String
    Dim db As bssConexion
    Dim sql As New bssSQL

    Public Sub setBodega(bod As String)
        Bodega = bod
    End Sub

    Public Sub setProv(codProv As String)
        Proveedor = codProv
    End Sub

    Public Sub setDB(DBConn As bssConexion)
        db = DBConn
    End Sub

    Public Sub setTipoVeh(tipo As String)
        TipoVeh = tipo
    End Sub

    Public Sub setObserv(txt As String)
        Observaciones = txt
    End Sub

    Public Sub setPedido(numPed As String)
        If Pedidos = "" Then
            Pedidos &= numPed
        Else
            Pedidos &= ", " & numPed
        End If
    End Sub

    Public Function generaConso() As String

        Dim tSql As String
        Dim rs As New ADODB.Recordset
        db.CalcConsec("SPCONSO", "999999")

        fSql = "UPDATE RDCC SET "
        fSql = fSql & " DAPROB = 'P', DAPROC='*', DFLAG='D', DESTPE='90', DFPROG=NOW(), "
        fSql = fSql & " DCONSO='" & Conso & "', "
        fSql = fSql & " DFEC01 = DFEC04, "
        fSql = fSql & " DSTSPED='1070' "
        fSql = fSql & " WHERE " 'DPEDID IN( " & Pedidos & " )"
        fSql = fSql & " DCONSO='" & Conso & "' "
        db.ExecuteSQL(fSql)

        fSql = "UPDATE RDCC SET DCABCON = 1 "
        fSql = fSql & " WHERE DCONSO='" & Conso & "'"
        fSql = fSql & " AND DPEDID * 10000 + DLINEA = ( "
        fSql = fSql & "       SELECT MIN(DPEDID * 10000 + DLINEA ) FROM RDCC "
        fSql = fSql & "       WHERE DCONSO='" & Conso & "' ) "
        db.ExecuteSQL(fSql)

        sql.mkSQL(" INSERT INTO RHCC (")
        sql.mkSQL("'", "HSFCNS", "SAT")                                       'Tipo Consolidado
        sql.mkSQL("'", "HCONSO", Conso)                                       'Consolidado
        sql.mkSQL(" ", "HESTAD", 1)                                           'Estado
        sql.mkSQL(" ", "HPER  ", " YEAR( NOW() ) * 100 + MONTH(NOW()) ")      'Periodo
        sql.mkSQL(" ", "HFESTAD", "NOW()")                                    'F.Estado
        sql.mkSQL(" ", "HPROVE", Proveedor)                                   'Transp Programado
        tSql = "( SELECT TNOMBRE FROM RTTRA WHERE TPROVE = " & Proveedor & ") "
        sql.mkSQL(" ", "HNPROVE", tSql)                                        'Transp Programado
        sql.mkSQL(" ", "HTRAPR", Proveedor)                                   'Transp Programado
        sql.mkSQL(" ", "HTIPO", TipoVeh)                                      'Tipo Vehículo
        sql.mkSQL("'", "HOBSER", Observaciones)                                'Observaciones

        tSql = "SELECT WMFAC FROM IWM WHERE LWHS='" & Bodega & "'"
        rs = db.ExecuteSQL(tSql)
        If Not rs.EOF Then
            sql.mkSQL("'", "HPLTA", rs("WMFAC").Value)                         'Planta
        End If
        rs.Close()

        sql.mkSQL("'", "HREF05", Bodega)    'Bodega
        sql.mkSQL("'", "HBOD", Bodega)      'Bodega

        'Mayor Linea de producto
        tSql = "SELECT DLINEAP, SUM(DPESO) AS PESOP FROM RDCC "
        tSql = tSql & " WHERE"
        tSql = tSql & " DCONSO='" & Conso & "'"
        tSql = tSql & " GROUP BY DLINEAP"
        tSql = tSql & " ORDER BY 2 DESC"
        rs = db.ExecuteSQL(tSql)
        sql.mkSQL("'", "HREF06", Left(rs("DLINEAP").Value, 15))
        rs.Close()

        sql.mkSQL("'", "HPRCDVA", "N/A")           'Normal/Predist/Crossdocking

        'Destino
        tSql = " SELECT DSPOST, DSHDEP, DIDIVI, SUM(DPESO) "
        tSql = tSql & " WHERE FROM RDCC WHERE DCONSO = '" & Conso & "' "
        tSql = tSql & " GROUP BY DSPOST, DSHDEP, DIDIVI"
        tSql = tSql & " ORDER BY 3 DESC"
        rs = db.ExecuteSQL(tSql)
        sql.mkSQL("'", "HDESTI", rs("DSHDEP").Value & rs("DSPOST").Value)      'Destino
        sql.mkSQL("'", "HCIUDAD", rs("DSPOST").Value)                          'Ciudad
        sql.mkSQL("'", "HDEPTO ", rs("DSHDEP").Value)                          'Depto
        sql.mkSQL("'", "HSEGME", rs("DIDIVI").Value)                           'Segmento
        rs.Close()

        sql.mkSQL("'", "HMERCA", "NACIONAL")                                   'Mercado

        tSql = "  SELECT ROUND( SUM(LLSQTY * IWGHT)/1000, 3 ) "
        tSql &= " FROM RDCC  INNER JOIN LLL ON DPEDID=LLORDN AND DLINEA=LLOLIN  "
        tSql &= " INNER JOIN LLH ON LLLOAD = LHLOAD   "
        tSql &= " INNER JOIN IIM ON LLPROD=IPROD  "
        tSql &= " WHERE DCONSO='" & Conso & "' "

        sql.mkSQL(" ", "HPETOT ", "( " & tSql & " )")                          'Peso Consolidado
        sql.mkSQL(" ", "HTONPAG", "( " & tSql & " )")
        sql.mkSQL("'", "HGENERA", "1")
        sql.mkSQL(" ", "HACTDAT", "NOW()")
        sql.mkSQL("'", "HACTUSR", "BODSAT")
        sql.mkSQL("'", "HDIVIS ", "CSECA", 11)                                 'Tipo de Carga
        sql.mkSQL("WHERE HCONSO = '" & Conso & "'")                                 'Tipo de Carga
        db.ExecuteSQL(sql.Sql)

        ActualizaRECC(Conso)
        Return Conso

    End Function


    Public Sub ActualizaRECC(Conso As String)
        Dim rs, rs1 As New ADODB.Recordset
        Dim bCompleto As Boolean

        'Actualiza número de Factura y de Planilla
        fSql = " SELECT DPEDID, DLINEA, LLLOAD, DPLANI  "
        fSql = fSql & " FROM RDCC INNER JOIN LLL ON DPEDID = LLORDN AND DLINEA=LLOLIN "
        fSql = fSql & " WHERE DCONSO = '" & Conso & "'"
        fSql = fSql & " AND DPLANI <> LLLOAD "
        rs = db.ExecuteSQL(fSql)
        Do While Not rs.EOF
            fSql = "UPDATE RDCC SET DPLANI = " & rs("LLLOAD").Value
            fSql = fSql & " WHERE "
            fSql = fSql & " DPEDID = " & rs("DPEDID").Value
            fSql = fSql & " AND DLINEA = " & rs("DLINEA").Value
            db.ExecuteSQL(fSql)
            rs.MoveNext()
        Loop
        rs.Close()
        fSql = " SELECT DPEDID, DLINEA, ILDPFX, ILDOCN, DLDPFX, DLDOCN  "
        fSql = fSql & " FROM RDCC INNER JOIN SIL ON DPEDID = ILORD AND DLINEA=ILSEQ "
        fSql = fSql & " WHERE DCONSO = '" & Conso & "'"
        fSql = fSql & " AND  DLDOCN <> ILDOCN  "
        rs = db.ExecuteSQL(fSql)
        Do While Not rs.EOF
            fSql = "UPDATE RDCC SET DLDPFX = '" & rs("ILDPFX").Value & "', DLDOCN = " & rs("ILDOCN").Value
            fSql = fSql & " WHERE "
            fSql = fSql & " DPEDID = " & rs("DPEDID").Value
            fSql = fSql & " AND DLINEA = " & rs("DLINEA").Value
            db.ExecuteSQL(fSql)
            rs.MoveNext()
        Loop
        rs.Close()

        bCompleto = True
        fSql = "SELECT DPLANI FROM RDCC LEFT OUTER JOIN RECC ON DNUMENT = EID AND DPLANI = EPLANI "
        fSql = fSql & " WHERE DCONSO = '" & Conso & "' AND EID IS NULL "
        rs = db.ExecuteSQL(fSql)
        If Not rs.EOF Then
            bCompleto = False
        End If
        rs.Close()
        fSql = "SELECT EPLANI FROM RECC LEFT OUTER JOIN RDCC ON EID = DNUMENT AND EPLANI = DPLANI"
        fSql = fSql & " WHERE ECONSO = '" & Conso & "' AND DPLANI IS NULL "
        rs = db.ExecuteSQL(fSql)
        If Not rs.EOF Then
            bCompleto = False
        End If
        rs.Close()
        If bCompleto Then
            fSql = "DELETE FROM RECC "
            fSql = fSql & " WHERE ECONSO='" & Conso & "' AND EPLANI = 0"
            db.ExecuteSQL(fSql)
            Exit Sub
        End If

        fSql = "UPDATE RDCC SET DNUMENT=0 "
        fSql = fSql & " WHERE "
        fSql = fSql & " DCONSO='" & Conso & "' "
        db.ExecuteSQL(fSql)

        fSql = "UPDATE RECC SET EPLANI = 0 WHERE ECONSO = '" & Conso & "'"
        db.ExecuteSQL(fSql)

        fSql = "SELECT DPEDID, DPLANI, MAX(DORDENT) AS DORDENT, DIDIVI, DCUST, DSHIP  "
        fSql = fSql & " FROM RDCC "
        fSql = fSql & " WHERE DCONSO='" & Conso & "' AND DPLANI > 0 "
        fSql = fSql & " GROUP BY DPEDID, DPLANI, DIDIVI, DCUST, DSHIP "
        fSql = fSql & " ORDER BY DPEDID, DPLANI "
        rs = db.ExecuteSQL(fSql)
        Do While Not rs.EOF
            fSql = "SELECT EID FROM RECC WHERE "
            fSql = fSql & "     ECONSO = '" & Conso & "'"
            fSql = fSql & " AND EPEDID = " & rs("DPEDID").Value
            fSql = fSql & " AND EIDIVI = '" & rs("DIDIVI").Value & "'"
            fSql = fSql & " AND ECUST  = " & rs("DCUST").Value
            fSql = fSql & " AND ESHIP  = " & rs("DSHIP").Value
            fSql = fSql & " AND EPLANI = 0"
            rs1 = db.ExecuteSQL(fSql)
            If Not rs1.EOF Then
                fSql = "UPDATE RECC SET EPLANI = " & rs("DPLANI").Value
                fSql = fSql & " WHERE EID = " & rs1("EID").Value
                db.ExecuteSQL(fSql)
            Else
                fSql = " INSERT INTO RECC("
                vSql = " SELECT"
                fSql = fSql & " ECONSO," : vSql = vSql & " DCONSO,"
                fSql = fSql & " EPEDID," : vSql = vSql & " DPEDID,"
                fSql = fSql & " EPLANI," : vSql = vSql & " DPLANI,"
                fSql = fSql & " EORDENT," : vSql = vSql & " MAX(DORDENT),"
                fSql = fSql & " EIDIVI," : vSql = vSql & " DIDIVI,"
                fSql = fSql & " ECUST," : vSql = vSql & " DCUST,"
                fSql = fSql & " ESHIP," : vSql = vSql & " DSHIP,"
                fSql = fSql & " EPESOD," : vSql = vSql & " SUM( DPESO ), "
                fSql = fSql & " ECANTD )" : vSql = vSql & " SUM( DCANT )"
                vSql = vSql & " FROM RDCC "
                vSql = vSql & " WHERE DPLANI = " & rs("DPLANI").Value
                vSql = vSql & " GROUP BY DCONSO, DPEDID, DPLANI, DIDIVI, "
                vSql = vSql & "          DCUST, DSHIP "
                db.ExecuteSQL(fSql & vSql)
            End If
            rs.MoveNext()
        Loop
        rs.Close()

        fSql = "        UPDATE RECC E SET ( EHNAME, ENSHIP, ESPOST, ESHDEP  ) = ("
        fSql = fSql & "    SELECT TCUSTNAME, TNAME, TPOST, TSTE "
        fSql = fSql & "    FROM RVESTTOT WHERE E.ECUST=TCUST AND E.ESHIP=TSHIP )"
        fSql = fSql & " WHERE "
        fSql = fSql & " ECONSO='" & Conso & "'"
        db.ExecuteSQL(fSql)

        fSql = "DELETE FROM RECC "
        fSql = fSql & " WHERE ECONSO='" & Conso & "' AND EPLANI = 0"
        db.ExecuteSQL(fSql)

        fSql = "UPDATE RDCC D SET ( DNUMENT ) = ("
        fSql = fSql & " SELECT EID FROM RECC WHERE D.DCONSO = ECONSO AND D.DPEDID = EPEDID "
        fSql = fSql & " AND D.DIDIVI=EIDIVI AND D.DCUST=ECUST AND D.DSHIP=ESHIP AND D.DPLANI = EPLANI)"
        fSql = fSql & " WHERE "
        fSql = fSql & " DCONSO='" & Conso & "' AND DPLANI > 0"
        db.ExecuteSQL(fSql)


    End Sub

End Class

