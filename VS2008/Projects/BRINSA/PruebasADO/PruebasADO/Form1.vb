﻿Imports System.Data.SqlClient
Imports IBM.Data.DB2.iSeries
Imports System.IO
Imports BSSClassLibrary

Public Class Form1

    Dim db As New bssConexion
    Dim sql As New bssSQL
    Dim u As New bssUtil

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Dim DB As iDB2Connection = New IBM.Data.DB2.iSeries.iDB2Connection
        Dim fSql As String

        Try
            DB.ConnectionString = Me.txtConnString.Text
        Catch ex As Exception
            MsgBox("1." & Err.Description)
            Exit Sub
        End Try


        'DB.ConnectionString = "Data Source=173.10.254.150;Default Collection=FERRERO;Persist Security Info=True;User ID=BSSLTDA;Password=PROGRAM"
        'DB.ConnectionString = "Data Source=SECU110106;Default Collection=BSS;Persist Security Info=True;User ID=USREXTDEV;Password=BT201308"
        'DB.ConnectionString = "Data Source=192.168.10.1;Default Collection=DESLX834F;Persist Security Info=True;User ID=DESLXSSA;Password=ABR2013"
        '"Provider=IBMDA400.DataSource;Data Source=173.10.254.150;User ID=BSSLTDA;Password=PROGRAM;Persist Security Info=True;Default Collection=BSSLX;Force Translate=0;"
        Try
            DB.Open()
        Catch ex As Exception
            MsgBox("2." & Err.Description)
            Exit Sub
        End Try
        Dim adaptador As iDB2DataAdapter = New IBM.Data.DB2.iSeries.iDB2DataAdapter

        'fSql = " SELECT DWRKID, DAPROC, DAPROB, DREF03, DOCCLI, DESTPE, "
        'fSql += "DRETEN, DPEDID, DLINEA, DCONSO, DFPROG, DCUST, DHNAME, "
        'fSql += "DSHIP, DNSHIP, DHAD1, DHAD2, DSHDEP, DSPOST, DIDIVI, "
        'fSql += "DPROD, DINAME, DCANTP, DPESOP, DIPESN, DIVAL, DODTE, "
        'fSql += "DFELIB, DFLDTE, DRDTE, DFECR, DDESCF, DFLAG, DCARG, "
        'fSql += "DDIAM, DFECEE, DLEADT, DIWHR, DSLMER, DSHZON, DSLGER, "
        'fSql += "DSAL, DSLNAM, DTRAN, DOBSS, DOBEN, DOBSA, DSALDO,  DADTE, "
        'fSql += "DVOBO, UNUMTR  "
        'fSql += "FROM  RDCCL01 R  "
        'fSql += "WHERE  ( USTSTR = 1 )  "
        'fSql += "AND DPLANT = '10'  AND (DPEDID = 874491)  "
        'fSql += "AND DESTPE <> '98'  "
        'fSql += " ORDER BY  DSPOST, DHNAME, DNSHIP, DPEDID, DPROD, DLINEA  "

        fSql = "SELECT * FROM FFPEDID"

        Dim sql As iDB2Command = New IBM.Data.DB2.iSeries.iDB2Command(fSql, DB)
        adaptador.SelectCommand = sql
        Dim tareas As DataSet = New DataSet
        adaptador.Fill(tareas, "ZCC")

        Dim tbl As DataTable = tareas.Tables("ZCC")

        'Me.grid01.DataSource = tbl

    End Sub


    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Dim archivos As DirectoryInfo = New DirectoryInfo("C:\Tareas")
        Dim fe As String = ""
        Dim exe As String
        Dim tarea As FileInfo
        Dim fec As Date = Now()
        fe = fec.ToString("yyyy-MM-dd-HH.mm.ss.000000")

        For Each tarea In archivos.GetFiles()
            exe = tarea.DirectoryName & "\Ejecutar\" & tarea.Name
            Debug.Print(exe)
            tarea.CopyTo(exe, True)
            tarea.Delete()
            Shell(exe, AppWinStyle.Hide, False, -1)
        Next

    End Sub

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        Dim fec As Date
        Dim fe As String = ""
        Dim db As New ADODB.Connection
        db.Open("Provider=IBMDA400.DataSource.1;Password=LXAPL;Persist Security Info=True;User ID=APLLX;Data Source=192.168.10.1;Force Translate=0;Default Collection=ERPLX834F;Convert Date Time To Char=FALSE;Application Name=""CDA""")
        Dim rs As New ADODB.Recordset
        rs.Open("SELECT PLFECR FROM RFPLCAB WHERE PLPEDID = 965584", db)
        fec = rs(0).Value
        fe = fec.ToString("yyyy-MM-dd HH:mm")
        MsgBox(fe)

    End Sub

    Private Sub Button4_Click(sender As System.Object, e As System.EventArgs) Handles Button4.Click
        Dim sql As New bssSQL
        Dim rs As New ADODB.Recordset

        db.abrirConexion("DESLX834F", "AS400")
        rs = db.ExecuteSQL("SELECT * FROM ZCC WHERE CCTABL = 'RFVBVER'")
        Do While Not rs.EOF
            Debug.Print(rs("CCCODE").Value)
            rs.MoveNext()
        Loop
        rs.Close()

        sql.salto(True)
        sql.mkSQL(" INSERT INTO ZCC( ")
        sql.mkSQL(" SELECT ")
        sql.mkSQL(" ", " CCID           ", "CCID           ", 0)   ' //Record ID (CC/CZ)
        sql.mkSQL(" ", " CCTABL         ", "CCTABL         ", 0)   ' //Table ID
        sql.mkSQL(" ", " CCCODE         ", "CCCODE         ", 0)   ' //Primary Code
        sql.mkSQL(" ", " CCLANG         ", "CCLANG         ", 0)   ' //Language Code
        sql.mkSQL(" ", " CCALTC         ", "CCALTC         ", 0)   ' //Alternate Code
        sql.mkSQL(" ", " CCDESC         ", "CCDESC         ", 0)   ' //Description
        sql.mkSQL(" ", " CCSDSC         ", "CCSDSC         ", 0)   ' //Short Description
        sql.mkSQL(" ", " CCNOT1         ", "CCNOT1         ", 0)   ' //Note 1
        sql.mkSQL(" ", " CCNOT2         ", "CCNOT2         ", 0)   ' //Note 2
        sql.mkSQL(" ", " CCUDC1         ", "CCUDC1         ", 0)   ' //User Code 1
        sql.mkSQL(" ", " CCUDC2         ", "CCUDC2         ", 0)   ' //User Code 2
        sql.mkSQL(" ", " CCUDC3         ", "CCUDC3         ", 0)   ' //User Code 3
        sql.mkSQL(" ", " CCRESV         ", "CCRESV         ", 0)   ' //Reservation
        sql.mkSQL(" ", " CCENDT         ", "CCENDT         ", 0)   ' //Entered Date
        sql.mkSQL(" ", " CCENTM         ", "CCENTM         ", 0)   ' //Entered Time
        sql.mkSQL(" ", " CCENUS         ", "CCENUS         ", 0)   ' //Entered By User
        sql.mkSQL(" ", " CCMNDT         ", "CCMNDT         ", 0)   ' //Last Maintenance Date
        sql.mkSQL(" ", " CCMNTM         ", "CCMNTM         ", 0)   ' //Last Maintenance Time
        sql.mkSQL(" ", " CCMNUS         ", "CCMNUS         ", 0)   ' //Last Maintenance User
        sql.mkSQL(" ", " CCALCR         ", "CCALCR         ", 0)   ' //Review Alternate Code
        sql.mkSQL(" ", " CCDESR         ", "CCDESR         ", 0)   ' //Review Description
        sql.mkSQL(" ", " CCSDSR         ", "CCSDSR         ", 0)   ' //Review Short Description
        sql.mkSQL(" ", " CCNOTR         ", "CCNOTR         ", 0)   ' //Review Note
        sql.mkSQL(" ", " CCREVR         ", "CCREVR         ", 10)   ' //Review Required
        sql.mkSQL("  FROM ZCC")
        sql.mkSQL("  WHERE ")
        sql.mkSQL(" CCTABL = 'FLCARGUE' AND CCID = 'CC' ")
        MsgBox(sql.v_Sql)

        sql.mkSQL(" INSERT INTO ZCC( ")
        sql.mkSQL(" VALUES ( ")
        sql.mkSQL("'", " CCID           ", "valor     ", 0)    '//Record ID (CC/CZ) 
        sql.mkSQL("'", " CCTABL         ", "valor     ", 0)    '//Table ID 
        sql.mkSQL("'", " CCCODE         ", "valor     ", 0)    '//Primary Code 
        sql.mkSQL("'", " CCLANG         ", "valor     ", 0)    '//Language Code 
        sql.mkSQL("'", " CCALTC         ", "valor     ", 0)    '//Alternate Code 
        sql.mkSQL("'", " CCDESC         ", "valor     ", 0)    '//Description 
        sql.mkSQL("'", " CCSDSC         ", "valor     ", 0)    '//Short Description 
        sql.mkSQL("'", " CCNOT1         ", "valor     ", 0)    '//Note 1 
        sql.mkSQL("'", " CCNOT2         ", "valor     ", 0)    '//Note 2 
        sql.mkSQL(" ", " CCUDC1         ", 5, 0)    '//User Code 1 
        sql.mkSQL(" ", " CCUDC2         ", "valor     ", 0)    '//User Code 2 
        sql.mkSQL(" ", " CCUDC3         ", "valor     ", 0)    '//User Code 3 
        sql.mkSQL(" ", " CCRESV         ", "valor     ", 0)    '//Reservation 
        sql.mkSQL(" ", " CCENDT         ", "valor     ", 0)    '//Entered Date 
        sql.mkSQL(" ", " CCENTM         ", "valor     ", 0)    '//Entered Time 
        sql.mkSQL("'", " CCENUS         ", "valor     ", 0)    '//Entered By User 
        sql.mkSQL(" ", " CCMNDT         ", "valor     ", 0)    '//Last Maintenance Date 
        sql.mkSQL(" ", " CCMNTM         ", "valor     ", 0)    '//Last Maintenance Time 
        sql.mkSQL("'", " CCMNUS         ", "valor     ", 0)    '//Last Maintenance User 
        sql.mkSQL("'", " CCALCR         ", "valor     ", 0)    '//Review Alternate Code 
        sql.mkSQL("'", " CCDESR         ", "valor     ", 0)    '//Review Description 
        sql.mkSQL("'", " CCSDSR         ", "valor     ", 0)    '//Review Short Description 
        sql.mkSQL("'", " CCNOTR         ", "valor     ", 0)    '//Review Note 
        sql.mkSQL("'", " CCREVR         ", "valor     ", 1)    '//Review Required 
        MsgBox(sql.v_Sql)

        sql.mkSQL(" UPDATE ZCC SET ")
        sql.mkSQL("'", " CCID           ", "valor     ", 0)    '//Record ID (CC/CZ) 
        sql.mkSQL("'", " CCTABL         ", "valor     ", 0)    '//Table ID 
        sql.mkSQL("'", " CCCODE         ", "valor     ", 0)    '//Primary Code 
        sql.mkSQL("'", " CCLANG         ", "valor     ", 0)    '//Language Code 
        sql.mkSQL("'", " CCALTC         ", "valor     ", 0)    '//Alternate Code 
        sql.mkSQL("'", " CCDESC         ", "valor     ", 0)    '//Description 
        sql.mkSQL("'", " CCSDSC         ", "valor     ", 0)    '//Short Description 
        sql.mkSQL("'", " CCNOT1         ", "valor     ", 0)    '//Note 1 
        sql.mkSQL("'", " CCNOT2         ", "valor     ", 0)    '//Note 2 
        sql.mkSQL(" ", " CCUDC1         ", 5, 0)    '//User Code 1 
        sql.mkSQL(" ", " CCUDC2         ", "valor     ", 0)    '//User Code 2 
        sql.mkSQL(" ", " CCUDC3         ", "valor     ", 0)    '//User Code 3 
        sql.mkSQL(" ", " CCRESV         ", "valor     ", 0)    '//Reservation 
        sql.mkSQL(" ", " CCENDT         ", "valor     ", 0)    '//Entered Date 
        sql.mkSQL(" ", " CCENTM         ", "valor     ", 0)    '//Entered Time 
        sql.mkSQL("'", " CCENUS         ", "valor     ", 0)    '//Entered By User 
        sql.mkSQL(" ", " CCMNDT         ", "valor     ", 0)    '//Last Maintenance Date 
        sql.mkSQL(" ", " CCMNTM         ", "valor     ", 0)    '//Last Maintenance Time 
        sql.mkSQL("'", " CCMNUS         ", "valor     ", 0)    '//Last Maintenance User 
        sql.mkSQL("'", " CCALCR         ", "valor     ", 0)    '//Review Alternate Code 
        sql.mkSQL("'", " CCDESR         ", "valor     ", 0)    '//Review Description 
        sql.mkSQL("'", " CCSDSR         ", "valor     ", 0)    '//Review Short Description 
        sql.mkSQL("'", " CCNOTR         ", "valor     ", 0)    '//Review Note 
        sql.mkSQL("'", " CCREVR         ", "valor     ", 1)    '//Review Required 
        sql.mkSQL("WHERE ")    '//Review Required 
        sql.mkSQL("CCTABL = 'RFVBVER' ")    '//Review Required 
        sql.mkSQL(" AND CCCODE = 'QQQQQ' ")    '//Review Required 
        MsgBox(sql.v_Sql)


    End Sub

    Private Sub Button5_Click(sender As System.Object, e As System.EventArgs) Handles Button5.Click
        Dim db As New bssConexion
        Dim u As New bssUtil
        Dim rs As New ADODB.Recordset
        Dim conso As New Consolidado

        db.abrirConexion("ERPLX834F", "AS400")
        MsgBox(db.comandoRemoto("RUTINAS", "INDPROD 20140201 20140215"))

        MsgBox(db.AhoraAS400())
        'rs = db.ExecuteSQL("DELETE FROM RHCC WHERE HCONSO = '' IDESC FROM IIM WHERE IPROD = '10035118'")
        'MsgBox(u.cAlfa(rs(0).Value))

        conso.setDB(db)
        conso.setBodega("EM")
        conso.setProv(5013)
        conso.setTipoVeh(4)
        conso.setPedido(946537)



    End Sub

    Private Sub Button6_Click(sender As System.Object, e As System.EventArgs) Handles Button6.Click
        Dim fec As Integer = 20140204
        Dim fecha As Date
        MsgBox(fec.ToString("####-##-##"))

        DateTime.TryParseExact(fec.ToString("####-##-##"), "yyyy-MM-dd", Nothing, Globalization.DateTimeStyles.None, fecha)
        MsgBox(fecha)

        Dim u As New bssUtil

        MsgBox(u.Ceros(23, 5))

        MsgBox(u.Ceros("34.5", 5))

    End Sub

    Private Sub Button7_Click(sender As System.Object, e As System.EventArgs) Handles Button7.Click
        Dim Tarea As bssTarea
        Dim DB As New clsConexion

        DB.gsDatasource = My.Settings.AS400
        DB.gsLib = My.Settings.AMBIENTE
        DB.ModuloActual = "PRUEBA"
        DB.Conexion()

        Tarea = New bssTarea
        With Tarea
            .Inicializa()
            .DB = DB
            .Categoria = "IAL"
            .Subcategoria = "PDF"
            .Clave = "111"
            .PalabraClave = "111"
            .setParam("CONSO", "111")
            .setParam("IAL", "222")
            .setParam("VER", "VER")
            '.Comando = Request.ServerVariables("APPL_PHYSICAL_PATH") & "rutina.bat"
            .Comando = "E:\DespachosDB2\rutina.bat"
            .EnviaTarea()
            MsgBox(.EsperaEjecucion())
        End With

    End Sub

    Private Sub Button8_Click(sender As System.Object, e As System.EventArgs) Handles Button8.Click
        revisaTareasMHT()


    End Sub

    Function paramWebConfig(webConfig, param)
        Dim oXML, oNode, oChild, oAttr

        oXML = CreateObject("Microsoft.XMLDOM")
        oXML.Async = "false"
        oXML.Load(webConfig)
        oNode = oXML.GetElementsByTagName("appSettings").Item(0)
        oChild = oNode.GetElementsByTagName("add")
        For Each oAttr In oChild
            If UCase(oAttr.getAttribute("key")) = UCase(param) Then
                Return oAttr.getAttribute("value")
            End If
        Next
        Return ""

    End Function

    Private Sub Button9_Click(sender As System.Object, e As System.EventArgs) Handles Button9.Click
        revisaPlanillas()
    End Sub
End Class
