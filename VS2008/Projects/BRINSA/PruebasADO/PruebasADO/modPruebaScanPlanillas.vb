﻿Module modPruebaScanPlanillas

    Private DB As clsConexion

    Sub revisaPlanillas()
        Dim fSql As String
        Dim rs As New ADODB.Recordset
        Dim resultado As String = ""

        If Not AbreConexion("") Then
            Exit Sub
        End If

        Dim qArchivo As String
        Dim aPlani() As String
        Dim Planilla As String
        Dim fecha As Date

        qArchivo = Dir(My.Settings.PLANILLAS_ORIGEN & "\*.*")
        Do While qArchivo <> ""
            'Debug.Print CampoNum(qArchivo), qArchivo, FileDateTime("P:\" & qArchivo)
            aPlani = Split(qArchivo, ".")
            Planilla = CampoNum(aPlani(0))
            If Val(Planilla) > 0 Then
                fecha = FileDateTime(My.Settings.PLANILLAS_ORIGEN & "\" & qArchivo)
                fSql = " SELECT EID FROM RECC WHERE EPLANI = " & Planilla & " AND ESCAN <> 2 "
                If DB.OpenRS(rs, fSql) Then
                    FileCopy(My.Settings.PLANILLAS_ORIGEN & "\" & qArchivo, My.Settings.PLANILLAS_DESTINO & "\" & qArchivo)
                    fSql = "UPDATE RECC SET ESCAN=2, ESCANF = '" & Format(fecha, "yyyy-MM-dd-HH.mm.ss.000000") & "' WHERE EPLANI = " & Planilla
                    DB.ExecuteSQL(fSql)
                Else
                    FileCopy(My.Settings.PLANILLAS_ORIGEN & "\" & qArchivo, My.Settings.PLANILLAS_ORIGEN & "\NoProcesados\" & qArchivo)
                End If
                Kill(My.Settings.PLANILLAS_ORIGEN & "\" & qArchivo)
                rs.Close()
            End If
            qArchivo = Dir()
        Loop

    End Sub

    'CampoNum()
    Function CampoNum(Dato) As String
        Dim i As Integer
        Dim Ascii As Integer
        Dim resultado As String = ""
        For i = 1 To Len(Dato)
            Ascii = Asc(Mid(Dato, i, 1))
            If Ascii >= Asc("0") And Ascii <= Asc("9") Then
                resultado &= Chr(Ascii)
            End If
        Next
        Return resultado

    End Function

    Private Function AbreConexion(ModuloActual As String) As Boolean

        DB = New clsConexion
        DB.gsDatasource = My.Settings.AS400
        DB.gsLib = My.Settings.AMBIENTE
        DB.ModuloActual = ModuloActual
        Return DB.Conexion()

    End Function


End Module
