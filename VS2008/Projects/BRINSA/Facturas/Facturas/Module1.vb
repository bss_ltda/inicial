﻿Imports System.IO
Imports ADODB

Module Module1

    Dim fSql As String
    Dim DB As New ADODB.Connection
    Dim flag As Integer
    Dim R As Integer
    Dim Total As Integer
    Dim Archivo As String
    Dim Descartados As String
    Dim CarpetaFacturas As String
    Dim gsConnString As String
    Dim gsForceTranslate As String
    Dim gsLIB As String
    Dim gsLastError As String
    Dim sUser As String


    Public Const RESOURCETYPE_ANY = &H0
    Public Const RESOURCETYPE_DISK = &H1
    Public Const RESOURCETYPE_PRINT = &H2
    Public Const RESOURCETYPE_UNKNOWN = &HFFFF

    Sub Main()
        Dim Ruta As String = "W:\"
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim sMsg As String
        Dim Output As String = ""
        CarpetaFacturas = "E:\Despachos_DB2\ImagenesTransportes\pdf\FacturaPDF"
        'CarpetaFacturas = "R:\ImagenesTransportes\pdf\FacturaPDF"
        'La unidad W: debe estar conectada a \\DOCUSHARE\FACTURAS
        'net use W: \\DOCUSHARE\FACTURAS APLDES2013 /user:Refisal1\APLDES

        If Not System.IO.Directory.Exists("W:\") Then
            WrtSqlError("DOCUSHARE", "W: debe estar conectado a \\DOCUSHARE\FACTURAS")
            Return
        End If
        If Not System.IO.Directory.Exists(CarpetaFacturas) Then

            WrtSqlError("FACTURAS", "Error en carpeta " & CarpetaFacturas)
            Return
        End If

        gsForceTranslate = "37"
        gsLIB = "ERPLX834F"
        gsConnString = "Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=APLLX;Password=LXAPL;Persist Security Info=True;Application Name=DFU001;Default Collection=ERPLX834F;Force Translate=37;"
        sUser = "SISTEMA"
        DB.Open(gsConnString)
        Console.WriteLine("Servicio iniciado")
        ''Dim conn As New ADODB.Connection
        ''gsConnString = "Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=APLLX;Password=LXAPL;Persist Security Info=True;Default Collection=" & gsLIB & ";Force Translate=" & gsForceTranslate & ";"
        ''conn.ConnectionString = gsConnString
        ''conn.Open()

        Try

            Console.WriteLine(CarpetaFacturas)

            Dim Modificados As Integer
            Modificados = 0
            Archivo = ""
            Descartados = ""
            Console.WriteLine("conexion exitosa iniciado")
            fSql = "SELECT CCNOT1 FROM ZCC WHERE CCTABL = 'RDOCPATH' AND CCCODE = 'FACTURAS'"
            Archivo = ""
            Descartados = ""
            Total = 0
            R = 0
            ''For Each dr1 In tareas1.Tables("RFFACCAB").Rows
            ''    Ruta = dr1("CCNOT1")
            ''    Console.WriteLine("Ruta Destino: " & Ruta)
            ''Next

            fSql = " SELECT FPREFIJ, DIGITS(FFACTUR) FFACTUR, TRIM(FPREFIJ) || DIGITS(FFACTUR ) || '_O.PDF' AS PDF, "
            fSql = fSql & " TRIM(FPREFIJ) || DIGITS(FFACTUR ) || '.PDF' AS PDFDEST "
            fSql = fSql & " FROM RFFACCAB "
            fSql = fSql & " WHERE FAFLAG02 = 0  AND FFACTUR <> 0"
            rs.Open(fSql, DB)
            Dim Nombre
            Dim nomDestino
            Archivo = ""
            Descartados = ""
            Total = 0
            R = 0
            Do While Not rs.EOF
                nomDestino = rs("FPREFIJ").Value & "" & rs("FFACTUR").Value
                Nombre = rs("PDF").Value
                nomDestino = rs("PDFDEST").Value
                sMsg = "Error con " & nomDestino
                Console.WriteLine(nomDestino)
                Debug.Print(nomDestino)
                Try
                    If Not System.IO.File.Exists(CarpetaFacturas & "\" & Nombre) Then
                        Console.WriteLine("Generando PDF " & Nombre)
                        sMsg = "E:\Despachos_DB2\FA " & rs("FPREFIJ").Value & " " & rs("FFACTUR").Value & " GEN 100"
                        Output = ""
                        '// Start the child process.
                        Dim p = New Process()
                        '// Redirect the output stream of the child process.
                        p.StartInfo.UseShellExecute = False
                        p.StartInfo.RedirectStandardOutput = True
                        p.StartInfo.FileName = "E:\Despachos_DB2\FA.BAT"
                        p.StartInfo.Arguments = rs("FPREFIJ").Value & " " & rs("FFACTUR").Value & " GEN 100"
                        p.Start()
                        Output = p.StandardOutput.ReadToEnd()
                        p.WaitForExit()
                        Debug.Print(sMsg)
                        Console.WriteLine(sMsg)
                    End If
                    If System.IO.File.Exists(CarpetaFacturas & "\" & Nombre) Then
                        System.IO.File.Copy(CarpetaFacturas & "\" & Nombre, Ruta & nomDestino, True)
                        fSql = "UPDATE RFFACCAB SET FAFLAG02=1 WHERE FPREFIJ = '" & rs("FPREFIJ").Value & "' AND FFACTUR = " & rs("FFACTUR").Value
                        DB.Execute(fSql)
                    Else
                        Console.WriteLine("No encontro " & CarpetaFacturas & "\" & Nombre)
                        WrtSqlError(nomDestino, "No Existe " & CarpetaFacturas & "\" & Nombre & " " & Output)
                    End If
                Catch ex As Exception
                    gsLastError = Err.Description.ToString()
                    'fSql = "{{ CALL RRIMPFFAC PARM( '" & dr("FPREFIJ") & "' '" & dr("FFACTUR") & "' 'GEN' '100' ) }} "
                    WrtSqlError(fSql, nomDestino & "<br/>" & gsLastError)
                    Console.WriteLine(gsLastError)
                End Try
                rs.MoveNext()
            Loop
            fSql = "UPDATE RFPARAM SET CCUDTS = NOW() WHERE CCTABL='PROGRAMADAS' AND CCCODE = 'FACTURAS'"
            DB.Execute(fSql)
            Console.WriteLine("Servicio Finalizado")
        Catch ex As Exception
            gsLastError = Err.Description
            WrtSqlError(fSql, gsLastError)
            Console.WriteLine(gsLastError)
        End Try

    End Sub
    Function Ceros(ByVal Val As Object, ByVal C As Integer)

        If CInt(C) > Len(Trim(Val)) Then
            Ceros = Right(RepiteChar(CInt(C), "0") & Trim(Val), CInt(C))
        Else
            Ceros = Val
        End If

    End Function

    Function RepiteChar(ByVal Cant As Integer, ByVal Txt As Char) As String
        Dim i As Integer

        RepiteChar = ""
        For i = 1 To Cant
            RepiteChar = RepiteChar & Txt
        Next

    End Function

    Sub WrtSqlError(sql As String, Descrip As String)
        Console.WriteLine(sql)
        Console.WriteLine(Descrip)
        Dim fSql As String
        Dim vSql As String
        Dim locVer As String

        Try
            locVer = My.Application.Info.ProductName & "." & My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & "." & My.Application.Info.Version.Revision
            fSql = " INSERT INTO RFLOG( "
            vSql = " VALUES ( "
            fSql = fSql & " USUARIO   , " : vSql = vSql & "'" & sUser & "', "      '//502A
            fSql = fSql & " PROGRAMA  , " : vSql = vSql & "'" & "VB.NET." & locVer & "." & "SQLERR" & "', "      '//502A
            fSql = fSql & " ALERT     , " : vSql = vSql & "1, "      '//1P0
            fSql = fSql & " EVENTO    , " : vSql = vSql & "'" & Replace(Descrip, "'", "''") & "', "      '//20002A
            fSql = fSql & " TXTSQL    ) " : vSql = vSql & "'" & Replace(sql, "'", "''") & "' ) "      '//5002A
            DB.Execute(fSql & vSql)
        Catch ex As Exception

        End Try

    End Sub


End Module

