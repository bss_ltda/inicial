﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports Excel = Microsoft.Office.Interop.Excel
Imports System
Imports System.Data
Imports System.Data.SqlClient

' Para permitir que se llame a este servicio Web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Service1
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function GenerarExcel(ByVal parametro As String) As String
        Dim objLibroExcel As Excel.Workbook
        Dim objHojaExcel As Excel.Worksheet
        Dim sConnectionString As String
        sConnectionString = "Password=" & My.Settings.Password & ";User ID=" & My.Settings.Usuario & ";" & _
                            "Initial Catalog=InfoNatura;" & _
                            "Data Source=" & My.Settings.Servidor & ""
        Dim objConn As New SqlConnection(sConnectionString)
        objConn.Open()
        Dim num As Integer
        num = 3
        Dim value As String
        value = parametro
        If value = "" Then
            value = 1
        End If

        Dim daAuthors1 As _
        New SqlDataAdapter("Select * From RFSTM where Id=" & value, objConn)
        Dim dsPubs1 As New DataSet("Pubsz1")
        daAuthors1.FillSchema(dsPubs1, SchemaType.Source, "Authors1")
        daAuthors1.Fill(dsPubs1, "Authors1")
        daAuthors1.MissingSchemaAction = MissingSchemaAction.AddWithKey
        daAuthors1.Fill(dsPubs1, "Authors1")
        Dim tblAuthors1 As DataTable
        tblAuthors1 = dsPubs1.Tables("Authors1")
        Dim drCurrent1 As DataRow
        Dim Sentencia(2) As String
        Sentencia(0) = ""
        For Each drCurrent1 In tblAuthors1.Rows
            Sentencia(0) = ""
            Sentencia(0) = drCurrent1("Sentencia")
        Next
        Dim objConn1 As New SqlConnection(sConnectionString)
        objConn1.Open()
        'Dim daAuthors As _
        'New SqlDataAdapter(Sentencia, objConn1)
        'Dim dsPubs As New DataSet("Pubs")
        'daAuthors.FillSchema(dsPubs, SchemaType.Source, "Authors")
        'daAuthors.Fill(dsPubs, "Authors")
        'Dim tblAuthors As DataTable
        'tblAuthors = dsPubs.Tables("Authors")
        'Dim tblAuthors3 As DataTable
        'tblAuthors3 = dsPubs.Tables("Authors")
        Dim m_Excel As Excel.Application
        m_Excel = New Excel.Application
        '    Dim oWord As Excel.Application
        ' m_Excel.Visible = True
        objLibroExcel = m_Excel.Workbooks.Add()
        objHojaExcel = objLibroExcel.Worksheets(1)
        Dim range As Excel.Range
        range = objHojaExcel.Range("A1", Reflection.Missing.Value)
        range.Name = "Hojas"

        With objHojaExcel.ListObjects.Add(SourceType:=0, Source:="ODBC;DSN=" & My.Settings.DSN & ";UID=" & My.Settings.Usuario & ";PWD=" & My.Settings.Password & ";", Destination:=objHojaExcel.Range("$A$1")).QueryTable
            .CommandText = Sentencia(0)
            .RowNumbers = False
            .FillAdjacentFormulas = False
            .PreserveFormatting = True
            .RefreshOnFileOpen = False
            .BackgroundQuery = True
            .RefreshStyle = 1
            .SavePassword = False
            .SaveData = True
            .AdjustColumnWidth = True
            .RefreshPeriod = 0
            .PreserveColumnInfo = True
            .ListObject.DisplayName = "Q_InfoNatura"
            .Refresh(BackgroundQuery:=False)
        End With



        'For Each dr1Current In tblAuthors.Columns
        '    objHojaExcel.Cells(fil, Columnas).value = dr1Current.ColumnName.ToString()
        '    Columnas = Columnas + 1
        'Next
        'fil = 1
        'For Each drCurrent In tblAuthors3.Rows
        '    fil = fil + 1
        '    For i = 1 To (tblAuthors3.Columns.Count)
        '        objHojaExcel.Cells(fil, i).value = drCurrent(i - 1).ToString
        '    Next

        'Next
        'If fil > 1 Then
        '    objHojaExcel.Range(tblAuthors3.Rows.Count & ":" & tblAuthors3.Columns.Count).Select()
        '    Try
        '        'Dim obj As Excel.XlListObjectSourceType = Excel.XlListObjectSourceType.xlSrcRange
        '        'Dim obj2 As Excel.XlYesNoGuess = Excel.XlYesNoGuess.xlYes
        '        'objHojaExcel.Cells.Select()
        '        'objHojaExcel.Range("A2").Activate()
        '        'objHojaExcel.Cells.EntireColumn.AutoFit()
        '        'objHojaExcel.ListObjects.AddEx(obj, objHojaExcel.Range(Cell1:=(1) & ":" & (tblAuthors.Rows.Count + 1)), , obj2).Name = "Reporte"
        '        'objHojaExcel.ListObjects("Reporte").TableStyle = "TableStyleMedium2"

        '    Catch ex As Exception
        '    End Try
        value = ""
        value = parametro
        value = "RprtCarter" & value
        If value = "" Then
            value = Date.Today.Month & "" & Date.Today.Day & "" & Date.Today.Year & "" & Date.Today.Hour & "" & Date.Today.Minute & "" & Date.Today.Second
        End If
        m_Excel.Application.DisplayAlerts = False
        m_Excel.ActiveWorkbook.SaveAs(My.Settings.CarpetaDestino & "" & value, CreateBackup:=False)
        m_Excel.ActiveWorkbook.Close()
        'End If
        Return "Hola a todos"
    End Function

End Class