﻿Module BSSCommon
    Public Const tNum = 1
    Public Const tStr = 2
    Public Const tXfn = 3

    Public Enum tDato
        tNum = 1
        tStr = 2
    End Enum

    Public Enum ChkVerApp
        AppOK = 1
        AppVersionIncorrect = 2
        AppSeparadorDecimal = 3
        AppDeshabilitada = 4
        AppEjecutando = 5
    End Enum
    Public fSql As String
    Public vSql As String


    Public Function CalcConsec(ByRef sID As String, ByRef bLee As Boolean, ByRef TopeMax As String) As Double
        Dim rs As ADODB.Recordset
        Dim locID As Double
        Dim sql As String
        Dim sConsec As String
        Dim tMax As Double

        sql = "SELECT CCDESC FROM ZCCL01 WHERE CCTABL = 'SECUENCE' AND CCCODE='" & sID & "'"
        rs = New ADODB.Recordset
        tMax = Val(TopeMax)

        With rs
            .CursorLocation = ADODB.CursorLocationEnum.adUseServer
            .Open(sql, DB_DB2, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)
            If Not (.BOF And .EOF) Then
                locID = Val(.Fields("CCDESC").Value) + 1
                If locID > tMax Then
                    locID = 1
                End If
                sConsec = Format(locID, New String("0", Len(TopeMax)))
                If Not bLee Then
                    .Fields("CCDESC").Value = sConsec
                    .Update()
                End If
                .Close()
            Else
                locID = 1
                .Close()
                sConsec = Format(1, New String("0", Len(8)))
                DB_DB2.Execute(" INSERT INTO ZCC (CCID, CCTABL, CCCODE, CCDESC ) " & " VALUES( 'CC', 'SECUENCE', '" & sID & "', '" & sConsec & "' )")
            End If
        End With

        CalcConsec = locID

    End Function

    Public Function CheckVer(ByRef Aplicacion As String) As ChkVerApp
        Dim rs As ADODB.Recordset
        Dim lVer As String = "No hay registro"
        Dim result As ChkVerApp = ChkVerApp.AppOK
        Dim locVer As String

        locVer = "(" & My.Application.Info.Version.Major & "." & _
                 My.Application.Info.Version.Minor & "." & _
                 My.Application.Info.Version.Revision & ")"

        rs = New ADODB.Recordset
        With rs
            .Open("SELECT * FROM ZCCL01 WHERE CCTABL='RFVBVER' AND CCCODE = '" & Aplicacion & "'", DB_DB2)
            If Not (.EOF And .BOF) Then
                lVer = Trim(.Fields("CCSDSC").Value)
                If InStr(lVer, "*NOCHK") > 0 Then
                    result = ChkVerApp.AppOK
                End If
                bChkProceso = rs.Fields("CCUDC3").Value = 1
                If result = ChkVerApp.AppOK And InStr(lVer, locVer) > 0 Then
                    result = ChkVerApp.AppOK
                Else
                    result = ChkVerApp.AppVersionIncorrect
                End If
                If result = ChkVerApp.AppOK And rs.Fields("CCUDC1").Value = 0 Then
                    result = ChkVerApp.AppDeshabilitada
                End If
                If result = ChkVerApp.AppOK And rs.Fields("CCUDC1").Value = 2 Then
                    result = ChkVerApp.AppEjecutando
                End If
                .Close()
            End If
        End With

        If result = ChkVerApp.AppOK And InStr(1, "" & Val("0.7"), ",") > 0 Then
            result = ChkVerApp.AppSeparadorDecimal
        End If

        Return result

    End Function

    Public Sub AppEjecutando(ByRef Aplicacion As String, ByVal Estado As Integer)

        DB_DB2.Execute("UPDATE ZCC SET CCUDC1 = " + CStr(Estado) + " WHERE CCTABL='RFVBVER' AND CCCODE = '" & Aplicacion & "'")

    End Sub

    Function CodeCB(ByRef valor As String, Optional ByRef sep As String = "") As String
        Dim aCode As Object

        aCode = Split(valor, IIf(sep = "", " ", sep))
        CodeCB = aCode(0)

    End Function

    Function DescCB(ByRef valor As String, Optional ByRef sep As String = "") As String
        Dim aCode As Object

        aCode = Split(valor, IIf(sep = "", " ", sep))
        DescCB = Trim(Mid(valor, Len(aCode(0)) + 1))

    End Function

    Sub mkSql(ByVal tipoF As tDato, ByVal campo As String, ByVal valor As Object, Optional ByVal fin As Integer = 0)
        Dim sep As String

        If Left(Trim(UCase(fSql)), 6) = "INSERT" Then
            sep = IIf(tipoF = tStr, "'", "")
            fSql = fSql & campo
            Select Case fin
                Case 0
                    fSql = fSql & ", "
                Case 10, 11, -1, 1
                    fSql = fSql & " ) "
            End Select
            If valor.ToString = "" Then
                valor = IIf(tipoF = tStr, "", "0")
            End If
            vSql = vSql & sep & CStr(valor) & sep
            Select Case fin
                Case 10
                    vSql = vSql & "  "
                Case 11, -1, 1
                    vSql = vSql & " )"
                Case 0
                    vSql = vSql & ", "
            End Select
        Else
            sep = IIf(tipoF = tStr, "'", "")
            If valor.ToString = "" Then
                valor = IIf(tipoF = tStr, "", "0")
            End If
            fSql = fSql & campo & " = " & sep & CStr(valor) & sep & IIf(fin = 10 Or fin = 11, " ", ", ")
        End If

    End Sub

    Sub mkUpd(ByVal tipoF As tDato, ByVal campo As String, ByVal valor As Object, Optional ByVal fin As Boolean = False)
        fSql += CStr(campo) + " = " + IIf(tipoF = tDato.tStr, "'", "") + valor + IIf(tipoF = tDato.tStr, "'", "") + IIf(fin, "", ", ")
    End Sub

    Public Function Busca(ByVal Sql As String, ByVal DB As ADODB.Connection, Optional ByRef Retorna As String = "") As Boolean
        Dim rs As New ADODB.Recordset
        Dim r As Boolean
        rs.Open(Sql, DB)
        If Not rs.EOF Then
            If Retorna <> "" Then
                Retorna = CStr(rs.Fields(0).Value)
            End If
        End If
        r = Not rs.EOF
        rs.Close()
        Return r

    End Function

    Public Function CopyFiles(ByVal sourcePath As String, ByVal DestinationPath As String) As Boolean

        If System.IO.File.Exists(sourcePath) = True Then
            System.IO.File.Copy(sourcePath, DestinationPath)
            Return True
        Else
            Return False
        End If

    End Function

    Public Function ExecuteSQL(ByVal sql As String, ByRef r As Integer) As ADODB.Recordset

        On Error Resume Next
        ExecuteSQL = DB_DB2.Execute(sql, r)
        If Err.Number <> 0 Then
            Console.Write(Err.Description)
            Console.Write(sql)
            Console.WriteLine("Presione una tecla.")
            Console.ReadKey()
            End
        End If
        On Error GoTo 0

    End Function

    Public Function dtFromAS400(ByVal Fecha As String) As Date
        Dim aFecha() As String
        Dim lFecha As String

        lFecha = Left(Fecha, 10) & "," & Replace(Mid(Fecha, 12, 5), ".", ",")
        lFecha = Replace(lFecha, "-", ",")
        'Debug.Print lFecha

        aFecha = Split(lFecha, ",")

        dtFromAS400 = DateSerial(CInt(aFecha(0)), CInt(aFecha(1)), CInt(aFecha(2)))
        dtFromAS400 = dtFromAS400 & " " & TimeSerial(CInt(aFecha(3)), CInt(aFecha(4)), 0)

    End Function
    Function Ceros(ByVal Val As Object, ByVal C As Integer)

        If CInt(C) > Len(Trim(Val)) Then
            Ceros = Right(RepiteChar(CInt(C), "0") & Trim(Val), CInt(C))
        Else
            Ceros = Val
        End If

    End Function

    Function RepiteChar(ByVal Cant As Integer, ByVal Txt As Char) As String
        Dim i As Integer

        RepiteChar = ""
        For i = 1 To Cant
            RepiteChar = RepiteChar & Txt
        Next

    End Function

    Function DescTipVeh(ByVal sTipVeh As Object) As String
        Dim sTipV As String
        Dim rs As New ADODB.Recordset

        sTipV = Ceros(Val(sTipVeh), 2)
        DescTipVeh = ""
        rs.Open("SELECT CCDESC FROM ZCCL01 WHERE CCTABL='SPTIPVEH' AND CCCODE='" & sTipV & "'", DB_DB2)
        If Not rs.EOF Then
            DescTipVeh = CampoAlfa(rs.Fields("CCDESC").Value, 0)
        End If
        rs.Close()
        rs = Nothing

    End Function

    Public Function CampoAlfa(ByVal dato As String, ByRef Largo As Integer) As String
        Dim i As Integer
        Dim ascii As Integer

        CampoAlfa = ""
        For i = 1 To Len(dato)
            ascii = Asc(Mid(dato, i, 1))
            If InStr(1, " !@#$&*()-_=+{}[]\|:;.?/<>", Chr(ascii)) > 0 Then
                CampoAlfa = CampoAlfa & Chr(ascii)
            ElseIf Chr(ascii) = "á" Then
                CampoAlfa = CampoAlfa & "a"
            ElseIf Chr(ascii) = "é" Then
                CampoAlfa = CampoAlfa & "e"
            ElseIf Chr(ascii) = "í" Then
                CampoAlfa = CampoAlfa & "i"
            ElseIf Chr(ascii) = "ó" Then
                CampoAlfa = CampoAlfa & "o"
            ElseIf Chr(ascii) = "ú" Then
                CampoAlfa = CampoAlfa & "u"
            ElseIf Chr(ascii) = "ñ" Then
                CampoAlfa = CampoAlfa & "n"
            ElseIf ascii = 10 Or ascii = 13 Then
                CampoAlfa = CampoAlfa & " "
            ElseIf ascii >= Asc("0") And ascii <= Asc("9") Then
                CampoAlfa = CampoAlfa & Chr(ascii)
            ElseIf ascii >= Asc("A") And ascii <= Asc("Z") Then
                CampoAlfa = CampoAlfa & Chr(ascii)
            ElseIf ascii >= Asc("a") And ascii <= Asc("z") Then
                CampoAlfa = CampoAlfa & Chr(ascii)
            End If
        Next

        If Largo <> 0 Then
            CampoAlfa = Left(CampoAlfa, Largo)
        End If

    End Function

    Function LastDayMonth(ByVal Fecha) As Date

        LastDayMonth = DateSerial(Year(Fecha), Month(Fecha), 1)
        LastDayMonth = DateAdd("m", 1, LastDayMonth)
        LastDayMonth = DateAdd("d", -1, LastDayMonth)

    End Function

    'sUrl()
    'Convierte parametros de url para pasar la url como parametro
    Function sUrl(ByVal p1 As String) As String
        Dim i As Integer
        Dim ascii As Integer
        Dim Result As String = ""

        For i = 1 To Len(p1)
            ascii = Asc(Mid(p1, i, 1))
            If InStr(1, " $&<>?;#:=,""'~+%/", Chr(ascii)) > 0 Then
                Result += "%" & CStr(Hex(ascii))
            Else
                Result += Chr(ascii)
            End If
        Next

        Return Result

    End Function

End Module


