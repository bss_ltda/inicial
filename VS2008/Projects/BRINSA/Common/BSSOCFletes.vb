﻿Module BSSOCFletes
    Public FL_Desde As Date
    Public FL_Hasta As Date
    Public FL_Periodo As Long           'HVALREP
    Public FL_ByPass As Integer         'ByPass de aprobacion de Orden

    Sub PrepaInfoOCFletes()
        Dim Sql As String
        Dim r As Integer
        Dim rs As New ADODB.Recordset
        Dim Peso As Double
        Dim CapVeh As Double
        Dim PesoConso As Double
        Dim FecFactu As Date
        Dim Planis As String

        If Not SiEsHora("CARGUECENDIS") Then
            Exit Sub
        End If
        PeriodoOCFletes()
        ChkFacturados()

        Sql = "UPDATE RHCC SET HOCFLE=0, HGENERA='2' "
        Sql = Sql & " WHERE HESTAD <> 9 AND HGENERA <> '0' AND HOCFLE <> 0 AND HOCFLE < 300000 "
        ExecuteSQL(Sql, r)

        Sql = "UPDATE RHCC SET HDIVIS = 'CSECA'"
        Sql = Sql & " WHERE HDIVIS IN( 'SAL', 'ASEO', 'MIX' ) AND HGENERA = '2'"
        ExecuteSQL(Sql, r)

        Sql = "UPDATE RHCC SET HGENERA='0' "
        Sql = Sql & " WHERE HGENERA = '2' AND ( HESTAD = 9 OR HPROVE = 9999 )"
        ExecuteSQL(Sql, r)

        Sql = "SELECT HCONSO, CASE WHEN RREF02='QSE' THEN 'CSECA' ELSE RREF02 END AS TIPCARG "
        Sql = Sql & " FROM RHCC INNER JOIN RDCC ON DCONSO=HCONSO "
        Sql = Sql & " INNER JOIN RIIM ON DPROD=RIPROD"
        Sql = Sql & " WHERE HDIVIS='QUIMICOS' AND HGENERA = '2'"
        rs.Open(Sql, DB_DB2)
        Do While Not rs.EOF
            Sql = "UPDATE RHCC SET HDIVIS = '" & rs.Fields("TIPCARG").Value & "'"
            Sql = Sql & " WHERE HCONSO='" & rs.Fields("HCONSO").Value & "'"
            ExecuteSQL(Sql, r)
            rs.MoveNext()
        Loop
        rs.Close()

        Sql = "SELECT * FROM RHCC WHERE HGENERA = '2' "
        rs.Open(Sql, DB_DB2)
        Do While Not rs.EOF
            Planis = ""
            PlanillasConso(rs.Fields("HCONSO").Value, Planis, FecFactu, PesoConso)
            If rs.Fields("HTONPAG").Value = 0 Then
                Peso = (rs.Fields("HTIQUT").Value - rs.Fields("HPEVAC").Value) / 1000
                Peso = PagarPeso(Ceros(rs.Fields("HTIPO").Value, 2), Peso, CapVeh, Left(rs.Fields("HDIVIS").Value, 5), _
                                 rs.Fields("HSEGME").Value, rs.Fields("HPROVE").Value, rs.Fields("HPLACA").Value)
            Else
                Peso = rs.Fields("HTONPAG").Value
            End If

            ActualizaCCD(rs.Fields("HCONSO").Value, False)

            rs.MoveNext()
        Loop
        rs.Close()


    End Sub

    Sub ActualizaCCD(ByVal Conso As String, ByVal bUpdTrf As Boolean)
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim sCargueCendis As String = ""
        Dim ValorCargue As Double, CapVeh As Double
        Dim pSql As String
        Dim r As Integer

        fSql = "SELECT RHCC.*, S.CCSDSC AS ESTADO, CASE WHEN TPROVE IS NULL THEN '' ELSE TNOMBRE END AS TNOMBRE "
        fSql = fSql & " FROM RHCC LEFT OUTER JOIN RTTRA ON HPROVE=TPROVE "
        fSql = fSql & " INNER JOIN VSPESTAD S ON HESTAD=CCCODE"
        fSql = fSql & " WHERE HCONSO='" & Conso & "'"
        rs.Open(fSql, DB_DB2)
        rs1.Open("SELECT * FROM RFFLCCD WHERE FCONSO='" & Conso & "'", DB_DB2)
        If rs1.EOF Then
            fSql = " INSERT INTO RFFLCCD("
            vSql = " VALUES( "
            mkSql(tNum, "FPER     ", rs.Fields("HVALREP").Value)                         'Periodo
            mkSql(tStr, "FCONSO   ", rs.Fields("HCONSO").Value)                          'Consolid
            mkSql(tNum, "FPROVE   ", rs.Fields("HPROVE").Value)                          'Transp
            mkSql(tStr, "FNPROVE  ", rs.Fields("HNPROVE").Value)                         'Transportador
            mkSql(tStr, "FTIPVEH  ", DescTipVeh(rs.Fields("HTIPO").Value))               'Tipo Vehiculo
            mkSql(tStr, "FPLACA   ", rs.Fields("HPLACA").Value)                          'Placa
            mkSql(tStr, "FPLTA    ", rs.Fields("HPLTA").Value)                           'Plta
            mkSql(tStr, "FBOD     ", rs.Fields("HREF05").Value)                          'Bod
            mkSql(tStr, "FTIPCARGA", rs.Fields("HDIVIS").Value)                          'Tipo Carga
            mkSql(tStr, "FSEGME   ", rs.Fields("HSEGME").Value)                          'Segmento
            mkSql(tStr, "FMERCA   ", rs.Fields("HMERCA").Value)                          'Mercado
            mkSql(tNum, "FENTRE   ", rs.Fields("HENTRE").Value)                          'Entregas
            mkSql(tStr, "FTIPCCD  ", rs.Fields("HPRCDVA").Value)                         'Tipo de Cargue CENDIS
            mkSql(tStr, "FTIPCCDR ", rs.Fields("HPRCDVA").Value)                         'Tipo de Cargue CENDIS
            mkSql(tNum, "FVCARGCD ", rs.Fields("HVALORCD").Value)                        'Vlr Cargue CENDIS
            mkSql(tNum, "FPESOBRUT", rs.Fields("HPETOT").Value, 1)                       'Peso Bruto
            ExecuteSQL(fSql & vSql, r)
        Else
            bUpdTrf = (rs1.Fields("STS02").Value = 0)
        End If

        CargueCendis(rs, sCargueCendis, ValorCargue, CapVeh)

        fSql = " UPDATE RFFLCCD SET "
        vSql = ""
        mkSql(tNum, "FPER     ", FL_Periodo)                            'Periodo
        mkSql(tNum, "FESTAD   ", rs.Fields("HESTAD").Value)                   'Estado
        mkSql(tStr, "FESTADD  ", rs.Fields("ESTADO").Value)                   'Estado
        mkSql(tNum, "FOCFLE   ", rs.Fields("HOCFLE").Value)                   'Orden Compra Fletes
        mkSql(tNum, "STS01   ", IIf(rs.Fields("HOCFLE").Value <> 0, 1, 0))    'Consolidado con Orden
        mkSql(tStr, "FINICARG ", Left(rs.Fields("HCARGI").Value, 10))         'Inicio de Cargue
        mkSql(tStr, "FCARGADO ", Left(rs.Fields("HFEC03").Value, 10))         'Fecha Factura
        mkSql(tNum, "FPROVE   ", rs.Fields("HPROVE").Value)                   'Transp
        mkSql(tStr, "FNPROVE  ", rs.Fields("TNOMBRE").Value)                  'Transportador
        mkSql(tStr, "FTIPVEH  ", DescTipVeh(rs.Fields("HTIPO").Value))        'Tipo Vehiculo
        mkSql(tStr, "FPLACA   ", rs.Fields("HPLACA").Value)                   'Placa
        mkSql(tStr, "FPLTA    ", rs.Fields("HPLTA").Value)                    'Plta
        mkSql(tStr, "FBOD     ", rs.Fields("HREF05").Value)                   'Bod
        mkSql(tStr, "FTIPCARGA", rs.Fields("HDIVIS").Value)                   'Tipo Carga
        mkSql(tStr, "FSEGME   ", rs.Fields("HSEGME").Value)                   'Segmento
        mkSql(tStr, "FMERCA   ", rs.Fields("HMERCA").Value)                   'Mercado
        mkSql(tNum, "FENTRE   ", rs.Fields("HENTRE").Value)                   'Entregas
        mkSql(tNum, "FPESOBRUT", rs.Fields("HPETOT").Value)                   'Peso Bruto

        pSql = "( SELECT IFNULL( AVG(PQORD), 0 ) "
        pSql = pSql & " FROM HPOL01 "
        pSql = pSql & " WHERE PORD = " & rs.Fields("HOCFLE").Value & " AND PORD <> 0 AND PLINE=1 )"
        mkSql(tDato.tNum, "FPESOPAG ", pSql)                                  'Peso Pagado
        mkSql(tDato.tNum, "FCAPVEH  ", CapVeh)                                'Transp

        If bUpdTrf Then
            mkSql(tDato.tStr, "FTIPCCDR ", sCargueCendis)                     'Tipo de Cargue CENDIS
            mkSql(tNum, " FTCARGCDR  ", ValorCargue)                          'Trf Cargue CENDIS
            mkSql(tNum, " FVCARGCDR  ", ValorCargue * CapVeh)                 'Vlr Cargue CENDIS
        End If

        mkSql(tDato.tStr, "FTIPCCD  ", sCargueCendis)                         'Tipo de Cargue CENDIS
        mkSql(tNum, " FTCARGCD  ", ValorCargue)                               'Trf Cargue CENDIS
        mkSql(tNum, " FVCARGCD  ", ValorCargue * CapVeh, 10)                  'Vlr Cargue CENDIS
        fSql = fSql & " WHERE FCONSO = '" & Conso & "'"

        ExecuteSQL(fSql, r)

        fSql = "UPDATE RFFLCCD SET "
        fSql = fSql & " FVCARGCD = FTCARGCD * FPESOPAG, "
        fSql = fSql & " FVCARGCDR = FTCARGCDR * FPESOPAG"
        fSql = fSql & " WHERE FCONSO = '" & Conso & "'"
        ExecuteSQL(fSql, r)

        rs.Close()
        rs1.Close()

    End Sub

    Public Sub CargueCendis(ByVal rs_RHCC As ADODB.Recordset, ByRef TipCargueCendis As String, ByRef ValorCargue As Double, ByRef CapVeh As Double)
        Dim rs As New ADODB.Recordset
        Dim rs2 As New ADODB.Recordset
        Dim sql As String
        Dim bUrbano As Boolean

        sql = "SELECT CCDESC FROM RTVEH INNER JOIN ZCC ON CCTABL = 'FLCAPVEH' AND VTIPVEHC=CCCODE"
        sql = sql & " WHERE VPLACA='" & rs_RHCC.Fields("HPLACA").Value & "' AND VTIPVEH='" & Ceros(rs_RHCC.Fields("HTIPO").Value, 2) & "'"
        rs2.Open(sql, DB_DB2)
        If Not rs2.EOF Then
            CapVeh = Val(rs2.Fields("CCDESC").Value)
        Else
            rs2.Close()
            rs2.Open("SELECT CCNOT1 FROM SPTIPVEH WHERE CCCODE=" & rs_RHCC.Fields("HTIPO").Value, DB_DB2)
            If Not rs2.EOF Then
                CapVeh = Val(rs2.Fields("CCNOT1").Value)
            End If
        End If
        rs2.Close()

        sql = "SELECT PCONSO FROM RESTIB WHERE PCONSO='" & rs_RHCC.Fields("HCONSO").Value & "' AND PTIPDES = 'PALLET'"
        rs.Open(sql, DB_DB2)
        If Not rs.EOF Then
            TipCargueCendis = "PALLET"
            rs.Close()
            rs = Nothing
            rs2 = Nothing
            Exit Sub
        End If
        sql = "SELECT * FROM ZCCL01 WHERE CCTABL='FLENTADD' AND CCCODE='" & rs_RHCC.Fields("HCIUDAD").Value & "*" & rs_RHCC.Fields("HDEPTO").Value & "'"
        rs2.Open(sql, DB_DB2)
        bUrbano = Not rs2.EOF
        rs2.Close()

        If rs_RHCC.Fields("HPRCDVA").Value = "PREDISTRIBUIDO" Then
            sql = "SELECT CCNOT1 FROM ZCC WHERE CCTABL='FLCARGUE' AND CCCODE='PREDISTRIBUIDO'"
            TipCargueCendis = "PREDISTRIBUIDO"
            rs2.Open(sql, DB_DB2)
            If Not rs2.EOF Then
                ValorCargue = Val(rs2.Fields(0).Value)
            End If
            rs2.Close()
        Else
            Select Case rs_RHCC.Fields("HREF05").Value
                Case "PT", "GJ"
                    sql = "SELECT CCNOT1 FROM ZCC WHERE CCTABL='FLCARGUE'"
                Case "TC", "TG"
                    sql = "SELECT CCNOT2 FROM ZCC WHERE CCTABL='FLCARGUE'"
                Case Else
                    sql = ""
            End Select
            If sql <> "" Then
                If bUrbano Then
                    sql = sql & " AND CCCODE='NORMAL'"
                    TipCargueCendis = "NORMAL"
                Else
                    rs2.Open("SELECT * FROM RHCDL01 WHERE SCONSO='" & rs_RHCC.Fields("HCONSO").Value & "'", DB_DB2)
                    If Not rs2.EOF Then
                        sql = sql & " AND CCCODE='CROSSDOCKING'"
                        TipCargueCendis = "CROSSDOCKING"
                    Else
                        sql = sql & " AND CCCODE='NORMAL'"
                        TipCargueCendis = "NORMAL"
                    End If
                    rs2.Close()
                End If
                rs2.Open(sql, DB_DB2)
                If Not rs2.EOF Then
                    ValorCargue = Val(rs2.Fields(0).Value)
                End If
                rs2.Close()
            End If
        End If

        If rs_RHCC.Fields("HPROVE").Value = 9999 Then
            TipCargueCendis = "N/A"
            ValorCargue = 0
        End If

        rs = Nothing
        rs2 = Nothing

    End Sub

    Sub ChkFacturados()
        Dim rs As New ADODB.Recordset
        Dim r As Integer

        fSql = " SELECT DISTINCT HCONSO"
        fSql = fSql & " FROM "
        fSql = fSql & "     RHCC INNER JOIN RDCC ON HCONSO=DCONSO"
        fSql = fSql & "     INNER JOIN SIL ON  SIL.ILORD = DPEDID AND ILSEQ = DLINEA"
        fSql = fSql & " WHERE"
        fSql = fSql & "     HGENERA = '1'"
        'fSql = fSql & " AND ILDATE >= 20110101"
        rs.Open(fSql, DB_DB2)
        Do While Not rs.EOF
            fSql = "UPDATE RHCC SET HGENERA = '2' WHERE HCONSO='" & rs.Fields("HCONSO").Value & "'"
            ExecuteSQL(fSql, r)
            rs.MoveNext()
        Loop
        rs.Close()

    End Sub

    Function PlanillasConso(ByVal Conso As String, ByRef Planis As String, ByRef FecFactu As Date, ByRef PesoConso As Double) As Boolean
        Dim sql As String
        Dim rs1 As New ADODB.Recordset
        Dim sFecF As String
        Dim r As Integer

        PesoConso = 0
        sql = "SELECT DPEDID, DLINEA, LLLOAD, LHSDTE, LHLDTE, LHLTME, (LLSQTY * IWGHT)/1000 AS PESO"
        sql = sql & " FROM RDCC INNER JOIN LLL ON DPEDID=LLORDN AND DLINEA=LLOLIN"
        sql = sql & " INNER JOIN LLH ON LLLOAD = LHLOAD "
        sql = sql & " INNER JOIN IIM ON LLPROD=IPROD"
        sql = sql & " WHERE DCONSO='" & Conso & "' AND LHLOAD < 800000 "
        rs1.Open(sql, DB_DB2)
        Do While Not rs1.EOF
            sql = "UPDATE RDCC SET DPLANI=" & rs1.Fields("LLLOAD").Value
            sql = sql & ", DFERCA='"
            sql = sql & Format(rs1.Fields("LHSDTE").Value, "0000\-00\-00\-")
            sql = sql & Format(rs1.Fields("LHLTME").Value, "00\.00\.00") & ".000000'"
            sql = sql & " WHERE DPEDID=" & rs1.Fields("DPEDID").Value & " AND DLINEA=" & rs1.Fields("DLINEA").Value
            ExecuteSQL(sql, r)
            PesoConso = PesoConso + rs1.Fields("PESO").Value
            rs1.MoveNext()
        Loop
        rs1.Close()

        sql = "SELECT DPEDID, DLINEA, ILID, ILDPFX, ILDOCN, ILLINE, ILDATE "
        sql = sql & " FROM RDCC INNER JOIN SIL ON ILORD = DPEDID AND ILSEQ = DLINEA"
        sql = sql & " WHERE DCONSO='" & Conso & "'"
        rs1.Open(sql, DB_DB2)
        Do While Not rs1.EOF
            sql = "UPDATE RDCC SET "
            sql = sql & " XCHGPRG='" & rs1.Fields("ILID").Value & "', "
            sql = sql & " XNAVRE='" & rs1.Fields("ILDPFX").Value & "', "
            sql = sql & " XNROINS=" & rs1.Fields("ILDOCN").Value & ", "
            sql = sql & " DENTRE=" & rs1.Fields("ILLINE").Value & ", "
            sql = sql & " XFEENDO='" & Format(rs1.Fields("ILDATE").Value, "0000\-00\-00") & "-14.00.00.000000'"
            sql = sql & " WHERE DPEDID=" & rs1.Fields("DPEDID").Value & " AND DLINEA=" & rs1.Fields("DLINEA").Value
            ExecuteSQL(sql, r)
            rs1.MoveNext()
        Loop
        rs1.Close()

        'XFEENDO FECHA DE FACTURA
        'XCHGPRG/XNAVRE/XNROINS/DENTRE  ILID/ILDPFX/ILDOCN/ILLINE NUMERO DE FACTURA

        sql = "SELECT DISTINCT DPLANI, DFERCA, XFEENDO, DATE(XFEENDO) FF, TIME(DFERCA) FH, YEAR(XFEENDO) AS YY,"
        sql = sql & " DATE(DFERCA) AS FPLANI, DATE(XFEENDO) AS FFACT"
        sql = sql & " FROM RDCC "
        sql = sql & " WHERE DCONSO='" & Conso & "' AND DPLANI <> 0"
        sql = sql & " ORDER BY XFEENDO, DPLANI"
        rs1.Open(sql, DB_DB2)
        sql = "" : sFecF = ""
        Do While Not rs1.EOF
            Planis = Planis & rs1.Fields("DPLANI").Value & " "
            If sFecF = "" Then
                sFecF = rs1.Fields("XFEENDO").Value
                PlanillasConso = False
                FecFactu = DateSerial(1900, 1, 1)
            End If
            If rs1.Fields("YY").Value > 2000 Then
                sFecF = rs1.Fields("FF").Value & "-" & rs1.Fields("FH").Value & ".000000"
                PlanillasConso = True
                FecFactu = dtFromAS400(rs1.Fields("XFEENDO").Value)
            End If
            sql = "UPDATE RHCC SET "
            sql = sql & " HCARGADO='" & rs1.Fields("DFERCA").Value & "', "
            sql = sql & " HFEC03='" & sFecF & "' "
            sql = sql & " WHERE HCONSO='" & Conso & "'"
            ExecuteSQL(sql, r)
            rs1.MoveNext()
        Loop
        rs1.Close()
        If sql <> "" Then
            ExecuteSQL(sql, r)
        End If

    End Function

    Public Function PagarPeso(ByVal sTipVeh As String, ByVal Tiquete As Double, ByRef CapVeh As Double, ByVal sTipCarga As String, _
                           ByVal sSegmento As String, ByVal dProve As Double, ByVal sPlaca As String) As Double
        Dim rs As New ADODB.Recordset
        Dim aTipNeg() As String
        Dim aCap() As String
        Dim PesoNeg As Double
        Dim PesoQuim As Double
        Dim PesoQuimSeco As Double
        Dim bExcepcion As Boolean
        Dim sql As String
        Dim r As Integer

        'TIQ, CAP NEG = Lo del tiquete, mínimo la capacidad negociada
        rs = ExecuteSQL("SELECT * FROM ZCCL01 WHERE CCTABL='SPTIPCAR' AND CCCODE='" & sTipCarga & "'", r)
        aTipNeg = Split(rs.Fields("CCNOT1").Value, ",")
        aTipNeg(0) = Trim(aTipNeg(0))
        aTipNeg(1) = Trim(aTipNeg(1))
        rs.Close()

        'Para mulas, verificar si la cap neg es 32 o 34 ton
        sql = "SELECT * FROM RTVEH "
        sql = sql & " WHERE VTIPVEH='04' AND VTIPVEH='" & sTipVeh & "'"
        sql = sql & " AND VPLACA='" & sPlaca & "' AND VPLACA <> ''"
        rs = ExecuteSQL(sql, r)
        If Not rs.EOF Then
            PesoNeg = Val(Mid(rs.Fields("VTIPVEHC").Value, 3, 2))
            CapVeh = PesoNeg
        Else
            PesoNeg = 0
        End If
        rs.Close()

        sql = "SELECT * FROM ZCCL01 WHERE CCTABL='FLCAPNEG' "
        sql = sql & " AND CCCODE='" & sTipVeh & Ceros(dProve, 8) & "'"
        rs = ExecuteSQL(sql, r)
        If Not rs.EOF Then
            If PesoNeg = 0 Then
                PagarPeso = CDbl(rs.Fields("CCNOT1").Value)
                CapVeh = PagarPeso
            ElseIf Tiquete < PesoNeg Then
                PagarPeso = CDbl(rs.Fields("CCNOT1").Value)
                CapVeh = PagarPeso
            Else
                PagarPeso = Tiquete
            End If
            bExcepcion = True
        Else
            rs.Close()
            sql = "SELECT * FROM ZCCL01 WHERE CCTABL='FLCAPNEG' "
            sql = sql & " AND CCCODE='" & sTipVeh & Ceros(0, 8) & "' AND CCNOT2='" & sTipCarga & "'"
            rs = ExecuteSQL(sql, r)
            If Not rs.EOF Then
                PagarPeso = CDbl(rs.Fields("CCNOT1").Value)
                CapVeh = PagarPeso
                bExcepcion = True
            Else
                rs.Close()
                sql = "SELECT * FROM ZCCL01 WHERE CCTABL='SPTIPVEH' AND CCCODE='" & sTipVeh & "'"
                rs = ExecuteSQL(sql, r)
                aCap = Split(rs.Fields("CCNOT1").Value, "_")
                CapVeh = IIf(CapVeh = 0, CDbl(aCap(0)), CapVeh)
                If PesoNeg = 0 Then
                    PesoNeg = CDbl(aCap(0))
                End If
                PesoQuim = CDbl(aCap(1))
                PesoQuimSeco = CDbl(aCap(2))
            End If
        End If
        rs.Close()
        rs = Nothing

        If bExcepcion Then
            'Toma la capacidad negociada por excepcion
        ElseIf sSegmento = "QUIMI" Then
            Select Case sTipCarga
                Case "QLA", "QLF"
                    PesoNeg = PesoQuim
                    CapVeh = PesoNeg
                Case Else
                    PesoNeg = PesoQuimSeco
                    CapVeh = PesoNeg
            End Select
            If Tiquete < PesoNeg Then
                PagarPeso = PesoNeg
            Else
                PagarPeso = Tiquete
            End If
        Else
            If aTipNeg(0) = "CAP NEG" Then
                PagarPeso = PesoNeg
            ElseIf aTipNeg(0) = "TIQ" And aTipNeg(1) = "TIQ" Then
                PagarPeso = Tiquete
            Else
                If Tiquete < PesoNeg Then
                    PagarPeso = PesoNeg
                Else
                    PagarPeso = Tiquete
                End If
            End If
        End If

    End Function

    Sub PeriodoOCFletes()
        Dim rs As New ADODB.Recordset
        Dim aFecha() As String

        rs.Open("SELECT * FROM ZCCL01 WHERE CCTABL='RFVBVER' AND CCCODE='ADMINFLETES'", DB_DB2)
        aFecha = Split(rs.Fields("CCNOT1").Value, "-")
        FL_Desde = DateSerial(CInt(aFecha(0)), CInt(aFecha(1)), 1)
        FL_Hasta = LastDayMonth(FL_Desde)
        FL_Periodo = Year(FL_Desde) * 100 + Month(FL_Desde)
        FL_ByPass = rs.Fields("CCUDC2").Value
        rs.Close()

    End Sub

    Function SiEsHora(ByVal Proceso As String) As Boolean
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim sql As String = ""
        Dim qUser As String = ""
        Dim aHora() As String
        Dim i As Integer
        Dim FechaActual As Date = Now()
        Dim bResult As Boolean = False

        sql = "SELECT Z.*, DAYOFWEEK( NOW()) AS DOW "
        sql = sql & " FROM ZCCL01 Z WHERE CCTABL='RFPROCES' AND CCCODE='" & Proceso & "'"
        rs.Open(sql, DB_DB2)
        sql = ""
        If rs.Fields("CCUDC2").Value = 1 Then
            sql = "UPDATE ZCCL01 SET CCUDC2=0"
            sql = sql & " WHERE CCTABL='RFPROCES' AND CCCODE='" & Proceso & "'"
        ElseIf rs.Fields("CCUDC1").Value = 1 Then
            sql = ""
            aHora = Split(rs.Fields("CCSDSC").Value, ",")
            sql = ""
            For i = 0 To UBound(aHora)
                ' rs.Fields(0).Value <> Fecha.ToString("yyyyMMddHH") 
                If Hour(FechaActual) = CInt(aHora(i)) Then
                    If rs.Fields("CCALTC").Value <> FechaActual.ToString("yyyyMMddHH") Then
                        sql = "UPDATE ZCCL01 SET CCALTC='" & FechaActual.ToString("yyyyMMddHH") & "' "
                        sql = sql & " WHERE CCTABL='RFPROCES' AND CCCODE='" & Proceso & "'"
                    End If
                End If
            Next
        End If
        If sql <> "" Then
            DB_DB2.Execute(sql)
            bResult = True
        Else
            bResult = False
            rs.Close()
        End If

        SiEsHora = bResult

    End Function

End Module
