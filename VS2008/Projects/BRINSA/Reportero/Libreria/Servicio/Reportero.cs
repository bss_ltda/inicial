﻿using System;
using System.Data;
using System.Text;
using BSS.Brinsa.Libreria.Dao;

namespace BSS.Brinsa.Libreria.Servicio
{
    /// <summary>
    /// Clase BSS.Brinsa.Libreria.Servicio.Reportero
    /// </summary>
    public class Reportero : DataDB2
    {
        /// <summary>
        /// listado precios distribucion
        /// </summary>
        /// <param name="luen">The luen.</param>
        /// <param name="grupo">The grupo.</param>
        /// <param name="linea">The linea.</param>
        /// <param name="empaque">The empaque.</param>
        /// <param name="marca">The marca.</param>
        /// <param name="tipo">The tipo.</param>
        /// <returns>System.Data.DataSet</returns>
        public DataSet ListadoPreciosDistribucion(string luen, string grupo, string linea, string empaque, string marca, string tipo)
        {
            //Valida los parametros para armar la consulta
            StringBuilder consulta = new StringBuilder();
            consulta.Append("SELECT * FROM RFPREDIS WHERE (LGRP <> '--ND') ");

            if (!string.IsNullOrEmpty(luen))
            {
                consulta.AppendFormat("AND (LUEN = '{0}') ", luen);
            }

            if (!string.IsNullOrEmpty(grupo))
            {
                consulta.AppendFormat("AND (LGRP IN ('{0}')) ", grupo.Replace(",", "','"));
            }

            if (!string.IsNullOrEmpty(linea))
            {
                consulta.AppendFormat("AND (LLIN IN ('{0}')) ", linea.Replace(",", "','"));
            }

            if (!string.IsNullOrEmpty(empaque))
            {
                consulta.AppendFormat("AND (LPAQ IN ('{0}')) ", empaque.Replace(",", "','"));
            }

            if (!string.IsNullOrEmpty(marca))
            {
                consulta.AppendFormat("AND (LMAR IN ('{0}')) ", marca.Replace(",", "','"));
            }

            if (!string.IsNullOrEmpty(tipo))
            {
                consulta.AppendFormat("AND (LTYP IN ('{0}')) ", tipo.Replace(",", "','"));
            }

            //Agrega el order by
            consulta.Append("ORDER BY LUEN, LGRP, LLIN, LPAQ, LMAR, LTYP");

            Query = consulta.ToString();
            Comando = BD.GetSqlStringCommand(Query);
            Comando.CommandTimeout = Utilidades.CommandTimeout;

            try
            {
                DataSet ds = BD.ExecuteDataSet(Comando);
                Log.For(this).Info(Query);
                return ds;
            }
            catch (Exception ex)
            {
                Log.For(this).Error(string.Format("Consulta en AS400 ({0})", Query), ex);
                throw;
            }
        }
    }
}