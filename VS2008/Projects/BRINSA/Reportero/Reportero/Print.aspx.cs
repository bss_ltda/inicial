﻿using System;
using System.Data;
using Microsoft.Reporting.WebForms;

namespace BSS.Brinsa.Reportero
{
    public partial class Print : System.Web.UI.Page
    {
        #region Propiedades
        /// <summary>
        /// Obtiene o establece opcion
        /// </summary>
        /// <value></value>
        public int Opcion
        {
            get
            {
                if (string.IsNullOrEmpty(Request["opt"]))
                    return 1;
                else
                    return Convert.ToInt32(Request["opt"]);
            }
        }
        #endregion

        #region Eventos
        /// <summary>
        /// page_ load
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    Cargar();
                }
                catch (Exception ex)
                {
                    uiMensaje.Text = ex.Message;
                }
            }
        }
        #endregion

        #region Metodos
        /// <summary>
        /// cargar
        /// </summary>
        private void Cargar()
        {
            switch (this.Opcion)
            {
                case 1:
                    PrintPrecioDist();
                    break;
                default:
                    uiMensaje.Text = "No se encontro reporte con la opcion";
                    break;
            }
        }

        /// <summary>
        /// print precio dist
        /// </summary>
        private void PrintPrecioDist()
        {
            BSS.Brinsa.Libreria.Servicio.Reportero reporte = new BSS.Brinsa.Libreria.Servicio.Reportero();
            DataSet dsResultado = reporte.ListadoPreciosDistribucion(Request["LUEN"], Request["GRUPO"], Request["LINEA"], Request["EMPAQUE"], Request["MARCA"], Request["TIPO"]);
            CargarReporte(dsResultado, "PrecioDist_RFPREDIS", "ReportePrecioDistribucion.rdlc", "Precios_Distribucion");
        }
        #endregion

        #region Metodos Reporte
        /// <summary>
        /// cargar reporte
        /// </summary>
        /// <param name="dsResultado">The ds resultado.</param>
        /// <param name="nombreDataSource">The nombre data source.</param>
        /// <param name="reporte">The reporte.</param>
        /// <param name="nombreArchivo">The nombre archivo.</param>
        private void CargarReporte(DataSet dsResultado, string nombreDataSource, string reporte, string nombreArchivo)
        {
            ReportDataSource dataSource = new ReportDataSource(nombreDataSource, dsResultado.Tables[0]);

            //limpia si tiene alguna datasource y agregamos el vigente
            visor.LocalReport.ReportPath = reporte;
            visor.LocalReport.DataSources.Clear();
            visor.LocalReport.DataSources.Add(dataSource);
            visor.LocalReport.Refresh();

            //The DeviceInfo settings should be changed based on the reportType
            //http://msdn2.microsoft.com/en-us/library/ms155397.aspx
            string deviceInfo =
            "<DeviceInfo>" +
            "  <OutputFormat>PDF</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.5in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.5in</MarginBottom>" +
            "</DeviceInfo>";

            //Para guardarlo
            string mimeType, encoding, fileNameExtension;
            string[] streams;
            Warning[] warnings;
            byte[] pdfContent = visor.LocalReport.Render("PDF", deviceInfo, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);
            string archivo = string.Format("{0}.{1}", nombreArchivo, fileNameExtension);

            Response.Clear();
            Response.ContentType = mimeType;
            //Response.AddHeader("Content-Type", "application/xls");
            Response.AddHeader("content-disposition", "attachment; filename=" + archivo);
            Response.BinaryWrite(pdfContent);
            Response.End();
        }
        #endregion
    }
}
