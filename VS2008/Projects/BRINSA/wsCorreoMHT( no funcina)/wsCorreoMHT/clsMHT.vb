﻿Public Class clsMHT

    Public IdMsg, Asunto, De, Para, CC, url, local As String
    Public DB As clsConexion
    Private mht As New Chilkat_v9_5_0.ChilkatMht
    Private mailman As New Chilkat_v9_5_0.ChilkatMailMan
    Private email As New Chilkat_v9_5_0.ChilkatEmail

    Function enviaMail() As String
        Dim resultado As String = ""

        mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
        mht.UnlockComponent("SMANRIMHT_dEnsKQ0g3Rue")

        mailman.SmtpHost = DB.getLXParam("SMTPHOST", local)
        mailman.SmtpUsername = DB.getLXParam("SMTPUSERMAIL", local)
        mailman.SmtpPassword = DB.getLXParam("SMTPPASSWORD", local)

        resultado = correoMHT()
        If resultado = "OK" Then
            email.fromName = IdMsg
            email.FromAddress = DB.getLXParam("SMTPUSERMAIL", "CCNOT1") '"admin.logistica@brinsa.com.co"
            Destinatarios("TO", Para)
            If CC.Trim <> "" Then
                Destinatarios("CC", CC)
            End If
            email.subject = Asunto
            If mailman.SendEmail(email) Then
                resultado = "OK"
            Else
                resultado = mailman.LastErrorText
            End If
        End If

        mailman.CloseSmtpConnection()

        Return resultado

    End Function

    Sub Destinatarios(Tipo As String, Correos As String)
        Dim aDest() As String = Split(Correos, ",")
        Dim rs As New ADODB.Recordset
        Dim fSql As String

        For Each dest In aDest

            If InStr(dest, "@") = 0 Then
                If InStr(dest.ToUpper.Trim, "SELECT ") = 0 Then
                    fSql = "SELECT UNOM, UEMAIL, UEMAILBAK FROM RCAU WHERE UUSR = '" & dest.ToUpper.Trim & "'"
                Else
                    fSql = dest.ToUpper.Trim
                End If
                rs = DB.ExecuteSQL(fSql)
                If Not rs.EOF Then
                    Select Case Tipo
                        Case "TO"
                            email.AddTo(rs("UNOM").Value, rs("UEMAILBAK").Value)
                        Case "CC"
                            email.AddCC(rs("UNOM").Value, rs("UEMAILBAK").Value)
                        Case "BCC"
                            email.AddBcc(rs("UNOM").Value, rs("UEMAILBAK").Value)
                    End Select
                End If
                rs.Close()
            Else
                Select Case Tipo
                    Case "TO"
                        email.AddTo("", dest)
                    Case "CC"
                        email.AddCC("", dest)
                    Case "BCC"
                        email.AddBcc("", dest)
                End Select
            End If
        Next

    End Sub

    'correoMHT()	
    Function correoMHT() As String
        Dim emlStr As String

        mht.UseCids = 1
        emlStr = mht.GetEML(url)
        If (emlStr = vbNullString) Then
            Return mht.LastErrorText
        End If
        correoMHT = email.SetFromMimeText(emlStr)
        If (correoMHT <> 1) Then
            Return email.LastErrorText
        End If

        Return "OK"

    End Function

    Sub Inicializa()
        IdMsg = ""
        Asunto = ""
        De = ""
        Para = ""
        CC = ""
        url = ""
        local = ""
    End Sub


End Class
