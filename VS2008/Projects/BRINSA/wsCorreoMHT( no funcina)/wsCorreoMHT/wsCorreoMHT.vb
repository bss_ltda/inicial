﻿Imports System.IO

Public Class wsCorreoMHT

    Private WithEvents tim As New Timers.Timer(1000)

    Private DB As clsConexion

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Agregue el código aquí para iniciar el servicio. Este método debería poner
        ' en movimiento los elementos para que el servicio pueda funcionar.
        tim.Enabled = True
    End Sub

    Protected Overrides Sub OnStop()
        ' Agregue el código aquí para realizar cualquier anulación necesaria para detener el servicio.
    End Sub

    Private Sub tim_Elapsed(sender As Object, e As System.Timers.ElapsedEventArgs) Handles tim.Elapsed

        revisaTareas()

    End Sub

    '<summary>Helper function to attach a debugger to the running service.</summary>
    <Conditional("DEBUG")>
    Shared Sub DebugMode()
        If Not Debugger.IsAttached Then
            Debugger.Launch()
        End If

        Debugger.Break()
    End Sub

    Sub revisaTareas()
        Dim fSql As String
        Dim pgm, param, exe As String
        Dim rs As New ADODB.Recordset
        Dim sts As Integer
        Dim msgT As String
        Dim tLXmsg As String
        Dim Accion As String

        If Not AbreConexion("") Then
            Exit Sub
        End If

        Try
            fSql = " SELECT * FROM  RFTASK WHERE STS1 = 0 "
            DB.OpenRS(rs, fSql)
            Do While Not rs.EOF
                Accion = UCase(rs("TTIPO").Value)
                If rs("FWRKID").Value <> 0 Then
                    If Accion <> "ULTIMO" Then
                        Accion = "PAQUETE"
                    End If
                Else
                    Accion = UCase(rs("TTIPO").Value)
                End If

                Select Case Accion
                    Case UCase("Shell")
                        pgm = rs("TPGM").Value
                        param = rs("TPRM").Value
                        exe = pgm & " " & param
                        sts = 1
                        msgT = "Comando Enviado."
                        tLXmsg = "EJECUTADA"
                        Try
                            Shell(exe, AppWinStyle.Hide, False, -1)
                        Catch ex As Exception
                            sts = -1
                            msgT = ex.Message.Replace("'", "''")
                            tLXmsg = "ERROR"
                        End Try
                        fSql = " UPDATE RFTASK SET "
                        fSql = fSql & " TLXMSG = '" & tLXmsg & "', "
                        fSql = fSql & " STS1 = " & sts & ", "
                        fSql = fSql & " TUPDDAT = NOW() , "
                        fSql = fSql & " TMSG = '" & msgT & "'"
                        fSql = fSql & " WHERE TNUMREG = " & rs("TNUMREG").Value
                        DB.ExecuteSQL(fSql)
                    Case UCase("Ultimo")
                        exe = "ImprimeDocumentos.exe " & rs("FWRKID").Value
                        sts = 1
                        msgT = "Paquete Enviado."
                        tLXmsg = "ENVIADO"
                        fSql = " UPDATE RFTASK SET "
                        fSql = fSql & " TLXMSG = '" & tLXmsg & "', "
                        fSql = fSql & " STS1 = " & sts & ", "
                        fSql = fSql & " TUPDDAT = NOW() , "
                        fSql = fSql & " TMSG = '" & msgT & "'"
                        fSql = fSql & " WHERE TNUMREG = " & rs("TNUMREG").Value
                        DB.ExecuteSQL(fSql)
                        Try
                            Shell(exe, AppWinStyle.Hide, False, -1)
                        Catch ex As Exception
                            msgT = ex.Message.Replace("'", "''")
                            DB.WrtSqlError(exe, msgT)
                        End Try
                    Case UCase("Java")

                End Select
                rs.MoveNext()
            Loop
            rs.Close()
            DB.Close()
        Catch ex As Exception
            sts = -1
            msgT = ex.Message.Replace("'", "''")
            tLXmsg = "ERROR"
            fSql = " UPDATE RFTASK SET "
            fSql = fSql & " TLXMSG = '" & tLXmsg & "', "
            fSql = fSql & " STS1 = " & sts & ", "
            fSql = fSql & " TUPDDAT = NOW() , "
            fSql = fSql & " TMSG = '" & msgT & "'"
            fSql = fSql & " WHERE TNUMREG = " & rs("TNUMREG").Value
            DB.ExecuteSQL(fSql)
        End Try

    End Sub

    Function AbreConexion(ModuloActual As String) As Boolean

        DB = New clsConexion
        DB.gsDatasource = My.Settings.AS400
        DB.gsLib = My.Settings.AMBIENTE
        DB.ModuloActual = ModuloActual
        Return DB.Conexion()

    End Function


End Class
