﻿Imports System
Imports System.Globalization
Imports System.IO
Imports System.Collections
Imports System.Xml.Serialization

Imports System.ComponentModel
Imports System.Xml
Imports System.Net
Imports ConsumoWsColtanques
Imports ConsumoWsColtanques.wsColtanques
Module Module1


    Dim gsProvider As String
    Dim gsDatasource As String
    Dim rs As New ADODB.Recordset
    Dim mp2Param(5) As String
    Dim AS400Param(5) As String
    Dim Resultado As Boolean = True
    Dim i As Integer
    Dim sLib
    Dim gsConnString
    Dim DB_DB2, DB_MP2
    Sub Main()
        CargaPrm()
        DevuelveResultado()
    End Sub
    Function CargaPrm() As Boolean


        gsProvider = "IBMDA400"
        gsDatasource = My.Settings.AS400
        sLib = My.Settings.AMBIENTE

        gsConnString = "Provider=" & gsProvider & ";Data Source=" & gsDatasource & ";"
        gsConnString = gsConnString & "Persist Security Info=False;Default Collection=" & sLib & ";"
        gsConnString = gsConnString & "Password=LXAPL;User ID=APLLX;"

        DB_DB2 = New ADODB.Connection
        DB_MP2 = New ADODB.Connection
        DB_DB2.Open(gsConnString)

        rs.Open(" SELECT * FROM " & sLib & ".ZCC WHERE CCTABL = 'LXLONG'", DB_DB2)
        Do While Not rs.EOF
            Select Case rs("CCCODE").Value.ToString
                Case "LXUSER"
                    AS400Param(0) = rs("CCDESC").Value.ToString
                Case "LXPASS"
                    AS400Param(1) = rs("CCDESC").Value.ToString
            End Select
            rs.MoveNext()
        Loop
        rs.Close()

        For i = 0 To 1
            If AS400Param(i) = "" Then
                Resultado = False
            End If
        Next

        gsConnString = "Provider=" & gsProvider & ";Data Source=" & gsDatasource & ";"
        gsConnString = gsConnString & "Persist Security Info=False;Default Collection=" & sLib & ";"
        gsConnString = gsConnString & "Password=" & AS400Param(1) & ";User ID=" & AS400Param(0) & ";"
        gsConnString = gsConnString & "Force Translate=0;"
        DB_DB2.Close()
        If Resultado Then
            DB_DB2.Open(gsConnString)
        Else
            'Error Conexion
        End If

        Return Resultado

    End Function

    Function DevuelveResultado()

        Dim fSql As String
        Dim xmlstr As String = ""
        Dim sw As New StringWriter()
        Dim writer As New XmlTextWriter(sw)
        Dim flag As Integer = 0
        Dim flag2 As Integer = 0


        fSql = " SELECT DPEDID, DLINEA, DPROD, IDESC, IUMR, CCDESC, IWGHT, RNTUL, RUXTUL, RUXEST, TLOT, -TQTY AS CANT "
        fSql &= " FROM RDCC INNER JOIN IIM ON DPROD = IPROD INNER JOIN ZCCL01 ON CCTABL = 'UNITMEAS' AND CCCODE = IUMR "
        fSql &= " INNER JOIN RIIM ON DPROD = RIPROD "
        fSql &= " INNER JOIN ITH ON DPEDID = TREF AND DPROD = TPROD AND TTYPE = 'B' "
        fSql &= " WHERE  "
        fSql &= " DCONSO = '355101'  "
        fSql &= " AND DPEDID = 934147  "

        rs.Open(fSql, DB_DB2)


        If Not rs.EOF Then
            writer.WriteStartDocument(True)
            writer.Formatting = Formatting.Indented
            writer.Indentation = 2
            writer.WriteStartElement("Reaprovisonamiento")
            writer.WriteStartElement("Usuario")
            writer.WriteString("BRINSA")
            writer.WriteEndElement()
            writer.WriteStartElement("Password")
            writer.WriteString("83023911")
            writer.WriteEndElement()
            writer.WriteStartElement("Pedido")
            writer.WriteString(rs("DPEDID").Value.ToString)
            writer.WriteEndElement()
            writer.WriteStartElement("Fecha")
            writer.WriteString(Date.Now.Date.ToString)
            writer.WriteEndElement()
        End If
        While Not rs.EOF
            writer.WriteStartElement("Linea")
            CrearNodoLine(rs("DLINEA").Value.ToString, rs("DPROD").Value.ToString, rs("IDESC").Value.ToString, rs("IUMR").Value.ToString, rs("CCDESC").Value.ToString, rs("IWGHT").Value.ToString, rs("RNTUL").Value.ToString, rs("RUXTUL").Value.ToString, rs("RUXEST").Value.ToString, rs("TLOT").Value.ToString, rs("CANT").Value.ToString, writer) ' Las veces que se desee Programa line
            writer.WriteEndElement()
            rs.MoveNext()
        End While
        rs.Close()
        writer.WriteEndElement()

        writer.Flush()
        xmlstr = sw.ToString
        writer.Close()
        Dim pDoc As New XmlDocument
        pDoc.LoadXml(xmlstr)
        Dim xml As String = pDoc.InnerXml
        DB_DB2.Close()
        Dim PruebaDoc As New XmlDocument
        Dim prueba = New ServiceReference1.WSDistribucionSoapClient
        Dim pruebaresutlado As DataTable
        pruebaresutlado = prueba.Reaprovisionamiento()


        Return pDoc

    End Function

    Function CrearNodoLine(ByVal linea As String, ByVal CodigoProducto As String, ByVal DescripcionProducto As String, ByVal Unidades As String, ByVal DescripcionUnidades As String, ByVal Peso As String, ByVal NumeroTendidos As String, ByVal UnidadesPorTendido As String, ByVal UnidadesPorEstiba As String, ByVal Lote As String, ByVal Cantidad As String, ByVal writer As XmlTextWriter)

        writer.WriteStartElement("LineaPedido")
        writer.WriteString(linea)
        writer.WriteEndElement()
        writer.WriteString(vbCrLf)
        writer.WriteStartElement("CodigoProducto")
        writer.WriteString(CodigoProducto)
        writer.WriteEndElement()
        writer.WriteString(vbCrLf)
        writer.WriteStartElement("DescripcionProducto")
        writer.WriteString(DescripcionProducto)
        writer.WriteEndElement()
        writer.WriteString(vbCrLf)
        writer.WriteStartElement("Unidades")
        writer.WriteString(Unidades)
        writer.WriteEndElement()
        writer.WriteString(vbCrLf)
        writer.WriteStartElement("DescripcionUnidades")
        writer.WriteString(DescripcionUnidades)
        writer.WriteEndElement()
        writer.WriteString(vbCrLf)
        writer.WriteStartElement("Peso")
        writer.WriteString(Peso)
        writer.WriteEndElement()
        writer.WriteString(vbCrLf)
        writer.WriteStartElement("NumeroTendidos")
        writer.WriteString(NumeroTendidos)
        writer.WriteEndElement()
        writer.WriteString(vbCrLf)
        writer.WriteStartElement("UnidadesPorTendido")
        writer.WriteString(UnidadesPorTendido)
        writer.WriteEndElement()
        writer.WriteString(vbCrLf)
        writer.WriteStartElement("UnidadesPorEstiba")
        writer.WriteString(UnidadesPorEstiba)
        writer.WriteEndElement()
        writer.WriteString(vbCrLf)
        writer.WriteStartElement("Lote")
        writer.WriteString(Lote)
        writer.WriteEndElement()
        writer.WriteString(vbCrLf)
        writer.WriteStartElement("Cantidad")
        writer.WriteString(Cantidad)
        writer.WriteEndElement()
        writer.WriteString(vbCrLf)
        Return 0
    End Function


End Module
