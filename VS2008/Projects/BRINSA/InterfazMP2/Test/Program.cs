﻿using System;
using System.Threading;
using BSS.Brinsa.MP2.Libreria.Servicio;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Procesar datos = new Procesar();
                datos.Iniciar();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine("Oprima una tecla para continuar...");
            Console.ReadLine();
        }

    }
}
