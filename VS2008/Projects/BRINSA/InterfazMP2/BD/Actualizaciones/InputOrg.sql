﻿--Agrega los campos adicionales para manejo de eliminados
ALTER TABLE dbo.inputord
	ADD 
		cantidad INT NULL
		, accion VARCHAR(1) NULL
		, estado INT
	    , seqnum_purreq INT NULL
GO
 