﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'TR' AND name = 'trg7')
	BEGIN
		DROP  Trigger dbo.trg7
	END
GO

CREATE Trigger dbo.trg7 ON ISSREC 
FOR INSERT, UPDATE AS 

	/*insert into */
DECLARE @numero_referencia varchar(25)      
DECLARE @tipoarticulo varchar(10)      
DECLARE @centrocosto varchar(15)      
DECLARE @reason varchar(50)      
DECLARE @tref varchar(10)      
DECLARE @WTRAN varchar(10)      
DECLARE @WCHARGETO varchar(10)      
DECLARE @LINENUMBER INT 
DECLARE @UOM VARCHAR(8)

select @tipoarticulo = itemtype from inserted      
      
IF @tipoarticulo = 'Stock' OR @tipoarticulo = 'Non-stock'       
 SELECT @numero_referencia = itemnum FROM inserted      
ELSE      
 SELECT @numero_referencia = servicecode FROM inserted    

/* Si es una DEVOLUCION se debe devolver al  Centro de Costro de donde salió */
SELECT @reason = reason FROM inserted      

IF @reason = 'Devolución del artículo' 
    SELECT @centrocosto =  fromcostcenter FROM INSERTED 
ELSE 	  
    SELECT @centrocosto = costcenter FROM INSERTED

SELECT @WTRAN = acctcode FROM inserted      
SELECT @WCHARGETO = CHARGETO FROM inserted      

IF ( @WTRAN = 'S1' OR  @WTRAN = 'S2') AND  @WCHARGETO='WorkOrder'
 SELECT @tref = NUMCHARGEDTO  FROM inserted      
ELSE      
 SELECT @tref = ponum from inserted

--********************* Agregado por BSS
-- Trae el numero de la linea, y se agrego al insert linea 56
SET @LINENUMBER = NULL 
SET @UOM = NULL 

SELECT 
	@LINENUMBER = PURREQ.LINENUMBER
	, @UOM = PURREQ.UOM
FROM ISSREC 
INNER JOIN PURREQ ON ISSREC.PONUM = PURREQ.PONUM 
AND ISSREC.SEQNUM = PURREQ.SEQNUM
AND ISSREC.ACCTCODE = PURREQ.ACCTCODE
WHERE (ISSREC.PONUM = @tref)
--********************* FIN BSS
     
INSERT inputmp2 (registro_t) select '|ITH|I|' + user + '|' + ISNULL(CONVERT(varchar(20), issuedate, 2), '*') + '|' +         
 ISNULL( CAST(QTY AS VARCHAR(10)), '*') + '|' + ISNULL(@tref , '*') + '|' + ISNULL(@numero_referencia,'*') + '|' + 

ISNULL(acctcode, '*') + '|' +        
  ISNULL(fromwarehouseid, '*') + '|' + ISNULL(substring(fromlocation,1,5), '*') + '|' + ISNULL(@centrocosto, '*')       
+ '|' + ISNULL(STR(RELEASENUM),'*') + '|' + ISNULL(STR(SEQNUM),'*') + '|' +      
+ ISNULL(STR(RECEIPTNUM),'*') + '|' + ISNULL(reason, '*') + '|' + ISNULL(transtype, '*')  +  '|' +  
ISNULL(towarehouseid, '*') + '|' + ISNULL(substring(tolocation,1,5), '*') + '|' + CAST(ISNULL(@LINENUMBER, 0) AS VARCHAR(20)) + '|' + ISNULL(@UOM, '') + '|' FROM inserted
GO