﻿--Este trigger se elimino y se agrego el codigo al AUDIT_DELETE_PURREQO

DROP TRIGGER dbo.MP2I_RegistrarInputOrd 
GO 

CREATE TRIGGER dbo.MP2I_RegistrarInputOrd 
ON PURREQ
	FOR DELETE 
AS 

	DECLARE 
		  @QTYREQUESTED FLOAT
		, @SEQNUM INT 

	SELECT 
		  @QTYREQUESTED = QTYREQUESTED
		, @SEQNUM = SEQNUM
	FROM 
		DELETED

	--valida si existe para insertar o editar
		UPDATE 
			inputord
		SET 		
			cantidad = @QTYREQUESTED,
			accion = 'D',
			estado = 0
		WHERE 
			(seqnum_purreq = @SEQNUM) 

GO 