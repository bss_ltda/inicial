IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'MP2I_ActualizarEstadoPo')
	BEGIN
		DROP  Procedure  dbo.MP2I_ActualizarEstadoPo
	END

GO

CREATE Procedure dbo.MP2I_ActualizarEstadoPo
(
	@orden varchar(20)
	, @estado varchar(12)
)
AS
/*****************************************************************************
Procedimiento:	MP2I_ActualizarEstadoPo
Descripcion:	Actualiza el estado de la tabla PO
Parametros:		Ninguno
Aplicacion:		Interfaz MP2
Fecha:			2009-04-27
Autor:			Ogonzalez
*****************************************************************************/
SET NOCOUNT ON

UPDATE PO SET STATUS = @estado WHERE (PONUM = @orden)
GO

GRANT EXEC ON dbo.MP2I_ActualizarEstadoPo TO PUBLIC

GO
