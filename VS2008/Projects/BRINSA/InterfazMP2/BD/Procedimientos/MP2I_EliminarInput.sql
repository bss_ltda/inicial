IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'MP2I_EliminarInput')
	BEGIN
		DROP  Procedure  dbo.MP2I_EliminarInput
	END

GO

CREATE Procedure dbo.MP2I_EliminarInput
(
	@numero int
	, @mensaje varchar(36)
)
AS
/*****************************************************************************
Procedimiento:	dbo.MP2I_EliminarInput
Descripcion:	Elimina un registro del input y lo guarda en la backup
Parametros:		Ninguno
Aplicacion:	
Fecha:			2009-04-24
Autor:			Ogonzalez
*****************************************************************************/
SET NOCOUNT ON

INSERT INTO InputMp2Bck 
	(numero_t, fecha_t, registro_t, mensaje_t)
SELECT 
	numero_t, fecha_t, registro_t, @mensaje
FROM 
	inputmp2 
WHERE 
	(numero_t = @numero)

DELETE FROM 
	inputmp2 
WHERE 
	(numero_t = @numero)
GO

GRANT EXEC ON dbo.MP2I_EliminarInput TO PUBLIC

GO
