﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'MP2I_ConsultarInput')
	BEGIN
		DROP  Procedure  dbo.MP2I_ConsultarInput
	END

GO

CREATE Procedure dbo.MP2I_ConsultarInput
AS
/*****************************************************************************
Procedimiento:	MP2I_ConsultarInput
Descripcion:	Trae la informacion del input
Parametros:		Ninguno
Aplicacion:		Interfaz MP2
Fecha:			2009-04-23
Autor:			Ogonzalez
*****************************************************************************/
SET NOCOUNT ON

SELECT
	numero_t AS Numero
	, fecha_t AS Fecha
	, registro_t AS Registro
FROM
	InputMP2 
WHERE 
	(registro_t LIKE '|ITH|%')
ORDER BY 
	2

GO

GRANT EXEC ON dbo.MP2I_ConsultarInput TO PUBLIC
GO