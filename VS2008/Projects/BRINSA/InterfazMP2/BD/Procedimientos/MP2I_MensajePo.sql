IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'MP2I_MensajePo')
	BEGIN
		DROP  Procedure  dbo.MP2I_MensajePo
	END

GO

CREATE Procedure dbo.MP2I_MensajePo
(
	@accion varchar(1)
	, @orden varchar(20)
	, @linea int
	, @seqnum int
)
AS
/*****************************************************************************
Procedimiento:	dbo.MP2I_MensajePo
Descripcion:	Mensaje PO
Parametros:		Ninguno
Aplicacion:		Interfaz MP2
Fecha:			2009-04-27
Autor:			Ogonzalez
*****************************************************************************/
SET NOCOUNT ON

DECLARE 
	@conteo int
	, @mensaje varchar(50)

SET @mensaje = SPACE(0)
SELECT @conteo = COUNT(*) FROM inputord WHERE (ponum_t = @orden)

--valida la accion
IF(@accion = 'D') BEGIN
	SET @mensaje = CASE WHEN (@conteo <= 1) THEN 'Delete' ELSE 'Deletel'END
	DELETE FROM inputord WHERE (ponum_t = @orden) AND (linea_t = @linea)
END

IF(@accion = 'I') BEGIN
	SET @mensaje = CASE WHEN (@conteo = 0) THEN 'Create' ELSE 'Createl'END
	INSERT INTO inputord (ponum_t, linea_t, accion, estado, seqnum_purreq) VALUES (@orden, @linea, 'I', 2, @seqnum)
END

IF(@accion = 'U') BEGIN
	IF(@conteo = 0) BEGIN 
		SET @mensaje = 'Create'
		INSERT INTO inputord (ponum_t, linea_t, accion, estado, seqnum_purreq) VALUES (@orden, @linea, 'U', 2, @seqnum)
	END 
	ELSE BEGIN 
		IF(NOT EXISTS(SELECT * FROM inputord WHERE (ponum_t = @orden) AND (linea_t = @linea))) BEGIN
			SET @mensaje = 'Createl'
			INSERT INTO inputord (ponum_t, linea_t, accion, estado, seqnum_purreq) VALUES (@orden, @linea, 'U', 2, @seqnum )
		END
		ELSE BEGIN
			SET @mensaje = 'Change'
		END
	END 
END

SELECT @mensaje
GO

GRANT EXEC ON dbo.MP2I_MensajePo TO PUBLIC

GO
