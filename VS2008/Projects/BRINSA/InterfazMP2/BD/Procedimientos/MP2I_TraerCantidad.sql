IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'MP2I_TraerCantidad')
	BEGIN
		DROP  Procedure  dbo.MP2I_TraerCantidad
	END

GO

CREATE Procedure dbo.MP2I_TraerCantidad
(
	@orden varchar(50)
	, @release varchar(50)
	, @secuencia varchar(50)
	, @receip varchar(50)
)
AS
/*****************************************************************************
Procedimiento:	dbo.MP2I_TraerCantidad
Descripcion:	Trae la cantidad recibida cuando es Recepcion compra
Parametros:		Ninguno
Aplicacion:		Interfaz MP2
Fecha:			2009-04-23
Autor:			Ogonzalez
*****************************************************************************/
SET NOCOUNT ON

DECLARE
	@cantidad decimal
	
SELECT 
	@cantidad = QTYRECEIVED
FROM 
	PORECEIV
WHERE 
	(PONUM = @orden) 
	AND (RELEASENUM = @release) 
	AND (SEQNUM = @secuencia) 
	AND (RECEIPTNUM = @receip)

SELECT COALESCE(@cantidad, 0)


GO

GRANT EXEC ON dbo.MP2I_TraerCantidad TO PUBLIC
GO