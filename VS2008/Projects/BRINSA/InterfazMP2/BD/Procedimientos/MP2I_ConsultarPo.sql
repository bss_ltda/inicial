IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'MP2I_ConsultarPo')
	BEGIN
		DROP  Procedure  dbo.MP2I_ConsultarPo
	END

GO

CREATE Procedure dbo.MP2I_ConsultarPo
AS
/*****************************************************************************
Procedimiento:	MP2I_ConsultarPo
Descripcion:	Trae la informacion de la orden
Parametros:		Ninguno
Aplicacion:		Interfaz MP2
Fecha:			2009-04-27
Autor:			Ogonzalez
*****************************************************************************/
SET NOCOUNT ON

DECLARE 
	@blancoDefecto varchar(1)
	, @fechaDefecto varchar(8)

SET @blancoDefecto = SPACE(1)
SET @fechaDefecto = '20999999'

SELECT 
	Detalle.PONUM
	, COALESCE(Detalle.QTYREQUESTED, @blancoDefecto) AS QTYREQUESTED
	, COALESCE(Detalle.LINENUMBER, @blancoDefecto) AS LINENUMBER
	, COALESCE(Detalle.UNITCOST, @blancoDefecto) AS UNITCOST
	, COALESCE(Detalle.ITEMNUM, @blancoDefecto) AS ITEMNUM
	, COALESCE(Detalle.ORDERWAREHOUSE, @blancoDefecto) AS ORDERWAREHOUSE
	, COALESCE(Detalle.COSTCENTER, @blancoDefecto) AS COSTCENTER
	, COALESCE(Orden.VENDORID, @blancoDefecto) AS VENDORID
	, COALESCE(Detalle.SITEID, @blancoDefecto) AS SITEID
	, CASE WHEN (Detalle.ITEMTYPE <> 'Service') 
		THEN COALESCE(CONVERT(varchar, Detalle.DUEDATE, 112), @fechaDefecto) 
		ELSE COALESCE(CONVERT(varchar, Detalle.DATEGENERATED, 112), @fechaDefecto) END 
		AS DATESERVICE
	, COALESCE(Detalle.SERVICECODE, @blancoDefecto) AS SERVICECODE
	, COALESCE(Detalle.ITEMTYPE, @blancoDefecto) AS ITEMTYPE
	, CONVERT(VARCHAR(250), Orden.NOTES) AS NOTES
FROM 
	po Orden 
INNER JOIN 
	purreq Detalle 
	ON Orden.PONUM = Detalle.PONUM
WHERE
	(Orden.status = 'Open')
	AND (Detalle.BPCS = 1)
ORDER BY 
	Detalle.PONUM
	, Detalle.LINENUMBER
GO

GRANT EXEC ON dbo.MP2I_ConsultarPo TO PUBLIC
GO