﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'MP2I_ConsultarLog')
	BEGIN
		DROP  Procedure  dbo.MP2I_ConsultarLog
	END

GO

CREATE Procedure dbo.MP2I_ConsultarLog
(
	@sessionId varchar(36)
)
AS
/*****************************************************************************
Procedimiento:	dbo.MP2I_ConsultarLog
Descripcion:	Consulta el log para el mail
Parametros:		Ninguno
Aplicacion:		Interfaz MP2
Fecha:			2009-04-24
Autor:			Ogonzalez
*****************************************************************************/
SET NOCOUNT ON

SELECT 
	numero_t
	, fecha_t
	, registro_t
	, mensaje_t 
FROM 
	inputlog 
WHERE 
	(sessionId = @sessionId)

GO

GRANT EXEC ON dbo.MP2I_ConsultarLog TO PUBLIC

GO
