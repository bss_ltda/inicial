﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'MP2I_ConsultarOrdenEliminada')
	BEGIN
		DROP  Procedure  dbo.MP2I_ConsultarOrdenEliminada
	END

GO

CREATE Procedure dbo.MP2I_ConsultarOrdenEliminada

AS
/*****************************************************************************
Procedimiento:	dbo.MP2I_ConsultarOrdenEliminada
Descripcion:	Consulta las ordenes eliminadas sin procesar
Parametros:		Ninguno
Aplicacion:		Interfaz MP2
Fecha:			2009-09-10
Autor:			Ogonzalez
*****************************************************************************/
SET NOCOUNT ON

SELECT 
	ponum_t AS PONUM
	, linea_t AS LINENUMBER 
FROM 
	inputord 
WHERE 
	(accion = 'D') 
	AND (estado = 0)
GO

GRANT EXEC ON dbo.MP2I_ConsultarOrdenEliminada TO PUBLIC
GO
