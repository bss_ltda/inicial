IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'MP2I_InsertarLog')
	BEGIN
		DROP  Procedure  dbo.MP2I_InsertarLog
	END

GO

CREATE Procedure dbo.MP2I_InsertarLog
(
	@estado varchar(1)
	, @registro varchar(36)
	, @mensaje varchar(300)
	, @sessionId varchar(36)
)
AS
/*****************************************************************************
Procedimiento:	dbo.MP2I_InsertarLog
Descripcion:	Inserta los errores de 400 en la tabla de log
Parametros:		Ninguno
Aplicacion:		Interfaz MP2
Fecha:			2009-04-24
Autor:			Ogonzalez
*****************************************************************************/
SET NOCOUNT ON

INSERT INTO inputlog (estado_t, registro_t, mensaje_t, sessionId) VALUES (@estado, @registro, @mensaje, @sessionId)
GO

GRANT EXEC ON dbo.MP2I_InsertarLog TO PUBLIC

GO
