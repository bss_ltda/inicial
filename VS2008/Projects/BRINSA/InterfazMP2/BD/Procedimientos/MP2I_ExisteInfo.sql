﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'MP2I_ExisteInfo')
	BEGIN
		DROP  Procedure  dbo.MP2I_ExisteInfo
	END

GO

CREATE Procedure dbo.MP2I_ExisteInfo
AS
/*****************************************************************************
Procedimiento:	MP2I_ExisteInfo
Descripcion:	Valida si existe informacion para exportar
Parametros:		Ninguno
Aplicacion:		Interfaz MP2
Fecha:			2009-04-23
Autor:			Ogonzalez
*****************************************************************************/
SET NOCOUNT ON

IF(
	EXISTS(SELECT * FROM InputMP2) 
	OR 
	EXISTS(SELECT * FROM po Orden INNER JOIN purreq Detalle ON Orden.PONUM = Detalle.PONUM WHERE (Orden.status = 'Open') AND (Detalle.BPCS = 1))
	OR 
	EXISTS(SELECT * FROM inputord WHERE (accion = 'D') AND (estado = 0))
)
	SELECT 1 
ELSE 
	SELECT 0
GO

GRANT EXEC ON dbo.MP2I_ExisteInfo TO PUBLIC
GO