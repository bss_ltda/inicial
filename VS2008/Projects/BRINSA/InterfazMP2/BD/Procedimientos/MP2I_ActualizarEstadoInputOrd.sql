﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'MP2I_ActualizarEstadoInputOrd')
	BEGIN
		DROP  Procedure  dbo.MP2I_ActualizarEstadoInputOrd
	END

GO

CREATE Procedure dbo.MP2I_ActualizarEstadoInputOrd
(
	@numero VARCHAR(20)
	, @linea INT 
	, @estado INT
)
AS
/*****************************************************************************
Procedimiento:	MP2I_ActualizarEstadoInputOrd
Descripcion:	Actualiza el estado de la tabla INPUTORD
Parametros:		Ninguno
Aplicacion:		Interfaz MP2
Fecha:			2009-09-10
Autor:			Ogonzalez
*****************************************************************************/
SET NOCOUNT ON

UPDATE
	inputord 
SET 
	estado = @estado 
WHERE 
	(ponum_t = @numero) 
	AND (linea_t = @linea)
GO

GRANT EXEC ON dbo.MP2I_ActualizarEstadoInputOrd TO PUBLIC
GO