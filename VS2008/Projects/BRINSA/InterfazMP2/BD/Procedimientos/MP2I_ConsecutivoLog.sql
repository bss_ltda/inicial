﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'MP2I_ConsecutivoLog')
	BEGIN
		DROP  Procedure  dbo.MP2I_ConsecutivoLog
	END

GO

CREATE Procedure dbo.MP2I_ConsecutivoLog
AS
/*****************************************************************************
Procedimiento:	dbo.MP2I_ConsecutivoLog
Descripcion:	Consulta el consecutivo
Parametros:		Ninguno
Aplicacion:		Interfaz MP2
Fecha:			2009-04-24
Autor:			Ogonzalez
*****************************************************************************/
SET NOCOUNT ON

--SELECT COUNT(DISTINCT sessionId) + 1 FROM inputlog WHERE (NOT sessionId IS NULL)
SELECT MAX(CONVERT(NUMERIC, COALESCE(sessionId, 0))) + 1 FROM inputlog WHERE (NOT sessionId IS NULL)

GO

GRANT EXEC ON dbo.MP2I_ConsecutivoLog TO PUBLIC

GO
