﻿using System;
using System.Collections.Generic;

namespace BSS.Brinsa.MP2.Libreria.Servicio
{
    /// <summary>
    /// Clase BSS.Brinsa.MP2.Libreria.Servicio.Procesar
    /// </summary>
    public class Procesar
    {
        #region Miembros
        /// <summary>
        /// Proceso de la base de SQL Server
        /// </summary>
        private ProcesarSQL procesoSQL;
        /// <summary>
        /// Procesos de la base de DB2
        /// </summary>
        private ProcesarDB2 procesoDB2;
        #endregion

        #region Propiedades
        #endregion

        #region Constructor
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="BSS.Brinsa.MP2.Libreria.Servicio.Procesar"/>
        /// </summary>
        public Procesar()
        {
            procesoSQL = new ProcesarSQL();
            procesoDB2 = new ProcesarDB2();
        }
        #endregion

        #region Metodos
        /// <summary>
        /// inicia el proceso
        /// </summary>
        public void Iniciar()
        {
            try
            {
                //genera el consecutivo
                string sessionId = procesoSQL.ConsecutivoLog();
                
                List<string> resultado = procesoSQL.Generar(sessionId);

                if (resultado == null)
                {
                    Log.For(this).Warn("No devolvio registros");
                    return;
                }

                //recorre e inserta
                foreach (string item in resultado)
                {
                    try
                    {
                        if (item.Length != 0)
                        {
                            procesoDB2.Insertar(item);
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        procesoSQL.InsertarLog(Utilidades.Estado.Error400, item, ex.Message);
                        Log.For(this).Error(string.Format("Item: {0}", item), ex);
                    }
                }

                //Trae el log y lo envia por correo
                procesoSQL.EnviarCorreoLog();
            }
            catch (Exception ex)
            {
                Log.For(this).Error("Iniciar", ex);
            }
        }
        #endregion
    }
}