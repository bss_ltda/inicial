﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using BSS.Brinsa.MP2.Libreria.Dao;
using BSS.Brinsa.MP2.Libreria.Dominio;

namespace BSS.Brinsa.MP2.Libreria.Servicio
{
    /// <summary>
    /// Clase BSS.Brinsa.MP2.Libreria.Servicio.ProcesarSQL (posee todos los metodos utilizados hacia el servidor SQL Server)
    /// </summary>
    public class ProcesarSQL : DataSQL
    {
        #region Miembros
        /// <summary>
        /// Diccionario de datos string
        /// </summary>
        private IDictionary<string, string> dicString;
        /// <summary>
        /// Diccionario de datos numericos
        /// </summary>
        private IDictionary<string, decimal> dicNumerico;
        /// <summary>
        /// Almacena las secuencia de comandos a procesar en el servidor de AS400
        /// </summary>
        private List<string> secuencia;
        /// <summary>
        /// Determina el nombre de la tabla
        /// </summary>
        private string TablaITP;
        /// <summary>
        /// Determina el nombre de la tabla
        /// </summary>
        private string TablaHPP;
        /// <summary>
        /// Clase que maneja los datos del 400
        /// </summary>
        private ProcesarDB2 procesoDB2;

        //private List<string> documentosBloqueados;
        #endregion

        #region Propiedades
        /// <summary>
        /// Obtiene o establece comandos
        /// </summary>
        /// <value></value>
        public List<string> Secuencia
        {
            get { return secuencia; }
            set { secuencia = value; }
        }

        /// <summary>
        /// Obtiene o establece procesar D b2
        /// </summary>
        /// <value></value>
        public ProcesarDB2 ProcesarDB2
        {
            get
            {
                if (procesoDB2 == null)
                    procesoDB2 = new ProcesarDB2();
                return procesoDB2;
            }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="BSS.Brinsa.MP2.Libreria.Servicio.ProcesarSQL"/>
        /// </summary>
        public ProcesarSQL()
        {
            TablaITP = ConfigurationSettings.AppSettings["TablaITP"];
            TablaHPP = ConfigurationSettings.AppSettings["TablaHPP"];
        }
        #endregion

        #region Metodos Publicos
        /// <summary>
        /// generar
        /// </summary>
        /// <returns>
        /// System.Collections.Generic.List&lt;System.String&gt;
        /// </returns>
        public List<string> Generar(string sessionId)
        {
            //Asigna el Id de la session
            this.SessionId = sessionId;

            //Valida si hay informacion para enviar
            //Lo que haya en InputMP2 Orden.status = 'Open' y Detalle.BPCS = 1
            //PROCESO PRINCIPAL
            if (ExisteInfo())
            {
                secuencia = new List<string>();
                ProcesarOrden();
                ProcesarTransaccion();
                ProcesarOrdenesEliminadas();
                return secuencia;
            }
            else
            {
                Log.For(this).Warn("No se encontraron registros para procesar");
            }
            return null;
        }

        /// <summary>
        /// enviar correo log
        /// </summary>
        public void EnviarCorreoLog()
        {
            try
            {
                IList<IDictionary<string, string>> log = ConsultarLog();

                //Valida que exista informacion
                if (log.Count > 0)
                {

                    StringBuilder correo = new StringBuilder();

                    correo.Append("<html><head><title>Informe de Inconsistencias</title></head></body>");
                    correo.Append("<table border=\"1\">");

                    correo.Append("<tr>");
                    correo.Append("<th>Numero</th>");
                    correo.Append("<th>Fecha</th>");
                    correo.Append("<th>Registro</th>");
                    correo.Append("<th>Mensaje</th>");
                    correo.Append("</tr>");

                    for (int i = 0; i < log.Count; i++)
                    {
                        correo.Append("<tr>");
                        correo.AppendFormat("<td>{0}</td>", log[i]["Numero"]);
                        correo.AppendFormat("<td>{0}</td>", log[i]["Fecha"]);
                        correo.AppendFormat("<td>{0}</td>", log[i]["Registro"]);
                        correo.AppendFormat("<td>{0}</td>", log[i]["Mensaje"]);
                        correo.Append("</tr>");
                    }
                    correo.Append("</table>");
                    correo.Append("</body></html>");

                    //Envia el correo
                    string asunto = string.Format("Informe de inconsistencias proceso MP2 (id = {0})", SessionId);
                    Utilidades.EnviarMail(ProcesarDB2.CorreoFrom(), ProcesarDB2.CorreoToCc(true), ProcesarDB2.CorreoToCc(false), asunto, correo.ToString());
                }

            }
            catch (Exception ex)
            {
                string mensaje = string.Format("No se pudo enviar el correo electronico {0}", ex.Message);
                Log.For(this).Warn(mensaje);
                InsertarLog(Utilidades.Estado.DatosInsuficientes, null, mensaje);
            }
        }

        #endregion

        #region Metodos Privados
        #region Metodos Privados de la Orden
        /// <summary>
        /// procesar orden
        /// </summary>
        private void ProcesarOrden()
        {
            //documentosBloqueados = new List<string>();

            //Trae la informacion requerida PO. Detalle de las ordenes en po y purreq 
            //po.ponum=purreq.ponum (Orden.status = 'Open')	AND (Detalle.BPCS = 1)
            List<Campo> lista = TraerInformacionPo();

            foreach (Campo item in lista)
            {
                try
                {
                    //if (!documentosBloqueados.Exists(delegate(string s) { return s == item.Numero; }))

                        ProcesarHPP(item);
                }
                catch (Exception ex)
                {
                    Log.For(this).Error(string.Format("Fallo: {0}={1}", item.Numero, item.Registro), ex);
                }
            }
        }

        /// <summary>
        /// procesar HPP
        /// </summary>
        /// <param name="item">The item.</param>
        private void ProcesarHPP(Campo item)
        {
            string mensaje = string.Empty;
            string temporal = string.Empty;

            string mensajeLineaCodigo = string.Format("{0}-{1}", item.RegistroDetalle[5], item.RegistroDetalle[6]);
            bool acceso = !(item.RegistroDetalle[6].Equals("*") || item.RegistroDetalle[7].Equals("*") || item.RegistroDetalle[8].Equals("*"));
            if (!acceso)
            {
                //documentosBloqueados.Add(item.Numero);
                mensaje = "Datos insuficientes";
                Log.For(this).Warn(mensaje);
                InsertarLog(Utilidades.Estado.DatosInsuficientes, mensajeLineaCodigo, mensaje);
                return;
            }

            //valida que existan los datos en el facility
            bool esServicio = item.RegistroDetalle[15].Equals("Service");
            if (ProcesarDB2.CargarDatosFacility(item.RegistroDetalle[12], esServicio) == null)
            {
                //documentosBloqueados.Add(item.Numero);
                mensaje = "No existen datos de correspondencia del facility";
                Log.For(this).Warn(mensaje);
                InsertarLog(Utilidades.Estado.DatosInsuficientes, mensajeLineaCodigo, mensaje);
                return;
            }

            dicString = new Dictionary<string, string>();
            dicNumerico = new Dictionary<string, decimal>();
            ArmarHPP(item);

            //Valida los impuestos para las ordenes de compra
            if (dicNumerico.ContainsKey("PHVEND"))
            {
                if (!ProcesarDB2.ValidarImpuesto(dicNumerico["PHVEND"], dicString["PPROD"]))
                {
                    //documentosBloqueados.Add(item.Numero);
                    mensaje = string.Format("Error en codigo de impuestos [ZRT] ({0}, {1}, {2})", item.Numero, dicNumerico["PHVEND"], dicString["PPROD"]);
                    Log.For(this).Warn(mensaje);
                    InsertarLog(Utilidades.Estado.DatosInsuficientes, mensajeLineaCodigo, mensaje);
                    return;
                }
            }
            else {
                mensaje = "No se cargo el campo PHVEND";
                Log.For(this).Warn(mensaje);
                InsertarLog(Utilidades.Estado.DatosInsuficientes, mensajeLineaCodigo, mensaje);
                return;
            }

            //Valida la bodega facility
            if (!ProcesarDB2.ValidarBodegaFacility(dicString["PHWHSE"], dicString["PHFAC"]))
            {
                //documentosBloqueados.Add(item.Numero);
                mensaje = string.Format("Error en bodega no pertenece al facility [IWM]({0}, {1}, {2})", item.Numero, dicString["PHWHSE"], dicString["PHFAC"]);
                Log.For(this).Warn(mensaje);
                InsertarLog(Utilidades.Estado.DatosInsuficientes, mensajeLineaCodigo, mensaje);
                return;
            }

            //Valida que exista el producto
            if (!ProcesarDB2.ValidarCampo("TPROD", dicString["PPROD"]))
            {
                //documentosBloqueados.Add(item.Numero);
                mensaje = string.Format("Error al validar TPROD: {0} ", dicString["PPROD"]);
                Log.For(this).Warn(mensaje);
                InsertarLog(Utilidades.Estado.DatosInsuficientes, mensajeLineaCodigo, mensaje);
                return;
            }

            //Si es creacion valida la cantidad de la orden
            if (!dicString["MPACTL"].Contains("Create"))
            {
                int linea = ProcesarDB2.ValidarCantidadOrden(dicNumerico["PHORD"], dicNumerico["PLINE"], dicNumerico["PQORD"]);
                if (linea > 0)
                {
                    //Actualiza
                    dicString["MPACTL"] = "Changel";
                    dicNumerico["PLINE"] = linea;
                }
                else if (linea == 0)
                {
                    //Crea
                    dicString["MPACTL"] = "Createl";
                }
                else
                {
                    mensaje = string.Format("Error en cantidad. Ord:{0} Prod:{1} Line:{2} Cant:{3} Op:{4}", item.Numero, dicString["PPROD"], dicNumerico["PLINE"], dicNumerico["PQORD"], dicString["MPACTL"]);
                    Log.For(this).Warn(mensaje);
                    InsertarLog(Utilidades.Estado.DatosInsuficientes, mensajeLineaCodigo, mensaje);
                    return;
                }
            }

            if (dicNumerico.ContainsKey("PHVEND"))
            {
                if (!ProcesarDB2.EsProveedorActivo(dicNumerico["PHVEND"]))
                {
                    //documentosBloqueados.Add(item.Numero);
                    mensaje = string.Format("Proveedor inactivo ({0}, {1})", item.Numero, dicNumerico["PHVEND"]);
                    Log.For(this).Warn(mensaje);
                    InsertarLog(Utilidades.Estado.DatosInsuficientes, mensajeLineaCodigo, mensaje);
                    return;
                }
            }
            else
            {
                mensaje = "No se cargo el campo PHVEND";
                Log.For(this).Warn(mensaje);
                InsertarLog(Utilidades.Estado.DatosInsuficientes, mensajeLineaCodigo, mensaje);
                return;
            }

            //adiciona la secuencia que va al 400
            secuencia.Add(ArmarSQL(TablaHPP));

            //Si el registro es change se inserta otro registro con el estado Changel
            if (dicString["MPACTL"].Equals("Change"))
            {
                dicString["MPACTL"] = "Changel";
                secuencia.Add(ArmarSQL(TablaHPP));
            }

            //actualiza el estado de la orden
            ActualizarEstadoPo(item.Numero, "Receiving");
        }

        /// <summary>
        /// procesar HPP
        /// </summary>
        /// <param name="item">El/la Item</param>
        private void ProcesarHPP(Orden item)
        {
            //Valida que exista en el HPO
            if (ProcesarDB2.ExisteEnHPO(item.Numero, item.Linea))
            {
                //Genera el insert
                StringBuilder sql = new StringBuilder();
                sql.Append("INSERT INTO HPP500 ");
                sql.Append("(MPDTNV, MPTMNV, MPACTL, PQORD, MPUSR, MPPRID, PHID, PHBUSY, PHORD, PHREVN, PHRVDT, PHFAC, PHWHSE, PHVEND, PHBVND, PHSHTP, PHSHIP, PHNAME, PHATTN, PHADR1, PHADR2, PHADR3, PHSTE, PHZIP, PHCOUN, PHAQDT, PHACDT, PHPRT, PHCMT, PHTERM, PHBUYC, PHFOBC, PHFOBP, PHSVIA, PHSSTS, PHCUR, PHRCRT, PHDEST, PHVTXC, PHEXTC, PHCRCC, PHCRNO, PHSRNO, PHDELT, PHCCNS, PHMNTR, PHCBY, PHLBY, PHLDT, PHLTM, PHLTZ, PID, PLINE, PPROD, PECST, PDDTE, PUM, PPRT, PLDTE, PCMT, PSHIP, PVITM, PBUYC, PSVIA, PWHSE, POCONT, HVDUE, HOUTS, HSHOP, HOPER, PAUTH, PCUST, PCORD, PCLIN, PBPRT, POITXC, POSHTY, POAQDT, POACDT, POFOBC, POFOBP, POSSTS, PODEST, POPRF, PODESC, PONIIT, POPSRC, POFENO, POSRCE, POEXTC, POTRNN, POCBY, POLBY, POLDT, POLTM, PLINBP, PNOTAS) ");
                sql.Append("SELECT ");
                sql.AppendFormat("'{0}' AS MPDTNV, '{1}' AS MPTMNV, 'Deletel' AS MPACTL, 0 AS PQORD, ", Utilidades.ArmarFecha(DateTime.Now), Utilidades.ArmarHora(DateTime.Now));
                sql.Append("MPUSR, MPPRID, PHID, PHBUSY, PHORD, PHREVN, PHRVDT, PHFAC, PHWHSE, PHVEND, PHBVND, PHSHTP, PHSHIP, PHNAME, PHATTN, PHADR1, PHADR2, PHADR3, PHSTE, PHZIP, PHCOUN, PHAQDT, PHACDT, PHPRT, PHCMT, PHTERM, PHBUYC, PHFOBC, PHFOBP, PHSVIA, PHSSTS, PHCUR, PHRCRT, PHDEST, PHVTXC, PHEXTC, PHCRCC, PHCRNO, PHSRNO, PHDELT, PHCCNS, PHMNTR, PHCBY, PHLBY, PHLDT, PHLTM, PHLTZ, PID, PLINE, PPROD, PECST, PDDTE, PUM, PPRT, PLDTE, PCMT, PSHIP, PVITM, PBUYC, PSVIA, PWHSE, POCONT, HVDUE, HOUTS, HSHOP, HOPER, PAUTH, PCUST, PCORD, PCLIN, PBPRT, POITXC, POSHTY, POAQDT, POACDT, POFOBC, POFOBP, POSSTS, PODEST, POPRF, PODESC, PONIIT, POPSRC, POFENO, POSRCE, POEXTC, POTRNN, POCBY, POLBY, POLDT, POLTM, PLINBP, PNOTAS ");
                sql.Append("FROM HPP500 ");
                sql.AppendFormat("WHERE (PHORD = '{0}') AND (PLINE = {1})", item.Numero, item.Linea);
                secuencia.Add(sql.ToString());

                //cambia el estado
                ActualizarEstadoInputOrd(item.Numero, item.Linea);
            }
        }

        /// <summary>
        /// armar HPP
        /// </summary>
        /// <param name="item">The item.</param>
        private void ArmarHPP(Campo item)
        {
            //*****Inserta los de Caracter
            dicString.Add("MPPRID", item.RegistroDetalle[1]);

            //dicString.Add("MPACTL", item.RegistroDetalle[2]);
            dicString.Add("MPACTL", MensajePo(item.RegistroDetalle[2], item.RegistroDetalle[5], item.RegistroDetalle[6], item.RegistroDetalle[16]));

            dicString.Add("MPUSR", item.RegistroDetalle[3]);
            
            if (item.RegistroDetalle[15].Equals("Service"))
            {
                dicString.Add("PPROD", item.RegistroDetalle[14]);
                //Nuevo para el manejo de los COMMODITIES
                dicString.Add("PONIIT", "1");
            }
            else
            {
                dicString.Add("PPROD", item.RegistroDetalle[8]);
                dicString.Add("PONIIT", "0");
            }

            //dicString.Add("PWHSE", item.RegistroDetalle[9]);
            dicString.Add("POPRF", item.RegistroDetalle[10]);

            //Trae los datos del facility
            bool esServicio = item.RegistroDetalle[15].Equals("Service");
            IDictionary<string, string> datosFacility = ProcesarDB2.CargarDatosFacility(item.RegistroDetalle[12], esServicio);

            dicString.Add("PHFAC", datosFacility["PHFAC"]);

            dicString.Add("PHWHSE", datosFacility["PHWHSE"]);
            dicString.Add("PWHSE", datosFacility["PHWHSE"]);
            dicString.Add("PHSHIP", datosFacility["PHSHIP"]);
            dicString.Add("PHBUYC", datosFacility["PHBUYC"]);

            //if (item.RegistroDetalle[12].Equals("10"))
            //{
            //    dicString.Add("PHWHSE", "AG");
            //    dicString.Add("PHSHIP", "0001");
            //    dicString.Add("PHBUYC", "D");
            //}
            //else
            //{
            //    dicString.Add("PHWHSE", "AC");
            //    dicString.Add("PHSHIP", "0003");
            //    dicString.Add("PHBUYC", "S");
            //}

            dicString.Add("PHSHTP", "0");

            dicString.Add("PNOTAS", item.Notas);

            //*****Inserta los tipo Numerico
            decimal valor;

            dicNumerico.Add("MPTMNV", Convert.ToDecimal(Utilidades.ArmarHora(item.Fecha)));
            dicNumerico.Add("MPDTNV", Convert.ToDecimal(Utilidades.ArmarFecha(item.Fecha)));

            if (Decimal.TryParse(item.RegistroDetalle[4], out valor))
                dicNumerico.Add("PQORD", valor);

            if (Decimal.TryParse(item.RegistroDetalle[5], out valor))
                dicNumerico.Add("PHORD", valor);

            if (Decimal.TryParse(item.RegistroDetalle[6], out valor))
            {
                dicNumerico.Add("PLINE", valor);
                dicString.Add("PODEST", valor.ToString());
            }

            if (Decimal.TryParse(item.RegistroDetalle[7], out valor))
                dicNumerico.Add("PECST", valor);

            if (Decimal.TryParse(item.RegistroDetalle[11], out valor))
                dicNumerico.Add("PHVEND", valor);

            if (Decimal.TryParse(Utilidades.ArmarFecha(item.RegistroDetalle[13]), out valor))
                dicNumerico.Add("PDDTE", valor);
        }

        /// <summary>
        /// procesar ordenes eliminadas
        /// </summary>
        private void ProcesarOrdenesEliminadas()
        {
            List<Orden> lista = TraerOrdenesEliminadas();

            foreach (Orden item in lista)
            {
                try
                {
                    ProcesarHPP(item);
                }
                catch (Exception ex)
                {
                    Log.For(this).Error(string.Format("Fallo Ordenes eliminadas: {0}={1}", item.Numero, item.Linea), ex);
                }
            }
        }
        #endregion

        #region Metodos Privados de la Transaccion
        /// <summary>
        /// procesar la informacion de la tabla de transacciones InputMP2
        /// </summary>
        private void ProcesarTransaccion()
        {
            //Trae la informacion requerida InputMP2
            List<Campo> lista = TraerInformacionInput();

            foreach (Campo item in lista)
            {
                try
                {
                    //if (item.RegistroDetalle[1].Equals("ITH"))
                    //{
                    ProcesarITH(item);
                    //}
                }
                catch (Exception ex)
                {
                    Log.For(this).Error(string.Format("Fallo: {0}={1}", item.Numero, item.Registro), ex);
                }
            }
        }

        /// <summary>
        /// procesar ITH
        /// </summary>
        /// <param name="item">The item.</param>
        private void ProcesarITH(Campo item)
        {
            string mensaje = string.Empty;
            string temporal = string.Empty;

            bool acceso = false;
            if (item.RegistroDetalle[8].Equals("S1"))
                acceso = !(item.RegistroDetalle[7].Equals("*") || item.RegistroDetalle[8].Equals("*"));
            else
                acceso = !(item.RegistroDetalle[6].Equals("*") || item.RegistroDetalle[7].Equals("*") || item.RegistroDetalle[8].Equals("*"));

            if (!acceso)
            {
                mensaje = string.Format("Datos insuficientes: 6:{0}, 7:{1}, 8:{2}", item.RegistroDetalle[6], item.RegistroDetalle[7], item.RegistroDetalle[8]);
                Log.For(this).Warn(mensaje);
                InsertarLog(Utilidades.Estado.DatosInsuficientes, item.Registro, mensaje);
                return;
            }

            //Validar que los Productos que son Commodities no pasen a BPCS a movimiento de inventario porque en BPCS se autoreciben.
            acceso = (Convert.ToDecimal(item.RegistroDetalle[7]) <= 90000000);
            if (!acceso)
            {
                mensaje = "Producto Commodities " + item.RegistroDetalle[7];
                Log.For(this).Warn(mensaje);
                InsertarLog(Utilidades.Estado.DatosInsuficientes, item.Registro, mensaje);
                return;
            }

            //Valida los campos
            if (!ProcesarDB2.ValidarCampo("TPROD", item.RegistroDetalle[7]))
            {
                mensaje = "Error al validar TPROD " + item.RegistroDetalle[7];
                InsertarLog(Utilidades.Estado.DatosInsuficientes, item.Registro, mensaje);
                Log.For(this).Warn(mensaje);
                return;
            }

            temporal = ProcesarDB2.ValidarCodigoTransaccion(item.RegistroDetalle[8]);
            if (string.IsNullOrEmpty(temporal))
            {
                mensaje = "Error al validar TTYPE " + item.RegistroDetalle[8];
                InsertarLog(Utilidades.Estado.DatosInsuficientes, item.Registro, mensaje);
                Log.For(this).Warn(mensaje);
                return;
            }
            else
            {
                item.RegistroDetalle[8] = temporal;
            }

            if (!ProcesarDB2.ValidarCampo("TWHS", item.RegistroDetalle[9]))
            {
                mensaje = "Error al validar TWHS " + item.RegistroDetalle[9];
                InsertarLog(Utilidades.Estado.DatosInsuficientes, item.Registro, mensaje);
                Log.For(this).Warn(mensaje);
                return;
            }

            temporal = ProcesarDB2.ValidarTipoTransaccion(item.RegistroDetalle[8], item.RegistroDetalle[6], item.RegistroDetalle[19], item.RegistroDetalle[20]);
            if (!string.IsNullOrEmpty(temporal))
            {
                mensaje = string.Format("{0} Tran:{1} Ord:{2} Lin:{3} Un:{4} ", temporal, item.RegistroDetalle[8], item.RegistroDetalle[6], item.RegistroDetalle[19], item.RegistroDetalle[20]);
                InsertarLog(Utilidades.Estado.DatosInsuficientes, item.Registro, mensaje);
                Log.For(this).Warn(mensaje);
                return;
            }

            //if (!ProcesarDB2.ValidarCampo("TREF", item.RegistroDetalle[6]))
            //{
            //    mensaje = "Error al validar TREF";
            //    InsertarLog(Utilidades.Estado.DatosInsuficientes, item.Registro, mensaje);
            //    Log.For(this).Warn(mensaje);
            //    return;
            //}

            //Si es de tipo T se debe insertar 2 registros uno positivo y el otro negativo
            //Transferencia entre almacenes
            if (item.RegistroDetalle[8].Equals("T"))
            {
                //guarda la cantidad temporalmente
                string tmpCantidad = item.RegistroDetalle[5];

                //guarda el registro negativo
                item.RegistroDetalle[8] = "T1";
                item.RegistroDetalle[5] = "-" + tmpCantidad;

                //Inicia el armado del insert que va al as400
                dicString = new Dictionary<string, string>();
                dicNumerico = new Dictionary<string, decimal>();
                ArmarITH(item);

                //Agrega la secuencia
                temporal = ArmarSQL(TablaITP);
                secuencia.Add(temporal);

                //guarda el registro positivo
                item.RegistroDetalle[9] = item.RegistroDetalle[17];
                item.RegistroDetalle[10] = item.RegistroDetalle[18];
                item.RegistroDetalle[5] = tmpCantidad;

                dicString = new Dictionary<string, string>();
                dicNumerico = new Dictionary<string, decimal>();
                ArmarITH(item);

                //Agrega la secuencia
                temporal = ArmarSQL(TablaITP);
                secuencia.Add(temporal);
            }
            else
            {
                //Inicia el armado del insert que va al as400
                dicString = new Dictionary<string, string>();
                dicNumerico = new Dictionary<string, decimal>();
                ArmarITH(item);

                //Agrega la secuencia
                temporal = ArmarSQL(TablaITP);
                secuencia.Add(temporal);
            }
            //Elimina del log
            EliminarLogInput(item.Numero, string.Empty);
        }

        /// <summary>
        /// armar ITH
        /// </summary>
        /// <param name="item">el/la item</param>
        private void ArmarITH(Campo item)
        {
            //Inserta los de Caracter
            dicString.Add("MPPRID", item.RegistroDetalle[1]);
            dicString.Add("MPACTL", "Post");
            //dicString.Add("MPUSR", item.RegistroDetalle[3]);
            dicString.Add("MPUSR", "MP2");
            dicString.Add("TPROD", item.RegistroDetalle[7]);

            //dicString.Add("TTYPE", item.RegistroDetalle[8]);
            dicString.Add("TTYPE", ProcesarDB2.ValidarCodigoTransaccion(item.RegistroDetalle[8]));
            dicString.Add("TWHS", item.RegistroDetalle[9]);
            dicString.Add("TLOCT", item.RegistroDetalle[10]);
            dicString.Add("TCOM", item.RegistroDetalle[11]);
            if ((item.RegistroDetalle[11].Substring(1).Equals("A")) || (item.RegistroDetalle[11].Substring(1).Equals("D")))
                dicString.Add("TRES", "02");
            else
                dicString.Add("TRES", "01");
            dicString.Add("NOTES", item.Notas);
            dicString.Add("THTUM", item.RegistroDetalle[20]);

            string consecutivo = string.Format("{0}{1}", "0".PadRight(10 - item.Numero.Length, '0'), item.Numero);
            dicString.Add("THADVN", consecutivo);

            //Inserta los tipo Numerico
            decimal valor;

            dicNumerico.Add("MPTMNV", Convert.ToDecimal(Utilidades.ArmarHora(item.Fecha)));
            dicNumerico.Add("MPDTNV", Convert.ToDecimal(Utilidades.ArmarFecha(item.Fecha)));

            if (Decimal.TryParse(Utilidades.ArmarFecha(item.RegistroDetalle[4]), out valor))
                dicNumerico.Add("TTDTE", valor);

            if (Decimal.TryParse(item.RegistroDetalle[5], out valor))
            {
                if ((item.RegistroDetalle[16].Equals("RV")) || (item.RegistroDetalle[16].Equals("IR")))
                {
                    dicNumerico.Add("TQTY", valor * -1);
                }
                else
                {
                    if (item.RegistroDetalle[16].Equals("Recepcion compra"))
                        dicNumerico.Add("TQTY", TraerCantidad(item.RegistroDetalle[6], item.RegistroDetalle[12], item.RegistroDetalle[13], item.RegistroDetalle[14]));
                    else
                        dicNumerico.Add("TQTY", valor);
                }
            }

            valor = 0;
            if (Decimal.TryParse(item.RegistroDetalle[6], out valor))
                dicNumerico.Add("TREF", valor);

            //Valida si la transaccion afecta una orden de compra, trayendo la linea sino viene en null
            string numeroLinea = ProcesarDB2.TransaccionAfectaOrden(item.RegistroDetalle[8], item.RegistroDetalle[6], item.RegistroDetalle[19]);
            if (!string.IsNullOrEmpty(numeroLinea))
            {
                valor = 0;
                if (Decimal.TryParse(numeroLinea, out valor))
                    dicNumerico.Add("THLIN", valor);
            }
            else
            {
                valor = 0;
                if (Decimal.TryParse(item.RegistroDetalle[19], out valor))
                    dicNumerico.Add("THLIN", valor);
            }
        }
        #endregion

        #region Metodo Generico
        /// <summary>
        /// arma el query de inserccion con el diccionario
        /// </summary>
        /// <param name="tabla">The tabla.</param>
        /// <returns>System.String</returns>
        private string ArmarSQL(string tabla)
        {
            if ((dicString.Count == 0) && (dicNumerico.Count == 0))
                return string.Empty;

            StringBuilder sql1 = new StringBuilder();
            StringBuilder sql2 = new StringBuilder();

            sql1.AppendFormat("INSERT INTO {0} (", tabla);

            //Itera los string
            foreach (KeyValuePair<string, string> item in dicString)
            {
                //no inserta asteriscos
                if (!string.IsNullOrEmpty(item.Value))
                {
                    if (!item.Value.Equals("*"))
                    {
                        sql1.AppendFormat("{0}, ", item.Key);
                        sql2.AppendFormat("'{0}', ", item.Value);
                    }
                }
            }

            //itera los numericos
            foreach (KeyValuePair<string, decimal> item in dicNumerico)
            {
                sql1.AppendFormat("{0}, ", item.Key);
                sql2.AppendFormat("{0}, ", item.Value);
            }
            sql1.Append(")");
            sql2.Append(")");

            //une las columnas con sus valores
            string sql = string.Format("{0} VALUES ({1}", sql1.ToString(), sql2.ToString());

            //quita las comas sobrantes
            sql = sql.Replace(", )", ")");

            return sql;
        }
        #endregion
        #endregion
    }
}