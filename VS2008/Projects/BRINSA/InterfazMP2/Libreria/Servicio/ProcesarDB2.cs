﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using BSS.Brinsa.MP2.Libreria.Dao;
using IBM.Data.DB2.iSeries;

namespace BSS.Brinsa.MP2.Libreria.Servicio
{
    /// <summary>
    /// Clase BSS.Brinsa.MP2.Libreria.Servicio.ProcesarDB2 (posee todos los metodos utilizados hacia el servidor DB2)
    /// </summary>
    public class ProcesarDB2 : DataDB2
    {
        #region Miembros
        #endregion

        #region Propiedades
        #endregion

        #region Constructor
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="BSS.Brinsa.MP2.Libreria.Servicio.ProcesarDB2"/>
        /// </summary>
        public ProcesarDB2() { }
        #endregion

        #region Metodos
        /// <summary>
        /// inserta el query o los query de inserccion
        /// </summary>
        /// <param name="sql">The SQL.</param>
        /// <returns>System.Boolean</returns>
        public bool Insertar(string sql)
        {
            Query = sql;
            Comando = BD.GetSqlStringCommand(Query);
            Comando.CommandTimeout = Utilidades.CommandTimeout;

            try
            {
                Registro = BD.ExecuteNonQuery(Comando);
                Log.For(this).Info(Query);
                return true;
            }
            catch (Exception ex)
            {
                Log.For(this).Error(string.Format("Insertando en AS400 ({0})", sql), ex);
                throw;
            }
        }

        /// <summary>
        /// validar campo
        /// </summary>
        /// <param name="campo">el/la campo</param>
        /// <param name="valor">el/la valor</param>
        /// <returns>System.Boolean</returns>
        public bool ValidarCampo(string campo, string valor)
        {
            decimal resultado;
            Query = string.Empty;
            switch (campo)
            {
                case "TREF":
                    if (!decimal.TryParse(valor, out resultado))
                        return false;
                    Query = string.Format("SELECT COUNT(*) FROM HPH WHERE PHORD = {0}", valor);
                    break;
                case "TPROD":
                    Query = string.Format("SELECT COUNT(*) FROM IIML01 WHERE IPROD = '{0}'", valor);
                    break;
                //case "TTYPE":
                //    Query = string.Format("SELECT COUNT(*) FROM ITE WHERE TTYPE = '{0}'", valor);
                //    break;
                case "TWHS":
                    Query = string.Format("SELECT COUNT(*) FROM IWML01 WHERE LWHS = '{0}'", valor);
                    break;
            }

            if (Query.Length == 0)
                return false;

            Comando = BD.GetSqlStringCommand(Query);
            Comando.CommandTimeout = Utilidades.CommandTimeout;

            try
            {
                Registro = (int)BD.ExecuteScalar(Comando);
                if (Registro > 0)
                    return true;
            }
            catch (Exception ex)
            {
                Log.For(this).Error(string.Format("Validando campo: {0}", campo), ex);
            }
            return false;
        }

        /// <summary>
        /// validar tipoTrasaccion transaccion
        /// </summary>
        /// <param name="tipoTrasaccion">el/la tipoTrasaccion</param>
        /// <returns>System.String</returns>
        public string ValidarCodigoTransaccion(string tipoTrasaccion)
        {
            Query = string.Format("SELECT SUBSTR(CCDESC, 1, 2) AS DESCRIPCION FROM ZCC WHERE (CCTABL = 'RTRAMP2') AND (NOT CCNOT1 = 'N') AND (CCCODE = '{0}')", tipoTrasaccion);
            Comando = BD.GetSqlStringCommand(Query);
            Comando.CommandTimeout = Utilidades.CommandTimeout;

            try
            {
                object resultado = BD.ExecuteScalar(Comando);
                if (resultado != null)
                    return resultado.ToString();
                return null;
            }
            catch (Exception ex)
            {
                Log.For(this).Error(string.Format("Validando codigo de la transaccion en AS400 ({0})", Query), ex);
                throw;
            }
        }

        /// <summary>
        /// es transaccion tipo
        /// </summary>
        /// <param name="campo">el/la campo</param>
        /// <param name="tipoTransaccion">el/la tipo transaccion</param>
        /// <returns>System.Boolean</returns>
        public bool EsTransaccionTipo(string campo, string tipoTransaccion)
        {
            Query = string.Empty;
            switch (tipoTransaccion)
            {
                case "TPO":
                    Query = string.Format("SELECT COUNT(*) FROM ITE WHERE (TPO = 'Y') AND (TTYPE = '{0}')", campo);
                    break;
                case "TSO":
                    Query = string.Format("SELECT COUNT(*) FROM ITE WHERE (TSO = 'Y') AND (TTYPE = '{0}')", campo);
                    break;
                case "TUM":
                    Query = string.Format("SELECT COUNT(*) FROM ITE WHERE (TUM = 'Y') AND (TTYPE = '{0}')", campo);
                    break;
                case "TRO":
                    Query = string.Format("SELECT COUNT(*) FROM ITE WHERE (TRO = 'Y') AND (TTYPE = '{0}')", campo);
                    break;
            }

            if (Query.Length == 0)
                return false;

            Comando = BD.GetSqlStringCommand(Query);
            Comando.CommandTimeout = Utilidades.CommandTimeout;

            try
            {
                Registro = (int)BD.ExecuteScalar(Comando);
                if (Registro > 0)
                    return true;
            }
            catch (Exception ex)
            {
                Log.For(this).Error(string.Format("Validando campo: {0}", campo), ex);
            }
            return false;
        }

        /// <summary>
        /// validar tipo transaccion
        /// </summary>
        /// <param name="tipoTransaccion">el/la tipo transaccion</param>
        /// <param name="orden">el/la orden</param>
        /// <param name="linea">el/la linea</param>
        /// <param name="und">el/la und</param>
        /// <returns>System.String</returns>
        public string ValidarTipoTransaccion(string tipoTransaccion, string orden, string linea, string und)
        {
            if (EsTransaccionTipo(tipoTransaccion, "TPO"))
            {
                Query = string.Format("SELECT COUNT(*) FROM HPO WHERE (PORD = {0}) AND (PODEST = '{1}')", orden, linea);
                if (!ExisteDato(Query))
                    return "No se encontro Orden de Compra";
            }

            if (EsTransaccionTipo(tipoTransaccion, "TSO"))
            {
                Query = string.Format("SELECT COUNT(*) FROM FSO WHERE (SORD = {0})", orden);
                if (!ExisteDato(Query))
                    return "No se encontro Orden de Fabricacion";
            }

            if (EsTransaccionTipo(tipoTransaccion, "TUM"))
            {
                Query = string.Format("SELECT COUNT(*) FROM ZCC WHERE (CCTABL = 'UNITMEAS') AND (CCCODE = '{0}')", und);
                if (!ExisteDato(Query))
                    return "No se encontro Unidad de Medida";
            }

            if (EsTransaccionTipo(tipoTransaccion, "TRO"))
            {
                Query = string.Format("SELECT COUNT(*) FROM ECH WHERE (HORD = {0})", orden);
                if (!ExisteDato(Query))
                    return "No se encontro Pedido";
            }

            return string.Empty;
        }

        /// <summary>
        /// validar impuesto
        /// </summary>
        /// <param name="proveedor">el/la proveedor</param>
        /// <param name="producto">el/la producto</param>
        /// <returns>System.Boolean</returns>
        public bool ValidarImpuesto(decimal proveedor, string producto)
        {
            Query = string.Format("SELECT COUNT(*) FROM ZRT INNER JOIN AVM ON AVM.VTAXCD = ZRT.RTCVCD INNER JOIN IIM ON IIM.TAXC1 = ZRT.RTICDE WHERE (AVM.VENDOR = {0}) AND (IIM.IPROD = '{1}')", proveedor, producto);
            Comando = BD.GetSqlStringCommand(Query);
            Comando.CommandTimeout = Utilidades.CommandTimeout;

            try
            {
                Registro = (int)BD.ExecuteScalar(Comando);

                if (Registro != 0)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                Log.For(this).Error(string.Format("Validando Impuestos en AS400 ({0})", Query), ex);
                throw;
            }
        }

        /// <summary>
        /// validar bodega facility
        /// </summary>
        /// <param name="bodega">el/la bodega</param>
        /// <param name="facility">el/la facility</param>
        /// <returns>System.Boolean</returns>
        public bool ValidarBodegaFacility(string bodega, string facility)
        {
            Query = string.Format("SELECT COUNT(*) FROM IWM WHERE (LWHS = '{0}') AND (WMFAC = '{1}')", bodega, facility);
            Comando = BD.GetSqlStringCommand(Query);
            Comando.CommandTimeout = Utilidades.CommandTimeout;

            try
            {
                Registro = (int)BD.ExecuteScalar(Comando);

                if (Registro != 0)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                Log.For(this).Error(string.Format("Validando bodega facility en AS400 ({0})", Query), ex);
                throw;
            }
        }

        /// <summary>
        /// validar cantidad orden
        /// </summary>
        /// <param name="orden">el/la orden</param>
        /// <param name="linea">el/la linea</param>
        /// <param name="cantidad">el/la cantidad</param>
        /// <returns>System.Boolean</returns>
        public int ValidarCantidadOrden(decimal orden, decimal linea, decimal cantidad)
        {
            int resultado = 0;
            Query = string.Format("SELECT * FROM HPO WHERE (PORD = {0}) AND (PODEST = '{1}')", orden, linea);
            Comando = BD.GetSqlStringCommand(Query);
            Comando.CommandTimeout = Utilidades.CommandTimeout;

            try
            {
                using (iDB2DataReader dataReader = BD.ExecuteReader(Comando))
                {
                    while (dataReader.Read())
                    {

                        if ((Convert.ToDecimal(dataReader["PQREC"]) <= cantidad) && (Convert.ToDecimal(dataReader["PQORD"]) != cantidad))
                            resultado = Convert.ToInt32(dataReader["PLINE"]);
                        else
                            resultado = -1;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.For(this).Error(string.Format("Cargando correspondencia del facility en AS400 ({0})", Query), ex);
                return 0;
            }

            return resultado;
        }

        /// <summary>
        /// existe dato
        /// </summary>
        /// <param name="query">el/la query</param>
        /// <returns>System.Boolean</returns>
        public bool ExisteDato(string query)
        {
            Query = query;
            Comando = BD.GetSqlStringCommand(Query);
            Comando.CommandTimeout = Utilidades.CommandTimeout;

            try
            {
                Registro = (int)BD.ExecuteScalar(Comando);

                if (Registro != 0)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                Log.For(this).Error(string.Format("Validando si existe el query en AS400 ({0})", Query), ex);
                throw;
            }
        }

        /// <summary>
        /// cargar datos facility
        /// </summary>
        /// <param name="facility">el/la facility</param>
        /// <param name="esServicio">el/la es servicio</param>
        /// <returns>
        /// System.Collections.Generic.IDictionary&lt;System.String,System.String&gt;
        /// </returns>
        public IDictionary<string, string> CargarDatosFacility(string facility, bool esServicio)
        {
            if (esServicio)
                Query = string.Format("SELECT CCDESC, CCSDSC, CCNOT1, CCNOT2 FROM ZCC WHERE (CCTABL = 'RMP2BPCS') AND (CCALTC = 'S') AND (CCCODE = '{0}')", facility);
            else
                Query = string.Format("SELECT CCDESC, CCSDSC, CCNOT1, CCNOT2 FROM ZCC WHERE (CCTABL = 'RMP2BPCS') AND (CCALTC = '') AND (CCCODE = '{0}')", facility);

            Comando = BD.GetSqlStringCommand(Query);
            Comando.CommandTimeout = Utilidades.CommandTimeout;

            IDictionary<string, string> resultado = new Dictionary<string, string>();

            try
            {
                using (iDB2DataReader dataReader = BD.ExecuteReader(Comando))
                {
                    while (dataReader.Read())
                    {
                        resultado.Add("PHFAC", dataReader["CCDESC"].ToString());
                        resultado.Add("PHWHSE", dataReader["CCSDSC"].ToString());
                        resultado.Add("PHSHIP", dataReader["CCNOT1"].ToString());
                        resultado.Add("PHBUYC", dataReader["CCNOT2"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Log.For(this).Error(string.Format("Cargando correspondencia del facility en AS400 ({0})", Query), ex);
                return null;
            }

            if (resultado.Count == 0)
                return null;
            return resultado;
        }

        /// <summary>
        /// es proveedor activo
        /// </summary>
        /// <param name="proveedor">el/la proveedor</param>
        /// <returns>System.Boolean</returns>
        public bool EsProveedorActivo(decimal proveedor)
        {
            Query = string.Format("SELECT COUNT(*) FROM AVM WHERE (VENDOR = {0}) AND (NOT VHOLD = 'Y')", proveedor);
            return ExisteDato(Query);
        }

        /// <summary>
        /// transaccion afecta orden
        /// </summary>
        /// <param name="tipoTransaccion">El/la Tipo transaccion</param>
        /// <param name="orden">El/la Orden</param>
        /// <param name="linea">El/la Linea</param>
        /// <returns>System.String</returns>
        public string TransaccionAfectaOrden(string tipoTransaccion, string orden, string linea)
        {
            if (EsTransaccionTipo(tipoTransaccion, "TPO"))
            {
                Query = string.Format("SELECT PLINE FROM HPO WHERE (PORD = {0}) AND (PODEST = '{1}')", orden, linea);

                Comando = BD.GetSqlStringCommand(Query);
                Comando.CommandTimeout = Utilidades.CommandTimeout;

                try
                {
                    object resultado = BD.ExecuteScalar(Comando);
                    if (resultado != null)
                        return resultado.ToString();
                    return null;
                }
                catch (Exception ex)
                {
                    Log.For(this).Error(string.Format("Buscando la linea real de la transaccion: {0}, {1}", orden, linea), ex);
                    throw;
                }
            }
            return null;
        }

        /// <summary>
        /// correo from
        /// </summary>
        /// <returns>System.Net.Mail.MailAddress</returns>
        public MailAddress CorreoFrom()
        { 
            MailAddress resultado = null;

            Query = "SELECT LMAIL, LNOMBRE FROM RMAILL WHERE (RMAILL.LTDEST = 'FROM') AND (LAPP = 'MP2') AND (LMOD = 'INTERFACE') AND (LFUNC = 'ALERTAS')";

            Comando = BD.GetSqlStringCommand(Query);
            Comando.CommandTimeout = Utilidades.CommandTimeout;

            try
            {
                using (iDB2DataReader dataReader = BD.ExecuteReader(Comando))
                {
                    while (dataReader.Read())
                    {
                        resultado = new MailAddress(dataReader["LMAIL"].ToString(), dataReader["LNOMBRE"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Log.For(this).Error(string.Format("No se encontro el correo FROM en AS400 ({0})", Query), ex);
            }
            return resultado;
        }

        /// <summary>
        /// correo to cc
        /// </summary>
        /// <param name="esPara">si es <c>true</c> [es para]. sino con copia</param>
        /// <returns>
        /// System.Collections.Generic.IList&lt;System.Net.Mail.MailAddress&gt;
        /// </returns>
        public IList<MailAddress> CorreoToCc(bool esPara)
        {
            IList<MailAddress> resultado = new List<MailAddress>();

            if (esPara)
                Query = "SELECT LMAIL, LNOMBRE FROM RMAILL WHERE (RMAILL.LTDEST = 'TO') AND (LAPP = 'MP2') AND (LMOD = 'INTERFACE') AND (LFUNC = 'ALERTAS')";
            else
                Query = "SELECT LMAIL, LNOMBRE FROM RMAILL WHERE (RMAILL.LTDEST = 'CC') AND (LAPP = 'MP2') AND (LMOD = 'INTERFACE') AND (LFUNC = 'ALERTAS')";

            Comando = BD.GetSqlStringCommand(Query);
            Comando.CommandTimeout = Utilidades.CommandTimeout;

            try
            {
                using (iDB2DataReader dataReader = BD.ExecuteReader(Comando))
                {
                    while (dataReader.Read())
                    {
                        resultado.Add(new MailAddress(dataReader["LMAIL"].ToString(), dataReader["LNOMBRE"].ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                Log.For(this).Error(string.Format("No se encontro el correo TO/CC en AS400 ({0})", Query), ex);
            }

            if(resultado.Count == 0)
                return null;
            return resultado;
        }

        /// <summary>
        /// Valida si existe en HPO con la cantidad recibida diferente de cero
        /// </summary>
        /// <param name="orden">El/la Orden</param>
        /// <param name="linea">El/la Linea</param>
        /// <returns>System.Boolean</returns>
        public bool ExisteEnHPO(string orden, int linea)
        {
            Query = string.Format("SELECT COUNT(*) FROM HPO WHERE (PORD = {0}) AND (PODEST = '{1}') AND (PQREC = 0)", orden, linea);
            if (ExisteDato(Query))
                return true;
            return false;
        }

        #endregion
    }
}