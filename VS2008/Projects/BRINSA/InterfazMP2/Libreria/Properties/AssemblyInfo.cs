﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Libreria intefaz MP2")]
[assembly: AssemblyDescription("Libreria de datos para la intefaz MP2")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Primate Developers for BSS")]
[assembly: AssemblyProduct("Libreria")]
[assembly: AssemblyCopyright("Copyright © Primate Developers 2009")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("3ffeac78-bad6-4128-a43b-d87dc33c6b2b")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]

//Log4Net
[assembly: log4net.Config.XmlConfigurator(ConfigFile = "Log4Net.config.xml", Watch = true)]