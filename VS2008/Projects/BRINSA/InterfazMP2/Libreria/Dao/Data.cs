﻿using System;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace BSS.Brinsa.MP2.Libreria.Dao
{
    /// <summary>
    /// Clase BSS.Brinsa.MP2.Libreria.Dao.Data
    /// </summary>
    public class Data
    {
        #region Miembros
        /// <summary>
        /// Objeto que contiene la base de datos
        /// </summary>
        private Database bd;
        /// <summary>
        /// Objeto de comando
        /// </summary>
        private DbCommand comando;
        /// <summary>
        /// Variable que almacena la consulta a la base de datos
        /// </summary>
        private String consulta;
        /// <summary>
        /// Numero de registros insertados
        /// </summary>
        private int registro;
        /// <summary>
        /// Session Id
        /// </summary>
        private string sessionId;
        #endregion

        #region Propiedades
        /// <summary>
        /// Devuelve o establece la base de datos
        /// </summary>
        protected Database BD
        {
            get { return this.bd; }
            set { this.bd = value; }
        }

        /// <summary>
        /// Devuelve o establece el comando
        /// </summary>
        protected DbCommand Comando
        {
            get { return this.comando; }
            set { this.comando = value; }
        }

        /// <summary>
        /// Devuelve o establece el query
        /// </summary>
        protected string Query
        {
            get { return this.consulta; }
            set { this.consulta = value; }
        }

        /// <summary>
        /// Devuelve o establece el procedimiento
        /// </summary>
        protected string Procedimiento
        {
            get { return this.consulta; }
            set { this.consulta = value; }
        }

        /// <summary>
        /// Devuelve o establece el <see cref="registro"/>
        /// </summary>
        public int Registro
        {
            get { return registro; }
            set { registro = value; }
        }

        /// <summary>
        /// Obtiene o establece session id
        /// </summary>
        /// <value></value>
        public string SessionId
        {
            get { return sessionId; }
            set { sessionId = value; }
        }
        #endregion

        #region Clase
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="BSS.Brinsa.MP2.Libreria.Dao.Data"/>
        /// </summary>
        protected Data() { }

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="BSS.Brinsa.MP2.Libreria.Dao.Data"/>
        /// </summary>
        /// <param name="baseDatos">The base datos.</param>
        protected Data(Utilidades.BaseDatos baseDatos)
            : this()
        {
            bd = DatabaseFactory.CreateDatabase(baseDatos.ToString());
        }
        #endregion

        #region Metodos
        #endregion
    }
}