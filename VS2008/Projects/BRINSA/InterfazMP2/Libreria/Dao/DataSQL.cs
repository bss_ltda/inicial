﻿using System;
using System.Collections.Generic;
using System.Data;
using BSS.Brinsa.MP2.Libreria.Dominio;

namespace BSS.Brinsa.MP2.Libreria.Dao
{
    /// <summary>
    /// Clase BSS.Brinsa.MP2.Libreria.Dao.DataSQL
    /// </summary>
    public class DataSQL : Data
    {
        #region Constructor
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="BSS.Brinsa.MP2.Libreria.Dao.DataSQL"/>
        /// </summary>
        public DataSQL()
            : base(Utilidades.BaseDatos.ConexionSQL)
        {
        }
        #endregion

        #region Metodos publicos
        /// <summary>
        /// insertar log
        /// </summary>
        /// <param name="estado">The estado.</param>
        /// <param name="registro">The registro.</param>
        /// <param name="mensaje">The mensaje.</param>
        public void InsertarLog(Utilidades.Estado estado, string registro, string mensaje)
        {
            Procedimiento = "MP2I_InsertarLog";
            Comando = BD.GetStoredProcCommand(Procedimiento);
            Comando.CommandTimeout = Utilidades.CommandTimeout;
            BD.AddInParameter(Comando, "estado", DbType.String, (int)estado);
            BD.AddInParameter(Comando, "registro", DbType.String, registro);
            BD.AddInParameter(Comando, "mensaje", DbType.String, mensaje);
            BD.AddInParameter(Comando, "sessionId", DbType.String, SessionId);

            try
            {
                BD.ExecuteNonQuery(Comando);
            }
            catch (Exception ex)
            {
                Log.For(this).Error("Insertar Log", ex);
            }
        }

        /// <summary>
        /// consultar log
        /// </summary>
        /// <returns>
        /// System.Collections.Generic.IList&lt;System.Collections.Generic.IDictionary&lt;System.String,System.String&gt;&gt;
        /// </returns>
        public IList<IDictionary<string, string>> ConsultarLog()
        {
            Procedimiento = "MP2I_ConsultarLog";
            Comando = BD.GetStoredProcCommand(Procedimiento);
            Comando.CommandTimeout = Utilidades.CommandTimeout;
            BD.AddInParameter(Comando, "sessionId", DbType.String, SessionId);

            IList<IDictionary<string, string>> log = new List<IDictionary<string, string>>();
            using (IDataReader dataReader = BD.ExecuteReader(Comando))
            {
                Dictionary<string, string> dato;
                while (dataReader.Read())
                {
                    dato = new Dictionary<string,string>();
                    dato.Add("Numero", dataReader["numero_t"].ToString());
                    dato.Add("Fecha", dataReader["fecha_t"].ToString());
                    dato.Add("Registro", dataReader["registro_t"].ToString());
                    dato.Add("Mensaje", dataReader["mensaje_t"].ToString());
                    log.Add(dato);
                }
            }
            return log;
        }

        /// <summary>
        /// consecutivo log
        /// </summary>
        /// <returns>System.String</returns>
        public string ConsecutivoLog() {
            Procedimiento = "MP2I_ConsecutivoLog";
            Comando = BD.GetStoredProcCommand(Procedimiento);
            Comando.CommandTimeout = Utilidades.CommandTimeout;

            string consecutivo = "1";
            try
            {
                consecutivo = BD.ExecuteScalar(Comando).ToString();
            }
            catch { 
            }
            return consecutivo;
        }
        #endregion

        #region Metodos protegidos (para ser usados por herencia)
        /// <summary>
        /// existe infovalida si existe la informacion en el input y en la tabla po
        /// </summary>
        /// <returns>System.Boolean</returns>
        protected bool ExisteInfo()
        {
            bool resultado = false;

            Procedimiento = "MP2I_ExisteInfo";
            Comando = BD.GetStoredProcCommand(Procedimiento);
            Comando.CommandTimeout = Utilidades.CommandTimeout;

            try
            {
                resultado = Convert.ToBoolean(BD.ExecuteScalar(Comando));
            }
            catch (Exception ex)
            {
                Log.For(this).Error("Validando si existe informacion", ex);
                resultado = false;
            }
            return resultado;
        }

        /// <summary>
        /// traer informacion del InputMP2
        /// </summary>
        /// <returns>
        /// System.Collections.Generic.List&lt;BSS.Brinsa.MP2.Libreria.Dominio.CampoInputMp2&gt;
        /// </returns>
        protected List<Campo> TraerInformacionInput()
        {
            Procedimiento = "MP2I_ConsultarInput";
            Comando = BD.GetStoredProcCommand(Procedimiento);
            Comando.CommandTimeout = Utilidades.CommandTimeout;

            List<Campo> seleccion = new List<Campo>();
            using (IDataReader dataReader = BD.ExecuteReader(Comando))
            {
                Campo campo;
                while (dataReader.Read())
                {
                    campo = new Campo();
                    campo.Numero = dataReader["Numero"].ToString();
                    campo.Fecha = Convert.ToDateTime(dataReader["Fecha"]);
                    campo.Registro = dataReader["Registro"].ToString();
                    seleccion.Add(campo);
                }
            }
            return seleccion;
        }

        /// <summary>
        /// eliminar log input
        /// </summary>
        /// <param name="numero">The numero.</param>
        /// <param name="mensaje">The mensaje.</param>
        protected void EliminarLogInput(string numero, string mensaje)
        {
            Procedimiento = "MP2I_EliminarInput";
            Comando = BD.GetStoredProcCommand(Procedimiento);
            Comando.CommandTimeout = Utilidades.CommandTimeout;
            BD.AddInParameter(Comando, "numero", DbType.String, numero);
            BD.AddInParameter(Comando, "mensaje", DbType.String, mensaje);

            try
            {
                Registro = BD.ExecuteNonQuery(Comando);
                mensaje = numero;
                Log.For(this).Info(mensaje);
            }
            catch (Exception ex)
            {
                Log.For(this).Error("Eliminar registro Input", ex);
            }
        }

        /// <summary>
        /// traer cantidad
        /// </summary>
        /// <param name="orden">The orden.</param>
        /// <param name="release">The release.</param>
        /// <param name="secuencia">The secuencia.</param>
        /// <param name="receip">The receip.</param>
        /// <returns>System.Decimal</returns>
        protected decimal TraerCantidad(string orden, string release, string secuencia, string receip)
        {
            Procedimiento = "MP2I_TraerCantidad";
            Comando = BD.GetStoredProcCommand(Procedimiento);
            Comando.CommandTimeout = Utilidades.CommandTimeout;
            BD.AddInParameter(Comando, "orden", DbType.String, orden);
            BD.AddInParameter(Comando, "release", DbType.String, release);
            BD.AddInParameter(Comando, "secuencia", DbType.String, secuencia);
            BD.AddInParameter(Comando, "receip", DbType.String, receip);

            Decimal resultado = 0;
            try
            {
                resultado = Convert.ToDecimal(BD.ExecuteScalar(Comando));
            }
            catch (Exception ex)
            {
                Log.For(this).Error("Error cuando se trae la cantidad recibida", ex);
                resultado = 0;
            }
            return resultado;
        }

        /// <summary>
        /// traer informacion po
        /// </summary>
        /// <returns>
        /// System.Collections.Generic.List&lt;BSS.Brinsa.MP2.Libreria.Dominio.CampoInputMp2&gt;
        /// </returns>
        protected List<Campo> TraerInformacionPo()
        {
            Procedimiento = "MP2I_ConsultarPo";
            Comando = BD.GetStoredProcCommand(Procedimiento);
            Comando.CommandTimeout = Utilidades.CommandTimeout;

            List<Campo> seleccion = new List<Campo>();
            using (IDataReader dataReader = BD.ExecuteReader(Comando))
            {
                Campo campo;
                while (dataReader.Read())
                {
                    campo = new Campo(true);
                    campo.Numero = dataReader["PONUM"].ToString();
                    campo.Fecha = DateTime.Now;
                    campo.RegistroDetalle.Add(string.Empty);
                    campo.RegistroDetalle.Add("HPP");
                    campo.RegistroDetalle.Add("U");
                    campo.RegistroDetalle.Add("MP2");
                    campo.RegistroDetalle.Add(dataReader["QTYREQUESTED"].ToString());
                    campo.RegistroDetalle.Add(campo.Numero);
                    campo.RegistroDetalle.Add(dataReader["LINENUMBER"].ToString());
                    campo.RegistroDetalle.Add(dataReader["UNITCOST"].ToString());
                    campo.RegistroDetalle.Add(dataReader["ITEMNUM"].ToString());
                    campo.RegistroDetalle.Add(dataReader["ORDERWAREHOUSE"].ToString());
                    campo.RegistroDetalle.Add(dataReader["COSTCENTER"].ToString());
                    campo.RegistroDetalle.Add(dataReader["VENDORID"].ToString());
                    campo.RegistroDetalle.Add(dataReader["SITEID"].ToString());
                    campo.RegistroDetalle.Add(dataReader["DATESERVICE"].ToString());
                    campo.RegistroDetalle.Add(dataReader["SERVICECODE"].ToString());
                    campo.RegistroDetalle.Add(dataReader["ITEMTYPE"].ToString());
                    campo.RegistroDetalle.Add(dataReader["SEQNUM"].ToString());
                    campo.Notas = dataReader["NOTES"].ToString();
                    seleccion.Add(campo);
                }
            }
            return seleccion;
        }

        /// <summary>
        /// actualizar estado po
        /// </summary>
        /// <param name="orden">The orden.</param>
        /// <param name="estado">The estado.</param>
        protected void ActualizarEstadoPo(string orden, string estado)
        {
            Procedimiento = "MP2I_ActualizarEstadoPo";
            Comando = BD.GetStoredProcCommand(Procedimiento);
            Comando.CommandTimeout = Utilidades.CommandTimeout;
            BD.AddInParameter(Comando, "orden", DbType.String, orden);
            BD.AddInParameter(Comando, "estado", DbType.String, estado);

            Registro = BD.ExecuteNonQuery(Comando);
        }

        /// <summary>
        /// actualizar estado input ord
        /// </summary>
        /// <param name="orden">El/la Orden</param>
        /// <param name="linea">El/la Linea</param>
        protected void ActualizarEstadoInputOrd(string orden, int linea)
        {
            Procedimiento = "MP2I_ActualizarEstadoInputOrd";
            Comando = BD.GetStoredProcCommand(Procedimiento);
            Comando.CommandTimeout = Utilidades.CommandTimeout;
            BD.AddInParameter(Comando, "numero", DbType.String, orden);
            BD.AddInParameter(Comando, "linea", DbType.Int32, linea);
            BD.AddInParameter(Comando, "estado", DbType.Int32, 2);

            Registro = BD.ExecuteNonQuery(Comando);
        }

        /// <summary>
        /// mensaje po
        /// </summary>
        /// <param name="accion">The accion.</param>
        /// <param name="orden">The orden.</param>
        /// <param name="linea">The linea.</param>
        /// <param name="seqnum_purreq">The linea.</param>
        /// <returns>System.String</returns>
        protected string MensajePo(string accion, string orden, string linea, string seqnum_purreq )
        {
            string resultado;

            Procedimiento = "MP2I_MensajePo";
            Comando = BD.GetStoredProcCommand(Procedimiento);
            Comando.CommandTimeout = Utilidades.CommandTimeout;
            BD.AddInParameter(Comando, "accion", DbType.String, accion);
            BD.AddInParameter(Comando, "orden", DbType.String, orden);
            BD.AddInParameter(Comando, "linea", DbType.String, linea);
            BD.AddInParameter(Comando, "seqnum", DbType.Int32, Convert.ToInt32( seqnum_purreq ));

            resultado = BD.ExecuteScalar(Comando).ToString();
            return resultado;
        }

        /// <summary>
        /// traer ordenes eliminadas
        /// </summary>
        /// <returns>
        /// System.Collections.Generic.List&lt;BSS.Brinsa.MP2.Libreria.Dominio.Orden&gt;
        /// </returns>
        protected List<Orden> TraerOrdenesEliminadas()
        {
            Procedimiento = "MP2I_ConsultarOrdenEliminada";
            Comando = BD.GetStoredProcCommand(Procedimiento);
            Comando.CommandTimeout = Utilidades.CommandTimeout;

            List<Orden> seleccion = new List<Orden>();
            using (IDataReader dataReader = BD.ExecuteReader(Comando))
            {
                Orden campo;
                while (dataReader.Read())
                {
                    campo = new Orden();
                    campo.Numero = dataReader["PONUM"].ToString();
                    campo.Linea = Convert.ToInt32(dataReader["LINENUMBER"]);
                    seleccion.Add(campo);
                }
            }
            return seleccion;
        }
        #endregion
    }
}