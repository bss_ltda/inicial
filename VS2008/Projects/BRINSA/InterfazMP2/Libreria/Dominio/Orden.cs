﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSS.Brinsa.MP2.Libreria.Dominio
{
    /// <summary>
    /// Clase BSS.Brinsa.MP2.Libreria.Dominio.Orden
    /// </summary>
    public class Orden
    {
        #region Miembros
        /// <summary>
        /// Numero
        /// </summary>
        private string numero;
        /// <summary>
        /// Linea
        /// </summary>
        private int linea;
        #endregion

        #region Propiedades
        /// <summary>
        /// Obtiene o establece numero
        /// </summary>
        /// <value></value>
        public string Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        /// <summary>
        /// Obtiene o establece linea
        /// </summary>
        /// <value></value>
        public int Linea
        {
            get { return linea; }
            set { linea = value; }
        }
        #endregion
    }
}
