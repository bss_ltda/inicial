﻿using System;
using System.Collections.Generic;

namespace BSS.Brinsa.MP2.Libreria.Dominio
{
    /// <summary>
    /// Clase BSS.Brinsa.MP2.Libreria.Dominio.Campo
    /// </summary>
    public class Campo
    {
        #region Miembros
        /// <summary>
        /// Numero documento
        /// </summary>
        private string numero;
        /// <summary>
        /// Fecha documento
        /// </summary>
        private DateTime fecha;
        /// <summary>
        /// Informacion separada por "|"
        /// </summary>
        private string registro;
        /// <summary>
        /// Informacion del detalle
        /// </summary>
        private List<string> registroDetalle;
        /// <summary>
        /// Valida si es campo
        /// </summary>
        private readonly bool esPo = false;
        /// <summary>
        /// notas
        /// </summary>
        private string notas;
        #endregion

        #region Propiedades
        /// <summary>
        /// Obtiene o establece numero
        /// </summary>
        /// <value></value>
        public string Numero
        {
            get { return this.numero; }
            set { this.numero = value; }
        }

        /// <summary>
        /// Obtiene o establece fecha
        /// </summary>
        /// <value></value>
        public DateTime Fecha
        {
            get { return this.fecha; }
            set { this.fecha = value; }
        }

        /// <summary>
        /// Obtiene o establece registro
        /// </summary>
        /// <value></value>
        public string Registro
        {
            get { return this.registro; }
            set { this.registro = value; }
        }

        /// <summary>
        /// Obtiene o establece registro detalle ya particionado por "|"
        /// </summary>
        /// <value></value>
        public List<string> RegistroDetalle
        {
            get
            {
                if (esPo)
                {
                    return registroDetalle;
                }
                else
                {
                    registroDetalle = new List<string>();
                    char[] caracter = new char[] { '|' };
                    foreach (string item in this.registro.Split(caracter, StringSplitOptions.None))
                    {
                        registroDetalle.Add(item);
                    }
                    return registroDetalle;
                }
            }
        }

        /// <summary>
        /// Obtiene o establece es po
        /// </summary>
        /// <value></value>
        public bool EsPo
        {
            get { return esPo; }
        }

        /// <summary>
        /// Obtiene o establece notas
        /// </summary>
        /// <value></value>
        public string Notas
        {
            get { return notas; }
            set { notas = value; }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="BSS.Brinsa.MP2.Libreria.Dominio.Campo"/>
        /// </summary>
        public Campo()
        {
            registroDetalle = new List<string>();
        }

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="BSS.Brinsa.MP2.Libreria.Dominio.Campo"/>
        /// </summary>
        /// <param name="esPo">if set to <c>true</c> [es po].</param>
        public Campo(bool esPo)
            : this()
        {
            this.esPo = esPo;
            this.notas = string.Empty;
        }
        #endregion
    }
}