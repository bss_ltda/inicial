﻿using System;
using System.Net.Mail;

namespace BSS.Brinsa.MP2.Libreria
{
    /// <summary>
    /// Clase BSS.Brinsa.MP2.Libreria.Utilidades
    /// </summary>
    public class Utilidades
    {
        #region Miembros
        /// <summary>
        /// Tiempo de espera de los comandos
        /// </summary>
        public const int CommandTimeout = 0;
        #endregion

        #region Enumeraciones
        /// <summary>
        /// Enumeracion para identificar la conexion de base de datos
        /// </summary>
        public enum BaseDatos
        {
            /// <summary>
            /// Conexion hacia SQL server
            /// </summary>
            ConexionSQL,
            /// <summary>
            /// Conexion hacia DB2
            /// </summary>
            ConexionDB2
        }

        /// <summary>
        /// Manejo de estados
        /// </summary>
        public enum Estado
        {
            /// <summary>
            /// Transaccion exitosa
            /// </summary>
            Exito = 1,
            /// <summary>
            /// Error datos insuficientes
            /// </summary>
            DatosInsuficientes = 2,
            /// <summary>
            /// Error Problema AS400
            /// </summary>
            Error400 = 3
        }
        #endregion

        #region Metodos
        /// <summary>
        /// armar fecha
        /// </summary>
        /// <param name="fecha">The fecha.</param>
        /// <returns>System.String</returns>
        public static string ArmarFecha(DateTime fecha)
        {
            return fecha.ToString("yyyyMMdd");
        }

        /// <summary>
        /// armar hora
        /// </summary>
        /// <param name="fecha">The fecha.</param>
        /// <returns>System.String</returns>
        public static string ArmarHora(DateTime fecha)
        {
            return fecha.ToString("HHmmss");
        }

        /// <summary>
        /// armar fecha
        /// </summary>
        /// <param name="fecha">The fecha.</param>
        /// <returns>System.String</returns>
        public static string ArmarFecha(string fecha)
        {
            return fecha;
            //return string.Format("20{0}", fecha.Replace(".", string.Empty));
        }
        #endregion

        #region Metodos Envio de correo
        /// <summary>
        /// Envia un correo electronico
        /// </summary>
        /// <param name="de"></param>
        /// <param name="para"></param>
        /// <param name="asunto"></param>
        /// <param name="mensaje"></param>
        public static void EnviarMail(String de, String para, String asunto, String mensaje)
        {
            EnviarMail(de, para, null, asunto, mensaje);
        }

        /// <summary>
        /// Enviars the mail.
        /// </summary>
        /// <param name="de">The de.</param>
        /// <param name="para">The para.</param>
        /// <param name="cc">The cc.</param>
        /// <param name="asunto">The asunto.</param>
        /// <param name="mensaje">The mensaje.</param>
        public static void EnviarMail(String de, String para, String cc, String asunto, String mensaje)
        {
            MailMessage correo = new MailMessage(de, para, asunto, mensaje);
            if (cc != null)
                correo.CC.Add(cc);
            correo.IsBodyHtml = true;
            EnviarMail(correo);
        }

        /// <summary>
        /// Envia un correo electronico
        /// </summary>
        /// <param name="de"></param>
        /// <param name="para"></param>
        /// <param name="cc"></param>
        /// <param name="asunto"></param>
        /// <param name="mensaje"></param>
        public static void EnviarMail(MailAddress de, MailAddress para, MailAddress cc, String asunto, String mensaje)
        {
            MailMessage correo = new MailMessage(de, para);
            if (cc != null)
                correo.CC.Add(cc);
            correo.IsBodyHtml = true;
            correo.Subject = asunto;
            correo.Body = mensaje;
            EnviarMail(correo);
        }

        /// <summary>
        /// enviar mail
        /// </summary>
        /// <param name="de">El/la De</param>
        /// <param name="para">El/la Para</param>
        /// <param name="cc">El/la Cc</param>
        /// <param name="asunto">El/la Asunto</param>
        /// <param name="mensaje">El/la Mensaje</param>
        public static void EnviarMail(MailAddress de, System.Collections.Generic.IList<MailAddress> para, System.Collections.Generic.IList<MailAddress> cc, string asunto, string mensaje)
        {
            if (de == null)
                return;
            if (para == null)
                return;

            MailMessage correo = new MailMessage();

            //Agrega el from
            correo.From = de;

            //Agrega el para
            foreach (MailAddress item in para)
            {
                correo.To.Add(item);
            }

            //Agrega el con copia si viene
            if (cc != null)
            {
                foreach (MailAddress item in cc)
                {
                    correo.To.Add(item);
                }
            }

            correo.Subject = asunto;
            correo.Body = mensaje;
            correo.IsBodyHtml = true;

            EnviarMail(correo);
        }


        /// <summary>
        /// Envia un correo electronico
        /// </summary>
        /// <param name="correo"></param>
        public static void EnviarMail(MailMessage correo)
        {

            SmtpClient smtp = new SmtpClient();

            try
            {
                smtp.Send(correo);
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}