﻿using System.ServiceProcess;

namespace BSS.Brinsa.MP2.MonitorMP2
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new Servicio() 
			};
            ServiceBase.Run(ServicesToRun);
        }
    }
}
