﻿using System;
using System.Diagnostics;
using System.ServiceProcess;
using System.Threading;
using BSS.Brinsa.MP2.Libreria.Servicio;
using BSS.Brinsa.MP2.Libreria;

namespace BSS.Brinsa.MP2.MonitorMP2
{
    /// <summary>
    /// Clase BSS.Brinsa.MP2.MonitorMP2.Servicio
    /// </summary>
    partial class Servicio : ServiceBase
    {
        #region Miembros
        /// <summary>
        /// temporizador
        /// </summary>
        private Timer temporizador;
        /// <summary>
        /// activo
        /// </summary>
        private bool activo = false;
        /// <summary>
        /// Mensaje
        /// </summary>
        private string mensaje;
        #endregion

        #region Propiedades
        #endregion

        #region Constructor
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="BSS.Brinsa.MP2.MonitorMP2.Servicio"/>
        /// </summary>
        public Servicio()
        {
            InitializeComponent();
            this.ServiceName = "MonitorMP2";
        }
        #endregion

        #region Eventos
        /// <summary>
        /// on start
        /// </summary>
        /// <param name="args">The args.</param>
        protected override void OnStart(string[] args)
        {
            // TODO: Add code here to start your service.
            TimerCallback delegado = new TimerCallback(Iniciar);
            
            //cada 1 minuto
            temporizador = new Timer(delegado, null, 1000, 60000);
            mensaje = "Iniciando programa monitoreo MP2";
            Log.For(this).Info(mensaje);
        }

        /// <summary>
        /// on stop
        /// </summary>
        protected override void OnStop()
        {
            // TODO: Add code here to perform any tear-down necessary to stop your service.
            mensaje = "Finalizando programa monitoreo";
            Log.For(this).Info(mensaje);
        }
        #endregion

        #region Metodos
        /// <summary>
        /// procesar
        /// </summary>
        /// <param name="estado">el/la estado</param>
        private void Iniciar(object estado)
        {
            if (activo)
            {
                mensaje = "Ya se esta realizando el monitoreo";
                Log.For(this).Warn(mensaje);
            }
            else
            {
                //activa el proceso para evitar mas sobrecargas
                activo = true;

                try
                {
                    //inicia el proceso
                    mensaje = "Inciando monitoreo...";
                    Log.For(this).Info(mensaje);

                    Procesar datos = new Procesar();
                    datos.Iniciar();

                    mensaje = "Finalizando monitoreo...";
                    Log.For(this).Info(mensaje);
                }
                catch (Exception ex)
                {
                    mensaje = string.Format("Ocurrio el siguiente error: {0}", ex.Message);
                    Log.For(this).Error(mensaje, ex);
                }
            }
            activo = false;
        }
        #endregion
    }
}