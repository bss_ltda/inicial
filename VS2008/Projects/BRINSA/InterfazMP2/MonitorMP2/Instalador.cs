﻿using System.ComponentModel;
using System.Configuration.Install;
using System.ServiceProcess;


namespace BSS.Brinsa.MP2.MonitorMP2
{
    [RunInstaller(true)]
    public partial class Instalador : Installer
    {
        /// <summary>
        /// serviceInstaller
        /// </summary>
        private ServiceInstaller serviceInstaller;
        /// <summary>
        /// processInstaller
        /// </summary>
        private ServiceProcessInstaller processInstaller;

        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="BSS.Brinsa.MP2.MonitorMP2.Instalador"/>
        /// </summary>
        public Instalador()
        {
            InitializeComponent();
            serviceInstaller = new ServiceInstaller();
            processInstaller = new ServiceProcessInstaller();

            //Indica la cuenta con el cual correra
            processInstaller.Account = ServiceAccount.LocalSystem;

            //Indica el tipo de inicio del servicio
            serviceInstaller.StartType = ServiceStartMode.Automatic;

            //Se agrega el nombre
            serviceInstaller.ServiceName = "MonitorMP2";
            serviceInstaller.DisplayName = "Monitor Interfaz MP2";
            serviceInstaller.Description = "Exporta los registros MP2 del servidor de SQL Server al servidor de AS400";

            Installers.Add(serviceInstaller);
            Installers.Add(processInstaller);
        }
    }
}