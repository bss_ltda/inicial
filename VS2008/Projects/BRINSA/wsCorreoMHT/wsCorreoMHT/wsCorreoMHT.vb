﻿Imports System.IO

Public Class wsCorreoMHT

    Private WithEvents tim As New Timers.Timer(1000)

    Private DB As clsConexion

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Agregue el código aquí para iniciar el servicio. Este método debería poner
        ' en movimiento los elementos para que el servicio pueda funcionar.
        tim.Enabled = True
    End Sub

    Protected Overrides Sub OnStop()
        ' Agregue el código aquí para realizar cualquier anulación necesaria para detener el servicio.
    End Sub

    Private Sub tim_Elapsed(sender As Object, e As System.Timers.ElapsedEventArgs) Handles tim.Elapsed

        revisaTareasMHT()

    End Sub

    '<summary>Helper function to attach a debugger to the running service.</summary>
    <Conditional("DEBUG")>
    Shared Sub DebugMode()
        If Not Debugger.IsAttached Then
            Debugger.Launch()
        End If

        Debugger.Break()
    End Sub

    Sub revisaTareasMHT()
        Dim fSql As String
        Dim rs As New ADODB.Recordset
        Dim Correo As New clsMHT
        Dim serverMHT As String
        Dim local As String = ""

        If Not AbreConexion("") Then
            Exit Sub
        End If

        serverMHT = DB.getLXParam("SMTPMHTSERVER", local)

        fSql = " SELECT * FROM  RMAILX WHERE LENVIADO = 0"
        DB.OpenRS(rs, fSql)
        Do While Not rs.EOF
            With Correo
                .Inicializa()
                .DB = DB
                .Asunto = rs("LASUNTO").Value
                .De = IIf(rs("LENVIA").Value = "", rs("LREF").Value, rs("LENVIA").Value)
                .Para = rs("LPARA").Value
                .CC = rs("LCOPIA").Value
                .url = rs("LCUERPO").Value
                If InStr(.url.ToUpper, "HTTP") = 0 Then
                    If InStr(.url.ToUpper, ".ASP") = 0 Then
                        .url = serverMHT & DB.getLXParam("SMTPAVISOGRAL", local) & "?LID=" & rs("LID").Value
                    Else
                        .url = serverMHT & .url
                    End If
                End If
                .local = My.Settings.LOCAL.ToUpper
                .enviaMail()
            End With
            fSql = " UPDATE RMAILX SET  "
            fSql = fSql & " LUPDDAT = NOW() , "
            fSql = fSql & " LENVIADO = 1  "
            fSql = fSql & " WHERE LID = " & rs("LID").Value
            DB.ExecuteSQL(fSql)
            rs.MoveNext()
        Loop
        rs.Close()
        DB.Close()

    End Sub

    Function AbreConexion(ModuloActual As String) As Boolean

        DB = New clsConexion
        DB.gsDatasource = My.Settings.AS400
        DB.gsLib = My.Settings.AMBIENTE
        DB.ModuloActual = ModuloActual
        Return DB.Conexion()

    End Function


End Class
