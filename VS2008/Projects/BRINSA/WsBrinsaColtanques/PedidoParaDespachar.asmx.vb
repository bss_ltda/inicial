﻿Imports System
Imports System.Globalization
Imports System.IO
Imports System.Collections
Imports System.Xml.Serialization
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Xml
Imports System.Net

' Para permitir que se llame a este servicio Web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class PedidoParaDespachar
    Inherits System.Web.Services.WebService

    Dim Programa As String
    Dim ERRORES As String
    Public Conn As New ADODB.Connection
    'Dim appSettings As NameValueCollection = ConfigurationManager.AppSettings
    Dim Biblioteca As String
    Dim rs As New ADODB.Recordset
    Dim ContadorLineas As Integer

    Structure Cabecera
        Dim Usuario As String
        Dim Password As String
        Dim Pedido As Double
        Dim Fecha As String
    End Structure
    Structure CabeceraDespacho
        Dim Usuario As String
        Dim Password As String
        Dim Manifiesto As String
        Dim Bodega As String

        Dim TipoVehiculo As String
        Dim Notas As String
        Dim DescTipoVehiculo As String
        Dim Placa As String
        Dim CedulaConductor As String
        Dim NombreConductor As String
        Dim CelularConductor As String
    End Structure

    Structure DetalleDespacho
        Dim Pedido As String
    End Structure

    Structure CabeceraPedidoDespachado
        Dim Usuario As String
        Dim Password As String
        Dim Pedido As Double

    End Structure
    Structure DetallePedidoDespachado

        Dim LineaPedido As Integer
        Dim Producto As String
        Dim Lote As String
        Dim Cantidad As Double
    End Structure

    Structure DetalleReporteEntrega

        Dim Usuario As String
        Dim Password As String
        Dim Planilla As String
        Dim Fecha As String
        Dim EstadoEntrega As Double
        Dim Reporte As String
    End Structure

    Structure DetalleReporteDespacho

        Dim Usuario As String
        Dim Password As String
        Dim NumeroUnico As Double
        Dim Consolidado As String
        Dim EstadoDespacho As Double
        Dim FechaReporte As String
        Dim Manifiesto As Double
        Dim ReporteGeneral As String
    End Structure
    Structure Detalle

        Dim LineaPedido As Integer
        Dim CodigoProducto As String
        Dim Lote As String
        Dim Cantidad As Double
    End Structure


    <WebMethod()> _
    Public Function ConfirmacionEntradaInventario(ByVal Pedido As String) As XmlDocument
        ContadorLineas = 0
        Dim nuevaLinea As Detalle
        Dim Cabeceraa As New Cabecera
        Dim Lineas As New List(Of Detalle)
        Dim documento As XDocument = XDocument.Parse(Pedido)
        'Dim fSql As String
        '      Dim DB1 As New ExecuteSQL
        Dim qx = From xee In documento.Elements("ConfirmacionEntradaInventario")
       Select New With { _
                .Usuario = xee.Element("Usuario").Value,
                .Password = xee.Element("Password").Value,
                .Pedido = xee.Element("Pedido").Value,
                .Fecha = xee.Element("Fecha").Value
                  }
        Try
            For Each elements In qx
                Cabeceraa = New Cabecera
                Cabeceraa.Usuario = elements.Usuario
                Cabeceraa.Password = elements.Password
                Cabeceraa.Pedido = elements.Pedido
                Cabeceraa.Fecha = elements.Fecha
            Next

        Catch
            Return DevuelveError("000", "Error en XML" & Err.Description, Err.Number, "ConfirmacionEntradaInventario")
        End Try

        Try

            Dim qx2 = From xe In documento.Descendants.Elements("Linea")
        Select New With { _
                          .LineaPedido = xe.Element("LineaPedido").Value,
                          .CodigoProducto = xe.Element("CodigoProducto").Value,
                          .Cantidad = xe.Element("Cantidad").Value,
                          .Lote = xe.Element("Lote").Value
                            }
            For Each elementos In qx2
                ContadorLineas = ContadorLineas + 1
                nuevaLinea = New Detalle
                nuevaLinea.Cantidad = elementos.Cantidad
                If nuevaLinea.Cantidad <= 0 Then
                    Return DevuelveError("000", "Cantidad no puede ser menor o igual que 0", 106, "ConfirmacionEntradaInventario")
                End If
                nuevaLinea.LineaPedido = elementos.LineaPedido
                nuevaLinea.CodigoProducto = elementos.CodigoProducto
                nuevaLinea.Cantidad = elementos.Cantidad
                nuevaLinea.Lote = elementos.Lote
                Lineas.Add(nuevaLinea)
            Next
            Return DevuelveResultado(Cabeceraa.Pedido, "ConfirmacionEntradaInventario")
        Catch

            Return DevuelveError("000", "Error en XML " & Err.Description, Err.Number, "ConfirmacionEntradaInventario")
        End Try
    End Function
    <WebMethod()> _
    Public Function PedidoDespachado(ByVal Pedido As String) As XmlDocument
        ContadorLineas = 0
        Dim nuevaLinea As DetallePedidoDespachado
        Dim Cabeceraa As New CabeceraPedidoDespachado
        Dim Lineas As New List(Of DetallePedidoDespachado)
        Dim documento As XDocument = XDocument.Parse(Pedido)
        'Dim fSql As String
        '      Dim DB1 As New ExecuteSQL
        Dim qx = From xee In documento.Elements("PedidoDespachado")
       Select New With { _
                .Usuario = xee.Element("Usuario").Value,
                .Password = xee.Element("Password").Value,
                .Pedido = xee.Element("Pedido").Value
                  }
        Try
            For Each elements In qx
                Cabeceraa = New CabeceraPedidoDespachado
                Cabeceraa.Usuario = elements.Usuario
                Cabeceraa.Password = elements.Password
                Cabeceraa.Pedido = elements.Pedido

            Next

        Catch
            Return DevuelveError("000", "Error en XML" & Err.Description, Err.Number, "PedidoDespachado")
        End Try

        Try

            Dim qx2 = From xe In documento.Descendants.Elements("Linea")
        Select New With { _
                          .LineaPedido = xe.Element("LineaPedido").Value,
                          .Producto = xe.Element("Producto").Value,
                          .Lote = xe.Element("Producto").Value,
                          .Cantidad = xe.Element("Cantidad").Value
                            }
            For Each elementos In qx2
                ContadorLineas = ContadorLineas + 1
                nuevaLinea = New DetallePedidoDespachado
                nuevaLinea.Cantidad = elementos.Cantidad
                If nuevaLinea.Cantidad <= 0 Then
                    Return DevuelveError("000", "Cantidad no puede ser menor o igual que 0", 106, "PedidoDespachado")
                End If
                nuevaLinea.LineaPedido = elementos.LineaPedido
                nuevaLinea.Producto = elementos.Producto
                nuevaLinea.Lote = elementos.Lote
                nuevaLinea.Cantidad = elementos.Cantidad
                Lineas.Add(nuevaLinea)
            Next
            Return DevuelveResultado(Cabeceraa.Pedido, "PedidoDespachado")
        Catch

            Return DevuelveError("000", "Error en XML " & Err.Description, Err.Number, "PedidoDespachado")
        End Try
    End Function
    <WebMethod()> _
    Public Function ReporteDespacho(ByVal Pedido As String) As XmlDocument
        ContadorLineas = 0
        Dim Cabeceraa As New DetalleReporteDespacho
        Dim documento As XDocument = XDocument.Parse(Pedido)
        'Dim fSql As String
        '      Dim DB1 As New ExecuteSQL
        Dim qx = From xee In documento.Elements("ReporteDespacho")
       Select New With { _
                .Usuario = xee.Element("Usuario").Value,
                .Password = xee.Element("Password").Value,
                .NumeroUnico = xee.Element("NumeroUnico").Value,
                .Consolidado = xee.Element("Consolidado").Value,
                .EstadoDespacho = xee.Element("EstadoDespacho").Value,
                .FechaReporte = xee.Element("FechaReporte").Value,
                .Manifiesto = xee.Element("Manifiesto").Value,
                .ReporteGeneral = xee.Element("ReporteGeneral").Value
                  }
        Try
            For Each elements In qx
                Cabeceraa = New DetalleReporteDespacho
                Cabeceraa.Usuario = elements.Usuario
                Cabeceraa.Password = elements.Password
                Cabeceraa.NumeroUnico = elements.NumeroUnico
                Cabeceraa.Consolidado = elements.Consolidado
                Cabeceraa.EstadoDespacho = elements.EstadoDespacho
                Cabeceraa.FechaReporte = elements.NumeroUnico
                Cabeceraa.Manifiesto = elements.Manifiesto
                Cabeceraa.ReporteGeneral = elements.ReporteGeneral

            Next
        Catch
            Return DevuelveError("000", "Error en XML" & Err.Description, Err.Number, "ReporteDespacho")
        End Try
        Return DevuelveResultado(Cabeceraa.Consolidado, "ReporteDespacho")
    End Function

    <WebMethod()> _
    Public Function ReporteEntrega(ByVal Pedido As String) As XmlDocument
        ContadorLineas = 0
        Dim Cabeceraa As New DetalleReporteEntrega
        Dim documento As XDocument = XDocument.Parse(Pedido)
        'Dim fSql As String
        '      Dim DB1 As New ExecuteSQL
        Dim qx = From xee In documento.Elements("ReporteEntrega")
       Select New With { _
                .Usuario = xee.Element("Usuario").Value,
                .Password = xee.Element("Password").Value,
                .Planilla = xee.Element("Planilla").Value,
                .Fecha = xee.Element("Fecha").Value,
                .EstadoEntrega = xee.Element("EstadoEntrega").Value,
                .Reporte = xee.Element("Reporte").Value
                   }
        Try
            For Each elements In qx
                Cabeceraa = New DetalleReporteEntrega
                Cabeceraa.Usuario = elements.Usuario
                Cabeceraa.Password = elements.Password
                Cabeceraa.Planilla = elements.Planilla
                Cabeceraa.Fecha = elements.Fecha
                Cabeceraa.EstadoEntrega = elements.EstadoEntrega
                Cabeceraa.Reporte = elements.Reporte
 

            Next
        Catch
            Return DevuelveError("000", "Error en XML" & Err.Description, Err.Number, "ReporteEntrega")
        End Try
        Return DevuelveResultado(Cabeceraa.Planilla, "ReporteEntrega")
    End Function

    <WebMethod()> _
    Public Function Despacho(ByVal Pedido As String) As XmlDocument
        ContadorLineas = 0
        Dim nuevaLinea As DetalleDespacho
        Dim Cabeceraa As New CabeceraDespacho
        Dim Lineas As New List(Of DetalleDespacho)
        Dim documento As XDocument = XDocument.Parse(Pedido)
        'Dim fSql As String
        '      Dim DB1 As New ExecuteSQL
        Dim qx = From xee In documento.Elements("Despacho")
       Select New With { _
        .Usuario = xee.Element("Usuario").Value,
        .Password = xee.Element("Password").Value,
        .Manifiesto = xee.Element("Manifiesto").Value,
        .Bodega = xee.Element("Bodega").Value,
        .TipoVehiculo = xee.Element("TipoVehiculo").Value,
        .Notas = xee.Element("Notas").Value,
        .DescTipoVehiculo = xee.Element("DescTipoVehiculo").Value,
        .Placa = xee.Element("Placa").Value,
        .CedulaConductor = xee.Element("CedulaConductor").Value,
        .NombreConductor = xee.Element("NombreConductor").Value,
        .CelularConductor = xee.Element("CelularConductor").Value
                  }
        Try
            For Each elements In qx
                Cabeceraa = New CabeceraDespacho

                Cabeceraa.Usuario = elements.Usuario
                Cabeceraa.Password = elements.Password
                Cabeceraa.Manifiesto = elements.Manifiesto
                Cabeceraa.Bodega = elements.Bodega
                Cabeceraa.TipoVehiculo = elements.TipoVehiculo
                Cabeceraa.Notas = elements.Notas
                Cabeceraa.DescTipoVehiculo = elements.DescTipoVehiculo
                Cabeceraa.Placa = elements.Placa
                Cabeceraa.CedulaConductor = elements.CedulaConductor
                Cabeceraa.NombreConductor = elements.NombreConductor
                Cabeceraa.CelularConductor = elements.CelularConductor
            Next

        Catch
            Return DevuelveError("000", "Error en XML" & Err.Description, Err.Number, "Despacho")
        End Try

        Try
            Dim qx2 = From xe In documento.Descendants.Elements("PedidoDepachado")
        Select New With { _
                          .Pedido = xe.Element("Pedido").Value
                            }
            For Each elementos In qx2
                ContadorLineas = ContadorLineas + 1
                nuevaLinea = New DetalleDespacho
                nuevaLinea.Pedido = elementos.Pedido
                Lineas.Add(nuevaLinea)
            Next
            Return DevuelveResultado(Cabeceraa.Manifiesto, "Despacho")
        Catch

            Return DevuelveError("000", "Error en XML " & Err.Description, Err.Number, "Despacho")
        End Try
    End Function
    Function DevuelveError(ByVal NumeroDocumento As String, ByVal ERROOR As String, ByVal CODIGO As String, ByVal proceso As String) As XmlDocument


        Dim xmlstr As String = ""
        Dim sw As New StringWriter()
        Dim writer As New XmlTextWriter(sw)
        writer.WriteStartDocument(True)
        writer.Formatting = Formatting.Indented
        writer.Indentation = 2
        CrearNodoError(ERROOR, CODIGO, NumeroDocumento, proceso, writer)
        writer.Flush()
        xmlstr = sw.ToString
        writer.Close()
        Dim pDoc As New XmlDocument
        pDoc.LoadXml(xmlstr)
        Return pDoc

    End Function

    Function CrearNodoError(ByVal ErrorXML As String, ByVal codigo As String, ByVal NumeroDocumento As String, ByVal proceso As String, ByVal writer As XmlTextWriter)


        writer.WriteStartElement(proceso) ' ENVELOPE
        writer.WriteStartElement("Documento")  ' PEDIDO
        writer.WriteString(NumeroDocumento)
        writer.WriteStartElement("Mensaje")     'MENSAJE
        writer.WriteStartElement("Error")       'ERROR
        writer.WriteStartElement("MensajeID")   'ID
        writer.WriteString(codigo)
        writer.WriteEndElement() ' MensajeId
        writer.WriteStartElement("MensajeDES")
        writer.WriteString(ErrorXML)
        writer.WriteEndElement() ' DES
        writer.WriteEndElement() ' ERROR
        writer.WriteEndElement() ' MENSAJE
        writer.WriteEndElement() ' PEDIDO
        writer.WriteEndElement() ' ENVELOPE
        Return 0
    End Function

    Function DevuelveResultado(ByVal NumeroDocumento As String, ByVal proceso As String)
        ' Dim fSql As String
        Dim xmlstr As String = ""
        Dim sw As New StringWriter()
        Dim writer As New XmlTextWriter(sw)
        Dim flag As Integer = 0
        Dim flag2 As Integer = 0

        Dim rs As New ADODB.Recordset

        writer.WriteStartDocument(True)
        writer.Formatting = Formatting.Indented
        writer.Indentation = 2
        writer.WriteStartElement("Respuesta")
        writer.WriteStartElement("Operacion")
        writer.WriteString(proceso)
        writer.WriteEndElement()
        If (proceso = "Despacho") Then
            writer.WriteStartElement("Consolidado")
            writer.WriteString("000000")
            writer.WriteEndElement()
            writer.WriteStartElement("Manifiesto")
        Else
            If (proceso = "ReporteDespacho") Then
                writer.WriteStartElement("Consolidado")
            Else
                If (proceso <> "ReporteEntrega") Then
                    writer.WriteStartElement("Pedido")
                Else
                    writer.WriteStartElement("Planilla")
                End If
            End If
        End If
        writer.WriteString(NumeroDocumento)
        writer.WriteEndElement()
        If (proceso <> "ConfirmacionEntradaInventario" And proceso <> "PedidoDespachado" And proceso <> "ReporteDespacho" And proceso <> "ReporteEntrega" And proceso <> "Despacho") Then
            writer.WriteStartElement("TotalLineas")
            writer.WriteString(ContadorLineas)
            writer.WriteEndElement()
        End If
        writer.WriteStartElement("Mensaje")
        writer.WriteString("Ok")
        writer.WriteEndElement()
        writer.WriteEndElement()

        writer.Flush()
        xmlstr = sw.ToString
        writer.Close()
        Dim pDoc As New XmlDocument
        pDoc.LoadXml(xmlstr)

        Return pDoc

    End Function










End Class