﻿'BRINSA-COLTANQUES
Imports System
Imports System.Globalization
Imports System.IO
Imports System.Collections
Imports System.Xml.Serialization
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Xml
Imports System.Net

' Para permitir que se llame a este servicio Web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class BodegaSatelite
    Inherits System.Web.Services.WebService
    Dim consolidado As String
    Public Const DB2_DATEDEC = " ( YEAR( NOW() ) * 10000 + MONTH( NOW()  ) * 100 + DAY( NOW() ) )"
    Public Const DB2_TIMEDEC = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    Public Const DB2_TIMEDEC0 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    Public Const DB2_TIMEDEC1 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 ) "
    Dim xmlRespuesta As String
    Dim Programa As String
    Dim ERRORES As String
    Dim Factura As String
    Dim Pedid As String
    Dim Ial As String
    Dim Oc As String
    Dim Bodega As String
    Dim conso As String
    Dim Factu As String
    Dim gsProvider As String
    Dim gsDatasource As String
    Dim rs As New ADODB.Recordset
    Dim rs1 As New ADODB.Recordset
    Dim mp2Param(5) As String
    Dim AS400Param(5) As String
    Dim Resultado As Boolean = True
    Dim i As Integer
    Dim sLib
    Dim gsConnString
    Dim DB_DB2, DB_MP2
    Dim C_ID As String
    Public Conn As New ADODB.Connection
    'Dim appSettings As NameValueCollection = ConfigurationManager.AppSettings
    Dim Biblioteca As String
    Dim ContadorLineas As Integer

    Structure Cabecera
        Dim Usuario As String
        Dim Password As String
        Dim Pedido As Double
        Dim Fecha As String
    End Structure
    Structure CabeceraDespacho
        Dim Usuario As String
        Dim Password As String
        Dim Manifiesto As String
        Dim Bodega As String

        Dim TipoVehiculo As String
        Dim Notas As String
        Dim DescTipoVehiculo As String
        Dim Placa As String
        Dim CedulaConductor As String
        Dim NombreConductor As String
        Dim CelularConductor As String
    End Structure

    Structure DetalleDespacho
        Dim Pedido As String
    End Structure

    Structure CabeceraPedidoDespachado
        Dim Usuario As String
        Dim Password As String
        Dim Pedido As Double

    End Structure

 
    Structure DetallePedidoDespachado

        Dim LineaPedido As Integer
        Dim Producto As String
        Dim Lote As String
        Dim Cantidad As Double
    End Structure

    Structure DetalleReporteEntrega

        Dim Usuario As String
        Dim Password As String
        Dim Planilla As String
        Dim FechaReporte As String
        Dim EstadoEntrega As Double
        Dim Reporte As String
    End Structure

    Structure DetalleReporteDespacho

        Dim Usuario As String
        Dim Password As String
        Dim NumeroUnico As Double
        Dim Consolidado As String
        Dim EstadoDespacho As Double
        Dim FechaReporte As String
        Dim Manifiesto As Double
        Dim ReporteGeneral As String
    End Structure
    Structure Detalle

        Dim LineaPedido As Integer
        Dim CodigoProducto As String
        Dim Lote As String
        Dim Cantidad As String
    End Structure


    <WebMethod()> _
    Public Function ConfirmacionEntradaInventario(ByVal XML As String) As XmlDocument
        CargaPrm()
        C_ID = CalcConsec("RFLOGWSCDA", False, "99999999")
        wrtLogWsCDA(C_ID, XML, "Prueba", "", "ConfirmacionEntradaInventario", "wsBrinsa", "EM")
        ContadorLineas = 0
        Dim nuevaLinea As Detalle
        Dim Cabeceraa As New Cabecera
        Dim Lineas As New List(Of Detalle)
        Dim documento As XDocument = XDocument.Parse(XML)
        'Dim fSql As String
        '      Dim DB1 As New ExecuteSQL
        Dim qx = From xee In documento.Elements("ConfirmacionEntradaInventario")
       Select New With { _
                .Usuario = xee.Element("Usuario").Value,
                .Password = xee.Element("Password").Value,
                .Pedido = xee.Element("Pedido").Value,
                .Fecha = xee.Element("Fecha").Value
                  }
        Try
            For Each elements In qx
                Cabeceraa = New Cabecera
                Cabeceraa.Usuario = elements.Usuario
                Cabeceraa.Password = elements.Password
                Cabeceraa.Pedido = elements.Pedido
                Cabeceraa.Fecha = elements.Fecha
            Next

        Catch
            Return DevuelveError("000", "Error Estructura  XML ", "5011", "ConfirmacionEntradaInventario")
        End Try

        Try

            Dim qx2 = From xe In documento.Descendants.Elements("Linea")
        Select New With { _
                          .LineaPedido = xe.Element("LineaPedido").Value,
                          .CodigoProducto = xe.Element("CodigoProducto").Value,
                          .Cantidad = xe.Element("Cantidad").Value,
                          .Lote = xe.Element("Lote").Value
                            }
            For Each elementos In qx2
                ContadorLineas = ContadorLineas + 1
                nuevaLinea = New Detalle
                nuevaLinea.Cantidad = elementos.Cantidad
                If nuevaLinea.Cantidad <= 0 Then
                    Return DevuelveError(Cabeceraa.Pedido, "Cantidad no puede ser menor o igual que 0", 5012, "ConfirmacionEntradaInventario")
                End If
                nuevaLinea.LineaPedido = elementos.LineaPedido
                nuevaLinea.CodigoProducto = elementos.CodigoProducto
                nuevaLinea.Cantidad = elementos.Cantidad
                nuevaLinea.Lote = elementos.Lote
                Lineas.Add(nuevaLinea)

                Try

                    Dim fSql As String
                    Dim vSql As String
                    fSql = "SELECT * FROM RDCC INNER JOIN ECL ON DPEDID=LORD AND DLINEA=LLINE AND DPROD=LPROD"
                    fSql = fSql & " WHERE  DPEDID = " & Cabeceraa.Pedido & " AND DLINEA = " & nuevaLinea.LineaPedido
                    fSql = fSql & " AND DPROD = '" & nuevaLinea.CodigoProducto & "' "
                    rs.Open(fSql, DB_DB2)
                    If Not rs.EOF Then
                        If rs("LRQTY").Value = 0 Then
                            rs.Close()
                        Else
                            rs.Close()
                            Return DevuelveError(Cabeceraa.Pedido, " La linea No " & nuevaLinea.LineaPedido & " Producto " & nuevaLinea.CodigoProducto & " ya fue recibida ", "5001", "ConfirmacionEntradaInventario")
                        End If
                    Else
                        rs.Close()
                        Return DevuelveError(Cabeceraa.Pedido, " Pedido " & Cabeceraa.Pedido & " Linea " & nuevaLinea.LineaPedido & " Producto " & nuevaLinea.CodigoProducto & " No existe ", "5002", "ConfirmacionEntradaInventario")
                    End If
                Catch
                    Return DevuelveError("000", "Error No se pudo procesar algunas consultas", "5003", "ConfirmacionEntradaInventario")
                End Try
            Next



            Dim qx3 = From xe In documento.Descendants.Elements("Linea")
Select New With { _
              .LineaPedido = xe.Element("LineaPedido").Value,
              .CodigoProducto = xe.Element("CodigoProducto").Value,
              .Cantidad = xe.Element("Cantidad").Value,
              .Lote = xe.Element("Lote").Value
                }
            For Each elementos In qx3
                ContadorLineas = ContadorLineas + 1
                nuevaLinea = New Detalle
                nuevaLinea.Cantidad = elementos.Cantidad
                nuevaLinea.LineaPedido = elementos.LineaPedido
                nuevaLinea.CodigoProducto = elementos.CodigoProducto
                nuevaLinea.Cantidad = elementos.Cantidad
                nuevaLinea.Lote = elementos.Lote
                Lineas.Add(nuevaLinea)

                Try

                    Dim fSql As String
                    Dim vSql As String
                    fSql = "SELECT * FROM RDCC INNER JOIN ECL ON DPEDID=LORD AND DLINEA=LLINE AND DPROD=LPROD"
                    fSql = fSql & " WHERE  DPEDID = " & Cabeceraa.Pedido & " AND DLINEA = " & nuevaLinea.LineaPedido
                    fSql = fSql & " AND DPROD = '" & nuevaLinea.CodigoProducto & "' "
                    rs.Open(fSql, DB_DB2)
                    Dim THADNV As String
                    THADNV = CalcConsec("TRANSEC", False, "99999999")
                    fSql = " INSERT INTO PRDFTRIN( "
                    vSql = " VALUES ( "
                    fSql = fSql & " PRDTNV    , " : vSql = vSql & " " & DB2_DATEDEC & ", "                           '//8S0   Fecha Novedad
                    fSql = fSql & " PRTMNV    , " : vSql = vSql & " " & DB2_TIMEDEC0 & ", "                          '//6S0   Hora Novedad
                    fSql = fSql & " PRUSR     , " : vSql = vSql & "'" & rs("LTOWH").Value.ToString & "', "                           '//10A   Usuario
                    fSql = fSql & " PRPRID    , " : vSql = vSql & "'" & "SAT" & "', "                                '//3A    Identificación Producto
                    fSql = fSql & " PRACTL    , " : vSql = vSql & "'" & "Post" & "', "                               '//10A   Tipo de Actualizacion
                    fSql = fSql & " TTYPE     , " : vSql = vSql & "'" & "H" & "', "                                  '//2A    Transaction Type
                    fSql = fSql & " TPROD     , " : vSql = vSql & "'" & rs("DPROD").Value.ToString & "', "                          '//35O   Item Number
                    fSql = fSql & " TLOT      , " : vSql = vSql & " ( SELECT LLOT FROM RILNL01 WHERE LPROD = '" & rs("DPROD").Value.ToString & "' ), "  '//25O   Lot Number
                    fSql = fSql & " TWHS      , " : vSql = vSql & "'" & rs("LTOWH").Value.ToString & "', "                          '//3A    Warehouse
                    fSql = fSql & " TLOCT     , " : vSql = vSql & " ( SELECT ILOC FROM IIM WHERE IPROD = '" & rs("DPROD").Value.ToString & "' ), "  '//10O   Location
                    fSql = fSql & " TQTY      , " : vSql = vSql & " " & nuevaLinea.Cantidad & ", "                       '//11P3  Transaction Quantity
                    fSql = fSql & " TREF      , " : vSql = vSql & " " & rs("DPEDID").Value.ToString & ", "                       '//8P0   Order Number
                    fSql = fSql & " THLIN     , " : vSql = vSql & " " & rs("DLINEA").Value.ToString & ", "                       '//4P0   Order Line Number
                    fSql = fSql & " TTDTE     , " : vSql = vSql & " " & DB2_DATEDEC & ", "                       '//8P0   Transaction Date
                    fSql = fSql & " THADVN    ) " : vSql = vSql & "'" & THADNV & "' ) "       '//15O   Advice Note Number
                    DB_DB2.Execute(fSql & vSql)
                    rs.Close()


                Catch
                    Return DevuelveError("000", "Error No se pudo procesar algunas consultas consulta", "5003", "ConfirmacionEntradaInventario")
                End Try
            Next




            Return DevuelveResultado(Cabeceraa.Pedido, "ConfirmacionEntradaInventario")
        Catch

            Return DevuelveError("000", "Error Estructura  XML ", "5011", "ConfirmacionEntradaInventario")
        End Try
    End Function
    <WebMethod()> _
    Public Function PedidoDespachado(ByVal XML As String) As XmlDocument
        CargaPrm()
        C_ID = CalcConsec("RFLOGWSCDA", False, "99999999")
        wrtLogWsCDA(C_ID, XML, "Prueba", "", "PedidoDespachado", "wsBrinsa", "EM")
        ContadorLineas = 0
        Dim nuevaLinea As DetallePedidoDespachado
        Dim Cabeceraa As New CabeceraPedidoDespachado
        Dim Lineas As New List(Of DetallePedidoDespachado)
        Dim documento As XDocument = XDocument.Parse(XML)
        'Dim fSql As String
        '      Dim DB1 As New ExecuteSQL
        Dim qx = From xee In documento.Elements("PedidoDespachado")
       Select New With { _
                .Usuario = xee.Element("Usuario").Value,
                .Password = xee.Element("Password").Value,
                .Pedido = xee.Element("Pedido").Value
                  }
        Try
            For Each elements In qx
                Cabeceraa = New CabeceraPedidoDespachado
                Cabeceraa.Usuario = elements.Usuario
                Cabeceraa.Password = elements.Password
                Cabeceraa.Pedido = elements.Pedido

            Next

        Catch
            Return DevuelveError("000", "Error Estructura  XML ", "5011", "ConfirmacionEntradaInventario")
        End Try

        Try

            Dim qx2 = From xe In documento.Descendants.Elements("Linea")
        Select New With { _
                          .LineaPedido = xe.Element("LineaPedido").Value,
                          .Producto = xe.Element("Producto").Value,
                          .Lote = xe.Element("Producto").Value,
                          .Cantidad = xe.Element("Cantidad").Value
                            }
            For Each elementos In qx2
                ContadorLineas = ContadorLineas + 1
                nuevaLinea = New DetallePedidoDespachado
                nuevaLinea.Cantidad = elementos.Cantidad
                If nuevaLinea.Cantidad <= 0 Then
            Return DevuelveError("000", "Cantidad no puede ser menor o igual que 0", 106, "PedidoDespachado")
                End If
            nuevaLinea.LineaPedido = elementos.LineaPedido
            nuevaLinea.Producto = elementos.Producto
            nuevaLinea.Lote = elementos.Lote
            nuevaLinea.Cantidad = elementos.Cantidad
            Lineas.Add(nuevaLinea)
            Next
            ' Return DevuelveResultado(Cabeceraa.Pedido, "PedidoDespachado")
        Catch

            Return DevuelveError("000", "Error Estructura  XML ", "5011", "ConfirmacionEntradaInventario")
        End Try



        Try

            Dim qx3 = From xe In documento.Descendants.Elements("Linea")
        Select New With { _
                          .LineaPedido = xe.Element("LineaPedido").Value,
                          .Producto = xe.Element("Producto").Value,
                          .Lote = xe.Element("Producto").Value,
                          .Cantidad = xe.Element("Cantidad").Value
                            }
            For Each elementos In qx3
                ContadorLineas = ContadorLineas + 1
                nuevaLinea = New DetallePedidoDespachado
                nuevaLinea.Cantidad = elementos.Cantidad
                nuevaLinea.LineaPedido = elementos.LineaPedido
                nuevaLinea.Producto = elementos.Producto
                nuevaLinea.Lote = elementos.Lote
                nuevaLinea.Cantidad = elementos.Cantidad
                Lineas.Add(nuevaLinea)

                fSql = "SELECT * FROM RDCC"
                fSql = fSql & " WHERE DPEDID = " & Cabeceraa.Pedido & " AND DLINEA = " & nuevaLinea.LineaPedido & " AND DPROD = '" & nuevaLinea.Producto & "' "
                rs.Open(fSql, DB_DB2)
                If Not rs.EOF Then
                    If rs("DCANTE").Value = 0 Then
                        fSql = "UPDATE RDCC SET DCANTE =" & nuevaLinea.Cantidad
                        fSql = fSql & " WHERE DPEDID = " & Cabeceraa.Pedido & " AND DLINEA = " & nuevaLinea.LineaPedido & " AND DPROD = '" & nuevaLinea.Producto & "' "
                        DB_DB2.Execute(fSql)
                        rs.Close()
                    Else
                        rs.Close()
                        Return DevuelveError(Cabeceraa.Pedido, "Linea No" & nuevaLinea.LineaPedido & " ya Despachada", "5006", "ConfirmacionEntradaInventario")
                    End If
                Else
                    rs.Close()
                    Return DevuelveError(Cabeceraa.Pedido, "Linea No " & nuevaLinea.LineaPedido & " no existe", "5007", "ConfirmacionEntradaInventario")
                End If
                '                Si(DCANTE = 0)
                'UPDATE RDCC SET DCANTE = <Cantidad> 
                'WHERE DPEDID = <Pedido> AND DLINEA = <LineaPedido> 
                '	Sino
                '		Error
                'Sino esta
                '	Error
            Next
            Return DevuelveResultado(Cabeceraa.Pedido, "PedidoDespachado")
        Catch

            Return DevuelveError("000", "Error Estructura  XML ", "5011", "PedidoDespachado")
        End Try

    End Function
    <WebMethod()> _
    Public Function ReporteDespacho(ByVal XML As String) As XmlDocument
        CargaPrm()
        C_ID = CalcConsec("RFLOGWSCDA", False, "99999999")
        wrtLogWsCDA(C_ID, XML, "Prueba", "", "ReporteDespacho", "wsBrinsa", "EM")
        ContadorLineas = 0
        Dim Cabeceraa As New DetalleReporteDespacho
        Dim documento As XDocument = XDocument.Parse(XML)
        'Dim fSql As String
        '      Dim DB1 As New ExecuteSQL
        Dim qx = From xee In documento.Elements("ReporteDespacho")
       Select New With { _
                .Usuario = xee.Element("Usuario").Value,
                .Password = xee.Element("Password").Value,
                .NumeroUnico = xee.Element("NumeroUnico").Value,
                .Consolidado = xee.Element("Consolidado").Value,
                .EstadoDespacho = xee.Element("EstadoDespacho").Value,
                .FechaReporte = xee.Element("FechaReporte").Value,
                .Manifiesto = xee.Element("Manifiesto").Value,
                .ReporteGeneral = xee.Element("ReporteGeneral").Value
                  }
        Try
            For Each elements In qx
                Cabeceraa = New DetalleReporteDespacho
                Cabeceraa.Usuario = elements.Usuario
                Cabeceraa.Password = elements.Password
                Cabeceraa.NumeroUnico = elements.NumeroUnico
                Cabeceraa.Consolidado = elements.Consolidado
                If elements.EstadoDespacho = 60 Then
                    Cabeceraa.EstadoDespacho = 6
                Else
                    Cabeceraa.EstadoDespacho = elements.EstadoDespacho
                End If


                Cabeceraa.FechaReporte = elements.FechaReporte
                Cabeceraa.Manifiesto = elements.Manifiesto
                Cabeceraa.ReporteGeneral = elements.ReporteGeneral

                Dim Fechas As Date = Convert.ToDateTime(Cabeceraa.FechaReporte)
                Cabeceraa.FechaReporte = Fechas.ToString("yyyy-MM-dd-HH.mm.ss.000000")

                fSql = "SELECT * FROM RHCC WHERE HCONSO ='" & Cabeceraa.Consolidado & "'"
                rs.Open(fSql, DB_DB2)
                If Not rs.EOF Then

                    fSql = "UPDATE RHCC SET HESTAD =" & Cabeceraa.EstadoDespacho & ", HFESTAD='" & Cabeceraa.FechaReporte & "'"
                    fSql = fSql & " WHERE HCONSO = '" & Cabeceraa.Consolidado & "'"
                    DB_DB2.Execute(fSql)
                    rs.Close()
                Else
                rs.Close()
                Return DevuelveError(Cabeceraa.Consolidado, "No existe Consolidado", "5009", "ConfirmacionEntradaInventario")
                End If

                fSql = " INSERT INTO RRCC( "
                vSql = " VALUES ( "
                fSql = fSql & " RTREP     , " : vSql = vSql & " 1, "     '//2S0   Tipo Rep
                fSql = fSql & " RCONSO    , " : vSql = vSql & "'" & Cabeceraa.Consolidado & "', "     '//6A    Consolidado
                fSql = fSql & " RSTSC     , " : vSql = vSql & " (SELECT HESTAD FROM RHCC WHERE HCONSO = '" & Cabeceraa.Consolidado & "'), "     '//2S0   Est Cons
                fSql = fSql & " RREPORT   , " : vSql = vSql & "'" & Cabeceraa.ReporteGeneral & "', "     '//502A  Reporte
                fSql = fSql & " RCRTUSR   , " : vSql = vSql & "'WSCDA', "     '//10A   Usuario
                fSql = fSql & " RAPP      ) " : vSql = vSql & "'WSCDA') "     '//15A   App
                DB_DB2.Execute(fSql & vSql)

            Next
        Catch
            Return DevuelveError("000", "Error Estructura  XML ", "5011", "ReporteDespacho")
        End Try
        Return DevuelveResultado(Cabeceraa.Consolidado, "ReporteDespacho")
    End Function

    <WebMethod()> _
    Public Function ReporteEntrega(ByVal XML As String) As XmlDocument
        CargaPrm()
        C_ID = CalcConsec("RFLOGWSCDA", False, "99999999")
        wrtLogWsCDA(C_ID, XML, "Prueba", "", "ReporteEntrega", "wsBrinsa", "EM")

        ContadorLineas = 0
        Dim Cabeceraa As New DetalleReporteEntrega
        Dim documento As XDocument = XDocument.Parse(XML)
        'Dim fSql As String
        '      Dim DB1 As New ExecuteSQL
        Dim qx = From xee In documento.Elements("ReporteEntrega")
       Select New With { _
                .Usuario = xee.Element("Usuario").Value,
                .Password = xee.Element("Password").Value,
                .Planilla = xee.Element("Planilla").Value,
                .FechaReporte = xee.Element("FechaReporte").Value,
                .EstadoEntrega = xee.Element("EstadoEntrega").Value,
                .Reporte = xee.Element("Reporte").Value
                   }
        Try
            For Each elements In qx
                Cabeceraa = New DetalleReporteEntrega
                Cabeceraa.Usuario = elements.Usuario
                Cabeceraa.Password = elements.Password
                Cabeceraa.Planilla = elements.Planilla
                Cabeceraa.FechaReporte = elements.FechaReporte
                Cabeceraa.EstadoEntrega = elements.EstadoEntrega
                Cabeceraa.Reporte = elements.Reporte

                Dim Fechas As Date = Convert.ToDateTime(Cabeceraa.FechaReporte)
                Cabeceraa.FechaReporte = Fechas.ToString("yyyy-MM-dd-HH.mm.ss.000000")
                fSql = "SELECT * FROM RECC WHERE EPLANI =" & Cabeceraa.Planilla
                rs.Open(fSql, DB_DB2)
                Dim consos As String
                consos = rs("ECONSO").Value
                If Not rs.EOF Then
                    Select Case Cabeceraa.EstadoEntrega

                        Case 35
                            fSql = "UPDATE RECC SET ESTS =" & Cabeceraa.EstadoEntrega & ", ELLECLI = '" & Cabeceraa.FechaReporte & "'"
                            fSql = fSql & " WHERE EPLANI = " & Cabeceraa.Planilla
                            DB_DB2.Execute(fSql)
                            rs.Close()

                        Case 50

                            fSql = "UPDATE RECC SET ESTS =" & Cabeceraa.EstadoEntrega & ", EFINENT = '" & Cabeceraa.FechaReporte & "'"
                            fSql = fSql & " WHERE EPLANI = " & Cabeceraa.Planilla
                            DB_DB2.Execute(fSql)
                            rs.Close()

                        Case Else

                            fSql = "UPDATE RECC SET ESTS =" & Cabeceraa.EstadoEntrega
                            fSql = fSql & " WHERE EPLANI = " & Cabeceraa.Planilla
                            DB_DB2.Execute(fSql)
                            rs.Close()

                    End Select

                  Else
                            rs.Close()
                            Return DevuelveError(Cabeceraa.Planilla, "No existe planilla", "5008", "ConfirmacionEntradaInventario")
                End If



                fSql = " INSERT INTO RRCC( "
                vSql = " VALUES ( "
                fSql = fSql & " RTREP     , " : vSql = vSql & " 1, "     '//2S0   Tipo Rep
                fSql = fSql & " RCONSO    , " : vSql = vSql & "'" & consos & "', "     '//6A    Consolidado
                fSql = fSql & " RPLANI    , " : vSql = vSql & " " & Cabeceraa.Planilla & ", "     '//6S0   Planilla
                fSql = fSql & " RSTSC     , " : vSql = vSql & " (SELECT HESTAD FROM RHCC WHERE HCONSO = '" & consos & "'), "     '//2S0   Est Cons
                fSql = fSql & " RREPORT   , " : vSql = vSql & "'" & Cabeceraa.Reporte & "', "     '//502A  Reporte
                fSql = fSql & " RCRTUSR   , " : vSql = vSql & "'WSCDA', "     '//10A   Usuario
                fSql = fSql & " RAPP      ) " : vSql = vSql & "'WSCDA') "     '//15A   App
                DB_DB2.Execute(fSql & vSql)

            Next
        Catch
            Return DevuelveError("000", "Error Estructura  XML ", "5011", "ReporteEntrega")
        End Try
        Return DevuelveResultado(Cabeceraa.Planilla, "ReporteEntrega")
    End Function

    <WebMethod()> _
    Public Function Despacho(ByVal XML As String) As XmlDocument
        CargaPrm()
        C_ID = CalcConsec("RFLOGWSCDA", False, "99999999")
        wrtLogWsCDA(C_ID, XML, "Inicio", "", "Despacho", "wsBrinsa", "EM")
        ContadorLineas = 0
        Dim nuevaLinea As DetalleDespacho
        Dim Cabeceraa As New CabeceraDespacho
        Dim Lineas As New List(Of DetalleDespacho)
        Dim documento As XDocument = XDocument.Parse(XML)
        'Dim fSql As String
        '      Dim DB1 As New ExecuteSQL
        Dim qx = From xee In documento.Elements("Despacho")
       Select New With { _
        .Usuario = xee.Element("Usuario").Value,
        .Password = xee.Element("Password").Value,
        .Manifiesto = xee.Element("Manifiesto").Value,
        .Bodega = xee.Element("Bodega").Value,
        .TipoVehiculo = xee.Element("TipoVehiculo").Value,
        .Notas = xee.Element("Notas").Value,
        .DescTipoVehiculo = xee.Element("DescTipoVehiculo").Value,
        .Placa = xee.Element("Placa").Value,
        .CedulaConductor = xee.Element("CedulaConductor").Value,
        .NombreConductor = xee.Element("NombreConductor").Value,
        .CelularConductor = xee.Element("CelularConductor").Value
                  }
        Try
            For Each elements In qx
                Cabeceraa = New CabeceraDespacho

                Cabeceraa.Usuario = elements.Usuario
                Cabeceraa.Password = elements.Password
                Cabeceraa.Manifiesto = elements.Manifiesto
                Cabeceraa.Bodega = elements.Bodega
                Cabeceraa.TipoVehiculo = elements.TipoVehiculo
                Cabeceraa.Notas = elements.Notas
                Cabeceraa.DescTipoVehiculo = elements.DescTipoVehiculo
                Cabeceraa.Placa = elements.Placa
                Cabeceraa.CedulaConductor = elements.CedulaConductor
                Cabeceraa.NombreConductor = elements.NombreConductor
                Cabeceraa.CelularConductor = elements.CelularConductor
            Next

        Catch
            Return DevuelveError("000", "Error Estructura  XML ", "5011", "Despacho")
        End Try

        wrtLogWsCDA(C_ID, XML, Cabeceraa.Manifiesto, "", "Despacho", "wsBrinsa", "EM")
        consolidado = ""
        Try
            Dim qx2 = From xe In documento.Descendants.Elements("PedidoDespachado")
        Select New With { _
                          .Pedido = xe.Element("Pedido").Value
                            }
            For Each elementos In qx2
                ContadorLineas = ContadorLineas + 1
                nuevaLinea = New DetalleDespacho
                nuevaLinea.Pedido = elementos.Pedido
                Lineas.Add(nuevaLinea)

                fSql = "SELECT * FROM RDCC WHERE DPEDID = " & nuevaLinea.Pedido & ""
                rs.Open(fSql, DB_DB2)
                If Not rs.EOF Then
                    If rs("DCONSO").Value <> "" Then
                        rs.Close()
                        Return DevuelveError(Cabeceraa.Manifiesto, "Pedido ya Consolidado", "5004", "Despacho")
                    Else
                    End If
                    rs.Close()
                Else
                    rs.Close()
                    Return DevuelveError(Cabeceraa.Manifiesto, "Pedido no Existe", "5005", "Despacho")
                End If
            Next

            fSql = "SELECT CCCODE FROM ZCC WHERE CCTABL = 'SPTIPVEH' AND CCENUS = '" & Cabeceraa.TipoVehiculo & "'"
            rs.Open(fSql, DB_DB2)
            If rs.EOF Then
                Return DevuelveError(Cabeceraa.Manifiesto, "Tipo de Vehiculo erroneo", "5010", "Despacho")
            End If
            rs.Close()


        Catch

            Return DevuelveError("000", "Error Estructura  XML ", "5011", "Despacho")
        End Try


        Try
            Dim qx3 = From xe In documento.Descendants.Elements("PedidoDespachado")
        Select New With { _
                          .Pedido = xe.Element("Pedido").Value
                            }
            consolidado = CalcConsec("SPCONSO", False, "999999")
            For Each elementos In qx3
                ContadorLineas = ContadorLineas + 1
                nuevaLinea = New DetalleDespacho
                nuevaLinea.Pedido = elementos.Pedido
                Lineas.Add(nuevaLinea)

                fSql = "SELECT * FROM RECC WHERE ECONSO = '" & consolidado & "' AND ESTS NOT IN( 50, 60 )"
                rs.Open(fSql, DB_DB2)
                If Not rs.EOF Then

                    Return DevuelveError(Cabeceraa.Manifiesto, "Consolidado no ha sido totalmente despachado", "5013", "Despacho")

                End If
                rs.Close()
                fSql = "SELECT * FROM RDCC WHERE DPEDID = " & nuevaLinea.Pedido & ""
                rs.Open(fSql, DB_DB2)


                fSql = "UPDATE RDCC SET DCONSO = '" & consolidado & "' WHERE DPEDID = " & nuevaLinea.Pedido & " AND UPRCON = 0"
                DB_DB2.Execute(fSql)
                rs.Close()
            Next

            fSql = " INSERT INTO RHCC( "
            vSql = " VALUES ( "
            fSql = fSql & " HSFCNS    , " : vSql = vSql & "'SAT', "     '//3A    Tipo Consolidado
            fSql = fSql & " HCONSO    , " : vSql = vSql & "'" & consolidado & "', "     '//6A    Consolidado
            fSql = fSql & " HPLACA    , " : vSql = vSql & "'" & Cabeceraa.Placa & "', "     '//6A    Placa
            fSql = fSql & " HPROVE    , " : vSql = vSql & "5013" & ", "     '//8P0   Transp
            fSql = fSql & " HNPROVE   , " : vSql = vSql & "'COLTANQUES', "     '//50A   Transportador
            fSql = fSql & " HCEDUL    , " : vSql = vSql & " " & Cabeceraa.CedulaConductor & ", "     '//18P0  Cedula Conductor
            fSql = fSql & " HCHOFE    , " : vSql = vSql & "'" & Cabeceraa.NombreConductor & "', "     '//30A   Conductor
            fSql = fSql & " HCELU     , " : vSql = vSql & "'" & Cabeceraa.CelularConductor & "', "     '//20A   Celular
            fSql = fSql & " HGENERA   , " : vSql = vSql & "'0', "     '//1A    Generada OC S/N
            fSql = fSql & " HMANIF    , " : vSql = vSql & "'" & Cabeceraa.Manifiesto & "', "     '//15A   Manifiesto del Transp
            fSql = fSql & " HTIPO     , " : vSql = vSql & " (SELECT CCCODE FROM ZCC WHERE CCTABL = 'SPTIPVEH' AND CCENUS = '" & Cabeceraa.TipoVehiculo & "')" & ", "     '//2P0   Tipo Vehiculo
            fSql = fSql & " HOBSER    , " : vSql = vSql & "'" & Cabeceraa.Notas & "', "     '//120A  Observaciones
            fSql = fSql & " HBOD      , " : vSql = vSql & "'" & Cabeceraa.Bodega & "', "     '//3A    Bodega
            fSql = fSql & " HREF05    ) " : vSql = vSql & "'" & Cabeceraa.Bodega & "' )"     '//15A   Bodega
            DB_DB2.Execute(fSql & vSql)
            CreaRECC(consolidado)


            fSql = "SELECT HESTAD FROM RHCC WHERE HCONSO = '" & consolidado & "'"
            rs.Open(fSql, DB_DB2)
            If rs.EOF Then
                Return DevuelveError(Cabeceraa.Manifiesto, "Consolidado No existe", "5013", "Despacho")
            End If
            rs.Close()



            fSql = " INSERT INTO RRCC( "
            vSql = " VALUES ( "
            fSql = fSql & " RTREP     , " : vSql = vSql & " 2, "     '//2S0   Tipo Rep
            fSql = fSql & " RCONSO    , " : vSql = vSql & "'" & consolidado & "', "     '//6A    Consolidado
            fSql = fSql & " RMANIF    , " : vSql = vSql & "'" & Cabeceraa.Manifiesto & "', "     '//15A   Manifiesto del Transp
            fSql = fSql & " RPLACA    , " : vSql = vSql & "'" & Cabeceraa.Placa & "', "     '//6A    Placa
            fSql = fSql & " RSTSC     , " : vSql = vSql & " (SELECT HESTAD FROM RHCC WHERE HCONSO = '" & consolidado & "'), "     '//2S0   Est Cons
            fSql = fSql & " RREPORT   , " : vSql = vSql & "'Consolidado generado.', "     '//502A  Reporte
            fSql = fSql & " RCRTUSR   , " : vSql = vSql & "'WSCDA', "     '//10A   Usuario
            fSql = fSql & " RAPP      ) " : vSql = vSql & "'WSCDA') "     '//15A   App
            DB_DB2.Execute(fSql & vSql)





            Return DevuelveResultado(Cabeceraa.Manifiesto, "Despacho")
        Catch
            Return DevuelveError("000", "Error Estructura  XML ", "5011", "Despacho")
        End Try


    End Function
    Function DevuelveError(ByVal NumeroDocumento As String, ByVal ERROOR As String, ByVal CODIGO As String, ByVal proceso As String) As XmlDocument
        ' Dim fSql As String
        Dim xmlstr As String = ""
        Dim sw As New StringWriter()
        Dim writer As New XmlTextWriter(sw)
        Dim flag As Integer = 0
        Dim flag2 As Integer = 0

        Dim rs As New ADODB.Recordset

        'writer.WriteStartDocument(True)
        writer.Formatting = Formatting.Indented
        writer.Indentation = 2
        writer.WriteStartElement("Respuesta")
        writer.WriteStartElement("Operacion")
        writer.WriteString(proceso)
        writer.WriteEndElement()
        If (proceso = "Despacho") Then
            writer.WriteStartElement("Consolidado")
            writer.WriteString(consolidado)
            writer.WriteEndElement()
            writer.WriteStartElement("Manifiesto")
        Else
            If (proceso = "ReporteDespacho") Then
                writer.WriteStartElement("Consolidado")
            Else
                If (proceso <> "ReporteEntrega") Then
                    writer.WriteStartElement("Pedido")
                Else
                    writer.WriteStartElement("Planilla")
                End If
            End If
        End If
        writer.WriteString(NumeroDocumento)
        writer.WriteEndElement()
        If (proceso <> "ConfirmacionEntradaInventario" And proceso <> "PedidoDespachado" And proceso <> "ReporteDespacho" And proceso <> "ReporteEntrega" And proceso <> "Despacho") Then
            writer.WriteStartElement("TotalLineas")
            writer.WriteString(ContadorLineas)
            writer.WriteEndElement()
        End If
        writer.WriteStartElement("Mensaje")
        writer.WriteString(CODIGO)
        writer.WriteEndElement()
        writer.WriteEndElement()

        writer.Flush()
        xmlstr = sw.ToString
        writer.Close()
        Dim pDoc As New XmlDocument
        pDoc.LoadXml(xmlstr)
        wrtLogWsCDA(C_ID, sw.ToString, "", "Fail", proceso, "wsBrinsa", "EM")

        Return pDoc



    End Function

    Function CrearNodoError(ByVal ErrorXML As String, ByVal codigo As String, ByVal NumeroDocumento As String, ByVal proceso As String, ByVal writer As XmlTextWriter)


        writer.WriteStartElement(proceso) ' ENVELOPE
        writer.WriteStartElement("Documento")  ' PEDIDO
        writer.WriteString(NumeroDocumento)
        writer.WriteStartElement("Mensaje")     'MENSAJE
        writer.WriteStartElement("Error")       'ERROR
        writer.WriteStartElement("MensajeID")   'ID
        writer.WriteString(codigo)
        writer.WriteEndElement() ' MensajeId
        writer.WriteStartElement("MensajeDES")
        writer.WriteString(ErrorXML)
        writer.WriteEndElement() ' DES
        writer.WriteEndElement() ' ERROR
        writer.WriteEndElement() ' MENSAJE
        writer.WriteEndElement() ' PEDIDO
        writer.WriteEndElement() ' ENVELOPE
        Return 0
    End Function

    Function DevuelveResultado(ByVal NumeroDocumento As String, ByVal proceso As String) As XmlDocument
        ' Dim fSql As String
        Dim xmlstr As String = ""
        Dim sw As New StringWriter()
        Dim writer As New XmlTextWriter(sw)
        Dim flag As Integer = 0
        Dim flag2 As Integer = 0

        Dim rs As New ADODB.Recordset

        'writer.WriteStartDocument(True)
        writer.Formatting = Formatting.Indented
        writer.Indentation = 2
        writer.WriteStartElement("Respuesta")
        writer.WriteStartElement("Operacion")
        writer.WriteString(proceso)
        writer.WriteEndElement()
        If (proceso = "Despacho") Then
            writer.WriteStartElement("Consolidado")
            writer.WriteString(consolidado)
            writer.WriteEndElement()
            writer.WriteStartElement("Manifiesto")
        Else
            If (proceso = "ReporteDespacho") Then
                writer.WriteStartElement("Consolidado")
            Else
                If (proceso <> "ReporteEntrega") Then
                    writer.WriteStartElement("Pedido")
                Else
                    writer.WriteStartElement("Planilla")
                End If
            End If
        End If
        writer.WriteString(NumeroDocumento)
        writer.WriteEndElement()
        If (proceso <> "ConfirmacionEntradaInventario" And proceso <> "PedidoDespachado" And proceso <> "ReporteDespacho" And proceso <> "ReporteEntrega" And proceso <> "Despacho") Then
            writer.WriteStartElement("TotalLineas")
            writer.WriteString(ContadorLineas)
            writer.WriteEndElement()
        End If
        writer.WriteStartElement("Mensaje")
        writer.WriteString("0000")
        writer.WriteEndElement()
        writer.WriteEndElement()

        writer.Flush()
        xmlstr = sw.ToString
        writer.Close()
        Dim pDoc As New XmlDocument
        pDoc.LoadXml(xmlstr)
        wrtLogWsCDA(C_ID, sw.ToString, "", "Pass", proceso, "wsBrinsa", "EM")

        Return pDoc

    End Function



    Private Sub InitializeComponent()

    End Sub

    Function CargaPrm() As Boolean


        gsProvider = "IBMDA400"
        gsDatasource = My.Settings.AS400
        sLib = My.Settings.AMBIENTE

        gsConnString = "Provider=" & gsProvider & ";Data Source=" & gsDatasource & ";"
        gsConnString = gsConnString & "Persist Security Info=False;Default Collection=" & sLib & ";"
        gsConnString = gsConnString & "Password=LXAPL;User ID=APLLX;"

        DB_DB2 = New ADODB.Connection
        DB_MP2 = New ADODB.Connection
        DB_DB2.Open(gsConnString)

        rs.Open(" SELECT * FROM " & sLib & ".ZCC WHERE CCTABL = 'LXLONG'", DB_DB2)
        Do While Not rs.EOF
            Select Case rs("CCCODE").Value.ToString
                Case "LXUSER"
                    AS400Param(0) = rs("CCDESC").Value.ToString
                Case "LXPASS"
                    AS400Param(1) = rs("CCDESC").Value.ToString
            End Select
            rs.MoveNext()
        Loop
        rs.Close()

        For i = 0 To 1
            If AS400Param(i) = "" Then
                Resultado = False
            End If
        Next

        gsConnString = "Provider=" & gsProvider & ";Data Source=" & gsDatasource & ";"
        gsConnString = gsConnString & "Persist Security Info=False;Default Collection=" & sLib & ";"
        gsConnString = gsConnString & "Password=" & AS400Param(1) & ";User ID=" & AS400Param(0) & ";"
        gsConnString = gsConnString & "Force Translate=0;"
        DB_DB2.Close()
        If Resultado Then
            DB_DB2.Open(gsConnString)
        Else
            'Error Conexion
        End If

        Return Resultado

    End Function
    Public Function CalcConsec(ByRef sID As String, ByRef bLee As Boolean, ByRef TopeMax As String) As Double
        Dim rs As ADODB.Recordset
        Dim locID As Double
        Dim sql As String
        Dim sConsec As String
        Dim tMax As Double

        sql = "SELECT CCDESC FROM ZCCL01 WHERE CCTABL = 'SECUENCE' AND CCCODE='" & sID & "'"
        rs = New ADODB.Recordset
        tMax = Val(TopeMax)

        With rs
            .CursorLocation = ADODB.CursorLocationEnum.adUseServer
            .Open(sql, DB_DB2, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)
            If Not (.BOF And .EOF) Then
                locID = Val(.Fields("CCDESC").Value) + 1
                If locID > tMax Then
                    locID = 1
                End If
                sConsec = Format(locID, New String("0", Len(TopeMax)))
                If Not bLee Then
                    .Fields("CCDESC").Value = sConsec
                    .Update()
                End If
                .Close()
            Else
                locID = 1
                .Close()
                sConsec = Format(1, New String("0", Len(8)))
                DB_DB2.Execute(" INSERT INTO ZCC (CCID, CCTABL, CCCODE, CCDESC ) " & " VALUES( 'CC', 'SECUENCE', '" & sID & "', '" & sConsec & "' )")
            End If
        End With

        CalcConsec = locID

    End Function

    Sub wrtLogWsCDA(ByVal logId As String, ByVal xml As String, ByVal id_msg As String, ByVal estado As String, ByVal operacion As String, ByVal fuente As String, ByVal usuario As String)
        Dim rs As New ADODB.RecordSet
        Dim dato() As Byte
        dato = System.Text.Encoding.UTF8.GetBytes("<?xml version=""1.0""?>" & xml)
        With rs
            .CursorLocation = ADODB.CursorLocationEnum.adUseServer
            .Open("SELECT * FROM RFLOGWSCDA WHERE C_ID = " & logId, DB_DB2, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)
            If .EOF Then
                .AddNew()
                .Fields("C_ID").Value = logId
                .Fields("C_XML").Value = dato
                .Fields("C_NOUN_ID").Value = operacion
                .Fields("C_MESSAGEID").Value = id_msg
                .Fields("C_SOURCE_NAME").Value = fuente
                .Fields("C_USER").Value = usuario
                .Fields("C_BODID").Value = ""
            Else
                If id_msg = "" Then

                    .Fields("C_WAS_PROCESSED").Value = 1
                    .Fields("C_REPLY").Value = dato
                    .Fields("C_PASS_FAIL").Value = estado
                Else
                    .Fields("C_MESSAGEID").Value = id_msg
                End If

            End If
            .Update()
            .Close()
        End With
        rs = Nothing
    End Sub

    Public Sub CreaRECC(ByVal Consolidado As String)

        fSql = "UPDATE RDCC SET DORDENT=0 "
        fSql = fSql & " WHERE "
        fSql = fSql & " DCONSO='" & Consolidado & "' "
        DB_DB2.Execute(fSql)

        fSql = " INSERT INTO RECC("
        vSql = " SELECT"
        fSql = fSql & " ECONSO," : vSql = vSql & " DCONSO,"
        fSql = fSql & " EPEDID," : vSql = vSql & " DPEDID,"
        fSql = fSql & " EPLANI," : vSql = vSql & " DPLANI,"
        fSql = fSql & " EORDENT," : vSql = vSql & " MAX(DORDENT),"
        fSql = fSql & " EIDIVI," : vSql = vSql & " DIDIVI,"
        fSql = fSql & " ECUST," : vSql = vSql & " DCUST,"
        'fSql = fSql & " EHNAME,":        vSql = vSql & " DHNAME,"
        fSql = fSql & " ESHIP," : vSql = vSql & " DSHIP,"
        'fSql = fSql & " ENSHIP,":        vSql = vSql & " DNSHIP,"
        'fSql = fSql & " ESPOST,":        vSql = vSql & " DSPOST,"
        'fSql = fSql & " ESHDEP,":        vSql = vSql & " DSHDEP,"
        fSql = fSql & " EPESOD," : vSql = vSql & " SUM( DPESO ), "
        fSql = fSql & " ECANTD )" : vSql = vSql & " SUM( DCANT )"
        vSql = vSql & " FROM RDCC "
        vSql = vSql & " WHERE "
        vSql = vSql & " DCONSO='" & Consolidado & "'"
        vSql = vSql & " AND DNUMENT=0"
        vSql = vSql & " GROUP BY DCONSO, DPEDID, DPLANI, DIDIVI, "
        'vSql = vSql & "          DCUST, DHNAME, DSHIP , DNSHIP, DSPOST, DSHDEP"
        vSql = vSql & "          DCUST, DSHIP "
        DB_DB2.Execute(fSql & vSql)

        fSql = "        UPDATE RECC E SET ( EHNAME, ENSHIP, ESPOST, ESHDEP  ) = ("
        fSql = fSql & "    SELECT TCUSTNAME, TNAME, TPOST, TSTE "
        fSql = fSql & "    FROM RVESTTOT WHERE E.ECUST=TCUST AND E.ESHIP=TSHIP )"
        fSql = fSql & " WHERE "
        fSql = fSql & " ECONSO='" & Consolidado & "'"
        DB_DB2.Execute(fSql)

        fSql = "UPDATE RDCC D SET ( DNUMENT ) = ("
        fSql = fSql & " SELECT EID FROM RECC WHERE D.DCONSO = ECONSO AND D.DPEDID = EPEDID "
        fSql = fSql & " AND D.DIDIVI=EIDIVI AND D.DCUST=ECUST AND D.DSHIP=ESHIP )"
        fSql = fSql & " WHERE "
        fSql = fSql & " DCONSO='" & Consolidado & "' AND DNUMENT=0"
        DB_DB2.Execute(fSql)


    End Sub
End Class