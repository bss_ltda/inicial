﻿Imports Microsoft.Office.Interop
Imports Microsoft.Office.Interop.Excel
Module BSSExportExcel



    '==========================================================================================================================
    '  Exporta a EXCEL
    '==========================================================================================================================
    Public Sub ReportXls(sql As String, sTit As String)
        Dim DB_LOC As New ADODB.Connection
        Dim rs As New ADODB.Recordset
        Dim aTit() As String
        Dim i As Integer

        With rs
            .CursorLocation = ADODB.CursorLocationEnum.adUseClient
            On Error Resume Next
            .Open(sql, DB_DB2, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockReadOnly)
            If Err.Number <> 0 Then
                MsgBox(.Source & vbCr & Err.Description)
                Exit Sub
            End If
            On Error GoTo 0
        End With

        'Create a new workbook in Excel
        Dim oExcel As Object
        Dim oBook As Object
        Dim oSheet As Object
        oExcel = CreateObject("Excel.Application")
        oBook = oExcel.Workbooks.Add
        oSheet = oBook.Worksheets(1)

        'Transfer the data to Excel
        aTit = Split(sTit, "|")
        If UBound(aTit) > 0 Then
            For i = 0 To rs.Fields.Count - 1
                oSheet.Cells(1, i + 1) = aTit(i + 1)
                oSheet.Cells(1, i + 1).Font.Bold = True
            Next
        Else
            For i = 0 To rs.Fields.Count - 1
                oSheet.Cells(1, i + 1) = (rs.Fields(i).Name) 'TitCampo(rs.Fields(i).Name)
                oSheet.Cells(1, i + 1).Font.Bold = True
            Next
        End If

        oSheet.Range("A2").CopyFromRecordset(rs)
        oSheet.ListObjects.Add(XlListObjectSourceType.xlSrcRange, oSheet.Range(oSheet.Cells(1, 1), _
                               oSheet.Cells(rs.RecordCount + 1, rs.Fields.Count)), , XlYesNoGuess.xlYes).Name = "Tabla1"
        oSheet.ListObjects("Tabla1").TableStyle = "TableStyleMedium2"

        oSheet.Cells.EntireColumn.AutoFit()
        oSheet.Cells.Rows("1:1").VerticalAlignment = XlVAlign.xlVAlignTop

        oExcel.Visible = True
        'oBook.SaveAs Replace(sXLS & "\Book" & Format(Now, "hhMMss") & ".xlsx", "\\", "\")
        'oExcel.Quit
        oExcel = Nothing

        rs.Close()
        rs = Nothing

    End Sub



End Module
