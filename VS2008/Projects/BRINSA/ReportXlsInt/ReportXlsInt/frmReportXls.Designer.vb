﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReportXls
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtRepVentas = New System.Windows.Forms.TextBox()
        Me.ButtonExcelPrecInd = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(16, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Rep Ventas"
        '
        'txtRepVentas
        '
        Me.txtRepVentas.Location = New System.Drawing.Point(85, 22)
        Me.txtRepVentas.Name = "txtRepVentas"
        Me.txtRepVentas.Size = New System.Drawing.Size(101, 20)
        Me.txtRepVentas.TabIndex = 1
        '
        'ButtonExcelPrecInd
        '
        Me.ButtonExcelPrecInd.Location = New System.Drawing.Point(196, 22)
        Me.ButtonExcelPrecInd.Name = "ButtonExcelPrecInd"
        Me.ButtonExcelPrecInd.Size = New System.Drawing.Size(64, 19)
        Me.ButtonExcelPrecInd.TabIndex = 2
        Me.ButtonExcelPrecInd.Text = "Generar"
        Me.ButtonExcelPrecInd.UseVisualStyleBackColor = True
        '
        'frmReportXls
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(396, 306)
        Me.Controls.Add(Me.ButtonExcelPrecInd)
        Me.Controls.Add(Me.txtRepVentas)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmReportXls"
        Me.Text = "Reportes Excel"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtRepVentas As System.Windows.Forms.TextBox
    Friend WithEvents ButtonExcelPrecInd As System.Windows.Forms.Button

End Class
