﻿Public Class frmReportXls


    Private Sub ButtonExcelPrecInd_Click(sender As System.Object, e As System.EventArgs) Handles ButtonExcelPrecInd.Click
        DB_DB2 = New ADODB.Connection
        DB_DB2.Open("Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=ERPLXSSA;Password=ABR2013;Persist Security Info=True;Default Collection=ERPLX834F;Force Translate=0;")

        'sbmLXjva("PIND", "ChkPrecIndustria", "RJPRCHKPIND", "-batch " & BATCH, "ORDERPOST")

        fSql = " SELECT " : vSql = ""
        fSql = fSql & " LVENDE    , " : vSql = vSql & "|Vendedor"
        fSql = fSql & " LCLIEN    , " : vSql = vSql & "|Cliente"
        fSql = fSql & " LNOMCLI   , " : vSql = vSql & "|Nombre Cliente"
        fSql = fSql & " LPTOEN    , " : vSql = vSql & "|Pto.Envio"
        fSql = fSql & " LNOMPTE   , " : vSql = vSql & "|Nombre Pto.Env"
        fSql = fSql & " LPROD     , " : vSql = vSql & "|Producto"
        fSql = fSql & " LLINEA    , " : vSql = vSql & "|Linea"
        fSql = fSql & " LINAME    , " : vSql = vSql & "|Descripcion Producto"
        fSql = fSql & " LPREUM    , " : vSql = vSql & "|Precio Lista UM"
        fSql = fSql & " LPRETO    , " : vSql = vSql & "|Precio Lista TO"
        fSql = fSql & " LFECIN    , " : vSql = vSql & "|Fecha Inicio Precio"
        fSql = fSql & " LFECFI    , " : vSql = vSql & "|Fecha Final Precio"
        fSql = fSql & " LPLAZO    , " : vSql = vSql & "|Plazo"
        fSql = fSql & " LMETH     , " : vSql = vSql & "|Metodo"
        fSql = fSql & " LUVEND      " : vSql = vSql & "|Solicito"
        fSql = fSql & " FROM RLISPRE01 INNER JOIN IIM"
        fSql = fSql & " ON LPROD = IPROD "
        fSql = fSql & " WHERE "
        fSql = fSql & "  LFLUJO = 'BA'"
        fSql = fSql & "  AND FOWNER = 'BPCS'"
        fSql = fSql & "  AND LVENDE = " & Me.txtRepVentas.Text
        fSql = fSql & "  AND  LPROD <> ''"
        fSql = fSql & " ORDER BY LNOMCLI asc, LNOMPTE asc"

        sbmLXjva()

        ReportXls(fSql, vSql)
        DB_DB2.Close()

    End Sub

    'sbmLXjva()
    Function sbmLXjva()
        Dim fSql As String = ""

        fSql = fSql & " RUNJVA CLASS(ChkPrecIndustria)  "
        fSql = fSql & " PARM('-lib' 'ERPLX834F' '-batch' '000000' ) "
        fSql = fSql & " CLASSPATH('/ERPLX834F/:/ERPLX834F/RJPRCHKPIND.jar:"
        fSql = fSql & "/BSS/lib/commons-cli-1.2.jar:"
        fSql = fSql & "/LXCONNECTOR_192.168.10.1-ERPLX834EC/LxBean.jar:"
        fSql = fSql & "/LXCONNECTOR_192.168.10.1-ERPLX834EC/LXCRuntime.jar:"
        fSql = fSql & "/LXCONNECTOR_192.168.10.1-ERPLX834EC/LXCPI.jar:"
        fSql = fSql & "/LXCONNECTOR_192.168.10.1-ERPLX834EC/lib/jt400.jar:"
        fSql = fSql & "/LXCONNECTOR_192.168.10.1-ERPLX834EC/lib/mailapi.jar:"
        fSql = fSql & "/LXCONNECTOR_192.168.10.1-ERPLX834EC/lib/smtp.jar')"

        DB_DB2.Execute("{{" & fSql & "}}")

    End Function

End Class
