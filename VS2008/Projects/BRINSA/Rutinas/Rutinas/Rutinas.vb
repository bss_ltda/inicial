﻿Imports BSSClassLibrary

Module Rutinas

    Dim db As New bssConexion
    Dim sql As New bssSQL
    Dim u As New bssUtil

    Sub Main(ByVal Parametros As String())

        db.abrirConexion(My.Settings.AMBIENTE, "AS400")
        Select Case Parametros(0)
            Case "INDPROD"
                IndicesProduccion(Parametros(1), Parametros(2))
        End Select


    End Sub

    Sub IndicesProduccion(Desde As String, Hasta As String)
        Dim rs As New ADODB.Recordset
        Dim fDesde As Date = u.dateDecToDate(Desde)
        Dim fHasta As Date = u.dateDecToDate(Hasta)

        Do While fDesde <= fHasta
            Console.WriteLine(Format(fDesde, "yyyyMMdd"))
            db.ExecuteSQL("{{ CALL RRPRIND '" & Format(fDesde, "yyyyMMdd") & "' }}")
            fDesde = DateAdd("d", 1, fDesde)
        Loop

        db.comandoEjecutado("INDPROD", "OK")

    End Sub

End Module
