﻿Imports Microsoft.Office.Interop
Imports Microsoft.Office.Interop.Excel
Module AdminFletes
    Dim bBCC As Boolean

    Public Sub AprobAjustesFletes()
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim sql As String = ""
        Dim aHora() As String
        Dim i As Integer
        Dim FechaActual As Date = Now()
        Dim Minutos
        Minutos = Date.Now.Minute
        sql = "SELECT Z.*, DAYOFWEEK(NOW()) AS DOW FROM ZCCL01 Z WHERE CCTABL='FLAVISOS' AND CCCODE='AJUSTES'"
        rs.Open(sql, DB_DB2)
        sql = ""
        If rs.Fields("CCUDC1").Value = 1 Then
            'Domingo/Sabado
            If rs.Fields("DOW").Value = 1 Or rs.Fields("DOW").Value = 7 Then
                rs.Close()
                Exit Sub
            End If
            If HoyEsFestivo() Then
                rs.Close()
                Exit Sub
            End If
            aHora = Split(rs.Fields("CCSDSC").Value, ",")
            sql = ""
            For i = 0 To UBound(aHora)
                If Hour(FechaActual) = CInt(aHora(i)) Then
                    If Minutos = 1 Or Minutos = 2 Or Minutos = 3 Or Minutos = 4 Or Minutos = 5 Then

                        If rs.Fields("CCALTC").Value <> FechaActual.ToString("yyyyMMddHH") Then
                            sql = "UPDATE ZCCL01 SET CCALTC='" & FechaActual.ToString("yyyyMMddHH") & "' "
                            sql = sql & " WHERE CCTABL='FLAVISOS' AND CCCODE='AJUSTES'"
                        End If
                    End If
                End If
            Next
            bBCC = (rs("CCUDC3").Value = 1)
        End If
        rs.Close()
        If sql <> "" Then
            DB_DB2.Execute(sql)
        Else
            Exit Sub
        End If
        sql = ""
        sql = sql & " SELECT DISTINCT CCALTC FROM ZCCL01 WHERE CCTABL='FLBODORI'"
        sql = sql & " UNION "
        sql = sql & " SELECT DISTINCT CCDESC FROM ZCCL01 WHERE CCTABL='FLBODORI'"
        rs.Open(sql, DB_DB2)
        Do While Not rs.EOF
            sql = "SELECT COUNT(*) AS PEND FROM RFFLADJ INNER JOIN RHCC ON RFFLADJ.ECONSO = RHCC.HCONSO "
            sql = sql & " WHERE (EPLANI = 0) AND (EOCFLET <> 0) AND EID <> 'TZ' AND RFFLADJ.FOWNER  = '" & rs.Fields(0).Value & "' "
            rs1.Open(sql, DB_DB2)
            If rs1.Fields("PEND").Value > 0 Then
                On Error Resume Next
                EnviaAviso(rs.Fields(0).Value, ServerRaizz & "tm/fletes/aprob_adj_bb.asp?", _
                           "Aprobacion de Ajustes Pendientes [AdminFletes]")
                On Error GoTo 0
            End If
            rs1.Close()
            rs.MoveNext()
        Loop
        rs.Close()
    End Sub

    Public Sub AprobTarifasFletes()
        Dim rs As New ADODB.Recordset
        Dim sql As String = ""
        Dim qUser As String = ""
        Dim aHora() As String
        Dim i As Integer
        Dim FechaActual As Date = Now()
        Dim Minutos
        Minutos = Date.Now.Minute
        sql = "SELECT Z.*, DAYOFWEEK(NOW()) AS DOW "
        sql = sql & " FROM ZCCL01 Z WHERE CCTABL='FLAVISOS' AND CCCODE='TARIFAS'"
        rs.Open(sql, DB_DB2)
        sql = ""
        If rs.Fields("CCUDC1").Value = 1 Then
            'Domingo/Sabado
            If rs.Fields("DOW").Value = 1 Or rs.Fields("DOW").Value = 7 Then
                rs.Close()
                Exit Sub
            End If
            If HoyEsFestivo() Then
                rs.Close()
                Exit Sub
            End If
            aHora = Split(rs.Fields("CCSDSC").Value, ",")
            sql = ""
            For i = 0 To UBound(aHora)
                ' rs.Fields(0).Value <> Fecha.ToString("yyyyMMddHH") 
                If Hour(FechaActual) = CInt(aHora(i)) Then
                    If Minutos = 1 Or Minutos = 2 Or Minutos = 3 Or Minutos = 4 Or Minutos = 5 Then
                        'If rs.Fields("CCALTC").Value <> FechaActual.ToString("yyyyMMddHH") Then
                        sql = "UPDATE ZCCL01 SET CCALTC='" & FechaActual.ToString("yyyyMMddHH") & "' "
                        sql = sql & " WHERE CCTABL='FLAVISOS' AND CCCODE='TARIFAS'"
                        'End If
                    End If

                End If
            Next
            bBCC = (rs("CCUDC3").Value = 1)
        End If
        rs.Close()
        If sql <> "" Then
            DB_DB2.Execute(sql)
        Else
            Exit Sub
        End If

        sql = ""
        sql = sql & " SELECT DISTINCT FOWNER, RCAU.UGRP"
        sql = sql & " FROM RFFLTRFL01 INNER JOIN RCAU ON FOWNER=UUSR AND UTIPO='TM' AND USTS='A' "
        sql = sql & " WHERE FID <> 'GA' "
        If qUser <> "" Then
            sql = sql & " AND FOWNER = '" & qUser & "'"
        End If
        rs.Open(sql, DB_DB2)
        Do While Not rs.EOF
            On Error Resume Next
            EnviaAviso(rs.Fields("FOWNER").Value, _
                       ServerRaizz & "tm/fletes/vista_p_bb.asp?", _
                       "Aprobacion de Tarifas de Fletes [AdminFletes]")
            On Error GoTo 0
            rs.MoveNext()
        Loop
        rs.Close()

    End Sub

    Public Sub PedidosAtrasados()
        Dim rs As New ADODB.Recordset
        Dim sql As String = ""
        Dim qUser As String = ""
        Dim aHora() As String
        Dim i As Integer
        Dim FechaActual As Date = Now()
        Dim Usuarios As String = ""
        Dim sep As String = ""

        sql = "SELECT Z.*, DAYOFWEEK(NOW()) AS DOW "
        sql = sql & " FROM ZCCL01 Z WHERE CCTABL='FLAVISOS' AND CCCODE='ATRASADOS'"
        rs.Open(sql, DB_DB2)
        sql = ""
        If rs.Fields("CCUDC1").Value = 1 Then
            'Domingo/Sabado
            If rs.Fields("DOW").Value = 1 Or rs.Fields("DOW").Value = 7 Then
                rs.Close()
                Exit Sub
            End If
            If HoyEsFestivo() Then
                rs.Close()
                Exit Sub
            End If
            aHora = Split(rs.Fields("CCSDSC").Value, ",")
            sql = ""
            For i = 0 To UBound(aHora)
                ' rs.Fields(0).Value <> Fecha.ToString("yyyyMMddHH") 
                If Hour(FechaActual) = CInt(aHora(i)) Then
                    If rs.Fields("CCALTC").Value <> FechaActual.ToString("yyyyMMddHH") Then
                        sql = "UPDATE ZCCL01 SET CCALTC='" & FechaActual.ToString("yyyyMMddHH") & "' "
                        sql = sql & " WHERE CCTABL='FLAVISOS' AND CCCODE='ATRASADOS'"
                    End If
                End If
            Next
            aHora = Split(rs.Fields("CCNOT1").Value, ",")
            For i = 0 To UBound(aHora)
                Usuarios = Usuarios & sep & "'" & aHora(i) & "'"
                sep = ", "
            Next
            bBCC = (rs("CCUDC3").Value = 1)
        End If
        rs.Close()
        If sql <> "" Then
            DB_DB2.Execute(sql)
        Else
            Exit Sub
        End If

        fSql = " UPDATE RDCCL01 D SET DCONSTS = (  "
        fSql = fSql & "   SELECT HSTS FROM RHCC WHERE D.DCONSO=HCONSO ) "
        fSql = fSql & " WHERE USTSTR = 1 AND DCONSO <> '' "
        DB_DB2.Execute(fSql)

        fSql = " UPDATE RDCCL01 SET DREF05 = CASE  "
        fSql = fSql & " WHEN DCONSTS =  40 AND DATE(DFEC02) <= DATE(NOW()) THEN 'A Tiempo' "
        fSql = fSql & " WHEN DCONSTS =  40 AND DATE(DFEC02) >  DATE(NOW()) THEN 'Atrasado' "
        fSql = fSql & " WHEN DCONSTS <> 40 AND DATE(DFEC02) >  DATE(NOW()) THEN 'A Tiempo' "
        fSql = fSql & " ELSE 'Atrasado' END "
        fSql = fSql & " WHERE USTSTR = 1 "
        DB_DB2.Execute(fSql)

        EnviaAlertaAtrasados(Usuarios, _
                    ServerRaizz & "tm/track/atrasados.asp", _
                   "Pedidos Atrasados", bBCC)

    End Sub


    Public Function EnviaAlertaAtrasados(ByVal Usuarios As String, ByVal url As String, ByVal Asunto As String, ByVal bBCC As Boolean) As Boolean
        Dim rs As New ADODB.Recordset
        Dim mailman As New Chilkat.MailMan()
        Dim mht As New Chilkat.Mht()

        ' Any string passed to UnlockComponent automatically begins a 30-day trial.
        Dim success As Boolean = True

        success = mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
        If (success <> True) Then
            GuardaLog(success, "ATRASADOS", "ALERTA", "", mailman.LastErrorHtml, "")
            rs.Close()
            Return False
        End If

        success = mht.UnlockComponent("SMANRIMHT_dEnsKQ0g3Rue")
        If (success <> True) Then
            GuardaLog(success, "ATRASADOS", "ALERTA", "", mailman.LastErrorHtml, "")
            rs.Close()
            Return False
        End If

        mht.UseCids = True

        ' Set the SMTP server host.
        mailman.SmtpHost = BSSSendDocs.My.Settings.MAILHOST()
        mailman.SmtpUsername = BSSSendDocs.My.Settings.MAILUSER
        mailman.SmtpPassword = BSSSendDocs.My.Settings.MAILPASS

        ' Create a simple email with a file attachment.
        Dim email As New Chilkat.Email()

        ' Create an email from an HTML file.
        email = mht.GetEmail(url)
        If (email Is Nothing) Then
            GuardaLog(False, "ATRASADOS", "ALERTA", "", mailman.LastErrorHtml, "")
            rs.Close()
            Return False
        End If


        ' This email will have both a plain-text body and an HTML body.
        email.Subject = Asunto

        rs.Open("SELECT * FROM RCAU WHERE UTIPO='TM' AND USTS='A' AND UUSR IN(" & Usuarios & ")", DB_DB2)
        Do While Not rs.EOF
            email.AddTo(rs.Fields("UNOM").Value, rs.Fields("UEMAIL").Value)
            rs.MoveNext()
        Loop

        email.From = "PedidosAtrasados" + "<" + "ALogistica@brinsa.com.co" + ">"

        ' Send email.
        success = mailman.SendEmail(email)
        If success Then
            Console.WriteLine("Enviado. Alerta Pedidos Bandeja")
        Else
            GuardaLog(success, "ATRASADOS", "ALERTA", "", mailman.LastErrorHtml, "")
        End If
        rs.Close()
        Return success

    End Function


    Public Sub AprobPreciosProveedor()
        Dim rs As New ADODB.Recordset
        Dim sql As String = ""
        Dim qUser As String = ""
        Dim aHora() As String
        Dim i As Integer
        Dim FechaActual As Date = Now()

        sql = "SELECT Z.*, DAYOFWEEK(NOW()) AS DOW "
        sql = sql & " FROM ZCCL01 Z WHERE CCTABL='FLAVISOS' AND CCCODE='PRECPROV'"
        rs.Open(sql, DB_DB2)
        sql = ""
        If rs.Fields("CCUDC1").Value = 1 Then
            'Domingo/Sabado
            If rs.Fields("DOW").Value = 1 Or rs.Fields("DOW").Value = 7 Then
                rs.Close()
                Exit Sub
            End If
            If HoyEsFestivo() Then
                rs.Close()
                Exit Sub
            End If
            aHora = Split(rs.Fields("CCSDSC").Value, ",")
            sql = ""
            For i = 0 To UBound(aHora)
                ' rs.Fields(0).Value <> Fecha.ToString("yyyyMMddHH") 
                If Hour(FechaActual) = CInt(aHora(i)) Then
                    If rs.Fields("CCALTC").Value <> FechaActual.ToString("yyyyMMddHH") Then
                        sql = "UPDATE ZCCL01 SET CCALTC='" & FechaActual.ToString("yyyyMMddHH") & "' "
                        sql = sql & " WHERE CCTABL='FLAVISOS' AND CCCODE='PRECPROV'"
                    End If
                End If
            Next
            bBCC = (rs("CCUDC3").Value = 1)
        End If
        rs.Close()
        If sql <> "" Then
            DB_DB2.Execute(sql)
        Else
            Exit Sub
        End If

        sql = " SELECT DISTINCT FOWNER FROM RFALPRE WHERE AID = 'PS' "
        If qUser <> "" Then
            sql = sql & " AND FOWNER = '" & qUser & "'"
        End If
        rs.Open(sql, DB_DB2)
        Do While Not rs.EOF
            On Error Resume Next
            EnviaAvisoPPP(rs.Fields("FOWNER").Value, _
                        ServerRaizz & "tm/ppp/pend_aviso.asp?", _
                       "Aprobacion de Precios Por Proveedor [PPP]", bBCC)
            On Error GoTo 0
            rs.MoveNext()
        Loop
        rs.Close()

    End Sub

    Public Sub AlertaBandeja()
        Dim rs As New ADODB.Recordset
        Dim Desde As String = ""
        Dim Hasta As String = ""
        Dim h As Integer = 0
        Dim f As Date
        Dim bOK As Boolean = False

        fSql = " SELECT "
        fSql &= "  MINUTE(NOW()) AS MINUTO "
        fSql &= ", ( NOW() - MAX(SCRTDAT) ) / 100 AS MINUTOS "
        fSql &= ", NOW() - 2 HOUR AS HORA "
        fSql &= ", NOW() AS AHORA "
        fSql &= ", DAYOFWEEK(NOW()) AS DIASEM "
        fSql &= " FROM RHSMGOSTS "
        rs.Open(fSql, DB_DB2)
        f = rs("AHORA").Value
        If HoyEsFestivo() Then
            rs.Close()
            Exit Sub
        End If
        If Hour(rs("AHORA").Value) > 5 And Hour(rs("AHORA").Value) < 22 Then
            If rs("MINUTO").Value >= 55 And (IsDBNull(rs("MINUTOS").Value) OrElse rs("MINUTOS").Value >= 120) Then
                h = Hour(rs("AHORA").Value)
                Select Case h
                    Case 8, 10, 12, 14, 16, 18, 20
                        Desde = f.ToString("yyyy-MM-dd") & "-" & Ceros(h - 3, 2) & ".00.00.000000"
                        Hasta = f.ToString("yyyy-MM-dd") & "-" & Ceros(h - 1, 2) & ".40.00.000000"
                        bOK = True
                End Select
            End If
        End If
        rs.Close()
        If bOK Then

            '           fSql = "SELECT *  FROM RHSMGO WHERE(HSTS01 = 0) AND ( ( DATE(HCRTDAT) = DATE( NOW() )) )"
            '          rs.Open(fSql, DB_DB2)

            'If Not rs.EOF Then
            EnviaAlertaBandeja()
            'End If

        End If

    End Sub


    Public Sub AlertaBandejaSMS()
        Dim rs As New ADODB.Recordset
        Dim Desde As String = ""
        Dim Hasta As String = ""
        Dim h As Integer = 0
        Dim bOK As Boolean = False


        fSql = " SELECT *"
        fSql &= " FROM RRCC WHERE ( RAPP = 'SMS' ) AND ( RTREP = 30 ) "
        rs.Open(fSql, DB_DB2)

        If Not rs.EOF Then
            EnviaAlertaPlanillaSMS()

        End If



    End Sub


    Public Function EnviaAlertaBandeja() As Boolean
        Dim rs As New ADODB.Recordset
        Dim mailman As New Chilkat.MailMan()
        Dim mht As New Chilkat.Mht()
        Dim aUsers() As String
        Dim sep As String = ""
        Dim i As Integer
        Dim bBCC As Boolean
        Dim Usuarios As String = ""
        Dim url As String = ServerRaizz & "tm/sc/alerta_ped.asp"
        Dim Asunto As String = "Estado Pedidos Bandeja"
        fSql = "SELECT * FROM ZCCL01 WHERE CCTABL='FLAVISOS' AND CCCODE='PEDBANDEJA'"
        rs.Open(fSql, DB_DB2)


        If rs.Fields("CCUDC1").Value = 1 Then
            aUsers = Split(rs.Fields("CCNOT1").Value, ",")
            For i = 0 To UBound(aUsers)
                Usuarios &= sep & "'" & aUsers(i) & "'"
                sep = ", "
            Next
            bBCC = (rs("CCUDC3").Value = 1)
        End If
        rs.Close()

        ' Any string passed to UnlockComponent automatically begins a 30-day trial.
        Dim success As Boolean = True

        success = mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
        If (success <> True) Then
            GuardaLog(success, "PEDBANDEJA", "ALERTA", "", mailman.LastErrorHtml, "")
            rs.Close()
            Return False
        End If

        success = mht.UnlockComponent("SMANRIMHT_dEnsKQ0g3Rue")
        If (success <> True) Then
            GuardaLog(success, "PEDBANDEJA", "ALERTA", "", mailman.LastErrorHtml, "")
            rs.Close()
            Return False
        End If

        mht.UseCids = True

        ' Set the SMTP server host.
        mailman.SmtpHost = BSSSendDocs.My.Settings.MAILHOST()
        mailman.SmtpUsername = BSSSendDocs.My.Settings.MAILUSER
        mailman.SmtpPassword = BSSSendDocs.My.Settings.MAILPASS

        ' Create a simple email with a file attachment.
        Dim email As New Chilkat.Email()

        ' Create an email from an HTML file.
        email = mht.GetEmail(url)
        If (email Is Nothing) Then
            GuardaLog(False, "PEDBANDEJA", "ALERTA", "", mailman.LastErrorHtml, "")
            rs.Close()
            Return False
        End If


        ' This email will have both a plain-text body and an HTML body.
        email.Subject = Asunto

        rs.Open("SELECT * FROM RCAU WHERE UTIPO='TM' AND USTS='A' AND UUSR IN(" & Usuarios & ") AND UEMAIL <> '' ", DB_DB2)
        Do While Not rs.EOF
            email.AddTo(rs.Fields("UNOM").Value, rs.Fields("UEMAIL").Value)
            rs.MoveNext()
        Loop

        email.From = "PedidosBandeja" + "<" + "ALogistica@brinsa.com.co" + ">"

        ' Send email.
        success = mailman.SendEmail(email)
        If success Then
            Console.WriteLine("Enviado. Pedidos Bandeja.")
        Else
            GuardaLog(success, "PEDBANDEJA", "ALERTA", "", mailman.LastErrorHtml, "")
        End If
        rs.Close()
        Return success

    End Function

    Public Function EnviaAlertaPlanillaSMS() As Boolean
        Dim rs As New ADODB.Recordset
        Dim mailman As New Chilkat.MailMan()
        Dim mht As New Chilkat.Mht()
        Dim aUsers() As String
        Dim sep As String = ""
        Dim i As Integer
        Dim bBCC As Boolean
        Dim Usuarios As String = ""
        Dim url As String = ServerRaizz & "tm/avisos/sms_rep.asp"
        Dim Asunto As String = "SMS PLANILLAS"
        fSql = "SELECT * FROM ZCCL01 WHERE CCTABL='FLAVISOS' AND CCCODE='SMSPLANI'"
        rs.Open(fSql, DB_DB2)


        If rs.Fields("CCUDC1").Value = 1 Then
            aUsers = Split(rs.Fields("CCNOT1").Value, ",")
            For i = 0 To UBound(aUsers)
                Usuarios &= sep & "'" & aUsers(i) & "'"
                sep = ", "
            Next
            bBCC = (rs("CCUDC3").Value = 1)
        End If
        rs.Close()

        ' Any string passed to UnlockComponent automatically begins a 30-day trial.
        Dim success As Boolean = True

        success = mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
        If (success <> True) Then
            GuardaLog(success, "SMSPLANI", "ALERTA", "", mailman.LastErrorHtml, "")
            rs.Close()
            Return False
        End If

        success = mht.UnlockComponent("SMANRIMHT_dEnsKQ0g3Rue")
        If (success <> True) Then
            GuardaLog(success, "SMSPLANI", "ALERTA", "", mailman.LastErrorHtml, "")
            rs.Close()
            Return False
        End If

        mht.UseCids = True

        ' Set the SMTP server host.
        mailman.SmtpHost = BSSSendDocs.My.Settings.MAILHOST()
        mailman.SmtpUsername = BSSSendDocs.My.Settings.MAILUSER
        mailman.SmtpPassword = BSSSendDocs.My.Settings.MAILPASS

        ' Create a simple email with a file attachment.
        Dim email As New Chilkat.Email()

        ' Create an email from an HTML file.
        email = mht.GetEmail(url)
        If (email Is Nothing) Then
            GuardaLog(False, "SMSPLANI", "ALERTA", "", mailman.LastErrorHtml, "")
            'rs.Close()
            Return False
        End If


        ' This email will have both a plain-text body and an HTML body.
        email.Subject = Asunto

        rs.Open("SELECT * FROM RCAU WHERE UTIPO='TM' AND USTS='A' AND UUSR IN(" & Usuarios & ") AND UEMAIL <> '' ", DB_DB2)
        Do While Not rs.EOF
            email.AddTo(rs.Fields("UNOM").Value, rs.Fields("UEMAIL").Value)
            Console.WriteLine(rs.Fields("UNOM").Value & " - " & rs.Fields("UEMAIL").Value)
            rs.MoveNext()
        Loop

        email.From = "Reporte Fin Entrega" + "<" + "ALogistica@brinsa.com.co" + ">"

        ' Send email.
        success = mailman.SendEmail(email)
        If success Then
            Console.WriteLine("Enviado SMS Planilla.")
            fSql = "UPDATE RRCC SET RTREP=31 WHERE RAPP='SMS' AND RTREP = 30 "
            DB_DB2.Execute(fSql)
        Else
            GuardaLog(success, "SMSPLANI", "ALERTA", "", mailman.LastErrorHtml, "")
        End If
        rs.Close()
        Return success

    End Function

    Public Function AvisoPaletizado() As Boolean

        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim mailman As New Chilkat.MailMan()
        Dim mht As New Chilkat.Mht()
        Dim sep As String = ""
        Dim i As Integer
        Dim Usuarios As String = ""

        fSql = "SELECT * FROM RMAILS WHERE SENVIADO = 0 AND SMOD = 'CONSOLIDACION'"
        rs.Open(fSql, DB_DB2)

        While Not rs.EOF
            fSql = "SELECT * FROM RMAILL WHERE LID=" & rs("SIDREF").Value.ToString
            rs1.Open(fSql, DB_DB1)

            While Not rs1.EOF
                Dim url As String = rs("SMHT").Value.ToString
                Dim Asunto As String = "Notificacion de Alistamiento Paletizado " & rs("SFILE").Value.ToString
                Dim success As Boolean = True

                success = mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
                If (success <> True) Then
                    GuardaLog(success, "TRANSPORTE", "PALETIZADO", "", mailman.LastErrorHtml, "")
                    rs.Close()
                    Return False
                End If
                success = mht.UnlockComponent("SMANRIMHT_dEnsKQ0g3Rue")

                If (success <> True) Then
                    GuardaLog(success, "TRANSPORTE", "PALETIZADO", "", mailman.LastErrorHtml, "")
                    rs.Close()
                    Return False
                End If

                mht.UseCids = True
                mailman.SmtpHost = BSSSendDocs.My.Settings.MAILHOST()
                mailman.SmtpUsername = BSSSendDocs.My.Settings.MAILUSER
                mailman.SmtpPassword = BSSSendDocs.My.Settings.MAILPASS

                Dim email As New Chilkat.Email()
                Dim aDestinos() As String
                Dim aUnaCuenta() As String
                email = mht.GetEmail(url)
                email.Subject = Asunto
                email.From = rs1.Fields("LMAIL").Value
                aDestinos = Split(rs1.Fields("LLSTTO").Value, vbCrLf)
                For i = 0 To UBound(aDestinos)
                    If InStr(aDestinos(i), "@") > 0 Then
                        aUnaCuenta = Split(aDestinos(i), ",")
                        If UBound(aUnaCuenta) = 1 Then
                            email.AddTo(Trim(aUnaCuenta(0)), Trim(aUnaCuenta(1)))
                        Else
                            email.AddTo("", Trim(aDestinos(i)))
                        End If
                        Debug.Print(aDestinos(i))
                    End If
                Next

                If Trim(rs1.Fields("LLSTCC").Value) <> "" Then
                    aDestinos = Split(rs1.Fields("LLSTCC").Value, vbCrLf)
                    For i = 0 To UBound(aDestinos)
                        If InStr(aDestinos(i), "@") > 0 Then
                            aUnaCuenta = Split(aDestinos(i), ",")
                            If UBound(aUnaCuenta) = 1 Then
                                email.AddCC(Trim(aUnaCuenta(0)), Trim(aUnaCuenta(1)))
                            Else
                                email.AddCC("", Trim(aDestinos(i)))
                            End If
                        End If
                    Next
                End If
                success = mailman.SendEmail(email)
                If success Then
                    fSql = "UPDATE RMAILS SET SENVIADO=1 WHERE SID=" & rs("SID").Value.ToString
                    DB_DB2.Execute(fSql)
                Else
                    GuardaLog(success, "TRANSPORTE", "PALETIZADO", "", mailman.LastErrorHtml, "")
                End If
                rs1.MoveNext()
            End While
            rs1.Close()
            rs.MoveNext()
        End While
        rs.Close()
        Return True

    End Function



    Public Function VencimientoIndustria() As Boolean

        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim mailman As New Chilkat.MailMan()
        Dim mht As New Chilkat.Mht()
        Dim sep As String = ""
        Dim i As Integer
        Dim Usuarios As String = ""


        fSql = " CREATE TABLE QTEMP.PVENCIDO AS ( "
        fSql = fSql & " SELECT LID "
        fSql = fSql & " FROM RLISPRE01 P INNER JOIN RCAU ON FUSRSOL = UUSR "
        fSql = fSql & " WHERE LVIGEN = 'VIGENTE'  AND LFECFIND <> 99991231 "
        fSql = fSql & " AND LFECFI - DATE(NOW()) < 4 AND LSTS01 = 0 ) WITH DATA  "
        DB_DB2.Execute(fSql)

        fSql = "UPDATE RLISPRE SET LSTS01 = 1 WHERE LID IN( SELECT LID FROM QTEMP.PVENCIDO )  "
        DB_DB2.Execute(fSql)

        fSql = "DROP TABLE QTEMP.PVENCIDO "
        DB_DB2.Execute(fSql)


        fSql = "SELECT UEMAIL, UNOM, FUSRSOL, LVENDE FROM RLISPRE01 INNER JOIN RCAU ON FUSRSOL = UUSR WHERE LSTS01 = 1 GROUP BY UEMAIL, UNOM, FUSRSOL, LVENDE "

        rs.Open(fSql, DB_DB1)

        While Not rs.EOF

            Dim url As String = ServerRaizz & "tm/prec_ind/pre_venc.asp?LVENDE=" & rs("LVENDE").Value
            Dim Asunto As String = "Notificacion Vencimiento Precio de Industrias"
            Dim success As Boolean = True

            success = mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
            If (success <> True) Then
                GuardaLog(success, "TRANSPORTE", "PALETIZADO", "", mailman.LastErrorHtml, "")
                rs.Close()
                Return False
            End If
            success = mht.UnlockComponent("SMANRIMHT_dEnsKQ0g3Rue")

            If (success <> True) Then
                GuardaLog(success, "TRANSPORTE", "PALETIZADO", "", mailman.LastErrorHtml, "")
                rs.Close()
                Return False
            End If

            mht.UseCids = True
            mailman.SmtpHost = BSSSendDocs.My.Settings.MAILHOST()
            mailman.SmtpUsername = BSSSendDocs.My.Settings.MAILUSER
            mailman.SmtpPassword = BSSSendDocs.My.Settings.MAILPASS

            Dim email As New Chilkat.Email()
            Dim aDestinos() As String
            Dim aUnaCuenta() As String
            email = mht.GetEmail(url)
            email.Subject = Asunto
            email.From = "PreciosIndustira" + "<" + "ALogistica@brinsa.com.co" + ">"
            email.AddTo(Trim(rs("UNOM").Value), Trim(rs("UEMAIL").Value))
            success = mailman.SendEmail(email)
            If success Then
                fSql = "UPDATE RLISPRE SET LSTS01 = 2 WHERE LVENDE = '" & rs("LVENDE").Value & "' AND LFECFI - DATE(NOW()) < 4"
                DB_DB2.Execute(fSql)
            Else
                GuardaLog(success, "AVISOS", "PRECIOSINDUSTRIA", "", mailman.LastErrorHtml, "")
            End If

            rs.MoveNext()
        End While
        rs.Close()
        Return True

    End Function


    Public Function AvisoCierrePedidosUL() As Boolean

        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim rs2 As New ADODB.Recordset
        Dim mailman As New Chilkat.MailMan()
        Dim mht As New Chilkat.Mht()
        Dim sep As String = ""
        Dim i As Integer
        Dim Usuarios As String = ""

        fSql = "SELECT * FROM RMAILX WHERE SENVIADO=0 AND LREF  = 'CIERRE_PED'"
        rs.Open(fSql, DB_DB2)

        While Not rs.EOF


            fSql = "SELECT * FROM RMAILL WHERE LID=" & rs("SIDREF").Value.ToString
            rs1.Open(fSql, DB_DB1)

            While Not rs1.EOF
                Dim url As String = rs("SMHT").Value.ToString
                Dim Asunto As String = "Notificacion de Alistamiento Paletizado " & rs("SFILE").Value.ToString
                Dim success As Boolean = True

                success = mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
                If (success <> True) Then
                    GuardaLog(success, "TRANSPORTE", "PALETIZADO", "", mailman.LastErrorHtml, "")
                    rs.Close()
                    Return False
                End If
                success = mht.UnlockComponent("SMANRIMHT_dEnsKQ0g3Rue")

                If (success <> True) Then
                    GuardaLog(success, "TRANSPORTE", "PALETIZADO", "", mailman.LastErrorHtml, "")
                    rs.Close()
                    Return False
                End If

                mht.UseCids = True
                mailman.SmtpHost = BSSSendDocs.My.Settings.MAILHOST()
                mailman.SmtpUsername = BSSSendDocs.My.Settings.MAILUSER
                mailman.SmtpPassword = BSSSendDocs.My.Settings.MAILPASS

                Dim email As New Chilkat.Email()
                Dim aDestinos() As String
                Dim aUnaCuenta() As String
                email = mht.GetEmail(url)
                email.Subject = Asunto
                email.From = rs1.Fields("LMAIL").Value
                aDestinos = Split(rs1.Fields("LLSTTO").Value, vbCrLf)
                For i = 0 To UBound(aDestinos)
                    If InStr(aDestinos(i), "@") > 0 Then
                        aUnaCuenta = Split(aDestinos(i), ",")
                        If UBound(aUnaCuenta) = 1 Then
                            email.AddTo(Trim(aUnaCuenta(0)), Trim(aUnaCuenta(1)))
                        Else
                            email.AddTo("", Trim(aDestinos(i)))
                        End If
                        Debug.Print(aDestinos(i))
                    End If
                Next

                If Trim(rs1.Fields("LLSTCC").Value) <> "" Then
                    aDestinos = Split(rs1.Fields("LLSTCC").Value, vbCrLf)
                    For i = 0 To UBound(aDestinos)
                        If InStr(aDestinos(i), "@") > 0 Then
                            aUnaCuenta = Split(aDestinos(i), ",")
                            If UBound(aUnaCuenta) = 1 Then
                                email.AddCC(Trim(aUnaCuenta(0)), Trim(aUnaCuenta(1)))
                            Else
                                email.AddCC("", Trim(aDestinos(i)))
                            End If
                        End If
                    Next
                End If
                success = mailman.SendEmail(email)
                If success Then
                    fSql = "UPDATE RMAILS SET SENVIADO=1 WHERE SID=" & rs("SID").Value.ToString
                    DB_DB2.Execute(fSql)
                Else
                    GuardaLog(success, "TRANSPORTE", "PALETIZADO", "", mailman.LastErrorHtml, "")
                End If
                rs1.MoveNext()
            End While
            rs1.Close()
            rs.MoveNext()
        End While
        rs.Close()
        Return True

    End Function




    Public Function EnviaAvisoPPP(ByVal sUser As String, ByVal url As String, ByVal Asunto As String, ByVal bBCC As Boolean) As Boolean
        Dim rs As New ADODB.Recordset
        Dim mailman As New Chilkat.MailMan()
        Dim mht As New Chilkat.Mht()
        Dim wrkId As Double

        ' Any string passed to UnlockComponent automatically begins a 30-day trial.
        Dim success As Boolean = True

        rs.Open("SELECT * FROM RCAU WHERE UTIPO='TM' AND USTS='A' AND UUSR = '" & sUser & "'", DB_DB2)
        If Not rs.EOF Then

            wrkId = CalcConsec("FLAVISOS", False, "9999")
            url = url & "UserLogin=" & sUser & "&FWRKID=" & wrkId

            success = mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
            If (success <> True) Then
                GuardaLog(success, "PPP", "AVISOAPROB", "", mailman.LastErrorHtml, "")
                rs.Close()
                Return False
            End If

            success = mht.UnlockComponent("SMANRIMHT_dEnsKQ0g3Rue")
            If (success <> True) Then
                GuardaLog(success, "PPP", "AVISOAPROB", "", mailman.LastErrorHtml, "")
                rs.Close()
                Return False
            End If

            mht.UseCids = True

            ' Set the SMTP server host.
            mailman.SmtpHost = BSSSendDocs.My.Settings.MAILHOST()
            mailman.SmtpUsername = BSSSendDocs.My.Settings.MAILUSER
            mailman.SmtpPassword = BSSSendDocs.My.Settings.MAILPASS

            ' Create a simple email with a file attachment.
            Dim email As New Chilkat.Email()

            ' Create an email from an HTML file.
            email = mht.GetEmail(url)
            If (email Is Nothing) Then
                GuardaLog(False, "PPP", "AVISOAPROB", "", mailman.LastErrorHtml, "")
                rs.Close()
                Return False
            End If

            ' This email will have both a plain-text body and an HTML body.
            email.Subject = Asunto
            'email.AddTo("Daniel Bernal", "daniel.bernal@brinsa.com.co")
            email.AddTo(rs.Fields("UNOM").Value, rs.Fields("UEMAIL").Value)

            email.From = "PreciosProveedor" + "<" + "ALogistica@brinsa.com.co" + ">"

            ' Send email.
            success = mailman.SendEmail(email)
            If success Then
                Console.WriteLine("Enviado. PPP.")
            Else
                GuardaLog(success, "PPP", "AVISOAPROB", "", mailman.LastErrorHtml, "")
            End If
        End If
        rs.Close()
        Return success

    End Function

    Public Function EnviaAviso(ByVal sUser As String, ByVal url As String, ByVal Asunto As String) As Boolean
        Dim rs As New ADODB.Recordset
        Dim mailman As New Chilkat.MailMan()
        Dim mht As New Chilkat.Mht()
        Dim wrkId As Double

        ' Any string passed to UnlockComponent automatically begins a 30-day trial.
        Dim success As Boolean = True

        rs.Open("SELECT * FROM RCAU WHERE UTIPO='TM' AND USTS='A' AND UUSR = '" & sUser & "'", DB_DB2)
        If Not rs.EOF Then

            success = mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
            If (success <> True) Then
                GuardaLog(success, "ADMINFLETES", "AVISOAPROB", "", mailman.LastErrorHtml, "")
                rs.Close()
                Return False
            End If

            success = mht.UnlockComponent("SMANRIMHT_dEnsKQ0g3Rue")
            If (success <> True) Then
                GuardaLog(success, "ADMINFLETES", "AVISOAPROB", "", mailman.LastErrorHtml, "")
                rs.Close()
                Return False
            End If

            mht.UseCids = True

            ' Set the SMTP server host.
            'mailman.SmtpHost = BSSSendDocs.My.Settings.MAILHOST()
            'mailman.SmtpUsername = BSSSendDocs.My.Settings.MAILUSER
            'mailman.SmtpPassword = BSSSendDocs.My.Settings.MAILPASS

            mailman.SmtpHost = My.Settings.MAILHOST
            mailman.SmtpUsername = My.Settings.MAILUSER
            mailman.SmtpPassword = My.Settings.MAILPASS

            ' Create a simple email with a file attachment.
            Dim email As New Chilkat.Email()

            ' Create an email from an HTML file.
            wrkId = CalcConsec("FLAVISOS", False, "9999")

            url = url & "UserLogin=" & sUser & "&FWRKID=" & wrkId
            email = mht.GetEmail(url)
            If (email Is Nothing) Then
                GuardaLog(False, "ADMINFLETES", "AVISOAPROB", "", mailman.LastErrorHtml, "")
                rs.Close()
                Return False
            End If

            ' This email will have both a plain-text body and an HTML body.
            email.Subject = Asunto
            'email.AddTo("Fredy Manrique", "fmanriq@yahoo.com")
            email.AddTo(rs.Fields("UNOM").Value, rs.Fields("UEMAIL").Value)

            email.From = "AdminFletes" + "<" + "ALogistica@brinsa.com.co" + ">"

            ' Send email.
            success = mailman.SendEmail(email)
            If success Then
                Console.WriteLine("Enviado.")
            Else
                GuardaLog(success, "ADMINFLETES", "AVISOAPROB", "", mailman.LastErrorHtml, "")
            End If
        End If
        rs.Close()
        Return success

    End Function

    Public Sub TiquetesIncompletos()
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim sql As String = ""
        Dim qUser As String = ""
        Dim aHora() As String
        Dim i As Integer
        Dim FechaActual As Date = Now()
        Dim Minutos
        Minutos = Date.Now.Minute

        sql = "SELECT Z.*, DAYOFWEEK( NOW()) AS DOW "
        sql = sql & " FROM ZCCL01 Z WHERE CCTABL='FLAVISOS' AND CCCODE='TIQUETES'"
        rs.Open(sql, DB_DB2)
        sql = ""
        If rs.Fields("CCUDC1").Value = 1 Then
            'Domingo
            If rs.Fields("DOW").Value = 1 Then
                rs.Close()
                Exit Sub
            End If

            sql = ""
            aHora = Split(rs.Fields("CCSDSC").Value, ",")
            sql = ""
            For i = 0 To UBound(aHora)
                ' rs.Fields(0).Value <> Fecha.ToString("yyyyMMddHH") 
                If Hour(FechaActual) = CInt(aHora(i)) Then
                    If Minutos = 1 Or Minutos = 2 Or Minutos = 3 Or Minutos = 4 Or Minutos = 5 Then

                        If rs.Fields("CCALTC").Value <> FechaActual.ToString("yyyyMMddHH") Then
                            sql = "UPDATE ZCCL01 SET CCALTC='" & FechaActual.ToString("yyyyMMddHH") & "' "
                            sql = sql & " WHERE CCTABL='FLAVISOS' AND CCCODE='TIQUETES'"
                        End If
                    End If
                End If
            Next
            bBCC = (rs("CCUDC3").Value = 1)
        End If
        If sql <> "" Then
            DB_DB2.Execute(sql)
        Else
            rs.Close()
            Exit Sub
        End If

        sql = ""
        sql = sql & " SELECT COUNT(*) AS TOT FROM RHCC WHERE (HESTAD <> 9  AND HPRODUCCD = 'ERRTIQ') "
        rs1.Open(sql, DB_DB2)
        If rs1.Fields("TOT").Value > 0 Then
            On Error Resume Next
            EnviaAvisoTiquetes(rs.Fields("CCNOT1").Value, rs.Fields("CCNOT2").Value, _
                       ServerRaizz & "tm/Porteria/err_peso.asp", _
                       "Tiquetes de Báscula Incompletos [AdminFletes]")
            On Error GoTo 0
        End If
        rs.Close()

    End Sub

    Public Function EnviaAvisoTiquetes(ByVal sUser As String, ByVal sUserCC As String, ByVal url As String, ByVal Asunto As String) As Boolean
        Dim rs As New ADODB.Recordset
        Dim mailman As New Chilkat.MailMan()
        Dim mht As New Chilkat.Mht()
        Dim wrkId As Double
        Dim aUserCC() As String = Split(sUserCC, ",")
        Dim i As Integer

        ' Any string passed to UnlockComponent automatically begins a 30-day trial.
        Dim success As Boolean = True

        rs.Open("SELECT * FROM RCAU WHERE UTIPO='TM' AND USTS='A' AND UUSR = '" & sUser & "'", DB_DB2)
        If Not rs.EOF Then

            success = mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
            If (success <> True) Then
                GuardaLog(success, "ADMINFLETES", "AVISOTIQ", "", mailman.LastErrorHtml, "")
                rs.Close()
                Return False
            End If

            success = mht.UnlockComponent("SMANRIMHT_dEnsKQ0g3Rue")
            If (success <> True) Then
                GuardaLog(success, "ADMINFLETES", "AVISOTIQ", "", mailman.LastErrorHtml, "")
                rs.Close()
                Return False
            End If

            mht.UseCids = True

            ' Set the SMTP server host.
            mailman.SmtpHost = BSSSendDocs.My.Settings.MAILHOST
            mailman.SmtpUsername = BSSSendDocs.My.Settings.MAILUSER
            mailman.SmtpPassword = BSSSendDocs.My.Settings.MAILPASS

            ' Create a simple email with a file attachment.
            Dim email As New Chilkat.Email()

            ' Create an email from an HTML file.
            wrkId = CalcConsec("FLAVISOS", False, "9999")

            email = mht.GetEmail(url)
            If (email Is Nothing) Then
                GuardaLog(False, "ADMINFLETES", "AVISOAPROB", "", mailman.LastErrorHtml, "")
                rs.Close()
                Return False
            End If

            ' This email will have both a plain-text body and an HTML body.
            email.Subject = Asunto
            'email.AddTo("Fredy Manrique", "fmanriq@yahoo.com")
            email.AddTo(rs.Fields("UNOM").Value, rs.Fields("UEMAIL").Value)

            rs.Close()
            For i = 0 To UBound(aUserCC)
                rs.Open("SELECT * FROM RCAU WHERE UTIPO='TM' AND USTS='A' AND UUSR = '" & aUserCC(i) & "'", DB_DB2)
                If Not rs.EOF Then
                    email.AddCC(rs.Fields("UNOM").Value, rs.Fields("UEMAIL").Value)
                End If
                rs.Close()
            Next

            email.From = "AdminFletes" + "<" + "ALogistica@brinsa.com.co" + ">"

            ' Send email.
            success = mailman.SendEmail(email)
            If success Then
                Console.WriteLine("Enviado. Tiquetes.")
            Else
                GuardaLog(success, "ADMINFLETES", "AVISOAPROB", "", mailman.LastErrorHtml, "")
            End If
        Else
            rs.Close()
        End If
        Return success

    End Function
    Public Function AvisoClienteNuevo() As Boolean
        Dim rs As New ADODB.Recordset
        Dim rsIni As New ADODB.Recordset
        Dim mailman As New Chilkat.MailMan()
        Dim mht As New Chilkat.Mht()
        Dim aUsers() As String
        Dim sep As String = ""
        Dim i As Integer
        Dim bBCC As Boolean
        Dim Usuarios As String = ""
        Dim url As String = ServerRaizz & "tm/sc/alerta_ped.asp"
        Dim Asunto As String = "Aviso Cliente Nuevo"

        fSql = "SELECT * FROM  DESLX834F.RCM WHERE CMUF01 = ' '"
        rsIni.Open(fSql, DB_DB2)

        While Not rsIni.EOF

            fSql = "SELECT * FROM  DESLX834F.ZCC WHERE CCTABL = 'FLAVISOS' AND CCCODE = 'CLIENTENUEVO'"
            rs.Open(fSql, DB_DB2)
            If rs.Fields("CCUDC1").Value = 1 Then
                aUsers = Split(rs.Fields("CCNOT1").Value, ",")
                For i = 0 To UBound(aUsers)
                    Usuarios &= sep & "'" & aUsers(i) & "'"
                    sep = ", "
                Next
                bBCC = (rs("CCUDC3").Value = 1)
            End If
            rs.Close()

            ' Any string passed to UnlockComponent automatically begins a 30-day trial.
            Dim success As Boolean = True

            success = mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
            If (success <> True) Then
                GuardaLog(success, "AVISOCLIENTENUEVO", "ALERTA", "", mailman.LastErrorHtml, "")
                rs.Close()
                Return False
            End If

            success = mht.UnlockComponent("SMANRIMHT_dEnsKQ0g3Rue")
            If (success <> True) Then
                GuardaLog(success, "AVISOCLIENTENUEVO", "ALERTA", "", mailman.LastErrorHtml, "")
                rs.Close()
                Return False
            End If

            mht.UseCids = True

            ' Set the SMTP server host.
            mailman.SmtpHost = BSSSendDocs.My.Settings.MAILHOST()
            mailman.SmtpUsername = BSSSendDocs.My.Settings.MAILUSER
            mailman.SmtpPassword = BSSSendDocs.My.Settings.MAILPASS

            ' Create a simple email with a file attachment.
            Dim email As New Chilkat.Email()

            ' Create an email from an HTML file.
            email = mht.GetEmail(ServerRaizz & "/alertas/rcm_new_rv.asp?CCUST=" & rsIni.Fields("CCUST").Value)
            If (email Is Nothing) Then
                GuardaLog(False, "PEDBANDEJA", "ALERTA", "", mailman.LastErrorHtml, "")
                rs.Close()
                Return False
            End If


            ' This email will have both a plain-text body and an HTML body.
            email.Subject = Asunto

            rs.Open("SELECT * FROM DESLX834F.RCAU WHERE UTIPO='TM' AND USTS='A' AND UUSR IN(" & Usuarios & ")", DB_DB2)
            Do While Not rs.EOF
                email.AddTo(rs.Fields("UNOM").Value, rs.Fields("UEMAIL").Value)
                rs.MoveNext()
            Loop

            email.From = "Cliente Nuevo" + "<" + "ALogistica@brinsa.com.co" + ">"

            ' Send email.
            success = mailman.SendEmail(email)

            If success Then
                fSql = "UPDATE DESLX834F.RCM SET  CMUF01 = '000'  WHERE CCUST =" & rsIni.Fields("CCUST").Value
                DB_DB2.Execute(fSql)
            Else

                GuardaLog(success, "PEDBANDEJA", "ALERTA", "", mailman.LastErrorHtml, "")
            End If
            rs.Close()

            rsIni.MoveNext()
        End While
        Return True

    End Function

    Public Function AvisoRProducto() As Boolean
        Dim rs As New ADODB.Recordset
        Dim rsIni As New ADODB.Recordset
        Dim mailman As New Chilkat.MailMan()
        Dim aUsers() As String
        Dim sep As String = ""
        Dim i As Integer
        Dim bBCC As Boolean
        Dim Usuarios As String = ""

        fSql = "SELECT RIPROD, IDESC FROM RIIM INNER JOIN IIM ON RIPROD = IPROD WHERE RFLAG03 = '2' "
        rsIni.Open(fSql, DB_DB2)

        While Not rsIni.EOF

            fSql = "SELECT * FROM  ZCC WHERE CCTABL = 'FLAVISOS' AND CCCODE = 'PRODNUEVO'"
            rs.Open(fSql, DB_DB2)
            If rs.Fields("CCUDC1").Value = 1 Then
                aUsers = Split(rs.Fields("CCNOT1").Value, ",")
                For i = 0 To UBound(aUsers)
                    Usuarios &= sep & "'" & aUsers(i) & "'"
                    sep = ", "
                Next
                bBCC = (rs("CCUDC3").Value = 1)
            End If
            rs.Close()

            Dim Asunto As String = " PRIMERA TRANSACCION R " & rsIni.Fields("RIPROD").Value & " - " & rsIni.Fields("IDESC").Value
            Dim Body As String = " PRIMERA TRANSACCION  R " & vbNewLine & rsIni.Fields("RIPROD").Value & " - " & rsIni.Fields("IDESC").Value
            fSql = "UPDATE RIIM SET  RFLAG03 = '3'  WHERE RIPROD ='" & rsIni.Fields("RIPROD").Value & "'"
            DB_DB2.Execute(fSql)

            Dim success As Boolean = True
            success = mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
            If (success <> True) Then
                GuardaLog(success, "AVISOCRPRODUCTO", "ALERTA", "", mailman.LastErrorHtml, "")
                rs.Close()
                Return False
            End If

            ' Set the SMTP server host.
            mailman.SmtpHost = BSSSendDocs.My.Settings.MAILHOST()
            mailman.SmtpUsername = BSSSendDocs.My.Settings.MAILUSER
            mailman.SmtpPassword = BSSSendDocs.My.Settings.MAILPASS

            ' Create a simple email with a file attachment.
            ' Create a simple email with a file attachment.
            Dim email As New Chilkat.Email()

            ' Create an email from an HTML file.

            email.Body = Body
            If (email Is Nothing) Then
                GuardaLog(False, "AVISORPRODUCTO", "ALERTA", "", mailman.LastErrorHtml, "")
                rs.Close()
                Return False
            End If


            ' This email will have both a plain-text body and an HTML body.
            email.Subject = Asunto

            rs.Open("SELECT * FROM RCAU WHERE UTIPO='TM' AND USTS='A' AND UUSR IN(" & Usuarios & ")", DB_DB2)
            Do While Not rs.EOF
                email.AddTo(rs.Fields("UNOM").Value, rs.Fields("UEMAIL").Value)
                rs.MoveNext()
            Loop

            email.From = "Primera Transaccion R " + "<" + "ALogistica@brinsa.com.co" + ">"

            success = mailman.SendEmail(email)
            rs.Close()
            rsIni.MoveNext()
        End While
        Return True

    End Function



    Public Function AvisoConsumoProducto() As Boolean
        Dim rs As New ADODB.Recordset
        Dim rsIni As New ADODB.Recordset
        Dim mailman As New Chilkat.MailMan()
        Dim mht As New Chilkat.Mht()
        Dim aUsers() As String
        Dim sep As String = ""
        Dim i As Integer
        Dim bBCC As Boolean
        Dim Usuarios As String = ""
        Dim url As String = ServerRaizz & "tm/sc/alerta_ped.asp"


        fSql = "SELECT RIPROD, IDESC FROM RIIM INNER JOIN IIM ON RIPROD = IPROD WHERE RFLAG04  = '2' "
        rsIni.Open(fSql, DB_DB2)

        While Not rsIni.EOF

            fSql = "SELECT * FROM  ZCC WHERE CCTABL = 'FLAVISOS' AND CCCODE = 'PRODNUEVO'"
            rs.Open(fSql, DB_DB2)
            If rs.Fields("CCUDC1").Value = 1 Then
                aUsers = Split(rs.Fields("CCNOT1").Value, ",")
                For i = 0 To UBound(aUsers)
                    Usuarios &= sep & "'" & aUsers(i) & "'"
                    sep = ", "
                Next
                bBCC = (rs("CCUDC3").Value = 1)
            End If
            rs.Close()


            Dim Asunto As String = "PRIMER CONSUMO " & rsIni.Fields("RIPROD").Value & " - " & rsIni.Fields("IDESC").Value
            Dim Body As String = "PRIMER CONSUMO " & vbNewLine & rsIni.Fields("RIPROD").Value & " - " & rsIni.Fields("IDESC").Value
            fSql = "UPDATE RIIM SET  RFLAG04  = '3'  WHERE RIPROD ='" & rsIni.Fields("RIPROD").Value & "'"
            DB_DB2.Execute(fSql)

            Dim success As Boolean = True

            success = mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
            If (success <> True) Then
                GuardaLog(success, "AVISOCONSUMOPRODUCTO", "ALERTA", "", mailman.LastErrorHtml, "")
                rs.Close()
                Return False
            End If

            success = mht.UnlockComponent("SMANRIMHT_dEnsKQ0g3Rue")
            If (success <> True) Then
                GuardaLog(success, "AVISOCONSUMOPRODUCTO", "ALERTA", "", mailman.LastErrorHtml, "")
                rs.Close()
                Return False
            End If

            mht.UseCids = True

            ' Set the SMTP server host.
            mailman.SmtpHost = BSSSendDocs.My.Settings.MAILHOST()
            mailman.SmtpUsername = BSSSendDocs.My.Settings.MAILUSER
            mailman.SmtpPassword = BSSSendDocs.My.Settings.MAILPASS

            Dim email As New Chilkat.Email()


            email.Body = Body
            If (email Is Nothing) Then
                GuardaLog(False, "AVISOCONSUMOPRODUCTO", "ALERTA", "", mailman.LastErrorHtml, "")
                rs.Close()
                Return False
            End If

            email.Subject = Asunto

            rs.Open("SELECT * FROM RCAU WHERE UTIPO='TM' AND USTS='A' AND UUSR IN(" & Usuarios & ")", DB_DB2)
            Do While Not rs.EOF
                email.AddTo(rs.Fields("UNOM").Value, rs.Fields("UEMAIL").Value)
                rs.MoveNext()
            Loop

            email.From = "Primer Consumo Producto" + "<" + "ALogistica@brinsa.com.co" + ">"

            success = mailman.SendEmail(email)
            rs.Close()
            rsIni.MoveNext()
        End While
        Return True

    End Function

    Public Function Predist() As Boolean

        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim mailman As New Chilkat.MailMan()
        Dim mht As New Chilkat.Mht()
        Dim sep As String = ""
        Dim i As Integer
        Dim Usuarios As String = ""

        fSql = "SELECT * FROM RMAILS WHERE SENVIADO=0 AND SMOD = 'PREDIST'"
        rs.Open(fSql, DB_DB2)

        While Not rs.EOF

          fSql = " SELECT " : vSql = ""

            fSql = fSql & " STAD4  , " : vSql = vSql & "|Tienda"
            fSql = fSql & " ETIENDAD  ,  " : vSql = vSql & "|Nombre Tienda"
            fSql = fSql & " SUM(EQCON) AS EQCON " : vSql = vSql & "|Cantidad"
            fSql = fSql & " FROM RDCCAD INNER JOIN EST "
            fSql = fSql & " ON EPCUST=TCUST AND ETIENDA=TSHIP "
            fSql = fSql & " WHERE ECONSO = '" & rs("SFILE").Value.ToString & "' "
            fSql = fSql & " GROUP BY ETIENDAD, ETIENDA, STAD4  "
            fSql = fSql & " ORDER BY EQCON DESC, ETIENDA  "
            '    ReportXls(fSql, vSql, rs("SFILE").Value.ToString)

            fSql = "SELECT * FROM RMAILL WHERE LID=" & rs("SIDREF").Value.ToString
            rs1.Open(fSql, DB_DB1)

            While Not rs1.EOF
                Dim url As String = rs("SMHT").Value.ToString
                Dim Asunto As String = "Etiqueta Predistribuido  " & rs("SFILE").Value.ToString
                Dim success As Boolean = True

                success = mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
                If (success <> True) Then
                    GuardaLog(success, "SEGUIMIENTO", "PREDIST", "", mailman.LastErrorHtml, "")
                    rs.Close()
                    Return False
                End If
                success = mht.UnlockComponent("SMANRIMHT_dEnsKQ0g3Rue")

                If (success <> True) Then
                    GuardaLog(success, "TRANSPORTE", "PALETIZADO", "", mailman.LastErrorHtml, "")
                    rs.Close()
                    Return False
                End If

                mht.UseCids = True
                mailman.SmtpHost = BSSSendDocs.My.Settings.MAILHOST()
                mailman.SmtpUsername = BSSSendDocs.My.Settings.MAILUSER
                mailman.SmtpPassword = BSSSendDocs.My.Settings.MAILPASS


                Dim email As New Chilkat.Email()
                Dim aDestinos() As String
                Dim aUnaCuenta() As String

                email.Subject = Asunto
                email.Body = Asunto
                email.From = rs1.Fields("LMAIL").Value
                aDestinos = Split(rs1.Fields("LLSTTO").Value, vbCrLf)
                For i = 0 To UBound(aDestinos)
                    If InStr(aDestinos(i), "@") > 0 Then
                        aUnaCuenta = Split(aDestinos(i), ",")
                        If UBound(aUnaCuenta) = 1 Then
                            email.AddTo(Trim(aUnaCuenta(0)), Trim(aUnaCuenta(1)))
                        Else
                            email.AddTo("", Trim(aDestinos(i)))
                        End If
                        Debug.Print(aDestinos(i))
                    End If
                Next

                If Trim(rs1.Fields("LLSTCC").Value) <> "" Then
                    aDestinos = Split(rs1.Fields("LLSTCC").Value, vbCrLf)
                    For i = 0 To UBound(aDestinos)
                        If InStr(aDestinos(i), "@") > 0 Then
                            aUnaCuenta = Split(aDestinos(i), ",")
                            If UBound(aUnaCuenta) = 1 Then
                                email.AddCC(Trim(aUnaCuenta(0)), Trim(aUnaCuenta(1)))
                            Else
                                email.AddCC("", Trim(aDestinos(i)))
                            End If
                        End If
                    Next
                End If
                email.AddFileAttachment(BSSSendDocs.My.Settings.CARPETARAIZ & "\PD" & rs("SFILE").Value.ToString & ".xlsx")
                success = mailman.SendEmail(email)
                If success Then
                    fSql = "UPDATE RMAILS SET SENVIADO=1 WHERE SID=" & rs("SID").Value.ToString
                    DB_DB2.Execute(fSql)
                Else
                    GuardaLog(success, "TRANSPORTE", "PALETIZADO", "", mailman.LastErrorHtml, "")
                End If
                rs1.MoveNext()
            End While
            rs1.Close()
            rs.MoveNext()
        End While
        rs.Close()

        Return True
    End Function



    Public Function ErrorEDI() As Boolean

        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim rs2 As New ADODB.Recordset
        Dim mailman As New Chilkat.MailMan()
        Dim mht As New Chilkat.Mht()
        Dim sep As String = ""
        Dim i As Integer
        Dim Usuarios As String = ""

        fSql = "SELECT * FROM RMAILS WHERE SENVIADO=0 AND SMOD = 'ERROR_EDI'"
        rs.Open(fSql, DB_DB2)

        While Not rs.EOF

            Dim url As String = "http://transportes.brinsa.com.co/tm/sc/admin/AvisoEDI.asp?ID=" & rs("SID").Value.ToString
            Dim Asunto As String = "Error EDI Orden No  " & rs("SREF").Value.ToString
            Dim success As Boolean = True

            success = mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
            If (success <> True) Then
                GuardaLog(success, "SEGUIMIENTO", "ERROREDI", "", mailman.LastErrorHtml, "")
                rs.Close()
                Return False
            End If
            success = mht.UnlockComponent("SMANRIMHT_dEnsKQ0g3Rue")

            If (success <> True) Then
                GuardaLog(success, "SEGUIMIENTO", "ERROREDI", "", mailman.LastErrorHtml, "")
                rs.Close()
                Return False
            End If


            mht.UseCids = True
            mailman.SmtpHost = BSSSendDocs.My.Settings.MAILHOST()
            mailman.SmtpUsername = BSSSendDocs.My.Settings.MAILUSER
            mailman.SmtpPassword = BSSSendDocs.My.Settings.MAILPASS
            Dim email As New Chilkat.Email()
            email = mht.GetEmail(url)

            If (email Is Nothing) Then
                GuardaLog(False, "FLAVISOS", "PEDDECIMALES", "", mailman.LastErrorHtml, "")
                rs.Close()
                Return False

            End If
                email.Subject = Asunto
            ' email.Body = Asunto
            email.From = "Errores EDI " + "<" + "ALogistica@brinsa.com.co" + ">"



            fSql = "SELECT * FROM  RFPARAM WHERE CCTABL = 'ALERTAEDI' AND CCCODE2 = '1' "
            rs1.Open(fSql, DB_DB2)

            While Not rs1.EOF
                fSql = "SELECT * FROM  RCAU WHERE UUSR = '" & rs1("CCCODE").Value & "'"
                rs2.Open(fSql, DB_DB2)

                If Not rs2.EOF Then
                    email.AddTo(rs2("UNOM").Value, rs2("UEMAIL").Value)
                End If
                rs2.Close()
                rs1.MoveNext()
            End While
            rs1.Close()

            success = mailman.SendEmail(email)
            If success Then
                fSql = "UPDATE RMAILS SET SENVIADO=1 WHERE SID=" & rs("SID").Value.ToString
                DB_DB2.Execute(fSql)
            Else
                GuardaLog(success, "SEGUIMIENTO", "ERROREDI", "", mailman.LastErrorHtml, "")
            End If
            rs.MoveNext()
        End While
        rs.Close()

        Return True
    End Function


    Public Function ReportXls(ByVal sql As String, ByVal sTit As String, ByVal conso As String)

        Dim DB_LOC As New ADODB.Connection
        Dim rs As New ADODB.Recordset
        Dim aTit() As String
        Dim i As Integer
        Dim obj As Excel.XlListObjectSourceType = Excel.XlListObjectSourceType.xlSrcRange
        Dim obj2 As Excel.XlYesNoGuess = Excel.XlYesNoGuess.xlYes
        Dim obj3 As Excel.Range
        Dim auxiliar

        With rs
            .CursorLocation = ADODB.CursorLocationEnum.adUseClient
            On Error Resume Next
            .Open(sql, DB_DB2)

            On Error GoTo 0
        End With

        'Create a new workbook in Excel
        Dim oExcel As Excel.Application '  Object
        Dim oBook As Excel.Workbook 'Object
        Dim oSheet As Excel.Worksheet  ' Object
        oExcel = CreateObject("Excel.Application")

        oBook = oExcel.Workbooks.Add
        oSheet = oBook.Worksheets(1)

        'Transfer the data to Excel

        aTit = Split(sTit, "|")
        If UBound(aTit) > 0 Then
            For i = 0 To rs.Fields.Count - 1
                oSheet.Cells(1, i + 1) = aTit(i + 1)
                oSheet.Cells(1, i + 1).Font.Bold = True
            Next
        Else
            For i = 0 To rs.Fields.Count - 1
                oSheet.Cells(1, i + 1) = (rs.Fields(i).Name) 'TitCampo(rs.Fields(i).Name)
                oSheet.Cells(1, i + 1).Font.Bold = True
            Next
        End If

        oSheet.Range("A2").CopyFromRecordset(rs)

        obj3 = oSheet.Range(oSheet.Cells(1, 1), oSheet.Cells(rs.RecordCount + 1, rs.Fields.Count))
        oSheet.ListObjects.AddEx(obj, obj3, , obj2).Name = "Tabla1"

        oSheet.ListObjects("Tabla1").TableStyle = "TableStyleMedium2"
        oSheet.Cells.EntireColumn.AutoFit()
        '   oSheet.Cells.Rows("1:1").VerticalAlignment =
        oExcel.Visible = True
        oExcel.Application.DisplayAlerts = False
        oBook.SaveAs(BSSSendDocs.My.Settings.CARPETARAIZ & "\PD" & conso & ".xlsx", CreateBackup:=False)
        oExcel.Quit()
        oExcel = Nothing

        rs.Close()
        rs = Nothing

    End Function

    Public Function AvisoPedidoConDecimales() As Boolean

        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim rs2 As New ADODB.Recordset
        Dim rs3 As New ADODB.Recordset
        Dim mailman As New Chilkat.MailMan()
        Dim mht As New Chilkat.Mht()
        Dim sep As String = ""
        Dim i As Integer
        Dim Usuarios As String = ""
        Dim aUsers() As String
        fSql = "SELECT * FROM RENTDE WHERE DQCHG = 1"
        rs.Open(fSql, DB_DB2)

        While Not rs.EOF
            fSql = "SELECT * FROM RENTDE WHERE DDOCNR =" & rs("DDOCNR").Value.ToString & " AND DQCHG = 1 AND DLINE = (SELECT  MIN (DLINE) FROM RENTDE WHERE DDOCNR = " & rs("DDOCNR").Value.ToString & " AND DQCHG = 1)"
            rs1.Open(fSql, DB_DB1)

            While Not rs1.EOF
                '              Dim url As String = rs("SMHT").Value.ToString
                Dim Asunto As String = "Notificacion de Pedido Con decimales  Ped No" & rs("DPEDLX").Value.ToString
                Dim success As Boolean = True
                fSql = "SELECT * FROM  ZCC WHERE CCTABL = 'FLAVISOS' AND CCCODE = 'PEDDECIMALES'"
                rs2.Open(fSql, DB_DB2)
                If rs2.Fields("CCUDC1").Value = 1 Then
                    aUsers = Split(rs2.Fields("CCNOT1").Value, ",")
                    For i = 0 To UBound(aUsers)
                        Usuarios &= sep & "'" & aUsers(i) & "'"
                        sep = ", "
                    Next
                    bBCC = (rs2("CCUDC3").Value = 1)
                End If
                rs2.Close()

                success = mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
                If (success <> True) Then
                    GuardaLog(success, "TRANSPORTE", "PALETIZADO", "", mailman.LastErrorHtml, "")
                    rs.Close()
                    Return False
                End If
                success = mht.UnlockComponent("SMANRIMHT_dEnsKQ0g3Rue")

                If (success <> True) Then
                    GuardaLog(success, "TRANSPORTE", "PALETIZADO", "", mailman.LastErrorHtml, "")
                    rs.Close()
                    Return False
                End If


                ' Create an email from an HTML file.
                Dim url As String = ServerRaizz & "tm/mail/PedidosDecimales.asp?DPEDLX=" & rs.Fields("DPEDLX").Value.ToString
                '                url = "http://transportes.brinsa.com.co//tm/mail/PedidosDecimales.asp?DPEDLX=1045399"
                mht.UseCids = True
                mailman.SmtpHost = BSSSendDocs.My.Settings.MAILHOST()
                mailman.SmtpUsername = BSSSendDocs.My.Settings.MAILUSER
                mailman.SmtpPassword = BSSSendDocs.My.Settings.MAILPASS

                Dim email As New Chilkat.Email()
                email = mht.GetEmail(url)


                If (email Is Nothing) Then
                    GuardaLog(False, "FLAVISOS", "PEDDECIMALES", "", mailman.LastErrorHtml, "")
                    rs.Close()
                    Return False

                End If


                ' This email will have both a plain-text body and an HTML body.
                email.Subject = Asunto

                rs3.Open("SELECT * FROM RCAU WHERE UTIPO='TM' AND USTS='A' AND UUSR IN(" & Usuarios & ")", DB_DB2)
                Do While Not rs3.EOF
                    email.AddTo(rs3.Fields("UNOM").Value, rs3.Fields("UEMAIL").Value)
                    rs3.MoveNext()
                Loop
                rs3.Close()

                email.From = "Pedido con decimales" + "<" + "ALogistica@brinsa.com.co" + ">"

                ' Send email.
                success = mailman.SendEmail(email)


                If success Then
                    fSql = "UPDATE RENTDE SET DQCHG =2 WHERE DDOCNR =" & rs("DDOCNR").Value.ToString & " AND DQCHG=1"
                    DB_DB2.Execute(fSql)
                Else
                    GuardaLog(success, "TRANSPORTE", "PEDDECIMALES", "", mailman.LastErrorHtml, "")
                End If
                rs1.MoveNext()
            End While
            rs1.Close()
            rs.MoveNext()
        End While
        rs.Close()
        Return True

    End Function

End Module
