﻿Imports System
Imports System.Timers
Imports System.Threading

Module LogicaNegocio
    Public DB_DB2 As ADODB.Connection
    Public DB_DB1 As ADODB.Connection
    Public gsConnString As String
    Public sFolderPDF As String
    Public sExt As String
    Public sLib As String
    Public Timer1 As System.Timers.Timer
    Public SessionId As Double
    Public bChkProceso As Boolean
    Public ServerRaizz As String

    Public Sub Proceso()

        CargaPrm()
        'Console.WriteLine(gsConnString)

        DB_DB2 = New ADODB.Connection
        DB_DB2.Open(gsConnString)


        DB_DB1 = New ADODB.Connection
        DB_DB1.Open(gsConnString)

        Console.WriteLine("Conexion OK")

        CargarUrl()
        AppEjecutando("SENDDOCS", 1)

        SessionId = CalcConsec("SENDDOCS", False, "99999999")
        Select CheckVer("SENDDOCS")
            Case ChkVerApp.AppDeshabilitada
                Console.WriteLine("Aplicacion Deshabilitada.")
                Thread.Sleep(5000)
                Exit Sub
            Case ChkVerApp.AppVersionIncorrect
                Console.WriteLine("Version Incorrecta.")
                Console.ReadKey()
                Exit Sub
                '    Case http://www.forticlient.com/video/002ChkVerApp.AppEjecutando
                '        Console.WriteLine("En Ejecucion.")
                '        Thread.Sleep(5000)
                '        Exit Sub
        End Select
        AppEjecutando("SENDDOCS", 2)

        Console.WriteLine("ErrorEDI")
        ErrorEDI()                              '<-----------------------------OK
        Console.WriteLine("Predist")
        Predist()

        Console.WriteLine("AvisoRProducto")
        AvisoRProducto()

        Console.WriteLine("AvisoPedidoConDecimales")
        AvisoPedidoConDecimales()              '<-----------------------------OK

        Console.WriteLine("AvisoConsumoProducto")
        AvisoConsumoProducto()

        Console.WriteLine("Aviso Cliente Nuevo")
        AvisoClienteNuevo()

        Console.WriteLine("Aviso Paletizado")
        AvisoPaletizado()

        Console.WriteLine("Pedidos Atrasados")
        PedidosAtrasados()

        Console.WriteLine("Alerta Pedidos Bandeja")
        AlertaBandeja()                         '<-----------------------------OK

        Console.WriteLine("Alerta SMS Planilla")
        AlertaBandejaSMS()

        Console.WriteLine("PPP")
        AprobPreciosProveedor()

        PrepaInfoOCFletes()
        Console.WriteLine("Tiquetes Incompletos.")
        TiquetesIncompletos()

        Console.WriteLine("Aprobacion de Ajustes")
        AprobAjustesFletes()

        Console.WriteLine("Aprobacion de Tarifas de Fletes")
        AprobTarifasFletes()

        Console.WriteLine("Vencimiento Precios de industria")
        VencimientoIndustria()

        AppEjecutando("SENDDOCS", 1)


        Console.WriteLine("Envío de Documentos. PAUSADO")
        EnvioDeDocumentos()


        Console.WriteLine("Envío de Documentos. PAUSADO")
        EnvioDeDocumentosIAL()

    End Sub



    Sub CargarUrl()

        Dim rs As New ADODB.Recordset
        rs.Open("SELECT CCNOT1   " & _
                "FROM ZCC WHERE CCTABL = 'LXLONG' AND CCCODE = 'SERVERRAIZ' ", DB_DB2)
        If Not rs.EOF Then
            ServerRaizz = rs("CCNOT1").Value & "/"
            'ServerRaizz = Replace(ServerRaizz, "//", "/")
        End If
        rs.Close()

    End Sub



    Sub EnvioDeDocumentos()

        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim Sql As String
        Dim locId As Integer
        Dim locDocumento As String
        Dim locAsunto As String
        Dim locModulo As String
        Dim Minutos As Integer = 30
        Dim r As Integer

        If Hour(Now()) >= 2 And Hour(Now()) < 6 Then
            Return
        End If

        '     TiposFactura()

        rs.Open("SELECT SID, SMOD, SSTS, SIDH, SREF, SFILE, SCRTDAT, SFENVIO, ( NOW() - SFENVIO ) AS DIFER, ( NOW() - SCRTDAT )  AS D0 " & _
                "FROM RMAILS WHERE SMOD = 'FACTURAS' AND SENVIADO  = 0 AND  SSTS < 90", DB_DB2)
        If rs.EOF Then
            Console.WriteLine("No hay envío de archivos pendientes.")
        End If
        Do While Not rs.EOF
            QueFolder(rs.Fields("SMOD").Value)
            'If Not Dir(sArchivo, FileAttribute.Archive) = "" Then
            locId = rs("SIDH").Value
            locDocumento = rs("SFILE").Value
            locModulo = rs("SMOD").Value
            locAsunto = qDoc(rs("SMOD").Value) & " No. " & rs.Fields("SREF").Value

            If rs.Fields("D0").Value > 100000 Then  'Hace mas de 10 horas
                Minutos = 60
            End If

            '     If rs.Fields("DIFER").Value > (Minutos * 100) Then
            Dim SSTS As String
            Dim SREF As String
            SSTS = rs.Fields("SREF").Value
            SREF = rs.Fields("SSTS").Value
            If EnviaDoc(locId, locDocumento, SSTS, locModulo, locAsunto, SREF) = True Then
                Sql = "UPDATE RMAILS SET "
                Sql = Sql & " SSTS = 99 , "
                Sql = Sql & " SFENVIO = NOW(), "
                Sql = Sql & " SENVIADO = 1 "
                Sql = Sql & " WHERE "
                Sql = Sql & " SID = " & rs.Fields("SID").Value
                ExecuteSQL(Sql, r)
            Else
                If rs.Fields("DIFER").Value > (Minutos * 100) Then
                    Sql = "UPDATE RMAILS SET "
                    If rs("SSTS").Value < 80 Then
                        Sql = Sql & " SSTS = SSTS + 1 , "
                    End If
                    Sql = Sql & " SFENVIO = NOW() + " & Minutos & " MINUTES ,"
                    Sql = Sql & " SENVIADO = 2 "
                    Sql = Sql & " WHERE "
                    Sql = Sql & " SID = " & rs.Fields("SID").Value
                    ExecuteSQL(Sql, r)
                End If
            End If
            ' End If
            rs.MoveNext()
        Loop
        rs.Close()

    End Sub

    Sub EnvioDeDocumentosIAL()

        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim Sql As String
        Dim locId As Integer
        Dim locDocumento As String
        Dim locAsunto As String
        Dim locModulo As String
        Dim Minutos As Integer = 30
        Dim r As Integer
        Dim numero

         If Hour(Now()) >= 2 And Hour(Now()) < 6 Then
            Return
        End If

        '     TiposFactura()

        rs.Open("SELECT SID, SMOD, SSTS, SIDH, SREF, SFILE, SCRTDAT, SFENVIO, ( NOW() - SFENVIO ) AS DIFER, ( NOW() - SCRTDAT )  AS D0 " & _
                "FROM RMAILS WHERE SMOD = 'IAL' AND SENVIADO  = 0 AND  SSTS < 90", DB_DB2)
        If rs.EOF Then
            Console.WriteLine("No hay envío de archivos pendientes.")
        End If
        Do While Not rs.EOF
            QueFolder(rs.Fields("SMOD").Value)
            'If Not Dir(sArchivo, FileAttribute.Archive) = "" Then
            locId = rs("SIDH").Value
            locDocumento = rs("SFILE").Value
            locModulo = rs("SMOD").Value
            numero = Replace(rs("SFILE").Value, "E:\BrinsaAP\ImagenesTransportes\pdf\IALpdf\", "")
            numero = Replace(numero, ".pdf", "")
            locAsunto = "Certificado de Calidad No " & numero

            If rs.Fields("D0").Value > 100000 Then  'Hace mas de 10 horas
                Minutos = 60
            End If

            '     If rs.Fields("DIFER").Value > (Minutos * 100) Then
            Dim SSTS As String
            Dim SREF As String
            SSTS = rs.Fields("SREF").Value
            SREF = rs.Fields("SSTS").Value

            If EnviaDoc(locId, locDocumento, SSTS, locModulo, locAsunto, SREF) = True Then
                Sql = "UPDATE RMAILS SET "
                Sql = Sql & " SSTS = 99 , "
                Sql = Sql & " SFENVIO = NOW(), "
                Sql = Sql & " SENVIADO = 1 "
                Sql = Sql & " WHERE "
                Sql = Sql & " SID = " & rs.Fields("SID").Value
                ExecuteSQL(Sql, r)
            Else
                If rs.Fields("DIFER").Value > (Minutos * 100) Then
                    Sql = "UPDATE RMAILS SET "
                    If rs("SSTS").Value < 80 Then
                        Sql = Sql & " SSTS = SSTS + 1 , "
                    End If
                    Sql = Sql & " SFENVIO = NOW() + " & Minutos & " MINUTES ,"
                    Sql = Sql & " SENVIADO = 2 "
                    Sql = Sql & " WHERE "
                    Sql = Sql & " SID = " & rs.Fields("SID").Value
                    ExecuteSQL(Sql, r)
                End If
            End If
            ' End If
            rs.MoveNext()
        Loop
        rs.Close()

    End Sub

    Sub TiposFactura()
        Dim rs As New ADODB.Recordset
        Dim fSql As String = ""
        Dim r As Integer

        fSql = " DECLARE GLOBAL TEMPORARY TABLE DOCSFAC "
        fSql &= "             (	QID NUMERIC(8, 0) NOT NULL DEFAULT 0 ,  "
        fSql &= " 	QPFX CHAR(2) CCSID 284 NOT NULL DEFAULT '' , "
        fSql &= " 	QFILE CHAR(30) CCSID 284 NOT NULL DEFAULT '' ) "
        fSql &= "       ON COMMIT DELETE ROWS "
        ExecuteSQL(fSql, r)

        fSql = " DELETE FROM QTEMP.DOCSFAC "
        ExecuteSQL(fSql, r)

        fSql = " INSERT INTO QTEMP.DOCSFAC "
        fSql &= " SELECT SID, LEFT(SREF, 2), SREF FROM RMAILS WHERE SMOD='FACTURAS' AND SSTS < 90 AND SENVIADO = 0"
        ExecuteSQL(fSql, r)

        fSql = " UPDATE QTEMP.DOCSFAC  "
        fSql &= " SET QPFX = CASE WHEN SUBSTR(QPFX, 2, 1) < '0' THEN QPFX ELSE LEFT(QPFX, 1) END "
        ExecuteSQL(fSql, r)

        fSql = " SELECT * FROM QTEMP.DOCSFAC LEFT OUTER JOIN ZCCL01 ON CCTABL='RDOCFAC' AND CCCODE =QPFX "
        fSql &= " WHERE CCCODE IS NULL "
        rs.Open(fSql, DB_DB2)
        Do While Not rs.EOF
            'fSql = "UPDATE RMAILS SET SSTS=98 WHERE SID=" & rs("QID").Value
            'ExecuteSQL(fSql, r)
            rs.MoveNext()
        Loop
        rs.Close()

    End Sub


    Function EnviaDoc(ByVal Id As Short, ByRef sDocumento As String, ByRef Factur As String, ByVal sPagina As String, ByVal sAsunto As String, ByVal Intento As Integer) As Boolean
        Dim rs As New ADODB.Recordset
        Dim mailman As New Chilkat.MailMan()
        Dim mht As New Chilkat.Mht()
        Dim aDestinos() As String
        Dim aUnaCuenta() As String
        Dim i As Integer
        Dim sArchivo As String

        ' Any string passed to UnlockComponent automatically begins a 30-day trial.
        Dim success As Boolean

        rs.Open("SELECT * FROM RMAILL WHERE LID = " & Id, DB_DB2)

        If Not rs.EOF Then

            If rs.Fields("LMOD").Value = "FACTURAS" Or rs.Fields("LMOD").Value = "IAL" Then
                sArchivo = sDocumento
            Else
                sArchivo = sFolderPDF + "\" + sDocumento + "." + sExt
            End If

            'If CopyFiles(sArchivo, sDocumento) Then
            sArchivo = sDocumento
            Console.WriteLine(rs.Fields("LMOD").Value)
            Console.WriteLine(sArchivo)

            success = mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")

            If (success <> True) Then
                GuardaLog(success, rs.Fields("LAPP").Value, rs.Fields("LMOD").Value, sArchivo, _
                          mailman.LastErrorHtml, sDocumento)
                rs.Close()
                Return False
            End If

            mailman.SmtpHost = BSSSendDocs.My.Settings.MAILHOST
            mailman.SmtpUsername = BSSSendDocs.My.Settings.MAILUSER
            mailman.SmtpPassword = BSSSendDocs.My.Settings.MAILPASS
            Dim email As New Chilkat.Email()

            success = mht.UnlockComponent("SMANRIMHT_dEnsKQ0g3Rue")
            If (success <> True) Then
                rs.Close()
                Return False
            End If

            mht.UseCids = True

            '   Create an email from an HTML file.
            '   " & request("IHDPFX") & "" & Ceros(request("IHDOCN"), 8) & "" & ext & ".pdf")
            '   
            If rs.Fields("LMOD").Value = "FACTURAS" Then
                email = mht.GetEmail(ServerRaizz & "Common/SendDocs/" & sPagina & ".asp?FAC=" & Factur)
            Else
                Dim numero
                numero = Replace(sDocumento, "E:\BrinsaAP\ImagenesTransportes\pdf\IALpdf\", "")
                numero = Replace(numero, ".pdf", "")
                email = mht.GetEmail(ServerRaizz & "Common/SendDocs/" & sPagina & ".asp?FAC=" & numero)
            End If

            If (email Is Nothing) Then
                GuardaLog(False, rs.Fields("LAPP").Value, rs.Fields("LMOD").Value, sArchivo, _
                          "Error en el link referido.", sDocumento)
                rs.Close()
                Return False
            End If

            email.Subject = sAsunto

            aDestinos = Split(rs.Fields("LLSTTO").Value, vbCrLf)
            For i = 0 To UBound(aDestinos)
                If InStr(aDestinos(i), "@") > 0 Then
                    aUnaCuenta = Split(aDestinos(i), ",")
                    If UBound(aUnaCuenta) = 1 Then
                        email.AddTo(Trim(aUnaCuenta(0)), Trim(aUnaCuenta(1)))
                    Else
                        email.AddTo("", Trim(aDestinos(i)))
                    End If
                    Debug.Print(aDestinos(i))
                End If
            Next

            'Si tiene destinatarios para copia
            If Trim(rs.Fields("LLSTCC").Value) <> "" Then
                aDestinos = Split(rs.Fields("LLSTCC").Value, vbCrLf)
                For i = 0 To UBound(aDestinos)
                    If InStr(aDestinos(i), "@") > 0 Then
                        aUnaCuenta = Split(aDestinos(i), ",")
                        If UBound(aUnaCuenta) = 1 Then
                            email.AddCC(Trim(aUnaCuenta(0)), Trim(aUnaCuenta(1)))
                        Else
                            email.AddCC("", Trim(aDestinos(i)))
                        End If
                    End If
                Next
            End If

            email.From = rs.Fields("LNOMBRE").Value + "<" + rs.Fields("LMAIL").Value + ">"

            Console.WriteLine(rs.Fields("LLSTTO").Value)
            Console.WriteLine(email.Subject)
            Console.WriteLine(email.From)

            ' Add a file attachment. 
            Dim contentType As String
            ' Returns the content-type of the attachment (determined by the file extension)
            ' This return value is for informational purposes only.
            contentType = email.AddFileAttachment(sArchivo)
            If contentType Is Nothing Then
                GuardaLog(False, rs.Fields("LAPP").Value, rs.Fields("LMOD").Value, sArchivo, _
                          email.LastErrorHtml, sDocumento)
                rs.Close()
                Return False
            End If

            ' Send email.
            success = mailman.SendEmail(email)
            GuardaLog(success, rs.Fields("LAPP").Value, rs.Fields("LMOD").Value, sArchivo, _
                      mailman.LastErrorHtml, sDocumento)

            If success Then
                Console.WriteLine("Enviado.")
            Else
                Console.Write(mailman.LastErrorText)
            End If
            System.IO.File.Delete(sArchivo)
        Else
            GuardaLog(False, rs.Fields("LAPP").Value, rs.Fields("LMOD").Value, sArchivo, _
                      "<hr><font color=""#FF0000"">No encontró el archivo" & sFolderPDF + "\" + sDocumento + "." + sExt & "</font><hr> ", sDocumento)
            If Intento >= 20 And Intento Mod 10 = 0 Then
                AvisaTecno("No encontró el archivo " & sFolderPDF + "\" + sDocumento + "." + sExt, sDocumento)
            End If
        End If
        'End If
        rs.Close()

        Return True

    End Function

    Public Sub QueFolder(ByVal sModulo As String)
        Dim rs As New ADODB.Recordset
        Dim Sql As String

        Sql = "SELECT  CCTABL, CCCODE,  CCDESC, CCSDSC, CCNOT1 || CCNOT2 AS FOLDER"
        Sql = Sql & "  FROM ZCC WHERE CCTABL = 'RDOCPATH' AND CCCODE = '" & sModulo & "'"
        rs.Open(Sql, DB_DB2)
        If Not rs.EOF Then
            sFolderPDF = Trim(rs.Fields("FOLDER").Value)
            sExt = Trim(rs.Fields("CCSDSC").Value)
        Else
            sFolderPDF = ""
            sExt = ""
        End If
        rs.Close()

    End Sub

    Public Sub GuardaLog(ByVal success As Boolean, ByVal sApp As String, ByVal sMod As String, _
                         ByVal sArchivo As String, ByVal sMsgError As String, ByVal sRef As String)
        Dim fSql As String
        Dim vSql As String

        fSql = " INSERT INTO RMAILLOG( "
        vSql = " VALUES( "
        fSql = fSql & "LIDLOG, " : vSql = vSql & " ( SELECT IFNULL( MAX( LIDLOG ), 0 ) + 1 FROM RMAILLOG ) , "
        If Not success Then
            fSql = fSql & "LSTS, " : vSql = vSql & "'" & "1" & "', "
        End If
        fSql = fSql & "LREF, " : vSql = vSql & "'" & Left(sRef, 2) & "', "
        fSql = fSql & "LAPP, " : vSql = vSql & "'" & Left(sArchivo, 10) & "', "
        fSql = fSql & "LERROR) " : vSql = vSql & "'" & sMsgError.Replace("'", """") & "' )"
        DB_DB2.Execute(fSql & vSql)

    End Sub

    Function qDoc(ByVal sMod As String) As String

        Select Case sMod
            Case "FACTURAS"
                Return "Factura"
            Case "PLANILLAS"
                Return "Planilla"
            Case "CERTQ"
                Return "Analisis de Laboratorio"
        End Select
        Return ""

    End Function

    Sub CargaPrm()

        gsConnString = BSSSendDocs.My.Settings.AS400

    End Sub

    Sub AvisaTecno(ByVal msg As String, ByVal docu As String)
        Dim rs As New ADODB.Recordset
        Dim mailman As New Chilkat.MailMan()
        Dim success As Boolean
        Dim aDestinos() As String
        Dim aUnaCuenta() As String
        Dim i As Integer

        rs.Open("SELECT * FROM RMAILL WHERE LID = -99", DB_DB2)
        If Not rs.EOF Then
            success = mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
            If (success <> True) Then
                rs.Close()
                Return
            End If

            '' Set the SMTP server host.
            mailman.SmtpHost = BSSSendDocs.My.Settings.MAILHOST
            mailman.SmtpUsername = BSSSendDocs.My.Settings.MAILUSER
            mailman.SmtpPassword = BSSSendDocs.My.Settings.MAILPASS

            Dim email As New Chilkat.Email()
            email.Subject = "Fallo Envio de documentos. Documento " & docu
            email.Body = msg & vbCrLf & ServerRaizz & "wms/docs/chequeo/senddocs/mail_chk.asp?s_SFILE=" & docu

            aDestinos = Split(rs.Fields("LLSTTO").Value, vbCrLf)
            For i = 0 To UBound(aDestinos)
                If InStr(aDestinos(i), "@") > 0 Then
                    aUnaCuenta = Split(aDestinos(i), ",")
                    If UBound(aUnaCuenta) = 1 Then
                        email.AddTo(Trim(aUnaCuenta(0)), Trim(aUnaCuenta(1)))
                    Else
                        email.AddTo("", Trim(aDestinos(i)))
                    End If
                    Debug.Print(aDestinos(i))
                End If
            Next

            'Si tiene destinatarios para copia
            If Trim(rs.Fields("LLSTCC").Value) <> "" Then
                aDestinos = Split(rs.Fields("LLSTCC").Value, vbCrLf)
                For i = 0 To UBound(aDestinos)
                    If InStr(aDestinos(i), "@") > 0 Then
                        aUnaCuenta = Split(aDestinos(i), ",")
                        If UBound(aUnaCuenta) = 1 Then
                            email.AddCC(Trim(aUnaCuenta(0)), Trim(aUnaCuenta(1)))
                        Else
                            email.AddCC("", Trim(aDestinos(i)))
                        End If
                    End If
                Next
            End If

            email.From = rs.Fields("LNOMBRE").Value + "<" + rs.Fields("LMAIL").Value + ">"

            If (Not mailman.SendEmail(email)) Then
            Else
            End If

        End If
    End Sub

End Module
