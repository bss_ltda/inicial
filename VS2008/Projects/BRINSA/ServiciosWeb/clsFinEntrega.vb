﻿Public Class clsFinEntrega
    Public Planilla As String
    Public pSender As String
    Public DB As clsConexion

    Function FinEntrega() As String
        Dim resultado As String = numPlani(Planilla)

        If Not IsNumeric(resultado) Then
            Return "90 Numero de Planilla Incorrecto"
        Else
            Planilla = Planilla.Substring(0, 6)
        End If
        If PlanillaOK() Then
            RegistraFinEntrega()
            resultado = "00 Fin de Entrega Registrado"
        Else
            resultado = "91 Planilla no encontrada."
        End If
        Return resultado

    End Function

    Private Function PlanillaOK() As Boolean
        Dim fSql As String
        Dim result As Boolean = False
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset

        If IsNumeric(Planilla) Then
            fSql = "SELECT * FROM RECC WHERE EPLANI = " & Planilla
            rs = DB.ExecuteSQL(fSql)
            If Not rs.EOF Then
                result = True
            Else
                fSql = "SELECT DCONSO, HSTS, HCELU FROM RDCC INNER JOIN RHCC ON DCONSO = HCONSO WHERE DPLANI = " & Planilla
                rs1 = DB.ExecuteSQL(fSql)
                If Not rs1.EOF Then
                    ActualizaRECC(rs1("DCONSO").Value)
                    rs.Requery()
                    If Not rs.EOF Then
                        result = True
                    End If
                End If
                rs1.Close()
            End If
            rs.Close()
        End If
        Return result

    End Function

    Private Sub RegistraFinEntrega()
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset

        fSql = "SELECT DCONSO, HSTS, HCELU FROM RDCC INNER JOIN RHCC ON DCONSO = HCONSO WHERE DPLANI = " & Planilla
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            fSql = " INSERT INTO RRCC( "
            vSql = " VALUES ( "
            fSql = fSql & " RTREP     , " : vSql = vSql & " 30 , "                                     '//2S0   Tipo Rep
            fSql = fSql & " RPLANI    , " : vSql = vSql & " " & Planilla & ", "                        '//6S0   Planilla
            fSql = fSql & " RCONSO    , " : vSql = vSql & "'" & rs("DCONSO").Value & "', "             '//6A    Consolidado
            fSql = fSql & " RSTSC     , " : vSql = vSql & " " & rs("HSTS").Value & ", "                '//2S0   Est Cons
            fSql = fSql & " RSTSE     , " : vSql = vSql & " 50, "                                      '//2S0   Est Entrega
            fSql = fSql & " RFECREP   , " : vSql = vSql & "NOW() , "                                   '//26Z   F.Reporte
            fSql = fSql & " RREPORT   , " : vSql = vSql & "'" & Planilla & " Fin de Entrega', "        '//502A  Reporte
            fSql = fSql & " RCRTUSR   , " : vSql = vSql & "'" & pSender & "', "                        '//10A   Usuario
            fSql = fSql & " RAPP      ) " : vSql = vSql & "'" & "SMS" & "' ) "                         '//15A   App

            DB.ExecuteSQL(fSql & vSql)

            fSql = "UPDATE RHCC SET HREPORT = '" & Planilla & " Fin de Entrega', "
            fSql = fSql & " HFECREP = NOW() "
            fSql = fSql & " WHERE HCONSO = '" & rs("DCONSO").Value & "'"
            DB.ExecuteSQL(fSql)

            fSql = "UPDATE RECC SET ESTS = 50, ESMS = 1 "
            fSql = fSql & ", EFINENT = NOW() "
            If pSender.Trim() = Replace(Replace(rs("HCELU").Value, " ", ""), "-", "") Then
                fSql = fSql & ", ECELOK = 1"
            End If
            fSql = fSql & " WHERE EPLANI = " & Planilla & " AND ESTS <= 50 "
            DB.ExecuteSQL(fSql)
            If DB.regActualizados() = 0 Then
                ActualizaRECC(rs("DCONSO").Value)
            End If

            fSql = "UPDATE RECC SET ESTS = 50 "
            fSql = fSql & " WHERE ECONSO='" & rs("DCONSO").Value & "' AND YEAR(EFINENT) >= 2014 AND ESTS < 50 AND EIDIVI <> 'QUIMICOS'"
            DB.ExecuteSQL(fSql)

            fSql = "SELECT * FROM RECC WHERE ECONSO = '" & rs("DCONSO").Value & "' AND ESTS < 50 AND EIDIVI <> 'QUIMICOS'"
            rs1 = DB.ExecuteSQL(fSql)
            If rs1.EOF Then
                fSql = "UPDATE RHCC SET HESTAD=6, HFESTAD=NOW() WHERE HCONSO='" & rs("DCONSO").Value & "'"
                DB.ExecuteSQL(fSql)
            End If
            rs1.Close()
        End If

    End Sub

    'ActualizaRECC()
    Public Sub ActualizaRECC(Conso As String)
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim bCompleto As Boolean
        Dim fSql, vSql As String

        'Actualiza número de Factura y de Planilla
        fSql = " SELECT DPEDID, DLINEA, LLLOAD, DPLANI  "
        fSql = fSql & " FROM RDCC INNER JOIN LLL ON DPEDID = LLORDN AND DLINEA=LLOLIN "
        fSql = fSql & " WHERE DCONSO = '" & Conso & "'"
        fSql = fSql & " AND DPLANI <> LLLOAD "
        rs = DB.ExecuteSQL(fSql)
        Do While Not rs.EOF
            fSql = "UPDATE RDCC SET DPLANI = " & rs("LLLOAD").Value
            fSql = fSql & " WHERE "
            fSql = fSql & " DPEDID = " & rs("DPEDID").Value
            fSql = fSql & " AND DLINEA = " & rs("DLINEA").Value
            DB.ExecuteSQL(fSql)
            rs.MoveNext()
        Loop
        rs.Close()
        fSql = " SELECT DPEDID, DLINEA, ILDPFX, ILDOCN, DLDPFX, DLDOCN  "
        fSql = fSql & " FROM RDCC INNER JOIN SIL ON DPEDID = ILORD AND DLINEA=ILSEQ "
        fSql = fSql & " WHERE DCONSO = '" & Conso & "'"
        fSql = fSql & " AND  DLDOCN <> ILDOCN  "
        DB.ExecuteSQL(fSql)
        rs = DB.ExecuteSQL(fSql)
        Do While Not rs.EOF
            fSql = "UPDATE RDCC SET DLDPFX = '" & rs("ILDPFX").Value & "', DLDOCN = " & rs("ILDOCN").Value
            fSql = fSql & " WHERE "
            fSql = fSql & " DPEDID = " & rs("DPEDID").Value
            fSql = fSql & " AND DLINEA = " & rs("DLINEA").Value
            DB.ExecuteSQL(fSql)
            rs.MoveNext()
        Loop
        rs.Close()

        bCompleto = True
        fSql = "SELECT DPLANI FROM RDCC LEFT OUTER JOIN RECC ON DNUMENT = EID AND DPLANI = EPLANI "
        fSql = fSql & " WHERE DCONSO = '" & Conso & "' AND EID IS NULL "
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            bCompleto = False
        End If
        rs.Close()
        fSql = "SELECT EPLANI FROM RECC LEFT OUTER JOIN RDCC ON EID = DNUMENT AND EPLANI = DPLANI"
        fSql = fSql & " WHERE ECONSO = '" & Conso & "' AND DPLANI IS NULL "
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            bCompleto = False
        End If
        rs.Close()
        If bCompleto Then
            rs = Nothing
            fSql = "DELETE FROM RECC "
            fSql = fSql & " WHERE ECONSO='" & Conso & "' AND EPLANI = 0"
            DB.ExecuteSQL(fSql)
            Exit Sub
        End If

        fSql = "UPDATE RDCC SET DNUMENT=0 "
        fSql = fSql & " WHERE "
        fSql = fSql & " DCONSO='" & Conso & "' "
        DB.ExecuteSQL(fSql)

        fSql = "UPDATE RECC SET EPLANI = 0 WHERE ECONSO = '" & Conso & "'"
        DB.ExecuteSQL(fSql)

        fSql = "SELECT DPEDID, DPLANI, MAX(DORDENT) AS DORDENT, DIDIVI, DCUST, DSHIP  "
        fSql = fSql & " FROM RDCC "
        fSql = fSql & " WHERE DCONSO='" & Conso & "' AND DPLANI > 0 "
        fSql = fSql & " GROUP BY DPEDID, DPLANI, DIDIVI, DCUST, DSHIP "
        fSql = fSql & " ORDER BY DPEDID, DPLANI "
        rs = DB.ExecuteSQL(fSql)
        Do While Not rs.EOF
            fSql = "SELECT EID FROM RECC WHERE "
            fSql = fSql & "     ECONSO = '" & Conso & "'"
            fSql = fSql & " AND EPEDID = " & rs("DPEDID").Value
            fSql = fSql & " AND EIDIVI = '" & rs("DIDIVI").Value & "'"
            fSql = fSql & " AND ECUST  = " & rs("DCUST").Value
            fSql = fSql & " AND ESHIP  = " & rs("DSHIP").Value
            fSql = fSql & " AND EPLANI = 0"
            rs1 = DB.ExecuteSQL(fSql)
            If Not rs1.EOF Then
                fSql = "UPDATE RECC SET EPLANI = " & rs("DPLANI").Value
                fSql = fSql & " WHERE EID = " & rs1("EID").Value
                DB.ExecuteSQL(fSql)
            Else
                fSql = " INSERT INTO RECC("
                vSql = " SELECT"
                fSql = fSql & " ECONSO," : vSql = vSql & " DCONSO,"
                fSql = fSql & " EPEDID," : vSql = vSql & " DPEDID,"
                fSql = fSql & " EPLANI," : vSql = vSql & " DPLANI,"
                fSql = fSql & " EORDENT," : vSql = vSql & " MAX(DORDENT),"
                fSql = fSql & " EFREQCIT," : vSql = vSql & " MAX(DFEC02),"
                fSql = fSql & " EIDIVI," : vSql = vSql & " DIDIVI,"
                fSql = fSql & " ECUST," : vSql = vSql & " DCUST,"
                fSql = fSql & " ESHIP," : vSql = vSql & " DSHIP,"
                fSql = fSql & " EPESOD," : vSql = vSql & " SUM( DPESO ), "
                fSql = fSql & " ECANTD )" : vSql = vSql & " SUM( DCANT )"
                vSql = vSql & " FROM RDCC "
                vSql = vSql & " WHERE DPLANI = " & rs("DPLANI").Value
                vSql = vSql & " GROUP BY DCONSO, DPEDID, DPLANI, DIDIVI, "
                vSql = vSql & "          DCUST, DSHIP "
                DB.ExecuteSQL(fSql & vSql)
            End If
            rs.MoveNext()
        Loop
        rs.Close()

        fSql = "        UPDATE RECC E SET ( EHNAME, ENSHIP, ESPOST, ESHDEP  ) = ("
        fSql = fSql & "    SELECT TCUSTNAME, TNAME, TPOST, TSTE "
        fSql = fSql & "    FROM RVESTTOT00 WHERE E.ECUST=TCUST AND E.ESHIP=TSHIP )"
        fSql = fSql & " WHERE "
        fSql = fSql & " ECONSO='" & Conso & "'"
        DB.ExecuteSQL(fSql)

        fSql = "DELETE FROM RECC "
        fSql = fSql & " WHERE ECONSO='" & Conso & "' AND EPLANI = 0"
        DB.ExecuteSQL(fSql)

        fSql = "UPDATE RDCC D SET ( DNUMENT ) = ("
        fSql = fSql & " SELECT EID FROM RECC WHERE D.DCONSO = ECONSO AND D.DPEDID = EPEDID "
        fSql = fSql & " AND D.DIDIVI=EIDIVI AND D.DCUST=ECUST AND D.DSHIP=ESHIP AND D.DPLANI = EPLANI)"
        fSql = fSql & " WHERE "
        fSql = fSql & " DCONSO='" & Conso & "' AND DPLANI > 0"
        DB.ExecuteSQL(fSql)
    End Sub

    Public Function numPlani(ByVal txtPlani As String) As String
        Dim plani As String = "ERRPLANI"
        Dim datPlani As String = ""
        Dim i As Integer
        Dim util As New bssUtil

        txtPlani = Replace(txtPlani, " ", "")
        txtPlani = Replace(txtPlani, "-", "")
        For i = 0 To Len(txtPlani) - 1
            If IsNumeric(txtPlani.Substring(i, 1)) Then
                datPlani &= txtPlani.Substring(i, 1)
            End If
        Next

        If datPlani.Length = 7 Then
            If txtPlani.Substring(6, 1) = util.dv(txtPlani.Substring(0, 6)) Then
                plani = txtPlani.Substring(0, 6)
            Else
                plani = "ERRDV"
            End If
        Else
            plani = "ERRDV"
        End If

        Return plani

    End Function

    Public Function ValidaPlanilla(numPlani As String) As String
        Dim pReporte, pPlanilla As String
        Dim s, sep As String
        Dim aReporte() As String
        Dim i As Integer

        pReporte = Replace(numPlani, "\n", " ")
        pReporte = Replace(pReporte, "\r", " ")
        pReporte = Replace(pReporte, "_", "-")
        pReporte = Replace(pReporte, "/", " ")
        pReporte = Trim(pReporte)
        pPlanilla = ""
        For i = 0 To Len(pReporte) - 1
            If IsNumeric(pReporte.Substring(i, 1)) Or pReporte.Substring(i, 1) = " " Then
                pPlanilla &= pReporte.Substring(i, 1)
            End If
        Next
        Do While InStr(pPlanilla, "  ") > 0
            pPlanilla = Replace(pPlanilla, "  ", " ")
        Loop
        aReporte = Split(pPlanilla, " ")
        i = 0
        s = ""
        s = ""
        sep = ""
        aReporte = Split(pPlanilla, " ")
        For i = 0 To UBound(aReporte) Step 2
            If i < UBound(aReporte) Then
                If aReporte(i + 1).Length = 1 Then
                    s &= sep & aReporte(i) & aReporte(i + 1)
                Else
                    s &= sep & aReporte(i) & " " & aReporte(i + 1)
                End If
            Else
                s &= sep & aReporte(i)
            End If
            sep = " "
        Next
        pPlanilla = s.Trim()
        Return pPlanilla

    End Function

End Class
