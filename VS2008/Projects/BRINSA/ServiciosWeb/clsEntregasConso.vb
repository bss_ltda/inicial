﻿Public Class clsEntregasConso
    Public Consolidado As String
    Public DB As clsConexion

    'ActualizaRECC()
    Public Sub ActualizaRECC()
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim bCompleto As Boolean

        'Actualiza número de Factura y de Planilla
        fSql = " SELECT DPEDID, DLINEA, LLLOAD, DPLANI  "
        fSql = fSql & " FROM RDCC INNER JOIN LLL ON DPEDID = LLORDN AND DLINEA=LLOLIN "
        fSql = fSql & " WHERE DCONSO = '" & Consolidado & "'"
        fSql = fSql & " AND DPLANI <> LLLOAD "
        rs = DB.ExecuteSQL(fSql)
        Do While Not rs.EOF
            fSql = "UPDATE RDCC SET DPLANI = " & rs("LLLOAD").Value
            fSql = fSql & " WHERE "
            fSql = fSql & " DPEDID = " & rs("DPEDID").Value
            fSql = fSql & " AND DLINEA = " & rs("DLINEA").Value
            DB.ExecuteSQL(fSql)
            rs.MoveNext()
        Loop
        rs.Close()
        fSql = " SELECT DPEDID, DLINEA, ILDPFX, ILDOCN, DLDPFX, DLDOCN  "
        fSql = fSql & " FROM RDCC INNER JOIN SIL ON DPEDID = ILORD AND DLINEA=ILSEQ "
        fSql = fSql & " WHERE DCONSO = '" & Consolidado & "'"
        fSql = fSql & " AND  DLDOCN <> ILDOCN  "
        DB.ExecuteSQL(fSql)
        rs = DB.ExecuteSQL(fSql)
        Do While Not rs.EOF
            fSql = "UPDATE RDCC SET DLDPFX = '" & rs("ILDPFX").Value & "', DLDOCN = " & rs("ILDOCN").Value
            fSql = fSql & " WHERE "
            fSql = fSql & " DPEDID = " & rs("DPEDID").Value
            fSql = fSql & " AND DLINEA = " & rs("DLINEA").Value
            DB.ExecuteSQL(fSql)
            rs.MoveNext()
        Loop
        rs.Close()

        bCompleto = True
        fSql = "SELECT DPLANI FROM RDCC LEFT OUTER JOIN RECC ON DNUMENT = EID AND DPLANI = EPLANI "
        fSql = fSql & " WHERE DCONSO = '" & Consolidado & "' AND EID IS NULL "
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            bCompleto = False
        End If
        rs.Close()

        fSql = "SELECT EPLANI FROM RECC "
        fSql = fSql & " WHERE ECONSO = '" & Consolidado & "'"
        rs = DB.ExecuteSQL(fSql)
        If rs.EOF Then
            bCompleto = False
        End If
        rs.Close()

        fSql = "SELECT EPLANI FROM RECC LEFT OUTER JOIN RDCC ON EID = DNUMENT AND EPLANI = DPLANI"
        fSql = fSql & " WHERE ECONSO = '" & Consolidado & "' AND DPLANI IS NULL "
        rs = DB.ExecuteSQL(fSql)
        If Not rs.EOF Then
            bCompleto = False
        End If
        rs.Close()
        If bCompleto Then
            rs = Nothing
            fSql = "DELETE FROM RECC "
            fSql = fSql & " WHERE ECONSO='" & Consolidado & "' AND EPLANI = 0"
            DB.ExecuteSQL(fSql)
            Exit Sub
        End If

        fSql = "UPDATE RDCC SET DNUMENT=0 "
        fSql = fSql & " WHERE "
        fSql = fSql & " DCONSO='" & Consolidado & "' "
        DB.ExecuteSQL(fSql)

        fSql = "UPDATE RECC SET EPLANI = 0 WHERE ECONSO = '" & Consolidado & "'"
        DB.ExecuteSQL(fSql)

        fSql = "SELECT DPEDID, DPLANI, MAX(DORDENT) AS DORDENT, DIDIVI, DCUST, DSHIP  "
        fSql = fSql & " FROM RDCC "
        fSql = fSql & " WHERE DCONSO='" & Consolidado & "' AND DPLANI > 0 "
        fSql = fSql & " GROUP BY DPEDID, DPLANI, DIDIVI, DCUST, DSHIP "
        fSql = fSql & " ORDER BY DPEDID, DPLANI "
        rs = DB.ExecuteSQL(fSql)
        Do While Not rs.EOF
            fSql = "SELECT EID FROM RECC WHERE "
            fSql = fSql & "     ECONSO = '" & Consolidado & "'"
            fSql = fSql & " AND EPEDID = " & rs("DPEDID").Value
            fSql = fSql & " AND EIDIVI = '" & rs("DIDIVI").Value & "'"
            fSql = fSql & " AND ECUST  = " & rs("DCUST").Value
            fSql = fSql & " AND ESHIP  = " & rs("DSHIP").Value
            fSql = fSql & " AND EPLANI = 0"
            rs1 = DB.ExecuteSQL(fSql)
            If Not rs1.EOF Then
                fSql = "UPDATE RECC SET EPLANI = " & rs("DPLANI").Value
                fSql = fSql & " WHERE EID = " & rs1("EID").Value
                DB.ExecuteSQL(fSql)
            Else
                fSql = " INSERT INTO RECC("
                vSql = " SELECT"
                fSql = fSql & " ECONSO," : vSql = vSql & " DCONSO,"
                fSql = fSql & " EPEDID," : vSql = vSql & " DPEDID,"
                fSql = fSql & " EPLANI," : vSql = vSql & " DPLANI,"
                fSql = fSql & " EORDENT," : vSql = vSql & " MAX(DORDENT),"
                fSql = fSql & " EFREQCIT," : vSql = vSql & " MAX(DFEC02),"
                fSql = fSql & " EIDIVI," : vSql = vSql & " DIDIVI,"
                fSql = fSql & " ECUST," : vSql = vSql & " DCUST,"
                fSql = fSql & " ESHIP," : vSql = vSql & " DSHIP,"
                fSql = fSql & " EPESOD," : vSql = vSql & " SUM( DPESO ), "
                fSql = fSql & " ECANTD )" : vSql = vSql & " SUM( DCANT )"
                vSql = vSql & " FROM RDCC "
                vSql = vSql & " WHERE DPLANI = " & rs("DPLANI").Value
                vSql = vSql & " GROUP BY DCONSO, DPEDID, DPLANI, DIDIVI, "
                vSql = vSql & "          DCUST, DSHIP "
                DB.ExecuteSQL(fSql & vSql)
            End If
            rs.MoveNext()
        Loop
        rs.Close()

        fSql = "        UPDATE RECC E SET ( EHNAME, ENSHIP, ESPOST, ESHDEP  ) = ("
        fSql = fSql & "    SELECT TCUSTNAME, TNAME, TPOST, TSTE "
        fSql = fSql & "    FROM RVESTTOT00 WHERE E.ECUST=TCUST AND E.ESHIP=TSHIP )"
        fSql = fSql & " WHERE "
        fSql = fSql & " ECONSO='" & Consolidado & "'"
        DB.ExecuteSQL(fSql)

        fSql = "DELETE FROM RECC "
        fSql = fSql & " WHERE ECONSO='" & Consolidado & "' AND EPLANI = 0"
        DB.ExecuteSQL(fSql)

        fSql = "UPDATE RDCC D SET ( DNUMENT ) = ("
        fSql = fSql & " SELECT EID FROM RECC WHERE D.DCONSO = ECONSO AND D.DPEDID = EPEDID "
        fSql = fSql & " AND D.DIDIVI=EIDIVI AND D.DCUST=ECUST AND D.DSHIP=ESHIP AND D.DPLANI = EPLANI)"
        fSql = fSql & " WHERE "
        fSql = fSql & " DCONSO='" & Consolidado & "' AND DPLANI > 0"
        DB.ExecuteSQL(fSql)
    End Sub


End Class
