﻿Public Class clsReportTransito
    Public Usuario As String
    Public Pass As String
    Public Conso As String
    Public Manifiesto As String
    Public Placa As String
    Public FechaReporte As String
    Public codUbicacion As String
    Public Ubicacion As String
    Public tipoOrigen As String
    Public tipoNovedad As String
    Public Reporte As String
    Public EstadEntrega As Integer
    Public DB As clsConexion

    Function ReportaNovedad() As String
        Dim resultado As String = ""
        Dim numReporte As String
        Dim rs As New ADODB.Recordset

        If DB.Login(Usuario, Pass) Then
            fSql = "SELECT * FROM RHCC WHERE HCONSO='" & Conso & "'"
            rs = DB.ExecuteSQL(fSql)
            If Not rs.EOF Then
                numReporte = DB.CalcConsec("RRCCREPTR", "99999999")
                fSql = " INSERT INTO RRCC( "
                vSql = " VALUES ( "
                fSql = fSql & " RTREP     , " : vSql = vSql & " " & 3 & ", "                                            '//2S0   Tipo Rep
                fSql = fSql & " RNUM      , " : vSql = vSql & " " & numReporte & ", "                                   '//11S0   
                fSql = fSql & " RCONSO    , " : vSql = vSql & "'" & Conso & "', "                                       '//6A    Consolidado
                fSql = fSql & " RMANIF    , " : vSql = vSql & "'" & Left(Manifiesto, 15) & "', "                        '//15A   Manifiesto del Transp
                fSql = fSql & " RPLACA    , " : vSql = vSql & "'" & Left(UCase(Trim(Placa)), 6) & "', "                 '//6A    Placa
                fSql = fSql & " RSTSC     , " : vSql = vSql & " " & rs("HSTS").Value & ", "                                     '//2S0   Est Cons
                fSql = fSql & " RSTSE     , " : vSql = vSql & " " & Left(Trim(EstadEntrega), 2) & ", "                  '//2S0   Est Entrega
                fSql = fSql & " RFECREP   , " : vSql = vSql & "'" & FechaReporte & "', "                                    '//26Z   F.Reporte
                fSql = fSql & " RCODUBI   , " : vSql = vSql & "'" & Left(Trim(UCase(codUbicacion)), 15) & "', "         '//15A   Codigo Ubicacion
                fSql = fSql & " RUBIC     , " : vSql = vSql & "'" & Left(Trim(UCase(Ubicacion)), 60) & "', "            '//60A   Ubicacion
                fSql = fSql & " RTIPORI   , " : vSql = vSql & "'" & Left(Trim(UCase(tipoOrigen)), 15) & "', "           '//15A   Tipo de Origen de Nov
                fSql = fSql & " RTIPNOV   , " : vSql = vSql & "'" & Left(Trim(UCase(tipoNovedad)), 15) & "', "          '//15A   Tipo de Novedad
                fSql = fSql & " RREPORT   , " : vSql = vSql & "'" & Left(Reporte, 500) & "', "                          '//502A  Reporte
                fSql = fSql & " RCRTUSR   , " : vSql = vSql & "'" & Usuario & "', "                                     '//10A   Usuario
                fSql = fSql & " RAPP      ) " : vSql = vSql & "'" & "WEBSERVICE" & "' ) "                               '//15A   App
                DB.ExecuteSQL(fSql & vSql)
                fSql = "UPDATE RHCC SET HREPORT = '" & Left(Trim(UCase(Ubicacion)) & "<BR>" & Reporte, 500) & "', "
                fSql = fSql & " HFECREP = '" & FechaReporte & "'"
                fSql = fSql & " WHERE HCONSO = '" & Conso & "'"
                DB.ExecuteSQL(fSql)
                resultado = numReporte & ": REPORTE GUARDADO"
                If My.Settings.REPORT_MAIL.ToUpper = "SI" Then
                    resultado = EnviaCorreo(Conso & " " & Placa, numReporte, rs("HTRAUSR").Value)
                    If resultado = "OK" Then
                        resultado = numReporte & ": REPORTE GUARDADO"
                    End If
                End If
            Else
                resultado = "-002: CONSOLIDADO NO EXISTE"
            End If
        Else
            resultado = "-001: USUARIO/PASSWORD NO VALIDO"
        End If
        rs.Close()
        DB.Close()

        Return resultado

    End Function

    Function EnviaCorreo(Placa As String, numReporte As String, usrTransp As String) As String
        Dim eMail As New clsMHT
        Dim resultado As String = ""
        Dim rs As New ADODB.Recordset
        Dim dest As String = usrTransp

        fSql = " SELECT RCRTUSR  "
        fSql = fSql & " FROM RRCC  "
        fSql = fSql & " WHERE RCONSO = '437255' AND RREPORT = 'Consolidado.' "
        rs = DB.ExecuteSQL(fSql)
        If DB.OpenRS(rs, fSql) Then
            dest &= "," & rs("RCRTUSR").Value
        End If
        rs.Close()
        rs = Nothing


        With eMail
            .Inicializa()
            .DB = DB
            .IdMsg = "Reporte Transito"
            .Asunto = Placa
            .De = "ReportT"
            .Para = dest
            .url = DB.getLXParam("SERVERRAIZ") & "tm/avisos/ReportT.asp?RNUM=" & numReporte
            If My.Settings.CFGMAIL_LOCAL.ToUpper = "SI" Then
                .local = "CCNOT1"
            End If
            resultado = .enviaMail()
        End With

        Return resultado
    End Function


End Class
