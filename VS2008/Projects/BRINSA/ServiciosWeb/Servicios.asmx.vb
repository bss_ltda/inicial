﻿Imports System
Imports System.Globalization
Imports System.IO
Imports System.Collections
Imports System.Xml.Serialization
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Xml
Imports System.Net

' Para permitir que se llame a este servicio Web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://br.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Servicios
    Inherits System.Web.Services.WebService
    Dim DB As clsConexion

    Structure SMSInfo
        Dim ModemName As String
        Dim Sender As String
        Dim PhoneNumber As String
        Dim SCTS As String
        Dim Data As String
        Dim ReceivedSMSC As String
    End Structure

    <WebMethod()> _
    Public Function SMSFinDeEntrega(ByVal xmlSMS As SMSInfo) As String
        Dim resultado, pHora, pPlanilla As String
        Dim aReporte() As String
        Dim i As Integer
        Dim Plani As New clsFinEntrega
        Dim Alerta As New clsCorreoMHT


        AbreConexion("FinDeEntrega")
        Plani.DB = DB

        resultado = ""
        With xmlSMS
            pHora = Mid(.SCTS, 1, 4) & "-"
            pHora += Mid(.SCTS, 5, 2) & "-"
            pHora += Mid(.SCTS, 7, 2) & " "
            pHora += Mid(.SCTS, 9, 2) & ":"
            pHora += Mid(.SCTS, 11, 2) & ":"
            pHora += Mid(.SCTS, 13, 2)
            .SCTS = pHora
            pPlanilla = Plani.ValidaPlanilla(.Data)
            aReporte = Split(pPlanilla, " ")
            For i = 0 To UBound(aReporte)
                Plani.Planilla = aReporte(i)
                Plani.pSender = .Sender
                resultado = Plani.FinEntrega()
                If resultado.Substring(1, 2) <> "00" Then
                    With Alerta
                        .Inicializa()
                        .DB = DB
                        .Referencia = "SMS PLANILLAS"
                        .Remitente = "Reporte Fin Entrega"
                        .Asunto = resultado
                        .Modulo = "FIN DE ENTREGA"
                        .Destinatario = .AvisarA()
                        .AddLine("ModemName: " & xmlSMS.ModemName)
                        .AddLine("Sender: " & xmlSMS.Sender)
                        .AddLine("PhoneNumber: " & xmlSMS.PhoneNumber)
                        .AddLine("SCTS: " & xmlSMS.SCTS)
                        .AddLine("Data: " & xmlSMS.Data)
                        .AddLine("ReceivedSMSC: " & xmlSMS.ReceivedSMSC)
                        .EnviaCorreo()
                    End With
                End If
            Next
        End With
        DB.Close()

        Return resultado

    End Function

    <WebMethod()> _
    Public Function FinDeEntrega(Celular As String, Planilla As String) As String
        Dim Resultado As String = ""
        Dim Plani As New clsFinEntrega

        If IsDBNull(Planilla) Or IsDBNull(Celular) Then
            Resultado = "Datos no validos"
        ElseIf Not IsNumeric(Planilla) Or Not IsNumeric(Celular) Then
            Resultado = "Datos no validos"
        ElseIf Len(Planilla) <> 7 Or Len(Celular) <> 10 Then
            Resultado = "Datos no validos"
        ElseIf AbreConexion("FinDeEntrega") Then
            Plani.Planilla = Planilla
            Plani.pSender = Celular
            Plani.DB = DB
            Resultado = Plani.FinEntrega()
            DB.Close()
        End If

        Return Resultado

    End Function

    <WebMethod()> _
    Public Function EntregasConso(Consolidado As String) As String
        Dim Resultado As String = ""
        Dim Conso As New clsEntregasConso

        If IsDBNull(Consolidado) Then
            Resultado = "ERROR"
        ElseIf Consolidado.Trim() = "" Then
            Resultado = "ERROR"
        ElseIf AbreConexion("EntregasConso") Then
            Conso.Consolidado = Consolidado
            Conso.DB = DB
            Conso.ActualizaRECC()
            Resultado = "OK"
            DB.Close()
        End If

        Return Resultado

    End Function

    <WebMethod()> _
    Public Function EnvioMHT(IdMsg As String, Asunto As String, De As String, Para As String, CC As String, url As String, c As String) As String
        Dim Resultado As String = ""
        If AbreConexion("EnvioMHT") Then
            If c = DB.getLXParam("WEBSERVICEPWD") Then
                Dim eMail As New clsMHT
                With eMail
                    .DB = DB
                    .IdMsg = IdMsg
                    .Asunto = Asunto
                    .De = De
                    .Para = Para
                    .CC = CC
                    .url = url
                    Resultado = .enviaMail()
                End With
            End If
        End If
        Return Resultado

    End Function

    <WebMethod()> _
    Public Function ReporteT(Usuario As String, Pass As String, Conso As String, Manifiesto As String, _
                               Placa As String, EstadEntrega As Integer, FechaReporte As String, _
                               codUbicacion As String, Ubicacion As String, _
                               tipoOrigen As String, tipoNovedad As String, Reporte As String) As String
        Dim pattern As String = "yyyy-MM-dd-hh.mm.ss"
        Dim Resultado As String = ""
        Dim dtReporte As Date
        Dim fecReporte As String

        If IsDate(FechaReporte) Then
            dtReporte = CDate(FechaReporte)
            fecReporte = dtReporte.ToString("yyyy-MM-dd-HH.mm.ss") & ".000000"
        Else
            Return "-003: FECHA INCORRECTA"
        End If

        If AbreConexion("ReporteT") Then
            Dim repTransito As New clsReportTransito
            With repTransito
                .DB = DB
                .Usuario = Usuario.ToUpper
                .Pass = Pass.ToUpper
                .Conso = Conso
                .Manifiesto = Manifiesto
                .Placa = Placa
                .FechaReporte = fecReporte
                .codUbicacion = codUbicacion
                .Ubicacion = Ubicacion
                .tipoOrigen = tipoOrigen
                .tipoNovedad = tipoNovedad
                .Reporte = Reporte
                .EstadEntrega = EstadEntrega
                Resultado = .ReportaNovedad()
            End With
        End If
        Return Resultado

    End Function

    Function AbreConexion(ModuloActual As String) As Boolean

        DB = New clsConexion
        DB.gsDatasource = My.Settings.AS400
        DB.gsLib = My.Settings.AMBIENTE
        DB.ModuloActual = ModuloActual
        Return DB.Conexion()

    End Function


End Class