﻿Imports System
Imports System.Globalization
Imports System.IO
Imports System.Collections
Imports System.Xml.Serialization
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Xml
Imports System.Net


' Para permitir que se llame a este servicio Web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class ReporteTransito
    Inherits System.Web.Services.WebService
    Dim DB As clsConexion
    Dim errProceso As String

    <WebMethod()> _
    Public Function ReporteT(Usuario As String, Pass As String, Conso As String, Manifiesto As String, _
                               Placa As String, EstadEntrega As Integer, FechaReporte As String, _
                               codUbicacion As String, Ubicacion As String, _
                               tipoOrigen As String, tipoNovedad As String, Reporte As String) As String


        Dim Resultado As String = ""
        Dim dtReporte As Date
        Dim fecReporte As String

        If IsDate(FechaReporte) Then
            dtReporte = CDate(FechaReporte)
            fecReporte = dtReporte.ToString("yyyy-MM-dd-HH.mm.ss") & ".000000"
        Else
            Return "-003: FECHA INCORRECTA"
        End If

        If AbreConexion("ReporteT") Then
            Dim repTransito As New clsReportTransito
            With repTransito
                .DB = DB
                .Usuario = Usuario.ToUpper
                .Pass = Pass.ToUpper
                .Conso = Conso
                .Manifiesto = Manifiesto
                .Placa = Placa
                .FechaReporte = fecReporte
                .codUbicacion = codUbicacion
                .Ubicacion = Ubicacion
                .tipoOrigen = tipoOrigen
                .tipoNovedad = tipoNovedad
                .Reporte = Reporte
                .EstadEntrega = EstadEntrega
                Resultado = .ReportaNovedad()
            End With
        End If
        Return Resultado

    End Function



    <WebMethod()> _
    Public Function DespachoDS(Consolidado As String) As String
        System.Text.Encoding.GetEncoding("UTF-8")

        If AbreConexion("DespachoDS") Then
            Dim rsRHCC As New ADODB.Recordset
            Dim rs As New ADODB.Recordset
            fSql = "SELECT CHAR( VARCHAR_FORMAT( HSALE,'YYYY-MMDD HH24:MI:SS'), 18 ) AS SALIDA  "
            fSql &= ", HBOD, HPROVE, HMANIF, HCIUDAD, HDEPTO "
            fSql &= ", HPLACA, HCEDUL, HCELU, HCHOFE"
            fSql &= " FROM RHCC H WHERE HCONSO = '" & Consolidado & "'"
            If DB.OpenRS(rsRHCC, fSql) Then
                Dim DestinoSeguro As New net.destinoseguro.www.WSdestino
                Dim DespachoTransp As New ServiceReferenceDestinoSeguro.setDespachoTranspRequest
                DespachoTransp.usr = "WSBRINSA"
                DespachoTransp.pwd = "LvL3qP2"
                'salida
                DespachoTransp.salida = Replace(rsRHCC("SALIDA").Value, " ", "T")
                'nittra
                fSql = "SELECT VMFSCD FROM AVM WHERE VENDOR = " & rsRHCC("HPROVE").Value
                If DB.OpenRS(rs, fSql) Then
                    Dim aNit() As String = Split(rs(0).Value, "-")
                    DespachoTransp.nittra = CampoNum(aNit(0))
                Else
                    errProceso &= "NIT Transportadora|"
                End If
                rs.Close()

                'manifi
                DespachoTransp.manifi = rsRHCC("HMANIF").Value
                'corigen
                fSql = "SELECT WMSLOC FROM IWM INNER JOIN RFDANE ON WMSLOC = CPOBLADO "
                fSql &= " WHERE LWHS = '" & rsRHCC("HBOD").Value & "'"
                If DB.OpenRS(rs, fSql) Then
                    DespachoTransp.corigen = rs(0).Value
                Else
                    errProceso &= "Codigo dane ciudad origen|"
                End If
                rs.Close()

                'cdestino
                fSql = " SELECT CPOBLADO FROM RFDANE "
                fSql &= " WHERE CLXDPTO = '" & rsRHCC("HDEPTO").Value & "' "
                fSql &= " AND CLXCIUD = '" & rsRHCC("HCIUDAD").Value & "' "
                fSql &= " AND CTIPO = 'CM' "
                If DB.OpenRS(rs, fSql) Then
                    DespachoTransp.cdestino = rs(0).Value
                Else
                    errProceso &= "Codigo dane ciudad destino|"
                End If
                rs.Close()

                DespachoTransp.placa = rsRHCC("HPLACA").Value
                DespachoTransp.cedula = rsRHCC("HCEDUL").Value
                DespachoTransp.nombres = rsRHCC("HCHOFE").Value
                DespachoTransp.cel = Replace(rsRHCC("HCELU").Value, " ", "")
                DespachoTransp.nitgen = "800221789"
                DespachoTransp.nomgen = "BRINSA S.A."
                DespachoTransp.sucursal = "1"
                DespachoTransp.it1 = Consolidado

                If errProceso <> "" Then
                    'avisa()
                    Return errProceso
                End If

                With (DespachoTransp)
                    Try
                        errProceso = DestinoSeguro.setDespachoTransp( _
                                        .usr, _
                                        .pwd, _
                                        .salida, _
                                        .nittra, _
                                        .manifi, _
                                        .corigen, _
                                        .cdestino, _
                                        .via, _
                                        .rut, _
                                        .placa, _
                                        .cmarca, _
                                        .ccarroceria, _
                                        .ccolor, _
                                        .modelo, _
                                        .cedula, _
                                        .nombres, _
                                        .apellidos, _
                                        .dir, _
                                        .tels, _
                                        .cel, _
                                        .nitgen, _
                                        .nomgen, _
                                        .sucursal, _
                                        .obs, _
                                        .it1, _
                                        .it2, _
                                        .it3, _
                                        .itemadicional, _
                                        .usrinterno)
                    Catch ex As Exception
                        Debug.Print(ex.Message)
                    End Try

                End With

            End If
        End If
        Return errProceso
    End Function

    Function AbreConexion(ModuloActual As String) As Boolean

        DB = New clsConexion
        DB.gsDatasource = My.Settings.AS400
        DB.gsLib = My.Settings.AMBIENTE
        DB.ModuloActual = ModuloActual
        Return DB.Conexion()

    End Function

    'CampoNum()
    Private Function CampoNum(Dato) As String
        Dim i As Integer
        Dim Ascii As Integer
        Dim resultado As String = ""
        For i = 1 To Len(Dato)
            Ascii = Asc(Mid(Dato, i, 1))
            If Ascii >= Asc("0") And Ascii <= Asc("9") Then
                resultado &= Chr(Ascii)
            End If
        Next
        Return resultado

    End Function

End Class