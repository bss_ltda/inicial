﻿Option Strict Off
Option Explicit On

Imports System.Globalization

Module BSSCommon
    Dim appSettings As NameValueCollection = _
        ConfigurationManager.AppSettings
    Public Const tNum As Integer = 1
    Public Const tStr As Integer = 2
    Public Const t_iF As Integer = 3
    Public Const t_iV As Integer = 4
    Public Const tFil As Integer = 5

    Public sModulo As String
    Public Sql As New mk_Sql
    Public gsConnstring As String
    Public sLIB As String
    Public sVersion As String
    Public Descripcion_Error As String
    Public Error_Tabla As String
    Public fSql As String
    Public vSql As String

    Public pCultureInfo As New CultureInfo("th-TH")
    Public LibParamCollection As New Dictionary(Of String, String)
    Public Sesiones As Integer

    Function DateCharDec(ByVal Fecha As Date) As String
        Return Fecha.ToString("yyyyMMdd")
    End Function

    Function TimeCharDec(ByVal Fecha As Date) As String
        Return Fecha.ToString("HHnnss")
    End Function

    Function dtAS400(ByVal fecha As Date) As String
        Return fecha.ToString("yyyy-MM-dd")
    End Function

    Function ParmItem(ByRef arreglo() As String, ByVal tag As String, ByVal valor As String) As String
        Dim i As Integer = UBound(arreglo)
        ReDim Preserve arreglo(i + 1)
        arreglo(i) = tag & ": " & valor & vbCrLf
        Return tag & ": " & valor & vbCrLf
    End Function

   
    Function Valor(ByVal txt As String) As Double
        If pCultureInfo.NumberFormat.NumberDecimalSeparator = "." Then
            Valor = CDbl(Replace(txt, ",", "."))
        Else
            Valor = CDbl(Replace(txt, ".", ","))
        End If
    End Function

    Sub Delay(ByVal dblSecs As Double)
        Dim CurrentTime As DateTime = Now
        Do While DateDiff("s", Now, CurrentTime) < dblSecs
        Loop
    End Sub

    Function Ceros(ByVal val As Object, ByVal C As Integer)
        Dim sCeros As String = New String("0", C)

        If CInt(C) > Len(Trim(val)) Then
            Ceros = Right(sCeros & Trim(val), C)
        Else
            Ceros = val
        End If

    End Function

    Function Espacios(ByVal val As Object, ByVal C As Integer)
        Dim sCeros As String = New String(" ", C)

        If CInt(C) > Len(Trim(val)) Then
            Espacios = Left(Trim(val) & sCeros, C)
        Else
            Espacios = val
        End If

    End Function

    'timeStampWin()
    Function timeStampWin() As String

        timeStampWin = "" & year(Now) & "-" & Month(Now) & "-" & Day(Now) & "-" & Hour(Now) & "." & Minute(Now) & "." & Second(Now) & ".000000"

    End Function


End Module
