﻿Public Class bssTarea
    Public Categoria As String
    Public Subcategoria As String
    Public Descripcion As String
    Public Clave As String
    Public PalabraClave As String
    Public Directo As Integer        '1. No lo envia al servicio de windows de tarea
    Public Interactivo As Integer    '1. Ejecuta y espera a que termine
    Public Rutina As String
    Public Comando As String
    Public Retenida As Integer
    Public Programar As String       'Ejecuta la tarea a la hora programada
    Public DB As clsConexion

    Private Parametros As String


    Function EnviaTarea()
        Dim fSql, vSql, numTarea, tipo As String
        Dim rs As New ADODB.Recordset
        Dim util As New bssUtil

        numTarea = DB.CalcConsec("RFTASKBATCH", "9999999")
        If Programar = "" Then
            Programar = Format(Now(), "yyyy-MM-dd-HH.mm.ss.000000")
        Else
            Programar = util.timeStampTxt(Programar)
        End If

        If Rutina Then
            Comando = "E:\Despachos_DB2\rutina.bat"
            tipo = "Rutina"
        Else
            tipo = "Shell"
        End If

        fSql = " INSERT INTO RFTASK( "
        vSql = " VALUES ( "
        fSql = fSql & " TCATEG    , " : vSql = vSql & "'" & Categoria & "', "                '//20A   Categoria
        fSql = fSql & " TSUBCAT   , " : vSql = vSql & "'" & Subcategoria & "', "             '//20A   Categoria
        fSql = fSql & " TDESC     , " : vSql = vSql & "'" & Descripcion & "', "              '//100A  Descripcion
        fSql = fSql & " TNUMTAREA , " : vSql = vSql & " " & numTarea & ", "                  '//7P0   Numero de Tarea
        fSql = fSql & " TFPROXIMA , " : vSql = vSql & "'" & Programar & "', "                '//26Z   Proxima Ejecucion
        fSql = fSql & " TTIPO     , " : vSql = vSql & "'" & tipo & "', "                            '//22A   Tipo
        fSql = fSql & " THLD      , " : vSql = vSql & " " & Retenida & ", "                  '//1P0   Retenida
        fSql = fSql & " TKEY      , " : vSql = vSql & "'" & Clave & "', "
        fSql = fSql & " TKEYWORD  , " : vSql = vSql & "'" & PalabraClave & "', "
        fSql = fSql & " STS1      , " : vSql = vSql & " " & Directo & ", "                   '//1S0   Sts 1
        fSql = fSql & " TPRM      , " : vSql = vSql & "'" & Parametros & "', "
        'fSql = fSql & " TCRTUSR   , " : vSql = vSql & "'" & Session("UserLogin") & "', "      '//10A   Usuario
        fSql = fSql & " TPGM      ) " : vSql = vSql & "'" & Comando & "') "                  '//502A  Programa
        DB.ExecuteSQL(fSql & vSql)

        If Directo = 1 Then
            fSql = "SELECT TNUMREG FROM RFTASK WHERE TNUMTAREA = " & numTarea
            rs = DB.ExecuteSQL(fSql)
            Dim sCmd As String
            sCmd = """" & Comando & """ " & rs("TNUMREG").Value
            Shell(sCmd, AppWinStyle.Hide, True)
            rs.Close()
            rs = Nothing
        End If

        EnviaTarea = numTarea

    End Function

    Sub setParam(nombre, valor)

        Parametros = Parametros & "|" & Trim(nombre) & "=" & Trim(valor)

    End Sub

    Sub Inicializa()

        Categoria = ""
        Subcategoria = ""
        Descripcion = ""
        Clave = ""
        PalabraClave = ""
        Comando = ""
        Programar = ""

        Directo = 0
        Retenida = 0
        Interactivo = True
        Rutina = False

    End Sub
End Class
