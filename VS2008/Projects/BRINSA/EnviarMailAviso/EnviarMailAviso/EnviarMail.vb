﻿Imports BSSClassLibrary
Module EnviarMail
    Dim db As New bssConexion
    Dim sql As New bssSQL
    Dim u As New bssUtil

    Sub Main()
        db.abrirConexion(My.Settings.AMBIENTE, "AS400")
        EnviarMail()
        '       EnviarMail2()
    End Sub
    Function EnviarMail()
        Dim fSql As String
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim mailman As New Chilkat.MailMan()
        Dim mht As New Chilkat.Mht()
        Dim sep As String = ""
        Dim i As Integer
        Dim Usuarios As String = ""

        fSql = "SELECT * FROM RMAILX WHERE LENVIADO = 0"
        rs = db.ExecuteSQL(fSql)
      
        While Not rs.EOF
            Dim url As String = My.Settings.SERVERRAIZ & "/tm/common/aviso.asp?LID=" & rs("LID").Value.ToString
            Dim Asunto As String = rs("LASUNTO").Value.ToString
            Dim success As Boolean = True

            success = mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
            If (success <> True) Then
                rs.Close()
                Return False
            End If
            success = mht.UnlockComponent("SMANRIMHT_dEnsKQ0g3Rue")

            If (success <> True) Then
                rs.Close()
                Return False
            End If

            mht.UseCids = True
            mailman.SmtpHost = My.Settings.MAILHOST()
            mailman.SmtpUsername = My.Settings.MAILUSER
            mailman.SmtpPassword = My.Settings.MAILPASS

            Dim email As New Chilkat.Email()
            Dim aDestinos() As String
            email = mht.GetEmail(url)
            email.Subject = Asunto
            Dim De As String
            De = rs.Fields("LENVIA").Value
            If De <> "" Then
                email.FromName = De
                If Not De.Contains("@") Then
                    email.FromAddress = "NoResponder@Brinsa.com.co"
                End If
            Else
                email.FromName = "Alertas BRINSA"
                email.FromAddress = "NoResponder@Brinsa.com.co"
            End If
            aDestinos = Split(rs.Fields("LPARA").Value, ",")
            For i = 0 To UBound(aDestinos)
                fSql = " SELECT UNOM, UEMAIL FROM RCAU WHERE UUSR = '" & Trim(aDestinos(i)) & "' "
                rs1 = db.ExecuteSQL(fSql)
                If Not rs1.EOF Then
                    If rs("UNOM").Value <> "" And rs("UEMAIL").Value <> "" Then
                        email.AddTo(rs("UNOM").Value, rs("UEMAIL").Value)
                    ElseIf rs("UEMAIL").Value <> "" Then
                        email.AddTo("", rs("UEMAIL").Value)
                    End If
                End If
                rs1.Close()
            Next

            If Trim(rs.Fields("LCOPIA").Value) <> "" Then
                aDestinos = Split(rs.Fields("LCOPIA").Value, vbCrLf)
                For i = 0 To UBound(aDestinos)
                    fSql = " SELECT UNOM, UEMAIL FROM RCAU WHERE UUSR = '" & Trim(aDestinos(i)) & "' "
                    rs1 = db.ExecuteSQL(fSql)
                    If Not rs1.EOF Then
                        If rs("UNOM").Value <> "" And rs("UEMAIL").Value <> "" Then
                            email.AddCC(rs("UNOM").Value, rs("UEMAIL").Value)
                        ElseIf rs("UEMAIL").Value <> "" Then
                            email.AddCC("", rs("UEMAIL").Value)
                        End If
                    End If
                    rs1.Close()
                Next
            End If
            success = mailman.SendEmail(email)
            If success Then
                fSql = "UPDATE RMAILX SET LENVIADO=1 WHERE LID=" & rs("LID").Value.ToString
                db.ExecuteSQL(fSql)
            Else
            End If
            rs.MoveNext()
        End While
        rs.Close()
        Return True
    End Function
    Function EnviarMail2()
        Dim fSql As String
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim mailman As New Chilkat.MailMan()
        Dim mht As New Chilkat.Mht()
        Dim sep As String = ""
        Dim i As Integer
        Dim Usuarios As String = ""

        fSql = "SELECT * FROM RFLOG WHERE ALERT = 1"
        rs = db.ExecuteSQL(fSql)

        While Not rs.EOF
            Dim url As String = My.Settings.SERVERRAIZ & "/tm/common/avisoLog.asp?ID=" & rs("ID").Value.ToString
            Dim Asunto As String = rs("PROGRAMA").Value.ToString
            Dim success As Boolean = True

            success = mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
            If (success <> True) Then
                rs.Close()
                Return False
            End If
            success = mht.UnlockComponent("SMANRIMHT_dEnsKQ0g3Rue")

            If (success <> True) Then
                rs.Close()
                Return False
            End If

            mht.UseCids = True
            mailman.SmtpHost = My.Settings.MAILHOST()
            mailman.SmtpUsername = My.Settings.MAILUSER
            mailman.SmtpPassword = My.Settings.MAILPASS

            Dim email As New Chilkat.Email()
            email = mht.GetEmail(url)
            email.Subject = Asunto
            email.From = "EnviaLog@BssLtda.com"
            email.AddTo("Fredy Manrique", "Fredy.Manrique@BssLtda.com")
            email.AddTo("Camila Arciniegas", "Camila.Arciniegas@BssLtda.com")
            success = mailman.SendEmail(email)
            If success Then
                fSql = "UPDATE RFLOG SET ALERT = 2 WHERE ID=" & rs("ID").Value.ToString
                db.ExecuteSQL(fSql)
            Else
            End If
            rs.MoveNext()
        End While
        rs.Close()

        Return True
    End Function
End Module
