﻿Public Class bssSQL

    Dim fSql As String = ""
    Dim vSql As String = ""
    Dim msgErr As String = ""
    Dim esInsert As Boolean = False
    Dim esUpdate As Boolean = False
    Dim esFin As Boolean = False
    Dim bSalto As Boolean = False

    Sub mkSQL(txtSql As String)

        If txtSql.ToUpper.Contains("INSERT INTO") Then
            esInsert = True
            fSql = txtSql & IIf(bSalto, vbCrLf, "")
        ElseIf txtSql.ToUpper.Contains("VALUES") Then
            vSql = txtSql & IIf(bSalto, vbCrLf, "")
        ElseIf txtSql.ToUpper.Contains("UPDATE") Then
            fSql = txtSql & IIf(bSalto, vbCrLf, "")
            vSql = ""
            esUpdate = True
            esInsert = False
        ElseIf esFin Then
            vSql = vSql & " " & txtSql & IIf(bSalto, vbCrLf, "")
        ElseIf esInsert Then
            vSql = txtSql
        Else
            fSql = txtSql
        End If
    End Sub

    Sub mkSql(sep As String, ByVal Campo As String, ByVal Valor As Object, Optional ByVal fin As Integer = 0)

        sep = Trim(sep)
        If esInsert Then
            fSql = fSql & Campo
            Select Case fin
                Case 0
                    fSql = fSql & " , "
                Case 10, 11, -1, 1
                    fSql = fSql & " ) "
            End Select
            If IsDBNull(Valor) Then
                Valor = IIf(sep = "'", "", "0")
            End If
            vSql = vSql & sep & CStr(Valor) & sep
            Select Case fin
                Case 10
                    vSql = vSql & "  "
                Case 11, -1, 1
                    vSql = vSql & " )"
                Case 0
                    vSql = vSql & " , "
            End Select
        Else
            If IsDBNull(Valor) Then
                Valor = IIf(sep = "", "", "0")
            End If
            fSql = fSql & Campo & " = " & sep & CStr(Valor) & sep & IIf(fin = 0, ", ", "")
        End If
        esFin = fin <> 0
        If bSalto Then
            fSql &= vbCrLf
            If vSql <> "" Then
                vSql &= vbCrLf
            End If
        End If

    End Sub

    Function Sql() As String
        Return fSql & vSql
    End Function

    Function v_fSql() As String
        Return fSql
    End Function

    Function v_Sql() As String
        Return vSql
    End Function

    Sub salto(v As Boolean)
        bSalto = v
    End Sub

    Function DB2_DATEDEC()
        Return " ( YEAR( NOW() ) * 10000 + MONTH( NOW()  ) * 100 + DAY( NOW() ) )"
    End Function
    Function DB2_TIMEDEC()
        Return " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    End Function
    Function DB2_TIMEDEC0()
        Return " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    End Function
    Function DB2_TIMEDEC1()
        Return " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 ) "
    End Function

End Class
