﻿Public Class bssConexion
    Private local As String
    Private gsAS400 As String = "192.168.10.1"
    Private gsLib As String
    Private sUser As String
    Private sUserApl As String
    Private sUserPass As String
    Private conn As New ADODB.Connection
    Private bBatchApp As Boolean = False
    Private nomApp As String = "VB.NET." & My.Application.Info.ProductName & "." & _
                               My.Application.Info.Version.Major & "." & _
                               My.Application.Info.Version.Minor & "." & _
                               My.Application.Info.Version.Revision
    Public Sentencia As String
    Public bLog As Boolean = False
    Public iNumLog As Integer
    Dim msgErr As String
    Dim gsConnString As String
    Dim rAfectados As Integer
    Dim Descripcion_Error As String

    Sub abrirConexion(qLib As String, ByVal qServer As String)
        Dim DB As New ADODB.Connection
        Dim rs As New ADODB.Recordset
        gsLib = qLib
        If qServer = "AS400" Then qServer = "APLUSR"
        DB.Open("Provider=IBMDA400.DataSource;Data Source=" & gsAS400 & ";User ID=APLLX;Password=LXAPL;Persist Security Info=True;" & _
                "Application Name=" & My.Application.Info.ProductName & ";Default Collection=" & gsLib & ";Force Translate=0;")
        rs.Open("SELECT * FROM ZCCL01 WHERE CCTABL = 'LXLONG' AND UPPER(CCSDSC) = '" & qServer.ToUpper() & "' ORDER BY CCUDC1", DB)
        Do While Not rs.EOF
            Select Case rs("CCUDC1").Value
                Case 1
                    gsConnString &= "Provider=" & rs("CCNOT1").Value & ";Data Source=" & rs("CCDESC").Value & ";"
                Case 2
                    gsConnString &= "User ID=" & rs("CCDESC").Value & ";"
                    sUser = rs("CCDESC").Value
                Case 3
                    gsConnString &= "Password=" & rs("CCDESC").Value & ";"
                Case 4
                    Select Case qServer.ToUpper()
                        Case "APLUSR", "AS400"
                            gsConnString &= "Default Collection=" & rs("CCDESC").Value & ";Force Translate=0;Convert Date Time To Char=FALSE"
                        Case "MP2"
                            gsConnString &= "Initial Catalog=" & rs("CCDESC").Value
                        Case "WMS"
                            gsConnString &= "Initial Catalog=" & rs("CCDESC").Value
                    End Select
            End Select
            rs.MoveNext()
        Loop
        rs.Close()
        DB.Close()
        Try
            conn.Open(gsConnString)
        Catch ex As Exception
            msgErr = Err.Description

        End Try

        'Provider=SQLOLEDB.1;User ID=mp2;Initial Catalog=t_mp2;Data Source=192.168.10.2;Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;Workstation ID=FM-WIN7U;Use Encryption for Data=False;Tag with column collation when possible=False;
        'Provider=IBMDA400.DataSource.1;Password=DIC2013;Persist Security Info=True;User ID=DESLXSSA;Data Source=192.168.10.1;Force Translate=0;Default Collection=DESLX834F;Convert Date Time To Char=FALSE

    End Sub

    Public Sub Close()
        conn.Close()
    End Sub

    Public Sub setTipoApp(v As Boolean)
        bBatchApp = v
    End Sub

    Public Function DspErr() As String
        Return "" 'Error_Tabla & " " & Sentencia & " " & vbCrLf & Descripcion_Error
    End Function

    Public Function ExecuteSQL(ByVal sql As String) As ADODB.Recordset
        Try
            Sentencia = sql
            Return Conn.Execute(sql, rAfectados)
        Catch
            WrtSqlError(sql, Err.Description)
            Return Nothing
        End Try

    End Function

    Public Function CalcConsec(ByRef sID As String, ByRef TopeMax As String) As String
        Dim rs As New ADODB.Recordset
        Dim locID As String
        Dim sql As String = ""
        Dim sConsec As String = ""
        Dim tMax As Double
        Dim sFmt As String
        Dim nDig As Integer

        Try
            nDig = Len(TopeMax)
            sFmt = New String("0", nDig)

            sql = "SELECT CCDESC FROM ZCCL01 WHERE CCTABL = 'SECUENCE' AND CCCODE='" & sID & "'"
            tMax = Val(TopeMax)

            With rs
                .CursorLocation = ADODB.CursorLocationEnum.adUseServer
                .Open(sql, conn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)
                If Not (.BOF And .EOF) Then
                    locID = Val(.Fields("CCDESC").Value) + 1
                    If locID > tMax Then
                        locID = 1
                    End If
                    sConsec = Right(sFmt & locID, nDig)
                    .Fields("CCDESC").Value = sConsec
                    .Update()
                    .Close()
                Else
                    locID = 1
                    .Close()
                    sConsec = Right(sFmt & locID, nDig)
                    sql = " INSERT INTO ZCC (CCID, CCTABL, CCCODE, CCDESC ) " & " VALUES( 'CC', 'SECUENCE', '" & sID & "', '" & sConsec & "' )"
                    conn.Execute(sql)
                End If
            End With
        Catch ex As Exception
            WrtSqlError(sql, Err.Description)
        End Try
        Return sConsec

    End Function

    Public Function Call_PGM(ByVal Pgm As String, ByVal pais As String) As Integer
        Dim r As Integer = 0
        'Dim Conn As 
        Try
            'Conn.Execute(" {{ " & Pgm & " }} ", r)
            Return 0
        Catch
            msgErr = Err.Description
            Return -99
        End Try


    End Function
    Public Function ShowError() As String
        Return msgErr
    End Function

    Function lastSQL() As String
        Return Sentencia
    End Function

    Function registros() As Integer
        Return rAfectados
    End Function

    Public Sub bitacora(Programa As String, ByVal Log As String)
        Dim Identificacion As String
        Dim Linea As String
        Dim sql As New bssSQL

        Identificacion = sUser
        Do While True
            Linea = Replace(Left(Log, 14900), "'", "''")
            sql.mkSQL(" INSERT INTO RFLOG( ")
            sql.mkSQL(" VALUES ( ")
            sql.mkSQL("'", " USUARIO   ", Identificacion, 0)             '//502A
            sql.mkSQL("'", " PROGRAMA  ", nomApp & "." & Programa, 0)    '//502A
            sql.mkSQL("'", " EVENTO    ", Linea, 1)                      '//15002A
            conn.Execute(sql.Sql)
            Log = Mid(Log, 14901)
            If Log = "" Then
                Exit Do
            End If
        Loop

    End Sub

    Public Function checkVer() As Boolean
        Dim rs As New ADODB.Recordset
        Dim lVer, locVer As String
        Dim msg As String
        Dim NomApp As String
        Dim Result As Boolean = False

        
        lVer = "No hay registro"

        locVer = "(" & My.Application.Info.Version.Major & "." & _
         My.Application.Info.Version.Minor & "." & _
         My.Application.Info.Version.Revision & ")"

        rs.Open("SELECT CCSDSC, CCUDC1, CCNOT2, CCDESC  FROM ZCCL01 WHERE CCTABL='RFVBVER' AND CCCODE = '" & My.Application.Info.ProductName & "'", conn)
        If Not rs.EOF Then
            lVer = rs.Fields("CCSDSC").Value
            NomApp = rs.Fields("CCDESC").Value
            If InStr(lVer, "*NOCHK") > 0 Then
                Result = True
            ElseIf InStr(lVer, locVer) > 0 Then
                Result = True
            End If
            If Result And rs.Fields("CCUDC1").Value = 0 Then
                msg = "La aplicacion " & My.Application.Info.ProductName & " no se puede usar en este momento." & vbCr
                msg = msg & "Razon: " & vbCr
                msg = msg & "=========================================" & vbCr
                If rs.Fields("CCNOT2").Value = "" Then
                    msg = msg & "Aplicacion en Mantenimiento." & vbCr
                Else
                    msg = msg & rs.Fields("CCNOT2").Value & vbCr
                End If
                msg = msg & "=========================================" & vbCr
                If Not bBatchApp Then
                    MsgBox(msg, vbExclamation, "Seguimiento a Pedidos")
                Else
                    Console.WriteLine(msg)
                End If
                rs.Close()
                Return False
            End If
            rs.Close()
        End If

        If Not Result And Not bBatchApp Then
            MsgBox("Aplicacion " & My.Application.Info.ProductName & vbCr & _
                       "=========================================" & vbCr & _
                       "Versión incorrecta del programa." & vbCr & vbCr & _
                       "Versión Registrada:" & vbTab & lVer & vbCr & vbCr & _
                       "Versión Actual:" & vbTab & locVer & vbCr & vbCr & _
                       "Comuníquese con Tecnología.")
        Else
            Console.WriteLine("Aplicacion " & My.Application.Info.ProductName & vbCr & _
                       "=========================================" & vbCr & _
                       "Versión incorrecta del programa." & vbCr & vbCr & _
                       "Versión Registrada:" & vbTab & lVer & vbCr & vbCr & _
                       "Versión Actual:" & vbTab & locVer & vbCr & vbCr & _
                       "Comuníquese con Tecnología.")

        End If

        Return Result

    End Function

    Function AhoraAS400() As Date
        Dim rs As New ADODB.Recordset
        Dim f As Date
        rs.Open("SELECT  CURRENT TIMESTAMP FROM SYSIBM.SYSDUMMY1", conn)
        f = rs(0).Value
        rs.Close()
        Return f

    End Function

    Function comandoRemoto(ByVal tabla As String, ByVal parametros As String) As String
        Dim rs As New ADODB.Recordset
        Dim rmtcmd As String
        Dim fSql As String

        'comandoRemoto( "RUTINAS", "INDPROD 20140201 20140215" )
        'RUNRMTCMD CMD( 'E:\Despachos_DB2\Rutinas\Rutinas INDPROD 20140201 20140215') RMTLOCNAME('192.168.10.6' *IP) RMTUSER('APLSSA') RMTPWD('REMOTO01')

        rmtcmd = "RUNRMTCMD CMD({CMD} {P1}')"
        rmtcmd = rmtcmd & " RMTLOCNAME({RMTLOCNAME} *IP)"
        rmtcmd = rmtcmd & " RMTUSER({RMTUSER})"
        rmtcmd = rmtcmd & " RMTPWD({RMTPWD})"

        rmtcmd = Replace(rmtcmd, "{P1}", parametros)

        fSql = "SELECT * FROM ZCC WHERE CCTABL='" & tabla & "'"
        rs.Open(fSql, conn)
        Do While Not rs.EOF
            rmtcmd = Replace(rmtcmd, "{" & UCase(rs("CCCODE").Value) & "}", rs("CCNOT1").Value)
            rs.MoveNext()
        Loop
        rs.Close()

        rmtcmd = Replace(rmtcmd, vbCrLf, "")
        rmtcmd = Replace(rmtcmd, vbCr, "")
        rmtcmd = Replace(rmtcmd, vbLf, "")

        Return rmtcmd

    End Function

    Sub comandoEjecutado(ByVal rutina As String, ByVal msg As String)
        Dim fSql As String

        fSql = " UPDATE ZCC SET CCUDC1 = 0"
        fSql = fSql & ", CCNOT1 = 'Finalizó " & Now.ToString("yyyy-MM-dd HH:mm:ss") & "' "
        fSql = fSql & ", CCNOT2 = '" & Left(msg, 30) & "'  "
        fSql = fSql & " WHERE CCTABL = 'RUTINAS'  AND CCCODE = '" & UCase(rutina) & "' "
        ExecuteSQL(fSql)

    End Sub

    Sub wrtSqlError(sql As String, Descrip As String)
        Dim DB As New ADODB.Connection
        Dim fSql As String
        Dim vSql As String

        Try
            DB.Open("Provider=IBMDA400.DataSource;Data Source=" & gsAS400 & ";User ID=APLLX;Password=LXAPL;Persist Security Info=True;Application Name=DFU001;Default Collection=" & gsLib & ";Force Translate=0;")
            fSql = " INSERT INTO RFLOG( "
            vSql = " VALUES ( "
            fSql = fSql & " USUARIO   , " : vSql = vSql & "'" & sUser & "', "      '//502A
            fSql = fSql & " PROGRAMA  , " : vSql = vSql & "'" & nomApp & "." & "SQLERR" & "', "      '//502A
            fSql = fSql & " EVENTO    , " : vSql = vSql & "'" & Replace(Descrip, "'", "''") & "', "      '//20002A
            fSql = fSql & " TXTSQL    ) " : vSql = vSql & "'" & Replace(sql, "'", "''") & "' ) "      '//5002A
            DB.Execute(fSql & vSql)
            DB.Close()
        Catch ex As Exception

        End Try
        If bBatchApp Then
            Console.WriteLine(Descrip)
            Console.WriteLine(sql)
        Else
            MsgBox(sql & vbCrLf & Descrip, MsgBoxStyle.Critical, gsLib)
        End If
    End Sub

    Sub wrtLogWsCDA(ByVal logId As String, ByVal xml As String, ByVal id_msg As String, ByVal estado As String, ByVal operacion As String, ByVal fuente As String, ByVal usuario As String)
        Dim rs As New ADODB.Recordset
        Dim dato() As Byte
        dato = System.Text.Encoding.UTF8.GetBytes("<?xml version=""1.0""?>" & xml)
        With rs
            .CursorLocation = ADODB.CursorLocationEnum.adUseServer
            .Open("SELECT * FROM RFLOGWSCDA WHERE C_ID = " & logId, conn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)
            If .EOF Then
                .AddNew()
                .Fields("C_ID").Value = logId
                .Fields("C_XML").Value = dato
                .Fields("C_NOUN_ID").Value = operacion
                .Fields("C_MESSAGEID").Value = id_msg
                .Fields("C_SOURCE_NAME").Value = fuente
                .Fields("C_USER").Value = usuario
                .Fields("C_BODID").Value = ""
            Else
                .Fields("C_WAS_PROCESSED").Value = 1
                .Fields("C_REPLY").Value = dato
                If id_msg <> "" Then
                    .Fields("C_MESSAGEID").Value = id_msg
                End If
                .Fields("C_PASS_FAIL").Value = estado
                End If
                .Update()
                .Close()
        End With
        rs = Nothing
    End Sub

End Class
