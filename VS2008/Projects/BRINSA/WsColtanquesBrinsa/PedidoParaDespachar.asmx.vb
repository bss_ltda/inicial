﻿Imports System
Imports System.Globalization
Imports System.IO
Imports System.Collections
Imports System.Xml.Serialization
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Xml
Imports System.Net

' Para permitir que se llame a este servicio Web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class PedidoParaDespachar
    Inherits System.Web.Services.WebService

    Dim Programa As String
    Dim ERRORES As String
    Public Conn As New ADODB.Connection
    'Dim appSettings As NameValueCollection = ConfigurationManager.AppSettings
    Dim Biblioteca As String
    Dim rs As New ADODB.Recordset
    Dim ContadorLineas As Integer


    Structure Cabecera
        Dim Usuario As String
        Dim Password As String
        Dim Pedido As Double
        Dim Fecha As String
    End Structure

    Structure Detalle
        Dim LineaPedido As Integer
        Dim CodigoProducto As String
        Dim DescripcionProducto As String
        Dim Unidades As String
        Dim DescripcionUnidades As String
        Dim Peso As Double
        Dim NumeroTendidos As String
        Dim UnidadesPorTendido As String
        Dim UnidadesPorEstiba As String
        Dim Lote As String
        Dim Cantidad As Double
    End Structure


    Structure CabeceraPedidoDespachar
        Dim Usuario As String
        Dim Password As String
        Dim Pedido As Double
        Dim Factura As String
        Dim Planilla As String
        Dim Dpto As String
        Dim Ciudad As String
        Dim CodigoDane As Integer
        Dim Cliente As String
        Dim NombreCliente As String
        Dim PuntoEnvio As String
        Dim NombrePuntoEnvio As String


        Dim Direccion As String
        Dim PlanilaPDF As String
        Dim FechaCita As String
        Dim Notas As String

    End Structure

    Structure DetallePedidoDespachar

        Dim LineaPedido As Integer
        Dim Producto As String
        Dim RequiereLote As String
        Dim Cantidad As Double
    End Structure

    Structure DetalleAvanceNovedad

        Dim Usuario As String
        Dim Password As String
        Dim Planilla As Double
        Dim Fecha As String
        Dim Avance As String
    End Structure

    <WebMethod()> _
    Public Function Reaprovisionamiento(ByVal Pedido As String) As XmlDocument
        ContadorLineas = 0
        Dim nuevaLinea As Detalle
        Dim Cabeceraa As New Cabecera
        Dim Lineas As New List(Of Detalle)
        Dim documento As XDocument = XDocument.Parse(Pedido)
        'Dim fSql As String
        '      Dim DB1 As New ExecuteSQL
        Dim qx = From xee In documento.Elements("Reaprovisonamiento")
       Select New With { _
                .Usuario = xee.Element("Usuario").Value,
                .Password = xee.Element("Password").Value,
                .Pedido = xee.Element("Pedido").Value,
                .Fecha = xee.Element("Fecha").Value
                  }
        Try
            For Each elements In qx
                Cabeceraa = New Cabecera
                Cabeceraa.Usuario = elements.Usuario
                Cabeceraa.Password = elements.Password
                Cabeceraa.Pedido = elements.Pedido
                Cabeceraa.Fecha = elements.Fecha
            Next

        Catch
            Return DevuelveError("000", "Error en XML" & Err.Description, Err.Number, "Reaprovisionamiento")
        End Try

        Try

            Dim qx2 = From xe In documento.Descendants.Elements("Linea")
        Select New With { _
                          .LineaPedido = xe.Element("LineaPedido").Value,
                          .CodigoProducto = xe.Element("CodigoProducto").Value,
                          .DescripcionProducto = xe.Element("DescripcionProducto").Value,
                          .Unidades = xe.Element("Unidades").Value,
                          .Peso = xe.Element("Peso").Value,
                          .Cantidad = xe.Element("NumeroTendidos").Value,
                          .UnidadesPorTendido = xe.Element("UnidadesPorTendido").Value,
                          .UnidadesPorEstiba = xe.Element("UnidadesPorEstiba").Value,
                          .Lote = xe.Element("Lote").Value,
                          .NumeroTendidos = xe.Element("NumeroTendidos").Value,
            .DescripcionUnidades = xe.Element("DescripcionUnidades").Value
                            }
            For Each elementos In qx2
                ContadorLineas = ContadorLineas + 1
                nuevaLinea = New Detalle
                nuevaLinea.Cantidad = elementos.Cantidad
                If nuevaLinea.Cantidad <= 0 Then
                    Return DevuelveError("000", "Cantidad no puede ser menor o igual que 0", 106, "Reaprovisionamiento")
                End If

                nuevaLinea.LineaPedido = elementos.LineaPedido
                nuevaLinea.CodigoProducto = elementos.CodigoProducto
                nuevaLinea.DescripcionProducto = elementos.DescripcionProducto
                nuevaLinea.Unidades = elementos.Unidades
                nuevaLinea.Peso = elementos.Peso
                nuevaLinea.Cantidad = elementos.Cantidad
                nuevaLinea.UnidadesPorTendido = elementos.UnidadesPorTendido
                nuevaLinea.UnidadesPorEstiba = elementos.UnidadesPorEstiba
                nuevaLinea.Lote = elementos.Lote
                nuevaLinea.DescripcionUnidades = elementos.DescripcionUnidades
                nuevaLinea.NumeroTendidos = elementos.NumeroTendidos
                Lineas.Add(nuevaLinea)
            Next
            Return DevuelveResultado(Cabeceraa.Pedido, "EntradaInventario")
        Catch

            Return DevuelveError("000", "Error en XML " & Err.Description, Err.Number, "Reaprovisionamiento")
        End Try
    End Function
    <WebMethod()> _
    Public Function PedidosDespachar(ByVal Pedido As String) As XmlDocument
        ContadorLineas = 0
        Dim nuevaLinea As DetallePedidoDespachar
        Dim Cabeceraa As New CabeceraPedidoDespachar
        Dim Lineas As New List(Of DetallePedidoDespachar)
        Dim documento As XDocument = XDocument.Parse(Pedido)
        'Dim fSql As String
        '      Dim DB1 As New ExecuteSQL
        Dim qx = From xee In documento.Elements("PedidoParaDespachar")
       Select New With { _
        .Usuario = xee.Element("Usuario").Value,
        .Password = xee.Element("Password").Value,
        .Pedido = xee.Element("Pedido").Value,
        .Factura = xee.Element("Factura").Value,
        .Planilla = xee.Element("Planilla").Value,
        .Cliente = xee.Element("Cliente").Value,
        .NombreCliente = xee.Element("NombreCliente").Value,
        .PuntoEnvio = xee.Element("PuntoEnvio").Value,
        .NombrePuntoEnvio = xee.Element("NombrePuntoEnvio").Value,
        .Dpto = xee.Element("Dpto").Value,
        .Ciudad = xee.Element("Ciudad").Value,
        .CodigoDane = xee.Element("CodigoDane").Value,
        .Direccion = xee.Element("Direccion").Value,
        .PlanillaPDF = xee.Element("PlanillaPDF").Value,
        .FechaCita = xee.Element("FechaCita").Value,
        .Notas = xee.Element("Notas").Value
                  }
        Try
            For Each elements In qx
                Cabeceraa = New CabeceraPedidoDespachar
                Cabeceraa.Usuario = elements.Usuario
                Cabeceraa.Password = elements.Password
                Cabeceraa.Pedido = elements.Pedido
                Cabeceraa.Factura = elements.Factura
                Cabeceraa.Planilla = elements.Planilla
                Cabeceraa.Cliente = elements.Planilla
                Cabeceraa.NombreCliente = elements.Planilla
                Cabeceraa.PuntoEnvio = elements.Planilla
                Cabeceraa.NombrePuntoEnvio = elements.Planilla
                Cabeceraa.Dpto = elements.Dpto
                Cabeceraa.Ciudad = elements.Ciudad
                Cabeceraa.CodigoDane = elements.CodigoDane
                Cabeceraa.Direccion = elements.Direccion
                Cabeceraa.PlanilaPDF = elements.PlanillaPDF
                Cabeceraa.FechaCita = elements.FechaCita
                Cabeceraa.Notas = elements.Notas
            Next

        Catch
            Return DevuelveError("000", "Error en XML" & Err.Description, Err.Number, "PedidosParaDespachar")

        End Try

        Try

            Dim qx2 = From xe In documento.Descendants.Elements("Linea")
        Select New With { _
                          .LineaPedido = xe.Element("LineaPedido").Value,
                          .Producto = xe.Element("Producto").Value,
                          .RequiereLote = xe.Element("RequiereLote").Value,
                          .Cantidad = xe.Element("Cantidad").Value
                            }
            For Each elementos In qx2
                ContadorLineas = ContadorLineas + 1
                nuevaLinea = New DetallePedidoDespachar
                nuevaLinea.Cantidad = elementos.Cantidad
                If nuevaLinea.Cantidad <= 0 Then
                    Return DevuelveError("000", "Cantidad no puede ser menor o igual que 0", 106, "PedidosParaDespachar")
                End If
                nuevaLinea.LineaPedido = elementos.LineaPedido
                nuevaLinea.Producto = elementos.Producto
                nuevaLinea.RequiereLote = elementos.RequiereLote
                nuevaLinea.Cantidad = elementos.Cantidad
                Lineas.Add(nuevaLinea)
            Next
            Return DevuelveResultado(Cabeceraa.Pedido, "PedidosPAraDespachar")
        Catch

            Return DevuelveError("000", "Error en XML " & Err.Description, Err.Number, "PedidosParaDespachar")
        End Try
    End Function

    <WebMethod()> _
    Public Function AvanceNovedad(ByVal Pedido As String) As XmlDocument

        ContadorLineas = 0
         Dim Cabeceraa As New DetalleAvanceNovedad
        Dim documento As XDocument = XDocument.Parse(Pedido)
        'Dim fSql As String
        '      Dim DB1 As New ExecuteSQL
        Dim qx = From xee In documento.Elements("AvanceNovedad")
       Select New With { _
                .Usuario = xee.Element("Usuario").Value,
                .Password = xee.Element("Password").Value,
                .Planilla = xee.Element("Planilla").Value,
                .Fecha = xee.Element("Fecha").Value,
                .Avance = xee.Element("Avance").Value
                  }
        Try
            For Each elements In qx
                Cabeceraa = New DetalleAvanceNovedad
                Cabeceraa.Usuario = elements.Usuario
                Cabeceraa.Password = elements.Password
                Cabeceraa.Planilla = elements.Planilla
                Cabeceraa.Fecha = elements.Fecha
                Cabeceraa.Avance = elements.Avance

            Next
        Catch
            Return DevuelveError("000", "Error en XML" & Err.Description, Err.Number, "AvanceNovedad")
        End Try
        Return DevuelveResultado(Cabeceraa.Planilla, "AvanceNovedad")
      End Function


    Function DevuelveError(ByVal NumeroDocumento As String, ByVal ERROOR As String, ByVal CODIGO As String, ByVal proceso As String) As XmlDocument


        Dim xmlstr As String = ""
        Dim sw As New StringWriter()
        Dim writer As New XmlTextWriter(sw)
        writer.WriteStartDocument(True)
        writer.Formatting = Formatting.Indented
        writer.Indentation = 2
        CrearNodoError(ERROOR, CODIGO, NumeroDocumento, proceso, writer)
        writer.Flush()
        xmlstr = sw.ToString
        writer.Close()
        Dim pDoc As New XmlDocument
        pDoc.LoadXml(xmlstr)
        Return pDoc

    End Function

    Function CrearNodoError(ByVal ErrorXML As String, ByVal codigo As String, ByVal NumeroDocumento As String, ByVal proceso As String, ByVal writer As XmlTextWriter)


        writer.WriteStartElement(proceso) ' ENVELOPE
        writer.WriteStartElement("Documento")  ' PEDIDO
        writer.WriteString(NumeroDocumento)
        writer.WriteStartElement("Mensaje")     'MENSAJE
        writer.WriteStartElement("Error")       'ERROR
        writer.WriteStartElement("MensajeID")   'ID
        writer.WriteString(codigo)
        writer.WriteEndElement() ' MensajeId
        writer.WriteStartElement("MensajeDES")
        writer.WriteString(ErrorXML)
        writer.WriteEndElement() ' DES
        writer.WriteEndElement() ' ERROR
        writer.WriteEndElement() ' MENSAJE
        writer.WriteEndElement() ' PEDIDO
        writer.WriteEndElement() ' ENVELOPE
        Return 0
    End Function

    Function DevuelveResultado(ByVal NumeroDocumento As String, ByVal proceso As String)
        ' Dim fSql As String
        Dim xmlstr As String = ""
        Dim sw As New StringWriter()
        Dim writer As New XmlTextWriter(sw)
        Dim flag As Integer = 0
        Dim flag2 As Integer = 0

        Dim rs As New ADODB.Recordset

        writer.WriteStartDocument(True)
        writer.Formatting = Formatting.Indented
        writer.Indentation = 2
        writer.WriteStartElement("Respuesta")
        writer.WriteStartElement("Operacion")
        writer.WriteString(proceso)
        writer.WriteEndElement()
        If (proceso <> "AvanceNovedad") Then
            writer.WriteStartElement("Pedido")
        Else
            writer.WriteStartElement("Planilla")
        End If
        writer.WriteString(NumeroDocumento)
        writer.WriteEndElement()
        If (proceso <> "AvanceNovedad") Then
            writer.WriteStartElement("TotalLineas")
            writer.WriteString(ContadorLineas)
            writer.WriteEndElement()
        End If
        writer.WriteStartElement("Mensaje")
        writer.WriteString("Ok")
        writer.WriteEndElement()
        writer.WriteEndElement()

        writer.Flush()
        xmlstr = sw.ToString
        writer.Close()
        Dim pDoc As New XmlDocument
        pDoc.LoadXml(xmlstr)

        Return pDoc

    End Function










End Class