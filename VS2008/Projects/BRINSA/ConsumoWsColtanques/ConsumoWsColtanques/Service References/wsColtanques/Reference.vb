﻿'------------------------------------------------------------------------------
' <auto-generated>
'     Este código fue generado por una herramienta.
'     Versión de runtime:4.0.30319.34003
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Namespace wsColtanques
    
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ServiceModel.ServiceContractAttribute(ConfigurationName:="wsColtanques.PedidoParaDespacharSoap")>  _
    Public Interface PedidoParaDespacharSoap
        
        'CODEGEN: Se está generando un contrato de mensaje, ya que el nombre de elemento Pedido del espacio de nombres http://tempuri.org/ no está marcado para aceptar valores nil.
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/Reaprovisionamiento", ReplyAction:="*")>  _
        Function Reaprovisionamiento(ByVal request As wsColtanques.ReaprovisionamientoRequest) As wsColtanques.ReaprovisionamientoResponse
        
        'CODEGEN: Se está generando un contrato de mensaje, ya que el nombre de elemento Pedido del espacio de nombres http://tempuri.org/ no está marcado para aceptar valores nil.
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/PedidosDespachar", ReplyAction:="*")>  _
        Function PedidosDespachar(ByVal request As wsColtanques.PedidosDespacharRequest) As wsColtanques.PedidosDespacharResponse
        
        'CODEGEN: Se está generando un contrato de mensaje, ya que el nombre de elemento Pedido del espacio de nombres http://tempuri.org/ no está marcado para aceptar valores nil.
        <System.ServiceModel.OperationContractAttribute(Action:="http://tempuri.org/AvanceNovedad", ReplyAction:="*")>  _
        Function AvanceNovedad(ByVal request As wsColtanques.AvanceNovedadRequest) As wsColtanques.AvanceNovedadResponse
    End Interface
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(IsWrapped:=false)>  _
    Partial Public Class ReaprovisionamientoRequest
        
        <System.ServiceModel.MessageBodyMemberAttribute(Name:="Reaprovisionamiento", [Namespace]:="http://tempuri.org/", Order:=0)>  _
        Public Body As wsColtanques.ReaprovisionamientoRequestBody
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Body As wsColtanques.ReaprovisionamientoRequestBody)
            MyBase.New
            Me.Body = Body
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.Runtime.Serialization.DataContractAttribute([Namespace]:="http://tempuri.org/")>  _
    Partial Public Class ReaprovisionamientoRequestBody
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=0)>  _
        Public Pedido As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Pedido As String)
            MyBase.New
            Me.Pedido = Pedido
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(IsWrapped:=false)>  _
    Partial Public Class ReaprovisionamientoResponse
        
        <System.ServiceModel.MessageBodyMemberAttribute(Name:="ReaprovisionamientoResponse", [Namespace]:="http://tempuri.org/", Order:=0)>  _
        Public Body As wsColtanques.ReaprovisionamientoResponseBody
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Body As wsColtanques.ReaprovisionamientoResponseBody)
            MyBase.New
            Me.Body = Body
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.Runtime.Serialization.DataContractAttribute([Namespace]:="http://tempuri.org/")>  _
    Partial Public Class ReaprovisionamientoResponseBody
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=0)>  _
        Public ReaprovisionamientoResult As System.Xml.Linq.XElement
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal ReaprovisionamientoResult As System.Xml.Linq.XElement)
            MyBase.New
            Me.ReaprovisionamientoResult = ReaprovisionamientoResult
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(IsWrapped:=false)>  _
    Partial Public Class PedidosDespacharRequest
        
        <System.ServiceModel.MessageBodyMemberAttribute(Name:="PedidosDespachar", [Namespace]:="http://tempuri.org/", Order:=0)>  _
        Public Body As wsColtanques.PedidosDespacharRequestBody
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Body As wsColtanques.PedidosDespacharRequestBody)
            MyBase.New
            Me.Body = Body
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.Runtime.Serialization.DataContractAttribute([Namespace]:="http://tempuri.org/")>  _
    Partial Public Class PedidosDespacharRequestBody
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=0)>  _
        Public Pedido As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Pedido As String)
            MyBase.New
            Me.Pedido = Pedido
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(IsWrapped:=false)>  _
    Partial Public Class PedidosDespacharResponse
        
        <System.ServiceModel.MessageBodyMemberAttribute(Name:="PedidosDespacharResponse", [Namespace]:="http://tempuri.org/", Order:=0)>  _
        Public Body As wsColtanques.PedidosDespacharResponseBody
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Body As wsColtanques.PedidosDespacharResponseBody)
            MyBase.New
            Me.Body = Body
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.Runtime.Serialization.DataContractAttribute([Namespace]:="http://tempuri.org/")>  _
    Partial Public Class PedidosDespacharResponseBody
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=0)>  _
        Public PedidosDespacharResult As System.Xml.Linq.XElement
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal PedidosDespacharResult As System.Xml.Linq.XElement)
            MyBase.New
            Me.PedidosDespacharResult = PedidosDespacharResult
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(IsWrapped:=false)>  _
    Partial Public Class AvanceNovedadRequest
        
        <System.ServiceModel.MessageBodyMemberAttribute(Name:="AvanceNovedad", [Namespace]:="http://tempuri.org/", Order:=0)>  _
        Public Body As wsColtanques.AvanceNovedadRequestBody
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Body As wsColtanques.AvanceNovedadRequestBody)
            MyBase.New
            Me.Body = Body
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.Runtime.Serialization.DataContractAttribute([Namespace]:="http://tempuri.org/")>  _
    Partial Public Class AvanceNovedadRequestBody
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=0)>  _
        Public Pedido As String
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Pedido As String)
            MyBase.New
            Me.Pedido = Pedido
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.ServiceModel.MessageContractAttribute(IsWrapped:=false)>  _
    Partial Public Class AvanceNovedadResponse
        
        <System.ServiceModel.MessageBodyMemberAttribute(Name:="AvanceNovedadResponse", [Namespace]:="http://tempuri.org/", Order:=0)>  _
        Public Body As wsColtanques.AvanceNovedadResponseBody
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal Body As wsColtanques.AvanceNovedadResponseBody)
            MyBase.New
            Me.Body = Body
        End Sub
    End Class
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0"),  _
     System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced),  _
     System.Runtime.Serialization.DataContractAttribute([Namespace]:="http://tempuri.org/")>  _
    Partial Public Class AvanceNovedadResponseBody
        
        <System.Runtime.Serialization.DataMemberAttribute(EmitDefaultValue:=false, Order:=0)>  _
        Public AvanceNovedadResult As System.Xml.Linq.XElement
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal AvanceNovedadResult As System.Xml.Linq.XElement)
            MyBase.New
            Me.AvanceNovedadResult = AvanceNovedadResult
        End Sub
    End Class
    
    <System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")>  _
    Public Interface PedidoParaDespacharSoapChannel
        Inherits wsColtanques.PedidoParaDespacharSoap, System.ServiceModel.IClientChannel
    End Interface
    
    <System.Diagnostics.DebuggerStepThroughAttribute(),  _
     System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")>  _
    Partial Public Class PedidoParaDespacharSoapClient
        Inherits System.ServiceModel.ClientBase(Of wsColtanques.PedidoParaDespacharSoap)
        Implements wsColtanques.PedidoParaDespacharSoap
        
        Public Sub New()
            MyBase.New
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String)
            MyBase.New(endpointConfigurationName)
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String, ByVal remoteAddress As String)
            MyBase.New(endpointConfigurationName, remoteAddress)
        End Sub
        
        Public Sub New(ByVal endpointConfigurationName As String, ByVal remoteAddress As System.ServiceModel.EndpointAddress)
            MyBase.New(endpointConfigurationName, remoteAddress)
        End Sub
        
        Public Sub New(ByVal binding As System.ServiceModel.Channels.Binding, ByVal remoteAddress As System.ServiceModel.EndpointAddress)
            MyBase.New(binding, remoteAddress)
        End Sub
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function wsColtanques_PedidoParaDespacharSoap_Reaprovisionamiento(ByVal request As wsColtanques.ReaprovisionamientoRequest) As wsColtanques.ReaprovisionamientoResponse Implements wsColtanques.PedidoParaDespacharSoap.Reaprovisionamiento
            Return MyBase.Channel.Reaprovisionamiento(request)
        End Function
        
        Public Function Reaprovisionamiento(ByVal Pedido As String) As System.Xml.Linq.XElement
            Dim inValue As wsColtanques.ReaprovisionamientoRequest = New wsColtanques.ReaprovisionamientoRequest()
            inValue.Body = New wsColtanques.ReaprovisionamientoRequestBody()
            inValue.Body.Pedido = Pedido
            Dim retVal As wsColtanques.ReaprovisionamientoResponse = CType(Me,wsColtanques.PedidoParaDespacharSoap).Reaprovisionamiento(inValue)
            Return retVal.Body.ReaprovisionamientoResult
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function wsColtanques_PedidoParaDespacharSoap_PedidosDespachar(ByVal request As wsColtanques.PedidosDespacharRequest) As wsColtanques.PedidosDespacharResponse Implements wsColtanques.PedidoParaDespacharSoap.PedidosDespachar
            Return MyBase.Channel.PedidosDespachar(request)
        End Function
        
        Public Function PedidosDespachar(ByVal Pedido As String) As System.Xml.Linq.XElement
            Dim inValue As wsColtanques.PedidosDespacharRequest = New wsColtanques.PedidosDespacharRequest()
            inValue.Body = New wsColtanques.PedidosDespacharRequestBody()
            inValue.Body.Pedido = Pedido
            Dim retVal As wsColtanques.PedidosDespacharResponse = CType(Me,wsColtanques.PedidoParaDespacharSoap).PedidosDespachar(inValue)
            Return retVal.Body.PedidosDespacharResult
        End Function
        
        <System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Function wsColtanques_PedidoParaDespacharSoap_AvanceNovedad(ByVal request As wsColtanques.AvanceNovedadRequest) As wsColtanques.AvanceNovedadResponse Implements wsColtanques.PedidoParaDespacharSoap.AvanceNovedad
            Return MyBase.Channel.AvanceNovedad(request)
        End Function
        
        Public Function AvanceNovedad(ByVal Pedido As String) As System.Xml.Linq.XElement
            Dim inValue As wsColtanques.AvanceNovedadRequest = New wsColtanques.AvanceNovedadRequest()
            inValue.Body = New wsColtanques.AvanceNovedadRequestBody()
            inValue.Body.Pedido = Pedido
            Dim retVal As wsColtanques.AvanceNovedadResponse = CType(Me,wsColtanques.PedidoParaDespacharSoap).AvanceNovedad(inValue)
            Return retVal.Body.AvanceNovedadResult
        End Function
    End Class
End Namespace
