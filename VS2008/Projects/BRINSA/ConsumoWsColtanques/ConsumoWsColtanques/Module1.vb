﻿'REAPROVISIONAMIENTO
Imports System
Imports System.Globalization
Imports System.IO
Imports System.Collections
Imports System.Xml.Serialization

Imports System.ComponentModel
Imports System.Xml
Imports System.Net
Imports ConsumoWsColtanques
Imports ConsumoWsColtanques.wsColtanques
Module Module1


    Dim gsProvider As String
    Dim gsDatasource As String
    Dim rs As New ADODB.Recordset
    Dim mp2Param(5) As String
    Dim AS400Param(5) As String
    Dim Resultado As Boolean = True
    Dim i As Integer
    Dim sLib
    Dim gsConnString
    Dim Pedid As String
    Dim conso As String
    Dim DB_DB2, DB_MP2
    Dim C_ID As String

    Structure respuesta

        Dim Operacion As String
        Dim Pedido As String
        Dim TotalLineas As String
        Dim Mensaje As String

    End Structure

    Sub Main(ByVal Pedido As String())

        Pedid = Pedido(1)
        conso = Pedido(0)
        CargaPrm()
        C_ID = CalcConsec("RFLOGWSCDA", False, "99999999")
        DevuelveResultado()
    End Sub
    Function CargaPrm() As Boolean


        gsProvider = "IBMDA400"
        gsDatasource = My.Settings.AS400
        sLib = My.Settings.AMBIENTE

        gsConnString = "Provider=" & gsProvider & ";Data Source=" & gsDatasource & ";"
        gsConnString = gsConnString & "Persist Security Info=False;Default Collection=" & sLib & ";"
        gsConnString = gsConnString & "Password=LXAPL;User ID=APLLX;"

        DB_DB2 = New ADODB.Connection
        DB_MP2 = New ADODB.Connection
        DB_DB2.Open(gsConnString)

        rs.Open(" SELECT * FROM " & sLib & ".ZCC WHERE CCTABL = 'LXLONG'", DB_DB2)
        Do While Not rs.EOF
            Select Case rs("CCCODE").Value.ToString
                Case "LXUSER"
                    AS400Param(0) = rs("CCDESC").Value.ToString
                Case "LXPASS"
                    AS400Param(1) = rs("CCDESC").Value.ToString
            End Select
            rs.MoveNext()
        Loop
        rs.Close()

        For i = 0 To 1
            If AS400Param(i) = "" Then
                Resultado = False
            End If
        Next

        gsConnString = "Provider=" & gsProvider & ";Data Source=" & gsDatasource & ";"
        gsConnString = gsConnString & "Persist Security Info=False;Default Collection=" & sLib & ";"
        gsConnString = gsConnString & "Password=" & AS400Param(1) & ";User ID=" & AS400Param(0) & ";"
        gsConnString = gsConnString & "Force Translate=0;"
        DB_DB2.Close()
        If Resultado Then
            DB_DB2.Open(gsConnString)
        Else
            'Error Conexion
        End If

        Return Resultado

    End Function

    Function DevuelveResultado()
        Dim fSql As String
        Dim xmlstr As String = ""
        Dim sw As New StringWriter()
        Dim writer As New XmlTextWriter(sw)
        Dim flag As Integer = 0
        Dim flag2 As Integer = 0
        Dim pedidos As String

        fSql = " SELECT DPEDID, DLINEA, DPROD, IDESC, IUMR, CCDESC, IWGHT, RNTUL, RUXTUL, RUXEST, TLOT, sum(-TQTY) AS CANT "
        fSql &= " FROM RDCC INNER JOIN IIM ON DPROD = IPROD INNER JOIN ZCCL01 ON CCTABL = 'UNITMEAS' AND CCCODE = IUMR "
        fSql &= " INNER JOIN RIIM ON DPROD = RIPROD "
        fSql &= " INNER JOIN ITH ON DPEDID = TREF AND DPROD = TPROD AND TTYPE = 'B' "
        fSql &= " WHERE  "
        fSql &= " DCONSO = '" & conso & "'  "
        fSql &= " AND DPEDID = " & Pedid & ""
        fSql &= " group by TLOT, DPEDID, DLINEA, DPROD, IDESC, IUMR, CCDESC, IWGHT, RNTUL, RUXTUL, RUXEST"

        rs.Open(fSql, DB_DB2)


        If Not rs.EOF Then
            ' writer.WriteStartDocument(True)
            writer.Formatting = Formatting.Indented
            writer.Indentation = 2
            writer.WriteStartElement("Reaprovisonamiento")
            writer.WriteStartElement("Usuario")
            writer.WriteString("BRINSA")
            writer.WriteEndElement()
            writer.WriteStartElement("Password")
            writer.WriteString("83023911.")
            writer.WriteEndElement()
            writer.WriteStartElement("Pedido")
            writer.WriteString(rs("DPEDID").Value.ToString)
            writer.WriteEndElement()

            pedidos = rs("DPEDID").Value.ToString
            writer.WriteStartElement("Fecha")
            writer.WriteString("2014-01-28")
            writer.WriteEndElement()
        End If
        While Not rs.EOF
            writer.WriteStartElement("Linea")
            CrearNodoLine(rs("DLINEA").Value.ToString, rs("DPROD").Value.ToString, rs("IDESC").Value.ToString, rs("IUMR").Value.ToString, rs("CCDESC").Value.ToString, rs("IWGHT").Value.ToString, rs("RNTUL").Value.ToString, rs("RUXTUL").Value.ToString, rs("RUXEST").Value.ToString, rs("TLOT").Value.ToString, rs("CANT").Value.ToString, writer) ' Las veces que se desee Programa line
            writer.WriteEndElement()
            rs.MoveNext()
        End While

        writer.WriteEndElement()

        writer.Flush()
        xmlstr = sw.ToString
        writer.Close()
        Dim pDoc As New XmlDocument
        pDoc.LoadXml(xmlstr)
        Dim xml As String = pDoc.InnerXml

        wrtLogWsCDA(C_ID, xml, "Pedido" & pedidos, "", "Reaprovisionamiento", "wsColtanques", "EM")
        Dim prueba = New co.com.coltanques.www.WSDistribucion
        Dim PruebaDoc As XmlDocument
        Dim PruebaString As String
        PruebaString = prueba.Reaprovisionamiento(xml)


        '  wrtLogWsCDA(C_ID, PruebaString, "Pedido" & pedidos, "Fail", "Reaprovisionamiento", "wsColtanques", "EM")


        Dim nuevaLinea As respuesta
        Dim Cabeceraa As New respuesta
        Dim documento As XDocument = XDocument.Parse(PruebaString)
        Dim errorr As Integer
        errorr = 0

        Dim qx = From xee In documento.Elements("Respuesta")
      Select New With { _
               .Operacion = xee.Element("Operacion").Value,
               .Pedido = xee.Element("Pedido").Value,
               .TotalLineas = xee.Element("TotalLineas").Value,
               .Mensaje = xee.Element("Mensaje").Value
                 }
        Try
            For Each elements In qx
                errorr = errorr + 1
                Cabeceraa = New respuesta
                Cabeceraa.Operacion = elements.Operacion
                Cabeceraa.Pedido = elements.Pedido
                Cabeceraa.TotalLineas = elements.TotalLineas
                Cabeceraa.Mensaje = elements.Mensaje
            Next

        Catch
            wrtLogWsCDA(C_ID, PruebaString, "Pedido" & pedidos, "Fail", "Reaprovisionamiento", "wsColtanques", "EM")
        End Try
        If errorr > 0 Then
            If Cabeceraa.Mensaje = "0000" Then
                wrtLogWsCDA(C_ID, PruebaString, "Pedido" & pedidos, "Pass", "Reaprovisionamiento", "wsColtanques", "EM")


                fSql = "INSERT INTO RFBSITH"
                fSql = fSql & " SELECT LEFT(TTDTE, 4), DCONSO, T.*"
                fSql = fSql & " FROM RDCC INNER JOIN ITH T ON DPEDID = TREF AND DPROD = TPROD AND TTYPE = 'B'"
                fSql = fSql & " WHERE "
                fSql = fSql & " DCONSO = '" & conso & "' "
                fSql = fSql & " AND DPEDID = " & Pedid

                DB_DB2.Execute(fSql)
            Else
                wrtLogWsCDA(C_ID, PruebaString, "Pedido" & pedidos, "Fail", "Reaprovisionamiento", "wsColtanques", "EM")
            End If
        Else
            wrtLogWsCDA(C_ID, PruebaString, "Pedido" & pedidos, "Fail", "Reaprovisionamiento", "wsColtanques", "EM")
        End If

            rs.Close()
            DB_DB2.Close()
            Return pDoc

    End Function

    Function CrearNodoLine(ByVal linea As String, ByVal CodigoProducto As String, ByVal DescripcionProducto As String, ByVal Unidades As String, ByVal DescripcionUnidades As String, ByVal Peso As String, ByVal NumeroTendidos As String, ByVal UnidadesPorTendido As String, ByVal UnidadesPorEstiba As String, ByVal Lote As String, ByVal Cantidad As String, ByVal writer As XmlTextWriter)

        writer.WriteStartElement("LineaPedido")
        writer.WriteString(linea)
        writer.WriteEndElement()
        writer.WriteString(vbCrLf)
        writer.WriteStartElement("CodigoProducto")
        writer.WriteString(CodigoProducto)
        writer.WriteEndElement()
        writer.WriteString(vbCrLf)
        writer.WriteStartElement("DescripcionProducto")
        writer.WriteString(DescripcionProducto)
        writer.WriteEndElement()
        writer.WriteString(vbCrLf)
        writer.WriteStartElement("Unidades")
        writer.WriteString(Unidades)
        writer.WriteEndElement()
        writer.WriteString(vbCrLf)
        writer.WriteStartElement("DescripcionUnidades")
        writer.WriteString(DescripcionUnidades)
        writer.WriteEndElement()
        writer.WriteString(vbCrLf)
        writer.WriteStartElement("Peso")
        writer.WriteString(Replace(Peso, ",", "."))
        writer.WriteEndElement()
        writer.WriteString(vbCrLf)
        writer.WriteStartElement("NumeroTendidos")
        writer.WriteString(Replace(NumeroTendidos, ",", "."))
        writer.WriteEndElement()
        writer.WriteString(vbCrLf)
        writer.WriteStartElement("UnidadesPorTendido")
        writer.WriteString(Replace(UnidadesPorTendido, ",", "."))
        writer.WriteEndElement()
        writer.WriteString(vbCrLf)
        writer.WriteStartElement("UnidadesPorEstiba")
        writer.WriteString(Replace(UnidadesPorEstiba, ",", "."))
        writer.WriteEndElement()
        writer.WriteString(vbCrLf)
        writer.WriteStartElement("Lote")
        writer.WriteString(Replace(Lote, ",", "."))
        writer.WriteEndElement()
        writer.WriteString(vbCrLf)
        writer.WriteStartElement("Cantidad")
        writer.WriteString(Replace(Cantidad, ",", "."))
        writer.WriteEndElement()
        writer.WriteString(vbCrLf)
        Return 0
    End Function

    Sub wrtLogWsCDA(ByVal logId As String, ByVal xml As String, ByVal id_msg As String, ByVal estado As String, ByVal operacion As String, ByVal fuente As String, ByVal usuario As String)
        Dim rs As New ADODB.RecordSet
        Dim dato() As Byte
        dato = System.Text.Encoding.UTF8.GetBytes("<?xml version=""1.0""?>" & xml)
        With rs
            .CursorLocation = ADODB.CursorLocationEnum.adUseServer
            .Open("SELECT * FROM RFLOGWSCDA WHERE C_ID = " & logId, DB_DB2, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)
            If .EOF Then
                .AddNew()
                .Fields("C_ID").Value = logId
                .Fields("C_XML").Value = dato
                .Fields("C_NOUN_ID").Value = operacion
                .Fields("C_MESSAGEID").Value = id_msg
                .Fields("C_SOURCE_NAME").Value = fuente
                .Fields("C_USER").Value = usuario
                .Fields("C_BODID").Value = ""
            Else
                .Fields("C_WAS_PROCESSED").Value = 1
                .Fields("C_REPLY").Value = dato
                .Fields("C_PASS_FAIL").Value = estado
            End If
            .Update()
            .Close()
        End With
        rs = Nothing
    End Sub
    Public Function CalcConsec(ByRef sID As String, ByRef bLee As Boolean, ByRef TopeMax As String) As Double
        Dim rs As ADODB.Recordset
        Dim locID As Double
        Dim sql As String
        Dim sConsec As String
        Dim tMax As Double

        sql = "SELECT CCDESC FROM ZCCL01 WHERE CCTABL = 'SECUENCE' AND CCCODE='" & sID & "'"
        rs = New ADODB.Recordset
        tMax = Val(TopeMax)

        With rs
            .CursorLocation = ADODB.CursorLocationEnum.adUseServer
            .Open(sql, DB_DB2, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)
            If Not (.BOF And .EOF) Then
                locID = Val(.Fields("CCDESC").Value) + 1
                If locID > tMax Then
                    locID = 1
                End If
                sConsec = Format(locID, New String("0", Len(TopeMax)))
                If Not bLee Then
                    .Fields("CCDESC").Value = sConsec
                    .Update()
                End If
                .Close()
            Else
                locID = 1
                .Close()
                sConsec = Format(1, New String("0", Len(8)))
                DB_DB2.Execute(" INSERT INTO ZCC (CCID, CCTABL, CCCODE, CCDESC ) " & " VALUES( 'CC', 'SECUENCE', '" & sID & "', '" & sConsec & "' )")
            End If
        End With

        CalcConsec = locID

    End Function
End Module
