﻿Public Class bssConexion
    Private local As String
    Private gsAS400 As String = "192.168.10.1"
    Private gsLib As String
    Private sUser As String
    Private sUserApl As String
    Private sUserPass As String
    Private conn As New ADODB.Connection

    Public Sentencia As String
    Public bLog As Boolean = False
    Public iNumLog As Integer
    Dim msgErr As String
    Dim gsConnString As String
    Dim rAfectados As Integer
    Dim Descripcion_Error As String

    Sub wrtLog(a As String, b As String)

    End Sub
    Sub abrirConexion(qLib As String, qServer As String)
        Dim DB As New ADODB.Connection
        Dim rs As New ADODB.Recordset
        gsLib = qLib
        DB.Open("Provider=IBMDA400.DataSource;Data Source=" & gsAS400 & ";User ID=APLLX;Password=LXAPL;Persist Security Info=True;" & _
                "Application Name=" & My.Application.Info.ProductName & ";Default Collection=" & gsLib & ";Force Translate=0;")
        rs.Open("SELECT * FROM ZCCL01 WHERE CCTABL = 'LXLONG' AND UPPER(CCSDSC) = '" & qServer.ToUpper() & "' ORDER BY CCUDC1", DB)
        Do While Not rs.EOF
            Select Case rs("CCUDC1").Value
                Case 1
                    gsConnString &= "Provider=" & rs("CCNOT1").Value & ";Data Source=" & rs("CCDESC").Value & ";"
                Case 2
                    gsConnString &= "User ID=" & rs("CCDESC").Value & ";"
                    sUser = rs("CCDESC").Value
                Case 3
                    gsConnString &= "Password=" & rs("CCDESC").Value & ";"
                Case 4
                    Select Case qServer.ToUpper()
                        Case "AS400"
                            gsConnString &= "Default Collection=" & rs("CCDESC").Value & ";Force Translate=0;Convert Date Time To Char=FALSE"
                        Case "MP2"
                            gsConnString &= "Initial Catalog=" & rs("CCDESC").Value
                        Case "WMS"
                            gsConnString &= "Initial Catalog=" & rs("CCDESC").Value
                    End Select
            End Select
            rs.MoveNext()
        Loop
        rs.Close()
        DB.Close()
        Try
            conn.Open(gsConnString)
        Catch ex As Exception
            msgErr = Err.Description

        End Try

        'Provider=SQLOLEDB.1;User ID=mp2;Initial Catalog=t_mp2;Data Source=192.168.10.2;Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;Workstation ID=FM-WIN7U;Use Encryption for Data=False;Tag with column collation when possible=False;
        'Provider=IBMDA400.DataSource.1;Password=DIC2013;Persist Security Info=True;User ID=DESLXSSA;Data Source=192.168.10.1;Force Translate=0;Default Collection=DESLX834F;Convert Date Time To Char=FALSE

    End Sub

    Public Sub Close()
        conn.Close()
    End Sub

    Public Function DspErr() As String
        Return "" 'Error_Tabla & " " & Sentencia & " " & vbCrLf & Descripcion_Error
    End Function

    Public Function ExecuteSQL(ByVal sql As String) As ADODB.Recordset
        Try
            Sentencia = sql
            Return Conn.Execute(sql, rAfectados)
        Catch
            Descripcion_Error = Err.Description
            Return Nothing
        End Try

    End Function

    Public Function rs(ByVal sql As String) As ADODB.Recordset
        Dim rsT As New ADODB.Recordset
        rsT.Open(sql, Conn)
        Return rsT
    End Function

    Public Function Cursor(ByRef rs As ADODB.Recordset, ByVal sql As String) As Integer

        Dim Result As Integer
        Sentencia = sql
        Try
            rs.Open(sql, Conn)
        Catch
            Descripcion_Error = Sentencia & " " & Err.Description
            wrtLog(Err.Description, iNumLog)
            Return -99
        End Try
        Result = IIf(rs.EOF, 0, 1)
        Return Result

    End Function

    Public Function Cursor(ByRef rs As ADODB.Recordset) As Integer
        Dim Result As Integer
        Try
            rs.Open(Sentencia, Conn)
        Catch
            Descripcion_Error = Sentencia & " " & Err.Description
            wrtLog(Err.Description, iNumLog)
            Return -99
        End Try
        Result = IIf(rs.EOF, 0, 1)
        Return Result
    End Function



    Public Function CalcConsec(ByRef sID As String, ByRef TopeMax As String) As String
        Return ""
    End Function
    Public Function Call_PGM(ByVal Pgm As String, ByVal pais As String) As Integer
        Dim r As Integer = 0
        'Dim Conn As 
        Try
            'Conn.Execute(" {{ " & Pgm & " }} ", r)
            Return 0
        Catch
            msgErr = Err.Description
            Return -99
        End Try



    End Function
    Public Function ShowError() As String
        Return msgErr
    End Function
    Sub setConnectionString(ByVal Prov As String, ByVal srv As String, ByVal usr As String, ByVal pass As String, ByVal sLib As String, ByVal sCatalogo As String)
        'Dim g As String
        'g = "Data Source=192.168.10.1;Default Collection=DESLX834F;Persist Security Info=True;User ID=DESLXSSA;Password=ABR2013"

        Dim g As String

        g = ""
        g = g & "DRIVER=Client Access ODBC Driver (32-bit); "
        g = g & "UID=" & usr & ";"
        g = g & "PWD=" & pass & ";"
        g = g & "SYSTEM=" & srv
        gsConnString = g
        wrtLog(g, iNumLog)


    End Sub

    Public Function CalcConsecV(ByRef sID As String, ByRef TopeMax As String) As Double
        Return CDbl(CalcConsec(sID, TopeMax))
    End Function


    'Sub  wrtLog(sql As String)
    '    Dim fSql, vSql As String

    '    If bLog Then
    '        fSql = " INSERT INTO BFLOG( "
    '        vSql = " VALUES ( "
    '        fSql = fSql & " LNUM      , " : vSql = vSql & " " & iNumLog & ", "
    '        fSql = fSql & " LLOG      ) " : vSql = vSql & "'" & Replace(sql, "'", "''") & "' )"
    '        DB.Execute(fSql & vSql)
    '    End If

    'End Sub

    Function SetNumLog() As Integer
        iNumLog = CInt(CalcConsec("WMSLOG", "99"))
        Return iNumLog
    End Function

    Public Function GetNumLog() As Integer
        Return iNumLog
    End Function

    Sub WrtSqlError(sql As String, Descrip As String)
        Dim DB As New ADODB.Connection
        Dim fSql As String
        Dim vSql As String

        Try
            DB.Open("Provider=IBMDA400.DataSource;Data Source=" & gsAS400 & ";User ID=APLLX;Password=LXAPL;Persist Security Info=True;Application Name=DFU001;Default Collection=" & gsLib & ";Force Translate=0;")
            DB.Open(gsConnString)
            fSql = " INSERT INTO RFLOG( "
            vSql = " VALUES ( "
            fSql = fSql & " USUARIO   , " : vSql = vSql & "'" & sUser & "', "      '//502A
            fSql = fSql & " PROGRAMA  , " : vSql = vSql & "'" & "VB." & My.Application.Info.ProductName & "." & _
                                                                My.Application.Info.Version.Major & "." & _
                                                                My.Application.Info.Version.Minor & "." & _
                                                                My.Application.Info.Version.Revision & "." & "SQLERR" & "', "      '//502A
            fSql = fSql & " EVENTO    , " : vSql = vSql & "'" & Replace(Descrip, "'", "''") & "', "      '//20002A
            fSql = fSql & " TXTSQL    ) " : vSql = vSql & "'" & Replace(sql, "'", "''") & "' ) "      '//5002A
            DB.Execute(fSql & vSql)
            DB.Close()
        Catch ex As Exception

        End Try

    End Sub

End Class
