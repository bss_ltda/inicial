﻿'BRINSA-COLTANQUES
Imports System
Imports System.Globalization
Imports System.IO
Imports System.Collections
Imports System.Xml.Serialization
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Xml
Imports System.Net
Imports BSSClassLibrary

' Para permitir que se llame a este servicio Web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class BodegaSatelite
    Inherits System.Web.Services.WebService
    Dim xmlRespuesta As String
    Dim Programa As String
    Dim ERRORES As String
    Dim Factura As String
    Dim Pedid As String
    Dim Ial As String
    Dim Oc As String
    Dim Bodega As String
    Dim conso As String
    Dim Factu As String
    Dim rs As New ADODB.Recordset
    Dim rs1 As New ADODB.Recordset
    Dim Resultado As Boolean = True
    Dim i As Integer
    Dim fSql As String
    Dim vSql As String

    Dim db As New bssConexion
    Dim sql As New bssSQL
    Dim u As New bssUtil

    Dim C_ID As String
    Dim ContadorLineas As Integer

    Structure Cabecera
        Dim Usuario As String
        Dim Password As String
        Dim Pedido As Double
        Dim Fecha As String
        Dim Bodega As String
    End Structure
    Structure CabeceraDespacho
        Dim Usuario As String
        Dim Password As String
        Dim Manifiesto As String
        Dim Bodega As String

        Dim TipoVehiculo As String
        Dim Notas As String
        Dim DescTipoVehiculo As String
        Dim Placa As String
        Dim CedulaConductor As String
        Dim NombreConductor As String
        Dim CelularConductor As String
    End Structure

    Structure DetalleDespacho
        Dim Pedido As String
    End Structure

    Structure CabeceraPedidoDespachado
        Dim Usuario As String
        Dim Password As String
        Dim Pedido As Double

    End Structure

 
    Structure DetallePedidoDespachado

        Dim LineaPedido As Integer
        Dim Producto As String
        Dim Lote As String
        Dim Cantidad As Double
    End Structure

    Structure DetalleReporteEntrega

        Dim Usuario As String
        Dim Password As String
        Dim Planilla As String
        Dim FechaReporte As String
        Dim EstadoEntrega As Double
        Dim Reporte As String
    End Structure

    Structure DetalleReporteDespacho

        Dim Usuario As String
        Dim Password As String
        Dim NumeroUnico As Double
        Dim Consolidado As String
        Dim EstadoDespacho As Double
        Dim FechaReporte As String
        Dim Manifiesto As Double
        Dim ReporteGeneral As String
    End Structure
    Structure Detalle

        Dim LineaPedido As Integer
        Dim CodigoProducto As String
        Dim Lote As String
        Dim Cantidad As String
        Dim Ubicacion As String
    End Structure


    <WebMethod()> _
    Public Function ConfirmacionEntradaInventario(ByVal XML As String) As XmlDocument
        db.abrirConexion(My.Settings.AMBIENTE, "AS400")
        C_ID = db.CalcConsec("RFLOGWSCDA", "99999999")
        db.wrtLogWsCDA(C_ID, XML, "Recepcion", "", "ConfirmacionEntradaInventario", "wsBrinsa", "EM")
        ContadorLineas = 0
        Dim nuevaLinea As Detalle
        Dim Cabeceraa As New Cabecera
        Dim Lineas As New List(Of Detalle)
        Dim documento As XDocument = XDocument.Parse(XML)
        Dim qx = From xee In documento.Elements("ConfirmacionEntradaInventario")
       Select New With { _
                .Usuario = xee.Element("Usuario").Value,
                .Password = xee.Element("Password").Value,
                .Pedido = xee.Element("Pedido").Value,
                .Fecha = xee.Element("Fecha").Value,
                .Bodega = xee.Element("Bodega").Value
                  }
        Try
            For Each elements In qx
                Cabeceraa = New Cabecera
                Cabeceraa.Usuario = elements.Usuario
                Cabeceraa.Password = elements.Password
                Cabeceraa.Pedido = elements.Pedido
                Cabeceraa.Fecha = elements.Fecha
                Cabeceraa.Bodega = elements.Bodega
            Next
            db.wrtLogWsCDA(C_ID, XML, "Pedido-" & Cabeceraa.Pedido, "", "ConfirmacionEntradaInventario", "wsBrinsa", "EM")
            If Not RevisaUsuario(Cabeceraa.Usuario, Cabeceraa.Password) Then
                Return DevuelveError(Cabeceraa.Pedido, "Usuario/Password Incorrecto", "5050", "ConfirmacionEntradaInventario")
            End If
        Catch
            Return DevuelveError("000", "Error Estructura  XML ", "5011", "ConfirmacionEntradaInventario")
        End Try

        Try
            Dim bPrimera As Boolean = True
            Dim qx2 = From xe In documento.Descendants.Elements("Linea")
        Select New With { _
                          .LineaPedido = xe.Element("LineaPedido").Value,
                          .CodigoProducto = xe.Element("CodigoProducto").Value,
                          .Cantidad = xe.Element("Cantidad").Value,
                          .Lote = xe.Element("Lote").Value,
                          .Ubicacion = xe.Element("Ubicacion").Value
                            }
            For Each elementos In qx2
                ContadorLineas = ContadorLineas + 1
                nuevaLinea = New Detalle
                nuevaLinea.Cantidad = elementos.Cantidad
                If nuevaLinea.Cantidad <= 0 Then
                    Return DevuelveError(Cabeceraa.Pedido, "Cantidad no puede ser menor o igual que 0", 5012, "ConfirmacionEntradaInventario")
                End If
                nuevaLinea.LineaPedido = elementos.LineaPedido
                nuevaLinea.CodigoProducto = elementos.CodigoProducto
                nuevaLinea.Cantidad = elementos.Cantidad
                nuevaLinea.Lote = elementos.Lote
                nuevaLinea.Ubicacion = elementos.Ubicacion
                Lineas.Add(nuevaLinea)

                Try

                    Dim fSql As String
                    fSql = "SELECT * FROM RDCC INNER JOIN ECL ON DPEDID=LORD AND DLINEA=LLINE AND DPROD=LPROD"
                    fSql = fSql & " WHERE  DPEDID = " & Cabeceraa.Pedido & " AND DLINEA = " & nuevaLinea.LineaPedido
                    fSql = fSql & " AND DPROD = '" & nuevaLinea.CodigoProducto & "' "
                    rs = db.ExecuteSQL(fSql)
                    If Not rs.EOF Then
                        If bPrimera Then
                            db.wrtLogWsCDA(C_ID, XML, "Pedido-" & rs("DCONSO").Value & "-" & Cabeceraa.Pedido, "", "ConfirmacionEntradaInventario", "wsBrinsa", "EM")
                            bPrimera = False
                        End If
                        If rs("LRQTY").Value = 0 Then
                            rs.Close()
                        Else
                            rs.Close()
                            Return DevuelveError(Cabeceraa.Pedido, " La linea No " & nuevaLinea.LineaPedido & " Producto " & nuevaLinea.CodigoProducto & " Ya fue recibida ", "5001", "ConfirmacionEntradaInventario")
                        End If
                    Else
                        rs.Close()
                        Return DevuelveError(Cabeceraa.Pedido, " Pedido " & Cabeceraa.Pedido & " Linea " & nuevaLinea.LineaPedido & " Producto " & nuevaLinea.CodigoProducto & " No existe ", "5002", "ConfirmacionEntradaInventario")
                    End If
                Catch
                    Return DevuelveError("000", "Error No se pudo procesar algunas consultas", "5003", "ConfirmacionEntradaInventario")
                End Try
            Next



            Dim qx3 = From xe In documento.Descendants.Elements("Linea")
Select New With { _
              .LineaPedido = xe.Element("LineaPedido").Value,
              .CodigoProducto = xe.Element("CodigoProducto").Value,
              .Cantidad = xe.Element("Cantidad").Value,
              .Lote = xe.Element("Lote").Value,
              .Ubicacion = xe.Element("Ubicacion").Value
                }
            For Each elementos In qx3
                ContadorLineas = ContadorLineas + 1
                nuevaLinea = New Detalle
                nuevaLinea.Cantidad = elementos.Cantidad
                nuevaLinea.LineaPedido = elementos.LineaPedido
                nuevaLinea.CodigoProducto = elementos.CodigoProducto
                nuevaLinea.Cantidad = elementos.Cantidad
                nuevaLinea.Lote = elementos.Lote
                nuevaLinea.Ubicacion = elementos.Ubicacion

                Lineas.Add(nuevaLinea)

                Try

                    Dim fSql As String
                    Dim vSql As String
                    fSql = "SELECT * FROM RDCC INNER JOIN ECL ON DPEDID=LORD AND DLINEA=LLINE AND DPROD=LPROD"
                    fSql = fSql & " WHERE  DPEDID = " & Cabeceraa.Pedido & " AND DLINEA = " & nuevaLinea.LineaPedido
                    fSql = fSql & " AND DPROD = '" & nuevaLinea.CodigoProducto & "' "
                    rs = db.ExecuteSQL(fSql)
                    Dim THADNV As String
                    THADNV = db.CalcConsec("TRANSEC", "99999999")
                    If nuevaLinea.Ubicacion = "QM" Or nuevaLinea.Ubicacion = "PT" Or nuevaLinea.Ubicacion = "SAL" Then
                        fSql = " INSERT INTO PRDFTRIN( "
                        vSql = " VALUES ( "
                        fSql = fSql & " PRDTNV    , " : vSql = vSql & " " & sql.DB2_DATEDEC() & ", "                           '//8S0   Fecha Novedad
                        fSql = fSql & " PRTMNV    , " : vSql = vSql & " " & sql.DB2_TIMEDEC0() & ", "                          '//6S0   Hora Novedad
                        fSql = fSql & " PRUSR     , " : vSql = vSql & "'" & rs("LTOWH").Value.ToString & "', "                           '//10A   Usuario
                        fSql = fSql & " PRPRID    , " : vSql = vSql & "'" & "SAT" & "', "                                '//3A    Identificación Producto
                        fSql = fSql & " PRACTL    , " : vSql = vSql & "'" & "Post" & "', "                               '//10A   Tipo de Actualizacion
                        fSql = fSql & " TTYPE     , " : vSql = vSql & "'" & "H" & "', "                                  '//2A    Transaction Type
                        fSql = fSql & " TPROD     , " : vSql = vSql & "'" & rs("DPROD").Value.ToString & "', "                          '//35O   Item Number
                        fSql = fSql & " TLOT      , " : vSql = vSql & " (SELECT IFNULL( max(LLOT), '' ) FROM RILNL01 WHERE LPROD = '" & rs("DPROD").Value.ToString & "' ), "  '//25O   Lot Number
                        fSql = fSql & " TWHS      , " : vSql = vSql & "'" & rs("LTOWH").Value.ToString & "', "                          '//3A    Warehouse
                        fSql = fSql & " TLOCT     , " : vSql = vSql & " (SELECT ILOC FROM IIM WHERE IPROD = '" & rs("DPROD").Value.ToString & "' ), "  '//10O   Location
                        fSql = fSql & " TQTY      , " : vSql = vSql & " " & nuevaLinea.Cantidad & ", "                       '//11P3  Transaction Quantity
                        fSql = fSql & " TREF      , " : vSql = vSql & " " & rs("DPEDID").Value.ToString & ", "                       '//8P0   Order Number
                        fSql = fSql & " THLIN     , " : vSql = vSql & " " & rs("DLINEA").Value.ToString & ", "                       '//4P0   Order Line Number
                        fSql = fSql & " TTDTE     , " : vSql = vSql & " " & sql.DB2_DATEDEC() & ", "                       '//8P0   Transaction Date
                        fSql = fSql & " THADVN    ) " : vSql = vSql & "'" & THADNV & "' ) "       '//15O   Advice Note Number
                        db.ExecuteSQL(fSql & vSql)
                    End If
                    rs.Close()
                Catch
                    Return DevuelveError("000", "Error No se pudo procesar algunas consultas consultas", "5003", "ConfirmacionEntradaInventario")
                End Try
            Next
            Return DevuelveResultado(Cabeceraa.Pedido, "ConfirmacionEntradaInventario")
        Catch

            Return DevuelveError("000", "Error Estructura  XML ", "5011", "ConfirmacionEntradaInventario")
        End Try
    End Function
    <WebMethod()> _
    Public Function PedidoDespachado(ByVal XML As String) As XmlDocument
        db.abrirConexion(My.Settings.AMBIENTE, "AS400")
        C_ID = db.CalcConsec("RFLOGWSCDA", "99999999")
        db.wrtLogWsCDA(C_ID, XML, "Pedido", "", "PedidoDespachado", "wsBrinsa", "EM")
        ContadorLineas = 0
        Dim nuevaLinea As DetallePedidoDespachado
        Dim Cabeceraa As New CabeceraPedidoDespachado
        Dim Lineas As New List(Of DetallePedidoDespachado)
        Dim documento As XDocument = XDocument.Parse(XML)
        'Dim fSql As String
        '      Dim DB1 As New ExecuteSQL
        Dim qx = From xee In documento.Elements("PedidoDespachado")
       Select New With { _
                .Usuario = xee.Element("Usuario").Value,
                .Password = xee.Element("Password").Value,
                .Pedido = xee.Element("Pedido").Value
                  }
        Try
            For Each elements In qx
                Cabeceraa = New CabeceraPedidoDespachado
                Cabeceraa.Usuario = elements.Usuario
                Cabeceraa.Password = elements.Password
                Cabeceraa.Pedido = elements.Pedido

            Next
            db.wrtLogWsCDA(C_ID, XML, "Pedido-" & Cabeceraa.Pedido, "", "PedidoDespachado", "wsBrinsa", "EM")
            fSql = "SELECT UPASS FROM RCAU WHERE UUSR = '" & Cabeceraa.Usuario & "'"
            rs = db.ExecuteSQL(fSql)
            If Not rs.EOF Then
                If Not rs("UPASS").Value = Cabeceraa.Password Then
                    Return DevuelveError(Cabeceraa.Pedido, "Password Incorrecto", "5050", "PedidoDespachado")
                End If
            Else
                Return DevuelveError(Cabeceraa.Pedido, "Usuario Incorrecto ", "5051", "PedidoDespachado")
            End If

        Catch
            Return DevuelveError("000", "Error Estructura  XML ", "5011", "PedidoDespachado")
        End Try

        Try

            Dim qx2 = From xe In documento.Descendants.Elements("Linea")
        Select New With { _
                          .LineaPedido = xe.Element("LineaPedido").Value,
                          .Producto = xe.Element("Producto").Value,
                          .Lote = xe.Element("Lote").Value,
                          .Cantidad = xe.Element("Cantidad").Value
                            }
            For Each elementos In qx2
                ContadorLineas = ContadorLineas + 1
                nuevaLinea = New DetallePedidoDespachado
                nuevaLinea.Cantidad = elementos.Cantidad
                If nuevaLinea.Cantidad <= 0 Then
                    Return DevuelveError("000", "Cantidad no puede ser menor o igual que 0", 106, "PedidoDespachado")
                End If
                nuevaLinea.LineaPedido = elementos.LineaPedido
                nuevaLinea.Producto = elementos.Producto
                nuevaLinea.Lote = elementos.Lote
                nuevaLinea.Cantidad = elementos.Cantidad
                Lineas.Add(nuevaLinea)
            Next
            ' Return DevuelveResultado(Cabeceraa.Pedido, "PedidoDespachado")
        Catch

            Return DevuelveError("000", "Error Estructura  XML ", "5011", "PedidoDespachado")
        End Try



        Try

            Dim qx3 = From xe In documento.Descendants.Elements("Linea")
        Select New With { _
                          .LineaPedido = xe.Element("LineaPedido").Value,
                          .Producto = xe.Element("Producto").Value,
                          .Lote = xe.Element("Producto").Value,
                          .Cantidad = xe.Element("Cantidad").Value
                            }
            For Each elementos In qx3
                ContadorLineas = ContadorLineas + 1
                nuevaLinea = New DetallePedidoDespachado
                nuevaLinea.Cantidad = elementos.Cantidad
                nuevaLinea.LineaPedido = elementos.LineaPedido
                nuevaLinea.Producto = elementos.Producto
                nuevaLinea.Lote = elementos.Lote
                nuevaLinea.Cantidad = elementos.Cantidad
                Lineas.Add(nuevaLinea)

                fSql = "SELECT * FROM RDCC"
                fSql = fSql & " WHERE DPEDID = " & Cabeceraa.Pedido & " AND DLINEA = " & nuevaLinea.LineaPedido & " AND DPROD = '" & nuevaLinea.Producto & "' "
                rs = db.ExecuteSQL(fSql)
                If Not rs.EOF Then
                    If rs("DCANTE").Value = 0 Then
                        fSql = "UPDATE RDCC SET DCANTE =" & nuevaLinea.Cantidad
                        fSql = fSql & " WHERE DPEDID = " & Cabeceraa.Pedido & " AND DLINEA = " & nuevaLinea.LineaPedido & " AND DPROD = '" & nuevaLinea.Producto & "' "
                        db.ExecuteSQL(fSql)
                        rs.Close()
                    Else
                        rs.Close()
                        Return DevuelveError(Cabeceraa.Pedido, "Linea No" & nuevaLinea.LineaPedido & " ya Despachada", "5006", "ConfirmacionEntradaInventario")
                    End If
                Else
                    rs.Close()
                    Return DevuelveError(Cabeceraa.Pedido, "Linea No " & nuevaLinea.LineaPedido & " no existe", "5007", "ConfirmacionEntradaInventario")
                End If
                '                Si(DCANTE = 0)
                'UPDATE RDCC SET DCANTE = <Cantidad> 
                'WHERE DPEDID = <Pedido> AND DLINEA = <LineaPedido> 
                '	Sino
                '		Error
                'Sino esta
                '	Error
            Next
            Return DevuelveResultado(Cabeceraa.Pedido, "PedidoDespachado")
        Catch

            Return DevuelveError("000", "Error Estructura  XML ", "5011", "PedidoDespachado")
        End Try

    End Function
    <WebMethod()> _
    Public Function ReporteDespacho(ByVal XML As String) As XmlDocument
        db.abrirConexion(My.Settings.AMBIENTE, "AS400")
        C_ID = db.CalcConsec("RFLOGWSCDA", "99999999")
        db.wrtLogWsCDA(C_ID, XML, "Consolidado", "", "ReporteDespacho", "wsBrinsa", "EM")
        ContadorLineas = 0
        Dim Cabeceraa As New DetalleReporteDespacho
        Dim documento As XDocument = XDocument.Parse(XML)
        'Dim fSql As String
        '      Dim DB1 As New ExecuteSQL
        Dim qx = From xee In documento.Elements("ReporteDespacho")
       Select New With { _
                .Usuario = xee.Element("Usuario").Value,
                .Password = xee.Element("Password").Value,
                .NumeroUnico = xee.Element("NumeroUnico").Value,
                .Consolidado = xee.Element("Consolidado").Value,
                .EstadoDespacho = xee.Element("EstadoDespacho").Value,
                .FechaReporte = xee.Element("FechaReporte").Value,
                .Manifiesto = xee.Element("Manifiesto").Value,
                .ReporteGeneral = xee.Element("ReporteGeneral").Value
                  }
        Try
            For Each elements In qx
                Cabeceraa = New DetalleReporteDespacho
                Cabeceraa.Usuario = elements.Usuario
                Cabeceraa.Password = elements.Password
                Cabeceraa.NumeroUnico = elements.NumeroUnico
                Cabeceraa.Consolidado = elements.Consolidado
                If elements.EstadoDespacho = 60 Then
                    fSql = "  SELECT * FROM RECC WHERE ECONSO = '" & Cabeceraa.Consolidado & "' AND YEAR(ELLECLI) = 1 AND YEAR(EFINENT) = 1"
                    rs = db.ExecuteSQL(fSql)
                    If Not rs.EOF Then
                        Return DevuelveError(Cabeceraa.Consolidado, "Consolidado no ha sido totalmente despachado", "5014", "ReporteDespacho")
                    End If
                    Cabeceraa.EstadoDespacho = 6
                Else
                    Cabeceraa.EstadoDespacho = elements.EstadoDespacho
                End If


                db.wrtLogWsCDA(C_ID, XML, "Consolidado-" & Cabeceraa.Consolidado, "", "ReporteDespacho", "wsBrinsa", "EM")
                Cabeceraa.FechaReporte = elements.FechaReporte
                Cabeceraa.Manifiesto = elements.Manifiesto
                Cabeceraa.ReporteGeneral = elements.ReporteGeneral


                fSql = "SELECT UPASS FROM RCAU WHERE UUSR = '" & Cabeceraa.Usuario & "'"
                rs = db.ExecuteSQL(fSql)
                If Not rs.EOF Then
                    If Not rs("UPASS").Value = Cabeceraa.Password Then
                        Return DevuelveError(Cabeceraa.Consolidado, "Password Incorrecto", "5050", "ReporteDespacho")
                    End If
                Else
                    '         Return DevuelveError(Cabeceraa.Consolidado, "Usuario Incorrecto ", "5051", "ReporteDespacho")
                End If
                rs.Close()

                Dim Fechas As Date = Convert.ToDateTime(Cabeceraa.FechaReporte)
                Cabeceraa.FechaReporte = Fechas.ToString("yyyy-MM-dd-HH.mm.ss.000000")

                fSql = "SELECT * FROM RHCC WHERE HCONSO ='" & Cabeceraa.Consolidado & "'"
                rs = db.ExecuteSQL(fSql)
                If Not rs.EOF Then

                    fSql = "UPDATE RHCC SET HESTAD =" & Cabeceraa.EstadoDespacho & ", HFESTAD='" & Cabeceraa.FechaReporte & "'"
                    fSql = fSql & " WHERE HCONSO = '" & Cabeceraa.Consolidado & "'"
                    db.ExecuteSQL(fSql)
                    rs.Close()
                Else
                    rs.Close()
                    Return DevuelveError(Cabeceraa.Consolidado, "No existe Consolidado", "5009", "ReporteDespacho")
                End If

                fSql = " INSERT INTO RRCC( "
                vSql = " VALUES ( "
                fSql = fSql & " RTREP     , " : vSql = vSql & " 1, "     '//2S0   Tipo Rep
                fSql = fSql & " RCONSO    , " : vSql = vSql & "'" & Cabeceraa.Consolidado & "', "     '//6A    Consolidado
                fSql = fSql & " RSTSC     , " : vSql = vSql & " (SELECT HESTAD FROM RHCC WHERE HCONSO = '" & Cabeceraa.Consolidado & "'), "     '//2S0   Est Cons
                fSql = fSql & " RREPORT   , " : vSql = vSql & "'" & Cabeceraa.ReporteGeneral & "', "     '//502A  Reporte
                fSql = fSql & " RCRTUSR   , " : vSql = vSql & "'WSCDA', "     '//10A   Usuario
                fSql = fSql & " RAPP      ) " : vSql = vSql & "'WSCDA') "     '//15A   App
                db.ExecuteSQL(fSql & vSql)

            Next
        Catch
            Return DevuelveError("000", "Error Estructura  XML ", "5011", "ReporteDespacho")
        End Try
        Return DevuelveResultado(Cabeceraa.Consolidado, "")
    End Function

    <WebMethod()> _
    Public Function ReporteEntrega(ByVal XML As String) As XmlDocument
        db.abrirConexion(My.Settings.AMBIENTE, "AS400")
        C_ID = db.CalcConsec("RFLOGWSCDA", "99999999")
        db.wrtLogWsCDA(C_ID, XML, "Planilla", "", "ReporteEntrega", "wsBrinsa", "EM")

        ContadorLineas = 0
        Dim Cabeceraa As New DetalleReporteEntrega
        Dim documento As XDocument = XDocument.Parse(XML)
        'Dim fSql As String
        '      Dim DB1 As New ExecuteSQL
        Dim qx = From xee In documento.Elements("ReporteEntrega")
       Select New With { _
                .Usuario = xee.Element("Usuario").Value,
                .Password = xee.Element("Password").Value,
                .Planilla = xee.Element("Planilla").Value,
                .FechaReporte = xee.Element("FechaReporte").Value,
                .EstadoEntrega = xee.Element("EstadoEntrega").Value,
                .Reporte = xee.Element("Reporte").Value
                   }
        Try
            For Each elements In qx
                Cabeceraa = New DetalleReporteEntrega
                Cabeceraa.Usuario = elements.Usuario
                Cabeceraa.Password = elements.Password
                Cabeceraa.Planilla = elements.Planilla
                Cabeceraa.FechaReporte = elements.FechaReporte
                Cabeceraa.EstadoEntrega = elements.EstadoEntrega
                Cabeceraa.Reporte = elements.Reporte

                Dim Fechas As Date = Convert.ToDateTime(Cabeceraa.FechaReporte)
                Cabeceraa.FechaReporte = Fechas.ToString("yyyy-MM-dd-HH.mm.ss.000000")
                db.wrtLogWsCDA(C_ID, XML, "Planilla-" & Cabeceraa.Planilla, "", "ReporteEntrega", "wsBrinsa", "EM")

                fSql = "SELECT UPASS FROM RCAU WHERE UUSR = '" & Cabeceraa.Usuario & "'"
                rs = db.ExecuteSQL(fSql)
                If Not rs.EOF Then
                    If Not rs("UPASS").Value = Cabeceraa.Password Then
                        Return DevuelveError(Cabeceraa.Planilla, "Password Incorrecto", "5050", "ReporteEntrega")
                    End If
                Else
                    Return DevuelveError(Cabeceraa.Planilla, "Usuario Incorrecto ", "5051", "ReporteEntrega")
                End If
                rs.Close()
                fSql = "SELECT * FROM RECC WHERE EPLANI =" & Cabeceraa.Planilla
                rs = db.ExecuteSQL(fSql)
                Dim consos As String
                consos = rs("ECONSO").Value
                If Not rs.EOF Then
                    Select Case Cabeceraa.EstadoEntrega

                        Case 35
                            fSql = "UPDATE RECC SET ESTS =" & Cabeceraa.EstadoEntrega & ", ELLECLI = '" & Cabeceraa.FechaReporte & "'"
                            fSql = fSql & " WHERE EPLANI = " & Cabeceraa.Planilla
                            db.ExecuteSQL(fSql)
                            rs.Close()

                        Case 50

                            fSql = "UPDATE RECC SET ESTS =" & Cabeceraa.EstadoEntrega & ", EFINENT = '" & Cabeceraa.FechaReporte & "'"
                            fSql = fSql & " WHERE EPLANI = " & Cabeceraa.Planilla
                            db.ExecuteSQL(fSql)
                            rs.Close()

                        Case Else

                            fSql = "UPDATE RECC SET ESTS =" & Cabeceraa.EstadoEntrega
                            fSql = fSql & " WHERE EPLANI = " & Cabeceraa.Planilla
                            db.ExecuteSQL(fSql)
                            rs.Close()

                    End Select

                Else
                    rs.Close()
                    Return DevuelveError(Cabeceraa.Planilla, "No existe planilla", "5008", "ReporteEntrega")
                End If

                fSql = " INSERT INTO RRCC( "
                vSql = " VALUES ( "
                fSql = fSql & " RTREP     , " : vSql = vSql & " 1, "     '//2S0   Tipo Rep
                fSql = fSql & " RCONSO    , " : vSql = vSql & "'" & consos & "', "     '//6A    Consolidado
                fSql = fSql & " RPLANI    , " : vSql = vSql & " " & Cabeceraa.Planilla & ", "     '//6S0   Planilla
                fSql = fSql & " RSTSC     , " : vSql = vSql & " (SELECT HESTAD FROM RHCC WHERE HCONSO = '" & consos & "'), "     '//2S0   Est Cons
                fSql = fSql & " RREPORT   , " : vSql = vSql & "'" & Cabeceraa.Reporte & "', "     '//502A  Reporte
                fSql = fSql & " RCRTUSR   , " : vSql = vSql & "'WSCDA', "     '//10A   Usuario
                fSql = fSql & " RAPP      ) " : vSql = vSql & "'WSCDA') "     '//15A   App
                db.ExecuteSQL(fSql & vSql)

            Next
        Catch
            Return DevuelveError("000", "Error Estructura  XML ", "5011", "ReporteEntrega")
        End Try
        Return DevuelveResultado(Cabeceraa.Planilla, "ReporteEntrega")
    End Function

    <WebMethod()> _
    Public Function Despacho(ByVal XML As String) As XmlDocument
        Dim Conso As New Consolidado
        Dim numConso As String

        db.abrirConexion(My.Settings.AMBIENTE, "AS400")
        C_ID = db.CalcConsec("RFLOGWSCDA", "99999999")
        db.wrtLogWsCDA(C_ID, XML, "Inicio", "", "Despacho", "wsBrinsa", "EM")
        ContadorLineas = 0
        Dim nuevaLinea As DetalleDespacho
        Dim Cabeceraa As New CabeceraDespacho
        Dim Lineas As New List(Of DetalleDespacho)
        Dim documento As XDocument = XDocument.Parse(XML)
        'Dim fSql As String
        '      Dim DB1 As New ExecuteSQL
        Dim qx = From xee In documento.Elements("Despacho")
       Select New With { _
        .Usuario = xee.Element("Usuario").Value,
        .Password = xee.Element("Password").Value,
        .Manifiesto = xee.Element("Manifiesto").Value,
        .Bodega = xee.Element("Bodega").Value,
        .TipoVehiculo = xee.Element("TipoVehiculo").Value,
        .Notas = xee.Element("Notas").Value,
        .DescTipoVehiculo = xee.Element("DescTipoVehiculo").Value,
        .Placa = xee.Element("Placa").Value,
        .CedulaConductor = xee.Element("CedulaConductor").Value,
        .NombreConductor = xee.Element("NombreConductor").Value,
        .CelularConductor = xee.Element("CelularConductor").Value
                  }
        Try
            For Each elements In qx
                Cabeceraa = New CabeceraDespacho

                Cabeceraa.Usuario = elements.Usuario
                Cabeceraa.Password = elements.Password
                Cabeceraa.Manifiesto = elements.Manifiesto
                Cabeceraa.Bodega = elements.Bodega
                Cabeceraa.TipoVehiculo = elements.TipoVehiculo
                Cabeceraa.Notas = elements.Notas
                Cabeceraa.DescTipoVehiculo = elements.DescTipoVehiculo
                Cabeceraa.Placa = elements.Placa
                Cabeceraa.CedulaConductor = elements.CedulaConductor
                Cabeceraa.NombreConductor = elements.NombreConductor
                Cabeceraa.CelularConductor = elements.CelularConductor
            Next
            fSql = "SELECT UPASS FROM RCAU WHERE UUSR = '" & Cabeceraa.Usuario & "'"
            rs = db.ExecuteSQL(fSql)
            If Not rs.EOF Then
                If Not rs("UPASS").Value = Cabeceraa.Password Then
                    Return DevuelveError(Cabeceraa.Manifiesto, "Password Incorrecto", "5050", "Despacho")
                End If
            Else
                Return DevuelveError(Cabeceraa.Manifiesto, "Usuario Incorrecto ", "5051", "Despacho")
            End If
            rs.Close()
        Catch
            Return DevuelveError("000", "Error Estructura  XML ", "5011", "Despacho")
        End Try

        db.wrtLogWsCDA(C_ID, XML, "Manifiesto-" & Cabeceraa.Manifiesto, "", "Despacho", "wsBrinsa", "EM")
        Try
            Dim qx2 = From xe In documento.Descendants.Elements("PedidoDespachado")
        Select New With { _
                          .Pedido = xe.Element("Pedido").Value
                            }
            For Each elementos In qx2
                ContadorLineas = ContadorLineas + 1
                nuevaLinea = New DetalleDespacho
                nuevaLinea.Pedido = elementos.Pedido
                Lineas.Add(nuevaLinea)

                fSql = "SELECT * FROM RDCC WHERE DPEDID = " & nuevaLinea.Pedido & ""
                rs = db.ExecuteSQL(fSql)
                If Not rs.EOF Then
                    If rs("DCONSO").Value <> "" Then
                        rs.Close()
                        Return DevuelveError(Cabeceraa.Manifiesto, "Pedido ya Consolidado", "5004", "Despacho")
                    Else
                    End If
                    rs.Close()
                Else
                    rs.Close()
                    Return DevuelveError(Cabeceraa.Manifiesto, "Pedido no Existe", "5005", "Despacho")
                End If
            Next

            fSql = "SELECT CCCODE FROM ZCC WHERE CCTABL = 'SPTIPVEH' AND CCENUS = '" & Cabeceraa.TipoVehiculo & "'"
            rs = db.ExecuteSQL(fSql)
            If rs.EOF Then
                Return DevuelveError(Cabeceraa.Manifiesto, "Tipo de Vehiculo erroneo", "5010", "Despacho")
            End If
            rs.Close()
        Catch

            Return DevuelveError("000", "Error Estructura  XML ", "5011", "Despacho")
        End Try


        Try
            Conso.setDB(db)

            Dim qx3 = From xe In documento.Descendants.Elements("PedidoDespachado")
        Select New With { _
                          .Pedido = xe.Element("Pedido").Value
                            }
            For Each elementos In qx3
                ContadorLineas = ContadorLineas + 1
                nuevaLinea = New DetalleDespacho
                nuevaLinea.Pedido = elementos.Pedido
                Lineas.Add(nuevaLinea)
                Conso.setPedido(nuevaLinea.Pedido)
            Next

            Conso.setBodega(Cabeceraa.Bodega)
            Conso.setProv(5013)
            Conso.setPlaca(Cabeceraa.Placa)
            Conso.setTipoVeh(Cabeceraa.TipoVehiculo)
            Conso.setConductor(Cabeceraa.CedulaConductor, Cabeceraa.NombreConductor, Cabeceraa.CelularConductor)
            Conso.setManifiesto(Cabeceraa.Manifiesto)
            Conso.setObserv(Cabeceraa.Notas)
            numConso = Conso.generaConso()
            If numConso <> "" Then
                Return DevuelveResultado(Cabeceraa.Manifiesto, "Despacho", numConso)
            Else
                Return DevuelveError(Cabeceraa.Manifiesto, "Consolidado No existe", "5013", "Despacho")
            End If
        Catch
            Return DevuelveError("000", "Error Estructura  XML ", "5011", "Despacho")
        End Try


    End Function
    Function DevuelveError(ByVal NumeroDocumento As String, ByVal ERROOR As String, ByVal CODIGO As String, ByVal proceso As String, Optional ByVal Consolidado As String = "") As XmlDocument
        Dim xmlstr As String = ""
        Dim sw As New StringWriter()
        Dim writer As New XmlTextWriter(sw)
        Dim flag As Integer = 0
        Dim flag2 As Integer = 0

        Dim rs As New ADODB.Recordset

        writer.Formatting = Formatting.Indented
        writer.Indentation = 2
        writer.WriteStartElement("Respuesta")
        writer.WriteStartElement("Operacion")
        writer.WriteString(proceso)
        writer.WriteEndElement()
        Select Case proceso
            Case "Despacho"
                writer.WriteStartElement("Consolidado")
                writer.WriteString(Consolidado)
                writer.WriteEndElement()
                writer.WriteStartElement("Manifiesto")
            Case "ReporteDespacho"
                writer.WriteStartElement("Consolidado")
            Case "ReporteEntrega"
                writer.WriteStartElement("Pedido")
            Case Else
                writer.WriteStartElement("Planilla")
        End Select
        writer.WriteString(NumeroDocumento)
        writer.WriteEndElement()
        If (proceso <> "ConfirmacionEntradaInventario" And proceso <> "PedidoDespachado" And proceso <> "ReporteDespacho" And proceso <> "ReporteEntrega" And proceso <> "Despacho") Then
            writer.WriteStartElement("TotalLineas")
            writer.WriteString(ContadorLineas)
            writer.WriteEndElement()
        End If
        writer.WriteStartElement("Mensaje")
        writer.WriteString(CODIGO)
        writer.WriteEndElement()
        writer.WriteStartElement("Mensajetexto")
        writer.WriteString(ERROOR)
        writer.WriteEndElement()


        writer.WriteEndElement()

        writer.Flush()
        xmlstr = sw.ToString
        writer.Close()
        Dim pDoc As New XmlDocument
        pDoc.LoadXml(xmlstr)
        db.wrtLogWsCDA(C_ID, sw.ToString, "", "Fail", proceso, "wsBrinsa", "EM")

        Return pDoc

    End Function

    Function CrearNodoError(ByVal ErrorXML As String, ByVal codigo As String, ByVal NumeroDocumento As String, ByVal proceso As String, ByVal writer As XmlTextWriter)


        writer.WriteStartElement(proceso) ' ENVELOPE
        writer.WriteStartElement("Documento")  ' PEDIDO
        writer.WriteString(NumeroDocumento)
        writer.WriteStartElement("Mensaje")     'MENSAJE
        writer.WriteStartElement("Error")       'ERROR
        writer.WriteStartElement("MensajeID")   'ID
        writer.WriteString(codigo)
        writer.WriteEndElement() ' MensajeId
        writer.WriteStartElement("MensajeDES")
        writer.WriteString(ErrorXML)
        writer.WriteEndElement() ' DES
        writer.WriteEndElement() ' ERROR
        writer.WriteEndElement() ' MENSAJE
        writer.WriteEndElement() ' PEDIDO
        writer.WriteEndElement() ' ENVELOPE
        Return 0
    End Function

    Function DevuelveResultado(ByVal NumeroDocumento As String, ByVal proceso As String, Optional ByVal Consolidado As String = "") As XmlDocument
        Dim xmlstr As String = ""
        Dim sw As New StringWriter()
        Dim writer As New XmlTextWriter(sw)
        Dim flag As Integer = 0
        Dim flag2 As Integer = 0

        Dim rs As New ADODB.Recordset

        'writer.WriteStartDocument(True)
        writer.Formatting = Formatting.Indented
        writer.Indentation = 2
        writer.WriteStartElement("Respuesta")
        writer.WriteStartElement("Operacion")
        writer.WriteString(proceso)
        writer.WriteEndElement()
        Select Case proceso
            Case "Despacho"
                writer.WriteStartElement("Consolidado")
                writer.WriteString(Consolidado)
                writer.WriteEndElement()
                writer.WriteStartElement("Manifiesto")
            Case "ReporteDespacho"
                writer.WriteStartElement("Consolidado")
            Case "ReporteEntrega"
                writer.WriteStartElement("Pedido")
            Case Else
                writer.WriteStartElement("Planilla")
        End Select
        writer.WriteString(NumeroDocumento)
        writer.WriteEndElement()
        If (proceso <> "ConfirmacionEntradaInventario" And proceso <> "PedidoDespachado" And proceso <> "ReporteDespacho" And proceso <> "ReporteEntrega" And proceso <> "Despacho") Then
            writer.WriteStartElement("TotalLineas")
            writer.WriteString(ContadorLineas)
            writer.WriteEndElement()
        End If
        writer.WriteStartElement("Mensaje")
        writer.WriteString("0000")
        writer.WriteEndElement()
        writer.WriteStartElement("Mensajetexto")
        writer.WriteString("Operación Exitosa")
        writer.WriteEndElement()
        writer.WriteEndElement()

        writer.Flush()
        xmlstr = sw.ToString
        writer.Close()
        Dim pDoc As New XmlDocument
        pDoc.LoadXml(xmlstr)
        db.wrtLogWsCDA(C_ID, sw.ToString, "", "Pass", proceso, "wsBrinsa", "EM")

        Return pDoc

    End Function

    Function RevisaUsuario(sUsr As String, sPass As String) As Boolean
        Dim Result As Boolean
        fSql = "SELECT UPASS FROM RCAU WHERE UUSR = '" & sUsr & "' AND UPASS = '" & sPass & "'"
        rs = db.ExecuteSQL(fSql)
        Result = Not rs.EOF
        rs.Close()
        Return Result
    End Function
End Class