﻿Module Aplicacion
    Public Const APP_NAME As String = "FacturaPDF"
    Public APP_PATH As String
    Public APP_VERSION As String

    Public Factura_Pref As String
    Public Factura_Num As String
    Public Factura_ID As String
    Public logDocNum As String
    Public PDF_Folder As String
    Public LOG_File As String

    Public Impresora_PDF As String
    Public PDFEXE As String
    Public Idioma As String
    Public Factura_Marca As String
    Public IAL_Prt As Integer
    Public Flag As Integer
    Public aPermisos() As Boolean
    Public Const ACC_TOTAL As Integer = 21

    Public bPRUEBAS As Boolean

    Public qConso As String
    Public qPedid As String
    Public qPlanilla As String
    Public qCliente As String
    Public qBodega As String
    Public qAPCHK01 As String
    Public qAPCHK02 As String
    Public qAFLAG01 As Integer

    Public aMarca() As String

    Function PDFPrtExe() As String
        PDFPrtExe = """E:\CLPrint\CLPrint.exe""{sp}/print{sp}/copies:{copias}{sp}/printer:""{printer}""{sp}/pdffile:""{doc}"""
    End Function

    Function RevisaPDFs() As Boolean
        Dim i As Integer
        Dim ArchivoPDF As String

        For i = 0 To 2
            If aMarca(i) <> "" Then
                ArchivoPDF = PDF_Folder & "\" & Factura_ID & "_" & aMarca(i) & ".pdf"
                If Dir(ArchivoPDF) = "" Then
                    Return True
                End If
            End If
        Next
        Return False

    End Function

    Sub Imprimir(qImpresora As String)
        Dim rs As New ADODB.Recordset
        Dim qPrinter As String
        Dim i As Integer
        Dim ArchivoPDF As String
        Dim comando As String

        If qImpresora <> "" Then
            fSql = "SELECT CCNOT1, CCUDC1 FROM ZCC WHERE CCTABL ='PRTFACTU' AND CCCODE = '" & qImpresora & "' AND CCUDC2 = 1"
            logDocsAutoSQL(fSql)
            If DB.OpenRS(rs, fSql) Then
                For i = 0 To 2
                    'If Not bPRUEBAS Then
                    If aMarca(i) <> "" Then
                        If Impresora_PDF = "" Then Impresora_PDF = CStr(rs("CCNOT1").Value)
                        ArchivoPDF = PDF_Folder & "\" & Factura_ID & "_" & aMarca(i) & ".pdf"
                        WrtTxtError(LOG_File, "Imprime " & ArchivoPDF)
                        'LogConso qConso, Now(), "Imprime Factura" & Factura_ID
                        comando = Replace(PDFEXE, "{printer}", Impresora_PDF)
                        comando = Replace(comando, "{copias}", "1")
                        comando = Replace(comando, "{doc}", ArchivoPDF)
                        comando = Replace(comando, "{sp}", " ")
                        WrtTxtError(LOG_File, comando)
                        Shell(comando, AppWinStyle.Hide, True, 300000) '5 minutos
                    End If
                Next
            Else
                WrtTxtError(LOG_File, "No econtró la impresora " & qImpresora)
            End If
            rs.Close()
            rs = Nothing
            Exit Sub
        End If


        'IMPRESION
        qPrinter = "PRINTER" & qBodega
        If qAPCHK01 = "S" Or qAPCHK02 = "S" Then
            fSql = "SELECT CCNOT1, CCUDC1 FROM ZCC WHERE CCTABL ='PDFFACTU' AND CCCODE = '" & qPrinter & "' AND CCUDC2 = 1"
            If DB.OpenRS(rs, fSql) Then
                If qAFLAG01 = 0 Then
                    LogConso(qConso, Now(), "Factura Impresa " & Factura_Pref & Factura_Num, "")
                    fSql = "UPDATE RFFACCAB SET FAFLAG01 = 1 "
                    fSql = fSql & " WHERE "
                    fSql = fSql & " FPREFIJ = '" & Factura_Pref & "'"            '//2A    Prefijo
                    fSql = fSql & " AND FFACTUR = " & Factura_Num                '//8P0   Factura
                    logDocsAutoSQL(fSql)
                    DB.ExecuteSQL(fSql)
                End If
                For i = 0 To 2
                    If Not bPRUEBAS Then
                        ArchivoPDF = PDF_Folder & "\" & Factura_ID & "_" & aMarca(i) & ".pdf"
                        'WrtTxtError LOG_File, "Imprime " & ArchivoPDF

                        comando = Replace(PDFEXE, "{printer}", CStr(rs("CCNOT1").Value))
                        comando = Replace(comando, "{doc}", ArchivoPDF)
                        comando = Replace(comando, "{sp}", " ")
                        WrtTxtError(LOG_File, comando)
                        Shell(comando)

                        'Shell """C:\Archivos de programa\Foxit Software\Foxit Reader\Foxit Reader.exe"" /t " & ArchivoPDF & " " & CStr(rs("CCNOT1"))
                    End If
                Next
            End If
            rs.Close()
        End If
        rs = Nothing

    End Sub

    Sub EnvioDocsFactu(Factura_Pref As String, Factura_Num As String)
        Dim rsCab As New ADODB.Recordset
        Dim rs As New ADODB.Recordset
        Dim rs2 As New ADODB.Recordset
        Dim sBodegas() As String
        Dim Ruta As String
        Dim Bodegas As String = ""

        Ruta = PDF_Folder & "\" & Trim(Factura_Pref) & "" & Trim(Factura_Num) & "_O.pdf"

        fSql = "SELECT * FROM  ZCC WHERE CCCODE  = '" & Factura_Pref & "' AND CCTABL = 'RDOCFAC' "
        If Not DB.OpenRS(rs2, fSql) Then
            rs.Close()
            Exit Sub
        End If

        If qCliente <> "" Then
            fSql = "SELECT * FROM  RMAILL WHERE LAPP = 'DOCS'  AND LMOD ='FACTURAS' AND LCUST = " & qCliente & " AND LTIPPE = 'PE'"
            If DB.OpenRS(rs, fSql) Then
                If bEnviarAviso(rs, Factura_Pref, Factura_Num) Then
                    sBodegas = Split(rs("LBODS").Value, ",")
                    Bodegas = Replace(rs("LBODS").Value, ",", "','")
                    fSql = "INSERT INTO RMAILS( SMOD, SIDH, SFILE, SREF )"
                    fSql = fSql & " SELECT  LMOD, LID, '" & Ruta & "', '" & Trim(Factura_Pref) & Trim(Factura_Num) & "'"
                    fSql = fSql & " FROM RFFACCAB INNER JOIN"
                    fSql = fSql & " EST INNER JOIN RMAILL ON LAPP = 'DOCS' AND LMOD='FACTURAS' AND TCUST = LCUST  AND TSHIP=LSHIP "
                    fSql = fSql & " ON FCLIENT=TCUST AND FPTOEN=TSHIP"
                    fSql = fSql & " WHERE FPREFIJ = '" & Factura_Pref & "' AND FFACTUR=" & Factura_Num
                    If rs("LBODS").Value <> "*ALL" Then
                        fSql = fSql & " AND FBODEGA IN( '" & Bodegas & "' )"
                    End If
                    logDocsAutoSQL(fSql)
                    DB.ExecuteSQL(fSql)
                End If
            End If
            rs.Close()

            fSql = "SELECT * FROM  RMAILL WHERE LAPP = 'DOCS'  AND LMOD ='FACTURAS' AND LCUST = " & qCliente & " AND LTIPPE <> 'PE'"
            If DB.OpenRS(rs, fSql) Then
                If bEnviarAviso(rs, Factura_Pref, Factura_Num) Then
                    sBodegas = Split(rs("LBODS").Value, ",")
                    fSql = "INSERT INTO RMAILS( SMOD, SIDH, SFILE, SREF )"
                    fSql = fSql & " SELECT  LMOD, LID, '" & Ruta & "', '" & Trim(Factura_Pref) & Trim(Factura_Num) & "'"
                    fSql = fSql & " FROM RFFACCAB INNER JOIN"
                    fSql = fSql & " ( EST INNER JOIN RMAILL ON LAPP = 'DOCS' AND LMOD='FACTURAS' AND TCUST = LCUST  AND STDPT1=LTIPPE )"
                    fSql = fSql & " ON FCLIENT=TCUST AND FPTOEN=TSHIP"
                    fSql = fSql & " WHERE FPREFIJ = '" & Factura_Pref & "' AND FFACTUR=" & Factura_Num & ""
                    If rs("LBODS").Value <> "*ALL" Then
                        fSql = fSql & " AND FBODEGA IN( '" & Bodegas & "' )"
                    End If
                    logDocsAutoSQL(fSql)
                    DB.ExecuteSQL(fSql)
                End If
            End If
        End If

    End Sub

    Function bEnviarAviso(rsM As ADODB.Recordset, Factura_Pref As String, Factura_Num As String) As Boolean
        Dim rs As New ADODB.Recordset
        Dim fSql As String

        fSql = "SELECT * FROM  RMAILS WHERE SMOD ='FACTURAS' AND SIDH = " & rsM("LID").Value & " AND SREF = '" & Factura_Pref.Trim & Factura_Num.Trim & "'"
        logDocsAutoSQL(fSql)
        rs.Open(fSql, DB)
        bEnviarAviso = rs.EOF
        rs.Close()
        rs = Nothing

    End Function


    Sub logDocsAuto(Doc As String, Flag As Integer, Documento As String, qConso As String, qPedid As String, qPlanilla As String, qPref As String, qFactu As String)
        Exit Sub
        '' ''If logDocNum = "" Then
        '' ''    logDocNum = DB.CalcConsec("RFLOGDOCA", "9999999999")
        '' ''    fSql = " INSERT INTO RFLOGDOCA( "
        '' ''    vSql = " VALUES ( "
        '' ''    mkSql(tNum, " DOCID     ", logDocNum, 0)         '//11P0
        '' ''    mkSql(tStr, " DOCPGM    ", AppEXEName(), 0)           '//50O
        '' ''    mkSql(tStr, " WINDAT    ", timeStampWin(), 0)          '//26Z   Fecha Windows
        '' ''    mkSql(tNum, " DOCPLANI  ", Val(qPlanilla), 0)         '//6P0   Planilla
        '' ''    mkSql(tStr, " DOCPREFI  ", qPref, 0)          '//2A    Pref
        '' ''    mkSql(tNum, " DOCFACTU  ", Val(qFactu), 0)         '//8P0
        '' ''    mkSql(tStr, " DOCDOCU   ", Documento, 0)
        '' ''    mkSql(tStr, " DOCDOC    ", Doc, 1)          '//150O
        '' ''    DB.ExecuteSQL(fSql & vSql)
        '' ''Else
        '' ''    fSql = " UPDATE RFLOGDOCA SET "
        '' ''    fSql = fSql & " ENDDAT = NOW(), DOCFLAG = " & Flag & ", "
        '' ''    fSql = fSql & " DOCCONSO  ='" & qConso & "', "      '//6A
        '' ''    fSql = fSql & " DOCPEDID  = " & Val(qPedid) & ", "        '//8P0
        '' ''    fSql = fSql & " DOCPLANI  = " & Val(qPlanilla) & ", "       '//6P0   Planilla
        '' ''    fSql = fSql & " DOCPREFI  ='" & qPref & "', "      '//2A    Pref
        '' ''    fSql = fSql & " DOCFACTU  = " & Val(qFactu)        '//8P0
        '' ''    fSql = fSql & " WHERE DOCID = " & logDocNum
        '' ''    DB.ExecuteSQL(fSql)
        '' ''End If

    End Sub
    Public Sub logDocsAutoSQL(ByVal Sql As String)
        '' ''DB.ExecuteSQL("UPDATE RFLOGDOCA SET DOCSQL = '" & Replace(Sql, "'", "''") & "' WHERE DOCID = " & logDocNum)
    End Sub

    Public Sub LogConso(Conso, Fecha, Reporte, colRRCCT)
        Dim fSql As String
        Dim vSql As String
        Dim rs As New ADODB.Recordset
        Dim campoRRCCT As String

        fSql = "UPDATE RHCC H SET HSTS = ( SELECT VESTAD FROM VSPESTAD WHERE H.HESTAD = CCCODE )"
        fSql = fSql & " WHERE HCONSO = '" & Conso & "'"
        DB.ExecuteSQL(fSql)

        fSql = "INSERT INTO RRCC("
        vSql = "VALUES( "
        fSql = fSql & "RCONSO, " : vSql = vSql & "'" & Conso & "'" & ", "
        fSql = fSql & "RSTSC, "
        vSql = vSql & "( SELECT IFNULL(SUM(HESTAD), 9) FROM RHCC WHERE HCONSO='" & Conso & "' )" & ", "
        fSql = fSql & "RTREP, " : vSql = vSql & " " & "2" & " " & ", "
        fSql = fSql & "RFECREP, " : vSql = vSql & " NOW(), "
        fSql = fSql & "RREPORT, " : vSql = vSql & "'" & Trim(Reporte) & "'" & ", "
        fSql = fSql & "RAPP, " : vSql = vSql & "'" & APP_NAME & "'" & ", "
        fSql = fSql & "RCRTUSR) " : vSql = vSql & "'" & DB.gsUser & "'" & ") "
        DB.ExecuteSQL(fSql & vSql)

        If colRRCCT <> -1 And colRRCCT <> 0 Then
            If colRRCCT = 0 Then
                fSql = "SELECT CCALTC FROM RHCC, VSPESTAD WHERE HESTAD=CCCODE AND HCONSO='" & Conso & "'"
                rs.Open(fSql, DB)
                campoRRCCT = "RFEC" & rs("CCALTC").Value
                rs.Close()
            Else
                campoRRCCT = "RFEC" & Ceros(colRRCCT, 2)
            End If
            fSql = "SELECT RCONSO FROM RRCCT WHERE RCONSO='" & Conso & "'"
            If Not DB.OpenRS(rs, fSql) Then
                DB.ExecuteSQL("INSERT INTO RRCCT( RCONSO ) VALUES('" & Conso & "')")
            End If
            rs.Close()
            fSql = "UPDATE RRCCT SET "
            fSql = fSql & campoRRCCT & " = NOW() "
            fSql = fSql & " WHERE RCONSO='" & Conso & "' AND YEAR(" & campoRRCCT & ")=1"
            DB.ExecuteSQL(fSql)
        End If

        rs = Nothing

    End Sub



End Module
