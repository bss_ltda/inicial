﻿Imports PDF_In_The_BoxCtl
Imports ADODB

Public Class frmFactura
    Dim bPaginaNueva As Boolean
    Dim rsTit As New ADODB.Recordset
    Dim rsCab As New ADODB.Recordset
    Dim rsPed As New ADODB.Recordset
    Dim rsDet As New ADODB.Recordset
    Dim rsNotas As New ADODB.Recordset
    Dim DocScanner As Integer, Observ As String, ImprimeIAL As String
    Dim bColor As Boolean
    Dim bQuimicos As Boolean
    Dim bExport As Boolean
    Dim Sellos() As String
    Dim Fila As Integer
    Dim TotNotas As Integer
    Dim TotFilas As Integer
    Dim Ta As TBoxTable

    Const CELDA_NORMAL As String = "BrushColor = 255, 255, 255"
    'Const CELDA_SOMBRA As String = "BrushColor = 215, 215, 215"
    Const CELDA_SOMBRA As String = "BrushColor = 255, 255, 255"
    Const TABLE_HEADER_STYLE As String = "ChildrenStyle=""BorderStyle=rect;FontSize=9;Fontname=Arial;FontBold=1;Alignment=Center;VertAlignment=Center"""
    Const ESTILO_TIT As String = "Normal;fontsize=6;Fontname=Corbel;BorderColor=Black;Alignment=Center;VertAlignment=Center"


    Private Sub frmFactura_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim parametro As String
        Dim aSubParam() As String
        Dim qPrinter As String = ""
        Dim rs As New ADODB.Recordset
        Dim bGenerar As Boolean
        Dim campoParam As String = ""

        aMarca = Split("O,CC,C", ",")
        Environment.GetCommandLineArgs()
        For Each parametro In Environment.GetCommandLineArgs()
            aSubParam = Split(parametro, "=")
            Select Case UCase(aSubParam(0))
                Case "S"
                    CARPETA_SITIO = Trim(aSubParam(1))
                Case "P"
                    Factura_Pref = Trim(aSubParam(1))
                Case "F"
                    Factura_Num = Ceros(Trim(aSubParam(1)), 8)
                Case "I"
                    qPrinter = aSubParam(1)
                Case "C"
                    If aSubParam(1) <> "" Then
                        aMarca(0) = IIf(Mid(aSubParam(1), 1, 1) = "1", "O", "")
                        aMarca(1) = IIf(Mid(aSubParam(1), 2, 1) = "1", "CC", "")
                        aMarca(2) = IIf(Mid(aSubParam(1), 3, 1) = "1", "C", "")
                    End If
            End Select
        Next

        Factura_ID = Factura_Pref & Factura_Num

        DB.bDateTimeToChar = True
        AbreConexion()

        If My.Settings.LOCAL = "SI" Then
            campoParam = "CCNOT2"
        End If
        CARPETA_SITIO = DB.getLXParam("SITIO_CARPETA", "TM", campoParam)
        CARPETA_IMG = DB.getLXParam("CARPETA_IMG", "TM", campoParam)
        bPRUEBAS = My.Settings.PRUEBAS.ToUpper() = "SI"

        PDF_Folder = CARPETA_IMG & "pdf\" & APP_NAME
        LOG_File = PDF_Folder & "\" & Factura_ID & ".txt"

        If Not DB.CheckVer() Then
            DB.WrtSqlError(DB.lastError, "")
            End
        End If

        PDFEXE = PDFPrtExe()
        'E:\CLPrint\CLPrint.exe" /print /printer:"PBASCULA_LOCAL" /pdffile:"E:\Despachos_DB2\ImagenesTransportes\pdf\FacturaPDF\A00746465_O.pdf
        logDocsAuto("", 0, Factura_Num, qConso, qPedid, qPlanilla, Factura_Pref, Factura_Num)

        bGenerar = True
        If qPrinter <> "GEN" Then
            bGenerar = RevisaPDFs()
        End If

        Try
            GeneraPdf(bGenerar)
        Catch ex As Exception
            DB.WrtSqlError(ex.Message, "GeneraPDF")
            End
        End Try
        logDocsAuto("", 1, Factura_Num, qConso, qPedid, qPlanilla, Factura_Pref, Factura_Num)

        If qPrinter <> "" Then
            If qPrinter <> "GEN" Then
                Imprimir(qPrinter)
            End If
        Else
            Imprimir("")
        End If
        logDocsAuto("", 2, Factura_Num, qConso, qPedid, qPlanilla, Factura_Pref, Factura_Num)

        EnvioDocsFactu(Factura_Pref, Factura_Num)

        DB.Close()

Manejo_Error:
        If Err.Number <> 0 Then
            WrtTxtError(LOG_File, "Main")
            End
        End If

    End Sub

    Public Sub GeneraPdf(bGenerar As Boolean)
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim i As Integer
        Dim ArchivoPDF As String
        Dim jSql As String

        Fila = 0

        DB.DropTable("QTEMP.CAB" & logDocNum)

        ' PARA ENCABEZADO Y PIE DE PAGINA
        fSql = "CREATE TABLE QTEMP.CAB" & logDocNum & " AS ( SELECT * FROM RFFACCAB"
        fSql = fSql & " WHERE "
        fSql = fSql & " FPREFIJ = '" & Factura_Pref & "'"       '//2A    Prefijo
        fSql = fSql & " AND FFACTUR = " & Factura_Num           '//8P0   Factura
        fSql = fSql & " ) WITH DATA "
        DB.ExecuteSQL(fSql)

        fSql = "SELECT * FROM  QTEMP.CAB" & logDocNum
        logDocsAutoSQL(fSql)
        If Not DB.OpenRS(rsCab, fSql, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly) Then
            rsCab.Close()
            WrtTxtError(LOG_File, "Factura no se encontro.")
            Exit Sub
        End If

        qConso = rsCab("FCONSOL").Value
        qPedid = rsCab("FPEDIDO").Value
        qBodega = rsCab("FBODEGA").Value
        qPlanilla = rsCab("FPLANIL").Value
        qCliente = rsCab("FCLIENT").Value
        qAFLAG01 = rsCab("FAFLAG01").Value
        qAPCHK01 = rsCab("FAPCHK01").Value
        qAPCHK02 = rsCab("FAPCHK02").Value

        If Not bGenerar Then
            rsCab.Close()
            rsDet = Nothing
            rsNotas = Nothing
            rsTit = Nothing
            rsCab = Nothing
            Exit Sub
        End If

        fSql = "SELECT * FROM RFPARAM WHERE CCTABL = 'FACTUTIT'"
        logDocsAutoSQL(fSql)

        DB.OpenRS(rsTit, fSql, 3, 3)
        bExport = (rsCab("FMERCADO").Value = "EXPORTACIONES")
        Idioma = IIf(rsCab("FIDIOMA").Value = "I", "ENG", "ESP")

        'tabla DETALLE
        fSql = " SELECT "
        fSql = fSql & " DPREFIJ   , "                       '//2A    Prefijo
        fSql = fSql & " DFACTUR   , "                       '//8P0   Factura
        'fSql = fSql & " VARCHAR(DLINEA) AS DLINEA , "       '//4P0   Linea
        fSql = fSql & " VARCHAR(LINEA) AS DLINEA , "       '//4P0   Linea
        fSql = fSql & " DCODPRO   , "                       '//35A   Codigo Producto
        fSql = fSql & " DDESPRO   , "                       '//50A   Descripcion Producto
        fSql = fSql & " DUNIMED   , "                       '//2A    Unidad Medida
        fSql = fSql & " VARCHAR(DPORIMP) AS DPORIMP   , "   '//5P2   Porc de Impue
        fSql = fSql & " DCANTID   , "                       '//11P2  Cantidad
        fSql = fSql & " DOUBLE( DVALUNI ) AS DVALUNI   , "  '//14P4  Valor Unitario
        fSql = fSql & " DOUBLE( DVALTOT ) AS DVALTOT   , "  '//14P2  Valor Total
        fSql = fSql & " DFPORIMP  , "                       '//6A    Porc de Impue
        fSql = fSql & " DFCANTID  , "                       '//14A   Cantidad
        fSql = fSql & " DFUNIDAD  , "                       '//14A   Unidad
        fSql = fSql & " DFVALUNI  , "                       '//18A   Valor Unitario
        fSql = fSql & " DFVALTOT  , "                       '//18A   Valor Total
        fSql = fSql & " DNOTLIN     "                       '//250A  Notas Linea Pedido
        '    fSql = fSql & " FROM RFFACDET {JOIN} "
        '    fSql = fSql & " WHERE "
        '    fSql = fSql & " DPREFIJ = '" & Factura_Pref & "'"   '//2A    Prefijo
        '    fSql = fSql & " AND DFACTUR = " & Factura_Num       '//8P0   Factura
        '    fSql = fSql & " ORDER BY 3"

        fSql = fSql & " FROM RFFACDET INNER JOIN ( "
        '    fSql = fSql & "     SELECT   IFNULL( MIN( DEC( VWMSLINEA, 6, 0 ) ), MIN(DLINEA) ) AS LINEA, DLINEA AS LINEAFAC "
        fSql = fSql & "     SELECT   IFNULL( MIN( DEC( VWMSLINEA, 6, 0 ) ), MIN(DLINEA) ) AS LINEA, DLINEA AS LINEAFAC "
        fSql = fSql & "     FROM RFFACDET  LEFT OUTER JOIN RDCCEW ON DPEDIDO=VPEDID AND DLINPED = VLINEA    "
        fSql = fSql & "     WHERE  DPREFIJ = '" & Factura_Pref & "' AND DFACTUR = " & Factura_Num
        fSql = fSql & "     GROUP BY DLINEA "
        fSql = fSql & " ) L ON DLINEA = LINEAFAC "
        fSql = fSql & " WHERE  DPREFIJ = '" & Factura_Pref & "' AND DFACTUR = " & Factura_Num
        fSql = fSql & " ORDER BY 3 "

        '    jSql = Replace(fSql, "{JOIN}", " INNER JOIN RDCCEW ON VCONSO = '" & qConso & "' AND DPEDIDO=VPEDID AND DLINPED = VLINEA")
        '    rs.Open jSql, DB
        '    If rs.EOF Then
        jSql = Replace(fSql, "{JOIN}", " LEFT OUTER JOIN RDCCEW ON DPEDIDO=VPEDID AND DLINPED = VLINEA ")
        '    End If
        '    rs.Close

        'Se crea archivo en QTEMP y se llena con el query
        logDocsAutoSQL(fSql)
        DetalleFactura(fSql)
        fSql = "SELECT * FROM QTEMP.FA" & logDocNum & " ORDER BY DORDENL "
        logDocsAutoSQL(fSql)
        If Not DB.OpenRS(rsDet, fSql, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly) Then
            rsTit.Close()
            rsDet.Close()
            rsCab.Close()
            WrtTxtError(LOG_File, "Factura sin detalle.")
            Exit Sub
        End If

        'Crea temporal de notas y totaliza detalle y numero de notas
        NotasFactura()
        fSql = "SELECT CAT, SNSEQ, SNDESC FROM QTEMP." & Trim(Factura_Pref) & Factura_Num & " WHERE SNDESC <> '' ORDER BY CAT, SNSEQ "
        logDocsAutoSQL(fSql)
        DB.OpenRS(rsNotas, fSql, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly)
        For i = 0 To 2
            If aMarca(i) <> "" Then
                Factura_Marca = aMarca(i)
                ArchivoPDF = PDF_Folder & "\" & Factura_ID & "_" & Factura_Marca & ".pdf"
                ArmaPDF(ArchivoPDF)
            End If
        Next

        rsDet.Close()
        rsNotas.Close()
        rsTit.Close()
        rsCab.Close()

    End Sub

    Sub ArmaPDF(ArchivoPDF As String)
        Dim Ta1 As TBoxTable
        Dim Detail As TBoxBand
        Dim Note As TBoxBand
        Dim Header As TBoxBand
        Dim Footer As TBoxBand
        Dim AfterFooter As TBoxBand
        Dim b2 As TBoxBand
        Dim sError As String

        logDocsAutoSQL("Entra ArmaPDF " & Microsoft.VisualBasic.Right(ArchivoPDF, 8))

        If Dir(ArchivoPDF) <> "" Then
            On Error Resume Next
            Kill(ArchivoPDF)
            If Err.Number <> 0 Then
                sError = Err.Description
                WrtTxtError(LOG_File, "ArmaPDF: Borrar " & ArchivoPDF & " " & sError)
                logDocsAutoSQL("ArmaPDF: Borrar " & ArchivoPDF & " " & sError)
                rsDet.Close()
                rsNotas.Close()
                rsTit.Close()
                rsCab.Close()
                rsDet = Nothing
                rsNotas = Nothing
                rsTit = Nothing
                rsCab = Nothing
                DB.Close()
                End
            End If
            On Error Resume Next
        End If

        ' Titulos
        With Me.AxPdfBox1
            .FileName = ArchivoPDF
            .Title = "FACTURA - " & Factura_ID
            .WantShow = bPRUEBAS
            .WantPageCount = True
            .PaperSizeName = "Letter"
            .BeginDoc()
            '.BottomMargin = 50
            'table
            Fila = 0
            Ta = .CreateTable("BorderStyle=None;Margins=130,200,100,150")
            'Set Ta = .CreateTable("BorderStyle=none;Alignment=Center;Margins=113,200,100,150")
            logDocsAutoSQL("Ta.Assign rsDet")
            Ta.Assign(rsDet)

            bPaginaNueva = True
            'Tabla del Detalle
            Detail = Ta.CreateBand()
            Detail.Role = TBandRole.rlDetail
            Detail.ChildrenStyle = "FontSize=9;Fontname=Arial;VertAlignment=Center"
            Detail.Height = 45
            Detail.ChildrenStyle = "FontSize=9;Fontname=Arial; VertAlignment=Center;BorderStyle=LeftRight"
            Detail.CreateCell(170, "BorderStyle=Left").CreateText("Alignment=Left").Bind("DCODPRO")
            Detail.CreateCell(710, "BorderStyle=none").CreateText("Alignment=Left").Bind("DDESPRO")
            Detail.CreateCell(180, "BorderStyle=none").CreateText("Alignment=Right").Bind("DFUNIDAD")
            Detail.CreateCell(120, "BorderStyle=none").CreateText("Alignment=Right").Bind("DPORIMP")
            Detail.CreateCell(150, "BorderStyle=none").CreateText("Alignment=Right").Bind("DFCANTID")
            Detail.CreateCell(100, "BorderStyle=none").CreateText("Alignment=Center").Bind("DUNIMED")
            If Not bExport Then
                'Detail.CreateCell(240, "BorderStyle=none").CreateText("Alignment=Right").Bind ("DFVALUNI")
                
                With Detail.CreateCell(240, "BorderStyle=none").CreateText
                    .Bind("DVALUNI")
                    .Format = "#,##0.00"
                    .Alignment = 1 'haRight
                End With

                'Detail.CreateCell(240, "BorderStyle=Right").CreateText("Alignment=Right;BorderRightMargin=10").Bind ("DFVALTOT")
                With Detail.CreateCell(240, "BorderStyle=Right").CreateText
                    .Bind("DVALTOT")
                    .Format = "#,##0.00"
                    .Alignment = 1 'haRight
                    .BorderRightMargin = 10
                End With
            Else
                With Detail.CreateCell(240, "BorderStyle=none").CreateText
                    .Bind("DVALUNI")
                    .Format = "#,##0.0000"
                    .Alignment = 1 'haRight
                End With

                With Detail.CreateCell(240, "BorderStyle=Right").CreateText
                    .Bind("DVALTOT")
                    .Format = "#,##0.00"
                    .Alignment = 1 'haRight
                    .BorderRightMargin = 10
                End With
            End If
            'NUEVO
            Detail.Breakable = True

            Ta.Put()

            Flag = 1
            b2 = .CreateBand("Normal;BorderStyle=LeftRight;Margins=130,200,100,150")
            b2.CreateCell(1910, "BorderStyle=Top").CreateText().Assign("")
            b2.Put()

            '        Set b2 = .CreateBand("Normal;BorderStyle=LeftRight;Margins=130,200,100,150")
            '        b2.Height = 50
            '        b2.CreateCell(1910, "BorderStyle=LeftRight").CreateText().Assign ""
            '        b2.Put

            Ta1 = .CreateTable("Margins=130,200,100,150;BorderStyle=LeftRight")
            Ta1.Assign(rsNotas)
            Note = Ta1.CreateBand()
            Note.Height = 45
            Note.Role = TBandRole.rlDetail
            Note.ChildrenStyle = "FontSize=9;Fontname=Arial; VertAlignment=Center;BorderStyle=LeftRight"
            Note.CreateCell(1910, "BorderStyle=Left").CreateText("Alignment=Left").Bind("SNDESC")
            Ta1.Put()
            .EndDoc()
            Debug.Print(.FileName())
            ArchivoPDF = .FileName

        End With
        logDocsAutoSQL("Sale ArmaPDF " & Microsoft.VisualBasic.Right(ArchivoPDF, 8))


    End Sub

    Private Sub AxPdfBox1_BeforePutBand(sender As Object, e As PDF_In_The_BoxCtl.IPdfBoxEvents) Handles AxPdfBox1.BeforePutBand

        If e.aBand.Role = 1 Then ' TBandRole.rlDetail  rlDetail = 1
            If bColor And Flag = 0 Then
                e.aBand.BrushColor = RGB(215, 215, 215)
            Else
                e.aBand.BrushColor = RGB(255, 255, 255)
            End If
            bColor = Not bColor
        End If
        Fila = Fila + 1
        If Fila > 28 Then
            AxPdfBox1.NewPage()
            Fila = 0
        End If

    End Sub


    Private Sub AxPdfBox1_OnBottomOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnBottomOfPageEvent) Handles AxPdfBox1.OnBottomOfPage
        Dim b As TBoxBand
        Dim rs As New ADODB.Recordset
        Dim aqui As String = ""
        Dim Pie As String
        Dim bSalto As Boolean

        With AxPdfBox1

            b = .CreateBand("Normal; BorderStyle=bottom;Margins=130,200,100,150")
            b.Breakable = False
            b.CreateCell(1910, "BorderStyle=None").CreateText("Alignment=left").Assign("")
            b.Put()
            If sender.LastPage Then
                If Fila > 16 Then
                    '            If (TotFilas + TotNotas + 2) Mod 29 > 16 Then
                    LinesAdd(29 - Fila)
                    .NewPage()
                    Fila = 0
                    bSalto = True
                End If
                FinDocumento(bSalto)
            End If

            b = .CreateBand("Normal; BorderStyle=None;Margins=130,200,100,150")
            b.Breakable = False
            b.Height = 25
            b.CreateCell(1910, "BorderStyle=None").CreateText("Alignment=left").Assign("")
            b.Put(130, 2390)

            Fila = 0
            Pie = ""
            Pie = Pie & Titulo("PIE02") & vbCrLf
            Pie = Pie & Titulo("PIE03") & vbCrLf
            Pie = Pie & Titulo("PIE04") & vbCrLf
            Pie = Pie & Titulo("PIE05") & vbCrLf
            Pie = Pie & Titulo("PIE06") & vbCrLf

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "BorderStyle=None;Alignment=Center"
            b.CreateCell(1030).CreateText().Assign(Pie)
            b.CreateCell(400).CreateText().Assign("")
            b.CreateCell(480).CreateImage("Alignment=Right;VertAlignment=Top").Assign(APP_PATH + "\abajo_dcha.png")
            b.Put()

            Select Case Factura_Marca
                Case "O"     'Original
                    Factura_Marca = Titulo("PIE07")
                Case "CC"    'Copia Cliente
                    Factura_Marca = Titulo("PIE08")
                Case "C"     'Copia
                    Factura_Marca = Titulo("PIE09")
            End Select

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "BorderStyle=None;Alignment=Center"
            b.CreateCell(900).CreateText("FontName=Arial;FontSize=6;Alignment=Left").Assign("Pag. {%PageNumber}" & IIf(Idioma = "ESP", " de ", " of ") & "{%PageCount}")
            b.CreateCell(530).CreateText("FontName=Arial;FontSize=6;Alignment=Left").Assign("v." & APP_VERSION)
            b.CreateCell(480).CreateText("FontName=Arial;FontSize=6;Alignment=Right").Assign(Factura_Marca)
            b.Put()

            .Put(Titulo("DER01" & Factura_Pref), 2071, 2100, 3800, 2500, "FontDegreeAngle = 90")

        End With

Manejo_Error:
        If Err.Number <> 0 Then
            WrtTxtError(LOG_File, "OnBottomOfPage " & aqui & vbCrLf & Err.Description)
            End
        End If

    End Sub

    Sub FinDocumento(bSalto As Boolean)
        Dim b As TBoxBand
        Dim txt As String
        Dim rs As New ADODB.Recordset
        Dim mUni As String
        Dim mCent As String

        'Monto escrito
        fSql = "SELECT * FROM RFPARAM WHERE CCTABL = 'MONEDA' AND CCCODE = '" & rsCab("FMONEDA").Value & "' AND CCCODE2 = '" & Idioma & "'"
        If DB.OpenRS(rs, fSql) Then
            mUni = rs("CCDESC").Value
            mCent = rs("CCSDSC").Value
        End If
        rs.Close()
        rs = Nothing

        With Me.AxPdfBox1

            If bSalto Then
                Fila = 0
                LinesAdd(17)
            Else
                LinesAdd(29 - Fila)
                Fila = 0
            End If

            b = .CreateBand("Margins=130,200,100,150")
            b.Height = 15
            b.ChildrenStyle = "Normal;fontsize=9;Fontname=Arial"
            b.CreateCell(1910, "BorderStyle=None").CreateText("Alignment=LEFT").Assign("")
            b.Put(130, 1900)

            b = .CreateBand("Normal;BorderStyle=Top;Margins=130,200,100,150")
            b.Breakable = False
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black; BorderStyle= None;VertAlignment=Center"

            If Idioma = "ESP" Then
                b.CreateCell(1180, "BorderStyle=LeftRight").CreateText().Assign(MontoEscrito(rsCab("FTOTFAC").Value, 1, "M", mUni, mCent))
            Else
                b.CreateCell(1180, "BorderStyle=LeftRight").CreateText().Assign(ConvertCurrencyToEnglish(rsCab("FTOTFAC").Value, mUni, mCent))
            End If
            b.CreateCell(390, "BorderStyle=Left").CreateText("Alignment=Right").Assign(Titulo("TIT101"))
            b.CreateCell(340, "BorderStyle=Right;FontSize=9;Alignment=Right").CreateText("BorderRightMargin=10").Assign(CStr(rsCab("FFSUBTOT").Value))
            b.Put()

            b = .CreateBand("Normal;BorderStyle=None;Margins=130,200,100,150")
            b.Breakable = False
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black; BorderStyle=  left;VertAlignment=Center"
            b.CreateCell(1180, "BorderStyle=LeftRight;FontSize=9").CreateText().Assign(" ")
            b.CreateCell(390, "BorderStyle=Left;BrushColor=215,215,215").CreateText("Alignment=Right").Assign(Titulo("TIT102"))
            b.CreateCell(340, "BorderStyle=Right;FontSize=9;BrushColor=215,215,215").CreateText("Alignment=Right;BorderRightMargin=10").Assign(CStr(Format(rsCab("FIMPUES"), "#,##0.00")))
            b.Put()

            b = .CreateBand("Normal;BorderStyle=None;Margins=130,200,100,150")
            b.Breakable = False
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black; BorderStyle=  left;VertAlignment=Center"
            b.CreateCell(1180, "BorderStyle=LeftRight").CreateText("Alignment=left").Assign(" ")
            b.CreateCell(390, "BorderStyle=Left").CreateText("Alignment=Right").Assign(Titulo("TIT103") & " " & CStr(rsCab("FMONEDA").Value))
            b.CreateCell(340, "BorderStyle=Right;FontSize=9").CreateText("Alignment=Right;BorderRightMargin=10").Assign(CStr(rsCab("FFTOTFAC").Value))
            b.Put()
            b = .CreateBand("Normal;BorderStyle=LeftRight;Margins=130,200,100,150")
            b.Breakable = False
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black; BorderStyle=left;VertAlignment=Center"
            b.CreateCell(1180, "BorderStyle=Top").CreateText("Alignment=left;FontBold=1").Assign(Titulo("TIT100") & vbCrLf & Emergencia(qPedid))
            b.CreateCell(390, "BorderStyle=Left;BrushColor=215,215,215").CreateText("Alignment=Right").Assign(Titulo("TIT104"))
            b.CreateCell(340, "BorderStyle=Right;FontSize=9;BrushColor=215,215,215").CreateText("Alignment=Right;BorderRightMargin=10").Assign(CStr(Format(rsCab("FFTOTCAJ").Value, "#,##0.00")))
            b.Put()

            b = .CreateBand("Normal;BorderStyle=None;Margins=130,200,100,150")
            b.Breakable = False
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black; BorderStyle=  left;VertAlignment=Center"
            b.CreateCell(1180, "BorderStyle=LeftRight").CreateText("Alignment=left;FontBold=1").Assign("")
            b.CreateCell(390, "BorderStyle=Left").CreateText("Alignment=Right").Assign(Titulo("TIT105"))
            b.CreateCell(340, "BorderStyle=Right;FontSize=10").CreateText("Alignment=Right;BorderRightMargin=10").Assign(CStr(rsCab("FFTOTKIL").Value))
            b.Put()

            b = .CreateBand("Normal;BorderStyle=Bottom;Margins=130,200,100,150")
            b.Breakable = False
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black; BorderStyle=  left;VertAlignment=Center"
            b.CreateCell(1180, "BorderStyle=LeftRight").CreateText("Alignment=left;FontBold=1").Assign("")
            b.CreateCell(390, "BorderStyle=Left; BrushColor=215,215,215").CreateText("Alignment=Right").Assign(Titulo("TIT106"))
            b.CreateCell(340, "BorderStyle=Right;FontSize=10;BrushColor=215,215,215").CreateText("Alignment=Right;BorderRightMargin=10").Assign(CStr(rsCab("FFTOTUNI").Value))
            b.Put()

            b = .CreateBand("Normal;BorderStyle=None;Margins=130,200,100,150")
            b.Breakable = False
            b.Height = 25
            b.ChildrenStyle = "Normal;FontSize=10;Fontname=Arial;BorderColor=Black;BorderStyle=None;VertAlignment=Center"
            b.CreateCell(1210, "BorderStyle=Top").CreateText("Alignment=left;FontBold=1").Assign("")
            b.Put()

            txt = ""
            If Idioma = "ENG" Then
                txt = Titulo("EXP01") & vbCrLf
                txt = txt & Titulo("EXP02") & vbCrLf
                txt = txt & Titulo("EXP03") & vbCrLf
                txt = txt & Titulo("EXP04") & vbCrLf
            End If

            b = .CreateBand("Normal;BorderStyle=None")
            b.Breakable = False
            b.ChildrenStyle = "Normal;BorderStyle=None;VertAlignment=Center"
            b.Height = 150
            b.CreateCell(600, "BorderStyle=None").CreateImage("Alignment=Center").Assign(APP_PATH + "\SelloFactura.png")
            b.CreateCell(600, "BorderStyle=None").CreateText().Assign("")
            b.CreateCell(710, "BorderStyle=None").CreateText().Assign(txt)
            b.Put()
            b = .CreateBand("Normal;BorderStyle=None;Margins=130,200,100,150")
            b.Breakable = False
            b.ChildrenStyle = "Normal;FontSize=10;Fontname=Arial;BorderColor=Black;BorderStyle=None;VertAlignment=Center"
            b.CreateCell(550, "BorderStyle=Top").CreateText("Alignment=Center;FontBold=1").Assign(Titulo("TIT111"))
            b.CreateCell(40, "BorderStyle=None").CreateText().Assign("")
            b.CreateCell(550, "BorderStyle=Top").CreateText("Alignment=Center;FontBold=1").Assign(Titulo("TIT112"))
            b.CreateCell(470, "BorderStyle=None").CreateText().Assign("")
            b.Put()

            b = .CreateBand("Normal;BorderStyle=None;Margins=130,200,100,150")
            b.Height = 25
            b.CreateCell(1210).CreateText().Assign("")
            b.Put()

            b = .CreateBand("Normal;BorderStyle=None;Margins=130,200,100,150")
            b.Height = 25
            b.CreateCell(1210).CreateText().Assign("")
            b.Put()

        End With

    End Sub


    Function Titulo(Clave As String) As String

        Titulo = Clave & " ??"
        rsTit.Filter = "CCCODE='" & Clave & "'"
        If Not rsTit.EOF Then
            If Idioma = "ESP" Then
                Titulo = rsTit("CCNOT1").Value
            Else
                Titulo = rsTit("CCNOT2").Value
            End If
        End If

    End Function


    Function NotasFactura() As Integer
        Dim rs As New ADODB.RecordSet
        Dim fSql As String

        fSql = ""
        '    If Emergencia(qPedid) Then
        '        fSql = " SELECT DEC( 0, 1, 0) AS CAT, SNSEQ, SNDESC   "
        '        fSql = fSql & " FROM ESNL01 "
        '        fSql = fSql & " WHERE SNTYPE = 'O' AND SNCUST = 88888888 "
        '        fSql = fSql & " UNION  "
        '    End If

        fSql = fSql & " SELECT DEC( 1, 1, 0) AS CAT, SNSEQ, SNDESC   "
        fSql = fSql & " FROM ESNL01 INNER JOIN SIL ON SNTYPE =  'L' AND SNCUST=ILORD AND SnSHIP=ILSEQ "
        fSql = fSql & " WHERE ILDPFX ='" & Factura_Pref & "'  AND ILDOCN=" & Factura_Num

        fSql = fSql & " UNION  "

        fSql = fSql & " SELECT DEC( 2, 1, 0) AS CAT, SNSEQ, SNDESC   "
        fSql = fSql & " FROM ESNL01 INNER JOIN SIH ON SNTYPE =  'O' AND SNCUST=SIORD AND SNSHIP=0 "
        fSql = fSql & " WHERE IHDPFX = '" & Factura_Pref & "'  AND IHDOCN=" & Factura_Num

        fSql = fSql & " UNION  "

        fSql = fSql & " SELECT DEC( 3, 1, 0) AS CAT, SNSEQ, SNDESC   "
        fSql = fSql & " FROM ESNL01 INNER JOIN RFFACCAB ON SNTYPE =  'C' AND SNCUST=FCLIENT AND SNSHIP=0 "
        fSql = fSql & " WHERE FPREFIJ = '" & Factura_Pref & "'  AND FFACTUR=" & Factura_Num

        fSql = fSql & " UNION  "

        fSql = fSql & " SELECT DEC( 4, 1, 0) AS CAT, SNSEQ, SNDESC   "
        fSql = fSql & " FROM ESNL01 INNER JOIN RFFACCAB ON SNTYPE =  'C' AND SNCUST=FCLIENT AND SNSHIP=FPTOEN "
        fSql = fSql & " WHERE FPREFIJ ='" & Factura_Pref & "'  AND FFACTUR=" & Factura_Num


        DB.DropTable("QTEMP." & Trim(Factura_Pref) & Factura_Num)


        fSql = "CREATE TABLE QTEMP." & Trim(Factura_Pref) & Factura_Num & " AS ( " & fSql & " ) WITH DATA"
        DB.ExecuteSQL(fSql)

        DB.OpenRS(rs, "SELECT COUNT(*) FROM QTEMP." & Factura_Pref.Trim & Factura_Num)
        TotNotas = rs(0).Value
        rs.Close()

        fSql = "SELECT COUNT(*) FROM RFFACDET"
        fSql = fSql & " WHERE "
        fSql = fSql & " DPREFIJ = '" & Factura_Pref & "'"   '//2A    Prefijo
        fSql = fSql & " AND DFACTUR = " & Factura_Num       '//8P0   Factura

        DB.OpenRS(rs, fSql)
        TotFilas = rs(0).Value
        rs.Close()
        Return TotFilas

    End Function

    Function Emergencia(Pedido As String) As String
        Dim rs As New ADODB.RecordSet

        Emergencia = ""
        fSql = " SELECT * FROM ECH INNER JOIN ECL ON HORD=LORD  "
        fSql = fSql & " WHERE LCONT='SALGR'  "
        fSql = fSql & " AND HTAX IN('CGCOE', 'CRCME', 'CRCNE', 'CRSME', 'CLESE')   "
        fSql = fSql & " AND HORD = " & Pedido
        If DB.OpenRS(rs, fSql) Then
            rs.Close()
            fSql = " SELECT SNSEQ, SNDESC   "
            fSql = fSql & " FROM ESNL01 "
            fSql = fSql & " WHERE SNTYPE = 'O' AND SNCUST = 88888888 "
            fSql = fSql & " ORDER BY SNSEQ  "
            DB.OpenRS(rs, fSql)
            Do While Not rs.EOF
                Emergencia = Emergencia & rs("SNDESC").Value & " "
                rs.MoveNext()
            Loop
        End If
        rs.Close()

    End Function


    Sub DetalleFactura(ByVal sqlDeta As String)

        DB.DropTable("QTEMP.FA" & logDocNum)

        fSql = "CREATE TABLE QTEMP.FA" & logDocNum & " (  "
        fSql = fSql & "     DORDENL DECIMAL(6, 0)  GENERATED ALWAYS AS IDENTITY,  "  'Para que genere el orden de linea
        fSql = fSql & "     DPREFIJ CHAR(2) CCSID 37 NOT NULL ,  "
        fSql = fSql & "     DFACTUR DECIMAL(8, 0) NOT NULL ,  "
        fSql = fSql & "     DLINEA VARCHAR(6) CCSID 284 NOT NULL ,  "
        fSql = fSql & "     DCODPRO CHAR(35) CCSID 37 NOT NULL ,  "
        fSql = fSql & "     DDESPRO CHAR(50) CCSID 37 NOT NULL ,  "
        fSql = fSql & "     DUNIMED CHAR(2) CCSID 37 NOT NULL ,  "
        fSql = fSql & "     DPORIMP VARCHAR(7) CCSID 284 NOT NULL ,  "
        fSql = fSql & "     DCANTID DECIMAL(11, 3) NOT NULL ,  "
        fSql = fSql & "     DVALUNI DOUBLE PRECISION NOT NULL ,  "
        fSql = fSql & "     DVALTOT DOUBLE PRECISION NOT NULL ,  "
        fSql = fSql & "     DFPORIMP CHAR(6) CCSID 37 NOT NULL ,  "
        fSql = fSql & "     DFCANTID CHAR(14) CCSID 37 NOT NULL ,  "
        fSql = fSql & "     DFUNIDAD CHAR(14) CCSID 37 NOT NULL ,  "
        fSql = fSql & "     DFVALUNI CHAR(16) CCSID 37 NOT NULL ,  "
        fSql = fSql & "     DFVALTOT CHAR(18) CCSID 37 NOT NULL ,  "
        fSql = fSql & "     DNOTLIN CHAR(250) CCSID 37 NOT NULL )   "
        DB.ExecuteSQL(fSql)

        fSql = "INSERT INTO QTEMP.FA" & logDocNum & "(DPREFIJ, DFACTUR, DLINEA, DCODPRO, DDESPRO, DUNIMED, "
        fSql = fSql & " DPORIMP, DCANTID, DVALUNI, DVALTOT, DFPORIMP, DFCANTID, DFUNIDAD, DFVALUNI, DFVALTOT, DNOTLIN) "
        DB.ExecuteSQL(fSql & sqlDeta)


    End Sub

    Sub LinesAdd(lineas As Integer)

        Exit Sub

        '' ''Dim rs As New ADODB.RecordSet
        '' ''Dim Ta1 As TBoxTable
        '' ''Dim Note As TBoxBand

        '' ''With Me.AxPdfBox1
        '' ''    rs.Open("SELECT CCNOT1 FROM RFPARAM WHERE CCTABL ='LINESADD' AND CCCODEN <= " & lineas, DB, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockPessimistic)
        '' ''    Ta1 = .CreateTable("Margins=130,200,100,150;BorderStyle=LeftRight")
        '' ''    Ta1.Assign(rs)
        '' ''    Note = Ta1.CreateBand()
        '' ''    Note.Height = 45
        '' ''    Note.Role = TBandRole.rlDetail
        '' ''    Note.ChildrenStyle = "FontSize=9;Fontname=Arial; VertAlignment=Center;BorderStyle=LeftRight"
        '' ''    Note.CreateCell(1910, "BorderStyle=Left").CreateText("Alignment=Left").Bind("CCNOT1")
        '' ''    Ta1.Put()
        '' ''    rs.Close()
        '' ''End With


    End Sub


End Class
