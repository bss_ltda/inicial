﻿'PedidoParaDespachar
Imports System
Imports System.Globalization
Imports System.IO
Imports System.Collections
Imports System.Xml.Serialization
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Xml
Imports System.Net
Imports ConsumoWsColtanquesPD
Module Module1
    Dim Factura As String
    Dim Pedid As String
    Dim Ial As String
    Dim Oc As String
    Dim Bodega As String
    Dim conso As String
    Dim Factu As String
    Dim gsProvider As String
    Dim gsDatasource As String
    Dim rs As New ADODB.Recordset
    Dim rs1 As New ADODB.Recordset
    Dim mp2Param(5) As String
    Dim AS400Param(5) As String
    Dim Resultado As Boolean = True
    Dim C_ID As String
    Dim i As Integer
    Dim sLib
    Dim gsConnString
    Dim DB_DB2, DB_MP2

    Structure respuesta

        Dim Operacion As String
        Dim Pedido As String
        Dim TotalLineas As String
        Dim Mensaje As String

    End Structure

    Sub Main(ByVal Pedido As String())

        Pedid = Pedido(0)

        CargaPrm()
        C_ID = CalcConsec("RFLOGWSCDA", False, "99999999")
        '        enviaLog()
        DevuelveResultado()
        ' Correo()
        DB_DB2.Close()
    End Sub
    Function CargaPrm() As Boolean


        gsProvider = "IBMDA400"
        gsDatasource = My.Settings.AS400
        sLib = My.Settings.AMBIENTE

        gsConnString = "Provider=" & gsProvider & ";Data Source=" & gsDatasource & ";"
        gsConnString = gsConnString & "Persist Security Info=False;Default Collection=" & sLib & ";"
        gsConnString = gsConnString & "Password=LXAPL;User ID=APLLX;"

        DB_DB2 = New ADODB.Connection
        DB_MP2 = New ADODB.Connection
        DB_DB2.Open(gsConnString)

        rs.Open(" SELECT * FROM " & sLib & ".ZCC WHERE CCTABL = 'LXLONG'", DB_DB2)
        Do While Not rs.EOF
            Select Case rs("CCCODE").Value.ToString
                Case "LXUSER"
                    AS400Param(0) = rs("CCDESC").Value.ToString
                Case "LXPASS"
                    AS400Param(1) = rs("CCDESC").Value.ToString
            End Select
            rs.MoveNext()
        Loop
        rs.Close()

        For i = 0 To 1
            If AS400Param(i) = "" Then
                Resultado = False
            End If
        Next

        gsConnString = "Provider=" & gsProvider & ";Data Source=" & gsDatasource & ";"
        gsConnString = gsConnString & "Persist Security Info=False;Default Collection=" & sLib & ";"
        gsConnString = gsConnString & "Password=" & AS400Param(1) & ";User ID=" & AS400Param(0) & ";"
        gsConnString = gsConnString & "Force Translate=0;"
        DB_DB2.Close()
        If Resultado Then
            DB_DB2.Open(gsConnString)
        Else
            'Error Conexion
        End If

        Return Resultado

    End Function

    Function DevuelveResultado()

        Dim fSql As String
        Dim xmlstr As String = ""
        Dim sw As New StringWriter()
        Dim writer As New XmlTextWriter(sw)
        Dim flag As Integer = 0
        Dim ped As String
        Dim flag2 As Integer = 0

        fSql = " SELECT "
        fSql = fSql & " PLBODS        , "                                       'Bodega
        fSql = fSql & " PLPEDID       , "                                       'Pedido 
        fSql = fSql & " TRIM(PLPFX) || PLDOCN AS FACTURA, "                     'Factura
        fSql = fSql & " PLPLANI       , "                                       'Planilla               6P 0  Planilla
        fSql = fSql & " PLCUST , "                                              'Codigo Cliente
        fSql = fSql & " PLCUSTD       , "                                       'NombreCliente        8P 0  Cliente
        fSql = fSql & " PLCUST || '-' || PLSHIP AS PENVIO, "                    'Codigo Punto de Envio
        fSql = fSql & " PLSHIPD       , "                                       'PtoEnvio
        fSql = fSql & " DPTONOM   , "           'Departamento
        fSql = fSql & " CIUDADNOM , "           'Ciudad
        fSql = fSql & " CCIUD     , "           'Codigo Dane   
        'fSql = fSql & " TRIM(PLHAD1) || ' ' TRIM(PLHAD2) || ' ' TRIM(PLHAD3) AS DIRECC     , "       'Direccion
        fSql = fSql & " TRIM(PLHAD1)  AS DIRECC     , "       'Direccion
        fSql = fSql & " PLFECR    , "     'Fecha Compromiso
        fSql = fSql & " PLPCHK01,"  'Factura
        fSql = fSql & " PLPCHK03,"  'IAL
        fSql = fSql & " PLPCHK04 "  'Orden de Compra
        fSql = fSql & " FROM RFPLCAB INNER JOIN RVLPC ON PLCIUDAD=PCPSCD  AND  PLDEP = PCSTCD "
        fSql = fSql & " WHERE "
        fSql = fSql & " PLPEDID =" & Pedid

        rs.Open(fSql, DB_DB2)
        If Not rs.EOF Then
            Console.WriteLine("validado")
            Factura = rs("PLPCHK01").Value.ToString
            Ial = rs("PLPCHK03").Value.ToString
            Oc = rs("PLPCHK04").Value.ToString
            Bodega = rs("PLBODS").Value.ToString
            Factu = rs("FACTURA").Value.ToString
            ' writer.WriteStartDocument()

            writer.Formatting = Formatting.Indented
            writer.Indentation = 2

            writer.WriteStartElement("PedidoParaDespachar")

            writer.WriteStartElement("Usuario")
            writer.WriteString("BRINSA")
            writer.WriteEndElement()

            writer.WriteStartElement("Password")
            writer.WriteString("83023911.")
            writer.WriteEndElement()

            writer.WriteStartElement("Bodega")
            writer.WriteString(rs("PLBODS").Value.ToString)
            writer.WriteEndElement()

            writer.WriteStartElement("Pedido")
            writer.WriteString(rs("PLPEDID").Value.ToString)
            ped = rs("PLPEDID").Value.ToString
            writer.WriteEndElement()

            writer.WriteStartElement("Factura")
            writer.WriteString(rs("FACTURA").Value.ToString)
            writer.WriteEndElement()

            writer.WriteStartElement("Planilla")
            writer.WriteString(rs("PLPLANI").Value.ToString)
            writer.WriteEndElement()

            writer.WriteStartElement("Cliente")
            writer.WriteString(rs("PLCUST").Value.ToString)
            writer.WriteEndElement()

            writer.WriteStartElement("NombreCliente")
            writer.WriteString(rs("PLCUSTD").Value.ToString)
            writer.WriteEndElement()

            writer.WriteStartElement("PuntoEnvio")
            writer.WriteString(rs("PENVIO").Value.ToString)
            writer.WriteEndElement()

            writer.WriteStartElement("NombrePuntoEnvio")
            writer.WriteString(rs("PLSHIPD").Value.ToString)
            writer.WriteEndElement()

            writer.WriteStartElement("Dpto")
            writer.WriteString(rs("DPTONOM").Value.ToString)
            writer.WriteEndElement()

            writer.WriteStartElement("Ciudad")
            writer.WriteString(rs("CIUDADNOM").Value.ToString)
            writer.WriteEndElement()

            writer.WriteStartElement("CodigoDane")
            writer.WriteString(rs("CCIUD").Value.ToString)
            writer.WriteEndElement()

            writer.WriteStartElement("Direccion")
            writer.WriteString(rs("DIRECC").Value.ToString)
            writer.WriteEndElement()

            writer.WriteStartElement("PlanillaPDF")
            writer.WriteString(rs("PLPLANI").Value.ToString & ".pdf")
            writer.WriteEndElement()

            writer.WriteStartElement("FechaCita")
            writer.WriteString("2013-11-02")
            writer.WriteEndElement()

            writer.WriteStartElement("Notas")
            writer.WriteString(rs("PLBODS").Value.ToString)
            writer.WriteEndElement()


            rs.Close()


            fSql = " SELECT DLINEA, DPROD, DCANT"
            fSql = fSql & " FROM RDCC"
            fSql = fSql & " WHERE "
            fSql = fSql & " DPEDID = " & Pedid

            rs.Open(fSql, DB_DB2)
            While Not rs.EOF
                Console.WriteLine("validado Linea")
                writer.WriteStartElement("Linea")
                CrearNodoLine(rs("DLINEA").Value.ToString, rs("DPROD").Value.ToString, rs("DCANT").Value.ToString, writer) ' Las veces que se desee Programa line
                writer.WriteEndElement()
                rs.MoveNext()
            End While
            rs.Close()
            writer.WriteEndElement()

            writer.Flush()
            xmlstr = sw.ToString
            writer.Close()
            Dim pDoc As New XmlDocument
            pDoc.LoadXml(xmlstr)
            Dim xml As String = pDoc.InnerXml
            'DB_DB2.Close()
            wrtLogWsCDA(C_ID, xml, "Pedido-" & ped, "", "PedidoParaDespachar", "wsColtanques", "EM")
            Dim PruebaDoc As XmlElement
            Console.WriteLine("validadoXML")
            Dim prueba = New ServiceReference1.WSDistribucionSoapClient
            Try
                Dim PruebaString As String
                PruebaString = prueba.PedidoParaDespachar(xml)
                Console.WriteLine("RespuestaXML")
               
                Dim Cabeceraa As New respuesta
                Dim documento As XDocument = XDocument.Parse(PruebaString)
                Dim errorr As Integer
                errorr = 0

                Dim qx = From xee In documento.Elements("Respuesta")
              Select New With { _
                       .Operacion = xee.Element("Operacion").Value,
                       .Pedido = xee.Element("Pedido").Value,
                       .TotalLineas = xee.Element("TotalLineas").Value,
                       .Mensaje = xee.Element("Mensaje").Value
                         }
                Try
                    For Each elements In qx
                        errorr = errorr + 1
                        Cabeceraa = New respuesta
                        Cabeceraa.Operacion = elements.Operacion
                        Cabeceraa.Pedido = elements.Pedido
                        Cabeceraa.TotalLineas = elements.TotalLineas
                        Cabeceraa.Mensaje = elements.Mensaje
                    Next

                Catch
                    wrtLogWsCDA(C_ID, PruebaString, "Pedido" & ped, "Fail", "Reaprovisionamiento", "wsColtanques", "EM")
                End Try
                If errorr > 0 Then
                    If Cabeceraa.Mensaje = "0000" Then
                        wrtLogWsCDA(C_ID, PruebaString, "Pedido" & ped, "Pass", "Reaprovisionamiento", "wsColtanques", "EM")
                    Else
                        wrtLogWsCDA(C_ID, PruebaString, "Pedido" & ped, "Fail", "Reaprovisionamiento", "wsColtanques", "EM")
                    End If
                Else
                    wrtLogWsCDA(C_ID, PruebaString, "Pedido" & ped, "Fail", "Reaprovisionamiento", "wsColtanques", "EM")
                End If

            Catch
                Console.WriteLine(Err.Description)
            End Try



            Return pDoc
        Else

        End If

    End Function

    Function CrearNodoLine(ByVal linea As String, ByVal Pedido As String, ByVal Cantidad As String, ByVal writer As XmlTextWriter)

        writer.WriteStartElement("LineaPedido")
        writer.WriteString(linea)
        writer.WriteEndElement()

        writer.WriteStartElement("Producto")
        writer.WriteString(Pedido)
        writer.WriteEndElement()

        writer.WriteStartElement("RequiereLote")
        writer.WriteString("1")
        writer.WriteEndElement()

        writer.WriteStartElement("Cantidad")
        writer.WriteString(Replace(Cantidad, ",", "."))
        writer.WriteEndElement()
        Return 0
    End Function

    'Function Correo()
    '    ' Create a mailman object for sending email.
    '    Dim fSql As String
    '    Dim sep As String = ""
    '    Dim Usuarios As String = ""
    '    Dim mailman As New Chilkat.MailMan()

    '    Dim success As Boolean
    '    success = mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
    '    If (success <> True) Then
    '        '       MsgBox(mailman.LastErrorText)
    '        Exit Function
    '    End If
    '    mailman.SmtpHost = "smtp.earthlink.net"
    '    Dim email As New Chilkat.Email()
    '    email.Body = "Pruebas correo archivos adjuntos"
    '    email.Subject = "This email has several file attachments."

    '    fSql = " SELECT * "
    '    fSql = fSql & " FROM RCAU"
    '    fSql = fSql & " WHERE "
    '    fSql = fSql & " UUSR = 'BOD-" & Bodega & "'"
    '    rs1.Open(fSql, DB_DB2)

    '    email.AddTo(rs1.Fields("UNOM").Value, rs1.Fields("UEMAIL").Value)

    '    email.From = "Pruba Archivos Adjuntos <ALogistica@brinsa.com.co>"
    '    Dim contentType As String

    '    If Factura = "S" Then
    '        contentType = email.AddFileAttachment(My.Settings.CARPETA_IMG & "pdf\FacturaPDF\" & Factu & ".pdf")
    '        If contentType Is Nothing Then
    '            '          MsgBox(email.LastErrorText)
    '        End If
    '    End If

    '    'If Ial = "S" Then
    '    '   contentType = email.AddFileAttachment(My.Settings.CARPETA_IMG & "pdf\FacturaPDF\" & Factu & ".pdf")
    '    '   If contentType Is Nothing Then
    '    '       MsgBox(email.LastErrorText)
    '    '   End If
    '    'End If

    '    'If Oc = "S" Then
    '    '   contentType = email.AddFileAttachment(My.Settings.CARPETA_IMG & "pdf\FacturaPDF\" & Factu & ".pdf")
    '    '   If contentType Is Nothing Then
    '    '       MsgBox(email.LastErrorText)
    '    '   End If
    '    'End If

    '    success = mailman.SendEmail(email)
    '    If success Then
    '        '      MsgBox("Sent email with attachments!")
    '    Else
    '        '     MsgBox(mailman.LastErrorText)
    '    End If

    'End Function

    Public Function QuitarCaracteres(ByVal cadena As String) As String

        cadena = cadena.Replace("ñ", "")
        cadena = cadena.Replace("Ñ", "")
        cadena = cadena.Replace("á", "a")
        cadena = cadena.Replace("é", "e")
        cadena = cadena.Replace("í", "i")
        cadena = cadena.Replace("ó", "o")
        cadena = cadena.Replace("ú", "u")
        cadena = cadena.Replace("Á", "A")
        cadena = cadena.Replace("É", "E")
        cadena = cadena.Replace("Í", "I")
        cadena = cadena.Replace("Ó", "O")
        cadena = cadena.Replace("Ú", "U")
        cadena = cadena.Replace("%", "")
        cadena = cadena.Replace("&", "")
        Return cadena
    End Function
    Public Function CalcConsec(ByRef sID As String, ByRef bLee As Boolean, ByRef TopeMax As String) As Double
        Dim rs As ADODB.Recordset
        Dim locID As Double
        Dim sql As String
        Dim sConsec As String
        Dim tMax As Double

        sql = "SELECT CCDESC FROM ZCCL01 WHERE CCTABL = 'SECUENCE' AND CCCODE='" & sID & "'"
        rs = New ADODB.Recordset
        tMax = Val(TopeMax)

        With rs
            .CursorLocation = ADODB.CursorLocationEnum.adUseServer
            .Open(sql, DB_DB2, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)
            If Not (.BOF And .EOF) Then
                locID = Val(.Fields("CCDESC").Value) + 1
                If locID > tMax Then
                    locID = 1
                End If
                sConsec = Format(locID, New String("0", Len(TopeMax)))
                If Not bLee Then
                    .Fields("CCDESC").Value = sConsec
                    .Update()
                End If
                .Close()
            Else
                locID = 1
                .Close()
                sConsec = Format(1, New String("0", Len(8)))
                DB_DB2.Execute(" INSERT INTO ZCC (CCID, CCTABL, CCCODE, CCDESC ) " & " VALUES( 'CC', 'SECUENCE', '" & sID & "', '" & sConsec & "' )")
            End If
        End With

        CalcConsec = locID

    End Function


    Sub enviaLog()

        Dim fSql As String
        fSql = "<Inventory identifier=""668612-wms-0-0-A-10033190-00000275"" priority=""8"" sourceName=""Inv-A -10033190 -WMSFTRIN"" actionType=""Post"">"     '" & vbCrLf
        fSql = fSql & "Post"     '" & vbCrLf
        fSql = fSql & "<AdviceNote>00000275</AdviceNote>"     '" & vbCrLf
        fSql = fSql & "<TxTypeCode>A</TxTypeCode>"     '" & vbCrLf
        fSql = fSql & "<inventoryitemcode>10033190</inventoryitemcode>"     '" & vbCrLf
        fSql = fSql & "<WarehouseCode>EM</WarehouseCode>"     '" & vbCrLf
        fSql = fSql & "<LocationCode>QM</LocationCode>"     '" & vbCrLf
        fSql = fSql & "<TxDate>2014-01-28</TxDate>"     '" & vbCrLf
        fSql = fSql & "<TxQty>-5.000</TxQty>"     '" & vbCrLf
        fSql = fSql & "<Comment>00000275</Comment>"     '" & vbCrLf
        fSql = fSql & "<LotCode>384</LotCode>"     '" & vbCrLf
        fSql = fSql & "</Inventory>"     '" & vbCrLf

        wrtLogWsCDA(C_ID, fSql, "Pedido-943223", "", "Reaprovisonamiento", "wsColtanques", "EM")
        wrtLogWsCDA(C_ID, fSql, "Pedido-943223", "Fail", "Reaprovisonamiento", "wsColtanques", "EM")
        wrtLogWsCDA(C_ID, fSql, "Pedido-943223", "Pass", "Reaprovisonamiento", "wsColtanques", "EM")
    End Sub

    Sub wrtLogWsCDA(ByVal logId As String, ByVal xml As String, ByVal id_msg As String, ByVal estado As String, ByVal operacion As String, ByVal fuente As String, ByVal usuario As String)
        Dim rs As New ADODB.Recordset
        Dim dato() As Byte
        dato = System.Text.Encoding.UTF8.GetBytes("<?xml version=""1.0""?>" & xml)
        With rs
            .CursorLocation = ADODB.CursorLocationEnum.adUseServer
            .Open("SELECT * FROM RFLOGWSCDA WHERE C_ID = " & logId, DB_DB2, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)
            If .EOF Then
                .AddNew()
                .Fields("C_ID").Value = logId
                .Fields("C_XML").Value = dato
                .Fields("C_NOUN_ID").Value = operacion
                .Fields("C_MESSAGEID").Value = id_msg
                .Fields("C_SOURCE_NAME").Value = fuente
                .Fields("C_USER").Value = usuario
                .Fields("C_BODID").Value = ""
            Else
                .Fields("C_WAS_PROCESSED").Value = 1
                .Fields("C_REPLY").Value = dato
                .Fields("C_PASS_FAIL").Value = estado
            End If
            .Update()
            .Close()
        End With
        rs = Nothing
    End Sub
End Module
