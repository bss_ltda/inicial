﻿Imports System.IO

Public Class wsSMS

    Private WithEvents tim As New Timers.Timer(1000)

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Agregue el código aquí para iniciar el servicio. Este método debería poner
        ' en movimiento los elementos para que el servicio pueda funcionar.
        tim.Enabled = True
    End Sub

    Protected Overrides Sub OnStop()
        ' Agregue el código aquí para realizar cualquier anulación necesaria para detener el servicio.
    End Sub

    Private Sub tim_Elapsed(sender As Object, e As System.Timers.ElapsedEventArgs) Handles tim.Elapsed
        RevisaSMS()
    End Sub

    Sub RevisaSMS()
        Dim smsFile As String
        Dim tarea As FileInfo
        Dim line As String = ""
        Dim aDato() As String
        Dim aReporte() As String
        Dim pSender As String = ""
        Dim pPlanilla As String = ""
        Dim pReporte As String = ""
        Dim pHora As String = ""
        Dim sep As String = ""
        Dim i As Integer
        Dim DB As New ADODB.Connection
        Dim rs As New ADODB.Recordset
        Dim fSql As String
        Dim vSql As String
        Dim fecha As Date = Now()
        Dim subCarpeta As String = fecha.ToString("yyyyMMdd")

        Dim archivos As DirectoryInfo = New DirectoryInfo("C:\Program Files (x86)\NowSMS\SMS-IN\" & subCarpeta)
        'Dim archivos As DirectoryInfo = New DirectoryInfo("C:\Program Files (x86)\NowSMS\SMS-IN")
        DB.Open("Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=APLLX;Password=LXAPL;Persist Security Info=True;Application Name=DFU001;Default Collection=ERPLX834F;Force Translate=37;")
        aReporte = Split(" ", " ")
        For Each tarea In archivos.GetFiles()
            smsFile = tarea.DirectoryName & "\" & tarea.Name
            Dim sr As StreamReader = New StreamReader(smsFile)
            line = ""
            pSender = ""
            pPlanilla = ""
            pReporte = ""
            pHora = ""
            Do Until line Is Nothing
                line = sr.ReadLine()
                If Not line Is Nothing Then
                    aDato = Split(line, "=")
                    Select Case UCase(aDato(0))
                        Case "SENDER"
                            pSender = aDato(1)
                        Case "SCTS"
                            pHora += Mid(aDato(1), 1, 4) & "-"
                            pHora += Mid(aDato(1), 5, 2) & "-"
                            pHora += Mid(aDato(1), 7, 2) & "-"
                            pHora += Mid(aDato(1), 9, 2) & "."
                            pHora += Mid(aDato(1), 11, 2) & "."
                            pHora += Mid(aDato(1), 13, 2) & ".000000"
                        Case "DATA"
                            aReporte = Split(aDato(1), " ")
                    End Select
                End If
            Loop
            sr.Close()
            For i = 0 To UBound(aReporte)
                pPlanilla = aReporte(i)
                If PlanillaOK(pPlanilla, DB) Then
                    fSql = "SELECT DCONSO, HSTS FROM RDCC INNER JOIN RHCC ON DCONSO = HCONSO WHERE DPLANI = " & pPlanilla
                    rs.Open(fSql, DB)
                    If Not rs.EOF Then
                        fSql = " INSERT INTO RRCC( "
                        vSql = " VALUES ( "
                        fSql = fSql & " RTREP     , " : vSql = vSql & " 30 , "                                              '//2S0   Tipo Rep
                        fSql = fSql & " RPLANI    , " : vSql = vSql & " " & pPlanilla & ", "                                '//6S0   Planilla
                        fSql = fSql & " RCONSO    , " : vSql = vSql & "'" & rs("DCONSO").Value & "', "                      '//6A    Consolidado
                        fSql = fSql & " RSTSC     , " : vSql = vSql & " " & rs("HSTS").Value & ", "                         '//2S0   Est Cons
                        fSql = fSql & " RSTSE     , " : vSql = vSql & " 50, "                                               '//2S0   Est Entrega
                        fSql = fSql & " RFECREP   , " : vSql = vSql & "'" & pHora & "', "                                   '//26Z   F.Reporte
                        fSql = fSql & " RREPORT   , " : vSql = vSql & "'" & pPlanilla & " Fin de Entrega', "                '//502A  Reporte
                        fSql = fSql & " RCRTUSR   , " : vSql = vSql & "'" & pSender & "', "                                 '//10A   Usuario
                        fSql = fSql & " RAPP      ) " : vSql = vSql & "'" & "SMS" & "' ) "                                  '//15A   App
                        DB.Execute(fSql & vSql)

                        fSql = "UPDATE RHCC SET HREPORT = '" & pPlanilla & " Fin de Entrega', "
                        fSql = fSql & " HFECREP = '" & pHora & "'"
                        fSql = fSql & " WHERE HCONSO = '" & rs("DCONSO").Value & "'"
                        DB.Execute(fSql)

                        fSql = "UPDATE RECC SET ESTS = 50, EFINENT = '" & pHora & "'"
                        fSql = fSql & " WHERE EPLANI = " & pPlanilla
                        DB.Execute(fSql)
                    End If
                    rs.Close()
                ElseIf Trim(pPlanilla) <> "" Then
                    fSql = " INSERT INTO RRCC( "
                    vSql = " VALUES ( "
                    fSql = fSql & " RTREP     , " : vSql = vSql & " 30 , "                                              '//2S0   Tipo Rep
                    fSql = fSql & " RPLANI    , " : vSql = vSql & " " & pPlanilla & ", "                                '//6S0   Planilla
                    fSql = fSql & " RCONSO    , " : vSql = vSql & "'ERRPLA', "                                          '//6A    Consolidado
                    fSql = fSql & " RSTSE     , " : vSql = vSql & " 50, "                                               '//2S0   Est Entrega
                    fSql = fSql & " RFECREP   , " : vSql = vSql & "'" & pHora & "', "                                   '//26Z   F.Reporte
                    fSql = fSql & " RREPORT   , " : vSql = vSql & "'" & pPlanilla & " Error en Planila', "              '//502A  Reporte
                    fSql = fSql & " RCRTUSR   , " : vSql = vSql & "'" & pSender & "', "                                 '//10A   Usuario
                    fSql = fSql & " RAPP      ) " : vSql = vSql & "'" & "SMS" & "' ) "                                  '//15A   App
                    DB.Execute(fSql & vSql)
                End If
            Next
            tarea.CopyTo("C:\SMS\Procesados\" & tarea.Name, True)
            tarea.Delete()
            'Shell(smsFile, AppWinStyle.Hide, False, -1)
        Next
        DB.Close()

    End Sub

    Function PlanillaOK(plani As String, DB As ADODB.Connection) As Boolean
        Dim fSql As String
        Dim result As Boolean = False
        Dim rs As New ADODB.Recordset
        If IsNumeric(plani) Then
            fSql = "SELECT * FROM RECC WHERE EPLANI = " & plani
            rs.Open(fSql, DB)
            If Not rs.EOF Then
                result = True
            End If
            rs.Close()
        End If
        Return result
    End Function


End Class
