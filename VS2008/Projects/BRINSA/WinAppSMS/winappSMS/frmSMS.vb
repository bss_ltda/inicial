﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Data.Common
Imports System.Net

Public Class frmSMS
    Dim DB As New ADODB.Connection
    Dim regA As Double

    Private Sub cmdSMS_Click(sender As System.Object, e As System.EventArgs) Handles cmdSMS.Click
        EjecutaProceso()
    End Sub

    Sub EjecutaProceso()
        Dim i As Integer

        Dim hostName As String
        Dim ipAddr() As IPAddress

        hostName = Dns.GetHostName()
        ipAddr = Dns.GetHostAddresses(hostName)

        For i = 0 To ipAddr.Count - 1
            If InStr(ipAddr(i).ToString, "192.168.0") > 0 Then
                Me.Text &= " " & ipAddr(i).ToString
                Me.lblIp.Text = ipAddr(i).ToString
            End If
        Next

        Do While vbTrue
            lblTime.Text = Now()
            lblEnEjecucion.Text = "Revisando ..."
            Me.lblEnEjecucion.ForeColor = Color.Blue
            Me.txtEjecutado.Text = Now()
            Application.DoEvents()
            RevisaSMS()
            lblEnEjecucion.Text = "En espera ..."
            Application.DoEvents()
            For i = 1 To Integer.Parse(Me.txtSegs.Text)
                lblTime.Text = Now()
                Application.DoEvents()
                If Me.chkParar.Checked Then
                    Me.lblEnEjecucion.Text = "No se está ejecutando !!!"
                    Me.lblEnEjecucion.ForeColor = Color.Red
                    Return
                End If
                Threading.Thread.Sleep(1000)
            Next
            Application.DoEvents()
        Loop

    End Sub
    Sub RevisaSMS()

        Dim fecha As Date = Now()
        Dim subCarpeta As String = fecha.ToString("yyyyMMdd")
        Dim smsFile As String
        Dim tarea As FileInfo
        Dim line As String = ""
        Dim aDato() As String
        Dim resp As String
        Dim wsSMS As New localhost.Servicios
        Dim wsInfo As New localhost.SMSInfo

        Dim archivos As DirectoryInfo
        If Me.chkFolder.Checked Then
            archivos = New DirectoryInfo("C:\Program Files\NowSMS\SMS-IN\" & subCarpeta)
        Else
            archivos = New DirectoryInfo("C:\SMS\20160112")
        End If
        Try
            archivos.GetFiles()
        Catch ex As Exception
            Return
        End Try

        For Each tarea In archivos.GetFiles()
            smsFile = tarea.DirectoryName & "\" & tarea.Name
            Dim sr As StreamReader = New StreamReader(smsFile)
            line = ""


            Do Until line Is Nothing
                line = sr.ReadLine()
                If Not line Is Nothing Then
                    aDato = Split(line, "=")
                    Select Case UCase(aDato(0))
                        Case UCase("ModemName")
                            wsInfo.ModemName = aDato(1)
                        Case UCase("Sender")
                            wsInfo.Sender = aDato(1)
                        Case UCase("PhoneNumber")
                            wsInfo.PhoneNumber = aDato(1)
                        Case "SCTS"
                            wsInfo.SCTS = aDato(1)
                        Case "DATA"
                            wsInfo.Data = aDato(1)
                        Case UCase("ReceivedSMSC")
                            wsInfo.ReceivedSMSC = aDato(1)
                    End Select
                End If
            Loop
            sr.Close()
            resp = wsSMS.SMSFinDeEntrega(wsInfo)
            If Me.chkFolder.Checked Then
                tarea.CopyTo("C:\SMS\Procesados\" & tarea.Name, True)
                tarea.Delete()
            End If
        Next
        DB.Close()

    End Sub

    Private Sub frmSMS_Disposed(sender As Object, e As System.EventArgs) Handles Me.Disposed
        End
    End Sub


    Private Sub frmSMS_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'EjecutaProceso()
    End Sub

    Private Sub txtPlanilla_TextChanged(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Sub Panel1_Paint(sender As System.Object, e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub

    Private Sub txtPlanilla_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtPlanilla.KeyPress
        Dim util As New bssUtil
        If e.KeyChar = vbCr Then
            Me.lblDV.Text = util.dv(Me.txtPlanilla.Text)
        End If
    End Sub

    Private Sub txtPlanilla_TextChanged_1(sender As System.Object, e As System.EventArgs) Handles txtPlanilla.TextChanged

    End Sub
End Class
