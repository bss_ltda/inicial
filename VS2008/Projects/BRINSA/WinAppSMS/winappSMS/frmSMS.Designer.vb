﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSMS
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtLog = New System.Windows.Forms.RichTextBox()
        Me.cmdSMS = New System.Windows.Forms.Button()
        Me.chkParar = New System.Windows.Forms.CheckBox()
        Me.lblTime = New System.Windows.Forms.Label()
        Me.lblEnEjecucion = New System.Windows.Forms.Label()
        Me.txtSegs = New System.Windows.Forms.TextBox()
        Me.txtEjecutado = New System.Windows.Forms.Label()
        Me.lblIp = New System.Windows.Forms.Label()
        Me.chkFolder = New System.Windows.Forms.CheckBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblDV = New System.Windows.Forms.Label()
        Me.txtPlanilla = New System.Windows.Forms.TextBox()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtLog
        '
        Me.txtLog.Location = New System.Drawing.Point(12, 12)
        Me.txtLog.Name = "txtLog"
        Me.txtLog.Size = New System.Drawing.Size(448, 541)
        Me.txtLog.TabIndex = 0
        Me.txtLog.Text = ""
        '
        'cmdSMS
        '
        Me.cmdSMS.Location = New System.Drawing.Point(475, 12)
        Me.cmdSMS.Name = "cmdSMS"
        Me.cmdSMS.Size = New System.Drawing.Size(75, 23)
        Me.cmdSMS.TabIndex = 1
        Me.cmdSMS.Text = "SMS"
        Me.cmdSMS.UseVisualStyleBackColor = True
        '
        'chkParar
        '
        Me.chkParar.AutoSize = True
        Me.chkParar.Location = New System.Drawing.Point(475, 42)
        Me.chkParar.Name = "chkParar"
        Me.chkParar.Size = New System.Drawing.Size(51, 17)
        Me.chkParar.TabIndex = 2
        Me.chkParar.Text = "Parar"
        Me.chkParar.UseVisualStyleBackColor = True
        '
        'lblTime
        '
        Me.lblTime.AutoSize = True
        Me.lblTime.Location = New System.Drawing.Point(472, 74)
        Me.lblTime.Name = "lblTime"
        Me.lblTime.Size = New System.Drawing.Size(28, 13)
        Me.lblTime.TabIndex = 11
        Me.lblTime.Text = "hora"
        '
        'lblEnEjecucion
        '
        Me.lblEnEjecucion.AutoSize = True
        Me.lblEnEjecucion.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEnEjecucion.ForeColor = System.Drawing.Color.Red
        Me.lblEnEjecucion.Location = New System.Drawing.Point(472, 102)
        Me.lblEnEjecucion.Name = "lblEnEjecucion"
        Me.lblEnEjecucion.Size = New System.Drawing.Size(219, 24)
        Me.lblEnEjecucion.TabIndex = 12
        Me.lblEnEjecucion.Text = "No se está ejecutando"
        '
        'txtSegs
        '
        Me.txtSegs.Location = New System.Drawing.Point(568, 16)
        Me.txtSegs.Name = "txtSegs"
        Me.txtSegs.Size = New System.Drawing.Size(30, 20)
        Me.txtSegs.TabIndex = 13
        Me.txtSegs.Text = "10"
        '
        'txtEjecutado
        '
        Me.txtEjecutado.AutoSize = True
        Me.txtEjecutado.Location = New System.Drawing.Point(473, 138)
        Me.txtEjecutado.Name = "txtEjecutado"
        Me.txtEjecutado.Size = New System.Drawing.Size(28, 13)
        Me.txtEjecutado.TabIndex = 14
        Me.txtEjecutado.Text = "hora"
        '
        'lblIp
        '
        Me.lblIp.AutoSize = True
        Me.lblIp.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIp.ForeColor = System.Drawing.Color.MediumBlue
        Me.lblIp.Location = New System.Drawing.Point(472, 177)
        Me.lblIp.Name = "lblIp"
        Me.lblIp.Size = New System.Drawing.Size(107, 24)
        Me.lblIp.TabIndex = 15
        Me.lblIp.Text = "ip address"
        '
        'chkFolder
        '
        Me.chkFolder.AutoSize = True
        Me.chkFolder.Checked = True
        Me.chkFolder.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFolder.Location = New System.Drawing.Point(568, 42)
        Me.chkFolder.Name = "chkFolder"
        Me.chkFolder.Size = New System.Drawing.Size(113, 17)
        Me.chkFolder.TabIndex = 16
        Me.chkFolder.Text = "Folder NOW/SMS"
        Me.chkFolder.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.lblDV)
        Me.Panel1.Controls.Add(Me.txtPlanilla)
        Me.Panel1.Location = New System.Drawing.Point(476, 264)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(214, 79)
        Me.Panel1.TabIndex = 17
        '
        'lblDV
        '
        Me.lblDV.AutoSize = True
        Me.lblDV.Location = New System.Drawing.Point(80, 17)
        Me.lblDV.Name = "lblDV"
        Me.lblDV.Size = New System.Drawing.Size(22, 13)
        Me.lblDV.TabIndex = 19
        Me.lblDV.Text = "DV"
        '
        'txtPlanilla
        '
        Me.txtPlanilla.Location = New System.Drawing.Point(13, 14)
        Me.txtPlanilla.Name = "txtPlanilla"
        Me.txtPlanilla.Size = New System.Drawing.Size(60, 20)
        Me.txtPlanilla.TabIndex = 18
        '
        'frmSMS
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(716, 575)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.chkFolder)
        Me.Controls.Add(Me.lblIp)
        Me.Controls.Add(Me.txtEjecutado)
        Me.Controls.Add(Me.txtSegs)
        Me.Controls.Add(Me.lblEnEjecucion)
        Me.Controls.Add(Me.lblTime)
        Me.Controls.Add(Me.chkParar)
        Me.Controls.Add(Me.cmdSMS)
        Me.Controls.Add(Me.txtLog)
        Me.Name = "frmSMS"
        Me.Text = "Reporte Fin de Entrega"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtLog As System.Windows.Forms.RichTextBox
    Friend WithEvents cmdSMS As System.Windows.Forms.Button
    Friend WithEvents chkParar As System.Windows.Forms.CheckBox
    Friend WithEvents lblTime As System.Windows.Forms.Label
    Friend WithEvents lblEnEjecucion As System.Windows.Forms.Label
    Friend WithEvents txtSegs As System.Windows.Forms.TextBox
    Friend WithEvents txtEjecutado As System.Windows.Forms.Label
    Friend WithEvents lblIp As System.Windows.Forms.Label
    Friend WithEvents chkFolder As System.Windows.Forms.CheckBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents txtPlanilla As System.Windows.Forms.TextBox
    Friend WithEvents lblDV As System.Windows.Forms.Label

End Class
