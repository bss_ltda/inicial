﻿Module BSSGetLogin

    Function GetLocalHostName(ByVal Usr As String) As String
        Dim nArch As Integer
        Dim sDomain As String
        Dim strLinea As String
        Dim P() As String

        ChkFolder("C:\AppData")
        nArch = FreeFile()
        FileOpen(nArch, "C:\AppData\info01.bat", OpenMode.Output)
        PrintLine(nArch, "@SET > C:\AppData\info01.txt")
        FileClose(nArch)

        Shell("C:\AppData\info01.bat")

        FileOpen(nArch, "C:\AppData\info01.txt", OpenMode.Output)
            Do While Not EOF(nArch)
            Input(nArch, strLinea)
                strLinea = Replace(strLinea, vbTab, " ")
                P = Split(strLinea, "=")
                If UBound(P) = 1 Then
                    Select Case UCase(Trim(P(0)))
                        Case "COMPUTERNAME" : sLocalHostName = Trim(P(1))
                        Case "USERNAME" : sLanUser = Trim(P(1))
                        Case "USERDOMAIN" : sDomain = Trim(P(1))
                    End Select
                End If
            Loop
        FileClose(nArch)
            sLanUser = sDomain & "\" & sLanUser
            GetLocalHostName = GetUserLogin(Usr, gsLIB)

    End Function


    Function GetUserLogin(ByVal qUsr As String, ByVal qLib As String) As String

        GetUserLogin = GetSetting("BSSAppData", "Settings", "UserLogin." & qLib, UCase(Trim(qUsr)))

    End Function

    Function PutUserLogin(ByVal qUsr As String, ByVal qLib As String) As String

        SaveSetting("BSSAppData", "Settings", "UserLogin." & qLib, UCase(Trim(qUsr)))

    End Function


End Module
