﻿Imports System.IO

Public Class wsTareas

    Private WithEvents tim As New Timers.Timer(30000)

    Private DB As ADODB.Connection

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Agregue el código aquí para iniciar el servicio. Este método debería poner
        ' en movimiento los elementos para que el servicio pueda funcionar.
        tim.Enabled = True
        EventLog1.WriteEntry("Inicia Servicion " & Now.ToString)
    End Sub

    Protected Overrides Sub OnStop()
        ' Agregue el código aquí para realizar cualquier anulación necesaria para detener el servicio.
        tim.Enabled = False
    End Sub

    Private Sub tim_Elapsed(sender As Object, e As System.Timers.ElapsedEventArgs) Handles tim.Elapsed

        tim.Enabled = False
        revisaTareas()
        tim.Enabled = True

    End Sub

    Sub revisaTareas()
        Dim fSql As String
        Dim pgm, param, exe As String
        Dim rs As New ADODB.Recordset
        Dim sts As Integer
        Dim msgT As String
        Dim tLXmsg As String

        Dim DB As New ADODB.Connection
        DB.Open("Provider=IBMDA400.DataSource;Data Source=172.20.4.31;User ID=SVPORFIN;Password=SVPORFIN;Persist Security Info=True;Convert Date Time To Char=TRUE;Application Name=WSTAREAS;Default Collection=ERPAPPLB;Force Translate=0;")
        Try
            fSql = " SELECT * FROM  BSSFTASK WHERE STS1 = 0 "
            rs.Open(fSql, DB)
            Do While Not rs.EOF
                Select Case UCase(rs("TTIPO").Value)
                    Case UCase("Shell")
                        pgm = rs("TPGM").Value
                        param = rs("TPRM").Value
                        exe = pgm & " " & param
                        sts = 1
                        msgT = "Comando Enviado."
                        tLXmsg = "EJECUTADA"
                        Try
                            Shell(exe, AppWinStyle.Hide, False, -1)
                        Catch ex As Exception
                            sts = -1
                            msgT = ex.Message.Replace("'", "''")
                            tLXmsg = "ERROR"
                        End Try
                        fSql = " UPDATE BSSFTASK SET "
                        fSql = fSql & " TLXMSG = '" & tLXmsg & "', "
                        fSql = fSql & " STS1 = " & sts & ", "
                        fSql = fSql & " TUPDDAT = NOW() , "
                        fSql = fSql & " TMSG = '" & msgT & "'"
                        fSql = fSql & " WHERE TNUMREG = " & rs("TNUMREG").Value
                        DB.Execute(fSql)
                    Case UCase("Java")

                End Select
                rs.MoveNext()
            Loop
            rs.Close()
            DB.Close()
        Catch ex As Exception
            sts = -1
            msgT = ex.Message.Replace("'", "''")
            tLXmsg = "ERROR"
            fSql = " UPDATE BSSFTASK SET "
            fSql = fSql & " TLXMSG = '" & tLXmsg & "', "
            fSql = fSql & " STS1 = " & sts & ", "
            fSql = fSql & " TUPDDAT = NOW() , "
            fSql = fSql & " TMSG = '" & msgT & "'"
            fSql = fSql & " WHERE TNUMREG = " & rs("TNUMREG").Value
            DB.Execute(fSql)
        End Try

    End Sub

    Private Sub EventLog1_EntryWritten(sender As System.Object, e As System.Diagnostics.EntryWrittenEventArgs) Handles EventLog1.EntryWritten

    End Sub
End Class
