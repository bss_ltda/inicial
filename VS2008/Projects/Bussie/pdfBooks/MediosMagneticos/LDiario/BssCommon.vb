﻿Imports System.Reflection
Imports System
Imports System.IO
Imports System.Text

Module BssCommon
    Public DB As New ADODB.Connection
    Public tNum = 1
    Public tStr = 2
    Public tXfn = 3
    Public AMBIENTE As Integer        'Ambiente Real/Pruebas/Access para configura LIBs y ADO
    Public gsConnString As String         'String de conexion a datos
    Public gsLIBs As String
    Public gsForceTranslate As String
    Public cmdConnString As String         'String de conexion a datos para CALL
    Public gsConnShape As String
    Public bPRUEBAS As Boolean
    Public bSimulConso As Boolean
    Public NO_ES_LX_BPCS As Boolean
    Public ServerRaiz As String
    Public CARPETA_IMG As String
    Public C_UPLOAD_DIR As String
    Public CARPETA_LOG As String
    Public CARPETA_SITIO As String
    Public sHTTP As String
    Public sHTTP_Tit As String
    Public sODBC As String
    Public IdSesion As Long
    Public bImgLX As Boolean
    Public bDateToDate As Boolean
    Public frmAncho As Integer
    Public frmAlto As Integer

    Public sFolder As String

    Public Const DB2_DATEDEC = " ( YEAR( NOW() ) * 10000 + MONTH( NOW()  ) * 100 + DAY( NOW() ) )"
    Public Const DB2_TIMEDEC = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    Public Const DB2_TIMEDEC0 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    Public Const DB2_TIMEDEC1 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 ) "
    Public Const F_TS As String = "'yyyy-MM-dd-00.00.00'"
    Public Const F_L As String = "'yyyy-MM-dd'"

    Public IEXP As String
    Public Const DB2_TS_EMPTY = "0001-01-01-00.00.00.000000"

    Public Enum tDato
        tNum = 1
        tStr = 2
        tXfn = 3
    End Enum
    Public fSql As String
    Public vSql As String
    Public eSql As String
    Public lastSql As String
    Public lastError As String
    Public regAfectados As Double
    Public sMsg As String

    Public sSys As String          'Nombre o Dirección IP
    Public sLib As String          'Nombre de Librería
    Public clLib As String          'Nombre de Librería para CL
    Public sProc As String
    Public bIni As Boolean

    Public sCargoWizFile As String
    Public IAL_Pdf As String
    Public PDF_Folder As String
    Public IAL_Carp_Firma As String
    Public LOG_File As String
    Public BatchApp As Boolean
    Public logDocNum As String

    Public sLanUser As String
    Public sLocalHostName As String
    Public sLocalIP As String
    Public sApp As String

    Public sUser As String
    Public sPass As String
    Public sUserShort As String

    Public sFlty As String

    Public sIpAddress As String
    Public sHostname As String

    'Para identificacion de procesos en Batch y monitoreo de funciones
    Public nBatch As Double
    Public funcionActual As String
    Public funcionAnterior As String

    Public aPermisos() As Boolean
    Public bACC_LECTURA As Boolean                      '
    Public Const ACC_SUPERUSER As Integer = 0                      '
    Public Const ACC_TOTAL As Integer = 21
    Public Const P_TOTAL As Integer = 20

    'Constantes para formatos de conversion de variables. Ej Format( pedido, K_LON_PED ) => 00345678
    Public K_LON_PED As Integer          '   Longitud Pedido
    Public K_LON_LIN As Integer          '   Longitud Pedido
    Public K_LON_PROV As Integer          '   Longitud Proveedor
    Public K_LON_CLIE As Integer          '   Longitud Cliente
    Public K_LON_PROD As Integer          '   Longitud Producto
    Public K_LON_OC As Integer          '   Longitud Orden de Compra

    'Constantes para longitud de los campos
    Public V_LON_PED As Integer          '   Longitud Pedido
    Public V_LON_LIN As Integer          '   Longitud Pedido
    Public V_LON_PROV As Integer          '   Longitud Proveedor
    Public V_LON_CLIE As Integer          '   Longitud Cliente
    Public V_LON_PROD As Integer          '   Longitud Producto
    Public V_LON_OC As Integer          '   Longitud Orden de Compra

    'Constantes para Veh Propio
    Public Const VEH_NOEXISTE As Integer = 0
    Public Const VEH_TERCERO As Integer = 1
    Public Const VEH_PROPIO As Integer = 2

    Public gsAS400 As String
    Public gsLIB As String
    Public gsUSER As String
    Public gsPASS As String
    Public gsJobD As String
    Public sCargoWiz As String
    Public bChkAcceso As Boolean

    Public gsWMS_USER As String
    Public gsWMS_PASS As String
    Public gsWMS_SERVER As String
    Public gsWMS_CAT As String

    Public gsWMA_USER As String
    Public gsWMA_PASS As String
    Public gsWMA_SERVER As String
    Public gsWMA_CAT As String

    Public SqlErr As String
    Public iDesdeCbProv As Integer
    Public sTxtCbProv As String

    Public Function ExecuteSQL(ByVal sql As String, ByVal r As Object) As ADODB.Recordset
        On Error GoTo SqlErr
        Dim txtErr As String
        Dim idErr As String

        sql = CStr(sql)
        lastSql = CStr(sql)
        ExecuteSQL = DB.Execute(sql, r)
        regAfectados = r

SqlErr:
        If Err.Number <> 0 Then
            If Not bPRUEBAS Then
                If InStr(sql, "{{") = 0 And InStr(UCase(sql), "DROP TABLE") = 0 Then
                    txtErr = Err.Number & " " & Err.Description
                    idErr = WrtSqlError(CStr(sql), txtErr)
                    If Not BatchApp Then
                        MsgBox("[" & Ceros(idErr, 5) & "]" & vbCrLf & Left(txtErr, 500) & vbCr & "----------------------------------------------------" & vbCr & CStr(sql), vbCritical, "ERROR " & idErr)
                    End If
                    End
                End If
            Else
                If InStr(UCase(sql), "DROP TABLE") = 0 Then
                    MsgBox(Err.Number & " " & Err.Description)
                    Debug.Print(sql)
                End If
            End If
        End If

    End Function

    'Ejemplo ExecuteCmd( "CALL RRNIF('2014', '01')" )
    Function ExecuteCmd(ByVal txtCmd As String)
        On Error GoTo SqlErr
        Dim txtErr As String
        Dim idErr As String
        Dim CMD As New ADODB.Command

        CMD = CreateObject("ADODB.Command")
        CMD.ActiveConnection = DB
        CMD.CommandText = txtCmd
        CMD.Execute()
        CMD = Nothing

SqlErr:
        If Err.Number <> 0 Then
            txtErr = Err.Number & " " & Err.Description
            idErr = WrtSqlError(CStr(txtCmd), txtErr)
            If Not BatchApp Then
                MsgBox("[" & Ceros(idErr, 5) & "]" & vbCrLf & Left(txtErr, 500) & vbCr & "----------------------------------------------------" & vbCr & CStr(txtCmd), vbCritical, "ERROR " & idErr)
            End If
            If Not bPRUEBAS Then End
        End If

    End Function

    Public Function CalcConsec(ByVal sID As String, ByVal TopeMax As String) As String
        Dim rs As ADODB.RecordSet
        Dim locID As Double
        Dim sql As String
        Dim sConsec As String
        Dim tMax As Double

        sql = "SELECT CCDESC FROM BSSZCC1 WHERE CCTABL = 'SECUENCE' AND CCCODE='" & sID & "'"
        rs = New ADODB.RecordSet
        tMax = val(TopeMax)

        With rs
            .CursorLocation = ADODB.CursorLocationEnum.adUseServer
            .Open(sql, DB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)
            If Not (.BOF And .EOF) Then
                locID = Val(.Fields("CCDESC").Value) + 1
                If locID > tMax Then
                    locID = 1
                End If
                sConsec = Format(locID, New String(ChrW(Len(TopeMax)), "0"))
                .Fields("CCDESC").Value = sConsec
                .Update()
                .Close()
            Else
                locID = 1
                .Close()
                sConsec = Format(1, New String(ChrW(Len(TopeMax)), "0"))
                DB.Execute(" INSERT INTO BSSZCC1 (CCID, CCTABL, CCCODE, CCDESC ) " & _
                           " VALUES( 'CC', 'SECUENCE', '" & sID & "', '" & sConsec & "' )")
            End If
        End With

        CalcConsec = sConsec
        rs = Nothing

    End Function

    Function ra()
        Debug.Print(regAfectados)
    End Function


    Function FormateaSQL(ByVal sql As String) As String
        Dim a As String

        a = UCase(sql)
        a = Replace(a, "INSERT", vbCrLf & "INSERT") & "    "
        a = Replace(a, "VALUES", vbCrLf & "VALUES") & "    "
        a = Replace(a, "SELECT", vbCrLf & "SELECT" & vbCrLf) & "    "
        a = Replace(a, "FROM", vbCrLf & "FROM " & vbCrLf & "    ")
        a = Replace(a, "INNER", vbCrLf & "INNER")
        a = Replace(a, "LEFT OUTER", vbCrLf & "LEFT OUTER")
        a = Replace(a, "WHERE", vbCrLf & "WHERE " & vbCrLf & "    ")
        a = Replace(a, "GROUP BY", vbCrLf & "GROUP BY " & vbCrLf & "    ")
        a = Replace(a, "ORDER BY", vbCrLf & "ORDER BY ")
        a = Replace(a, "HAVING", vbCrLf & "HAVING ")

        FormateaSQL = a

    End Function


    Function timeStampWin() As String

        timeStampWin = "" & Year(Now) & "-" & Month(Now) & "-" & Date.Now.Day & "-" & Hour(Now) & "." & Minute(Now) & "." & Second(Now) & ".000000"

    End Function

    Function WrtSqlError(ByVal sql As String, ByVal Descrip As String) As String
        Dim conn As New ADODB.Connection
        Dim rs As New ADODB.Recordset
        Dim r As Integer
        Dim fSql As String
        Dim vSql As String

        If bPRUEBAS Then Exit Function

        Debug.Print(Descrip)
        Debug.Print(sql)

        If gsConnString = "" Then
            gsConnString = "Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=APLLX;Password=LXAPL;Persist Security Info=True;Default Collection=" & gsLIB & ";Force Translate=" & gsForceTranslate & ";"
        End If

        On Error Resume Next
        conn.Open(gsConnString)
        If Err.Number = 0 Then
            fSql = " INSERT INTO RFLOG( "
            vSql = " VALUES ( "
            fSql = fSql & " USUARIO   , " : vSql = vSql & "'" & sUser & "', "      '//502A
            fSql = fSql & " PROGRAMA  , " : vSql = vSql & "'" & "VB." & My.Settings.CarpetaGneral & "." & App.System.Diagnostics.FileVersionInfo.GetVersionInfo(My.Settings.CarpetaGneral).FileMajorPart & "." & System.Diagnostics.FileVersionInfo.GetVersionInfo(My.Settings.CarpetaGneral).FileMinorPart & "." & System.Diagnostics.FileVersionInfo.GetVersionInfo(My.Settings.CarpetaGneral).FilePrivatePart & "." & "SQLERR" & "', "      '//502A
            fSql = fSql & " ALERT     , " : vSql = vSql & "1, "      '//1P0 
            fSql = fSql & " EVENTO    , " : vSql = vSql & "'" & Replace(Descrip, "'", "''") & "', "      '//20002A
            fSql = fSql & " FECHAWIN  , " : vSql = vSql & "'" & timeStampWin() & "', "      '//26Z
            fSql = fSql & " TXTSQL    ) " : vSql = vSql & "'" & Replace(sql, "'", "''") & "' ) "      '//5002A
            conn.Execute(fSql & vSql, r)
            fSql = "SELECT MAX(ID) FROM RFLOG "
            rs.Open(fSql, DB)
            WrtSqlError = CStr(rs(0).Value)
            rs.Close()
            rs = Nothing
            conn.Close()
        End If
        On Error GoTo 0
        conn = Nothing

    End Function



    Public Function wrtLog(ByVal txt As String, ByRef iNumLog As Integer) As String
        Dim fEntrada As String = ""
        Dim num As String = Date.Today.Month & Date.Today.Day & Date.Today.Year & Date.Today.Hour & Date.Today.Minute & Date.Today.Second


        Dim strStreamW As Stream = Nothing
        'Dim appPath As String = System.Web.HttpContext.Current.Request.ApplicationPath
        'Dim CarpetaWeb As String = HttpContext.Current.Request.MapPath(appPath)
        fEntrada = Replace(My.Settings.CarpetaGneral & "\wmslog" & num & ".txt", "\\", "\")

        If File.Exists(fEntrada) Then
            strStreamW = File.Open(fEntrada, FileMode.Open) 'Abrimos el archivo
        Else
            strStreamW = File.Create(fEntrada) ' lo creamos
        End If
        strStreamW.Close()
        Try
            ' Create an instance of StreamReader to read from a file.
            Dim sr As StreamReader = New StreamReader(fEntrada)
            Dim sb As New StringBuilder()

            sb.Append(sr.ReadToEnd())

            sb.AppendLine("SistemaDeBodega (" & My.Application.Info.Version.Major & "." & _
                   My.Application.Info.Version.Minor & "." & _
                   My.Application.Info.Version.Revision & ")")
            '  sb.AppendLine("[" & My.Settings.SERVER & "][" & My.Settings.USER & "][" & My.Settings.LIBRARY & "]")

            sb.AppendLine(txt)
            sr.Close()

            Dim sw As StreamWriter = New StreamWriter(fEntrada)
            sw.WriteLine(sb.ToString())
            sw.Close()

            Return "OK"
        Catch E As Exception
            Return E.Message
        End Try

    End Function



    Public Sub WrtTxtError(ByVal Archivo As String, ByVal Texto As String)

        wrtLog("Archivo:  " & Archivo & " Texto: " & Texto, "111")
        '     Dim strErr As String
        '     Dim nArch As Integer

        '     If Err.Description <> "" Then
        '         Texto = Texto & vbCrLf & Err.Description
        '     ElseIf lastError <> "" Then
        '         Texto = Texto & vbCrLf & lastError
        '     End If

        '     Debug.Print(Texto)

        '     If Archivo = "" Then
        '         Exit Sub
        '     End If

        '     nArch = FreeFile()
        '     On Error Resume Next



        '     nArch = FreeFile()
        '     FileOpen(nArch, Archivo, OpenMode.Output)

        '     If Err.Number <> 0 Then
        '     Open "\" & App.EXEName & ".log" For Append As #nArch
        '     End If
        '     On Error GoTo 0



        '     PrintLine(nArch, "This is a sample.")
        'Print #nArch, App.EXEName, "(" & App.Major & "." & App.Minor & "." & App.Revision & ")", Format(Now, "yyyy-MM-dd hh:mm:ss")
        ' Print #nArch, "Procedimiento: ", funcionActual, Err.Source
        ' Print #nArch, "Mensaje: ", Texto
        '     If Err.Number <> 0 Then
        '     Print #nArch, "Numero: ", Err.Number
        '     Print #nArch, "Descripcion: ", Err.Description
        '     End If
        ' Print #nArch, "Numero: ", Err.Number
        ' Print #nArch, "Descripcion: ", Err.Description
        ' Print #nArch, "*************************************************************************"
        '     Close(nArch)

        '     Debug.Print(Err.Description)
        '     Debug.Print(Texto)

    End Sub


    'ReportX()
    Function ReportX(ByVal op, ByVal Orden, ByVal sql, ByVal Id)
        Dim locSql

        locSql = Replace(sql, "'", "''")

        ExecuteSQL("DELETE FROM RFSTM WHERE SQID = " & Id & " AND SQOP = '" & op & "' AND SQORDEN = " & Orden, "")

        fSql = " INSERT INTO RFSTM( "
        vSql = " VALUES ( "
        mkSql(tNum, " SQID      ", Id, 0)          'IdSesion
        mkSql(tNum, " SQORDEN   ", Orden, 0)    'Orden
        mkSql(tStr, " SQOP      ", op, 0)       'Operacion
        mkSql(tStr, " SQVAL     ", locSql, 1)   'SQL

        ExecuteSQL(fSql & vSql, "")

    End Function

    'ReportXlsSend()
    Function ReportXlsSend(ByVal xlsSQL As String, ByVal xlsTIT As String) As String
        Dim XID As String

        XID = CalcConsec("RFREPORTX", "999999999")
        fSql = " INSERT INTO RFREPORTX( "
        vSql = " VALUES ( "
        mkSql(tNum, " XID       ", XID, 0)         '//9P0
        mkSql(tStr, " XSQL      ", Replace(xlsSQL, "'", "''"), 0)            '//10002A
        mkSql(tStr, " XTIT      ", Replace(xlsTIT, "'", "''"), 0)           '//10002A
        mkSql(tStr, " CRTUSR    ", sUser, 1)          '//10A
        ExecuteSQL(fSql & vSql, "")
        ReportXlsSend = XID

    End Function

    Public Function dv(ByVal numero)
        Dim i As Long
        Dim Cod As String
        Dim Impares As Long
        Dim Pares As Long
        Dim Total As Long

        Cod = numero
        Pares = 0 : Impares = 0

        For i = 1 To Len(CStr(numero))
            If i Mod 2 = 0 Then
                Pares = Pares + CLng(Mid(Cod, i, 1))
            Else
                Impares = Impares + CLng(Mid(Cod, i, 1))
            End If
        Next i

        Total = Pares + (3 * Impares)

        dv = 10 - (Total Mod 10)
        If dv = 10 Then dv = 0

    End Function


    Public Function logDocsAuto(ByVal Doc As String, ByVal flag As Integer, ByVal Documento As String, ByVal qConso As String, ByVal qPedid As String, ByVal qPlanilla As String, ByVal qPref As String, ByVal qFactu As String) As String

        If logDocNum = "" Then
            logDocNum = CalcConsec("RFLOGDOCA", "9999999999")
            fSql = " INSERT INTO RFLOGDOCA( "
            vSql = " VALUES ( "
            mkSql(tNum, " DOCID     ", logDocNum, 0)         '//11P0
            mkSql(tStr, " DOCPGM    ", My.Settings.CarpetaGneral, 0)           '//50O
            mkSql(tStr, " WINDAT    ", timeStampWin(), 0)          '//26Z   Fecha Windows
            mkSql(tNum, " DOCPLANI  ", Val(qPlanilla), 0)         '//6P0   Planilla
            mkSql(tStr, " DOCPREFI  ", qPref, 0)          '//2A    Pref
            mkSql(tNum, " DOCFACTU  ", Val(qFactu), 0)         '//8P0
            mkSql(tStr, " DOCDOCU   ", Documento, 0)
            mkSql(tStr, " DOCDOC    ", Doc, 1)          '//150O
            ExecuteSQL(fSql & vSql, "")
        Else
            fSql = " UPDATE RFLOGDOCA SET "
            fSql = fSql & " ENDDAT = NOW(), DOCFLAG = " & flag & ", "
            fSql = fSql & " DOCCONSO  ='" & qConso & "', "      '//6A
            fSql = fSql & " DOCPEDID  = " & Val(qPedid) & ", "        '//8P0
            fSql = fSql & " DOCPLANI  = " & Val(qPlanilla) & ", "       '//6P0   Planilla
            fSql = fSql & " DOCPREFI  ='" & qPref & "', "      '//2A    Pref
            fSql = fSql & " DOCFACTU  = " & Val(qFactu)        '//8P0
            fSql = fSql & " WHERE DOCID = " & logDocNum
            ExecuteSQL(fSql, "")
        End If


    End Function

    Public Sub logDocsAutoSQL(ByVal sql As String)
        ExecuteSQL("UPDATE RFLOGDOCA SET DOCSQL = '" & Replace(sql, "'", "''") & "' WHERE DOCID = " & logDocNum, "")
    End Sub

    Public Function CreaLoteGen(ByVal Producto As String, ByVal Lote As String)

        fSql = " INSERT INTO ILN( "
        vSql = " VALUES ( "
        fSql = fSql & " LNID      , " : vSql = vSql & "'LN', "                                      '//2A    Record ID (LN/LZ)
        fSql = fSql & " LCTNR     , " : vSql = vSql & "'S', "                                       '//1A    Reference 1
        fSql = fSql & " LLOT      , " : vSql = vSql & "'" + Lote + "', "                            '//25O   Lot Number
        fSql = fSql & " LPROD     , " : vSql = vSql & "'" + Producto + "', "                        '//35O   Item Number
        fSql = fSql & " LEXDT     , " : vSql = vSql & "99999999, "                                  '//8P0   Lot Expiration Date
        fSql = fSql & " LRET      , " : vSql = vSql & "99999999, "                                  '//8P0   Lot Retest Date
        fSql = fSql & " LMRB      , " : vSql = vSql & "'A', "                                       '//2A    Lot Status; A = Active
        fSql = fSql & " LLRD      , " : vSql = vSql & " " & DB2_DATEDEC & ", "                      '//8P0   Last Trans Date
        fSql = fSql & " LNMFGD    , " : vSql = vSql & " " & DB2_DATEDEC & ", "                      '//8P0   Date Manufactured
        fSql = fSql & " LSTDTR    , " : vSql = vSql & " " & DB2_DATEDEC & ", "                      '//8P0   Availability Date
        fSql = fSql & " LQAAP     , " : vSql = vSql & " " & DB2_DATEDEC & ", "                      '//8P0   Lot QA Approval Date
        fSql = fSql & " LNUOM     , " : vSql = vSql & "'RL', "                                      '//2A    Unit of Measure
        fSql = fSql & " LNCBY     , " : vSql = vSql & "'SISTEMA', "                                 '//10A   Created by User
        fSql = fSql & " LNCDT     , " : vSql = vSql & " " & DB2_DATEDEC + ", "                      '//8P0   Created on Date
        fSql = fSql & " LNCTM     , " : vSql = vSql & " " & DB2_TIMEDEC0 + ", "                     '//6P0   Created at Time
        fSql = fSql & " LNLPGM    ) " : vSql = vSql & "'INV130D2') "                                '//10A   Last Maintenance Program
        ExecuteSQL(fSql & vSql, "")

    End Function



    Function CodeCB(ByVal valor As String, ByVal sep As String) As String
        Dim aCode

        aCode = Split(valor, IIf(sep = "", " ", sep))
        CodeCB = Trim(aCode(0))

    End Function

    Function DescCB(ByVal valor As String, ByVal sep As String) As String

        DescCB = Trim(Mid(valor, InStr(1, valor, IIf(sep = "", " ", sep)) + 1))

    End Function



    Public Function v(ByVal DATO As Object) As String

        If String.IsNullOrEmpty(DATO.Trim) Then
            v = ""
        Else
            v = Trim(CStr(DATO))
        End If

    End Function




    Public Function vn(ByVal vlr) As Double
        vn = Replace(vlr, ",", ".")
    End Function


    Public Function CopieArchivo(ByVal Origen As String, ByVal Destino As String) As Boolean
        Dim fs

        fs = CreateObject("Scripting.FileSystemObject")
        fs.CopyFile(Origen, Destino)

        If Err.Number <> 0 Then
            MsgBox(Origen & vbCr & Destino & vbCr & Err.Number & vbCr & Err.Description, vbCritical, "Copiando Archivo")
            CopieArchivo = False
        Else
            CopieArchivo = True
        End If
        fs = Nothing

    End Function

    Public Function LogConso(ByVal Conso, ByVal Fecha, ByVal Reporte, ByVal colRRCCT)
        Dim fSql As String
        Dim vSql As String
        Dim rs As New ADODB.Recordset
        Dim r As Integer
        Dim campoRRCCT As String

        fSql = "UPDATE RHCC H SET HSTS = ( SELECT VESTAD FROM VSPESTAD WHERE H.HESTAD = CCCODE )"
        fSql = fSql & " WHERE HCONSO = '" & Conso & "'"
        ExecuteSQL(fSql, "")

        fSql = "INSERT INTO RRCC("
        vSql = "VALUES( "
        fSql = fSql & "RCONSO, " : vSql = vSql & "'" & Conso & "'" & ", "
        fSql = fSql & "RSTSC, "
        vSql = vSql & "( SELECT IFNULL(SUM(HESTAD), 9) FROM RHCC WHERE HCONSO='" & Conso & "' )" & ", "
        fSql = fSql & "RTREP, " : vSql = vSql & " " & "2" & " " & ", "
        fSql = fSql & "RFECREP, " : vSql = vSql & " NOW(), "
        fSql = fSql & "RREPORT, " : vSql = vSql & "'" & Trim(Reporte) & "'" & ", "
        fSql = fSql & "RAPP, " : vSql = vSql & "'" & sApp & "'" & ", "
        fSql = fSql & "RCRTUSR) " : vSql = vSql & "'" & sUser & "'" & ") "
        ExecuteSQL(fSql & vSql, "")

        If colRRCCT <> -1 And colRRCCT <> 0 Then
            If colRRCCT = 0 Then
                fSql = "SELECT CCALTC FROM RHCC, VSPESTAD WHERE HESTAD=CCCODE AND HCONSO='" & Conso & "'"
                rs.Open(fSql, DB)
                campoRRCCT = "RFEC" & rs("CCALTC").Value
                rs.Close()
            Else
                campoRRCCT = "RFEC" & Ceros(colRRCCT, 2)
            End If
            rs.Open("SELECT RCONSO FROM RRCCT WHERE RCONSO='" & Conso & "'", DB)
            If rs.EOF Then
                DB.Execute("INSERT INTO RRCCT( RCONSO ) VALUES('" & Conso & "')")
            End If
            rs.Close()
            fSql = "UPDATE RRCCT SET "
            fSql = fSql & campoRRCCT & " = NOW() "
            fSql = fSql & " WHERE RCONSO='" & Conso & "' AND YEAR(" & campoRRCCT & ")=1"
            ExecuteSQL(fSql, r)
        End If

        rs = Nothing

    End Function

    Sub LeeParametros(ByVal NomApp As String)
        Dim nArch As Integer
        Dim strArch As String
        Dim strLinea As String
        Dim P
        Dim i As Integer
        Dim msg As String
        Dim conn As New ADODB.Connection
        Dim rs As New ADODB.Recordset
        Dim DATO As String
        Dim bChkConn As Boolean

        ReDim aPermisos(ACC_TOTAL)
        For i = 0 To 21
            aPermisos(i) = False
        Next

        sApp = NomApp
        nArch = FreeFile()
        strArch = My.Settings.CarpetaGneral & "\" & My.Settings.CarpetaGneral & ".ini"
        bChkConn = True
        bChkAcceso = True



        FileOpen(nArch, strArch, OpenMode.Input)
        'Open strArch For Input As nArch       
        Do While Not EOF(nArch)
            Input(nArch, strLinea)
            strLinea = Replace(strLinea, vbTab, " ")
            P = Split(strLinea, "=")
            If UBound(P) = 1 Then
                Select Case UCase(Trim(P(0)))
                    Case "AS400" : gsAS400 = Trim(P(1))
                    Case "LIB" : gsLIB = Trim(P(1))
                    Case "CARGOWIZ" : sCargoWiz = Trim(P(1))
                    Case "PRUEBAS" : bPRUEBAS = (Trim(P(1)) = "1") Or (Trim(P(1)) = "SI")
                    Case "CARPETA_IAL" : IAL_Pdf = Trim(P(1))
                    Case "CARPETA_IMG" : IAL_Carp_Firma = Trim(P(1))
                    Case "CARPETA_SITIO" : CARPETA_SITIO = Trim(P(1))
                    Case "CHK_ACCESO" : bChkAcceso = (UCase(Trim(P(1))) = "SI")
                    Case "LX" : bImgLX = (UCase(Trim(P(1))) = "SI")
                End Select
            ElseIf UBound(P) > 1 Then
                If UCase(Trim(P(0))) = "CONEXION" Then
                    gsConnString = ConexionFija(P)
                    bChkConn = False
                End If
            End If
        Loop
        'Close(nArch)

        bImgLX = bImgLX And (gsLIB = "ERPLX834F")

        '

        If bChkConn Then
            If gsAS400 = "" Then
                msg = msg & "Error en servidor AS400" & vbCr
            End If

            If gsLIB = "" Then
                msg = msg & "Error en Biblioteca" & vbCr
            End If

            If msg <> "" Then
                MsgBox(msg, vbCritical, sApp)
                End
            End If

            sLib = gsLIB & "."
            clLib = gsLIB & "/"
        End If

        If Dir("C:\Program Files\Google\Chrome\Application\chrome.exe") <> "" Then
            IEXP = "C:\Program Files\Google\Chrome\Application\chrome.exe "
        ElseIf Dir("C:\Program Files (x86)\Google\Chrome\Application\chrome.exe") <> "" Then
            IEXP = "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe "
        Else
            IEXP = "C:\Archivos de programa\Internet Explorer\iexplore.exe "
        End If


    End Sub

    Function LeeParametrosEsp(ByVal NomApp As String, ByVal parametro As String) As String
        Dim nArch As Integer
        Dim strArch As String
        Dim strLinea As String
        Dim P

        sApp = NomApp
        nArch = FreeFile()

        strArch = My.Settings.CarpetaGneral
        strArch = strArch & "\LibrosMyB.ini"
        '     nArch = FreeFile()
        '     FileOpen(nArch, Archivo, OpenMode.Output)

        FileOpen(nArch, strArch, OpenMode.Input)
        ' Open strArch For Input As nArch        'Cargar Direcciones Bases de Datos
        Do While Not EOF(nArch)
            Input(nArch, strLinea)
            strLinea = Replace(strLinea, vbTab, " ")
            P = Split(strLinea, "=")
            If UBound(P) = 1 Then
                If UCase(Trim(P(0))) = parametro Then
                    LeeParametrosEsp = Trim(P(1))
                End If
            End If
        Loop
        FileClose(nArch)

    End Function
    Function LeeParametrosWeb(ByVal Archivo)
        Dim nArch As Integer
        Dim strLinea As String
        Dim P() As String

        nArch = FreeFile()
        nArch = FreeFile()
        FileOpen(nArch, Archivo, OpenMode.Input)

        'Open Archivo For Input As nArch        'Cargar Direcciones Bases de Datos
        Do While Not EOF(nArch)
            Input(nArch, strLinea)
            strLinea = Replace(strLinea, vbTab, " ")
            P = Split(strLinea, "=")
            If UBound(P) > 1 Then
                P(1) = Replace(P(1), vbTab, " ")
                P(1) = Replace(P(1), """", "")
                P(2) = Replace(P(2), "/", "")
                P(2) = Replace(P(2), ">", "")
                P(2) = Replace(P(2), " ", "")
                P(2) = Replace(P(2), """", "")

                Select Case UCase(Trim(P(1)))
                    Case "AS400_SERVER VALUE"
                        gsAS400 = Trim(P(2))
                    Case "AS400_LIB VALUE"
                        gsLIB = Trim(P(2))
                        sLib = gsLIB & "."
                        clLib = gsLIB
                    Case "AS400_USER VALUE"
                        gsUSER = Trim(P(2))
                    Case "AS400_PASS VALUE"
                        gsPASS = Trim(P(2))

                End Select
            End If
        Loop
        ServerRaiz = getLXParamIni("SERVERRAIZ", "CCDESC", gsUSER, gsPASS, gsLIB, gsAS400)
        CARPETA_IMG = getLXParamIni("CARPETA_IMG", "CCDESC", gsUSER, gsPASS, gsLIB, gsAS400)
        C_UPLOAD_DIR = getLXParamIni("C_UPLOAD_DIR", "CCDESC", gsUSER, gsPASS, gsLIB, gsAS400)
        CARPETA_LOG = getLXParamIni("CARPETA_LOG", "CCDESC", gsUSER, gsPASS, gsLIB, gsAS400)

        'Close(nArch)


    End Function


    Function getLXParamIni(prm, campo, usr, pass, libb, as400)
        Dim rs As New ADODB.Recordset
        Dim DB_LOC As New ADODB.Connection
        DB_LOC.Open("Provider=IBMDA400.DataSource;Data Source=" & as400 & ";User ID=" & usr & ";Password=" & pass & ";Persist Security Info=True;Default Collection=" & libb & ";Force Translate=37;")

        If campo = "" Then
            campo = "CCDESC"
        End If
        rs.Open("SELECT " & campo & " AS DATO FROM BSSPARAM WHERE CCTABL='LXLONG' AND CCCODE='" & prm & "'", DB_LOC)
        If Not rs.EOF Then
            getLXParamIni = rs("DATO").Value
        End If
        rs.Close()
        rs = Nothing
        DB_LOC.Close()
    End Function

    Function getLXParam(ByVal prm As String, ByVal Campo As String) As String
        Dim rs As New ADODB.Recordset

        If Campo = "" Then
            Campo = "CCDESC"
        End If
        rs.Open("SELECT " & Campo & " AS DATO FROM BSSZCC1 WHERE CCTABL='LXLONG' AND CCCODE='" & prm & "'", DB)
        If Not rs.EOF Then
            getLXParam = rs("DATO").Value
        End If
        rs.Close()
        rs = Nothing

    End Function

    Function LXParam() As Boolean
        Dim rs As New ADODB.Recordset
        Dim DATO As String
        Dim msg As String
        Dim DB_LOC As New ADODB.Connection

        If Not bChkAcceso Then
            LXParam = True
            Exit Function
        End If

        DB_LOC.Open("Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=APLLX;Password=LXAPL;Persist Security Info=True;Default Collection=" & gsLIB & ";Force Translate=" & gsForceTranslate & ";")

        rs.Open("SELECT * FROM BSSZCC1 WHERE CCTABL='LXLONG'", DB_LOC)
        Do While Not rs.EOF
            DATO = Trim(rs("CCDESC").Value)
            Select Case UCase(Trim(rs("CCCODE").Value))
                Case "LXUSER"
                    If bImgLX Or BatchApp Or sLib <> "ERPLX834F" Then
                        gsUSER = DATO
                        LXParam = True
                    End If
                Case "LXLIB" : gsLIB = DATO
                Case "LXPASS" : gsPASS = DATO
                Case "LXJOBD" : gsJobD = DATO
                Case "PEDIDO" : K_LON_PED = CInt(DATO) : V_LON_PED = Val("0" & DATO)
                Case "LINEA" : K_LON_LIN = CInt(DATO) : V_LON_LIN = Val("0" & DATO)
                Case "PROVEEDOR" : K_LON_PROV = CInt(DATO) : V_LON_PROV = Val("0" & DATO)
                Case "CLIENTE" : K_LON_CLIE = CInt(DATO) : V_LON_CLIE = Val("0" & DATO)
                Case "PRODUCTO" : K_LON_PROD = CInt(DATO) : V_LON_PROD = Val("0" & DATO)
                Case "OCOMPRA" : K_LON_OC = CInt(DATO) : V_LON_OC = Val("0" & DATO)
                Case "WMSSERV" : gsWMS_SERVER = DATO
                Case "WMSUSER" : gsWMS_USER = DATO
                Case "WMSPASS" : gsWMS_PASS = DATO
                Case "WMSCATALOG" : gsWMS_CAT = DATO
                Case "WMASERV" : gsWMA_SERVER = DATO
                Case "WMAUSER" : gsWMA_USER = DATO
                Case "WMAPASS" : gsWMA_PASS = DATO
                Case "WMACATALOG" : gsWMA_CAT = DATO
            End Select
            rs.MoveNext()
        Loop
        rs.Close()
        rs = Nothing

        RevisaParam(V_LON_CLIE, "LONG CLIENTE", msg)
        RevisaParam(V_LON_PROD, "LONG PRODUCTO", msg)
        RevisaParam(V_LON_PED, "LONG PEDIDO", msg)
        RevisaParam(V_LON_LIN, "LONG LINE", msg)
        RevisaParam(V_LON_PROV, "LONG PROVEEDOR", msg)

        If msg <> "" Then
            If Not BatchApp Then
                MsgBox(msg, vbCritical, sApp)
            Else
                WrtTxtError(LOG_File, msg)
            End If
            End
        End If

    End Function

    Function HTTP_Dir(ByVal pag As String, ByVal Param As String) As String
        Dim rs As New ADODB.Recordset

        rs.Open("SELECT * FROM BSSZCC1 WHERE CCTABL='RFVBVER' AND CCCODE='PAGTMS'", DB)
        If Not rs.EOF Then
            HTTP_Dir = rs("CCNOT1").Value
            rs.Close()
            rs.Open("SELECT * FROM BSSZCC1 WHERE CCTABL='RFPAGWEB' AND CCCODE='" & pag & "'", DB)
            If Not rs.EOF Then
                HTTP_Dir = HTTP_Dir & rs("CCNOT1").Value
                If Param <> "" Then
                    HTTP_Dir = HTTP_Dir & "?" & Param
                End If
            End If
        End If
        rs.Close()
        rs = Nothing

    End Function

    Sub RevisaParam(ByVal Param As Integer, ByVal qParam As String, ByVal msg As String)

        If Param = 0 Then
            msg = msg & "Error en el parametro " & qParam & vbCr
        End If

    End Sub

    Function Nom_App() As String
        Dim rs As New ADODB.Recordset

        rs.Open("SELECT * FROM BSSZCC1 WHERE CCTABL='RFVBVER' AND CCCODE='" & sApp & "'", DB)
        Nom_App = rs("CCDESC").Value
        rs.Close()
        rs = Nothing

    End Function


    Function BSSZCC1Desc(ByVal Tabla As String, ByVal codigo As String) As String
        Dim rs As New ADODB.Recordset

        rs.Open("SELECT * FROM BSSZCC1 WHERE CCTABL='" & Tabla & "' AND CCCODE='" & codigo & "'", DB)
        If Not rs.EOF Then
            BSSZCC1Desc = Replace(rs("CCDESC").Value, "'", "")
        End If
        rs.Close()
        rs = Nothing

    End Function

    Function Buscar(ByVal sql As String) As String
        Dim rs As New ADODB.Recordset

        rs.Open(sql, DB)
        If Not rs.EOF Then
            Buscar = CStr(rs(0).Value)
        End If
        rs.Close()
        rs = Nothing

    End Function


    Function mkConnectionString(ByVal srv As String, ByVal Usr As String, ByVal pass As String, ByVal Libs As String)
        Dim g As String

        If EsLxBpcs() Then
            g = makeConnectionString("IBMDA400.DataSource.1", srv, Usr, pass, Libs, "")
        Else
            g = gsConnString
        End If

        mkConnectionString = g

    End Function

    Function makeConnectionString(ByVal Prov As String, ByVal srv As String, ByVal Usr As String, ByVal pass As String, ByVal libS As String, ByVal catalog As String)
        Dim g As String

        If gsForceTranslate = "" Then gsForceTranslate = "37"

        g = ""
        g = g & "Provider=" & Prov & ";"
        g = g & "Password=" & pass & ";"
        g = g & "User ID=" & Usr & ";"
        g = g & "Data Source=" & srv & ";"
        If libS <> "" Then  ' If lib <> "" Then 
            g = g & "Default Collection=" & libS & ";"
            g = g & "Force Translate=" & gsForceTranslate & ";"
            If APP_NAME <> "" Then
                g = g & "Application Name=" & APP_NAME & ";"
            End If
        End If
        If catalog <> "" Then
            If InStr(UCase(Prov), "IBM") > 0 Then
                g = g & "Persist Security Info=False;"
                g = g & "Initial Catalog=" & catalog & ";"
                g = g & "Force Translate=" & gsForceTranslate & ";"
            Else
                g = g & "Persist Security Info=True;"
                g = g & "Database=" & catalog & ";"
            End If
        End If
        If InStr(UCase(Prov), "IBM") > 0 Then
            If bDateToDate Then
                g = g & "Convert Date Time To Char=FALSE"
            End If
        End If

        makeConnectionString = g

    End Function

    Function QueLib() As String
        Dim aPrm() As String
        Dim i As Integer

        aPrm = Split(DB.ConnectionString, ";")
        For i = 0 To UBound(aPrm)
            Debug.Print(aPrm(i))
        Next

    End Function

    Function getConnParam(ByVal gs As String, ByVal P As String) As String
        Dim aGS() As String
        Dim aPrm() As String
        Dim i As Integer

        aGS = Split(gs, ";")
        getConnParam = ""
        For i = 0 To UBound(aGS)
            aPrm = Split(aGS(i), "=")
            If UBound(aPrm) = 1 Then
                If Trim(UCase(P)) = Trim(UCase(aPrm(0))) Then
                    getConnParam = aPrm(1)
                    Exit For
                End If
            End If
        Next

    End Function

    Public Sub Bitacora(ByVal Programa As String, ByVal Log As String)
        Dim Identificacion As String
        Dim Linea As String

        Identificacion = sUser & "." & sLanUser & "." & sLocalHostName & "." & sLocalIP
        Do While True
            'Linea = CampoAlfa(Left(Log, 2000))
            Linea = Replace(Left(Log, 14900), "'", "''")
            On Error Resume Next
            fSql = " INSERT INTO RFLOG( "
            vSql = " VALUES ( "
            mkSql(tStr, " USUARIO   ", Identificacion, 0)          '//502A
            mkSql(tStr, " PROGRAMA  ", "VB." & App.EXEName & "." & App.Major & "." & App.Minor & "." & App.Revision & "." & Programa, 0)          '//502A
            mkSql(tStr, " FECHAWIN  ", timeStampWin(), 0)          '//26Z
            mkSql(tStr, " EVENTO    ", Linea, 1)                                                                                                  '//15002A
            ExecuteSQL(fSql & vSql, "")
            On Error GoTo 0
            Log = Mid(Log, 14901)
            If Log = "" Then
                Exit Do
            End If
        Loop


    End Sub

    'setFuncActiva()
    Public Function setFuncActiva(ByVal txt As String)

        If txt <> "" Then
            funcionAnterior = funcionActual
            funcionActual = txt
        Else
            funcionActual = funcionAnterior
        End If

    End Function


    Public Function CheckVer(ByVal Aplicacion As String) As Boolean
        Dim rs As ADODB.Recordset
        Dim lVer As String
        Dim msg As String
        Dim NomApp As String

        rs = New ADODB.Recordset

        lVer = "No hay registro"

        With rs
            rs.Open("SELECT CCSDSC, CCUDC1, CCNOT2, CCDESC  FROM BSSZCC1 WHERE CCTABL='RFVBVER' AND UPPER(CCCODE) = '" & UCase(Aplicacion) & "'", DB)
            If Not (.EOF And .BOF) Then
                lVer = Trim(.Fields("CCSDSC").Value)
                NomApp = .Fields("CCSDSC").Value
                If InStr(lVer, "*NOCHK") > 0 Then
                    CheckVer = True
                ElseIf InStr(lVer, "(" & App.Major & "." & App.Minor & "." & App.Revision & ")") > 0 Then
                    CheckVer = True
                End If
                If CheckVer And rs("CCUDC1").Value = 0 Then
                    msg = "La aplicacion " & NomApp & " no se puede usar en este momento." & vbCr
                    msg = msg & "Razon: " & vbCr
                    msg = msg & "=========================================" & vbCr
                    If Trim(rs("CCNOT2").Value) = "" Then
                        msg = msg & "Aplicacion en Mantenimiento." & vbCr
                    Else
                        msg = msg & rs("CCNOT2").Value & vbCr
                    End If
                    msg = msg & "=========================================" & vbCr
                    If Not BatchApp Then
                        MsgBox(msg, vbExclamation, "Seguimiento a Pedidos")
                    Else
                        SqlErr = msg
                    End If
                    CheckVer = False
                    .Close()
                    Exit Function
                End If
                .Close()
            End If
        End With

        rs = Nothing
        'CORREGIR
        '   If Not CheckVer Then
        '      msg = "Aplicacion " & Aplicacion & vbCr & _
        '             "=========================================" & vbCr & _
        '             "Versión incorrecta del programa." & vbCr & _
        '             "Versión Registrada:" & vbTab & lVer & vbCr & _
        '             "Versión Actual:" & vbTab & "(" & App.Major & "." & App.Minor & "." & App.Revision & ")" & vbCr & _
        '             "Comuníquese con Sistemas"
        '      Debug.Print Aplicacion, lVer, "(" & App.Major & "." & App.Minor & "." & App.Revision & ")"
        '      If Not BatchApp Then
        '        MsgBox msg
        '      Else
        '        SqlErr = msg
        '      End If
        '   End If

        If InStr(1, "" & Val("0.7"), ",") > 0 Then
            If Not BatchApp Then
                MsgBox("La configuración regional de su computador" & vbCr & _
                       "está con la coma (,) como separador decimal." & vbCr & _
                       "Debe estar el punto (.) como separador decimal." & vbCr & _
                       "Esto puede causar problemas cuando se usan decimales.", vbCritical, "PROBLEMAS EN CONFIGURACION")
            Else
                SqlErr = "Error en configuracion regional del pc."
            End If
            CheckVer = False
        End If

    End Function

    Public Sub ChkFolder(ByVal sFolder As String)
        Dim fso
        Dim aFolder
        Dim i As Integer
        Dim lFolder As String
        Dim sep As String

        fso = CreateObject("Scripting.FileSystemObject")
        aFolder = Split(sFolder, "\")
        For i = 0 To UBound(aFolder)
            lFolder = lFolder & sep & aFolder(i)
            If Not fso.FolderExists(lFolder) Then
                Call MkDir(lFolder)
            End If
            sep = "\"
        Next

    End Sub


    Public Function tsAS400(ByVal Fecha As Date) As String
        tsAS400 = Format(Fecha, "yyyy-mm-dd-hh.mm.ss.000000")
    End Function

    Public Function dtAS400(ByVal Fecha As String) As String
        dtAS400 = Left(Fecha, 10) & " " & Replace(Mid(Fecha, 12, 5), ".", ":")
    End Function

    Public Function dtFromAS400(ByVal Fecha As String) As Date
        Dim aFecha() As String
        Dim lFecha As String

        lFecha = Left(Fecha, 10) & "," & Replace(Mid(Fecha, 12, 5), ".", ",")
        lFecha = Replace(lFecha, "-", ",")
        'Debug.Print lFecha

        aFecha = Split(lFecha, ",")

        dtFromAS400 = DateSerial(CInt(aFecha(0)), CInt(aFecha(1)), CInt(aFecha(2)))
        dtFromAS400 = dtFromAS400 & " " & TimeSerial(CInt(aFecha(3)), CInt(aFecha(4)), 0)

    End Function

    Function DateDec(ByVal Fecha)
        DateDec = Format(Fecha, "yyyymmdd")
    End Function

    Function TimeDec(ByVal Fecha)
        TimeDec = Format(Fecha, "hhnnss")
    End Function

    Function HoraFromDEC(ByVal Fecha As String) As Date
        Dim fec As Date
        fec = DateSerial(Left(Fecha, 4), Mid(Fecha, 5, 2), Mid(Fecha, 7, 2))
        HoraFromDEC = fec & " " & TimeSerial(Mid(Fecha, 9, 2), Mid(Fecha, 11, 2), Mid(Fecha, 13, 2))
    End Function

    Function LastDayMonth(ByVal Fecha) As Date

        LastDayMonth = DateSerial(Year(Fecha), Month(Fecha), 1)
        LastDayMonth = DateAdd("m", 1, LastDayMonth)
        LastDayMonth = DateAdd("d", -1, LastDayMonth)

    End Function



    Function AhoraAS400() As Date
        Dim rs As New ADODB.Recordset

        fSql = "SELECT  CURRENT TIMESTAMP FROM SYSIBM.SYSDUMMY1"
        rs.Open(fSql, DB)
        AhoraAS400 = rs(0).Value
        rs.Close()
        rs = Nothing


    End Function

    Public Function validaAcceso(ByVal Usr As String, ByVal pass As String) As Boolean
        Dim rs As New ADODB.Recordset

        wrtLog("validaAcceso Ingreso", "111")

        If Not BatchApp Then
            Cursor.Current = Cursors.WaitCursor
            PutUserLogin(Usr, gsLIB)
        End If

        validaAcceso = False
        If EsLxBpcs() Then
            sUser = Trim(UCase(Usr))
            sPass = Trim(UCase(pass))
        End If

        wrtLog("validaAcceso Abrir Conexion", "111")

        DB = New ADODB.Connection
        DB.CursorLocation = ADODB.CursorLocationEnum.adUseServer
        On Error Resume Next
        DB.Open(mkConnectionString(gsAS400, sUser, sPass, gsLIB))
        If Err.Number <> 0 Then
            If Not BatchApp Then
                MsgBox(Err.Description, vbCritical, "Conexion AS400")
            Else
                WrtTxtError(LOG_File, "Conexion AS400")
            End If
            Exit Function
        End If
        On Error GoTo 0
        wrtLog(DB.ConnectionString, "111")
        wrtLog("validaAcceso Validacion", "111")
        If bChkAcceso Then
            If Not ChkAccess() Then
                If Not BatchApp Then
                    MsgBox("Usuario no tiene acceso.", vbInformation, "Ingreso")
                Else
                    WrtTxtError(LOG_File, "Usuario no tiene acceso.")
                End If
            Else
                validaAcceso = True
            End If
        Else
            validaAcceso = True
        End If

        If DB.State = 1 Then
            DB.Close()
        End If

        If validaAcceso Then
            wrtLog("validaAcceso Validacion usuario", "111")
            'Si se especifico usuario en el archivo de parametros, utiliza el usuario
            If gsUSER = "" Then
                gsConnString = mkConnectionString(gsAS400, sUser, sPass, gsLIB)
            Else
                gsConnString = mkConnectionString(gsAS400, gsUSER, gsPASS, gsLIB)
            End If

            cmdConnString = gsConnString & ";"

            DB.Open(gsConnString)
            If LXParam() Then
                DB.Close()
                gsConnString = mkConnectionString(gsAS400, gsUSER, gsPASS, gsLIB)
                DB.Open(gsConnString)
            End If


            wrtLog("validaAcceso Validacion IdSesion", "111")
            IdSesion = 1
            wrtLog("validaAcceso Validacion IdSesionmmm" & IdSesion, "111")
            If LeeParametrosEsp(APP_NAME, "EXCELCONN") = "SI" Then
                wrtLog("validaAcceso Validacion LeeParametrosEsp", "111")
                SaveSetting("BSSApp", "ExcelVBA", "gsConnString", gsConnString)
            Else
                On Error Resume Next
                wrtLog("validaAcceso Validacion Error", "111")
                DeleteSetting("BSSApp", "ExcelVBA", "gsConnString")
                On Error GoTo 0
            End If
        End If

        If Not BatchApp Then
            'Me.Cursor = System.Windows.Forms.Cursors.Default
        End If

    End Function

    Function Conecte()
        If DB.State = 0 Then
            setDefaultConnection()
        End If
    End Function
    Function setDefaultConnection()

        LeeParametros(APP_NAME)
        If MsgBox("Conexion Utilizando " & gsLIB, vbYesNo, "Conexion") = vbNo Then
            End
        End If
        DB.Open("Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=APLLX;Password=LXAPL;Persist Security Info=True;Default Collection=" & gsLIB & ";Force Translate=" & gsForceTranslate & ";")
        LXParam()
        validaAcceso(gsUSER, gsPASS)

    End Function

    Public Function ChkAccess() As Boolean
        Dim rs As New ADODB.Recordset
        Dim sql As String
        Dim i As Integer


        If EsLxBpcs() Then
            sUserShort = NombreAbr(sUser, sUserShort)
            If sUserShort = "" Then
                ChkAccess = False
                Exit Function
            End If

            'Si encuentra la aplicacion
            rs.Open("SELECT * FROM RCAA WHERE AAPP='" & sApp & "'", DB)
            If Not rs.EOF Then
                rs.Close()
                'Busca acceso del usuario
                sql = "SELECT ACC00, ACC01, ACC02, ACC03, ACC04, ACC05, ACC06, ACC07,"
                sql = sql & " ACC08, ACC09, ACC10, ACC11, ACC12, ACC12, ACC13, ACC14, "
                sql = sql & " ACC15, ACC16, ACC17, ACC18, ACC19, ACC20 FROM RCAA "
                sql = sql & " WHERE ASTS = 'A' AND AAPP='" & sApp & "' AND AUSR='" & sUser & "'"
                rs.Open(sql, DB)
                ChkAccess = False
                If Not rs.EOF Then
                    ChkAccess = True
                    For i = 0 To 20
                        aPermisos(i) = rs(i).Value = "X"
                    Next
                Else
                    rs.Close()
                    sql = "SELECT * FROM RCAA "
                    sql = sql & " WHERE ASTS = 'A' AND AAPP='" & sApp & "' AND AUSR='*ALL'"
                    rs.Open(sql, DB)
                    If Not rs.EOF Then
                        ChkAccess = True
                    End If

                    '            ElseIf EsLxBpcs() Then
                    '                sql = " INSERT INTO RCAA( ASTS, AAPP, AUSR )"
                    '                sql = sql & " SELECT DISTINCT 'A', '" & sApp & "',"
                    '                sql = sql & " X3USER FROM ZX3L01 WHERE X3PROG = 'ORDPDESP'"
                    '                sql = sql & " AND X3USER='" & sUser & "'"
                    '                ChkAccess = True
                    '                ExecuteSQL sql
                End If
            Else
                ChkAccess = True
            End If
        Else
            rs.Open("SELECT * FROM RCAU WHERE UUSR='" & sUser & "' AND UPASS='" & sPass & "'", DB)
            If Not rs.EOF Then
                ChkAccess = True
            End If
            rs.Close()
            sql = "SELECT ACC00, ACC01, ACC02, ACC03, ACC04, ACC05, ACC06, ACC07,"
            sql = sql & " ACC08, ACC09, ACC10, ACC11, ACC12, ACC12, ACC13, ACC14, "
            sql = sql & " ACC15, ACC16, ACC17, ACC18, ACC19, ACC20 FROM RCAA "
            sql = sql & " WHERE ASTS = 'A' AND AAPP='" & sApp & "' AND AUSR='" & sUser & "'"
            rs.Open(sql, DB)
            ChkAccess = False
            If Not rs.EOF Then
                ChkAccess = True
                For i = 0 To 20
                    aPermisos(i) = rs(i).Value = "X"
                Next
            End If
        End If

        If bACC_LECTURA Then
            ChkAccess = True
        End If
        aPermisos(21) = ChkAccess
        rs.Close()
        rs = Nothing

    End Function

    'NombreAbr()
    Function NombreAbr(ByVal Usr, ByVal n_abrev) As String
        Dim rs, rs1, i, nomA

        If n_abrev <> "" Then
            NombreAbr = n_abrev
            Exit Function
        End If

        rs = ExecuteSQL("SELECT * FROM RCAU WHERE UTIPO='TM' AND UUSR='" & Usr & "'", "")
        If rs.EOF Then
            NombreAbr = ""
            rs.Close()
            rs = Nothing
            Exit Function
        Else
            NombreAbr = Trim(rs("UUSRABR"))
        End If
        If rs("UUSRABR") = "" Then
            i = 0
            Do While True
                i = i + 1
                If i = 100 Then
                    NombreAbr = "ERR"
                    Exit Do
                End If
                nomA = Left(UCase(Usr), 2)
                rs1 = ExecuteSQL("SELECT * FROM RCAU WHERE UTIPO='TM' AND UUSRABR='" & nomA & Ceros(i, 2) & "'", "")
                If rs1.EOF Then
                    ExecuteSQL("UPDATE RCAU SET UUSRABR='" & nomA & Ceros(i, 2) & "' WHERE UTIPO='TM' AND UUSR='" & Usr & "'", "")
                    NombreAbr = nomA & Ceros(i, 2)
                    Exit Do
                End If
                rs1.Close()
            Loop
            rs1.Close()
        End If
        rs.Close()
        rs = Nothing

    End Function


    Public Function SoloPedids(ByVal DATO As String, ByVal sep As String) As String
        Dim i As Integer
        Dim Ascii As Integer

        DATO = Replace(DATO, " ", "")
        For i = 1 To Len(DATO)
            Ascii = Asc(Mid(DATO, i, 1))
            If sep <> "" Then
                If InStr(1, sep, Chr(Ascii)) > 0 Then
                    SoloPedids = SoloPedids & Chr(Ascii)
                End If
            End If
            If Ascii >= Asc("0") And Ascii <= Asc("9") Then
                SoloPedids = SoloPedids & Chr(Ascii)
            Else
                SoloPedids = SoloPedids & ""
            End If
        Next
        SoloPedids = Replace(SoloPedids, ",", ", ")

    End Function

    Public Function CampoAlfa(ByVal DATO As String, ByVal Largo As Integer) As String
        Dim i As Integer
        Dim Ascii As Integer

        For i = 1 To Len(DATO)
            Ascii = Asc(Mid(DATO, i, 1))
            If InStr(1, " !@#$&*()-_=+{}[]\|:;,.?/<>""", Chr(Ascii)) > 0 Then
                CampoAlfa = CampoAlfa & Chr(Ascii)
            ElseIf Chr(Ascii) = "á" Then
                CampoAlfa = CampoAlfa & "a"
            ElseIf Chr(Ascii) = "é" Then
                CampoAlfa = CampoAlfa & "e"
            ElseIf Chr(Ascii) = "í" Then
                CampoAlfa = CampoAlfa & "i"
            ElseIf Chr(Ascii) = "ó" Then
                CampoAlfa = CampoAlfa & "o"
            ElseIf Chr(Ascii) = "ú" Then
                CampoAlfa = CampoAlfa & "u"
            ElseIf Chr(Ascii) = "Á" Then
                CampoAlfa = CampoAlfa & "A"
            ElseIf Chr(Ascii) = "É" Then
                CampoAlfa = CampoAlfa & "E"
            ElseIf Chr(Ascii) = "Í" Then
                CampoAlfa = CampoAlfa & "I"
            ElseIf Chr(Ascii) = "Ó" Then
                CampoAlfa = CampoAlfa & "O"
            ElseIf Chr(Ascii) = "Ú" Then
                CampoAlfa = CampoAlfa & "U"
            ElseIf Chr(Ascii) = "ñ" Then
                CampoAlfa = CampoAlfa & "n"
            ElseIf Chr(Ascii) = "Ñ" Then
                CampoAlfa = CampoAlfa & "N"
            ElseIf Ascii = 10 Or Ascii = 13 Then
                CampoAlfa = CampoAlfa & " "
            ElseIf Ascii >= Asc("0") And Ascii <= Asc("9") Then
                CampoAlfa = CampoAlfa & Chr(Ascii)
            ElseIf Ascii >= Asc("A") And Ascii <= Asc("Z") Then
                CampoAlfa = CampoAlfa & Chr(Ascii)
            ElseIf Ascii >= Asc("a") And Ascii <= Asc("z") Then
                CampoAlfa = CampoAlfa & Chr(Ascii)
            Else
                CampoAlfa = CampoAlfa & "?"
            End If
        Next

        If Largo <> 0 Then
            CampoAlfa = Left(CampoAlfa, Largo)
        End If

    End Function

    Public Function ExtractCode(ByVal txt As String, ByVal sep As String) As String
        Dim aUno() As String

        aUno = Split(txt, sep)
        If UBound(aUno) > 0 Then
            ExtractCode = Trim(aUno(0))
        End If

    End Function

    Public Sub EditIni()
        Dim X

        X = Shell("notepad " & App.Path & "\" & App.EXEName & ".ini", vbNormalFocus)


    End Sub

    'Formato Grid
    'Private Sub Command1_Click()
    'Dim i As Integer
    'Dim Campo As String
    'Dim TitCol As String
    'Dim rs As New ADODB.Recordset
    '
    '    With Me.Grid
    '      For i = 0 To .Cols - 1
    '         Campo = ""
    '         If i > 0 Then
    '            Campo = Me.Adodc1.Recordset.Fields(i - 1).Name
    '            rs.Open "SELECT WHFTXT FROM BSS.FFDRH WHERE WHFLDE='" & Campo & "'", DB
    '            If Not rs.EOF Then
    '               TitCol = rs(0)
    '            Else
    '               TitCol = ""
    '            End If
    '            rs.Close
    '         End If
    '         Debug.Print ".TextMatrix(0, i ) = " & Chr(34) & .TextMatrix(0, i) & Chr(34) & _
    '                     ":   .ColWidth( i ) = " & .ColWidth(i) & _
    '                     ":   .ColAlignment( i ) = " & _
    '                     IIf(IsNumeric(.TextMatrix(1, i)), "flexAlignRightCenter ", "flexAlignLeftCenter ") & _
    '                     ": i=i+1           '" & i & " - " & Campo & "      " & TitCol
    '      Next
    '    End With
    '    Set rs = Nothing
    '
    'End Sub

    Public Function BSSEncryption(ByVal original As String, ByVal operatore As Boolean, ByVal clef As String)

        If Len(clef) = 6 Then
            Dim i, j, k As Long
            Dim a1, a2, b1, b2, temp, intdecrypt(0 To 35) As Byte
            Dim str, Result, strdecrypt(0 To 5, 0 To 5) As String

            For i = 0 To 5
                intdecrypt(i) = Asc(Mid(LCase(clef), i + 1, 1))
            Next i

            temp = 97

            For i = 6 To 35
                Do Until intdecrypt(i) <> 0
                    If Not (intdecrypt(0) = temp Or intdecrypt(1) = temp Or intdecrypt(2) = temp Or intdecrypt(3) = temp Or intdecrypt(4) = temp Or intdecrypt(5) = temp) Then intdecrypt(i) = temp
                    temp = temp + 1
                    If temp = 123 Then temp = 32
                    If temp = 35 Then temp = 39
                    If temp = 42 Then temp = 44
                    If temp = 47 Then temp = 63
                Loop
            Next i


            For i = 0 To 5
                For j = 0 To 5
                    strdecrypt(i, j) = Chr(intdecrypt((6 * i) + j))
                Next j
            Next i

            For i = 1 To Len(original)
                Select Case Asc(Mid(original, i, 1))
                    Case 32 To 34, 39 To 41, 44 To 46, 63
                        str = str & Mid(original, i, 1)
                    Case 65 To 90
                        str = str & Chr(Asc(Mid(original, i, 1)) + 32)
                    Case 97 To 122
                        str = str & Mid(original, i, 1)
                End Select
            Next i

            If Len(str) Mod 2 = 1 Then str = str & "q"
            For i = 1 To Len(str) Step 2
                For j = 0 To 5
                    For k = 0 To 5
                        If Mid(str, i, 1) = strdecrypt(j, k) Then
                            a1 = j
                            a2 = k
                        End If
                    Next k
                Next j
                For j = 0 To 5
                    For k = 0 To 5
                        If Mid(str, i + 1, 1) = strdecrypt(j, k) Then
                            b1 = j
                            b2 = k
                        End If
                    Next k
                Next j

                If operatore = False Then
                    Result = Result & strdecrypt(a2, b1) & strdecrypt(b2, a1)
                Else
                    Result = Result & strdecrypt(b2, a1) & strdecrypt(a2, b1)
                End If
            Next i

            If Not Result = "" Then
                If Mid(Result, Len(Result), 1) = "q" Then Result = Left(Result, Len(Result) - 1)
            End If
            BSSEncryption = Result
        Else
            BSSEncryption = "clef invalide"
        End If

    End Function
    Public Function UpdVer(ByVal qLib As String) As String
        Dim aCambio() As String

        aCambio = Split(App.Comments, ":")
        If UBound(aCambio) = 1 Then
            UpdVer = UpdVerX(Trim(aCambio(1)), CDbl(aCambio(0)), qLib)
        Else
            UpdVer = UpdVerX("", 0, qLib)
        End If

    End Function

    Public Function UpdVerX(ByVal sCambio As String, ByVal CSI As Double, ByVal qLib As String) As String
        Dim conn As New ADODB.Connection
        Dim r As Integer
        Dim sVersion As String
        Dim sVerOld As String
        Dim rs As New ADODB.Recordset
        Dim locConnString As String

        LeeParametros(APP_NAME)
        sVersion = "(" & App.Major & "." & App.Minor & "." & App.Revision & ")"

        If qLib = "" Then
            qLib = gsLIB
        End If
        locConnString = mkConnectionString(gsAS400, "", "", qLib)
        conn.Open(locConnString)
        rs.Open("SELECT CCSDSC FROM BSSZCC1 WHERE CCTABL='RFVBVER' AND CCCODE='" & APP_NAME & "'", conn)
        If rs.EOF Then
            fSql = " INSERT INTO BSSZCC1( "
            vSql = " VALUES ( "
            mkSql(tStr, " CCID      ", "CC", 0)              'Record ID; CC/CZ
            mkSql(tStr, " CCTABL    ", "RFVBVER", 0)         'Table ID
            mkSql(tStr, " CCCODE    ", APP_NAME, 0)          'Primary Code
            mkSql(tNum, " CCUDC1    ", 1, 0)                 '//1P0   User Code 1
            mkSql(tStr, " CCALTC    ", "00  ", 0)            'Alternate Code
            mkSql(tStr, " CCDESC    ", Left(App.ProductName, 3), 0)         'Description
            mkSql(tStr, " CCSDSC    ", "(0.0.0)", 1)         'Short Description
            conn.Execute(fSql & vSql)
            rs.Requery()
        End If
        sVerOld = rs("CCSDSC").Value
        fSql = " UPDATE BSSZCC1 SET "
        fSql = fSql & " CCSDSC    = '" & sVersion & "', "       '//30G   Short Description
        fSql = fSql & " CCMNDT    = " & DB2_DATEDEC & ", "       '//8P0   Last Maintenance Date
        fSql = fSql & " CCMNTM    = " & DB2_TIMEDEC0 & ", "       '//6P0   Last Maintenance Time
        If CSI <> 0 Then
            fSql = fSql & " CCENDT    = " & CSI & ", "       '//6P0   Ultimo CSI
        End If
        fSql = fSql & " CCMNUS    ='" & getConnParam(locConnString, "User ID") & "' "      '//10A   Last Maintenance User
        fSql = fSql & " WHERE "
        fSql = fSql & " CCTABL = 'RFVBVER'  "
        fSql = fSql & " AND CCCODE ='" & APP_NAME & "'"      '//15O   Primary Code
        conn.Execute(fSql, r)

        '    msg = "Aplicacion...: " & APP_NAME
        '    msg = msg & vbCr & "Ver Anterior: " & sVerOld
        '    msg = msg & vbCr & "Ver Nueva...: " & sVersion
        '    msg = msg & vbCr & IIf(r <> 0, "Actualizado " & r & " registro", "No Actualizo")
        '    msg = msg & vbCr & gsAS400 & vbCr & qLib
        '    MsgBox msg
        rs.Close()

        fSql = " INSERT INTO RFVBVER( "
        vSql = " VALUES ( "
        mkSql(tStr, " VBAPP     ", APP_NAME, 0)          '//15A
        mkSql(tStr, " VBVER     ", sVersion, 0)          '//15A
        mkSql(tNum, " VBCSI     ", CSI, 0)         '//8P0
        mkSql(tStr, " VBCHG     ", sCambio, 1)         '//301

        conn.Execute(fSql & vSql)
        conn.Close()
        conn = Nothing
        rs = Nothing

        Debug.Print("Aplicacion..: " & APP_NAME)
        Debug.Print("Ver Anterior: " & sVerOld)
        Debug.Print("Ver Nueva...: " & sVersion)
        Debug.Print("Cambio......: " & sCambio)
        Debug.Print("CSI.........: " & CSI)
        Debug.Print(IIf(r <> 0, "Actualizado " & r & " registro", "No Actualizo"))
        Debug.Print(gsAS400, sLib)


        UpdVerX = sVersion

        'Debug.Print APP_NAME, sVerOld

    End Function

    Sub mkSql(ByVal tipoF As tDato, ByVal Campo As String, ByVal valor As Object, ByVal Fin As Integer)
        Dim sep As String

        If Left(Trim(UCase(fSql)), 6) = "INSERT" Then
            sep = IIf(tipoF = tStr, "'", "")
            fSql = fSql & Campo
            Select Case Fin
                Case 0
                    fSql = fSql & " , "
                Case 10, 11, -1, 1
                    fSql = fSql & " ) "
            End Select
            If String.IsNullOrEmpty(valor.Trim) Then
                valor = IIf(tipoF = tStr, "", "0")
            End If
            vSql = vSql & sep & CStr(valor) & sep
            Select Case Fin
                Case 10
                    vSql = vSql & "  "
                Case 11, -1, 1
                    vSql = vSql & " )"
                Case 0
                    vSql = vSql & " , "
            End Select
        Else
            sep = IIf(tipoF = tStr, "'", "")
            If String.IsNullOrEmpty(valor.Trim) Then
                valor = IIf(tipoF = tStr, "", "0")
            End If
            fSql = fSql & Campo & " = " & sep & CStr(valor) & sep & IIf(Fin = 10 Or Fin = 11, " ", " , ")
        End If

    End Sub

    Function Ceros(ByVal val As Object, ByVal C As Integer)

        If CInt(C) > Len(Trim(val)) Then
            Ceros = Right(New String(ChrW(CInt(C)), "0") & Trim(val), CInt(C))
        Else
            Ceros = val
        End If

    End Function

    Public Function nnnn(ByVal numOC As Long) As String
        Dim fSql As String

        fSql = "INSERT INTO ESN("
        fSql = fSql & " SNID, SNTYPE, SNCUST, "
        fSql = fSql & " SNSHIP, SNSEQ, SNDESC, SNPRT, SNPIC, "
        fSql = fSql & " SNINV, SNSTMT, SNDOCR, SNENDT, SNENTM, SNENUS, SNMNDT, SNMNTM, "
        fSql = fSql & " SNMNUS )"
        fSql = fSql & " SELECT "
        fSql = fSql & " SNID, SNTYPE, "
        fSql = fSql & numOC & ", "
        fSql = fSql & " SNSHIP, SNSEQ, SNDESC, SNPRT, SNPIC, "
        fSql = fSql & " SNINV, SNSTMT, SNDOCR, SNENDT, SNENTM, SNENUS, SNMNDT, SNMNTM, "
        fSql = fSql & " SNMNUS "
        fSql = fSql & " FROM RFFLESN "
        fSql = fSql & " WHERE  "
        fSql = fSql & " SNID = 'SN'  AND SNTYPE = 'P' "
        fSql = fSql & " AND SNCUST = " '& rs.Fields("PODEST").Value

        nnnn = fSql

    End Function

    Function LookUp(ByVal ColumnName As String, ByVal TableName As String, ByVal Where As String) As String
        Dim rs As New ADODB.Recordset

        rs.Open("SELECT " & ColumnName & " FROM " & TableName & " WHERE =" & Where, DB)
        If Not rs.EOF Then
            LookUp = CStr(rs(0).Value)
        Else
            LookUp = ""
        End If
        rs.Close()
        rs = Nothing

    End Function

    Function TitCampo(ByVal Campo As String) As String
        Dim rs As New ADODB.Recordset
        Dim Result As String

        rs.Open("SELECT WHCHD1, WHCHD2, WHCHD3 FROM RFFFD WHERE WHFLDI='" & Campo & "'", DB)
        If Not rs.EOF Then
            Result = rs("WHCHD1").Value
            If rs("WHCHD2").Value <> "" Then
                Result = Result & vbLf & rs("WHCHD2").Value
            End If
            If rs("WHCHD3").Value <> "" Then
                Result = Result & vbLf & rs("WHCHD3").Value
            End If
        Else
            Result = Campo
        End If
        rs.Close()
        rs = Nothing

        TitCampo = Result


    End Function



    Public Sub VerURL(ByVal url As String, ByVal Param As String)

        If Param <> "" Then
            Shell(IEXP & "http://transportes.brinsa.com.co/" & url & Param, vbNormalNoFocus)
        Else
            Shell(IEXP & "http://transportes.brinsa.com.co/" & url, vbNormalNoFocus)
        End If

    End Sub

    Public Function vf(ByVal qVal As String) As Double

        vf = Val(Replace(qVal, ",", ""))

    End Function

    Public Function VehPropio(ByVal Placa As String) As Integer
        Dim rs As New ADODB.Recordset

        '0 No esta la placa
        '1 Veh de Tercero
        '2 Veh Propio

        rs.Open("SELECT VPROPI FROM RTVEH WHERE VPLACA = '" & Placa & "'", DB)
        If Not rs.EOF Then
            VehPropio = Val(rs("VPROPI")) + 1
        End If
        rs.Close()
        rs = Nothing

    End Function

    Public Function EsLxBpcs() As Boolean
        EsLxBpcs = Not NO_ES_LX_BPCS
    End Function

    Public Function ConexionFija(ByVal P) As String
        Dim i As Integer
        Dim s As String
        Dim sep As String

        s = "" : sep = ""
        For i = 1 To UBound(P) - 1
            s = s & P(i) & "="
        Next
        s = s & P(i)

        ConexionFija = s

    End Function

    'wrt()
    ' Public Sub wrt(ByVal txt As String, ByVal FileDebug As String)
    '     Dim fs, f, tFile, Linea
    '     Dim CARPETA_LOG As String


    '     Dim i As Integer
    '     Dim nArch As Integer
    '     Dim sArch As String
    '     Dim sLinea As String
    '     Dim AS1_IP As String
    '     Dim rs As New ADODB.Recordset
    '     Dim sFolderFtes As String

    '     CARPETA_LOG = "C:\inetpub\wwwroot\pdf\"

    '     If FileDebug = "" Then
    '         tFile = CARPETA_LOG & "sql" & (Year(Now()) * 10000 + Month(Now()) * 100 + Date.Now.Day) & ".htm"
    '     Else
    '         tFile = CARPETA_LOG & FileDebug & "_" & (Year(Now()) * 10000 + Month(Now()) * 100 + Date.Now.Day) & ".htm"
    '     End If

    '     nArch = FreeFile()
    'Open tFile For Append As #nArch
    'Print #nArch, "<br/><pre>"
    '     'Print #nArch, Now()
    '     'Print #nArch, "<br/>"
    'Print #nArch, txt & ";"
    'Print #nArch, "</pre><br/>"
    '     'Print #nArch, "<hr/>"
    'Close #nArch


    ' End Sub

    'dsp()
    Sub dsp(ByVal txt)

        Debug.Print(CStr(txt))

    End Sub

    'dspSQL()
    Sub dspSQL()

        Debug.Print(lastSql)

    End Sub



    Function CopyPaste(ByVal sql As String) As String

        Clipboard.Clear()
        sql = Replace(Replace(Replace(sql, vbCrLf, ""), vbCr, ""), vbLf, "")
        Do While InStr(1, sql, "  ") > 0
            sql = Replace(sql, "  ", " ")
        Loop
        Clipboard.SetText(sql)
        CopyPaste = sql

    End Function


    Function CCDLookUp(ByVal ColumnName, ByVal TableName, ByVal Where, ByVal Connection)
        Dim RecordSet
        Dim Result
        Dim sql
        Dim ErrorMessage

        sql = "SELECT " & ColumnName
        If Len(CStr(TableName)) > 0 Then sql = sql & " FROM " & TableName
        If Len(CStr(Where)) > 0 Then sql = sql & " WHERE " & Where

        RecordSet = Connection.Execute(sql)
        If Not RecordSet.EOF Then
            Result = RecordSet(0)
        End If
        CCDLookUp = Result

    End Function


    'ChkContenedor1()
    Public Function ChkContenedor(ByVal XCNTR)
        Dim i, a, j
        Dim Digito, refc, nValor, nTotal, nPow2

        ChkContenedor = ""
        XCNTR = UCase(Trim(XCNTR))
        If XCNTR = "NA" Then
            ChkContenedor = "NA"
            Exit Function
        End If

        If XCNTR = "0" Or XCNTR = "000000" Then
            ChkContenedor = "000000"
            Exit Function
        End If

        If Len(XCNTR) < 11 Then
            Exit Function
        End If
        XCNTR = XCNTR & "     "
        'GRIU 122185-5
        For i = 1 To 4
            a = Asc(Mid(XCNTR, i, 1))
            If a < Asc("A") Or a > Asc("Z") Then
                ChkContenedor = ""
                Exit Function
            Else
                ChkContenedor = ChkContenedor & Chr(a)
            End If
        Next

        If Mid(XCNTR, i, 1) = " " Then
            j = 6
        Else
            j = 5
        End If

        For i = j To j + 5
            a = Asc(Mid(XCNTR, i, 1))
            If a < Asc("0") Or a > Asc("9") Then
                ChkContenedor = ""
                Exit Function
            Else
                ChkContenedor = ChkContenedor & Chr(a)
            End If
        Next
        If Mid(XCNTR, i, 1) <> "-" Then
            Digito = Mid(XCNTR, i, 1)
        Else
            Digito = Mid(XCNTR, i + 1, 1)
        End If
        '          1         2         3
        '012345678901234567890123456789012345678
        refc = "0123456789A_BCDEFGHIJK_LMNOPQRSTU_VWXYZ"
        nValor = 0 : nTotal = 0 : nPow2 = 1
        For i = 1 To 10
            nValor = InStr(1, refc, Mid(ChkContenedor, i, 1)) - 1
            nTotal = nTotal + (nValor * nPow2)
            nPow2 = nPow2 * 2
        Next

        nTotal = nTotal Mod 11
        If nTotal >= 10 Then
            nTotal = 0
        End If

        If Not IsNumeric(Digito) Then
            ChkContenedor = ""
        ElseIf CInt(Digito) <> nTotal Then
            ChkContenedor = ""
        Else
            ChkContenedor = Mid(ChkContenedor, 1, 4) & " " & Mid(ChkContenedor, 5, 6) & "-" & Digito
        End If

    End Function



    'DescripProdPlani()
    'BONIFICACION Y PREPACKS Y NOMBRE ESPECIAL PARA EL PRODUCTO
    Function DescripProdPlani(ByVal Pedido As Long, ByVal Linea As Integer, ByVal DescProd As String)
        Dim rs As New ADODB.Recordset
        Dim sDescripProd As String
        Dim Factu As String

        fSql = " SELECT LRES1, CLNPSC, LNET, LCUST, LSHIP, IXDESC"
        fSql = fSql & " FROM ECL LEFT OUTER JOIN EIXL01 ON  LCUST=IXCUST AND LPROD=IXPROD"
        fSql = fSql & " WHERE LORD=" & Pedido & " AND LLINE=" & Linea

        rs.Open(fSql, DB)
        If rs("LRES1").Value = "PK" Or rs("LRES1").Value = "PB" Then
            sDescripProd = "-PPK"
        ElseIf rs("LNET").Value = 0 Then
            If Factu <> "" Then
                sDescripProd = " ***BONIF***"
            End If
        Else
            sDescripProd = ""
        End If

        If Not String.IsNullOrEmpty(rs("IXDESC").Value.Trim) Then
            DescProd = rs("IXDESC")
        End If

        rs.Close()
        rs = Nothing

        DescripProdPlani = DescProd & sDescripProd

    End Function


    'Sub QueryArray(ByVal aSql As VariantType, ByVal sql As String)
    '    Dim pos As Integer
    '    Dim i As Integer

    '    pos = Math.Round((Len(sql) / 200) + 0.4999999999999, 0)
    '    ReDim aSql(pos)

    '    For i = 0 To pos - 1
    '        aSql(i) = Mid(sql, i * 200 + 1, 200)
    '    Next

    'End Sub

    'Public Function DAStoMS(ByVal pDato As Object, ByVal iFrmFec As Integer) As Date
    '    If iFrmFec = adDBTimeStamp Or iFrmFec = 129 Then
    '        DAStoMS = CDate(Left(pDato, 10) & " " & Replace(Mid(pDato, 12, 8), ".", ":"))
    '    Else
    '        DAStoMS = CDate(pDato)
    '    End If

    'End Function

    '==========================================================================================================================
    '  Funciones JAVA
    '==========================================================================================================================
    'RUNJVA_VB()
    Function RUNJVA_VB(ByVal Modulo, ByVal Jar, ByVal p1, ByVal Clase, ByVal idTrabajo, ByVal EnBatch, ByVal Jobq)
        Dim Job, varCmd

        Jobq = IIf(Jobq = "", "QBATCH", Jobq)
        Job = "JVA" & CalcConsec("RFPIBATCH", "9999999")

        fSql = " INSERT INTO RFPIBATCH( "
        vSql = " VALUES ( "
        mkSql(tStr, " BJOB      ", Job, 0)                            '//10A   Trabajo
        mkSql(tStr, " BMODULO   ", Modulo, 0)                         '//10  Categoria
        mkSql(tStr, " BPGM      ", Jar, 0)                            '//50  Paquete jar
        mkSql(tStr, " BDESC     ", Clase, 0)                          '//10  Clase
        mkSql(tStr, " BBATCH    ", idTrabajo, 0)                      '//100  Batch
        mkSql(tStr, " BCRTUSR   ", Session("UserLogin"), 0)           '//10A   Usuario
        mkSql(tStr, " BPARAM    ", Replace(Trim(p1), "'", "''"), 1)   '//5002A Parametros
        ExecuteSQL(fSql & vSql, "")

        varCmd = ""
        varCmd = "CALL PGM(RCJVAPGM) "
        varCmd = varCmd & " PARM('" & UCase(Left(EnBatch, 1)) & "' '" & Job & "'"
        varCmd = varCmd & " '" & Jar & "' '" & Clase & "' '" & Jobq & "')"
        ExecuteSQL("{{ " & varCmd & " }}", "")

        RUNJVA_VB = Job

    End Function

    'resultRUNJVA_VB()
    Function resultRUNJVA_VB(ByVal Trabajo, ByVal parametro)
        Dim aGS, aPrm, rs, i

        fSql = "SELECT * FROM RFPIBATCH WHERE BJOB='" & Trabajo & "'"
        rs = ExecuteSQL(fSql, "")
        If Not rs.EOF Then
            If parametro = "" Then
                Select Case CInt(rs("BEND"))
                    Case 1
                        resultRUNJVA_VB = "OK"
                    Case 0, -1
                        resultRUNJVA_VB = rs("BMSG")
                End Select
            Else
                aGS = Split(rs("BRESULT"), ";")
                resultRUNJVA_VB = ""
                For i = 0 To UBound(aGS)
                    aPrm = Split(aGS(i), "=")
                    If UBound(aPrm) = 1 Then
                        If Trim(UCase(parametro)) = Trim(UCase(aPrm(0))) Then
                            resultRUNJVA_VB = aPrm(1)
                            Exit For
                        End If
                    End If
                Next
            End If
        End If
        rs.Close()
        rs = Nothing

    End Function

    'ProperCase()
    Function ProperCase(ByVal txt As String) As String
        Dim aDato() As String
        Dim i As Integer
        Dim s As String
        Dim sep As String

        aDato = Split(txt, " ")
        For i = 0 To UBound(aDato)
            If Trim(aDato(i)) <> "" Then
                s = s & sep & UCase(Left(aDato(i), 1)) & LCase(Mid(aDato(i), 2))
                sep = " "
            End If
        Next
        ProperCase = s

    End Function

    'Justif()


    Function Session(ByVal Variable As String) As String

        Select Case UCase(Variable)
            Case "USERLOGIN"
                Session = sUser
        End Select

    End Function

    Function paramJava(ByVal parm As String) As String
        Dim aDato() As String
        Dim i As Integer
        Dim sep As String

        aDato = Split(Replace(parm, "'", ""), " ")
        paramJava = ""
        sep = ""
        For i = 0 To UBound(aDato)
            paramJava = paramJava & sep & "'" & aDato(i) & "'"
            sep = " "
        Next

    End Function


    Function ExeJAVASometido(ByVal Pgm, ByVal p1, ByVal wrk)

        fSql = " SBMJOB CMD(  "
        fSql = fSql & " RUNJVA CLASS(com.bss.Main)  "
        fSql = fSql & " PARM('-s' '" & gsAS400 & "' '-l' '" & gsLIB & "' '-u' '" & gsUSER & "' '-p' '" & gsPASS & "'"
        fSql = fSql & " '-usr' '" & sUser & "' " & p1 & " )  "
        fSql = fSql & " CLASSPATH('/BSS/:/BSS/" & Pgm & ".jar:/BSS/lib/jt400.jar')) "
        fSql = fSql & " JOB(" & wrk & ") "
        fSql = fSql & " JOBD(" & gsJobD & ") "

        ExecuteSQL("{{ " & fSql & " }}", "")

    End Function


    Function ExeJAVA(ByVal Pgm, ByVal p1)

        fSql = " RUNJVA CLASS(com.bss.Main)  "
        fSql = fSql & " PARM('-s' '" & gsAS400 & "' '-l' '" & gsLIB & "' '-u' '" & gsUSER & "' '-p' '" & gsPASS & "'"
        fSql = fSql & " '-usr' '" & sUser & "' " & p1 & " )  "
        fSql = fSql & " CLASSPATH('/BSS/:/BSS/" & Pgm & ".jar:/BSS/lib/jt400.jar') "

        ExecuteSQL("{{ " & fSql & " }}", "")

    End Function


    'ExeJavaFletes()
    Function ExeJavaFletes(ByVal Pgm, ByVal p1, ByVal Clase, ByVal Jobq)
        Dim wrkNum

        wrkNum = UCase(Left("FL", 4)) & CalcConsec("JVAWRKNUM", "999999")        ' WrkNumJAVA("FL")
        p1 = Trim(p1)

        ExecuteSQL("DELETE FROM RFFLSPLF", "")

        fSql = " SBMJOB CMD( RUNJVA CLASS({CLASE}) PARM( '-lib' '{LIB}' {PARAM} '-job' '{JOB}' )"
        fSql = fSql & " CLASSPATH('/{LIB}/:/{LIB}/{PGM}.jar:"
        fSql = fSql & "/BSS/lib/commons-cli-1.2.jar:"
        fSql = fSql & "/LXCONNECTOR_192.168.10.1-ERPLX834EC/LxBean.jar:"
        fSql = fSql & "/LXCONNECTOR_192.168.10.1-ERPLX834EC/LXCRuntime.jar:"
        fSql = fSql & "/LXCONNECTOR_192.168.10.1-ERPLX834EC/LXCPI.jar:"
        fSql = fSql & "/LXCONNECTOR_192.168.10.1-ERPLX834EC/lib/jt400.jar:"
        fSql = fSql & "/LXCONNECTOR_192.168.10.1-ERPLX834EC/lib/mailapi.jar:"
        fSql = fSql & "/LXCONNECTOR_192.168.10.1-ERPLX834EC/lib/smtp.jar'))"
        fSql = fSql & " JOB({JOB}) JOBD({JOBD}) JOBQ(QBATCH) "

        fSql = Replace(fSql, "{CLASE}", Clase)
        fSql = Replace(fSql, "{PARAM}", paramJava(CStr(p1)))
        fSql = Replace(fSql, "{PGM}", Pgm)
        fSql = Replace(fSql, "{LIB}", gsLIB)
        fSql = Replace(fSql, "{JOB}", wrkNum)
        fSql = Replace(fSql, "{JOBD}", getLXParam("LXJOBD", ""))

        ExecuteSQL("{{ " & fSql & " }}", "")
        ExeJavaFletes = wrkNum
        Debug.Print(wrkNum)

    End Function


    Public Sub sbmLXjvaOC(ByVal arch As String, ByVal Orden As String, ByVal Linea As String, ByVal Batch As String)

        fSql = " SBMJOB CMD( RUNJVA CLASS(OrdenCompra) PARM( '-lib' '{LIB}' '-arch' '{ARCH}'"
        If Trim(Orden) <> "" Then
            fSql = fSql & " '-orden' '{ORDEN}'"
        End If
        If Trim(Linea) <> "" Then
            fSql = fSql & " '-linea' '{LINEA}'"
        End If
        If Trim(Batch) <> "" Then
            fSql = fSql & " '-batch' '{BATCH}'"
        End If
        fSql = fSql & " )"
        fSql = fSql & " CLASSPATH('/{LIB}/:/{LIB}/RJLXOC.jar:"
        fSql = fSql & "/BSS/lib/commons-cli-1.2.jar:"
        fSql = fSql & "/LXCONNECTOR_192.168.10.1-ERPLX834EC/LxBean.jar:"
        fSql = fSql & "/LXCONNECTOR_192.168.10.1-ERPLX834EC/LXCRuntime.jar:"
        fSql = fSql & "/LXCONNECTOR_192.168.10.1-ERPLX834EC/LXCPI.jar:"
        fSql = fSql & "/LXCONNECTOR_192.168.10.1-ERPLX834EC/lib/jt400.jar:"
        fSql = fSql & "/LXCONNECTOR_192.168.10.1-ERPLX834EC/lib/mailapi.jar:"
        fSql = fSql & "/LXCONNECTOR_192.168.10.1-ERPLX834EC/lib/smtp.jar'))"
        fSql = fSql & " JOB(OC" & Orden & ") JOBQ(LXCJOBQ) "

        fSql = Replace(fSql, "{LIB}", gsLIB)
        fSql = Replace(fSql, "{ARCH}", arch)
        fSql = Replace(fSql, "{ORDEN}", Orden)
        fSql = Replace(fSql, "{LINEA}", Linea)
        fSql = Replace(fSql, "{BATCH}", Batch)

        ExecuteSQL("{{" & fSql & "}}", "")

    End Sub

    Function pdfParam(ByVal tipoPDF, ByVal p1)
        Dim rs, CMD

        CMD = " RUNRMTCMD CMD({CMD} {P1}') "
        CMD = CMD & " RMTLOCNAME({RMTLOCNAME} *IP)"
        CMD = CMD & "    RMTUSER({RMTUSER}) "
        CMD = CMD & "    RMTPWD({RMTPWD}) "
        CMD = Replace(CMD, "{P1}", p1)

        fSql = "SELECT * FROM BSSZCC1 WHERE CCTABL='" & tipoPDF & "'"
        rs = ExecuteSQL(fSql, "")
        Do While Not rs.EOF
            CMD = Replace(CMD, "{" & UCase(rs("CCCODE")) & "}", rs("CCNOT1"))
            rs.MoveNext()
        Loop
        rs.Close()
        rs = Nothing

        pdfParam = CMD

    End Function

    Function UsrMetodoESPR(ByVal Metodo As String) As String

        If Metodo = "" Then
            UsrMetodoESPR = "'" & NombreAbr(sUser, sUserShort) & " [' || SPMETH || ']'"
        Else
            UsrMetodoESPR = NombreAbr(sUser, sUserShort) & " ['" & Metodo & "']"
        End If

    End Function


    Function CorreoDirecto(ByVal Asunto As String, ByVal Envia As String, ByVal Para As String, ByVal CopiaA As String, ByVal Mensaje As String) As String
        Dim LID As String
        Dim rs As New ADODB.Recordset

        fSql = "SELECT CCNOT1 FROM BSSZCC1 WHERE CCTABL = 'FLAVISOS' AND CCCODE = '" & Para & "'"
        rs.Open(fSql, DB)

        LID = CalcConsec("RMAILX", "99999999")
        fSql = " INSERT INTO RMAILX( "
        vSql = " VALUES( "
        mkSql(tNum, " LID       ", LID, 0)              '//8P0
        mkSql(tStr, " LREF      ", My.Settings.CarpetaGneral, 0)      '//25A
        mkSql(tStr, " LASUNTO   ", Asunto, 0)           '//102A
        mkSql(tStr, " LENVIA    ", Envia, 0)            '//152A
        If Not rs.EOF Then
            mkSql(tStr, " LPARA     ", rs("CCNOT1"), 0)             '//2002A
        Else
            mkSql(tStr, " LPARA     ", "Fredy.Manrique@BssLtda.com", 0)             '//2002A
        End If
        mkSql(tStr, " LCOPIA    ", CopiaA, 0)           '//2002A
        mkSql(tStr, " LCUERPO   ", Replace(Mensaje, "'", "''"), 1)          '//10002A
        ExecuteSQL(fSql & vSql, "")
        rs.Close()
        rs = Nothing
        CorreoDirecto = LID

    End Function

    Function PDFPrtExe() As String

        '    comando = Replace(PDFEXE, "{printer}", Impresora_PDF)
        '    comando = Replace(comando, "{copias}", "1")
        '    comando = Replace(comando, "{doc}", ArchivoPDF)
        '    comando = Replace(comando, "{sp}", " ")

        PDFPrtExe = """E:\CLPrint\CLPrint.exe""{sp}/print{sp}/copies:{copias}{sp}/printer:""{printer}""{sp}/pdffile:""{doc}"""

    End Function



    '==========================================================================================================================
    '  Funciones para LX / BPCS
    '==========================================================================================================================

    'LX_Prod()
    Function LX_Prod(ByVal Prod)
        Dim aProd() As String

        aProd = Split(Prod, " ")
        LX_Prod = Left(aProd(0) & Space(K_LON_PROD), K_LON_PROD)

    End Function

    'LX_Cli()
    Function LX_Cli(ByVal clie)
        Dim aCli() As String

        aCli = Split(clie, " ")
        LX_Cli = Ceros(aCli(0), K_LON_CLIE)

    End Function

    'LX_Cli2()
    Function LX_Cli2(ByVal clie)
        Dim aCli() As String

        aCli = Split(clie, " ")
        LX_Cli2 = Left(aCli(0) & Space(K_LON_CLIE), K_LON_CLIE)

    End Function


    'LXAS_Prod()
    Function LXAS_Prod(ByVal Campo)
        LXAS_Prod = " CHAR('" & Campo & "', " & K_LON_PROD & " )"
    End Function

    'LXAS_Cli()
    Function LXAS_Cli(ByVal Campo)
        LXAS_Cli = " DIGITS( DEC( " & Campo & ", " & K_LON_CLIE & ", 0 ) ) "
    End Function

    'LXAS_KeyProdCli()
    Function LXAS_KeyProdCli(ByVal Prod, ByVal cli)
        LXAS_KeyProdCli = " CHAR('" & Prod & "', " & K_LON_PROD & " ) || DIGITS( DEC( " & cli & ", " & K_LON_CLIE & ", 0 ) ) "
    End Function

    'LXAS_KeyProdTipo()
    Function LXAS_KeyProdTipo(ByVal Prod, ByVal key)

        LXAS_KeyProdTipo = " LEFT(" & Prod & ", " & K_LON_PROD & " ) || "
        LXAS_KeyProdTipo = LXAS_KeyProdTipo & "RIGHT( REPLACE( DIGITS( DEC( 0, " & K_LON_CLIE & ", 0 )), '0', ' ' ) || "
        LXAS_KeyProdTipo = LXAS_KeyProdTipo & "TRIM(" & key & "), " & K_LON_CLIE & ") "

    End Function


    '==========================================================================================================================

    Private Function App() As Object
        Throw New NotImplementedException
    End Function





End Module
