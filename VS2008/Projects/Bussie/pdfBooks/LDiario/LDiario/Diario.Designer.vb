﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Diario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Diario))
        Me.Balance = New AxPDF_In_The_BoxCtl.AxPdfBox()
        CType(Me.Balance, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Balance
        '
        Me.Balance.Enabled = True
        Me.Balance.Location = New System.Drawing.Point(72, 104)
        Me.Balance.Name = "Balance"
        Me.Balance.OcxState = CType(resources.GetObject("Balance.OcxState"), System.Windows.Forms.AxHost.State)
        Me.Balance.Size = New System.Drawing.Size(28, 28)
        Me.Balance.TabIndex = 0
        '
        'Diario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 273)
        Me.Controls.Add(Me.Balance)
        Me.Name = "Diario"
        Me.Text = "Form1"
        CType(Me.Balance, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Balance As AxPDF_In_The_BoxCtl.AxPdfBox

End Class
