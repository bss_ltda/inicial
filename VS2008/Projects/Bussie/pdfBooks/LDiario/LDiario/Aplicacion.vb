﻿Imports System.Reflection
Imports System.IO
Module Aplicacion
    'Option Explicit
    Public Control As String
    Public IAL_Prt As Integer
    Public Consecutivo As String
    Public key
    Public Division As String
    Public aPermisos() As Boolean
    Public Const ACC_TOTAL As Integer = 21
    Public Const APP_NAME As String = "ListDespaPDF"

    Sub Main()
        On Error GoTo Manejo_Error
        Dim parametro As String
        Dim aParam() As String

        Dim i As Integer

        BatchApp = True

        aParam = Split(Command(), " ")
        If UBound(aParam) <> 1 Then
            ' End
        End If

        CARPETA_SITIO = Trim(aParam(0))
        Control = Trim(aParam(1))
        Consecutivo = Trim(aParam(2))
        bDateToDate = False

        'CARPETA_SITIO = LeeParametrosEsp(APP_NAME, "CARPETA_SITIO")
        bPRUEBAS = (LeeParametrosEsp(APP_NAME, "PRUEBAS") = "1") Or (LeeParametrosEsp(APP_NAME, "PRUEBAS") = "SI")
        LeeParametrosWeb(CARPETA_SITIO & "\Web.Config")
        PDF_Folder = CARPETA_IMG & "\pdf\Libros\"
        LOG_File = PDF_Folder & "\LD" & Control & ".log"
        WrtTxtError(LOG_File, Control)

        bDateToDate = True
        If Not validaAcceso(gsUSER, gsPASS) Then
            End
        End If

        Diario.Balancee()
        'Unload(LibrosMyB)

        DB.Close()
        DB = Nothing

Manejo_Error:
        If Err.Number <> 0 Then
            WrtTxtError(LOG_File, "Main")
            End
        End If
    End Sub


End Module
