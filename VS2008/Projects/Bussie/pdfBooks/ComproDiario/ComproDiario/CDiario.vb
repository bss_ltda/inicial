﻿Imports PDF_In_The_BoxCtl

'Imports Scripting
Imports System.IO
Imports System.Text

Public Class CDiario
    Dim flag As Integer
    Dim rsTit As New ADODB.Recordset
    Dim rsCab As New ADODB.Recordset
    Dim rsPed As New ADODB.Recordset
    Dim rsDet As New ADODB.Recordset
    Dim rsNotas As New ADODB.Recordset
    Dim DocScanner As Integer, Observ As String, ImprimeIAL As String
    Dim bColor As Boolean
    Dim bQuimicos As Boolean
    Dim Sellos() As String
    Dim Fila As Integer
    Dim Ta As TBoxTable
    Dim Comprobante
    Dim Periodo
    Dim Referencia
    Dim FechaImpresion
    Const CELDA_NORMAL As String = "BrushColor = 255, 255, 255"
    'Const CELDA_SOMBRA As String = "BrushColor = 215, 215, 215"
    Const CELDA_SOMBRA As String = "BrushColor = 255, 255, 255"
    Const TABLE_HEADER_STYLE As String = "ChildrenStyle=""BorderStyle=rect;FontSize=9;Fontname=Arial;FontBold=1;Alignment=Center;VertAlignment=Center"""
    Const ESTILO_TIT As String = "Normal;fontsize=6;Fontname=Corbel;BorderColor=Black;Alignment=Center;VertAlignment=Center"

    Private Sub CDiario_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Try
            flag = 0
            'On Error GoTo Manejo_Error
            Dim aParam() As String

            BatchApp = True
            aParam = Split(Command(), " ")
            If UBound(aParam) <> 1 Then
                ' End
            End If
            NumPaginas = 1
            CARPETA_SITIO = Trim(aParam(0))
            Periodo = Trim(aParam(1))
            Comprobante = Trim(aParam(2))
            Consecutivo = Trim(aParam(3))
            Referencia = Trim(aParam(4))
            FechaImpresion = Trim(aParam(5))
            key = Trim(aParam(6))

            'CARPETA_SITIO = LeeParametrosEsp(APP_NAME, "CARPETA_SITIO")
            bPRUEBAS = (LeeParametrosEsp(APP_NAME, "PRUEBAS") = "1") Or (LeeParametrosEsp(APP_NAME, "PRUEBAS") = "SI")
            LeeParametrosWeb(CARPETA_SITIO & "\Web.Config")
            PDF_Folder = CARPETA_IMG & "\pdf\Libros\"
            LOG_File = PDF_Folder & "\LD" & Control & ".log"
            WrtTxtError(LOG_File, Control)



            bDateToDate = True
            If Not validaAcceso(gsUSER, gsPASS) Then
                End
            End If

            Balancee()
            Application.Exit()

        Catch ex As Exception
            wrtLogHtml("", ex.Message.ToString)
        End Try
    End Sub

    Private Sub Balance_BeforePutBand(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_BeforePutBandEvent) Handles Balance.BeforePutBand
        If e.aBand.Role = TBandRole.rlDetail Then
            If bColor And flag = 0 Then

                e.aBand.BrushColor = RGB(215, 215, 215)
            Else
                e.aBand.BrushColor = RGB(255, 255, 255)
            End If
            bColor = Not bColor
        End If
        Fila = Fila + 1
        Debug.Print(Balance.PenY)
        If Fila >= 30 Then
            Balance.NewPage()
            Fila = 0
        End If
Manejo_Error:
        If Err.Number <> 0 Then
            WrtTxtError(LOG_File, "BeforePutBand")
            End
        End If

    End Sub


 

    Private Sub Balance_Enter(sender As System.Object, e As System.EventArgs) Handles Balance.Enter

    End Sub

    Private Sub Balance_OnBottomOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnBottomOfPageEvent) Handles Balance.OnBottomOfPage
        Dim b As TBoxBand
        With Balance

            b = .CreateBand("Normal; BorderStyle=bottom;Margins=130,200,100,150")
            b.Breakable = False
            b.CreateCell(2500, "BorderStyle=None").CreateText("Alignment=left").Assign("")
            b.Put()
            If e.lastPage Then
                If Fila < 30 Then

                    b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
                    b.ChildrenStyle = "BorderStyle=rect;Alignment=Center"
                    b.CreateCell(1010).CreateText().Assign("")
                    b.CreateCell(480).CreateImage("Alignment=Right;VertAlignment=Top").Assign(My.Settings.CarpetaGneral + "\Fin del Informe.png")
                    b.CreateCell(1010).CreateText().Assign("")
                    b.Put()


                End If

                If Balance.PageCount Mod 2 = 0 Then
                    FinDocumento()
                End If

            End If
            NumPaginas = NumPaginas + 1
        End With

    End Sub

    Private Sub Balance_OnTopOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnTopOfPageEvent) Handles Balance.OnTopOfPage
        Dim b2 As TBoxBand
        Dim b As TBoxBand
        Dim b3 As TBoxBand
        Dim b4 As TBoxBand
        Dim b5 As TBoxBand
        Dim b6 As TBoxBand
        Dim img As TBoxImage
        Dim Ta As TBoxTable
        With Balance
            .DefineStyle("Normal", "0; Margins=130,200,100,150; FontSize=6;Fontname=Arial;BorderColor=Black")
            .Style = "Normal"

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=12;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(1200).CreateText("Alignment=Left;FontSize=12").Assign("COMPROBANTE DIARIO")
            b.CreateCell(1000).CreateText("Alignment=Right;FontSize=6;FontColor=Red").Assign(Referencia)
            b.CreateCell(100).CreateText("Alignment=Left;FontSize=6;FontColor=Blue").Assign(Consecutivo + Balance.PageCount)
            b.CreateCell(200).CreateText("Alignment=Right;FontSize=6").Assign(FechaImpresion)

            b.Put()

            Dim rs As New ADODB.Recordset
            Dim s As String
            Dim ArchivoPDF As String



            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=6;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(200).CreateText("Alignment=Left;FontSize=6").Assign("PERIODO")
            b.CreateCell(300).CreateText("FontName=Arial; FontSize=6; FontBold=1").Assign(Periodo)
            b.CreateCell(1600).CreateText("Alignment=Left;VertAlignment=Top").Assign(" ")
            b.CreateCell(400).CreateImage("Alignment=Right;VertAlignment=Top").Assign(My.Settings.CarpetaGneral + "\BRINSA-IAL.png")

            b.Put()

            b = .CreateBand("BorderStyle=Top;Margins=130,200,100,180")
            b.Height = 50
            b.ChildrenStyle = "BorderStyle=None;Alignment=Center"
            b.CreateCell(2250).CreateText().Assign("")



        End With

    End Sub



    Sub Balancee()


        Dim Sql As String
        Dim rs As New ADODB.Recordset
        Dim rs2 As New ADODB.Recordset
        Dim rs3 As New ADODB.Recordset
        Dim s As String
        Dim ArchivoPDF As String

        ArchivoPDF = PDF_Folder & "ComproDiario" & Periodo & Comprobante & ".pdf"
        With Balance
            .FileName = ArchivoPDF
            .Title = "Libro Diario - "
            .WantShow = bPRUEBAS
            .WantPageCount = True
            .PaperSizeName = "Letter"
            .Orientation = TPrinterOrientation.poLandscape
            .BeginDoc()
            Const NUM_FORMAT As String = "#,##0.00"
            Dim b As TBoxBand
            Fila = 0

            fSql = " UPDATE BSSFTASK SET  "
            fSql = fSql & " TLXMSG = 'EJECUTANDO',  "
            fSql = fSql & " TMSG = 'Generando Comprobante Diario'  "
            fSql = fSql & " WHERE TKEY = '" & key & "' "
            DB.Execute(fSql)

            fSql = " SELECT "
            fSql = fSql & " DCOMPRO,  DEVENOM, DOUBLE(DEVENUM) DEVENUM "
            fSql = fSql & " FROM BSSFFNCOM "
            fSql = fSql & " WHERE "
            fSql = fSql & " DPER = " & Periodo & " "
            fSql = fSql & " AND DTIPCOM = '" & Comprobante & "'"
            fSql = fSql & " AND DESTCOM =3 GROUP BY DCOMPRO,  DEVENOM, DEVENUM"
            rs2.Open(fSql, DB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockReadOnly)
            If rs2.EOF Then
                WrtTxtError(LOG_File, "Factura no se encontro.")
                fSql = " UPDATE BSSFTASK SET  "
                fSql = fSql & " TLXMSG = 'EJECUTADA',  "
                fSql = fSql & " TMSG = 'Comprobante Diario no Existe ',  "
                fSql = fSql & " STS2 = 1 "
                fSql = fSql & " WHERE TKEY = '" & key & "' "
                DB.Execute(fSql)
                Exit Sub
            End If

            While Not rs2.EOF
                b = .CreateBand("BorderStyle=none;Margins=130,200,100,180")
                b.ChildrenStyle = "BorderStyle=rect;Alignment=Right"
                b.Height = 50
                b.CreateCell(2550, "BorderStyle=none;FontSize=6;Alignment=Right").CreateText("BorderRightMargin=10").Assign("")
                b.Put()



                b = .CreateBand("BorderStyle=none;Margins=130,200,100,180")
                b.ChildrenStyle = "BorderStyle=rect;Alignment=Left"
                b.Height = 50
                b.CreateCell(637, "BorderStyle=none;FontSize=6;Alignment=Left").CreateText("BorderRightMargin=10").Assign("Comprobante: " & rs2("DCOMPRO").Value)
                b.CreateCell(639, "BorderStyle=none;FontSize=6;Alignment=Left").CreateText("BorderRightMargin=10").Assign("Nombre Evento: " & rs2("DEVENOM").Value)
                b.CreateCell(637, "BorderStyle=none;FontSize=6;Alignment=Left").CreateText("BorderRightMargin=10").Assign("Codio Evento: " & rs2("DEVENUM").Value)
                b.CreateCell(637, "BorderStyle=none;FontSize=6;Alignment=Left").CreateText("BorderRightMargin=10").Assign("")


                b.Put()

                b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
                b.ChildrenStyle = "BorderStyle=rect"
                b.Height = 50
                b.CreateCell(200, "BorderStyle=rect;FontSize=6").CreateText("BorderRightMargin=10").Assign("COMPROBANTE")
                b.CreateCell(150, "BorderStyle=rect;FontSize=6").CreateText("BorderRightMargin=10").Assign("CUENTA")
                b.CreateCell(100, "BorderStyle=rect;FontSize=6").CreateText("BorderRightMargin=10").Assign("CC")
                b.CreateCell(550, "BorderStyle=rect;FontSize=6").CreateText("BorderRightMargin=10").Assign("DESCRIPCION")
                'b.CreateCell(200, "BorderStyle=rect;FontSize=6").CreateText("BorderRightMargin=10").Assign("INVENTARIO")
                '                b.CreateCell(150, "BorderStyle=rect;FontSize=6").CreateText("BorderRightMargin=10").Assign("APA")
                b.CreateCell(550, "BorderStyle=rect;FontSize=6").CreateText("BorderRightMargin=10").Assign("CONCEPTO")
                b.CreateCell(300, "BorderStyle=rect;FontSize=6").CreateText("BorderRightMargin=10").Assign("TERCERO")
                b.CreateCell(250, "BorderStyle=rect;FontSize=6").CreateText("BorderRightMargin=10").Assign("COD TERCERO")
                b.CreateCell(150, "BorderStyle=rect;FontSize=6").CreateText("BorderRightMargin=10").Assign("NIT")
                b.CreateCell(150, "BorderStyle=rect;FontSize=6").CreateText("BorderRightMargin=10").Assign("DEBITO")
                b.CreateCell(150, "BorderStyle=rect;FontSize=6").CreateText("BorderRightMargin=10").Assign("CREDITO")
                b.Put()

                fSql = " SELECT "
                fSql = fSql & " DDOCUM ,"
                fSql = fSql & " DCUENTA ,"
                fSql = fSql & " DCENCOS ,"
                fSql = fSql & " DDESCTA ,"
                fSql = fSql & " DCODPRD ,"
                fSql = fSql & " DCONCEP ,"
                fSql = fSql & " DTERNOM ,"
                fSql = fSql & " DOUBLE(DTERCOD)DTERCOD ,"
                fSql = fSql & " DTERNIT ,"
                fSql = fSql & " DOUBLE(DDEBITO) DDEBITO,"
                fSql = fSql & " DOUBLE(DCREDITO)DCREDITO "
                fSql = fSql & " FROM BSSFFNCOM "
                fSql = fSql & " WHERE "
                fSql = fSql & " DCOMPRO ='" & rs2("DCOMPRO").Value & "'"
                fSql = fSql & "AND DPER = " & Periodo & " "
                fSql = fSql & " AND DTIPCOM = '" & Comprobante & "'"
                fSql = fSql & " AND DESTCOM =3  "
     
                rs.Open(fSql, DB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockReadOnly)


                'Dim Ta As TBoxTable
                Ta = .CreateTable("BorderStyle=None")
                Ta.Assign(rs)

                Dim Detail As TBoxBand
                Detail = Ta.CreateBand()
                Detail.Role = TBandRole.rlDetail
                Detail.ChildrenStyle = "FontSize=6;Fontname=Arial; VertAlignment=Center"
                Detail.Height = 35
                Detail.ChildrenStyle = "FontSize=6;Fontname=Arial; VertAlignment=Center;BorderStyle=LeftRight"
                Detail.CreateCell(200, "BorderStyle=Left").CreateText("Alignment=Left").Bind("DDOCUM")
                Detail.CreateCell(150, "BorderStyle=Left").CreateText("Alignment=Left").Bind("DCUENTA")
                Detail.CreateCell(100, "BorderStyle=Left").CreateText("Alignment=Left").Bind("DCENCOS")
                Detail.CreateCell(550, "BorderStyle=Left").CreateText("Alignment=Left").Bind("DDESCTA")
                'Detail.CreateCell(200, "BorderStyle=Left").CreateText("Alignment=Left").Bind("DCODPRD")

                Detail.CreateCell(550, "BorderStyle=Left").CreateText("Alignment=Left").Bind("DCONCEP")
                Detail.CreateCell(300, "BorderStyle=Left").CreateText("Alignment=Left").Bind("DTERNOM")
                Detail.CreateCell(250, "BorderStyle=Left").CreateText("Alignment=Left").Bind("DTERCOD")
                Detail.CreateCell(150, "BorderStyle=Left").CreateText("Alignment=Left").Bind("DTERNIT")
                Dim Temp As Double
                With Detail.CreateCell(150, "BorderStyle=Left").CreateText("Alignment=Right")
                    .Bind("DDEBITO")
                    .Format = NUM_FORMAT
                End With
                With Detail.CreateCell(150, "BorderStyle=LeftRight").CreateText("Alignment=Right")
                    .Bind("DCREDITO")
                    .Format = NUM_FORMAT
                End With

                Detail.Breakable = True
                Ta.Put()

                b = .CreateBand("BorderStyle=Top;Margins=130,200,100,180")
                b.Height = 50
                b.ChildrenStyle = "BorderStyle=None;Alignment=Center"
                b.CreateCell(2250).CreateText().Assign("")
                'b.Put

                fSql = " SELECT "
                fSql = fSql & " DOUBLE(SUM(DDEBITO)) DEBITO, "
                fSql = fSql & " DOUBLE(SUM(DCREDITO)) CREDITO"
                fSql = fSql & " FROM BSSFFNCOM "
                fSql = fSql & " WHERE "
                fSql = fSql & " DCOMPRO ='" & rs2("DCOMPRO").Value & "'"
                fSql = fSql & "AND DPER = " & Periodo & " "
                fSql = fSql & " AND DTIPCOM = '" & Comprobante & "'"
                fSql = fSql & " AND DESTCOM =3 "
                rs3.Open(fSql, DB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockReadOnly)

                b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
                b.ChildrenStyle = "BorderStyle=rect"
                b.Height = 50
                b.CreateCell(200, "BorderStyle=Top;FontSize=6").CreateText("BorderRightMargin=10").Assign("")
                b.CreateCell(150, "BorderStyle=Top;FontSize=6").CreateText("BorderRightMargin=10").Assign("")
                b.CreateCell(100, "BorderStyle=Top;FontSize=6").CreateText("BorderRightMargin=10").Assign("")
                b.CreateCell(450, "BorderStyle=Top;FontSize=6").CreateText("BorderRightMargin=10").Assign("")
                b.CreateCell(200, "BorderStyle=Top;FontSize=6").CreateText("BorderRightMargin=10").Assign("")
                b.CreateCell(150, "BorderStyle=Top;FontSize=6").CreateText("BorderRightMargin=10").Assign("")
                b.CreateCell(300, "BorderStyle=Top;FontSize=6").CreateText("BorderRightMargin=10").Assign("")
                b.CreateCell(250, "BorderStyle=Top;FontSize=6").CreateText("BorderRightMargin=10").Assign("")
                b.CreateCell(300, "BorderStyle=Top;FontSize=6").CreateText("BorderRightMargin=10").Assign("")
                b.CreateCell(150, "BorderStyle=Top;FontSize=6").CreateText("BorderRightMargin=10").Assign("Totales")



                With b.CreateCell(150, "BorderStyle=Left").CreateText("Alignment=Right")
                    .Assign(CStr((rs3("DEBITO").Value)))
                    .Format = "#,##0.00"
                End With
                With b.CreateCell(150, "BorderStyle=LeftRight").CreateText("Alignment=Right")
                    .Assign(CStr((rs3("CREDITO").Value)))
                    .Format = "#,##0.00"
                End With
                'b.CreateCell(150, "BorderStyle=Top;FontSize=6").CreateText("BorderRightMargin=10").Assign rs3("DEBITO").Value
                'b.CreateCell(150, "BorderStyle=Top;FontSize=6").CreateText("BorderRightMargin=10").Assign rs3("CREDITO").Value
                b.Put()
                rs3.Close()
                rs.Close()
                rs2.MoveNext()
            End While
            b = .CreateBand("BorderStyle=Top;Margins=130,200,100,180")
            b.ChildrenStyle = "BorderStyle=Top;Alignment=Center"
            b.Height = 100
            b.CreateCell(100, "BorderStyle=Top;FontSize=10;Alignment=Right").CreateText("BorderRightMargin=10").Assign(" ")
            b.Put()


            fSql = " UPDATE BSSFTASK SET  "
            fSql = fSql & " TLXMSG = 'EJECUTADA',  "
            fSql = fSql & " TMSG = 'Comprobante Diario Generado No Paginas: " & NumPaginas & " ',  "
            fSql = fSql & " TURL = '" & Replace(ArchivoPDF, "C:\PortalLB", "") & "',  "
            fSql = fSql & " STS2 = 1 "
            fSql = fSql & " WHERE TKEY = '" & key & "' "
            DB.Execute(fSql)

            rs2.Close()
            .EndDoc()


        End With
    End Sub
    Function FinDocumento()
        Dim b As TBoxBand
        With Me.Balance

            Balance.NewPage()
            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "BorderStyle=rect;Alignment=Center"
            b.CreateCell(1010).CreateText().Assign("")
            b.CreateCell(480).CreateImage("Alignment=Right;VertAlignment=Top").Assign(My.Settings.CarpetaGneral + "\Fin del Informe.png")
            b.CreateCell(1010).CreateText().Assign("")
            b.Put()

        End With
    End Function
End Class