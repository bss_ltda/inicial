﻿Imports PDF_In_The_BoxCtl

'Imports Scripting
Imports System.IO
Public Class InvBala
    Dim flag As Integer
    Dim rsTit As New ADODB.Recordset
    Dim rsCab As New ADODB.Recordset
    Dim rsPed As New ADODB.Recordset
    Dim rsDet As New ADODB.Recordset
    Dim rsNotas As New ADODB.Recordset
    Dim DocScanner As Integer, Observ As String, ImprimeIAL As String
    Dim bColor As Boolean
    Dim bQuimicos As Boolean
    Dim Sellos() As String
    Dim Fila As Integer
    Dim Referencia
    Dim FechaImpresion
    Dim Ta As TBoxTable
    Const CELDA_NORMAL As String = "BrushColor = 255, 255, 255"
    'Const CELDA_SOMBRA As String = "BrushColor = 215, 215, 215"
    Const CELDA_SOMBRA As String = "BrushColor = 255, 255, 255"
    Const TABLE_HEADER_STYLE As String = "ChildrenStyle=""BorderStyle=rect;FontSize=9;Fontname=Arial;FontBold=1;Alignment=Center;VertAlignment=Center"""
    Const ESTILO_TIT As String = "Normal;fontsize=6;Fontname=Corbel;BorderColor=Black;Alignment=Center;VertAlignment=Center"

    Private Sub AxPdfBox1_BeforePutBand(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_BeforePutBandEvent) Handles Balance.BeforePutBand
        If e.aBand.Role = TBandRole.rlDetail Then
            If bColor And flag = 0 Then

                e.aBand.BrushColor = RGB(215, 215, 215)
            Else
                e.aBand.BrushColor = RGB(255, 255, 255)
            End If
            bColor = Not bColor
        End If
        Fila = Fila + 1
        Debug.Print(Balance.PenY)
        If Fila >= 30 Then
            Balance.NewPage()
            Fila = 0
        End If
Manejo_Error:
        If Err.Number <> 0 Then
            WrtTxtError(LOG_File, "BeforePutBand")
            End
        End If

    End Sub

    Private Sub AxPdfBox1_Enter(sender As System.Object, e As System.EventArgs) Handles Balance.Enter

    End Sub

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        flag = 0
        'On Error GoTo Manejo_Error
        Dim aParam() As String

        BatchApp = True
        aParam = Split(Command(), " ")
        If UBound(aParam) <> 1 Then
            ' End
        End If

        CARPETA_SITIO = Trim(aParam(0))
        Control = Trim(aParam(1))
        Consecutivo = Trim(aParam(2))
        Referencia = Trim(aParam(3))
        FechaImpresion = Trim(aParam(4))
        key = Trim(aParam(5))
        bDateToDate = False

        'CARPETA_SITIO = LeeParametrosEsp(APP_NAME, "CARPETA_SITIO")
        bPRUEBAS = (LeeParametrosEsp(APP_NAME, "PRUEBAS") = "1") Or (LeeParametrosEsp(APP_NAME, "PRUEBAS") = "SI")
        LeeParametrosWeb(CARPETA_SITIO & "\Web.Config")
        PDF_Folder = CARPETA_IMG & "\pdf\Libros\"
        LOG_File = PDF_Folder & "\LD" & Control & ".log"
        WrtTxtError(LOG_File, Control)

        bDateToDate = True
        If Not validaAcceso(gsUSER, gsPASS) Then
            End
        End If

        Balancee()

        Me.Close()
    End Sub

    Private Sub Balance_OnBottomOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnBottomOfPageEvent) Handles Balance.OnBottomOfPage
        Dim b As TBoxBand
        With Balance

            b = .CreateBand("Normal; BorderStyle=bottom;Margins=130,200,100,150")
            b.Breakable = False
            b.CreateCell(2500, "BorderStyle=None").CreateText("Alignment=left").Assign("")
            b.Put()
            If e.lastPage Then
                If Fila < 30 Then

                    b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
                    b.ChildrenStyle = "BorderStyle=rect;Alignment=Center"
                    b.CreateCell(1010).CreateText().Assign("")
                    b.CreateCell(480).CreateImage("Alignment=Right;VertAlignment=Top").Assign(My.Settings.CarpetaGneral + "\Fin del Informe.png")
                    b.CreateCell(1010).CreateText().Assign("")
                    b.Put()


                End If

                If Balance.PageCount Mod 2 = 0 Then
                    FinDocumento()
                End If

            End If

            NumPaginas = NumPaginas + 1
        End With

    End Sub

    Private Sub Balance_OnTopOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnTopOfPageEvent) Handles Balance.OnTopOfPage
        Dim b As TBoxBand
        Dim b2 As TBoxBand
        Dim b3 As TBoxBand
        Dim b4 As TBoxBand
        Dim b5 As TBoxBand
        Dim b6 As TBoxBand
        Dim img As TBoxImage
        Dim Ta As TBoxTable
        With Balance
            .DefineStyle("Normal", "0; Margins=130,200,100,150; fontsize=10;Fontname=Arial;BorderColor=Black")
            .Style = "Normal"
            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.Height = 75
            b.ChildrenStyle = "Normal;FontSize=12;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(1200).CreateText("Alignment=Left;FontSize=12").Assign("LIBRO INVENTARIO Y BALANCES")
            b.CreateCell(1000).CreateText("Alignment=Right;FontSize=8;FontColor=Red").Assign(Referencia.ToString)
            b.CreateCell(100).CreateText("Alignment=Left;FontSize=8;FontColor=Blue").Assign(Consecutivo + Balance.PageCount)
            b.CreateCell(200).CreateText("Alignment=Right;FontSize=8").Assign(FechaImpresion.ToString)

            b.Put()


            Dim rs As New ADODB.Recordset
            Dim s As String
            Dim ArchivoPDF As String

            fSql = " SELECT "
            fSql = fSql & " DIGITS(BYEAR) BYEAR,   "
            fSql = fSql & " DIGITS(BPERIO) BPERIO  "
            fSql = fSql & " FROM BSSFFNBAL "
            fSql = fSql & " WHERE "
            fSql = fSql & " BCONTROL = '" & Control & "'"
            rs.Open(fSql, DB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)
            If rs.EOF Then
                WrtTxtError(LOG_File, "Libro sin datos")
                Exit Sub
            End If

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=8;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(200).CreateText("Alignment=Left;FontSize=8").Assign("PERIODO")
            b.CreateCell(300).CreateText("FontName=Arial; FontSize=8; FontBold=1").Assign(rs("BYEAR").Value & "" & rs("BPERIO").Value)
            b.CreateCell(1600).CreateText("Alignment=Left;VertAlignment=Top").Assign(" ")
            b.CreateCell(400).CreateImage("Alignment=Right;VertAlignment=Top").Assign(My.Settings.CarpetaGneral + "\BRINSA-IAL.png")

            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "BorderStyle=TopBottom;Alignment=Right"
            b.Height = 50
            b.CreateCell(400, "BorderStyle=rect;FontSize=10;Alignment=Right").CreateText("BorderRightMargin=10").Assign("CUENTA")
            b.CreateCell(1300, "BorderStyle=rect;FontSize=10;Alignment=Right").CreateText("BorderRightMargin=10").Assign("DESCRIPCION")
            b.CreateCell(800, "BorderStyle=rect;FontSize=10;Alignment=Right").CreateText("BorderRightMargin=10").Assign("FINAL")

            b.Put()

        End With
    End Sub
    Sub Balancee()


        Dim Sql As String
        Dim rs As New ADODB.Recordset
        Dim s As String
        Dim ArchivoPDF As String


        fSql = " UPDATE BSSFTASK SET  "
        fSql = fSql & " TLXMSG = 'EJECUTANDO',  "
        fSql = fSql & " TMSG = 'Generando Libro Inventario y Balances'  "
        fSql = fSql & " WHERE TKEY = '" & key & "' "
        DB.Execute(fSql)

        fSql = " SELECT "
        fSql = fSql & " DIGITS(BYEAR) BYEAR,   "
        fSql = fSql & " DIGITS(BPERIO) BPERIO,  "
        fSql = fSql & " BCUENTA, "
        fSql = fSql & " BDESCTA, "
        fSql = fSql & " DOUBLE(BSALANT) BSALANT, "
        fSql = fSql & " DOUBLE(BDEBITO) BDEBITO, "
        fSql = fSql & " DOUBLE(BCREDITO) BCREDITO, "
        fSql = fSql & " DOUBLE(BSALFIN) BSALFIN"
        fSql = fSql & " FROM BSSFFNBAL "
        fSql = fSql & " WHERE "
        fSql = fSql & " BCONTROL = '" & Control & "'"
        fSql = fSql & " ORDER BY BCUENTA"
        rs.Open(fSql, DB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)

        If rs.EOF Then
            WrtTxtError(LOG_File, "Factura no se encontro.")
            fSql = " UPDATE BSSFTASK SET  "
            fSql = fSql & " TLXMSG = 'EJECUTADA',  "
            fSql = fSql & " TMSG = 'Libro Inventario y Banances no existe ',  "
            fSql = fSql & " STS2 = 1 "
            fSql = fSql & " WHERE TKEY = '" & key & "' "
            DB.Execute(fSql)
            Exit Sub
        End If

        ArchivoPDF = PDF_Folder & "IyB" & Control & ".pdf"
        With Balance
            .FileName = ArchivoPDF

            .Title = "Libro IyB - "
            .WantShow = bPRUEBAS
            .WantPageCount = True
            .PaperSizeName = "Letter"
            .Orientation = TPrinterOrientation.poLandscape
            .BeginDoc()
            Const NUM_FORMAT As String = "#,##0.00"
            Dim b As TBoxBand
            Fila = 0


            Dim Ta As TBoxTable
            Ta = .CreateTable("BorderStyle=None")
            Ta.Assign(rs)

            Dim Detail As TBoxBand
            Detail = Ta.CreateBand()
            Detail.Role = TBandRole.rlDetail
            Detail.ChildrenStyle = "FontSize=8;Fontname=Arial; VertAlignment=Center"
            Detail.Height = 35
            Detail.ChildrenStyle = "FontSize=8;Fontname=Arial; VertAlignment=Center;BorderStyle=LeftRight"
            Detail.CreateCell(400, "BorderStyle=Left").CreateText("Alignment=Left").Bind("BCUENTA")
            Detail.CreateCell(1300, "BorderStyle=none").CreateText("Alignment=Left").Bind("BDESCTA")
            With Detail.CreateCell(800, "BorderStyle=none").CreateText("Alignment=Right")
                .Bind("BSALFIN")
                .Format = NUM_FORMAT

            End With
            Detail.Breakable = True
            Ta.Put()
            b = .CreateBand("BorderStyle=Top;Margins=130,200,100,180")
            b.ChildrenStyle = "BorderStyle=Top;Alignment=Center"
            b.Height = 100
            b.CreateCell(100, "BorderStyle=Top;FontSize=10;Alignment=Right").CreateText("BorderRightMargin=10").Assign(" ")
            b.Put()


            fSql = " UPDATE BSSFTASK SET  "
            fSql = fSql & " TLXMSG = 'EJECUTADA',  "
            fSql = fSql & " TMSG = 'Libro Inventario y Banances Generado No Paginas: " & NumPaginas & " ',  "
            fSql = fSql & " TURL = '" & Replace(ArchivoPDF, "C:\PortalLB", "") & "',  "
            fSql = fSql & " STS2 = 1 "
            fSql = fSql & " WHERE TKEY = '" & key & "' "
            DB.Execute(fSql)

            rs.Close()
            .EndDoc()
        End With


    End Sub



    Function FinDocumento()
        Dim b As TBoxBand
        With Me.Balance

            Balance.NewPage()
            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "BorderStyle=rect;Alignment=Center"
            b.CreateCell(1010).CreateText().Assign("")
            b.CreateCell(480).CreateImage("Alignment=Right;VertAlignment=Top").Assign(My.Settings.CarpetaGneral + "\Fin del Informe.png")
            b.CreateCell(1010).CreateText().Assign("")
            b.Put()

        End With
    End Function

End Class
