﻿Imports System.IO
Imports System.Text
Imports System.Collections.Generic

Module modApoyo
    Public DB As New ADODB.Connection
    Public Desde_CCS As Boolean
    Public ServerRaiz
    Public SessionAppAcc
    Public pagLogin
    Public AppMsg
    Public AppServer
    Public IdSession

    Public gsAS400 As String
    Public gsLIB As String
    Public gsLIB_LX As String
    Public gsUSER As String
    Public gsPASS As String
    Public gsUserLogin As String
    Public gsLXLONG As String
    Public gsLXLONG_CAMPO As String
    Public gsConnectionString As String

    Public bRevisaPwd As Boolean


    'setSession()
    Sub setSession(qSesion, qValor)

    End Sub

    'Reingresa()
    Function Reingresa()

        LimpiaSession()

        setSession("Retorne", Request_ServerVariables("URL"))
        If Request_ServerVariables("QUERY_STRING") <> "" Then
            setSession("Retorne", Session("Retorne") & "?" & Request_ServerVariables("QUERY_STRING"))
        End If

        response_Redirect(ServerRaiz & pagLogin & "?MSGAPP=" & AppMsg & "&ret_link=" & Replace(Server_UrlEncode(Session("Retorne")), "%5F", "_"))

    End Function

    Sub LimpiaSession()

    End Sub

    Function Request_ServerVariables(url)
        Return url
    End Function

    're__()
    Sub re__(txt)
        Dim pag
        pag = "<hr />URL:" & Request_ServerVariables("URL") & "?" & Request_ServerVariables("QUERY_STRING")
        pag = pag & "<hr />Form: " & Request_Form("")
        rw__(pag)
        rw__(txt)
        If Desde_CCS Then
            UnloadPage()
        End If
        response_End()
    End Sub

    'rw__()
    Sub rw__(txt)
        'response.Write(txt)
        'response.Write("<hr>")

    End Sub

    Function ExecuteSQL(sql)
        Return DB.Execute(sql)
    End Function

    Function Request_Form(parm)
        'return Request.Form(param)
        Return ""
    End Function

    Sub UnloadPage()

    End Sub

    Sub response_End()
        'response.end
    End Sub

    'EnRojo()
    Function EnRojo(txt)
        Return "<span style=""color:#FF0000"">" & txt & "</span>"
    End Function

    'Ceros()
    Function Ceros(val, c)
        Dim m

        On Error Resume Next
        If CDbl(val) < 0 Then
            val = Math.Abs(CDbl(val))
            m = "-"
        Else
            m = ""
        End If
        If Err.Number <> 0 Then
            response_Write(val)
            response_End()
        End If
        On Error GoTo 0

        If CInt(c) > Len(Trim(val)) Then
            Ceros = m & Right(Repite(CInt(c), "0") & Trim(val), CInt(c))
        Else
            Ceros = m & val
        End If

    End Function

    Sub response_Write(txt)
        Console.WriteLine(CStr(txt))
    End Sub

    Function sinDiagonal(url)
        If Mid(url, 1, 1) = "/" Then
            sinDiagonal = Mid(url, 2)
        Else
            sinDiagonal = url
        End If
    End Function

    Function AppAccess(n)
        Dim aAcc, i

        If SessionAppAcc = "" Then
            Return False
        End If

        aAcc = Split(CStr(n), ",")
        For i = 0 To UBound(aAcc)
            If Left(SessionAppAcc, 1) = "1" Then
                Return True
            Else
                If (Mid(SessionAppAcc, CInt(Trim(aAcc(i))) + 1, 1) = "1") Then
                    Return True
                End If
            End If
        Next

    End Function

    Sub response_Redirect(txt)
        Console.WriteLine(CStr(txt))
    End Sub

    Function Session(parm As String)

        'Session = lSession.ItemByKey(UCase(parm))
        Return parm

    End Function

    Function Server_UrlEncode(txt)
        Server_UrlEncode = txt
    End Function

    Function getSession()
        Dim rs As New ADODB.Recordset
        Dim fSql As String
        Dim s As String

        fSql = " SELECT CCDESC FROM ZCC WHERE CCTABL = 'SECUENCE' AND CCCODE = 'IDSESSION' "
        rs = ExecuteSQL(fSql)
        If Not rs.EOF Then
            IdSession = rs("CCDESC")
        Else
            rs.Close()
            fSql = " SELECT CCDESC FROM ZCC WHERE CCTABL = 'SECUENCE' AND CCCODE = 'RTNOW' "
            rs = ExecuteSQL(fSql)
            If Not rs.EOF Then
                IdSession = rs("CCDESC")
            End If
        End If
        rs.Close()

        'rs.Open "SELECT * FROM RFSESS WHERE IDSESS = " & IdSession & " AND UPPER(PARAM) = '" & UCase(Parm) & "'", DB
        rs.Open("SELECT * FROM RFSESS WHERE IDSESS = " & IdSession, DB)
        'lSession.Initialize()
        'Do While Not rs.EOF
        '    'lSession.Add rs("VALOR"), UCase(rs("PARAM"))
        '    lSession.add(UCase(rs("PARAM").Value), rs("VALOR").Value)
        '    rs.MoveNext()
        'Loop
        rs.Close()
        rs = Nothing
        s = " | IdSession = " & IdSession
        s = s & " | gsAS400 = " & gsAS400
        s = s & " | gsLIB = " & gsLIB
        s = s & " | Session(""LIB"") = " & Session("LIB")
        s = s & " | Session(""UserLogin"") = " & Session("UserLogin")
        s = s & " | Session(""MenuActual"") = " & Session("MenuActual")
        s = s & " | Conexion " & IIf(DB.State = 1, "Abierta", "Cerrada")
        Debug.Print(s)

    End Function


    Function notepad(txt As String, arch As String)
        Dim iLogTXT As Integer
        Dim sArchivo As String

        iLogTXT = FreeFile()
        If arch = "" Then
            sArchivo = "c:\temp\log0001.txt"
        Else
            sArchivo = arch
        End If


        Dim mydocpath As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
        Dim sb As New StringBuilder()
        sb.Append(txt)

        Using sr As StreamWriter = New StreamWriter(sArchivo)
            sr.Write(sb.ToString())
        End Using

        Shell("""C:\Program Files (x86)\Notepad++\notepad++.exe"" " & sArchivo, 1)
        notepad = sArchivo

    End Function

    'VendedorIndustria()
    Sub VendedorIndustria(CodVend)

        If CInt("0" & CodVend) <> 0 Then
            setSession("ID_VENDOR", Ceros(CodVend, 6))
        Else
            setSession("ID_VENDOR", "")
        End If

    End Sub

    'Server_CreateObject()
    Function Server_CreateObject(obj)

        Select Case UCase(obj)
            Case UCase("Chilkat.Mht")
                ' Server_CreateObject = New ChilkatMht
            Case UCase("Chilkat.MailMan2")
                'Set Server_CreateObject = New ChilkatMailMan2
            Case UCase("ADODB.RecordSet")
                Server_CreateObject = New ADODB.Recordset
            Case UCase("ADODB.Connection")
                Server_CreateObject = New ADODB.Connection
            Case UCase("ADODB.Command")
                Server_CreateObject = New ADODB.Command
            Case UCase("clsDBConnection1")
                Server_CreateObject = New ADODB.Command
        End Select

    End Function

    'mkConnectionString()
    Function mkConnectionString(srv, usr, pass, biblioteca)
        Dim g

        g = "Provider=IBMDA400.DataSource.1;"
        g = g & "Password=" & pass & ";"
        g = g & "User ID=" & usr & ";"
        g = g & "Data Source=" & srv & ";"
        g = g & "Persist Security Info=False;"
        g = g & "Default Collection=" & biblioteca & ";"
        g = g & "Force Translate=0;"
        g = g & "Convert Date Time To Char=FALSE"
        mkConnectionString = g

        If srv = "ACCESS" Then
            mkConnectionString = "Provider=MSDASQL.1;Persist Security Info=False;Data Source=" & biblioteca
        End If

    End Function

    'NombreAbr()
    Function NombreAbr(usr, n_abrev)
        Dim rs, rs1, i, nomA

        If n_abrev <> "" Then
            Return n_abrev
        End If

        rs = ExecuteSQL("SELECT * FROM RCAU WHERE UTIPO='TM' AND UUSR='" & usr & "'")
        If rs("UUSRABR").Value = "" Then
            i = 0
            Do While True
                i = i + 1
                If i = 100 Then
                    NombreAbr = "ERR"
                    Exit Do
                End If
                nomA = Left(UCase(usr), 2)
                rs1 = ExecuteSQL("SELECT * FROM RCAU WHERE UTIPO='TM' AND UUSRABR='" & nomA & Ceros(i, 2) & "'")
                If rs1.EOF Then
                    ExecuteSQL("UPDATE RCAU SET UUSRABR='" & nomA & Ceros(i, 2) & "' WHERE UTIPO='TM' AND UUSR='" & usr & "'")
                    NombreAbr = nomA & Ceros(i, 2)
                    Exit Do
                End If
                rs1.Close()
            Loop
        Else
            NombreAbr = rs("UUSRABR").Value
        End If
        rs.Close()
        rs = Nothing

    End Function

    'QueLIB()
    Function QueLIB(gs)
        Dim aGS, biblioteca, i

        aGS = Split(gs, ";")
        QueLIB = ""
        For i = 0 To UBound(aGS)
            If InStr(1, UCase(aGS(i)), UCase("Default Collection")) <> 0 Then
                biblioteca = Split(aGS(i), "=")
                QueLIB = biblioteca(1)
            End If
        Next

    End Function

    Function CalcConsec(sID, TopeMax)
        Dim rs, locID, sql

        sql = "SELECT CCDESC FROM zcc WHERE CCTABL = 'SECUENCE' AND CCCODE='" & sID & "'"
        rs = Server_CreateObject("ADODB.recordset")
        rs.CursorLocation = 2 'adUseServer
        rs.CursorType = 2     'adOpenDynamic
        rs.LockType = 2       'adLockPessimistic
        rs.Open(sql, DB)

        'Para utilizar el numero toma el numero y lo aumenta en uno
        'Es decir el que aparece es el ultimo generado
        If Not rs.EOF Then
            locID = CDbl(rs("CCDESC")) + 1
            If locID > CDbl(TopeMax) Then
                locID = 1
            End If
            locID = Repite(Len(TopeMax), "0") & locID
            locID = Right(locID, Len(TopeMax))
            rs("CCDESC") = locID
            rs.Update()
            rs.Close()
        Else
            locID = 1
            locID = Repite(Len(TopeMax), "0") & locID
            locID = Right(locID, Len(TopeMax))
            rs.Close()
            sql = " INSERT INTO zcc (CCID, CCTABL, CCCODE, CCDESC ) "
            sql = sql & " VALUES( 'CC', 'SECUENCE', '" & sID & "', '" & locID & "' )"
            DB.Execute(sql)
        End If

        CalcConsec = locID
        rs = Nothing

    End Function

    Function Repite(veces As Integer, x As String)
        Dim s
        s = New String(x, veces)
        Return s.ToString()

    End Function


End Module
