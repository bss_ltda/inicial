﻿Public Class Form1

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'InfoNaturaDataSet3.RFPARAM' Puede moverla o quitarla según sea necesario.
        Me.RFPARAMTableAdapter.Fill(Me.InfoNaturaDataSet3.RFPARAM)
        'TODO: esta línea de código carga datos en la tabla 'InfoNaturaDataSet2.RCAA' Puede moverla o quitarla según sea necesario.
        Me.RCAATableAdapter.Fill(Me.InfoNaturaDataSet2.RCAA)
        'TODO: esta línea de código carga datos en la tabla 'InfoNaturaDataSet1.RCAU' Puede moverla o quitarla según sea necesario.
        Me.RCAUTableAdapter.Fill(Me.InfoNaturaDataSet1.RCAU)
        'TODO: esta línea de código carga datos en la tabla 'InfoNaturaDataSet.RCAM' Puede moverla o quitarla según sea necesario.
        Me.RCAMTableAdapter.Fill(Me.InfoNaturaDataSet.RCAM)

    End Sub

    Private Sub RCAMBindingSource_CurrentChanged(sender As System.Object, e As System.EventArgs) Handles RCAMBindingSource.CurrentChanged

    End Sub


    Private Sub DataGridView1_CellClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick

    End Sub

    Private Sub DataGridView1_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub sqlAnd_Click(sender As System.Object, e As System.EventArgs) Handles sqlAnd.Click
        Dim s As String
        Dim j As Integer
        Dim dv As DataGridView

        Select Case Me.TAB.SelectedIndex
            Case 0
                dv = Me.DataGridView1
            Case 1
                dv = Me.DataGridView2
            Case 2
                dv = Me.DataGridView3
            Case 3
                dv = Me.DataGridView4
            Case Else
                dv = Me.DataGridView1
        End Select

        With dv
            j = .CurrentCell.ColumnIndex
            s = .Columns.Item(j).HeaderText
            If IsNumeric(.CurrentCell.Value) Then
                s &= " = " & .CurrentCell.Value
            Else
                s &= " = '" & .CurrentCell.Value & "'"
            End If
        End With

        If Me.txtWhere.Text = "" Then
            Me.txtWhere.Text = s
        Else
            Me.txtWhere.Text &= " AND " & s
        End If

    End Sub

    Private Sub sqlEXE_Click(sender As System.Object, e As System.EventArgs) Handles sqlEXE.Click
        RCAMBindingSource.Filter = Me.txtWhere.Text
    End Sub
End Class
