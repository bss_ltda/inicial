﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TAB = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.AIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ALVLDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AORDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AAPPDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AMODDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AGRPDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.APARENTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ATITDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ALINKDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACLASSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AISETDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.APARM1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.APARM2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.APARM3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.APARM4DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.APARM5DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ADESCDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RCAMBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InfoNaturaDataSet = New dfu.InfoNaturaDataSet()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.RCAMTableAdapter = New dfu.InfoNaturaDataSetTableAdapters.RCAMTableAdapter()
        Me.sqlAnd = New System.Windows.Forms.Button()
        Me.txtWhere = New System.Windows.Forms.TextBox()
        Me.sqlEXE = New System.Windows.Forms.Button()
        Me.DataGridView2 = New System.Windows.Forms.DataGridView()
        Me.InfoNaturaDataSetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InfoNaturaDataSetBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.InfoNaturaDataSet1 = New dfu.InfoNaturaDataSet1()
        Me.RCAUBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.RCAUTableAdapter = New dfu.InfoNaturaDataSet1TableAdapters.RCAUTableAdapter()
        Me.USTSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UTIPODataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UUSRDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UUSRBOLDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UUSRABRDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UMODDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UPLANTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UWHSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UGRPDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UTRADataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UVENDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UPASSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UNOMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UEMAILDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UFECCADDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UDIASDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UUSRR1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UUSRR2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UUSRR3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UUSRR4DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UUSRR5DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UCATEG1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UCATEG2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UCATEG3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UCATEG4DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UCATEG5DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UUENDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UANILLDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.USFCNSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UBODSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UQRYDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UTUDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.DataGridView3 = New System.Windows.Forms.DataGridView()
        Me.InfoNaturaDataSet2 = New dfu.InfoNaturaDataSet2()
        Me.RCAABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.RCAATableAdapter = New dfu.InfoNaturaDataSet2TableAdapters.RCAATableAdapter()
        Me.ASTSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AAPPDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AMODDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ADESCDataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AUSRDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AAPPMNUDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AOPCMNUDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AURLDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACC00DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACC01DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACC02DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACC03DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACC04DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACC05DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACC06DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACC07DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACC08DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACC09DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACC10DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACC11DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACC12DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACC13DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACC14DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACC15DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACC16DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACC17DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACC18DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACC19DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ACC20DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AAUTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AUPDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridView4 = New System.Windows.Forms.DataGridView()
        Me.InfoNaturaDataSet3 = New dfu.InfoNaturaDataSet3()
        Me.RFPARAMBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.RFPARAMTableAdapter = New dfu.InfoNaturaDataSet3TableAdapters.RFPARAMTableAdapter()
        Me.CCNUMDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCTABLDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCCODEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCCODE2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCCODE3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCCODENDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCLANGDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCALTCDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCDESCDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCSDSCDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCNOT1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCNOT2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCUDC0DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCUDC1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCUDC2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCUDC3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCUDC4DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCUDC5DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCUDC6DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCUDC7DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCUDC8DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCUDC9DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCUDV1DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCUDV2DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCUDV3DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCUDV4DataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCUDTSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCUDDTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCRESVDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCENDTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCENUSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCMNDTDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCMNUSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CCNOTEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TAB.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RCAMBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InfoNaturaDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InfoNaturaDataSetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InfoNaturaDataSetBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InfoNaturaDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RCAUBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InfoNaturaDataSet2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RCAABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InfoNaturaDataSet3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RFPARAMBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TAB
        '
        Me.TAB.Controls.Add(Me.TabPage1)
        Me.TAB.Controls.Add(Me.TabPage2)
        Me.TAB.Controls.Add(Me.TabPage3)
        Me.TAB.Controls.Add(Me.TabPage4)
        Me.TAB.Location = New System.Drawing.Point(22, 59)
        Me.TAB.Name = "TAB"
        Me.TAB.SelectedIndex = 0
        Me.TAB.Size = New System.Drawing.Size(1244, 533)
        Me.TAB.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.DataGridView1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1236, 507)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "RCAM"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'DataGridView1
        '
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.AIDDataGridViewTextBoxColumn, Me.ALVLDataGridViewTextBoxColumn, Me.AORDDataGridViewTextBoxColumn, Me.AAPPDataGridViewTextBoxColumn, Me.AMODDataGridViewTextBoxColumn, Me.AGRPDataGridViewTextBoxColumn, Me.APARENTDataGridViewTextBoxColumn, Me.ATITDataGridViewTextBoxColumn, Me.ALINKDataGridViewTextBoxColumn, Me.ACLASSDataGridViewTextBoxColumn, Me.AISETDataGridViewTextBoxColumn, Me.APARM1DataGridViewTextBoxColumn, Me.APARM2DataGridViewTextBoxColumn, Me.APARM3DataGridViewTextBoxColumn, Me.APARM4DataGridViewTextBoxColumn, Me.APARM5DataGridViewTextBoxColumn, Me.ADESCDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.RCAMBindingSource
        Me.DataGridView1.Location = New System.Drawing.Point(6, 6)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(1224, 495)
        Me.DataGridView1.TabIndex = 0
        '
        'AIDDataGridViewTextBoxColumn
        '
        Me.AIDDataGridViewTextBoxColumn.DataPropertyName = "AID"
        Me.AIDDataGridViewTextBoxColumn.HeaderText = "AID"
        Me.AIDDataGridViewTextBoxColumn.Name = "AIDDataGridViewTextBoxColumn"
        '
        'ALVLDataGridViewTextBoxColumn
        '
        Me.ALVLDataGridViewTextBoxColumn.DataPropertyName = "ALVL"
        Me.ALVLDataGridViewTextBoxColumn.HeaderText = "ALVL"
        Me.ALVLDataGridViewTextBoxColumn.Name = "ALVLDataGridViewTextBoxColumn"
        '
        'AORDDataGridViewTextBoxColumn
        '
        Me.AORDDataGridViewTextBoxColumn.DataPropertyName = "AORD"
        Me.AORDDataGridViewTextBoxColumn.HeaderText = "AORD"
        Me.AORDDataGridViewTextBoxColumn.Name = "AORDDataGridViewTextBoxColumn"
        '
        'AAPPDataGridViewTextBoxColumn
        '
        Me.AAPPDataGridViewTextBoxColumn.DataPropertyName = "AAPP"
        Me.AAPPDataGridViewTextBoxColumn.HeaderText = "AAPP"
        Me.AAPPDataGridViewTextBoxColumn.Name = "AAPPDataGridViewTextBoxColumn"
        '
        'AMODDataGridViewTextBoxColumn
        '
        Me.AMODDataGridViewTextBoxColumn.DataPropertyName = "AMOD"
        Me.AMODDataGridViewTextBoxColumn.HeaderText = "AMOD"
        Me.AMODDataGridViewTextBoxColumn.Name = "AMODDataGridViewTextBoxColumn"
        '
        'AGRPDataGridViewTextBoxColumn
        '
        Me.AGRPDataGridViewTextBoxColumn.DataPropertyName = "AGRP"
        Me.AGRPDataGridViewTextBoxColumn.HeaderText = "AGRP"
        Me.AGRPDataGridViewTextBoxColumn.Name = "AGRPDataGridViewTextBoxColumn"
        '
        'APARENTDataGridViewTextBoxColumn
        '
        Me.APARENTDataGridViewTextBoxColumn.DataPropertyName = "APARENT"
        Me.APARENTDataGridViewTextBoxColumn.HeaderText = "APARENT"
        Me.APARENTDataGridViewTextBoxColumn.Name = "APARENTDataGridViewTextBoxColumn"
        '
        'ATITDataGridViewTextBoxColumn
        '
        Me.ATITDataGridViewTextBoxColumn.DataPropertyName = "ATIT"
        Me.ATITDataGridViewTextBoxColumn.HeaderText = "ATIT"
        Me.ATITDataGridViewTextBoxColumn.Name = "ATITDataGridViewTextBoxColumn"
        '
        'ALINKDataGridViewTextBoxColumn
        '
        Me.ALINKDataGridViewTextBoxColumn.DataPropertyName = "ALINK"
        Me.ALINKDataGridViewTextBoxColumn.HeaderText = "ALINK"
        Me.ALINKDataGridViewTextBoxColumn.Name = "ALINKDataGridViewTextBoxColumn"
        '
        'ACLASSDataGridViewTextBoxColumn
        '
        Me.ACLASSDataGridViewTextBoxColumn.DataPropertyName = "ACLASS"
        Me.ACLASSDataGridViewTextBoxColumn.HeaderText = "ACLASS"
        Me.ACLASSDataGridViewTextBoxColumn.Name = "ACLASSDataGridViewTextBoxColumn"
        '
        'AISETDataGridViewTextBoxColumn
        '
        Me.AISETDataGridViewTextBoxColumn.DataPropertyName = "AISET"
        Me.AISETDataGridViewTextBoxColumn.HeaderText = "AISET"
        Me.AISETDataGridViewTextBoxColumn.Name = "AISETDataGridViewTextBoxColumn"
        '
        'APARM1DataGridViewTextBoxColumn
        '
        Me.APARM1DataGridViewTextBoxColumn.DataPropertyName = "APARM1"
        Me.APARM1DataGridViewTextBoxColumn.HeaderText = "APARM1"
        Me.APARM1DataGridViewTextBoxColumn.Name = "APARM1DataGridViewTextBoxColumn"
        '
        'APARM2DataGridViewTextBoxColumn
        '
        Me.APARM2DataGridViewTextBoxColumn.DataPropertyName = "APARM2"
        Me.APARM2DataGridViewTextBoxColumn.HeaderText = "APARM2"
        Me.APARM2DataGridViewTextBoxColumn.Name = "APARM2DataGridViewTextBoxColumn"
        '
        'APARM3DataGridViewTextBoxColumn
        '
        Me.APARM3DataGridViewTextBoxColumn.DataPropertyName = "APARM3"
        Me.APARM3DataGridViewTextBoxColumn.HeaderText = "APARM3"
        Me.APARM3DataGridViewTextBoxColumn.Name = "APARM3DataGridViewTextBoxColumn"
        '
        'APARM4DataGridViewTextBoxColumn
        '
        Me.APARM4DataGridViewTextBoxColumn.DataPropertyName = "APARM4"
        Me.APARM4DataGridViewTextBoxColumn.HeaderText = "APARM4"
        Me.APARM4DataGridViewTextBoxColumn.Name = "APARM4DataGridViewTextBoxColumn"
        '
        'APARM5DataGridViewTextBoxColumn
        '
        Me.APARM5DataGridViewTextBoxColumn.DataPropertyName = "APARM5"
        Me.APARM5DataGridViewTextBoxColumn.HeaderText = "APARM5"
        Me.APARM5DataGridViewTextBoxColumn.Name = "APARM5DataGridViewTextBoxColumn"
        '
        'ADESCDataGridViewTextBoxColumn
        '
        Me.ADESCDataGridViewTextBoxColumn.DataPropertyName = "ADESC"
        Me.ADESCDataGridViewTextBoxColumn.HeaderText = "ADESC"
        Me.ADESCDataGridViewTextBoxColumn.Name = "ADESCDataGridViewTextBoxColumn"
        '
        'RCAMBindingSource
        '
        Me.RCAMBindingSource.DataMember = "RCAM"
        Me.RCAMBindingSource.DataSource = Me.InfoNaturaDataSet
        '
        'InfoNaturaDataSet
        '
        Me.InfoNaturaDataSet.DataSetName = "InfoNaturaDataSet"
        Me.InfoNaturaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.DataGridView2)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1236, 507)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "RCAU"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'RCAMTableAdapter
        '
        Me.RCAMTableAdapter.ClearBeforeFill = True
        '
        'sqlAnd
        '
        Me.sqlAnd.Location = New System.Drawing.Point(478, 8)
        Me.sqlAnd.Name = "sqlAnd"
        Me.sqlAnd.Size = New System.Drawing.Size(74, 28)
        Me.sqlAnd.TabIndex = 1
        Me.sqlAnd.Text = "And"
        Me.sqlAnd.UseVisualStyleBackColor = True
        '
        'txtWhere
        '
        Me.txtWhere.Location = New System.Drawing.Point(22, 12)
        Me.txtWhere.Name = "txtWhere"
        Me.txtWhere.Size = New System.Drawing.Size(416, 20)
        Me.txtWhere.TabIndex = 2
        '
        'sqlEXE
        '
        Me.sqlEXE.Location = New System.Drawing.Point(558, 8)
        Me.sqlEXE.Name = "sqlEXE"
        Me.sqlEXE.Size = New System.Drawing.Size(73, 28)
        Me.sqlEXE.TabIndex = 3
        Me.sqlEXE.Text = "EXE"
        Me.sqlEXE.UseVisualStyleBackColor = True
        '
        'DataGridView2
        '
        Me.DataGridView2.AutoGenerateColumns = False
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.USTSDataGridViewTextBoxColumn, Me.UTIPODataGridViewTextBoxColumn, Me.UUSRDataGridViewTextBoxColumn, Me.UUSRBOLDataGridViewTextBoxColumn, Me.UUSRABRDataGridViewTextBoxColumn, Me.UMODDataGridViewTextBoxColumn, Me.UPLANTDataGridViewTextBoxColumn, Me.UWHSDataGridViewTextBoxColumn, Me.UGRPDataGridViewTextBoxColumn, Me.UTRADataGridViewTextBoxColumn, Me.UVENDDataGridViewTextBoxColumn, Me.UPASSDataGridViewTextBoxColumn, Me.UNOMDataGridViewTextBoxColumn, Me.UEMAILDataGridViewTextBoxColumn, Me.UFECCADDataGridViewTextBoxColumn, Me.UDIASDataGridViewTextBoxColumn, Me.UUSRR1DataGridViewTextBoxColumn, Me.UUSRR2DataGridViewTextBoxColumn, Me.UUSRR3DataGridViewTextBoxColumn, Me.UUSRR4DataGridViewTextBoxColumn, Me.UUSRR5DataGridViewTextBoxColumn, Me.UCATEG1DataGridViewTextBoxColumn, Me.UCATEG2DataGridViewTextBoxColumn, Me.UCATEG3DataGridViewTextBoxColumn, Me.UCATEG4DataGridViewTextBoxColumn, Me.UCATEG5DataGridViewTextBoxColumn, Me.UUENDataGridViewTextBoxColumn, Me.UANILLDataGridViewTextBoxColumn, Me.USFCNSDataGridViewTextBoxColumn, Me.UBODSDataGridViewTextBoxColumn, Me.UQRYDataGridViewTextBoxColumn, Me.UTUDataGridViewTextBoxColumn})
        Me.DataGridView2.DataSource = Me.RCAUBindingSource
        Me.DataGridView2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView2.Location = New System.Drawing.Point(3, 3)
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.Size = New System.Drawing.Size(1230, 501)
        Me.DataGridView2.TabIndex = 0
        '
        'InfoNaturaDataSetBindingSource
        '
        Me.InfoNaturaDataSetBindingSource.DataSource = Me.InfoNaturaDataSet
        Me.InfoNaturaDataSetBindingSource.Position = 0
        '
        'InfoNaturaDataSetBindingSource1
        '
        Me.InfoNaturaDataSetBindingSource1.DataSource = Me.InfoNaturaDataSet
        Me.InfoNaturaDataSetBindingSource1.Position = 0
        '
        'InfoNaturaDataSet1
        '
        Me.InfoNaturaDataSet1.DataSetName = "InfoNaturaDataSet1"
        Me.InfoNaturaDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'RCAUBindingSource
        '
        Me.RCAUBindingSource.DataMember = "RCAU"
        Me.RCAUBindingSource.DataSource = Me.InfoNaturaDataSet1
        '
        'RCAUTableAdapter
        '
        Me.RCAUTableAdapter.ClearBeforeFill = True
        '
        'USTSDataGridViewTextBoxColumn
        '
        Me.USTSDataGridViewTextBoxColumn.DataPropertyName = "USTS"
        Me.USTSDataGridViewTextBoxColumn.HeaderText = "USTS"
        Me.USTSDataGridViewTextBoxColumn.Name = "USTSDataGridViewTextBoxColumn"
        '
        'UTIPODataGridViewTextBoxColumn
        '
        Me.UTIPODataGridViewTextBoxColumn.DataPropertyName = "UTIPO"
        Me.UTIPODataGridViewTextBoxColumn.HeaderText = "UTIPO"
        Me.UTIPODataGridViewTextBoxColumn.Name = "UTIPODataGridViewTextBoxColumn"
        '
        'UUSRDataGridViewTextBoxColumn
        '
        Me.UUSRDataGridViewTextBoxColumn.DataPropertyName = "UUSR"
        Me.UUSRDataGridViewTextBoxColumn.HeaderText = "UUSR"
        Me.UUSRDataGridViewTextBoxColumn.Name = "UUSRDataGridViewTextBoxColumn"
        '
        'UUSRBOLDataGridViewTextBoxColumn
        '
        Me.UUSRBOLDataGridViewTextBoxColumn.DataPropertyName = "UUSRBOL"
        Me.UUSRBOLDataGridViewTextBoxColumn.HeaderText = "UUSRBOL"
        Me.UUSRBOLDataGridViewTextBoxColumn.Name = "UUSRBOLDataGridViewTextBoxColumn"
        '
        'UUSRABRDataGridViewTextBoxColumn
        '
        Me.UUSRABRDataGridViewTextBoxColumn.DataPropertyName = "UUSRABR"
        Me.UUSRABRDataGridViewTextBoxColumn.HeaderText = "UUSRABR"
        Me.UUSRABRDataGridViewTextBoxColumn.Name = "UUSRABRDataGridViewTextBoxColumn"
        '
        'UMODDataGridViewTextBoxColumn
        '
        Me.UMODDataGridViewTextBoxColumn.DataPropertyName = "UMOD"
        Me.UMODDataGridViewTextBoxColumn.HeaderText = "UMOD"
        Me.UMODDataGridViewTextBoxColumn.Name = "UMODDataGridViewTextBoxColumn"
        '
        'UPLANTDataGridViewTextBoxColumn
        '
        Me.UPLANTDataGridViewTextBoxColumn.DataPropertyName = "UPLANT"
        Me.UPLANTDataGridViewTextBoxColumn.HeaderText = "UPLANT"
        Me.UPLANTDataGridViewTextBoxColumn.Name = "UPLANTDataGridViewTextBoxColumn"
        '
        'UWHSDataGridViewTextBoxColumn
        '
        Me.UWHSDataGridViewTextBoxColumn.DataPropertyName = "UWHS"
        Me.UWHSDataGridViewTextBoxColumn.HeaderText = "UWHS"
        Me.UWHSDataGridViewTextBoxColumn.Name = "UWHSDataGridViewTextBoxColumn"
        '
        'UGRPDataGridViewTextBoxColumn
        '
        Me.UGRPDataGridViewTextBoxColumn.DataPropertyName = "UGRP"
        Me.UGRPDataGridViewTextBoxColumn.HeaderText = "UGRP"
        Me.UGRPDataGridViewTextBoxColumn.Name = "UGRPDataGridViewTextBoxColumn"
        '
        'UTRADataGridViewTextBoxColumn
        '
        Me.UTRADataGridViewTextBoxColumn.DataPropertyName = "UTRA"
        Me.UTRADataGridViewTextBoxColumn.HeaderText = "UTRA"
        Me.UTRADataGridViewTextBoxColumn.Name = "UTRADataGridViewTextBoxColumn"
        '
        'UVENDDataGridViewTextBoxColumn
        '
        Me.UVENDDataGridViewTextBoxColumn.DataPropertyName = "UVEND"
        Me.UVENDDataGridViewTextBoxColumn.HeaderText = "UVEND"
        Me.UVENDDataGridViewTextBoxColumn.Name = "UVENDDataGridViewTextBoxColumn"
        '
        'UPASSDataGridViewTextBoxColumn
        '
        Me.UPASSDataGridViewTextBoxColumn.DataPropertyName = "UPASS"
        Me.UPASSDataGridViewTextBoxColumn.HeaderText = "UPASS"
        Me.UPASSDataGridViewTextBoxColumn.Name = "UPASSDataGridViewTextBoxColumn"
        '
        'UNOMDataGridViewTextBoxColumn
        '
        Me.UNOMDataGridViewTextBoxColumn.DataPropertyName = "UNOM"
        Me.UNOMDataGridViewTextBoxColumn.HeaderText = "UNOM"
        Me.UNOMDataGridViewTextBoxColumn.Name = "UNOMDataGridViewTextBoxColumn"
        '
        'UEMAILDataGridViewTextBoxColumn
        '
        Me.UEMAILDataGridViewTextBoxColumn.DataPropertyName = "UEMAIL"
        Me.UEMAILDataGridViewTextBoxColumn.HeaderText = "UEMAIL"
        Me.UEMAILDataGridViewTextBoxColumn.Name = "UEMAILDataGridViewTextBoxColumn"
        '
        'UFECCADDataGridViewTextBoxColumn
        '
        Me.UFECCADDataGridViewTextBoxColumn.DataPropertyName = "UFECCAD"
        Me.UFECCADDataGridViewTextBoxColumn.HeaderText = "UFECCAD"
        Me.UFECCADDataGridViewTextBoxColumn.Name = "UFECCADDataGridViewTextBoxColumn"
        '
        'UDIASDataGridViewTextBoxColumn
        '
        Me.UDIASDataGridViewTextBoxColumn.DataPropertyName = "UDIAS"
        Me.UDIASDataGridViewTextBoxColumn.HeaderText = "UDIAS"
        Me.UDIASDataGridViewTextBoxColumn.Name = "UDIASDataGridViewTextBoxColumn"
        '
        'UUSRR1DataGridViewTextBoxColumn
        '
        Me.UUSRR1DataGridViewTextBoxColumn.DataPropertyName = "UUSRR1"
        Me.UUSRR1DataGridViewTextBoxColumn.HeaderText = "UUSRR1"
        Me.UUSRR1DataGridViewTextBoxColumn.Name = "UUSRR1DataGridViewTextBoxColumn"
        '
        'UUSRR2DataGridViewTextBoxColumn
        '
        Me.UUSRR2DataGridViewTextBoxColumn.DataPropertyName = "UUSRR2"
        Me.UUSRR2DataGridViewTextBoxColumn.HeaderText = "UUSRR2"
        Me.UUSRR2DataGridViewTextBoxColumn.Name = "UUSRR2DataGridViewTextBoxColumn"
        '
        'UUSRR3DataGridViewTextBoxColumn
        '
        Me.UUSRR3DataGridViewTextBoxColumn.DataPropertyName = "UUSRR3"
        Me.UUSRR3DataGridViewTextBoxColumn.HeaderText = "UUSRR3"
        Me.UUSRR3DataGridViewTextBoxColumn.Name = "UUSRR3DataGridViewTextBoxColumn"
        '
        'UUSRR4DataGridViewTextBoxColumn
        '
        Me.UUSRR4DataGridViewTextBoxColumn.DataPropertyName = "UUSRR4"
        Me.UUSRR4DataGridViewTextBoxColumn.HeaderText = "UUSRR4"
        Me.UUSRR4DataGridViewTextBoxColumn.Name = "UUSRR4DataGridViewTextBoxColumn"
        '
        'UUSRR5DataGridViewTextBoxColumn
        '
        Me.UUSRR5DataGridViewTextBoxColumn.DataPropertyName = "UUSRR5"
        Me.UUSRR5DataGridViewTextBoxColumn.HeaderText = "UUSRR5"
        Me.UUSRR5DataGridViewTextBoxColumn.Name = "UUSRR5DataGridViewTextBoxColumn"
        '
        'UCATEG1DataGridViewTextBoxColumn
        '
        Me.UCATEG1DataGridViewTextBoxColumn.DataPropertyName = "UCATEG1"
        Me.UCATEG1DataGridViewTextBoxColumn.HeaderText = "UCATEG1"
        Me.UCATEG1DataGridViewTextBoxColumn.Name = "UCATEG1DataGridViewTextBoxColumn"
        '
        'UCATEG2DataGridViewTextBoxColumn
        '
        Me.UCATEG2DataGridViewTextBoxColumn.DataPropertyName = "UCATEG2"
        Me.UCATEG2DataGridViewTextBoxColumn.HeaderText = "UCATEG2"
        Me.UCATEG2DataGridViewTextBoxColumn.Name = "UCATEG2DataGridViewTextBoxColumn"
        '
        'UCATEG3DataGridViewTextBoxColumn
        '
        Me.UCATEG3DataGridViewTextBoxColumn.DataPropertyName = "UCATEG3"
        Me.UCATEG3DataGridViewTextBoxColumn.HeaderText = "UCATEG3"
        Me.UCATEG3DataGridViewTextBoxColumn.Name = "UCATEG3DataGridViewTextBoxColumn"
        '
        'UCATEG4DataGridViewTextBoxColumn
        '
        Me.UCATEG4DataGridViewTextBoxColumn.DataPropertyName = "UCATEG4"
        Me.UCATEG4DataGridViewTextBoxColumn.HeaderText = "UCATEG4"
        Me.UCATEG4DataGridViewTextBoxColumn.Name = "UCATEG4DataGridViewTextBoxColumn"
        '
        'UCATEG5DataGridViewTextBoxColumn
        '
        Me.UCATEG5DataGridViewTextBoxColumn.DataPropertyName = "UCATEG5"
        Me.UCATEG5DataGridViewTextBoxColumn.HeaderText = "UCATEG5"
        Me.UCATEG5DataGridViewTextBoxColumn.Name = "UCATEG5DataGridViewTextBoxColumn"
        '
        'UUENDataGridViewTextBoxColumn
        '
        Me.UUENDataGridViewTextBoxColumn.DataPropertyName = "UUEN"
        Me.UUENDataGridViewTextBoxColumn.HeaderText = "UUEN"
        Me.UUENDataGridViewTextBoxColumn.Name = "UUENDataGridViewTextBoxColumn"
        '
        'UANILLDataGridViewTextBoxColumn
        '
        Me.UANILLDataGridViewTextBoxColumn.DataPropertyName = "UANILL"
        Me.UANILLDataGridViewTextBoxColumn.HeaderText = "UANILL"
        Me.UANILLDataGridViewTextBoxColumn.Name = "UANILLDataGridViewTextBoxColumn"
        '
        'USFCNSDataGridViewTextBoxColumn
        '
        Me.USFCNSDataGridViewTextBoxColumn.DataPropertyName = "USFCNS"
        Me.USFCNSDataGridViewTextBoxColumn.HeaderText = "USFCNS"
        Me.USFCNSDataGridViewTextBoxColumn.Name = "USFCNSDataGridViewTextBoxColumn"
        '
        'UBODSDataGridViewTextBoxColumn
        '
        Me.UBODSDataGridViewTextBoxColumn.DataPropertyName = "UBODS"
        Me.UBODSDataGridViewTextBoxColumn.HeaderText = "UBODS"
        Me.UBODSDataGridViewTextBoxColumn.Name = "UBODSDataGridViewTextBoxColumn"
        '
        'UQRYDataGridViewTextBoxColumn
        '
        Me.UQRYDataGridViewTextBoxColumn.DataPropertyName = "UQRY"
        Me.UQRYDataGridViewTextBoxColumn.HeaderText = "UQRY"
        Me.UQRYDataGridViewTextBoxColumn.Name = "UQRYDataGridViewTextBoxColumn"
        '
        'UTUDataGridViewTextBoxColumn
        '
        Me.UTUDataGridViewTextBoxColumn.DataPropertyName = "UTU"
        Me.UTUDataGridViewTextBoxColumn.HeaderText = "UTU"
        Me.UTUDataGridViewTextBoxColumn.Name = "UTUDataGridViewTextBoxColumn"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.DataGridView3)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(1236, 507)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "RCAA"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.DataGridView4)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(1236, 507)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "RFPARAM"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'DataGridView3
        '
        Me.DataGridView3.AutoGenerateColumns = False
        Me.DataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView3.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ASTSDataGridViewTextBoxColumn, Me.AAPPDataGridViewTextBoxColumn1, Me.AMODDataGridViewTextBoxColumn1, Me.ADESCDataGridViewTextBoxColumn1, Me.AUSRDataGridViewTextBoxColumn, Me.AAPPMNUDataGridViewTextBoxColumn, Me.AOPCMNUDataGridViewTextBoxColumn, Me.AURLDataGridViewTextBoxColumn, Me.ACC00DataGridViewTextBoxColumn, Me.ACC01DataGridViewTextBoxColumn, Me.ACC02DataGridViewTextBoxColumn, Me.ACC03DataGridViewTextBoxColumn, Me.ACC04DataGridViewTextBoxColumn, Me.ACC05DataGridViewTextBoxColumn, Me.ACC06DataGridViewTextBoxColumn, Me.ACC07DataGridViewTextBoxColumn, Me.ACC08DataGridViewTextBoxColumn, Me.ACC09DataGridViewTextBoxColumn, Me.ACC10DataGridViewTextBoxColumn, Me.ACC11DataGridViewTextBoxColumn, Me.ACC12DataGridViewTextBoxColumn, Me.ACC13DataGridViewTextBoxColumn, Me.ACC14DataGridViewTextBoxColumn, Me.ACC15DataGridViewTextBoxColumn, Me.ACC16DataGridViewTextBoxColumn, Me.ACC17DataGridViewTextBoxColumn, Me.ACC18DataGridViewTextBoxColumn, Me.ACC19DataGridViewTextBoxColumn, Me.ACC20DataGridViewTextBoxColumn, Me.AAUTDataGridViewTextBoxColumn, Me.AUPDDataGridViewTextBoxColumn})
        Me.DataGridView3.DataSource = Me.RCAABindingSource
        Me.DataGridView3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView3.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView3.Name = "DataGridView3"
        Me.DataGridView3.Size = New System.Drawing.Size(1236, 507)
        Me.DataGridView3.TabIndex = 0
        '
        'InfoNaturaDataSet2
        '
        Me.InfoNaturaDataSet2.DataSetName = "InfoNaturaDataSet2"
        Me.InfoNaturaDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'RCAABindingSource
        '
        Me.RCAABindingSource.DataMember = "RCAA"
        Me.RCAABindingSource.DataSource = Me.InfoNaturaDataSet2
        '
        'RCAATableAdapter
        '
        Me.RCAATableAdapter.ClearBeforeFill = True
        '
        'ASTSDataGridViewTextBoxColumn
        '
        Me.ASTSDataGridViewTextBoxColumn.DataPropertyName = "ASTS"
        Me.ASTSDataGridViewTextBoxColumn.HeaderText = "ASTS"
        Me.ASTSDataGridViewTextBoxColumn.Name = "ASTSDataGridViewTextBoxColumn"
        '
        'AAPPDataGridViewTextBoxColumn1
        '
        Me.AAPPDataGridViewTextBoxColumn1.DataPropertyName = "AAPP"
        Me.AAPPDataGridViewTextBoxColumn1.HeaderText = "AAPP"
        Me.AAPPDataGridViewTextBoxColumn1.Name = "AAPPDataGridViewTextBoxColumn1"
        '
        'AMODDataGridViewTextBoxColumn1
        '
        Me.AMODDataGridViewTextBoxColumn1.DataPropertyName = "AMOD"
        Me.AMODDataGridViewTextBoxColumn1.HeaderText = "AMOD"
        Me.AMODDataGridViewTextBoxColumn1.Name = "AMODDataGridViewTextBoxColumn1"
        '
        'ADESCDataGridViewTextBoxColumn1
        '
        Me.ADESCDataGridViewTextBoxColumn1.DataPropertyName = "ADESC"
        Me.ADESCDataGridViewTextBoxColumn1.HeaderText = "ADESC"
        Me.ADESCDataGridViewTextBoxColumn1.Name = "ADESCDataGridViewTextBoxColumn1"
        '
        'AUSRDataGridViewTextBoxColumn
        '
        Me.AUSRDataGridViewTextBoxColumn.DataPropertyName = "AUSR"
        Me.AUSRDataGridViewTextBoxColumn.HeaderText = "AUSR"
        Me.AUSRDataGridViewTextBoxColumn.Name = "AUSRDataGridViewTextBoxColumn"
        '
        'AAPPMNUDataGridViewTextBoxColumn
        '
        Me.AAPPMNUDataGridViewTextBoxColumn.DataPropertyName = "AAPPMNU"
        Me.AAPPMNUDataGridViewTextBoxColumn.HeaderText = "AAPPMNU"
        Me.AAPPMNUDataGridViewTextBoxColumn.Name = "AAPPMNUDataGridViewTextBoxColumn"
        '
        'AOPCMNUDataGridViewTextBoxColumn
        '
        Me.AOPCMNUDataGridViewTextBoxColumn.DataPropertyName = "AOPCMNU"
        Me.AOPCMNUDataGridViewTextBoxColumn.HeaderText = "AOPCMNU"
        Me.AOPCMNUDataGridViewTextBoxColumn.Name = "AOPCMNUDataGridViewTextBoxColumn"
        '
        'AURLDataGridViewTextBoxColumn
        '
        Me.AURLDataGridViewTextBoxColumn.DataPropertyName = "AURL"
        Me.AURLDataGridViewTextBoxColumn.HeaderText = "AURL"
        Me.AURLDataGridViewTextBoxColumn.Name = "AURLDataGridViewTextBoxColumn"
        '
        'ACC00DataGridViewTextBoxColumn
        '
        Me.ACC00DataGridViewTextBoxColumn.DataPropertyName = "ACC00"
        Me.ACC00DataGridViewTextBoxColumn.HeaderText = "ACC00"
        Me.ACC00DataGridViewTextBoxColumn.Name = "ACC00DataGridViewTextBoxColumn"
        '
        'ACC01DataGridViewTextBoxColumn
        '
        Me.ACC01DataGridViewTextBoxColumn.DataPropertyName = "ACC01"
        Me.ACC01DataGridViewTextBoxColumn.HeaderText = "ACC01"
        Me.ACC01DataGridViewTextBoxColumn.Name = "ACC01DataGridViewTextBoxColumn"
        '
        'ACC02DataGridViewTextBoxColumn
        '
        Me.ACC02DataGridViewTextBoxColumn.DataPropertyName = "ACC02"
        Me.ACC02DataGridViewTextBoxColumn.HeaderText = "ACC02"
        Me.ACC02DataGridViewTextBoxColumn.Name = "ACC02DataGridViewTextBoxColumn"
        '
        'ACC03DataGridViewTextBoxColumn
        '
        Me.ACC03DataGridViewTextBoxColumn.DataPropertyName = "ACC03"
        Me.ACC03DataGridViewTextBoxColumn.HeaderText = "ACC03"
        Me.ACC03DataGridViewTextBoxColumn.Name = "ACC03DataGridViewTextBoxColumn"
        '
        'ACC04DataGridViewTextBoxColumn
        '
        Me.ACC04DataGridViewTextBoxColumn.DataPropertyName = "ACC04"
        Me.ACC04DataGridViewTextBoxColumn.HeaderText = "ACC04"
        Me.ACC04DataGridViewTextBoxColumn.Name = "ACC04DataGridViewTextBoxColumn"
        '
        'ACC05DataGridViewTextBoxColumn
        '
        Me.ACC05DataGridViewTextBoxColumn.DataPropertyName = "ACC05"
        Me.ACC05DataGridViewTextBoxColumn.HeaderText = "ACC05"
        Me.ACC05DataGridViewTextBoxColumn.Name = "ACC05DataGridViewTextBoxColumn"
        '
        'ACC06DataGridViewTextBoxColumn
        '
        Me.ACC06DataGridViewTextBoxColumn.DataPropertyName = "ACC06"
        Me.ACC06DataGridViewTextBoxColumn.HeaderText = "ACC06"
        Me.ACC06DataGridViewTextBoxColumn.Name = "ACC06DataGridViewTextBoxColumn"
        '
        'ACC07DataGridViewTextBoxColumn
        '
        Me.ACC07DataGridViewTextBoxColumn.DataPropertyName = "ACC07"
        Me.ACC07DataGridViewTextBoxColumn.HeaderText = "ACC07"
        Me.ACC07DataGridViewTextBoxColumn.Name = "ACC07DataGridViewTextBoxColumn"
        '
        'ACC08DataGridViewTextBoxColumn
        '
        Me.ACC08DataGridViewTextBoxColumn.DataPropertyName = "ACC08"
        Me.ACC08DataGridViewTextBoxColumn.HeaderText = "ACC08"
        Me.ACC08DataGridViewTextBoxColumn.Name = "ACC08DataGridViewTextBoxColumn"
        '
        'ACC09DataGridViewTextBoxColumn
        '
        Me.ACC09DataGridViewTextBoxColumn.DataPropertyName = "ACC09"
        Me.ACC09DataGridViewTextBoxColumn.HeaderText = "ACC09"
        Me.ACC09DataGridViewTextBoxColumn.Name = "ACC09DataGridViewTextBoxColumn"
        '
        'ACC10DataGridViewTextBoxColumn
        '
        Me.ACC10DataGridViewTextBoxColumn.DataPropertyName = "ACC10"
        Me.ACC10DataGridViewTextBoxColumn.HeaderText = "ACC10"
        Me.ACC10DataGridViewTextBoxColumn.Name = "ACC10DataGridViewTextBoxColumn"
        '
        'ACC11DataGridViewTextBoxColumn
        '
        Me.ACC11DataGridViewTextBoxColumn.DataPropertyName = "ACC11"
        Me.ACC11DataGridViewTextBoxColumn.HeaderText = "ACC11"
        Me.ACC11DataGridViewTextBoxColumn.Name = "ACC11DataGridViewTextBoxColumn"
        '
        'ACC12DataGridViewTextBoxColumn
        '
        Me.ACC12DataGridViewTextBoxColumn.DataPropertyName = "ACC12"
        Me.ACC12DataGridViewTextBoxColumn.HeaderText = "ACC12"
        Me.ACC12DataGridViewTextBoxColumn.Name = "ACC12DataGridViewTextBoxColumn"
        '
        'ACC13DataGridViewTextBoxColumn
        '
        Me.ACC13DataGridViewTextBoxColumn.DataPropertyName = "ACC13"
        Me.ACC13DataGridViewTextBoxColumn.HeaderText = "ACC13"
        Me.ACC13DataGridViewTextBoxColumn.Name = "ACC13DataGridViewTextBoxColumn"
        '
        'ACC14DataGridViewTextBoxColumn
        '
        Me.ACC14DataGridViewTextBoxColumn.DataPropertyName = "ACC14"
        Me.ACC14DataGridViewTextBoxColumn.HeaderText = "ACC14"
        Me.ACC14DataGridViewTextBoxColumn.Name = "ACC14DataGridViewTextBoxColumn"
        '
        'ACC15DataGridViewTextBoxColumn
        '
        Me.ACC15DataGridViewTextBoxColumn.DataPropertyName = "ACC15"
        Me.ACC15DataGridViewTextBoxColumn.HeaderText = "ACC15"
        Me.ACC15DataGridViewTextBoxColumn.Name = "ACC15DataGridViewTextBoxColumn"
        '
        'ACC16DataGridViewTextBoxColumn
        '
        Me.ACC16DataGridViewTextBoxColumn.DataPropertyName = "ACC16"
        Me.ACC16DataGridViewTextBoxColumn.HeaderText = "ACC16"
        Me.ACC16DataGridViewTextBoxColumn.Name = "ACC16DataGridViewTextBoxColumn"
        '
        'ACC17DataGridViewTextBoxColumn
        '
        Me.ACC17DataGridViewTextBoxColumn.DataPropertyName = "ACC17"
        Me.ACC17DataGridViewTextBoxColumn.HeaderText = "ACC17"
        Me.ACC17DataGridViewTextBoxColumn.Name = "ACC17DataGridViewTextBoxColumn"
        '
        'ACC18DataGridViewTextBoxColumn
        '
        Me.ACC18DataGridViewTextBoxColumn.DataPropertyName = "ACC18"
        Me.ACC18DataGridViewTextBoxColumn.HeaderText = "ACC18"
        Me.ACC18DataGridViewTextBoxColumn.Name = "ACC18DataGridViewTextBoxColumn"
        '
        'ACC19DataGridViewTextBoxColumn
        '
        Me.ACC19DataGridViewTextBoxColumn.DataPropertyName = "ACC19"
        Me.ACC19DataGridViewTextBoxColumn.HeaderText = "ACC19"
        Me.ACC19DataGridViewTextBoxColumn.Name = "ACC19DataGridViewTextBoxColumn"
        '
        'ACC20DataGridViewTextBoxColumn
        '
        Me.ACC20DataGridViewTextBoxColumn.DataPropertyName = "ACC20"
        Me.ACC20DataGridViewTextBoxColumn.HeaderText = "ACC20"
        Me.ACC20DataGridViewTextBoxColumn.Name = "ACC20DataGridViewTextBoxColumn"
        '
        'AAUTDataGridViewTextBoxColumn
        '
        Me.AAUTDataGridViewTextBoxColumn.DataPropertyName = "AAUT"
        Me.AAUTDataGridViewTextBoxColumn.HeaderText = "AAUT"
        Me.AAUTDataGridViewTextBoxColumn.Name = "AAUTDataGridViewTextBoxColumn"
        '
        'AUPDDataGridViewTextBoxColumn
        '
        Me.AUPDDataGridViewTextBoxColumn.DataPropertyName = "AUPD"
        Me.AUPDDataGridViewTextBoxColumn.HeaderText = "AUPD"
        Me.AUPDDataGridViewTextBoxColumn.Name = "AUPDDataGridViewTextBoxColumn"
        '
        'DataGridView4
        '
        Me.DataGridView4.AutoGenerateColumns = False
        Me.DataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView4.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CCNUMDataGridViewTextBoxColumn, Me.CCIDDataGridViewTextBoxColumn, Me.CCTABLDataGridViewTextBoxColumn, Me.CCCODEDataGridViewTextBoxColumn, Me.CCCODE2DataGridViewTextBoxColumn, Me.CCCODE3DataGridViewTextBoxColumn, Me.CCCODENDataGridViewTextBoxColumn, Me.CCLANGDataGridViewTextBoxColumn, Me.CCALTCDataGridViewTextBoxColumn, Me.CCDESCDataGridViewTextBoxColumn, Me.CCSDSCDataGridViewTextBoxColumn, Me.CCNOT1DataGridViewTextBoxColumn, Me.CCNOT2DataGridViewTextBoxColumn, Me.CCUDC0DataGridViewTextBoxColumn, Me.CCUDC1DataGridViewTextBoxColumn, Me.CCUDC2DataGridViewTextBoxColumn, Me.CCUDC3DataGridViewTextBoxColumn, Me.CCUDC4DataGridViewTextBoxColumn, Me.CCUDC5DataGridViewTextBoxColumn, Me.CCUDC6DataGridViewTextBoxColumn, Me.CCUDC7DataGridViewTextBoxColumn, Me.CCUDC8DataGridViewTextBoxColumn, Me.CCUDC9DataGridViewTextBoxColumn, Me.CCUDV1DataGridViewTextBoxColumn, Me.CCUDV2DataGridViewTextBoxColumn, Me.CCUDV3DataGridViewTextBoxColumn, Me.CCUDV4DataGridViewTextBoxColumn, Me.CCUDTSDataGridViewTextBoxColumn, Me.CCUDDTDataGridViewTextBoxColumn, Me.CCRESVDataGridViewTextBoxColumn, Me.CCENDTDataGridViewTextBoxColumn, Me.CCENUSDataGridViewTextBoxColumn, Me.CCMNDTDataGridViewTextBoxColumn, Me.CCMNUSDataGridViewTextBoxColumn, Me.CCNOTEDataGridViewTextBoxColumn})
        Me.DataGridView4.DataSource = Me.RFPARAMBindingSource
        Me.DataGridView4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView4.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView4.Name = "DataGridView4"
        Me.DataGridView4.Size = New System.Drawing.Size(1236, 507)
        Me.DataGridView4.TabIndex = 0
        '
        'InfoNaturaDataSet3
        '
        Me.InfoNaturaDataSet3.DataSetName = "InfoNaturaDataSet3"
        Me.InfoNaturaDataSet3.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'RFPARAMBindingSource
        '
        Me.RFPARAMBindingSource.DataMember = "RFPARAM"
        Me.RFPARAMBindingSource.DataSource = Me.InfoNaturaDataSet3
        '
        'RFPARAMTableAdapter
        '
        Me.RFPARAMTableAdapter.ClearBeforeFill = True
        '
        'CCNUMDataGridViewTextBoxColumn
        '
        Me.CCNUMDataGridViewTextBoxColumn.DataPropertyName = "CCNUM"
        Me.CCNUMDataGridViewTextBoxColumn.HeaderText = "CCNUM"
        Me.CCNUMDataGridViewTextBoxColumn.Name = "CCNUMDataGridViewTextBoxColumn"
        '
        'CCIDDataGridViewTextBoxColumn
        '
        Me.CCIDDataGridViewTextBoxColumn.DataPropertyName = "CCID"
        Me.CCIDDataGridViewTextBoxColumn.HeaderText = "CCID"
        Me.CCIDDataGridViewTextBoxColumn.Name = "CCIDDataGridViewTextBoxColumn"
        '
        'CCTABLDataGridViewTextBoxColumn
        '
        Me.CCTABLDataGridViewTextBoxColumn.DataPropertyName = "CCTABL"
        Me.CCTABLDataGridViewTextBoxColumn.HeaderText = "CCTABL"
        Me.CCTABLDataGridViewTextBoxColumn.Name = "CCTABLDataGridViewTextBoxColumn"
        '
        'CCCODEDataGridViewTextBoxColumn
        '
        Me.CCCODEDataGridViewTextBoxColumn.DataPropertyName = "CCCODE"
        Me.CCCODEDataGridViewTextBoxColumn.HeaderText = "CCCODE"
        Me.CCCODEDataGridViewTextBoxColumn.Name = "CCCODEDataGridViewTextBoxColumn"
        '
        'CCCODE2DataGridViewTextBoxColumn
        '
        Me.CCCODE2DataGridViewTextBoxColumn.DataPropertyName = "CCCODE2"
        Me.CCCODE2DataGridViewTextBoxColumn.HeaderText = "CCCODE2"
        Me.CCCODE2DataGridViewTextBoxColumn.Name = "CCCODE2DataGridViewTextBoxColumn"
        '
        'CCCODE3DataGridViewTextBoxColumn
        '
        Me.CCCODE3DataGridViewTextBoxColumn.DataPropertyName = "CCCODE3"
        Me.CCCODE3DataGridViewTextBoxColumn.HeaderText = "CCCODE3"
        Me.CCCODE3DataGridViewTextBoxColumn.Name = "CCCODE3DataGridViewTextBoxColumn"
        '
        'CCCODENDataGridViewTextBoxColumn
        '
        Me.CCCODENDataGridViewTextBoxColumn.DataPropertyName = "CCCODEN"
        Me.CCCODENDataGridViewTextBoxColumn.HeaderText = "CCCODEN"
        Me.CCCODENDataGridViewTextBoxColumn.Name = "CCCODENDataGridViewTextBoxColumn"
        '
        'CCLANGDataGridViewTextBoxColumn
        '
        Me.CCLANGDataGridViewTextBoxColumn.DataPropertyName = "CCLANG"
        Me.CCLANGDataGridViewTextBoxColumn.HeaderText = "CCLANG"
        Me.CCLANGDataGridViewTextBoxColumn.Name = "CCLANGDataGridViewTextBoxColumn"
        '
        'CCALTCDataGridViewTextBoxColumn
        '
        Me.CCALTCDataGridViewTextBoxColumn.DataPropertyName = "CCALTC"
        Me.CCALTCDataGridViewTextBoxColumn.HeaderText = "CCALTC"
        Me.CCALTCDataGridViewTextBoxColumn.Name = "CCALTCDataGridViewTextBoxColumn"
        '
        'CCDESCDataGridViewTextBoxColumn
        '
        Me.CCDESCDataGridViewTextBoxColumn.DataPropertyName = "CCDESC"
        Me.CCDESCDataGridViewTextBoxColumn.HeaderText = "CCDESC"
        Me.CCDESCDataGridViewTextBoxColumn.Name = "CCDESCDataGridViewTextBoxColumn"
        '
        'CCSDSCDataGridViewTextBoxColumn
        '
        Me.CCSDSCDataGridViewTextBoxColumn.DataPropertyName = "CCSDSC"
        Me.CCSDSCDataGridViewTextBoxColumn.HeaderText = "CCSDSC"
        Me.CCSDSCDataGridViewTextBoxColumn.Name = "CCSDSCDataGridViewTextBoxColumn"
        '
        'CCNOT1DataGridViewTextBoxColumn
        '
        Me.CCNOT1DataGridViewTextBoxColumn.DataPropertyName = "CCNOT1"
        Me.CCNOT1DataGridViewTextBoxColumn.HeaderText = "CCNOT1"
        Me.CCNOT1DataGridViewTextBoxColumn.Name = "CCNOT1DataGridViewTextBoxColumn"
        '
        'CCNOT2DataGridViewTextBoxColumn
        '
        Me.CCNOT2DataGridViewTextBoxColumn.DataPropertyName = "CCNOT2"
        Me.CCNOT2DataGridViewTextBoxColumn.HeaderText = "CCNOT2"
        Me.CCNOT2DataGridViewTextBoxColumn.Name = "CCNOT2DataGridViewTextBoxColumn"
        '
        'CCUDC0DataGridViewTextBoxColumn
        '
        Me.CCUDC0DataGridViewTextBoxColumn.DataPropertyName = "CCUDC0"
        Me.CCUDC0DataGridViewTextBoxColumn.HeaderText = "CCUDC0"
        Me.CCUDC0DataGridViewTextBoxColumn.Name = "CCUDC0DataGridViewTextBoxColumn"
        '
        'CCUDC1DataGridViewTextBoxColumn
        '
        Me.CCUDC1DataGridViewTextBoxColumn.DataPropertyName = "CCUDC1"
        Me.CCUDC1DataGridViewTextBoxColumn.HeaderText = "CCUDC1"
        Me.CCUDC1DataGridViewTextBoxColumn.Name = "CCUDC1DataGridViewTextBoxColumn"
        '
        'CCUDC2DataGridViewTextBoxColumn
        '
        Me.CCUDC2DataGridViewTextBoxColumn.DataPropertyName = "CCUDC2"
        Me.CCUDC2DataGridViewTextBoxColumn.HeaderText = "CCUDC2"
        Me.CCUDC2DataGridViewTextBoxColumn.Name = "CCUDC2DataGridViewTextBoxColumn"
        '
        'CCUDC3DataGridViewTextBoxColumn
        '
        Me.CCUDC3DataGridViewTextBoxColumn.DataPropertyName = "CCUDC3"
        Me.CCUDC3DataGridViewTextBoxColumn.HeaderText = "CCUDC3"
        Me.CCUDC3DataGridViewTextBoxColumn.Name = "CCUDC3DataGridViewTextBoxColumn"
        '
        'CCUDC4DataGridViewTextBoxColumn
        '
        Me.CCUDC4DataGridViewTextBoxColumn.DataPropertyName = "CCUDC4"
        Me.CCUDC4DataGridViewTextBoxColumn.HeaderText = "CCUDC4"
        Me.CCUDC4DataGridViewTextBoxColumn.Name = "CCUDC4DataGridViewTextBoxColumn"
        '
        'CCUDC5DataGridViewTextBoxColumn
        '
        Me.CCUDC5DataGridViewTextBoxColumn.DataPropertyName = "CCUDC5"
        Me.CCUDC5DataGridViewTextBoxColumn.HeaderText = "CCUDC5"
        Me.CCUDC5DataGridViewTextBoxColumn.Name = "CCUDC5DataGridViewTextBoxColumn"
        '
        'CCUDC6DataGridViewTextBoxColumn
        '
        Me.CCUDC6DataGridViewTextBoxColumn.DataPropertyName = "CCUDC6"
        Me.CCUDC6DataGridViewTextBoxColumn.HeaderText = "CCUDC6"
        Me.CCUDC6DataGridViewTextBoxColumn.Name = "CCUDC6DataGridViewTextBoxColumn"
        '
        'CCUDC7DataGridViewTextBoxColumn
        '
        Me.CCUDC7DataGridViewTextBoxColumn.DataPropertyName = "CCUDC7"
        Me.CCUDC7DataGridViewTextBoxColumn.HeaderText = "CCUDC7"
        Me.CCUDC7DataGridViewTextBoxColumn.Name = "CCUDC7DataGridViewTextBoxColumn"
        '
        'CCUDC8DataGridViewTextBoxColumn
        '
        Me.CCUDC8DataGridViewTextBoxColumn.DataPropertyName = "CCUDC8"
        Me.CCUDC8DataGridViewTextBoxColumn.HeaderText = "CCUDC8"
        Me.CCUDC8DataGridViewTextBoxColumn.Name = "CCUDC8DataGridViewTextBoxColumn"
        '
        'CCUDC9DataGridViewTextBoxColumn
        '
        Me.CCUDC9DataGridViewTextBoxColumn.DataPropertyName = "CCUDC9"
        Me.CCUDC9DataGridViewTextBoxColumn.HeaderText = "CCUDC9"
        Me.CCUDC9DataGridViewTextBoxColumn.Name = "CCUDC9DataGridViewTextBoxColumn"
        '
        'CCUDV1DataGridViewTextBoxColumn
        '
        Me.CCUDV1DataGridViewTextBoxColumn.DataPropertyName = "CCUDV1"
        Me.CCUDV1DataGridViewTextBoxColumn.HeaderText = "CCUDV1"
        Me.CCUDV1DataGridViewTextBoxColumn.Name = "CCUDV1DataGridViewTextBoxColumn"
        '
        'CCUDV2DataGridViewTextBoxColumn
        '
        Me.CCUDV2DataGridViewTextBoxColumn.DataPropertyName = "CCUDV2"
        Me.CCUDV2DataGridViewTextBoxColumn.HeaderText = "CCUDV2"
        Me.CCUDV2DataGridViewTextBoxColumn.Name = "CCUDV2DataGridViewTextBoxColumn"
        '
        'CCUDV3DataGridViewTextBoxColumn
        '
        Me.CCUDV3DataGridViewTextBoxColumn.DataPropertyName = "CCUDV3"
        Me.CCUDV3DataGridViewTextBoxColumn.HeaderText = "CCUDV3"
        Me.CCUDV3DataGridViewTextBoxColumn.Name = "CCUDV3DataGridViewTextBoxColumn"
        '
        'CCUDV4DataGridViewTextBoxColumn
        '
        Me.CCUDV4DataGridViewTextBoxColumn.DataPropertyName = "CCUDV4"
        Me.CCUDV4DataGridViewTextBoxColumn.HeaderText = "CCUDV4"
        Me.CCUDV4DataGridViewTextBoxColumn.Name = "CCUDV4DataGridViewTextBoxColumn"
        '
        'CCUDTSDataGridViewTextBoxColumn
        '
        Me.CCUDTSDataGridViewTextBoxColumn.DataPropertyName = "CCUDTS"
        Me.CCUDTSDataGridViewTextBoxColumn.HeaderText = "CCUDTS"
        Me.CCUDTSDataGridViewTextBoxColumn.Name = "CCUDTSDataGridViewTextBoxColumn"
        '
        'CCUDDTDataGridViewTextBoxColumn
        '
        Me.CCUDDTDataGridViewTextBoxColumn.DataPropertyName = "CCUDDT"
        Me.CCUDDTDataGridViewTextBoxColumn.HeaderText = "CCUDDT"
        Me.CCUDDTDataGridViewTextBoxColumn.Name = "CCUDDTDataGridViewTextBoxColumn"
        '
        'CCRESVDataGridViewTextBoxColumn
        '
        Me.CCRESVDataGridViewTextBoxColumn.DataPropertyName = "CCRESV"
        Me.CCRESVDataGridViewTextBoxColumn.HeaderText = "CCRESV"
        Me.CCRESVDataGridViewTextBoxColumn.Name = "CCRESVDataGridViewTextBoxColumn"
        '
        'CCENDTDataGridViewTextBoxColumn
        '
        Me.CCENDTDataGridViewTextBoxColumn.DataPropertyName = "CCENDT"
        Me.CCENDTDataGridViewTextBoxColumn.HeaderText = "CCENDT"
        Me.CCENDTDataGridViewTextBoxColumn.Name = "CCENDTDataGridViewTextBoxColumn"
        '
        'CCENUSDataGridViewTextBoxColumn
        '
        Me.CCENUSDataGridViewTextBoxColumn.DataPropertyName = "CCENUS"
        Me.CCENUSDataGridViewTextBoxColumn.HeaderText = "CCENUS"
        Me.CCENUSDataGridViewTextBoxColumn.Name = "CCENUSDataGridViewTextBoxColumn"
        '
        'CCMNDTDataGridViewTextBoxColumn
        '
        Me.CCMNDTDataGridViewTextBoxColumn.DataPropertyName = "CCMNDT"
        Me.CCMNDTDataGridViewTextBoxColumn.HeaderText = "CCMNDT"
        Me.CCMNDTDataGridViewTextBoxColumn.Name = "CCMNDTDataGridViewTextBoxColumn"
        '
        'CCMNUSDataGridViewTextBoxColumn
        '
        Me.CCMNUSDataGridViewTextBoxColumn.DataPropertyName = "CCMNUS"
        Me.CCMNUSDataGridViewTextBoxColumn.HeaderText = "CCMNUS"
        Me.CCMNUSDataGridViewTextBoxColumn.Name = "CCMNUSDataGridViewTextBoxColumn"
        '
        'CCNOTEDataGridViewTextBoxColumn
        '
        Me.CCNOTEDataGridViewTextBoxColumn.DataPropertyName = "CCNOTE"
        Me.CCNOTEDataGridViewTextBoxColumn.HeaderText = "CCNOTE"
        Me.CCNOTEDataGridViewTextBoxColumn.Name = "CCNOTEDataGridViewTextBoxColumn"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1300, 620)
        Me.Controls.Add(Me.sqlEXE)
        Me.Controls.Add(Me.txtWhere)
        Me.Controls.Add(Me.sqlAnd)
        Me.Controls.Add(Me.TAB)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.TAB.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RCAMBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InfoNaturaDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InfoNaturaDataSetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InfoNaturaDataSetBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InfoNaturaDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RCAUBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        CType(Me.DataGridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InfoNaturaDataSet2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RCAABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InfoNaturaDataSet3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RFPARAMBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TAB As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents InfoNaturaDataSet As dfu.InfoNaturaDataSet
    Friend WithEvents RCAMBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents RCAMTableAdapter As dfu.InfoNaturaDataSetTableAdapters.RCAMTableAdapter
    Friend WithEvents AIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ALVLDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AORDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AAPPDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AMODDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AGRPDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents APARENTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ATITDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ALINKDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACLASSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AISETDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents APARM1DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents APARM2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents APARM3DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents APARM4DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents APARM5DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ADESCDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents sqlAnd As System.Windows.Forms.Button
    Friend WithEvents txtWhere As System.Windows.Forms.TextBox
    Friend WithEvents sqlEXE As System.Windows.Forms.Button
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents InfoNaturaDataSetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InfoNaturaDataSetBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents InfoNaturaDataSet1 As dfu.InfoNaturaDataSet1
    Friend WithEvents RCAUBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents RCAUTableAdapter As dfu.InfoNaturaDataSet1TableAdapters.RCAUTableAdapter
    Friend WithEvents USTSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UTIPODataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UUSRDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UUSRBOLDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UUSRABRDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UMODDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UPLANTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UWHSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UGRPDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UTRADataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UVENDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UPASSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UNOMDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UEMAILDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UFECCADDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UDIASDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UUSRR1DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UUSRR2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UUSRR3DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UUSRR4DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UUSRR5DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UCATEG1DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UCATEG2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UCATEG3DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UCATEG4DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UCATEG5DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UUENDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UANILLDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents USFCNSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UBODSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UQRYDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UTUDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents DataGridView3 As System.Windows.Forms.DataGridView
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents InfoNaturaDataSet2 As dfu.InfoNaturaDataSet2
    Friend WithEvents RCAABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents RCAATableAdapter As dfu.InfoNaturaDataSet2TableAdapters.RCAATableAdapter
    Friend WithEvents ASTSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AAPPDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AMODDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ADESCDataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AUSRDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AAPPMNUDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AOPCMNUDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AURLDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACC00DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACC01DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACC02DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACC03DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACC04DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACC05DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACC06DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACC07DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACC08DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACC09DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACC10DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACC11DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACC12DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACC13DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACC14DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACC15DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACC16DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACC17DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACC18DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACC19DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ACC20DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AAUTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AUPDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridView4 As System.Windows.Forms.DataGridView
    Friend WithEvents InfoNaturaDataSet3 As dfu.InfoNaturaDataSet3
    Friend WithEvents RFPARAMBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents RFPARAMTableAdapter As dfu.InfoNaturaDataSet3TableAdapters.RFPARAMTableAdapter
    Friend WithEvents CCNUMDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCTABLDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCCODEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCCODE2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCCODE3DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCCODENDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCLANGDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCALTCDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCDESCDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCSDSCDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCNOT1DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCNOT2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCUDC0DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCUDC1DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCUDC2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCUDC3DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCUDC4DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCUDC5DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCUDC6DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCUDC7DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCUDC8DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCUDC9DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCUDV1DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCUDV2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCUDV3DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCUDV4DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCUDTSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCUDDTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCRESVDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCENDTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCENUSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCMNDTDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCMNUSDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CCNOTEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn

End Class
