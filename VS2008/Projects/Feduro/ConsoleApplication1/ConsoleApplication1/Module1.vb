﻿Imports System.IO
Imports System.Text

Module Module1
    Public gsKeyWords As String = ""

    Sub Main()
        wrtLogHtml("sql1", "SQL0104: S mbolo no v lido. S mbolos v lidos: ( + - ? : DAY INF NAN NOT RID ROW. Causa . . . . . : Se ha detectado un error de sintaxis en el s mbolo . El s mbolo no es un s mbolo v lido. Una lista parcial de s mbolos v lidos es ( + - ? : DAY INF NAN NOT RID ROW. Esta lista presupone que la sentencia es correcta hasta el s mbolo. El error puede estar anteriormente en la sentencia, pero la sintaxis de la sentencia aparece como v lida hasta este punto. Recuperaci n . : Efect e una o m s de las siguientes acciones y vuelva a intentar la petici n: -- Verifique la sentencia SQL en el  rea del s mbolo . Corrija la sentencia. El error podr a ser la omisi n de una coma o comillas; podr a tratarse de una palabra con errores ortogr ficos, o podr a estar relacionado con el orden de las cl usulas. -- Si el s mbolo de error es , corrija la sentencia SQL porque no finaliza con una cl usula v lida. (IBMDA400 Command) ")
    End Sub


    Public Sub wrtLogHtml(sql As String, ByVal Descrip As String)
        Dim fEntrada As String = ""
        Dim linea As String = Format(Now(), "yyyy-MM-dd HH:mm:ss") & "|"
        Dim strStreamW As Stream = Nothing
        Dim sApp As String = My.Application.Info.ProductName & "." & _
                                   My.Application.Info.Version.Major & "." & _
                                   My.Application.Info.Version.Minor & "." & _
                                   My.Application.Info.Version.Revision

        If gsKeyWords.Trim <> "" Then
            Descrip = gsKeyWords & "<br>" & Descrip
        End If

        linea &= System.Net.Dns.GetHostName() & "|" & sApp & "|" & Descrip.Replace("|", "?") & "|" & sql.Replace("|", "?")

        fEntrada = Replace("c:\Feduro\log\log" & Format(Now(), "yyyyMM") & ".csv", "\\", "\")

        If File.Exists(fEntrada) Then
            strStreamW = File.Open(fEntrada, FileMode.Open) 'Abrimos el archivo
        Else
            strStreamW = File.Create(fEntrada) ' lo creamos
        End If
        strStreamW.Close()
        Try
            ' Create an instance of StreamReader to read from a file.
            Dim sr As StreamReader = New StreamReader(fEntrada)
            Dim sb As New StringBuilder()
            sb.Append(sr.ReadToEnd())
            sb.Append(linea)
            sr.Close()

            Dim sw As StreamWriter = New StreamWriter(fEntrada)
            sw.WriteLine(sb.ToString())
            sw.Close()

        Catch E As Exception

        End Try

    End Sub

End Module
