﻿Imports PDF_In_The_BoxCtl
Imports System
Imports System.IO
Imports System.Text
Public Class Diario
    Dim flag As Integer
    Dim rsTit As New ADODB.Recordset
    Dim rsCab As New ADODB.Recordset
    Dim rsPed As New ADODB.Recordset
    Dim rsDet As New ADODB.Recordset
    Dim rsNotas As New ADODB.Recordset
    Dim DocScanner As Integer, Observ As String, ImprimeIAL As String
    Dim bColor As Boolean
    Dim FechaImpresion
    Dim Referencia
    Public gsKeyWords As String = ""
    Dim key
    Dim bQuimicos As Boolean
    Dim NumPaginas
    Dim Sellos() As String
    Dim Fila As Integer
    Dim Ta As TBoxTable
    Const CELDA_NORMAL As String = "BrushColor = 255, 255, 255"
    'Const CELDA_SOMBRA As String = "BrushColor = 215, 215, 215"
    Const CELDA_SOMBRA As String = "BrushColor = 255, 255, 255"
    Const TABLE_HEADER_STYLE As String = "ChildrenStyle=""BorderStyle=rect;FontSize=9;Fontname=Arial;FontBold=1;Alignment=Center;VertAlignment=Center"""
    Const ESTILO_TIT As String = "Normal;fontsize=6;Fontname=Corbel;BorderColor=Black;Alignment=Center;VertAlignment=Center"

    Private Sub Balance_BeforePutBand(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_BeforePutBandEvent) Handles Balance.BeforePutBand

        If e.aBand.Role = TBandRole.rlDetail Then
            If bColor And flag = 0 Then

                e.aBand.BrushColor = RGB(215, 215, 215)
            Else
                e.aBand.BrushColor = RGB(255, 255, 255)
            End If
            bColor = Not bColor
        End If
        Fila = Fila + 1
        Debug.Print(Balance.PenY)
        If Fila >= 35 Then
            Balance.NewPage()
            Fila = 0
        End If
Manejo_Error:
        If Err.Number <> 0 Then
            WrtTxtError(LOG_File, "BeforePutBand")
            End
        End If

    End Sub
   

    Sub Balancee()

        Dim rs As New ADODB.Recordset
        Dim rs2 As New ADODB.Recordset
        Dim rs3 As New ADODB.Recordset
        Dim ArchivoPDF As String



        ArchivoPDF = PDF_Folder & "LibroDiario" & Control & ".pdf"
        With Balance
            .FileName = ArchivoPDF
            .Title = "Libro Diario - "
            .WantShow = bPRUEBAS
            .WantPageCount = True
            .PaperSizeName = "Letter"
            .Orientation = TPrinterOrientation.poLandscape
            .BeginDoc()
            Const NUM_FORMAT As String = "#,##0.00"
            Dim b As TBoxBand
            Fila = 0

            fSql = " UPDATE BSSFTASK SET  "
            fSql = fSql & " TLXMSG = 'EJECUTANDO',  "
            fSql = fSql & " TMSG = 'Generando Libro Diario'  "
            fSql = fSql & " WHERE TKEY = '" & key & "' "
            DB.Execute(fSql)


            fSql = " SELECT "
            fSql = fSql & " DCOMPRO "
            fSql = fSql & " FROM BSSFFNDIA "
            fSql = fSql & " WHERE "
            fSql = fSql & " DCONTROL = '" & Control & "'"
            fSql = fSql & " GROUP BY DCOMPRO ORDER BY DCOMPRO"
            rs2.Open(fSql, DB, 2, 2)
            If rs2.EOF Then
                WrtTxtError(LOG_File, "Libro no se encontro.")
                fSql = " UPDATE BSSFTASK SET  "
                fSql = fSql & " TLXMSG = 'EJECUTADA',  "
                fSql = fSql & " TMSG = 'Libro Diario',  "
                fSql = fSql & " STS2 = 1 "
                fSql = fSql & " WHERE TKEY = '" & key & "' "
                DB.Execute(fSql)
                Exit Sub
               
            End If

            While (Not rs2.EOF)
                 fSql = " SELECT "
                fSql = fSql & " DIGITS(DYEAR) BYEAR,   "
                fSql = fSql & " DIGITS(DPERIO) BPERIO,  "
                fSql = fSql & " DCOMPRO, "
                fSql = fSql & " DCUENTA, "
                fSql = fSql & " DDESCTA, "
                fSql = fSql & " DOUBLE(DDEBITO) DDEBITO, "
                fSql = fSql & " DOUBLE(DCREDITO) DCREDITO"
                fSql = fSql & " FROM BSSFFNDIA "
                fSql = fSql & " WHERE "
                fSql = fSql & " DCONTROL = '" & Control & "'"
                fSql = fSql & " AND DCOMPRO = '" & rs2("DCOMPRO").Value & "'"
                fSql = fSql & " GROUP BY DYEAR, DPERIO, DCOMPRO, DCUENTA ,DDESCTA, DDEBITO, DCREDITO"
                fSql = fSql & " ORDER BY DCOMPRO"

                rs.Open(fSql, DB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockReadOnly)

                Dim Ta As TBoxTable
                Ta = .CreateTable("BorderStyle=None")
                Ta.Assign(rs)

                Dim Detail As TBoxBand
                Detail = Ta.CreateBand()
                Detail.Role = TBandRole.rlDetail
                Detail.ChildrenStyle = "FontSize=8;Fontname=Arial; VertAlignment=Center"
                Detail.Height = 35
                Detail.ChildrenStyle = "FontSize=8;Fontname=Arial; VertAlignment=Center;BorderStyle=LeftRight"
                Detail.CreateCell(100, "BorderStyle=Left").CreateText("Alignment=Left").Bind("DCOMPRO")
                Detail.CreateCell(300, "BorderStyle=Left").CreateText("Alignment=Left").Bind("DCUENTA")
                Detail.CreateCell(900, "BorderStyle=none").CreateText("Alignment=Left").Bind("DDESCTA")
                With Detail.CreateCell(625, "BorderStyle=none").CreateText("Alignment=Right")
                    .Bind("DDEBITO")
                    .Format = NUM_FORMAT
                End With
                With Detail.CreateCell(625, "BorderStyle=none").CreateText("Alignment=Right")
                    .Bind("DCREDITO")
                    .Format = NUM_FORMAT
                End With

                Detail.Breakable = True
                Ta.Put()


                fSql = " SELECT "
                fSql = fSql & " DOUBLE(SUM(DDEBITO)) DEBITO, "
                fSql = fSql & " DOUBLE(SUM(DCREDITO)) CREDITO"
                fSql = fSql & " FROM BSSFFNDIA "
                fSql = fSql & " WHERE "
                fSql = fSql & " DCONTROL = '" & Control & "'"
                fSql = fSql & " AND DCOMPRO = '" & rs2("DCOMPRO").Value & "'"
                rs3.Open(fSql, DB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockReadOnly)

                Dim Ta1 As TBoxTable
                Ta1 = .CreateTable("BorderStyle=None")
                Ta1.Assign(rs3)


                Dim Detail1 As TBoxBand
                Detail1 = Ta1.CreateBand()
                Detail1.Role = TBandRole.rlDetail
                Detail1.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center"
                Detail1.Height = 35
                Detail1.ChildrenStyle = "FontSize=8;Fontname=Arial; VertAlignment=Center;BorderStyle=none"

                With Detail1.CreateCell(1925, "BorderStyle=none").CreateText("Alignment=Right")
                    .Bind("DEBITO")
                    .Format = NUM_FORMAT
                End With

                With Detail1.CreateCell(625, "BorderStyle=none").CreateText("Alignment=Right")
                    .Bind("CREDITO")
                    .Format = NUM_FORMAT
                End With

                Detail1.Breakable = True
                Ta1.Put()

                b = .CreateBand("BorderStyle=Top;Margins=130,200,100,180")
                b.Height = 50
                b.ChildrenStyle = "BorderStyle=Top;Alignment=Center"
                b.CreateCell(1010).CreateText().Assign("")
                b.CreateCell(480).CreateText().Assign("")
                b.CreateCell(1010).CreateText().Assign("")
                b.Put()
                rs3.Close()
                rs.Close()
                rs2.MoveNext()
            End While
            fSql = " UPDATE BSSFTASK SET  "
            fSql = fSql & " TLXMSG = 'EJECUTADA',  "
            fSql = fSql & " TMSG = 'Libro Diario  Generado No Paginas: " & NumPaginas & " ',  "
            fSql = fSql & " TURL = '" & Replace(ArchivoPDF, "C:\PortalLB", "") & "',  "
            fSql = fSql & " STS2 = 1 "
            fSql = fSql & " WHERE TKEY = '" & key & "' "
            DB.Execute(fSql)

            rs2.Close()

            .EndDoc()
        End With

    End Sub



    Function FinDocumento()
        FinDocumento = ""
        Dim b As TBoxBand
        With Me.Balance

            Balance.NewPage()
            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "BorderStyle=rect;Alignment=Center"
            b.CreateCell(1010).CreateText().Assign("")
            b.CreateCell(480).CreateImage("Alignment=Right;VertAlignment=Top").Assign(My.Settings.CarpetaGneral + "\Fin del Informe.png")
            b.CreateCell(1010).CreateText().Assign("")
            b.Put()

        End With
        Return FinDocumento
    End Function

    Private Sub Balance_Enter(sender As System.Object, e As System.EventArgs) Handles Balance.Enter

    End Sub

    Private Sub Balance_OnBottomOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnBottomOfPageEvent) Handles Balance.OnBottomOfPage
        Dim b As TBoxBand
        Dim rs3 As New ADODB.Recordset
  

        With Balance

            If e.lastPage Then
                If Fila < 45 Then
                    b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
                    b.ChildrenStyle = "BorderStyle=None;Alignment=Center"
                    b.Height = 50
                    b.CreateCell(1040).CreateText().Assign("")
                    b.CreateCell(480).CreateText().Assign("Fin del Informe")
                    '               b.CreateCell(480).CreateImage("Alignment=Right;VertAlignment=Top").Assign App.Path + "\Fin del Informe.png"
                    b.CreateCell(1030).CreateText().Assign("")
                    b.Put()
                End If

                If Balance.PageCount Mod 2 = 0 Then
                    FinDocumento()
                End If
            End If
            NumPaginas = NumPaginas + 1
        End With

    End Sub

    Private Sub Balance_OnTopOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnTopOfPageEvent) Handles Balance.OnTopOfPage

        Dim b As TBoxBand

        With Balance
            .DefineStyle("Normal", "0; Margins=130,200,100,150; fontsize=10;Fontname=Arial;BorderColor=Black")
            .Style = "Normal"

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=12;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(1200).CreateText("Alignment=Left;FontSize=12").Assign("LIBRO DIARIO")
            b.CreateCell(1000).CreateText("Alignment=Right;FontSize=8;FontColor=Red").Assign(Referencia.ToString)
            b.CreateCell(100).CreateText("Alignment=Left;FontSize=8;FontColor=Blue").Assign(Consecutivo + Balance.PageCount)
            b.CreateCell(200).CreateText("Alignment=Right;FontSize=8").Assign(FechaImpresion.ToString)

            b.Put()

            Dim rs As New ADODB.Recordset
 
            fSql = " SELECT "
            fSql = fSql & " DIGITS(DYEAR) BYEAR,   "
            fSql = fSql & " DIGITS(DPERIO) BPERIO  "
            fSql = fSql & " FROM BSSFFNDIA "
            fSql = fSql & " WHERE "
            fSql = fSql & " DCONTROL = '" & Control & "'"
            rs.Open(fSql, DB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockReadOnly)
            If rs.EOF Then
                WrtTxtError(LOG_File, "Libro sin datos")
                Exit Sub
            End If

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=8;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(200).CreateText("Alignment=Left;FontSize=8").Assign("PERIODO")
            b.CreateCell(300).CreateText("FontName=Arial; FontSize=8; FontBold=1").Assign(rs("BYEAR").Value & "" & rs("BPERIO").Value)
            b.CreateCell(1600).CreateText("Alignment=Left;VertAlignment=Top").Assign(" ")
            b.CreateCell(400).CreateImage("Alignment=Right;VertAlignment=Top").Assign(My.Settings.CarpetaGneral + "\FEDURO.png")

            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.ChildrenStyle = "BorderStyle=rect"
            b.Height = 50
            b.CreateCell(100, "BorderStyle=rect;FontSize=10;Alignment=Left").CreateText("BorderRightMargin=10").Assign("COMPROBANTE")
            b.CreateCell(300, "BorderStyle=rect;FontSize=10;Alignment=Left").CreateText("BorderRightMargin=10").Assign("CUENTA")
            b.CreateCell(900, "BorderStyle=rect;FontSize=10;Alignment=Left").CreateText("BorderRightMargin=10").Assign("DESCRIPCION")
            b.CreateCell(625, "BorderStyle=rect;FontSize=10;Alignment=Right").CreateText("BorderRightMargin=10").Assign("DEBITO")
            b.CreateCell(625, "BorderStyle=rect;FontSize=10;Alignment=Right").CreateText("BorderRightMargin=10").Assign("CREDITO")

            b.Put()


        End With




    End Sub

    Public Sub wrtLogHtml(sql As String, ByVal Descrip As String)
        Dim fEntrada As String = ""
        Dim linea As String = Format(Now(), "yyyy-MM-dd HH:mm:ss") & "|"
        Dim strStreamW As Stream = Nothing
        Dim sApp As String = My.Application.Info.ProductName & "." & _
                                   My.Application.Info.Version.Major & "." & _
                                   My.Application.Info.Version.Minor & "." & _
                                   My.Application.Info.Version.Revision

        Descrip = Descrip.Replace(vbCrLf, vbCr)
        Descrip = Descrip.Replace(vbLf, vbCr)
        Descrip = Descrip.Replace(vbCr, "<BR/>")

        If gsKeyWords.Trim <> "" Then
            Descrip = gsKeyWords & "<br>" & Descrip
        End If

        linea &= System.Net.Dns.GetHostName() & "|" & sApp & "|" & Descrip.Replace("|", "?") & "|" & sql.Replace("|", "?")

        fEntrada = Replace(My.Settings.CarpetaGneral & "\log" & Format(Now(), "yyyyMM") & ".csv", "\\", "\")

        If File.Exists(fEntrada) Then
            strStreamW = File.Open(fEntrada, FileMode.Open) 'Abrimos el archivo
        Else
            strStreamW = File.Create(fEntrada) ' lo creamos
        End If
        strStreamW.Close()
        Try
            ' Create an instance of StreamReader to read from a file.
            Dim sr As StreamReader = New StreamReader(fEntrada)
            Dim sb As New StringBuilder()
            sb.Append(sr.ReadToEnd())
            sb.Append(linea)
            sr.Close()

            Dim sw As StreamWriter = New StreamWriter(fEntrada)
            sw.WriteLine(sb.ToString())
            sw.Close()

        Catch E As Exception

        End Try
        Application.Exit()
    End Sub



    Private Sub Diario_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        Try
            NumPaginas = 1
            flag = 0
            Dim aParam() As String

            BatchApp = True
            aParam = Split(Command(), " ")
            If UBound(aParam) <> 1 Then
                ' End
            End If

            CARPETA_SITIO = Trim(aParam(0))
            Control = Trim(aParam(1))
            Consecutivo = Trim(aParam(2))
            Referencia = Trim(aParam(3))
            FechaImpresion = Trim(aParam(4))
            key = Trim(aParam(5))


            bDateToDate = False

            'CARPETA_SITIO = LeeParametrosEsp(APP_NAME, "CARPETA_SITIO")
            bPRUEBAS = (LeeParametrosEsp(APP_NAME, "PRUEBAS") = "1") Or (LeeParametrosEsp(APP_NAME, "PRUEBAS") = "SI")
            LeeParametrosWeb(CARPETA_SITIO & "\Web.Config")
            PDF_Folder = CARPETA_IMG & "\pdf\Libros\"
            LOG_File = PDF_Folder & "\LD" & Control & ".log"
            bDateToDate = True
            If Not validaAcceso(gsUSER, gsPASS) Then
                End
            End If
            Balancee()
            Application.Exit()

        Catch ex As Exception
            wrtLogHtml("", ex.Message.ToString)
        End Try
    End Sub
End Class
