﻿Imports PDF_In_The_BoxCtl
Imports System
Imports System.IO
Imports System.Text

Public Class MayoryBalances

    Dim flag As Integer
    Dim rsTit As New ADODB.Recordset
    Dim rsCab As New ADODB.Recordset
    Dim rsPed As New ADODB.Recordset
    Dim rsDet As New ADODB.Recordset
    Dim rsNotas As New ADODB.Recordset
    Dim DocScanner As Integer, Observ As String, ImprimeIAL As String
    Dim bColor As Boolean
    Dim FechaImpresion
    Dim Referencia As String
    Dim bQuimicos As Boolean
    Dim Sellos() As String
    Dim Fila As Integer
    Dim Ta As TBoxTable
    Const CELDA_NORMAL As String = "BrushColor = 255, 255, 255"
    'Const CELDA_SOMBRA As String = "BrushColor = 215, 215, 215"
    Const CELDA_SOMBRA As String = "BrushColor = 255, 255, 255"
    Const TABLE_HEADER_STYLE As String = "ChildrenStyle=""BorderStyle=rect;FontSize=9;Fontname=Arial;FontBold=1;Alignment=Center;VertAlignment=Center"""
    Const ESTILO_TIT As String = "Normal;fontsize=6;Fontname=Corbel;BorderColor=Black;Alignment=Center;VertAlignment=Center"



    Private Sub Balance_BeforePutBand(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_BeforePutBandEvent) Handles Balance.BeforePutBand
        If e.aBand.Role = TBandRole.rlDetail Then
            If bColor And flag = 0 Then

                e.aBand.BrushColor = RGB(215, 215, 215)
            Else
                e.aBand.BrushColor = RGB(255, 255, 255)
            End If
            bColor = Not bColor
        End If
        Fila = Fila + 1
        Debug.Print(Balance.PenY)
        wrtLog(Balance.PenY, "111")
        If Fila >= 35 Then
            Balance.NewPage()
            Fila = 0
        End If
Manejo_Error:
        If Err.Number <> 0 Then
            WrtTxtError(LOG_File, "BeforePutBand")
            End
        End If

    End Sub

    Private Sub Balance_Enter(sender As System.Object, e As System.EventArgs) Handles Balance.Enter

    End Sub

    Private Sub Balance_OnBottomOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnBottomOfPageEvent) Handles Balance.OnBottomOfPage

        Dim b As TBoxBand
        Dim Sql As String
        Const NUM_FORMAT As String = "#,##0.00"
        Dim rs3 As New ADODB.Recordset
        Dim s As String
        Dim ArchivoPDF As String


        With Balance

            If e.lastPage Then
                If Fila < 45 Then
                    'b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
                    'b.ChildrenStyle = "BorderStyle=None;Alignment=Center"
                    'b.Height = 50
                    'b.CreateCell(1040).CreateText().Assign("")
                    'b.CreateCell(480).CreateText().Assign("Fin del Informe")
                    ''               b.CreateCell(480).CreateImage("Alignment=Right;VertAlignment=Top").Assign App.Path + "\FinInform.png"
                    'b.CreateCell(1030).CreateText().Assign("")
                    'b.Put()
                End If

                If Balance.PageCount Mod 2 = 0 Then
                    wrtLog("Fin", "111")
                    FinDocumento()
                End If

            End If
            wrtLog("Pagina; " & NumPagina, "111")
            NumPagina = NumPagina + 1
        End With

    End Sub



    Private Sub Balance_OnTopOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnTopOfPageEvent) Handles Balance.OnTopOfPage
        Dim b2 As TBoxBand
        Dim b3 As TBoxBand
        Dim b4 As TBoxBand
        Dim b5 As TBoxBand
        Dim b6 As TBoxBand
        Dim img As TBoxImage
        Dim Ta As TBoxTable
        With Balance
            .DefineStyle("Normal", "0; Margins=130,200,100,150; fontsize=10;Fontname=Arial;BorderColor=Black")
            .Style = "Normal"
            Dim b As TBoxBand

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=12;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(1200).CreateText("Alignment=Left;FontSize=12").Assign("LIBRO MAYOR Y BALANCES")
            b.CreateCell(1000).CreateText("Alignment=Right;FontSize=8;FontColor=Red").Assign(" " + Referencia.ToString)
            b.CreateCell(100).CreateText("Alignment=Left;FontSize=8;FontColor=Blue").Assign(" " + Consecutivo + Balance.PageCount)
            b.CreateCell(200).CreateText("Alignment=Right;FontSize=8").Assign("" + FechaImpresion.ToString)

            b.Put()

            Dim rs As New ADODB.Recordset
            Dim s As String
            Dim ArchivoPDF As String

            fSql = " SELECT "
            fSql = fSql & " DIGITS(BYEAR) BYEAR,   "
            fSql = fSql & " DIGITS(BPERIO) BPERIO  "
            fSql = fSql & " FROM BSSFFNBAL "
            fSql = fSql & " WHERE "
            fSql = fSql & " BCONTROL = '" & Control & "'"
            rs.Open(fSql, DB, 2, 2)
            If rs.EOF Then
                WrtTxtError(LOG_File, "Libro sin datos")
                Exit Sub
            End If

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=8;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(200).CreateText("Alignment=Left;FontSize=8").Assign("PERIODO")
            b.CreateCell(300).CreateText("FontName=Arial; FontSize=8; FontBold=1").Assign(rs("BYEAR").Value & "" & rs("BPERIO").Value)
            b.CreateCell(1600).CreateText("Alignment=Left;VertAlignment=Top").Assign(" ")
            b.CreateCell(400).CreateImage("Alignment=Right;VertAlignment=Top").Assign(My.Settings.CarpetaGneralJ + "\FEDURO.png")

            b.Put()

            '        Set b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            '        b.ChildrenStyle = "BorderStyle=None;Alignment=Center"
            '        b.Height = 100
            '        b.CreateCell(100, "BorderStyle=None;FontSize=10;Alignment=Right").CreateText("BorderRightMargin=10").Assign " "
            '        b.Put

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.ChildrenStyle = "BorderStyle=rect"
            b.Height = 50
            b.CreateCell(200, "BorderStyle=rect;FontSize=8;Alignment=Left").CreateText("BorderRightMargin=10").Assign("CUENTA")
            b.CreateCell(700, "BorderStyle=rect;FontSize=8;Alignment=Left").CreateText("BorderRightMargin=10").Assign("DESCRIPCION")
            b.CreateCell(400, "BorderStyle=rect;FontSize=8;Alignment=Right").CreateText("BorderRightMargin=10").Assign("INICIAL")
            b.CreateCell(400, "BorderStyle=rect;FontSize=8;Alignment=Right").CreateText("BorderRightMargin=10").Assign("DEBITO")
            b.CreateCell(400, "BorderStyle=rect;FontSize=8;Alignment=Right").CreateText("BorderRightMargin=10").Assign("CREDITO")
            b.CreateCell(400, "BorderStyle=rect;FontSize=8;Alignment=Right").CreateText("BorderRightMargin=10").Assign("FINAL")

            b.Put()



        End With

    End Sub

    Private Sub Diario_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        wrtLog("INGRESO 1.0 ", 111)
        Console.WriteLine("INGRESO 1.0")
        flag = 0
        NumPagina = 1
        'On Error GoTo Manejo_Error
        Dim aParam() As String

        BatchApp = True
        aParam = Split(Command(), " ")
        If UBound(aParam) <> 1 Then
            ' End
        End If

        CARPETA_SITIO = Trim(aParam(0))
        Control = Trim(aParam(1))
        Consecutivo = Trim(aParam(2))
        Referencia = Trim(aParam(3))
        FechaImpresion = Trim(aParam(4))
        key = Trim(aParam(5))

        wrtLog(CARPETA_SITIO, "111")
        wrtLog(Control, "111")
        wrtLog(Consecutivo, "111")
        wrtLog(FechaImpresion, "111")

        bDateToDate = False

        'CARPETA_SITIO = LeeParametrosEsp(APP_NAME, "CARPETA_SITIO")
        wrtLog(APP_NAME, "111")
        bPRUEBAS = (LeeParametrosEsp(APP_NAME, "PRUEBAS") = "1") Or (LeeParametrosEsp(APP_NAME, "PRUEBAS") = "SI")
        LeeParametrosWeb(CARPETA_SITIO & "\Web.Config")
        wrtLog(CARPETA_SITIO & "\Web.Config", "111")
        PDF_Folder = CARPETA_IMG & "\pdf\Libros\"
        wrtLog(CARPETA_IMG & "pdf\Libros\", "111")
        LOG_File = PDF_Folder & "\LD" & Control & ".log"
        wrtLog(LOG_File, "111")
        wrtLog("validaAcceso", "111")
        bDateToDate = True
        If Not validaAcceso(gsUSER, gsPASS) Then
            wrtLog("Valida Acceso ERR", "111")
            End
        End If
        wrtLog("BALANCE IR", "111")
        Balancee()
        Me.Close()
    End Sub

    Public Function wrtLog(ByVal txt As String, ByRef iNumLog As Integer) As String
        Dim fEntrada As String = ""
        Dim num As String = Date.Today.Month & Date.Today.Day & Date.Today.Year & Date.Today.Hour & Date.Today.Minute & Date.Today.Second


        Dim strStreamW As Stream = Nothing
        'Dim appPath As String = System.Web.HttpContext.Current.Request.ApplicationPath
        'Dim CarpetaWeb As String = HttpContext.Current.Request.MapPath(appPath)
        fEntrada = Replace(My.Settings.CarpetaGneralJ & "\wmslog" & num & ".txt", "\\", "\")

        If File.Exists(fEntrada) Then
            strStreamW = File.Open(fEntrada, FileMode.Open) 'Abrimos el archivo
        Else
            strStreamW = File.Create(fEntrada) ' lo creamos
        End If
        strStreamW.Close()
        Try
            ' Create an instance of StreamReader to read from a file.
            Dim sr As StreamReader = New StreamReader(fEntrada)
            Dim sb As New StringBuilder()

            sb.Append(sr.ReadToEnd())

            sb.AppendLine("SistemaDeBodega (" & My.Application.Info.Version.Major & "." & _
                   My.Application.Info.Version.Minor & "." & _
                   My.Application.Info.Version.Revision & ")")
            '  sb.AppendLine("[" & My.Settings.SERVER & "][" & My.Settings.USER & "][" & My.Settings.LIBRARY & "]")

            sb.AppendLine(txt)
            sr.Close()

            Dim sw As StreamWriter = New StreamWriter(fEntrada)
            sw.WriteLine(sb.ToString())
            sw.Close()

            Return "OK"
        Catch E As Exception
            Return E.Message
        End Try

    End Function


    Sub Balancee()

        wrtLog("BALANCE OK", "111")
        Dim Sql As String
        Dim rs As New ADODB.Recordset
        Dim s As String
        Dim ArchivoPDF As String


        fSql = " UPDATE BSSFTASK SET  "
        fSql = fSql & " TLXMSG = 'EJECUTANDO',  "
        fSql = fSql & " TMSG = 'Generando Libro Mayor y Balances'  "
        fSql = fSql & " WHERE TKEY = '" & key & "' "
        DB.Execute(fSql)

        fSql = " SELECT "
        fSql = fSql & " DIGITS(BYEAR) BYEAR,   "
        fSql = fSql & " DIGITS(BPERIO) BPERIO,  "
        fSql = fSql & " BCUENTA, "
        fSql = fSql & " BDESCTA, "
        fSql = fSql & " DOUBLE(BSALANT) BSALANT, "
        fSql = fSql & " DOUBLE(BDEBITO) BDEBITO, "
        fSql = fSql & " DOUBLE(BCREDITO) BCREDITO, "
        fSql = fSql & " DOUBLE(BSALFIN) BSALFIN"
        fSql = fSql & " FROM BSSFFNBAL "
        fSql = fSql & " WHERE "
        fSql = fSql & " BCONTROL = '" & Control & "'"
        fSql = fSql & " ORDER BY BCUENTA"
        rs.Open(fSql, DB, 2, 2)
        If rs.EOF Then
            WrtTxtError(LOG_File, "Factura no se encontro.")
            fSql = " UPDATE BSSFTASK SET  "
            fSql = fSql & " TLXMSG = 'EJECUTADA',  "
            fSql = fSql & " TMSG = 'Libro Mayor y Banances no Existe ',  "
            fSql = fSql & " STS2 = 1 "
            fSql = fSql & " WHERE TKEY = '" & key & "' "
            DB.Execute(fSql)
            Exit Sub
        End If





        ArchivoPDF = PDF_Folder & "MyB" & Control & ".pdf"
        wrtLog(ArchivoPDF, "111")
        With Balance
            .FileName = ArchivoPDF
            .Title = "Libro MyB - "
            .WantShow = bPRUEBAS
            .WantPageCount = True
            .PaperSizeName = "Letter"
            .Orientation = TPrinterOrientation.poLandscape
            .BeginDoc()
            wrtLog("BEGIN", "111")
            Const NUM_FORMAT As String = "#,##0.00"
            Dim b As TBoxBand
            Fila = 0

            wrtLog(ArchivoPDF, "111")
            Dim Ta As TBoxTable
            Ta = .CreateTable("BorderStyle=None")
            Ta.Assign(rs)

            Dim Detail As TBoxBand
            Detail = Ta.CreateBand()
            Detail.Role = TBandRole.rlDetail
            Detail.ChildrenStyle = "FontSize=8;Fontname=Arial; VertAlignment=Center"
            Detail.Height = 35
            Detail.ChildrenStyle = "FontSize=8;Fontname=Arial; VertAlignment=Center;BorderStyle=LeftRight"
            Detail.CreateCell(200, "BorderStyle=Left").CreateText("Alignment=Left").Bind("BCUENTA")
            Detail.CreateCell(700, "BorderStyle=none").CreateText("Alignment=Left").Bind("BDESCTA")

            With Detail.CreateCell(400, "BorderStyle=none").CreateText("Alignment=Right")
                .Bind("BSALANT")
                .Format = NUM_FORMAT
            End With
            With Detail.CreateCell(400, "BorderStyle=none").CreateText("Alignment=Right")
                .Bind("BDEBITO")
                .Format = NUM_FORMAT
            End With
            With Detail.CreateCell(400, "BorderStyle=none").CreateText("Alignment=Right")
                .Bind("BCREDITO")
                .Format = NUM_FORMAT
            End With
            With Detail.CreateCell(400, "BorderStyle=Right").CreateText("Alignment=Right")
                .Bind("BSALFIN")
                .Format = NUM_FORMAT

            End With
            Detail.Breakable = True
            Ta.Put()

            b = .CreateBand("BorderStyle=Top;Margins=130,200,100,180")
            b.ChildrenStyle = "BorderStyle=Top;Alignment=Center"
            b.Height = 100
            b.CreateCell(100, "BorderStyle=Top;FontSize=10;Alignment=Right").CreateText("BorderRightMargin=10").Assign(" ")
            b.Put()


        
            rs.Close()
            Try
                .EndDoc()
            Catch ex As Exception
                wrtLog("Execpion", "111")
                wrtLog(fSql, "111")
                wrtLog(ex.Message.ToString, "111")
                wrtLog("Execpion:", "111")
                wrtLog(ex.InnerException.ToString, "111")
            End Try

            fSql = " UPDATE BSSFTASK SET  "
            fSql = fSql & " TLXMSG = 'EJECUTADA',  "
            fSql = fSql & " TMSG = 'Libro Mayor y Banances Generado No Paginas: " & NumPagina & " ',  "
            fSql = fSql & " TURL = '" & Replace(ArchivoPDF, "C:\Feduro", "") & "',  "
            fSql = fSql & " STS2 = 1 "
            fSql = fSql & " WHERE TKEY = '" & key & "' "
            DB.Execute(fSql)
        End With

    End Sub



    Function FinDocumento()
        Dim b As TBoxBand
        With Me.Balance
            Fila = 0
            Balance.NewPage()
            wrtLog("fwwin222", "111")
            'b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            'b.ChildrenStyle = "BorderStyle=rect;Alignment=Center"
            'b.CreateCell(1010).CreateText().Assign("")
            'b.CreateCell(480).CreateImage("Alignment=Right;VertAlignment=Top").Assign(My.Settings.CarpetaGneralJ + "\FinInform.png")
            'b.CreateCell(1010).CreateText().Assign("")
            'b.Put()

            wrtLog("fin222", "111")
        End With
    End Function



End Class
