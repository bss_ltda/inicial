﻿Module BSSGetLogin

  

    Function GetUserLogin(ByVal qUsr As String, ByVal qLib As String) As String

        GetUserLogin = GetSetting("BSSAppData", "Settings", "UserLogin." & qLib, UCase(Trim(qUsr)))

    End Function

    Function PutUserLogin(ByVal qUsr As String, ByVal qLib As String) As String

        SaveSetting("BSSAppData", "Settings", "UserLogin." & qLib, UCase(Trim(qUsr)))

    End Function


End Module
