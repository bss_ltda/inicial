﻿Imports PDF_In_The_BoxCtl

'Imports Scripting
Imports System.IO
Public Class InvBala
    Dim flag As Integer
    Dim rsTit As New ADODB.Recordset
    Dim rsCab As New ADODB.Recordset
    Dim rsPed As New ADODB.Recordset
    Dim rsDet As New ADODB.Recordset
    Dim rsNotas As New ADODB.Recordset
    Dim DocScanner As Integer, Observ As String, ImprimeIAL As String
    Dim bColor As Boolean
    Dim bQuimicos As Boolean
    Dim Sellos() As String
    Dim Fila As Integer
    Dim Referencia
    Dim FechaImpresion
    Dim Ta As TBoxTable
    Const CELDA_NORMAL As String = "BrushColor = 255, 255, 255"
    'Const CELDA_SOMBRA As String = "BrushColor = 215, 215, 215"
    Const CELDA_SOMBRA As String = "BrushColor = 255, 255, 255"
    Const TABLE_HEADER_STYLE As String = "ChildrenStyle=""BorderStyle=rect;FontSize=9;Fontname=Arial;FontBold=1;Alignment=Center;VertAlignment=Center"""
    Const ESTILO_TIT As String = "Normal;fontsize=6;Fontname=Corbel;BorderColor=Black;Alignment=Center;VertAlignment=Center"

    Private Sub AxPdfBox1_BeforePutBand(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_BeforePutBandEvent) Handles Balance.BeforePutBand
        If e.aBand.Role = TBandRole.rlDetail Then
            If bColor And flag = 0 Then

                e.aBand.BrushColor = RGB(215, 215, 215)
            Else
                e.aBand.BrushColor = RGB(255, 255, 255)
            End If
            bColor = Not bColor
        End If
        Fila = Fila + 1
        Debug.Print(Balance.PenY)
        If Fila >= 30 Then
            Balance.NewPage()
            Fila = 0
        End If
Manejo_Error:
        If Err.Number <> 0 Then
            WrtTxtError(LOG_File, "BeforePutBand")
            End
        End If

    End Sub

    Private Sub AxPdfBox1_Enter(sender As System.Object, e As System.EventArgs) Handles Balance.Enter

    End Sub

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        flag = 0
        'On Error GoTo Manejo_Error
        Dim aParam() As String

        BatchApp = True
        aParam = Split(Command(), " ")
        If UBound(aParam) <> 1 Then
            ' End
        End If

            CARPETA_SITIO = Trim(aParam(0))
        Control = Trim(aParam(1))


        bDateToDate = False

        'CARPETA_SITIO = LeeParametrosEsp(APP_NAME, "CARPETA_SITIO")
        bPRUEBAS = (LeeParametrosEsp(APP_NAME, "PRUEBAS") = "1") Or (LeeParametrosEsp(APP_NAME, "PRUEBAS") = "SI")
        LeeParametrosWeb(CARPETA_SITIO & "\Web.Config")
        PDF_Folder = CARPETA_IMG & "\pdf\Libros\"
        LOG_File = PDF_Folder & "\LD" & Control & ".log"
        WrtTxtError(LOG_File, Control)

        bDateToDate = True
        If Not validaAcceso(gsUSER, gsPASS) Then
            End
        End If

        Balancee()

        Me.Close()
    End Sub

    Private Sub Balance_OnBottomOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnBottomOfPageEvent) Handles Balance.OnBottomOfPage
        Dim b As TBoxBand
        With Balance

            b = .CreateBand("Normal; BorderStyle=bottom;Margins=130,200,100,150")
            b.Breakable = False
            b.CreateCell(2500, "BorderStyle=None").CreateText("Alignment=left").Assign("")
            b.Put()
            If e.lastPage Then
                If Fila < 30 Then

                    b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
                    b.ChildrenStyle = "BorderStyle=rect;Alignment=Center"
                    b.CreateCell(1010).CreateText().Assign("")
                    b.CreateCell(480).CreateImage("Alignment=Right;VertAlignment=Top").Assign(My.Settings.CarpetaGneral + "\Fin del Informe.png")
                    b.CreateCell(1010).CreateText().Assign("")
                    b.Put()


                End If

                If Balance.PageCount Mod 2 = 0 Then
                    FinDocumento()
                End If

            End If
        End With

    End Sub

    Private Sub Balance_OnTopOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnTopOfPageEvent) Handles Balance.OnTopOfPage
        Dim b As TBoxBand
        Dim b3 As TBoxBand
        Dim b4 As TBoxBand
        Dim b5 As TBoxBand
        Dim b6 As TBoxBand
        Dim img As TBoxImage
        Dim Ta As TBoxTable
        With Balance
            .DefineStyle("Normal", "0; Margins=130,200,100,150; fontsize=10;Fontname=Arial;BorderColor=Black")
            .Style = "Normal"
            img = .CreateImage
            img.Assign(My.Settings.CarpetaGneral + "\BRINSA-IAL.png", 0)
            Dim FechaMas3 As String
            img.Put(1600, 100, 2000, 250)


            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.Height = 75
            b.CreateCell(910).CreateText("Alignment=Left;FontSize=9").Assign(" ")
            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=16;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(1800).CreateText("Alignment=left;FontSize=16").Assign("COMPROBANTE DE INFORME DIARIO")
            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.Height = 75
            b.CreateCell(910).CreateText("Alignment=Left;FontSize=9").Assign(" ")
            b.Put()


            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.Height = 75
            b.CreateCell(910).CreateText("Alignment=Left;FontSize=9").Assign(" ")
            b.Put()


            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=16;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(200).CreateText("Alignment=left;FontSize=10").Assign("Compañia:")
            b.CreateCell(500).CreateText("Alignment=left;FontSize=10").Assign("BRINSA S.A")

            b.Put()


            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=16;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(200).CreateText("Alignment=left;FontSize=10").Assign("NIT:")
            b.CreateCell(500).CreateText("Alignment=left;FontSize=10").Assign("A8000221789-2")
            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=16;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(200).CreateText("Alignment=left;FontSize=10").Assign("Servidor:")
            b.CreateCell(500).CreateText("Alignment=left;FontSize=10").Assign("AS/400")

            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=16;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(200).CreateText("Alignment=left;FontSize=10").Assign("Direccion:")
            b.CreateCell(500).CreateText("Alignment=left;FontSize=10").Assign("KM 6 VIA CAJICA- ZIPAQUIRA")

            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=16;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(200).CreateText("Alignment=left;FontSize=10").Assign("Telefono:")
            b.CreateCell(500).CreateText("Alignment=left;FontSize=10").Assign("(57) 1 4846000")

            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=16;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(500).CreateText("Alignment=left;FontSize=10").Assign("")
            b.Height = 70
            b.Put()

        End With
    End Sub
    Sub Balancee()


        Dim sql As String
        Dim rs As New ADODB.Recordset
        Dim s As String
        Dim ArchivoPDF As String
        Dim rsDet As New ADODB.Recordset
        Dim Ta As TBoxTable
        Dim Ta1 As TBoxTable
        Dim Ta2 As TBoxTable
        Dim Ta3 As TBoxTable
        Dim Ta4 As TBoxTable
        Dim Ta6 As TBoxTable
        Dim Ta7 As TBoxTable
        Dim Ta8 As TBoxTable
        Dim Ta9 As TBoxTable
        Dim Ta11 As TBoxTable

        ArchivoPDF = PDF_Folder & "INFDIA" & Control & ".pdf"

        With Balance
            .FileName = ArchivoPDF
            .Title = "Comprobate de Informativo Diario - "
            .WantShow = bPRUEBAS
            .WantPageCount = True
            .PaperSizeName = "Letter"
            .Orientation = TPrinterOrientation.poLandscape
            .BeginDoc()
            Const NUM_FORMAT As String = "#,##0.00"
            Dim b As TBoxBand
            Fila = 0

            sql = ""
            sql = sql & "SELECT VPREFI "
            sql = sql & "FROM BSSFFNVEN "
            sql = sql & "WHERE "
            sql = sql & " VCONTROL = " & Control
            rsPed.Open(sql, DB)
            Do While Not rsPed.EOF
                bColor = False
                sql = ""
                sql = sql & "SELECT VPREFI, "
                sql = sql & "VFACIN,  "
                sql = sql & "VFECHA,  "
                sql = sql & "VFACFI,  "
                sql = sql & "DOUBLE (VNUMTRA) VNUMTRA,  "
                sql = sql & "DOUBLE (VVENGRA) VVENGRA,  "
                sql = sql & "DOUBLE (VVENEXC) VVENEXC,  "
                sql = sql & "DOUBLE (VVENEXE) VVENEXE,  "
                sql = sql & "DOUBLE (VIVA) VIVA,  "
                sql = sql & "DOUBLE (VVENGRAD) VVENGRAD, "
                sql = sql & "DOUBLE (VVENEXCD) VVENEXCD,  "
                sql = sql & "DOUBLE (VVENEXED) VVENEXED,  "
                sql = sql & "VCRTDAT  "
                sql = sql & "FROM BSSFFNVEN "
                sql = sql & "WHERE "
                sql = sql & " VCONTROL = " & Control
                sql = sql & " AND VPREFI= '" & rsPed("VPREFI").Value & "'"
                rsDet.Open(sql, DB, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)



                Ta = .CreateTable("BorderStyle=None")
                Ta.Assign(rsDet)

                Ta1 = .CreateTable("BorderStyle=None")
                Ta1.Assign(rsDet)

                Ta2 = .CreateTable("BorderStyle=None")
                Ta2.Assign(rsDet)

                Ta3 = .CreateTable("BorderStyle=None")
                Ta3.Assign(rsDet)

                Ta4 = .CreateTable("BorderStyle=None")
                Ta4.Assign(rsDet)

                Ta6 = .CreateTable("BorderStyle=None")
                Ta6.Assign(rsDet)

                Ta7 = .CreateTable("BorderStyle=None")
                Ta7.Assign(rsDet)

                Ta8 = .CreateTable("BorderStyle=None")
                Ta8.Assign(rsDet)

                Ta9 = .CreateTable("BorderStyle=None")
                Ta9.Assign(rsDet)

                Ta11 = .CreateTable("BorderStyle=None")
                Ta11.Assign(rsDet)
                Dim Detail As TBoxBand

                Detail = Ta.CreateBand()
                Detail.Role = TBandRole.rlBodyFooter
                Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center"
                Detail.Height = 45
                Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center;BorderStyle=LeftRight"
                Detail.CreateCell(800, "BorderStyle=none").CreateText("Alignment=Left").Assign("Prefijo                " & rsDet("VPREFI").Value)


                Detail = Ta1.CreateBand()
                Detail.Role = TBandRole.rlBodyFooter
                Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center"
                Detail.Height = 45
                Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center;BorderStyle=LeftRight"
                Detail.CreateCell(1200, "BorderStyle=none").CreateText("Alignment=Left").Assign("Fecha Comprobante                   " & rsDet("VFECHA").Value)

                ArchivoPDF = PDF_Folder & "INFDIA" & Control & ".pdf"

                With Balance
                    .FileName = ArchivoPDF
                    .Title = "Comprobate de Informativo Diario - "
                    .WantShow = bPRUEBAS
                    .WantPageCount = True
                    .PaperSizeName = "Letter"
                    .Orientation = TPrinterOrientation.poLandscape
                    .BeginDoc()
                    Fila = 0

                    sql = ""
                    sql = sql & "SELECT VPREFI "
                    sql = sql & "FROM BSSFFNVEN "
                    sql = sql & "WHERE "
                    sql = sql & " VCONTROL = " & Control
                    rsPed.Open(sql, DB)
                    Do While Not rsPed.EOF
                        bColor = False
                        sql = ""
                        sql = sql & "SELECT VPREFI, "
                        sql = sql & "VFACIN,  "
                        sql = sql & "VFECHA,  "
                        sql = sql & "VFACFI,  "
                        sql = sql & "DOUBLE (VNUMTRA) VNUMTRA,  "
                        sql = sql & "DOUBLE (VVENGRA) VVENGRA,  "
                        sql = sql & "DOUBLE (VVENEXC) VVENEXC,  "
                        sql = sql & "DOUBLE (VVENEXE) VVENEXE,  "
                        sql = sql & "DOUBLE (VIVA) VIVA,  "
                        sql = sql & "DOUBLE (VVENGRAD) VVENGRAD, "
                        sql = sql & "DOUBLE (VVENEXCD) VVENEXCD,  "
                        sql = sql & "DOUBLE (VVENEXED) VVENEXED,  "
                        sql = sql & "VCRTDAT  "
                        sql = sql & "FROM BSSFFNVEN "
                        sql = sql & "WHERE "
                        sql = sql & " VCONTROL = " & Control
                        sql = sql & " AND VPREFI= '" & rsPed("VPREFI").Value & "'"
                        rsDet.Open(sql, DB, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockReadOnly)
                        Ta = .CreateTable("BorderStyle=None")
                        Ta.Assign(rsDet)

                        Ta1 = .CreateTable("BorderStyle=None")
                        Ta1.Assign(rsDet)

                        Ta2 = .CreateTable("BorderStyle=None")
                        Ta2.Assign(rsDet)

                        Ta3 = .CreateTable("BorderStyle=None")
                        Ta3.Assign(rsDet)

                        Ta4 = .CreateTable("BorderStyle=None")
                        Ta4.Assign(rsDet)

                        Ta6 = .CreateTable("BorderStyle=None")
                        Ta6.Assign(rsDet)

                        Ta7 = .CreateTable("BorderStyle=None")
                        Ta7.Assign(rsDet)

                        Ta8 = .CreateTable("BorderStyle=None")
                        Ta8.Assign(rsDet)

                        Ta9 = .CreateTable("BorderStyle=None")
                        Ta9.Assign(rsDet)

                        Ta11 = .CreateTable("BorderStyle=None")
                        Ta11.Assign(rsDet)

                        Detail = Ta.CreateBand()
                        Detail.Role = TBandRole.rlBodyFooter
                        Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center"
                        Detail.Height = 45
                        Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center;BorderStyle=LeftRight"
                        Detail.CreateCell(800, "BorderStyle=none").CreateText("Alignment=Left").Assign("Prefijo                " & rsDet("VPREFI").Value)


                        Detail = Ta1.CreateBand()
                        Detail.Role = TBandRole.rlBodyFooter
                        Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center"
                        Detail.Height = 45
                        Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center;BorderStyle=LeftRight"
                        Detail.CreateCell(1200, "BorderStyle=none").CreateText("Alignment=Left").Assign("Fecha Comprobante                   " & rsDet("VFECHA").Value & " A " & rsDet("VFECHA").Value)


                        Detail = Ta2.CreateBand()
                        Detail.Role = TBandRole.rlBodyFooter
                        Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center"
                        Detail.Height = 45
                        Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center;BorderStyle=LeftRight"
                        Detail.CreateCell(800, "BorderStyle=none").CreateText("Alignment=Left").Assign("Facturas del                                " & rsDet("VFACIN").Value & " Al " & rsDet("VFACFI").Value)
                        Detail = Ta3.CreateBand()

                        Detail.Role = TBandRole.rlBodyFooter
                        Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center"
                        Detail.Height = 45
                        Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center;BorderStyle=LeftRight"
                        Detail.CreateCell(800, "BorderStyle=none").CreateText("Alignment=Left").Assign("Numero de Transacciones         " & rsDet("VNUMTRA").Value)

                        Detail = Ta4.CreateBand()

                        Detail.Role = TBandRole.rlBodyFooter
                        Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center"
                        Detail.Height = 45
                        Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center;BorderStyle=LeftRight"
                        Detail.CreateCell(800, "BorderStyle=none").CreateText("Alignment=Left").Assign("Medio de pago:                           Ventas a Credito")


                        Detail = Ta6.CreateBand()

                        Detail.Role = TBandRole.rlDetail
                        Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center"
                        Detail.Height = 45
                        Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center;BorderStyle=none"
                        Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Left").Assign("Concepto")
                        Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Right").Assign("Valor Ventas")
                        Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Right").Assign("Valor IVA")


                        Detail = Ta7.CreateBand()
                        Detail.Role = TBandRole.rlDetail
                        Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center"
                        Detail.Height = 45
                        Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center;BorderStyle=none"
                        Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Left").Assign("Ventas Exentas")
                        With Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Right")
                            .Bind("VVENEXE")
                            .Format = NUM_FORMAT
                        End With
                        Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Right").Assign(".00")

                        Detail = Ta8.CreateBand()
                        Detail.Role = TBandRole.rlDetail
                        Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center"
                        Detail.Height = 45
                        Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center;BorderStyle=none"
                        Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Left").Assign("Ventas Excluidas")
                        With Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Right")
                            .Bind("VVENEXC")
                            .Format = NUM_FORMAT
                        End With
                        Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Right").Assign(".00")
                        Detail = Ta9.CreateBand()
                        Detail.Role = TBandRole.rlDetail
                        Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center"
                        Detail.Height = 45
                        Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center;BorderStyle=none"
                        Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Left").Assign("Ventas Gravasdad del 16.00")
                        With Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Right")
                            .Bind("VVENGRA")
                            .Format = NUM_FORMAT
                        End With
                        With Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Right")
                            .Bind("VIVA")
                            .Format = NUM_FORMAT
                        End With

                        Detail = Ta11.CreateBand()
                        Detail.Role = TBandRole.rlDetail
                        Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center"
                        Detail.Height = 45
                        Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center;BorderStyle=none"
                        Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Left").Assign("Totales")
                        With Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Right")
                            .Assign(rsDet("VVENGRA").Value + rsDet("VVENEXC").Value + rsDet("VVENEXE").Value)
                            .Format = NUM_FORMAT
                        End With
                        With Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Right")
                            .Bind("VIVA")
                            .Format = NUM_FORMAT
                        End With

                        Ta.Put()
                        b = .CreateBand("Normal;Margins=130,200,100,150")
                        b.Height = 20
                        b.ChildrenStyle = "Normal;fontsize=4"
                        b.CreateCell(1910, "BorderStyle=none").CreateText("Alignment=LEFT").Assign("")
                        b.Put()
                        Ta1.Put()
                        Ta2.Put()
                        Ta3.Put()
                        Ta4.Put()
                        b = .CreateBand("Normal;Margins=130,200,100,150")
                        b.Height = 20
                        b.ChildrenStyle = "Normal;fontsize=4"
                        b.CreateCell(1910, "BorderStyle=none").CreateText("Alignment=LEFT").Assign("")
                        b.Put()
                        Ta6.Put()
                        Ta7.Put()
                        Ta8.Put()
                        Ta9.Put()
                        Ta11.Put()
                        b = .CreateBand("Normal;Margins=130,200,100,150")
                        b.Height = 20
                        b.ChildrenStyle = "Normal;fontsize=4"
                        b.CreateCell(1910, "BorderStyle=none").CreateText("Alignment=LEFT").Assign("")
                        b.Put()
                        rsDet.Close()
                        rsPed.MoveNext()
                        If Not (rsPed.EOF) Then
                            Fila = 0
                            Balance.NewPage()
                        End If
                    Loop

                    ArchivoPDF = .FileName
                    .EndDoc()
                End With
                rsPed.Close()
                rsDet = Nothing
                rsPed = Nothing


                Detail = Ta2.CreateBand()
                Detail.Role = TBandRole.rlBodyFooter
                Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center"
                Detail.Height = 45
                Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center;BorderStyle=LeftRight"
                Detail.CreateCell(800, "BorderStyle=none").CreateText("Alignment=Left").Assign("Facturas del                                " & rsDet("VFACIN").Value & " Al " & rsDet("VFACFI").Value)
                Detail = Ta3.CreateBand()

                Detail.Role = TBandRole.rlBodyFooter
                Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center"
                Detail.Height = 45
                Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center;BorderStyle=LeftRight"
                Detail.CreateCell(800, "BorderStyle=none").CreateText("Alignment=Left").Assign("Numero de Transacciones         " & rsDet("VNUMTRA").Value)

                Detail = Ta4.CreateBand()

                Detail.Role = TBandRole.rlBodyFooter
                Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center"
                Detail.Height = 45
                Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center;BorderStyle=LeftRight"
                Detail.CreateCell(800, "BorderStyle=none").CreateText("Alignment=Left").Assign("Medio de pago:                           Ventas a Credito")


                Detail = Ta6.CreateBand()

                Detail.Role = TBandRole.rlDetail
                Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center"
                Detail.Height = 45
                Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center;BorderStyle=none"
                Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Left").Assign("Concepto")
                Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Right").Assign("Valor Ventas")
                Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Right").Assign("Valor IVA")


                Detail = Ta7.CreateBand()
                Detail.Role = TBandRole.rlDetail
                Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center"
                Detail.Height = 45
                Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center;BorderStyle=none"
                Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Left").Assign("Ventas Exentas")
                With Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Right")
                    .Bind("VVENEXE")
                    .Format = NUM_FORMAT
                End With
                Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Right").Assign(".00")

                Detail = Ta8.CreateBand()
                Detail.Role = TBandRole.rlDetail
                Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center"
                Detail.Height = 45
                Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center;BorderStyle=none"
                Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Left").Assign("Ventas Excluidas")
                With Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Right")
                    .Bind("VVENEXC")
                    .Format = NUM_FORMAT
                End With
                Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Right").Assign(".00")
                Detail = Ta9.CreateBand()
                Detail.Role = TBandRole.rlDetail
                Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center"
                Detail.Height = 45
                Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center;BorderStyle=none"
                Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Left").Assign("Ventas Gravasdad del 16.00")
                With Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Right")
                    .Bind("VVENGRA")
                    .Format = NUM_FORMAT
                End With
                With Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Right")
                    .Bind("VIVA")
                    .Format = NUM_FORMAT
                End With

                Detail = Ta11.CreateBand()
                Detail.Role = TBandRole.rlDetail
                Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center"
                Detail.Height = 45
                Detail.ChildrenStyle = "FontSize=10;Fontname=Arial; VertAlignment=Center;BorderStyle=none"
                Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Left").Assign("Totales")
                With Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Right")
                    .Assign(rsDet("VVENGRA").Value + rsDet("VVENEXC").Value + rsDet("VVENEXE").Value)
                    .Format = NUM_FORMAT
                End With
                With Detail.CreateCell(500, "BorderStyle=none").CreateText("Alignment=Right")
                    .Bind("VIVA")
                    .Format = NUM_FORMAT
                End With

                Ta.Put()
                b = .CreateBand("Normal;Margins=130,200,100,150")
                b.Height = 20
                b.ChildrenStyle = "Normal;fontsize=4"
                b.CreateCell(1910, "BorderStyle=none").CreateText("Alignment=LEFT").Assign("")
                b.Put()
                Ta1.Put()
                Ta2.Put()
                Ta3.Put()
                Ta4.Put()
                b = .CreateBand("Normal;Margins=130,200,100,150")
                b.Height = 20
                b.ChildrenStyle = "Normal;fontsize=4"
                b.CreateCell(1910, "BorderStyle=none").CreateText("Alignment=LEFT").Assign("")
                b.Put()
                Ta6.Put()
                Ta7.Put()
                Ta8.Put()
                Ta9.Put()
                Ta11.Put()
                b = .CreateBand("Normal;Margins=130,200,100,150")
                b.Height = 20
                b.ChildrenStyle = "Normal;fontsize=4"
                b.CreateCell(1910, "BorderStyle=none").CreateText("Alignment=LEFT").Assign("")
                b.Put()
                rsDet.Close()
                rsPed.MoveNext()
                If Not (rsPed.EOF) Then
                    Fila = 0
                    Balance.NewPage()
                End If
            Loop

            ArchivoPDF = .FileName
            .EndDoc()
        End With
        rsPed.Close()
        rsDet = Nothing
        rsPed = Nothing


    End Sub



    Function FinDocumento()
        Dim b As TBoxBand
        With Me.Balance

            Balance.NewPage()
            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "BorderStyle=rect;Alignment=Center"
            b.CreateCell(1010).CreateText().Assign("")
            b.CreateCell(480).CreateImage("Alignment=Right;VertAlignment=Top").Assign(My.Settings.CarpetaGneral + "\Fin del Informe.png")
            b.CreateCell(1010).CreateText().Assign("")
            b.Put()

        End With
    End Function

End Class
