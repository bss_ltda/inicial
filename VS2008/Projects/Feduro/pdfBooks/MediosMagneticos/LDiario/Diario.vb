﻿Imports PDF_In_The_BoxCtl
Imports System
Imports System.IO
Imports System.Text
Public Class Diario
    Dim flag As Integer
    Dim rsTit As New ADODB.Recordset
    Dim rsCab As New ADODB.Recordset
    Dim rsPed As New ADODB.Recordset
    Dim rsDet As New ADODB.Recordset
    Dim rsNotas As New ADODB.Recordset
    Dim DocScanner As Integer, Observ As String, ImprimeIAL As String
    Dim bColor As Boolean
    Dim FechaImpresion
    Dim Referencia
    Dim bQuimicos As Boolean
    Dim Sellos() As String
    Dim Fila As Integer
    Dim Ta As TBoxTable
    Const CELDA_NORMAL As String = "BrushColor = 255, 255, 255"
    'Const CELDA_SOMBRA As String = "BrushColor = 215, 215, 215"
    Const CELDA_SOMBRA As String = "BrushColor = 255, 255, 255"
    Const TABLE_HEADER_STYLE As String = "ChildrenStyle=""BorderStyle=rect;FontSize=9;Fontname=Arial;FontBold=1;Alignment=Center;VertAlignment=Center"""
    Const ESTILO_TIT As String = "Normal;fontsize=6;Fontname=Corbel;BorderColor=Black;Alignment=Center;VertAlignment=Center"

    Public Function wrtLog(ByVal txt As String, ByRef iNumLog As Integer) As String
        Dim fEntrada As String = ""
        Dim num As String = Date.Today.Month & Date.Today.Day & Date.Today.Year & Date.Today.Hour & Date.Today.Minute & Date.Today.Second


        Dim strStreamW As Stream = Nothing
        'Dim appPath As String = System.Web.HttpContext.Current.Request.ApplicationPath
        'Dim CarpetaWeb As String = HttpContext.Current.Request.MapPath(appPath)
        fEntrada = Replace(My.Settings.CarpetaGneral & "\wmslog" & num & ".txt", "\\", "\")

        If File.Exists(fEntrada) Then
            strStreamW = File.Open(fEntrada, FileMode.Open) 'Abrimos el archivo
        Else
            strStreamW = File.Create(fEntrada) ' lo creamos
        End If
        strStreamW.Close()
        Try
            ' Create an instance of StreamReader to read from a file.
            Dim sr As StreamReader = New StreamReader(fEntrada)
            Dim sb As New StringBuilder()

            sb.Append(sr.ReadToEnd())

            sb.AppendLine("SistemaDeBodega (" & My.Application.Info.Version.Major & "." & _
                   My.Application.Info.Version.Minor & "." & _
                   My.Application.Info.Version.Revision & ")")
            '  sb.AppendLine("[" & My.Settings.SERVER & "][" & My.Settings.USER & "][" & My.Settings.LIBRARY & "]")

            sb.AppendLine(txt)
            sr.Close()

            Dim sw As StreamWriter = New StreamWriter(fEntrada)
            sw.WriteLine(sb.ToString())
            sw.Close()

            Return "OK"
        Catch E As Exception
            Return E.Message
        End Try

    End Function


    



    Function Balancee()

        fSql = " UPDATE BSSFTASK SET  "
        fSql = fSql & " TLXMSG = 'EJECUTANDO',  "
        fSql = fSql & " TMSG = 'Actualizando Medios Magneticos'  "
        fSql = fSql & " WHERE TKEY = '" & key & "' "
        DB.Execute(fSql)



        fSql = "UPDATE ZCC SET CCUDC1 = 1, CCNOT1 = 'En Ejecucion " & Date.Now & "' WHERE CCID = 'CC' AND CCTABL = 'RUTINAS' AND CCCODE = 'FNZMEDIOS'"
        DB.Execute(fSql)


        fSql = "CALL PGM(" & gsLIB & "/BSSCFNMM) "
        fSql = fSql & " PARM('" & Control & "')"
        DB.Execute("{{ " & fSql & " }}")

        fSql = "UPDATE ZCC SET CCUDC1 = 0, CCNOT1 = 'Ejecutado " & Date.Now & "' WHERE CCID = 'CC' AND CCTABL = 'RUTINAS' AND CCCODE = 'FNZMEDIOS'"
        DB.Execute(fSql)


        fSql = " UPDATE BSSFTASK SET  "
        fSql = fSql & " TLXMSG = 'EJECUTADO',  "
        fSql = fSql & " TMSG = 'Actualizacion Correcta Medios Magnetico'  "
        fSql = fSql & " WHERE TKEY = '" & key & "' "
        DB.Execute(fSql)



    End Function

    Private Sub Diario_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        wrtLog("INGRESO 1.0 ", 111)
        Console.WriteLine("INGRESO 1.0")
        flag = 0
        'On Error GoTo Manejo_Error
        Dim aParam() As String

        BatchApp = True
        aParam = Split(Command(), " ")
        If UBound(aParam) <> 1 Then
            ' End
        End If

        CARPETA_SITIO = Trim(aParam(0))
        Control = Trim(aParam(1))
        key = Trim(aParam(2))
    

        wrtLog(CARPETA_SITIO, "111")
        wrtLog(Control, "111")
        wrtLog(Consecutivo, "111")
        wrtLog(FechaImpresion, "111")

        bDateToDate = False

        'CARPETA_SITIO = LeeParametrosEsp(APP_NAME, "CARPETA_SITIO")
        wrtLog(APP_NAME, "111")
        bPRUEBAS = (LeeParametrosEsp(APP_NAME, "PRUEBAS") = "1") Or (LeeParametrosEsp(APP_NAME, "PRUEBAS") = "SI")
        LeeParametrosWeb(CARPETA_SITIO & "\Web.Config")
        wrtLog(CARPETA_SITIO & "\Web.Config", "111")
        PDF_Folder = CARPETA_IMG & "\pdf\Libros\"
        wrtLog(CARPETA_IMG & "\pdf\Libros\", "111")
        LOG_File = PDF_Folder & "\LD" & Control & ".log"
        wrtLog(LOG_File, "111")
        wrtLog("validaAcceso", "111")
        bDateToDate = True
        If Not validaAcceso(gsUSER, gsPASS) Then
            wrtLog("Valida Acceso ERR", "111")
            End
        End If
        wrtLog("BALANCE IR", "111")
        Balancee()
        Me.Close()
    End Sub
End Class
