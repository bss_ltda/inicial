﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Print.aspx.cs" Inherits="BSS.Brinsa.Reportero.Print" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=9.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Print</title>
</head>
<body>
    <form id="forma" runat="server">
    <asp:Label ID="uiMensaje" runat="server"></asp:Label>
    <rsweb:ReportViewer ID="visor" runat="server">
    </rsweb:ReportViewer>
    </form>
</body>
</html>
