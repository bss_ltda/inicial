﻿using System;
using IBM.Data.DB2.iSeries;

namespace BSS.Brinsa.Libreria.Dao
{
    /// <summary>
    /// Clase BSS.Brinsa.Libreria.Dao.DataDB2
    /// </summary>
    public class DataDB2
    {
        #region Miembros
        /// <summary>
        /// Objeto que contiene la base de datos
        /// </summary>
        private DB2Database bd;
        /// <summary>
        /// Objeto de comando
        /// </summary>
        private iDB2Command comando;
        /// <summary>
        /// Variable que almacena la consulta a la base de datos
        /// </summary>
        private String consulta;
        /// <summary>
        /// Numero de registros insertados
        /// </summary>
        private int registro;
        #endregion

        #region Propiedades
        /// <summary>
        /// Devuelve o establece la base de datos
        /// </summary>
        /// <value></value>
        protected DB2Database BD
        {
            get { return bd; }
            set { bd = value; }
        }

        /// <summary>
        /// Devuelve o establece el comando
        /// </summary>
        /// <value></value>
        protected iDB2Command Comando
        {
            get { return comando; }
            set { comando = value; }
        }

        /// <summary>
        /// Devuelve o establece el query
        /// </summary>
        protected string Query
        {
            get { return this.consulta; }
            set { this.consulta = value; }
        }

        /// <summary>
        /// Devuelve o establece el procedimiento
        /// </summary>
        protected string Procedimiento
        {
            get { return this.consulta; }
            set { this.consulta = value; }
        }

        /// <summary>
        /// Devuelve o establece el <see cref="registro"/>
        /// </summary>
        public int Registro
        {
            get { return registro; }
            set { registro = value; }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="BSS.Brinsa.Libreria.Dao.DataDB2"/>
        /// </summary>
        public DataDB2()
        {
            BD = new DB2Database(Utilidades.BaseDatos.ConexionDB2.ToString());
        }
        #endregion

        #region Metodos Protegidos
        /// <summary>
        /// inserta el query o los query de inserccion
        /// </summary>
        /// <param name="sql">The SQL.</param>
        /// <returns>System.Boolean</returns>
        protected bool Insertar(string sql)
        {
            Query = sql;
            Comando = BD.GetSqlStringCommand(Query);
            Comando.CommandTimeout = Utilidades.CommandTimeout;

            try
            {
                Registro = BD.ExecuteNonQuery(Comando);
                Log.For(this).Info(Query);
                return true;
            }
            catch (Exception ex)
            {
                Log.For(this).Error(string.Format("Insertando en AS400 ({0})", sql), ex);
                throw;
            }
        }

        /// <summary>
        /// existe dato
        /// </summary>
        /// <param name="query">el/la query</param>
        /// <returns>System.Boolean</returns>
        protected bool ExisteDato(string query)
        {
            Query = query;
            Comando = BD.GetSqlStringCommand(Query);
            Comando.CommandTimeout = Utilidades.CommandTimeout;

            try
            {
                Registro = (int)BD.ExecuteScalar(Comando);

                if (Registro != 0)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                Log.For(this).Error(string.Format("Validando si existe el dato en AS400 ({0})", Query), ex);
                throw;
            }
        }
        #endregion

        #region Metodos publicos
        #endregion

        #region Metodos privados
        #endregion

    }
}