﻿
Option Strict Off
Option Explicit On

Imports System
Imports System.IO


Module BSSCommon

    Public Const tNum As Integer = 1
    Public Const tStr As Integer = 2
    Public Const t_iF As Integer = 3
    Public Const t_iV As Integer = 4
    Public Const tFil As Integer = 5

    Public DB As New SQL_Exe
    Public Sql As New Make_Sql
    Public gsConnstring As String
    Public sLIB As String
    Public sVersion As String

    Public Enum ChkVerApp
        AppOK = 1
        AppVersionIncorrect = 2
        AppSeparadorDecimal = 3
        AppDeshabilitada = 4
        AppEjecutando = 5
    End Enum

    Function CodeCB(ByRef valor As String, Optional ByRef sep As String = "") As String
        Dim aCode As Object

        aCode = Split(valor, IIf(sep = "", " ", sep))
        CodeCB = aCode(0)

    End Function

    Function DescCB(ByRef valor As String, Optional ByRef sep As String = "") As String
        Dim aCode As Object

        aCode = Split(valor, IIf(sep = "", " ", sep))
        DescCB = Trim(Mid(valor, Len(aCode(0)) + 1))

    End Function

    Public Function Busca(ByVal Sql As String, ByVal DB As ADODB.Connection, Optional ByRef Retorna As String = "") As Boolean
        Dim rs As New ADODB.Recordset
        Dim r As Boolean
        rs.Open(Sql, DB)
        If Not rs.EOF Then
            If Retorna <> "" Then
                Retorna = CStr(rs.Fields(0).Value)
            End If
        End If
        r = Not rs.EOF
        rs.Close()
        Return r

    End Function

    Function DateCharDec(ByVal Fecha As Date) As String
        Return Fecha.ToString("yyyyMMdd")
    End Function

    Function dtAS400(ByVal fecha As Date) As String
        Return fecha.ToString("yyyy-MM-dd")
    End Function

    Public Function CheckVer(ByVal Aplicacion As String) As Boolean
        Dim rs As New ADODB.Recordset
        Dim lVer, locVer As String
        Dim msg As String
        Dim NomApp As String
        Dim Result As Boolean = False

        rs = New ADODB.Recordset

        lVer = "No hay registro"

        locVer = "(" & My.Application.Info.Version.Major & "." & _
         My.Application.Info.Version.Minor & "." & _
         My.Application.Info.Version.Revision & ")"

        If DB.Cursor(rs, "SELECT CCSDSC, CCUDC1, CCNOT2, CCDESC  FROM ZCCL01 WHERE CCTABL='RFVBVER' AND CCCODE = '" & Aplicacion & "'") Then
            lVer = rs.Fields("CCSDSC").Value
            NomApp = rs.Fields("CCDESC").Value
            If InStr(lVer, "*NOCHK") > 0 Then
                Result = True
            ElseIf InStr(lVer, locVer) > 0 Then
                Result = True
            End If
            If Result And rs.Fields("CCUDC1").Value = 0 Then
                msg = "La aplicacion " & Aplicacion & " no se puede usar en este momento." & vbCr
                msg = msg & "Razon: " & vbCr
                msg = msg & "=========================================" & vbCr
                If rs.Fields("CCNOT2").Value = "" Then
                    msg = msg & "Aplicacion en Mantenimiento." & vbCr
                Else
                    msg = msg & rs.Fields("CCNOT2").Value & vbCr
                End If
                msg = msg & "=========================================" & vbCr
                MsgBox(msg, vbExclamation, "Seguimiento a Pedidos")
                rs.Close()
                Return False
            End If
            rs.Close()
        End If

        If Not Result Then
            MsgBox("Aplicacion " & Aplicacion & vbCr & _
                   "=========================================" & vbCr & _
                   "Versión incorrecta del programa." & vbCr & vbCr & _
                   "Versión Registrada:" & vbTab & lVer & vbCr & vbCr & _
                   "Versión Actual:" & vbTab & locVer & vbCr & vbCr & _
                   "Comuníquese con Tecnología.")
        End If

        Return Result

    End Function

    Function Acceso(ByVal App As String, ByVal Usuario As String) As Boolean
        Dim rs As New ADODB.Recordset
        Dim Result As Boolean

        Result = DB.Cursor(rs, "SELECT CCCODE FROM ZCCL01 WHERE CCTABL='" & App & "' AND CCCODE='" & UCase(Usuario) & "'")
        rs.Close()
        Return Result

    End Function

    Sub wrtSQ(ByVal txt As String, ByVal tit As String)
        Dim sw As StreamWriter = New StreamWriter("E:\TEMP\sql.txt", True)
        Dim a As String = UCase(txt)

        a = Replace(a, "INSERT", vbCrLf & "INSERT") & "    "
        a = Replace(a, "VALUES", vbCrLf & "VALUES") & "    "
        a = Replace(a, "SELECT", vbCrLf & "SELECT") & "    "
        a = Replace(a, "FROM", vbCrLf & "FROM " & vbCrLf & "    ")
        a = Replace(a, "INNER", vbCrLf & "INNER")
        a = Replace(a, "LEFT OUTER", vbCrLf & "LEFT OUTER")
        a = Replace(a, "WHERE", vbCrLf & "WHERE " & vbCrLf & "    ")
        a = Replace(a, "GROUP BY", vbCrLf & "GROUP BY " & vbCrLf & "    ")
        a = Replace(a, "ORDER BY", vbCrLf & "ORDER BY ")
        a = Replace(a, "HAVING", vbCrLf & "HAVING ")

        sw.WriteLine(tit & vbCrLf & a & vbCrLf & "=======================================================================================")


        sw.Close()
    End Sub

End Module
