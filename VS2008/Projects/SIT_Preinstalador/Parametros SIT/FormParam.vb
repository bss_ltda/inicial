﻿Imports System
Imports System.IO


Public Class FormParametros

    Dim sWebConfig As String

    Public Shared Sub SetParametros(ByVal Archivo As String, ByVal FolderInicial As String, ByVal ArchWebConfig As String)
        Dim fEntrada As String = ""
        Dim fSalida As String = ""

        If ArchWebConfig = "" Then
            fEntrada = System.AppDomain.CurrentDomain.BaseDirectory() & "SIT_Scripts\" & Archivo & ".txt"
            fSalida = FolderInicial & "\PaqueteInstalacion\" & Archivo
        Else
            fEntrada = System.AppDomain.CurrentDomain.BaseDirectory() & "SIT_Scripts\Web\Publish\web.config.txt"
            fSalida = ArchWebConfig
        End If

        Try
            ' Create an instance of StreamReader to read from a file.
            Dim sr As StreamReader = New StreamReader(fEntrada)
            Dim sw As StreamWriter = New StreamWriter(fSalida)
            Dim line As String
            Do
                line = sr.ReadLine()
                If InStr(line, "{AS400}") <> 0 Then
                    line = Replace(line, "{AS400}", FormParametros.txtAS400.Text)
                End If
                If InStr(line, "{INITIALCATALOG}") <> 0 Then
                    line = Replace(line, "{INITIALCATALOG}", FormParametros.txtDB400.Text)
                End If
                If InStr(line, "{LIBRERIA}") <> 0 Then
                    line = Replace(line, "{LIBRERIA}", FormParametros.txtLIB400.Text)
                End If
                If InStr(line, "{USUARIO}") <> 0 Then
                    line = Replace(line, "{USUARIO}", UCase(FormParametros.txtUser400.Text))
                End If
                If InStr(line, "{PASSWORD}") <> 0 Then
                    line = Replace(line, "{PASSWORD}", UCase(FormParametros.txtPass400.Text))
                End If

                'Cadena de actualizacion
                If InStr(line, "{DATASOURCEFUENTE}") <> 0 Then
                    line = Replace(line, "{DATASOURCEFUENTE}", FormParametros.txtDataSource.Text)
                End If
                If InStr(line, "{INITIALCATALOGFUENTE}") <> 0 Then
                    line = Replace(line, "{INITIALCATALOGFUENTE}", FormParametros.txtIntialCatalog.Text)
                End If
                If InStr(line, "{USERIDFUENTE}") <> 0 Then
                    line = Replace(line, "{USERIDFUENTE}", FormParametros.txtUser.Text)
                End If
                If InStr(line, "{PASSWORDFUENTE}") <> 0 Then
                    line = Replace(line, "{PASSWORDFUENTE}", FormParametros.txtPass.Text)
                End If

                If InStr(line, "{FLR_SITDTS}") <> 0 Then
                    line = Replace(line, "{FLR_SITDTS}", FormParametros.txtFolderDTS.Text)
                End If

                If InStr(line, "{FLR_DESCARGAS}") <> 0 Then
                    line = Replace(line, "{FLR_DESCARGAS}", FormParametros.txtFolderDescarga.Text)
                End If

                sw.WriteLine(line)
            Loop Until line Is Nothing
            sr.Close()
            sw.Close()

        Catch E As Exception
            ' Let the user know what went wrong.
            Console.WriteLine("Debe ubicar el archivo.")
            Console.WriteLine(E.Message)
        End Try
    End Sub


    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdAS400.Click

        GuardaConfig()

        If Not Directory.Exists(Me.txtFolderDTS.Text) Then
            MsgBox("La cartpeta de Procedimientos no Existe", vbEmpty, Application.ProductName)
            Return
        End If

        If Not Directory.Exists(Me.txtFolderDescarga.Text) Then
            MsgBox("La cartpeta de Descarga no Existe", vbEmpty, Application.ProductName)
            Return
        End If

        If Not ChkConexion(1) Then
            MsgBox("Falló la conexión con el AS400.", vbEmpty, Application.ProductName)
            Return
        End If

        If Not ChkConexion(2) Then
            MsgBox("Falló la conexión con SQLServer.", vbEmpty, Application.ProductName)
            Return
        End If


        If Not File.Exists(Me.txtFolderDTS.Text & "PaqueteInstalacion\Web\Publish\web.config") Then
            If Not UnzipInstall(Me.txtFolderDTS.Text) Then
                Return
            End If
        End If

        SetParametros("BD\DB2.sql", Me.txtFolderDTS.Text, "")
        SetParametros("BD\SQL.sql", Me.txtFolderDTS.Text, "")
        SetParametros("BD\DTS\ActualizarMaestro.dtsx", Me.txtFolderDTS.Text, "")
        SetParametros("BD\DTS\ActualizarMovimiento.dtsx", Me.txtFolderDTS.Text, "")
        SetParametros("BD\DTS\ActualizarSaldo.dtsx", Me.txtFolderDTS.Text, "")
        SetParametros("BD\DTS\ActualizarTercero.dtsx", Me.txtFolderDTS.Text, "")
        SetParametros("BD\DTS\configCuenta.dtsConfig", Me.txtFolderDTS.Text, "")
        SetParametros("BD\DTS\configMaestro.dtsConfig", Me.txtFolderDTS.Text, "")
        SetParametros("BD\DTS\configMovimiento.dtsConfig", Me.txtFolderDTS.Text, "")
        SetParametros("BD\DTS\configTercero.dtsConfig", Me.txtFolderDTS.Text, "")
        SetParametros("Web\Publish\web.config", Me.txtFolderDTS.Text, "")

        MsgBox("Parámetros Aplicados.", vbInformation, Application.ProductName)

        Me.ButtonScriptsSQL.Enabled = True
        Me.ButtonInstallSIT.Enabled = True


    End Sub


    Private Sub FormParametros_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.txtAS400.Text = GetSetting(Application.ProductName, Application.ProductName, "txtAS400")
        Me.txtDB400.Text = GetSetting(Application.ProductName, Application.ProductName, "txtDB400")
        Me.txtLIB400.Text = GetSetting(Application.ProductName, Application.ProductName, "txtLIB400")
        Me.txtUser400.Text = GetSetting(Application.ProductName, Application.ProductName, "txtUser400")
        Me.txtPass400.Text = GetSetting(Application.ProductName, Application.ProductName, "txtPass400")

        Me.txtFolderDTS.Text = GetSetting(Application.ProductName, Application.ProductName, "txtFolderDTS")
        Me.txtFolderDescarga.Text = GetSetting(Application.ProductName, Application.ProductName, "txtFolderDescarga")

        Me.txtDataSource.Text = GetSetting(Application.ProductName, Application.ProductName, "txtDataSource")
        Me.txtIntialCatalog.Text = GetSetting(Application.ProductName, Application.ProductName, "txtIntialCatalog")
        Me.txtUser.Text = GetSetting(Application.ProductName, Application.ProductName, "txtUser")
        Me.txtPass.Text = GetSetting(Application.ProductName, Application.ProductName, "txtPass")

        sWebConfig = GetSetting(Application.ProductName, Application.ProductName, "sWebConfig")
        If File.Exists(sWebConfig) Then
            Me.ButtonWebConfig.ForeColor = Color.Black
        Else
            Me.ButtonWebConfig.ForeColor = Color.Red
        End If

        Me.cmdAS400.Tag = "0000"

    End Sub

    Private Sub GuardaConfig()
        Call SaveSetting(Application.ProductName, Application.ProductName, "txtAS400", Me.txtAS400.Text)
        Call SaveSetting(Application.ProductName, Application.ProductName, "txtDB400", Me.txtDB400.Text)
        Call SaveSetting(Application.ProductName, Application.ProductName, "txtLIB400", Me.txtLIB400.Text)
        Call SaveSetting(Application.ProductName, Application.ProductName, "txtUser400", Me.txtUser400.Text)
        Call SaveSetting(Application.ProductName, Application.ProductName, "txtPass400", Me.txtPass400.Text)

        Call SaveSetting(Application.ProductName, Application.ProductName, "txtFolderDTS", Me.txtFolderDTS.Text)
        Call SaveSetting(Application.ProductName, Application.ProductName, "txtFolderDescarga", Me.txtFolderDescarga.Text)

        Call SaveSetting(Application.ProductName, Application.ProductName, "txtDataSource", Me.txtDataSource.Text)
        Call SaveSetting(Application.ProductName, Application.ProductName, "txtIntialCatalog", Me.txtIntialCatalog.Text)
        Call SaveSetting(Application.ProductName, Application.ProductName, "txtUser", Me.txtUser.Text)
        Call SaveSetting(Application.ProductName, Application.ProductName, "txtPass", Me.txtPass.Text)
        Call SaveSetting(Application.ProductName, Application.ProductName, "txtPass2", Me.txtPass.Text)

        Call SaveSetting(Application.ProductName, Application.ProductName, "sWebConfig", sWebConfig)

    End Sub


    Private Sub cmdPruebaSQL_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim Conn As New ADODB.Connection
        Dim sConnString As New System.Text.StringBuilder()
        Dim bOK As Boolean = True

        sConnString.Append("Provider=SQLOLEDB.1;Data Source=" & Me.txtDataSource.Text & ";Persist Security Info=True;")
        sConnString.Append("Initial Catalog=" + Me.txtIntialCatalog.Text + ";")
        sConnString.Append("Password=" + Me.txtPass.Text + ";")
        sConnString.Append("User ID=" + Me.txtUser.Text + ";")
        Conn.ConnectionString = sConnString.ToString()

        Try
            Conn.Open()
            Conn.Close()
        Catch ex As Exception
            bOK = False
            MsgBox(Err.Description)
        End Try

        If bOK Then
            MsgBox("La conexión de prueba se realizó correctamente.")
        End If

    End Sub


    Function UnzipInstall(ByVal Carpeta As String) As Boolean
        Dim zip As New Chilkat.Zip()

        Dim unlocked As Boolean
        unlocked = zip.UnlockComponent("SMANRIZIP_ZrtwHkdh6IvN")
        If (Not unlocked) Then
            MsgBox(zip.LastErrorText)
            Return False
        End If

        Dim success As Boolean
        success = zip.OpenZip("PaqueteInstalacion.zip")

        ' Unzip to a specific directory:
        Dim count As Integer
        count = zip.Unzip(Carpeta)
        If (count = -1) Then
            MsgBox(zip.LastErrorText)
            Return False
        Else
            'MsgBox("Unzipped " + Str(count) + " files and directories")
            Return True
        End If

    End Function

    Private Sub ButtonFolderDTS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonFolderDTS.Click
        Me.txtFolderDTS.Text = SelCarpeta("Procedimientos", Me.txtFolderDTS.Text)
        GuardaConfig()

    End Sub

    Private Sub ButtonFolderDescarga_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonFolderDescarga.Click
        Me.txtFolderDescarga.Text = SelCarpeta("Descargas", Me.txtFolderDescarga.Text)
        GuardaConfig()

    End Sub

    Function SelCarpeta(ByVal Para As String, ByVal FolderInicial As String) As String
        Dim Result As String = FolderInicial
        Try
            ' Configuración del FolderBrowserDialog  
            With FolderBrowserDialog1
                .Reset() ' resetea  
                ' leyenda  
                .Description = " Seleccionar una carpeta para " & Para
                ' Path " Mis documentos "  
                If FolderInicial <> "" Then
                    .SelectedPath = FolderInicial
                Else
                    .SelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.MyComputer)
                End If

                ' deshabilita el botón " crear nueva carpeta "  
                '.ShowNewFolderButton = False
                '.RootFolder = Environment.SpecialFolder.Desktop  
                '.RootFolder = Environment.SpecialFolder.StartMenu  

                Dim ret As DialogResult = .ShowDialog ' abre el diálogo  

                ' si se presionó el botón aceptar ...  
                If ret = Windows.Forms.DialogResult.OK Then
                    Result = .SelectedPath.ToString()
                End If

                .Dispose()

            End With
        Catch oe As Exception
            MsgBox(oe.Message, MsgBoxStyle.Critical)
        End Try

        Return Result

    End Function


    Function ChkConexion(ByVal qConn As Integer) As Boolean
        Dim Conn As New ADODB.Connection
        Dim sConnString As String = ""
        Dim bOK As Boolean = True
        'Return True
        Conn.ConnectionTimeout = 20

        If qConn = 1 Then
            sConnString = "Provider=IBMDA400.DataSource;"
            sConnString += " Data Source=" & Me.txtAS400.Text & ";"
            sConnString += " User ID=" & Me.txtUser400.Text & ";"
            sConnString += " Password=" & Me.txtPass400.Text & ";"
            sConnString += " Persist Security Info=True;"
            sConnString += " Default Collection=" & Me.txtLIB400.Text & ";"
            sConnString += " Force Translate=0;"
        Else
            sConnString = "Provider=SQLOLEDB.1;Data Source=" & Me.txtDataSource.Text & ";Persist Security Info=True;"
            sConnString += "Initial Catalog=" + Me.txtIntialCatalog.Text + ";"
            sConnString += "Password=" + Me.txtPass.Text + ";"
            sConnString += "User ID=" + Me.txtUser.Text + ";"
        End If
        Conn.ConnectionString = sConnString

        Try
            Conn.Open()
            Conn.Close()
        Catch ex As Exception
            bOK = False
            MsgBox(Err.Description)
        End Try

        Return bOK

    End Function

    Private Sub cmdPruebaSQL_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPruebaSQL.Click

        GuardaConfig()

        If ChkConexion(2) Then
            MsgBox("La prueba de conexión fue satisfactoria.", vbInformation, Application.ProductName)
        End If
    End Sub

    Private Sub cmdPruebaAS400_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPruebaAS400.Click

        GuardaConfig()

        If ChkConexion(1) Then
            MsgBox("La prueba de conexión fue satisfactoria.", vbInformation, Application.ProductName)
        End If
    End Sub


    Private Sub ButtonScriptsSQL_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonScriptsSQL.Click

        If ChkConexion(2) Then
            Dim sComando As String = ""
            If MsgBox("Esta opción ejecuta el script de la base de datos SIT." & vbCrLf & _
                       "Sí ya existe. Se elminan tablas y datos." & vbCrLf & vbCrLf & _
                       "Continúa?", vbExclamation + vbYesNo) = vbYes Then
                sComando += "sqlcmd -U" + Me.txtUser.Text + " "
                sComando += "-P" + Me.txtPass.Text + " "
                sComando += "-S " & Me.txtDataSource.Text & " "
                sComando += "-i """ & Me.txtFolderDTS.Text & "\PaqueteInstalacion\BD\SQL.sql"" "
                sComando += "-o """ & Me.txtFolderDTS.Text & "\PaqueteInstalacion\BD\log_install.txt"" "
                'sqlcmd -Usitadmin -Padminsit -S 192.200.11aaa2.5 -i  D:\AppData\SIT_Scripts\prueba1.sql
                Shell(sComando, AppWinStyle.Hide, True)
                sComando = "notepad """ & Me.txtFolderDTS.Text & "\PaqueteInstalacion\BD\log_install.txt"" "
                Shell(sComando, AppWinStyle.NormalFocus, False)
                Me.ButtonInstallSIT.Enabled = True
                MsgBox("Script de la Base de Datos Ejecutado", vbInformation, Application.ProductName)
            End If
        End If

    End Sub

    Private Sub ButtonInstallSIT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonInstallSIT.Click
        If ChkConexion(1) And ChkConexion(2) Then
            If MsgBox("Esta opción Instalará la Aplicaci;on WEB." & vbCrLf & vbCrLf & _
                       "Continúa?", vbExclamation + vbYesNo) = vbYes Then
                Shell(Me.txtFolderDTS.Text & "\PaqueteInstalacion\Web\setup.exe", AppWinStyle.Hide, True)
                MsgBox("Aplicación Web Instalada.", vbInformation, Application.ProductName)
            End If
        End If
    End Sub

    Private Sub ButtonWebConfig_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ButtonWebConfig.Click

        If Not ChkConexion(1) Then
            MsgBox("Falló la conexión con el AS400.", vbEmpty, Application.ProductName)
            Return
        End If

        If Not ChkConexion(2) Then
            MsgBox("Falló la conexión con SQLServer.", vbEmpty, Application.ProductName)
            Return
        End If

        OpenFileDialog1.InitialDirectory = IIf(sWebConfig = "", "C:\Inetpub\wwwroot", Replace(sWebConfig, "\web.config", ""))
        OpenFileDialog1.Filter = "web config (*.config)|web.config"
        OpenFileDialog1.FileName = "web.config"
        OpenFileDialog1.FilterIndex = 2
        OpenFileDialog1.RestoreDirectory = True

        If OpenFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            If OpenFileDialog1.CheckFileExists() Then
                sWebConfig = OpenFileDialog1.FileName
                GuardaConfig()
                SetParametros("web.config", Me.txtFolderDTS.Text, sWebConfig)
                MsgBox("Parámetros de Conexión Actualizados.", vbInformation, Application.ProductName)
            End If
        End If

    End Sub

    Private Sub txtFolderDTS_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFolderDTS.GotFocus
        Me.ButtonFolderDTS.Focus()
    End Sub

    Private Sub txtFolderDescarga_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFolderDescarga.GotFocus
        Me.ButtonFolderDescarga.Focus()
    End Sub

End Class
