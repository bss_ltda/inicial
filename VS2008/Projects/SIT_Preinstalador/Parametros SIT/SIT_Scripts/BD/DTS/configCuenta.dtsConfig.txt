<?xml version="1.0"?>
<DTSConfiguration>
	<DTSConfigurationHeading>
		<DTSConfigurationFileInfo GeneratedBy="CHIMPANCE\Oscar" GeneratedFromPackageName="ActualizarSaldo" GeneratedFromPackageID="{2DBE1C0B-5A7E-42E4-ADB8-9EB8846A3C50}" GeneratedDate="2012-04-18 07:00:59 PM"/>
	</DTSConfigurationHeading>
	<Configuration ConfiguredType="Property" Path="\Package.Connections[ArchivoCxC].Properties[ConnectionString]" ValueType="String">
		<ConfiguredValue>{FLR_DESCARGAS}\cxc.csv</ConfiguredValue>
	</Configuration>
	<Configuration ConfiguredType="Property" Path="\Package.Connections[ArchivoCxP].Properties[ConnectionString]" ValueType="String">
		<ConfiguredValue>{FLR_DESCARGAS}\cxp.csv</ConfiguredValue>
	</Configuration>
	<Configuration ConfiguredType="Property" Path="\Package.Connections[ArchivoOtro].Properties[ConnectionString]" ValueType="String">
		<ConfiguredValue>{FLR_DESCARGAS}\otro.csv</ConfiguredValue>
	</Configuration>
	<Configuration ConfiguredType="Property" Path="\Package.Connections[CarpetaDestino].Properties[ConnectionString]" ValueType="String">
		<ConfiguredValue>{FLR_DESCARGAS}</ConfiguredValue>
	</Configuration>
	<Configuration ConfiguredType="Property" Path="\Package.Connections[CONEXIONDB2].Properties[ConnectionString]" ValueType="String">
		<ConfiguredValue>Data Source={AS400};User ID={USUARIO};Password={PASSWORD};Initial Catalog={BASEDEDATOS};Provider=IBMDA400.DataSource.1;Persist Security Info=True;</ConfiguredValue>
	</Configuration>
	<Configuration ConfiguredType="Property" Path="\Package.Connections[CONEXIONSQL].Properties[ConnectionString]" ValueType="String">
		<ConfiguredValue>Data Source={DATASOURCEFUENTE};User ID={USERIDFUENTE};Password={PASSWORDFUENTE};Initial Catalog={INITIALCATALOGFUENTE};Provider=SQLNCLI10.1;Persist Security Info=True;Auto Translate=False;Application Name=SSIS-ActualizarCuenta;</ConfiguredValue>
	</Configuration>
	<Configuration ConfiguredType="Property" Path="\Package.Connections[FTP].Properties[ServerName]" ValueType="String">
		<ConfiguredValue>{AS400}</ConfiguredValue>
	</Configuration>
	<Configuration ConfiguredType="Property" Path="\Package.Connections[FTP].Properties[ServerPassword]" ValueType="String">
		<ConfiguredValue>{PASSWORD}</ConfiguredValue>
	</Configuration>
	<Configuration ConfiguredType="Property" Path="\Package.Connections[FTP].Properties[ServerUserName]" ValueType="String">
		<ConfiguredValue>{USUARIO}</ConfiguredValue>
	</Configuration>
</DTSConfiguration>