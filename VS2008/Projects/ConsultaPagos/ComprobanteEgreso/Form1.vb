Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class Form1
	Inherits System.Windows.Forms.Form
	'UPGRADE_ISSUE: CrystalReport1 object was not upgraded. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6B85A2A7-FE9F-4FBE-AA0C-CF11AC86A305"'
	Dim Report As New CrystalReport1
	
	Private Sub Form1_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
		'UPGRADE_WARNING: Screen property Screen.MousePointer has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
		
		Dim aParam() As String
		
		
		If Trim(VB.Command()) = "" Then
			MsgBox("Hacen falta lo parametros", MsgBoxStyle.Critical, "Informe Pagos")
			End
		End If
		
		'UPGRADE_WARNING: Screen property Screen.MousePointer has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor
		aParam = Split(VB.Command(), "|")
		
		'gsAS400         '6
		'gsLIB           '7
		'sUser           '8
		'sPass           '9
		'gsUSER          '10
		'gsPASS          '11
		
		
		If aParam(10) = "" Then
			Me.Adodc1.ConnectionString = "Provider=IBMDA400.DataSource.1;Password=" & aParam(9) & ";Persist Security Info=True;" & "User ID=" & aParam(8) & ";Data Source=" & aParam(6) & ";Default Collection=" & aParam(7)
		Else
			Me.Adodc1.ConnectionString = "Provider=IBMDA400.DataSource.1;Password=" & aParam(11) & ";Persist Security Info=True;" & "User ID=" & aParam(10) & ";Data Source=" & aParam(6) & ";Default Collection=" & aParam(7)
		End If
		
		
		Me.Adodc1.RecordSource = "SELECT * FROM ZCCL01 WHERE CCTABL='RFVBVER' AND CCCODE='CPAGOS'"
		Me.Adodc1.Refresh()
		
		With Report
			'UPGRADE_WARNING: Couldn't resolve default property of object Report.ParameterFields. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			.ParameterFields.GetItemByName("ID").ClearCurrentValueAndRange()
			'UPGRADE_WARNING: Couldn't resolve default property of object Report.ParameterFields. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			.ParameterFields.GetItemByName("cuenta").ClearCurrentValueAndRange()
			'UPGRADE_WARNING: Couldn't resolve default property of object Report.ParameterFields. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			.ParameterFields.GetItemByName("banco").ClearCurrentValueAndRange()
			'UPGRADE_WARNING: Couldn't resolve default property of object Report.ParameterFields. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			.ParameterFields.GetItemByName("desde").ClearCurrentValueAndRange()
			'UPGRADE_WARNING: Couldn't resolve default property of object Report.ParameterFields. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			.ParameterFields.GetItemByName("hasta").ClearCurrentValueAndRange()
			'UPGRADE_WARNING: Couldn't resolve default property of object Report.ParameterFields. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			.ParameterFields.GetItemByName("usuario").ClearCurrentValueAndRange()
			'UPGRADE_WARNING: Couldn't resolve default property of object Report.ParameterFields. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			.ParameterFields.GetItemByName("bnk").ClearCurrentValueAndRange()
			'UPGRADE_WARNING: Couldn't resolve default property of object Report.ParameterFields. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			.ParameterFields.GetItemByName("phone").ClearCurrentValueAndRange()
			'UPGRADE_WARNING: Couldn't resolve default property of object Report.ParameterFields. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			.ParameterFields.GetItemByName("email").ClearCurrentValueAndRange()
			
			'UPGRADE_WARNING: Couldn't resolve default property of object Report.ParameterFields. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			.ParameterFields.GetItemByName("ID").AddCurrentValue(CShort(aParam(0)))
			'UPGRADE_WARNING: Couldn't resolve default property of object Report.ParameterFields. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			.ParameterFields.GetItemByName("cuenta").AddCurrentValue(aParam(1))
			'UPGRADE_WARNING: Couldn't resolve default property of object Report.ParameterFields. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			.ParameterFields.GetItemByName("banco").AddCurrentValue(aParam(2))
			'UPGRADE_WARNING: Couldn't resolve default property of object Report.ParameterFields. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			.ParameterFields.GetItemByName("desde").AddCurrentValue(aParam(3))
			'UPGRADE_WARNING: Couldn't resolve default property of object Report.ParameterFields. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			.ParameterFields.GetItemByName("hasta").AddCurrentValue(aParam(4))
			'UPGRADE_WARNING: Couldn't resolve default property of object Report.ParameterFields. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			.ParameterFields.GetItemByName("usuario").AddCurrentValue(aParam(5))
			'UPGRADE_WARNING: Couldn't resolve default property of object Report.ParameterFields. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
			.ParameterFields.GetItemByName("bnk").AddCurrentValue(aParam(6))
			If Not Me.Adodc1.Recordset.EOF Then
				'UPGRADE_WARNING: Couldn't resolve default property of object Report.ParameterFields. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				.ParameterFields.GetItemByName("phone").AddCurrentValue(CStr(Me.Adodc1.Recordset.Fields("CCNOT1").Value))
				'UPGRADE_WARNING: Couldn't resolve default property of object Report.ParameterFields. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
				.ParameterFields.GetItemByName("email").AddCurrentValue(CStr(Me.Adodc1.Recordset.Fields("CCNOT2").Value))
			End If
		End With
		
		
		
		'UPGRADE_WARNING: Couldn't resolve default property of object CRViewer1.ReportSource. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		'UPGRADE_WARNING: Couldn't resolve default property of object Report. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
		CRViewer1.ReportSource = Report
		CRViewer1.ViewReport()
		'UPGRADE_WARNING: Screen property Screen.MousePointer has a new behavior. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6BA9B8D2-2A32-4B6E-8D36-44949974A5B4"'
		System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
		
	End Sub
	
	'UPGRADE_WARNING: Event Form1.Resize may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
	Private Sub Form1_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Resize
		CRViewer1.Top = 0
		CRViewer1.Left = 0
		CRViewer1.Height = ClientRectangle.Height
		CRViewer1.Width = ClientRectangle.Width
		
	End Sub
End Class