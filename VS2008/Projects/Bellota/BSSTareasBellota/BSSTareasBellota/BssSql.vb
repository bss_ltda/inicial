﻿

Imports System.IO
Imports MySql.Data.MySqlClient

Module BSSSql


    Public conn As New ADODB.Connection
    Public local As String
    Public gsAS400 As String = "192.168.40.10"
    Public gsLib As String = ""
    Public sUser As String
    Public sUserApl As String
    Public sUserPass As String
    Public bBatchApp As Boolean = False
    Public nomApp As String = "VB.NET." & My.Application.Info.ProductName & "." & _
                               My.Application.Info.Version.Major & "." & _
                               My.Application.Info.Version.Minor & "." & _
                               My.Application.Info.Version.Revision
    Public Sentencia As String
    Public bLog As Boolean = False
    Public iNumLog As Integer
    Public msgErr As String
    Public gsConnString As String
    Public rAfectados As Integer
    Public Descripcion_Error As String
    Public conexion As MySqlConnection
    Public DB As New ADODB.Connection
    Public DBMySql As New ADODB.Connection
    Public rs As New ADODB.Recordset
    Public fSql
    Public vSql
    Public bSalto
    Public esInsert
    Public esUpdate
    Public esFin


    Public Function Call_PGM(ByVal Pgm As String) As Integer
        Dim r As Integer = 0
        'Dim Conn As 

        conn.Open("Provider=IBMDA400.DataSource.1;Password=zxc01asd;Persist Security Info=True;User ID=COSIS001;Data Source=213.62.216.85;Force Translate=0;Library List=PP61BPCSO,CP61BPCSF,CP61BPCESP,PP61BPCPTF,PP61CPEUSR,CP61BPCUSR,CP61BPCUSF,PP61BAMUSF,M6168345C;Naming Convention=1;Application Name=DFU001")

        Try
            conn.Execute(" {{ " & Pgm & " }} ", r)
            Return 0
        Catch
            msgErr = Err.Description
            Return -99
        End Try



    End Function

    Public Function CalcConsec(ByRef sID As String, ByRef TopeMax As String) As String
        ' Dim rs As New ADODB.Recordset
        Dim locID As String
        Dim sql As String = ""
        Dim sConsec As String = ""
        Dim tMax As Double
        Dim sFmt As String
        Dim nDig As Integer
        Dim cmd As New MySqlCommand
        Dim rs As New ADODB.Recordset
        Try
            nDig = Len(TopeMax)
            sFmt = New String("0", nDig)


            sql = "SELECT CCDESC FROM ZCCL01 WHERE CCTABL = 'SECUENCE' AND CCCODE='" & sID & "'"
            tMax = Val(TopeMax)

            With rs
                .CursorLocation = ADODB.CursorLocationEnum.adUseServer
                .Open(sql, DB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)
                If Not (.BOF And .EOF) Then
                    locID = Val(.Fields("CCDESC").Value) + 1
                    If locID > tMax Then
                        locID = 1
                    End If
                    sConsec = Right(sFmt & locID, nDig)
                    .Fields("CCDESC").Value = sConsec
                    .Update()
                    .Close()
                Else

                    locID = 1
                    .Close()
                    sConsec = Right(sFmt & locID, nDig)
                    sql = " INSERT INTO ZCC (CCID, CCTABL, CCCODE, CCDESC ) " & " VALUES( 'CC', 'SECUENCE', '" & sID & "', '" & sConsec & "' )"
                    DB.Execute(sql)
                End If

            End With

        Catch ex As Exception
            ' WrtSqlError(sql, Err.Description)
        End Try


        Return sConsec

    End Function
    Sub wrtLog(operacion, usuario, programa, evento, sql)
        Dim fSql, vSql

        fSql = " INSERT INTO BFLOG( "
        vSql = " VALUES ( "
        fSql = fSql & " OPERACION , " : vSql = vSql & "'" & operacion & "', "     '//102A  
        fSql = fSql & " USUARIO   , " : vSql = vSql & "'" & usuario & "', "     '//502A  
        fSql = fSql & " PROGRAMA  , " : vSql = vSql & "'" & programa & "', "    '//502A  
        fSql = fSql & " EVENTO    , " : vSql = vSql & "'" & evento & "', "     '//15002A
        fSql = fSql & " ALERT    , " : vSql = vSql & "1, "
        fSql = fSql & " TXTSQL    ) " : vSql = vSql & "'" & sql & "' ) "     '//10002A
        DBMySql.Execute(fSql & vSql)

    End Sub
    Sub abrirConexion()
        Dim ConnectionString As String
        'DB.Open("Provider=IBMDA400.DataSource.1;Password=zxc01asd;Persist Security Info=True;User ID=COSIS001;Data Source=213.62.216.85;Force Translate=0;Library List=PP61BPCSO,CP61BPCSF,CP61BPCESP,PP61BPCPTF,PP61CPEUSR,CP61BPCUSR,CP61BPCUSF,PP61BAMUSF,M6168345C;Naming Convention=1;Application Name=DFU001")
        'ConnectionString = "Provider=MSDASQL.1;Password=cntrsnSpcl20;Persist Security Info=True;User ID=root;Data Source=BELLOTAPR;Initial Catalog=iBellotaPruebas"
        'DBMySql.Open(ConnectionString)
        DB = New ADODB.Connection
        gsConnString = ""

        Try
            ConnectionString = "Provider=MSDASQL.1;Password=" & paramWebConfig(My.Settings.WebConfig, "MYSQL_PASS") & ";Persist Security Info=True;User ID=" & paramWebConfig(My.Settings.WebConfig, "MYSQL_USER") & ";Data Source=" & paramWebConfig(My.Settings.WebConfig, "MYSQL_ODBC") & ";Initial Catalog=" & paramWebConfig(My.Settings.WebConfig, "MYSQL_LIB")
            DBMySql.Open(ConnectionString)

        Catch

        End Try

        Try

            gsConnString = "Provider=" & My.Settings.gsProvider & ";Data Source=" & paramWebConfig(My.Settings.WebConfig, "AS400_SERVER") & ";"
            gsConnString = gsConnString & "Persist Security Info=False;;Library List=PP61BPCSO,CP61BPCSF,CP61BPCESP,PP61BPCPTF,PP61CPEUSR,CP61BPCUSR,CP61BPCUSF,PP61BAMUSF,M6168345C;"
            gsConnString = gsConnString & "Password=" & paramWebConfig(My.Settings.WebConfig, "AS400_PASS") & ";User ID=" & paramWebConfig(My.Settings.WebConfig, "AS400_USER") & ";"
            gsConnString = gsConnString & "Force Translate=0;Naming Convention=1;"
            DB.Open(gsConnString)


        Catch ex As Exception
            wrtLog("CargarParam", "COSIS001", "Ejecucion de Transacciones", ex.ToString, gsConnString)
        End Try
    End Sub
    Function paramWebConfig(webConfig, param)
        Dim oXML, oNode, oChild, oAttr

        oXML = CreateObject("Microsoft.XMLDOM")
        oXML.Async = "false"
        oXML.Load(webConfig)
        oNode = oXML.GetElementsByTagName("appSettings").Item(0)
        oChild = oNode.GetElementsByTagName("add")
        For Each oAttr In oChild
            If UCase(oAttr.getAttribute("key")) = UCase(param) Then
                Return oAttr.getAttribute("value")
            End If
        Next
        Return ""

    End Function


    Sub mkSql_h(sep)
        mkSql(sep, "", "", 0)
    End Sub

    Sub mkSql_f(sep)
        mkSql(sep, "", "", 0)
    End Sub


    Public Function ExecuteSQL(ByVal sql As String) As ADODB.Recordset
        Try
            Sentencia = sql
            Return DB.Execute(sql)
        Catch
            'WrtSqlError(sql, Err.Description)
            Return Nothing
        End Try

    End Function

    Sub mkSql_d(sep, Campo, valor, Fin)
        mkSql(sep, Campo, valor, Fin)
    End Sub

    Sub mkSql(sep, Campo, valor, Fin)

        If Campo = "" Then
            If InStr(sep, "INSERT INTO") > 0 Then
                esInsert = True
                fSql = sep & IIf(bSalto, vbCrLf, "")
            ElseIf InStr(sep, "VALUES") Then
                vSql = sep & IIf(bSalto, vbCrLf, "")
            ElseIf InStr(sep, "UPDATE") Then
                fSql = sep & IIf(bSalto, vbCrLf, "")
                vSql = ""
                esUpdate = True
                esInsert = False
            ElseIf esFin Then
                vSql = vSql & " " & sep & IIf(bSalto, vbCrLf, "")
            ElseIf esInsert Then
                vSql = sep
            Else
                fSql = sep
            End If
        Else
            sep = Trim(sep)
            If esInsert Then
                fSql = fSql & Campo
                Select Case Fin
                    Case 0
                        fSql = fSql & " , "
                    Case 10, 11, -1, 1
                        fSql = fSql & " ) "
                End Select
                If valor = Nothing Then
                    valor = IIf(sep = "'", "", "0")
                End If
                vSql = vSql & sep & CStr(valor) & sep
                Select Case Fin
                    Case 10
                        vSql = vSql & "  "
                    Case 11, -1, 1
                        vSql = vSql & " )"
                    Case 0
                        vSql = vSql & " , "
                End Select
            Else
                If valor = Nothing Then
                    valor = IIf(sep = "", "", "0")
                End If
                fSql = fSql & Campo & " = " & sep & CStr(valor) & sep & IIf(Fin = 0, ", ", "")
            End If
            esFin = Fin <> 0
            If bSalto Then
                fSql = fSql & vbCrLf
                If vSql <> "" Then
                    vSql = vSql & vbCrLf
                End If
            End If
        End If

    End Sub

    Function Sql()
        Sql = fSql & vSql
    End Function

    Function v_fSql()
        v_fSql = fSql
    End Function

    Function v_Sql()
        v_Sql = vSql
    End Function

    Sub salto(v)
        bSalto = v
    End Sub

    Function DB2_DATEDEC()
        DB2_DATEDEC = " ( YEAR( NOW() ) * 10000 + MONTH( NOW()  ) * 100 + DAY( NOW() ) )"
    End Function
    Function DB2_TIMEDEC()
        DB2_TIMEDEC = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    End Function
    Function DB2_TIMEDEC0()
        DB2_TIMEDEC0 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    End Function
    Function DB2_TIMEDEC1()
        DB2_TIMEDEC1 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 ) "
    End Function


End Module
