﻿
Imports System.IO
Imports MySql.Data.MySqlClient

Public Class wsTareas
    Private WithEvents tim As New Timers.Timer(30000)
    Protected Overrides Sub OnStart(ByVal args() As String)
        tim.Enabled = True
    End Sub

    Protected Overrides Sub OnStop()
        ' Agregue el código aquí para realizar cualquier anulación necesaria para detener el servicio.
    End Sub

    Private Sub tim_Elapsed(sender As Object, e As System.Timers.ElapsedEventArgs) Handles tim.Elapsed

        tim.Enabled = False

        abrirConexion()
        actualizacionPR()
        TransaccionAcero()
        TransaccionRAcero()
        TransaccionRJ()

        tim.Enabled = True

    End Sub

    Sub actualizacionPR()

        Dim cmd As New MySqlCommand
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim rs2 As New ADODB.Recordset

        fSql = "SELECT TORD FROM BFREP WHERE TSTS <> 2 GROUP BY TORD "
        Try
            rs.Open(fSql, DBMySql)
        Catch E As Exception
            wrtLog("Actualizacion Cant PR", "COSIS001", "ServicioAutomatico", E.Message, fSql)
            Return
        End Try


        While Not rs.EOF
            fSql = "SELECT SQFIN FROM FSO WHERE SORD =" & rs("TORD").Value
            Try
                rs1.Open(fSql, DBMySql)

            Catch E As Exception
                wrtLog("Actualizacion Cant PR", "COSIS001", "ServicioAutomatico", E.Message, fSql)
                Exit Sub
            End Try


            If Not rs1.EOF Then

                fSql = "UPDATE BFREP SET TQRECB = " & rs1("SQFIN").Value & " WHERE  TORD =" & rs("TORD").Value
                Try
                    DBMySql.Execute(fSql & vSql)
                Catch E As Exception
                    wrtLog("Actualizacion Cant PR", "COSIS001", "ServicioAutomatico", E.Message, fSql)
                    Exit Sub
                End Try
            End If
            rs.MoveNext()
            rs1.Close()
        End While
        rs.Close()
    End Sub


    Sub TransaccionRAcero()

        Dim cmd As New MySqlCommand
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim Sql As String
        Dim rs2 As New ADODB.Recordset
        Dim canti As Double
        Dim canti1 As Double


        fSql = "SELECT * FROM BFREPACE INNER JOIN BFREPP ON CPRODPT  = PPROD "
        fSql = fSql & " WHERE CENVIADO = 0 AND CTIPO = 'REVERSION'"

        Try
            rs.Open(fSql, DBMySql)
        Catch E As Exception
            wrtLog("TransaccionRACero", "COSIS001", "ServicioAutomatico", E.Message, fSql)
            Exit Sub
        End Try
        While Not rs.EOF

            fSql = "SELECT * FROM BFREP INNER JOIN BFREPP ON TPROD  = PPROD "
            fSql = fSql & " WHERE TORD ='" & rs("CORDEN").Value & "'"
            Try
                rs2.Open(fSql, DBMySql)

            Catch E As Exception
                wrtLog("TransaccionRACero", "COSIS001", "ServicioAutomatico", E.Message, fSql)
                Exit Sub
            End Try

            Sql = " INSERT INTO BFCIM1 "
            Sql = Sql & "	(AOPER, "
            Sql = Sql & "		AORD, "
            Sql = Sql & "		APROD,"
            Sql = Sql & "		AQTY"
            Sql = Sql & "		)"
            Sql = Sql & "		VALUES"
            Sql = Sql & "		('-CA', "
            Sql = Sql & "		'" & rs2("CORDEN").Value & "', "
            Sql = Sql & "		'" & rs2("CPRODPT").Value & "',"
            Sql = Sql & "		'" & rs2("CQREQPT").Value & "'"
            Sql = Sql & "		)"
            Try
                DBMySql.Execute(Sql)

            Catch E As Exception
                wrtLog("TransaccionRACero", "COSIS001", "ServicioAutomatico", E.Message, fSql)
                Exit Sub
            End Try



            '///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            '///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            Dim Fecha = ""
            If Fecha = "" Then
                Fecha = DB2_DATEDEC()
            End If

            Dim cons
            cons = CalcConsec("CIMNUM", "99999999")

            mkSql_h(" INSERT INTO BFCIM ( ")
            mkSql_h(" VALUES ( ")
            mkSql_d("'", " INID   ", "IN", 0)                   '//2A    Record Identifier
            mkSql_d("'", " INTRAN ", "WT", 0)               '//2A    Transaction Type
            mkSql_d(" ", " INCTNR ", 0, 0)            '//2A    Transaction Type
            mkSql_d("'", " INMFGR ", 0, 0)            '//2A    Transaction Type
            mkSql_d(" ", " INDATE ", Fecha, 0)                  '//8P0   Date		
            mkSql_d(" ", " INTRN  ", Right(cons, 5), 0)                  '//5P0   INTRN
            mkSql_d("'", " INMLOT  ", "CA" & cons, 0)                '//5P0   INTRN
            mkSql_d("'", " INREAS ", "01", 0)                  '//5P0   INTRN
            mkSql_d(" ", " INREF  ", rs2("TORD").Value, 0)                  '//8P0   Date
            mkSql_d("'", " INPROD ", rs2("PBODPP").Value, 0)               '//15A   Item Number
            mkSql_d("'", " INWHSE ", rs2("PBODPP").Value, 0)                 '//2A    Warehouse
            mkSql_d("'", " INLOT  ", rs2("PLOTE").Value, 0)                   '//10A   Lot Number
            mkSql_d("'", " INLOC  ", rs2("PLOCPP").Value, 0)              '//6A    Location Code
            canti = CDbl(rs("CQREQPP").Value)
            mkSql_d("'", " INQTY  ", canti, 0)                '//10A   Creted by
            mkSql_d("'", " INCBY  ", "COSIS001", 0)                '//10A   Creted by
            mkSql_d(" ", " INCDT  ", DB2_DATEDEC(), 0)      '//8P0   Date Created
            mkSql_d(" ", " INCTM  ", DB2_TIMEDEC(), 1)      '//6P0   Time Created

            Try
                DB.Execute(fSql & vSql)

            Catch E As Exception
                wrtLog("TransaccionRACero", "COSIS001", "ServicioAutomatico", E.Message, fSql & vSql)
                Exit Sub
            End Try

            fSql = "SELECT * FROM CIC "
            fSql = fSql & " WHERE ICPROD = '" & rs("CPRODPP").Value & "' AND ICFAC = 'IC'"
            Try
                rs1.Open(fSql, DBMySql)

            Catch E As Exception
                wrtLog("TransaccionRACero", "COSIS001", "ServicioAutomatico", E.Message, fSql)
                Exit Sub
            End Try

            cons = CalcConsec("CIMNUM", "99999999")
            mkSql_h(" INSERT INTO BFCIM ( ")
            mkSql_h(" VALUES ( ")
            mkSql_d("'", " INID   ", "IN", 0)                   '//2A    Record Identifier
            mkSql_d("'", " INTRAN ", "CI", 0)               '//2A    Transaction Type
            mkSql_d(" ", " INCTNR ", 0, 0)            '//2A    Transaction Type
            mkSql_d("'", " INMFGR ", 0, 0)            '//2A    Transaction Type
            mkSql_d(" ", " INDATE ", Fecha, 0)                  '//8P0   Date		
            mkSql_d(" ", " INTRN  ", Right(cons, 5), 0)                  '//5P0   INTRN
            mkSql_d("'", " INMLOT  ", "RP" & cons, 0)                '//5P0   INTRN
            mkSql_d("'", " INREAS ", "01", 0)                  '//5P0   INTRN
            mkSql_d(" ", " INREF  ", rs2("CORDEN").Value, 0)                  '//8P0   Date
            mkSql_d("'", " INPROD ", rs2("CPRODPP").Value, 0)               '//15A   Item Number
            mkSql_d("'", " INWHSE ", rs1("ICPLN").Value, 0)                 '//2A    Warehouse
            mkSql_d("'", " INLOT  ", "", 0)                   '//10A   Lot Number
            mkSql_d("'", " INLOC  ", "", 0)              '//6A    Location Code

            canti1 = CDbl(rs("CQREQPP").Value)
            mkSql_d("'", " INQTY  ", canti1, 0)                '//10A   Creted by
            mkSql_d("'", " INCBY  ", "COSIS001", 0)                '//10A   Creted by
            mkSql_d(" ", " INCDT  ", DB2_DATEDEC(), 0)      '//8P0   Date Created
            mkSql_d(" ", " INCTM  ", DB2_TIMEDEC(), 1)      '//6P0   Time Created
            Try
                DB.Execute(fSql & vSql)
                rs2.Close()
                rs1.Close()

            Catch E As Exception
                wrtLog("TransaccionRACero", "COSIS001", "ServicioAutomatico", E.Message, fSql & vSql)
                Exit Sub
            End Try

            '///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            '///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            Dim aReversar
            aReversar = Split(rs("CORDENR").Value, "-")
            fSql = "SELECT * FROM BFREPACE INNER JOIN BFREPP ON CPRODPT  = PPROD "
            fSql = fSql & " WHERE CORDEN =" & aReversar(1)
            Try
                rs2.Open(fSql, DBMySql)

            Catch E As Exception
                wrtLog("TransaccionRACero", "COSIS001", "ServicioAutomatico", E.Message, fSql & vSql)
                Exit Sub
            End Try
            If Fecha = "" Then
                Fecha = DB2_DATEDEC()
            End If

            cons = CalcConsec("CIMNUM", "99999999")

            mkSql_h(" INSERT INTO BFCIM ( ")
            mkSql_h(" VALUES ( ")
            mkSql_d("'", " INID   ", "IN", 0)                   '//2A    Record Identifier
            mkSql_d("'", " INTRAN ", "WT", 0)               '//2A    Transaction Type
            mkSql_d(" ", " INCTNR ", 0, 0)            '//2A    Transaction Type
            mkSql_d("'", " INMFGR ", 0, 0)            '//2A    Transaction Type
            mkSql_d(" ", " INDATE ", Fecha, 0)                  '//8P0   Date		
            mkSql_d(" ", " INTRN  ", Right(cons, 5), 0)                  '//5P0   INTRN
            mkSql_d("'", " INMLOT  ", "CA" & cons, 0)                '//5P0   INTRN
            mkSql_d("'", " INREAS ", "01", 0)                  '//5P0   INTRN
            mkSql_d(" ", " INREF  ", rs2("CORDEN").Value, 0)                  '//8P0   Date
            mkSql_d("'", " INPROD ", rs2("CPRODPT").Value, 0)               '//15A   Item Number
            mkSql_d("'", " INWHSE ", rs2("PBODPP").Value, 0)                 '//2A    Warehouse
            mkSql_d("'", " INLOT  ", rs2("PLOTE").Value, 0)                   '//10A   Lot Number
            mkSql_d("'", " INLOC  ", rs2("PLOCPP").Value, 0)              '//6A    Location Code

            canti = -(CDbl(rs2("CQREQPT").Value))
            mkSql_d("'", " INQTY  ", canti, 0)                '//10A   Creted by
            mkSql_d("'", " INCBY  ", "COSIS001", 0)                '//10A   Creted by
            mkSql_d(" ", " INCDT  ", DB2_DATEDEC(), 0)      '//8P0   Date Created
            mkSql_d(" ", " INCTM  ", DB2_TIMEDEC(), 1)      '//6P0   Time Created
            Try
                DB.Execute(fSql & vSql)
            Catch E As Exception
                wrtLog("TransaccionRACero", "COSIS001", "ServicioAutomatico", E.Message, fSql & vSql)
                Exit Sub
            End Try

            fSql = "SELECT * FROM CIC "
            fSql = fSql & " WHERE ICPROD = '" & rs2("CPRODPP").Value & "' AND ICFAC = 'IC'"
            Try
                rs1.Open(fSql, DBMySql)
            Catch E As Exception
                wrtLog("TransaccionRACero", "COSIS001", "ServicioAutomatico", E.Message, fSql & vSql)
                Exit Sub
            End Try
            cons = CalcConsec("CIMNUM", "99999999")
            mkSql_h(" INSERT INTO BFCIM ( ")
            mkSql_h(" VALUES ( ")
            mkSql_d("'", " INID   ", "IN", 0)                   '//2A    Record Identifier
            mkSql_d("'", " INTRAN ", "CI", 0)               '//2A    Transaction Type
            mkSql_d(" ", " INCTNR ", 0, 0)            '//2A    Transaction Type
            mkSql_d("'", " INMFGR ", 0, 0)            '//2A    Transaction Type
            mkSql_d(" ", " INDATE ", Fecha, 0)                  '//8P0   Date		
            mkSql_d(" ", " INTRN  ", Right(cons, 5), 0)                  '//5P0   INTRN
            mkSql_d("'", " INMLOT  ", "RP" & cons, 0)                '//5P0   INTRN
            mkSql_d("'", " INREAS ", "01", 0)                  '//5P0   INTRN
            mkSql_d(" ", " INREF  ", rs2("CORDEN").Value, 0)                  '//8P0   Date
            mkSql_d("'", " INPROD ", rs2("CPRODPP").Value, 0)               '//15A   Item Number
            mkSql_d("'", " INWHSE ", rs1("ICPLN").Value, 0)                 '//2A    Warehouse
            mkSql_d("'", " INLOT  ", "", 0)                   '//10A   Lot Number
            mkSql_d("'", " INLOC  ", "", 0)              '//6A    Location Code

            canti1 = -(CDbl(rs2("CQREQPP").Value))
            mkSql_d("'", " INQTY  ", canti1, 0)                '//10A   Creted by
            mkSql_d("'", " INCBY  ", "COSIS001", 0)                '//10A   Creted by
            mkSql_d(" ", " INCDT  ", DB2_DATEDEC(), 0)      '//8P0   Date Created
            mkSql_d(" ", " INCTM  ", DB2_TIMEDEC(), 1)      '//6P0   Time Created
            Try
                DB.Execute(fSql & vSql)
            Catch E As Exception
                wrtLog("TransaccionRACero", "COSIS001", "ServicioAutomatico", E.Message, fSql & vSql)
                Exit Sub
            End Try
            fSql = " CALL CP61BPCUSR/BCTRAINVB "
            Try
                DB.Execute(fSql)

                fSql = "UPDATE bfrepace SET CENVIADO =1 WHERE WRKID = " & rs("WRKID").Value
                DBMySql.Execute(fSql)

                fSql = "UPDATE BFREP SET TQREQACEB = TQREQACEB + " & canti & " WHERE TNUMREP = " & rs("TNUMREP").Value
                DBMySql.Execute(fSql)

                fSql = "UPDATE BFREP SET TRPODACEB = TRPODACEB + " & canti1 & " WHERE TNUMREP = " & rs("TNUMREP").Value
                DBMySql.Execute(fSql)

                rs.Close()
                rs1.Close()
                rs.MoveNext()
            Catch E As Exception
                wrtLog("TransaccionRACero", "COSIS001", "ServicioAutomatico", E.Message, fSql)
                Exit Sub
            End Try
        End While
        Try
            fSql = " CALL CP61BPCUSR/BCTRAINVB "
            DB.Execute(fSql)
        Catch E As Exception
            wrtLog("TransaccionRACero", "COSIS001", "ServicioAutomatico", E.Message, fSql)
            Exit Sub
        End Try
    End Sub



    Sub TransaccionRJ()

        Dim cmd As New MySqlCommand
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim Sql As String
        Dim canti As Double
        Dim Fecha = ""

        Try
            rs.Open("SELECT * FROM ibellotapruebas.BFREP INNER JOIN BFREPP ON TPROD = PPROD INNER JOIN BFREPTURRJ ON TNUMREP = RNUMREP WHERE RENVIADO = 0", DBMySql)
        Catch E As Exception
            wrtLog("TransaccionRJ", "COSIS001", "ServicioAutomatico", E.Message, fSql)
            Exit Sub
        End Try
        While Not rs.EOF
            If Fecha = "" Then
                Fecha = DB2_DATEDEC()
            End If
            Dim cons
            cons = CalcConsec("CIMNUM", "99999999")

            mkSql_h(" INSERT INTO BFCIM ( ")
            mkSql_h(" VALUES ( ")
            mkSql_d("'", " INID   ", "IN", 0)                   '//2A    Record Identifier
            mkSql_d("'", " INTRAN ", "RJ", 0)               '//2A    Transaction Type
            mkSql_d(" ", " INCTNR ", rs("RPROCESO"), 0)            '//2A    Transaction Type
            mkSql_d("'", " INMFGR ", rs("RPROCESO"), 0)            '//2A    Transaction Type
            mkSql_d(" ", " INDATE ", Fecha, 0)                  '//8P0   Date		
            mkSql_d(" ", " INTRN  ", Right(cons, 5), 0)                  '//5P0   INTRN
            mkSql_d("'", " INMLOT  ", "RP" & cons, 0)                '//5P0   INTRN
            mkSql_d("'", " INREAS ", rs("RCAUSAL").Value, 0)                  '//5P0   INTRN
            mkSql_d(" ", " INREF  ", rs("TORD").Value, 0)                  '//8P0   Date
            mkSql_d("'", " INPROD ", rs("TPROD").Value, 0)               '//15A   Item Number
            mkSql_d("'", " INWHSE ", rs("PBODPP").Value, 0)                 '//2A    Warehouse
            mkSql_d("'", " INLOT  ", rs("PLOTE").Value, 0)                   '//10A   Lot Number
            mkSql_d("'", " INLOC  ", rs("PLOCPP").Value, 0)              '//6A    Location Code
            canti = (CDbl(rs("RQREP").Value))
            mkSql_d("'", " INQTY  ", canti, 0)                '//10A   Creted by
            mkSql_d("'", " INCBY  ", "COSIS001", 0)                '//10A   Creted by
            mkSql_d(" ", " INCDT  ", DB2_DATEDEC(), 0)      '//8P0   Date Created
            mkSql_d(" ", " INCTM  ", DB2_TIMEDEC(), 1)      '//6P0   Time Created
            Try
                DB.Execute(fSql & vSql)

            Catch E As Exception
                wrtLog("TransaccionRJ", "COSIS001", "ServicioAutomatico", E.Message, fSql & vSql)
                Exit Sub
            End Try
            Sql = " INSERT INTO BFCIM1 "
            Sql = Sql & "	(AOPER, "
            Sql = Sql & "		AORD, "
            Sql = Sql & "		APROD,"
            Sql = Sql & "		AQTY"
            Sql = Sql & "		)"
            Sql = Sql & "		VALUES"
            Sql = Sql & "		('RP', "
            Sql = Sql & "		'" & rs("TORD").Value & "', "
            Sql = Sql & "		'" & rs("TPROD").Value & "',"
            Sql = Sql & "		'" & rs("RQREP").Value & "'"
            Sql = Sql & "		)"

            Try

                DBMySql.Execute(Sql)

                Sql = "UPDATE BFREPTURRJ SET RENVIADO = 1  WHERE RWRKID = " & rs("RWRKID").Value
                DBMySql.Execute(Sql)

                Sql = "UPDATE BFREP SET TQREDB = TQREDB +  " & rs("RQREP").Value & " WHERE RWRKID = " & rs("RWRKID").Value
                DBMySql.Execute(Sql)

                rs.MoveNext()

            Catch E As Exception
                wrtLog("TransaccionRJ", "COSIS001", "ServicioAutomatico", E.Message, Sql)
                Exit Sub
            End Try
        End While
        rs.Close()

        Try
            fSql = " CALL CP61BPCUSR/BCTRAINVB "
            DB.Execute(fSql)
        Catch E As Exception
            wrtLog("TransaccionRJ", "COSIS001", "ServicioAutomatico", E.Message, fSql)
            Exit Sub
        End Try

    End Sub

    Sub TransaccionAcero()

        Dim cmd As New MySqlCommand
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim Sql As String
        Dim Fecha = ""
        Dim cons As String


        fSql = "SELECT * FROM BFREPACE INNER JOIN BFREPP ON CPRODPT  = PPROD "
        fSql = fSql & " WHERE CENVIADO = 0 AND CTIPO = 'REPORTE'"
        Try
            rs.Open(fSql, DBMySql)
        Catch E As Exception
            wrtLog("TransaccionAcero", "COSIS001", "ServicioAutomatico", E.Message, fSql)
            Exit Sub
        End Try
        While Not rs.EOF
            Sql = " INSERT INTO BFCIM1 "
            Sql = Sql & "	(AOPER, "
            Sql = Sql & "		AORD, "
            Sql = Sql & "		APROD,"
            Sql = Sql & "		AQTY"
            Sql = Sql & "		)"
            Sql = Sql & "		VALUES"
            Sql = Sql & "		('CA', "
            Sql = Sql & "		'" & rs("CORDEN").Value & "', "
            Sql = Sql & "		'" & rs("CPRODPT").Value & "',"
            Sql = Sql & "		'" & rs("CQREQPT").Value & "'"
            Sql = Sql & "		)"
            Try
                DBMySql.Execute(Sql)

            Catch E As Exception
                wrtLog("TransaccionAcero", "COSIS001", "ServicioAutomatico", E.Message, Sql)
                Exit Sub
            End Try

            If Fecha = "" Then
                Fecha = DB2_DATEDEC()
            End If

            cons = CalcConsec("CIMNUM", "99999999")

            mkSql_h(" INSERT INTO BFCIM ( ")
            mkSql_h(" VALUES ( ")
            mkSql_d("'", " INID   ", "IN", 0)                   '//2A    Record Identifier
            mkSql_d("'", " INTRAN ", "WT", 0)               '//2A    Transaction Type
            mkSql_d(" ", " INCTNR ", 0, 0)            '//2A    Transaction Type
            mkSql_d("'", " INMFGR ", 0, 0)            '//2A    Transaction Type
            mkSql_d(" ", " INDATE ", Fecha, 0)                  '//8P0   Date		
            mkSql_d(" ", " INTRN  ", Right(cons, 5), 0)                  '//5P0   INTRN
            mkSql_d("'", " INMLOT  ", "CA" & cons, 0)                '//5P0   INTRN
            mkSql_d("'", " INREAS ", "01", 0)                  '//5P0   INTRN
            mkSql_d(" ", " INREF  ", rs("CORDEN").Value, 0)                  '//8P0   Date
            mkSql_d("'", " INPROD ", rs("CPRODPT").Value, 0)               '//15A   Item Number
            mkSql_d("'", " INWHSE ", rs("PBODPP").Value, 0)                 '//2A    Warehouse
            mkSql_d("'", " INLOT  ", rs("PLOTE").Value, 0)                   '//10A   Lot Number
            mkSql_d("'", " INLOC  ", rs("PLOCPP").Value, 0)              '//6A    Location Code
            Dim canti
            Dim canti1
            canti = CDbl(rs("CQREQPT").Value)
            mkSql_d("'", " INQTY  ", canti, 0)                '//10A   Creted by
            mkSql_d("'", " INCBY  ", "COSIS001", 0)                '//10A   Creted by
            mkSql_d(" ", " INCDT  ", DB2_DATEDEC(), 0)      '//8P0   Date Created
            mkSql_d(" ", " INCTM  ", DB2_TIMEDEC(), 1)      '//6P0   Time Created
            Try
                DB.Execute(fSql & vSql)
            Catch E As Exception
                wrtLog("TransaccionAcero", "COSIS001", "ServicioAutomatico", E.Message, fSql & vSql)
                Exit Sub
            End Try
            fSql = "SELECT * FROM CIC "
            fSql = fSql & " WHERE ICPROD = '" & rs("CPRODPP").Value & "' AND ICFAC = 'IC'"
            Try
                rs1.Open(fSql, DBMySql)
            Catch E As Exception
                wrtLog("TransaccionAcero", "COSIS001", "ServicioAutomatico", E.Message, fSql)
                Exit Sub
            End Try
            cons = CalcConsec("CIMNUM", "99999999")
            mkSql_h(" INSERT INTO BFCIM ( ")
            mkSql_h(" VALUES ( ")
            mkSql_d("'", " INID   ", "IN", 0)                   '//2A    Record Identifier
            mkSql_d("'", " INTRAN ", "CI", 0)               '//2A    Transaction Type
            mkSql_d(" ", " INCTNR ", 0, 0)            '//2A    Transaction Type
            mkSql_d("'", " INMFGR ", 0, 0)            '//2A    Transaction Type
            mkSql_d(" ", " INDATE ", Fecha, 0)                  '//8P0   Date		
            mkSql_d(" ", " INTRN  ", Right(cons, 5), 0)                  '//5P0   INTRN
            mkSql_d("'", " INMLOT  ", "RP" & cons, 0)                '//5P0   INTRN
            mkSql_d("'", " INREAS ", "01", 0)                  '//5P0   INTRN
            mkSql_d(" ", " INREF  ", rs("CORDEN").Value, 0)                  '//8P0   Date
            mkSql_d("'", " INPROD ", rs("CPRODPP").Value, 0)               '//15A   Item Number
            mkSql_d("'", " INWHSE ", rs1("ICPLN").Value, 0)                 '//2A    Warehouse
            mkSql_d("'", " INLOT  ", "", 0)                   '//10A   Lot Number
            mkSql_d("'", " INLOC  ", "", 0)              '//6A    Location Code

            canti1 = CDbl(rs("CQREQPP").Value)
            mkSql_d("'", " INQTY  ", canti1, 0)                '//10A   Creted by
            mkSql_d("'", " INCBY  ", "COSIS001", 0)                '//10A   Creted by
            mkSql_d(" ", " INCDT  ", DB2_DATEDEC(), 0)      '//8P0   Date Created
            mkSql_d(" ", " INCTM  ", DB2_TIMEDEC(), 1)      '//6P0   Time Created
            Try
                DB.Execute(fSql & vSql)

                vSql = ""

                fSql = " CALL CP61BPCUSR/BCTRAINVB "
                DB.Execute(fSql)

                Sql = "UPDATE bfrepace SET CENVIADO =1 WHERE WRKID = " & rs("WRKID").Value
                DBMySql.Execute(Sql)

                Sql = "UPDATE BFREP SET TQREQACEB = TQREQACEB + " & canti & " WHERE TNUMREP = " & rs("TNUMREP").Value
                DBMySql.Execute(Sql)

                Sql = "UPDATE BFREP SET TRPODACEB = TRPODACEB + " & canti1 & " WHERE TNUMREP = " & rs("TNUMREP").Value
                DBMySql.Execute(Sql)

                rs.Close()
                rs1.Close()
                rs.MoveNext()

            Catch E As Exception
                wrtLog("TransaccionAcero", "COSIS001", "ServicioAutomatico", E.Message, fSql & vSql)
                Exit Sub
            End Try
        End While
        Try
            fSql = " CALL CP61BPCUSR/BCTRAINVB "
            DB.Execute(fSql)
        Catch E As Exception
            wrtLog("TransaccionAcero", "COSIS001", "ServicioAutomatico", E.Message, fSql)
            Exit Sub
        End Try
    End Sub
End Class
