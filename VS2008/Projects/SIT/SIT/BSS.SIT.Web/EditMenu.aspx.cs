﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using BSS.SIT.Common;
using BSS.SIT.Data.Model;
using BSS.SIT.Web.Code;

namespace BSS.SIT.Web
{
    /// <summary>
    /// Clase BSS.SIT.Web.EditMenu
    /// </summary>
    public partial class EditMenu : System.Web.UI.Page, IPage
    {
        #region Propiedades
        /// <summary>
        /// Gets the id.
        /// </summary>
        /// <value>The id.</value>
        public int Id
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["Id"]))
                    return Assign.ToInt(Request["Id"]);
                return 0;
            }
        }

        /// <summary>
        /// Gets or sets the aplicacion.
        /// </summary>
        /// <value>The aplicacion.</value>
        public Aplicacion Aplicacion
        {
            get { return (Aplicacion)Session["AplicacionSIT"]; }
            set { Session["AplicacionSIT"] = value; }
        }
        #endregion

        #region Eventos
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    LoadUI();
                }
                catch (Exception ex)
                {
                    Master.Message = Utility.ShowError(this, ex);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the uiGuardar control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void uiGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Process();
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }
        #endregion

        #region IPage Members
        /// <summary>
        /// Loads the data.
        /// </summary>
        public void LoadData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the UI.
        /// </summary>
        public void LoadUI()
        {
            //Carga los combos
            using (SitDb db = new SitDb())
            {
                var listaMenus = db.Menus.ToList();
                uiPadre.DataSource = listaMenus;
                uiPadre.DataValueField = "Id";
                uiPadre.DataTextField = "Descripcion";
                uiPadre.DataBind();
                uiPadre.Items.Insert(0, new ListItem("[Seleccione]", string.Empty));
            }

            uiPosicion.Attributes.Add("onkeypress", "return isNumeric(event);");

            if (this.Id != 0)
                LoadObject();
        }

        /// <summary>
        /// Loads the object.
        /// </summary>
        public void LoadObject()
        {
            using (SitDb db = new SitDb())
            {
                var item = db.Menus.SingleOrDefault(x => x.Id == this.Id);
                if (item == null)
                    throw new ApplicationException("Registro no encontrado.");

                uiDescripcion.Text = item.Descripcion;
                uiPadre.SelectedValue = Assign.ToStringCombo(item.PadreId.GetValueOrDefault());
                uiPosicion.Text = Assign.ToString(item.Posicion.GetValueOrDefault());
                uiUrl.Text = item.Url;
            }
        }

        /// <summary>
        /// Loads the element.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="index">The index.</param>
        public void LoadElement<T>(T index)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Processes this instance.
        /// </summary>
        public void Process()
        {
            if (this.Id == 0)
            {
                if (ValidateForm())
                {
                    //Inserta la informacion
                    var item = new BSS.SIT.Data.Model.Menu();

                    item.Descripcion = uiDescripcion.Text;
                    item.PadreId = Assign.ToInt(uiPadre.SelectedValue);
                    item.Posicion = Assign.ToInt(uiPosicion.Text);
                    item.Url = uiUrl.Text;
                    item.FechaActualizacion = DateTime.Now;
                    item.UsuarioIdActualizacion = Aplicacion.Usuario.Id;

                    using (SitDb db = new SitDb())
                    {
                        db.Menus.InsertOnSubmit(item);
                        db.SubmitChanges();
                    }
                    Master.Message = "Registro creado exitosamente.";
                }
            }
            else
            {
                //Actualiza la informacion
                using (SitDb db = new SitDb())
                {
                    var item = db.Menus.SingleOrDefault(x => x.Id == this.Id);
                    item.Descripcion = uiDescripcion.Text;
                    item.PadreId = Assign.ToInt(uiPadre.SelectedValue);
                    item.Posicion = Assign.ToInt(uiPosicion.Text);
                    item.Url = uiUrl.Text;
                    item.FechaActualizacion = DateTime.Now;
                    item.UsuarioIdActualizacion = Aplicacion.Usuario.Id;
                    db.SubmitChanges();
                }
                Master.Message = "Registro editado exitosamente.";
            }
        }

        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns></returns>
        public bool ValidateForm()
        {
            //Valida si un Menu y un book ya existen
            using (SitDb db = new SitDb())
            {
                int conteo = db.Menus.Count(x => x.Descripcion == uiDescripcion.Text);
                //si no existe pasa bien
                if (conteo != 0)
                {
                    Master.Message = "El nombre de Menu ya existe.";
                    return false;
                }
            }
            return true;
        }
        #endregion
    }
}