﻿using System;
using System.Linq;
using BSS.SIT.Common;
using BSS.SIT.Data.Model;
using System.Collections.Generic;
using System.Data;

namespace BSS.SIT.Web
{
    /// <summary>
    /// 
    /// </summary>
    public partial class ViewTercero : System.Web.UI.Page
    {
        #region Eventos
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Handles the Click event of the uiBuscar control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void uiBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                using (SitDb db = new SitDb())
                {
                    var result = db.ConsultarTercero(uiCodigo.Text, uiCodigoFiscal.Text, uiRazonSocial.Text, uiEnError.Checked).ToList();
                    if (result.Count > 0)
                    {
                        DataSet dsResultado = Utility.ConvertToDataSet<ConsultarTerceroResult>(result);
                        dsResultado.Tables[0].TableName = "DsTercero";
                        //this.uiControlReporte.Exportar(Utility.Exportar.EXCEL, dsResultado, "Reportes\\ReporteTercero.rdlc", "Terceros");
                        this.uiControlReporte.ExportarExcel(dsResultado, "Terceros");
                    }
                    else
                    {
                        Master.Message = "No se encontraron resultados";
                    }

                }
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }
        #endregion
    }
}