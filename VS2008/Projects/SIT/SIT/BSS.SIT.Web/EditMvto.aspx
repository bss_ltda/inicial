﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="EditMvto.aspx.cs" Inherits="BSS.SIT.Web.EditMvto" %>

<%@ MasterType VirtualPath="~/MainSite.Master" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <a id="TemplateInfo"></a>
    <h1>
        Editar Movimiento</h1>
    <asp:UpdatePanel ID="updatePanelForm" runat="server">
        <ContentTemplate>
            <p>
                <label>
                    LedgerBook</label>
                <asp:DropDownList ID="uiLedgerBook" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiLedgerBook" runat="server" ErrorMessage="*" ControlToValidate="uiLedgerBook" Display="Dynamic" />
                <label>
                    Comprobante Numero</label>
                <asp:TextBox ID="uiComprobanteNumero" runat="server" />
                <asp:RequiredFieldValidator ID="Req_uiComprobanteNumero" runat="server" ErrorMessage="*" ControlToValidate="uiComprobanteNumero" Display="Dynamic" />
                <label>
                    Comprobante Linea</label>
                <asp:TextBox ID="uiComprobanteLinea" runat="server" />
                <asp:RequiredFieldValidator ID="Req_uiComprobanteLinea" runat="server" ErrorMessage="*" ControlToValidate="uiComprobanteLinea" Display="Dynamic" />
                <label>
                    Periodo</label>
                <asp:DropDownList ID="uiPeriodo" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="1" Text="1"></asp:ListItem>
                    <asp:ListItem Value="2" Text="2"></asp:ListItem>
                    <asp:ListItem Value="3" Text="3"></asp:ListItem>
                    <asp:ListItem Value="4" Text="4"></asp:ListItem>
                    <asp:ListItem Value="5" Text="5"></asp:ListItem>
                    <asp:ListItem Value="6" Text="6"></asp:ListItem>
                    <asp:ListItem Value="7" Text="7"></asp:ListItem>
                    <asp:ListItem Value="8" Text="8"></asp:ListItem>
                    <asp:ListItem Value="9" Text="9"></asp:ListItem>
                    <asp:ListItem Value="10" Text="10"></asp:ListItem>
                    <asp:ListItem Value="11" Text="11"></asp:ListItem>
                    <asp:ListItem Value="12" Text="12"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiPeriodo" runat="server" ErrorMessage="*" ControlToValidate="uiPeriodo" Display="Dynamic" />
                <br />
                <br />
                <asp:Button ID="uiBuscar" runat="server" Text="Buscar" CssClass="button" OnClick="uiBuscar_Click" />
                <br />
                <br />
                <asp:Panel ID="uiPanel" runat="server" Visible="false">
                    <label>
                        Codigo Impuesto (LHJRF2)</label>
                    <asp:TextBox ID="uiCodigoImpuesto" runat="server" />
                    <label>
                        Codigo Tercero (LHJRF1)</label>
                    <asp:TextBox ID="uiCodigoTercero" runat="server" />
                    <br />
                    <br />
                    <asp:Button ID="uiActualizar" runat="server" Text="Actualizar" CssClass="button" OnClick="uiProcesar_Click" />
                </asp:Panel>
            </p>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
