﻿using System;
using System.Data;
using Microsoft.Reporting.WebForms;
using BSS.SIT.Common;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Web.UI;

namespace BSS.SIT.Web
{
    /// <summary>
    /// Clase BSS.SIT.Web.Reportero2
    /// </summary>
    public partial class Reportero2 : System.Web.UI.UserControl
    {
        #region Eventos
        /// <summary>
        /// page_ load
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">La instancia de <see cref="System.EventArgs"/> que contiene los datos del evento.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //this.uiReporte.Visible = false;
            }
        }
        #endregion

        #region Armador del reporte
        /// <summary>
        /// cargar reporte pantalla
        /// </summary>
        /// <param name="dsResultado">The ds resultado.</param>
        /// <param name="reporte">The reporte.</param>
        public void CargarReportePantalla(DataSet dsResultado, string reporte)
        {
            CargarReportePantalla(dsResultado, reporte, null);
        }

        /// <summary>
        /// cargar reporte pantalla
        /// </summary>
        /// <param name="dsResultado">The ds resultado.</param>
        /// <param name="reporte">The reporte.</param>
        /// <param name="parametro">The parametro.</param>
        public void CargarReportePantalla(DataSet dsResultado, string reporte, string parametro)
        {
            ReportDataSource dataSource = new ReportDataSource(dsResultado.Tables[0].TableName, dsResultado.Tables[0]);

            //limpia si tiene alguna datasource y agregamos el vigente
            uiReporte.LocalReport.ReportPath = reporte;
            uiReporte.LocalReport.DataSources.Clear();
            uiReporte.LocalReport.DataSources.Add(dataSource);

            //agrega los parametros
            if (!string.IsNullOrEmpty(parametro))
                uiReporte.LocalReport.SetParameters(new ReportParameter("paramGroup", parametro));

            uiReporte.LocalReport.EnableExternalImages = true;
            uiReporte.LocalReport.Refresh();
        }

        /// <summary>
        /// exportar
        /// </summary>
        /// <param name="exportar">The exportar.</param>
        /// <param name="dsResultado">The ds resultado.</param>
        /// <param name="reporte">The reporte.</param>
        /// <param name="nombreArchivo">The nombre archivo.</param>
        public void Exportar(Utility.Exportar exportar, DataSet dsResultado, string reporte, string nombreArchivo)
        {
            //limpia si tiene alguna datasource y agregamos el vigente
            uiReporte.LocalReport.ReportPath = reporte;
            uiReporte.LocalReport.DataSources.Clear();

            //Agrega las tablas
            ReportDataSource dataSource;
            foreach (DataTable item in dsResultado.Tables)
            {
                dataSource = new ReportDataSource(item.TableName, item);
                uiReporte.LocalReport.DataSources.Add(dataSource);
            }

            uiReporte.LocalReport.EnableExternalImages = true;
            uiReporte.LocalReport.Refresh();

            //Para guardarlo
            string mimeType, encoding, fileNameExtension;
            string[] streams;
            Warning[] warnings;

            byte[] pdfContent = uiReporte.LocalReport.Render(exportar.ToString(), null, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);
            string archivo = string.Format("{0}.{1}", nombreArchivo, fileNameExtension);

            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("content-disposition", "attachment; filename=" + archivo);
            Response.BinaryWrite(pdfContent);
            Response.End();
        }

        /// <summary>
        /// exportar excel
        /// </summary>
        /// <param name="dsResultado">The ds resultado.</param>
        /// <param name="nombreArchivo">The nombre archivo.</param>
        public void ExportarExcel(DataSet dsResultado, string nombreArchivo)
        {
            string separador = "\t";
            DataTable dt = dsResultado.Tables[0];
            string archivo = Path.Combine(Request.PhysicalApplicationPath, string.Format("Reportes\\{0}.xls", nombreArchivo));

            using (StreamWriter sw = new StreamWriter(archivo, false))
            {
                //Columnas
                foreach (DataColumn dc in dt.Columns)
                {
                    sw.Write(dc.ColumnName);
                    sw.Write(separador);
                }
                sw.Write(sw.NewLine);

                //Filas
                foreach (DataRow dr in dt.Rows)
                {
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        sw.Write(dr[i]);
                        sw.Write(separador);
                    }
                    sw.Write(sw.NewLine);
                }
            }

            string url = string.Format("~/Reportes/{0}.xls", nombreArchivo);
            Response.Redirect(url, false);
        }
        #endregion
    }
}