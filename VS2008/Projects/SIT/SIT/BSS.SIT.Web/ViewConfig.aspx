﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="ViewConfig.aspx.cs" Inherits="BSS.SIT.Web.ViewConfig" %>
<%@ MasterType VirtualPath="~/MainSite.Master" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <a id="TemplateInfo"></a>
    <h1>
        Configuraciones</h1>
    <div>
    </div>
    <br />
    <asp:UpdatePanel ID="updatePanelForm" runat="server">
        <ContentTemplate>
            <label>
                Mensaje ICA</label>
            <asp:TextBox ID="uiMensajeICA" runat="server" TextMode="MultiLine" Columns="30" Rows="3" />
            <asp:RequiredFieldValidator ID="Req_uiMensajeICA" runat="server" ErrorMessage="*" ControlToValidate="uiMensajeICA" Display="Dynamic" />
            <br />
            <br />
            <asp:Button ID="uiActualizar" runat="server" Text="Actualizar" onclick="uiActualizar_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
