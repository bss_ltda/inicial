﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="ProcessCuenta.aspx.cs" Inherits="BSS.SIT.Web.ProcessCuenta" %>

<%@ MasterType VirtualPath="~/MainSite.Master" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
    <meta http-equiv="refresh" content="300">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <a id="TemplateInfo"></a>
    <h1>
        Cargar Saldos CxC CxP</h1>
    <asp:UpdatePanel ID="updatePanelForm" runat="server">
        <ContentTemplate>
            <p>
                Antes de correr esta opcion verifique que tiene el archivo RARSALDO y APHSALDO con los saldos de cartera al 31 de diciembre
            </p>
            <p>
                <label>
                    LedgerBook</label>
                <asp:DropDownList ID="uiLedgerBook" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiLedgerBook" runat="server" ErrorMessage="*" ControlToValidate="uiLedgerBook" Display="Dynamic" />
                <br />
                <br />
                <asp:Button ID="uiProcesar" runat="server" Text="Procesar" OnClick="uiProcesar_Click" />
                <br />
                <br />
                <label>
                    Estado:</label>
                <asp:TextBox ID="uiProceso" runat="server" ReadOnly="true" CssClass="readonly" Columns="30" Rows="5" TextMode="MultiLine" />
            </p>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
