﻿using System;
using System.Web;
using BSS.SIT.Web.Code;

namespace BSS.SIT.Web
{
    /// <summary>
    /// Master Page
    /// </summary>
    public partial class MainSite : System.Web.UI.MasterPage, IPage
    {
        #region Propiedades
        /// <summary>
        /// Gets or sets the mensaje.
        /// </summary>
        /// <value>The mensaje.</value>
        public string Message
        {
            get { return uiMessage.Text; }
            set { uiMessage.Text = value; }
        }

        /// <summary>
        /// Gets or sets the aplicacion.
        /// </summary>
        /// <value>The aplicacion.</value>
        public Aplicacion Aplicacion
        {
            get { return (Aplicacion)Session["AplicacionSIT"]; }
            set { Session["AplicacionSIT"] = value; }
        }
        #endregion

        #region Eventos
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            ValidaSesion();
        }
        #endregion

        #region Metodos
        /// <summary>
        /// Validas the sesion.
        /// </summary>
        public void ValidaSesion()
        {
            try
            {
                if (HttpContext.Current.Session["AplicacionSIT"] == null)
                {
                    HttpContext.Current.Response.Redirect("Login.aspx");
                }
                else
                {
                    if (HttpContext.Current.Session != null)
                    {
                        if (HttpContext.Current.Session.IsNewSession)
                        {
                            string cookieSession = HttpContext.Current.Request.Headers["Cookie"];
                            if ((cookieSession != null) && (cookieSession.IndexOf("ASP.NET_SessionId") >= 0))
                            {
                                if (HttpContext.Current.Request.IsAuthenticated)
                                {
                                    System.Web.Security.FormsAuthentication.SignOut();
                                }
                                HttpContext.Current.Response.Redirect("Login.aspx");
                            }
                        }
                    }
                }
            }
            catch
            {
                HttpContext.Current.Response.Redirect("Login.aspx");
            }
        }
        #endregion

        #region IPage Members
        /// <summary>
        /// Loads the data.
        /// </summary>
        public void LoadData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the UI.
        /// </summary>
        public void LoadUI()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the object.
        /// </summary>
        public void LoadObject()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the element.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="index">The index.</param>
        public void LoadElement<T>(T index)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Processes this instance.
        /// </summary>
        public void Process()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns></returns>
        public bool ValidateForm()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}