﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Reportero.aspx.cs" Inherits="BSS.SIT.Web.Reportero" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>S.I.T. Reportes</title>
    <link href="App_Themes/Main/main.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="forma" runat="server">
    <!-- wrap starts here -->
    <div id="wrap">
        <!--header -->
        <div id="header">
            <h1 id="logo-text">
                S.<span class="gray">I</span>.T.</h1>
            <h2 id="slogan">
                Sistema de informacion tributaria</h2>
            <div class="search">
                <img src="images/logoCTN.png" alt="logo" width="55px" height="37px" />
            </div>
        </div>
        <!-- menu -->
        <div id="menu">
            <%--
            <ul>
                <li id="current"><a href="Default.aspx">Inicio</a></li>
                <li><a href="ViewLedgerBook.aspx">Ledger Books</a></li>
                <li><a href="#">Administrador</a></li>
            </ul>
            --%>
        </div>
        <!-- content-wrap starts here -->
        <div id="content-wrap">
            <div>
                <asp:Label ID="uiMessage" runat="server" CssClass="error" />
            </div>
            <div id="main">
                <asp:Label ID="uiMensaje" runat="server" />
                <br />
                <a href="javascript:history.go(-1)">Volver</a>
            </div>
            <!-- content-wrap ends here -->
        </div>
        <!--footer starts here-->
        <div id="footer">
            <p>
                CTN &copy; 2011
            </p>
        </div>
        <!-- wrap ends here -->
    </div>
    <div>
        <rsweb:ReportViewer ID="visor" runat="server" Font-Names="Verdana" Font-Size="8pt" Height="660px" Width="960px" Visible="false">
        </rsweb:ReportViewer>
    </div>
    </form>
</body>
</html>
