﻿using System;
using System.Linq;
using BSS.SIT.Common;
using BSS.SIT.Data.Model;
using BSS.SIT.Web.Code;
using System.Web.UI.WebControls;

namespace BSS.SIT.Web
{
    /// <summary>
    /// Pagina de edicion del usuario
    /// </summary>
    public partial class EditUsuario : System.Web.UI.Page, IPage
    {
        #region Propiedades
        /// <summary>
        /// Gets the id.
        /// </summary>
        /// <value>The id.</value>
        public int Id
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["Id"]))
                    return Assign.ToInt(Request["Id"]);
                return 0;
            }
        }
        #endregion

        #region Eventos
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    LoadUI();
                }
                catch (Exception ex)
                {
                    Master.Message = Utility.ShowError(this, ex);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the uiGuardar control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void uiGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Process();
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }
        #endregion

        #region IPage Members
        /// <summary>
        /// Loads the data.
        /// </summary>
        public void LoadData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the UI.
        /// </summary>
        public void LoadUI()
        {
            using (SitDb db = new SitDb())
            {
                var listaRol = db.Rols.ToList();
                uiRol.DataSource = listaRol;
                uiRol.DataValueField = "Id";
                uiRol.DataTextField = "Descripcion";
                uiRol.DataBind();
                uiRol.Items.Insert(0, new ListItem("[Seleccione]", string.Empty));
            }

            if (this.Id != 0)
                LoadObject();
        }

        /// <summary>
        /// Loads the object.
        /// </summary>
        public void LoadObject()
        {
            using (SitDb db = new SitDb())
            {
                var item = db.Usuarios.SingleOrDefault(x => x.Id == this.Id);
                if (item == null)
                    throw new ApplicationException("Registro no encontrado.");

                uiNombreCompleto.Text = item.NombreCompleto;
                uiNombreUsuario.Text = item.NombreUsuario;
                //uiClave.Text = item.Clave;
                uiClave.Attributes.Add("value", item.Clave);
                uiRol.SelectedValue = Assign.ToString(item.RolId);
                uiActivo.Checked = item.Activo.Value;
            }
        }

        /// <summary>
        /// Loads the element.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="index">The index.</param>
        public void LoadElement<T>(T index)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Processes this instance.
        /// </summary>
        public void Process()
        {
            if (this.Id == 0)
            {
                if (ValidateForm())
                {
                    //Inserta la informacion
                    Usuario item = new Usuario();
                    item.NombreCompleto = uiNombreCompleto.Text;
                    item.NombreUsuario = uiNombreUsuario.Text;
                    item.Clave = uiClave.Text;
                    item.RolId = Assign.ToInt(uiRol.SelectedValue);
                    item.Activo = uiActivo.Checked;

                    using (SitDb db = new SitDb())
                    {
                        db.Usuarios.InsertOnSubmit(item);
                        db.SubmitChanges();
                    }
                    Master.Message = "Registro creado exitosamente.";
                }
            }
            else
            {
                //Actualiza la informacion
                using (SitDb db = new SitDb())
                {
                    var item = db.Usuarios.SingleOrDefault(x => x.Id == this.Id);
                    item.NombreCompleto = uiNombreCompleto.Text;
                    item.NombreUsuario = uiNombreUsuario.Text;
                    item.Clave = uiClave.Text;
                    item.RolId = Assign.ToInt(uiRol.SelectedValue);
                    item.Activo = uiActivo.Checked;
                    db.SubmitChanges();
                }
                Master.Message = "Registro editado exitosamente.";
            }
        }

        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns></returns>
        public bool ValidateForm()
        {
            //Valida si un ledger y un book ya existen
            using (SitDb db = new SitDb())
            {
                int conteo = db.Usuarios.Count(x => x.NombreUsuario == uiNombreUsuario.Text);
                //si no existe pasa bien
                if (conteo != 0)
                {
                    Master.Message = "El nombre de usuario ya existe.";
                    return false;
                }
            }
            return true;
        }
        #endregion
    }
}