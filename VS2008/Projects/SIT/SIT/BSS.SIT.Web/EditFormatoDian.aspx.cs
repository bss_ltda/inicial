﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using BSS.SIT.Common;
using BSS.SIT.Data.Model;

namespace BSS.SIT.Web
{
    /// <summary>
    /// Clase BSS.SIT.Web.EditFormatoDian
    /// </summary>

    public partial class EditFormatoDian : System.Web.UI.Page
    {
        #region Propiedades
        /// <summary>
        /// Gets the id.
        /// </summary>
        /// <value>The id.</value>
        public int Id
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["Id"]))
                    return Assign.ToInt(Request["Id"]);
                return 0;
            }
        }
        #endregion

        #region Eventos
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    LoadUI();
                }
                catch (Exception ex)
                {
                    Master.Message = Utility.ShowError(this, ex);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the uiGuardar control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void uiGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Process();
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }
        #endregion

        #region IPage Members
        /// <summary>
        /// Loads the data.
        /// </summary>
        public void LoadData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the UI.
        /// </summary>
        public void LoadUI()
        {
            //Carga los combos
            using (SitDb db = new SitDb())
            {
                var listaFormatos = db.FormatoDians.Select(x => new { Id = x.CodigoFormato, Desc = string.Format("{0}", x.CodigoFormato) }).ToList();
                uiCodigoFormato.DataSource = listaFormatos;
                uiCodigoFormato.DataValueField = "Id";
                uiCodigoFormato.DataTextField = "Desc";
                uiCodigoFormato.DataBind();
                uiCodigoFormato.Items.Insert(0, new ListItem("[Seleccione]", string.Empty));
            }

            if (this.Id != 0)
            {
                LoadObject();
                Habilitar();
            }
        }

        /// <summary>
        /// Loads the object.
        /// </summary>
        public void LoadObject()
        {
            using (SitDb db = new SitDb())
            {
                var item = db.FormatoDians.SingleOrDefault(x => x.Id == this.Id);
                if (item == null)
                    throw new ApplicationException("Registro no encontrado.");

                uiCodigoFormato.SelectedValue = item.CodigoFormato;
                uiDescripcion.Text = item.Descripcion;
                uiFuente.Text = item.Fuente;
                uiTope.Text = Assign.ToString(item.Tope);
            }
        }

        /// <summary>
        /// Loads the element.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="index">The index.</param>
        public void LoadElement<T>(T index)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Processes this instance.
        /// </summary>
        public void Process()
        {
            if (this.Id == 0)
            {
                if (ValidateForm())
                {
                    //Inserta la informacion
                    FormatoDian item = new FormatoDian();
                    item.CodigoFormato = uiCodigoFormato.SelectedValue;
                    item.Descripcion = uiDescripcion.Text;
                    item.Fuente = uiFuente.Text;
                    item.Tope = Assign.ToDecimal(uiTope.Text);

                    using (SitDb db = new SitDb())
                    {
                        db.FormatoDians.InsertOnSubmit(item);
                        db.SubmitChanges();
                    }
                    Master.Message = "Registro creado exitosamente.";
                }
            }
            else
            {
                //Actualiza la informacion
                using (SitDb db = new SitDb())
                {
                    var item = db.FormatoDians.SingleOrDefault(x => x.Id == this.Id);

                    //item.Formato = uiFormato.SelectedValue;
                    //item.Concepto = uiConcepto.Text;
                    //item.Descripcion = uiDescripcion.Text;
                    item.Tope = Assign.ToDecimal(uiTope.Text);
                    db.SubmitChanges();
                }
                Master.Message = "Registro editado exitosamente.";
            }
        }

        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns></returns>
        public bool ValidateForm()
        {
            return true;
        }
        #endregion

        #region Metodos
        private void Habilitar()
        {
            bool habilitar = false;
            uiCodigoFormato.Enabled = habilitar;
            uiDescripcion.Enabled = habilitar;
            uiFuente.Enabled = habilitar;
        }
        #endregion
    }
}