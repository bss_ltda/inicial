﻿using System;
using System.Linq;
using System.Web.Security;
using BSS.SIT.Common;
using BSS.SIT.Data.Model;
using BSS.SIT.Web.Code;

namespace BSS.SIT.Web
{
    /// <summary>
    /// Pagina de inicio de sesion
    /// </summary>
    public partial class Login : System.Web.UI.Page, IPage
    {
        #region Eventos
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            //    try
            //    {
            //        LoadUI();
            //    }
            //    catch (Exception ex)
            //    {
            //        uiMessage.Text = Utility.ShowError(this, ex);
            //    }
            //}
        }

        /// <summary>
        /// Handles the Click event of the uiIniciar control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void uiIniciar_Click(object sender, EventArgs e)
        {
            try
            {
                Process();
            }
            catch (Exception ex)
            {
                uiMessage.Text = Utility.ShowError(this, ex);
            }
        }
        #endregion

        #region IPage Members
        /// <summary>
        /// Loads the data.
        /// </summary>
        public void LoadData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the UI.
        /// </summary>
        public void LoadUI()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the object.
        /// </summary>
        public void LoadObject()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the element.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="index">The index.</param>
        public void LoadElement<T>(T index)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Processes this instance.
        /// </summary>
        public void Process()
        {
            //Valida que el usuario exista
            using (SitDb db = new SitDb())
            {
                var usuario = db.Usuarios.SingleOrDefault(x => x.NombreUsuario == uiUsuario.Text && x.Clave == uiPassword.Text);
                if (usuario == null)
                {
                    throw new ApplicationException("Usuario o clave incorrectos");
                }

                if(!usuario.Activo.Value)
                    throw new ApplicationException("Usuario inactivo");

                //Crea la sesion
                Aplicacion aplicacion = new Aplicacion();
                aplicacion.Usuario = usuario;
                Session["AplicacionSIT"] = aplicacion;
                
                FormsAuthentication.SetAuthCookie(usuario.NombreUsuario, false);
                Response.Redirect("Default.aspx", false);
            }
        }

        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns></returns>
        public bool ValidateForm()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}