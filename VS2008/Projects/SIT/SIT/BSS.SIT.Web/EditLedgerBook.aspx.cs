﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using BSS.SIT.Common;
using BSS.SIT.Data.Model;
using BSS.SIT.Web.Code;

namespace BSS.SIT.Web
{
    /// <summary>
    /// Clase de creacion y edicion del ledgerbook
    /// </summary>
    public partial class EditLedgerBook : System.Web.UI.Page, IPage
    {
        #region Propiedades
        /// <summary>
        /// Gets the id.
        /// </summary>
        /// <value>The id.</value>
        public int Id
        {
            get {
                if (!string.IsNullOrEmpty(Request["Id"]))
                    return Assign.ToInt(Request["Id"]);
                return 0; 
            }
        }
        #endregion

        #region Eventos
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    LoadUI();
                }
                catch (Exception ex)
                {
                    Master.Message = Utility.ShowError(this, ex);
                }
            }
        }

        /// <summary>
        /// Handles the SelectedIndexChanged event of the uiLedger control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void uiLedger_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadElement(uiLedger.SelectedValue);
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }

        /// <summary>
        /// Handles the Click event of the uiGuardar control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void uiGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Process();
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }
        #endregion

        #region IPage Members
        /// <summary>
        /// Loads the data.
        /// </summary>
        public void LoadData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the UI.
        /// </summary>
        public void LoadUI()
        {
            //Trae los ledger existentes
            using (SitDb db = new SitDb())
            {
                var lista = db.Ledgers.ToList();
                uiLedger.DataSource = lista;
                uiLedger.DataValueField = "LedgerCode";
                uiLedger.DataTextField = "LedgerCode";
                uiLedger.DataBind();
                uiLedger.Items.Insert(0, new ListItem("[Seleccione]", ""));

                //Trae la informaicion de la compania
                var companyInfo = db.Companies.ToList();
                uiCompaniaId.DataSource = companyInfo;
                uiCompaniaId.DataValueField = "Codigo";
                uiCompaniaId.DataTextField = "Nombre";
                uiCompaniaId.DataBind();
                uiCompaniaId.Items.Insert(0, new ListItem("[Seleccione]", ""));
            }

            uiBook.Attributes.Add("onchange", "seleccionarBook(this.value);");
            uiBookId.Attributes.Add("readonly", "readonly");
            uiBookYear.Attributes.Add("readonly", "readonly");

            if (this.Id != 0)
                LoadObject();
        }

        /// <summary>
        /// Loads the object.
        /// </summary>
        public void LoadObject()
        {
            using (SitDb db = new SitDb())
            {
                var item = db.LedgerBooks.SingleOrDefault(x => x.Id == this.Id);
                if (item == null)
                    throw new ApplicationException("Registro no encontrado.");

                uiLedger.SelectedValue = item.Ledger;
                LoadElement(uiLedger.SelectedValue);

                uiChartAccount.SelectedValue = item.ChartAccount;

                uiBook.SelectedValue = string.Format("{0}|{1}", item.Book, Assign.ToString(item.Ano));
                uiBookId.Text = item.Book;
                uiBookYear.Text = Assign.ToString(item.Ano);

                uiCompaniaId.SelectedValue = item.CompaniaId;
                uiCompania.SelectedValue = item.Compania;
                uiCuenta.SelectedValue = item.Cuenta;
                uiCentroCosto.SelectedValue = item.CentroCosto;

                if (item.Tercero != null)
                {
                    uiTercero.SelectedValue = item.Tercero;
                    uiTerceroReferencia.Text = string.Empty;
                    uiReferencia1.Checked = false;
                }
                else
                {
                    uiTercero.SelectedValue = string.Empty;
                    uiTerceroReferencia.Text = item.TerceroReferencia;
                    uiReferencia1.Checked = true;
                }

                uiTasaImpuesto.SelectedValue = item.TasaImpuesto;
            }
        }

        /// <summary>
        /// Loads the element.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="index">The index.</param>
        public void LoadElement<T>(T index)
        {
            uiBookId.Text = string.Empty;
            uiBookYear.Text = string.Empty;

            //Trae la informaicion asociada al ledger
            using (SitDb db = new SitDb())
            {
                //Trae la informacion del ledger account
                var chartAccountInfo = db.LedgerDetails.Where(x => x.DSLDGR == Assign.ToString(index)).Select(x => new Select<string, string> { Code = x.DSCOA, Description = x.DSCOA }).Distinct().ToList();
                uiChartAccount.DataSource = chartAccountInfo;
                uiChartAccount.DataValueField = "Code";
                uiChartAccount.DataTextField = "Description";
                uiChartAccount.DataBind();
                uiChartAccount.Items.Insert(0, new ListItem("[Seleccione]", ""));


                //Trae la informacion de los books
                var bookInfo = db.BookDetails.Where(x => x.TYLDGR == Assign.ToString(index)).Select(x => new Select<string, string> { Code = string.Format("{0}|{1}", x.TYBOOK, x.TYYEAR), Description = string.Format("{0} - {1}", x.TYLDES, x.TYYEAR) }).ToList();
                uiBook.DataSource = bookInfo;
                uiBook.DataValueField = "Code";
                uiBook.DataTextField = "Description";
                uiBook.DataBind();
                uiBook.Items.Insert(0, new ListItem("[Seleccione]", ""));

                //Trae la informacion de los segmentos
                var ledgerInfo = db.LedgerDetails.Where(x => x.DSLDGR == Assign.ToString(index)).ToList();

                var lista = ledgerInfo.Where(x => x.SGCSEG == 0);
                uiCompania.DataSource = lista;
                uiCompania.DataValueField = "CSSEQ";
                uiCompania.DataTextField = "SGLDES";
                uiCompania.DataBind();
                uiCompania.Items.Insert(0, new ListItem("[Seleccione]", ""));

                uiCentroCosto.DataSource = lista;
                uiCentroCosto.DataValueField = "CSSEQ";
                uiCentroCosto.DataTextField = "SGLDES";
                uiCentroCosto.DataBind();
                uiCentroCosto.Items.Insert(0, new ListItem("[Seleccione]", ""));

                uiTercero.DataSource = lista;
                uiTercero.DataValueField = "CSSEQ";
                uiTercero.DataTextField = "SGLDES";
                uiTercero.DataBind();
                uiTercero.Items.Insert(0, new ListItem("[Seleccione]", ""));

                var listaCuenta = ledgerInfo.Where(x => x.SGCSEG == 1);
                uiCuenta.DataSource = listaCuenta;
                uiCuenta.DataValueField = "CSSEQ";
                uiCuenta.DataTextField = "SGLDES";
                uiCuenta.DataBind();
                uiCuenta.Items.Insert(0, new ListItem("[Seleccione]", ""));
            }
        }

        /// <summary>
        /// Processes this instance.
        /// </summary>
        public void Process()
        {
            if (this.Id == 0)
            {
                if (ValidateForm())
                {
                    //Inserta la informacion
                    LedgerBook item = new LedgerBook();
                    item.Ledger = uiLedger.SelectedValue;
                    item.ChartAccount = uiChartAccount.SelectedValue;
                    item.Book = uiBookId.Text;
                    item.Ano = Assign.ToInt(uiBookYear.Text);
                    item.CompaniaId = uiCompaniaId.SelectedValue;
                    item.Compania = uiCompania.SelectedValue;
                    item.Cuenta = uiCuenta.SelectedValue;
                    item.CentroCosto = uiCentroCosto.SelectedValue;

                    //valida si el tercero es una referencia o es una seleccion
                    if (!uiReferencia1.Checked)
                    {
                        item.Tercero = uiTercero.SelectedValue;
                        item.TerceroReferencia = null;
                    }
                    else
                    {
                        item.Tercero = null;
                        item.TerceroReferencia = uiTerceroReferencia.Text;
                    }

                    item.TasaImpuesto = uiTasaImpuesto.SelectedValue;

                    using (SitDb db = new SitDb())
                    {
                        db.LedgerBooks.InsertOnSubmit(item);
                        db.SubmitChanges();
                    }
                    Master.Message = "Registro creado exitosamente.";
                }
            }
            else
            {
                //Actualiza la informacion
                using (SitDb db = new SitDb())
                {
                    var item = db.LedgerBooks.SingleOrDefault(x => x.Id == this.Id);
                    item.Ledger = uiLedger.SelectedValue;
                    item.ChartAccount = uiChartAccount.SelectedValue;
                    item.Book = uiBookId.Text;
                    item.Ano = Assign.ToInt(uiBookYear.Text);
                    item.CompaniaId = uiCompaniaId.SelectedValue;
                    item.Compania = uiCompania.SelectedValue;
                    item.Cuenta = uiCuenta.SelectedValue;
                    item.CentroCosto = uiCentroCosto.SelectedValue;

                    //valida si el tercero es una referencia o es una seleccion
                    if (!uiReferencia1.Checked)
                    {
                        item.Tercero = uiTercero.SelectedValue;
                        item.TerceroReferencia = null;
                    }
                    else
                    {
                        item.Tercero = null;
                        item.TerceroReferencia = uiTerceroReferencia.Text;
                    }

                    item.TasaImpuesto = uiTasaImpuesto.SelectedValue;
                    db.SubmitChanges();
                }
                Master.Message = "Registro editado exitosamente.";

            }
        }

        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns></returns>
        public bool ValidateForm()
        {
            //Valida si un ledger y un book ya existen
            using (SitDb db = new SitDb())
            {
                int conteo = db.LedgerBooks.Count(x => x.Ledger == uiLedger.SelectedValue && x.Book == uiBookId.Text && x.Ano == Assign.ToInt(uiBookYear.Text) && x.CompaniaId == uiCompaniaId.SelectedValue);
                //si no existe pasa bien
                if (conteo != 0)
                {
                    Master.Message = "El Ledger y Book seleccionado para el ano y compania seleccionada ya existen.";
                    return false;
                }
            }
            return true;
        }
        #endregion
    }
}