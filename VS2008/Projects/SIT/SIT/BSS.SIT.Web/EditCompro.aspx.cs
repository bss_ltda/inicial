﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using BSS.SIT.Common;
using BSS.SIT.Data.Model;
using BSS.SIT.Web.Code;

namespace BSS.SIT.Web
{
    /// <summary>
    /// Clase de edicion de comprobantes
    /// </summary>
    public partial class EditCompro : System.Web.UI.Page, IPage
    {
        #region Propiedades
        /// <summary>
        /// Gets the id.
        /// </summary>
        /// <value>The id.</value>
        public int Id
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["Id"]))
                    return Assign.ToInt(Request["Id"]);
                return 0;
            }
        }
        #endregion

        #region Eventos
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    LoadUI();
                }
                catch (Exception ex)
                {
                    Master.Message = Utility.ShowError(this, ex);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the uiGuardar control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void uiGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Process();
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }
        #endregion

        #region IPage Members
        /// <summary>
        /// Loads the data.
        /// </summary>
        public void LoadData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the UI.
        /// </summary>
        public void LoadUI()
        {
            //Carga los combos
            using (SitDb db = new SitDb())
            {
                var listaLedgers = db.LedgerBooks
                    .Select(x => new { Id = x.Id, Desc = string.Format("{0} - {1} - {2} - {3} - {4}", x.Ledger, x.Book, x.ChartAccount, x.Ano, x.CompaniaId) }).ToList();
                uiLedgerBook.DataSource = listaLedgers;
                uiLedgerBook.DataValueField = "Id";
                uiLedgerBook.DataTextField = "Desc";
                uiLedgerBook.DataBind();
                uiLedgerBook.Items.Insert(0, new ListItem("[Seleccione]", string.Empty));

                var listaVouchers = db.Vouchers.OrderBy(x => x.CODIGO).ToList();
                uiCodigo.DataSource = listaVouchers;
                uiCodigo.DataValueField = "Codigo";
                uiCodigo.DataTextField = "Codigo";
                uiCodigo.DataBind();
                uiCodigo.Items.Insert(0, new ListItem("[Seleccione]", string.Empty));
            }

            if (this.Id != 0)
                LoadObject();
        }

        /// <summary>
        /// Loads the object.
        /// </summary>
        public void LoadObject()
        {
            using (SitDb db = new SitDb())
            {
                var item = db.Comprobantes.SingleOrDefault(x => x.Id == this.Id);
                if (item == null)
                    throw new ApplicationException("Registro no encontrado.");

                uiLedgerBook.SelectedValue = Assign.ToString(item.LedgerBookId);
                uiCodigo.SelectedValue = item.Codigo;
            }
        }

        /// <summary>
        /// Loads the element.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="index">The index.</param>
        public void LoadElement<T>(T index)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Processes this instance.
        /// </summary>
        public void Process()
        {
            if (this.Id == 0)
            {
                if (ValidateForm())
                {
                    //Inserta la informacion
                    Comprobante item = new Comprobante();
                    item.LedgerBookId = Assign.ToInt(uiLedgerBook.SelectedValue);
                    item.Codigo = uiCodigo.SelectedValue;
                    
                    using (SitDb db = new SitDb())
                    {
                        db.Comprobantes.InsertOnSubmit(item);
                        db.SubmitChanges();
                    }
                    Master.Message = "Registro creado exitosamente.";
                }
            }
            else
            {
                //Actualiza la informacion
                using (SitDb db = new SitDb())
                {
                    var item = db.Comprobantes.SingleOrDefault(x => x.Id == this.Id);
                    item.LedgerBookId = Assign.ToInt(uiLedgerBook.SelectedValue);
                    item.Codigo = uiCodigo.SelectedValue;
                    db.SubmitChanges();
                }
                Master.Message = "Registro editado exitosamente.";

            }
        }

        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns></returns>
        public bool ValidateForm()
        {
            //Valida si un ledger y un book ya existen
            using (SitDb db = new SitDb())
            {
                int conteo = db.Comprobantes.Count(x => x.Codigo == uiCodigo.SelectedValue);
                //si no existe pasa bien
                if (conteo != 0)
                {
                    Master.Message = "Codigo ya existe.";
                    return false;
                }
            }
            return true;
        }
        #endregion
    }
}