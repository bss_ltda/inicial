﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="EditLedgerAccountMassive.aspx.cs" Inherits="BSS.SIT.Web.EditLedgerAccountMassive" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/MainSite.Master" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <a id="TemplateInfo"></a>
    <h1>
        Asociar cuentas masivo</h1>
    <asp:UpdatePanel ID="updatePanelForm" runat="server">
        <ContentTemplate>
            <p>
                <label>
                    LedgerBook</label>
                <asp:DropDownList ID="uiLedgerBook" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiLedgerBook" runat="server" ErrorMessage="*" ControlToValidate="uiLedgerBook" Display="Dynamic" />
                <label>
                    Cuenta Inicial
                </label>
                <asp:DropDownList ID="uiCuentaInicial" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiCuentaInicial" runat="server" ErrorMessage="*" ControlToValidate="uiCuentaInicial" Display="Dynamic" />
                <label>
                    Cuenta Final
                </label>
                <asp:DropDownList ID="uiCuentaFinal" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiCuentaFinal" runat="server" ErrorMessage="*" ControlToValidate="uiCuentaFinal" Display="Dynamic" />
                <label>
                    Clasificacion</label>
                <asp:DropDownList ID="uiClasificacion" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiClasificacion" runat="server" ErrorMessage="*" ControlToValidate="uiClasificacion" Display="Dynamic" />
                <label>
                    Detalle S.I.T</label>
                <asp:DropDownList ID="uiTif" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="1" Text="Si"></asp:ListItem>
                    <asp:ListItem Value="0" Text="No"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiTif" runat="server" ErrorMessage="*" ControlToValidate="uiTif" Display="Dynamic" />
                <br />
                <br />
                <asp:Button ID="uiGuardar" runat="server" Text="Guardar" CssClass="button" onclick="uiGuardar_Click" />
            </p>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
