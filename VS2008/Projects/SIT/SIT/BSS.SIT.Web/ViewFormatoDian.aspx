﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="ViewFormatoDian.aspx.cs" Inherits="BSS.SIT.Web.ViewFormatoDian" %>
<%@ MasterType VirtualPath="~/MainSite.Master" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <a id="TemplateInfo"></a>
    <h1>
        Formato Dian Topes</h1>
    <div>
        <%--<a id="buttonAdd" href="EditFormatoConcepto.aspx?Id=0">Crear</a>--%>
    </div>
    <br />
    <asp:GridView ID="uiResults" runat="server" CellPadding="4" EnableModelValidation="True" ForeColor="#333333" GridLines="None" EmptyDataText="No se encontraron registros" AutoGenerateColumns="False" CellSpacing="1" DataKeyNames="ID" OnRowCommand="uiResults_RowCommand">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="CodigoFormato" HeaderText="CodigoFormato" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="Fuente" HeaderText="Fuente" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="Tope" HeaderText="Tope" ItemStyle-HorizontalAlign="Center" DataFormatString ="{0:n0}" />
            <asp:ButtonField ButtonType="Link" HeaderText="Editar" ShowHeader="True" Text="<img src='images/brick_edit.png' title='Editar' style='border-width: 0;'>" CommandName="Edit1" ItemStyle-HorizontalAlign="Center" />
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    </asp:GridView>
</asp:Content>
