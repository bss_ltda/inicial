﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="EditVoucher.aspx.cs" Inherits="BSS.SIT.Web.EditVoucher" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/MainSite.Master" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <a id="TemplateInfo"></a>
    <h1>
        Editar Comprobante</h1>
    <asp:UpdatePanel ID="updatePanelForm" runat="server">
        <ContentTemplate>
            <p>
                <label>
                    LedgerBook</label>
                <asp:DropDownList ID="uiLedgerBook" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiLedgerBook" runat="server" ErrorMessage="*" ControlToValidate="uiLedgerBook" Display="Dynamic" />
                <br />
                <br />
                <asp:Button ID="uiGuardar" runat="server" Text="Guardar" CssClass="button" />
            </p>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
