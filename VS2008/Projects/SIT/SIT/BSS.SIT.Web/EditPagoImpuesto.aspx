﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="EditPagoImpuesto.aspx.cs" Inherits="BSS.SIT.Web.EditPagoImpuesto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ MasterType VirtualPath="~/MainSite.Master" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <a id="TemplateInfo"></a>
    <h1>
        Editar Pago Formato</h1>
    <asp:UpdatePanel ID="updatePanelForm" runat="server">
        <ContentTemplate>
            <p>
                <label>
                    LedgerBook</label>
                <asp:DropDownList ID="uiLedgerBook" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiLedgerBook" runat="server" ErrorMessage="*" ControlToValidate="uiLedgerBook" Display="Dynamic" />
                <label>
                    Periodo</label>
                <asp:DropDownList ID="uiBimestre" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="1" Text="Bimestre 1"></asp:ListItem>
                    <asp:ListItem Value="2" Text="Bimestre 2"></asp:ListItem>
                    <asp:ListItem Value="3" Text="Bimestre 3"></asp:ListItem>
                    <asp:ListItem Value="4" Text="Bimestre 4"></asp:ListItem>
                    <asp:ListItem Value="5" Text="Bimestre 5"></asp:ListItem>
                    <asp:ListItem Value="6" Text="Bimestre 6"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiBimestre" runat="server" ErrorMessage="*" ControlToValidate="uiBimestre" Display="Dynamic" />
                <label>
                    Numero de pago</label>
                <asp:TextBox ID="uiNumeroPago" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Req_uiNumeroPago" runat="server" ErrorMessage="*" ControlToValidate="uiNumeroPago" Display="Dynamic" />
                <label>
                    Numero de declaracion</label>
                <asp:TextBox ID="uiNumeroDeclaracion" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Req_uiNumeroDeclaracion" runat="server" ErrorMessage="*" ControlToValidate="uiNumeroDeclaracion" Display="Dynamic" />
                <label>
                    Fecha de declaracion</label>
                <asp:TextBox ID="uiFechaDeclaracion" runat="server" CssClass="readOnlyText"></asp:TextBox>
                <asp:CalendarExtender ID="uiCal_uiFechaDeclaracion" runat="server" TargetControlID="uiFechaDeclaracion" Format="yyyy-MM-dd" />
                <asp:RequiredFieldValidator ID="Req_uiFechaDeclaracion" runat="server" ErrorMessage="*" ControlToValidate="uiFechaDeclaracion" Display="Dynamic" />
                <label>
                    Sticker</label>
                <asp:TextBox ID="uiSticker" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Req_uiSticker" runat="server" ErrorMessage="*" ControlToValidate="uiSticker" Display="Dynamic" />
                <label>
                    Fecha de pago</label>
                <asp:TextBox ID="uiFechaPago" runat="server" CssClass="readOnlyText"></asp:TextBox>
                <asp:CalendarExtender ID="uiCal_uiFechaPago" runat="server" TargetControlID="uiFechaPago" Format="yyyy-MM-dd" />
                <asp:RequiredFieldValidator ID="Req_uiFechaPago" runat="server" ErrorMessage="*" ControlToValidate="uiFechaPago" Display="Dynamic" />
                <br />
                <br />
                <asp:Button ID="uiGuardar" runat="server" Text="Guardar" CssClass="button" OnClick="uiGuardar_Click" />
            </p>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
