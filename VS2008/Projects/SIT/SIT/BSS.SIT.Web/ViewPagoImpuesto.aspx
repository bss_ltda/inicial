﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="ViewPagoImpuesto.aspx.cs" Inherits="BSS.SIT.Web.ViewPagoImpuesto" %>

<%@ MasterType VirtualPath="~/MainSite.Master" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <a id="TemplateInfo"></a>
    <h1>
        Relacion Pago de Impuestos</h1>
    <div>
        <a id="buttonAdd" href="EditPagoImpuesto.aspx?Id=0">Crear</a>
    </div>
    <label>
        LedgerBook</label>
    <asp:DropDownList ID="uiLedgerBook" runat="server" AutoPostBack="True" onselectedindexchanged="uiLedgerBook_SelectedIndexChanged">
        <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
    </asp:DropDownList>
    <asp:RequiredFieldValidator ID="Req_uiLedgerBook" runat="server" ErrorMessage="*" ControlToValidate="uiLedgerBook" Display="Dynamic" />
    <br />
    <asp:GridView ID="uiResults" runat="server" CellPadding="4" EnableModelValidation="True" ForeColor="#333333" GridLines="None" EmptyDataText="No se encontraron registros" AutoGenerateColumns="False" CellSpacing="1" DataKeyNames="ID" OnRowCommand="uiResults_RowCommand">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="Bimestre" HeaderText="Bimestre" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="NumeroPago" HeaderText="N.Pago" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="NumeroDeclaracion" HeaderText="N.Declaracion" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="FechaDeclaracion" HeaderText="F.Declaracion" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:yyyy-MM-dd}" />
            <asp:BoundField DataField="Sticker" HeaderText="Sticker" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="FechaPago" HeaderText="F.Pago" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:yyyy-MM-dd}" />
            <asp:ButtonField ButtonType="Link" HeaderText="Editar" ShowHeader="True" Text="<img src='images/brick_edit.png' title='Editar' style='border-width: 0;'>" CommandName="Edit1" ItemStyle-HorizontalAlign="Center" />
            <asp:ButtonField ButtonType="Link" HeaderText="Eliminar" ShowHeader="True" Text="<img src='images/brick_delete.png' title='Eliminar' style='border-width: 0;' onclick='return confirmDelete();'>" CommandName="Delete1" ItemStyle-HorizontalAlign="Center" />
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    </asp:GridView>
</asp:Content>
