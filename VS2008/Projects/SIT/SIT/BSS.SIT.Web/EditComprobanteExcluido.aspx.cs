﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using BSS.SIT.Common;
using BSS.SIT.Data.Model;

namespace BSS.SIT.Web
{
    /// <summary>
    /// Clase BSS.SIT.Web.EditComprobanteExcluido
    /// </summary>
    public partial class EditComprobanteExcluido : System.Web.UI.Page
    {
        #region Propiedades
        /// <summary>
        /// Gets the id.
        /// </summary>
        /// <value>The id.</value>
        public int Id
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["Id"]))
                    return Assign.ToInt(Request["Id"]);
                return 0;
            }
        }
        #endregion

        #region Eventos
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    LoadUI();
                }
                catch (Exception ex)
                {
                    Master.Message = Utility.ShowError(this, ex);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the uiGuardar control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void uiGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Process();
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }
        /// <summary>
        /// UI formato_ selected index changed
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">La instancia de <see cref="System.EventArgs"/> que contiene los datos del evento.</param>
        protected void uiFormato_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadConceptos();
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }
        
        #endregion

        #region IPage Members
        /// <summary>
        /// Loads the data.
        /// </summary>
        public void LoadData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the UI.
        /// </summary>
        public void LoadUI()
        {
            //Carga los combos
            using (SitDb db = new SitDb())
            {
                var ListaFormatos = db.FormatoDians.Select(x => new { Id = x.CodigoFormato, Desc = string.Format("{0}", x.CodigoFormato) }).ToList();
                uiFormato.DataSource = ListaFormatos;
                uiFormato.DataValueField = "Id";
                uiFormato.DataTextField = "Desc";
                uiFormato.DataBind();
                uiFormato.Items.Insert(0, new ListItem("[Seleccione]", string.Empty));

                var listaVouchers = db.Vouchers.OrderBy(x => x.CODIGO).ToList();
                uiComprobante.DataSource = listaVouchers;
                uiComprobante.DataValueField = "Codigo";
                uiComprobante.DataTextField = "Codigo";
                uiComprobante.DataBind();
                uiComprobante.Items.Insert(0, new ListItem("[Seleccione]", string.Empty));
            }

            if (this.Id != 0)
                LoadObject();
        }

        /// <summary>
        /// Loads the object.
        /// </summary>
        public void LoadObject()
        {
            using (SitDb db = new SitDb())
            {
                var item = db.ComprobanteExcluidos.SingleOrDefault(x => x.Id == this.Id);
                if (item == null)
                    throw new ApplicationException("Registro no encontrado.");

                uiFormato.SelectedValue = item.CodigoFormato;
                LoadConceptos();
                uiConcepto.SelectedValue = item.Concepto;
                uiComprobante.SelectedValue = item.Comprobante;
            }
        }

        /// <summary>
        /// Loads the element.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="index">The index.</param>
        public void LoadElement<T>(T index)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Processes this instance.
        /// </summary>
        public void Process()
        {
            if (this.Id == 0)
            {
                if (ValidateForm())
                {
                    //Inserta la informacion
                    ComprobanteExcluido item = new ComprobanteExcluido();
                    item.CodigoFormato = uiFormato.SelectedValue;
                    item.Concepto = uiConcepto.SelectedValue;
                    item.Comprobante = uiComprobante.SelectedValue;
                    
                    using (SitDb db = new SitDb())
                    {
                        db.ComprobanteExcluidos.InsertOnSubmit(item);
                        db.SubmitChanges();
                    }
                    Master.Message = "Registro creado exitosamente.";
                }
            }
            else
            {
                //Actualiza la informacion
                using (SitDb db = new SitDb())
                {
                    var item = db.ComprobanteExcluidos.SingleOrDefault(x => x.Id == this.Id);

                    item.CodigoFormato = uiFormato.SelectedValue;
                    item.Concepto = uiConcepto.SelectedValue;
                    item.Comprobante = uiComprobante.SelectedValue;
                    db.SubmitChanges();
                }
                Master.Message = "Registro editado exitosamente.";
            }
        }

        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns></returns>
        public bool ValidateForm()
        {
            return true;
        }
        #endregion

        #region Metodos
        private void Habilitar()
        {
            bool habilitar = true;
            uiFormato.Enabled = habilitar;
            uiConcepto.Enabled = habilitar;
            uiComprobante.Enabled = habilitar;
        }
        #endregion

        #region Metodos privados
        
        private void LoadConceptos()
        {
            //Carga los combos
            using (SitDb db = new SitDb())
            {
                var listaConcepto = db.FormatoConceptos.Where(x => x.Formato == uiFormato.SelectedValue).Select(x => new { Id = x.Concepto, Descripcion = string.Format("{0} - {1}", x.Concepto, x.Descripcion) }).ToList();
                uiConcepto.DataSource = listaConcepto;
                uiConcepto.DataValueField = "Id";
                uiConcepto.DataTextField = "Descripcion";
                uiConcepto.DataBind();
                uiConcepto.Items.Insert(0, new ListItem("[Seleccione]", string.Empty));
            }
        }
        #endregion    
    }
}