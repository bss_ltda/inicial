﻿using BSS.SIT.Data.Model;

namespace BSS.SIT.Web.Code
{
    /// <summary>
    /// Clase de aplicacion para la sesion
    /// </summary>
    public class Aplicacion
    {
        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>The message.</value>
        public string Message { get; set; }
        /// <summary>
        /// Gets or sets the usuario.
        /// </summary>
        /// <value>The usuario.</value>
        public Usuario Usuario { get; set; }
    }
}