﻿
namespace BSS.SIT.Web.Code
{
    /// <summary>
    /// Interfaz de paginas
    /// </summary>
    public interface IPage
    {
        /// <summary>
        /// Loads the data.
        /// </summary>
        void LoadData();
        /// <summary>
        /// Loads the UI.
        /// </summary>
        void LoadUI();
        /// <summary>
        /// Loads the object.
        /// </summary>
        void LoadObject();
        /// <summary>
        /// Loads the element.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="index">The index.</param>
        void LoadElement<T>(T index);
        /// <summary>
        /// Processes this instance.
        /// </summary>
        void Process();
        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns></returns>
        bool ValidateForm();
    }
}
