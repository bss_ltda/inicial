﻿
namespace BSS.SIT.Web.Code
{
    /// <summary>
    /// Clase BSS.SIT.Web.Code.ListaCuenta
    /// </summary>
    public class ListaCuenta
    {
        /// <summary>
        /// Obtiene o establece id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Obtiene o establece cuenta
        /// </summary>
        public string Cuenta { get; set; }
        /// <summary>
        /// Obtiene o establece descripcion
        /// </summary>
        public string Descripcion { get; set; }
        /// <summary>
        /// Obtiene o establece tipo
        /// </summary>
        public string Tipo { get; set; }
        /// <summary>
        /// Obtiene o establece valor
        /// </summary>
        public int Valor { get; set; }
    }
}