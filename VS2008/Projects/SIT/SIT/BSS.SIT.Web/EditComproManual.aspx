﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="EditComproManual.aspx.cs" Inherits="BSS.SIT.Web.EditComproManual" %>
<%@ MasterType VirtualPath="~/MainSite.Master" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <a id="TemplateInfo"></a>
    <h1>
        Editar Comprobante Manual</h1>
    <asp:UpdatePanel ID="updatePanelForm" runat="server">
        <ContentTemplate>
            <p>
                <label>
                    LedgerBook</label>
                <asp:DropDownList ID="uiLedgerBook" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiLedgerBook" runat="server" ErrorMessage="*" ControlToValidate="uiLedgerBook" Display="Dynamic" />
                <label>Codigo</label>
                <asp:DropDownList ID="uiCodigo" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiCodigo" runat="server" ErrorMessage="*" ControlToValidate="uiCodigo" Display="Dynamic" />
                <label>Valida Contra Proveedor / Cliente</label>
                <asp:DropDownList ID="uiValidaPC" runat="server">
                    <asp:ListItem Value="" Text="[Ninguno]" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="C" Text="C" />
                    <asp:ListItem Value="P" Text="P" />
                </asp:DropDownList>
                <br />
                <br />
                <asp:Button ID="uiGuardar" runat="server" Text="Guardar" CssClass="button" OnClick="uiGuardar_Click" />
            </p>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
