﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="BSS.SIT.Web.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//ES" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es-co" lang="es-co">
<head runat="server">
    <title>S.I.T. Sistema de informacion tributaria</title>
    <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
    <meta name="description" content="Sistema de informacion tributaria" />
    <meta name="keywords" content="SIT" />
    <link href="App_Themes/Main/main.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="forma" runat="server">
    <!-- wrap starts here -->
    <div id="wrap">
        <!--header -->
        <div id="header">
            <h1 id="logo-text">
                S.<span class="gray">I</span>.T.</h1>
            <h2 id="slogan">
                Sistema de informacion tributaria</h2>
            <div class="search">
                <img src="images/logoCTN.png" alt="logo" width="55px" height="37px" />
            </div>
        </div>
        <!-- menu -->
        <div id="menu">
            <%--
            <ul>
                <li id="current"><a href="Default.aspx">Inicio</a></li>
                <li><a href="ViewLedgerBook.aspx">Ledger Books</a></li>
                <li><a href="#">Administrador</a></li>
            </ul>
            --%>
        </div>
        <!-- content-wrap starts here -->
        <div id="content-wrap">
            <div>
                <asp:Label ID="uiMessage" runat="server" CssClass="error" />
            </div>
            <div id="main">
                <label>
                    Usuario:</label>
                <asp:TextBox ID="uiUsuario" runat="server" />
                <asp:RequiredFieldValidator ID="Req_uiUsuario" runat="server" ErrorMessage="*" ControlToValidate="uiUsuario" />
                <label>
                    Password:</label>
                <asp:TextBox ID="uiPassword" runat="server" TextMode="Password" />
                <asp:RequiredFieldValidator ID="Req_uiPassword" runat="server" ErrorMessage="*" ControlToValidate="uiPassword" />
                <br />
                <br />
                <asp:Button ID="uiIniciar" runat="server" Text="Iniciar" onclick="uiIniciar_Click" />
            </div>
            <!-- content-wrap ends here -->
        </div>
        <!--footer starts here-->
        <div id="footer">
            <p>
                CTN &copy; 2011
            </p>
        </div>
        <!-- wrap ends here -->
    </div>
    <div id="primateBusyBox" style="display: none;">
        <table>
            <tr>
                <td rowspan="2">
                    <img src="images/loading.gif" alt="cargando" style="border-width: 0;" />
                </td>
                <td>
                    <h2>
                        Por favor espere</h2>
                </td>
            </tr>
            <tr>
                <td>
                    <h4>
                        Estamos procesando la informacion</h4>
                </td>
            </tr>
        </table>
        <%--Creada por: Primate Developers www.primate.co--%>
    </div>
    </form>
</body>
</html>
