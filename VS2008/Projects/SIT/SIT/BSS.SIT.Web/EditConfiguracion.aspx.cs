﻿using System;
using System.Linq;
using BSS.SIT.Common;
using BSS.SIT.Data.Model;
using BSS.SIT.Web.Code;

namespace BSS.SIT.Web
{
    /// <summary>
    /// Pagina de configuraciones del sistema
    /// </summary>
    public partial class EditConfiguracion : System.Web.UI.Page, IPage
    {
        #region Propiedades
        #endregion

        #region Eventos
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    LoadUI();
                }
                catch (Exception ex)
                {
                    Master.Message = Utility.ShowError(this, ex);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the uiGuardar control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void uiGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Process();
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }
        #endregion

        #region IPage Members
        /// <summary>
        /// Loads the data.
        /// </summary>
        public void LoadData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the UI.
        /// </summary>
        public void LoadUI()
        {
            //trae la informacion de las configuraciones
            using (SitDb db = new SitDb())
            {
                //trae el campo de terceros PrincipalTercero
                var campoTercero = db.Configuracions.SingleOrDefault(x => x.Llave == Utility.Configuracion.PRINCIPALCLIENTE.ToString());
                if (campoTercero != null)
                    uiCampoPrincipalTercero.SelectedValue = campoTercero.Valor;

                //trae el campo de proveedores PrincipalProveedor
                var campoProveedor = db.Configuracions.SingleOrDefault(x => x.Llave == Utility.Configuracion.PRINCIPALPROVEEDOR.ToString());
                if (campoProveedor != null)
                    uiCampoPrincipalProveedor.SelectedValue = campoProveedor.Valor;
            }
        }

        /// <summary>
        /// Loads the object.
        /// </summary>
        public void LoadObject()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the element.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="index">The index.</param>
        public void LoadElement<T>(T index)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Processes this instance.
        /// </summary>
        public void Process()
        {
            //trae la informacion de las configuraciones
            using (SitDb db = new SitDb())
            {
                //trae el campo de terceros PrincipalTercero
                db.ActualizarConfiguracion(Utility.Configuracion.PRINCIPALCLIENTE.ToString(), uiCampoPrincipalTercero.SelectedValue);

                //trae el campo de proveedores PrincipalProveedor
                db.ActualizarConfiguracion(Utility.Configuracion.PRINCIPALPROVEEDOR.ToString(), uiCampoPrincipalProveedor.SelectedValue);

                Master.Message = "Registros actualizados exitosamente.";
            }
        }

        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns></returns>
        public bool ValidateForm()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}