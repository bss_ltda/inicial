﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="ViewMvto.aspx.cs" Inherits="BSS.SIT.Web.ViewMvto" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ MasterType VirtualPath="~/MainSite.Master" %>
<%@ Register src="Reportero2.ascx" tagname="Reportero2" tagprefix="uc1" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <a id="TemplateInfo"></a>
    <h1>
        Movimiento</h1>
    <label>
        LedgerBook</label>
    <asp:DropDownList ID="uiLedgerBook" runat="server">
        <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
    </asp:DropDownList>
    <asp:RequiredFieldValidator ID="Req_uiLedgerBook" runat="server" ErrorMessage="*" ControlToValidate="uiLedgerBook" Display="Dynamic" />
    <label>
        Periodo</label>
    <asp:DropDownList ID="uiPeriodo" runat="server">
        <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
        <asp:ListItem Value="1" Text="1"></asp:ListItem>
        <asp:ListItem Value="2" Text="2"></asp:ListItem>
        <asp:ListItem Value="3" Text="3"></asp:ListItem>
        <asp:ListItem Value="4" Text="4"></asp:ListItem>
        <asp:ListItem Value="5" Text="5"></asp:ListItem>
        <asp:ListItem Value="6" Text="6"></asp:ListItem>
        <asp:ListItem Value="7" Text="7"></asp:ListItem>
        <asp:ListItem Value="8" Text="8"></asp:ListItem>
        <asp:ListItem Value="9" Text="9"></asp:ListItem>
        <asp:ListItem Value="10" Text="10"></asp:ListItem>
        <asp:ListItem Value="11" Text="11"></asp:ListItem>
        <asp:ListItem Value="12" Text="12"></asp:ListItem>
    </asp:DropDownList>
    <asp:RequiredFieldValidator ID="Req_uiPeriodo" runat="server" ErrorMessage="*" ControlToValidate="uiPeriodo" Display="Dynamic" />
    <label>
        Traer Errores
    </label>
    <asp:CheckBox ID="uiEnError" runat="server" />
    <br />
    <br />
    <asp:Button ID="uiBuscar" runat="server" Text="Buscar" OnClick="uiBuscar_Click" />
    <br />
    <uc1:Reportero2 ID="uiControlReporte" runat="server" Visible="false" />
    <br />
</asp:Content>
