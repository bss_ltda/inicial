﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="ExportarFormato.aspx.cs" Inherits="BSS.SIT.Web.ExportarFormato" %>
<%@ MasterType VirtualPath="~/MainSite.Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <a id="TemplateInfo"></a>
    <h1>
        Generar formatos</h1>
    <p>
        <label>
            LedgerBook</label>
        <asp:DropDownList ID="uiLedgerBook" runat="server" >
            <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
        </asp:DropDownList>
        <asp:RequiredFieldValidator ID="Req_uiLedgerBook" runat="server" ErrorMessage="*" ControlToValidate="uiLedgerBook" Display="Dynamic" />
        <label>
            Formato</label>
        <asp:DropDownList ID="uiFormato" runat="server" Width="300px">
            <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
        </asp:DropDownList>
        <asp:RequiredFieldValidator ID="Req_uiFormato" runat="server" ErrorMessage="*" ControlToValidate="uiFormato" Display="Dynamic" />
        <br />
        <br />
        <asp:Button ID="uiGuardar" runat="server" Text="Guardar" CssClass="button" OnClick="uiGuardar_Click" />
    </p>
</asp:Content>
