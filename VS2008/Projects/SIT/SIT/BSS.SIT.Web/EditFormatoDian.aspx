﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="EditFormatoDian.aspx.cs" Inherits="BSS.SIT.Web.EditFormatoDian" %>
<%@ MasterType VirtualPath="~/MainSite.Master" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <a id="TemplateInfo"></a>
    <h1>
        Editar Concepto</h1>
    <asp:UpdatePanel ID="updatePanelForm" runat="server">
        <ContentTemplate>   
            <p>
                <label>
                    Codigo Formato</label>
                <asp:DropDownList ID="uiCodigoFormato" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiCodigoFormato" runat="server" ErrorMessage="*" ControlToValidate="uiCodigoFormato" Display="Dynamic" />
                <label>Descripcion</label>
                <asp:TextBox ID="uiDescripcion" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Req_uiDescripcion" runat="server" ErrorMessage="*" ControlToValidate="uidescripcion" Display="Dynamic" />
                <label>Fuente</label>
                <asp:TextBox ID="uiFuente" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Req_uiFuente" runat="server" ErrorMessage="*" ControlToValidate="uiFuente" Display="Dynamic" />
                <label>Tope</label>
                <asp:TextBox ID="uiTope" runat="server" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="Req_uiTope" runat="server" ErrorMessage="*" ControlToValidate="uiTope" Display="Dynamic" />
                <br />
                <br />
                <asp:Button ID="uiGuardar" runat="server" Text="Guardar" CssClass="button" OnClick="uiGuardar_Click" />
            </p>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
