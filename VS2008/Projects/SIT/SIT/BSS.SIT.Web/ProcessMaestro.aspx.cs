﻿using System;
using System.Linq;
using BSS.SIT.Common;
using BSS.SIT.Data.Model;
using BSS.SIT.Web.Code;

namespace BSS.SIT.Web
{
    /// <summary>
    /// Clase BSS.SIT.Web.ProcessMaestro
    /// </summary>
    public partial class ProcessMaestro : System.Web.UI.Page, IPage
    {
        #region Eventos
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    LoadUI();
                }
                catch (Exception ex)
                {
                    Master.Message = Utility.ShowError(this, ex);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the uiProcesar control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void uiProcesar_Click(object sender, EventArgs e)
        {
            try
            {
                Process();
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }
        #endregion

        #region IPage Members
        /// <summary>
        /// Loads the data.
        /// </summary>
        public void LoadData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the UI.
        /// </summary>
        public void LoadUI()
        {
            LoadObject();
        }

        /// <summary>
        /// Loads the object.
        /// </summary>
        public void LoadObject()
        {
            using (SitDb db = new SitDb())
            {
                var proceso = db.Configuracions.SingleOrDefault(x => x.Llave == Utility.Configuracion.PROCESOMAESTRO.ToString());
                uiProceso.Text = string.Empty;
                if (proceso != null)
                    uiProceso.Text = proceso.Valor;
            }
        }

        /// <summary>
        /// Loads the element.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="index">The index.</param>
        public void LoadElement<T>(T index)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Processes this instance.
        /// </summary>
        public void Process()
        {
            using (SitDb db = new SitDb())
            {
                db.ActualizarMaestroJob();
            }

            System.Threading.Thread.Sleep(1000);
            LoadObject();
            Master.Message = "Proceso de maestros enviado enviado.";
        }

        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns></returns>
        public bool ValidateForm()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}