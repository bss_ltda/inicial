﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="ProcessOtraCuenta.aspx.cs" Inherits="BSS.SIT.Web.ProcessOtraCuenta" %>

<%@ MasterType VirtualPath="~/MainSite.Master" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <a id="TemplateInfo"></a>
    <h1>
        Cargar Otras Cuentas</h1>
    <asp:UpdatePanel ID="updatePanelForm" runat="server">
        <ContentTemplate>
            <p>
                <label>
                    LedgerBook</label>
                <asp:DropDownList ID="uiLedgerBook" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiLedgerBook" runat="server" ErrorMessage="*" ControlToValidate="uiLedgerBook" Display="Dynamic" />
                <br />
                <br />
                <asp:Button ID="uiProcesar" runat="server" Text="Procesar" OnClick="uiProcesar_Click" />
                <br />
                <br />
                Estado:
                <asp:TextBox ID="uiProceso" runat="server" ReadOnly="true" CssClass="readonly" Columns="30" />
            </p>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
