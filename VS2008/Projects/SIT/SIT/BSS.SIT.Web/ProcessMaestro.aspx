﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="ProcessMaestro.aspx.cs" Inherits="BSS.SIT.Web.ProcessMaestro" %>
<%@ MasterType VirtualPath="~/MainSite.Master" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <a id="TemplateInfo"></a>
    <h1>
        Cargar Maestros</h1>
    <asp:UpdatePanel ID="updatePanelForm" runat="server">
        <ContentTemplate>
            <p>
                <asp:Button ID="uiProcesar" runat="server" Text="Procesar" onclick="uiProcesar_Click" />
                <br />
                <br />
                <label>
                    Estado:</label>
                <asp:TextBox ID="uiProceso" runat="server" ReadOnly="true" CssClass="readonly" Columns="30" Rows="5" TextMode="MultiLine" />
            </p>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
