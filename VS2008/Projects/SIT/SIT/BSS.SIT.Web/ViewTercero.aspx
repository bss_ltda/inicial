﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="ViewTercero.aspx.cs" Inherits="BSS.SIT.Web.ViewTercero" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ MasterType VirtualPath="~/MainSite.Master" %>
<%@ Register src="Reportero2.ascx" tagname="Reportero2" tagprefix="uc1" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <a id="TemplateInfo"></a>
    <h1>
        Terceros</h1>
<%--    <asp:UpdatePanel ID="updatePanelReporte" runat="server">
        <ContentTemplate>
--%>            <label>
                Codigo
            </label>
            <asp:TextBox ID="uiCodigo" runat="server" />
            <label>
                Codigo Fiscal
            </label>
            <asp:TextBox ID="uiCodigoFiscal" runat="server" />
            <label>
                Razon Social
            </label>
            <asp:TextBox ID="uiRazonSocial" runat="server" Columns="50" />
            <label>
                Traer Errores
            </label>
            <asp:CheckBox ID="uiEnError" runat="server" />
            <br />
            <br />
<%--        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="uiBuscar" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
--%>    <asp:Button ID="uiBuscar" runat="server" Text="Buscar" OnClick="uiBuscar_Click" />
    <br />
    <uc1:Reportero2 ID="uiControlReporte" runat="server" Visible="false" />
    <br />
</asp:Content>
