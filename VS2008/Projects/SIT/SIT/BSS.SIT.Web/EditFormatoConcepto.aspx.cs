﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using BSS.SIT.Common;
using BSS.SIT.Data.Model;
using BSS.SIT.Web.Code;

namespace BSS.SIT.Web
{
    /// <summary>
    /// Clase BSS.SIT.Web.EditFormatoConcepto
    /// </summary>
    public partial class EditFormatoConcepto : System.Web.UI.Page, IPage
    {
        #region Propiedades
        /// <summary>
        /// Gets the id.
        /// </summary>
        /// <value>The id.</value>
        public int Id
        {
            get
            {
                if (!string.IsNullOrEmpty(Request["Id"]))
                    return Assign.ToInt(Request["Id"]);
                return 0;
            }
        }
        #endregion

        #region Eventos
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    LoadUI();
                }
                catch (Exception ex)
                {
                    Master.Message = Utility.ShowError(this, ex);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the uiGuardar control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void uiGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Process();
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }
        #endregion

        #region IPage Members
        /// <summary>
        /// Loads the data.
        /// </summary>
        public void LoadData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the UI.
        /// </summary>
        public void LoadUI()
        {
            //Carga los combos
            using (SitDb db = new SitDb())
            {
                var listaFormatos = db.FormatoDians.Select(x => new { Id = x.CodigoFormato, Desc = string.Format("{0}", x.CodigoFormato) }).ToList();
                uiFormato.DataSource = listaFormatos;
                uiFormato.DataValueField = "Id";
                uiFormato.DataTextField = "Desc";
                uiFormato.DataBind();
                uiFormato.Items.Insert(0, new ListItem("[Seleccione]", string.Empty));
            }

            if (this.Id != 0)
            {
                LoadObject();
                Habilitar();
            }
        }

        /// <summary>
        /// Loads the object.
        /// </summary>
        public void LoadObject()
        {
            using (SitDb db = new SitDb())
            {
                var item = db.FormatoConceptos.SingleOrDefault(x => x.Id == this.Id);
                if (item == null)
                    throw new ApplicationException("Registro no encontrado.");

                uiFormato.SelectedValue = item.Formato;
                uiConcepto.Text = item.Concepto;
                uiDescripcion.Text = item.Descripcion;
                uiDeclaracion.SelectedValue = item.Declaracion;
            }
        }

        /// <summary>
        /// Loads the element.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="index">The index.</param>
        public void LoadElement<T>(T index)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Processes this instance.
        /// </summary>
        public void Process()
        {
            if (this.Id == 0)
            {
                if (ValidateForm())
                {
                    //Inserta la informacion
                    FormatoConcepto item = new FormatoConcepto();
                    item.Formato = uiFormato.SelectedValue;
                    item.Concepto = uiConcepto.Text;
                    item.Descripcion = uiDescripcion.Text;
                    item.Declaracion = uiDeclaracion.SelectedValue;

                    using (SitDb db = new SitDb())
                    {
                        db.FormatoConceptos.InsertOnSubmit(item);
                        db.SubmitChanges();
                    }
                    Master.Message = "Registro creado exitosamente.";
                }
            }
            else
            {
                //Actualiza la informacion
                using (SitDb db = new SitDb())
                {
                    var item = db.FormatoConceptos.SingleOrDefault(x => x.Id == this.Id);

                    //item.Formato = uiFormato.SelectedValue;
                    //item.Concepto = uiConcepto.Text;
                    //item.Descripcion = uiDescripcion.Text;
                    item.Declaracion = uiDeclaracion.SelectedValue;
                    db.SubmitChanges();
                }
                Master.Message = "Registro editado exitosamente.";
            }
        }

        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns></returns>
        public bool ValidateForm()
        {
            return true;
        }
        #endregion

        #region Metodos
        private void Habilitar()
        {
            bool habilitar = false;
            uiFormato.Enabled = habilitar;
            uiConcepto.Enabled = habilitar;
            uiDescripcion.Enabled = habilitar;
        }
        #endregion
    }
}