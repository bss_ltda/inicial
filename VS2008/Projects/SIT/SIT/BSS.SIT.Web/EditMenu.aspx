﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="EditMenu.aspx.cs" Inherits="BSS.SIT.Web.EditMenu" %>

<%@ MasterType VirtualPath="~/MainSite.Master" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <a id="TemplateInfo"></a>
    <h1>
        Editar Menu</h1>
    <asp:UpdatePanel ID="updatePanelForm" runat="server">
        <ContentTemplate>
            <p>
                <label>
                    Descripcion</label>
                <asp:TextBox ID="uiDescripcion" runat="server" MaxLength="200" Columns="80" />
                <asp:RequiredFieldValidator ID="Req_Descripcion" runat="server" ErrorMessage="*" ControlToValidate="uiDescripcion" Display="Dynamic" />
                <label>
                    Padre</label>
                <asp:DropDownList ID="uiPadre" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiPadre" runat="server" ErrorMessage="*" ControlToValidate="uiPadre" Display="Dynamic" />
                <label>
                    Posicion</label>
                <asp:TextBox ID="uiPosicion" runat="server" MaxLength="2" Columns="5" />
                <asp:RequiredFieldValidator ID="Req_uiPosicion" runat="server" ErrorMessage="*" ControlToValidate="uiPosicion" Display="Dynamic" />
                <label>
                    Url</label>
                <asp:TextBox ID="uiUrl" runat="server" MaxLength="200" Columns="80" />
                <asp:RequiredFieldValidator ID="Req_Url" runat="server" ErrorMessage="*" ControlToValidate="uiUrl" Display="Dynamic" />
                <br />
                <br />
                <asp:Button ID="uiGuardar" runat="server" Text="Guardar" CssClass="button" OnClick="uiGuardar_Click" />
            </p>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
