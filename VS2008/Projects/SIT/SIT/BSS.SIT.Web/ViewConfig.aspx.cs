﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using BSS.SIT.Common;
using BSS.SIT.Data.Model;
using BSS.SIT.Web.Code;

namespace BSS.SIT.Web
{
    /// <summary>
    /// Clase BSS.SIT.Web.ViewConfig
    /// </summary>
    public partial class ViewConfig : System.Web.UI.Page, IPage
    {
        #region Eventos
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadUI();
            }
        }

        /// <summary>
        /// UI actualizar_ click
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">La instancia de <see cref="System.EventArgs"/> que contiene los datos del evento.</param>
        protected void uiActualizar_Click(object sender, EventArgs e)
        {
            try
            {
                Process();
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }
        #endregion

        #region IPage Members
        /// <summary>
        /// Loads the data.
        /// </summary>
        public void LoadData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the UI.
        /// </summary>
        public void LoadUI()
        {
            using (SitDb db = new SitDb())
            {
                var configuraciones = db.Configuracions.ToList();

                //guarda configuracion del campo ICA
                var config = configuraciones.SingleOrDefault(x => x.Llave == Utility.Configuracion.MENSAJEICA.ToString());
                if (config != null)
                    uiMensajeICA.Text = config.Valor;
            }
        }

        /// <summary>
        /// Loads the object.
        /// </summary>
        public void LoadObject()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the element.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="index">The index.</param>
        public void LoadElement<T>(T index)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Processes this instance.
        /// </summary>
        public void Process()
        {
            using (SitDb db = new SitDb())
            {

                //trae la configuracion del campo ICA
                db.ActualizarConfiguracion(Utility.Configuracion.MENSAJEICA.ToString(), uiMensajeICA.Text);
            }
            Master.Message = "Configuracion actualizada exitosamente";
        }

        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns></returns>
        public bool ValidateForm()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}