﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using BSS.SIT.Common;
using BSS.SIT.Data.Model;
using BSS.SIT.Web.Code;

namespace BSS.SIT.Web
{
    /// <summary>
    /// Clase BSS.SIT.Web.GenerarFormato
    /// </summary>
    public partial class GenerarFormato : System.Web.UI.Page, IPage
    {
        #region Miembros
        #endregion

        #region Eventos
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    LoadUI();
                }
                catch (Exception ex)
                {
                    Master.Message = Utility.ShowError(this, ex);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the uiGuardar control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void uiGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Process();
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }

        /// <summary>
        /// UI ledger book_ selected index changed
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">La instancia de <see cref="System.EventArgs"/> que contiene los datos del evento.</param>
        protected void uiLedgerBook_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadCuentas();
                LoadCuentasAsociadas();
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }

        /// <summary>
        /// UI formato_ selected index changed
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">La instancia de <see cref="System.EventArgs"/> que contiene los datos del evento.</param>
        protected void uiFormato_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadConceptos();
                LoadCuentasAsociadas();
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }

        /// <summary>
        /// UI concepto_ selected index changed
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">La instancia de <see cref="System.EventArgs"/> que contiene los datos del evento.</param>
        protected void uiConcepto_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadCuentasAsociadas();
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }

        }

        /// <summary>
        /// Handles the Click event of the uiEliminarMasivo control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void uiEliminarMasivo_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteMassive();
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }
        #endregion

        #region IPage Members
        /// <summary>
        /// Loads the data.
        /// </summary>
        public void LoadData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the UI.
        /// </summary>
        public void LoadUI()
        {
            //Carga los combos
            using (SitDb db = new SitDb())
            {
                var listaLedgers = db.LedgerBooks
                    .Select(x => new { Id = x.Id, Desc = string.Format("{0} - {1} - {2} - {3} - {4}", x.Ledger, x.Book, x.ChartAccount, x.Ano, x.CompaniaId) }).ToList();
                uiLedgerBook.DataSource = listaLedgers;
                uiLedgerBook.DataValueField = "Id";
                uiLedgerBook.DataTextField = "Desc";
                uiLedgerBook.DataBind();
                uiLedgerBook.Items.Insert(0, new ListItem("[Seleccione]", string.Empty));

                var listaFormato = db.FormatoDians.Select(x => new { Id = x.CodigoFormato, Descripcion = string.Format("{0} - {1}", x.CodigoFormato, x.Descripcion) }).ToList();
                uiFormato.DataSource = listaFormato;
                uiFormato.DataValueField = "Id";
                uiFormato.DataTextField = "Descripcion";
                uiFormato.DataBind();
                uiFormato.Items.Insert(0, new ListItem("[Seleccione]", string.Empty));
            }
        }

        /// <summary>
        /// Loads the object.
        /// </summary>
        public void LoadObject()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the element.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="index">The index.</param>
        public void LoadElement<T>(T index)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Processes this instance.
        /// </summary>
        public void Process()
        {
            //Envia los datos para la inserccion masiva
            using (SitDb db = new SitDb())
            {
                //elimina lo que exista para ledger book
                var listado = db.MedioMagneticoDians.Where(x => x.LedgerBookId == Assign.ToInt(uiLedgerBook.SelectedValue));
                db.MedioMagneticoDians.DeleteAllOnSubmit(listado);

                db.CrearMedioMagnetico(Assign.ToInt(uiLedgerBook.SelectedValue), uiFormato.SelectedValue, uiConcepto.SelectedValue, uiCuentaInicial.SelectedValue, uiCuentaFinal.SelectedValue, uiTipo.SelectedValue, Assign.ToInt(uiCampoValor.SelectedValue));

                Master.Message = "Registro creado Existosamente";

                LoadCuentas();
                LoadCuentasAsociadas();
            }
        }

        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns></returns>
        public bool ValidateForm()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Metodos privados
        private void LoadCuentas()
        {
            //Carga los combos
            using (SitDb db = new SitDb())
            {
                var listaCuenta = db.LedgerAccounts.Where(x => x.LedgerBookId == Assign.ToInt(uiLedgerBook.SelectedValue)).OrderBy(x => x.Cuenta).Select(x => new { Id = x.Cuenta, Descripcion = string.Format("{0} - {1}", x.Cuenta, x.Descripcion) }).ToList();
                
                uiCuentaInicial.DataSource = listaCuenta;
                uiCuentaInicial.DataValueField = "Id";
                uiCuentaInicial.DataTextField = "Descripcion";
                uiCuentaInicial.DataBind();
                uiCuentaInicial.Items.Insert(0, new ListItem("[Seleccione]", string.Empty));

                uiCuentaFinal.DataSource = listaCuenta;
                uiCuentaFinal.DataValueField = "Id";
                uiCuentaFinal.DataTextField = "Descripcion";
                uiCuentaFinal.DataBind();
                uiCuentaFinal.Items.Insert(0, new ListItem("[Seleccione]", string.Empty));
            }
        }

        private void LoadConceptos()
        {
            //Carga los combos
            using (SitDb db = new SitDb())
            {
                var listaConcepto = db.FormatoConceptos.Where(x => x.Formato == uiFormato.SelectedValue).Select(x => new { Id = x.Concepto, Descripcion = string.Format("{0} - {1}", x.Concepto, x.Descripcion) }).ToList();

                uiConcepto.DataSource = listaConcepto;
                uiConcepto.DataValueField = "Id";
                uiConcepto.DataTextField = "Descripcion";
                uiConcepto.DataBind();
                uiConcepto.Items.Insert(0, new ListItem("[Seleccione]", string.Empty));
            }
        }

        private void LoadCuentasAsociadas()
        {
            using (SitDb db = new SitDb())
            {
                List<LedgerAccount> cuentas = db.LedgerAccounts.ToList();

                var result = db.MedioMagneticoDians.Where(x => x.LedgerBookId == Assign.ToInt(uiLedgerBook.SelectedValue) && x.Formato == uiFormato.SelectedValue && x.Concepto == uiConcepto.SelectedValue);
                var listaResult = result.OrderBy(x => x.Cuentas).ToList();

                var lista = listaResult.Select(x => new ListaCuenta
                {
                    Id = x.Id,
                    Cuenta = x.Cuentas,
                    Descripcion = cuentas.FirstOrDefault(z => z.Cuenta == x.Cuentas).Descripcion,
                    Tipo = x.Tipo,
                    Valor = x.Valor.Value
                });

                uiResults.DataSource = lista;
                uiResults.DataBind();
            }
        }

        private void DeleteMassive()
        {
            using (SitDb db = new SitDb())
            {

                foreach (GridViewRow row in uiResults.Rows)
                {
                    bool isChecked = ((HtmlInputCheckBox)row.FindControl("uiSeleccion")).Checked;
                    if (isChecked)
                    {
                        int id = Assign.ToInt(uiResults.DataKeys[row.RowIndex].Value);
                        var item = db.MedioMagneticoDians.SingleOrDefault(x => x.Id == id);
                        db.MedioMagneticoDians.DeleteOnSubmit(item);
                    }
                }
                db.SubmitChanges();
            }
            LoadCuentas();
            LoadCuentasAsociadas();
            Master.Message = "Registros eliminados exitosamente";
        }
        #endregion
    }
}