﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Reportero2.ascx.cs" Inherits="BSS.SIT.Web.Reportero2" %>
<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>
<div>
    <rsweb:ReportViewer 
        ID="uiReporte" 
        runat="server" 
        Width="700px" 
        Height="600px" 
        ShowCredentialPrompts="False" 
        ShowDocumentMapButton="False" 
        ShowParameterPrompts="False" 
        ShowPromptAreaButton="False" 
        ShowRefreshButton="False"
        InteractivityPostBackMode="AlwaysAsynchronous"
        >
    </rsweb:ReportViewer>
</div>