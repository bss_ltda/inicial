﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using BSS.SIT.Common;
using BSS.SIT.Data.Model;
using BSS.SIT.Web.Code;
using System.Text;

namespace BSS.SIT.Web
{
    /// <summary>
    /// Clase BSS.SIT.Web.EditMvto
    /// </summary>
    public partial class EditMvto : System.Web.UI.Page, IPage
    {
        #region Eventos
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    LoadUI();
                }
                catch (Exception ex)
                {
                    Master.Message = Utility.ShowError(this, ex);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the uiProcesar control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void uiProcesar_Click(object sender, EventArgs e)
        {
            try
            {
                Process();
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
            finally
            {
                uiPanel.Visible = false;
            }
        }

        /// <summary>
        /// UI buscar_ click
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">La instancia de <see cref="System.EventArgs"/> que contiene los datos del evento.</param>
        protected void uiBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Buscar();
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }
        #endregion

        #region IPage Members
        /// <summary>
        /// Loads the data.
        /// </summary>
        public void LoadData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the UI.
        /// </summary>
        public void LoadUI()
        {
            //Carga los combos
            using (SitDb db = new SitDb())
            {
                var listaLedgers = db.LedgerBooks
                    .Select(x => new { Id = x.Id, Desc = string.Format("{0} - {1} - {2} - {3} - {4}", x.Ledger, x.Book, x.ChartAccount, x.Ano, x.CompaniaId) }).ToList();
                uiLedgerBook.DataSource = listaLedgers;
                uiLedgerBook.DataValueField = "Id";
                uiLedgerBook.DataTextField = "Desc";
                uiLedgerBook.DataBind();
                uiLedgerBook.Items.Insert(0, new ListItem("[Seleccione]", string.Empty));
            }
        }

        /// <summary>
        /// Loads the object.
        /// </summary>
        public void LoadObject()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the element.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="index">The index.</param>
        public void LoadElement<T>(T index)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Processes this instance.
        /// </summary>
        public void Process()
        {
            //Guarda la informacion armando el query
            LedgerBook item = null;
            using (SitDb db = new SitDb())
            {
                item = db.LedgerBooks.SingleOrDefault(x => x.Id == Assign.ToInt(uiLedgerBook.SelectedValue));
            }
            if (item != null)
            {
                StringBuilder query = new StringBuilder();
                query.Append("UPDATE GLH SET ");
                query.AppendFormat("{0} = '{1}', ", item.TasaImpuesto, uiCodigoImpuesto.Text);
                query.AppendFormat("{0} = '{1}' ", item.TerceroReferencia, uiCodigoTercero.Text);
                query.Append("WHERE ");
                query.AppendFormat("(LHLDGR = '{0}') AND ", item.Ledger);
                query.AppendFormat("(LHBOOK = '{0}') AND ", item.Book);
                query.AppendFormat("(LHYEAR = '{0}') AND ", item.Ano);
                query.AppendFormat("(LHPERD = '{0}') AND ", uiPeriodo.SelectedValue);
                query.AppendFormat("(LHJNLN = '{0}') AND ", uiComprobanteLinea.Text);
                query.AppendFormat("(LHJNEN = '{0}') ", uiComprobanteNumero.Text);

                Data.DB2.ProcesarDB2 procesar = new Data.DB2.ProcesarDB2();
                procesar.Actualizar(query.ToString());
                Master.Message = "Item actualizado";
            }
            else
            {
                Master.Message = "Item no encontrado";
            }
        }

        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns></returns>
        public bool ValidateForm()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Metodos Privados
        private void Buscar()
        {
            using (SitDb db = new SitDb())
            {
                var item = db.ErrorMvtos.FirstOrDefault(x =>
                    x.LedgerBookId == Assign.ToInt(uiLedgerBook.SelectedValue) &&
                    x.ComprobanteLinea == Assign.ToInt(uiComprobanteLinea.Text) &&
                    x.ComprobanteNumero == uiComprobanteNumero.Text &&
                    x.Periodo == Assign.ToInt(uiPeriodo.SelectedValue)
                    );

                if (item != null)
                {
                    uiCodigoImpuesto.Text = item.TasaImpuesto;
                    uiCodigoTercero.Text = item.SegmentoTercero;

                    uiPanel.Visible = true;
                }
                else
                {
                    Master.Message = "Item no encontrado";
                    uiPanel.Visible = false;
                }
            }
        }
        #endregion
    }
}