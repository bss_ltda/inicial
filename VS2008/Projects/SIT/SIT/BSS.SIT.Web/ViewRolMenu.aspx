﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="ViewRolMenu.aspx.cs" Inherits="BSS.SIT.Web.ViewRolMenu" %>

<%@ MasterType VirtualPath="~/MainSite.Master" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <a id="TemplateInfo"></a>
    <h1>
        Asociar Rol al Menu</h1>
    <div>
    </div>
    <br />
    <asp:UpdatePanel ID="updatePanelForm" runat="server">
        <ContentTemplate>
            <label>
                Rol</label>
            <asp:DropDownList ID="uiRol" runat="server" AutoPostBack="True" onselectedindexchanged="uiRol_SelectedIndexChanged">
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="Req_uiRol" runat="server" ErrorMessage="*" ControlToValidate="uiRol" Display="Dynamic" />
            <br />
            <label>
                Menu</label>
            <asp:TreeView ID="uiMenu" runat="server" ShowCheckBoxes="Leaf" ImageSet="Arrows">
                <HoverNodeStyle Font-Underline="True" ForeColor="#5555DD" />
                <NodeStyle Font-Names="Verdana" Font-Size="8pt" ForeColor="Black" HorizontalPadding="5px" NodeSpacing="0px" VerticalPadding="0px" />
                <ParentNodeStyle Font-Bold="False" />
                <SelectedNodeStyle Font-Underline="True" ForeColor="#5555DD" HorizontalPadding="0px" VerticalPadding="0px" />
            </asp:TreeView>
            <br />
            <asp:Button ID="uiActualizar" runat="server" Text="Actualizar" onclick="uiActualizar_Click" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
