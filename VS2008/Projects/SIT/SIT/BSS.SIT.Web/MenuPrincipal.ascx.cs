﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Caching;
using BSS.SIT.Common;
using BSS.SIT.Data.Model;
using BSS.SIT.Web.Code;

namespace BSS.SIT.Web
{
    /// <summary>
    /// Clase BSS.SIT.Web.MenuPrincipal
    /// </summary>
    public partial class MenuPrincipal : System.Web.UI.UserControl
    {
        #region Propiedades
        /// <summary>
        /// Gets or sets the aplicacion.
        /// </summary>
        /// <value>The aplicacion.</value>
        public Aplicacion Aplicacion
        {
            get { return (Aplicacion)Session["AplicacionSIT"]; }
            set { Session["AplicacionSIT"] = value; }
        }
        #endregion

        #region Eventos
        /// <summary>
        /// page_ load
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">La instancia de <see cref="System.EventArgs"/> que contiene los datos del evento.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    uiMenu.Text = CrearMenu();
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
        }
        #endregion

        #region Metodos
        /// <summary>
        /// crear menu
        /// </summary>
        /// <returns>
        /// System.String
        /// </returns>
        public string CrearMenu()
        {
            //toma el rol
            int idRol = Aplicacion.Usuario.RolId;

            String nombreCache = string.Format("menu{0}", idRol);
            string menu = HttpContext.Current.Cache[nombreCache] as string;

            if (menu == null)
            {
                try
                {
                    //llama la base de datos
                    SitDb db = new SitDb();

                    //trae lo que este en menu x rol
                    var lista = db.ListarMenuRol(idRol).ToList();

                    //Crea el nodo padre
                    Node nodoPadre = new Node();

                    //recorre la lista
                    Node nodo;
                    foreach (var item in lista)
                    {
                        //Valida si el elemento es padre
                        if (item.Id.Equals(item.PadreId.GetValueOrDefault()))
                        {
                            nodo = new Node();
                            nodo.Value = item.Id.ToString();
                            nodo.Text = item.Descripcion;
                            nodo.NavigateUrl = item.Url;

                            //Agregamos el nodo
                            nodoPadre.ChildNodes.Add(nodo);

                            //Agregamos los hijos recursivamente
                            AgregarNodo(ref nodo, lista);
                        }
                    }

                    StringBuilder contenido = new StringBuilder();
                    contenido.AppendFormat("<ul>{0}", Environment.NewLine);
                    contenido.AppendFormat("<li><a href=\"{0}\">{1}</a></li>{2}", "Default.aspx", "INICIO", Environment.NewLine);
                    foreach (var item in nodoPadre.ChildNodes)
                    {
                        if (item.ChildNodes.Count == 0)
                        {
                            contenido.AppendFormat("<li><a href=\"{0}\">{1}</a></li>{2}", item.NavigateUrl, item.Text, Environment.NewLine);
                        }
                        else
                        {
                            contenido.AppendFormat("<li><a href=\"{0}\">{1}</a>{2}", item.NavigateUrl, item.Text, Environment.NewLine);
                            contenido.AppendFormat("<ul>{0}", Environment.NewLine);
                            ArmarHijo(ref contenido, item);
                            contenido.AppendFormat("</ul>{0}", Environment.NewLine);
                            contenido.AppendFormat("</li>{0}", Environment.NewLine);
                        }
                    }
                    
                    //antes de cerrar agrega el logout
                    contenido.AppendFormat("<li><a href=\"{0}\">{1}</a></li>{2}", "Logout.aspx", "SALIR", Environment.NewLine);

                    //cierra la lista
                    contenido.AppendFormat("</ul>", Environment.NewLine);

                    menu = contenido.ToString();
                    HttpContext.Current.Cache.Insert(nombreCache, menu, null, DateTime.Now.AddMinutes(7), Cache.NoSlidingExpiration);
                }
                catch { }
            }
            return menu;
        }
        #endregion

        #region Metodos Privados
        private void ArmarHijo(ref StringBuilder contenido, Node nodoPadre)
        {
            foreach (var item in nodoPadre.ChildNodes)
            {
                if (item.ChildNodes.Count == 0)
                {
                    contenido.AppendFormat("<li><a href=\"{0}\">{1}</a></li>{2}", item.NavigateUrl, item.Text, Environment.NewLine);
                }
                else
                {
                    contenido.AppendFormat("<li><a href=\"{0}\">{1}</a>{2}", item.NavigateUrl, item.Text, Environment.NewLine);
                    contenido.AppendFormat("<ul>{0}", Environment.NewLine);
                    ArmarHijo(ref contenido, item);
                    contenido.AppendFormat("</ul>", Environment.NewLine);
                    contenido.AppendFormat("</li>", Environment.NewLine);
                }
            }

        }

        private void AgregarNodo(ref Node nodo, List<ListarMenuRolResult> lista)
        {
            Node nodoHijo;
            foreach (var item in lista)
            {
                if ((item.PadreId.ToString().Equals(nodo.Value)) && !(item.Id.Equals(item.PadreId)))
                {
                    nodoHijo = new Node();
                    nodoHijo.Value = item.Id.ToString();
                    nodoHijo.Text = item.Descripcion;
                    nodoHijo.NavigateUrl = item.Url;

                    //Agregamos el nodo
                    nodo.ChildNodes.Add(nodoHijo);

                    //Agregamos los hijos recursivamente
                    AgregarNodo(ref nodoHijo, lista);
                }
            }
        }
        #endregion
    }
}