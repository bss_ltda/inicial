﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="EditFormatoConcepto.aspx.cs" Inherits="BSS.SIT.Web.EditFormatoConcepto" %>
<%@ MasterType VirtualPath="~/MainSite.Master" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <a id="TemplateInfo"></a>
    <h1>
        Editar Concepto</h1>
    <asp:UpdatePanel ID="updatePanelForm" runat="server">
        <ContentTemplate>
            <p>
                <label>
                    Formato</label>
                <asp:DropDownList ID="uiFormato" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiFormato" runat="server" ErrorMessage="*" ControlToValidate="uiFormato" Display="Dynamic" />
                <label>Concepto</label>
                <asp:TextBox ID="uiConcepto" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Req_uiConcepto" runat="server" ErrorMessage="*" ControlToValidate="uiConcepto" Display="Dynamic" />
                <label>Descripcion</label>
                <asp:TextBox ID="uiDescripcion" runat="server" Columns="50" Rows="3" TextMode="MultiLine"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Req_uiDescripcion" runat="server" ErrorMessage="*" ControlToValidate="uiDescripcion" Display="Dynamic" />
                <label>
                    Declaracion</label>
                <asp:DropDownList ID="uiDeclaracion" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="T" Text="Tercero"></asp:ListItem>
                    <asp:ListItem Value="D" Text="Declarante"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiDeclaracion" runat="server" ErrorMessage="*" ControlToValidate="uiDeclaracion" Display="Dynamic" />
                <br />
                <br />
                <asp:Button ID="uiGuardar" runat="server" Text="Guardar" CssClass="button" OnClick="uiGuardar_Click" />
            </p>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
