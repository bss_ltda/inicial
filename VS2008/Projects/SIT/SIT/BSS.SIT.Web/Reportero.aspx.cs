﻿using System;
using System.Data;
using System.Linq;
using BSS.SIT.Common;
using BSS.SIT.Data.Model;
using BSS.SIT.Web.Code;
using Microsoft.Reporting.WebForms;

namespace BSS.SIT.Web
{
    /// <summary>
    /// Clase BSS.SIT.Web.Reportero
    /// </summary>
    public partial class Reportero : System.Web.UI.Page, IPage
    {
        #region Miembros
        #endregion

        #region Propiedades
        /// <summary>
        /// Gets the ledger book id.
        /// </summary>
        public int LedgerBookId
        {
            get
            {
                if (string.IsNullOrEmpty(Request["LedgerBookId"]))
                    return 0;
                else
                    return Assign.ToInt(Request["LedgerBookId"]);
            }
        }

        /// <summary>
        /// Gets the formato.
        /// </summary>
        public int Formato
        {
            get
            {
                if (string.IsNullOrEmpty(Request["Formato"]))
                    return 0;
                else
                    return Assign.ToInt(Request["Formato"]);
            }
        }

        /// <summary>
        /// Gets the identificacion.
        /// </summary>
        public string Identificacion
        {
            get
            {
                if (string.IsNullOrEmpty(Request["Identificacion"]))
                    return string.Empty;
                return Request["Identificacion"];
            }
        }

        /// <summary>
        /// Gets the bimestre.
        /// </summary>
        public int Bimestre
        {
            get
            {
                if (string.IsNullOrEmpty(Request["Bimestre"]))
                    return 0;
                else
                    return Assign.ToInt(Request["Bimestre"]);
            }
        }

        /// <summary>
        /// Gets the exportar A.
        /// </summary>
        public Utility.Exportar ExportarA
        {
            get
            {
                if (string.IsNullOrEmpty(Request["ExportarA"]))
                    return Utility.Exportar.EXCEL;
                else if (Request["ExportarA"].Equals("EXCEL"))
                    return Utility.Exportar.EXCEL;
                else 
                    return Utility.Exportar.PDF;
            }
        }
        #endregion

        #region Eventos
        /// <summary>
        /// page_ load
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">La instancia de <see cref="System.EventArgs"/> que contiene los datos del evento.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                LoadData();
            }
            catch (Exception ex)
            {
                uiMensaje.Text = ex.Message;
            }

        }
        #endregion

        #region Metodos
        /// <summary>
        /// load data
        /// </summary>
        public void LoadData()
        {
            switch (this.Formato)
            {
                case 1001:
                    Generar1001();
                    break;
                case 1002:
                    Generar1002();
                    break;
                case 1003:
                    Generar1003();
                    break;
                case 1004:
                    Generar1004();
                    break;
                case 1005:
                    Generar1005();
                    break;
                case 1006:
                    Generar1006();
                    break;
                case 1007:
                    Generar1007();
                    break;
                case 1008:
                    Generar1008();
                    break;
                case 1009:
                    Generar1009();
                    break;
                case 1011:
                    Generar1011();
                    break;
                case 1012:
                    Generar1012();
                    break;
                case 1043:
                    Generar1043();
                    break;
                case 1044:
                    Generar1044();
                    break;
                case 1045:
                    Generar1045();
                    break;
                case 9990:
                    GenerarRenta();
                    break;
                case 9991:
                    GenerarIca();
                    break;
                case 9992:
                    GenerarIva();
                    break;
                default:
                    uiMensaje.Text = "No hay opcion";
                    break;
            }
        }

        /// <summary>
        /// load UI
        /// </summary>
        public void LoadUI()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// load object
        /// </summary>
        public void LoadObject()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// load element
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="index">The index.</param>
        public void LoadElement<T>(T index)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// process
        /// </summary>
        public void Process()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// validate form
        /// </summary>
        /// <returns>
        /// System.Boolean
        /// </returns>
        public bool ValidateForm()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Metodos para Formatos
        private void Generar1001()
        {
            using (SitDb db = new SitDb())
            {
                db.CommandTimeout = 0;
                var result = db.Formato1001(this.LedgerBookId).ToList();
                var data = Utility.ConvertToDataSet<Formato1001Result>(result);
                if (data != null)
                    CargarReporte(data, "Ds1001", "Reporte1001.rdlc", "Formato1001", Utility.Exportar.EXCEL);
                else
                    uiMensaje.Text = "No existen datos para el proveedor";
            }
        }

        private void Generar1002()
        {
            //N/A
            //using (SitDb db = new SitDb())
            //{
            //    db.CommandTimeout = 0;
            //    var result = db.Formato1002(this.LedgerBookId).ToList();
            //    var data = Utility.ConvertToDataSet<Formato1002Result>(result);
            //    if (data != null)
            //        CargarReporte(data, "Ds1002", "Reporte1002.rdlc", "Formato1002", Utility.Exportar.EXCEL);
            //    else
            //        uiMensaje.Text = "No existen datos para el proveedor";
            //}
        }

        private void Generar1003()
        {
            using (SitDb db = new SitDb())
            {
                db.CommandTimeout = 0;
                var result = db.Formato1003(this.LedgerBookId).ToList();
                var data = Utility.ConvertToDataSet<Formato1003Result>(result);
                if (data != null)
                    CargarReporte(data, "Ds1003", "Reporte1003.rdlc", "Formato1003", Utility.Exportar.EXCEL);
                else
                    uiMensaje.Text = "No existen datos para el proveedor";
            }
        }

        private void Generar1004()
        {
            using (SitDb db = new SitDb())
            {
                db.CommandTimeout = 0;
                var result = db.Formato1004(this.LedgerBookId).ToList();
                var data = Utility.ConvertToDataSet<Formato1004Result>(result);
                if (data != null)
                    CargarReporte(data, "Ds1004", "Reporte1004.rdlc", "Formato1004", Utility.Exportar.EXCEL);
                else
                    uiMensaje.Text = "No existen datos para el proveedor";
            }
        }

        private void Generar1005()
        {
            using (SitDb db = new SitDb())
            {
                db.CommandTimeout = 0;
                var result = db.Formato1005(this.LedgerBookId).ToList();
                var data = Utility.ConvertToDataSet<Formato1005Result>(result);
                if (data != null)
                    CargarReporte(data, "Ds1005", "Reporte1005.rdlc", "Formato1005", Utility.Exportar.EXCEL);
                else
                    uiMensaje.Text = "No existen datos para el proveedor";
            }
        }

        private void Generar1006()
        {
            using (SitDb db = new SitDb())
            {
                db.CommandTimeout = 0;
                var result = db.Formato1006(this.LedgerBookId).ToList();
                var data = Utility.ConvertToDataSet<Formato1006Result>(result);
                if (data != null)
                    CargarReporte(data, "Ds1006", "Reporte1006.rdlc", "Formato1006", Utility.Exportar.EXCEL);
                else
                    uiMensaje.Text = "No existen datos para el proveedor";
            }
        }

        private void Generar1007()
        {
            using (SitDb db = new SitDb())
            {
                db.CommandTimeout = 0;
                var result = db.Formato1007(this.LedgerBookId).ToList();
                var data = Utility.ConvertToDataSet<Formato1007Result>(result);
                if (data != null)
                    CargarReporte(data, "Ds1007", "Reporte1007.rdlc", "Formato1007", Utility.Exportar.EXCEL);
                else
                    uiMensaje.Text = "No existen datos para el proveedor";
            }
        }

        private void Generar1008()
        {
            using (SitDb db = new SitDb())
            {
                db.CommandTimeout = 0;
                var result = db.Formato1008(this.LedgerBookId).ToList();
                var data = Utility.ConvertToDataSet<Formato1008Result>(result);
                if (data != null)
                    CargarReporte(data, "Ds1008", "Reporte1008.rdlc", "Formato1008", Utility.Exportar.EXCEL);
                else
                    uiMensaje.Text = "No existen datos para el proveedor";
            }
        }

        private void Generar1009()
        {
            using (SitDb db = new SitDb())
            {
                db.CommandTimeout = 0;
                var result = db.Formato1009(this.LedgerBookId).ToList();
                var data = Utility.ConvertToDataSet<Formato1009Result>(result);
                if (data != null)
                    CargarReporte(data, "Ds1009", "Reporte1009.rdlc", "Formato1009", Utility.Exportar.EXCEL);
                else
                    uiMensaje.Text = "No existen datos para el proveedor";
            }
        }

        private void Generar1011()
        {
            using (SitDb db = new SitDb())
            {
                db.CommandTimeout = 0;
                var result = db.Formato1011(this.LedgerBookId).ToList();
                var data = Utility.ConvertToDataSet<Formato1011Result>(result);
                if (data != null)
                    CargarReporte(data, "Ds1011", "Reporte1011.rdlc", "Formato1011", Utility.Exportar.EXCEL);
                else
                    uiMensaje.Text = "No existen datos para el proveedor";
            }
        }

        private void Generar1012()
        {
            using (SitDb db = new SitDb())
            {
                db.CommandTimeout = 0;
                var result = db.Formato1012(this.LedgerBookId).ToList();
                var data = Utility.ConvertToDataSet<Formato1012Result>(result);
                if (data != null)
                    CargarReporte(data, "Ds1012", "Reporte1012.rdlc", "Formato1012", Utility.Exportar.EXCEL);
                else
                    uiMensaje.Text = "No existen datos para el proveedor";
            }
        }

        private void Generar1043()
        {
            using (SitDb db = new SitDb())
            {
                db.CommandTimeout = 0;
                var result = db.Formato1043(this.LedgerBookId).ToList();
                var data = Utility.ConvertToDataSet<Formato1043Result>(result);
                if (data != null)
                    CargarReporte(data, "Ds1043", "Reporte1043.rdlc", "Formato1043", Utility.Exportar.EXCEL);
                else
                    uiMensaje.Text = "No existen datos para el proveedor";
            }
        }

        private void Generar1044()
        {
            using (SitDb db = new SitDb())
            {
                db.CommandTimeout = 0;
                var result = db.Formato1044(this.LedgerBookId).ToList();
                var data = Utility.ConvertToDataSet<Formato1044Result>(result);
                if (data != null)
                    CargarReporte(data, "Ds1044", "Reporte1044.rdlc", "Formato1044", Utility.Exportar.EXCEL);
                else
                    uiMensaje.Text = "No existen datos para el proveedor";
            }
        }

        private void Generar1045()
        {
            using (SitDb db = new SitDb())
            {
                db.CommandTimeout = 0;
                var result = db.Formato1045(this.LedgerBookId).ToList();
                var data = Utility.ConvertToDataSet<Formato1045Result>(result);
                if (data != null)
                    CargarReporte(data, "Ds1045", "Reporte1045.rdlc", "Formato1045", Utility.Exportar.EXCEL);
                else
                    uiMensaje.Text = "No existen datos para el proveedor";
            }
        }
        #endregion

        #region Metodos para Certificados
        private void GenerarRenta()
        {
            using (SitDb db = new SitDb())
            {
                var result = db.RetencionFuenteRenta(this.LedgerBookId, this.Identificacion).ToList();
                var data = Utility.ConvertToDataSet<RetencionFuenteRentaResult>(result);
                if (data != null)
                    CargarReporte(data, "DsCertificadoRenta", "ReporteRenta.rdlc", "CertificadoRetencionFteRenta", this.ExportarA);
                else
                    uiMensaje.Text = "No existen datos para el proveedor";
            }
        }

        private void GenerarIca()
        {
            using (SitDb db = new SitDb())
            {
                //trae el mensaje del ica
                ReportParameter parametro;
                var config = db.Configuracions.SingleOrDefault(x => x.Llave == Utility.Configuracion.MENSAJEICA.ToString());
                if (config != null)
                    parametro = new ReportParameter("MensajeIca", config.Valor);
                else
                    parametro = new ReportParameter("MensajeIca", "--------");


                var result1 = db.RetencionFuenteIca(this.LedgerBookId, this.Identificacion, this.Bimestre).ToList();
                var data1 = Utility.ConvertToDataSet<RetencionFuenteIcaResult>(result1);

                var result2 = db.ConsultarPagoImpuesto(this.LedgerBookId, this.Bimestre).ToList();
                var data2 = Utility.ConvertToDataSet<ConsultarPagoImpuestoResult>(result2);

                if (data1 == null)
                    throw new ApplicationException("No se encontro informacion para generar el certificado");
                if (data2 == null)
                    throw new ApplicationException("No se encontro informacion de stickers asociados");

                //Inserta los dos resultado en otro dataset
                DataSet dsResultado = new DataSet();
                data1.Tables[0].TableName = "DsCertificadoIca";
                dsResultado.Tables.Add(data1.Tables[0].Copy());
                data2.Tables[0].TableName = "DsPagoImpuesto";
                dsResultado.Tables.Add(data2.Tables[0].Copy());

                if ((data1 != null) && (data2 != null))
                    CargarReporteMultiple(dsResultado, "ReporteIca.rdlc", "CertificadoRetencionFteIca", this.ExportarA, parametro);
                else
                    uiMensaje.Text = "No existen datos para el proveedor";


                //if (data1 != null)
                //    CargarReporte(data1, "DsCertificadoIca", "ReporteIca.rdlc", "CertificadoRetencionFteIca", this.ExportarA, parametro);
                //else
                //    uiMensaje.Text = "No existen datos para el proveedor";
            }
        }

        private void GenerarIva()
        {
            using (SitDb db = new SitDb())
            {
                var result1 = db.RetencionFuenteIva(this.LedgerBookId, this.Identificacion, this.Bimestre).ToList();
                var data1 = Utility.ConvertToDataSet<RetencionFuenteIvaResult>(result1);

                var result2 = db.ConsultarPagoImpuesto(this.LedgerBookId, this.Bimestre).ToList();
                var data2 = Utility.ConvertToDataSet<ConsultarPagoImpuestoResult>(result2);

                if (data1 == null)
                    throw new ApplicationException("No se encontro informacion para generar el certificado");
                if (data2 == null)
                    throw new ApplicationException("No se encontro informacion de stickers asociados");

                //Inserta los dos resultado en otro dataset
                DataSet dsResultado = new DataSet();
                data1.Tables[0].TableName = "DsCertificadoIva";
                dsResultado.Tables.Add(data1.Tables[0].Copy());
                data2.Tables[0].TableName = "DsPagoImpuesto";
                dsResultado.Tables.Add(data2.Tables[0].Copy());

                if ((data1 != null) && (data2 != null))
                    CargarReporteMultiple(dsResultado, "ReporteIva.rdlc", "CertificadoRetencionFteIva", this.ExportarA);
                else
                    uiMensaje.Text = "No existen datos para el proveedor";
            }
        }
        #endregion

        #region Reportes
        private void CargarReporte(DataSet dsResultado, string nombreDataSource, string reporte, string nombreArchivo, Utility.Exportar exportar)
        {
            CargarReporte(dsResultado, nombreDataSource, reporte, nombreArchivo, exportar, null);
        }

        private void CargarReporte(DataSet dsResultado, string nombreDataSource, string reporte, string nombreArchivo, Utility.Exportar exportar, ReportParameter parametro)
        {
            string nombreReporte = string.Format("Reportes/{0}", reporte);
            ReportDataSource dataSource = new ReportDataSource(nombreDataSource, dsResultado.Tables[0]);

            //limpia si tiene alguna datasource y agregamos el vigente
            visor.LocalReport.ReportPath = nombreReporte;
            visor.LocalReport.DataSources.Clear();
            visor.LocalReport.DataSources.Add(dataSource);

            //agrega los parametros
            if (parametro != null)
                visor.LocalReport.SetParameters(parametro);

            visor.LocalReport.Refresh();

            //Para guardarlo
            string mimeType, encoding, fileNameExtension;
            string[] streams;
            Warning[] warnings;
            byte[] pdfContent = visor.LocalReport.Render(exportar.ToString(), null, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);
            string archivo = string.Format("{0}.{1}", nombreArchivo, fileNameExtension);

            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("Content-Type", "application/xls");
            Response.AddHeader("content-disposition", "attachment; filename=" + archivo);
            Response.BinaryWrite(pdfContent);
            Response.End();
        }

        private void CargarReporteMultiple(DataSet dsResultado, string reporte, string nombreArchivo, Utility.Exportar exportar)
        {
            CargarReporteMultiple(dsResultado, reporte, nombreArchivo, exportar, null);
        }

        private void CargarReporteMultiple(DataSet dsResultado, string reporte, string nombreArchivo, Utility.Exportar exportar, ReportParameter parametro)
        {
            string nombreReporte = string.Format("Reportes/{0}", reporte);

            //limpia si tiene alguna datasource y agregamos el vigente
            visor.LocalReport.ReportPath = nombreReporte;
            visor.LocalReport.DataSources.Clear();

            for (int i = 0; i < dsResultado.Tables.Count; i++)
            {
                ReportDataSource dataSource = new ReportDataSource(dsResultado.Tables[i].TableName, dsResultado.Tables[i]);
                visor.LocalReport.DataSources.Add(dataSource);
            }

            //agrega los parametros
            if (parametro != null)
                visor.LocalReport.SetParameters(parametro);
            
            visor.LocalReport.Refresh();

            //Para guardarlo
            string mimeType, encoding, fileNameExtension;
            string[] streams;
            Warning[] warnings;
            byte[] pdfContent = visor.LocalReport.Render(exportar.ToString(), null, out mimeType, out encoding, out fileNameExtension, out streams, out warnings);
            string archivo = string.Format("{0}.{1}", nombreArchivo, fileNameExtension);

            Response.Clear();
            Response.ContentType = mimeType;
            Response.AddHeader("Content-Type", "application/xls");
            Response.AddHeader("content-disposition", "attachment; filename=" + archivo);
            Response.BinaryWrite(pdfContent);
            Response.End();
        }
        #endregion
    }
}