﻿function pageLoad() {
    /// <summary>
    /// Evento inicial de la pagina
    /// </summary>
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_beginRequest(beginRequestHandler);
    prm.add_endRequest(endRequestHandler);
}

function beginRequestHandler(sender, e) {
    /// <summary>
    /// inicia el request de la pagina
    /// </summary>
    //$.blockUI());
    $.blockUI({ message: $('#primateBusyBox') });
}

function endRequestHandler(sender, e) {
    /// <summary>
    /// inicia el request de la pagina
    /// </summary>
    $.unblockUI();
}

function confirmDelete() {
    if (confirm('Desea eliminar este registro?')) {
        return true;
    }
    return false;
}

function isNumeric(e) {
    /// <summary>
    /// valida si la tecla orpimida es numerica
    /// </summary>
    var tecla = (document.all) ? e.keyCode : e.which;

    if (tecla == 8 || tecla == 0)
        return true;

    var patron = /\d/;
    return patron.test(String.fromCharCode(tecla));
}