﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="UpdateLibreria.aspx.cs" Inherits="BSS.SIT.Web.UpdateLibreria" %>
<%@ MasterType VirtualPath="~/MainSite.Master" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <a id="TemplateInfo"></a>
    <h1>
        Actualizar libreria</h1>
    <asp:UpdatePanel ID="updatePanelForm" runat="server">
        <ContentTemplate>
            <p>
                <label>
                    Base de Datos</label>
                <asp:TextBox ID="uiBaseDatos" runat="server" />
                <asp:RequiredFieldValidator ID="Req_uiBaseDatos" runat="server" ErrorMessage="*" ControlToValidate="uiBaseDatos" Display="Dynamic" />
                <label>
                    Libreria de Datos</label>
                <asp:TextBox ID="uiLibreria" runat="server" />
                <asp:RequiredFieldValidator ID="Req_uiLibreria" runat="server" ErrorMessage="*" ControlToValidate="uiLibreria" Display="Dynamic" />
                <br />
                <br />
                <asp:Button ID="uiGuardar" runat="server" Text="Actualizar" CssClass="button" OnClick="uiGuardar_Click" />
            </p>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>