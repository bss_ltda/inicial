﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using BSS.SIT.Common;
using BSS.SIT.Data.Model;
using BSS.SIT.Web.Code;
using System.Text;

namespace BSS.SIT.Web
{
    /// <summary>
    /// Clase BSS.SIT.Web.ProcessCuenta
    /// </summary>
    public partial class ProcessCuenta : System.Web.UI.Page, IPage
    {
        #region Eventos
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    LoadUI();
                }
                catch (Exception ex)
                {
                    Master.Message = Utility.ShowError(this, ex);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the uiProcesar control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void uiProcesar_Click(object sender, EventArgs e)
        {
            try
            {
                Process();
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }
        #endregion

        #region IPage Members
        /// <summary>
        /// Loads the data.
        /// </summary>
        public void LoadData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the UI.
        /// </summary>
        public void LoadUI()
        {
            //Carga los combos
            using (SitDb db = new SitDb())
            {
                var listaLedgers = db.LedgerBooks
                    .Select(x => new { Id = x.Id, Desc = string.Format("{0} - {1} - {2} - {3} - {4}", x.Ledger, x.Book, x.ChartAccount, x.Ano, x.CompaniaId) }).ToList();
                uiLedgerBook.DataSource = listaLedgers;
                uiLedgerBook.DataValueField = "Id";
                uiLedgerBook.DataTextField = "Desc";
                uiLedgerBook.DataBind();
                uiLedgerBook.Items.Insert(0, new ListItem("[Seleccione]", string.Empty));
            }

            LoadObject();
        }

        /// <summary>
        /// Loads the object.
        /// </summary>
        public void LoadObject()
        {
            using (SitDb db = new SitDb())
            {
                var result = db.ConsultarEstado("CUENTA").SingleOrDefault();
                if (result != null)
                {
                    StringBuilder cadena = new StringBuilder();
                    cadena.AppendFormat("Estado Proceso: {0}.{1}", result.Estado, Environment.NewLine);
                    cadena.AppendFormat("Registros CXP: {0}.{1}", result.Conteo1, Environment.NewLine);
                    cadena.AppendFormat("Registros CXC: {0}.{1}", result.Conteo2, Environment.NewLine);
                    cadena.AppendFormat("Registros Otros: {0}.{1}", result.Conteo3, Environment.NewLine);
                    uiProceso.Text = cadena.ToString();
                }
            }
        }

        /// <summary>
        /// Loads the element.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="index">The index.</param>
        public void LoadElement<T>(T index)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Processes this instance.
        /// </summary>
        public void Process()
        {
            using (SitDb db = new SitDb())
            {
                //Actualiza la configuracion para despues leerla en el job
                db.ActualizarConfiguracion(Utility.Configuracion.PROCESOSALDOCUENTALEDGERBOOK.ToString(), uiLedgerBook.SelectedValue);

                db.ActualizarCuentaJob();
            }

            System.Threading.Thread.Sleep(1000);
            LoadObject();
            Master.Message = "Proceso de cuentas enviado.";
        }

        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns></returns>
        public bool ValidateForm()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}