﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="ProcessTercero.aspx.cs" Inherits="BSS.SIT.Web.ProcessTercero" %>

<%@ MasterType VirtualPath="~/MainSite.Master" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
    <meta http-equiv="refresh" content="300">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <a id="TemplateInfo"></a>
    <h1>
        Cargar Terceros</h1>
    <asp:UpdatePanel ID="updatePanelForm" runat="server">
        <ContentTemplate>
            <p>
                <asp:Button ID="uiProcesar" runat="server" Text="Procesar" OnClick="uiProcesar_Click" />
                <br />
                <br />
                <label>
                    Estado:</label>
                <asp:TextBox ID="uiProceso" runat="server" ReadOnly="true" CssClass="readonly" Columns="30" Rows="5" TextMode="MultiLine" />
            </p>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
