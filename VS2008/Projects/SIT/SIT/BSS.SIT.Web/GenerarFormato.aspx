﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="GenerarFormato.aspx.cs" Inherits="BSS.SIT.Web.GenerarFormato" %>

<%@ MasterType VirtualPath="~/MainSite.Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <a id="TemplateInfo"></a>
    <h1>
        Configurar formatos</h1>
    <asp:UpdatePanel ID="updatePanelForm" runat="server">
        <ContentTemplate>
            <p>
                <label>
                    LedgerBook</label>
                <asp:DropDownList ID="uiLedgerBook" runat="server" AutoPostBack="True" OnSelectedIndexChanged="uiLedgerBook_SelectedIndexChanged">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiLedgerBook" runat="server" ErrorMessage="*" ControlToValidate="uiLedgerBook" Display="Dynamic" />
                <label>
                    Formato</label>
                <asp:DropDownList ID="uiFormato" runat="server" Width="300px" AutoPostBack="True" OnSelectedIndexChanged="uiFormato_SelectedIndexChanged">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiFormato" runat="server" ErrorMessage="*" ControlToValidate="uiFormato" Display="Dynamic" />
                <label>
                    Concepto</label>
                <asp:DropDownList ID="uiConcepto" runat="server" Width="300px" AutoPostBack="True" OnSelectedIndexChanged="uiConcepto_SelectedIndexChanged">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiConcepto" runat="server" ErrorMessage="*" ControlToValidate="uiConcepto" Display="Dynamic" />
                <label>
                    Cuenta Inicial
                </label>
                <asp:DropDownList ID="uiCuentaInicial" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiCuentaInicial" runat="server" ErrorMessage="*" ControlToValidate="uiCuentaInicial" Display="Dynamic" />
                <label>
                    Cuenta Final
                </label>
                <asp:DropDownList ID="uiCuentaFinal" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiCuentaFinal" runat="server" ErrorMessage="*" ControlToValidate="uiCuentaFinal" Display="Dynamic" />
                <label>
                    Tipo</label>
                <asp:DropDownList ID="uiTipo" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="D" Text="Debito"></asp:ListItem>
                    <asp:ListItem Value="C" Text="Credito"></asp:ListItem>
                    <asp:ListItem Value="T" Text="Debito - Credito"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiTipo" runat="server" ErrorMessage="*" ControlToValidate="uiTipo" Display="Dynamic" />
                <label>
                    Campo Valor</label>
                <asp:DropDownList ID="uiCampoValor" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="1" Text="Valor 1"></asp:ListItem>
                    <asp:ListItem Value="2" Text="Valor 2"></asp:ListItem>
                    <asp:ListItem Value="3" Text="Valor 3"></asp:ListItem>
                    <asp:ListItem Value="4" Text="Valor 4"></asp:ListItem>
                    <asp:ListItem Value="5" Text="Valor 5"></asp:ListItem>
                    <asp:ListItem Value="6" Text="Valor 6"></asp:ListItem>
                    <asp:ListItem Value="7" Text="Valor 7"></asp:ListItem>
                    <asp:ListItem Value="8" Text="Valor 8"></asp:ListItem>
                    <asp:ListItem Value="9" Text="Valor 9"></asp:ListItem>
                    <asp:ListItem Value="10" Text="Valor 10"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiCampoValor" runat="server" ErrorMessage="*" ControlToValidate="uiCampoValor" Display="Dynamic" />
                <br />
                <br />
                <asp:Button ID="uiGuardar" runat="server" Text="Guardar" CssClass="button" OnClick="uiGuardar_Click" />
                <br />
                <label>
                    Cuentas Existente</label>
                <asp:GridView ID="uiResults" runat="server" CellPadding="4" EnableModelValidation="True" ForeColor="#333333" GridLines="None" EmptyDataText="No se encontraron registros" AutoGenerateColumns="False" CellSpacing="1" DataKeyNames="Id">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Seleccion">
                            <ItemTemplate>
                                <input id="uiSeleccion" type="checkbox" runat="server" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="Cuenta" HeaderText="Cuenta" ItemStyle-HorizontalAlign="Center">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" ItemStyle-HorizontalAlign="Center">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Tipo" HeaderText="Tipo" ItemStyle-HorizontalAlign="Center">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Valor" HeaderText="Campo Valor" ItemStyle-HorizontalAlign="Center">
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                </asp:GridView>
                <asp:Button ID="uiEliminarMasivo" runat="server" Text="Eliminar seleccion" CssClass="button" OnClientClick="return confirmDelete();" onclick="uiEliminarMasivo_Click" />
            </p>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
