﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using BSS.SIT.Common;
using BSS.SIT.Data.Model;
using BSS.SIT.Web.Code;

namespace BSS.SIT.Web
{
    /// <summary>
    /// Clase BSS.SIT.Web.ViewRolMenu
    /// </summary>
    public partial class ViewRolMenu : System.Web.UI.Page, IPage
    {
        #region Miembros
        private string nodeId;
        #endregion

        #region Eventos
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadUI();
            }
        }

        /// <summary>
        /// UI rol_ selected index changed
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">La instancia de <see cref="System.EventArgs"/> que contiene los datos del evento.</param>
        protected void uiRol_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                CargarRol(Assign.ToInt(uiRol.SelectedValue));
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }

        /// <summary>
        /// UI actualizar_ click
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">La instancia de <see cref="System.EventArgs"/> que contiene los datos del evento.</param>
        protected void uiActualizar_Click(object sender, EventArgs e)
        {
            try
            {
                Process();
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }
        #endregion

        #region IPage Members
        /// <summary>
        /// Loads the data.
        /// </summary>
        public void LoadData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the UI.
        /// </summary>
        public void LoadUI()
        {
            using (SitDb db = new SitDb())
            {
                var listaRol = db.Rols.ToList();
                uiRol.DataSource = listaRol;
                uiRol.DataValueField = "Id";
                uiRol.DataTextField = "Descripcion";
                uiRol.DataBind();
                uiRol.Items.Insert(0, new ListItem("[Seleccione]", string.Empty));

                var menu = db.ListarMenu().ToList();
                CargarMenu(menu);
            }
        }

        /// <summary>
        /// Loads the object.
        /// </summary>
        public void LoadObject()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the element.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="index">The index.</param>
        public void LoadElement<T>(T index)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Processes this instance.
        /// </summary>
        public void Process()
        {
            int rolId = Assign.ToInt(uiRol.SelectedValue);
            List<int> listado = CargarSeleccionados(uiMenu.CheckedNodes);

            if (listado.Count == 0)
                throw new ApplicationException("No se encontraron items para asociar");

            using (SitDb db = new SitDb())
            {
                //elimina lo que hay por rol
                var items = db.RolMenus.Where(x => x.RolId == rolId);
                db.RolMenus.DeleteAllOnSubmit(items);

                //inserta los nuevos cambios
                var itemsNuevos = listado.Select(x => new RolMenu { RolId = rolId, MenuId = x });
                db.RolMenus.InsertAllOnSubmit(itemsNuevos);

                //envia a la bd
                db.SubmitChanges();

                //elimina la cache para q sea recreada
                String nombreCache = string.Format("menu{0}", rolId);
                HttpContext.Current.Cache.Remove(nombreCache);

                Master.Message = "Asociacion realizada exitosamente";
            }
        }

        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns></returns>
        public bool ValidateForm()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Metodos privados
        private void CargarMenu(List<ListarMenuResult> menu)
        {
            TreeNodeCollection col;
            col = uiMenu.Nodes;
            if (col.Count != 0)
                uiMenu.Nodes.Remove(col[0]);

            TreeNode nodoPadre = new TreeNode();
            nodoPadre.Value = "0";
            nodoPadre.Text = "Inicio";
            nodoPadre.NavigateUrl = "Default.aspx";
            nodoPadre.SelectAction = TreeNodeSelectAction.None;
            nodoPadre.ShowCheckBox = false;

            TreeNode nodo;
            foreach (var item in menu)
            {
                //Valida si el elemento es padre
                if (item.Id.Equals(item.PadreId.GetValueOrDefault()))
                {
                    nodo = new TreeNode();
                    nodo.Value = Assign.ToString(item.Id);
                    nodo.Text = item.Descripcion;
                    nodo.NavigateUrl = item.Url;
                    nodo.SelectAction = TreeNodeSelectAction.None;

                    //Agregamos el nodo
                    nodoPadre.ChildNodes.Add(nodo);

                    //Agregamos los hijos recursivamente
                    AgregarNodo(ref nodo, menu);
                }
            }

            //Agregamos el nodo padre
            uiMenu.Nodes.Add(nodoPadre);
            uiMenu.DataBind();
        }

        private void AgregarNodo(ref TreeNode nodo, List<ListarMenuResult> menu)
        {
            TreeNode nodoHijo;
            foreach (var item in menu)
            {
                if ((item.PadreId.GetValueOrDefault().ToString().Equals(nodo.Value)) && !(item.Id.Equals(item.PadreId.GetValueOrDefault())))
                {
                    nodoHijo = new TreeNode();
                    nodoHijo.Value = Assign.ToString(item.Id);
                    nodoHijo.Text = item.Descripcion;
                    nodoHijo.NavigateUrl = item.Url;
                    if (item.Url.Equals("#"))
                        nodoHijo.SelectAction = TreeNodeSelectAction.None;

                    //Agregamos el nodo
                    nodo.ChildNodes.Add(nodoHijo);

                    //Agregamos los hijos recursivamente
                    AgregarNodo(ref nodoHijo, menu);
                }
            }
        }

        private void CargarRol(int rolId)
        {
            List<ListarMenuRolResult> lista = new List<ListarMenuRolResult>();
            
            using (SitDb db = new SitDb())
            {
                lista = db.ListarMenuRol(rolId).ToList();
            }

            foreach (var item in lista)
            {
                nodeId = null;
                BuscarNodo(uiMenu.Nodes, item.Id.ToString());

                if (nodeId != null)
                {
                    TreeNode nodoEncontrado = uiMenu.FindNode(nodeId);
                    if (!nodoEncontrado.NavigateUrl.Equals("#"))
                        nodoEncontrado.Checked = true;
                }
            }
            uiMenu.DataBind();

        }

        private void BuscarNodo(TreeNodeCollection coleccion, string id)
        {
            for (int i = 0; i < coleccion.Count; i++)
            {
                if (coleccion[i].Value.Equals(id))
                {
                    nodeId = coleccion[i].ValuePath;
                    break;
                }

                //sino sigue buscando el nodo
                BuscarNodo(coleccion[i].ChildNodes, id);

            }
            return;
        }

        private List<int> CargarSeleccionados(TreeNodeCollection coleccion)
        {
            List<int> resultado = new List<int>();
            foreach (TreeNode item in coleccion)
            {
                //valida si el papa es la raiz le coloca el mismo numero
                char[] separador = new char[] { '/' };
                string[] arreglo = item.ValuePath.Split(separador);

                foreach (string id in arreglo)
                {
                    //que no sea cero y no este repetido
                    if (!id.Equals("0"))
                        if (!resultado.Exists(delegate(int i) { return i == Convert.ToInt32(id); }))
                            resultado.Add(Convert.ToInt32(id));
                }
            }
            return resultado;
        }
        #endregion
    }
}