﻿using System;
using System.Linq;
using BSS.SIT.Common;
using BSS.SIT.Data.Model;
using BSS.SIT.Web.Code;

namespace BSS.SIT.Web
{
    /// <summary>
    /// Clase BSS.SIT.Web.ParamMedioMagnetico
    /// </summary>
    public partial class ParamMedioMagnetico : System.Web.UI.Page, IPage
    {
        #region Propiedades
        #endregion

        #region Eventos
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    LoadUI();
                }
                catch (Exception ex)
                {
                    Master.Message = Utility.ShowError(this, ex);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the uiGuardar control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void uiGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                Process();
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }
        #endregion

        #region IPage Members
        /// <summary>
        /// Loads the data.
        /// </summary>
        public void LoadData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the UI.
        /// </summary>
        public void LoadUI()
        {
            //trae la informacion de las configuraciones
            using (SitDb db = new SitDb())
            {
                //trae el campo resumido
                var campoResumido = db.Configuracions.SingleOrDefault(x => x.Llave == Utility.Configuracion.PARAMRESUMIDO.ToString());
                if (campoResumido != null)
                    uiEsResumido.Checked = Assign.ToBool(campoResumido.Valor);
                else
                    uiEsResumido.Checked = false;

                //trae el campo de nit extranjero
                var campoExtranjero = db.Configuracions.SingleOrDefault(x => x.Llave == Utility.Configuracion.PARAMNITEXTRANJERO.ToString());
                if (campoExtranjero != null)
                    uiGeneraConsecutivo.Checked = Assign.ToBool(campoExtranjero.Valor);
                else
                    uiGeneraConsecutivo.Checked = false;
            }
        }

        /// <summary>
        /// Loads the object.
        /// </summary>
        public void LoadObject()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the element.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="index">The index.</param>
        public void LoadElement<T>(T index)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Processes this instance.
        /// </summary>
        public void Process()
        {
            //trae la informacion de las configuraciones
            using (SitDb db = new SitDb())
            {
                //guarda la info
                db.ActualizarConfiguracion(Utility.Configuracion.PARAMRESUMIDO.ToString(), uiEsResumido.Checked.ToString());

                //extranejero
                db.ActualizarConfiguracion(Utility.Configuracion.PARAMNITEXTRANJERO.ToString(), uiGeneraConsecutivo.Checked.ToString());

                Master.Message = "Registros actualizados exitosamente.";
            }
        }

        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns></returns>
        public bool ValidateForm()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}