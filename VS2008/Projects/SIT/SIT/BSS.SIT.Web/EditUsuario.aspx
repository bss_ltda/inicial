﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="EditUsuario.aspx.cs" Inherits="BSS.SIT.Web.EditUsuario" %>

<%@ MasterType VirtualPath="~/MainSite.Master" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <script type="text/javascript">
        function validarReferenciaTercero() {
            if ($('#ctl00_uiContentPlaceHolder_uiReferencia1').is(':checked')) {
                $('#ctl00_uiContentPlaceHolder_uiTercero').attr('disabled', 'disabled');
                $('#ctl00_uiContentPlaceHolder_uiTerceroReferencia').removeAttr('disabled');
                $('#ctl00_uiContentPlaceHolder_uiTerceroReferencia').removeClass('readonly');

                $('#ctl00_uiContentPlaceHolder_uiTercero').attr('selectedIndex', '0');
            }
            else {
                $('#ctl00_uiContentPlaceHolder_uiTercero').removeAttr('disabled');
                $('#ctl00_uiContentPlaceHolder_uiTerceroReferencia').attr('disabled', 'disabled');
                $('#ctl00_uiContentPlaceHolder_uiTerceroReferencia').addClass('readonly');

                $('#ctl00_uiContentPlaceHolder_uiTerceroReferencia').attr('selectedIndex', '0');
            }
        }

        function seleccionarClave(valor) {
            if (valor.length != 0) {
                var arreglo = valor.split('|');
                $('#ctl00_uiContentPlaceHolder_uiClaveId').val(arreglo[0]);
                $('#ctl00_uiContentPlaceHolder_uiClaveYear').val(arreglo[1]);
            }
            else {
                $('#ctl00_uiContentPlaceHolder_uiClaveId').val('');
                $('#ctl00_uiContentPlaceHolder_uiClaveYear').val('');
            }
        }

        $(function () {
            validarReferenciaTercero();
        }); 
    </script>
    <a id="TemplateInfo"></a>
    <h1>
        Editar Usuario</h1>
    <asp:UpdatePanel ID="updatePanelForm" runat="server">
        <ContentTemplate>
            <p>
                <label>
                    Nombre Completo</label>
                <asp:TextBox ID="uiNombreCompleto" runat="server" MaxLength="200" Columns="80" />
                <asp:RequiredFieldValidator ID="Req_uiNombreCompleto" runat="server" ErrorMessage="*" ControlToValidate="uiNombreCompleto" Display="Dynamic" />
                <label>
                    Usuario</label>
                <asp:TextBox ID="uiNombreUsuario" runat="server" MaxLength="50" Columns="50" />
                <asp:RequiredFieldValidator ID="Req_uiNombreUsuario" runat="server" ErrorMessage="*" ControlToValidate="uiNombreUsuario" Display="Dynamic" />
                <label>
                    Password</label>
                <asp:TextBox ID="uiClave" runat="server" MaxLength="50" Columns="50" TextMode="Password" />
                <asp:RequiredFieldValidator ID="Req_uiClave" runat="server" ErrorMessage="*" ControlToValidate="uiClave" Display="Dynamic" />
                <label>
                    Rol</label>
                <asp:DropDownList ID="uiRol" runat="server">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiRol" runat="server" ErrorMessage="*" ControlToValidate="uiRol" Display="Dynamic" />
                <label>
                    Activo</label>
                <asp:CheckBox ID="uiActivo" runat="server" />
                <br />
                <br />
                <asp:Button ID="uiGuardar" runat="server" Text="Guardar" CssClass="button" OnClick="uiGuardar_Click" />
            </p>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
