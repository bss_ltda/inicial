﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="ViewLedgerAccount.aspx.cs" Inherits="BSS.SIT.Web.ViewLedgerAccount" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/MainSite.Master" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <a id="TemplateInfo"></a>
    <h1>
        Cuentas</h1>
    <div>
        <a id="buttonAdd" href="EditLedgerAccountMassive.aspx">Crear Masivo</a>
    </div>
    <br />
    <h4>
        Buscador</h4>
    <p>
        <label>
            LedgerBook</label>
        <asp:DropDownList ID="uiLedgerBook" runat="server">
            <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
        </asp:DropDownList>
        <asp:RequiredFieldValidator ID="Req_uiLedgerBook" runat="server" ErrorMessage="*" ControlToValidate="uiLedgerBook" Display="Dynamic" />
        <label>
             Cuenta Inicial</label>
        <asp:DropDownList ID="uiCuentaInicial" runat="server">
            <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
        </asp:DropDownList>
        <asp:RequiredFieldValidator ID="Req_uiCuentaInicial" runat="server" ErrorMessage="*" ControlToValidate="uiCuentaInicial" Display="Dynamic" />
        <label>
             Cuenta Final</label>
            <asp:DropDownList ID="uiCuentaFinal" runat="server">
        <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
        </asp:DropDownList>
        <asp:RequiredFieldValidator ID="Req_uiCuentaFinal" runat="server" ErrorMessage="*" ControlToValidate="uiCuentaFinal" Display="Dynamic" />
        <label>
            Clasificacion</label>
        <asp:DropDownList ID="uiClasificacion" runat="server">
            <asp:ListItem Value="" Text="[Todas]" Selected="True"></asp:ListItem>
        </asp:DropDownList>
        <label>
            Detalle S.I.T</label>
        <asp:DropDownList ID="uiTif" runat="server">
            <asp:ListItem Value="" Text="[Todos]" Selected="True"></asp:ListItem>
            <asp:ListItem Value="1" Text="Si"></asp:ListItem>
            <asp:ListItem Value="0" Text="No"></asp:ListItem>
        </asp:DropDownList>
        <br />
        <br />
        <asp:Button ID="uiBuscar" runat="server" Text="Buscar" CssClass="button" OnClick="uiBuscar_Click" />
        <input id="uiReset" type="reset" value="Limpiar" class="button" />
    </p>
    <asp:UpdatePanel ID="updatePanelResultado" runat="server">
        <ContentTemplate>
            <asp:GridView ID="uiResults" runat="server" CellPadding="4" EnableModelValidation="True" ForeColor="#333333" GridLines="None" EmptyDataText="No se encontraron registros" AutoGenerateColumns="False" CellSpacing="1" DataKeyNames="Id" OnRowCommand="uiResults_RowCommand">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                    <asp:TemplateField HeaderText="Seleccion">
                        <ItemTemplate>
                            <input id="uiSeleccion" type="checkbox" runat="server" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="Cuenta" HeaderText="Cuenta" ItemStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" ItemStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ClasificacionCuenta" HeaderText="Clasificacion" ItemStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:CheckBoxField DataField="Tif" HeaderText="SIT" ItemStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:CheckBoxField>
                    <asp:ButtonField ButtonType="Link" HeaderText="Editar" ShowHeader="True" Text="<img src='images/brick_edit.png' title='Editar' style='border-width: 0;'>" CommandName="Edit1" ItemStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:ButtonField>
                    <asp:ButtonField ButtonType="Link" HeaderText="Eliminar" ShowHeader="True" Text="<img src='images/brick_delete.png' title='Eliminar' style='border-width: 0;' onclick='return confirmDelete();'>" CommandName="Delete1" ItemStyle-HorizontalAlign="Center">
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:ButtonField>
                </Columns>
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            </asp:GridView>
            <asp:Button ID="uiEliminarMasivo" runat="server" Text="Eliminar seleccion" CssClass="button" OnClientClick="return confirmDelete();" onclick="uiEliminarMasivo_Click" />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="uiBuscar" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
