﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="EditComprobanteExcluido.aspx.cs" Inherits="BSS.SIT.Web.EditComprobanteExcluido" %>
<%@ MasterType VirtualPath="~/MainSite.Master" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <a id="TemplateInfo"></a>
    <h1>
        Editar Comprobante Excluido</h1>
    <asp:UpdatePanel ID="updatePanelForm" runat="server">
        <ContentTemplate>   
            <p>
                <label>
                    Codigo Formato</label>
                <asp:DropDownList ID="uiFormato" runat="server" Width="300px" AutoPostBack="True" OnSelectedIndexChanged="uiFormato_SelectedIndexChanged">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiFormato" runat="server" ErrorMessage="*" ControlToValidate="uiFormato" Display="Dynamic" />
                <label>Concepto</label>
                <asp:DropDownList ID="uiConcepto" runat="server" Width="300px">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiConcepto" runat="server" ErrorMessage="*" ControlToValidate="uiConcepto" Display="Dynamic" />
                <label>Comprobante</label>
                <asp:DropDownList ID="uiComprobante" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiComprobante" runat="server" ErrorMessage="*" ControlToValidate="uiComprobante" Display="Dynamic" />
                <br />
                <br />
                <asp:Button ID="uiGuardar" runat="server" Text="Guardar" CssClass="button" OnClick="uiGuardar_Click" />
            </p>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
