﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="EditConfiguracion.aspx.cs" Inherits="BSS.SIT.Web.EditConfiguracion" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ MasterType VirtualPath="~/MainSite.Master" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <a id="TemplateInfo"></a>
    <h1>
        Editar Configuracion</h1>
    <asp:UpdatePanel ID="updatePanelForm" runat="server">
        <ContentTemplate>
            <p>
                <label>
                    Campo Es Principal para clientes:</label>
                <asp:DropDownList ID="uiCampoPrincipalTercero" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="CMFF01" Text="CMFF01"></asp:ListItem>
                    <asp:ListItem Value="CMFF02" Text="CMFF02"></asp:ListItem>
                    <asp:ListItem Value="CMFF03" Text="CMFF03"></asp:ListItem>
                    <asp:ListItem Value="CMFF04" Text="CMFF04"></asp:ListItem>
                    <asp:ListItem Value="CMFF05" Text="CMFF05"></asp:ListItem>
                    <asp:ListItem Value="CMFF06" Text="CMFF06"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiCampoPrincipalTercero" runat="server" ErrorMessage="*" ControlToValidate="uiCampoPrincipalTercero" Display="Dynamic" />
                <label>
                    Campo Es Principal para proveedores:</label>
                <asp:DropDownList ID="uiCampoPrincipalProveedor" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="VMREF1" Text="VMREF1"></asp:ListItem>
                    <asp:ListItem Value="VMREF2" Text="VMREF2"></asp:ListItem>
                    <asp:ListItem Value="VMREF3" Text="VMREF3"></asp:ListItem>
                    <asp:ListItem Value="VMREF4" Text="VMREF4"></asp:ListItem>
                    <asp:ListItem Value="VMREF5" Text="VMREF5"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiCampoPrincipalProveedor" runat="server" ErrorMessage="*" ControlToValidate="uiCampoPrincipalProveedor" Display="Dynamic" />
                <br />
                <br />
                <asp:Button ID="uiGuardar" runat="server" Text="Guardar" CssClass="button" OnClick="uiGuardar_Click" />
            </p>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
