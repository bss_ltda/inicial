﻿using System;
using System.Linq;
using System.Web.UI.WebControls;
using BSS.SIT.Common;
using BSS.SIT.Data.Model;
using BSS.SIT.Web.Code;

namespace BSS.SIT.Web
{
    /// <summary>
    /// Clase BSS.SIT.Web.ViewPagoImpuesto
    /// </summary>
    public partial class ViewPagoImpuesto : System.Web.UI.Page, IPage
    {
        #region Eventos
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadUI();
            }
        }

        /// <summary>
        /// Handles the RowCommand event of the uiResults control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCommandEventArgs"/> instance containing the event data.</param>
        protected void uiResults_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int index = Assign.ToInt(e.CommandArgument);
                int id = Assign.ToInt(uiResults.DataKeys[index].Value);

                if (e.CommandName == "Edit1")
                    Edit(id);
                else if (e.CommandName == "Delete1")
                    Delete(id);
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }

        /// <summary>
        /// UI ledger book_ selected index changed
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">La instancia de <see cref="System.EventArgs"/> que contiene los datos del evento.</param>
        protected void uiLedgerBook_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadElement(uiLedgerBook.SelectedValue);
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }
        #endregion

        #region IPage Members
        /// <summary>
        /// Loads the data.
        /// </summary>
        public void LoadData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the UI.
        /// </summary>
        public void LoadUI()
        {
            using (SitDb db = new SitDb())
            {
                var listaLedgers = db.LedgerBooks
                .Select(x => new { Id = x.Id, Desc = string.Format("{0} - {1} - {2} - {3} - {4}", x.Ledger, x.Book, x.ChartAccount, x.Ano, x.CompaniaId) }).ToList();
                uiLedgerBook.DataSource = listaLedgers;
                uiLedgerBook.DataValueField = "Id";
                uiLedgerBook.DataTextField = "Desc";
                uiLedgerBook.DataBind();
                uiLedgerBook.Items.Insert(0, new ListItem("[Seleccione]", string.Empty));

                LoadElement(uiLedgerBook.SelectedValue);
            }
        }

        /// <summary>
        /// Loads the object.
        /// </summary>
        public void LoadObject()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the element.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="index">The index.</param>
        public void LoadElement<T>(T index)
        {
            using (SitDb db = new SitDb())
            {
                var lista = db.PagoImpuestos.Where(x => x.LedgerBookId == Assign.ToInt(index)).ToList();
                uiResults.DataSource = lista;
                uiResults.DataBind();
            }
        }

        /// <summary>
        /// Processes this instance.
        /// </summary>
        public void Process()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns></returns>
        public bool ValidateForm()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Metodos privados
        private void Edit(int id)
        {
            string url = string.Format("EditPagoImpuesto.aspx?Id={0}", id);
            Response.Redirect(url, false);
        }

        private void Delete(int id)
        {
            using (SitDb db = new SitDb())
            {
                var item = db.PagoImpuestos.SingleOrDefault(x => x.Id == id);
                db.PagoImpuestos.DeleteOnSubmit(item);
                db.SubmitChanges();
            }
            LoadUI();
            Master.Message = "Registro eliminado exitosamente";
        }
        #endregion
    }
}