﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="ViewCertificadoIva.aspx.cs" Inherits="BSS.SIT.Web.ViewCertificadoIva" %>
<%@ MasterType VirtualPath="~/MainSite.Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <a id="TemplateInfo"></a>
    <h1>
        Certificado de IVA</h1>
    <p>
        <label>
            LedgerBook</label>
        <asp:DropDownList ID="uiLedgerBook" runat="server" >
            <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
        </asp:DropDownList>
        <asp:RequiredFieldValidator ID="Req_uiLedgerBook" runat="server" ErrorMessage="*" ControlToValidate="uiLedgerBook" Display="Dynamic" />
        <label>
            Identificiacion
        </label>
        <asp:TextBox ID="uiIdentificacion" runat="server" />
        <asp:RequiredFieldValidator ID="Req_uiIdentificacion" runat="server" ErrorMessage="*" ControlToValidate="uiIdentificacion" Display="Dynamic" />
        <label>
            Periodo</label>
        <asp:DropDownList ID="uiBimestre" runat="server" >
            <asp:ListItem Value="0" Text="Todo" Selected="True"></asp:ListItem>
            <asp:ListItem Value="1" Text="Bimestre 1"></asp:ListItem>
            <asp:ListItem Value="2" Text="Bimestre 2"></asp:ListItem>
            <asp:ListItem Value="3" Text="Bimestre 3"></asp:ListItem>
            <asp:ListItem Value="4" Text="Bimestre 4"></asp:ListItem>
            <asp:ListItem Value="5" Text="Bimestre 5"></asp:ListItem>
            <asp:ListItem Value="6" Text="Bimestre 6"></asp:ListItem>
        </asp:DropDownList>
        <label>
            Formato</label>
        <asp:RadioButtonList ID="uiFormato" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table">
            <asp:ListItem Value="PDF" Text="PDF" Selected="True" />
            <asp:ListItem Value="EXCEL" Text="EXCEL" />
        </asp:RadioButtonList>
        <br />
        <br />
        <asp:Button ID="uiGuardar" runat="server" Text="Guardar" CssClass="button" OnClick="uiGuardar_Click" />
    </p>
</asp:Content>
