﻿using System;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using BSS.SIT.Common;
using BSS.SIT.Data.Model;
using BSS.SIT.Web.Code;

namespace BSS.SIT.Web
{
    /// <summary>
    /// Pagina del leedger account
    /// </summary>
    public partial class ViewLedgerAccount : System.Web.UI.Page, IPage
    {
        #region Eventos
        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    LoadUI();
                }
                catch (Exception ex)
                {
                    Master.Message = Utility.ShowError(this, ex);
                }
            }
        }

        /// <summary>
        /// Handles the Click event of the uiBuscar control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void uiBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Process();
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }

        /// <summary>
        /// Handles the RowCommand event of the uiResults control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Web.UI.WebControls.GridViewCommandEventArgs"/> instance containing the event data.</param>
        protected void uiResults_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                int index = Assign.ToInt(e.CommandArgument);
                int id = Assign.ToInt(uiResults.DataKeys[index].Value);

                if (e.CommandName == "Edit1")
                    Edit(id);
                else if (e.CommandName == "Delete1")
                    Delete(id);
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }

        /// <summary>
        /// Handles the Click event of the uiEliminarMasivo control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void uiEliminarMasivo_Click(object sender, EventArgs e)
        {
            try
            {
                DeleteMassive();
            }
            catch (Exception ex)
            {
                Master.Message = Utility.ShowError(this, ex);
            }
        }
        #endregion

        #region IPage Members
        /// <summary>
        /// Loads the data.
        /// </summary>
        public void LoadData()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the UI.
        /// </summary>
        public void LoadUI()
        {
            //Carga los combos
            using (SitDb db = new SitDb())
            {
                var listaLedgers = db.LedgerBooks
                    .Select(x => new { Id = x.Id, Desc = string.Format("{0} - {1} - {2} - {3} - {4}", x.Ledger, x.Book, x.ChartAccount, x.Ano, x.CompaniaId) }).ToList();
                uiLedgerBook.DataSource = listaLedgers;
                uiLedgerBook.DataValueField = "Id";
                uiLedgerBook.DataTextField = "Desc";
                uiLedgerBook.DataBind();
                uiLedgerBook.Items.Insert(0, new ListItem("[Seleccione]", string.Empty));

                var listaClasificacion = db.ClasificacionCuentas.Select(x => new { Id = x.Id, Descripcion = string.Format("{0} - {1}", x.Codigo, x.Descripcion) }).ToList();
                uiClasificacion.DataSource = listaClasificacion;
                uiClasificacion.DataValueField = "Id";
                uiClasificacion.DataTextField = "Descripcion";
                uiClasificacion.DataBind();
                uiClasificacion.Items.Insert(0, new ListItem("[Todas]", string.Empty));

                var listaCuenta = db.AccountDetails.OrderBy(x => x.SVSGVL).Select(x => new { Id = x.SVSGVL, Descripcion = string.Format("{0} - {1}", x.SVSGVL, x.SVLDES) }).ToList();

                uiCuentaInicial.DataSource = listaCuenta;
                uiCuentaInicial.DataValueField = "Id";
                uiCuentaInicial.DataTextField = "Descripcion";
                uiCuentaInicial.DataBind();
                uiCuentaInicial.Items.Insert(0, new ListItem("[Seleccione]", string.Empty));

                uiCuentaFinal.DataSource = listaCuenta;
                uiCuentaFinal.DataValueField = "Id";
                uiCuentaFinal.DataTextField = "Descripcion";
                uiCuentaFinal.DataBind();
                uiCuentaFinal.Items.Insert(0, new ListItem("[Seleccione]", string.Empty));

                uiEliminarMasivo.Visible = false;
            }
        }

        /// <summary>
        /// Loads the object.
        /// </summary>
        public void LoadObject()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Loads the element.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="index">The index.</param>
        public void LoadElement<T>(T index)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Processes this instance.
        /// </summary>
        public void Process()
        {
            //Genera la consulta segun los filtros de busqueda
            using (SitDb db = new SitDb())
            {
                bool? esTiff;
                if(uiTif.SelectedValue.Length == 0)
                    esTiff = null;
                else if(uiTif.SelectedValue.Equals("1"))
                    esTiff = true;
                else
                    esTiff = false;

                var lista = db.FilterLedgerAccount(Assign.ToInt(uiLedgerBook.SelectedValue), uiCuentaInicial.SelectedValue, uiCuentaFinal.SelectedValue, Assign.ToInt(uiClasificacion.SelectedValue), esTiff).ToList();
                uiResults.DataSource = lista;
                uiResults.DataBind();

                uiEliminarMasivo.Visible = (lista.Count > 0);
            }
        }

        /// <summary>
        /// Validates the form.
        /// </summary>
        /// <returns></returns>
        public bool ValidateForm()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Metodos privados
        private void Edit(int id)
        {
            string url = string.Format("EditLedgerAccount.aspx?Id={0}", id);
            Response.Redirect(url, false);
        }

        private void Delete(int id)
        {
            using (SitDb db = new SitDb())
            {
                var item = db.LedgerAccounts.SingleOrDefault(x => x.Id == id);
                db.LedgerAccounts.DeleteOnSubmit(item);
                db.SubmitChanges();
            }
            Process();
            Master.Message = "Registro eliminado exitosamente";
        }

        private void DeleteMassive()
        {
            using (SitDb db = new SitDb())
            {

                foreach (GridViewRow row in uiResults.Rows)
                {
                    bool isChecked = ((HtmlInputCheckBox)row.FindControl("uiSeleccion")).Checked;
                    if (isChecked)
                    {
                        int id = Assign.ToInt(uiResults.DataKeys[row.RowIndex].Value);
                        var item = db.LedgerAccounts.SingleOrDefault(x => x.Id == id);
                        db.LedgerAccounts.DeleteOnSubmit(item);
                    }
                }
                db.SubmitChanges();
            }
            Process();
            Master.Message = "Registros eliminados exitosamente";
        }
        #endregion
    }
}