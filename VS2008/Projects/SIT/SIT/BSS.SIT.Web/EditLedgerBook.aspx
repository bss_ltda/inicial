﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainSite.Master" AutoEventWireup="true" CodeBehind="EditLedgerBook.aspx.cs" Inherits="BSS.SIT.Web.EditLedgerBook" %>

<%@ MasterType VirtualPath="~/MainSite.Master" %>
<asp:Content ID="uiEncabezado" ContentPlaceHolderID="uiHeader" runat="server">
</asp:Content>
<asp:Content ID="uiContenido" ContentPlaceHolderID="uiContentPlaceHolder" runat="server">
    <script type="text/javascript">
        function validarReferenciaTercero() {
            if ($('#ctl00_uiContentPlaceHolder_uiReferencia1').is(':checked')) {
                $('#ctl00_uiContentPlaceHolder_uiTercero').attr('disabled', 'disabled');
                $('#ctl00_uiContentPlaceHolder_uiTerceroReferencia').removeAttr('disabled');
                $('#ctl00_uiContentPlaceHolder_uiTerceroReferencia').removeClass('readonly');

                $('#ctl00_uiContentPlaceHolder_uiTercero').attr('selectedIndex', '0');
            }
            else {
                $('#ctl00_uiContentPlaceHolder_uiTercero').removeAttr('disabled');
                $('#ctl00_uiContentPlaceHolder_uiTerceroReferencia').attr('disabled', 'disabled');
                $('#ctl00_uiContentPlaceHolder_uiTerceroReferencia').addClass('readonly');

                $('#ctl00_uiContentPlaceHolder_uiTerceroReferencia').attr('selectedIndex', '0');
            }
        }

        function seleccionarBook(valor) {
            if (valor.length != 0) {
                var arreglo = valor.split('|');
                $('#ctl00_uiContentPlaceHolder_uiBookId').val(arreglo[0]);
                $('#ctl00_uiContentPlaceHolder_uiBookYear').val(arreglo[1]);
            }
            else {
                $('#ctl00_uiContentPlaceHolder_uiBookId').val('');
                $('#ctl00_uiContentPlaceHolder_uiBookYear').val('');
            }
        }

        $(function () {
            validarReferenciaTercero();
        }); 
    </script>
    <a id="TemplateInfo"></a>
    <h1>
        Editar Libro</h1>
<%--    <asp:UpdatePanel ID="updatePanelForm" runat="server">
        <ContentTemplate>
--%>            <p>
                <label>
                    Ledger</label>
                <asp:DropDownList ID="uiLedger" runat="server" AutoPostBack="True" OnSelectedIndexChanged="uiLedger_SelectedIndexChanged">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiLedger" runat="server" ErrorMessage="*" ControlToValidate="uiLedger" Display="Dynamic" />
                <label>
                    Chart of Account</label>
                <asp:DropDownList ID="uiChartAccount" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiChartAccount" runat="server" ErrorMessage="*" ControlToValidate="uiChartAccount" Display="Dynamic" />
                <label>
                    Book</label>
                <asp:DropDownList ID="uiBook" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiBook" runat="server" ErrorMessage="*" ControlToValidate="uiBook" Display="Dynamic" />
                <asp:TextBox ID="uiBookId" runat="server" CssClass="readonly" />
                <asp:TextBox ID="uiBookYear" runat="server" CssClass="readonly" Columns="5" />
                <label>
                    Compania</label>
                <asp:DropDownList ID="uiCompaniaId" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiCompaniaId" runat="server" ErrorMessage="*" ControlToValidate="uiCompaniaId" Display="Dynamic" />
                <label>
                    Segmento Compania</label>
                <asp:DropDownList ID="uiCompania" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiCompania" runat="server" ErrorMessage="*" ControlToValidate="uiCompania" Display="Dynamic" />
                <label>
                    Segmento Cuenta</label>
                <asp:DropDownList ID="uiCuenta" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiCuenta" runat="server" ErrorMessage="*" ControlToValidate="uiCuenta" Display="Dynamic" />
                <label>
                    Segmento Centro de Costo</label>
                <asp:DropDownList ID="uiCentroCosto" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiCentroCosto" runat="server" ErrorMessage="*" ControlToValidate="uiCentroCosto" Display="Dynamic" />
                <label>
                    Segmento Tercero</label>
                <asp:DropDownList ID="uiTercero" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                </asp:DropDownList>
                <input id="uiReferencia1" type="checkbox" runat="server" onclick="validarReferenciaTercero();" />
                <asp:DropDownList ID="uiTerceroReferencia" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True" />
                    <asp:ListItem Value="1" Text="Reference 1" />
                    <asp:ListItem Value="2" Text="Reference 2" />
                </asp:DropDownList>
                <label>
                    Segmento Tasa Impuesto</label>
                <asp:DropDownList ID="uiTasaImpuesto" runat="server">
                    <asp:ListItem Value="" Text="[Seleccione]" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="1" Text="Reference 1" />
                    <asp:ListItem Value="2" Text="Reference 2" />
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="Req_uiTasaImpuesto" runat="server" ErrorMessage="*" ControlToValidate="uiTasaImpuesto" Display="Dynamic" />
                <br />
                <br />
                <asp:Button ID="uiGuardar" runat="server" Text="Guardar" CssClass="button" OnClick="uiGuardar_Click" />
            </p>
<%--        </ContentTemplate>
    </asp:UpdatePanel>
--%></asp:Content>
