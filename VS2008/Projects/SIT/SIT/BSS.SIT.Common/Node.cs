﻿using System.Collections.Generic;
using System.Linq;

namespace BSS.SIT.Common
{
    /// <summary>
    /// Clase BSS.SIT.Common.Node
    /// </summary>
    public class Node
    {
        #region Propiedades
        /// <summary>
        /// Obtiene o establece value
        /// </summary>
        public string Value { get; set; }
        /// <summary>
        /// Obtiene o establece text
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// Obtiene o establece navigate URL
        /// </summary>
        public string NavigateUrl { get; set; }
        /// <summary>
        /// Obtiene o establece child nodes
        /// </summary>
        public IList<Node> ChildNodes { get; set; }
        /// <summary>
        /// Gets the has children.
        /// </summary>
        public bool HasChildren { get { return ChildNodes.Any(); } }
        #endregion

        #region Constructor
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="BSS.SIT.Common.Node"/>
        /// </summary>
        public Node()
        {
            ChildNodes = new List<Node>();
        }
        #endregion
    }
}