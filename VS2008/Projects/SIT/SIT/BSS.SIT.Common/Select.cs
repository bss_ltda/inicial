﻿
namespace BSS.SIT.Common
{
    /// <summary>
    /// Clase de seleccion para combos
    /// </summary>
    /// <typeparam name="T1">The type of the 1.</typeparam>
    /// <typeparam name="T2">The type of the 2.</typeparam>
    public class Select<T1, T2>
    {
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>The code.</value>
        public T1 Code { get; set; }
        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        /// <value>The description.</value>
        public T2 Description { get; set; }
    }
}
