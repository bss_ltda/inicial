﻿
using System;
namespace BSS.SIT.Common
{
    /// <summary>
    /// Class of assign
    /// </summary>
    public class Assign
    {
        /// <summary>
        /// Toes the int.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static int ToInt<T>(T value)
        {
            int result = 0;
            if (value != null)
            {
                int.TryParse(value.ToString(), out result);
            }
            return result;
        }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public static string ToString<T>(T value)
        {
            if (value != null)
                return value.ToString();
            return string.Empty;
        }

        /// <summary>
        /// to report
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        /// <returns>
        /// System.Object
        /// </returns>
        public static Object ToReport<T>(T value)
        {
            if (value == null)
                return DBNull.Value;
            return value;
        }

        /// <summary>
        /// real type
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>
        /// System.Type
        /// </returns>
        public static Type RealType(Type type)
        {
            if ((type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>))))
                return Nullable.GetUnderlyingType(type);
            return type;
        }

        /// <summary>
        /// to bool
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">The value.</param>
        /// <returns>
        /// System.Boolean
        /// </returns>
        public static bool ToBool<T>(T value)
        {
            bool result = false;
            if (value != null)
            {
                bool.TryParse(value.ToString(), out result);
            }
            return result;
        }

        /// <summary>
        /// to string date
        /// </summary>
        /// <param name="nullable">The nullable.</param>
        /// <returns>
        /// System.String
        /// </returns>
        public static string ToStringDate(DateTime? nullable)
        {
            if (nullable.HasValue)
                return nullable.Value.ToShortDateString();
            return string.Empty;
        }

        /// <summary>
        /// to date time nullable
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        /// System.Nullable&lt;System.DateTime&gt;
        /// </returns>
        public static DateTime? ToDateTimeNullable(string value)
        {
            DateTime result;
            if (value != null)
            {
                if (DateTime.TryParse(value, out result))
                    return result;
            }
            return null;
        }

        /// <summary>
        /// to decimal
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns>
        /// System.Decimal
        /// </returns>
        public static decimal ToDecimal(string value)
        {
            decimal result = 0;
            if (value != null)
            {
                decimal.TryParse(value.ToString(), out result);
            }
            return result;
        }

        /// <summary>
        /// to string combo
        /// </summary>
        /// <param name="objeto">The objeto.</param>
        /// <returns>
        /// System.String
        /// </returns>
        public static string ToStringCombo(int objeto)
        {
            if (objeto != 0)
                return objeto.ToString();
            return string.Empty;
        }
    }
}