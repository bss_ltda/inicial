﻿using System;
using System.Collections.Generic;
using System.Data;

namespace BSS.SIT.Common
{
    /// <summary>
    /// Clase de metodos utiles
    /// </summary>
    public class Utility
    {
        #region Miembros
        /// <summary>
        /// Tiempo de espera de los comandos
        /// </summary>
        public const int CommandTimeout = 0;
        #endregion

        #region Metodos
        /// <summary>
        /// Shows the error.
        /// </summary>
        /// <param name="objeto">The objeto.</param>
        /// <param name="ex">The ex.</param>
        /// <returns></returns>
        public static string ShowError(Object objeto, Exception ex)
        {
            return string.Format("Ha ocurrido el siguiente error: {0}", ex.Message);
        }

        /// <summary>
        /// convert to data set
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list.</param>
        /// <returns>
        /// System.Data.DataSet
        /// </returns>
        public static DataSet ConvertToDataSet<T>(IList<T> list)
        {
            if (list == null)
            {
                return null;
            }

            DataSet ds = new DataSet();
            DataTable dt = new DataTable(typeof(T).Name);
            DataColumn column;
            DataRow row;

            System.Reflection.PropertyInfo[] propertyInfo = typeof(T).GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);

            //Si viene vacia solo genera los campos
            if (list.Count <= 0)
            {
                for (int i = 0, j = propertyInfo.Length; i < j; i++)
                {
                    System.Reflection.PropertyInfo pInfo = propertyInfo[i];
                    string name = pInfo.Name;
                    if (dt.Columns[name] == null)
                    {
                        column = new DataColumn(name, Assign.RealType(pInfo.PropertyType));
                        dt.Columns.Add(column);
                    }
                }
            }
            else
            {
                //Genera los campos y agrega los valores
                foreach (T t in list)
                {
                    if (t == null)
                    {
                        continue;
                    }
                    row = dt.NewRow();
                    for (int i = 0, j = propertyInfo.Length; i < j; i++)
                    {
                        System.Reflection.PropertyInfo pInfo = propertyInfo[i];
                        string name = pInfo.Name;
                        if (dt.Columns[name] == null)
                        {
                            column = new DataColumn(name, Assign.RealType(pInfo.PropertyType));
                            dt.Columns.Add(column);
                        }

                        row[name] = Assign.ToReport(pInfo.GetValue(t, null));
                    }
                    dt.Rows.Add(row);
                }
            }
            ds.Tables.Add(dt);
            return ds;
        }
        #endregion

        #region Enums
        /// <summary>
        /// Obtiene o establece exportar
        /// </summary>
        public enum Exportar
        {
            /// <summary>
            /// Exporta  a Formato Excel
            /// </summary>
            EXCEL,
            /// <summary>
            /// Exporta  a Formato PDF
            /// </summary>
            PDF
        }

        /// <summary>
        /// Configuraciones
        /// </summary>
        public enum Configuracion
        {
            /// <summary>
            /// Base de datos de DB2
            /// </summary>
            BASEDATOS,
            /// <summary>
            /// Libreria de consultas
            /// </summary>
            LIBRERIA,
            /// <summary>
            /// llave para el proceso de maestros
            /// </summary>
            PROCESOMAESTRO,
            /// <summary>
            /// llave para el proceso de terceros
            /// </summary>
            PROCESOTERCERO,
            /// <summary>
            /// campo de informacion del proceso del tercero
            /// </summary>
            PRINCIPALCLIENTE,
            /// <summary>
            /// campo principal del proveedor
            /// </summary>
            PRINCIPALPROVEEDOR,
            /// <summary>
            /// campo de informacion del proceso del movimiento
            /// </summary>
            PROCESOMOVIMIENTO,
            /// <summary>
            /// Indica el id del ledger a procesar en el job
            /// </summary>
            PROCESOMOVIMIENTOLEDGERBOOK,
            /// <summary>
            /// Indica el periodo del ledger a procesar en el job
            /// </summary>
            PROCESOMOVIMIENTOPERIODO,
            /// <summary>
            /// campo de informacion del proceso de cuentas
            /// </summary>
            PROCESOCUENTA,
            /// <summary>
            /// Indica el id del ledger a procesar en el job
            /// </summary>
            PROCESOSALDOCUENTALEDGERBOOK,
            /// <summary>
            /// Mensaje de ICA
            /// </summary>
            MENSAJEICA,
            /// <summary>
            /// campo de informacion del proceso de cuentas
            /// </summary>
            PROCESOSALDOOTROS,
            /// <summary>
            /// Indica el id del ledger a procesar en el job
            /// </summary>
            PROCESOSALDOOTROSLEDGERBOOK,
            /// <summary>
            /// Parametro de resumido
            /// </summary>
            PARAMRESUMIDO,
            /// <summary>
            /// Parametro de extranjero
            /// </summary>
            PARAMNITEXTRANJERO
        }
        #endregion
    }
}