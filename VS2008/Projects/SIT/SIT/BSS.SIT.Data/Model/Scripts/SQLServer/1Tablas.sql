﻿USE SIT
GO

--TABLAS
IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'Rol') BEGIN
	DROP TABLE dbo.Rol
END
GO

CREATE TABLE dbo.Rol
(
	Id int IDENTITY(1,1) NOT NULL,
	Descripcion varchar(100) NULL,
 CONSTRAINT PK_Rol PRIMARY KEY NONCLUSTERED (Id ASC) ON [PRIMARY]
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'Usuario') BEGIN
	DROP TABLE dbo.Usuario
END
GO

CREATE TABLE dbo.Usuario
(
	Id int NOT NULL IDENTITY(1,1),
	NombreCompleto varchar(200) NOT NULL, 
	NombreUsuario varchar(50) NOT NULL, 
	Clave varchar(50) NOT NULL, 
	RolId INT NOT NULL,
	Activo BIT DEFAULT(1) NULL,
	CONSTRAINT PK_Usuario PRIMARY KEY NONCLUSTERED (Id ASC) ON [PRIMARY]
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'Menu') BEGIN
	DROP TABLE dbo.Menu
END
GO

CREATE TABLE dbo.Menu
(
	Id int IDENTITY(1,1) NOT NULL,
	Descripcion varchar(50) NULL,
	PadreId int NULL,
	Posicion int NULL,
	Url varchar(50) NULL,
	FechaActualizacion datetime NULL,
	UsuarioIdActualizacion int NULL,
 CONSTRAINT PK_Menu PRIMARY KEY NONCLUSTERED (Id ASC) ON [PRIMARY]
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'RolMenu') BEGIN
	DROP TABLE dbo.RolMenu
END
GO

CREATE TABLE dbo.RolMenu
(
	RolId int NOT NULL,
	MenuId int NOT NULL,
	CONSTRAINT PK_RolMenu PRIMARY KEY NONCLUSTERED (RolId ASC, MenuId ASC) ON [PRIMARY]
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'Configuracion') BEGIN
	DROP TABLE dbo.Configuracion
END
GO

CREATE TABLE dbo.Configuracion
(
	Id int IDENTITY(1,1) NOT NULL,
	Llave varchar(50) NULL,
	Valor varchar(1000) NULL,
	CONSTRAINT PK_Configuracion PRIMARY KEY NONCLUSTERED (Id ASC) ON [PRIMARY]
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'LedgerBook') BEGIN
	DROP TABLE dbo.LedgerBook
END
GO

CREATE TABLE dbo.LedgerBook
(
	Id int NOT NULL IDENTITY(1,1),
	Ledger varchar(50) NOT NULL, 
	Book varchar(50) NOT NULL, 
	ChartAccount varchar(50) NOT NULL, 
	Ano int not null, 
	CompaniaId varchar(50) NOT NULL, 
	Compania varchar(50) NOT NULL, 
	Cuenta varchar(50) NOT NULL, 
	CentroCosto varchar(50) NOT NULL, 
	Tercero varchar(50) NULL, 
	TerceroReferencia varchar(50) NULL, 
	TasaImpuesto varchar(50) NULL,
	CONSTRAINT PK_LedgerBook PRIMARY KEY NONCLUSTERED (Id ASC) ON [PRIMARY]
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'LedgerDetail') BEGIN
	DROP TABLE dbo.LedgerDetail
END
GO

CREATE TABLE dbo.LedgerDetail (
	DSLDGR varchar(8),
	DSPTAB varchar(10),
	DSCOA varchar(8),
	CSSEQ numeric(3,0),
	CSSGMN varchar(10),
	CSMANF numeric(1,0),
	SGLDES varchar(30),
	SGCOLH varchar(16),
	SGCSEG numeric(1,0)
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'Company') BEGIN
	DROP TABLE dbo.Company
END
GO

CREATE TABLE dbo.Company (
	CODIGO numeric(2,0),
	ID varchar(16),
	DV varchar(16),
	NOMBRE varchar(30),
	DIRECCION varchar(50),
	CODIGOCIUDAD varchar(30),
	CODIGODPTO varchar(30),
	CODIGOPAIS varchar(30)
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'BookDetail') BEGIN
	DROP TABLE dbo.BookDetail
END
GO

CREATE TABLE dbo.BookDetail (
	TYLDGR varchar(8),
	TYBOOK varchar(10),
	TYYEAR int,
	TYLDES varchar(30)
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'ClasificacionCuenta') BEGIN
	DROP TABLE dbo.ClasificacionCuenta
END
GO

CREATE TABLE dbo.ClasificacionCuenta
(
	Id int NOT NULL IDENTITY(1,1),
	Codigo varchar(50) NOT NULL, 
	Descripcion varchar(50) NOT NULL, 
	CONSTRAINT PK_ClasificacionCuenta PRIMARY KEY NONCLUSTERED (Id ASC) ON [PRIMARY]
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'AccountDetail') BEGIN
	DROP TABLE dbo.AccountDetail
END
GO

CREATE TABLE dbo.AccountDetail (
	DSLDGR varchar(8),
	DSLDES varchar(30),
	DSCOA varchar(8),
	CSSGMN varchar(10),
	CSSEQ int,
	SGLDES varchar(30),
	SGCSEG int,
	SVSGVL varchar(16),
	SVLDES varchar(30)
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'LedgerAccount') BEGIN
	DROP TABLE dbo.LedgerAccount
END
GO

CREATE TABLE dbo.LedgerAccount
(
	Id int NOT NULL IDENTITY(1,1),
	LedgerBookId int NOT NULL, 
	Cuenta varchar(50) NOT NULL, 
	Descripcion varchar(50) NOT NULL, 
	Tipo varchar(50) NOT NULL, 
	ClasificacionCuentaId int NOT NULL, 
	Tif bit NOT NULL default(0), 
	CONSTRAINT PK_LedgerAccount PRIMARY KEY NONCLUSTERED (Id ASC) ON [PRIMARY]
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'Comprobante') BEGIN
	DROP TABLE dbo.Comprobante
END
GO

CREATE TABLE dbo.Comprobante
(
	Id int NOT NULL IDENTITY(1,1),
	LedgerBookId int not null, 
	Codigo varchar(2) NOT NULL, 
	CONSTRAINT PK_Comprobante PRIMARY KEY NONCLUSTERED (Id ASC) ON [PRIMARY]
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'Voucher') BEGIN
	DROP TABLE dbo.Voucher
END
GO

CREATE TABLE dbo.Voucher (
	CODIGO varchar(2)
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'ComprobanteManual') BEGIN
	DROP TABLE dbo.ComprobanteManual
END
GO

CREATE TABLE dbo.ComprobanteManual
(
	Id int NOT NULL IDENTITY(1,1),
	LedgerBookId int not null, 
	Codigo varchar(2) NOT NULL, 
	ValidaPC varchar(1) NULL,
	CONSTRAINT PK_ComprobanteManual PRIMARY KEY NONCLUSTERED (Id ASC) ON [PRIMARY]
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'Zrc') BEGIN
	DROP TABLE dbo.Zrc
END
GO

CREATE TABLE dbo.Zrc (
	RCCRTE numeric(7,4),
	RCNRTE numeric(7,4),
	RCDESC varchar(30),
	RCRTCD varchar(5),
	RCEDTE numeric(8,0)
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'TempTercero') BEGIN
	DROP TABLE dbo.TempTercero
END
GO

CREATE TABLE dbo.TempTercero (
	IDENTIFICADOR varchar(2),
	TIPOTERCERO varchar(1),
	CODIGO numeric(8,0),
	SECUENCIA int,
	IDENTIFICACION varchar(16),
	COMPANIA numeric(2,0),
	RAZONSOCIAL varchar(50),
	CODIGOPAIS varchar(30),
	CODIGOCIUDAD varchar(30),
	CODIGODPTO varchar(30),
	ESPRINCIPAL varchar(16),
	DIRECCION varchar(152)
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'Tercero') BEGIN
	DROP TABLE dbo.Tercero
END
GO

CREATE TABLE dbo.Tercero
(
	Id int NOT NULL IDENTITY(1,1),
	Identificador varchar(50) NULL, 
	TipoTercero varchar(1) NOT NULL, 
	Codigo decimal(18, 0) NOT NULL, 
	Secuencia int NULL,
	TipoIdentificacion varchar(1) NULL, 
	CodigoFiscal varchar(50) NULL, 
	Digito varchar(1) NULL, 
	Compania varchar(50) NULL, 
	RazonSocial varchar(200) NULL, 
	CodigoPais varchar(4) NULL, 
	CodigoCiudad varchar(3) NULL, 
	CodigoDpto varchar(2) NULL, 
	Direccion varchar(200) NULL, 
	ClienteOficial bit NULL, 
	CONSTRAINT PK_Tercero PRIMARY KEY NONCLUSTERED (Id ASC) ON [PRIMARY]
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'ErrorTercero') BEGIN
	DROP TABLE dbo.ErrorTercero
END
GO

CREATE TABLE dbo.ErrorTercero
(
	Id int NOT NULL IDENTITY(1,1),
	Identificador varchar(50) NULL, 
	TipoTercero varchar(1) NOT NULL, 
	Codigo varchar(50) NOT NULL, 
	Secuencia int NULL,
	TipoIdentificacion varchar(1) NULL, 
	CodigoFiscal varchar(50) NULL, 
	Digito varchar(1) NULL, 
	Compania varchar(50) NULL, 
	RazonSocial varchar(200) NULL, 
	CodigoPais varchar(4) NULL, 
	CodigoCiudad varchar(3) NULL, 
	CodigoDpto varchar(2) NULL, 
	ClienteOficial bit NULL, 
	Direccion varchar(200) NULL, 
	Mensaje varchar(max) NULL, 
	CodigoError varchar(50) NULL, 
	CONSTRAINT PK_ErrorTercero PRIMARY KEY NONCLUSTERED (Id ASC) ON [PRIMARY]
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'Ciudad') BEGIN
	DROP TABLE dbo.Ciudad
END
GO

CREATE TABLE dbo.Ciudad
(
	Id int NOT NULL IDENTITY(1,1),
	PaisId varchar (200) NOT NULL,
	Pais varchar(200) NOT NULL,
	DptoId varchar(200) NOT NULL, 
	Departamento varchar(200) NOT NULL, 
	CiudadId varchar(200) NOT NULL, 
	Ciudad varchar(200) NOT NULL, 
	CONSTRAINT PK_Ciudad PRIMARY KEY NONCLUSTERED (Id ASC) ON [PRIMARY]
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'TempMovimiento') BEGIN
	DROP TABLE dbo.TempMovimiento
END
GO

CREATE TABLE dbo.TempMovimiento(
	COMPROBANTELINEA decimal(7, 0) NOT NULL,
	COMPROBANTENUMERO varchar(50) NOT NULL,
	COMPROBANTEFECHA decimal(8, 0) NOT NULL,
	DOCUMENTONUMERO varchar(50) NOT NULL,
	DOCUMENTOFECHA decimal(8, 0) NOT NULL,
	DESCRIPCIONTRANSACCION varchar(50) NOT NULL,
	CODIGORAZON varchar(50) NOT NULL,
	VALOR decimal(16, 2) NOT NULL,
	VALORDEBITO decimal(15, 2) NOT NULL,
	VALORCREDITO decimal(15, 2) NOT NULL,
	SEGMENTOCOMPANIA varchar(50) NULL,
	SEGMENTOCUENTA varchar(50) NULL,
	SEGMENTOCENTROCOSTO varchar(50) NULL,
	SEGMENTOTERCERO varchar(50) NULL,
	TASAIMPUESTO varchar(50) NULL,
	TIPOCOMPROBANTE decimal(2, 0) NOT NULL,
	DOCUMENTOFECHAENCABEZADO decimal(8, 0) NOT NULL
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'Movimiento') BEGIN
	DROP TABLE dbo.Movimiento
END
GO

CREATE TABLE dbo.Movimiento
(
	Id int NOT NULL IDENTITY(1,1),
	LedgerBookId int NOT NULL,
	Periodo int NULL, 
	ComprobanteLinea int NULL, 
	ComprobanteNumero varchar(50) NULL, 
	ComprobanteFecha varchar(50) NULL, 
	DocumentoNumero varchar(50) NULL, 
	DocumentoFecha varchar(50) NULL, 
	DescripcionTransaccion varchar(500) NULL, 
	CodigoRazon varchar(50) NULL, 
	Valor money NULL, 
	SegmentoCompania varchar(50) NULL, 
	SegmentoCuenta varchar(50) NULL, 
	SegmentoCentroCosto varchar(50) NULL, 
	SegmentoTercero decimal(18, 0) NULL, 
	TasaImpuesto varchar(50) NULL, 
	ImpuestoRetencion decimal(18,2) NULL, 
	ImpuestoDescripcion varchar(30) NULL, 
	ValorBase money NULL, 
	ValorDebito money NULL, 
	ValorCredito money NULL, 
	TipoTercero varchar(1) NULL, 
	CONSTRAINT PK_Movimiento PRIMARY KEY NONCLUSTERED (Id ASC) ON [PRIMARY]
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'ErrorMvto') BEGIN
	DROP TABLE dbo.ErrorMvto
END
GO

CREATE TABLE dbo.ErrorMvto
(
	Id int NOT NULL IDENTITY(1,1),
	LedgerBookId int NOT NULL,
	Periodo int NULL, 
	ComprobanteLinea int NULL, 
	ComprobanteNumero varchar(50) NULL, 
	ComprobanteFecha varchar(50) NULL, 
	DocumentoNumero varchar(50) NULL, 
	DocumentoFecha varchar(50) NULL, 
	DescripcionTransaccion varchar(500) NULL, 
	CodigoRazon varchar(50) NULL, 
	Valor money NULL, 
	SegmentoCompania varchar(50) NULL, 
	SegmentoCuenta varchar(50) NULL, 
	SegmentoCentroCosto varchar(50) NULL, 
	SegmentoTercero varchar(50) NULL, 
	TasaImpuesto varchar(50) NULL, 
	ImpuestoRetencion decimal(18,2) NULL, 
	ImpuestoDescripcion varchar(30) NULL, 
	ValorBase money NULL, 
	ValorDebito money NULL, 
	ValorCredito money NULL, 
	TipoTercero varchar(1) NULL, 
	Mensaje varchar(max) NULL, 
	CodigoError varchar(50) NULL, 
	CONSTRAINT PK_ErrorMvto PRIMARY KEY NONCLUSTERED (Id ASC) ON [PRIMARY]
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'SaldoCxP') BEGIN
	DROP TABLE dbo.SaldoCxP
END
GO

CREATE TABLE dbo.SaldoCxP
(
	CodigoCompania varchar(50) NULL,
	CodigoTercero varchar(50) NULL,
	Valor money NULL
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'SaldoCxC') BEGIN
	DROP TABLE dbo.SaldoCxC
END
GO

CREATE TABLE dbo.SaldoCxC
(
	CodigoCompania varchar(50) NULL,
	CodigoTercero varchar(50) NULL,
	Valor money NULL
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'SaldoOtro') BEGIN
	DROP TABLE dbo.SaldoOtro
END
GO

CREATE TABLE dbo.SaldoOtro
(
	LedgerBookId INT NULL, 
	CodigoCuenta varchar(50) NULL,
	ValorDebito money NULL,
	ValorCredito money NULL,
	Valor money NULL
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'FormatoConcepto') BEGIN
	DROP TABLE dbo.FormatoConcepto
END
GO

CREATE TABLE dbo.FormatoConcepto
(
	Id int IDENTITY(1,1) NOT NULL,
	Formato varchar(50) NULL,
	Concepto varchar(50) NULL,
	Descripcion varchar(MAX) NULL,
	Declaracion varchar(50) NULL,
 CONSTRAINT PK_FormatoConcepto PRIMARY KEY NONCLUSTERED (Id ASC) ON [PRIMARY]
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'FormatoDian') BEGIN
	DROP TABLE dbo.FormatoDian
END
GO

CREATE TABLE dbo.FormatoDian
(
	Id int IDENTITY(1,1) NOT NULL,
	CodigoFormato varchar(50) NULL,
	Descripcion varchar(max) NULL,
	Fuente varchar(50) NULL,
	Tope money NULL,
	CONSTRAINT PK_FormatoDian PRIMARY KEY NONCLUSTERED (Id ASC) ON [PRIMARY]
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'ComprobanteExcluido') BEGIN
	DROP TABLE dbo.ComprobanteExcluido
END
GO

CREATE TABLE dbo.ComprobanteExcluido
(
	Id int IDENTITY(1,1) NOT NULL,
	CodigoFormato varchar(50) NULL,
	Concepto varchar(50) NULL,
	Comprobante varchar(50) NULL,
 CONSTRAINT PK_ComprobanteExcluido PRIMARY KEY NONCLUSTERED (id ASC)  ON [PRIMARY]
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'MedioMagneticoDian') BEGIN
	DROP TABLE dbo.MedioMagneticoDian
END
GO

CREATE TABLE dbo.MedioMagneticoDian
(
	Id int IDENTITY(1,1) NOT NULL,
	Formato varchar(50) NULL,
	Concepto varchar(50) NULL,
	Cuentas varchar(50) NULL,
	Tipo varchar(50) NULL,
	Valor int NULL,
	LedgerBookId int NULL,
	CONSTRAINT PK_MedioMagneticoDian PRIMARY KEY NONCLUSTERED (Id ASC) ON [PRIMARY]
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'PagoImpuesto') BEGIN
	DROP TABLE dbo.PagoImpuesto
END
GO

CREATE TABLE dbo.PagoImpuesto
(
	Id INT NOT NULL IDENTITY(1,1),
	LedgerBookId INT NULL, 
	Bimestre INT NOT NULL,
	NumeroPago varchar(50) NULL, 
	NumeroDeclaracion varchar(50) NULL,
	FechaDeclaracion DATETIME NULL, 
	Sticker varchar(50) NULL, 
	FechaPago DATETIME NULL,
	CONSTRAINT PK_PagoImpuesto PRIMARY KEY NONCLUSTERED (Id ASC) ON [PRIMARY]
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'U' AND NAME = 'Bimestre') BEGIN
	DROP TABLE dbo.Bimestre
END
GO

CREATE TABLE dbo.Bimestre
(
	Id int IDENTITY(1,1) NOT NULL,
	Codigo int NULL,
	Periodo int NULL,
	Descripcion varchar(50) NULL,
 CONSTRAINT PK_Bimestre PRIMARY KEY NONCLUSTERED (Id ASC) ON [PRIMARY]
)
GO

--RELACIONES
IF(OBJECT_ID('FK_Usuario_Rol') IS NULL) BEGIN
	ALTER TABLE Usuario
	WITH CHECK ADD CONSTRAINT FK_Usuario_Rol
	FOREIGN KEY(RolId) REFERENCES Rol(Id)
END 
GO

IF(OBJECT_ID('FK_RolMenu_Rol') IS NULL) BEGIN
	ALTER TABLE RolMenu
	WITH CHECK ADD CONSTRAINT FK_RolMenu_Rol
	FOREIGN KEY(RolId) REFERENCES Rol(Id)
END 
GO

IF(OBJECT_ID('FK_RolMenu_Menu') IS NULL) BEGIN
	ALTER TABLE RolMenu
	WITH CHECK ADD CONSTRAINT FK_RolMenu_Menu
	FOREIGN KEY(MenuId) REFERENCES Menu(Id)
END 
GO

IF(OBJECT_ID('FK_Comprobante_LedgerBook') IS NULL) BEGIN
	ALTER TABLE Comprobante
	WITH CHECK ADD CONSTRAINT FK_Comprobante_LedgerBook
	FOREIGN KEY(LedgerBookId) REFERENCES LedgerBook(Id)
END 
GO

IF(OBJECT_ID('FK_LedgerAccount_LedgerBook') IS NULL) BEGIN
	ALTER TABLE LedgerAccount
	WITH CHECK ADD CONSTRAINT FK_LedgerAccount_LedgerBook
	FOREIGN KEY(LedgerBookId) REFERENCES LedgerBook(Id)
END 
GO

IF(OBJECT_ID('FK_LedgerAccount_ClasificacionCuenta') IS NULL) BEGIN
	ALTER TABLE LedgerAccount
	WITH CHECK ADD CONSTRAINT FK_LedgerAccount_ClasificacionCuenta
	FOREIGN KEY(ClasificacionCuentaId) REFERENCES ClasificacionCuenta(Id)
END 
GO

--INDICES
CREATE NONCLUSTERED INDEX [IX_ComprobanteManual] ON [dbo].[ComprobanteManual] 
(
	[LedgerBookId] ASC,
	[Codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Zrc] ON [dbo].[Zrc] 
(
	[RCRTCD] ASC,
	[RCEDTE] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Tercero] ON [dbo].[Tercero] 
(
	[TipoTercero] ASC,
	[Codigo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_LedgerAccount] ON [dbo].[LedgerAccount] 
(
	[LedgerBookId] ASC,
	[Cuenta] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Movimiento] ON [dbo].[Movimiento] 
(
	[LedgerBookId] ASC,
	[Periodo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
