﻿IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'P' AND NAME = 'Formato1009') BEGIN
	DROP PROCEDURE dbo.Formato1009
END
GO

CREATE PROCEDURE dbo.Formato1009

	@LedgerBookId INT
AS 
SET NOCOUNT ON

DECLARE 
	@Concepto INT
	, @Formato NVARCHAR(50)
	, @CompaniaId NVARCHAR(50)
	, @EsResumido BIT 
	, @EsConsecutivo BIT
	, @Tope MONEY
SET @Formato = '1009'

--Trae las parametricas del reporte
SELECT TOP 1 @Concepto = Concepto FROM MedioMagneticoDian WHERE (Formato = @Formato) AND (LedgerBookId = @LedgerBookId)
SELECT @CompaniaId = CompaniaId FROM LedgerBook WHERE (Id = @LedgerBookId)
SELECT @EsResumido = CASE WHEN Valor = 'True' THEN 1 ELSE 0 END  FROM Configuracion WHERE (Llave = 'ParamResumido')
SELECT @EsConsecutivo = CASE WHEN Valor = 'True' THEN 1 ELSE 0 END  FROM Configuracion WHERE (Llave = 'ParamNitExtranjero')
SELECT @Tope = Tope FROM FormatoDian WHERE (CodigoFormato = @Formato)

--Datos de la empresa
DECLARE
	 @IdCompania NVARCHAR(50)
	, @DvCompania NVARCHAR(1)
	, @Nombre NVARCHAR(50)
	, @Direccion NVARCHAR(50)
	, @CodigoCiudad NVARCHAR(50)
	, @CodigoDpto NVARCHAR(50)
	, @CodigoPais NVARCHAR(50)
	, @CompaniaCodigo NVARCHAR(10)

--Trae la informacion de la compania por si se requiere
SELECT @CompaniaCodigo = CompaniaId FROM LedgerBook WHERE (Id = @LedgerBookId)
SELECT @IdCompania = Id, @DvCompania = Dv, @Nombre = Nombre, @Direccion = Direccion, @CodigoCiudad = CodigoCiudad, @CodigoDpto = CodigoDpto, @CodigoPais = CodigoPais FROM Company WHERE (Codigo = @CompaniaCodigo)

DECLARE @id INT, @consecutivo INT

--Genera la tabla resultante
DECLARE @Resultado TABLE(
	id INT IDENTITY(1, 1), 
	cpt nvarchar(50) NULL,
	tdoc int NULL,
	nid nvarchar(50) NULL,
	dv nvarchar(1) NULL,
	apl1 varchar(50) NULL,
	apl2 varchar(50) NULL,
	nom1 varchar(50) NULL,
	nom2 varchar(50) NULL,
	raz nvarchar(200) NULL,
	dir nvarchar(200) NULL,
	dpto nvarchar(2) NULL,
	mun nvarchar(3) NULL,
	pais nvarchar(4) NULL,
	sal money NULL
)

--Para los datos temporales del resumido
DECLARE @ResultadoTemporal TABLE(
	id INT IDENTITY(1, 1), 
	cpt nvarchar(50) NULL,
	tdoc int NULL,
	nid nvarchar(50) NULL,
	dv nvarchar(1) NULL,
	apl1 varchar(50) NULL,
	apl2 varchar(50) NULL,
	nom1 varchar(50) NULL,
	nom2 varchar(50) NULL,
	raz nvarchar(200) NULL,
	dir nvarchar(200) NULL,
	dpto nvarchar(2) NULL,
	mun nvarchar(3) NULL,
	pais nvarchar(4) NULL,
	sal money NULL
)

IF(@EsResumido = 0) BEGIN 
	--DETALLADO
	INSERT INTO @Resultado (
		cpt,
		tdoc,
		nid,
		dv,
		apl1,
		apl2,
		nom1,
		nom2,
		raz,
		dir,
		dpto,
		mun,
		pais,
		sal
	)
	SELECT
		@Concepto AS cpt
		, dbo.ObtenerTipoDocumento(TerceroOficial.TipoIdentificacion, @EsConsecutivo) AS tdoc
		, CASE WHEN (TerceroOficial.TipoIdentificacion = 'E') AND (@EsConsecutivo = 1) THEN SPACE(0)
		  ELSE TerceroOficial.CodigoFiscal
		  END AS nid
		, TerceroOficial.Digito AS dv
		, CASE WHEN TerceroOficial.TipoIdentificacion = 'C' THEN 
		  dbo.ObtenerParteNombre(TerceroOficial.RazonSocial, 'Apellido1') 
		  ELSE SPACE(0)
		  END AS apl1
		, CASE WHEN TerceroOficial.TipoIdentificacion = 'C' THEN 
		  dbo.ObtenerParteNombre(TerceroOficial.RazonSocial, 'Apellido2')
		  ELSE SPACE(0)
		  END AS apl2
		, CASE WHEN TerceroOficial.TipoIdentificacion = 'C' THEN 
		  dbo.ObtenerParteNombre(TerceroOficial.RazonSocial, 'Nombre1') 
		  ELSE SPACE(0)
		  END AS nom1
		, CASE WHEN TerceroOficial.TipoIdentificacion = 'C' THEN 
		  dbo.ObtenerParteNombre(TerceroOficial.RazonSocial, 'Nombre2') 
		  ELSE SPACE(0)
		  END AS nom2
		, CASE WHEN NOT TerceroOficial.TipoIdentificacion = 'C' THEN 
		  TerceroOficial.RazonSocial
		  ELSE SPACE(0)
		  END AS raz
		, TerceroOficial.Direccion AS dir
		, CASE WHEN TerceroOficial.TipoIdentificacion = 'E' THEN SPACE(0)
		  ELSE TerceroOficial.CodigoDpto
		  END AS dpto
		, CASE WHEN TerceroOficial.TipoIdentificacion = 'E' THEN SPACE(0)
		  ELSE TerceroOficial.CodigoCiudad
		  END AS mun
		, TerceroOficial.CodigoPais AS pais
		, SUM(ABS(SaldoCxP.Valor)) AS sal	
	FROM 
		SaldoCxP
	INNER JOIN
		Tercero 
		ON Tercero.TipoTercero = 'P'
		AND Tercero.Codigo = SaldoCxP.CodigoTercero
	INNER JOIN 
		TerceroOficial 
		ON Tercero.TipoTercero = TerceroOficial.TipoTercero
		AND Tercero.CodigoFiscal = TerceroOficial.CodigoFiscal
	GROUP BY
		TerceroOficial.TipoIdentificacion
		, TerceroOficial.CodigoFiscal
		, TerceroOficial.Digito
		, TerceroOficial.RazonSocial
		, TerceroOficial.Direccion
		, TerceroOficial.CodigoDpto
		, TerceroOficial.CodigoCiudad
		, TerceroOficial.CodigoPais
	END
ELSE BEGIN
	--RESUMIDO con TOPE MAYOR O IGUAL
	INSERT INTO @ResultadoTemporal (
		cpt,
		tdoc,
		nid,
		dv,
		apl1,
		apl2,
		nom1,
		nom2,
		raz,
		dir,
		dpto,
		mun,
		pais,
		sal
	)
	SELECT
		@Concepto AS cpt
		, dbo.ObtenerTipoDocumento(TerceroOficial.TipoIdentificacion, @EsConsecutivo) AS tdoc
		, CASE WHEN (TerceroOficial.TipoIdentificacion = 'E') AND (@EsConsecutivo = 1) THEN SPACE(0)
		  ELSE TerceroOficial.CodigoFiscal
		  END AS nid
		, TerceroOficial.Digito AS dv
		, CASE WHEN TerceroOficial.TipoIdentificacion = 'C' THEN 
		  dbo.ObtenerParteNombre(TerceroOficial.RazonSocial, 'Apellido1') 
		  ELSE SPACE(0)
		  END AS apl1
		, CASE WHEN TerceroOficial.TipoIdentificacion = 'C' THEN 
		  dbo.ObtenerParteNombre(TerceroOficial.RazonSocial, 'Apellido2')
		  ELSE SPACE(0)
		  END AS apl2
		, CASE WHEN TerceroOficial.TipoIdentificacion = 'C' THEN 
		  dbo.ObtenerParteNombre(TerceroOficial.RazonSocial, 'Nombre1') 
		  ELSE SPACE(0)
		  END AS nom1
		, CASE WHEN TerceroOficial.TipoIdentificacion = 'C' THEN 
		  dbo.ObtenerParteNombre(TerceroOficial.RazonSocial, 'Nombre2') 
		  ELSE SPACE(0)
		  END AS nom2
		, CASE WHEN NOT TerceroOficial.TipoIdentificacion = 'C' THEN 
		  TerceroOficial.RazonSocial
		  ELSE SPACE(0)
		  END AS raz
		, TerceroOficial.Direccion AS dir
		, CASE WHEN TerceroOficial.TipoIdentificacion = 'E' THEN SPACE(0)
		  ELSE TerceroOficial.CodigoDpto
		  END AS dpto
		, CASE WHEN TerceroOficial.TipoIdentificacion = 'E' THEN SPACE(0)
		  ELSE TerceroOficial.CodigoCiudad
		  END AS mun
		, TerceroOficial.CodigoPais AS pais
		, SUM(ABS(SaldoCxP.Valor)) AS sal	
	FROM 
		SaldoCxP
	INNER JOIN
		Tercero 
		ON Tercero.TipoTercero = 'P'
		AND Tercero.Codigo = SaldoCxP.CodigoTercero
	INNER JOIN 
		TerceroOficial 
		ON Tercero.TipoTercero = TerceroOficial.TipoTercero
		AND Tercero.CodigoFiscal = TerceroOficial.CodigoFiscal
	WHERE
		(ABS(SaldoCxP.Valor) >= @Tope)
	GROUP BY
		TerceroOficial.TipoIdentificacion
		, TerceroOficial.CodigoFiscal
		, TerceroOficial.Digito
		, TerceroOficial.RazonSocial
		, TerceroOficial.Direccion
		, TerceroOficial.CodigoDpto
		, TerceroOficial.CodigoCiudad
		, TerceroOficial.CodigoPais
	
	--RESUMIDO con TOPE MENOR CUANTIAS MENORES
	INSERT INTO @ResultadoTemporal (
		cpt,
		tdoc,
		nid,
		dv,
		apl1,
		apl2,
		nom1,
		nom2,
		raz,
		dir,
		dpto,
		mun,
		pais,
		sal
	)
	SELECT
		@Concepto AS cpt
		, '43' AS tdoc
		, '222222222' AS nid
		, SPACE(0)AS dv
		, SPACE(0) AS apl1
		, SPACE(0) AS apl2
		, SPACE(0) AS nom1
		, SPACE(0) AS nom2
		, 'CUANTIAS MENORES' AS raz
		, @Direccion AS dir
		, @CodigoDpto AS dpto
		, @CodigoCiudad AS mun
		, @CodigoPais AS pais
		, SUM(ABS(SaldoCxP.Valor)) AS sal	
	FROM 
		SaldoCxP
	WHERE 
		(ABS(SaldoCxP.Valor) < @Tope)
		
	--Trae los datos por Tercero
	INSERT INTO @Resultado (
		cpt,
		tdoc,
		nid,
		dv,
		apl1,
		apl2,
		nom1,
		nom2,
		raz,
		dir,
		dpto,
		mun,
		pais,
		sal
	)
	SELECT 
		Tabla.cpt,
		Tabla.tdoc,
		Tabla.nid,
		Tabla.dv,
		Tabla.apl1,
		Tabla.apl2,
		Tabla.nom1,
		Tabla.nom2,
		Tabla.raz,
		Tabla.dir,
		Tabla.dpto,
		Tabla.mun,
		Tabla.pais,
		Tabla.sal AS sal
	FROM 
		@ResultadoTemporal Tabla
	INNER JOIN 
		FormatoConcepto 
		ON FormatoConcepto.Formato = @Formato
		AND Tabla.cpt = FormatoConcepto.Concepto
		AND FormatoConcepto.Declaracion = 'T'
	WHERE 
		(sal > 0)	

	--Trae los datos por Declarante
	INSERT INTO @Resultado (
		cpt,
		tdoc,
		nid,
		dv,
		apl1,
		apl2,
		nom1,
		nom2,
		raz,
		dir,
		dpto,
		mun,
		pais,
		sal
	)
	SELECT 
		Tabla.cpt,
		'31' AS tdoc,
		@IdCompania AS nid,
		@DvCompania AS dv,
		SPACE(0) AS apl1,
		SPACE(0) AS apl2,
		SPACE(0) AS nom1,
		SPACE(0) AS nom2,
		@Nombre AS raz,
		@Direccion AS dir,
		@CodigoDpto AS dpto,
		@CodigoCiudad AS mun,
		@CodigoPais AS pais,
		SUM(Tabla.sal) AS sal
	FROM 
		@ResultadoTemporal Tabla
	INNER JOIN 
		FormatoConcepto 
		ON FormatoConcepto.Formato = @Formato
		AND Tabla.cpt = FormatoConcepto.Concepto
		AND FormatoConcepto.Declaracion = 'D'
	WHERE 
		(sal > 0)	
	GROUP BY 
		Tabla.cpt
END

--Valida si es por consecutivo y recorre los 43 con nit en blanco
IF(@EsConsecutivo = 1) BEGIN 
	--actualiza con un consecutivo el tipo 43
	DECLARE ExtranjeroCursor CURSOR FAST_FORWARD FOR 
	SELECT id FROM @Resultado WHERE (tdoc = 43) AND (LEN(nid) = 0)

	OPEN ExtranjeroCursor
	FETCH NEXT FROM ExtranjeroCursor INTO @id
	
	SET @consecutivo = 1
	WHILE (@@FETCH_STATUS = 0) BEGIN
		IF(@consecutivo < 10)
			UPDATE @Resultado SET nid = '44444400' + CONVERT(VARCHAR, @consecutivo) WHERE (id = @id)
		ELSE 
			UPDATE @Resultado SET nid = '4444440' + CONVERT(VARCHAR, @consecutivo) WHERE (id = @id)
		SET @consecutivo = @consecutivo + 1
		FETCH NEXT FROM ExtranjeroCursor INTO @id
	END

	CLOSE ExtranjeroCursor
	DEALLOCATE ExtranjeroCursor
END 

--presenta el resultado ya validado
SELECT 
	cpt,
	tdoc,
	nid,
	dv,
	apl1,
	apl2,
	nom1,
	nom2,
	raz,
	dir,
	dpto,
	mun,
	pais,
	SUM(sal) AS sal
FROM 
	@Resultado 
WHERE 
	(sal > 0)
GROUP BY
	cpt,
	tdoc,
	nid,
	dv,
	apl1,
	apl2,
	nom1,
	nom2,
	raz,
	dir,
	dpto,
	mun,
	pais
ORDER BY 
	nid
GO
