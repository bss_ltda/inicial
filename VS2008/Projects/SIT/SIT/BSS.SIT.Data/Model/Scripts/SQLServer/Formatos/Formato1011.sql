﻿IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'P' AND NAME = 'Formato1011') BEGIN
	DROP PROCEDURE dbo.Formato1011
END
GO

CREATE PROCEDURE dbo.Formato1011

	@LedgerBookId INT
AS 
SET NOCOUNT ON

DECLARE 
	@Formato NVARCHAR(50)
	, @EsResumido BIT 
	, @EsConsecutivo BIT
	, @Tope MONEY
SET @Formato = '1011'

--Trae las parametricas del reporte
SELECT @EsResumido = CASE WHEN Valor = 'True' THEN 1 ELSE 0 END  FROM Configuracion WHERE (Llave = 'ParamResumido')
SELECT @EsConsecutivo = CASE WHEN Valor = 'True' THEN 1 ELSE 0 END  FROM Configuracion WHERE (Llave = 'ParamNitExtranjero')
SELECT @Tope = Tope FROM FormatoDian WHERE (CodigoFormato = @Formato)

--Datos de la empresa
DECLARE
	 @IdCompania NVARCHAR(50)
	, @DvCompania NVARCHAR(1)
	, @Nombre NVARCHAR(50)
	, @Direccion NVARCHAR(50)
	, @CodigoCiudad NVARCHAR(50)
	, @CodigoDpto NVARCHAR(50)
	, @CodigoPais NVARCHAR(50)
	, @CompaniaCodigo NVARCHAR(10)

--Trae la informacion de la compania por si se requiere
SELECT @CompaniaCodigo = CompaniaId FROM LedgerBook WHERE (Id = @LedgerBookId)
SELECT @IdCompania = Id, @DvCompania = Dv, @Nombre = Nombre, @Direccion = Direccion, @CodigoCiudad = CodigoCiudad, @CodigoDpto = CodigoDpto, @CodigoPais = CodigoPais FROM Company WHERE (Codigo = @CompaniaCodigo)

DECLARE @id INT, @consecutivo INT

--Genera la tabla resultante
DECLARE @Resultado TABLE(
	id INT IDENTITY(1, 1), 
	cpt nvarchar(50) NULL,
	sal money NULL
)

--Para los datos temporales del resumido
DECLARE @ResultadoTemporal TABLE(
	id INT IDENTITY(1, 1), 
	cpt nvarchar(50) NULL,
	sal money NULL
)

	--DETALLADO
	INSERT INTO @Resultado (
		cpt,
		sal
	)
	SELECT
		MedioMagneticoDian.Concepto AS cpt
		, CASE 
			WHEN MedioMagneticoDian.Tipo = 'D' THEN SUM(ABS(SaldoOtro.ValorDebito))
			WHEN MedioMagneticoDian.Tipo = 'C' THEN SUM(ABS(SaldoOtro.ValorCredito))
			ELSE SUM(ABS(SaldoOtro.ValorDebito - SaldoOtro.ValorCredito))
		  END AS sal
	FROM	
		SaldoOtro 
	INNER JOIN 
		MedioMagneticoDian 
		ON MedioMagneticoDian.Cuentas = SaldoOtro.CodigoCuenta
		AND MedioMagneticoDian.LedgerBookId = SaldoOtro.LedgerBookId
	WHERE 
		(MedioMagneticoDian.Formato = @Formato)
		AND (MedioMagneticoDian.LedgerBookId = @LedgerBookId)
	GROUP BY
		MedioMagneticoDian.Concepto
		, MedioMagneticoDian.Valor
		, MedioMagneticoDian.Tipo

--presenta el resultado ya validado
SELECT 
	cpt,
	SUM(sal) AS sal
FROM 
	@Resultado 
WHERE 
	(sal > 0)
GROUP BY
	cpt
GO
