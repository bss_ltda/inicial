﻿IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'P' AND NAME = 'Formato1004') BEGIN
	DROP PROCEDURE dbo.Formato1004
END
GO

CREATE PROCEDURE dbo.Formato1004

	@LedgerBookId INT
AS 
SET NOCOUNT ON

DECLARE 
	@Formato NVARCHAR(50)
	, @EsResumido BIT 
	, @EsConsecutivo BIT
	, @Tope MONEY
SET @Formato = '1004'

--Trae las parametricas del reporte
SELECT @EsResumido = CASE WHEN Valor = 'True' THEN 1 ELSE 0 END  FROM Configuracion WHERE (Llave = 'ParamResumido')
SELECT @EsConsecutivo = CASE WHEN Valor = 'True' THEN 1 ELSE 0 END  FROM Configuracion WHERE (Llave = 'ParamNitExtranjero')
SELECT @Tope = Tope FROM FormatoDian WHERE (CodigoFormato = @Formato)

--Datos de la empresa
DECLARE
	 @IdCompania NVARCHAR(50)
	, @DvCompania NVARCHAR(1)
	, @Nombre NVARCHAR(50)
	, @Direccion NVARCHAR(50)
	, @CodigoCiudad NVARCHAR(50)
	, @CodigoDpto NVARCHAR(50)
	, @CodigoPais NVARCHAR(50)
	, @CompaniaCodigo NVARCHAR(10)

--Trae la informacion de la compania por si se requiere
SELECT @CompaniaCodigo = CompaniaId FROM LedgerBook WHERE (Id = @LedgerBookId)
SELECT @IdCompania = Id, @DvCompania = Dv, @Nombre = Nombre, @Direccion = Direccion, @CodigoCiudad = CodigoCiudad, @CodigoDpto = CodigoDpto, @CodigoPais = CodigoPais FROM Company WHERE (Codigo = @CompaniaCodigo)

DECLARE @id INT, @consecutivo INT

--Genera la tabla resultante
DECLARE @Resultado TABLE(
	id INT IDENTITY(1, 1), 
	cpt nvarchar(50) NULL,
	vpag money NULL,
	vdes money NULL
)

--Para los datos temporales del resumido
DECLARE @ResultadoTemporal TABLE(
	id INT IDENTITY(1, 1), 
	cpt nvarchar(50) NULL,
	vpag money NULL,
	vdes money NULL
)

	--DETALLADO
	INSERT INTO @Resultado (
		cpt,
		vpag,
		vdes
	)
	SELECT
		MedioMagneticoDian.Concepto AS cpt
		, CASE WHEN MedioMagneticoDian.Valor = 1 THEN
			CASE 
				WHEN MedioMagneticoDian.Tipo = 'D' THEN SUM(ABS(SaldoOtro.ValorDebito))
				WHEN MedioMagneticoDian.Tipo = 'C' THEN SUM(ABS(SaldoOtro.ValorCredito))
				ELSE SUM(ABS(SaldoOtro.ValorDebito - SaldoOtro.ValorCredito))
			END
		  ELSE 0
		  END AS vpag
		, CASE WHEN MedioMagneticoDian.Valor = 2 THEN
			CASE 
				WHEN MedioMagneticoDian.Tipo = 'D' THEN SUM(ABS(SaldoOtro.ValorDebito))
				WHEN MedioMagneticoDian.Tipo = 'C' THEN SUM(ABS(SaldoOtro.ValorCredito))
				ELSE SUM(ABS(SaldoOtro.ValorDebito - SaldoOtro.ValorCredito))
			END
		  ELSE 0
		  END AS vdes
	FROM 
		SaldoOtro 
	INNER JOIN 
		MedioMagneticoDian 
		ON MedioMagneticoDian.Cuentas = SaldoOtro.CodigoCuenta
		AND MedioMagneticoDian.LedgerBookId = SaldoOtro.LedgerBookId
	WHERE 
		(MedioMagneticoDian.Formato = @Formato)
		AND (MedioMagneticoDian.LedgerBookId = @LedgerBookId)
	GROUP BY
		MedioMagneticoDian.Concepto
		, MedioMagneticoDian.Valor
		, MedioMagneticoDian.Tipo

--presenta el resultado ya validado
SELECT 
	cpt,
	SUM(vpag) AS vpag,
	SUM(vdes) AS vdes
FROM 
	@Resultado 
WHERE 
	(vpag > 0) OR (vdes > 0)
GROUP BY
	cpt
GO
