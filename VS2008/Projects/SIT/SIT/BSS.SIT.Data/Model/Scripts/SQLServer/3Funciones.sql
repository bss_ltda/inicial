﻿USE SIT
GO

IF EXISTS (SELECT * FROM sysobjects WHERE type = 'TF' AND name = 'Crearlista')
 BEGIN
  DROP FUNCTION dbo.Crearlista
 END
GO

/***********************************************
Descripcion:	funcion que realiza un split y deuvelve una tabla
Autor:			Ogonzalez (www.primate.co)
Fecha:			2011/01/12
************************************************/

CREATE FUNCTION dbo.Crearlista (@lista text, @delimitador varchar(1))
	RETURNS @tbl TABLE (pos int IDENTITY(1, 1) NOT NULL, valor varchar(50))
AS
BEGIN
	DECLARE 
		@Pos int
		, @Textpos int
		, @chunklen smallint
		, @tmpstr varchar(4000)
		, @leftover varchar(4000)
		, @tmpval varchar(4000)

SET @Textpos = 1
SET @leftover = ''

WHILE (@Textpos <= datalength(@lista) / 2) BEGIN
	SET @chunklen = 4000 - datalength(@leftover) / 2
	SET @tmpstr = @leftover + substring(@lista, @Textpos, @chunklen)
	SET @Textpos = @Textpos + @chunklen

	SET @Pos = charindex(@delimitador, @tmpstr)

	WHILE (@Pos > 0) BEGIN
		SET @tmpval = ltrim(rtrim(left(@tmpstr, @Pos - 1)))
		INSERT @tbl (valor) VALUES(@tmpval)
		SET @tmpstr = substring(@tmpstr, @Pos + 1, len(@tmpstr))
		SET @Pos = charindex(@delimitador, @tmpstr)
	END

	SET @leftover = @tmpstr
END

INSERT @tbl (valor) VALUES (ltrim(rtrim(@leftover)))
RETURN
END
GO

IF EXISTS (SELECT * FROM sysobjects WHERE type = 'FN' AND name = 'ObtenerParteNombre') BEGIN
	DROP FUNCTION dbo.ObtenerParteNombre
END
GO

/***********************************************
Descripcion:	funcion que parte el nombre
Autor:			Ogonzalez (www.primate.co)
Fecha:			2011/01/11
************************************************/

CREATE FUNCTION dbo.ObtenerParteNombre (
	@NombreCompleto VARCHAR(50)
	, @Tipo VARCHAR(50)
)
RETURNS VARCHAR(50)
AS
BEGIN
	DECLARE
		@Nombre1 VARCHAR(60)
		, @Nombre2 VARCHAR(60)
		, @Apellido1 VARCHAR(60)
		, @Apellido2 VARCHAR(60)
		, @Conteo INT 

	--debe realizar un split por espacio
	SELECT @Conteo = COUNT(*) FROM dbo.Crearlista(@NombreCompleto, SPACE(1))

	IF(@Conteo = 5) BEGIN 
		SELECT @Apellido1 = valor FROM dbo.Crearlista(@NombreCompleto, SPACE(1)) WHERE (pos = 1)

		SELECT @Apellido2 = valor FROM dbo.Crearlista(@NombreCompleto, SPACE(1)) WHERE (pos = 2)
		SELECT @Apellido2 = @Apellido2 + SPACE(1) + valor FROM dbo.Crearlista(@NombreCompleto, SPACE(1)) WHERE (pos = 3)

		SELECT @Nombre1 = valor FROM dbo.Crearlista(@NombreCompleto, SPACE(1)) WHERE (pos = 4)
		SELECT @Nombre2 = valor FROM dbo.Crearlista(@NombreCompleto, SPACE(1)) WHERE (pos = 5)
	END 
	ELSE BEGIN 
		SELECT @Apellido1 = valor FROM dbo.Crearlista(@NombreCompleto, SPACE(1)) WHERE (pos = 1)

		SELECT @Apellido2 = valor FROM dbo.Crearlista(@NombreCompleto, SPACE(1)) WHERE (pos = 2)

		SELECT @Nombre1 = valor FROM dbo.Crearlista(@NombreCompleto, SPACE(1)) WHERE (pos = 3)
		SELECT @Nombre2 = valor FROM dbo.Crearlista(@NombreCompleto, SPACE(1)) WHERE (pos = 4)
	END 

	IF(@Tipo = 'Nombre1')
		RETURN COALESCE(@Nombre1, SPACE(0))
	IF(@Tipo = 'Nombre2')
		RETURN COALESCE(@Nombre2, SPACE(0))
	IF(@Tipo = 'Apellido1')
		RETURN COALESCE(@Apellido1, SPACE(0))
	IF(@Tipo = 'Apellido2')
		RETURN COALESCE(@Apellido2, SPACE(0))
	RETURN SPACE(0)
END
GO

IF EXISTS (SELECT * FROM sysobjects WHERE type = 'FN' AND name = 'ObtenerTipoDocumento') BEGIN
	DROP FUNCTION dbo.ObtenerTipoDocumento
END
GO

/***********************************************
Descripcion:	funcion el tipo de documento
Autor:			Ogonzalez (www.primate.co)
Fecha:			2011/02/14
************************************************/

CREATE FUNCTION dbo.ObtenerTipoDocumento (
	@TipoIdentificacion nVARCHAR(50)
	, @esConsecutivo BIT 
)
RETURNS INT
AS
BEGIN
	IF(@TipoIdentificacion = 'A')
		RETURN 31
	
	IF(@TipoIdentificacion = 'C')
		RETURN 13
	IF(@TipoIdentificacion = 'E') BEGIN
		IF(@esConsecutivo = 1)
			RETURN 43
		RETURN 42
	END 

	IF(@TipoIdentificacion = 'T')
		RETURN 12

	RETURN 0
END
GO

IF EXISTS (SELECT * FROM sysobjects WHERE type = 'FN' AND name = 'ObtenerCampoAlfa') BEGIN
	DROP FUNCTION dbo.ObtenerCampoAlfa
END
GO

/***********************************************
Descripcion:	funcion que obtiene el campo alpha
Autor:			Ogonzalez (www.primate.co)
Fecha:			2011/02/20
************************************************/

CREATE FUNCTION dbo.ObtenerCampoAlfa (
	@Dato nVARCHAR(200)
)
RETURNS nVARCHAR(200)
AS
BEGIN
	DECLARE
		@i INT
		, @ascii INT
		, @CampoAlfa nVARCHAR(200)

	SET @i = 1
	SET @CampoAlfa = SPACE(0)
	WHILE( @i <= DATALENGTH(@Dato)) BEGIN 
		SET @ascii = ASCII(SUBSTRING(@Dato, @i, 1))
		
		IF(CHARINDEX(SPACE(1), CHAR(@ascii), 1) > 0) BEGIN
			--!@#$&*()-_=+{}[]\|:;.?/<>
			SET @CampoAlfa = @CampoAlfa + CHAR(@ascii)
		END
		ELSE IF(CHAR(@ascii) = 'á') BEGIN
			SET @CampoAlfa = @CampoAlfa + 'a'
		END
		ELSE IF(CHAR(@ascii) = 'é') BEGIN
			SET @CampoAlfa = @CampoAlfa + 'e'
		END
		ELSE IF(CHAR(@ascii) = 'í') BEGIN
			SET @CampoAlfa = @CampoAlfa + 'i'
		END
		ELSE IF(CHAR(@ascii) = 'ó') BEGIN
			SET @CampoAlfa = @CampoAlfa + 'o'
		END
		ELSE IF(CHAR(@ascii) = 'ú') BEGIN
			SET @CampoAlfa = @CampoAlfa + 'u'
		END
		ELSE IF(CHAR(@ascii) = 'ñ') BEGIN
			SET @CampoAlfa = @CampoAlfa + 'n'
		END
		ELSE IF(@ascii = 10 OR @ascii = 13) BEGIN
			SET @CampoAlfa = @CampoAlfa + SPACE(1)
		END
		ELSE IF(@ascii >= ASCII('0') AND @ascii <= ASCII('9')) BEGIN
			SET @CampoAlfa = @CampoAlfa + CHAR(@ascii)
		END 
		ELSE IF(@ascii >= ASCII('A') AND @ascii <= ASCII('Z')) BEGIN
			SET @CampoAlfa = @CampoAlfa + CHAR(@ascii)
		END
		ELSE IF(@ascii >= ASCII('a') AND @ascii <= ASCII('z')) BEGIN
			SET @CampoAlfa = @CampoAlfa + CHAR(@ascii)
		END 
		
		SET @i = @i + 1
	END 

	RETURN @CampoAlfa
END
GO

IF EXISTS (SELECT * FROM sysobjects WHERE type = 'FN' AND name = 'ObtenerValorCampo') BEGIN
	DROP FUNCTION dbo.ObtenerValorCampo
END
GO

/***********************************************
Descripcion:	funcion el tipo de documento
Autor:			Ogonzalez (www.primate.co)
Fecha:			2011/02/14
************************************************/

CREATE FUNCTION dbo.ObtenerValorCampo (
	@Id INT,
	@Valor INT,
	@Tipo NVARCHAR(1),
	@ValorDebito MONEY,
	@ValorCredito MONEY,
	@ValorTotal MONEY
)
RETURNS MONEY
AS
BEGIN
	DECLARE @Result MONEY
	SET @Result = 0
	
	IF(@Valor = @Id) BEGIN
		IF(@Tipo = 'D') 
			SET @Result = ABS(@ValorDebito)
		ELSE IF(@Tipo = 'C') 
			SET @Result = ABS(@ValorCredito)
		ELSE 
			SET @Result = ABS(@ValorTotal)
	END
	RETURN @Result
END
GO
