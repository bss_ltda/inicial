﻿USE SIT
GO

--VISTAS
IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'V' AND NAME = 'Ledger') BEGIN
	DROP VIEW dbo.Ledger
END
GO

CREATE VIEW dbo.Ledger
AS 
SELECT DISTINCT 
	DSLDGR AS LedgerCode
FROM 
	LedgerDetail
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'V' AND NAME = 'TerceroOficial') BEGIN
	DROP VIEW dbo.TerceroOficial
END
GO

CREATE VIEW dbo.TerceroOficial
AS
SELECT 
	TipoTercero, 
	TipoIdentificacion, 
	CodigoFiscal, 
	Digito, 
	RazonSocial, 
	Direccion, 
	CodigoDpto, 
	CodigoCiudad, 
	CodigoPais 
FROM 
	Tercero 
WHERE 
	(ClienteOficial = 1)
GO
