﻿USE SIT
GO

--PROCEDIMIENTOS
IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'P' AND NAME = 'ListarMenuRol') BEGIN
	DROP PROCEDURE dbo.ListarMenuRol
END
GO

CREATE PROCEDURE dbo.ListarMenuRol
	@RolId INT
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON

SELECT 
	Menu.Id
	, Menu.PadreId
	, Menu.Descripcion
	, Menu.Url 
FROM 
	Menu
INNER JOIN 
	RolMenu 
	ON Menu.Id = RolMenu.MenuId
WHERE 
	(RolMenu.RolId = @RolId)
ORDER BY 
	Menu.PadreId, Menu.Id, Menu.Posicion
	
RETURN @@ROWCOUNT
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'P' AND NAME = 'ListarMenu') BEGIN
	DROP PROCEDURE dbo.ListarMenu
END
GO

CREATE PROCEDURE dbo.ListarMenu
AS
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SET NOCOUNT ON

SELECT
	Menu.PadreId
	, MenuPadre.Descripcion AS Padre
	, Menu.Id
	, Menu.Descripcion
	, Menu.Posicion
	, Menu.Url
FROM 
	Menu 
INNER JOIN Menu MenuPadre 
	ON Menu.PadreId = MenuPadre.Id
ORDER BY 
	Menu.PadreId, Menu.Posicion
	
RETURN @@ROWCOUNT
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'P' AND NAME = 'ActualizarConfiguracion ') BEGIN
	DROP PROCEDURE dbo.ActualizarConfiguracion 
END
GO

CREATE PROCEDURE ActualizarConfiguracion 
	@Llave VARCHAR(50),
	@Valor VARCHAR(1000)
AS 
SET NOCOUNT ON
IF(NOT EXISTS(SELECT Id FROM Configuracion WHERE (Llave = @Llave)))
	INSERT INTO Configuracion (Llave, Valor) VALUES (UPPER(@Llave), @Valor) 
UPDATE Configuracion SET Valor = UPPER(@Valor) WHERE (Llave = @Llave)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'P' AND NAME = 'FilterLedgerAccount') BEGIN
	DROP PROCEDURE dbo.FilterLedgerAccount
END
GO

CREATE PROCEDURE dbo.FilterLedgerAccount
	@ledgerBookId INT, 
	@cuentaInicial nvarchar(50), 
	@cuentaFinal nvarchar(50), 
	@clasificacionId INT, 
	@detalleTif BIT = NULL
AS
SET NOCOUNT ON

DECLARE
	@sql nvarchar(3000)
	, @sqlCriterios nvarchar(500)

SET @sql = '
SELECT 
	LedgerAccount.Id,
	LedgerAccount.Cuenta,
	LedgerAccount.Descripcion,
	ClasificacionCuenta.Codigo + '' - '' + ClasificacionCuenta.Descripcion AS ClasificacionCuenta,
	LedgerAccount.Tif
FROM 
	LedgerAccount
INNER JOIN 
	ClasificacionCuenta
	ON LedgerAccount.ClasificacionCuentaId = ClasificacionCuenta.Id
	'
SET @sqlCriterios = ''
SET @sqlCriterios = @sqlCriterios + '(LedgerAccount.LedgerBookId = @ledgerBookId) AND '

IF(LEN(@cuentaInicial) <> 0 AND LEN(@cuentaFinal) <> 0)
	SET @sqlCriterios = @sqlCriterios + '(LedgerAccount.Cuenta BETWEEN @cuentaInicial AND @cuentaFinal) AND '
ELSE IF(LEN(@cuentaInicial) <> 0 AND LEN(@cuentaFinal) = 0)
	SET @sqlCriterios = @sqlCriterios + '(LedgerAccount.Cuenta >= @cuentaInicial) AND '
ELSE IF(LEN(@cuentaInicial) = 0 AND LEN(@cuentaFinal) <> 0)
	SET @sqlCriterios = @sqlCriterios + '(LedgerAccount.Cuenta <= @cuentaFinal) AND '

IF(@clasificacionId <> 0)
	SET @sqlCriterios = @sqlCriterios + '(LedgerAccount.ClasificacionCuentaId = @clasificacionId) AND '

IF(NOT @detalleTif IS NULL)
	SET @sqlCriterios = @sqlCriterios + '(LedgerAccount.Tif = @detalleTif) AND '

--Agrega el where
IF(LEN(@sqlCriterios) <> 0)
	SET @sql = @sql + 'WHERE ' + SUBSTRING(@sqlCriterios, 1, LEN(@sqlCriterios) - 4)

SET @sql = @sql + ' ORDER BY LedgerAccount.Cuenta'

EXECUTE sp_executesql @sql, N'@ledgerBookId INT, @cuentaInicial nvarchar(50),  @cuentaFinal nvarchar(50),  @clasificacionId INT,  @detalleTif BIT', @ledgerBookId, @cuentaInicial, @cuentaFinal, @clasificacionId, @detalleTif
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'P' AND NAME = 'MassiveLedgerAccount') BEGIN
	DROP PROCEDURE dbo.MassiveLedgerAccount
END
GO

CREATE PROCEDURE dbo.MassiveLedgerAccount
	@ledgerBookId INT, 
	@cuentaInicial nvarchar(50), 
	@cuentaFinal nvarchar(50), 
	@clasificacionId INT, 
	@detalleTif BIT
AS
SET NOCOUNT ON
DECLARE 
	@Ledger nvarchar(50),
	@ChartAccount nvarchar(50)

--Trae la informacion del ledger y book
SELECT 
	@Ledger = Ledger, 
	@ChartAccount = ChartAccount
FROM 
	LedgerBook 
WHERE 
	(Id = @LedgerBookId)

--Selecciona las cuentas asociadas al ledgerbook
INSERT INTO LedgerAccount (
	LedgerBookId,
	Cuenta,
	Descripcion,
	Tipo,
	ClasificacionCuentaId,
	Tif
) 
SELECT 
	@LedgerBookId AS LedgerBookId,
	AccountDetail.SVSGVL AS Cuenta,
	AccountDetail.SVLDES AS Descripcion,
	'AS' AS Tipo,
	@clasificacionId AS ClasificacionCuentaId,
	@detalleTif AS Tif
FROM 
	AccountDetail AccountDetail
WHERE
	(AccountDetail.DSLDGR = @Ledger)
	AND (AccountDetail.DSCOA = @ChartAccount)
	AND (NOT AccountDetail.SVSGVL IN (SELECT Cuenta FROM LedgerAccount WHERE (LedgerBookId = @ledgerBookId)))
	AND (AccountDetail.SVSGVL BETWEEN @cuentaInicial AND @cuentaFinal)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'P' AND NAME = 'ActualizarMaestroJob') BEGIN
	DROP PROCEDURE dbo.ActualizarMaestroJob
END
GO

CREATE PROCEDURE dbo.ActualizarMaestroJob
AS
SET NOCOUNT ON
EXEC msdb.dbo.sp_start_job N'ActualizarMaestros'
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'P' AND NAME = 'ActualizarTerceroJob') BEGIN
	DROP PROCEDURE dbo.ActualizarTerceroJob
END
GO

CREATE PROCEDURE dbo.ActualizarTerceroJob
AS
SET NOCOUNT ON
EXEC msdb.dbo.sp_start_job N'ActualizarTerceros'
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'P' AND NAME = 'ActualizarTercero') BEGIN
	DROP PROCEDURE dbo.ActualizarTercero
END
GO

CREATE PROCEDURE dbo.ActualizarTercero
AS
SET NOCOUNT ON

DECLARE 
	@Identificador nvarchar(2),
	@TipoTercero nvarchar(1),
	@Codigo DECIMAL(6, 0),
	@Secuencia INT,
	@Identificacion nvarchar(16),
	@Compania DECIMAL(2, 0),
	@RazonSocial nvarchar(30),
	@CodigoPais nvarchar(4),
	@CodigoCiudad nvarchar(3),
	@CodigoDpto nvarchar(2),
	@ClienteOficial INT,
	@Direccion nvarchar(200)

--Variables locales
DECLARE 
	@TipoIdentificacion nvarchar(1),
	@CodigoFiscal nvarchar(50),
	@Digito nvarchar(1),
	@Validando NVARCHAR(50),
	@CampoEsPrincipal nvarchar(50)

--CREA EL CURSOR PARA EL CLIENTE
DECLARE CustomerCursor CURSOR FAST_FORWARD FOR 
SELECT  
	Identificador,
	TipoTercero,
	Codigo,
	Secuencia,
	Identificacion,
	Compania,
	RazonSocial,
	CodigoPais,
	CodigoCiudad,
	CodigoDpto,
	CASE WHEN EsPrincipal = '1' THEN 1 ELSE 0 END AS ClienteOficial,
	Direccion
FROM 
	TempTercero

OPEN CustomerCursor
FETCH NEXT FROM CustomerCursor INTO @Identificador, @TipoTercero, @Codigo, @Secuencia, @Identificacion, @Compania, @RazonSocial, @CodigoPais, @CodigoCiudad, @CodigoDpto, @ClienteOficial, @Direccion

WHILE (@@FETCH_STATUS = 0) BEGIN
BEGIN TRY
	SET @Validando = 'Tipo Identificacion: ' + @Identificacion
	SET @TipoIdentificacion = SUBSTRING(@Identificacion, 1, 1)
	
	SET @Validando = 'Codigo Fiscal: ' + @Identificacion
	SET @CodigoFiscal = NULL
	SET @CodigoFiscal =
	CASE WHEN SUBSTRING(@Identificacion, 1, 1) = 'A' THEN 
		SUBSTRING(@Identificacion, 2, CHARINDEX('-', @Identificacion) - 2) 
	ELSE 
		SUBSTRING(@Identificacion, 2, LEN(@Identificacion)) 
	END

	SET @Validando = 'Digito: ' + @Identificacion
	SET @Digito =  NULL
	SET @Digito = 
	CASE WHEN SUBSTRING(@Identificacion, 1, 1) = 'A' THEN 
		SUBSTRING(@Identificacion, CHARINDEX('-', @Identificacion) + 1, 1)
	ELSE 
		SPACE(0)
	END

	SET @Validando = NULL
	IF(LEN(COALESCE(@CodigoFiscal, SPACE(0))) = 0)
		RAISERROR('Codigo fiscal vacio.', 16, 1);
	
	--Valida si existe el departamento y la ciudad
	SET @Validando = NULL
	IF(NOT EXISTS(SELECT Id FROM Ciudad WHERE (PaisId = @CodigoPais) AND (DptoId = @CodigoDpto) AND (CiudadId = @CodigoCiudad)))
		RAISERROR('Ciudad y/o Departamento y/o Pais no encontrado.', 16, 1);
		
	--Valida que ese nit no este principal otra vez
	SET @Validando = NULL
	IF(EXISTS(SELECT Id FROM Tercero WHERE (TipoTercero = 'C') AND (ClienteOficial = 1) AND (CodigoFiscal = @CodigoFiscal)) AND (@ClienteOficial = 1))
		RAISERROR('Cliente oficial repetido.', 16, 1);

	--Valida el tipo de identificacion
	IF(@TipoIdentificacion = 'E')
		SET @Direccion = SPACE(0)

	INSERT INTO Tercero (
		Identificador,
		TipoTercero,
		Codigo,
		Secuencia,
		TipoIdentificacion,
		CodigoFiscal,
		Digito,
		Compania,
		RazonSocial,
		CodigoPais,
		CodigoCiudad,
		CodigoDpto,
		ClienteOficial,
		Direccion
	) VALUES ( 
		@Identificador,
		@TipoTercero,
		@Codigo,
		@Secuencia,
		@TipoIdentificacion,
		@CodigoFiscal,
		@Digito,
		@Compania,
		@RazonSocial,
		@CodigoPais,
		@CodigoCiudad,
		@CodigoDpto,
		@ClienteOficial,
		@Direccion
	) 
END TRY
BEGIN CATCH
	INSERT INTO ErrorTercero (
		Identificador,
		TipoTercero,
		Codigo,
		Secuencia,
		TipoIdentificacion,
		CodigoFiscal,
		Digito,
		Compania,
		RazonSocial,
		CodigoPais,
		CodigoCiudad,
		CodigoDpto,
		ClienteOficial,
		Direccion,
		Mensaje, 
		CodigoError
	) VALUES ( 
		@Identificador,
		@TipoTercero,
		@Codigo,
		@Secuencia,
		@TipoIdentificacion,
		@CodigoFiscal,
		@Digito,
		@Compania,
		@RazonSocial,
		@CodigoPais,
		@CodigoCiudad,
		@CodigoDpto,
		@ClienteOficial,
		@Direccion,
		COALESCE(@Validando, ERROR_MESSAGE()),
		ERROR_LINE()
	) 
END CATCH;

FETCH NEXT FROM CustomerCursor INTO @Identificador, @TipoTercero, @Codigo, @Secuencia, @Identificacion, @Compania, @RazonSocial, @CodigoPais, @CodigoCiudad, @CodigoDpto, @ClienteOficial, @Direccion
END
CLOSE CustomerCursor
DEALLOCATE CustomerCursor

--Se pasa de A a C los que no cumplen la regla de NIT
UPDATE 
	Tercero
SET 
	TipoIdentificacion = 'C'
WHERE 
	(TipoIdentificacion = 'A') 
	AND NOT CodigoFiscal IN (
		SELECT 
			CodigoFiscal 
		FROM 
			Tercero 
		WHERE 
			(LEN(CodigoFiscal) = 9)
			AND (TipoIdentificacion = 'A') 
			AND ((CodigoFiscal LIKE '8%') OR (CodigoFiscal LIKE '9%'))
	)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'P' AND NAME = 'ActualizarMovimientoJob') BEGIN
	DROP PROCEDURE dbo.ActualizarMovimientoJob
END
GO

CREATE PROCEDURE dbo.ActualizarMovimientoJob
AS
	EXEC msdb.dbo.sp_start_job N'ActualizarMovimientos'
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'P' AND NAME = 'ActualizarMovimiento') BEGIN
	DROP PROCEDURE dbo.ActualizarMovimiento
END
GO

CREATE PROCEDURE dbo.ActualizarMovimiento
AS 
SET NOCOUNT ON

DECLARE
	@LedgerBookId INT,
	@PeriodoId INT

--Trae
SELECT @LedgerBookId = CONVERT(INT, Valor) FROM Configuracion WHERE (Llave = 'PROCESOMOVIMIENTOLEDGERBOOK')
SELECT @PeriodoId = CONVERT(INT, Valor) FROM Configuracion WHERE (Llave = 'PROCESOMOVIMIENTOPERIODO')

--Variables Globales
DECLARE 
	@Ledger nVARCHAR(50),
	@Book nVARCHAR(50),
	@Ano INT, 
	@CompaniaId nVARCHAR(50),
	@SegCompania nVARCHAR(50),
	@SegCuenta nVARCHAR(50),
	@SegCentroCosto nVARCHAR(50),
	@SegTercero nVARCHAR(50),
	@SegTerceroRef nVARCHAR(50),
	@SegTasaImpuesto nVARCHAR(50),
	@CampoValor nVARCHAR(50), 
	@Llave nVARCHAR(50)

--Variables locales	
DECLARE 
	@Mensaje VARCHAR(50),
	@CodigoError INT,
	@BaseDatos NVARCHAR(50),
	@Libreria NVARCHAR(50)
SET @CodigoError = 397
		
--Trae la informacion del ledger y book
SELECT 
	@Ledger = Ledger, 
	@Book = Book, 
	@Ano = Ano, 
	@CompaniaId = CompaniaId,
	@SegCompania = Compania,
	@SegCuenta = Cuenta,
	@SegCentroCosto = CentroCosto,
	@SegTercero = COALESCE(Tercero, SPACE(0)),
	@SegTerceroRef = COALESCE(TerceroReferencia, SPACE(0)),
	@SegTasaImpuesto = TasaImpuesto 
FROM 
	LedgerBook 
WHERE 
	(Id = @LedgerBookId)

SELECT @BaseDatos = Valor FROM Configuracion WHERE (Llave = 'BASEDATOS')
SELECT @Libreria = Valor FROM Configuracion WHERE (Llave = 'LIBRERIA')

--Realiza las validaciones masivamente
--COMPROBANTEFECHA
SET @Mensaje = 'Fecha no valida. [COMPROBANTEFECHA]'
INSERT INTO ErrorMvto (
	LedgerBookId,
	Periodo,
	ComprobanteLinea,
	ComprobanteNumero,
	ComprobanteFecha,
	DocumentoNumero,
	DocumentoFecha,
	DescripcionTransaccion,
	CodigoRazon,
	Valor,
	SegmentoCompania,
	SegmentoCuenta,
	SegmentoCentroCosto,
	SegmentoTercero,
	TasaImpuesto,
	ImpuestoRetencion,
	ImpuestoDescripcion,
	ValorBase,
	ValorDebito,
	ValorCredito,
	TipoTercero,
	Mensaje, 
	CodigoError
)
SELECT
	@LedgerBookId,
	@PeriodoId,
	ComprobanteLinea,
	ComprobanteNumero,
	ComprobanteFecha,
	DocumentoNumero,
	DocumentoFecha,
	DescripcionTransaccion,
	CodigoRazon,
	Valor,
	SegmentoCompania,
	SegmentoCuenta,
	SegmentoCentroCosto,
	SegmentoTercero,
	TasaImpuesto,
	NULL,
	NULL,
	NULL,
	ValorDebito,
	ValorCredito,
	NULL,
	@Mensaje, 
	@CodigoError
FROM 
	TempMovimiento 
WHERE 
	(ISDATE(COMPROBANTEFECHA) = 0)

--Elimina de la temporal
DELETE FROM TempMovimiento WHERE (ISDATE(COMPROBANTEFECHA) = 0)

--DOCUMENTOFECHA
SET @Mensaje = 'Fecha no valida. [DOCUMENTOFECHA]'
INSERT INTO ErrorMvto (
	LedgerBookId,
	Periodo,
	ComprobanteLinea,
	ComprobanteNumero,
	ComprobanteFecha,
	DocumentoNumero,
	DocumentoFecha,
	DescripcionTransaccion,
	CodigoRazon,
	Valor,
	SegmentoCompania,
	SegmentoCuenta,
	SegmentoCentroCosto,
	SegmentoTercero,
	TasaImpuesto,
	ImpuestoRetencion,
	ImpuestoDescripcion,
	ValorBase,
	ValorDebito,
	ValorCredito,
	TipoTercero,
	Mensaje, 
	CodigoError
)
SELECT
	@LedgerBookId,
	@PeriodoId,
	ComprobanteLinea,
	ComprobanteNumero,
	ComprobanteFecha,
	DocumentoNumero,
	DocumentoFecha,
	DescripcionTransaccion,
	CodigoRazon,
	Valor,
	SegmentoCompania,
	SegmentoCuenta,
	SegmentoCentroCosto,
	SegmentoTercero,
	TasaImpuesto,
	NULL,
	NULL,
	NULL,
	ValorDebito,
	ValorCredito,
	NULL,
	@Mensaje, 
	@CodigoError
FROM 
	TempMovimiento 
WHERE 
	((CASE WHEN ISDATE(DOCUMENTOFECHA) = 0 THEN CASE WHEN ISDATE(DOCUMENTOFECHAENCABEZADO) = 0 THEN 0 ELSE 1 END ELSE 1 END) = 0)

--Elimina de la temporal
DELETE FROM TempMovimiento WHERE ((CASE WHEN ISDATE(DOCUMENTOFECHA) = 0 THEN CASE WHEN ISDATE(DOCUMENTOFECHAENCABEZADO) = 0 THEN 0 ELSE 1 END ELSE 1 END) = 0)

--DOCUMENTONUMERO
SET @Mensaje = 'Documento vacio. [DOCUMENTONUMERO]'
INSERT INTO ErrorMvto (
	LedgerBookId,
	Periodo,
	ComprobanteLinea,
	ComprobanteNumero,
	ComprobanteFecha,
	DocumentoNumero,
	DocumentoFecha,
	DescripcionTransaccion,
	CodigoRazon,
	Valor,
	SegmentoCompania,
	SegmentoCuenta,
	SegmentoCentroCosto,
	SegmentoTercero,
	TasaImpuesto,
	ImpuestoRetencion,
	ImpuestoDescripcion,
	ValorBase,
	ValorDebito,
	ValorCredito,
	TipoTercero,
	Mensaje, 
	CodigoError
)
SELECT
	@LedgerBookId,
	@PeriodoId,
	ComprobanteLinea,
	ComprobanteNumero,
	ComprobanteFecha,
	DocumentoNumero,
	DocumentoFecha,
	DescripcionTransaccion,
	CodigoRazon,
	Valor,
	SegmentoCompania,
	SegmentoCuenta,
	SegmentoCentroCosto,
	SegmentoTercero,
	TasaImpuesto,
	NULL,
	NULL,
	NULL,
	ValorDebito,
	ValorCredito,
	NULL,
	@Mensaje, 
	@CodigoError
FROM 
	TempMovimiento 
WHERE 
	(LEN(DOCUMENTONUMERO) = 0)

--Elimina de la temporal
DELETE FROM TempMovimiento WHERE (LEN(DOCUMENTONUMERO) = 0)

--SEGMENTOTERCERO ERRADO
SET @Mensaje = 'Tercero Errado. [SEGMENTOTERCERO]'
INSERT INTO ErrorMvto (
	LedgerBookId,
	Periodo,
	ComprobanteLinea,
	ComprobanteNumero,
	ComprobanteFecha,
	DocumentoNumero,
	DocumentoFecha,
	DescripcionTransaccion,
	CodigoRazon,
	Valor,
	SegmentoCompania,
	SegmentoCuenta,
	SegmentoCentroCosto,
	SegmentoTercero,
	TasaImpuesto,
	ImpuestoRetencion,
	ImpuestoDescripcion,
	ValorBase,
	ValorDebito,
	ValorCredito,
	TipoTercero,
	Mensaje, 
	CodigoError
)
SELECT
	@LedgerBookId,
	@PeriodoId,
	ComprobanteLinea,
	ComprobanteNumero,
	ComprobanteFecha,
	DocumentoNumero,
	DocumentoFecha,
	DescripcionTransaccion,
	CodigoRazon,
	Valor,
	SegmentoCompania,
	SegmentoCuenta,
	SegmentoCentroCosto,
	SegmentoTercero,
	TasaImpuesto,
	NULL,
	NULL,
	NULL,
	ValorDebito,
	ValorCredito,
	NULL,
	@Mensaje, 
	@CodigoError
FROM 
	TempMovimiento 
WHERE 
	(ISNUMERIC(SEGMENTOTERCERO) = 0)

--Elimina de la temporal
DELETE FROM TempMovimiento WHERE (ISNUMERIC(SEGMENTOTERCERO) = 0)

--SEGMENTOTERCERO
SET @Mensaje = 'Tercero no encontrado. [SEGMENTOTERCERO]'
INSERT INTO ErrorMvto (
	LedgerBookId,
	Periodo,
	ComprobanteLinea,
	ComprobanteNumero,
	ComprobanteFecha,
	DocumentoNumero,
	DocumentoFecha,
	DescripcionTransaccion,
	CodigoRazon,
	Valor,
	SegmentoCompania,
	SegmentoCuenta,
	SegmentoCentroCosto,
	SegmentoTercero,
	TasaImpuesto,
	ImpuestoRetencion,
	ImpuestoDescripcion,
	ValorBase,
	ValorDebito,
	ValorCredito,
	TipoTercero,
	Mensaje, 
	CodigoError
)
SELECT
	@LedgerBookId,
	@PeriodoId,
	ComprobanteLinea,
	ComprobanteNumero,
	ComprobanteFecha,
	DocumentoNumero,
	DocumentoFecha,
	DescripcionTransaccion,
	CodigoRazon,
	Valor,
	SegmentoCompania,
	SegmentoCuenta,
	SegmentoCentroCosto,
	SegmentoTercero,
	TasaImpuesto,
	NULL,
	NULL,
	NULL,
	ValorDebito,
	ValorCredito,
	NULL,
	@Mensaje, 
	@CodigoError
FROM 
	TempMovimiento
WHERE 
	(SegmentoCompania = @CompaniaId)  
	AND NOT SegmentoTercero IN (
		SELECT 
			CONVERT(VARCHAR, Tercero.Codigo)
		FROM 
			Tercero
		INNER JOIN 
		(
			SELECT DISTINCT
				TempMovimiento.SEGMENTOTERCERO,
				CASE 
					WHEN NOT ComprobanteManual.ValidaPC IS NULL THEN ComprobanteManual.ValidaPC
					WHEN TempMovimiento.TIPOCOMPROBANTE IN (2, 3, 6) THEN 'C'
					ELSE 'P'
				END AS TipoTercero
			FROM 
				TempMovimiento
		LEFT OUTER JOIN 
			ComprobanteManual
			ON ComprobanteManual.Codigo = SUBSTRING(TempMovimiento.COMPROBANTENUMERO, 1, 2)
		) T
		ON CONVERT(VARCHAR, Tercero.Codigo) = T.SEGMENTOTERCERO
		AND (Tercero.TipoTercero = T.TipoTercero)
	WHERE
		(Tercero.Compania = @CompaniaId)
)

--Elimina de la temporal
DELETE 
	FROM 
		TempMovimiento
	WHERE 
		(SegmentoCompania = @CompaniaId)  
		AND NOT SegmentoTercero IN (
			SELECT 
				CONVERT(VARCHAR, Tercero.Codigo)
			FROM 
				Tercero
			INNER JOIN 
			(
				SELECT DISTINCT
					TempMovimiento.SEGMENTOTERCERO,
					CASE 
						WHEN NOT ComprobanteManual.ValidaPC IS NULL THEN ComprobanteManual.ValidaPC
						WHEN TempMovimiento.TIPOCOMPROBANTE IN (2, 3, 6) THEN 'C'
						ELSE 'P'
					END AS TipoTercero
				FROM 
					TempMovimiento
			LEFT OUTER JOIN 
				ComprobanteManual
				ON ComprobanteManual.Codigo = SUBSTRING(TempMovimiento.COMPROBANTENUMERO, 1, 2)
			) T
			ON CONVERT(VARCHAR, Tercero.Codigo) = T.SEGMENTOTERCERO
			AND (Tercero.TipoTercero = T.TipoTercero)
		WHERE
			(Tercero.Compania = @CompaniaId)
	)

--LOS QUE SON CLASE BS, CC O CP EL CAMPO REFERENCIA 2 ESTÁ EN BLANCO NO SE VALIDAN Y VAN DIRECTO AL MOVIMIENTO
INSERT INTO Movimiento (
	LedgerBookId,
	Periodo,
	ComprobanteLinea,
	ComprobanteNumero,
	ComprobanteFecha,
	DocumentoNumero,
	DocumentoFecha,
	DescripcionTransaccion,
	CodigoRazon,
	Valor,
	SegmentoCompania,
	SegmentoCuenta,
	SegmentoCentroCosto,
	SegmentoTercero,
	TasaImpuesto,
	ImpuestoRetencion,
	ImpuestoDescripcion,
	ValorBase,
	ValorDebito,
	ValorCredito,
	TipoTercero
) 
SELECT 
	@LedgerBookId,
	@PeriodoId,
	COMPROBANTELINEA,
	COMPROBANTENUMERO,
	COMPROBANTEFECHA,
	DOCUMENTONUMERO,
	DOCUMENTOFECHA,
	DESCRIPCIONTRANSACCION,
	CODIGORAZON,
	VALOR,
	SEGMENTOCOMPANIA,
	SEGMENTOCUENTA,
	SEGMENTOCENTROCOSTO,
	SEGMENTOTERCERO,
	TASAIMPUESTO,
	NULL,
	NULL,
	NULL,
	VALORDEBITO,
	VALORCREDITO,
	CASE 
		WHEN NOT ComprobanteManual.ValidaPC IS NULL THEN ComprobanteManual.ValidaPC
		WHEN TempMovimiento.TIPOCOMPROBANTE IN (2, 3, 6) THEN 'C'
		ELSE 'P'
	END AS TIPOTERCERO
FROM 
	TempMovimiento
LEFT OUTER JOIN 
	ComprobanteManual
	ON ComprobanteManual.Codigo = SUBSTRING(TempMovimiento.COMPROBANTENUMERO, 1, 2)
WHERE 
	TempMovimiento.SEGMENTOCUENTA IN (
	SELECT 
		LedgerAccount.Cuenta
	FROM 
		LedgerAccount 
	INNER JOIN 
		ClasificacionCuenta 
		ON LedgerAccount.ClasificacionCuentaId = ClasificacionCuenta.Id
	WHERE 
		(LedgerAccount.LedgerBookId = @LedgerBookId)
		AND (ClasificacionCuenta.Codigo IN ('BS', 'CC', 'CP'))
)
--Elimina en la temporal
DELETE FROM TempMovimiento
WHERE 
	TempMovimiento.SEGMENTOCUENTA IN (
	SELECT 
		LedgerAccount.Cuenta
	FROM 
		LedgerAccount 
	INNER JOIN 
		ClasificacionCuenta 
		ON LedgerAccount.ClasificacionCuentaId = ClasificacionCuenta.Id
	WHERE 
		(LedgerAccount.LedgerBookId = 1)
		AND (ClasificacionCuenta.Codigo IN ('BS', 'CC', 'CP'))
)

--TASAIMPUESTO
SET @Mensaje = 'Tasa vacia. [TASAIMPUESTO]'
INSERT INTO ErrorMvto (
	LedgerBookId,
	Periodo,
	ComprobanteLinea,
	ComprobanteNumero,
	ComprobanteFecha,
	DocumentoNumero,
	DocumentoFecha,
	DescripcionTransaccion,
	CodigoRazon,
	Valor,
	SegmentoCompania,
	SegmentoCuenta,
	SegmentoCentroCosto,
	SegmentoTercero,
	TasaImpuesto,
	ImpuestoRetencion,
	ImpuestoDescripcion,
	ValorBase,
	ValorDebito,
	ValorCredito,
	TipoTercero,
	Mensaje, 
	CodigoError
)
SELECT
	@LedgerBookId,
	@PeriodoId,
	ComprobanteLinea,
	ComprobanteNumero,
	ComprobanteFecha,
	DocumentoNumero,
	DocumentoFecha,
	DescripcionTransaccion,
	CodigoRazon,
	Valor,
	SegmentoCompania,
	SegmentoCuenta,
	SegmentoCentroCosto,
	SegmentoTercero,
	TasaImpuesto,
	NULL,
	NULL,
	NULL,
	ValorDebito,
	ValorCredito,
	NULL,
	@Mensaje, 
	@CodigoError
FROM 
	TempMovimiento 
WHERE 
	(LEN(COALESCE(TASAIMPUESTO, SPACE(0))) = 0)

--Elimina de la temporal
DELETE FROM TempMovimiento WHERE (LEN(COALESCE(TASAIMPUESTO, SPACE(0))) = 0)

--VALORBASE REALIZA EL CALCULO FECHA <= ZRC.RCCRTE
INSERT INTO Movimiento (
	LedgerBookId,
	Periodo,
	ComprobanteLinea,
	ComprobanteNumero,
	ComprobanteFecha,
	DocumentoNumero,
	DocumentoFecha,
	DescripcionTransaccion,
	CodigoRazon,
	Valor,
	SegmentoCompania,
	SegmentoCuenta,
	SegmentoCentroCosto,
	SegmentoTercero,
	TasaImpuesto,
	ImpuestoRetencion,
	ImpuestoDescripcion,
	ValorBase,
	ValorDebito,
	ValorCredito,
	TipoTercero
) 
SELECT 
	@LedgerBookId,
	@PeriodoId,
	COMPROBANTELINEA,
	COMPROBANTENUMERO,
	COMPROBANTEFECHA,
	DOCUMENTONUMERO,
	DOCUMENTOFECHA,
	DESCRIPCIONTRANSACCION,
	CODIGORAZON,
	VALOR,
	SEGMENTOCOMPANIA,
	SEGMENTOCUENTA,
	SEGMENTOCENTROCOSTO,
	SEGMENTOTERCERO,
	TASAIMPUESTO,
	ZRC.RCCRTE AS ImpuestoRetencion, 
	ZRC.RCDESC AS ImpuestoDescripcion, 
	((TempMovimiento.VALOR / ZRC.RCCRTE) * 100) AS ValorBase,
	VALORDEBITO,
	VALORCREDITO,
	CASE 
		WHEN NOT ComprobanteManual.ValidaPC IS NULL THEN ComprobanteManual.ValidaPC
		WHEN TempMovimiento.TIPOCOMPROBANTE IN (2, 3, 6) THEN 'C'
		ELSE 'P'
	END AS TIPOTERCERO
FROM 
	TempMovimiento
LEFT OUTER JOIN 
	ComprobanteManual
	ON ComprobanteManual.Codigo = SUBSTRING(TempMovimiento.COMPROBANTENUMERO, 1, 2)
INNER JOIN 
	ZRC
	ON TempMovimiento.TASAIMPUESTO = ZRC.RCRTCD
	AND ZRC.RCEDTE <= DOCUMENTOFECHA

--VALORBASE REALIZA EL CALCULO FECHA >= ZRC.RCEDTE
INSERT INTO Movimiento (
	LedgerBookId,
	Periodo,
	ComprobanteLinea,
	ComprobanteNumero,
	ComprobanteFecha,
	DocumentoNumero,
	DocumentoFecha,
	DescripcionTransaccion,
	CodigoRazon,
	Valor,
	SegmentoCompania,
	SegmentoCuenta,
	SegmentoCentroCosto,
	SegmentoTercero,
	TasaImpuesto,
	ImpuestoRetencion,
	ImpuestoDescripcion,
	ValorBase,
	ValorDebito,
	ValorCredito,
	TipoTercero
) 
SELECT 
	@LedgerBookId,
	@PeriodoId,
	COMPROBANTELINEA,
	COMPROBANTENUMERO,
	COMPROBANTEFECHA,
	DOCUMENTONUMERO,
	DOCUMENTOFECHA,
	DESCRIPCIONTRANSACCION,
	CODIGORAZON,
	VALOR,
	SEGMENTOCOMPANIA,
	SEGMENTOCUENTA,
	SEGMENTOCENTROCOSTO,
	SEGMENTOTERCERO,
	TASAIMPUESTO,
	ZRC.RCNRTE AS ImpuestoRetencion, 
	ZRC.RCDESC AS ImpuestoDescripcion, 
	((TempMovimiento.VALOR / ZRC.RCNRTE) * 100) AS ValorBase,
	VALORDEBITO,
	VALORCREDITO,
	CASE 
		WHEN NOT ComprobanteManual.ValidaPC IS NULL THEN ComprobanteManual.ValidaPC
		WHEN TempMovimiento.TIPOCOMPROBANTE IN (2, 3, 6) THEN 'C'
		ELSE 'P'
	END AS TIPOTERCERO
FROM 
	TempMovimiento
LEFT OUTER JOIN 
	ComprobanteManual
	ON ComprobanteManual.Codigo = SUBSTRING(TempMovimiento.COMPROBANTENUMERO, 1, 2)
INNER JOIN 
	ZRC
	ON TempMovimiento.TASAIMPUESTO = ZRC.RCRTCD
	AND ZRC.RCEDTE >= DOCUMENTOFECHA
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'P' AND NAME = 'ActualizarCuentaJob') BEGIN
	DROP PROCEDURE dbo.ActualizarCuentaJob
END
GO

CREATE PROCEDURE dbo.ActualizarCuentaJob
AS
	EXEC msdb.dbo.sp_start_job N'ActualizarCuentas'
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'P' AND NAME = 'ConsultarEstado') BEGIN
	DROP PROCEDURE dbo.ConsultarEstado
END
GO

CREATE PROCEDURE dbo.ConsultarEstado
	@Tabla VARCHAR(50)
AS
DECLARE
	@Estado VARCHAR(1000),
	@Conteo1 INT,
	@Conteo2 INT,
	@Conteo3 INT

IF(@Tabla = 'MOVIMIENTO') BEGIN
	SELECT @Estado = Valor FROM Configuracion WHERE (Llave = 'PROCESOMOVIMIENTO')
	SELECT @Conteo1 = COUNT(*) FROM TempMovimiento
	SELECT @Conteo2 = COUNT(*) FROM Movimiento
	SELECT @Conteo3 = COUNT(*) FROM ErrorMvto
END
ELSE IF(@Tabla = 'TERCERO') BEGIN
	SELECT @Estado = Valor FROM Configuracion WHERE (Llave = 'PROCESOTERCERO')
	SELECT @Conteo1 = COUNT(*) FROM TempTercero
	SELECT @Conteo2 = COUNT(*) FROM Tercero
	SELECT @Conteo3 = COUNT(*) FROM ErrorTercero
END
ELSE BEGIN --CUENTAS
	SELECT @Estado = Valor FROM Configuracion WHERE (Llave = 'PROCESOCUENTA')
	SELECT @Conteo1 = COUNT(*) FROM SaldoCxP
	SELECT @Conteo2 = COUNT(*) FROM SaldoCxC
	SELECT @Conteo3 = COUNT(*) FROM SaldoOtro
END
SELECT @Estado AS Estado, @Conteo1 AS Conteo1, @Conteo2 AS Conteo2, @Conteo3 AS Conteo3
RETURN @@ROWCOUNT
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'P' AND NAME = 'ConsultarTercero') BEGIN
	DROP PROCEDURE dbo.ConsultarTercero
END
GO

CREATE PROCEDURE dbo.ConsultarTercero
	@Codigo NVARCHAR(50) = NULL,
	@CodigoFiscal NVARCHAR(50) = NULL,
	@RazonSocial NVARCHAR(200) = NULL,
	@EnError BIT = 0
AS
SET NOCOUNT ON
DECLARE
	@sql nvarchar(3000)
	, @sqlCriterios nvarchar(500)

IF(@EnError = 0) BEGIN
	SET @sql = ' 
	SELECT 
		Id,
		Identificador,
		TipoTercero,
		CONVERT(varchar(50), Codigo) AS Codigo,
		Secuencia,
		TipoIdentificacion,
		CodigoFiscal,
		Digito,
		Compania,
		RazonSocial,
		CodigoPais,
		CodigoCiudad,
		CodigoDpto,
		ClienteOficial,
		Direccion,
		'' '' Mensaje
	FROM 
		Tercero
	'
END
ELSE BEGIN 
	SET @sql = ' 
	SELECT 
		Id,
		Identificador,
		TipoTercero,
		Codigo,
		Secuencia,
		TipoIdentificacion,
		CodigoFiscal,
		Digito,
		Compania,
		RazonSocial,
		CodigoPais,
		CodigoCiudad,
		CodigoDpto,
		ClienteOficial,
		Direccion,
		Mensaje
	FROM 
		ErrorTercero Tercero
	'
END

SET @sqlCriterios = ''
IF(LEN(COALESCE(@Codigo, SPACE(0))) <> 0)
	SET @sqlCriterios = @sqlCriterios + '(Tercero.Codigo = @Codigo) AND '
IF(LEN(COALESCE(@CodigoFiscal, SPACE(0))) <> 0)
	SET @sqlCriterios = @sqlCriterios + '(Tercero.CodigoFiscal = @CodigoFiscal) AND '
IF(LEN(COALESCE(@RazonSocial, SPACE(0))) <> 0) BEGIN 
	SET @RazonSocial = '%' + REPLACE(@RazonSocial, SPACE(1), '%') + '%'
	SET @sqlCriterios = @sqlCriterios + '(Tercero.RazonSocial LIKE @RazonSocial) AND '
END 

--Agrega el where
IF(LEN(@sqlCriterios) <> 0)
	SET @sql = @sql + 'WHERE ' + SUBSTRING(@sqlCriterios, 1, LEN(@sqlCriterios) - 4)

EXECUTE sp_executesql @sql, N'@Codigo NVARCHAR(50), @CodigoFiscal NVARCHAR(50), @RazonSocial NVARCHAR(200), @EnError BIT ', @Codigo, @CodigoFiscal, @RazonSocial, @EnError
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'P' AND NAME = 'ConsultarMovimiento') BEGIN
	DROP PROCEDURE dbo.ConsultarMovimiento
END
GO

CREATE PROCEDURE dbo.ConsultarMovimiento
	@LedgerBookId int,
	@Periodo int,
	@EnError BIT 
AS

DECLARE
	@sql nvarchar(3000)
	, @sqlCriterios nvarchar(500)

IF(@EnError = 0) BEGIN
	SET @sql = ' 
	SELECT 
		Id,
		LedgerBookId,
		Periodo,
		ComprobanteLinea,
		ComprobanteNumero,
		ComprobanteFecha,
		DocumentoNumero,
		DocumentoFecha,
		DescripcionTransaccion,
		CodigoRazon,
		Valor,
		SegmentoCompania,
		SegmentoCuenta,
		SegmentoCentroCosto,
		CONVERT(varchar(50), SegmentoTercero) AS SegmentoTercero,
		TasaImpuesto,
		ImpuestoRetencion,
		ValorBase,
		ValorDebito,
		ValorCredito,
		TipoTercero,
		'''' AS Mensaje
	FROM 
		Movimiento
	'
END
ELSE BEGIN 
	SET @sql = ' 
	SELECT 
		Id,
		LedgerBookId,
		Periodo,
		ComprobanteLinea,
		ComprobanteNumero,
		ComprobanteFecha,
		DocumentoNumero,
		DocumentoFecha,
		DescripcionTransaccion,
		CodigoRazon,
		Valor,
		SegmentoCompania,
		SegmentoCuenta,
		SegmentoCentroCosto,
		SegmentoTercero,
		TasaImpuesto,
		ImpuestoRetencion,
		ValorBase,
		ValorDebito,
		ValorCredito,
		TipoTercero,
		Mensaje
	FROM 
		ErrorMvto Movimiento
	'
END

IF(ISNUMERIC(@LedgerBookId) = 0)
	SET @LedgerBookId = 0
IF(ISNUMERIC(@Periodo) = 0)
	SET @Periodo = 0

SET @sqlCriterios = ''
SET @sqlCriterios = @sqlCriterios + '(Movimiento.LedgerBookId = @LedgerBookId) AND '
SET @sqlCriterios = @sqlCriterios + '(Movimiento.Periodo = @Periodo) AND '

--Agrega el where
IF(LEN(@sqlCriterios) <> 0)
	SET @sql = @sql + 'WHERE ' + SUBSTRING(@sqlCriterios, 1, LEN(@sqlCriterios) - 4)

EXECUTE sp_executesql @sql, N'@LedgerBookId VARCHAR(8), @Periodo VARCHAR(2), @EnError BIT ', @LedgerBookId, @Periodo, @EnError
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'P' AND NAME = 'CrearMedioMagnetico') BEGIN
	DROP PROCEDURE dbo.CrearMedioMagnetico
END
GO

CREATE PROCEDURE dbo.CrearMedioMagnetico
	@LedgerBookId INT 
	, @Formato VARCHAR(50)
	, @Concepto VARCHAR(50)
	, @CuentaInicial VARCHAR(50)
	, @CuentaFinal VARCHAR(50)
	, @Tipo VARCHAR(1)
	, @CampoValor INT
AS

--DELETE FROM MedioMagneticoDian WHERE (LedgerBookId = @LedgerBookId)

INSERT INTO MedioMagneticoDian (Formato, Concepto, Cuentas, Tipo, Valor, LedgerBookId) 
SELECT 
	@Formato
	, @Concepto
	, Cuenta
	, @Tipo
	, @CampoValor
	, @LedgerBookId
FROM 
	LedgerAccount 
WHERE 
	(LedgerBookId = @LedgerBookId) 
	AND (Cuenta BETWEEN @CuentaInicial AND @CuentaFinal)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'P' AND NAME = 'ActualizarSaldoCxC') BEGIN
	DROP PROCEDURE dbo.ActualizarSaldoCxC
END
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'P' AND NAME = 'RetencionFuenteRenta') BEGIN
	DROP PROCEDURE dbo.RetencionFuenteRenta
END
GO

CREATE PROCEDURE dbo.RetencionFuenteRenta
	@LedgerBookId INT 
	, @Identificacion NVARCHAR(50)
AS 

--Trae la info del ledgerbook
DECLARE 
	@Clasificacion NVARCHAR(2)
	, @Ano INT 
	, @Compania INT

SET @Clasificacion = 'RT'

SELECT @Ano = Ano, @Compania = CompaniaId FROM LedgerBook WHERE (Id = @LedgerBookId)

--Trae la informacion del tercero
DECLARE
	@TipoIdentificacion NVARCHAR(1)
	, @Codigo NVARCHAR(50)
	, @RazonSocial NVARCHAR(200)
	, @TipoTercero NVARCHAR(1)
	, @Nit NVARCHAR(50)

SELECT TOP 1 
	@TipoIdentificacion = TipoIdentificacion
	, @Nit = CASE WHEN TipoIdentificacion = 'A' THEN CodigoFiscal + '-' + Digito ELSE CodigoFiscal END
	, @RazonSocial = RazonSocial
	, @TipoTercero = TipoTercero
	, @Codigo = Codigo 
FROM 
	Tercero 
WHERE 
	(CodigoFiscal = @Identificacion)
	AND (TipoTercero = 'P')

--Trae la informacion del declarante
DECLARE
	 @IdCompania NVARCHAR(50)
	, @Nombre NVARCHAR(50)
	, @Direccion NVARCHAR(50)
	, @CodigoCiudad NVARCHAR(50)
	, @CodigoDpto NVARCHAR(50)
	, @CodigoPais NVARCHAR(50)

--Trae la informacion de la compania por si se requiere
SELECT 
	@IdCompania = Id + '-' + Dv
	, @Nombre = Nombre
	, @Direccion = Direccion
	, @CodigoCiudad = CodigoCiudad
	, @CodigoDpto = CodigoDpto
	, @CodigoPais = CodigoPais 
FROM 
	Company
WHERE 
	(CODIGO = @Compania)

SELECT
	@Nombre AS NombreEmpresa 
	, @IdCompania AS NitEmpresa
	, @Direccion AS Direccion
	, @Ano AS AnoGravable
	, @Nit AS Identificacion
	, @RazonSocial AS RazonSocial
	, Movimiento.TasaImpuesto AS Codigo
	, Movimiento.ImpuestoDescripcion AS Concepto
	, ABS(Movimiento.ImpuestoRetencion) AS Impuesto
	, ABS(SUM(Movimiento.ValorBase)) AS ValorBase
	, ABS(SUM(Movimiento.Valor)) AS ValorRetenido
FROM
	Movimiento
INNER JOIN 
	LedgerAccount 
	ON Movimiento.LedgerBookId = LedgerAccount.LedgerBookId
	AND Movimiento.SegmentoCuenta = LedgerAccount.Cuenta
INNER JOIN 
	ClasificacionCuenta
	ON LedgerAccount.ClasificacionCuentaId = ClasificacionCuenta.Id
WHERE 
	(Movimiento.LedgerBookId = @LedgerBookId)
	AND (Movimiento.SegmentoTercero = @Codigo)
	AND (ClasificacionCuenta.Codigo = @Clasificacion)
GROUP BY 
	Movimiento.TasaImpuesto
	, Movimiento.ImpuestoDescripcion
	, Movimiento.ImpuestoRetencion
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'P' AND NAME = 'RetencionFuenteIca') BEGIN
	DROP PROCEDURE dbo.RetencionFuenteIca
END
GO

CREATE PROCEDURE dbo.RetencionFuenteIca
	@LedgerBookId INT 
	, @Identificacion NVARCHAR(50)
	, @Bimestre INT 
AS 

--Si el bimestre es cero fue que envio todos
IF (@Bimestre = 0)
	SET @Bimestre = NULL

DECLARE 
	@Clasificacion NVARCHAR(2)
	, @Ano INT 
	, @Compania INT
		
SET @Clasificacion = 'IC'

--Trae la info del ledgerbook
SELECT @Ano = Ano, @Compania = CompaniaId FROM LedgerBook WHERE (Id = @LedgerBookId)

--Trae la informacion del declarante
DECLARE
	 @IdCompania NVARCHAR(50)
	, @Nombre NVARCHAR(50)
	, @Direccion NVARCHAR(50)
	, @CodigoCiudad NVARCHAR(50)
	, @CodigoDpto NVARCHAR(50)
	, @CodigoPais NVARCHAR(50)

--Trae la informacion de la compania por si se requiere
SELECT 
	@IdCompania = Id + '-' + Dv
	, @Nombre = Nombre
	, @Direccion = Direccion
	, @CodigoCiudad = CodigoCiudad
	, @CodigoDpto = CodigoDpto
	, @CodigoPais = CodigoPais 
FROM 
	Company
WHERE 
	(CODIGO = @Compania)

SELECT
	Bimestre.Descripcion AS Bimestre
	, @Nombre AS NombreEmpresa 
	, @IdCompania AS NitEmpresa
	, @Direccion AS Direccion
	, @Ano AS AnoGravable
	, CASE WHEN Tercero.TipoIdentificacion = 'A' THEN Tercero.CodigoFiscal + '-' + Tercero.Digito ELSE Tercero.CodigoFiscal END AS Identificacion
	, Tercero.RazonSocial AS RazonSocial
	, Movimiento.TasaImpuesto AS Codigo
	, Movimiento.ImpuestoDescripcion AS Concepto
	, ABS(SUM(Movimiento.ValorBase)) AS ValorBase
	, ABS(Movimiento.ImpuestoRetencion) AS PorcentajeRet
	, ABS(SUM(Movimiento.Valor)) AS ValorRetenido
FROM
	Movimiento
INNER JOIN 
	Tercero
	ON Movimiento.SegmentoTercero = Tercero.Codigo
	AND Movimiento.TipoTercero = Tercero.TipoTercero
INNER JOIN 
	LedgerAccount 
	ON Movimiento.LedgerBookId = LedgerAccount.LedgerBookId
	AND Movimiento.SegmentoCuenta = LedgerAccount.Cuenta
INNER JOIN 
	ClasificacionCuenta
	ON LedgerAccount.ClasificacionCuentaId = ClasificacionCuenta.Id
INNER JOIN 
	Bimestre
	ON Movimiento.Periodo = Bimestre.Periodo
WHERE 
	(Movimiento.LedgerBookId = @LedgerBookId)
	AND (Tercero.CodigoFiscal = @Identificacion)
	AND (Movimiento.TipoTercero = 'P')
	AND (ClasificacionCuenta.Codigo = @Clasificacion)
	AND (Bimestre.Codigo = COALESCE(@Bimestre, Bimestre.Codigo))
GROUP BY 
	Bimestre.Descripcion
	, Tercero.TipoIdentificacion
	, Tercero.CodigoFiscal
	, Tercero.Digito
	, Tercero.RazonSocial
	, Movimiento.TasaImpuesto
	, Movimiento.ImpuestoDescripcion
	, Movimiento.ImpuestoRetencion
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'P' AND NAME = 'RetencionFuenteIva') BEGIN
	DROP PROCEDURE dbo.RetencionFuenteIva
END
GO

CREATE PROCEDURE dbo.RetencionFuenteIva
	@LedgerBookId INT 
	, @Identificacion NVARCHAR(50)
	, @Bimestre INT 

AS 

--Si el bimestre es cero fue que envio todos
IF (@Bimestre = 0)
	SET @Bimestre = NULL

DECLARE 
	@Clasificacion NVARCHAR(2)
	, @Ano INT 
	, @Compania INT

SET @Clasificacion = 'RI'

--Trae la info del ledgerbook
SELECT @Ano = Ano, @Compania = CompaniaId FROM LedgerBook WHERE (Id = @LedgerBookId)

--Trae la informacion del declarante
DECLARE
	 @IdCompania NVARCHAR(50)
	, @Nombre NVARCHAR(50)
	, @Direccion NVARCHAR(50)
	, @CodigoCiudad NVARCHAR(50)
	, @CodigoDpto NVARCHAR(50)
	, @CodigoPais NVARCHAR(50)

--Trae la informacion de la compania por si se requiere
SELECT 
	@IdCompania = Id + '-' + Dv
	, @Nombre = Nombre
	, @Direccion = Direccion
	, @CodigoCiudad = CodigoCiudad
	, @CodigoDpto = CodigoDpto
	, @CodigoPais = CodigoPais 
FROM 
	Company
WHERE 
	(CODIGO = @Compania)

SELECT
	Bimestre.Descripcion AS Bimestre
	, @Nombre AS NombreEmpresa 
	, @IdCompania AS NitEmpresa
	, @Direccion AS Direccion
	, @Ano AS AnoGravable
	, CASE WHEN Tercero.TipoIdentificacion = 'A' THEN Tercero.CodigoFiscal + '-' + Tercero.Digito ELSE Tercero.CodigoFiscal END AS Identificacion
	, Tercero.RazonSocial AS RazonSocial
	, Movimiento.TasaImpuesto AS Codigo
	, Movimiento.ImpuestoDescripcion AS Concepto
	, ABS(SUM(Movimiento.ValorBase)) AS ValorBase
	, 16 AS PorcentajeIva
	, ABS(SUM(Movimiento.Valor) * 2) AS ValorIva
	, ABS(Movimiento.ImpuestoRetencion) AS PorcentajeRet
	, ABS(SUM(Movimiento.Valor)) AS ValorRetenido
FROM
	Movimiento
INNER JOIN 
	Tercero
	ON Movimiento.SegmentoTercero = Tercero.Codigo
	AND Movimiento.TipoTercero = Tercero.TipoTercero
INNER JOIN 
	LedgerAccount 
	ON Movimiento.LedgerBookId = LedgerAccount.LedgerBookId
	AND Movimiento.SegmentoCuenta = LedgerAccount.Cuenta
INNER JOIN 
	ClasificacionCuenta
	ON LedgerAccount.ClasificacionCuentaId = ClasificacionCuenta.Id
INNER JOIN 
	Bimestre
	ON Movimiento.Periodo = Bimestre.Periodo
WHERE 
	(Movimiento.LedgerBookId = @LedgerBookId)
	AND (Tercero.CodigoFiscal = @Identificacion)
	AND (Movimiento.TipoTercero = 'P')
	AND (ClasificacionCuenta.Codigo = @Clasificacion)
	AND (Bimestre.Codigo = COALESCE(@Bimestre, Bimestre.Codigo))
GROUP BY 
	Bimestre.Descripcion
	, Tercero.TipoIdentificacion
	, Tercero.CodigoFiscal
	, Tercero.Digito
	, Tercero.RazonSocial
	, Movimiento.TasaImpuesto
	, Movimiento.ImpuestoDescripcion
	, Movimiento.ImpuestoRetencion
GO

IF EXISTS (SELECT * FROM sysobjects WHERE TYPE = 'P' AND NAME = 'ConsultarPagoImpuesto') BEGIN
	DROP PROCEDURE dbo.ConsultarPagoImpuesto
END
GO

CREATE PROCEDURE dbo.ConsultarPagoImpuesto
	@LedgerBookId INT 
	, @Bimestre INT 
AS

IF(@Bimestre = 0)
	SET @Bimestre = NULL
SELECT 
	NumeroDeclaracion AS Declaracion,
	FechaDeclaracion,
	Sticker,
	FechaPago 
FROM 
	PagoImpuesto 
WHERE 
	(LedgerBookId = @LedgerBookId) 
	AND (Bimestre = COALESCE(@Bimestre, Bimestre))
ORDER BY 
	NumeroPago
GO
