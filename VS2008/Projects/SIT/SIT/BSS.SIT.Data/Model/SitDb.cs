﻿
namespace BSS.SIT.Data.Model
{
    /// <summary>
    /// Clase BSS.SIT.Data.Model.SitDb
    /// </summary>
    public class SitDb : SITRepositorioDataContext
    {
        public SitDb()
            : base(System.Configuration.ConfigurationManager.ConnectionStrings["SITConnectionString"].ConnectionString)
        {
            this.CommandTimeout = 0;
        }
    }
}
