﻿using System;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Security.Permissions;
using IBM.Data.DB2.iSeries;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace BSS.SIT.Data.DB2
{
    /// <summary>
    /// Clase BSS.Brinsa.MP2.Libreria.Dao.DB2Database
    /// </summary>
    [SecurityPermission(SecurityAction.InheritanceDemand, UnmanagedCode = true)]
    [SecurityPermission(SecurityAction.Demand, UnmanagedCode = true)]
    public class Database
    {
        #region Miembros
        /// <summary>
        /// cadena vacia
        /// </summary>
        internal string exceptionNullOrEmptyString = "Cadena vacia";
        /// <summary>
        /// connectionString
        /// </summary>
        private readonly string connectionString;
        #endregion

        #region Propiedades
        /// <summary>
        /// Obtiene o establece connection string
        /// </summary>
        /// <value></value>
        public string ConnectionString
        {
            get { return connectionString; }
        }
        #endregion

        #region Constructor
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="BSS.Brinsa.MP2.Libreria.Dao.DB2Database"/>
        /// </summary>
        /// <param name="connectionName">el/la connection name</param>
        public Database(string connectionName)
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
            if (string.IsNullOrEmpty(connectionString)) throw new ArgumentException(exceptionNullOrEmptyString, "connectionString");
        }

        /// <summary>
        /// Destructor database
        /// </summary>
        ~Database() { }
        #endregion

        #region Metodos
        /// <summary>
        /// create connection
        /// </summary>
        /// <returns>IBM.Data.DB2.iSeries.iDB2Connection</returns>
        public virtual iDB2Connection CreateConnection()
        {
            iDB2Connection newConnection = new iDB2Connection();
            newConnection.ConnectionString = ConnectionString;
            return newConnection;
        }

        /// <summary>
        /// get SQL string command
        /// </summary>
        /// <param name="query">el/la query</param>
        /// <returns>IBM.Data.DB2.iSeries.iDB2Command</returns>
        public iDB2Command GetSqlStringCommand(string query)
        {
            if (string.IsNullOrEmpty(query)) throw new ArgumentException(exceptionNullOrEmptyString, "query");
            return CreateCommandByCommandType(CommandType.Text, query);
        }

        /// <summary>
        /// create command by command type
        /// </summary>
        /// <param name="commandType">el/la command type</param>
        /// <param name="commandText">el/la command text</param>
        /// <returns>IBM.Data.DB2.iSeries.iDB2Command</returns>
        private iDB2Command CreateCommandByCommandType(CommandType commandType, string commandText)
        {
            iDB2Command command = new iDB2Command();
            command.CommandType = commandType;
            command.CommandText = commandText;
            return command;
        }

        /// <summary>
        /// execute reader
        /// </summary>
        /// <param name="command">el/la command</param>
        /// <returns>IBM.Data.DB2.iSeries.iDB2DataReader</returns>
        public virtual iDB2DataReader ExecuteReader(iDB2Command command)
        {
            ConnectionWrapper wrapper = GetOpenConnection(false);

            try
            {
                //
                // JS-L: I moved the PrepareCommand inside the try because it can fail.
                //
                PrepareCommand(command, wrapper.Connection);

                //
                // If there is a current transaction, we'll be using a shared connection, so we don't
                // want to close the connection when we're done with the reader.
                //
                return DoExecuteReader(command, CommandBehavior.CloseConnection);
            }
            catch
            {
                wrapper.Connection.Close();
                throw;
            }
        }

        /// <summary>
        /// execute scalar
        /// </summary>
        /// <param name="command">el/la command</param>
        /// <returns>System.Object</returns>
        public virtual object ExecuteScalar(iDB2Command command)
        {
            if (command == null) throw new ArgumentNullException("command");

            using (ConnectionWrapper wrapper = GetOpenConnection())
            {
                PrepareCommand(command, wrapper.Connection);
                return DoExecuteScalar(command);
            }
        }

        /// <summary>
        /// execute non query
        /// </summary>
        /// <param name="command">el/la command</param>
        /// <returns>System.Int32</returns>
        public virtual int ExecuteNonQuery(iDB2Command command)
        {
            using (ConnectionWrapper wrapper = GetOpenConnection())
            {
                PrepareCommand(command, wrapper.Connection);
                return DoExecuteNonQuery(command);
            }
        }

        /// <summary>
        /// execute data set
        /// </summary>
        /// <param name="command">el/la command</param>
        /// <returns>System.Data.DataSet</returns>
        public virtual DataSet ExecuteDataSet(iDB2Command command)
        {
            DataSet dataSet = new DataSet();
            dataSet.Locale = CultureInfo.InvariantCulture;
            LoadDataSet(command, dataSet, "Table");
            return dataSet;
        }

        /// <summary>
        /// load data set
        /// </summary>
        /// <param name="command">el/la command</param>
        /// <param name="dataSet">el/la data set</param>
        /// <param name="tableName">el/la table name</param>
        public virtual void LoadDataSet(iDB2Command command, DataSet dataSet, string tableName)
        {
            LoadDataSet(command, dataSet, new string[] { tableName });
        }

        /// <summary>
        /// load data set
        /// </summary>
        /// <param name="command">el/la command</param>
        /// <param name="dataSet">el/la data set</param>
        /// <param name="tableNames">el/la table names</param>
        public virtual void LoadDataSet(iDB2Command command, DataSet dataSet, string[] tableNames)
        {
            using (ConnectionWrapper wrapper = GetOpenConnection())
            {
                PrepareCommand(command, wrapper.Connection);
                DoLoadDataSet(command, dataSet, tableNames);
            }
        }


        /// <summary>
        /// get open connection
        /// </summary>
        /// <returns>
        /// BSS.Brinsa.MP2.Libreria.Dao.DB2Database.ConnectionWrapper
        /// </returns>
        protected ConnectionWrapper GetOpenConnection()
        {
            return GetOpenConnection(true);
        }

        /// <summary>
        /// get open connection
        /// </summary>
        /// <param name="disposeInnerConnection">el/la dispose inner connection</param>
        /// <returns>
        /// BSS.Brinsa.MP2.Libreria.Dao.DB2Database.ConnectionWrapper
        /// </returns>
        protected ConnectionWrapper GetOpenConnection(bool disposeInnerConnection)
        {
            iDB2Connection connection = null;
            if (connection != null)
                return new ConnectionWrapper(connection, false);
            else
                return new ConnectionWrapper(GetNewOpenConnection(), disposeInnerConnection);
        }

        /// <summary>
        /// get new open connection
        /// </summary>
        /// <returns>IBM.Data.DB2.iSeries.iDB2Connection</returns>
        internal iDB2Connection GetNewOpenConnection()
        {
            iDB2Connection connection = null;
            try
            {
                try
                {
                    connection = CreateConnection();
                    connection.Open();
                }
                catch (Exception)
                {
                    throw;
                }
            }
            catch
            {
                if (connection != null)
                    connection.Close();

                throw;
            }

            return connection;
        }

        /// <summary>
        /// <para>Assigns a <paramref name="connection"/> to the <paramref name="command"/> and discovers parameters if needed.</para>
        /// </summary>
        /// <param name="command"><para>The command that contains the query to prepare.</para></param>
        /// <param name="connection">The connection to assign to the command.</param>
        protected static void PrepareCommand(iDB2Command command, iDB2Connection connection)
        {
            if (command == null) throw new ArgumentNullException("command");
            if (connection == null) throw new ArgumentNullException("connection");

            command.Connection = connection;
        }

        /// <summary>
        /// <para>Assigns a <paramref name="transaction"/> to the <paramref name="command"/> and discovers parameters if needed.</para>
        /// </summary>
        /// <param name="command"><para>The command that contains the query to prepare.</para></param>
        /// <param name="transaction">The transaction to assign to the command.</param>
        protected static void PrepareCommand(iDB2Command command, iDB2Transaction transaction)
        {
            if (command == null) throw new ArgumentNullException("command");
            if (transaction == null) throw new ArgumentNullException("transaction");

            PrepareCommand(command, transaction.Connection);
            command.Transaction = transaction;
        }

        /// <summary>
        /// do execute scalar
        /// </summary>
        /// <param name="command">el/la command</param>
        /// <returns>System.Object</returns>
        object DoExecuteScalar(iDB2Command command)
        {
            try
            {
                DateTime startTime = DateTime.Now;
                object returnValue = command.ExecuteScalar();
                return returnValue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// do execute non query
        /// </summary>
        /// <param name="command">el/la command</param>
        /// <returns>System.Int32</returns>
        protected int DoExecuteNonQuery(iDB2Command command)
        {
            try
            {
                DateTime startTime = DateTime.Now;
                int rowsAffected = command.ExecuteNonQuery();
                return rowsAffected;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// do execute reader
        /// </summary>
        /// <param name="command">el/la command</param>
        /// <param name="cmdBehavior">el/la CMD behavior</param>
        /// <returns>IBM.Data.DB2.iSeries.iDB2DataReader</returns>
        iDB2DataReader DoExecuteReader(iDB2Command command, CommandBehavior cmdBehavior)
        {
            try
            {
                
                DateTime startTime = DateTime.Now;
                iDB2DataReader reader = command.ExecuteReader(cmdBehavior);
                return reader;
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// do load data set
        /// </summary>
        /// <param name="command">el/la command</param>
        /// <param name="dataSet">el/la data set</param>
        /// <param name="tableNames">el/la table names</param>
        void DoLoadDataSet(iDB2Command command, DataSet dataSet, string[] tableNames)
        {
            if (tableNames == null) throw new ArgumentNullException("tableNames");
            if (tableNames.Length == 0)
            {
                throw new ArgumentException(exceptionNullOrEmptyString, "tableNames");
            }
            for (int i = 0; i < tableNames.Length; i++)
            {
                if (string.IsNullOrEmpty(tableNames[i])) throw new ArgumentException(exceptionNullOrEmptyString, string.Concat("tableNames[", i, "]"));
            }

            using (DbDataAdapter adapter = GetDataAdapter(UpdateBehavior.Standard))
            {
                ((IDbDataAdapter)adapter).SelectCommand = command;

                try
                {
                    DateTime startTime = DateTime.Now;
                    string systemCreatedTableNameRoot = "Table";
                    for (int i = 0; i < tableNames.Length; i++)
                    {
                        string systemCreatedTableName = (i == 0) ? systemCreatedTableNameRoot : systemCreatedTableNameRoot + i;

                        adapter.TableMappings.Add(systemCreatedTableName, tableNames[i]);
                    }

                    adapter.Fill(dataSet);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// get data adapter
        /// </summary>
        /// <returns>System.Data.Common.DbDataAdapter</returns>
        public DbDataAdapter GetDataAdapter()
        {
            return GetDataAdapter(UpdateBehavior.Standard);
        }

        /// <summary>
        /// get data adapter
        /// </summary>
        /// <param name="updateBehavior">el/la update behavior</param>
        /// <returns>System.Data.Common.DbDataAdapter</returns>
        protected DbDataAdapter GetDataAdapter(UpdateBehavior updateBehavior)
        {
            DbDataAdapter adapter = new iDB2DataAdapter();

            if (updateBehavior == UpdateBehavior.Continue)
            {
                SetUpRowUpdatedEvent(adapter);
            }
            return adapter;
        }

        /// <summary>
        /// set up row updated event
        /// </summary>
        /// <param name="adapter">el/la adapter</param>
        protected virtual void SetUpRowUpdatedEvent(DbDataAdapter adapter) { }
        #endregion

        #region Clases internas
        /// <summary>
        /// Clase BSS.Brinsa.MP2.Libreria.Dao.DB2Database.ConnectionWrapper
        /// </summary>
        protected class ConnectionWrapper : IDisposable
        {
            readonly iDB2Connection connection;
            readonly bool disposeConnection;

            /// <summary>
            /// Inicializa una nueva instancia de la clase <see cref="BSS.Brinsa.MP2.Libreria.Dao.DB2Database.ConnectionWrapper"/>
            /// </summary>
            /// <param name="connection">el/la connection</param>
            /// <param name="disposeConnection">el/la dispose connection</param>
            public ConnectionWrapper(iDB2Connection connection, bool disposeConnection)
            {
                this.connection = connection;
                this.disposeConnection = disposeConnection;
            }

            /// <summary>
            ///		Gets the actual connection.
            /// </summary>
            public iDB2Connection Connection
            {
                get { return connection; }
            }

            /// <summary>
            ///		Dispose the wrapped connection, if appropriate.
            /// </summary>
            public void Dispose()
            {
                if (disposeConnection)
                    connection.Dispose();
            }
        }
        #endregion
    }
}