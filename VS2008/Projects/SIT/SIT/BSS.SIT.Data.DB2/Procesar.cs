﻿using System;
using BSS.SIT.Common;

namespace BSS.SIT.Data.DB2
{
    /// <summary>
    /// Clase BSS.Brinsa.MP2.Libreria.Servicio.ProcesarDB2 (posee todos los metodos utilizados hacia el servidor DB2)
    /// </summary>
    public class ProcesarDB2 : Data
    {
        #region Miembros
        #endregion

        #region Propiedades
        #endregion

        #region Constructor
        /// <summary>
        /// Inicializa una nueva instancia de la clase <see cref="BSS.Brinsa.MP2.Libreria.Servicio.ProcesarDB2"/>
        /// </summary>
        public ProcesarDB2() { }
        #endregion

        #region Metodos
        /// <summary>
        /// envia una consulta para insertar o actualizar
        /// </summary>
        /// <param name="sql">The SQL.</param>
        /// <returns>
        /// System.Boolean
        /// </returns>
        public bool Actualizar(string sql)
        {
            Query = sql;
            Comando = BD.GetSqlStringCommand(Query);
            Comando.CommandTimeout = Utility.CommandTimeout;

            try
            {
                Registro = BD.ExecuteNonQuery(Comando);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// existe dato
        /// </summary>
        /// <param name="query">el/la query</param>
        /// <returns>System.Boolean</returns>
        public bool ExisteDato(string query)
        {
            Query = query;
            Comando = BD.GetSqlStringCommand(Query);
            Comando.CommandTimeout = Utility.CommandTimeout;

            try
            {
                Registro = (int)BD.ExecuteScalar(Comando);

                if (Registro != 0)
                    return true;
                return false;
            }
            catch (Exception ex)
            {
                //Log.For(this).Error(string.Format("Validando si existe el query en AS400 ({0})", Query), ex);
                throw ex;
            }
        }
        #endregion
    }
}