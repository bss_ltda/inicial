﻿Module NombresDIAN
    Dim oNombre(4) As String
    Dim Parte As Integer

    Function NombreDIAN(ByVal NombreCompleto As String) As String
        Dim aNombre() As String
        Dim i As Integer
        Dim bCompuesto As Boolean
        Dim Dato As String
        Dim Casada As Integer
        Dim Apell01 As String = ""
        Dim Apell02 As String = ""
        Dim Nombre01 As String = ""
        Dim Nombre02 As String = ""

        NombreDIAN = ""
        Dato = UCase(Trim(CampoAlfa(Trim(NombreCompleto))))
        Do While InStr(1, Dato, "  ") > 0
            Dato = Replace(Dato, "  ", " ")
        Loop
        aNombre = Split(Trim(Dato), " ")
        Parte = 0 'Primer Apellido
        For i = 0 To UBound(aNombre)
            Select Case UCase(Trim(aNombre(i)))
                Case "DE", "LA", "DEL", "LOS", "LAS"
                    Agrega((Trim(aNombre(i))), oNombre(Parte))
                    bCompuesto = True
                Case Else
                    If Not bCompuesto Then
                        If i > 0 Then
                            If Parte < 3 Then
                                Parte = Parte + 1
                            End If
                        End If
                    End If
                    bCompuesto = False
                    Agrega(Trim(aNombre(i)), oNombre(Parte))
            End Select
            'QueNombre
        Next
        If InStr(1, oNombre(0), " DE LOS ") = 0 And InStr(1, oNombre(0), " DE LAS ") = 0 And _
            InStr(1, oNombre(0), " DE LA ") = 0 Then
            Casada = InStr(1, oNombre(0), " DE ")
            If Casada > 0 Then
                If oNombre(1) <> "" Then
                    If oNombre(2) = "" Then
                        oNombre(2) = oNombre(1)
                    ElseIf oNombre(3) = "" Then
                        oNombre(3) = oNombre(2)
                        oNombre(2) = oNombre(1)
                    Else
                        oNombre(3) = oNombre(3) & " " & oNombre(2)
                        oNombre(2) = oNombre(1)
                    End If
                End If
                oNombre(1) = Trim(Mid(oNombre(0), Casada))
                oNombre(0) = Left(oNombre(0), Casada)
                If Parte < 3 Then Parte = Parte + 1
            End If
        End If

        Select Case Parte
            Case 0
                Apell01 = oNombre(0)
            Case 1
                Apell01 = oNombre(0)
                Nombre01 = oNombre(1)
            Case 2
                Apell01 = oNombre(0)
                Apell02 = oNombre(1)
                Nombre01 = oNombre(2)
            Case 3
                Apell01 = oNombre(0)
                Apell02 = oNombre(1)
                Nombre01 = oNombre(2)
                Nombre02 = oNombre(3)
        End Select

        For i = 0 To 3
            oNombre(i) = ""
        Next

        NombreDIAN = Apell01 & "|" & Apell02 & "|" & Nombre01 & "|" & Nombre02

    End Function

    Sub Agrega(ByVal Parte1 As String, ByVal Parte2 As String)

        oNombre(Parte) = IIf(Trim(Parte1) = "", Parte2, Parte2 & " " & Parte1)

    End Sub

    Public Function CampoAlfa(ByVal Dato As String) As String
        Dim i As Integer
        Dim ascii As Integer

        CampoAlfa = ""
        For i = 1 To Len(Dato)
            ascii = Asc(Mid(Dato, i, 1))
            If InStr(1, " ", Chr(ascii)) > 0 Then
                CampoAlfa = CampoAlfa & Chr(ascii)
                '!@#$&*()-_=+{}[]\|:;.?/<>
            ElseIf Chr(ascii) = "á" Then
                CampoAlfa = CampoAlfa & "a"
            ElseIf Chr(ascii) = "é" Then
                CampoAlfa = CampoAlfa & "e"
            ElseIf Chr(ascii) = "í" Then
                CampoAlfa = CampoAlfa & "i"
            ElseIf Chr(ascii) = "ó" Then
                CampoAlfa = CampoAlfa & "o"
            ElseIf Chr(ascii) = "ú" Then
                CampoAlfa = CampoAlfa & "u"
            ElseIf Chr(ascii) = "ñ" Then
                CampoAlfa = CampoAlfa & "n"
            ElseIf ascii = 10 Or ascii = 13 Then
                CampoAlfa = CampoAlfa & " "
            ElseIf ascii >= Asc("0") And ascii <= Asc("9") Then
                CampoAlfa = CampoAlfa & Chr(ascii)
            ElseIf ascii >= Asc("A") And ascii <= Asc("Z") Then
                CampoAlfa = CampoAlfa & Chr(ascii)
            ElseIf ascii >= Asc("a") And ascii <= Asc("z") Then
                CampoAlfa = CampoAlfa & Chr(ascii)
            End If
        Next

    End Function



End Module
