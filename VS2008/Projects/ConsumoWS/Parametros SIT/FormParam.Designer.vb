﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormParametros
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.ButtonWebConfig = New System.Windows.Forms.Button()
        Me.ButtonInstallSIT = New System.Windows.Forms.Button()
        Me.ButtonScriptsSQL = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtFolderDTS = New System.Windows.Forms.TextBox()
        Me.ButtonFolderDTS = New System.Windows.Forms.Button()
        Me.ButtonFolderDescarga = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtFolderDescarga = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cmdPruebaSQL = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtPass = New System.Windows.Forms.TextBox()
        Me.txtUser = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtIntialCatalog = New System.Windows.Forms.TextBox()
        Me.txtDataSource = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cmdPruebaAS400 = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtLIB400 = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtDB400 = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtPass400 = New System.Windows.Forms.TextBox()
        Me.txtUser400 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtAS400 = New System.Windows.Forms.TextBox()
        Me.cmdAS400 = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Location = New System.Drawing.Point(9, 52)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(670, 398)
        Me.TabControl1.TabIndex = 1
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.SystemColors.Control
        Me.TabPage1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TabPage1.Controls.Add(Me.ButtonWebConfig)
        Me.TabPage1.Controls.Add(Me.ButtonInstallSIT)
        Me.TabPage1.Controls.Add(Me.ButtonScriptsSQL)
        Me.TabPage1.Controls.Add(Me.GroupBox3)
        Me.TabPage1.Controls.Add(Me.GroupBox2)
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Controls.Add(Me.cmdAS400)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(662, 372)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "LX/BPCS"
        '
        'ButtonWebConfig
        '
        Me.ButtonWebConfig.Location = New System.Drawing.Point(360, 337)
        Me.ButtonWebConfig.Name = "ButtonWebConfig"
        Me.ButtonWebConfig.Size = New System.Drawing.Size(162, 25)
        Me.ButtonWebConfig.TabIndex = 44
        Me.ButtonWebConfig.Text = "webConfig"
        Me.ButtonWebConfig.UseVisualStyleBackColor = True
        '
        'ButtonInstallSIT
        '
        Me.ButtonInstallSIT.Enabled = False
        Me.ButtonInstallSIT.Location = New System.Drawing.Point(186, 336)
        Me.ButtonInstallSIT.Name = "ButtonInstallSIT"
        Me.ButtonInstallSIT.Size = New System.Drawing.Size(162, 25)
        Me.ButtonInstallSIT.TabIndex = 43
        Me.ButtonInstallSIT.Text = "Instalar la Aplicacion"
        Me.ButtonInstallSIT.UseVisualStyleBackColor = True
        '
        'ButtonScriptsSQL
        '
        Me.ButtonScriptsSQL.Enabled = False
        Me.ButtonScriptsSQL.Location = New System.Drawing.Point(12, 337)
        Me.ButtonScriptsSQL.Name = "ButtonScriptsSQL"
        Me.ButtonScriptsSQL.Size = New System.Drawing.Size(162, 25)
        Me.ButtonScriptsSQL.TabIndex = 36
        Me.ButtonScriptsSQL.Text = "Ejecutar Scripts de SQLServer"
        Me.ButtonScriptsSQL.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtFolderDTS)
        Me.GroupBox3.Controls.Add(Me.ButtonFolderDTS)
        Me.GroupBox3.Controls.Add(Me.ButtonFolderDescarga)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.txtFolderDescarga)
        Me.GroupBox3.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GroupBox3.Location = New System.Drawing.Point(12, 6)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(613, 93)
        Me.GroupBox3.TabIndex = 42
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Carpetas"
        '
        'txtFolderDTS
        '
        Me.txtFolderDTS.Location = New System.Drawing.Point(92, 19)
        Me.txtFolderDTS.Name = "txtFolderDTS"
        Me.txtFolderDTS.Size = New System.Drawing.Size(474, 20)
        Me.txtFolderDTS.TabIndex = 50
        '
        'ButtonFolderDTS
        '
        Me.ButtonFolderDTS.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonFolderDTS.Location = New System.Drawing.Point(571, 17)
        Me.ButtonFolderDTS.Name = "ButtonFolderDTS"
        Me.ButtonFolderDTS.Size = New System.Drawing.Size(24, 20)
        Me.ButtonFolderDTS.TabIndex = 36
        Me.ButtonFolderDTS.UseVisualStyleBackColor = True
        '
        'ButtonFolderDescarga
        '
        Me.ButtonFolderDescarga.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ButtonFolderDescarga.Location = New System.Drawing.Point(571, 45)
        Me.ButtonFolderDescarga.Name = "ButtonFolderDescarga"
        Me.ButtonFolderDescarga.Size = New System.Drawing.Size(24, 20)
        Me.ButtonFolderDescarga.TabIndex = 36
        Me.ButtonFolderDescarga.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(7, 45)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(58, 13)
        Me.Label11.TabIndex = 49
        Me.Label11.Text = "Descargas"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(7, 19)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(79, 13)
        Me.Label10.TabIndex = 48
        Me.Label10.Text = "Procedimientos"
        '
        'txtFolderDescarga
        '
        Me.txtFolderDescarga.Location = New System.Drawing.Point(92, 45)
        Me.txtFolderDescarga.Name = "txtFolderDescarga"
        Me.txtFolderDescarga.Size = New System.Drawing.Size(474, 20)
        Me.txtFolderDescarga.TabIndex = 47
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cmdPruebaSQL)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.txtPass)
        Me.GroupBox2.Controls.Add(Me.txtUser)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.txtIntialCatalog)
        Me.GroupBox2.Controls.Add(Me.txtDataSource)
        Me.GroupBox2.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GroupBox2.Location = New System.Drawing.Point(320, 105)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(306, 225)
        Me.GroupBox2.TabIndex = 41
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "SQL Server"
        '
        'cmdPruebaSQL
        '
        Me.cmdPruebaSQL.Location = New System.Drawing.Point(28, 160)
        Me.cmdPruebaSQL.Name = "cmdPruebaSQL"
        Me.cmdPruebaSQL.Size = New System.Drawing.Size(105, 25)
        Me.cmdPruebaSQL.TabIndex = 50
        Me.cmdPruebaSQL.Text = "Probar Conexion"
        Me.cmdPruebaSQL.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(32, 104)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(53, 13)
        Me.Label4.TabIndex = 48
        Me.Label4.Text = "Password"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(32, 78)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(43, 13)
        Me.Label3.TabIndex = 47
        Me.Label3.Text = "Usuario"
        '
        'txtPass
        '
        Me.txtPass.Location = New System.Drawing.Point(140, 104)
        Me.txtPass.Name = "txtPass"
        Me.txtPass.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPass.Size = New System.Drawing.Size(147, 20)
        Me.txtPass.TabIndex = 45
        '
        'txtUser
        '
        Me.txtUser.Location = New System.Drawing.Point(140, 78)
        Me.txtUser.Name = "txtUser"
        Me.txtUser.Size = New System.Drawing.Size(147, 20)
        Me.txtUser.TabIndex = 44
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(32, 52)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(77, 13)
        Me.Label2.TabIndex = 42
        Me.Label2.Text = "Base de Datos"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(32, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(101, 13)
        Me.Label1.TabIndex = 40
        Me.Label1.Text = "Servidor SQLServer"
        '
        'txtIntialCatalog
        '
        Me.txtIntialCatalog.Location = New System.Drawing.Point(140, 52)
        Me.txtIntialCatalog.Name = "txtIntialCatalog"
        Me.txtIntialCatalog.Size = New System.Drawing.Size(147, 20)
        Me.txtIntialCatalog.TabIndex = 43
        Me.txtIntialCatalog.Text = "SIT"
        '
        'txtDataSource
        '
        Me.txtDataSource.Location = New System.Drawing.Point(140, 26)
        Me.txtDataSource.Name = "txtDataSource"
        Me.txtDataSource.Size = New System.Drawing.Size(147, 20)
        Me.txtDataSource.TabIndex = 41
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cmdPruebaAS400)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.txtLIB400)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtDB400)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtPass400)
        Me.GroupBox1.Controls.Add(Me.txtUser400)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.txtAS400)
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.ControlText
        Me.GroupBox1.Location = New System.Drawing.Point(12, 105)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(288, 225)
        Me.GroupBox1.TabIndex = 38
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "AS400"
        '
        'cmdPruebaAS400
        '
        Me.cmdPruebaAS400.Location = New System.Drawing.Point(10, 160)
        Me.cmdPruebaAS400.Name = "cmdPruebaAS400"
        Me.cmdPruebaAS400.Size = New System.Drawing.Size(105, 25)
        Me.cmdPruebaAS400.TabIndex = 50
        Me.cmdPruebaAS400.Text = "Probar Conexion"
        Me.cmdPruebaAS400.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(26, 78)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(85, 13)
        Me.Label9.TabIndex = 48
        Me.Label9.Text = "Libreria de datos"
        '
        'txtLIB400
        '
        Me.txtLIB400.Location = New System.Drawing.Point(117, 78)
        Me.txtLIB400.Name = "txtLIB400"
        Me.txtLIB400.Size = New System.Drawing.Size(147, 20)
        Me.txtLIB400.TabIndex = 40
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(26, 52)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(77, 13)
        Me.Label6.TabIndex = 47
        Me.Label6.Text = "Base de Datos"
        '
        'txtDB400
        '
        Me.txtDB400.Location = New System.Drawing.Point(117, 52)
        Me.txtDB400.Name = "txtDB400"
        Me.txtDB400.Size = New System.Drawing.Size(147, 20)
        Me.txtDB400.TabIndex = 39
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(26, 133)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(53, 13)
        Me.Label5.TabIndex = 46
        Me.Label5.Text = "Password"
        '
        'txtPass400
        '
        Me.txtPass400.Location = New System.Drawing.Point(117, 130)
        Me.txtPass400.Name = "txtPass400"
        Me.txtPass400.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPass400.Size = New System.Drawing.Size(147, 20)
        Me.txtPass400.TabIndex = 42
        '
        'txtUser400
        '
        Me.txtUser400.Location = New System.Drawing.Point(117, 104)
        Me.txtUser400.Name = "txtUser400"
        Me.txtUser400.Size = New System.Drawing.Size(147, 20)
        Me.txtUser400.TabIndex = 41
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(26, 107)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(43, 13)
        Me.Label7.TabIndex = 45
        Me.Label7.Text = "Usuario"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(26, 26)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(81, 13)
        Me.Label8.TabIndex = 44
        Me.Label8.Text = "Servidor AS400"
        '
        'txtAS400
        '
        Me.txtAS400.Location = New System.Drawing.Point(117, 26)
        Me.txtAS400.Name = "txtAS400"
        Me.txtAS400.Size = New System.Drawing.Size(147, 20)
        Me.txtAS400.TabIndex = 38
        '
        'cmdAS400
        '
        Me.cmdAS400.Location = New System.Drawing.Point(534, 337)
        Me.cmdAS400.Name = "cmdAS400"
        Me.cmdAS400.Size = New System.Drawing.Size(90, 25)
        Me.cmdAS400.TabIndex = 7
        Me.cmdAS400.Text = "Aceptar"
        Me.cmdAS400.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label13.Location = New System.Drawing.Point(5, 9)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(137, 24)
        Me.Label13.TabIndex = 35
        Me.Label13.Text = "Instalador SIT"
        '
        'FormParametros
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(692, 462)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "FormParametros"
        Me.Text = "Parametros"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents cmdAS400 As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents ButtonFolderDescarga As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdPruebaSQL As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtPass As System.Windows.Forms.TextBox
    Friend WithEvents txtUser As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtIntialCatalog As System.Windows.Forms.TextBox
    Friend WithEvents txtDataSource As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdPruebaAS400 As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtLIB400 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtDB400 As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtPass400 As System.Windows.Forms.TextBox
    Friend WithEvents txtUser400 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtAS400 As System.Windows.Forms.TextBox
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtFolderDescarga As System.Windows.Forms.TextBox
    Friend WithEvents ButtonFolderDTS As System.Windows.Forms.Button
    Friend WithEvents ButtonScriptsSQL As System.Windows.Forms.Button
    Friend WithEvents ButtonInstallSIT As System.Windows.Forms.Button
    Friend WithEvents ButtonWebConfig As System.Windows.Forms.Button
    Friend WithEvents txtFolderDTS As System.Windows.Forms.TextBox

End Class
