<%@import namespace = "system.data.sqlclient" %>
<%@import namespace = "system.data" %>
<%@ Import Namespace= "System.Net" %>
<%@ Import Namespace= "mywebservice" %>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>citystatewebserviceclient</title>
		<script language="vb" runat="server">
			
			Sub Submit_Click(Src As Object, E As EventArgs)
				
				Dim d1 As mywebservice.mywebservice = New mywebservice.mywebservice()
				Dim MyData As DataSet 
				
				mydata = new dataset()
				
				mydata =  d1.GetCities(txtstate.text) 
				citylist.datasource = mydata.Tables("city").DefaultView

				citylist.databind()
				mydatagrid.datasource = mydata.Tables("city").DefaultView
				mydatagrid.databind()
				
			End Sub
		

		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<TABLE height="93" cellSpacing="0" cellPadding="0" width="324" align="center" bgColor="white" border="0" ms_1d_layout="TRUE">
	<TR>
	<TD width="235" height="31">State*:</TD>
	<TD width="202" height="31"><ASP:TEXTBOX id="txtstate" text="nari" Runat="server" Width="204px"></ASP:TEXTBOX></TD>
	<td width="19" height="31"><asp:requiredfieldvalidator id="Validatorstate" runat="server" Width="25px" Font-Bold="True" ControlToValidate="txtstate" Font-Size="8pt" Display="Dynamic">
						*  <br>
					</asp:requiredfieldvalidator></td>
				</TR>
				<tr>
					<TD width="235" height="31">City:</TD>
					<td><asp:dropdownlist id="citylist" AutoPostBack="True" DataValueField="city" runat="server" Width="336px"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td align="middle"><input id="Submit1" type="submit" value="Get Cities" name="Submit1" runat="server" OnServerClick="Submit_Click"></td>
				</tr>
				<tr>
					<td>
						<ASP:DataGrid id="MyDataGrid" runat="server" Width="370px" BackColor="#ccccff" BorderColor="black" ShowFooter="false" CellPadding="3" CellSpacing="0" Font-Name="Verdana" Font-Size="8pt" HeaderStyle-BackColor="#aaaadd" EnableViewState="false" />
					</td>
				</tr>
			</TABLE>
		</form>
	</body>
</HTML>
