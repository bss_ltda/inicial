﻿Partial Class Ribbon1
    Inherits Microsoft.Office.Tools.Ribbon.OfficeRibbon

    <System.Diagnostics.DebuggerNonUserCode()> _
   Public Sub New(ByVal container As System.ComponentModel.IContainer)
        MyClass.New()

        'Requerido para la compatibilidad con el Diseñador de composiciones de clases Windows.Forms
        If (container IsNot Nothing) Then
            container.Add(Me)
        End If

    End Sub

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New()
        MyBase.New()

        'El Diseñador de componentes requiere esta llamada.
        InitializeComponent()

    End Sub

    'Component invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de componentes
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de componentes requiere el siguiente procedimiento
    'Se puede modificar con el Diseñador de componentes.
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim RibbonDialogLauncher1 As Microsoft.Office.Tools.Ribbon.RibbonDialogLauncher = New Microsoft.Office.Tools.Ribbon.RibbonDialogLauncher()
        Me.Tab1 = New Microsoft.Office.Tools.Ribbon.RibbonTab()
        Me.GroupClientes = New Microsoft.Office.Tools.Ribbon.RibbonGroup()
        Me.ToggleButtonBaja = New Microsoft.Office.Tools.Ribbon.RibbonToggleButton()
        Me.Tab1.SuspendLayout()
        Me.GroupClientes.SuspendLayout()
        Me.SuspendLayout()
        '
        'Tab1
        '
        Me.Tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office
        Me.Tab1.Groups.Add(Me.GroupClientes)
        Me.Tab1.Label = "TabAddIns"
        Me.Tab1.Name = "Tab1"
        '
        'GroupClientes
        '
        Me.GroupClientes.DialogLauncher = RibbonDialogLauncher1
        Me.GroupClientes.Items.Add(Me.ToggleButtonBaja)
        Me.GroupClientes.Label = "Maestro de Clientes"
        Me.GroupClientes.Name = "GroupClientes"
        '
        'ToggleButtonBaja
        '
        Me.ToggleButtonBaja.Label = "Bajar de DataWhirl"
        Me.ToggleButtonBaja.Name = "ToggleButtonBaja"
        '
        'Ribbon1
        '
        Me.Name = "Ribbon1"
        Me.RibbonType = "Microsoft.Excel.Workbook"
        Me.Tabs.Add(Me.Tab1)
        Me.Tab1.ResumeLayout(False)
        Me.Tab1.PerformLayout()
        Me.GroupClientes.ResumeLayout(False)
        Me.GroupClientes.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Tab1 As Microsoft.Office.Tools.Ribbon.RibbonTab
    Friend WithEvents GroupClientes As Microsoft.Office.Tools.Ribbon.RibbonGroup
    Friend WithEvents ToggleButtonBaja As Microsoft.Office.Tools.Ribbon.RibbonToggleButton
End Class

Partial Class ThisRibbonCollection
    Inherits Microsoft.Office.Tools.Ribbon.RibbonReadOnlyCollection

    <System.Diagnostics.DebuggerNonUserCode()> _
    Friend ReadOnly Property Ribbon1() As Ribbon1
        Get
            Return Me.GetRibbon(Of Ribbon1)()
        End Get
    End Property
End Class
