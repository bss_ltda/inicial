﻿Public Class ExecuteSQL
    Public DB As New ADODB.Connection

    Public Function Open(ByVal gsConnString As String) As Boolean
        Try
            DB.Open(gsConnString)
            Return (DB.State = 1)
        Catch
            MsgBox(Err.Description)
            Return False
        End Try

    End Function

    Public Sub Close()
        DB.Close()
    End Sub

    Public Function ExecuteSQL(ByVal sql As String) As Integer

        Dim r As Integer = 0

        Try
            DB.Execute(sql, r)
        Catch
            MsgBox(Err.Description)
            Debug.Print(sql)
            Return -99
        End Try

        Return r

    End Function

    Public Function Cursor(ByRef rs As ADODB.Recordset, ByVal sql As String) As Boolean

        Dim Result As Boolean
        Try
            rs.Open(sql, DB)
        Catch
            MsgBox(Err.Description)
            Debug.Print(sql)
            Return False
        End Try
        Result = (Not rs.EOF)
        Return Result

    End Function

    Public Function CalcConsec(ByRef sID As String, ByRef TopeMax As String) As String
        Dim rs As New ADODB.Recordset
        Dim locID As String
        Dim sql As String
        Dim sConsec As String = ""
        Dim tMax As Double
        Dim sFmt As String
        Dim nDig As Integer


        nDig = Len(TopeMax)
        sFmt = New String("0", nDig)

        sql = "SELECT CCDESC FROM ZCCL01 WHERE CCTABL = 'SECUENCE' AND CCCODE='" & sID & "'"
        tMax = Val(TopeMax)

        With rs
            .CursorLocation = ADODB.CursorLocationEnum.adUseServer
            .Open(sql, DB, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)
            If Not (.BOF And .EOF) Then
                locID = Val(.Fields("CCDESC").Value) + 1
                If locID > tMax Then
                    locID = 1
                End If
                sConsec = Right(sFmt & locID, nDig)
                .Fields("CCDESC").Value = sConsec
                .Update()
                .Close()
            Else
                locID = 1
                .Close()
                sConsec = Right(sFmt & locID, nDig)
                DB.Execute(" INSERT INTO ZCC (CCID, CCTABL, CCCODE, CCDESC ) " & " VALUES( 'CC', 'SECUENCE', '" & sID & "', '" & sConsec & "' )")
            End If
        End With

        Return sConsec

    End Function

    Function mkConnectionString(ByVal srv As String, ByVal usr As String, ByVal pass As String, ByVal qLib As String) As String
        Dim g As String

        g = "Provider=IBMDA400.DataSource.1;"
        g &= "Password=" & pass & ";"
        g &= "User ID=" & usr & ";"
        g &= "Data Source=" & srv & ";"
        g &= "Persist Security Info=False;"
        g &= "Default Collection=" & qLib

        Return g

    End Function



End Class
