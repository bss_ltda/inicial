﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Security
Imports Microsoft.Office.Tools.Excel

' La información general sobre un ensamblado se controla mediante el siguiente 
' conjunto de atributos. Cambie estos atributos para modificar la información
' asociada con un ensamblado.

' Revisar los valores de los atributos del ensamblado

<Assembly: AssemblyTitle("dwListaPrecioPorCanal")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("")> 
<Assembly: AssemblyProduct("dwListaPrecioPorCanal")> 
<Assembly: AssemblyCopyright("Copyright ©  2012")> 
<Assembly: AssemblyTrademark("")> 

' Si establece ComVisible como false, los tipos de este ensamblado no estarán visibles 
' para los componentes COM. Si necesita obtener acceso a un tipo de este ensamblado desde 
' COM, establezca el atributo ComVisible como true en este tipo.
<Assembly: ComVisible(False)>

'El siguiente GUID sirve como identificador de typelib si este proyecto se expone a COM
<Assembly: Guid("dc51a535-eabd-431b-b980-3ec4efb9f3ca")> 

' La información de versión de un ensamblado consta de los cuatro valores siguientes:
'
'      Versión principal
'      Versión secundaria 
'      Número de compilación
'      Revisión
'
' Puede especificar todos los valores o usar los valores predeterminados de número de compilación y de revisión 
' mediante el asterisco ('*'), como se muestra a continuación:
' <Ensamblado: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 

' El atributo ExcelLocale1033 controla la configuración regional que se pasa al modelo de objetos de Excel. Si establece
' ExcelLocale1033 en true, dicho modelo se comportará de la misma manera en todas las configuraciones 
' regionales, lo que coincide con el comportamiento de Visual Basic para 
' Aplicaciones. Si establece ExcelLocale1033 en false, el modelo de objetos de Excel se comportará
' de forma diferente cuando los usuarios tengan configuraciones regionales distintas, lo que coincide con el comportamiento de 
' Visual Studio Tools para Office, versión 2003. Esta circunstancia puede causar resultados 
' inesperados en información que depende de la configuración regional, como por ejemplo nombres de fórmula y formatos de fecha.

<Assembly: ExcelLocale1033(True)>
