﻿Partial Class Ribbon1
    Inherits Microsoft.Office.Tools.Ribbon.OfficeRibbon

    <System.Diagnostics.DebuggerNonUserCode()> _
   Public Sub New(ByVal container As System.ComponentModel.IContainer)
        MyClass.New()

        'Requerido para la compatibilidad con el Diseñador de composiciones de clases Windows.Forms
        If (container IsNot Nothing) Then
            container.Add(Me)
        End If

    End Sub

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New()
        MyBase.New()

        'El Diseñador de componentes requiere esta llamada.
        InitializeComponent()

    End Sub

    'Component invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de componentes
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de componentes requiere el siguiente procedimiento
    'Se puede modificar con el Diseñador de componentes.
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Tab1 = New Microsoft.Office.Tools.Ribbon.RibbonTab()
        Me.GroupPrec = New Microsoft.Office.Tools.Ribbon.RibbonGroup()
        Me.DropDownCanal = New Microsoft.Office.Tools.Ribbon.RibbonDropDown()
        Me.ButtonSubirPrecios = New Microsoft.Office.Tools.Ribbon.RibbonButton()
        Me.ButtonGenenraCanal = New Microsoft.Office.Tools.Ribbon.RibbonButton()
        Me.CargaCanales = New Microsoft.Office.Tools.Ribbon.RibbonButton()
        Me.Tab1.SuspendLayout()
        Me.GroupPrec.SuspendLayout()
        Me.SuspendLayout()
        '
        'Tab1
        '
        Me.Tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office
        Me.Tab1.Groups.Add(Me.GroupPrec)
        Me.Tab1.Label = "TabAddIns"
        Me.Tab1.Name = "Tab1"
        '
        'GroupPrec
        '
        Me.GroupPrec.Items.Add(Me.DropDownCanal)
        Me.GroupPrec.Items.Add(Me.ButtonGenenraCanal)
        Me.GroupPrec.Items.Add(Me.ButtonSubirPrecios)
        Me.GroupPrec.Items.Add(Me.CargaCanales)
        Me.GroupPrec.Label = "Precios por Canal"
        Me.GroupPrec.Name = "GroupPrec"
        '
        'DropDownCanal
        '
        Me.DropDownCanal.Label = "Canal"
        Me.DropDownCanal.Name = "DropDownCanal"
        '
        'ButtonSubirPrecios
        '
        Me.ButtonSubirPrecios.Label = "Subir Precios"
        Me.ButtonSubirPrecios.Name = "ButtonSubirPrecios"
        '
        'ButtonGenenraCanal
        '
        Me.ButtonGenenraCanal.Label = "Bajar Base"
        Me.ButtonGenenraCanal.Name = "ButtonGenenraCanal"
        '
        'CargaCanales
        '
        Me.CargaCanales.Label = "Cargar"
        Me.CargaCanales.Name = "CargaCanales"
        '
        'Ribbon1
        '
        Me.Name = "Ribbon1"
        Me.RibbonType = "Microsoft.Excel.Workbook"
        Me.Tabs.Add(Me.Tab1)
        Me.Tab1.ResumeLayout(False)
        Me.Tab1.PerformLayout()
        Me.GroupPrec.ResumeLayout(False)
        Me.GroupPrec.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Tab1 As Microsoft.Office.Tools.Ribbon.RibbonTab
    Friend WithEvents GroupPrec As Microsoft.Office.Tools.Ribbon.RibbonGroup
    Friend WithEvents DropDownCanal As Microsoft.Office.Tools.Ribbon.RibbonDropDown
    Friend WithEvents ButtonGenenraCanal As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents ButtonSubirPrecios As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents CargaCanales As Microsoft.Office.Tools.Ribbon.RibbonButton
End Class

Partial Class ThisRibbonCollection
    Inherits Microsoft.Office.Tools.Ribbon.RibbonReadOnlyCollection

    <System.Diagnostics.DebuggerNonUserCode()> _
    Friend ReadOnly Property Ribbon1() As Ribbon1
        Get
            Return Me.GetRibbon(Of Ribbon1)()
        End Get
    End Property
End Class
