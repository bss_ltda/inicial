﻿Imports Microsoft.Office.Tools.Ribbon
Imports Npgsql

Public Class Ribbon1

    Private Sub Ribbon1_Load(ByVal sender As System.Object, ByVal e As RibbonUIEventArgs) Handles MyBase.Load

    End Sub

    Private Sub CargaCanales_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles CargaCanales.Click
        If Me.DropDownCanal.Items.Count > 0 Then
            Exit Sub
        End If

        If Not CheckVer("LISTPREC") Then
            Exit Sub
        End If
        Dim gsConnString As String
        Dim fSql As String

        '---------------------------------------------------------------------------------
        '               CANALES
        '---------------------------------------------------------------------------------
        gsConnString = "Server=" & My.Settings.Server & ";Port=5432;Database=Whirl01;User Id=postgres;Password=wh533799;"
        Dim DB As NpgsqlConnection = New NpgsqlConnection(gsConnString)
        'DB.ConnectionString = gsConnString
        DB.Open()

        fSql = "SELECT distinct cod_reg as cod_canal, regional as canal FROM clientes_dw WHERE regional <> '' ORDER BY 1 "
        Dim command As New NpgsqlCommand(fSql, DB)
        Dim dr As Npgsql.NpgsqlDataReader
        dr = command.ExecuteReader()

        While (dr.Read())
            Dim a As Microsoft.Office.Tools.Ribbon.RibbonDropDownItem = New Microsoft.Office.Tools.Ribbon.RibbonDropDownItem()
            a.Label = dr.Item("canal")
            a.Tag = dr.Item("cod_canal").ToString
            Me.DropDownCanal.Items.Add(a)
        End While
        dr.Close()
        DB.Close()
    End Sub

    Public Function CheckVer(Aplicacion As String) As Boolean
        Dim gsConnString As String
        Dim fSql As String
        Dim C_APP_VER As String = My.Application.Info.Version.Major & "." & _
                                  My.Application.Info.Version.Minor & "." & _
                                  My.Application.Info.Version.Revision


        gsConnString = "Server=" & My.Settings.Server & ";Port=5432;Database=Whirl01;User Id=postgres;Password=wh533799;"
        Dim DB As NpgsqlConnection = New NpgsqlConnection(gsConnString)
        DB.Open()

        CheckVer = True
        fSql = "SELECT valor FROM parametros WHERE tabla='VERSION' AND codigo='" & Aplicacion & "'"
        Dim command As New NpgsqlCommand(fSql, DB)
        Dim dr As Npgsql.NpgsqlDataReader
        dr = command.ExecuteReader()

        If (dr.Read()) Then
            If Trim(dr.Item("VALOR")) < C_APP_VER Then
                Dim upd As New NpgsqlCommand("UPDATE parametros SET valor='" & C_APP_VER & "' WHERE tabla='VERSION' AND codigo='" & Aplicacion & "'", DB)
            ElseIf Trim(dr.Item("VALOR")) > C_APP_VER Then
                MsgBox("Versión incorrecta del programa." & vbCr & vbCr & _
                       "Versión Registrada: (" & vbTab & Trim(dr.Item("VALOR")) & ")" & vbCr & vbCr & _
                       "Versión Actual:" & vbTab & "(" & C_APP_VER & ")" & vbCr & vbCr & _
                       "Comuníquese con Tecnologia.")
                CheckVer = False
            End If
        Else
            Dim upd As New NpgsqlCommand("INSERT INTO parametros  (tabla, codigo, valor ) VALUES( " & _
                       "'VERSION', '" & Aplicacion & "', '" & C_APP_VER & "' ) ", DB)
            upd.ExecuteNonQuery()
        End If
        dr.Close()
        DB.Close()

    End Function

End Class
