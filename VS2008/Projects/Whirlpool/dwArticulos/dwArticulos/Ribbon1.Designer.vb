﻿Partial Class Ribbon1
    Inherits Microsoft.Office.Tools.Ribbon.RibbonBase

    <System.Diagnostics.DebuggerNonUserCode()> _
   Public Sub New(ByVal container As System.ComponentModel.IContainer)
        MyClass.New()

        'Requerido para la compatibilidad con el Diseñador de composiciones de clases Windows.Forms
        If (container IsNot Nothing) Then
            container.Add(Me)
        End If

    End Sub

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New()
        MyBase.New(Globals.Factory.GetRibbonFactory())

        'El Diseñador de componentes requiere esta llamada.
        InitializeComponent()

    End Sub

    'Component invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de componentes
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de componentes requiere el siguiente procedimiento
    'Se puede modificar usando el Diseñador de componentes.
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Tab1 = Me.Factory.CreateRibbonTab
        Me.GroupArticulos = Me.Factory.CreateRibbonGroup
        Me.ButtonSubeDesc = Me.Factory.CreateRibbonButton
        Me.ButtonBajaDescont = Me.Factory.CreateRibbonButton
        Me.DropDownPer = Me.Factory.CreateRibbonDropDown
        Me.Tab1.SuspendLayout()
        Me.GroupArticulos.SuspendLayout()
        '
        'Tab1
        '
        Me.Tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office
        Me.Tab1.Groups.Add(Me.GroupArticulos)
        Me.Tab1.Label = "TabAddIns"
        Me.Tab1.Name = "Tab1"
        '
        'GroupArticulos
        '
        Me.GroupArticulos.Items.Add(Me.ButtonSubeDesc)
        Me.GroupArticulos.Items.Add(Me.ButtonBajaDescont)
        Me.GroupArticulos.Items.Add(Me.DropDownPer)
        Me.GroupArticulos.Label = "Articulos DW"
        Me.GroupArticulos.Name = "GroupArticulos"
        '
        'ButtonSubeDesc
        '
        Me.ButtonSubeDesc.Label = "Actualiza Descontinuados"
        Me.ButtonSubeDesc.Name = "ButtonSubeDesc"
        '
        'ButtonBajaDescont
        '
        Me.ButtonBajaDescont.Label = "Consulta Descontinuados"
        Me.ButtonBajaDescont.Name = "ButtonBajaDescont"
        '
        'DropDownPer
        '
        Me.DropDownPer.Label = "Periodo"
        Me.DropDownPer.Name = "DropDownPer"
        '
        'Ribbon1
        '
        Me.Name = "Ribbon1"
        Me.RibbonType = "Microsoft.Excel.Workbook"
        Me.Tabs.Add(Me.Tab1)
        Me.Tab1.ResumeLayout(False)
        Me.Tab1.PerformLayout()
        Me.GroupArticulos.ResumeLayout(False)
        Me.GroupArticulos.PerformLayout()

    End Sub

    Friend WithEvents Tab1 As Microsoft.Office.Tools.Ribbon.RibbonTab
    Friend WithEvents GroupArticulos As Microsoft.Office.Tools.Ribbon.RibbonGroup
    Friend WithEvents ButtonSubeDesc As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents ButtonBajaDescont As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents DropDownPer As Microsoft.Office.Tools.Ribbon.RibbonDropDown
End Class

Partial Class ThisRibbonCollection

    <System.Diagnostics.DebuggerNonUserCode()> _
    Friend ReadOnly Property Ribbon1() As Ribbon1
        Get
            Return Me.GetRibbon(Of Ribbon1)()
        End Get
    End Property
End Class
