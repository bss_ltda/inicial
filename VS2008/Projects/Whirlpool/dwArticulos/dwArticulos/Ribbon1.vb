﻿Imports Microsoft.Office.Tools.Ribbon
Imports Npgsql

Public Class Ribbon1

    Private Sub Ribbon1_Load(ByVal sender As System.Object, ByVal e As RibbonUIEventArgs) Handles MyBase.Load
        Dim i As Integer

        For i = 1 To 12
            Dim per As Microsoft.Office.Tools.Ribbon.RibbonDropDownItem = Me.Factory.CreateRibbonDropDownItem
            per.Label = Year(Now()).ToString & "-" & Right("0" & i.ToString, 2)
            Me.DropDownPer.Items.Add(per)
        Next

        Me.DropDownPer.SelectedItemIndex = Month(Now) - 1

    End Sub

    Private Sub ButtonSubeDesc_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles ButtonSubeDesc.Click
        Dim gsConnString As String
        Dim fSql, vSql As String
        Dim qRow As Integer
        Dim qPeriodo As String = Me.DropDownPer.Items(Me.DropDownPer.SelectedItemIndex).Label
        Dim msg As String = vbCr & vbCr & "Periodo: " & qPeriodo & vbCr

        If MsgBox("Actualiza DESCONTINUADOS en DataWhirlpool?" & msg, MsgBoxStyle.Information + MsgBoxStyle.YesNo, "ARTICULOS") = MsgBoxResult.No Then
            Exit Sub
        End If

        If Not CheckVer("LISTPREC") Then
            Exit Sub
        End If

        '---------------------------------------------------------------------------------
        '               
        '---------------------------------------------------------------------------------
        gsConnString = "Server=" & My.Settings.Server & ";Port=5432;Database=Whirl01;User Id=postgres;Password=wh533799;"
        Dim DB As NpgsqlConnection = New NpgsqlConnection(gsConnString)


        Dim x As System.Array
        Dim ws As Excel.Worksheet = CType(Globals.ThisWorkbook.ActiveSheet, Excel.Worksheet)
        x = ws.Range(ws.Cells(1, 1), ws.Cells(1000, 6)).Value
        Dim bErr As Boolean = False
        Try
            bErr = bErr Or (x(1, 1).ToString <> "sku")
        Catch ex As Exception
            MsgBox("La hoja no corresponde a Articulos Descontinuados ", MsgBoxStyle.Exclamation, "ARTICULOS")
            Exit Sub
        End Try
        If bErr Then
            MsgBox("La hoja no corresponde a Articulos Descontinuados ", MsgBoxStyle.Exclamation, "ARTICULOS")
            Exit Sub
        End If

        DB.Open()


        fSql = "delete from art_desconti "
        fSql &= " WHERE ano=" & Left(qPeriodo, 4) & " AND mes=" & Right(qPeriodo, 2)
        Dim upd As New NpgsqlCommand(fSql, DB)
        upd.ExecuteNonQuery()

        For qRow = 2 To 2000
            If x(qRow, 1) = Nothing Then
                Exit For
            End If
            If Trim(x(qRow, 1).ToString) = "" Then
                Exit For
            End If

            fSql = " INSERT INTO art_desconti( "
            vSql = " VALUES("
            fSql &= " ano, " : vSql &= " " & CInt(Left(qPeriodo, 4)) & " , "
            fSql &= " mes,   " : vSql &= " " & CInt(Right(qPeriodo, 2)) & ", "
            fSql &= " cod_art ) " : vSql &= "'" & x(qRow, 1).ToString & "' )"
            Try
                upd.CommandText = fSql & vSql
                upd.ExecuteNonQuery()
            Catch ex As Exception
                MsgBox("Fila " & qRow.ToString & vbCr & ex.Message, MsgBoxStyle.Critical, "ARTICULOS")
                DB.Close()
                Exit Sub
            End Try
        Next
        DB.Close()
        MsgBox("Articulos DESCONTINUADOS Actualizados." & msg & vbCr & (qRow - 2).ToString & " Registros.", MsgBoxStyle.Information, "ARTICULOS")
    End Sub

    Public Function CheckVer(Aplicacion As String) As Boolean
        Dim gsConnString As String
        Dim fSql As String
        Dim C_APP_VER As String = My.Application.Info.Version.Major & "." & _
                                  My.Application.Info.Version.Minor & "." & _
                                  My.Application.Info.Version.Revision


        gsConnString = "Server=" & My.Settings.Server & ";Port=5432;Database=Whirl01;User Id=postgres;Password=wh533799;"
        Dim DB As NpgsqlConnection = New NpgsqlConnection(gsConnString)
        Try

            DB.Open()

            CheckVer = True
            fSql = "SELECT valor FROM parametros WHERE tabla='VERSION' AND codigo='" & Aplicacion & "'"
            Dim command As New NpgsqlCommand(fSql, DB)
            Dim dr As Npgsql.NpgsqlDataReader
            dr = command.ExecuteReader()

            If (dr.Read()) Then
                If Trim(dr.Item("VALOR")) < C_APP_VER Then
                    Dim upd As New NpgsqlCommand("UPDATE parametros SET valor='" & C_APP_VER & "' WHERE tabla='VERSION' AND codigo='" & Aplicacion & "'", DB)
                ElseIf Trim(dr.Item("VALOR")) > C_APP_VER Then
                    MsgBox("Versión incorrecta del programa." & vbCr & vbCr & _
                           "Versión Registrada: (" & vbTab & Trim(dr.Item("VALOR")) & ")" & vbCr & vbCr & _
                           "Versión Actual:" & vbTab & "(" & C_APP_VER & ")" & vbCr & vbCr & _
                           "Comuníquese con Tecnologia.")
                    CheckVer = False
                End If
            Else
                Dim upd As New NpgsqlCommand("INSERT INTO parametros  (tabla, codigo, valor ) VALUES( " & _
                           "'VERSION', '" & Aplicacion & "', '" & C_APP_VER & "' ) ", DB)
                upd.ExecuteNonQuery()
            End If
            dr.Close()
            DB.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
            Return False
        End Try

    End Function

    Private Sub ButtonBajaDescont_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles ButtonBajaDescont.Click
        Dim gsConnString As String
        Dim fSql As String

        '---------------------------------------------------------------------------------
        '               
        '---------------------------------------------------------------------------------

        If Not CheckVer("LISTPREC") Then
            Exit Sub
        End If

        Try
            Dim qPeriodo As String = Me.DropDownPer.Items(Me.DropDownPer.SelectedItemIndex).Label

            gsConnString = "Server=" & My.Settings.Server & ";Port=5432;Database=Whirl01;User Id=postgres;Password=wh533799;"
            Dim DB As NpgsqlConnection = New NpgsqlConnection(gsConnString)
            'DB.ConnectionString = gsConnString
            DB.Open()

            fSql = " SELECT d.cod_art as sku, a.articulo "
            fSql &= " FROM  art_desconti d INNER JOIN articulos a on d.cod_art = a.cod_art "
            fSql &= " WHERE ano=" & Left(qPeriodo, 4) & " AND mes=" & Right(qPeriodo, 2)

            Dim command As New NpgsqlCommand(fSql, DB)
            Dim dr As Npgsql.NpgsqlDataReader
            dr = command.ExecuteReader()
            Dim i As Integer = 2
            Dim j As Integer = 0
            With Globals.ThisWorkbook.Sheets.Add()
                For j = 0 To dr.FieldCount() - 1
                    .Cells(1, j + 1) = dr.GetName(j).ToString()
                Next
                While dr.Read()
                    For j = 0 To dr.FieldCount() - 1
                        .Cells(i, j + 1) = dr.Item(j)
                    Next
                    i += 1
                End While
                'If i > 2 Then
                '    .ListObjects.Add(1, .Range(.Cells(1, 1), .Cells(i, j)), 1).Name = "tblDescont"
                '    .ListObjects("tblDescont").TableStyle = "TableStyleMedium2"
                '    .Cells.EntireColumn.AutoFit()
                'End If
            End With
            dr.Close()
            DB.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        End Try

    End Sub
End Class
