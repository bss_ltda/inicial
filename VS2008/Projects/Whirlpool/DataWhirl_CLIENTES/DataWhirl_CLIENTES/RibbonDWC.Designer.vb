﻿Partial Class RibbonDWC
    Inherits Microsoft.Office.Tools.Ribbon.OfficeRibbon

    <System.Diagnostics.DebuggerNonUserCode()> _
   Public Sub New(ByVal container As System.ComponentModel.IContainer)
        MyClass.New()

        'Requerido para la compatibilidad con el Diseñador de composiciones de clases Windows.Forms
        If (container IsNot Nothing) Then
            container.Add(Me)
        End If

    End Sub

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New()
        MyBase.New()

        'El Diseñador de componentes requiere esta llamada.
        InitializeComponent()

    End Sub

    'Component invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de componentes
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de componentes requiere el siguiente procedimiento
    'Se puede modificar con el Diseñador de componentes.
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Tab1 = New Microsoft.Office.Tools.Ribbon.RibbonTab()
        Me.GroupDWC = New Microsoft.Office.Tools.Ribbon.RibbonGroup()
        Me.ToggleBajaDWC = New Microsoft.Office.Tools.Ribbon.RibbonToggleButton()
        Me.Tab1.SuspendLayout()
        Me.GroupDWC.SuspendLayout()
        Me.SuspendLayout()
        '
        'Tab1
        '
        Me.Tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office
        Me.Tab1.Groups.Add(Me.GroupDWC)
        Me.Tab1.Label = "TabAddIns"
        Me.Tab1.Name = "Tab1"
        '
        'GroupDWC
        '
        Me.GroupDWC.Items.Add(Me.ToggleBajaDWC)
        Me.GroupDWC.Label = "Maestro de Clientes"
        Me.GroupDWC.Name = "GroupDWC"
        '
        'ToggleBajaDWC
        '
        Me.ToggleBajaDWC.Label = "Bajar de DataWhirl"
        Me.ToggleBajaDWC.Name = "ToggleBajaDWC"
        '
        'RibbonDWC
        '
        Me.Name = "RibbonDWC"
        Me.RibbonType = "Microsoft.Excel.Workbook"
        Me.Tabs.Add(Me.Tab1)
        Me.Tab1.ResumeLayout(False)
        Me.Tab1.PerformLayout()
        Me.GroupDWC.ResumeLayout(False)
        Me.GroupDWC.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Tab1 As Microsoft.Office.Tools.Ribbon.RibbonTab
    Friend WithEvents GroupDWC As Microsoft.Office.Tools.Ribbon.RibbonGroup
    Friend WithEvents ToggleBajaDWC As Microsoft.Office.Tools.Ribbon.RibbonToggleButton
End Class

Partial Class ThisRibbonCollection
    Inherits Microsoft.Office.Tools.Ribbon.RibbonReadOnlyCollection

    <System.Diagnostics.DebuggerNonUserCode()> _
    Friend ReadOnly Property RibbonDWC() As RibbonDWC
        Get
            Return Me.GetRibbon(Of RibbonDWC)()
        End Get
    End Property
End Class
