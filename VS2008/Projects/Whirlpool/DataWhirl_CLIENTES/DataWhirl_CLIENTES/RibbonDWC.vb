﻿Imports Microsoft.Office.Tools.Ribbon
Imports Npgsql

Public Class RibbonDWC

    Private Sub RibbonDWC_Load(ByVal sender As System.Object, ByVal e As RibbonUIEventArgs) Handles MyBase.Load

    End Sub

    Private Sub ToggleBajaDWC_Click(sender As System.Object, e As Microsoft.Office.Tools.Ribbon.RibbonControlEventArgs) Handles ToggleBajaDWC.Click
        Dim gsConnString As String
        Dim fSql As String

        '---------------------------------------------------------------------------------
        '               TERRITORIO
        '---------------------------------------------------------------------------------
        gsConnString = "Server=localhost;Port=5432;Database=Whirl01;User Id=postgres;Password=wh533799;"
        Dim DB As NpgsqlConnection = New NpgsqlConnection(gsConnString)
        'DB.ConnectionString = gsConnString
        DB.Open()

        fSql = "SELECT cod_ger as codigo, gerencia  as territorio FROM gerencias ORDER BY 1"
        Dim command As New NpgsqlCommand(fSql, DB)
        Dim dr As Npgsql.NpgsqlDataReader
        dr = command.ExecuteReader()
        Globals.Hoja5.Cells.Clear()


        Dim i As Integer = 2
        With Globals.Hoja5
            .Cells(1, 1) = "Codigo"
            .Cells(1, 2) = "Territorio"
            While dr.Read()
                .Cells(i, 1) = dr.Item("codigo")
                .Cells(i, 2) = dr.Item("territorio")
                i += 1
            End While
            .ListObjects.Add(1, .Range(.Cells(1, 1), .Cells(i, 2)), 1).Name = "tblTerritorio"
            .ListObjects("tblTerritorio").TableStyle = "TableStyleMedium2"
        End With
        dr.Close()

        '---------------------------------------------------------------------------------
        '               CANAL
        '---------------------------------------------------------------------------------
        command.CommandText = "SELECT cod_reg as codigo,  regional as canal,  cod_ger as territorio  FROM regionales ORDER BY 1"
        dr = command.ExecuteReader()
        Globals.Hoja4.Cells.Clear()
        i = 2
        With Globals.Hoja4
            .Cells(1, 1) = "Codigo"
            .Cells(1, 2) = "Canal"
            .Cells(1, 3) = "Territorio"
            .Cells(1, 4) = "Descripcion"
            While dr.Read()
                .Cells(i, 1) = dr.Item("codigo")
                .Cells(i, 2) = dr.Item("canal")
                .Cells(i, 3) = dr.Item("territorio")
                i += 1
            End While
            .ListObjects.Add(1, .Range(.Cells(1, 1), .Cells(i, 4)), 1).Name = "tblCanal"
            .ListObjects("tblCanal").TableStyle = "TableStyleMedium2"
            .Cells(2, 4).FormulaR1C1 = "=VLOOKUP([@territorio],tblTerritorio,2,FALSE)"
        End With
        dr.Close()

        '---------------------------------------------------------------------------------
        '               KAM
        '---------------------------------------------------------------------------------
        command.CommandText = "SELECT cod_rep as codigo, representante as KAM, cod_reg as Canal FROM representantes ORDER BY 1"
        dr = command.ExecuteReader()
        Globals.Hoja3.Cells.Clear()
        i = 2
        With Globals.Hoja3
            .Cells(1, 1) = "Codigo"
            .Cells(1, 2) = "KAM"
            .Cells(1, 3) = "Canal"
            .Cells(1, 4) = "Descripcion"
            .Cells(1, 5) = "Territorio"
            While dr.Read()
                .Cells(i, 1) = dr.Item("codigo")
                .Cells(i, 2) = dr.Item("KAM")
                .Cells(i, 3) = dr.Item("canal")
                i += 1
            End While
            .ListObjects.Add(1, .Range(.Cells(1, 1), .Cells(i, 5)), 1).Name = "tblKAM"
            .ListObjects("tblKAM").TableStyle = "TableStyleMedium2"
            .Cells(2, 4).FormulaR1C1 = "=VLOOKUP([@canal],tblCanal,2,FALSE)"
            .Cells(2, 5).FormulaR1C1 = "=VLOOKUP([@canal],tblCanal,4,FALSE)"
        End With
        dr.Close()

        '---------------------------------------------------------------------------------
        '               KAMJR
        '---------------------------------------------------------------------------------
        command.CommandText = "SELECT cod_ks2 as codigo, ka2_nombre as KAMJR, cod_reg as Canal FROM representantes ORDER BY 1"
        dr = command.ExecuteReader()
        Globals.Hoja3.Cells.Clear()
        i = 2
        With Globals.Hoja3
            .Cells(1, 1) = "Codigo"
            .Cells(1, 2) = "KAMJR"
            .Cells(1, 3) = "Canal"
            .Cells(1, 4) = "Descripcion"
            .Cells(1, 5) = "Territorio"
            While dr.Read()
                .Cells(i, 1) = dr.Item("codigo")
                .Cells(i, 2) = dr.Item("KAM")
                .Cells(i, 3) = dr.Item("canal")
                i += 1
            End While
            .ListObjects.Add(1, .Range(.Cells(1, 1), .Cells(i, 5)), 1).Name = "tblKAM"
            .ListObjects("tblKAM").TableStyle = "TableStyleMedium2"
            .Cells(2, 4).FormulaR1C1 = "=VLOOKUP([@canal],tblCanal,2,FALSE)"
            .Cells(2, 5).FormulaR1C1 = "=VLOOKUP([@canal],tblCanal,4,FALSE)"
        End With
        dr.Close()

        '---------------------------------------------------------------------------------
        '               GRUPOS
        '---------------------------------------------------------------------------------
        command.CommandText = "SELECT cod_grupo as codigo, grupo, cod_rep as KAM FROM grupos ORDER BY 1"
        dr = command.ExecuteReader()
        Globals.Hoja2.Cells.Clear()
        i = 2
        With Globals.Hoja2
            .Cells(1, 1) = "Codigo"
            .Cells(1, 2) = "Grupo"
            .Cells(1, 3) = "KAM"
            .Cells(1, 4) = "Descripcion"
            .Cells(1, 5) = "Canal"
            .Cells(1, 6) = "Territorio"
            While dr.Read()
                .Cells(i, 1) = dr.Item("codigo")
                .Cells(i, 2) = dr.Item("grupo")
                .Cells(i, 3) = dr.Item("KAM")
                i += 1
            End While
            .ListObjects.Add(1, .Range(.Cells(1, 1), .Cells(i, 6)), 1).Name = "tblGrupos"
            .ListObjects("tblGrupos").TableStyle = "TableStyleMedium2"
            .Cells(2, 4).FormulaR1C1 = "=VLOOKUP([@kam],tblKAM,2,FALSE)"
            .Cells(2, 5).FormulaR1C1 = "=VLOOKUP([@kam],tblKAM,4,FALSE)"
            .Cells(2, 6).FormulaR1C1 = "=VLOOKUP([@kam],tblKAM,5,FALSE)"
        End With
        dr.Close()


        '---------------------------------------------------------------------------------
        '               CLIENTES
        '---------------------------------------------------------------------------------
        fSql = " SELECT clientes.cod_cli as codigo,  "
        fSql = fSql & " clientes.cod_cli_map as equivalencia,  "
        fSql = fSql & " clientes.cliente, clientes.cod_grupo as grupo, negocio "
        fSql = fSql & " FROM public.clientes clientes "
        fSql = fSql & " ORDER BY 1"

        command.CommandText = fSql.ToString()
        dr = command.ExecuteReader()
        Globals.Hoja1.Cells.Clear()

        i = 2
        With Globals.Hoja1
            .Cells(1, 1) = "Codigo"
            .Cells(1, 2) = "Equivalencia"
            .Cells(1, 3) = "Clientes"
            .Cells(1, 4) = "Grupo"
            .Cells(1, 5) = "Negocio"
            .Cells(1, 6) = "Descripcion"
            .Cells(1, 7) = "KAM"
            .Cells(1, 8) = "Canal"
            .Cells(1, 9) = "Territorio"
            While dr.Read()
                '.Cells(i, 1).FormulaR1C1 = "=T(""" & dr.Item("codigo") & """)"
                '.Cells(i, 2).FormulaR1C1 = "=T(""" & dr.Item("equivalencia") & """)"
                .Cells(i, 1) = "'" & dr.Item("codigo")
                .Cells(i, 2) = "'" & dr.Item("equivalencia")
                .Cells(i, 3) = dr.Item("cliente")
                .Cells(i, 4) = dr.Item("grupo")
                .Cells(i, 5) = dr.Item("negocio")
                i += 1
            End While
            .ListObjects.Add(1, .Range(.Cells(1, 1), .Cells(i, 9)), 1).Name = "tblClientes"
            .ListObjects("tblClientes").TableStyle = "TableStyleMedium2"
            .Cells(2, 6).FormulaR1C1 = "=VLOOKUP([@grupo],tblGrupos,2,FALSE)"
            .Cells(2, 7).FormulaR1C1 = "=VLOOKUP([@grupo],tblGrupos,4,FALSE)"
            .Cells(2, 8).FormulaR1C1 = "=VLOOKUP([@grupo],tblGrupos,5,FALSE)"
            .Cells(2, 9).FormulaR1C1 = "=VLOOKUP([@grupo],tblGrupos,6,FALSE)"
        End With
        dr.Close()


        DB.Close()
    End Sub
End Class
