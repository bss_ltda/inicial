﻿Partial Class Ribbon1
    Inherits Microsoft.Office.Tools.Ribbon.RibbonBase

    <System.Diagnostics.DebuggerNonUserCode()> _
   Public Sub New(ByVal container As System.ComponentModel.IContainer)
        MyClass.New()

        'Requerido para la compatibilidad con el Diseñador de composiciones de clases Windows.Forms
        If (container IsNot Nothing) Then
            container.Add(Me)
        End If

    End Sub

    <System.Diagnostics.DebuggerNonUserCode()> _
    Public Sub New()
        MyBase.New(Globals.Factory.GetRibbonFactory())

        'El Diseñador de componentes requiere esta llamada.
        InitializeComponent()

    End Sub

    'Component invalida a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de componentes
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de componentes requiere el siguiente procedimiento
    'Se puede modificar usando el Diseñador de componentes.
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Tab1 = Me.Factory.CreateRibbonTab
        Me.LIstasPrecios = Me.Factory.CreateRibbonGroup
        Me.DropDownCanal = Me.Factory.CreateRibbonDropDown
        Me.ButtonGenenraCanal = Me.Factory.CreateRibbonButton
        Me.ButtonSubirPrecios = Me.Factory.CreateRibbonButton
        Me.ButtonCargaCanales = Me.Factory.CreateRibbonButton
        Me.DropDownPer = Me.Factory.CreateRibbonDropDown
        Me.Group1 = Me.Factory.CreateRibbonGroup
        Me.Tab1.SuspendLayout()
        Me.LIstasPrecios.SuspendLayout()
        Me.Group1.SuspendLayout()
        '
        'Tab1
        '
        Me.Tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office
        Me.Tab1.Groups.Add(Me.LIstasPrecios)
        Me.Tab1.Groups.Add(Me.Group1)
        Me.Tab1.Label = "TabAddIns"
        Me.Tab1.Name = "Tab1"
        '
        'LIstasPrecios
        '
        Me.LIstasPrecios.Items.Add(Me.ButtonCargaCanales)
        Me.LIstasPrecios.Items.Add(Me.DropDownCanal)
        Me.LIstasPrecios.Items.Add(Me.DropDownPer)
        Me.LIstasPrecios.Label = "Precios por Canal"
        Me.LIstasPrecios.Name = "LIstasPrecios"
        '
        'DropDownCanal
        '
        Me.DropDownCanal.Label = " Canal"
        Me.DropDownCanal.Name = "DropDownCanal"
        '
        'ButtonGenenraCanal
        '
        Me.ButtonGenenraCanal.Label = "Bajar Base"
        Me.ButtonGenenraCanal.Name = "ButtonGenenraCanal"
        '
        'ButtonSubirPrecios
        '
        Me.ButtonSubirPrecios.Label = "Subir Precios"
        Me.ButtonSubirPrecios.Name = "ButtonSubirPrecios"
        '
        'ButtonCargaCanales
        '
        Me.ButtonCargaCanales.Label = "Cargar Canales"
        Me.ButtonCargaCanales.Name = "ButtonCargaCanales"
        '
        'DropDownPer
        '
        Me.DropDownPer.Label = "Periodo"
        Me.DropDownPer.Name = "DropDownPer"
        '
        'Group1
        '
        Me.Group1.Items.Add(Me.ButtonGenenraCanal)
        Me.Group1.Items.Add(Me.ButtonSubirPrecios)
        Me.Group1.Label = "Acciones"
        Me.Group1.Name = "Group1"
        '
        'Ribbon1
        '
        Me.Name = "Ribbon1"
        Me.RibbonType = "Microsoft.Excel.Workbook"
        Me.Tabs.Add(Me.Tab1)
        Me.Tab1.ResumeLayout(False)
        Me.Tab1.PerformLayout()
        Me.LIstasPrecios.ResumeLayout(False)
        Me.LIstasPrecios.PerformLayout()
        Me.Group1.ResumeLayout(False)
        Me.Group1.PerformLayout()

    End Sub

    Friend WithEvents Tab1 As Microsoft.Office.Tools.Ribbon.RibbonTab
    Friend WithEvents LIstasPrecios As Microsoft.Office.Tools.Ribbon.RibbonGroup
    Friend WithEvents DropDownCanal As Microsoft.Office.Tools.Ribbon.RibbonDropDown
    Friend WithEvents ButtonGenenraCanal As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents ButtonSubirPrecios As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents ButtonCargaCanales As Microsoft.Office.Tools.Ribbon.RibbonButton
    Friend WithEvents DropDownPer As Microsoft.Office.Tools.Ribbon.RibbonDropDown
    Friend WithEvents Group1 As Microsoft.Office.Tools.Ribbon.RibbonGroup
End Class

Partial Class ThisRibbonCollection

    <System.Diagnostics.DebuggerNonUserCode()> _
    Friend ReadOnly Property Ribbon1() As Ribbon1
        Get
            Return Me.GetRibbon(Of Ribbon1)()
        End Get
    End Property
End Class
