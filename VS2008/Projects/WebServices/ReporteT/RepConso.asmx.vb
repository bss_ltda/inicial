﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Service1
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function InsertRRCC(ByVal Usuario As String, ByVal Pass As String, ByVal Conso As String, ByVal Manifiesto As String, _
                               ByVal Placa As String, ByVal EstadEntrega As Integer, ByVal FechaReporte As String, _
                               ByVal codUbicacion As String, ByVal Ubicacion As String, _
                               ByVal tipoOrigen As String, ByVal tipoNovedad As String, ByVal Reporte As String) As String
        Dim DB As New ADODB.Connection
        Dim fSql, vSql As String
        Dim rs As New ADODB.Recordset
        Dim Result As String = "OK"
        Dim gsConnString As String = "Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=APLSSA;Password=SSAAPL;Persist Security Info=True;Default Collection=BPCSF;Force Translate=0;"

        Usuario = Usuario.ToUpper
        Pass = Pass.ToUpper

        DB.Open(gsConnString)
        rs.Open("SELECT * FROM RCAU WHERE UUSR='" & Usuario & "' AND UPASS='" & Pass & "'", DB)
        If rs.EOF Then
            Return "Usuario Invalido."
        End If
        rs.Close()

        rs.Open("SELECT HESTAD FROM RHCC WHERE HCONSO='" & Conso & "'", DB)
        If rs.EOF Then
            Return "Consolidado no Existe."
        End If
        rs.Close()

        If Len(Trim(Manifiesto)) > 15 Then
            Return "Longitud de manifiesto excedida"
        End If

        If Len(Trim(codUbicacion)) > 15 Then
            Return "Longitud de Codigo de Ubicacion excedida"
        End If

        If Len(Trim(tipoOrigen)) > 15 Then
            Return "Longitud de Tipo de Origen excedida"
        End If

        If Len(Trim(tipoNovedad)) > 15 Then
            Return "Longitud de Tipo de Novedad excedida"
        End If

        If Len(Trim(Placa)) > 6 Then
            Return "Longitud de placa excedida"
        End If

        fSql = "INSERT INTO RRCC("
        vSql = "VALUES( "
        fSql = fSql & " RCONSO, " : vSql = vSql & "'" & Conso & "'" & ", "
        fSql = fSql & " RSTSC, "
        vSql = vSql & " ( SELECT HESTAD FROM RHCC WHERE HCONSO='" & Conso & "' )" & ", "
        fSql = fSql & " RTREP, " : vSql = vSql & " " & "3" & " " & ", "
        fSql = fSql & " RMANIF    , " : vSql = vSql & "'" & Manifiesto & "', "     'Manifiesto del Transp
        fSql = fSql & " RPLACA    , " : vSql = vSql & "'" & UCase(Placa) & "', "     'Placa
        fSql = fSql & " RSTSE     , " : vSql = vSql & " " & EstadEntrega & ", "      'Est Entrega
        fSql = fSql & " RFECREP   , " : vSql = vSql & "'" & Replace(Replace(FechaReporte, " ", "-"), ":", ".") & ".000000', "
        fSql = fSql & " RCODUBI   , " : vSql = vSql & "'" & codUbicacion & "', "     'Codigo Ubicacion
        fSql = fSql & " RUBIC     , " : vSql = vSql & "'" & Ubicacion & "', "     'Ubicacion
        fSql = fSql & " RTIPORI   , " : vSql = vSql & "'" & tipoOrigen & "', "     'Tipo de Origen de Nov
        fSql = fSql & " RTIPNOV   , " : vSql = vSql & "'" & tipoNovedad & "', "     'Tipo de Novedad
        fSql = fSql & " RREPORT   , " : vSql = vSql & "'" & Replace(Left(Reporte, 500), "'", "''") & "'" & ", "
        fSql = fSql & " RAPP      , " : vSql = vSql & "'" & "WEBSERVICE" & "', "
        fSql = fSql & " RCRTUSR   ) " : vSql = vSql & "'" & Usuario & "'" & ") "
        DB.Execute(fSql & vSql)
        DB.Close()

        Return Result

    End Function


End Class