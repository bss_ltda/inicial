﻿Imports System
Imports System.Globalization
Imports System.IO
Imports System.Collections
Imports System.Xml.Serialization
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Xml
Imports System.Net


<System.Web.Services.WebService(Namespace:="http://soap.bellota.co/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Pedidos
    Dim Programa As String
    Dim ERRORES As String
    Public Conn As New ADODB.Connection
    Dim appSettings As NameValueCollection = ConfigurationManager.AppSettings
    Dim nuevaLinea As DetallePedido
    Dim Biblioteca As String
    Dim Cabecera As New CabeceraPedido
    Dim Lineas As New List(Of DetallePedido)
    Dim rs As New ADODB.Recordset


    Structure CabeceraPedido
        Dim NumeroDocumento As String
        Dim OrdenDeCompra As String
        Dim Cliente As String
        Dim PuntoDeEnvio As String
        Dim FechaRequerido As String
        Dim Notas As String
        Dim EnFirme As String
        Dim Pais As String
    End Structure

    Structure DetallePedido
        Dim LineaNumero As Integer
        Dim Producto As String
        Dim Cantidad As Double
        Dim Precio As Double
        Dim UnidadDeMedida As String
        Dim NotaLinea As String
    End Structure

    Dim Usuario As String = "WEBPED"
    Dim numLinea As Integer = 1
    Dim FechaTransaccion As Integer


    <WebMethod()> _
    Public Function CrearPedido(ByVal Pedido As String) As XmlDocument



        System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("es-CO")
        System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator = "."
        System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyGroupSeparator = ","
        System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator = "."
        System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberGroupSeparator = ","

        Dim Cabecera As New CabeceraPedido
        Dim Lineas As New List(Of DetallePedido)
        Dim documento As XDocument = XDocument.Parse(Pedido)
        Dim fSql As String
        Dim DB1 As New ExecuteSQL
        Dim qx = From xee In documento.Elements("Orden")
        Select New With { _
                 .NumeroDocumento = xee.Element("NumeroDocumento").Value,
                 .OrdenDeCompra = xee.Element("OrdenDeCompra").Value,
                 .Cliente = xee.Element("Cliente").Value,
                 .PuntoDeEnvio = xee.Element("PuntoDeEnvio").Value,
                 .FechaRequerido = xee.Element("FechaRequerido").Value,
                 .Notas = xee.Element("Notas").Value,
                 .EnFirme = xee.Element("EnFirme").Value,
                 .Pais = xee.Element("Pais").Value
                   }
        Try
            For Each elements In qx
                Cabecera = New CabeceraPedido
                Cabecera.Cliente = elements.Cliente
                Cabecera.FechaRequerido = elements.FechaRequerido
                Cabecera.Notas = elements.Notas
                Cabecera.NumeroDocumento = elements.NumeroDocumento
                Cabecera.OrdenDeCompra = elements.OrdenDeCompra
                Cabecera.PuntoDeEnvio = elements.PuntoDeEnvio
                Cabecera.EnFirme = elements.EnFirme
                Cabecera.Pais = elements.Pais
                Biblioteca = Cabecera.Pais

                If Cabecera.Pais = "Colombia" Then
                    Biblioteca = appSettings(3)
                Else
                    Biblioteca = appSettings(5)
                End If

                If Not SetConexion(DB1, Cabecera.Pais) Then
                    Return DevuelveError(Cabecera.NumeroDocumento, "Error en Conexion" & DB1.ShowError(), "903")
                End If
                If Cabecera.EnFirme = 0 Then
                    fSql = " SELECT * FROM " & Biblioteca & ".FFPEDID WHERE ADOCNR = " & Cabecera.NumeroDocumento
                    'wrtLog(fSql, 0)
                    DB1.Cursor(rs, (fSql))
                    While Not rs.EOF
                        fSql = "DELETE  FROM " & Biblioteca & ".FFPEDID WHERE ADOCNR = " & Cabecera.NumeroDocumento
                        Sql.Delete(DB1, fSql)
                        rs.MoveNext()
                    End While
                    rs.Close()
                End If
            Next

        Catch
            DB1.Close()
            Return DevuelveError("000", "Error en XML" & Err.Description, Err.Number)
        End Try

        Try

            Dim qx2 = From xe In documento.Descendants.Elements("Linea")
        Select New With { _
                          .LineaNumero = xe.Element("LineaNumero").Value,
                          .Producto = xe.Element("Producto").Value,
                          .Cantidad = xe.Element("Cantidad").Value,
                          .UnidadDeMedida = xe.Element("UnidadDeMedida").Value
                            }
            For Each elementos In qx2
                nuevaLinea = New DetallePedido
                nuevaLinea.Cantidad = elementos.Cantidad
                If nuevaLinea.Cantidad <= 0 Then
                    DB1.Close()
                    Return DevuelveError("000", "Cantidad no puede ser menor o igual que 0", 106)
                End If
                nuevaLinea.LineaNumero = elementos.LineaNumero
                nuevaLinea.Producto = elementos.Producto
                nuevaLinea.UnidadDeMedida = elementos.UnidadDeMedida
            Next

        Catch
            DB1.Close()
            Return DevuelveError("000", "Error en XML " & Err.Description, Err.Number)
        End Try

        Dim qx1 = From xe In documento.Descendants.Elements("Linea")
Select New With { _
                .LineaNumero = xe.Element("LineaNumero").Value,
                .Producto = xe.Element("Producto").Value,
                .Cantidad = xe.Element("Cantidad").Value,
                .UnidadDeMedida = xe.Element("UnidadDeMedida").Value
                  }

        If Cabecera.EnFirme = 0 Then
            For Each elementos In qx1
                nuevaLinea = New DetallePedido
                nuevaLinea.Cantidad = elementos.Cantidad
                nuevaLinea.LineaNumero = elementos.LineaNumero
                nuevaLinea.Producto = elementos.Producto
                nuevaLinea.UnidadDeMedida = elementos.UnidadDeMedida

                '   fSql = " SELECT * FROM " & Biblioteca & ".FFPEDID WHERE ADOCNR = " & Cabecera.NumeroDocumento & " AND ALINE ='" & nuevaLinea.LineaNumero & "'"
                '    'wrtLog(fSql, 0)
                '    DB1.Cursor(rs, (fSql))

                '         While Not rs.EOF
                'Return DevuelveError(Cabecera.NumeroDocumento, "Lineas repetidas dentro del documento", "202")
                'rs.MoveNext()
                'End While
                'rs.Close()
                Sql.mkSql(t_iF, " INSERT INTO " & Biblioteca & ".FFPEDID ( ")
                Sql.mkSql(t_iV, " VALUES ( ")
                Sql.mkSql(tNum, " AQORD     ", nuevaLinea.Cantidad, 0)                 '//11S3  Cantidad
                Sql.mkSql(tNum, " ALINE     ", nuevaLinea.LineaNumero, 0)              '//4S0   Linea del Docume
                Sql.mkSql(tStr, " AUSER     ", Usuario, 0)                             '//10A   Usuario Ingreso
                Sql.mkSql(tNum, " ACUST     ", Cabecera.Cliente, 0)                    '//8S0   Cliente
                Sql.mkSql(tStr, " ANOTE     ", Cabecera.Notas, 0)                      '//50A   Nota al pedido
                Sql.mkSql(tNum, " ASHIP     ", Cabecera.PuntoDeEnvio, 0)               '//4S0   Punto de Envio
                Sql.mkSql(tStr, " AUNMED       ", nuevaLinea.UnidadDeMedida, 0)           '//2A    Unidad Medida
                Sql.mkSql(tStr, " APO       ", Cabecera.OrdenDeCompra, 0)              '//15A   Orden de Compra
                Sql.mkSql(tNum, " ARDTE     ", Cabecera.FechaRequerido, 0)             '//8S0   Fecha de Requerido
                Sql.mkSql(tStr, " APROD     ", nuevaLinea.Producto, 0)                 '//15A   Producto

                Sql.mkSql(tNum, " ADOCNR    ", Cabecera.NumeroDocumento, 1)            '//8S0   Documen

                If (Sql.Insert(DB1) = -99) Then
                    DB1.Close()
                    Return DevuelveError(Cabecera.NumeroDocumento, "", "902")
                End If
                Lineas.Add(nuevaLinea)

            Next
        End If
        Dim cero As String
        cero = Ceros(Cabecera.NumeroDocumento, 8)
        Dim Biblioteca1 As String
        Biblioteca1 = Espacios(appSettings(4), 10)
        Dim Biblioteca2 As String
        Biblioteca2 = Espacios(appSettings(6), 10)

        If Cabecera.Pais = "Colombia" Then
            Programa = "CALL " & appSettings(4) & "CCFCGENPED('" & cero & "','" & Cabecera.EnFirme & "','" & Biblioteca1 & "' )"
            'wrtLog(Programa, 0)
            If (DB1.Call_PGM("CALL " & appSettings(4) & ".FCGENPED('" & cero & "','" & Cabecera.EnFirme & "','" & Biblioteca1 & "' )", Cabecera.Pais)) = -99 Then
                DB1.Close()
                WrtLogError(DB1.ShowError.ToString, Cabecera.Pais, Biblioteca, Cabecera.NumeroDocumento)

                Return DevuelveError(Cabecera.NumeroDocumento, DB1.ShowError, "902")
            End If
        Else
            Programa = "CALL " & appSettings(6) & ".FCGENPED('" & cero & "','" & Cabecera.EnFirme & "','" & Biblioteca2 & "' )"
            'wrtLog(Programa, 0)
            If (DB1.Call_PGM("CALL " & appSettings(6) & ".FCGENPED(" & cero & ", " & Cabecera.EnFirme & "," & Biblioteca2 & " )", Cabecera.Pais)) = -99 Then
                DB1.Close()
                WrtLogError(DB1.ShowError.ToString, Cabecera.Pais, Biblioteca, Cabecera.NumeroDocumento)
                Return DevuelveError(Cabecera.NumeroDocumento, DB1.ShowError, "902")

            End If

        End If


        fSql = " INSERT INTO " & Biblioteca & ".FFPEDLOG( "
        vSql = " SELECT "
        fSql = fSql & " ADOCNR    , " : vSql = vSql & " ADOCNR    , "     '//8S0   Documen
        fSql = fSql & " ALINE     , " : vSql = vSql & " ALINE     , "     '//3S0   Linea del Docume
        fSql = fSql & " ACUST     , " : vSql = vSql & " ACUST     , "     '//6S0   Cliente
        fSql = fSql & " ASHIP     , " : vSql = vSql & " ASHIP     , "     '//4S0   Punto de Envio
        fSql = fSql & " APEDLX    , " : vSql = vSql & " APEDLX    , "     '//6S0   Pedido BPCS
        fSql = fSql & " ALINLX    , " : vSql = vSql & " ALINLX    , "     '//3S0   Linea del pedido BPCS
        fSql = fSql & " APROD     , " : vSql = vSql & " APROD     , "     '//15A   Producto
        fSql = fSql & " AQORD     , " : vSql = vSql & " AQORD     , "     '//11S3  Cantidad Ped
        fSql = fSql & " AQORDST   , " : vSql = vSql & " AQORDST   , "     '//11S3  Cantidad Ped Stk
        fSql = fSql & " AVALOR    , " : vSql = vSql & " AVALOR    , "     '//15S3  Valor UM Ped
        fSql = fSql & " AVALST    , " : vSql = vSql & " AVALST    , "     '//15S3  Valor UM Stk
        fSql = fSql & " ATASA1    , " : vSql = vSql & " ATASA1    , "     '//5A    Tasa Impuesto 1
        fSql = fSql & " AVALIM1   , " : vSql = vSql & " AVALIM1   , "     '//14S4  Valor Impuesto 1
        fSql = fSql & " ATASA2    , " : vSql = vSql & " ATASA2    , "     '//5A    Tasa Impuesto 2
        fSql = fSql & " AVALIM2   , " : vSql = vSql & " AVALIM2   , "     '//14S4  Valor Impuesto 2
        fSql = fSql & " ATASA3    , " : vSql = vSql & " ATASA3    , "     '//5A    Tasa Impuesto 3
        fSql = fSql & " AVALIM3   , " : vSql = vSql & " AVALIM3   , "     '//14S4  Valor Impuesto 3
        fSql = fSql & " ATASA4    , " : vSql = vSql & " ATASA4    , "     '//5A    Tasa Impuesto 4
        fSql = fSql & " AVALIM4   , " : vSql = vSql & " AVALIM4   , "     '//14S4  Valor Impuesto 4
        fSql = fSql & " ATASA5    , " : vSql = vSql & " ATASA5    , "     '//5A    Tasa Impuesto 5
        fSql = fSql & " AVALIM5   , " : vSql = vSql & " AVALIM5   , "     '//14S4  Valor Impuesto 5
        fSql = fSql & " ATASA6    , " : vSql = vSql & " ATASA6    , "     '//5A    
        fSql = fSql & " AVALIM6   , " : vSql = vSql & " AVALIM6   , "     '//14S4  
        fSql = fSql & " ATASA7    , " : vSql = vSql & " ATASA7    , "     '//5A    
        fSql = fSql & " AVALIM7   , " : vSql = vSql & " AVALIM7   , "     '//14S4  
        fSql = fSql & " ATASA8    , " : vSql = vSql & " ATASA8    , "     '//5A    
        fSql = fSql & " AVALIM8   , " : vSql = vSql & " AVALIM8   , "     '//14S4  
        fSql = fSql & " APO       , " : vSql = vSql & " APO       , "     '//15A   Orden de Compra
        fSql = fSql & " ARDTE     , " : vSql = vSql & " ARDTE     , "     '//8S0   Fecha de Requerido
        fSql = fSql & " ANOTE     , " : vSql = vSql & " ANOTE     , "     '//150A  Nota al pedido
        fSql = fSql & " ALNOTE    , " : vSql = vSql & " ALNOTE    , "     '//50A   Nota a la linea
        fSql = fSql & " AERROR    , " : vSql = vSql & " AERROR    , "     '//3S0   Codigo Error
        fSql = fSql & " ADESER    , " : vSql = vSql & " ADESER    , "     '//30A   Descripcion Error
        fSql = fSql & " AUSER     , " : vSql = vSql & " AUSER     , "     '//10A   Usuario Ingreso
        fSql = fSql & " AATUPD    , " : vSql = vSql & " AATUPD    , "     '//26Z   Fecha
        fSql = fSql & " AUNMED    , " : vSql = vSql & " AUNMED    , "     '//2A    Unidad Medida
        fSql = fSql & " AFLAG     , " : vSql = vSql & " AFLAG     , "     '//1A    Flag de Procesado
        fSql = fSql & " ADOCIN   ) " : vSql = vSql & " ADOCIN    "     '//6P0   
        vSql = vSql & " FROM " & Biblioteca & ".FFPEDID"
        vSql = vSql & " WHERE "
        vSql = vSql & " ADOCNR = " & Cabecera.NumeroDocumento
        DB1.ExecuteSQL(fSql & vSql)

        Dim flag As Integer = 0
        Dim flag2 As Integer = 0

        fSql = " SELECT * FROM " & Biblioteca & ".FFPEDID WHERE ADOCNR = " & Cabecera.NumeroDocumento
        'wrtLog(fSql, 0)
        DB1.Cursor(rs, (fSql))

        While Not rs.EOF
            flag2 = flag2 + 1
            If rs("AERROR").Value <> 0 Then
                Dim errDesc As String = rs("ADESER").Value
                Dim errCode As String = rs("AERROR").Value
                rs.Close()
                DB1.Close()
                Return DevuelveError(Cabecera.NumeroDocumento, errDesc, errCode)
            End If
            rs.MoveNext()
        End While
        rs.Close()
        DB1.Close()
        Return DevuelveResultado(Cabecera.NumeroDocumento, Cabecera.Pais)


    End Function

    Function WrtLogError(ByVal sError As String, ByVal PAIS As String, ByVal BIBLIOTECA As String, ByVal DOCUMENTO As String)
        Dim DB1 As New ExecuteSQL

        Try
            SetConexion(DB1, PAIS)

            fSql = " INSERT INTO " & BIBLIOTECA & ".FFPEDLOG( "
            vSql = " SELECT "
            fSql = fSql & " ADOCNR    , " : vSql = vSql & " ADOCNR    , "     '//8S0   Documen
            fSql = fSql & " ALINE     , " : vSql = vSql & " ALINE     , "     '//3S0   Linea del Docume
            fSql = fSql & " ACUST     , " : vSql = vSql & " ACUST     , "     '//6S0   Cliente
            fSql = fSql & " ASHIP     , " : vSql = vSql & " ASHIP     , "     '//4S0   Punto de Envio
            fSql = fSql & " APEDLX    , " : vSql = vSql & " APEDLX    , "     '//6S0   Pedido BPCS
            fSql = fSql & " ALINLX    , " : vSql = vSql & " ALINLX    , "     '//3S0   Linea del pedido BPCS
            fSql = fSql & " APROD     , " : vSql = vSql & " APROD     , "     '//15A   Producto
            fSql = fSql & " AQORD     , " : vSql = vSql & " AQORD     , "     '//11S3  Cantidad Ped
            fSql = fSql & " AQORDST   , " : vSql = vSql & " AQORDST   , "     '//11S3  Cantidad Ped Stk
            fSql = fSql & " AVALOR    , " : vSql = vSql & " AVALOR    , "     '//15S3  Valor UM Ped
            fSql = fSql & " AVALST    , " : vSql = vSql & " AVALST    , "     '//15S3  Valor UM Stk
            fSql = fSql & " ATASA1    , " : vSql = vSql & " ATASA1    , "     '//5A    Tasa Impuesto 1
            fSql = fSql & " AVALIM1   , " : vSql = vSql & " AVALIM1   , "     '//14S4  Valor Impuesto 1
            fSql = fSql & " ATASA2    , " : vSql = vSql & " ATASA2    , "     '//5A    Tasa Impuesto 2
            fSql = fSql & " AVALIM2   , " : vSql = vSql & " AVALIM2   , "     '//14S4  Valor Impuesto 2
            fSql = fSql & " ATASA3    , " : vSql = vSql & " ATASA3    , "     '//5A    Tasa Impuesto 3
            fSql = fSql & " AVALIM3   , " : vSql = vSql & " AVALIM3   , "     '//14S4  Valor Impuesto 3
            fSql = fSql & " ATASA4    , " : vSql = vSql & " ATASA4    , "     '//5A    Tasa Impuesto 4
            fSql = fSql & " AVALIM4   , " : vSql = vSql & " AVALIM4   , "     '//14S4  Valor Impuesto 4
            fSql = fSql & " ATASA5    , " : vSql = vSql & " ATASA5    , "     '//5A    Tasa Impuesto 5
            fSql = fSql & " AVALIM5   , " : vSql = vSql & " AVALIM5   , "     '//14S4  Valor Impuesto 5
            fSql = fSql & " ATASA6    , " : vSql = vSql & " ATASA6    , "     '//5A    
            fSql = fSql & " AVALIM6   , " : vSql = vSql & " AVALIM6   , "     '//14S4  
            fSql = fSql & " ATASA7    , " : vSql = vSql & " ATASA7    , "     '//5A    
            fSql = fSql & " AVALIM7   , " : vSql = vSql & " AVALIM7   , "     '//14S4  
            fSql = fSql & " ATASA8    , " : vSql = vSql & " ATASA8    , "     '//5A    
            fSql = fSql & " AVALIM8   , " : vSql = vSql & " AVALIM8   , "     '//14S4  
            fSql = fSql & " APO       , " : vSql = vSql & " APO       , "     '//15A   Orden de Compra
            fSql = fSql & " ARDTE     , " : vSql = vSql & " ARDTE     , "     '//8S0   Fecha de Requerido
            fSql = fSql & " ANOTE     , " : vSql = vSql & "'" & Left(sError, 150) & "'," '//150A  Nota al pedido
            fSql = fSql & " ALNOTE    , " : vSql = vSql & " ALNOTE    , "     '//50A   Nota a la linea
            fSql = fSql & " AERROR    , " : vSql = vSql & " 999     , "     '//3S0   Codigo Error
            fSql = fSql & " ADESER    , " : vSql = vSql & " 'ERROR AS 400'    , "     '//30A   Descripcion Error
            fSql = fSql & " AUSER     , " : vSql = vSql & " AUSER     , "     '//10A   Usuario Ingreso
            fSql = fSql & " AATUPD    , " : vSql = vSql & " AATUPD    , "     '//26Z   Fecha
            fSql = fSql & " AUNMED    , " : vSql = vSql & " AUNMED    , "     '//2A    Unidad Medida
            fSql = fSql & " AFLAG     , " : vSql = vSql & " AFLAG     , "     '//1A    Flag de Procesado
            fSql = fSql & " ADOCIN   ) " : vSql = vSql & " ADOCIN    "     '//6P0   
            vSql = vSql & " FROM " & BIBLIOTECA & ".FFPEDID"
            vSql = vSql & " WHERE "
            vSql = vSql & " ADOCNR = " & DOCUMENTO
            DB1.ExecuteSQL(fSql & vSql)
            DB1.Close()
        Catch

        End Try
    End Function
    Function DevuelveResultado(ByVal NumeroDocumento As String, ByVal pais As String)
        Dim fSql As String
        Dim xmlstr As String = ""
        Dim sw As New StringWriter()
        Dim writer As New XmlTextWriter(sw)
        Dim flag As Integer = 0
        Dim flag2 As Integer = 0
        Dim DB As New ExecuteSQL
        Dim rs As New ADODB.Recordset

        If Not SetConexion(DB, pais) Then
            Return DevuelveError(Cabecera.NumeroDocumento, "Error en Conexion" & DB.ShowError(), "903")
        End If

        writer.WriteStartDocument(True)
        writer.Formatting = Formatting.Indented
        writer.Indentation = 2
        writer.WriteStartElement("Pedido")
        writer.WriteStartElement("NumeroDocumento")
        writer.WriteString(NumeroDocumento)
        writer.WriteEndElement()
        writer.WriteStartElement("Programa")
        writer.WriteString(Programa)
        writer.WriteEndElement()
        writer.WriteStartElement("EstadoPrograma")
        writer.WriteString(ERRORES)
        writer.WriteEndElement()
        writer.WriteStartElement("NumeroPedido")

        fSql = " SELECT * FROM " & Biblioteca & ".FFPEDID WHERE ADOCNR = " & NumeroDocumento
        'wrtLog(fSql, 0)
        DB.Cursor(rs, (fSql))
        'wrtLog("antes: " & CStr(rs("APEDLX").Value), 0)
        If Not rs.EOF Then
            'wrtLog("pEDIDO: " & CStr(rs("APEDLX").Value), 0)
            writer.WriteString(rs("APEDLX").Value)
            writer.WriteEndElement()
        End If

        While Not rs.EOF
            CrearNodoLine(rs("ALINE").Value, rs("AVALOR").Value, Cabecera.NumeroDocumento, rs("AVALIM1").Value, rs("ATASA1").Value, rs("AVALIM2").Value, rs("ATASA2").Value, rs("AVALIM3").Value, rs("ATASA3").Value, rs("AVALIM4").Value, rs("ATASA4").Value, rs("AVALIM5").Value, rs("ATASA5").Value, rs("AVALIM6").Value, rs("ATASA6").Value, rs("AVALIM7").Value, rs("ATASA7").Value, writer) ' Las veces que se desee Programa line
            rs.MoveNext()
        End While
        rs.Close()
        writer.WriteEndElement()

        writer.Flush()
        xmlstr = sw.ToString
        writer.Close()
        Dim pDoc As New XmlDocument
        pDoc.LoadXml(xmlstr)
        DB.Close()
        Return pDoc

    End Function

    Function CrearNodoError(ByVal ErrorXML As String, ByVal codigo As String, ByVal NumeroDocumento As String, ByVal writer As XmlTextWriter)


        writer.WriteStartElement("Envelope") ' ENVELOPE
        writer.WriteStartElement("Pedido")  ' PEDIDO
        writer.WriteString(NumeroDocumento)
        writer.WriteStartElement("Mensaje")     'MENSAJE
        writer.WriteStartElement("Error")       'ERROR
        writer.WriteStartElement("MensajeID")   'ID
        writer.WriteString(codigo)
        writer.WriteEndElement() ' MensajeId
        writer.WriteStartElement("MensajeDES")
        writer.WriteString(ErrorXML)
        writer.WriteEndElement() ' DES
        writer.WriteEndElement() ' ERROR
        writer.WriteEndElement() ' MENSAJE
        writer.WriteEndElement() ' PEDIDO
        writer.WriteEndElement() ' ENVELOPE
        Return 0
    End Function




    Function CrearNodoLine(ByVal Linea As String, ByVal precio As String, ByVal NumeroDocumento As String, ByVal Impuesto1 As String, ByVal Descripcion1 As String, ByVal Impuesto2 As String, ByVal Descripcion2 As String, ByVal Impuesto3 As String, ByVal Descripcion3 As String, ByVal Impuesto4 As String, ByVal Descripcion4 As String, ByVal Impuesto5 As String, ByVal Descripcion5 As String, ByVal Impuesto6 As String, ByVal Descripcion6 As String, ByVal Impuesto7 As String, ByVal Descripcion7 As String, ByVal writer As XmlTextWriter)

        precio.ToString(System.Globalization.CultureInfo.InvariantCulture)
        Impuesto1.ToString(System.Globalization.CultureInfo.InvariantCulture)
        Impuesto2.ToString(System.Globalization.CultureInfo.InvariantCulture)
        Impuesto3.ToString(System.Globalization.CultureInfo.InvariantCulture)
        Impuesto4.ToString(System.Globalization.CultureInfo.InvariantCulture)
        Impuesto5.ToString(System.Globalization.CultureInfo.InvariantCulture)

        writer.WriteStartElement("Linea")
        writer.WriteStartElement("LineaNumero")
        writer.WriteString(Linea)
        writer.WriteEndElement()
        writer.WriteStartElement("Precio")
        writer.WriteString(precio)
        writer.WriteEndElement()
        If Impuesto1 <> 0 Or Impuesto2 <> 0 Or Impuesto3 <> 0 Or Impuesto4 <> 0 Or Impuesto5 <> 0 Then
            writer.WriteStartElement("Impuestos")
            If Impuesto1 <> 0 Then
                writer.WriteStartElement("Impuesto")
                writer.WriteStartElement("ImpuestoNo")
                writer.WriteString(1)
                writer.WriteEndElement()
                writer.WriteStartElement("ImpuestoTipo")
                writer.WriteString(Descripcion1)
                writer.WriteEndElement()
                writer.WriteStartElement("ImpuestoValor")
                writer.WriteString(Impuesto1)
                writer.WriteEndElement()
                writer.WriteEndElement()
            End If
            If Impuesto2 <> 0 Then
                writer.WriteStartElement("Impuesto")
                writer.WriteStartElement("ImpuestoNo")
                writer.WriteString(2)
                writer.WriteEndElement()
                writer.WriteStartElement("ImpuestoTipo")
                writer.WriteString(Descripcion2)
                writer.WriteEndElement()
                writer.WriteStartElement("ImpuestoValor")
                writer.WriteString(Impuesto2)
                writer.WriteEndElement()
                writer.WriteEndElement()
            End If
            If Impuesto3 <> 0 Then
                writer.WriteStartElement("Impuesto")
                writer.WriteStartElement("ImpuestoNo")
                writer.WriteString(3)
                writer.WriteEndElement()
                writer.WriteStartElement("ImpuestoTipo")
                writer.WriteString(Descripcion3)
                writer.WriteEndElement()
                writer.WriteStartElement("ImpuestoValor")
                writer.WriteString(Impuesto3)
                writer.WriteEndElement()
                writer.WriteEndElement()
            End If
            If Impuesto4 <> 0 Then
                writer.WriteStartElement("Impuesto")
                writer.WriteStartElement("ImpuestoNo")
                writer.WriteString(4)
                writer.WriteEndElement()
                writer.WriteStartElement("ImpuestoTipo")
                writer.WriteString(Descripcion4)
                writer.WriteEndElement()
                writer.WriteStartElement("ImpuestoValor")
                writer.WriteString(Impuesto4)
                writer.WriteEndElement()
                writer.WriteEndElement()
            End If
            If Impuesto5 <> 0 Then
                writer.WriteStartElement("Impuesto")
                writer.WriteStartElement("ImpuestoNo")
                writer.WriteString(5)
                writer.WriteEndElement()
                writer.WriteStartElement("ImpuestoTipo")
                writer.WriteString(Descripcion5)
                writer.WriteEndElement()
                writer.WriteStartElement("ImpuestoValor")
                writer.WriteString(Impuesto5)
                writer.WriteEndElement()
                writer.WriteEndElement()
            End If

            If Impuesto6 <> 0 Then
                writer.WriteStartElement("Impuesto")
                writer.WriteStartElement("ImpuestoNo")
                writer.WriteString(6)
                writer.WriteEndElement()
                writer.WriteStartElement("ImpuestoTipo")
                writer.WriteString(Descripcion6)
                writer.WriteEndElement()
                writer.WriteStartElement("ImpuestoValor")
                writer.WriteString(Impuesto6)
                writer.WriteEndElement()
                writer.WriteEndElement()
            End If

            If Impuesto7 <> 0 Then
                writer.WriteStartElement("Impuesto")
                writer.WriteStartElement("ImpuestoNo")
                writer.WriteString(7)
                writer.WriteEndElement()
                writer.WriteStartElement("ImpuestoTipo")
                writer.WriteString(Descripcion7)
                writer.WriteEndElement()
                writer.WriteStartElement("ImpuestoValor")
                writer.WriteString(Impuesto7)
                writer.WriteEndElement()
                writer.WriteEndElement()
            End If
            writer.WriteEndElement()
        End If
        writer.WriteEndElement()
        Return 0
    End Function


    Function DevuelveError(ByVal NumeroDocumento As String, ByVal ERROOR As String, ByVal CODIGO As String) As XmlDocument


        Dim xmlstr As String = ""
        Dim sw As New StringWriter()
        Dim writer As New XmlTextWriter(sw)
        writer.WriteStartDocument(True)
        writer.Formatting = Formatting.Indented
        writer.Indentation = 2
        CrearNodoError(ERROOR, CODIGO, NumeroDocumento, writer)
        writer.Flush()
        xmlstr = sw.ToString
        writer.Close()
        Dim pDoc As New XmlDocument
        pDoc.LoadXml(xmlstr)
        Return pDoc

    End Function



End Class


