﻿Imports System
Imports System.Globalization
Imports System.IO
Imports System.Collections
Imports System.Xml.Serialization
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Xml
Imports System.Net


<System.Web.Services.WebService(Namespace:="http://soap.bellota.co/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Pedidos
    Dim LLAMAR As String
    Dim ERRORES As String
    Public Conn As New ADODB.Connection
    Dim appSettings As NameValueCollection = _
            ConfigurationManager.AppSettings
    '   Public Conn As New ADODB.Connection
    Dim Biblioteca As String
    Dim clientes As New cliente
    Dim rs As New ADODB.Recordset


    Structure cliente
        Dim Cliente As String
        Dim Pais As String
    End Structure


    Dim Usuario As String = "WEBPED"
    Dim numLinea As Integer = 1
    '   Dim iDB2 As iDB2Command
    Dim FechaTransaccion As Integer
    '  Dim adaptador As iDB2DataAdapter = New IBM.Data.DB2.iSeries.iDB2DataAdapter


    <WebMethod()> _
    Public Function CrearPedido(ByVal Pedido As String) As XmlDocument
        Dim Cabecera As New cliente
        Dim documento As XDocument = XDocument.Parse(Pedido)

        Dim qx = From xee In documento.Elements("Clientes")
        Select New With { _
                 .Cliente = xee.Element("Cliente").Value,
        .Pais = xee.Element("Pais").Value
                   }

        If Cabecera.Pais = "Colombia" Then
            Biblioteca = appSettings(3)
        Else
            Biblioteca = appSettings(5)
        End If
        For Each elements In qx
            Cabecera.Cliente = elements.Cliente

            Dim insert As New ExecuteSQL
            SetConexion(insert, Cabecera.Pais)
            Sql.mkSql(t_iF, " INSERT INTO  FFPRECIO ( ")
            Sql.mkSql(t_iV, " VALUES ( ")
            Sql.mkSql(tNum, " PCUST      ", Cabecera.Cliente, 0)
            Sql.mkSql(tNum, " PUSER      ", Usuario, 1)
            '//10A   Usuario Ingreso
            Sql.Insert(insert)
         Next

        Dim programas As New ExecuteSQL
        Dim programa As String
        SetConexion(programas, Cabecera.Pais)
        LLAMAR = "CALL PGM(" & appSettings(4) & "/FCPRECIO ) PARM('" & Cabecera.Cliente & "' )"

        Dim cliente As String
        cliente = Ceros(Cabecera.Cliente, 6)
        Dim Biblioteca1 As String
        Biblioteca1 = Espacios(appSettings(4), 10)
        Dim Biblioteca2 As String
        Biblioteca2 = Espacios(appSettings(6), 10)

        If Cabecera.Pais = "Colombia" Then
            programa = "CALL " & appSettings(4) & ".FCLISPRE ('" & cliente & "','" & Biblioteca1 & "')"
            wrtLog(programa, 0)
            ERRORES = programas.Call_PGM("CALL " & appSettings(4) & ".FCLISPRE ('" & cliente & "','" & Biblioteca1 & "')", Cabecera.Pais)
        Else
            programa = "CALL " & appSettings(4) & ".FCLISPRE ('" & cliente & "','" & Biblioteca1 & "')"
            wrtLog(programa, 0)
            ERRORES = programas.Call_PGM("CALL " & appSettings(4) & ".FCLISPRE ('" & cliente & "','" & Biblioteca1 & "')", Cabecera.Pais)
        End If

        Dim DB As New ExecuteSQL

        SetConexion(DB, Cabecera.Pais)
        DB.Open()

        Dim fsql As String


        fsql = " SELECT * FROM " & Biblioteca & ".FFPRECIO WHERE PCUST = " & cliente

        Dim flag As Integer = 0
        Dim flag2 As Integer = 0

        DB.Cursor(rs, (fsql))

        While Not rs.EOF
            flag2 = flag2 + 1
            '           If rs("PERROR").Value <> "000" Or rs("PERROR").Value <> "0" Then
            ' Return DevuelveError(Cabecera.Cliente, rs("PDESER").Value, rs("PERROR").Value)
            '          flag = 1
            '         Exit FunctionS
            '        End If
            rs.MoveNext()

        End While

        If flag = 0 And flag2 > 0 Then
            Return DevuelveResultado(cliente, Cabecera.Pais)
        End If

    End Function


    Function DevuelveResultado(ByVal Cliente As String, ByVal pais As String)

        Dim rs As New ADODB.Recordset
        Dim xmlstr As String = ""
        Dim sw As New StringWriter()
        Dim writer As New XmlTextWriter(sw)
        writer.WriteStartDocument(True)
        writer.Formatting = Formatting.Indented
        writer.Indentation = 2
        writer.WriteStartElement("Precios")
        writer.WriteStartElement("Cliente")
        writer.WriteString(Cliente)
        writer.WriteEndElement()
        writer.WriteStartElement("Programa")
        writer.WriteString(LLAMAR)
        writer.WriteEndElement()
        writer.WriteStartElement("EstadoPrograma")
        writer.WriteString(ERRORES)
        writer.WriteEndElement()


        Dim DB2 As New ExecuteSQL
        SetConexion(DB2, pais)
        DB2.Open()


        Dim fsql As String

        fsql = " SELECT * FROM  " & Biblioteca & ".FFPRECIO WHERE PCUST   = " & Cliente
        Dim flag As Integer = 0
        Dim flag2 As Integer = 0

        DB2.Cursor(rs, (fsql))


        While Not rs.EOF

            CrearNodoLine(rs("PPROD").Value, rs("PVALOR").Value, writer) ' Las veces que se desee llamar line
            rs.MoveNext()
        End While

        writer.WriteEndElement()

        writer.Flush()
        xmlstr = sw.ToString
        writer.Close()
        Dim pDoc As New XmlDocument
        pDoc.LoadXml(xmlstr)
        Return pDoc

    End Function

    Function CrearNodoError(ByVal ErrorXML As String, ByVal codigo As String, ByVal NumeroDocumento As String, ByVal writer As XmlTextWriter)


        writer.WriteStartElement("Envelope")
        writer.WriteStartElement("Cliente")
        writer.WriteString(NumeroDocumento)
        writer.WriteStartElement("Mensaje")
        writer.WriteStartElement("Error")
        writer.WriteStartElement("MensajeID")
        writer.WriteString(codigo)
        writer.WriteEndElement() ' MensajeId
        writer.WriteStartElement("MensajeDES")
        writer.WriteString(ErrorXML)
        writer.WriteEndElement() ' MensajeId
        writer.WriteEndElement() ' ERROR
        writer.WriteEndElement() ' MENSAJE
        writer.WriteEndElement() ' PEDIDO
        writer.WriteEndElement() ' ENVELOPE
        Return 0
    End Function




    Function CrearNodoLine(ByVal precio As String, ByVal producto As String, ByVal writer As XmlTextWriter)

        writer.WriteStartElement("PRODUCTOS")
        writer.WriteStartElement("Producto")
        writer.WriteString(producto)
        writer.WriteEndElement()
        writer.WriteStartElement("Precio")
        writer.WriteString(precio)
        writer.WriteEndElement()
        writer.WriteEndElement()

        Return 0
    End Function


    Function DevuelveError(ByVal NumeroDocumento As String, ByVal ERROOR As String, ByVal CODIGO As String) As XmlDocument

        Dim xmlstr As String = ""
        Dim sw As New StringWriter()
        Dim writer As New XmlTextWriter(sw)
        writer.WriteStartDocument(True)
        writer.Formatting = Formatting.Indented
        writer.Indentation = 2
        CrearNodoError(ERROOR, CODIGO, NumeroDocumento, writer)
        writer.WriteEndDocument()
        writer.Flush()
        xmlstr = sw.ToString
        writer.Close()
        Dim pDoc As New XmlDocument
        pDoc.LoadXml(xmlstr)
        Return pDoc

    End Function



End Class


