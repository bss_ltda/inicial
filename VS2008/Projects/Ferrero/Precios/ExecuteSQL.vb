﻿Imports IBM.Data.DB2.iSeries
Public Class ExecuteSQL
    '   Public Conn As New ADODB.Connection
    Public Sentencia As String
    Public bLog As Boolean = False
    Public iNumLog As Integer
    Public Conn As New ADODB.Connection
    Dim gsConnString As String

    Public Function Open() As Boolean



        Try

            Conn.Open(gsConnString)
          
        Catch
            Descripcion_Error = Err.Description
            wrtLog(Err.Description, iNumLog)
            Return False
        End Try
        Return True
    End Function

    Public Sub Close()
        
    End Sub

    Public Function State() As Integer
        ' Return Conn.State
        Return 0
    End Function

    Public Function DspErr() As String
        Return Error_Tabla & " " & Sentencia & " " & vbCrLf & Descripcion_Error
    End Function

    Public Function ExecuteSQL(ByVal sql As String) As Integer
        Dim r As Integer = 0

        Try
            Sentencia = sql
            wrtLog(sql, iNumLog)
            Conn.Execute(sql, r)
        Catch
            'MsgBox(Err.Description)
            'Debug.Print(sql)
            If InStr(sql, "QTEMP.CIMWMS") = 0 Then
                Descripcion_Error = Err.Description
                wrtLog(Err.Description, iNumLog)
                Return -99
            ElseIf InStr(Err.Description, "SQL0601") = 0 Then
                Descripcion_Error = Err.Description
                wrtLog(Err.Description, iNumLog)
                Return -99
            End If
        End Try

        Return r

    End Function

    Public Function Cursor(ByRef rs As ADODB.Recordset, ByVal sql As String) As Integer

        Dim Result As Integer
        Sentencia = sql
        Try
            rs.Open(sql, Conn)
        Catch
            Descripcion_Error = Sentencia & " " & Err.Description
            wrtLog(Err.Description, iNumLog)
            Return -99
        End Try
        Result = IIf(rs.EOF, 0, 1)
        Return Result

    End Function

    Public Function Cursor(ByRef rs As ADODB.Recordset) As Integer
        Dim Result As Integer
        Try
            rs.Open(Sentencia, Conn)
        Catch
            Descripcion_Error = Sentencia & " " & Err.Description
            wrtLog(Err.Description, iNumLog)
            Return -99
        End Try
        Result = IIf(rs.EOF, 0, 1)
        Return Result
    End Function



    Public Function CalcConsec(ByRef sID As String, ByRef TopeMax As String) As String
        Return ""
    End Function
    Public Function Call_PGM(ByVal Pgm As String, ByVal pais As String) As String
        Dim r As Integer = 0
        'Dim Conn As 
        Dim DB As New ExecuteSQL
        SetConexion(DB, pais)
        DB.Open()


        Try

            Conn.Execute(" {{ " & Pgm & " }} ", r)
            Return "OK"
        Catch
            Return Err.Description
        End Try



    End Function
    Sub setConnectionString(ByVal Prov As String, ByVal srv As String, ByVal usr As String, ByVal pass As String, ByVal sLib As String, ByVal sCatalogo As String)
        Dim g As String

        g = ""
        g = g & "DRIVER=Client Access ODBC Driver (32-bit); "
        g = g & "UID=" & usr & ";"
        g = g & "PWD=" & pass & ";"
        g = g & "SYSTEM=" & srv
        gsConnString = g
        wrtLog(g, iNumLog)
        Conn.Open(gsConnString)
    End Sub

    Public Function CalcConsecV(ByRef sID As String, ByRef TopeMax As String) As Double
        Return CDbl(CalcConsec(sID, TopeMax))
    End Function


    'Sub WrtLog(sql As String)
    '    Dim fSql, vSql As String

    '    If bLog Then
    '        fSql = " INSERT INTO BFLOG( "
    '        vSql = " VALUES ( "
    '        fSql = fSql & " LNUM      , " : vSql = vSql & " " & iNumLog & ", "
    '        fSql = fSql & " LLOG      ) " : vSql = vSql & "'" & Replace(sql, "'", "''") & "' )"
    '        DB.Execute(fSql & vSql)
    '    End If

    'End Sub

    Function SetNumLog() As Integer
        iNumLog = CInt(CalcConsec("WMSLOG", "99"))
        Return iNumLog
    End Function

    Public Function GetNumLog() As Integer
        Return iNumLog
    End Function


End Class

