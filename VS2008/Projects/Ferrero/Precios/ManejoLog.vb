﻿Imports System
Imports System.IO

Module ManejoLog

    Public Function wrtLog(ByVal txt As String, ByRef iNumLog As Integer) As String
        Dim fEntrada As String = ""
        Dim num As String = Right("000" & iNumLog, 3)

        Dim appPath As String = System.Web.HttpContext.Current.Request.ApplicationPath
        Dim CarpetaWeb As String = HttpContext.Current.Request.MapPath(appPath)

        fEntrada = Replace(CarpetaWeb & "\wmslog" & num & ".txt", "\\", "\")
        Try
            ' Create an instance of StreamReader to read from a file.
            Dim sr As StreamReader = New StreamReader(fEntrada)
            Dim sb As New StringBuilder()

            sb.Append(sr.ReadToEnd())

            sb.AppendLine("SistemaDeBodega (" & My.Application.Info.Version.Major & "." & _
                   My.Application.Info.Version.Minor & "." & _
                   My.Application.Info.Version.Revision & ")")
            '  sb.AppendLine("[" & My.Settings.SERVER & "][" & My.Settings.USER & "][" & My.Settings.LIBRARY & "]")

            sb.AppendLine(txt)
            sr.Close()

            Dim sw As StreamWriter = New StreamWriter(fEntrada)
            sw.WriteLine(sb.ToString())
            sw.Close()

            Return "OK"
        Catch E As Exception
            Return E.Message
        End Try

    End Function

    Public Function crtLog(ByVal txt As String, ByRef iNumLog As String) As String
        Dim fEntrada As String = ""
        Dim num As String = Right("000" & iNumLog, 3)

        Dim appPath As String = System.Web.HttpContext.Current.Request.ApplicationPath
        Dim CarpetaWeb As String = HttpContext.Current.Request.MapPath(appPath)

        fEntrada = Replace(CarpetaWeb & "\wmslog" & num & ".txt", "\\", "\")
        Try
            ' Create an instance of StreamReader to read from a file.
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(fEntrada)
            sb.AppendLine("LOG " & txt)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            Return "OK"
        Catch E As Exception
            Return E.Message
        End Try

    End Function


End Module
