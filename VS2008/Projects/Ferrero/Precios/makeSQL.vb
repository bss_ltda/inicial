﻿Imports IBM.Data.DB2.iSeries
Public Class mk_Sql

    Dim fSql As String
    Dim vSql As String
    Dim msgErr As String

    Public Sub mkSql(ByVal tipoF As Integer, ByVal Texto As String)
        Select Case tipoF
            Case t_iF
                fSql = Texto
            Case t_iV
                vSql = Texto
            Case tFil
                vSql &= Texto
        End Select
    End Sub

    Public Sub mkSql(ByVal tipoF As Integer, ByVal Campo As String, ByVal Valor As Object, Optional ByVal fin As Integer = 0)
        Dim sep As String = ""

        If Left(Trim(UCase(fSql)), 6) = "INSERT" Then
            sep = IIf(tipoF = tStr, "'", "")
            fSql = fSql & Campo
            Select Case fin
                Case 0
                    fSql = fSql & " , "
                Case 10, 11, -1, 1
                    fSql = fSql & " ) "
            End Select
            If IsDBNull(Valor) Then
                Valor = IIf(tipoF = tStr, "", "0")
            End If
            vSql = vSql & sep & CStr(Valor) & sep
            Select Case fin
                Case 10
                    vSql = vSql & "  "
                Case 11, -1, 1
                    vSql = vSql & " )"
                Case 0
                    vSql = vSql & " , "
            End Select
        Else
            sep = IIf(tipoF = tStr, "'", "")
            If IsDBNull(Valor) Then
                Valor = IIf(tipoF = tStr, "", "0")
            End If
            fSql = fSql & Campo & " = " & sep & CStr(Valor) & sep & IIf(fin = 10 Or fin = 11, " ", " , ")
        End If

    End Sub

    Public Sub inifSql(ByVal txt As String)
        fSql = txt
    End Sub

    Public Sub inivSql(ByVal txt As String)
        vSql = txt
    End Sub

    Public Function Insert(ByRef DB As ExecuteSQL) As Integer
        Dim r As Integer

        r = DB.ExecuteSQL(fSql & vSql)
        msgErr = ""
        If r = -99 Then
            msgErr = Descripcion_Error
        End If
        Return r


    End Function

    Public Function ShowError() As String
        Return msgErr
    End Function


  

    Public Function stm(ByVal txt As String) As String

        If txt = "?" Then
            Return fSql
        ElseIf txt = "" Then
            fSql = ""
        Else
            fSql &= txt
        End If
        Return ""

    End Function

    Public Function stm() As String
        Return fSql
    End Function


    Public Function stmV(ByVal txt As String) As String

        If txt = "?" Then
            Return vSql
        ElseIf txt = "" Then
            vSql = ""
        Else
            vSql &= txt
        End If
        Return ""

    End Function

    Public Function Sentencia() As String
        Return fSql & vbCrLf & vSql
    End Function

End Class
