﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.IO
Imports System.Xml

' Para permitir que se llame a este servicio Web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente.
' <System.Web.Script.Services.ScriptService()> _
<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Service1
    Inherits System.Web.Services.WebService

    Structure CabeceraPedido
        Dim NumeroDocumento As String
        Dim OrdenDeCompra As String
        Dim Cliente As String
        Dim PuntoDeEnvio As String
        Dim FechaRequerido As String
        Dim Notas As String
    End Structure

    Structure DetallePedido
        Dim LineaNumero As Integer
        Dim Producto As String
        Dim Cantidad As Double
        Dim Precio As Double
        Dim UnidadDeMedida As String
        Dim NotaLinea As String
    End Structure

    Dim LineaP As String


    <WebMethod()> _
    Public Function IngresaPedido(ByVal Pedido As String) As XmlDocument
        Dim resultado As String = ""
        Dim stream As System.IO.StringReader
        stream = New StringReader(Pedido)
        Dim datosPedido As XmlTextReader = New XmlTextReader(stream)

        Dim Lineas As New List(Of DetallePedido)
        Dim nuevaLinea As DetallePedido
        Dim nombreN, valorN As String
        Dim Cabecera As New CabeceraPedido
        Dim i As Integer

        Do While (datosPedido.Read())
            If datosPedido.NodeType = XmlNodeType.Element Then
                If Not datosPedido.HasAttributes Then 'If attributes exist
                    nombreN = datosPedido.Name
                    If nombreN <> "Linea" Then
                        datosPedido.Read()
                        valorN = datosPedido.Value
                        With Cabecera
                            Select Case nombreN
                                Case "NumeroDocumento"
                                    .NumeroDocumento = valorN
                                Case "OrdenDeCompra"
                                    .OrdenDeCompra = valorN
                                Case "Cliente"
                                    .Cliente = valorN
                                Case "PuntoDeEnvio"
                                    .PuntoDeEnvio = valorN
                                Case "FechaRequerido"
                                    .FechaRequerido = valorN
                                Case "Notas"
                                    .Notas = valorN
                            End Select
                        End With
                    Else
                        datosPedido.Read()
                        nuevaLinea = New DetallePedido
                        Do While (datosPedido.Name <> "Line")
                            LineaP = datosPedido.Name
                            datosPedido.Read()
                            With nuevaLinea
                                Select Case LineaP
                                    Case "LineaNumero"
                                        .LineaNumero = datosPedido.Value
                                    Case "Producto"
                                        .Producto = datosPedido.Value
                                    Case "Cantidad"
                                        .Cantidad = datosPedido.Value
                                    Case "Precio"
                                        .Precio = datosPedido.Value
                                    Case "UnidadDeMedida"
                                        .UnidadDeMedida = datosPedido.Value
                                    Case "NotaLinea"
                                        .NotaLinea = datosPedido.Value
                                End Select
                            End With
                        Loop
                        Lineas.Add(nuevaLinea)
                    End If
                End If
            End If
        Loop

        For i = 0 To Lineas.Count - 1
            'detalle
        Next

        Return DevuelveResultado(Cabecera.NumeroDocumento)

    End Function

    Function DevuelveResultado(NumeroDocumento As String) As XmlDocument
        Dim s As String
        s = "<Envelope> "
        s &= "  <Pedido identifier=""" & NumeroDocumento & """>Create "
        s &= "    <Mensaje> "
        s &= "        <NumeroDePedido>908615</NumeroDePedido> "
        s &= "    </Mensaje> "
        s &= "  </Pedido> "
        s &= "</Envelope> "

        s = " <Envelope> "
        s &= "   <Pedido identifier=""" & NumeroDocumento & """>Create "
        s &= "      <Mensaje> "
        s &= "         <Error>UME4497 "
        s &= "            <MensajeId>UME4497</MensajeId> "
        s &= "            <MensajeId>Punto de envio no es valido.</MensajeId> "
        s &= "         </Error> "
        s &= "      </Mensaje> "
        s &= "   </Pedido> "
        s &= " </Envelope> "

        s = "<Envelope> "
        s &= " <Valorizacion identifier=""" & NumeroDocumento & """>Valorizar "
        s &= "   <NumeroDocumento>0001</NumeroDocumento> "
        s &= "   <Line> "
        s &= " 	  <LineaNumero>1</LineaNumero> "
        s &= "      <Precio>1456.55</Precio> "
        s &= "   </Line> "
        s &= "   <Line> "
        s &= " 	  <LineaNumero>2</LineaNumero> "
        s &= "      <Precio>12342.22</Precio> "
        s &= "   </Line> "
        s &= " </Valorizacion> "
        s &= " </Envelope> "

        Dim pDoc As New XmlDocument
        pDoc.LoadXml(s)
        Return pDoc

    End Function

End Class