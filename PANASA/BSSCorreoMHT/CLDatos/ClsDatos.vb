﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Text

Public Class ClsDatos
    Public Cadena As String
    Public ReadOnly conexion As SqlConnection
    Private adapter As SqlDataAdapter = New SqlDataAdapter()
    Private trans As SqlTransaction
    Private builder As SqlCommandBuilder
    Private command As SqlCommand
    Private WithEvents ds As DataSet
    Public fSql As String
    Private numFilas As Integer
    Private continuarguardando As Boolean
    Private dt As DataTable
    Public appPath As String = Path.GetFullPath(My.Application.Info.DirectoryPath & "\Log\")
    Private data
    Public mensaje As String
    Public gsKeyWords As String
    Public local As String = IIf(My.Settings.LOCAL.ToUpper = "SI", "CCNOT1", "")

    Sub New()
        If My.Settings.PRUEBAS = "SI" Then
            Cadena = My.Settings.BASEDATOSLOCAL
        Else
            Cadena = My.Settings.BASEDATOS
        End If
        Try
            conexion = New SqlConnection(Cadena)
            ' Si el directorio no existe, crearlo
            Console.WriteLine("Creando directorio..." & appPath)
            If Not Directory.Exists(appPath) Then
                Directory.CreateDirectory(appPath)
            End If
        Catch ex As Exception
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\CONEXIONError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo Establecer conexion." & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try
    End Sub

#Region "ACTUALIZAR"

    ''' <summary>
    ''' Actualiza la tabla RFLOG 
    ''' </summary>
    ''' <param name="datos">RFLOG</param>
    ''' <param name="tipo">0- SET ALERT = @ALERT WHERE ID = @ID</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 04/05/2016</remarks>
    Public Function ActualizarRFLOGRow(ByVal datos As RFLOG, ByVal tipo As Integer) As Boolean
        trans = conexion.BeginTransaction()
        Select Case tipo
            Case 0
                fSql = " UPDATE RFLOG SET ALERT = " & datos.ALERT
                fSql &= " WHERE ID = " & datos.ID
        End Select

        command = New SqlCommand
        command.Connection = conexion
        command.Transaction = trans
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As Data.SqlClient.SqlException
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Catch ex As Exception
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Finally
            If continuarguardando = True Then
                trans.Commit()
            Else
                data = Nothing
                data = New RFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .OPERACION = "Actualizar RFLOG"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarRFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla RFPARAM 
    ''' </summary>
    ''' <param name="datos">RFPARAM</param>
    ''' <param name="tipo">0- SET @Campo = @Valor WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = @CCCODE</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 04/05/2016</remarks>
    Public Function ActualizarRFPARAMRow(ByVal datos As RFPARAM, ByVal tipo As Integer) As Boolean
        trans = conexion.BeginTransaction()
        Select Case tipo
            Case 0
                If datos.Campo = "" Then
                    datos.Campo = "CCDESC"
                End If
                fSql = "UPDATE RFPARAM SET  " & datos.Campo & " = " & CDbl(datos.Valor)
                fSql &= " WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER('" & datos.CCCODE & "')"
        End Select

        command = New SqlCommand
        command.Connection = conexion
        command.Transaction = trans
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As Data.SqlClient.SqlException
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFPARAMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla RFPARAM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Catch ex As Exception
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFPARAMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla RFPARAM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Finally
            If continuarguardando = True Then
                trans.Commit()
            Else
                data = Nothing
                data = New RFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .OPERACION = "Actualizar RFPARAM"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarRFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla RMAILX 
    ''' </summary>
    ''' <param name="datos">RMAILX</param>
    ''' <param name="tipo">0- SET LUPDDAT = NOW(), LENVIADO = @LENVIADO, LMSGERR = @LMSGERR WHERE LID = @LID</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 04/05/2016</remarks>
    Public Function ActualizarRMAILXRow(ByVal datos As RMAILX, ByVal tipo As Integer) As Boolean
        trans = conexion.BeginTransaction()
        Select Case tipo
            Case 0
                fSql = " UPDATE RMAILX SET "
                fSql &= " LUPDDAT = GetDate(), "
                fSql &= " LENVIADO = " & datos.LENVIADO & ", "
                fSql &= " LMSGERR = '" & datos.LMSGERR & "' "
                fSql &= " WHERE LID = " & datos.LID
        End Select

        command = New SqlCommand
        command.Connection = conexion
        command.Transaction = trans
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As Data.SqlClient.SqlException
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RMAILXError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla RMAILX.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Catch ex As Exception
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RMAILXError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla RMAILX.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Finally
            If continuarguardando = True Then
                trans.Commit()
            Else
                data = Nothing
                data = New RFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .OPERACION = "Actualizar RMAILX"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarRFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

#End Region

#Region "CONSULTAR"

    ''' <summary>
    ''' Consulta la tabla RCAU con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por UUSR devuelve UNOM y (0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 04/05/2016</remarks>
    Public Function ConsultarRCAUdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        trans = conexion.BeginTransaction()

        Select Case tipoparametro
            Case 0
                fSql = "SELECT UNOM, " & parametroBusqueda(0) & " AS UEMAIL FROM RCAU WHERE UUSR = '" & parametroBusqueda(1) & "'"
        End Select
        adapter = New SqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adapter.SelectCommand.Transaction = trans
        builder = New SqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        trans.Commit()
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla RFLOG con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por ALERT=1 devuelve COUNT,
    ''' 1- Busca por ALERT=1</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 04/05/2016</remarks>
    Public Function ConsultarRFLOGdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        trans = conexion.BeginTransaction()

        Select Case tipoparametro
            Case 0
                fSql = " SELECT COUNT(*) AS TOT FROM RFLOG WHERE ALERT = 1"
            Case 1
                fSql = " SELECT * FROM  RFLOG WHERE ALERT = 1"
        End Select
        adapter = New SqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adapter.SelectCommand.Transaction = trans
        builder = New SqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        trans.Commit()
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla RFPARAM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por CCCODE(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 04/05/2016</remarks>
    Public Function ConsultarRFPARAMdt2(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        trans = conexion.BeginTransaction()

        Select Case tipoparametro
            Case 0
                fSql = "SELECT CCDESC, CCSDSC FROM RFPARAM WHERE CCTABL='CORREOMHT' AND UPPER(CCCODE) = UPPER('" & parametroBusqueda(0) & "') AND CCUDC1 = 1"
        End Select
        adapter = New SqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adapter.SelectCommand.Transaction = trans
        builder = New SqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        trans.Commit()
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla RFPARAM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por CCCODE(1) devuelve (0)</param>
    ''' <returns>Un String</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 04/05/2016</remarks>
    Public Function ConsultarRFPARAMdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As String
        dt = Nothing
        trans = conexion.BeginTransaction()
        Console.WriteLine(conexion.ConnectionString)
        Select Case tipoparametro
            Case 0
                If parametroBusqueda(0) = "" Then
                    parametroBusqueda(0) = "CCDESC"
                End If
                fSql = "SELECT " & parametroBusqueda(0) & " AS DATO FROM RFPARAM WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER('" & parametroBusqueda(1) & "')"
                Console.WriteLine(fSql)
        End Select
        adapter = New SqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adapter.SelectCommand.Transaction = trans
        builder = New SqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        Try
            numFilas = adapter.Fill(ds)
            gsKeyWords = "[ok]"
        Catch ex As Exception
            gsKeyWords = ex.ToString
            Console.WriteLine(ex.ToString)
        End Try

        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        trans.Commit()
        Return dt.Rows(0).Item(0)
    End Function

    ''' <summary>
    ''' Consulta la tabla RMAILX con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por LENVIADO = 0 retorna COUNT,
    ''' 1- Busca por LENVIADO = 0</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 04/05/2016</remarks>
    Public Function ConsultarRMAILXdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        trans = conexion.BeginTransaction()

        Select Case tipoparametro
            Case 0
                fSql = " SELECT COUNT(*) AS TOT FROM  RMAILX WHERE LENVIADO = 0"
            Case 1
                fSql = " SELECT * FROM  RMAILX WHERE LENVIADO = 0"
        End Select
        adapter = New SqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adapter.SelectCommand.Transaction = trans
        builder = New SqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        trans.Commit()
        Return dt
    End Function

#End Region

#Region "FUNCIONES"

    Public Function AbrirConexion() As Boolean
        Dim dat As New ClsDatosAS400
        Dim abierta As Boolean = False
        Try
            If My.Settings.LOCAL = "SI" Then
                If conexion.State = ConnectionState.Open Then
                    abierta = True
                Else
                    conexion.Open()
                    abierta = True
                End If
            Else
                abierta = dat.AbrirConexion()
            End If
            Console.WriteLine("Conexión " & abierta)
        Catch ex1 As SqlException
            abierta = False
            mensaje = ex1.ToString
        Catch ex As Exception
            abierta = False
            mensaje = ex.ToString
        End Try
        Return abierta
    End Function

    Public Function CerrarConexion() As Boolean
        Dim dat As New ClsDatosAS400
        Dim cerrada As Boolean = False
        Try
            If My.Settings.LOCAL = "SI" Then
                conexion.Close()
                cerrada = True
            Else
                cerrada = dat.CerrarConexion
            End If
        Catch ex1 As SqlException
            cerrada = False
            mensaje = ex1.ToString
        Catch ex As Exception
            cerrada = False
            mensaje = ex.ToString
        End Try
        Return cerrada
    End Function

    Sub WrtSqlError(sql As String, ByVal Descrip As String)
        Dim continuar As Boolean
        Dim application As String

        If gsKeyWords.Trim <> "" Then
            Descrip = gsKeyWords & "<br>" & Descrip
        End If
        Descrip = Left(Descrip, 20000)

        Try
            application = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
            data = Nothing
            data = New RFLOG
            With data
                .USUARIO = System.Net.Dns.GetHostName()
                .PROGRAMA = application
                .ALERT = "1"
                .EVENTO = Replace(Descrip, "'", "''")
                .TXTSQL = Replace(sql, "'", "''")
            End With
            continuar = GuardarRFLOGRow(data)
            If continuar = False Then
                'ERROR DE GUARDADO
            End If
        Catch ex As Exception

        End Try
    End Sub

#End Region

#Region "GUARDAR"

    ''' <summary>
    ''' Guarda RFLOG
    ''' </summary>
    ''' <param name="datos">RFLOG</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 04/05/2016</remarks>
    Public Function GuardarRFLOGRow(ByVal datos As RFLOG) As Boolean
        Dim continuarguardandoRFLOG As Boolean

        trans = conexion.BeginTransaction()

        fSql = "INSERT INTO RFLOG (USUARIO, OPERACION, PROGRAMA, EVENTO, TXTSQL, ALERT)"
        fSql &= " VALUES ('" & datos.USUARIO & "', '" & datos.OPERACION & "', '" & datos.PROGRAMA & "', '" & datos.EVENTO.Replace("'", "''") & "', '" & datos.TXTSQL.Replace("'", "''") & "', " & datos.ALERT & ")"

        command = New SqlCommand
        command.Connection = conexion
        command.Transaction = trans
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardandoRFLOG = True
        Catch ex As Data.SqlClient.SqlException
            trans.Rollback()
            continuarguardandoRFLOG = False
            mensaje = ex.message
        Catch ex As Exception
            trans.Rollback()
            continuarguardandoRFLOG = False
            mensaje = ex.Message
        Finally
            If continuarguardandoRFLOG = True Then
                trans.Commit()
            Else
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End If
        End Try
        Return continuarguardandoRFLOG
    End Function

#End Region

End Class

Public Class RFLOG
    Public OPERACION As String
    Public PROGRAMA As String
    Public EVENTO As String
    Public TXTSQL As String
    Public ALERT As String
    Public ID As String
    Public USUARIO As String
End Class

Public Class RMAILX
    Public LID As String
    Public LMSGERR As String
    Public LENVIADO As String
    Public LUPDDAT As String
End Class

Public Class RFPARAM
    Public CCCODE As String
    Public Campo As String
    Public Valor As String
End Class
