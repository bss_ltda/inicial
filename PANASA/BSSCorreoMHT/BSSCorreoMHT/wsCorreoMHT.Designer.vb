﻿Imports System.ServiceProcess
Imports CLDatos

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class wsCorreoMHT
    Inherits ServiceBase

    'UserService reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de componentes
    Private components As System.ComponentModel.IContainer

    ' NOTA: el Diseñador de componentes requiere el siguiente procedimiento
    ' Se puede modificar usando el Diseñador de componentes. No lo modifique
    ' usando el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.EventLog1 = New System.Diagnostics.EventLog()
        CType(Me.EventLog1, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'EventLog1
        '
        Me.EventLog1.Log = "Application"
        Me.EventLog1.Source = "BSSCorreoMHT"
        '
        'wsCorreoMHT
        '
        Me.ServiceName = "wsCorreoMHT"
        CType(Me.EventLog1, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents EventLog1 As System.Diagnostics.EventLog

    ' Punto de entrada principal del proceso
    <MTAThread()> _
    <System.Diagnostics.DebuggerNonUserCode()> _
    Shared Sub Main()
        If (Environment.UserInteractive) Then
            Dim correo As New Correo
            Dim as400correo As New CorreoAS400
            'Dim servicio As wsCorreoMHT = New wsCorreoMHT()
            If My.Settings.AS400CORREO = "SI" Then
                as400correo.Inicializador()
            Else
                correo.Inicializador()
            End If
            'servicio.revisaTareasMHT()
            Console.WriteLine("Inicia Servicio Consola")
        Else
            Dim ServicesToRun() As System.ServiceProcess.ServiceBase
            ServicesToRun = New System.ServiceProcess.ServiceBase() {New wsCorreoMHT}
            System.ServiceProcess.ServiceBase.Run(ServicesToRun)
        End If
    End Sub

End Class
