﻿Imports IBM.Data.DB2.iSeries
Imports System.IO
Imports System.Text

Public Class ClsDatosAS400
    Public conexionIBM As iDB2Connection
    Private adapterIBM As iDB2DataAdapter = New iDB2DataAdapter()
    Private builderIBM As iDB2CommandBuilder
    Private commandIBM As iDB2Command
    Private numFilas As Integer
    Private dt As DataTable
    Private ds As DataSet
    Public appPath As String = Path.GetFullPath(My.Application.Info.DirectoryPath & "\Log\")
    Public mensaje As String
    Public fSql As String
    Private data

#Region "ACTUALIZAR"

    ''' <summary>
    ''' Actualiza la tabla GLH 
    ''' </summary>
    ''' <param name="datos">GLH</param>
    ''' <param name="tipo">0- SET LHJRF1 = @LHJRF1, LHJRF2 = @LHJRF2 WHERE LHLDGR = @LHLDGR AND LHBOOK = @LHBOOK AND LHYEAR = @LHYEAR AND LHPERD = @LHPERD AND LHJNEN = @LHJNEN AND LHJNLN = @LHJNLN</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 02/08/2016</remarks>
    Public Function ActualizarGLHRow(ByVal datos As GLH, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean = False

        Select Case tipo
            Case 0
                fSql = " UPDATE GLH SET  "
                fSql &= " LHJRF1 = '" & datos.LHJRF1 & "', "
                fSql &= " LHJRF2 = '" & datos.LHJRF2 & "' "
                fSql &= " WHERE LHLDGR = '" & datos.LHLDGR & "' "
                fSql &= " AND LHBOOK = '" & datos.LHBOOK & "' "
                fSql &= " AND LHYEAR = " & datos.LHYEAR
                fSql &= " AND LHPERD = " & datos.LHPERD
                fSql &= " AND LHJNEN = '" & datos.LHJNEN & "' "
                fSql &= " AND LHJNLN = " & datos.LHJNLN
        End Select

        commandIBM = New iDB2Command
        If Not conexionIBM.State = ConnectionState.Open Then
            data = Nothing
            data = New RFLOG
            With data
                .USUARIO = Net.Dns.GetHostName()
                .OPERACION = "Actualizar GLH"
                .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                .EVENTO = "Conexion Cerrada"
                .TXTSQL = fSql
                .ALERT = 1
                .LKEY = " LHLDGR=" & datos.LHLDGR & " LHBOOK=" & datos.LHBOOK
            End With
            GuardarRFLOGRow(data)
            Return continuarguardando
        End If
        commandIBM.Connection = conexionIBM

        Try
            commandIBM.CommandText = fSql
            commandIBM.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As iDB2Exception
            continuarguardando = False
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\GLHError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo actualizar la tabla GLH.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()

                data = Nothing
                data = New RFLOG
                With data
                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Actualizar GLH"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                    .LKEY = " LHLDGR=" & datos.LHLDGR & " LHBOOK=" & datos.LHBOOK
                End With
                GuardarRFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

#End Region

#Region "CONSULTAR"

    ''' <summary>
    ''' Consulta la tabla AVM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca donde VENDOR(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 02/08/2016</remarks>
    Public Function ConsultarAVMdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing
        If conexionIBM IsNot Nothing Then
            Select Case tipoparametro
                Case 0
                    fSql = "SELECT VENDOR FROM AVM WHERE VENDOR = " & parametroBusqueda(0)
            End Select
            adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)
            adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
            builderIBM = New iDB2CommandBuilder(adapterIBM)
            dt = New DataTable()
            ds = New DataSet()
            Try
                numFilas = adapterIBM.Fill(ds)
                If numFilas > 0 Then
                    dt = ds.Tables(0)
                Else
                    dt = Nothing
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\AVMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo consultar en la tabla AVM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            adapterIBM = Nothing
            builderIBM = Nothing
        Else
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\AVMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No existe conexión" & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End If
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla BSSFFNPAR con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca donde PTIPCOM(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 02/08/2016</remarks>
    Public Function ConsultarBSSFFNPARdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing
        If conexionIBM IsNot Nothing Then
            Select Case tipoparametro
                Case 0
                    fSql = "SELECT PREF1, PREF2 FROM BSSFFNPAR WHERE PTIPCOM = '" & parametroBusqueda(0) & "'"
            End Select
            adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)
            adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
            builderIBM = New iDB2CommandBuilder(adapterIBM)
            dt = New DataTable()
            ds = New DataSet()
            Try
                numFilas = adapterIBM.Fill(ds)
                If numFilas > 0 Then
                    dt = ds.Tables(0)
                Else
                    dt = Nothing
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BSSFFNPARError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo consultar en la tabla BSSFFNPAR.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            adapterIBM = Nothing
            builderIBM = Nothing
        Else
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BSSFFNPARError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No existe conexión" & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End If
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla RCM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca donde CCUST(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 02/08/2016</remarks>
    Public Function ConsultarRCMdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing
        If conexionIBM IsNot Nothing Then
            Select Case tipoparametro
                Case 0
                    fSql = "SELECT CCUST FROM RCM WHERE CCUST = " & parametroBusqueda(0)
            End Select
            adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)
            adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
            builderIBM = New iDB2CommandBuilder(adapterIBM)
            dt = New DataTable()
            ds = New DataSet()
            Try
                numFilas = adapterIBM.Fill(ds)
                If numFilas > 0 Then
                    dt = ds.Tables(0)
                Else
                    dt = Nothing
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\RCMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo consultar en la tabla RCM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            adapterIBM = Nothing
            builderIBM = Nothing
        Else
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RCMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No existe conexión" & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End If
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla ZRC con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca donde RCRTCD(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 02/08/2016</remarks>
    Public Function ConsultarZRCdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing
        If conexionIBM IsNot Nothing Then
            Select Case tipoparametro
                Case 0
                    fSql = "SELECT RCRTCD FROM ZRC WHERE RCRTCD = '" & parametroBusqueda(0) & "'"
            End Select
            adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)
            adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
            builderIBM = New iDB2CommandBuilder(adapterIBM)
            dt = New DataTable()
            ds = New DataSet()
            Try
                numFilas = adapterIBM.Fill(ds)
                If numFilas > 0 Then
                    dt = ds.Tables(0)
                Else
                    dt = Nothing
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\ZRCError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo consultar en la tabla ZRC.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            adapterIBM = Nothing
            builderIBM = Nothing
        Else
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ZRCError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No existe conexión" & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End If
        Return dt
    End Function

#End Region

#Region "FUNCIONES"

    Public Function AbrirConexion() As Boolean
        Dim abierta As Boolean = False

        Try
            Console.WriteLine("Creando Directorio")
            If Not Directory.Exists(appPath) Then
                Directory.CreateDirectory(appPath)
            End If
            Console.WriteLine("Validando Conexion abierta")
            If conexionIBM IsNot Nothing Then
                If conexionIBM.State = ConnectionState.Open Then
                    abierta = True
                    Console.WriteLine("Conexion Existente Abierta")
                Else
                    Console.WriteLine("Abriendo Conexion")
                    conexionIBM = New iDB2Connection
                    conexionIBM.ConnectionString = My.Settings.BASEDATOS
                    conexionIBM.Open()
                    Console.WriteLine("Conexion Abierta")
                    abierta = True
                End If
            Else
                Console.WriteLine("Abriendo Conexion")
                conexionIBM = New iDB2Connection
                conexionIBM.ConnectionString = My.Settings.BASEDATOS
                conexionIBM.Open()
                Console.WriteLine("Conexion Abierta")
                abierta = True
            End If

        Catch ex1 As iDB2Exception
            abierta = False
            mensaje = ex1.Message
        Catch ex As Exception
            abierta = False
            mensaje = ex.Message
        End Try
        If abierta = False Then
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionAS400Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine(mensaje)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End If
        Return abierta
    End Function

    Public Function Conexion(ByVal DataSource As String, ByVal UserID As String, ByVal Password As String, ByVal DefaultCollection As String) As Boolean
        Dim abierta As Boolean = False
        Dim cad As String = "DataSource={DataSource};Password={Password};User ID={UserID};LibraryList={DefaultCollection};SchemaSearchList={DefaultCollection};Naming=System;CharBitDataCcsid=1;CharBitDataAsString=true"
        If DataSource <> "" And UserID <> "" And Password <> "" And DefaultCollection <> "" Then
            cad.Replace("{DataSource}", DataSource)
            cad.Replace("{Password}", Password)
            cad.Replace("{UserID}", UserID)
            cad.Replace("{DefaultCollection}", DefaultCollection)
        End If
        Try
            Console.WriteLine("Creando Directorio")
            If Not Directory.Exists(appPath) Then
                Directory.CreateDirectory(appPath)
            End If
            Console.WriteLine("Validando Conexion abierta")
            If conexionIBM IsNot Nothing Then
                If conexionIBM.State = ConnectionState.Open Then
                    abierta = True
                    Console.WriteLine("Conexion Existente Abierta")
                Else
                    Console.WriteLine("Abriendo Conexion")
                    conexionIBM = New iDB2Connection
                    conexionIBM.ConnectionString = cad
                    conexionIBM.Open()
                    Console.WriteLine("Conexion Abierta")
                    abierta = True
                End If
            Else
                Console.WriteLine("Abriendo Conexion")
                conexionIBM = New iDB2Connection
                conexionIBM.ConnectionString = cad
                conexionIBM.Open()
                Console.WriteLine("Conexion Abierta")
                abierta = True
            End If

        Catch ex1 As iDB2Exception
            abierta = False
            mensaje = ex1.Message
        Catch ex As Exception
            abierta = False
            mensaje = ex.Message
        End Try
        If abierta = False Then
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionAS400Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine(mensaje)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End If
        Return abierta
    End Function

    Public Function CerrarConexion() As Boolean
        Dim cerrada As Boolean = False
        Try
            conexionIBM.Close()
            cerrada = True
        Catch ex1 As iDB2Exception
            cerrada = False
            mensaje = ex1.Message
        Catch ex As Exception
            cerrada = False
            mensaje = ex.Message
        End Try
        Return cerrada
    End Function

#End Region

#Region "GUARDAR"

    ''' <summary>
    ''' Guarda RFLOG
    ''' </summary>
    ''' <param name="datos">RFLOG</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 02/08/2016</remarks>
    Public Function GuardarRFLOGRow(ByVal datos As RFLOG) As Boolean
        Dim continuarguardandoRFLOG As Boolean

        fSql = "INSERT INTO RFLOG (USUARIO, OPERACION, PROGRAMA, EVENTO, TXTSQL, ALERT)"
        fSql &= " VALUES ('" & datos.USUARIO & "', '" & datos.OPERACION & "', '" & datos.PROGRAMA & "', '" & datos.EVENTO.Replace("'", "''") & "', '" & datos.TXTSQL.Replace("'", "''") & "', " & datos.ALERT & ")"

        commandIBM = New iDB2Command
        commandIBM.Connection = conexionIBM
        Try
            commandIBM.CommandText = fSql
            commandIBM.ExecuteNonQuery()
            continuarguardandoRFLOG = True
        Catch ex As Data.SqlClient.SqlException
            continuarguardandoRFLOG = False
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardandoRFLOG = False
            mensaje = ex.Message
        Finally
            If continuarguardandoRFLOG = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End If
        End Try
        Return continuarguardandoRFLOG
    End Function

#End Region

End Class

Public Class RFLOG
    Public USUARIO As String
    Public OPERACION As String
    Public PROGRAMA As String
    Public EVENTO As String
    Public TXTSQL As String
    Public ALERT As String
End Class

Public Class GLH
    Public LHJRF1 As String
    Public LHJRF2 As String
    Public LHLDGR As String
    Public LHBOOK As String
    Public LHYEAR As String
    Public LHPERD As String
    Public LHJNEN As String
    Public LHJNLN As String
End Class
