﻿Imports CLDatos

' NOTA: puede usar el comando "Cambiar nombre" del menú contextual para cambiar el nombre de clase "ServicioDatos" en el código, en svc y en el archivo de configuración a la vez.
' NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione ServicioDatos.svc o ServicioDatos.svc.vb en el Explorador de soluciones e inicie la depuración.
Public Class ServicioDatos
    Implements IServicioDatos
    Private datos As New ClsDatosAS400
    Private data

    Public Function NumComprobantesGLH(ByVal xml As String) As Respuesta Implements IServicioDatos.NumComprobantesGLH
        Dim docEntrada As XDocument = XDocument.Parse(xml)
        Dim msgLin As String = ""
        Dim Resultado As New Respuesta
        Dim linResultado As New lineaRespuesta
        Dim GLH As New ActualizaGLH
        Dim lineaGLH As lineaActualizaGLH
        Dim i As Integer = 1
        Dim iOK As Integer = 0
        Dim iBAD As Integer = 0
        Dim parametro(0) As String
        Dim dtBSSFFNPAR As DataTable
        Dim dtRCM As DataTable
        Dim dtAVM As DataTable
        Dim dtZRC As DataTable
        Dim continuar As Boolean

        Dim qx = From xe In docEntrada.Elements("GLH") Select New With { _
            .DataSource = xe.Element("DataSource").Value,
            .UserID = xe.Element("UserID").Value,
            .Password = xe.Element("Password").Value,
            .DefaultCollection = xe.Element("DefaultCollection").Value
        }

        Resultado.lineaRespuesta = New List(Of lineaRespuesta)

        With GLH
            .DataSource = qx.First.DataSource
            .UserID = qx.First.UserID
            .Password = qx.First.Password
            .DefaultCollection = qx.First.DefaultCollection
            Try
                If Not datos.Conexion(.DataSource, .UserID, .Password, .DefaultCollection) Then
                    linResultado.linea = 0
                    linResultado.Descripcion = "No se pudo conectar."
                    Resultado.lineaRespuesta.Add(linResultado)
                    Return Resultado
                End If
            Catch ex As Exception
                linResultado.linea = 0
                linResultado.Descripcion = ex.ToString
                Resultado.lineaRespuesta.Add(linResultado)
                Return Resultado
            End Try
        End With

        GLH.lineasGLH = New List(Of lineaActualizaGLH)

        Dim qd = From xe In docEntrada.Descendants.Elements("Linea") Select New With { _
            .Ledger = xe.Element("LHLDGR").Value,
            .Book = xe.Element("LHBOOK").Value,
            .Year = xe.Element("LHYEAR").Value,
            .Period = xe.Element("LHPERD").Value,
            .JournalEntryNumber = xe.Element("LHJNEN").Value,
            .JournalEntryLine = xe.Element("LHJNLN").Value,
            .Reference1 = xe.Element("LHJRF1").Value,
            .Reference2 = xe.Element("LHJRF2").Value
        }

        For Each e In qd
            lineaGLH = New lineaActualizaGLH
            With lineaGLH
                .Ledger = e.Ledger
                .Book = e.Book
                .Year = e.Year
                .Period = e.Period
                .JournalEntryNumber = e.JournalEntryNumber
                .JournalEntryLine = e.JournalEntryLine
                .Reference1 = e.Reference1
                .Reference2 = e.Reference2
            End With
            GLH.lineasGLH.Add(lineaGLH)
        Next

        Try
            For Each e In GLH.lineasGLH
                With e
                    linResultado.linea = i
                    linResultado.Descripcion = ""
                    parametro(0) = Left(.JournalEntryNumber, 2)
                    dtBSSFFNPAR = datos.ConsultarBSSFFNPARdt(parametro, 0)
                    If dtBSSFFNPAR IsNot Nothing Then
                        Select Case dtBSSFFNPAR.Rows(0).Item("PREF1")
                            Case "PR"
                                If UCase(Left(.Reference1, 1)) = "C" Then
                                    If IsNumeric(.Reference1.Substring(2)) Then
                                        parametro(0) = .Reference1.Substring(2)
                                        dtRCM = datos.ConsultarRCMdt(parametro, 0)
                                        If dtRCM Is Nothing Then
                                            linResultado.Descripcion &= "No encontró Cliente. "
                                        End If
                                    Else
                                        linResultado.Descripcion &= "Error en codigo del cliente. "
                                    End If
                                ElseIf IsNumeric(.Reference1) Then
                                    parametro(0) = .Reference1
                                    dtAVM = datos.ConsultarAVMdt(parametro, 0)
                                    If dtAVM Is Nothing Then
                                        linResultado.Descripcion &= "No encontró Proveedor. "
                                    End If
                                Else
                                    linResultado.Descripcion &= "Error en codigo de Proveedor. "
                                End If
                            Case "CL"
                                If IsNumeric(.Reference1) Then
                                    parametro(0) = .Reference1
                                    dtRCM = datos.ConsultarRCMdt(parametro, 0)
                                    If dtRCM Is Nothing Then
                                        linResultado.Descripcion &= "No encontró Cliente. "
                                    End If
                                Else
                                    linResultado.Descripcion &= "Error en codigo del cliente. "
                                End If
                        End Select
                        If dtBSSFFNPAR.Rows(0).Item("PREF2") = "TA" Then
                            parametro(0) = .Reference2
                            dtZRC = datos.ConsultarZRCdt(parametro, 0)
                            If dtZRC Is Nothing Then
                                linResultado.Descripcion &= "No encontró Tasa. "
                            End If
                        End If
                    End If

                    If linResultado.Descripcion = "" Then
                        data = Nothing
                        data = New GLH
                        With data
                            .LHJRF1 = .Reference1
                            .LHJRF2 = .Reference2
                            .LHLDGR = .Ledger
                            .LHBOOK = .Book
                            .LHYEAR = .Year
                            .LHPERD = .Period
                            .LHJNEN = .JournalEntryNumber
                            .LHJNLN = .JournalEntryLine
                        End With
                        continuar = datos.ActualizarGLHRow(data, 0)
                        If continuar = False Then
                            linResultado.Descripcion = "NO ACTUALIZADO"
                            iBAD += 1
                        Else
                            linResultado.Descripcion = "OK"
                            iOK += 1
                        End If
                    End If
                    Resultado.lineaRespuesta.Add(linResultado)
                    i += 1
                End With
            Next
            linResultado.linea = 0
            linResultado.Descripcion = String.Format("Registros recibidos {1}{0}Registros Actualizados {2}{0}Registros no actualizados {3}{0}", vbCrLf, i - 1, iOK, iBAD)
            Resultado.lineaRespuesta.Add(linResultado)
        Catch ex As Exception
            linResultado.linea = 0
            linResultado.Descripcion = ex.ToString
            Resultado.lineaRespuesta.Add(linResultado)
            Return Resultado
        End Try

        datos.CerrarConexion()
        Return Resultado
    End Function

End Class
