﻿Imports System.ServiceModel

' NOTA: puede usar el comando "Cambiar nombre" del menú contextual para cambiar el nombre de interfaz "IServicioDatos" en el código y en el archivo de configuración a la vez.
<ServiceContract()>
Public Interface IServicioDatos

    <OperationContract()>
    Function NumComprobantesGLH(ByVal xml As String) As Respuesta

End Interface

<DataContract()>
Public Class Respuesta
    <DataMember()>
    Public Property lineaRespuesta As List(Of lineaRespuesta)
End Class

<DataContract()>
Public Class lineaRespuesta
    <DataMember()>
    Public Property linea As Integer
    <DataMember()>
    Public Property Descripcion As String
End Class

<DataContract()>
Public Class ActualizaGLH
    <DataMember()>
    Public Property DataSource As String
    <DataMember()>
    Public Property UserID As String
    <DataMember()>
    Public Property Password As String
    <DataMember()>
    Public Property DefaultCollection As String
    <DataMember()>
    Public Property lineasGLH As List(Of lineaActualizaGLH)
End Class

<DataContract()>
Public Class lineaActualizaGLH
    <DataMember()>
    Public Property Ledger As String
    <DataMember()>
    Public Property Book As String
    <DataMember()>
    Public Property Year As Integer
    <DataMember()>
    Public Property Period As Integer
    <DataMember()>
    Public Property JournalEntryNumber As String
    <DataMember()>
    Public Property JournalEntryLine As Integer
    <DataMember()>
    Public Property Reference1 As String
    <DataMember()>
    Public Property Reference2 As String
End Class

