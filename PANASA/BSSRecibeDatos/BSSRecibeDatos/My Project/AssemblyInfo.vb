﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' La información general de un ensamblado se controla mediante el siguiente 
' conjunto de atributos. Cambie estos atributos para modificar la información
' asociada a un ensamblado.

' Revisar los valores de los atributos del ensamblado

<Assembly: AssemblyTitle("BSSRecibeDatos")> 
<Assembly: AssemblyDescription("Recibe datos para actualizar LX")> 
<Assembly: AssemblyCompany("BSS LTDA")> 
<Assembly: AssemblyProduct("BSSRecibeDatos")> 
<Assembly: AssemblyCopyright("Copyright © BSSLTDA  2016")> 
<Assembly: AssemblyTrademark("BSSLTDA WebService Updates")> 

<Assembly: ComVisible(False)>

'El siguiente GUID sirve como identificador de la biblioteca de tipos si este proyecto se expone a COM
<Assembly: Guid("874ccf8f-6554-43ca-b088-8c1f46cb184c")> 

' La información de versión de un ensamblado consta de los siguientes cuatro valores:
'
'      Versión principal
'      Versión secundaria 
'      Número de compilación
'      Revisión
'
' Puede especificar todos los valores o usar los valores predeterminados (número de versión de compilación y de revisión) 
' usando el símbolo '*' como se muestra a continuación:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
