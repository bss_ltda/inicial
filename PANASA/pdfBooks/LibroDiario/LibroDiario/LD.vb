﻿Imports CLDatos
Imports iTextSharp.text
Imports System.Text
Imports System.IO
Imports iTextSharp.text.pdf

Module LD
    Public datos As New ClsDatosAS400
    Private dtBSSFFNDIA As DataTable
    Public FechaImpresion
    Public Referencia As String
    Private data
    Private CARPETA_SITIO As String
    Private CARPETA_IMG As String
    Public Control As String
    Public Consecutivo As String
    Private tarea As String
    Private bPRUEBAS As Boolean
    Private PDF_Folder As String
    Private LOG_File As String
    Public LOGO As String
    Public NOMEMP As String
    Public NIT As String
    Private PGM400 As String
    Public AAAA As String
    Public PERIODO As String
    Private LIBRO As String
    'Private AMBIENTE As String
    Private ArchivoPDF As String
    Private document As Document
    Private PageCount As Integer

    Sub Main()
        Dim campoParam As String = ""
        Dim aSubParam() As String
        Dim continuar As Boolean
        Try
            Environment.GetCommandLineArgs()
            For Each parametro In Environment.GetCommandLineArgs()
                aSubParam = Split(parametro, "=")
                Select Case UCase(aSubParam(0))
                    Case "TAREA"
                        tarea = aSubParam(1)
                    Case "IDREPORTE"
                        Control = aSubParam(1)
                    Case "CONSECUTIVO"
                        Consecutivo = aSubParam(1)
                    Case "REFERENCIA"
                        Referencia = aSubParam(1)
                    Case "FECHA"
                        FechaImpresion = aSubParam(1)
                    Case "PGM400"
                        PGM400 = aSubParam(1)
                    Case "AAAA"
                        AAAA = aSubParam(1)
                    Case "PERIODO"
                        PERIODO = aSubParam(1)
                    Case "LIBRO"
                        LIBRO = aSubParam(1)
                        'Case "AMBIENTE"
                        '    AMBIENTE = aSubParam(1)
                End Select
            Next
            Console.WriteLine("Abriendo Conexion")
            If Not datos.AbrirConexion() Then
                Console.WriteLine("Error Abriendo Conexion")
                Return
            End If

            Try
                'LLAMA PROGRAMA AS400
                datos.LlamaPrograma(PGM400, AAAA & "', '" & PERIODO & "', '" & Control & "', '" & LIBRO)
            Catch ex As Exception
                data = Nothing
                data = New RFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .OPERACION = "Llamar Programa"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = ex.Message
                    .TXTSQL = datos.fSql
                    .ALERT = 1
                End With
                continuar = datos.GuardarRFLOGRow(data)
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\ProgramaError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo llamar al programa." & vbCrLf & ex.Message)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try

            If datos.Piloto = "SI" Then
                campoParam = "CCNOT2"
            End If
            CARPETA_SITIO = datos.ConsultarRFPARAMString("SITIO_CARPETA", campoParam)
            CARPETA_IMG = datos.ConsultarRFPARAMString("CARPETA_IMG", campoParam)
            LOGO = datos.ConsultarRFPARAMString("LOGO", campoParam)
            NOMEMP = datos.ConsultarRFPARAMString("NOMBRE", campoParam)
            NIT = datos.ConsultarRFPARAMString("NIT", campoParam)
            bPRUEBAS = datos.Piloto.ToUpper() = "SI"
            PDF_Folder = CARPETA_IMG & "pdf\Libros\"
            Try
                If Not Directory.Exists(PDF_Folder) Then
                    Directory.CreateDirectory(PDF_Folder)
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\DirectorioError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo crear Directorio." & vbCrLf & ex.Message)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            LOG_File = PDF_Folder & "LD" & Control & ".log"
            Balancee()
            datos.CerrarConexion()
            Return
        Catch ex As Exception
            datos.wrtLogHtml("", ex.Message.ToString)
            datos.CerrarConexion()
            Return
        End Try
    End Sub

    Sub Balancee()
        Dim continuar As Boolean
        Console.WriteLine("Generando documento...")
        data = Nothing
        data = New RFTASK
        With data
            .TLXMSG = "EJECUTANDO"
            .TMSG = "Generando Libro Diario"
            .TNUMREG = tarea
        End With
        If datos.Piloto = "NO" Then
            continuar = datos.ActualizarRFTASKRow(data, 0)
        Else
            continuar = True
        End If
        If continuar = True Then
            ArchivoPDF = PDF_Folder & "LibroDiario" & Control & ".pdf"
            pdf()

            If bPRUEBAS Then
                'EJECUTA
                Dim prc As Process = New System.Diagnostics.Process()
                prc.StartInfo.FileName = ArchivoPDF
                prc.Start()
            End If
        End If
    End Sub

    Sub pdf()
        Dim continuar As Boolean
        Dim parametro(1) As String

        document = New Document(New Rectangle(288.0F, 144.0F), 36, 36, 140, 40)
        document.SetPageSize(iTextSharp.text.PageSize.LETTER.Rotate())
        Console.WriteLine(document.PageSize.Width.ToString)
        document.AddTitle("Libro Diario - ")
        Dim es As eventos = New eventos()
        Dim pw As PdfWriter = PdfWriter.GetInstance(document, New FileStream(ArchivoPDF, FileMode.Create))
        pw.PageEvent = es
        document.Open()

        parametro(0) = Control
        dtBSSFFNDIA = datos.ConsultarBSSFFNDIAdt(parametro, 0)
        If dtBSSFFNDIA IsNot Nothing Then
            Dim unaTabla As PdfPTable = GenerarTabla2()
            document.Add(unaTabla)
            PageCount = pw.PageNumber
            data = Nothing
            data = New RFTASK
            With data
                .TLXMSG = "EJECUTADA"
                .TMSG = "Libro Diario Generado N° Paginas: " & PageCount
                .TURL = Replace(ArchivoPDF, CARPETA_SITIO, "\")
                .STS2 = "1"
                .TNUMREG = tarea
            End With
            If datos.Piloto = "NO" Then
                continuar = datos.ActualizarRFTASKRow(data, 2)
            End If
            If continuar = False Then
                'ERROR DE ACTUALIZADO
            End If
        Else
            datos.WrtTxtError(LOG_File, "Libro no se encontro.")
            data = Nothing
            data = New RFTASK
            With data
                .TLXMSG = "EJECUTADA"
                .TMSG = "Libro Diario no Existe"
                .STS2 = "1"
                .TNUMREG = tarea
            End With
            If datos.Piloto = "NO" Then
                continuar = datos.ActualizarRFTASKRow(data, 1)
            End If
            If continuar = False Then
                'ERROR DE ACTUALIZADO
            Else
                datos.WrtTxtError(LOG_File, "Libro sin datos")
            End If
        End If

        Dim tablafin As PdfPTable = fintabla(document)
        document.Add(tablafin)
        FinDocumento()

        document.Close()
    End Sub

    Public Function GenerarTabla() As PdfPTable
        Dim parametro(1) As String
        Dim flag As Integer = 0
        Dim continuar As Boolean
        Dim dtBSSFFNDIA2 As DataTable
        Dim dtBSSFFNDIA3 As DataTable

        Console.WriteLine("Generando una tabla...")
        Dim tablaPadre As New PdfPTable(1)
        tablaPadre.SetWidthPercentage(New Single() {615}, PageSize.LETTER)
        tablaPadre.HorizontalAlignment = Element.ALIGN_CENTER
        tablaPadre.DefaultCell.BorderWidth = 0.5

        Dim unaTabla As New PdfPTable(5)
        unaTabla.HorizontalAlignment = Element.ALIGN_CENTER
        unaTabla.DefaultCell.BorderWidth = 0

        unaTabla.SetWidthPercentage(New Single() {35, 50, 290, 120, 120}, PageSize.LETTER)
        'Headers
        parametro(0) = Control
        dtBSSFFNDIA = datos.ConsultarBSSFFNDIAdt(parametro, 0)
        If dtBSSFFNDIA IsNot Nothing Then
            Dim header1 As New PdfPCell(New Paragraph("COMP", FontFactory.GetFont("Arial", 8, Font.BOLD)))
            header1.PaddingBottom = 5
            header1.HorizontalAlignment = 1
            header1.VerticalAlignment = Element.ALIGN_MIDDLE
            Dim header2 As New PdfPCell(New Paragraph("CUENTA", FontFactory.GetFont("Arial", 8, Font.BOLD)))
            header2.PaddingBottom = 5
            header2.HorizontalAlignment = 1
            header2.VerticalAlignment = Element.ALIGN_MIDDLE
            Dim header3 As New PdfPCell(New Paragraph("DESCRIPCIÓN", FontFactory.GetFont("Arial", 8, Font.BOLD)))
            header3.PaddingBottom = 5
            header3.HorizontalAlignment = 1
            header3.VerticalAlignment = Element.ALIGN_MIDDLE
            Dim header4 As New PdfPCell(New Paragraph("DÉBITO", FontFactory.GetFont("Arial", 8, Font.BOLD)))
            header4.PaddingBottom = 5
            header4.HorizontalAlignment = 1
            header4.VerticalAlignment = Element.ALIGN_MIDDLE
            Dim header5 As New PdfPCell(New Paragraph("CRÉDITO", FontFactory.GetFont("Arial", 8, Font.BOLD)))
            header5.PaddingBottom = 5
            header5.HorizontalAlignment = 1
            header5.VerticalAlignment = Element.ALIGN_MIDDLE

            header1.BorderWidth = 0
            header2.BorderWidth = 0
            header3.BorderWidth = 0
            header4.BorderWidth = 0
            header5.BorderWidth = 0

            header2.BorderWidthLeft = 0.5
            header3.BorderWidthLeft = 0.5
            header4.BorderWidthLeft = 0.5
            header5.BorderWidthLeft = 0.5

            header1.BorderWidthBottom = 0.5
            header2.BorderWidthBottom = 0.5
            header3.BorderWidthBottom = 0.5
            header4.BorderWidthBottom = 0.5
            header5.BorderWidthBottom = 0.5

            unaTabla.AddCell(header1)
            unaTabla.AddCell(header2)
            unaTabla.AddCell(header3)
            unaTabla.AddCell(header4)
            unaTabla.AddCell(header5)
            unaTabla.HeaderRows = 1

            Dim celda As PdfPCell
            For Each rowP As DataRow In dtBSSFFNDIA.Rows
                parametro(0) = Control
                parametro(1) = rowP("DCOMPRO")
                dtBSSFFNDIA2 = datos.ConsultarBSSFFNDIAdt(parametro, 1)
                If dtBSSFFNDIA2 IsNot Nothing Then
                    Dim celda2 As PdfPCell
                    For Each row As DataRow In dtBSSFFNDIA2.Rows
                        celda2 = New PdfPCell(New Paragraph(row("DCOMPRO"), FontFactory.GetFont("Arial", 8)))
                        celda2.BorderWidth = 0
                        If flag = 1 Then
                            celda2.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                        End If
                        unaTabla.AddCell(celda2)

                        celda2 = New PdfPCell(New Paragraph(row("DCUENTA"), FontFactory.GetFont("Arial", 8)))
                        celda2.BorderWidth = 0
                        celda2.BorderWidthLeft = 0.5
                        If flag = 1 Then
                            celda2.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                        End If
                        unaTabla.AddCell(celda2)

                        celda2 = New PdfPCell(New Paragraph(row("DDESCTA"), FontFactory.GetFont("Arial", 8)))
                        celda2.BorderWidth = 0
                        celda2.BorderWidthLeft = 0.5
                        If flag = 1 Then
                            celda2.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                        End If
                        unaTabla.AddCell(celda2)

                        celda2 = New PdfPCell(New Paragraph(FormatNumber(row("DDEBITO"), 2), FontFactory.GetFont("Arial", 8)))
                        celda2.BorderWidth = 0
                        celda2.BorderWidthLeft = 0.5
                        celda2.HorizontalAlignment = 2
                        If flag = 1 Then
                            celda2.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                        End If
                        unaTabla.AddCell(celda2)

                        celda2 = New PdfPCell(New Paragraph(FormatNumber(row("DCREDITO"), 2), FontFactory.GetFont("Arial", 8)))
                        celda2.BorderWidth = 0
                        celda2.BorderWidthLeft = 0.5
                        celda2.HorizontalAlignment = 2
                        If flag = 1 Then
                            celda2.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                            flag = 0
                        Else
                            flag += 1
                        End If
                        unaTabla.AddCell(celda2)
                    Next

                    parametro(0) = Control
                    parametro(1) = rowP("DCOMPRO")
                    dtBSSFFNDIA3 = datos.ConsultarBSSFFNDIAdt(parametro, 2)
                    If dtBSSFFNDIA3 IsNot Nothing Then
                        celda = New PdfPCell(New Paragraph(FormatNumber(dtBSSFFNDIA3.Rows(0).Item("DEBITO"), 2), FontFactory.GetFont("Arial", 8)))
                        celda.Colspan = 4
                        celda.BorderWidth = 0
                        celda.BorderWidthLeft = 0.5
                        celda.BorderWidthTop = 0.5
                        celda.BorderWidthBottom = 0.5
                        If flag = 1 Then
                            'celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                            flag = 0
                        Else
                            flag += 1
                        End If
                        unaTabla.AddCell(celda)

                        celda = New PdfPCell(New Paragraph(FormatNumber(dtBSSFFNDIA3.Rows(0).Item("CREDITO"), 2), FontFactory.GetFont("Arial", 8)))
                        celda.BorderWidth = 0
                        celda.BorderWidthLeft = 0.5
                        celda.BorderWidthTop = 0.5
                        celda.BorderWidthBottom = 0.5
                        If flag = 1 Then
                            'celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                            flag = 0
                        Else
                            flag += 1
                        End If
                        unaTabla.AddCell(celda)
                        celda = New PdfPCell(New Paragraph(" ", FontFactory.GetFont("Arial", 8)))
                        celda.BorderWidth = 0
                        celda.Colspan = 5
                        unaTabla.AddCell(celda)
                    End If
                End If
            Next
            data = Nothing
            data = New RFTASK
            With data
                .TLXMSG = "EJECUTADA"
                .TMSG = "Libro Diario Generado N° Paginas: " & PageCount
                .TURL = Replace(ArchivoPDF, CARPETA_SITIO, "\")
                .STS2 = "1"
                .TNUMREG = tarea
            End With
            If datos.Piloto = "NO" Then
                continuar = datos.ActualizarRFTASKRow(data, 2)
            End If
            If continuar = False Then
                'ERROR DE ACTUALIZADO
            End If
        Else
            datos.WrtTxtError(LOG_File, "Libro no se encontro.")
            data = Nothing
            data = New RFTASK
            With data
                .TLXMSG = "EJECUTADA"
                .TMSG = "Libro Diario no Existe"
                .STS2 = "1"
                .TNUMREG = tarea
            End With
            If datos.Piloto = "NO" Then
                continuar = datos.ActualizarRFTASKRow(data, 1)
            End If
            If continuar = False Then
                'ERROR DE ACTUALIZADO
            Else
                datos.WrtTxtError(LOG_File, "Libro sin datos")
            End If
        End If
        Dim celdapapa As New PdfPCell(unaTabla)
        celdapapa.BorderWidth = 0.5
        tablaPadre.AddCell(celdapapa)

        Return tablaPadre
    End Function

    Public Function GenerarTabla2() As PdfPTable
        Dim parametro(1) As String
        Dim flag As Integer = 0

        Dim dtBSSFFNDIA2 As DataTable
        Dim dtBSSFFNDIA3 As DataTable

        Console.WriteLine("Generando una tabla...")

        Dim tablaPadre As New PdfPTable(1)
        tablaPadre.SetWidthPercentage(New Single() {615}, PageSize.LETTER)
        tablaPadre.HorizontalAlignment = Element.ALIGN_CENTER
        tablaPadre.DefaultCell.BorderWidth = 0

        'Headers
        

            Dim header1 As New PdfPCell(New Paragraph("COMP", FontFactory.GetFont("Arial", 8, Font.BOLD)))
            header1.PaddingBottom = 5
            header1.HorizontalAlignment = 1
            header1.VerticalAlignment = Element.ALIGN_MIDDLE
            Dim header2 As New PdfPCell(New Paragraph("CUENTA", FontFactory.GetFont("Arial", 8, Font.BOLD)))
            header2.PaddingBottom = 5
            header2.HorizontalAlignment = 1
            header2.VerticalAlignment = Element.ALIGN_MIDDLE
            Dim header3 As New PdfPCell(New Paragraph("DESCRIPCIÓN", FontFactory.GetFont("Arial", 8, Font.BOLD)))
            header3.PaddingBottom = 5
            header3.HorizontalAlignment = 1
            header3.VerticalAlignment = Element.ALIGN_MIDDLE
            Dim header4 As New PdfPCell(New Paragraph("DÉBITO", FontFactory.GetFont("Arial", 8, Font.BOLD)))
            header4.PaddingBottom = 5
            header4.HorizontalAlignment = 1
            header4.VerticalAlignment = Element.ALIGN_MIDDLE
            Dim header5 As New PdfPCell(New Paragraph("CRÉDITO", FontFactory.GetFont("Arial", 8, Font.BOLD)))
            header5.PaddingBottom = 5
            header5.HorizontalAlignment = 1
            header5.VerticalAlignment = Element.ALIGN_MIDDLE

            header1.BorderWidth = 0
            header2.BorderWidth = 0
            header3.BorderWidth = 0
            header4.BorderWidth = 0
            header5.BorderWidth = 0

            header2.BorderWidthLeft = 0.5
            header3.BorderWidthLeft = 0.5
            header4.BorderWidthLeft = 0.5
            header5.BorderWidthLeft = 0.5

            header1.BorderWidthBottom = 0.5
            header2.BorderWidthBottom = 0.5
            header3.BorderWidthBottom = 0.5
            header4.BorderWidthBottom = 0.5
            header5.BorderWidthBottom = 0.5

            Dim tablaHijaTotal As PdfPTable

            Dim tablaHijaDatos As New PdfPTable(1)
            tablaHijaDatos.SetWidthPercentage(New Single() {615}, PageSize.LETTER)
            tablaHijaDatos.HorizontalAlignment = Element.ALIGN_CENTER
            tablaHijaDatos.DefaultCell.BorderWidth = 0
            For Each rowP As DataRow In dtBSSFFNDIA.Rows
                parametro(0) = Control
                parametro(1) = rowP("DCOMPRO")
                dtBSSFFNDIA2 = datos.ConsultarBSSFFNDIAdt(parametro, 1)
                If dtBSSFFNDIA2 IsNot Nothing Then
                    tablaHijaTotal = New PdfPTable(5)
                    tablaHijaTotal.HorizontalAlignment = Element.ALIGN_CENTER
                    tablaHijaTotal.DefaultCell.BorderWidth = 0
                    tablaHijaTotal.SetWidthPercentage(New Single() {35, 50, 310, 110, 110}, PageSize.LETTER)
                    tablaHijaTotal.AddCell(header1)
                    tablaHijaTotal.AddCell(header2)
                    tablaHijaTotal.AddCell(header3)
                    tablaHijaTotal.AddCell(header4)
                    tablaHijaTotal.AddCell(header5)
                    tablaHijaTotal.HeaderRows = 1

                    Dim celdaDatos As PdfPCell
                    Dim celdaTotal As PdfPCell
                    For Each row As DataRow In dtBSSFFNDIA2.Rows
                        celdaDatos = New PdfPCell(New Paragraph(row("DCOMPRO"), FontFactory.GetFont("Arial", 8)))
                        celdaDatos.FixedHeight = 12
                        celdaDatos.BorderWidth = 0
                        If flag = 1 Then
                            celdaDatos.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                        End If
                        tablaHijaTotal.AddCell(celdaDatos)

                        celdaDatos = New PdfPCell(New Paragraph(row("DCUENTA"), FontFactory.GetFont("Arial", 8)))
                        celdaDatos.FixedHeight = 12
                        celdaDatos.BorderWidth = 0
                        celdaDatos.BorderWidthLeft = 0.5
                        If flag = 1 Then
                            celdaDatos.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                        End If
                        tablaHijaTotal.AddCell(celdaDatos)

                        celdaDatos = New PdfPCell(New Paragraph(row("DDESCTA"), FontFactory.GetFont("Arial", 8)))
                        celdaDatos.FixedHeight = 12
                        celdaDatos.BorderWidth = 0
                        celdaDatos.BorderWidthLeft = 0.5
                        If flag = 1 Then
                            celdaDatos.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                        End If
                        tablaHijaTotal.AddCell(celdaDatos)

                        celdaDatos = New PdfPCell(New Paragraph(FormatNumber(row("DDEBITO"), 2), FontFactory.GetFont("Arial", 8)))
                        celdaDatos.FixedHeight = 12
                        celdaDatos.BorderWidth = 0
                        celdaDatos.BorderWidthLeft = 0.5
                        celdaDatos.HorizontalAlignment = 2
                        If flag = 1 Then
                            celdaDatos.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                        End If
                        tablaHijaTotal.AddCell(celdaDatos)

                        celdaDatos = New PdfPCell(New Paragraph(FormatNumber(row("DCREDITO"), 2), FontFactory.GetFont("Arial", 8)))
                        celdaDatos.FixedHeight = 12
                        celdaDatos.BorderWidth = 0
                        celdaDatos.BorderWidthLeft = 0.5
                        celdaDatos.HorizontalAlignment = 2
                        If flag = 1 Then
                            celdaDatos.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                            flag = 0
                        Else
                            flag += 1
                        End If
                        tablaHijaTotal.AddCell(celdaDatos)
                        tablaHijaTotal.SplitRows = True
                        tablaHijaTotal.SplitLate = False
                        tablaHijaTotal.SpacingAfter = 0
                        tablaHijaTotal.SpacingBefore = 0
                        tablaHijaTotal.Complete = False
                    Next

                    parametro(0) = Control
                    parametro(1) = rowP("DCOMPRO")
                    dtBSSFFNDIA3 = datos.ConsultarBSSFFNDIAdt(parametro, 2)

                    If dtBSSFFNDIA3 IsNot Nothing Then
                        celdaTotal = New PdfPCell(New Paragraph(FormatNumber(dtBSSFFNDIA3.Rows(0).Item("DEBITO"), 2), FontFactory.GetFont("Arial", 8, Font.BOLD)))
                        celdaTotal.Colspan = 4
                        celdaTotal.BorderWidth = 0
                        celdaTotal.BorderWidthTop = 0.5
                        celdaTotal.HorizontalAlignment = 2
                        tablaHijaTotal.AddCell(celdaTotal)

                        celdaTotal = New PdfPCell(New Paragraph(FormatNumber(dtBSSFFNDIA3.Rows(0).Item("CREDITO"), 2), FontFactory.GetFont("Arial", 8, Font.BOLD)))
                        celdaTotal.BorderWidth = 0
                        celdaTotal.BorderWidthLeft = 0.5
                        celdaTotal.BorderWidthTop = 0.5
                        celdaTotal.HorizontalAlignment = 2
                        If flag = 1 Then
                            flag = 0
                        End If
                        tablaHijaTotal.AddCell(celdaTotal)
                        tablaHijaTotal.SplitRows = True
                        tablaHijaTotal.SplitLate = False
                        tablaHijaTotal.SpacingAfter = 0
                        tablaHijaTotal.SpacingBefore = 0
                        tablaHijaTotal.Complete = True

                        Dim celdapapa As New PdfPCell(tablaHijaTotal)
                        celdapapa.BorderWidth = 0.5
                        tablaHijaDatos.AddCell(celdapapa)
                        Dim celda1 As New PdfPCell(New Paragraph(""))
                        celda1.FixedHeight = 18
                        celda1.BorderWidth = 0
                        tablaHijaDatos.AddCell(celda1)
                        tablaHijaDatos.SplitRows = True
                        tablaHijaDatos.SplitLate = False
                        tablaHijaDatos.SpacingAfter = 0
                        tablaHijaDatos.SpacingBefore = 0
                        tablaHijaDatos.Complete = True
                    End If
                End If
            Next
            Dim celdapapa2 As New PdfPCell(tablaHijaDatos)
            celdapapa2.BorderWidth = 0
            tablaPadre.AddCell(celdapapa2)
            
        
        Return tablaPadre
    End Function

    Private Function fintabla(doc As Document) As PdfPTable

        Dim unaTabla As New PdfPTable(1)
        unaTabla.SetWidthPercentage(New Single() {625}, PageSize.LETTER)
        unaTabla.DefaultCell.BorderWidth = 0
        Dim fin As New PdfPCell(New Paragraph("Fin del Informe", FontFactory.GetFont("Arial", 12, Font.BOLD)))
        fin.BorderWidth = 0
        fin.HorizontalAlignment = Element.ALIGN_CENTER
        unaTabla.AddCell(fin)
        Return unaTabla
    End Function

    Sub FinDocumento()
        If PageCount Mod 2 = 0 Then
            document.NewPage()
            Dim unaTabla As New PdfPTable(1)
            unaTabla.SetWidthPercentage(New Single() {625}, PageSize.LETTER)
            unaTabla.DefaultCell.BorderWidth = 0
            Dim fin As New PdfPCell(New Paragraph("Fin del Informe", FontFactory.GetFont("Arial", 12, Font.BOLD)))
            fin.BorderWidth = 0.5
            fin.HorizontalAlignment = Element.ALIGN_CENTER
            unaTabla.AddCell(fin)

            document.Add(unaTabla)
        End If
    End Sub

End Module
