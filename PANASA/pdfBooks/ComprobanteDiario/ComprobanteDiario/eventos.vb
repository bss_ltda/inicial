﻿Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports CLDatos

Public Class eventos
    Inherits PdfPageEventHelper

    Private Titulo As New PdfPCell(New Paragraph("COMPROBANTE DIARIO", FontFactory.GetFont("Arial", 11, Font.BOLD)))
    Private Ref As New PdfPCell(New Paragraph(Referencia.ToString, FontFactory.GetFont("Arial", 8, BaseColor.RED)))
    Private Fech As New PdfPCell(New Paragraph(FechaImpresion.ToString, FontFactory.GetFont("Arial", 8)))
    Private Nombre As New PdfPCell(New Paragraph(NOMEMP.ToString, FontFactory.GetFont("Arial", 8, Font.BOLD)))
    Private Nitt As New PdfPCell(New Paragraph(NIT.ToString, FontFactory.GetFont("Arial", 8)))
    Private periodot As New PdfPCell(New Paragraph("PERIODO", FontFactory.GetFont("Arial", 8)))
    Private aniomes As New PdfPCell(New Paragraph(Periodo, FontFactory.GetFont("Arial", 8, Font.BOLD)))
    Private libro As New PdfPCell(New Paragraph("LIBRO", FontFactory.GetFont("Arial", 8)))
    Private librot As New PdfPCell(New Paragraph(Book, FontFactory.GetFont("Arial", 8, Font.BOLD)))
    Private imagen As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(LOGO)
    Private percentage As Single = 0.0F
    Private imageCell As PdfPCell = New PdfPCell(imagen)
    Private data
    Private datos As New ClsDatosAS400
    Private continuar As Boolean

    Public Overrides Sub OnEndPage(writer As PdfWriter, document As Document)
        Try
            Console.WriteLine(writer.PageNumber)
            If writer.PageNumber = 100 Or writer.PageNumber = 1000 Then
                data = Nothing
                data = New RFTASK
                With data
                    .TLXMSG = "EJECUTADA"
                    .TMSG = "N° Paginas Generadas: " & writer.PageNumber
                    .STS2 = "1"
                    .TNUMREG = tarea
                End With
                If datos.Piloto = "NO" Then
                    continuar = datos.ActualizarRFTASKRow(data, 2)
                End If
                If continuar = False Then
                    'ERROR DE ACTUALIZADO
                End If
            End If
            'Console.WriteLine(writer.CurrentPageNumber)
            Dim pdfTab As New PdfPTable(6)
            Dim Cons As New PdfPCell(New Paragraph(Consecutivo + writer.PageNumber, FontFactory.GetFont("Arial", 8, BaseColor.BLUE)))

            '===== TITULO =========
            Titulo.Colspan = 3
            Titulo.PaddingBottom = 5.0F
            Titulo.BorderWidth = 0
            Titulo.HorizontalAlignment = Element.ALIGN_LEFT

            '===== REFERENCIA =========
            Ref.BorderWidth = 0
            Ref.HorizontalAlignment = Element.ALIGN_RIGHT

            '===== CONSECUTIVO =========
            Cons.BorderWidth = 0
            Cons.HorizontalAlignment = Element.ALIGN_LEFT

            '===== FECHA =========
            Fech.BorderWidth = 0
            Fech.HorizontalAlignment = 2

            '===== NOMBRE EMP =========
            Nombre.Colspan = 6
            Nombre.BorderWidth = 0
            Nombre.HorizontalAlignment = 0

            '===== NIT =========
            Nitt.Colspan = 6
            Nitt.BorderWidth = 0
            Nitt.HorizontalAlignment = 0

            '===== PERIODO =========
            periodot.BorderWidth = 0
            periodot.HorizontalAlignment = 0
            periodot.VerticalAlignment = Element.ALIGN_MIDDLE

            '===== PERIODO AÑO MES =========
            aniomes.BorderWidth = 0
            aniomes.HorizontalAlignment = 0
            aniomes.VerticalAlignment = Element.ALIGN_MIDDLE

            '===== LIBRO =========
            libro.BorderWidth = 0
            libro.HorizontalAlignment = Element.ALIGN_RIGHT
            libro.VerticalAlignment = Element.ALIGN_MIDDLE

            '===== NOMBRE LIBRO =========
            librot.BorderWidth = 0
            librot.HorizontalAlignment = Element.ALIGN_LEFT
            librot.VerticalAlignment = Element.ALIGN_MIDDLE

            '===== IMAGEN LOGO =========
            imagen.BorderWidth = 0
            imagen.Alignment = Element.ALIGN_RIGHT

            '===== ESCALANDO IMAGEN =========
            percentage = 78 / imagen.Width
            imagen.ScalePercent(percentage * 100)

            imageCell.BorderWidth = 0
            imageCell.Colspan = 3
            imageCell.HorizontalAlignment = 2

            pdfTab.AddCell(Titulo)
            pdfTab.AddCell(Ref)
            pdfTab.AddCell(Cons)
            pdfTab.AddCell(Fech)
            pdfTab.AddCell(Nombre)
            pdfTab.AddCell(Nitt)
            pdfTab.AddCell(periodot)
            pdfTab.AddCell(aniomes)
            pdfTab.AddCell(libro)
            pdfTab.AddCell(librot)
            pdfTab.AddCell(imageCell)

            pdfTab.SetWidthPercentage(New Single() {50, 50, 65, 460, 45, 50}, PageSize.LETTER)
            pdfTab.WriteSelectedRows(0, -1, 34.8, document.PageSize.Height - 42, writer.DirectContent)
        Catch ex As System.StackOverflowException
            Console.WriteLine(ex.StackTrace & vbCrLf & ex.Message)
        End Try
    End Sub
End Class
