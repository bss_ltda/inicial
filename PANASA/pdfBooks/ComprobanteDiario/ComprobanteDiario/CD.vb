﻿Imports CLDatos
Imports System.IO
Imports System.Text
Imports iTextSharp.text.pdf
Imports iTextSharp.text

Module CD
    Private datos As New ClsDatosAS400
    Public FechaImpresion
    Public Referencia As String
    Private data
    Private CARPETA_SITIO As String
    Private CARPETA_IMG As String
    Private Control As String
    Public Consecutivo As String
    Public tarea As String
    Private bPRUEBAS As Boolean
    Private PDF_Folder As String
    Private LOG_File As String
    Private Comprobante
    Public Periodo
    Public Book
    Public LOGO As String
    Public NOMEMP As String
    Public NIT As String
    Private document As Document
    Private PageCount As Integer
    Private ArchivoPDF As String
    Private dtBSSFFNCOM As DataTable
    Private pw As PdfWriter

    Sub Main()
        Dim campoParam As String = ""
        Dim aSubParam() As String
        Try
            Environment.GetCommandLineArgs()
            For Each parametro In Environment.GetCommandLineArgs()
                aSubParam = Split(parametro, "=")
                Select Case UCase(aSubParam(0))
                    Case "TAREA"
                        tarea = aSubParam(1)
                    Case "COMPROBANTE"
                        Comprobante = aSubParam(1)
                    Case "CONSECUTIVO"
                        Consecutivo = aSubParam(1)
                    Case "REFERENCIA"
                        Referencia = aSubParam(1)
                    Case "FECHA"
                        FechaImpresion = aSubParam(1)
                    Case "BOOK"
                        Book = aSubParam(1)
                    Case "PERIODO"
                        Periodo = aSubParam(1)
                End Select
            Next

            If Not datos.AbrirConexion() Then
                Return
            End If
            If datos.Piloto.ToUpper() = "SI" Then
                campoParam = "CCNOT2"
            End If
            CARPETA_SITIO = datos.ConsultarRFPARAMString("SITIO_CARPETA", campoParam)
            CARPETA_IMG = datos.ConsultarRFPARAMString("CARPETA_IMG", campoParam)
            LOGO = datos.ConsultarRFPARAMString("LOGO", campoParam)
            NOMEMP = datos.ConsultarRFPARAMString("NOMBRE", campoParam)
            NIT = datos.ConsultarRFPARAMString("NIT", campoParam)

            datos.wrtLog("CARPETA_SITIO" & CARPETA_SITIO, "111")
            datos.wrtLog("Periodo" & Periodo, "111")
            datos.wrtLog("Comprobante" & Comprobante, "111")
            datos.wrtLog("Consecutivo" & Consecutivo, "111")
            datos.wrtLog("Referencia" & Referencia, "111")
            datos.wrtLog("FechaImpresion" & FechaImpresion, "111")

            bPRUEBAS = datos.Piloto.ToUpper() = "SI"
            PDF_Folder = CARPETA_IMG & "pdf\Libros\"
            Try
                If Not Directory.Exists(PDF_Folder) Then
                    Directory.CreateDirectory(PDF_Folder)
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\DirectorioError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo crear Directorio." & vbCrLf & ex.Message)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try

            LOG_File = PDF_Folder & "LD" & Control & ".log"
            datos.WrtTxtError(LOG_File, Control)

            Balancee()
            'Console.ReadLine()

            datos.CerrarConexion()
            Return
        Catch ex As Exception
            datos.wrtLogHtml("", ex.Message.ToString)
            datos.CerrarConexion()
            Return
        End Try
    End Sub

    Sub Balancee()
        Dim continuar As Boolean
        Console.WriteLine("Generando documento...")
        data = Nothing
        data = New RFTASK
        With data
            .TLXMSG = "EJECUTANDO"
            .TMSG = "Generando Comprobante Diario"
            .TNUMREG = tarea
        End With
        If datos.Piloto = "NO" Then
            continuar = datos.ActualizarRFTASKRow(data, 0)
        Else
            continuar = True
        End If
        If continuar = True Then
            ArchivoPDF = PDF_Folder & "ComproDiario" & Periodo & Comprobante & ".pdf"
            pdf()

            If bPRUEBAS Then
                'EJECUTA
                Dim prc As Process = New System.Diagnostics.Process()
                prc.StartInfo.FileName = ArchivoPDF
                prc.Start()
            End If
        End If
    End Sub

    Sub pdf()
        Dim continuar As Boolean
        Dim parametro(3) As String

        document = New Document(New Rectangle(288.0F, 144.0F), 36, 36, 140, 40)
        document.SetPageSize(iTextSharp.text.PageSize.LETTER.Rotate())
        Console.WriteLine(document.PageSize.Width.ToString)
        document.AddTitle("Libro ComproDiario - ")
        Dim es As eventos = New eventos()
        pw = PdfWriter.GetInstance(document, New FileStream(ArchivoPDF, FileMode.Create))
        pw.PageEvent = es
        document.Open()

        parametro(0) = Periodo
        parametro(1) = Comprobante
        parametro(2) = Book
        dtBSSFFNCOM = datos.ConsultarBSSFFNCOMdt(parametro, 0)
        If dtBSSFFNCOM IsNot Nothing Then
            Dim unaTabla As PdfPTable = GenerarTabla()
            document.Add(unaTabla)
            PageCount = pw.PageNumber
            data = Nothing
            data = New RFTASK
            With data
                .TLXMSG = "EJECUTADA"
                .TMSG = "Comprobante Diario Generado N° Paginas: " & PageCount
                .TURL = Replace(ArchivoPDF, CARPETA_SITIO, "\")
                .STS2 = "1"
                .TNUMREG = tarea
            End With
            If datos.Piloto = "NO" Then
                continuar = datos.ActualizarRFTASKRow(data, 2)
            End If
            If continuar = False Then
                'ERROR DE ACTUALIZADO
            End If
        Else
            datos.WrtTxtError(LOG_File, "Libro no se encontro.")
            data = Nothing
            data = New RFTASK
            With data
                .TLXMSG = "EJECUTADA"
                .TMSG = "Comprobante Diario no Existe"
                .STS2 = "1"
                .TNUMREG = tarea
            End With
            If datos.Piloto = "NO" Then
                continuar = datos.ActualizarRFTASKRow(data, 1)
            End If
            If continuar = False Then
                'ERROR DE ACTUALIZADO
            Else
                datos.WrtTxtError(LOG_File, "Libro sin datos")
            End If
        End If

        Dim tablafin As PdfPTable = fintabla(document)
        document.Add(tablafin)
        FinDocumento()

        document.Close()
    End Sub

    Private Function fintabla(doc As Document) As PdfPTable

        Dim unaTabla As New PdfPTable(1)
        unaTabla.SetWidthPercentage(New Single() {625}, PageSize.LETTER)
        unaTabla.DefaultCell.BorderWidth = 0
        Dim fin As New PdfPCell(New Paragraph("Fin del Informe", FontFactory.GetFont("Arial", 12, Font.BOLD)))
        fin.BorderWidth = 0
        fin.HorizontalAlignment = Element.ALIGN_CENTER
        unaTabla.AddCell(fin)
        Return unaTabla
    End Function

    Sub FinDocumento()
        If PageCount Mod 2 = 0 Then
            document.NewPage()
            Dim unaTabla As New PdfPTable(1)
            unaTabla.SetWidthPercentage(New Single() {625}, PageSize.LETTER)
            unaTabla.DefaultCell.BorderWidth = 0
            Dim fin As New PdfPCell(New Paragraph("Fin del Informe", FontFactory.GetFont("Arial", 12, Font.BOLD)))
            fin.BorderWidth = 0.5
            fin.HorizontalAlignment = Element.ALIGN_CENTER
            unaTabla.AddCell(fin)

            document.Add(unaTabla)
        End If
    End Sub

    Public Function GenerarTabla() As PdfPTable
        Dim parametro(3) As String
        Dim flag As Integer = 0
        Dim dtBSSFFNCOM2 As DataTable
        Dim dtBSSFFNCOM3 As DataTable

        Console.WriteLine("Generando una tabla...")

        Dim tablaPadre As New PdfPTable(1)
        tablaPadre.SetWidthPercentage(New Single() {615}, PageSize.LETTER)
        tablaPadre.HorizontalAlignment = Element.ALIGN_CENTER
        tablaPadre.DefaultCell.BorderWidth = 0

        'Headers

        Dim header1 As New PdfPCell(New Paragraph("DOCUMENTO", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        Dim header2 As New PdfPCell(New Paragraph("CUENTA", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        Dim header3 As New PdfPCell(New Paragraph("DESCRIPCIÓN", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        Dim header4 As New PdfPCell(New Paragraph("CONCEPTO", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        Dim header5 As New PdfPCell(New Paragraph("TERCERO", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        Dim header6 As New PdfPCell(New Paragraph("COD TERCERO", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        Dim header7 As New PdfPCell(New Paragraph("NIT", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        Dim header8 As New PdfPCell(New Paragraph("DÉBITO", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        Dim header9 As New PdfPCell(New Paragraph("CRÉDITO", FontFactory.GetFont("Arial", 8, Font.BOLD)))

        header1.PaddingBottom = 5
        header1.HorizontalAlignment = 1
        header1.VerticalAlignment = Element.ALIGN_MIDDLE

        header2.PaddingBottom = 5
        header2.HorizontalAlignment = 1
        header2.VerticalAlignment = Element.ALIGN_MIDDLE

        header3.PaddingBottom = 5
        header3.HorizontalAlignment = 1
        header3.VerticalAlignment = Element.ALIGN_MIDDLE

        header4.PaddingBottom = 5
        header4.HorizontalAlignment = 1
        header4.VerticalAlignment = Element.ALIGN_MIDDLE

        header5.PaddingBottom = 5
        header5.HorizontalAlignment = 1
        header5.VerticalAlignment = Element.ALIGN_MIDDLE

        header6.PaddingBottom = 5
        header6.HorizontalAlignment = 1
        header6.VerticalAlignment = Element.ALIGN_MIDDLE

        header7.PaddingBottom = 5
        header7.HorizontalAlignment = 1
        header7.VerticalAlignment = Element.ALIGN_MIDDLE

        header8.PaddingBottom = 5
        header8.HorizontalAlignment = 1
        header8.VerticalAlignment = Element.ALIGN_MIDDLE

        header9.PaddingBottom = 5
        header9.HorizontalAlignment = 1
        header9.VerticalAlignment = Element.ALIGN_MIDDLE

        header1.BorderWidth = 0
        header2.BorderWidth = 0
        header3.BorderWidth = 0
        header4.BorderWidth = 0
        header5.BorderWidth = 0
        header6.BorderWidth = 0
        header7.BorderWidth = 0
        header8.BorderWidth = 0
        header9.BorderWidth = 0

        header2.BorderWidthLeft = 0.5
        header3.BorderWidthLeft = 0.5
        header4.BorderWidthLeft = 0.5
        header5.BorderWidthLeft = 0.5
        header6.BorderWidthLeft = 0.5
        header7.BorderWidthLeft = 0.5
        header8.BorderWidthLeft = 0.5
        header9.BorderWidthLeft = 0.5

        header1.BorderWidthBottom = 0.5
        header2.BorderWidthBottom = 0.5
        header3.BorderWidthBottom = 0.5
        header4.BorderWidthBottom = 0.5
        header5.BorderWidthBottom = 0.5
        header6.BorderWidthBottom = 0.5
        header7.BorderWidthBottom = 0.5
        header8.BorderWidthBottom = 0.5
        header9.BorderWidthBottom = 0.5

        Dim tablaHijaTotal As PdfPTable
        Dim tablaTitulos1 As PdfPTable
        Dim tablaHijaDatos As New PdfPTable(1)
        tablaHijaDatos.SetWidthPercentage(New Single() {615}, PageSize.LETTER)
        tablaHijaDatos.HorizontalAlignment = Element.ALIGN_CENTER
        tablaHijaDatos.DefaultCell.BorderWidth = 0

        Dim titulo1 As PdfPCell
        Dim titulo2 As PdfPCell
        Dim titulo3 As PdfPCell
        For Each row As DataRow In dtBSSFFNCOM.Rows
            titulo1 = New PdfPCell(New Paragraph("Comprobante: " & row("DCOMPRO"), FontFactory.GetFont("Arial", 8)))
            titulo2 = New PdfPCell(New Paragraph("Nombre Evento: " & row("DEVENOM"), FontFactory.GetFont("Arial", 8)))
            titulo3 = New PdfPCell(New Paragraph("Evento: " & row("DEVENUM"), FontFactory.GetFont("Arial", 8)))

            titulo1.FixedHeight = 16

            titulo2.FixedHeight = 16

            titulo3.FixedHeight = 16

            titulo1.BorderWidth = 0
            titulo2.BorderWidth = 0
            titulo3.BorderWidth = 0

            tablaTitulos1 = New PdfPTable(3)
            tablaTitulos1.SetWidthPercentage(New Single() {150, 150, 315}, PageSize.LETTER)
            tablaTitulos1.HorizontalAlignment = Element.ALIGN_CENTER
            tablaTitulos1.DefaultCell.BorderWidth = 0

            tablaTitulos1.AddCell(titulo1)
            tablaTitulos1.AddCell(titulo2)
            tablaTitulos1.AddCell(titulo3)

            tablaHijaTotal = New PdfPTable(9)
            tablaHijaTotal.HorizontalAlignment = Element.ALIGN_CENTER
            tablaHijaTotal.DefaultCell.BorderWidth = 0
            tablaHijaTotal.SetWidthPercentage(New Single() {50, 40, 119, 103, 83, 60, 40, 60, 60}, PageSize.LETTER)

            tablaHijaTotal.AddCell(header1)
            tablaHijaTotal.AddCell(header2)
            tablaHijaTotal.AddCell(header3)
            tablaHijaTotal.AddCell(header4)
            tablaHijaTotal.AddCell(header5)
            tablaHijaTotal.AddCell(header6)
            tablaHijaTotal.AddCell(header7)
            tablaHijaTotal.AddCell(header8)
            tablaHijaTotal.AddCell(header9)
            tablaHijaTotal.HeaderRows = 2

            parametro(0) = row("DCOMPRO")
            parametro(1) = Periodo
            parametro(2) = Comprobante
            parametro(3) = Book
            dtBSSFFNCOM2 = datos.ConsultarBSSFFNCOMdt(parametro, 1)
            Dim celdaDatos As PdfPCell
            Dim celdaTotal As PdfPCell
            If dtBSSFFNCOM2 IsNot Nothing Then

                For Each row2 As DataRow In dtBSSFFNCOM2.Rows
                    celdaDatos = New PdfPCell(New Paragraph(row2("DDOCUM"), FontFactory.GetFont("Arial", 6)))
                    celdaDatos.FixedHeight = 12
                    celdaDatos.BorderWidth = 0
                    If flag = 1 Then
                        celdaDatos.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                    End If
                    tablaHijaTotal.AddCell(celdaDatos)

                    celdaDatos = New PdfPCell(New Paragraph(row2("DCUENTA"), FontFactory.GetFont("Arial", 6)))
                    celdaDatos.FixedHeight = 12
                    celdaDatos.BorderWidth = 0
                    celdaDatos.BorderWidthLeft = 0.5
                    If flag = 1 Then
                        celdaDatos.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                    End If
                    tablaHijaTotal.AddCell(celdaDatos)

                    celdaDatos = New PdfPCell(New Paragraph(row2("DDESS02"), FontFactory.GetFont("Arial", 6)))
                    celdaDatos.FixedHeight = 12
                    celdaDatos.BorderWidth = 0
                    celdaDatos.BorderWidthLeft = 0.5
                    If flag = 1 Then
                        celdaDatos.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                    End If
                    tablaHijaTotal.AddCell(celdaDatos)

                    celdaDatos = New PdfPCell(New Paragraph(row2("DCONCEP"), FontFactory.GetFont("Arial", 6)))
                    celdaDatos.FixedHeight = 12
                    celdaDatos.BorderWidth = 0
                    celdaDatos.BorderWidthLeft = 0.5
                    If flag = 1 Then
                        celdaDatos.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                    End If
                    tablaHijaTotal.AddCell(celdaDatos)

                    celdaDatos = New PdfPCell(New Paragraph(row2("DTERNOM"), FontFactory.GetFont("Arial", 6)))
                    celdaDatos.FixedHeight = 12
                    celdaDatos.BorderWidth = 0
                    celdaDatos.BorderWidthLeft = 0.5
                    If flag = 1 Then
                        celdaDatos.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                    End If
                    tablaHijaTotal.AddCell(celdaDatos)

                    celdaDatos = New PdfPCell(New Paragraph(row2("DTERCOD"), FontFactory.GetFont("Arial", 6)))
                    celdaDatos.FixedHeight = 12
                    celdaDatos.BorderWidth = 0
                    celdaDatos.BorderWidthLeft = 0.5
                    If flag = 1 Then
                        celdaDatos.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                    End If
                    tablaHijaTotal.AddCell(celdaDatos)

                    celdaDatos = New PdfPCell(New Paragraph(row2("DTERNIT"), FontFactory.GetFont("Arial", 6)))
                    celdaDatos.FixedHeight = 12
                    celdaDatos.BorderWidth = 0
                    celdaDatos.BorderWidthLeft = 0.5
                    If flag = 1 Then
                        celdaDatos.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                    End If
                    tablaHijaTotal.AddCell(celdaDatos)

                    celdaDatos = New PdfPCell(New Paragraph(FormatNumber(row2("DDEBITO"), 2), FontFactory.GetFont("Arial", 6)))
                    celdaDatos.FixedHeight = 12
                    celdaDatos.BorderWidth = 0
                    celdaDatos.BorderWidthLeft = 0.5
                    celdaDatos.HorizontalAlignment = Element.ALIGN_RIGHT
                    If flag = 1 Then
                        celdaDatos.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                    End If
                    tablaHijaTotal.AddCell(celdaDatos)

                    celdaDatos = New PdfPCell(New Paragraph(FormatNumber(row2("DCREDITO"), 2), FontFactory.GetFont("Arial", 6)))
                    celdaDatos.FixedHeight = 12
                    celdaDatos.BorderWidth = 0
                    celdaDatos.BorderWidthLeft = 0.5
                    celdaDatos.HorizontalAlignment = Element.ALIGN_RIGHT
                    If flag = 1 Then
                        celdaDatos.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                        flag = 0
                    Else
                        flag += 1
                    End If
                    tablaHijaTotal.AddCell(celdaDatos)
                    tablaHijaTotal.SplitRows = True
                    'tablaHijaTotal.SplitLate = False
                    'tablaHijaTotal.SpacingAfter = 0
                    'tablaHijaTotal.SpacingBefore = 0
                    'tablaHijaTotal.Complete = False
                Next
            End If
            parametro(0) = row("DCOMPRO")
            parametro(1) = Periodo
            parametro(2) = Comprobante
            parametro(3) = Book
            dtBSSFFNCOM3 = datos.ConsultarBSSFFNCOMdt(parametro, 2)
            If dtBSSFFNCOM3 IsNot Nothing Then
                celdaTotal = New PdfPCell(New Paragraph("Totales", FontFactory.GetFont("Arial", 6, Font.BOLD)))
                celdaTotal.FixedHeight = 13
                celdaTotal.Colspan = 7
                celdaTotal.BorderWidth = 0
                celdaTotal.BorderWidthTop = 0.5
                celdaTotal.HorizontalAlignment = Element.ALIGN_RIGHT
                tablaHijaTotal.AddCell(celdaTotal)

                celdaTotal = New PdfPCell(New Paragraph(FormatNumber(dtBSSFFNCOM3.Rows(0).Item("DEBITO"), 2), FontFactory.GetFont("Arial", 6, Font.BOLD)))
                celdaTotal.FixedHeight = 13
                celdaTotal.BorderWidth = 0
                celdaTotal.BorderWidthLeft = 0.5
                celdaTotal.BorderWidthTop = 0.5
                celdaTotal.HorizontalAlignment = Element.ALIGN_RIGHT
                tablaHijaTotal.AddCell(celdaTotal)

                celdaTotal = New PdfPCell(New Paragraph(FormatNumber(dtBSSFFNCOM3.Rows(0).Item("CREDITO"), 2), FontFactory.GetFont("Arial", 6, Font.BOLD)))
                celdaTotal.FixedHeight = 13
                celdaTotal.BorderWidth = 0
                celdaTotal.BorderWidthLeft = 0.5
                celdaTotal.BorderWidthTop = 0.5
                celdaTotal.HorizontalAlignment = Element.ALIGN_RIGHT
                If flag = 1 Then
                    flag = 0
                End If
                tablaHijaTotal.AddCell(celdaTotal)
                tablaHijaTotal.SplitRows = True
                'tablaHijaTotal.SplitLate = False
                'tablaHijaTotal.SpacingAfter = 0
                'tablaHijaTotal.SpacingBefore = 0
                'tablaHijaTotal.Complete = True

                Dim celdatitulos As New PdfPCell(tablaTitulos1)
                celdatitulos.BorderWidth = 0
                tablaHijaDatos.AddCell(celdatitulos)

                Dim celdapapa As New PdfPCell(tablaHijaTotal)
                celdapapa.BorderWidth = 0.5
                tablaHijaDatos.AddCell(celdapapa)
                Dim celda1 As New PdfPCell(New Paragraph(""))
                celda1.FixedHeight = 18
                celda1.BorderWidth = 0
                tablaHijaDatos.AddCell(celda1)
                tablaHijaDatos.SplitRows = True
                'tablaHijaDatos.SplitLate = False
                'tablaHijaDatos.SpacingAfter = 0
                'tablaHijaDatos.SpacingBefore = 0
                'tablaHijaDatos.Complete = True

                
            End If
        Next

        Dim celdapapa2 As New PdfPCell(tablaHijaDatos)
        celdapapa2.BorderWidth = 0
        tablaPadre.AddCell(celdapapa2)

        Return tablaPadre
    End Function

End Module
