﻿Imports System.ServiceProcess
Imports CLDatos
Imports IBM.Data.DB2.iSeries
Imports System.Text
Imports System.IO

Public Class wsTareas
    Private WithEvents m_timer As New Timers.Timer(30000)
    Private WithEvents timerevent As New Timers.Timer(3600000)
    Private datos As New ClsDatos

    Protected Overrides Sub OnStart(ByVal args() As String)
        Me.EventLog1.WriteEntry("Inicia")
        m_timer.Enabled = True
        timerevent.Enabled = True
    End Sub

    'Public Shared Sub StartService(serviceName As String, timeoutMilliseconds As Integer)
    '    Dim service As New ServiceController(serviceName)
    '    Try
    '        Dim timeout As TimeSpan = TimeSpan.FromMilliseconds(timeoutMilliseconds)
    '        service.Start()
    '        service.WaitForStatus(ServiceControllerStatus.Running, timeout)
    '    Catch
    '        ' ...
    '    End Try
    'End Sub

    'Public Shared Sub StopService(serviceName As String, timeoutMilliseconds As Integer)
    '    Dim service As New ServiceController(serviceName)
    '    Try
    '        Dim timeout As TimeSpan = TimeSpan.FromMilliseconds(timeoutMilliseconds)
    '        service.Stop()
    '        service.WaitForStatus(ServiceControllerStatus.Stopped, timeout)
    '    Catch
    '        ' ...
    '    End Try
    'End Sub

    'Public Shared Sub RestartService(serviceName As String, timeoutMilliseconds As Integer)
    '    Dim service As New ServiceController(serviceName)
    '    Try
    '        Dim millisec1 As Integer = Environment.TickCount
    '        Dim timeout As TimeSpan = TimeSpan.FromMilliseconds(timeoutMilliseconds)
    '        service.Stop()
    '        service.WaitForStatus(ServiceControllerStatus.Stopped, timeout)
    '        ' count the rest of the timeout
    '        Dim millisec2 As Integer = Environment.TickCount
    '        timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds - (millisec2 - millisec1))
    '        service.Start()
    '        service.WaitForStatus(ServiceControllerStatus.Running, timeout)
    '    Catch 
    '    End Try
    'End Sub

    Protected Overrides Sub OnStop()
        m_timer.Stop()
    End Sub

    Private Sub m_timer_Elapsed(sender As Object, e As Timers.ElapsedEventArgs) Handles m_timer.Elapsed
        Try
            m_timer.Enabled = False
            revisaTareas()
            m_timer.Enabled = True
        Catch ex4 As iDB2NullValueException
            Me.EventLog1.WriteEntry(datos.fSql & vbCrLf & ex4.InnerException.ToString & vbCrLf & ex4.Message & vbCrLf & ex4.StackTrace, EventLogEntryType.Error)
        Catch ex3 As iDB2ConnectionTimeoutException
            Me.EventLog1.WriteEntry(datos.fSql & vbCrLf & ex3.InnerException.ToString & vbCrLf & ex3.Message & vbCrLf & ex3.StackTrace, EventLogEntryType.Error)
        Catch ex2 As iDB2DCFunctionErrorException
            Me.EventLog1.WriteEntry(datos.fSql & vbCrLf & ex2.InnerException.ToString & vbCrLf & ex2.Message & vbCrLf & ex2.StackTrace, EventLogEntryType.Error)
        Catch ex1 As iDB2Exception
            Me.EventLog1.WriteEntry(datos.fSql & vbCrLf & ex1.InnerException.ToString & vbCrLf & ex1.Message & vbCrLf & ex1.StackTrace, EventLogEntryType.Error)
        Catch ex As Exception
            Me.EventLog1.WriteEntry(datos.fSql & vbCrLf & ex.Message & vbCrLf & ex.StackTrace, EventLogEntryType.Error)
        End Try

    End Sub

    Private Sub txt(ByVal mensaje As String)
        Dim sb As New StringBuilder()
        Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
        sb.AppendLine(mensaje)
        sb.AppendLine(Now().ToString)
        sw.WriteLine(sb.ToString())
        sw.Close()
    End Sub

    Private Sub timerevent_Elapsed(sender As Object, e As Timers.ElapsedEventArgs) Handles timerevent.Elapsed
        Dim dtSys As DataTable
        Me.EventLog1.WriteEntry("Servicio Activo")

        If Not datos.AbrirConexion() Then
            Console.WriteLine("Error Abriendo Conexión " & datos.mensaje & vbCrLf & datos.conexionIBM.ConnectionString)
            Me.EventLog1.WriteEntry("Error Abriendo Conexión " & datos.mensaje & vbCrLf & datos.conexionIBM.ConnectionString)
            Exit Sub
        End If
        Me.EventLog1.WriteEntry("Conexión Correcta")

        dtSys = datos.ConsultarSYSIBMSYSDUMMY1dt()
        If dtSys IsNot Nothing Then
            Me.EventLog1.WriteEntry(String.Format("Hora AS400: {0:yyyy-MM-dd HH:mm:ss}, Zona horaria: {1}", dtSys.Rows(0).Item(0), dtSys.Rows(0).Item(1)))
        Else
            Me.EventLog1.WriteEntry("Error en Consulta de Hora")
        End If

        datos.CerrarConexion()
        Me.EventLog1.WriteEntry("Conexión Cerrada")
    End Sub

End Class
