﻿Imports IBM.Data.DB2.iSeries
Imports System.IO
Imports System.Text

Public Class ClsDatos
    Public conexionIBM As New iDB2Connection()
    Private adapterIBM As iDB2DataAdapter = New iDB2DataAdapter()
    Private transIBM As iDB2Transaction
    Private builderIBM As iDB2CommandBuilder
    Private commandIBM As iDB2Command
    Private numFilas As Integer
    Private dt As DataTable
    Private WithEvents ds As DataSet
    Private continuarguardando As Boolean
    Public appPath As String = Path.GetFullPath(My.Application.Info.DirectoryPath & "\Log\")
    Public mensaje As String
    Public fSql As String
    Private data

#Region "ACTUALIZAR"

    ''' <summary>
    ''' Actualiza la tabla RFTASK 
    ''' </summary>
    ''' <param name="datos">RFTASK</param>
    ''' <param name="tipo">0- SET TLXMSG = @TLXMSG, STS1 = @STS1, TUPDDAT = NOW(), TMSG = @TMSG WHERE TNUMREG = @TNUMREG</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 23/05/2016</remarks>
    Public Function ActualizarRFTASKRow(ByVal datos As RFTASK, ByVal tipo As Integer) As Boolean
        Select Case tipo
            Case 0
                fSql = " UPDATE RFTASK SET "
                fSql &= " TLXMSG = '" & datos.TLXMSG & "', "
                fSql &= " STS1 = " & datos.STS1 & ", "
                fSql &= " TUPDDAT = NOW() , "
                fSql &= " TMSG = '" & datos.TMSG & "', "
                fSql &= " TCMD = '" & datos.TCMD & "'"
                fSql &= " WHERE TNUMREG = " & datos.TNUMREG
        End Select

        commandIBM = New iDB2Command
        commandIBM.Connection = conexionIBM
        Try
            commandIBM.CommandText = fSql
            commandIBM.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As Data.SqlClient.SqlException
            continuarguardando = False
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\RFTASKError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo actualizar la tabla RFTASK. Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()

                data = Nothing
                data = New RFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .OPERACION = "Actualizar RFTASK"
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarRFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

#End Region

#Region "CONSULTAR"

    ''' <summary>
    ''' Consulta la tabla RFTASK con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- </param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 23/05/2016</remarks>
    Public Function ConsultarRFTASKdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT * FROM RFTASK WHERE STS1 = 0 AND THLD = 0 AND TFPROXIMA <= NOW()"
        End Select
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)
        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla SYSIBM.SYSDUMMY1 con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 02/05/2016</remarks>
    Public Function ConsultarSYSIBMSYSDUMMY1dt() As DataTable
        dt = Nothing

        fSql = "SELECT NOW(), CURRENT_TIMEZONE  FROM SYSIBM.SYSDUMMY1"

        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)
        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

#End Region

#Region "FUNCIONES"

    Public Function AbrirConexion() As Boolean
        Dim abierta As Boolean = False
        Try
            If Not Directory.Exists(appPath) Then
                Directory.CreateDirectory(appPath)
            End If
            If conexionIBM.State = ConnectionState.Open Then
                abierta = True
            Else
                Console.WriteLine(conexionIBM)
                Console.WriteLine(My.Settings.BASEDATOS)
                If conexionIBM Is Nothing Then
                    Console.WriteLine("nothing")
                End If
                conexionIBM.ConnectionString = My.Settings.BASEDATOS
                conexionIBM.Open()
                abierta = True
                Console.WriteLine("OK")
            End If
        Catch ex5 As iDB2ConnectionFailedException
            abierta = False
            mensaje = ex5.InnerException.ToString & vbCrLf & ex5.Message
        Catch ex4 As iDB2InvalidConnectionStringException
            abierta = False
            mensaje = ex4.InnerException.ToString & vbCrLf & ex4.Message
        Catch ex1 As iDB2Exception
            abierta = False
            mensaje = ex1.InnerException.ToString & vbCrLf & ex1.Message
        Catch ex As Exception
            abierta = False
            mensaje = ex.InnerException.ToString & vbCrLf & ex.Message
        End Try
        If abierta = False Then
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo realizar la conexión.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & "Conexion: " & conexionIBM.ConnectionString)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End If
        Return abierta
    End Function

    Public Function CerrarConexion() As Boolean
        Dim cerrada As Boolean = False
        Try
            conexionIBM.Close()
            cerrada = True
        Catch ex1 As iDB2Exception
            cerrada = False
            mensaje = ex1.InnerException.ToString & vbCrLf & ex1.Message
        Catch ex As Exception
            cerrada = False
            mensaje = ex.InnerException.ToString & vbCrLf & ex.Message
        End Try
        If cerrada = False Then
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo Cerrar la conexión.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & "Conexion: " & conexionIBM.ConnectionString)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End If
        Return cerrada
    End Function

    Sub WrtSqlError(sql As String, ByVal Descrip As String)
        Dim continuar As Boolean

        Try
            data = Nothing
            data = New RFLOG
            With data
                .USUARIO = System.Net.Dns.GetHostName()
                .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                .ALERT = "1"
                .EVENTO = Descrip
                .TXTSQL = sql
            End With
            continuar = GuardarRFLOGRow(data)
            If continuar = False Then
                'ERROR DE GUARDADO
            End If
        Catch ex As Exception

        End Try
    End Sub

#End Region

#Region "GUARDAR"

    ''' <summary>
    ''' Guarda RFLOG
    ''' </summary>
    ''' <param name="datos">RFLOG</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 18/05/2016</remarks>
    Public Function GuardarRFLOGRow(ByVal datos As RFLOG) As Boolean
        Dim continuarguardandoRFLOG As Boolean

        fSql = "INSERT INTO RFLOG (USUARIO, OPERACION, PROGRAMA, EVENTO, TXTSQL, ALERT)"
        fSql &= " VALUES ('" & datos.USUARIO & "', '" & datos.OPERACION & "', '" & datos.PROGRAMA & "', '" & datos.EVENTO.Replace("'", "''") & "', '" & datos.TXTSQL.Replace("'", "''") & "', " & datos.ALERT & ")"

        commandIBM = New iDB2Command
        commandIBM.Connection = conexionIBM
        Try
            commandIBM.CommandText = fSql
            commandIBM.ExecuteNonQuery()
            continuarguardandoRFLOG = True
        Catch ex As Data.SqlClient.SqlException
            continuarguardandoRFLOG = False
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardandoRFLOG = False
            mensaje = ex.Message
        Finally
            If continuarguardandoRFLOG = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End If
        End Try
        Return continuarguardandoRFLOG
    End Function

#End Region

End Class

Public Class RFLOG
    Public USUARIO As String
    Public OPERACION As String
    Public PROGRAMA As String
    Public EVENTO As String
    Public TXTSQL As String
    Public ALERT As String
End Class

Public Class RFTASK
    Public TLXMSG As String
    Public STS1 As String
    Public TMSG As String
    Public TCMD As String
    Public TNUMREG As String
End Class
