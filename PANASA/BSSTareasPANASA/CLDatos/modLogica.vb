﻿Imports System.Text
Imports System.IO

Public Module modLogica
    Private datos As New ClsDatos
    Private data

    Sub revisaTareas()
        Dim dtRFTASK As DataTable
        Dim parametro(0) As String
        Dim pgm, param, exe, msgT, tLXmsg, Accion As String
        Dim sts As Integer
        Dim continuar As Boolean
        Dim eventl As New EventLog

        eventl.Source = "BSSTareasPANASA"
        eventl.Log = "Application"
        'datos = Nothing
        'datos = New ClsDatos

        Console.WriteLine("Abriendo Conexión...")
        If Not datos.AbrirConexion() Then
            eventl.WriteEntry("Error Abriendo Conexión " & datos.mensaje & vbCrLf & datos.conexionIBM.ConnectionString)
            Console.WriteLine("Error Abriendo Conexión " & datos.mensaje & vbCrLf & datos.conexionIBM.ConnectionString)
            Exit Sub
        End If
        eventl.WriteEntry("Conexión Correcta Proceso")
        Console.WriteLine("Consultando Tareas...")
        dtRFTASK = datos.ConsultarRFTASKdt(parametro, 0)
        If dtRFTASK IsNot Nothing Then
            eventl.WriteEntry("N° de Tareas: " & dtRFTASK.Rows.Count)
            Console.WriteLine("Recorriendo Tareas...")
            For Each row As DataRow In dtRFTASK.Rows
                Accion = UCase(row("TTIPO"))
                If row("FWRKID") <> 0 Then
                    If Accion <> "ULTIMO" Then
                        Accion = "PAQUETE"
                    End If
                Else
                    Accion = UCase(row("TTIPO"))
                End If
                Console.WriteLine(Accion)
                eventl.WriteEntry("Tipo de Tarea: " & Accion)
                Select Case Accion
                    Case UCase("Shell")
                        If row("TPGM") IsNot DBNull.Value Then
                            pgm = row("TPGM")
                            Console.WriteLine("Programa: " & row("TPGM"))
                        Else
                            pgm = ""
                        End If
                        If row("TPRM") IsNot DBNull.Value Then
                            param = row("TPRM")
                            Console.WriteLine("Parametros: " & row("TPRM"))
                        Else
                            param = ""
                        End If
                        If row("TNUMTAREA") = 1 Then
                            exe = pgm & param.Replace("|", " ") & " TAREA=" & row("TNUMREG")
                        Else
                            exe = pgm & param.Replace("|", " ")
                        End If
                        'exe = pgm & " " & param.Replace("|", " ")
                        Console.WriteLine("Comando: " & exe)
                        sts = 1
                        msgT = "Comando Enviado."
                        tLXmsg = "EJECUTADA"
                        Try
                            Console.WriteLine("Ejecutando: " & exe)
                            eventl.WriteEntry("Ejecutando: " & exe & " de tarea: " & row("TNUMREG"))
                            Shell(exe, AppWinStyle.Hide, False, -1)
                        Catch ex As Exception
                            Console.WriteLine("Error Ejecutando: " & exe & vbCrLf & ex.Message)
                            sts = -1
                            msgT = ex.Message.Replace("'", "''")
                            tLXmsg = "ERROR"
                            rflog("Ejecutando Shell", msgT, "Error Ejecutando " & exe, 1)
                        End Try
                        data = Nothing
                        data = New RFTASK
                        With data
                            .TLXMSG = tLXmsg
                            .STS1 = sts
                            .TMSG = msgT
                            .TCMD = "" & exe & ""
                            .TNUMREG = row("TNUMREG")
                        End With
                        Console.WriteLine("Actualizando RFTASK...")
                        continuar = datos.ActualizarRFTASKRow(data, 0)
                        If continuar = False Then
                            'ERROR DE ACTUALIZADO
                            Console.WriteLine("Error Actualizando RFTASK " & vbCrLf & datos.mensaje)
                        Else
                            Console.WriteLine("RFTASK Actualizado")
                        End If
                    Case UCase("Ultimo")
                        exe = "ImprimeDocumentos.exe " & row("FWRKID").Value
                        Console.WriteLine("Comando: " & exe)
                        sts = 1
                        msgT = "Paquete Enviado."
                        tLXmsg = "ENVIADO"
                        data = Nothing
                        data = New RFTASK
                        With data
                            .TLXMSG = tLXmsg
                            .STS1 = sts
                            .TMSG = msgT
                            .TCMD = "" & exe & ""
                            .TNUMREG = row("TNUMREG")
                        End With
                        Console.WriteLine("Actualizando RFTASK...")
                        continuar = datos.ActualizarRFTASKRow(data, 0)
                        If continuar = False Then
                            'ERROR DE ACTUALIZADO
                            Console.WriteLine("Error Actualizando RFTASK " & vbCrLf & datos.mensaje)
                        Else
                            Console.WriteLine("RFTASK Actualizado")
                        End If
                        Try
                            Console.WriteLine("Ejecutando: " & exe)
                            eventl.WriteEntry("Ejecutando: " & exe & " de tarea: " & row("TNUMREG"))
                            Shell(exe, AppWinStyle.Hide, False, -1)
                        Catch ex As Exception
                            Console.WriteLine("Error Ejecutando: " & exe & vbCrLf & ex.Message)
                            msgT = ex.Message.Replace("'", "''")
                            datos.WrtSqlError(exe, msgT)
                        End Try
                    Case UCase("Rutina")
                        If row("TPGM") IsNot DBNull.Value Then
                            pgm = row("TPGM")
                            Console.WriteLine("Programa: " & row("TPGM"))
                        Else
                            pgm = ""
                        End If
                        'Ejecutable de rutinas con parametro TNUMREG de RFTASK
                        exe = pgm & " " & row("TNUMREG").Value
                        sts = 1
                        msgT = "Comando Enviado."
                        tLXmsg = "EJECUTADA"
                        Try
                            Console.WriteLine("Ejecutando: " & exe)
                            eventl.WriteEntry("Ejecutando: " & exe & " de tarea: " & row("TNUMREG"))
                            Shell(exe, AppWinStyle.Hide, False, -1)
                        Catch ex As Exception
                            Console.WriteLine("Error Ejecutando: " & exe & vbCrLf & ex.Message)
                            sts = -1
                            msgT = ex.Message.Replace("'", "''")
                            tLXmsg = "ERROR"
                        End Try
                        data = Nothing
                        data = New RFTASK
                        With data
                            .TLXMSG = tLXmsg
                            .STS1 = sts
                            .TMSG = msgT
                            .TCMD = "" & exe & ""
                            .TNUMREG = row("TNUMREG")
                        End With
                        Console.WriteLine("Actualizando RFTASK...")
                        continuar = datos.ActualizarRFTASKRow(data, 0)
                        If continuar = False Then
                            'ERROR DE ACTUALIZADO
                            Console.WriteLine("Error Actualizando RFTASK " & vbCrLf & datos.mensaje)
                        Else
                            Console.WriteLine("RFTASK Actualizado")
                        End If
                    Case UCase("Java")
                End Select
            Next
        Else
            'Dim sb As New StringBuilder()
            'Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\RFTASKNothing" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            'sb.AppendLine("No hay tareas para ejecutar" & vbCrLf & datos.fSql)
            'sb.AppendLine(Now().ToString)
            'sw.WriteLine(sb.ToString())
            'sw.Close()
            Console.WriteLine("No hay Tareas para ejecutar")
        End If
        Console.WriteLine("FIN")
        datos.CerrarConexion()
        eventl.WriteEntry("Conexión Cerrada Proceso")
    End Sub

    Sub rflog(ByVal OPERACION As String, ByVal EVENTO As String, ByVal TXTSQL As String, ByVal ALERT As String)
        data = Nothing
        data = New RFLOG
        With data
            .USUARIO = System.Net.Dns.GetHostName()
            .OPERACION = OPERACION
            .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
            .EVENTO = EVENTO
            .TXTSQL = TXTSQL
            .ALERT = ALERT
        End With
        datos.GuardarRFLOGRow(data)
    End Sub

End Module
