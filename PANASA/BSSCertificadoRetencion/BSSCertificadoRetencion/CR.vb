﻿Imports CLDatos
Imports iTextSharp.text
Imports System.IO
Imports System.Text
Imports iTextSharp.text.pdf

Module CR
    Private datos As New ClsDatosAS400
    Private dtCab As DataTable
    Private dtDet As DataTable
    Private dtNotas As DataTable
    Dim totalvalor As Double
    Private data
    Private CARPETA_SITIO As String
    Private CARPETA_IMG As String
    Private TASK_No As String
    Private FDESDE As String
    Private FHASTA As String
    Private FTERNIT As String
    Private PDF_Folder As String
    Private bPRUEBAS As Boolean
    Public LOGO As String
    Public PIE As String
    Public NOMBRE As String
    Public NIT As String
    Private LOG_File As String
    Private ArchivoPDF As String
    Private document As Document
    Private PeriodoAAAA As String
    Private PeriodoMM As String
    Private NumeroDocumento As String
    Public RENGLON3 As String
    Public RENGLON4 As String
    Private cb As PdfContentByte
    Private pw As PdfWriter

    Sub Main()
        Dim parametro As String
        Dim aSubParam() As String
        Dim campoParam As String = ""
        Try
            Environment.GetCommandLineArgs()
            For Each parametro In Environment.GetCommandLineArgs()
                aSubParam = Split(parametro, "=")
                Select Case UCase(aSubParam(0))
                    Case "C"
                        NumeroDocumento = datos.Ceros(Trim(aSubParam(1)), 8)
                    Case "A"
                        PeriodoAAAA = aSubParam(1)
                    Case "M"
                        PeriodoMM = datos.Ceros(Trim(aSubParam(1)), 2)
                    Case "TAREA"
                        TASK_No = aSubParam(1)
                End Select
            Next

            If Not datos.AbrirConexion() Then
                Return
            End If

            If datos.Piloto = "SI" Then
                campoParam = "CCNOT2"
            End If
            CARPETA_SITIO = datos.ConsultarRFPARAMString("SITIO_CARPETA", campoParam, "", 0)
            CARPETA_IMG = datos.ConsultarRFPARAMString("CARPETA_IMG", campoParam, "", 0)
            LOGO = datos.ConsultarRFPARAMString("LOGO", campoParam, "", 0)
            PIE = datos.ConsultarRFPARAMString("PIE", campoParam, "", 0)
            NOMBRE = datos.ConsultarRFPARAMString("NOMBRE", campoParam, "", 0)
            NIT = datos.ConsultarRFPARAMString("NIT", campoParam, "", 0)
            RENGLON3 = datos.ConsultarRFPARAMString("RENGLON3", campoParam, "", 0)
            RENGLON4 = datos.ConsultarRFPARAMString("RENGLON4", campoParam, "", 0)
            bPRUEBAS = datos.Piloto.ToUpper() = "SI"
            PDF_Folder = CARPETA_SITIO & "xm\pdf\CertifRet\" & PeriodoAAAA & PeriodoMM
            Try
                If Not Directory.Exists(PDF_Folder) Then
                    Directory.CreateDirectory(PDF_Folder)
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\DirectorioError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo crear Directorio." & vbCrLf & ex.Message)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            LOG_File = PDF_Folder & "\" & NumeroDocumento & ".txt"
            datos.WrtTxtError(LOG_File, FTERNIT)

            Dim arguments As String() = Environment.GetCommandLineArgs()
            datos.WrtTxtError(LOG_File, String.Join(", ", arguments))

            If Not datos.CheckVer() Then
                datos.WrtSqlError(datos.lastError, "")
                End
            End If

            GenerarPDF()
            data = Nothing
            data = New RFTASK
            With data
                .STS2 = 1
                .TNUMREG = TASK_No
            End With
            datos.ActualizarRFTASKRow(data, 0)
            datos.CerrarConexion()
            Return
        Catch ex As Exception
            datos.wrtLogHtml("", ex.Message.ToString)
            datos.CerrarConexion()
            Return
        End Try
    End Sub

    Sub GenerarPDF()
        Dim Resultado As Boolean = False
        Dim parametro(2) As String

        parametro(0) = PeriodoAAAA
        parametro(1) = PeriodoMM
        parametro(2) = NumeroDocumento
        dtCab = datos.ConsultarBSSFFNCERdt(parametro, 0)
        If dtCab Is Nothing Then
            datos.WrtTxtError(LOG_File, "Certificado no encontrado.")
            Return
        End If

        dtDet = datos.ConsultarBSSFFNCERdt(parametro, 1)
        If dtDet Is Nothing Then
            datos.WrtTxtError(LOG_File, "Certificado sin detalle 1.")
            Return
        End If

        parametro(0) = PeriodoAAAA
        parametro(1) = PeriodoMM
        dtNotas = datos.ConsultarRMMTPIdt(parametro, 0)
        If dtNotas Is Nothing Then
            datos.WrtTxtError(LOG_File, "Sin declaracion.")
            Return
        End If

        ArchivoPDF = PDF_Folder & "\" & NumeroDocumento & ".pdf"
        'ArchivoPDF = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() & ".pdf"
        ArmaPDF()

        If bPRUEBAS Then
            'EJECUTA
            Dim prc As Process = New System.Diagnostics.Process()
            prc.StartInfo.FileName = ArchivoPDF
            prc.Start()
        End If
    End Sub

    Sub ArmaPDF()
        document = New Document(New Rectangle(288.0F, 144.0F), 36, 36, 140, 40)
        document.SetPageSize(iTextSharp.text.PageSize.LETTER)
        Console.WriteLine(document.PageSize.Width.ToString)
        document.AddTitle("Certificado de Retencion - " & NumeroDocumento)
        Dim es As eventos = New eventos()
        pw = PdfWriter.GetInstance(document, New FileStream(ArchivoPDF, FileMode.Create))
        pw.PageEvent = es
        document.Open()

        Dim unaTabla As PdfPTable = GenerarTabla()
        document.Add(unaTabla)

        Dim unaTabla2 As PdfPTable = detalleCerti()
        document.Add(unaTabla2)

        Dim unaTabla4 As PdfPTable = FinTabla()
        document.Add(unaTabla4)

        piet()

        document.Close()
    End Sub

    Public Function GenerarTabla() As PdfPTable
        Dim flag As Integer = 0
        Console.WriteLine("Generando una tabla...")
        Dim tablaPadre As New PdfPTable(1)
        tablaPadre.SetWidthPercentage(New Single() {615}, PageSize.LETTER)
        tablaPadre.HorizontalAlignment = Element.ALIGN_CENTER
        tablaPadre.DefaultCell.BorderWidth = 0.5

        Dim unaTabla As New PdfPTable(4)
        unaTabla.HorizontalAlignment = Element.ALIGN_CENTER
        unaTabla.DefaultCell.BorderWidth = 0
        unaTabla.SetWidthPercentage(New Single() {153, 153, 155, 154}, PageSize.LETTER)
        unaTabla.DefaultCell.FixedHeight = 100.0F

        Dim celda As PdfPCell

        celda = New PdfPCell(New Paragraph("CERTIFICADO RETENCIÓN IVA", FontFactory.GetFont("Arial", 11, Font.BOLD)))
        celda.BorderWidth = 0
        celda.Colspan = 4
        celda.PaddingBottom = 10
        celda.HorizontalAlignment = Element.ALIGN_CENTER
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("AÑO", FontFactory.GetFont("Arial", 9)))
        celda.BorderWidth = 0
        celda.BorderWidthTop = 0.5
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("PERIODO", FontFactory.GetFont("Arial", 9)))
        celda.BorderWidth = 0
        celda.BorderWidthTop = 0.5
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("FECHA EXPEDICIÓN", FontFactory.GetFont("Arial", 9)))
        celda.BorderWidth = 0
        celda.BorderWidthTop = 0.5
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("RÉGIMEN", FontFactory.GetFont("Arial", 9)))
        celda.BorderWidth = 0
        celda.BorderWidthTop = 0.5
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(PeriodoAAAA, FontFactory.GetFont("Arial", 9, Font.BOLD)))
        celda.BorderWidth = 0
        celda.PaddingBottom = 5
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(datos.ConsultarRFPARAMString(PeriodoMM, "CCCODE2", "FNZBIMES", 0), FontFactory.GetFont("Arial", 9, Font.BOLD)))
        celda.BorderWidth = 0
        celda.PaddingBottom = 5
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(Now.ToString("dd/MM/yyyy"), FontFactory.GetFont("Arial", 9, Font.BOLD)))
        celda.BorderWidth = 0
        celda.PaddingBottom = 5
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(IIf(dtCab.Rows(0).Item("VTAXCD").ToString.Substring(0, 2) = "RS", "SIMPLIFICADO", "COMÚN"), FontFactory.GetFont("Arial", 9, Font.BOLD)))
        celda.BorderWidth = 0
        celda.PaddingBottom = 5
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("CIUDAD DONDE SE PRACTICÓ LA RETENCIÓN", FontFactory.GetFont("Arial", 9)))
        celda.BorderWidth = 0
        celda.Colspan = 2
        celda.BorderWidthTop = 0.5
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("CIUDAD DONDE SE CONSIGNÓ LA RETENCIÓN", FontFactory.GetFont("Arial", 9)))
        celda.BorderWidth = 0
        celda.Colspan = 2
        celda.BorderWidthTop = 0.5
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("PEREIRA", FontFactory.GetFont("Arial", 9, Font.BOLD)))
        celda.BorderWidth = 0
        celda.Colspan = 2
        celda.PaddingBottom = 5
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("PEREIRA", FontFactory.GetFont("Arial", 9, Font.BOLD)))
        celda.BorderWidth = 0
        celda.Colspan = 2
        celda.PaddingBottom = 5
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("NOMBRE O RAZÓN SOCIAL", FontFactory.GetFont("Arial", 9)))
        celda.BorderWidth = 0
        celda.Colspan = 2
        celda.BorderWidthTop = 0.5
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("NIT", FontFactory.GetFont("Arial", 9)))
        celda.BorderWidth = 0
        celda.Colspan = 2
        celda.BorderWidthTop = 0.5
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(Trim(dtCab.Rows(0).Item("VNDNAM")), FontFactory.GetFont("Arial", 9, Font.BOLD)))
        celda.BorderWidth = 0
        celda.Colspan = 2
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(Trim(dtCab.Rows(0).Item("CTERNIT")), FontFactory.GetFont("Arial", 9, Font.BOLD)))
        celda.BorderWidth = 0
        celda.Colspan = 2
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Por los conceptos que se detallan a continuación :", FontFactory.GetFont("Arial", 9, Font.BOLD)))
        celda.BorderWidth = 0
        celda.Colspan = 4
        celda.PaddingBottom = 10
        celda.PaddingTop = 40
        unaTabla.AddCell(celda)

        Dim celdapapa As New PdfPCell(unaTabla)
        celdapapa.BorderWidth = 0
        tablaPadre.AddCell(celdapapa)

        Return tablaPadre
    End Function

    Public Function detalleCerti() As PdfPTable
        Dim flag As Integer = 0
        Dim totalIVA As Double = 0
        Dim totalTASA As Double = 0
        Dim totalRETENCION As Double = 0
        Console.WriteLine("Generando detalleCerti...")
        Dim tablaPadre As New PdfPTable(1)
        tablaPadre.SetWidthPercentage(New Single() {615}, PageSize.LETTER)
        tablaPadre.HorizontalAlignment = Element.ALIGN_CENTER
        tablaPadre.DefaultCell.BorderWidth = 0.5

        Dim unaTabla As New PdfPTable(5)
        unaTabla.HorizontalAlignment = Element.ALIGN_CENTER
        unaTabla.DefaultCell.BorderWidth = 0
        unaTabla.SetWidthPercentage(New Single() {265, 119, 75, 83, 73}, PageSize.LETTER)
        unaTabla.DefaultCell.FixedHeight = 100.0F

        Dim celda As PdfPCell

        Dim header1 As New PdfPCell(New Paragraph("CONCEPTO", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.WHITE)))
        header1.BorderColor = New iTextSharp.text.BaseColor(133, 133, 133)
        header1.FixedHeight = 19
        header1.BackgroundColor = New iTextSharp.text.BaseColor(194, 194, 194)
        header1.PaddingBottom = 3
        header1.HorizontalAlignment = 1
        header1.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header2 As New PdfPCell(New Paragraph("BASE", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.WHITE)))
        header2.BorderColor = New iTextSharp.text.BaseColor(133, 133, 133)
        header2.BackgroundColor = New iTextSharp.text.BaseColor(194, 194, 194)
        header2.PaddingBottom = 3
        header2.HorizontalAlignment = 1
        header2.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header3 As New PdfPCell(New Paragraph("IVA", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.WHITE)))
        header3.BorderColor = New iTextSharp.text.BaseColor(133, 133, 133)
        header3.BackgroundColor = New iTextSharp.text.BaseColor(194, 194, 194)
        header3.PaddingBottom = 3
        header3.HorizontalAlignment = 1
        header3.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header4 As New PdfPCell(New Paragraph("TASA %", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.WHITE)))
        header4.BorderColor = New iTextSharp.text.BaseColor(133, 133, 133)
        header4.BackgroundColor = New iTextSharp.text.BaseColor(194, 194, 194)
        header4.PaddingBottom = 3
        header4.HorizontalAlignment = 1
        header4.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header5 As New PdfPCell(New Paragraph("RETENCIÓN", FontFactory.GetFont("Arial", 9, Font.BOLD, BaseColor.WHITE)))
        header5.BorderColor = New iTextSharp.text.BaseColor(133, 133, 133)
        header5.BackgroundColor = New iTextSharp.text.BaseColor(194, 194, 194)
        header5.PaddingBottom = 3
        header5.HorizontalAlignment = 1
        header5.VerticalAlignment = Element.ALIGN_MIDDLE

        header1.BorderWidth = 0
        header2.BorderWidth = 0
        header3.BorderWidth = 0
        header4.BorderWidth = 0
        header5.BorderWidth = 0

        header2.BorderWidthLeft = 0.5
        header3.BorderWidthLeft = 0.5
        header4.BorderWidthLeft = 0.5
        header5.BorderWidthLeft = 0.5

        header1.BorderWidthBottom = 0.5
        header2.BorderWidthBottom = 0.5
        header3.BorderWidthBottom = 0.5
        header4.BorderWidthBottom = 0.5
        header5.BorderWidthBottom = 0.5

        unaTabla.AddCell(header1)
        unaTabla.AddCell(header2)
        unaTabla.AddCell(header3)
        unaTabla.AddCell(header4)
        unaTabla.AddCell(header5)
        unaTabla.HeaderRows = 1

        If dtDet IsNot Nothing Then
            For Each row As DataRow In dtDet.Rows
                totalIVA += row("BASE")
                totalTASA += row("IVA")
                totalRETENCION += row("RTI")
                celda = New PdfPCell(New Paragraph(row("CTASIVA"), FontFactory.GetFont("Arial", 9)))
                celda.HorizontalAlignment = Element.ALIGN_CENTER
                celda.BorderWidth = 0
                If flag = 1 Then
                    celda.BackgroundColor = New iTextSharp.text.BaseColor(237, 237, 237)
                End If
                unaTabla.AddCell(celda)

                celda = New PdfPCell(New Paragraph(FormatNumber(row("BASE"), 2), FontFactory.GetFont("Arial", 9)))
                celda.BorderWidth = 0
                celda.BorderWidthLeft = 0.5
                celda.BorderColor = New iTextSharp.text.BaseColor(133, 133, 133)
                celda.HorizontalAlignment = Element.ALIGN_RIGHT
                If flag = 1 Then
                    celda.BackgroundColor = New iTextSharp.text.BaseColor(237, 237, 237)
                End If
                unaTabla.AddCell(celda)

                celda = New PdfPCell(New Paragraph(FormatNumber(row("IVA"), 2), FontFactory.GetFont("Arial", 9)))
                celda.BorderWidth = 0
                celda.BorderWidthLeft = 0.5
                celda.BorderColor = New iTextSharp.text.BaseColor(133, 133, 133)
                celda.HorizontalAlignment = Element.ALIGN_RIGHT
                If flag = 1 Then
                    celda.BackgroundColor = New iTextSharp.text.BaseColor(237, 237, 237)
                End If
                unaTabla.AddCell(celda)

                celda = New PdfPCell(New Paragraph(FormatNumber(row("CPORRTI"), 2), FontFactory.GetFont("Arial", 9)))
                celda.BorderWidth = 0
                celda.BorderWidthLeft = 0.5
                celda.BorderColor = New iTextSharp.text.BaseColor(133, 133, 133)
                celda.HorizontalAlignment = Element.ALIGN_RIGHT
                If flag = 1 Then
                    celda.BackgroundColor = New iTextSharp.text.BaseColor(237, 237, 237)
                End If
                unaTabla.AddCell(celda)

                celda = New PdfPCell(New Paragraph(FormatNumber(row("RTI"), 2), FontFactory.GetFont("Arial", 9)))
                celda.BorderWidth = 0
                celda.BorderWidthLeft = 0.5
                celda.BorderColor = New iTextSharp.text.BaseColor(133, 133, 133)
                celda.HorizontalAlignment = Element.ALIGN_RIGHT
                If flag = 1 Then
                    celda.BackgroundColor = New iTextSharp.text.BaseColor(237, 237, 237)
                    flag = 0
                Else
                    flag += 1
                End If
                unaTabla.AddCell(celda)
            Next
        End If

        celda = New PdfPCell(New Paragraph("TOTAL $", FontFactory.GetFont("Arial", 9, Font.BOLD)))
        celda.BorderWidth = 0
        celda.BorderWidthTop = 0.5
        celda.BorderColor = New iTextSharp.text.BaseColor(133, 133, 133)
        celda.HorizontalAlignment = Element.ALIGN_RIGHT
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(FormatNumber(totalIVA, 2), FontFactory.GetFont("Arial", 9)))
        celda.BorderWidth = 0
        celda.BorderWidthLeft = 0.5
        celda.BorderWidthTop = 0.5
        celda.BorderColor = New iTextSharp.text.BaseColor(133, 133, 133)
        celda.HorizontalAlignment = Element.ALIGN_RIGHT
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(FormatNumber(totalTASA, 2), FontFactory.GetFont("Arial", 9)))
        celda.BorderWidth = 0
        celda.BorderWidthLeft = 0.5
        celda.BorderWidthTop = 0.5
        celda.BorderColor = New iTextSharp.text.BaseColor(133, 133, 133)
        celda.HorizontalAlignment = Element.ALIGN_RIGHT
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(""))
        celda.BorderWidth = 0
        celda.BorderWidthLeft = 0.5
        celda.BorderWidthTop = 0.5
        celda.BorderColor = New iTextSharp.text.BaseColor(133, 133, 133)
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(FormatNumber(totalRETENCION, 2), FontFactory.GetFont("Arial", 9)))
        celda.BorderWidth = 0
        celda.BorderWidthLeft = 0.5
        celda.BorderWidthTop = 0.5
        celda.BorderColor = New iTextSharp.text.BaseColor(133, 133, 133)
        celda.HorizontalAlignment = Element.ALIGN_RIGHT
        unaTabla.AddCell(celda)



        Dim celdapapa As New PdfPCell(unaTabla)
        celdapapa.BorderWidth = 0.5
        celdapapa.BorderColor = New iTextSharp.text.BaseColor(133, 133, 133)
        tablaPadre.AddCell(celdapapa)

        celda = New PdfPCell(New Paragraph(""))
        celda.FixedHeight = 16
        celda.BorderWidth = 0
        tablaPadre.AddCell(celda)

        Return tablaPadre
    End Function

    Public Function detalleCerti2() As PdfPTable
        Dim flag As Integer = 0
        Console.WriteLine("Generando detalleCerti2...")
        Dim tablaPadre As New PdfPTable(1)
        tablaPadre.SetWidthPercentage(New Single() {360}, PageSize.LETTER)
        tablaPadre.HorizontalAlignment = Element.ALIGN_CENTER
        tablaPadre.DefaultCell.BorderWidth = 0.5

        Dim unaTabla As New PdfPTable(4)
        unaTabla.HorizontalAlignment = Element.ALIGN_CENTER
        unaTabla.DefaultCell.BorderWidth = 0
        unaTabla.SetWidthPercentage(New Single() {100, 80, 100, 80}, PageSize.LETTER)
        unaTabla.DefaultCell.FixedHeight = 100.0F

        Dim celda As PdfPCell

        Dim header1 As New PdfPCell(New Paragraph("Declaración", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header1.PaddingBottom = 3
        header1.HorizontalAlignment = 1
        header1.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header2 As New PdfPCell(New Paragraph("Fecha", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header2.PaddingBottom = 3
        header2.HorizontalAlignment = 1
        header2.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header3 As New PdfPCell(New Paragraph("Código de Pago", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header3.PaddingBottom = 3
        header3.HorizontalAlignment = 1
        header3.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header4 As New PdfPCell(New Paragraph("Fecha", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header4.PaddingBottom = 3
        header4.HorizontalAlignment = 1
        header4.VerticalAlignment = Element.ALIGN_MIDDLE

        header1.BorderWidth = 0
        header2.BorderWidth = 0
        header3.BorderWidth = 0
        header4.BorderWidth = 0

        header2.BorderWidthLeft = 0.5
        header3.BorderWidthLeft = 0.5
        header4.BorderWidthLeft = 0.5

        header1.BorderWidthBottom = 0.5
        header2.BorderWidthBottom = 0.5
        header3.BorderWidthBottom = 0.5
        header4.BorderWidthBottom = 0.5

        unaTabla.AddCell(header1)
        unaTabla.AddCell(header2)
        unaTabla.AddCell(header3)
        unaTabla.AddCell(header4)
        unaTabla.HeaderRows = 1

        If dtNotas IsNot Nothing Then
            For Each row As DataRow In dtNotas.Rows
                celda = New PdfPCell(New Paragraph(row("RPNDEC"), FontFactory.GetFont("Arial", 8)))
                celda.HorizontalAlignment = Element.ALIGN_CENTER
                celda.BorderWidth = 0
                If flag = 1 Then
                    celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                End If
                unaTabla.AddCell(celda)

                celda = New PdfPCell(New Paragraph(CDate(row("RPFDEC")).ToString("yyyy-MM-dd"), FontFactory.GetFont("Arial", 8)))
                celda.BorderWidth = 0
                celda.BorderWidthLeft = 0.5
                celda.HorizontalAlignment = Element.ALIGN_CENTER
                If flag = 1 Then
                    celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                End If
                unaTabla.AddCell(celda)

                celda = New PdfPCell(New Paragraph(row("RPNSTI"), FontFactory.GetFont("Arial", 8)))
                celda.BorderWidth = 0
                celda.BorderWidthLeft = 0.5
                celda.HorizontalAlignment = Element.ALIGN_CENTER
                If flag = 1 Then
                    celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                End If
                unaTabla.AddCell(celda)

                celda = New PdfPCell(New Paragraph(CDate(row("RPFSTI")).ToString("yyyy-MM-dd"), FontFactory.GetFont("Arial", 8)))
                celda.BorderWidth = 0
                celda.BorderWidthLeft = 0.5
                celda.HorizontalAlignment = Element.ALIGN_CENTER
                If flag = 1 Then
                    celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
                    flag = 0
                Else
                    flag += 1
                End If
                unaTabla.AddCell(celda)
            Next
        End If



        Dim celdapapa As New PdfPCell(unaTabla)
        celdapapa.BorderWidth = 0.5
        tablaPadre.AddCell(celdapapa)

        celda = New PdfPCell(New Paragraph(""))
        celda.FixedHeight = 16
        celda.BorderWidth = 0
        tablaPadre.AddCell(celda)

        Return tablaPadre
    End Function

    Public Function FinTabla() As PdfPTable
        Dim flag As Integer = 0
        Console.WriteLine("Generando FinTabla...")
        Dim tablaPadre As New PdfPTable(1)
        tablaPadre.SetWidthPercentage(New Single() {615}, PageSize.LETTER)
        tablaPadre.HorizontalAlignment = Element.ALIGN_CENTER
        tablaPadre.DefaultCell.BorderWidth = 0.5

        Dim unaTabla As New PdfPTable(1)
        unaTabla.HorizontalAlignment = Element.ALIGN_CENTER
        unaTabla.DefaultCell.BorderWidth = 0
        unaTabla.SetWidthPercentage(New Single() {615}, PageSize.LETTER)
        unaTabla.DefaultCell.FixedHeight = 100.0F

        Dim celda As PdfPCell

        Dim texto As String = "La retención efectuada fue debidamente consignada en la Dirección de impuestos y Aduanas Nacionales de la Ciudad de"
        texto &= " PEREIRA. Se expide en concordancia con las disposiciones legales contenidas en el artículo 381 del estatuto tributario"

        celda = New PdfPCell(New Paragraph(texto, FontFactory.GetFont("Arial", 9)))
        celda.BorderWidth = 0
        celda.HorizontalAlignment = Element.ALIGN_JUSTIFIED
        celda.PaddingBottom = 40
        celda.PaddingTop = 60
        unaTabla.AddCell(celda)

        Dim texto2 As String = "NOTA: Este certificado impreso por computador se expide sin firma autógrafa de conformidad con lo"
        texto2 &= " dispuesto en el Decreto 380 de Febrero 27 de 1996."

        celda = New PdfPCell(New Paragraph(texto2, FontFactory.GetFont("Arial", 9)))
        celda.BorderWidth = 0
        celda.HorizontalAlignment = Element.ALIGN_JUSTIFIED
        unaTabla.AddCell(celda)

        Dim celdapapa As New PdfPCell(unaTabla)
        celdapapa.BorderWidth = 0
        tablaPadre.AddCell(celdapapa)

        Return tablaPadre
    End Function

    Public Function PieTabla() As PdfPTable
        Dim flag As Integer = 0
        Console.WriteLine("Generando FinTabla...")
        Dim tablaPadre As New PdfPTable(1)
        tablaPadre.SetWidthPercentage(New Single() {615}, PageSize.LETTER)
        tablaPadre.HorizontalAlignment = Element.ALIGN_CENTER
        tablaPadre.DefaultCell.BorderWidth = 0.5

        Dim unaTabla As New PdfPTable(1)
        unaTabla.HorizontalAlignment = Element.ALIGN_CENTER
        unaTabla.DefaultCell.BorderWidth = 0
        unaTabla.SetWidthPercentage(New Single() {615}, PageSize.LETTER)
        unaTabla.DefaultCell.FixedHeight = 100.0F

        Dim celda As PdfPCell
        Dim pieemp As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(PIE)
        celda = New PdfPCell(pieemp)

        celda.BorderWidth = 0

        pieemp.ScalePercent(65)

        celda.HorizontalAlignment = Element.ALIGN_CENTER
        celda.VerticalAlignment = Element.ALIGN_MIDDLE

        unaTabla.AddCell(celda)
        
        Dim celdapapa As New PdfPCell(unaTabla)
        celdapapa.BorderWidth = 0
        tablaPadre.AddCell(celdapapa)

        cb = pw.DirectContent
        cb.MoveTo(40, document.PageSize.GetBottom(50))
        cb.LineTo(document.PageSize.Width - 40, document.PageSize.GetBottom(50))
        cb.Stroke()

        Return tablaPadre
    End Function

    Private Sub piet()
        Dim pieemp As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(PIE)
        pieemp.ScalePercent(65)
        pieemp.SetAbsolutePosition(34.8, document.PageSize.GetBottom(50))
        cb = pw.DirectContent
        cb.MoveTo(34.8, document.PageSize.GetBottom(50))
        cb.AddImage(pieemp)
        'cb.LineTo(document.PageSize.Width - 40, document.PageSize.GetBottom(50))
        cb.Stroke()
    End Sub


End Module
