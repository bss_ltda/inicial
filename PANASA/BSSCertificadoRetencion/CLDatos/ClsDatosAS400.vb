﻿Imports IBM.Data.DB2.iSeries
Imports System.IO
Imports System.Text

Public Class ClsDatosAS400
    Public conexionIBM As New iDB2Connection()
    Private adapterIBM As iDB2DataAdapter = New iDB2DataAdapter()
    Private transIBM As iDB2Transaction
    Private builderIBM As iDB2CommandBuilder
    Private commandIBM As iDB2Command
    Private numFilas As Integer
    Private dt As DataTable
    Private ds As DataSet
    Private continuarguardando As Boolean
    Public appPath As String = Path.GetFullPath(My.Application.Info.DirectoryPath & "\Log\")
    Public mensaje As String
    Public fSql As String
    Private data
    Public lastError As String = ""
    Public ModuloActual As String = ""
    Public Piloto As String = My.Settings.LOCAL

#Region "ACTUALIZAR"

    ''' <summary>
    ''' Actualiza la tabla RFTASK 
    ''' </summary>
    ''' <param name="datos">RFTASK</param>
    ''' <param name="tipo">0- SET STS2 = @STS2 WHERE TNUMREG = @TNUMREG</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 23/05/2016</remarks>
    Public Function ActualizarRFTASKRow(ByVal datos As RFTASK, ByVal tipo As Integer) As Boolean
        Select Case tipo
            Case 0
                fSql = "UPDATE RFTASK SET STS2 = " & datos.STS2 & " WHERE TNUMREG = " & datos.TNUMREG
        End Select

        commandIBM = New iDB2Command
        commandIBM.Connection = conexionIBM
        Try
            commandIBM.CommandText = fSql
            commandIBM.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As Data.SqlClient.SqlException
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFTASKError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla RFTASK.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFTASKError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla RFTASK. Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                data = Nothing
                data = New RFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .OPERACION = "Actualizar RFTASK"
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarRFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

#End Region

#Region "CONSULTAR"

    ''' <summary>
    ''' Consulta la tabla BSSFFNCER con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por CYEAR(0) - CPERIO(1) y CSECUEN(2), 1- CYEAR(0) - CPERIO(1) y CSECUEN(2)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 23/05/2016</remarks>
    Public Function ConsultarBSSFFNCERdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        Select Case tipoparametro
            Case 0
                fSql = " SELECT CYEAR, CPERIO, CSECUEN, TRIM(VARCHAR(CTERNIT)) CTERNIT, VNDNAM, VENDOR, VMID, VMDATN, CUSR1, VMUF02, VTAXCD  "
                fSql &= " FROM BSSFFNCER INNER JOIN AVM BSSAVM ON "
                fSql &= " BSSFFNCER.CPROCOD = BSSAVM.VENDOR "
                fSql &= String.Format(" WHERE CYEAR = {0} ", parametroBusqueda(0))
                fSql &= String.Format(" AND CPERIO = {0} ", parametroBusqueda(1))
                fSql &= String.Format(" AND CSECUEN = {0} ", parametroBusqueda(2))
                fSql &= " AND CUSR1 = '0' "
                fSql &= " GROUP BY CYEAR, CPERIO, CSECUEN, CTERNIT, VNDNAM, VENDOR, VMID, VMDATN, CUSR1, VMUF02, VTAXCD  "
            Case 1
                fSql = " SELECT '' VACIO1, '' VACIO2, VARCHAR(CCUEIVA) CCUEIVA, VARCHAR(CTASIVA) CTASIVA, DOUBLE(CPORIVA) CPORIVA, VARCHAR(CCUERTI) CCUERTI, VARCHAR(CTASRTI) CTASRTI,  "
                fSql &= " DOUBLE(Abs(CPORRTI)) AS CPORRTI, DOUBLE(Sum(CVALBAS)) AS BASE,  "
                fSql &= " DOUBLE(Sum(CVALIVA)) AS IVA, DOUBLE(Abs(Sum(CVALRTI))) AS RTI  "
                fSql &= " FROM BSSFFNCER "
                fSql &= String.Format(" WHERE CYEAR = {0} ", parametroBusqueda(0))
                fSql &= String.Format(" AND CPERIO = {0} ", parametroBusqueda(1))
                fSql &= String.Format(" AND CSECUEN = {0} ", parametroBusqueda(2))
                fSql &= " AND CUSR1 = '0' "
                fSql &= " GROUP BY CCUEIVA, CTASIVA, CPORIVA, CCUERTI, CTASRTI, CPORRTI  "
            Case 2
                fSql = " SELECT CYEAR, CPERIO, CSECUEN, VNDNAM, VMDATN, CCNOT2 "
                fSql = fSql & " FROM BSSFFNCER INNER JOIN AVM BSSAVM ON "
                fSql = fSql & " BSSFFNCER.CPROCOD = BSSAVM.VENDOR "
                fSql = fSql & " INNER JOIN RFPARAM ON CCTABL = 'MESES' AND CCCODEN = CPERIO "
                fSql = fSql & " WHERE BSSFFNCER.CYEAR = " & parametroBusqueda(0)
                fSql = fSql & " AND BSSFFNCER.CPERIO = " & parametroBusqueda(1)
                fSql = fSql & " AND CUSR1 = '0' "
                If parametroBusqueda(2) <> "" Then
                    fSql = fSql & " AND CSECUEN = " & parametroBusqueda(2)
                End If
                fSql = fSql & " AND CSECUEN >= 63"
                fSql = fSql & " GROUP BY CYEAR, CPERIO, CSECUEN, VNDNAM, VMDATN, CCNOT2 "
                fSql = fSql & " ORDER BY CSECUEN"
        End Select
        'fSql &= " FETCH FIRST 20 ROWS ONLY "
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)

        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla RMMTPI con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por RPAFS(0) - RPBIM(1)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 23/05/2016</remarks>
    Public Function ConsultarRMMTPIdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        Select Case tipoparametro
            Case 0
                 fSql = " SELECT '' VACIO1, '' VACIO2, RPNDEC, RPFDEC, RPNSTI, RPFSTI"
                fSql &= " FROM RMMTPI"
                fSql &= String.Format(" WHERE RPAFS = {0} ", parametroBusqueda(0))
                fSql &= String.Format(" AND RPBIM = {0} ", parametroBusqueda(1))
        End Select
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)

        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla RFPARAM con un determinado parametro de busqueda.Retorna un String
    ''' </summary>
    ''' <param name="prm">parametro de Busqueda</param>
    ''' <param name="Campo"></param>
    ''' <returns>Un String</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 23/05/2016</remarks>
    Public Function ConsultarRFPARAMString(ByVal prm As String, ByVal Campo As String, ByVal cctabl As String, ByVal tipo As Integer) As String
        dt = Nothing


        If Campo = "" Then
            Campo = "CCNOT1"
        End If
        If cctabl = "" Then
            cctabl = "LXLONG"
        End If
        Select Case tipo
            Case 0
                fSql = "SELECT " & Campo & " AS DATO "
                fSql &= " FROM RFPARAM "
                fSql &= " WHERE CCTABL='" & cctabl & "' AND UPPER(CCCODE) = UPPER('" & prm & "')"
            Case 1
                fSql = " SELECT CCNOT2 AS DATO FROM RFPARAM WHERE CCTABL = '" & cctabl & "' AND CCCODEN = " & prm
        End Select
        

        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)

        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt.Rows(0).Item("DATO")
    End Function

    ''' <summary>
    ''' Consulta la tabla ZCCL01 con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por CCCODE(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 23/05/2016</remarks>
    Public Function ConsultarZCCL01dt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT CCSDSC, CCUDC1, CCNOT2, CCDESC  FROM ZCCL01 WHERE CCTABL='RFVBVER' AND UPPER(CCCODE) = '" & parametroBusqueda(0) & "'"
        End Select
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)

        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

#End Region

#Region "FUNCIONES"

    Public Sub WrtTxtError(Archivo As String, ByVal Texto As String)
        Dim thisFile As System.IO.FileInfo
        Try
            Using w As StreamWriter = File.AppendText(Archivo)
                Log(Texto, w)
            End Using
        Catch ex As Exception
            thisFile = My.Computer.FileSystem.GetFileInfo(Archivo)
            ' Si el directorio no existe, crearlo
            If Not Directory.Exists(thisFile.Directory.ToString) Then
                Directory.CreateDirectory(thisFile.Directory.ToString)
            End If
            Try
                Using w As StreamWriter = File.AppendText(Archivo)
                    Log(Texto, w)
                End Using
            Catch ex1 As Exception

            End Try
        End Try
    End Sub

    Public Sub Log(logMessage As String, w As TextWriter)
        w.Write(vbCrLf + "Entrada de Log : ")
        w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString())
        w.WriteLine("  :{0}", logMessage)
        w.WriteLine(" ")
    End Sub

    Public Function CheckVer() As Boolean
        Dim lVer As String
        Dim msg As String
        Dim NomApp As String
        Dim App As clsApp = New clsApp(System.Reflection.Assembly.GetExecutingAssembly)
        Dim Resultado As Boolean = True
        Dim gsAppName As String
        Dim gsAppPath
        Dim gsAppVersion As String
        Dim dtZCCL01 As DataTable
        Dim parametro(0) As String


        gsAppPath = App.Path
        gsAppName = App.EXEName
        gsAppVersion = App.Major.ToString & "." & App.Minor.ToString & "." & App.Revision.ToString

        lVer = "No hay registro"

        parametro(0) = gsAppName.ToUpper
        dtZCCL01 = ConsultarZCCL01dt(parametro, 0)
        If dtZCCL01 IsNot Nothing Then
            lVer = Trim(dtZCCL01.Rows(0).Item("CCSDSC"))
            NomApp = dtZCCL01.Rows(0).Item("CCDESC")
            If InStr(lVer, "*NOCHK") > 0 Then
                Resultado = True
            ElseIf InStr(lVer, "(" & gsAppVersion & ")") > 0 Then
                Resultado = True
            End If
            If Resultado And dtZCCL01.Rows(0).Item("CCUDC1") = 0 Then
                msg = "La aplicacion " & NomApp & " no se puede usar en este momento." & vbCr
                msg = msg & "Razon: " & vbCr
                msg = msg & "=========================================" & vbCr
                If Trim(dtZCCL01.Rows(0).Item("CCNOT2")) = "" Then
                    msg = msg & "Aplicacion en Mantenimiento." & vbCr
                Else
                    msg = msg & dtZCCL01.Rows(0).Item("CCNOT2") & vbCr
                End If
                msg = msg & "=========================================" & vbCr
                Return False
            End If
        End If

        If Not Resultado Then
            msg = "Aplicacion " & App.EXEName.ToUpper & vbCr & _
                   "=========================================" & vbCr & _
                   "Versión incorrecta del programa." & vbCr & _
                   "Versión Registrada: " & lVer & vbCr & _
                   "Versión Actual: " & "(" & gsAppVersion & ")"
            lastError = msg
        End If

        Return Resultado

    End Function

    Public Sub WrtSqlError(sql As String, Descrip As String)
        Dim aApl() As String
        Dim sApp As String = "VB.NET"

        Try
            aApl = Split(System.Reflection.Assembly.GetExecutingAssembly.FullName, ", ")
            If UBound(aApl) > 0 Then
                sApp = aApl(0) & IIf(ModuloActual <> "", "." & ModuloActual, "")
            End If

            data = Nothing
            data = New RFLOG
            With data
                .USUARIO = System.Net.Dns.GetHostName()
                .PROGRAMA = sApp
                .ALERT = "1"
                .EVENTO = Replace(Descrip, "'", "''")
                .TXTSQL = Replace(sql, "'", "''")
            End With
            GuardarRFLOGRow(data)

        Catch ex As Exception

        End Try

    End Sub

    Public Function AbrirConexion() As Boolean
        Dim abierta As Boolean = False
        Console.WriteLine("Abriendo conexion")
        Try
            If Not Directory.Exists(appPath) Then
                Directory.CreateDirectory(appPath)
            End If
            If conexionIBM.State = ConnectionState.Open Then
                abierta = True
            Else
                If Piloto = "SI" Then
                    conexionIBM = New iDB2Connection(My.Settings.BASEDATOSLOCAL)
                Else
                    conexionIBM = New iDB2Connection(My.Settings.BASEDATOS)
                End If

                conexionIBM.Open()
                abierta = True
            End If
            Console.WriteLine(abierta)
        Catch ex1 As iDB2Exception
            abierta = False
            mensaje = ex1.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo realizar la conexión.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex1.InnerException.ToString & vbCrLf & "Conexion: " & conexionIBM.ConnectionString & vbCrLf & ex1.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Catch ex As Exception
            abierta = False
            mensaje = ex.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo realizar la conexión.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.InnerException.ToString & vbCrLf & "Conexion: " & conexionIBM.ConnectionString & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try
        Return abierta
    End Function

    Public Function CerrarConexion() As Boolean
        Dim cerrada As Boolean = False
        Try
            conexionIBM.Close()
            cerrada = True
        Catch ex1 As iDB2Exception
            cerrada = False
            mensaje = ex1.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo Cerrar la conexión.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex1.InnerException.ToString & vbCrLf & "Conexion: " & conexionIBM.ConnectionString & vbCrLf & ex1.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Catch ex As Exception
            cerrada = False
            mensaje = ex.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo Cerrar la conexión.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.InnerException.ToString & vbCrLf & "Conexion: " & conexionIBM.ConnectionString & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try
        Return cerrada
    End Function

    Public Sub wrtLog(Descrip As String, gsKeyWords As String, Sql As String)
        Console.WriteLine(Descrip)
        data = Nothing
        data = New RFLOG
        With data
            .USUARIO = System.Net.Dns.GetHostName()
            .OPERACION = "Correo"
            .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
            .ALERT = "1"
            .EVENTO = Replace(Descrip, "'", "''")
            .LKEY = gsKeyWords
            .TXTSQL = Sql
        End With
        GuardarRFLOGRow(data)
    End Sub

    Public Sub wrtLogHtml(sql As String, ByVal Descrip As String)
        Dim fEntrada As String = ""
        Dim linea As String = Format(Now(), "yyyy-MM-dd HH:mm:ss") & "|"
        Dim strStreamW As Stream = Nothing
        Dim sApp As String = My.Application.Info.ProductName & "." & _
                                   My.Application.Info.Version.Major & "." & _
                                   My.Application.Info.Version.Minor & "." & _
                                   My.Application.Info.Version.Revision

        Descrip = Descrip.Replace(vbCrLf, vbCr)
        Descrip = Descrip.Replace(vbLf, vbCr)
        Descrip = Descrip.Replace(vbCr, "<BR/>")

        'If gsKeyWords.Trim <> "" Then
        '    Descrip = gsKeyWords & "<br>" & Descrip
        'End If

        linea &= System.Net.Dns.GetHostName() & "|" & sApp & "|" & Descrip.Replace("|", "?") & "|" & sql.Replace("|", "?")

        fEntrada = Replace(appPath & "\" & Format(Now(), "yyyyMM") & ".csv", "\\", "\")
        If Not Directory.Exists(appPath) Then
            Directory.CreateDirectory(appPath)
        End If
        If File.Exists(fEntrada) Then
            strStreamW = File.Open(fEntrada, FileMode.Open) 'Abrimos el archivo
        Else
            strStreamW = File.Create(fEntrada) ' lo creamos
        End If
        strStreamW.Close()
        Try
            ' Create an instance of StreamReader to read from a file.
            Dim sr As StreamReader = New StreamReader(fEntrada)
            Dim sb As New StringBuilder()
            sb.Append(sr.ReadToEnd())
            sb.Append(linea)
            sr.Close()
            Dim sw As StreamWriter = New StreamWriter(fEntrada)
            sw.WriteLine(sb.ToString())
            sw.Close()

        Catch E As Exception

        End Try
        'Application.Exit()
    End Sub

    Public Function Ceros(val As String, c As Integer)
        Dim result As String
        Dim sCeros As String = New String("0", c)
        result = Right(sCeros & val, c)
        Return result
    End Function

#End Region

#Region "GUARDAR"

    ''' <summary>
    ''' Guarda RFLOG
    ''' </summary>
    ''' <param name="datos">RFLOG</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 23/05/2016</remarks>
    Public Function GuardarRFLOGRow(ByVal datos As RFLOG) As Boolean

        fSql = "INSERT INTO RFLOG (USUARIO, OPERACION, PROGRAMA, EVENTO, TXTSQL, ALERT, LKEY)"
        fSql &= " VALUES ('" & datos.USUARIO & "', '" & datos.OPERACION & "', '" & datos.PROGRAMA & "', '" & datos.EVENTO.Replace("'", "''") & "', '" & IIf(datos.TXTSQL = "", " ", datos.TXTSQL.Replace("'", "''")) & "', " & datos.ALERT & ", '" & IIf(datos.LKEY = "", DBNull.Value, datos.LKEY) & "')"
        'System.Net.Dns.GetHostName()
        'My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString

        commandIBM = New iDB2Command
        commandIBM.Connection = conexionIBM
        Try
            commandIBM.CommandText = fSql
            commandIBM.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As Data.SqlClient.SqlException
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo guardar en la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Catch ex As Exception
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo guardar en la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Finally

        End Try
        Return continuarguardando
    End Function

#End Region

End Class

Public Class RFLOG
    Public USUARIO As String
    Public PROGRAMA As String
    Public OPERACION As String
    Public EVENTO As String
    Public TXTSQL As String
    Public ALERT As String
    Public LKEY As String
End Class

Public Class RFTASK
    Public TLXMSG As String
    Public STS1 As String
    Public STS2 As String
    Public TMSG As String
    Public TNUMREG As String
End Class
