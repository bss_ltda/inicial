﻿Public Class class_EnviarCorreo
    Public Asunto As String
    Public url As String
    Public datos
    Dim mht As Chilkat.Mht
    Dim email As Chilkat.Email
    Dim mailman As Chilkat.MailMan
    Dim sErrorXML, sErrorHTML, sErrorTXT

    Function InicializaMHT()
        mht = New Chilkat.Mht
        mailman = New Chilkat.MailMan
        email = New Chilkat.Email

        mailman.UnlockComponent("FMANRQ.CB40517_Le6wzL45oMlf")
        mht.UnlockComponent("FMANRQ.CB40517_Le6wzL45oMlf")

        mailman.SmtpHost = datos.ConsultarRFPARAMString("SMTPHOST", "", "", 0)
        mailman.SmtpUsername = datos.ConsultarRFPARAMString("SMTPUSERNAME", "", "", 0)
        mailman.SmtpPassword = datos.ConsultarRFPARAMString("SMTPPASSWORD", "", "", 0)

        InicializaMHT = correoMHT(email, mht, url)
    End Function

    'correoMHT()
    Function correoMHT(email, mht, url)
        Dim emlStr

        mht.UseCids = 1
        emlStr = mht.GetEML(url)
        If (emlStr = vbNullString) Then
            Console.WriteLine(mht.LastErrorText)
        End If
        correoMHT = email.SetFromMimeText(emlStr)
        If (correoMHT <> 1) Then
            Console.WriteLine(email.LastErrorText)
        End If

    End Function

    Sub Desde(Nombre, Correo)
        email.fromName = Nombre
        email.FromAddress = Correo
    End Sub

    Sub Destinatario(Nombre, Correo)
        email.AddTo(Nombre, Correo)
        email.AddBcc("", "AlertasCorreo2015@gmail.com")
        'email.AddBcc("", "agente.retenedor@feduro.net")
    End Sub

    Sub DestinatarioUsuario(Usuario)
        'Dim rs, fSql

        'fSql = " SELECT UNOM, UEMAIL FROM RCAU WHERE UUSR = '" & Usuario & "' "
        'rs = ExecuteSql(fSql)
        'If Not rs.EOF Then
        '    email.AddTo(rs("UNOM").Value, rs("UEMAIL").Value)
        'End If
        'rs.Close()
        'rs = Nothing

    End Sub

    Function Envia()
        email.Subject = Asunto
        If email.fromName = "" Then
            email.FromName = datos.ConsultarRFPARAMString("NOMBRE", "", "", 0)
            email.FromAddress = datos.ConsultarRFPARAMString("SMTPUSERMAIL", "", "", 0)
        End If

        If mailman.SendEmail(email) Then
            Envia = True
        Else
            Envia = False
            sErrorHTML = mailman.LastErrorHtml
            sErrorTXT = mailman.LastErrorText
            sErrorXML = mailman.LastErrorXml
        End If
        If (mailman.CloseSmtpConnection() <> 1) Then

        End If
        email = Nothing
        mailman = Nothing
        mht = Nothing
    End Function

    Function Archivo(nombreArchivo As String) As Boolean
        Dim contentType As String
        contentType = email.AddFileAttachment(nombreArchivo)
        If (contentType = vbNullString) Then
            Console.WriteLine(email.LastErrorText)
            Return False
        End If
        Return True
    End Function

    Function ErrorHtml()
        ErrorHtml = sErrorHTML
    End Function

    Function ErrorTXT()
        ErrorTXT = sErrorTXT
    End Function

    Function ErrorXML()
        ErrorXML = sErrorXML
    End Function

End Class
