﻿Imports System.IO
Module Module1

    Public Class Archivo
        Public Modulo As String
        Public Nombre As String
        Public Extension As String
        Public SeparadorCampo As String
        Public FinRegistro As String
        Public Descripcion As String
        Public ArchivoSalida As String
        Public Columnas As Columna()
    End Class

    Public Class Columna
        Public Titulo As String
        Public Tipo As String
        Public PermiteNulo As String
        Public ValorPorDefecto As String

        Public Sub New()
            Titulo = ""
            Tipo = ""
            PermiteNulo = ""
            ValorPorDefecto = ""
        End Sub
    End Class

    Public Class colArchivo
        Public col As Single
        Public tipo As String
        Public tit As String
        Public fmt As String
        Public bNulo As Boolean
        Public defaultValue As String
    End Class

    Public Class FechaHora
        Public yyyy As Integer
        Public mm As Integer
        Public dd As Integer
        Public hh As Integer
        Public nn As Integer
        Public ss As Integer
        Public ampm As String

        Public pos_yyyy As Single
        Public pos_mm As Single
        Public pos_dd As Single
        Public pos_hh As Single
        Public pos_nn As Single
        Public pos_ss As Single
        Public pos_ampm As Single

        Public Sub New()
            pos_yyyy = -1
            pos_mm = -1
            pos_dd = -1
            pos_hh = -1
            pos_nn = -1
            pos_ss = -1
            pos_ampm = -1

            yyyy = 0
            mm = 0
            dd = 0
            hh = 0
            nn = 0
            ss = 0
            ampm = ""

        End Sub

        Public Function getFechaFmt(fmt As String) As String

            If pos_ampm <> -1 Then
                Dim ampmLoc As String
                ampmLoc = ampm.Replace(".", "").ToUpper()
                ampmLoc = ampmLoc.Replace(" ", "")
                Select Case ampmLoc
                    Case "AM"
                        If hh = 12 Then
                            hh = 0
                        End If
                    Case "PM"
                        If hh <> 12 Then
                            hh += 12
                        End If
                End Select
            End If

            If yyyy < 100 Then
                yyyy += 2000
            End If

            Dim NewDateTime As Date = New Date(yyyy, mm, dd, hh, nn, ss, 0)
            Return NewDateTime.ToString(fmt)

        End Function


    End Class


    Public archivoList As List(Of Archivo)
    Public fmtCols As List(Of colArchivo)

    Sub CargaArchivos()
        Dim dataPath As String = System.Windows.Forms.Application.StartupPath & "\..\..\data\"
        Dim customerListPath = Path.GetFullPath(Path.Combine(dataPath, "archivos.xml"))

        'Dim customerListPath = "C:\Dropbox\AppBSS\VisualStudio\Natura\BSSValidaArchivos\BSSValidaArchivos\data\archivos.xml"


        Dim list = XDocument.Load(customerListPath).Root.Elements("Archivo")

        archivoList = (From ee In list
                       Select New Archivo With {
                            .Modulo = CStr(ee.<Modulo>.Value),
                            .Nombre = CStr(ee.<Nombre>.Value),
                            .Extension = CStr(ee.<Extension>.Value),
                            .SeparadorCampo = CStr(ee.<SeparadorCampo>.Value),
                            .FinRegistro = CStr(ee.<FinRegistro>.Value),
                            .ArchivoSalida = CStr(ee.<ArchivoSalida>.Value),
                            .Descripcion = CStr(ee.<Descripcion>.Value),
                            .Columnas = (
                                       From o In ee...<Columnas>...<Columna>
                                       Select New Columna With {
                                            .Titulo = CStr(o.<Titulo>.Value),
                                            .Tipo = CStr(o.<Tipo>.Value),
                                            .PermiteNulo = CStr(o.<PermiteNulo>.Value),
                                            .ValorPorDefecto = CStr(o.<ValorPorDefecto>.Value)}).ToArray()
                        }).ToList()

    End Sub

    Function GetDirPath(ByVal file As String) As String
        Dim fi As New FileInfo(file)
        Return fi.Directory.ToString
    End Function

    Sub dsp(txt)
        Console.WriteLine(txt)
    End Sub


    Function transform(dato As String, tipo As String, formato As String, bNulo As Boolean, valorDefecto As String, punto As Boolean, ByRef msgErr As String) As String

        dato = dato.Trim()
        If Left(dato, 1) = """" Then
            dato = Mid(dato, 2)
        End If

        If Right(dato, 1) = """" Then
            dato = Mid(dato, 1, Len(dato) - 1)
        End If

        'Ejemplo que funcion con el archivo de Atencion al Cliente<Tipo>D,in=dd/MM/yyyy hh:mm:ss t.t.|sepFecha=/|sepHora=:|ampm=t.t.|out=yyyy-MM-dd HH:mm</Tipo>


        Select Case tipo
            Case "A"
                dato = dato.Replace("|", "?")
            Case "N"
                If punto Then
                    dato = dato.Replace(",", "")
                Else
                    dato = dato.Replace(".", "")
                    dato = dato.Replace(",", ".")
                End If
                If dato = "" And bNulo Then
                Else
                    If Not IsNumeric(dato) Then
                        msgErr = "El dato no es numerico [" & dato & "]"
                    End If
                End If
            Case "D"
                Dim aFmtFec() As String = Split(formato, "|")
                Dim aPrm() As String
                Dim inFec, sepFecha, sepHora, ampm, outFec As String
                inFec = "" : sepFecha = "" : sepHora = "" : ampm = "" : outFec = ""

                dato = dato.Replace("a. m.", "a.m.")
                dato = dato.Replace("p. m.", "p.m.")
                dato = dato.Replace("A. m.", "a.m.")
                dato = dato.Replace("P. m.", "p.m.")
                dato = dato.Replace("A. M.", "a.m.")
                dato = dato.Replace("P. M.", "p.m.")

                Dim Fecha As New FechaHora
                For Each elem In aFmtFec
                    aPrm = Split(elem, "=")
                    Select Case aPrm(0)
                        Case "in"
                            inFec = aPrm(1)
                        Case "sepFecha"
                            sepFecha = aPrm(1)
                        Case "sepHora"
                            sepHora = aPrm(1)
                        Case "ampm"
                            ampm = aPrm(1)
                        Case "out"
                            outFec = aPrm(1)
                    End Select
                Next

                Dim fmtFecIn As String
                Dim fec As New FechaHora
                fmtFecIn = inFec.Replace(" ", ",")
                If sepFecha <> "" Then
                    fmtFecIn = fmtFecIn.Replace(sepFecha, ",")
                End If
                If sepHora <> "" Then
                    fmtFecIn = fmtFecIn.Replace(sepHora, ",")
                End If

                aPrm = Split(fmtFecIn, ",")
                For k = 0 To UBound(aPrm)
                    Select Case aPrm(k)
                        Case "yyyy", "yy"
                            fec.pos_yyyy = k
                        Case "MM"
                            fec.pos_mm = k
                        Case "dd"
                            fec.pos_dd = k
                        Case "hh"
                            fec.pos_hh = k
                        Case "mm"
                            fec.pos_nn = k
                        Case "ss"
                            fec.pos_ss = k
                        Case ampm
                            fec.pos_ampm = k
                    End Select
                Next

                '<Tipo>D,in=dd/MM/yyyy hh:mm:ss a.m.|sepFecha=/|sepHora=:|ampm=a.m.|out=yyyy-MM-dd hh:mm</Tipo>

                fmtFecIn = dato.Replace(" ", ",")
                If sepFecha <> "" Then
                    fmtFecIn = fmtFecIn.Replace(sepFecha, ",")
                End If
                If sepHora <> "" Then
                    fmtFecIn = fmtFecIn.Replace(sepHora, ",")
                End If
                aPrm = Split(fmtFecIn, ",")
                With fec
                    If .pos_yyyy <> -1 Then .yyyy = aPrm(.pos_yyyy)
                    If .pos_mm <> -1 Then .mm = aPrm(.pos_mm)
                    If .pos_dd <> -1 Then .dd = aPrm(.pos_dd)
                    If .pos_hh <> -1 Then .hh = aPrm(.pos_hh)
                    If .pos_nn <> -1 Then .nn = aPrm(.pos_nn)
                    If .pos_ss <> -1 Then .ss = aPrm(.pos_ss)
                    If .pos_ampm <> -1 Then .ampm = aPrm(.pos_ampm)
                End With

                dato = fec.getFechaFmt(outFec)
        End Select

        Return dato
    End Function
End Module

