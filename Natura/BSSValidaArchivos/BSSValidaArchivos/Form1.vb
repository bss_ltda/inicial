﻿Imports System.IO
Imports System.Text

Public Class Form1
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        CargaArchivos()
        For Each arch In archivoList
            Me.cbArchivos.Items.Add(arch.Nombre)
            Me.Text = arch.Modulo
            Me.lblTit.Text = "Validador de Archivos para " & arch.Modulo
        Next
        Me.Label1.Text = ""
        Me.PanelPrincipal.Enabled = False


    End Sub


    Private Sub cbArchivos_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbArchivos.SelectedIndexChanged
        Dim archs = From Archivo In archivoList
                    Where Archivo.Nombre = Me.cbArchivos.Text
                    Select Archivo
        Dim colA As colArchivo
        Dim aTipo() As String

        fmtCols = New List(Of colArchivo)

        For Each arch In archs
            Me.Label1.Text = arch.Descripcion
            For Each col In arch.Columnas
                colA = New colArchivo
                With colA
                    .col = -1
                    .tit = col.Titulo
                    aTipo = Split(col.Tipo, ",")
                    .tipo = aTipo(0)
                    .fmt = aTipo(1)
                    .defaultValue = col.ValorPorDefecto
                    If col.PermiteNulo.Trim().ToUpper() = "SI" Then
                        .bNulo = True
                    Else
                        .bNulo = False
                    End If
                End With
                fmtCols.Add(colA)
            Next
        Next

        fmtCols(5).col = 1
        fmtCols = fmtCols.OrderBy(Function(x) x.col).ToList()

        Me.PanelPrincipal.Enabled = True



    End Sub

    'Para el formato de hora en 24 horas se debe poner HH (en mayuscula)
    '<Tipo> D, in = dd / MM / yyyy hh:mm:ss t.t.|sepFecha=/|sepHora=:|ampm=t.t.|out=yyyy-MM-dd HH:mm</Tipo>

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        Dim openFileDialog1 As New OpenFileDialog()
        Dim sepCampo As String = ""
        Dim sep As String = ""
        Dim finReg As String = ""
        Dim ext As String = ""
        Dim salida As String = ""
        Dim i As Int32


        Dim archs = From Archivo In archivoList
                    Where Archivo.Nombre = Me.cbArchivos.Text
                    Select Archivo
        Dim colA As colArchivo
        Dim aTipo() As String

        Me.Button1.Enabled = False

        fmtCols = New List(Of colArchivo)

        For Each arch In archs
            Me.Label1.Text = arch.Descripcion
            ext = arch.Extension
            salida = arch.ArchivoSalida

            Select Case arch.FinRegistro
                Case "CRLF"
                    finReg = vbCrLf
                Case "CR"
                    finReg = vbCrLf
                Case "LF"
                    finReg = vbCrLf
            End Select

            Select Case arch.SeparadorCampo
                Case "TAB"
                    sepCampo = vbTab
                Case Else
                    sepCampo = arch.SeparadorCampo
            End Select

            For Each col In arch.Columnas
                colA = New colArchivo
                With colA
                    .col = -1
                    .tit = col.Titulo
                    aTipo = Split(col.Tipo, ",")
                    .tipo = aTipo(0)
                    .fmt = aTipo(1)
                    .defaultValue = col.ValorPorDefecto
                    If col.PermiteNulo.Trim().ToUpper() = "SI" Then
                        .bNulo = True
                    Else
                        .bNulo = False
                    End If
                End With
                fmtCols.Add(colA)
            Next
        Next

        aTipo = Split(salida, ",")
        If UBound(aTipo) = 2 Then
            salida = aTipo(0) & CDate(Me.dtProceso.Value).ToString(aTipo(1)) & "." & aTipo(2)
        End If
        openFileDialog1.Filter = "archivos (*." & ext & ")|*." & ext
        openFileDialog1.FilterIndex = 2
        openFileDialog1.RestoreDirectory = True

        If openFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then

            Me.txtFileIn.Text = openFileDialog1.FileName()

        End If


        Dim bPrimeraFila As Boolean = False
        Dim datos = File.ReadAllText(Me.txtFileIn.Text, Encoding.Default)
        Dim aDatos() As String = Split(datos, finReg)
        Dim aLinea() As String
        Dim strSalida As New StringBuilder

        datos = ""
        For i = 0 To UBound(aDatos)
            For j = 0 To fmtCols.Count - 1
                If InStr(aDatos(i).ToUpper, fmtCols(j).tit.ToUpper) = 0 Then
                    bPrimeraFila = False
                    Exit For
                Else
                    fmtCols(j).col = j
                    bPrimeraFila = True
                End If
            Next
            If bPrimeraFila Then
                Exit For
            ElseIf i > 20 Then
                Exit For
            End If
        Next

        If Not bPrimeraFila Then
            MsgBox("Archivo no Corresponde", vbExclamation, "Validacion de Datos")
            Exit Sub
        End If

        aLinea = Split(aDatos(i), sepCampo)
        For k = 0 To UBound(aLinea)
            For Each col In fmtCols
                If aLinea(k).Trim().ToUpper = col.tit.Trim().ToUpper() Then
                    col.col = k
                    Exit For
                End If
            Next
        Next

        finReg = vbCrLf

        sep = ""
        For Each elem In fmtCols
            If elem.col <> -1 Then
                strSalida.Append(sep & elem.tit)
                sep = "|"
            End If
        Next
        strSalida.Append(finReg)

        Dim msgErr As String = ""
        For k = i + 1 To UBound(aDatos)
            aLinea = Split(aDatos(k), sepCampo)
            Debug.Print(k, aDatos(k))
            If UBound(aLinea) + 1 >= fmtCols.Count Then
                If k Mod 100 = 0 Then
                    Me.ProgressBar1.Value = k / UBound(aDatos) * 100
                End If
                sep = ""
                For Each elem In fmtCols
                    If elem.col <> -1 Then
                        strSalida.Append(sep & transform(aLinea(elem.col), elem.tipo, elem.fmt, elem.bNulo, elem.defaultValue, Me.sepDecPunto.Checked, msgErr))
                        If msgErr <> "" Then
                            MsgBox("Fila: " & k & vbCrLf & "Columna: " & elem.tit & vbCrLf & msgErr & vbCrLf & "Confirme el separador decimal", vbExclamation, "Error Validando Datos")
                            Exit Sub
                        End If
                        sep = "|"
                    End If
                Next
                strSalida.Append(finReg)
            End If
        Next
        File.WriteAllText(GetDirPath(openFileDialog1.FileName) & "\" & salida, strSalida.ToString())
        Me.txtFileOut.Text = GetDirPath(openFileDialog1.FileName) & "\" & salida
        Me.txtFileOut.BackColor = Color.Aquamarine
        Me.ProgressBar1.Value = 100

        Me.Button1.Enabled = True
        MsgBox("Archivo Generado" & vbCrLf & salida, vbInformation, "Validacion de Archivos")


    End Sub
End Class
