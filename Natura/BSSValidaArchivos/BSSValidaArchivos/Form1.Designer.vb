﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cbArchivos = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.PanelPrincipal = New System.Windows.Forms.Panel()
        Me.txtFileOut = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtFileIn = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dtProceso = New System.Windows.Forms.DateTimePicker()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.sepDecComa = New System.Windows.Forms.RadioButton()
        Me.sepDecPunto = New System.Windows.Forms.RadioButton()
        Me.lblArchivo = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.lblTit = New System.Windows.Forms.Label()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.PanelPrincipal.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cbArchivos
        '
        Me.cbArchivos.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbArchivos.FormattingEnabled = True
        Me.cbArchivos.Location = New System.Drawing.Point(91, 60)
        Me.cbArchivos.Name = "cbArchivos"
        Me.cbArchivos.Size = New System.Drawing.Size(302, 24)
        Me.cbArchivos.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(418, 63)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 16)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Label1"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(32, 63)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Archivo"
        '
        'PanelPrincipal
        '
        Me.PanelPrincipal.Controls.Add(Me.ProgressBar1)
        Me.PanelPrincipal.Controls.Add(Me.txtFileOut)
        Me.PanelPrincipal.Controls.Add(Me.Label5)
        Me.PanelPrincipal.Controls.Add(Me.txtFileIn)
        Me.PanelPrincipal.Controls.Add(Me.Panel2)
        Me.PanelPrincipal.Controls.Add(Me.Panel1)
        Me.PanelPrincipal.Controls.Add(Me.lblArchivo)
        Me.PanelPrincipal.Controls.Add(Me.Button1)
        Me.PanelPrincipal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanelPrincipal.Location = New System.Drawing.Point(24, 120)
        Me.PanelPrincipal.Name = "PanelPrincipal"
        Me.PanelPrincipal.Size = New System.Drawing.Size(822, 390)
        Me.PanelPrincipal.TabIndex = 14
        '
        'txtFileOut
        '
        Me.txtFileOut.BackColor = System.Drawing.SystemColors.Window
        Me.txtFileOut.Location = New System.Drawing.Point(145, 328)
        Me.txtFileOut.Name = "txtFileOut"
        Me.txtFileOut.Size = New System.Drawing.Size(647, 22)
        Me.txtFileOut.TabIndex = 20
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(17, 331)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(114, 16)
        Me.Label5.TabIndex = 19
        Me.Label5.Text = "Archivo de Salida"
        '
        'txtFileIn
        '
        Me.txtFileIn.Location = New System.Drawing.Point(145, 290)
        Me.txtFileIn.Name = "txtFileIn"
        Me.txtFileIn.Size = New System.Drawing.Size(647, 22)
        Me.txtFileIn.TabIndex = 18
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.dtProceso)
        Me.Panel2.Location = New System.Drawing.Point(228, 3)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(262, 226)
        Me.Panel2.TabIndex = 17
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(17, 15)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(207, 16)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "Fecha de Aplicacion del Proceso"
        '
        'dtProceso
        '
        Me.dtProceso.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtProceso.Location = New System.Drawing.Point(59, 35)
        Me.dtProceso.Name = "dtProceso"
        Me.dtProceso.Size = New System.Drawing.Size(111, 22)
        Me.dtProceso.TabIndex = 13
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.sepDecComa)
        Me.Panel1.Controls.Add(Me.sepDecPunto)
        Me.Panel1.Location = New System.Drawing.Point(4, 2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(166, 88)
        Me.Panel1.TabIndex = 16
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(126, 16)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Separador Decimal"
        '
        'sepDecComa
        '
        Me.sepDecComa.AutoSize = True
        Me.sepDecComa.Location = New System.Drawing.Point(72, 58)
        Me.sepDecComa.Name = "sepDecComa"
        Me.sepDecComa.Size = New System.Drawing.Size(62, 20)
        Me.sepDecComa.TabIndex = 6
        Me.sepDecComa.TabStop = True
        Me.sepDecComa.Text = "Coma"
        Me.sepDecComa.UseVisualStyleBackColor = True
        '
        'sepDecPunto
        '
        Me.sepDecPunto.AutoSize = True
        Me.sepDecPunto.Location = New System.Drawing.Point(71, 35)
        Me.sepDecPunto.Name = "sepDecPunto"
        Me.sepDecPunto.Size = New System.Drawing.Size(60, 20)
        Me.sepDecPunto.TabIndex = 5
        Me.sepDecPunto.TabStop = True
        Me.sepDecPunto.Text = "Punto"
        Me.sepDecPunto.UseVisualStyleBackColor = True
        '
        'lblArchivo
        '
        Me.lblArchivo.AutoSize = True
        Me.lblArchivo.Location = New System.Drawing.Point(17, 293)
        Me.lblArchivo.Name = "lblArchivo"
        Me.lblArchivo.Size = New System.Drawing.Size(122, 16)
        Me.lblArchivo.TabIndex = 15
        Me.lblArchivo.Text = "Archivo de Entrada"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(11, 251)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(137, 29)
        Me.Button1.TabIndex = 14
        Me.Button1.Text = "Seleccionar Archivo"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'lblTit
        '
        Me.lblTit.AutoSize = True
        Me.lblTit.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTit.Location = New System.Drawing.Point(36, 21)
        Me.lblTit.Name = "lblTit"
        Me.lblTit.Size = New System.Drawing.Size(72, 24)
        Me.lblTit.TabIndex = 15
        Me.lblTit.Text = "Label6"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(145, 356)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(645, 27)
        Me.ProgressBar1.TabIndex = 21
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(876, 549)
        Me.Controls.Add(Me.lblTit)
        Me.Controls.Add(Me.PanelPrincipal)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cbArchivos)
        Me.Name = "Form1"
        Me.Text = "Validador de Archivos"
        Me.PanelPrincipal.ResumeLayout(False)
        Me.PanelPrincipal.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents cbArchivos As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents PanelPrincipal As Panel
    Friend WithEvents txtFileOut As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtFileIn As TextBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Label4 As Label
    Friend WithEvents dtProceso As DateTimePicker
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label3 As Label
    Friend WithEvents sepDecComa As RadioButton
    Friend WithEvents sepDecPunto As RadioButton
    Friend WithEvents lblArchivo As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents lblTit As Label
    Friend WithEvents ProgressBar1 As ProgressBar
End Class
