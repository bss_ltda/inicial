﻿Imports CLDatos

Module Principal
    Private datos As New ClsDatos
    Private data

    Sub Main()
        Dim aSubParam() As String
        Dim PROCEDIMIENTO As String = ""
        Dim PARAMAETROS As String = ""
        Dim TAREA As String = ""
        Dim continuar As Boolean

        Environment.GetCommandLineArgs()
        For Each parametro In Environment.GetCommandLineArgs()
            aSubParam = Split(parametro, "=")
            Select Case UCase(aSubParam(0))
                Case "PROC"
                    PROCEDIMIENTO = aSubParam(1)
                    Console.WriteLine(PROCEDIMIENTO)
                Case "PARAMS"
                    PARAMAETROS = aSubParam(1).Replace(":", "=")
                    Console.WriteLine(PARAMAETROS)
                Case "TAREA"
                    TAREA = aSubParam(1)
                    Console.WriteLine(TAREA)
            End Select
        Next
        Console.WriteLine("Abriendo Conexión...")
        If Not datos.AbrirConexion() Then
            Console.WriteLine("Error Abriendo Conexión...")
            Exit Sub
        End If
        Console.WriteLine("Conectado.")
        Console.WriteLine("Ejecutando Procedimiento... " & PROCEDIMIENTO & " " & PARAMAETROS)
        continuar = datos.Ejecutar(PROCEDIMIENTO, PARAMAETROS)
        If continuar = False Then
            Console.WriteLine("Error Ejecutando Procedimiento " & datos.mensaje)
            data = Nothing
            data = New RFTASK
            With data
                .TLXMSG = "ERROR DE EJECUCION"
                .STS1 = "-1"
                .STS2 = "-1"
                .TUPDDAT = Now().ToString("yyyy-MM-dd HH:mm:ss")
                .TMSG = "ERROR"
                .TNUMREG = TAREA
            End With
            Console.WriteLine("Actualizando RFTASK...")
            continuar = datos.ActualizarRFTASKRow(data, 0)
            If continuar = False Then
                Console.WriteLine("Error Actualizando RFTASK " & datos.mensaje)
            Else
                Console.WriteLine("RFTASK Actualizado")
            End If
        Else
            Console.WriteLine("Procedimiento Ejecutado.")
            data = Nothing
            data = New RFTASK
            With data
                .TLXMSG = "EJECUTADO"
                .STS1 = "1"
                .STS2 = "1"
                .TUPDDAT = Now().ToString("yyyy-MM-dd HH:mm:ss")
                .TMSG = "OK"
                .TNUMREG = TAREA
            End With
            Console.WriteLine("Actualizando RFTASK...")
            continuar = datos.ActualizarRFTASKRow(data, 0)
            If continuar = False Then
                Console.WriteLine("Error Actualizando RFTASK " & datos.mensaje)
            Else
                Console.WriteLine("RFTASK Actualizado")
            End If
        End If
        Console.WriteLine("Cerrando Conexión...")
        datos.CerrarConexion()
    End Sub

End Module
