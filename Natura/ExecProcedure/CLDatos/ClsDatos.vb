﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Text

Public Class ClsDatos
    Private Cadena As String
    Private ReadOnly conexion As SqlConnection
    Private adapter As SqlDataAdapter = New SqlDataAdapter()
    Private trans As SqlTransaction
    Private builder As SqlCommandBuilder
    Private command As SqlCommand
    Private WithEvents ds As DataSet
    Private fSql As String
    Private numFilas As Integer
    Private continuarguardando As Boolean
    Private dt As DataTable
    Private appPath As String = Path.GetFullPath(My.Application.Info.DirectoryPath & "\Log\")
    Private data
    Public mensaje As String

    Sub New()
        If My.Settings.PRUEBAS = "SI" Then
            Cadena = My.Settings.BASEDATOSLOCAL
        Else
            Cadena = My.Settings.BASEDATOS
        End If
        Try
            conexion = New SqlConnection(Cadena)
            ' Si el directorio no existe, crearlo
            If Not Directory.Exists(appPath) Then
                Directory.CreateDirectory(appPath)
            End If
        Catch ex As Exception
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\CONEXIONError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo Establecer conexion." & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try
    End Sub

#Region "ACTUALIZAR"

    ''' <summary>
    ''' Actualiza la tabla RFTASK 
    ''' </summary>
    ''' <param name="datos">RFTASK</param>
    ''' <param name="tipo">0- SET TLXMSG = @TLXMSG, STS1 = @STS1, STS2 = @STS2, TUPDDAT = NOW(), TMSG = @TMSG WHERE TNUMREG = @TNUMREG</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 13/05/2016</remarks>
    Public Function ActualizarRFTASKRow(ByVal datos As RFTASK, ByVal tipo As Integer) As Boolean

        trans = conexion.BeginTransaction()
        Select Case tipo
            Case 0
                fSql = "SET LANGUAGE 'English'; UPDATE RFTASK SET "
                fSql &= " TLXMSG = '" & datos.TLXMSG & "', "
                fSql &= " STS1 = " & datos.STS1 & ", "
                fSql &= " STS2 = " & datos.STS2 & ", "
                fSql &= " TUPDDAT = '" & datos.TUPDDAT & "', "
                fSql &= " TMSG = '" & datos.TMSG & "'"
                fSql &= " WHERE TNUMREG = " & datos.TNUMREG
        End Select

        command = New SqlCommand
        command.Connection = conexion
        command.Transaction = trans
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As Data.SqlClient.SqlException
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFTASKError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo guardar en la tabla RFTASK.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.ToString
        Catch ex As Exception
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFTASKError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo guardar en la tabla RFTASK.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.ToString
        Finally
            If continuarguardando = True Then
                trans.Commit()
            Else
                data = Nothing
                data = New RFLOG
                With data
                    .OPERACION = "Guardar RFTASK"
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarRFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

#End Region

#Region "CONSULTAR"

    ''' <summary>
    ''' Consulta la tabla RFTASK con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- TNUMREG(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 13/05/2016</remarks>
    Public Function ConsultarRFTASKdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        trans = conexion.BeginTransaction()

        Select Case tipoparametro
            Case 0
                fSql = "SELECT * FROM  RFTASK WHERE TNUMREG = " & parametroBusqueda(0)
        End Select
        adapter = New SqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adapter.SelectCommand.Transaction = trans
        builder = New SqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        trans.Commit()
        Return dt
    End Function

#End Region

#Region "FUNCIONES"

    ''' <summary>
    ''' Ejecuta Procedimiento Almacenado
    ''' </summary>
    ''' <returns>Un Booleano</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 12-05-2016</remarks>
    Public Function Ejecutar(ByVal procedure As String, ByVal params As String) As Boolean
        Dim parametros() As String
        Dim parametros2() As String
        Dim continuarguardando2 As Boolean = False
        parametros = Split(params, ",")
        command = New SqlCommand(procedure, conexion)
        command.CommandType = CommandType.StoredProcedure
        For i = 0 To parametros.Length - 1
            Console.WriteLine(parametros(i))
            parametros2 = Split(parametros(i), "=")
            command.Parameters.AddWithValue(parametros2(0), parametros2(1))
        Next
        Try
            command.CommandTimeout = 0
            command.ExecuteNonQuery()
            continuarguardando2 = True
        Catch ex As Exception
            continuarguardando2 = False
            mensaje = ex.Message & vbCrLf & procedure & " " & params
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\" & procedure & "Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("Error al ejecutar el procedimiento " & procedure & ".  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & procedure & " " & params)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Finally
            command.Dispose()
            If continuarguardando2 = False Then
                data = Nothing
                data = New RFLOG
                With data
                    .OPERACION = "Ejecutar Procedimiento"
                    .EVENTO = mensaje
                    .TXTSQL = procedure & " " & params
                    .ALERT = 1
                End With
                GuardarRFLOGRow(data)
            End If
        End Try
        Return continuarguardando2
    End Function

    Public Function AbrirConexion() As Boolean
        Dim abierta As Boolean = False
        Try
            If conexion.State = ConnectionState.Open Then
                abierta = True
            Else
                conexion.Open()
                abierta = True
            End If
        Catch ex1 As SqlException
            abierta = False
            mensaje = ex1.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine(ex1.Message & vbCrLf & ex1.ToString)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Catch ex As Exception
            abierta = False
            mensaje = ex.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine(ex.Message & vbCrLf & ex.ToString)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try
        Return abierta
    End Function

    Public Function CerrarConexion() As Boolean
        Dim cerrada As Boolean = False
        Try
            conexion.Close()
            cerrada = True
        Catch ex1 As SqlException
            cerrada = False
            mensaje = ex1.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine(ex1.Message & vbCrLf & ex1.ToString)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Catch ex As Exception
            cerrada = False
            mensaje = ex.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine(ex.Message & vbCrLf & ex.ToString)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try
        Return cerrada
    End Function

#End Region

#Region "GUARDAR"

    ''' <summary>
    ''' Guarda RFLOG
    ''' </summary>
    ''' <param name="datos">RFLOG</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 13/05/2016</remarks>
    Public Function GuardarRFLOGRow(ByVal datos As RFLOG) As Boolean

        trans = conexion.BeginTransaction()

        fSql = "INSERT INTO RFLOG (USUARIO, OPERACION, PROGRAMA, EVENTO, TXTSQL, ALERT)"
        fSql &= " VALUES ('" & System.Net.Dns.GetHostName() & "', '" & datos.OPERACION & "', '" & My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString & "', '" & datos.EVENTO.Replace("'", "") & "', '" & datos.TXTSQL.Replace("'", "") & "', " & datos.ALERT & ")"

        command = New SqlCommand
        command.Connection = conexion
        command.Transaction = trans
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As Data.SqlClient.SqlException
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo guardar en la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Catch ex As Exception
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo guardar en la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Finally
            If continuarguardando = True Then
                trans.Commit()

            End If
        End Try
        Return continuarguardando
    End Function

#End Region

End Class

Public Class RFLOG
    Public OPERACION As String
    Public EVENTO As String
    Public TXTSQL As String
    Public ALERT As String
End Class

Public Class RFTASK
    Public TLXMSG As String
    Public STS1 As String
    Public STS2 As String
    Public TUPDDAT As String
    Public TMSG As String
    Public TNUMREG As String
End Class
