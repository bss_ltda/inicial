﻿Imports CLDatos
Imports iTextSharp.text.pdf
Imports iTextSharp.text
Imports System.IO
Imports System.Text

Module SA
    Public datos As New ClsDatos
    Public barcodes As Barcode
    Private dtBSSFFNBAL As DataTable
    Private data
    Private CARPETA_SITIO As String
    Private CARPETA_IMG As String
    Public NumeroDocumento As String
    Private bPRUEBAS As Boolean
    Private PDF_Folder As String
    Private Firma_Folder As String
    Private LOG_File As String
    Public LOGO As String
    Public LOGOADELANTO As String
    Private ArchivoPDF As String
    Private document As Document
    Private PageCount As Integer
    Private dtCab As DataTable
    Private dtDet As DataTable
    Private dtNotas As DataTable
    Private totalvalor As Double
    Private totalcop As Double
    Private totalcambio As Double


    Sub Main()
        Dim campoParam As String = ""
        Dim aSubParam() As String

        Try
            Environment.GetCommandLineArgs()
            For Each parametro In Environment.GetCommandLineArgs()
                aSubParam = Split(parametro, "=")
                Select Case UCase(aSubParam(0))
                    Case "DOC"
                        NumeroDocumento = datos.Ceros(Trim(aSubParam(1)), 8)
                End Select
            Next

            If Not datos.AbrirConexion() Then
                Return
            End If

            If datos.Piloto = "SI" Then
                campoParam = "CCNOT2"
            End If
            CARPETA_SITIO = datos.ConsultarRFPARAMString("SITIO_CARPETA", campoParam, "")
            CARPETA_IMG = datos.ConsultarRFPARAMString("CARPETA_IMG", campoParam, "")
            LOGO = datos.ConsultarRFPARAMString("LOGO", campoParam, "")
            LOGOADELANTO = datos.ConsultarRFPARAMString("LOGADE", campoParam, "")
            bPRUEBAS = datos.Piloto.ToUpper() = "SI"
            PDF_Folder = CARPETA_SITIO & "xm\SRG\docs\SolicitudAdelanto\" & NumeroDocumento & "\"

            Try
                If Not Directory.Exists(PDF_Folder) Then
                    Directory.CreateDirectory(PDF_Folder)
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\DirectorioError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo crear Directorio." & vbCrLf & ex.Message)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            Firma_Folder = CARPETA_SITIO & "xm\SRG\firma\"
            Try
                If Not Directory.Exists(Firma_Folder) Then
                    Directory.CreateDirectory(Firma_Folder)
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\DirectorioError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo crear Directorio." & vbCrLf & ex.Message)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            LOG_File = PDF_Folder & NumeroDocumento & ".txt"
            datos.WrtTxtError(LOG_File, NumeroDocumento)
            Dim arguments As String() = Environment.GetCommandLineArgs()
            datos.WrtTxtError(LOG_File, String.Join(", ", arguments))
            If Not datos.CheckVer() Then
                datos.WrtSqlError(datos.mensaje, "")
                End
            End If

            GenerarPDF()
            datos.CerrarConexion()
            Return
        Catch ex As Exception
            datos.WrtTxtError(LOG_File, ex.Message.ToString)
            datos.CerrarConexion()
            Return
        End Try
    End Sub

    Sub GenerarPDF()
        Dim Resultado As Boolean = False
        Dim parametro(0) As String

        parametro(0) = NumeroDocumento
        dtCab = datos.ConsultarRCAUdt(parametro, 0)
        If dtCab Is Nothing Then
            datos.WrtTxtError(LOG_File, "Datos no encontrados.")
            Return
        End If

        ArchivoPDF = PDF_Folder & NumeroDocumento & ".pdf"
        ''fileName = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() & ".pdf"
        'ArchivoPDF = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() & ".pdf"
        ArmaPDF()

        If bPRUEBAS Then
            'EJECUTA
            Dim prc As Process = New System.Diagnostics.Process()
            prc.StartInfo.FileName = ArchivoPDF
            prc.Start()
        End If
    End Sub

    Sub ArmaPDF()

        document = New Document(New Rectangle(288.0F, 144.0F), 36, 36, 110, 45)
        document.SetPageSize(iTextSharp.text.PageSize.LETTER)
        Console.WriteLine(document.PageSize.Width.ToString)
        document.AddTitle("Solicitud de Adelanto - " & NumeroDocumento)
        Dim es As eventos = New eventos()
        Dim pw As PdfWriter = PdfWriter.GetInstance(document, New FileStream(ArchivoPDF, FileMode.Create))
        pw.PageEvent = es
        document.Open()


        Dim unaTabla As PdfPTable = GenerarTabla()
        document.Add(unaTabla)

        Dim finTabla As PdfPTable = GenerarfinTabla()
        finTabla.SplitRows = True
        finTabla.SplitLate = False
        document.Add(finTabla)
        PageCount = pw.PageNumber

        document.Close()
    End Sub

    Public Function GenerarTabla() As PdfPTable
        Dim flag As Integer = 0
        Dim bm As Drawing.Bitmap = Nothing

        Console.WriteLine("Generando una tabla...")
        Dim tablaPadre As New PdfPTable(1)
        tablaPadre.SetWidthPercentage(New Single() {615}, PageSize.LETTER)
        tablaPadre.HorizontalAlignment = Element.ALIGN_CENTER
        tablaPadre.DefaultCell.BorderWidth = 0.5

        Dim unaTabla As New PdfPTable(6)
        unaTabla.HorizontalAlignment = Element.ALIGN_CENTER
        unaTabla.DefaultCell.BorderWidth = 0
        unaTabla.SetWidthPercentage(New Single() {90, 163, 90, 90, 85, 97}, PageSize.LETTER)
        unaTabla.DefaultCell.FixedHeight = 100.0F

        Dim celda As PdfPCell

        celda = New PdfPCell(New Paragraph("Datos de Colaborador", FontFactory.GetFont("Arial", 12, Font.BOLD)))
        celda.FixedHeight = 20
        celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
        celda.Colspan = 6
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Tipo", FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(dtCab.Rows(0).Item("UTIPO"), FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Código SAP", FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(Trim(dtCab.Rows(0).Item("UUSR")), FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Proceso", FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(datos.Ceros(Trim(dtCab.Rows(0).Item("SNUMDOC")), 4), FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Nombre", FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(Trim(dtCab.Rows(0).Item("UNOM")), FontFactory.GetFont("Arial", 10)))
        celda.Colspan = 5
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Empresa", FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(Trim(dtCab.Rows(0).Item("UCATEG1")), FontFactory.GetFont("Arial", 10)))
        celda.Colspan = 2
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Centro de Costo", FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)
        Dim camponulo As String = ""
        If dtCab.Rows(0).Item("SCENCCOST") IsNot DBNull.Value Then
            camponulo = Trim(dtCab.Rows(0).Item("SCENCCOST"))
        End If

        If dtCab.Rows(0).Item("CENTROCOSTO") IsNot DBNull.Value Then
            camponulo &= " " & Trim(dtCab.Rows(0).Item("CENTROCOSTO"))
        End If


        celda = New PdfPCell(New Paragraph(camponulo, FontFactory.GetFont("Arial", 8)))
        celda.Colspan = 2
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Área", FontFactory.GetFont("Arial", 10)))
        celda.VerticalAlignment = Element.ALIGN_MIDDLE
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(Trim(dtCab.Rows(0).Item("UCATEG2")), FontFactory.GetFont("Arial", 10)))
        celda.Colspan = 2
        celda.VerticalAlignment = Element.ALIGN_MIDDLE
        unaTabla.AddCell(celda)

        bm = ClsDatos.Code39(NumeroDocumento.ToString, False, 20)
        'bm.Save("C:\Temp\barras.jpg")
        Dim img As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(datos.ConvertToByteArray(bm))
        Dim percentage As Single = 0.0F
        Dim codbarras As PdfPCell = New PdfPCell(img)
        codbarras.Colspan = 3
        codbarras.HorizontalAlignment = Element.ALIGN_CENTER
        codbarras.VerticalAlignment = Element.ALIGN_MIDDLE
        codbarras.FixedHeight = 25
        unaTabla.AddCell(codbarras)

        celda = New PdfPCell(New Paragraph("Viaje o Evento", FontFactory.GetFont("Arial", 12, Font.BOLD)))
        celda.FixedHeight = 20
        celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
        celda.Colspan = 6
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Origen", FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(Trim(dtCab.Rows(0).Item("SORIGEN")), FontFactory.GetFont("Arial", 9)))
        celda.Colspan = 2
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Documento SAP", FontFactory.GetFont("Arial", 10)))
        celda.Colspan = 2
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(""))
        celda.VerticalAlignment = Element.ALIGN_MIDDLE
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Destino", FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(Trim(dtCab.Rows(0).Item("SDESTINO")), FontFactory.GetFont("Arial", 9)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Fecha Salida", FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(CDate(dtCab.Rows(0).Item("SFSALIDA")).ToString("dd-MMM-yy"), FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Fecha Llegada", FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(CDate(dtCab.Rows(0).Item("SFLLEGADA")).ToString("dd-MMM-yy"), FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Localidad", FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(Trim(dtCab.Rows(0).Item("SLOCALIDAD")), FontFactory.GetFont("Arial", 10)))
        celda.Colspan = 2
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Días de viaje", FontFactory.GetFont("Arial", 10)))
        celda.Colspan = 2
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(Trim(dtCab.Rows(0).Item("DIAS")), FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        Dim justifi As New PdfPTable(2)
        justifi.HorizontalAlignment = Element.ALIGN_CENTER
        justifi.DefaultCell.BorderWidth = 0
        justifi.SetWidthPercentage(New Single() {120, 495}, PageSize.LETTER)

        celda = New PdfPCell(New Paragraph("Justificación del viaje", FontFactory.GetFont("Arial", 10)))
        celda.VerticalAlignment = Element.ALIGN_MIDDLE
        justifi.AddCell(celda)

        celda = New PdfPCell(New Paragraph(Trim(dtCab.Rows(0).Item("SJUSTIFIC")), FontFactory.GetFont("Arial", 9)))
        'celda.FixedHeight = 30
        justifi.AddCell(celda)

        celda = New PdfPCell(justifi)
        celda.Colspan = 6
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Adelanto", FontFactory.GetFont("Arial", 12, Font.BOLD)))
        celda.FixedHeight = 20
        celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
        celda.Colspan = 6
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Valor del adelanto en moneda local", FontFactory.GetFont("Arial", 10)))
        celda.Colspan = 2
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(FormatNumber(dtCab.Rows(0).Item("SVALORLOC"), 2), FontFactory.GetFont("Arial", 10)))
        celda.Colspan = 2
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Fecha", FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(CDate(dtCab.Rows(0).Item("SFECDOC")).ToString("dd-MMM-yy"), FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        'totalvalor = 0
        'totalcambio = 0
        'totalcop = 0

        'If dtNotas IsNot Nothing Then
        '    For Each row As DataRow In dtNotas.Rows
        '        totalvalor += CDbl(row("VALOR"))
        '        totalcop += CDbl(row("VALORLOC"))
        '    Next
        'End If

        'Dim valordev As String
        'valordev = FormatNumber(CDbl(dtCab.Rows(0).Item("SVALORLOC")) - totalvalor, 2)
        'Dim valordevtext As String
        'If CDbl(valordev) > 0 Then
        '    valordevtext = "Valor a regresar COP"
        'Else
        '    valordevtext = "Valor a reembolsar COP"
        'End If
        'celda = New PdfPCell(New Paragraph(valordevtext, FontFactory.GetFont("Arial", 10)))
        'celda.Colspan = 2
        'unaTabla.AddCell(celda)

        'celda = New PdfPCell(New Paragraph(valordev.Replace("-", ""), FontFactory.GetFont("Arial", 10)))
        'celda.Colspan = 4
        'unaTabla.AddCell(celda)

        Dim celdapapa As New PdfPCell(unaTabla)
        celdapapa.BorderWidth = 0
        tablaPadre.AddCell(celdapapa)

        Return tablaPadre
    End Function

    Public Function GenerarfinTabla() As PdfPTable
        Dim flag As Integer = 0

        Console.WriteLine("Generando una tabla3...")
        Dim tablaPadre As New PdfPTable(1)
        tablaPadre.SetWidthPercentage(New Single() {615}, PageSize.LETTER)
        tablaPadre.HorizontalAlignment = Element.ALIGN_CENTER
        tablaPadre.DefaultCell.BorderWidth = 0.5

        Dim unaTabla As New PdfPTable(4)
        unaTabla.HorizontalAlignment = Element.ALIGN_CENTER
        unaTabla.DefaultCell.BorderWidth = 0
        unaTabla.SetWidthPercentage(New Single() {200, 200, 107, 107}, PageSize.LETTER)
        unaTabla.DefaultCell.FixedHeight = 100.0F

        Dim celda As PdfPCell

        celda = New PdfPCell(New Paragraph(""))
        celda.FixedHeight = 15
        celda.Colspan = 4
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Su rendición de cuentas solo será valida con la entrega de esta planilla debidamente aprobada y con los comprobantes en adjunto.", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        celda.FixedHeight = 16
        celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
        celda.Colspan = 4
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(""))
        celda.FixedHeight = 10
        celda.Colspan = 4
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        Dim texto As String = "Por medio del presente, declaro de manera expresa y bajo juramento que la información aquí  mencionada y los documentos anexos "
        texto &= "(de ser aplicable) son exactos, veraces e íntegros. Asimismo, declaro conocer que las eventuales irregularidades sobre estas "
        texto &= "declaraciones están sujetas a las sanciones previstas en los reglamentos internos, en el Código de Conducta de Natura y las leyes "
        texto &= "laborales locales. En el supuesto que no sea presentado el informe de gastos y sus respectivos sustentos hasta el día 20 del mes "
        texto &= "siguiente a la fecha de mi regreso a labores o de la utilización del anticipo conforme con lo descrito en la Norma de Gastos con Viajes, "
        texto &= "autorizo de manera expresa y libre, el descuento directo sin previo aviso en la planilla del valor total del anticipo recibido en tanto será considerado este monto como un préstamo a mi persona por parte de la compañía. Caso por cuenta de las leyes laborales locales no sea posible realizar el descuento del monto total en apenas un salario, estoy de acuerdo que Natura haga el descuento en cuotas."

        celda = New PdfPCell(New Paragraph(texto, FontFactory.GetFont("Arial", 8, BaseColor.RED)))
        celda.HorizontalAlignment = Element.ALIGN_CENTER
        celda.BorderWidth = 0
        celda.Colspan = 4
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(""))
        celda.FixedHeight = 10
        celda.Colspan = 4
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Firmas", FontFactory.GetFont("Arial", 10, Font.BOLD)))
        celda.FixedHeight = 17
        celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
        celda.Colspan = 4
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Nº de identificación del Colaborador: ", FontFactory.GetFont("Arial", 9)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(dtCab.Rows(0).Item("SSOLICITA").ToString, FontFactory.GetFont("Arial", 9)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Fecha: ", FontFactory.GetFont("Arial", 9)))
        unaTabla.AddCell(celda)
        Dim fec As String = ""

        If dtCab.Rows(0).Item("SFSOLICITA") IsNot DBNull.Value Then
            fec = CDate(dtCab.Rows(0).Item("SFSOLICITA")).ToString("dd-MMM-yy")
        End If
        celda = New PdfPCell(New Paragraph(fec, FontFactory.GetFont("Arial", 9)))
        unaTabla.AddCell(celda)

        If dtCab.Rows(0).Item("UUPLFILE1") Is DBNull.Value Then
            celda = New PdfPCell(New Paragraph("Nombre del Colaborador: ", FontFactory.GetFont("Arial", 9)))
            unaTabla.AddCell(celda)

            celda = New PdfPCell(New Paragraph(Trim(dtCab.Rows(0).Item("UNOM").ToString), FontFactory.GetFont("Arial", 9)))
            celda.Colspan = 3
            unaTabla.AddCell(celda)
        Else
            celda = New PdfPCell(New Paragraph("Nombre del Colaborador: ", FontFactory.GetFont("Arial", 9)))
            celda.VerticalAlignment = Element.ALIGN_MIDDLE
            celda.Rowspan = 2
            unaTabla.AddCell(celda)

            Dim firmacola As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(Firma_Folder & Trim(dtCab.Rows(0).Item("UUPLFILE1")))
            firmacola.ScalePercent(65)
            celda = New PdfPCell(firmacola)
            celda.Colspan = 3
            celda.FixedHeight = 52
            celda.VerticalAlignment = Element.ALIGN_MIDDLE
            celda.HorizontalAlignment = Element.ALIGN_CENTER
            unaTabla.AddCell(celda)

            celda = New PdfPCell(New Paragraph(Trim(dtCab.Rows(0).Item("UNOM").ToString), FontFactory.GetFont("Arial", 9)))
            celda.HorizontalAlignment = Element.ALIGN_CENTER
            celda.Colspan = 3
            unaTabla.AddCell(celda)
        End If

        celda = New PdfPCell(New Paragraph(""))
        celda.FixedHeight = 8
        celda.BackgroundColor = New iTextSharp.text.BaseColor(215, 215, 215)
        celda.Colspan = 4
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Nº de identificación del Gestor: ", FontFactory.GetFont("Arial", 9)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(dtCab.Rows(0).Item("SAPRUEBA").ToString, FontFactory.GetFont("Arial", 9)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Fecha: ", FontFactory.GetFont("Arial", 9)))
        unaTabla.AddCell(celda)
        Dim feca As String = ""

        If dtCab.Rows(0).Item("SFAPRUEBA") IsNot DBNull.Value Then
            feca = CDate(dtCab.Rows(0).Item("SFAPRUEBA")).ToString("dd-MMM-yy")
        End If
        celda = New PdfPCell(New Paragraph(feca, FontFactory.GetFont("Arial", 9)))
        unaTabla.AddCell(celda)

        If dtCab.Rows(0).Item("UUPLFILE1GESTOR") Is DBNull.Value Then
            celda = New PdfPCell(New Paragraph("Nombre del Gestor: ", FontFactory.GetFont("Arial", 9)))
            unaTabla.AddCell(celda)

            celda = New PdfPCell(New Paragraph(Trim(dtCab.Rows(0).Item("UNOMGESTOR").ToString), FontFactory.GetFont("Arial", 9)))
            celda.Colspan = 3
            unaTabla.AddCell(celda)
        Else
            celda = New PdfPCell(New Paragraph("Nombre del Gestor: ", FontFactory.GetFont("Arial", 9)))
            celda.Rowspan = 2
            celda.VerticalAlignment = Element.ALIGN_MIDDLE
            unaTabla.AddCell(celda)

            Dim firmages As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(Firma_Folder & Trim(dtCab.Rows(0).Item("UUPLFILE1GESTOR")))
            firmages.ScalePercent(65)
            celda = New PdfPCell(firmages)

            celda.Colspan = 3
            celda.FixedHeight = 52
            celda.VerticalAlignment = Element.ALIGN_MIDDLE
            celda.HorizontalAlignment = Element.ALIGN_CENTER
            unaTabla.AddCell(celda)

            celda = New PdfPCell(New Paragraph(Trim(dtCab.Rows(0).Item("UNOMGESTOR").ToString), FontFactory.GetFont("Arial", 9)))
            celda.HorizontalAlignment = Element.ALIGN_CENTER
            celda.Colspan = 3
            unaTabla.AddCell(celda)
        End If

        Dim celdapapa As New PdfPCell(unaTabla)
        celdapapa.BorderWidth = 0
        tablaPadre.AddCell(celdapapa)

        Return tablaPadre
    End Function


End Module
