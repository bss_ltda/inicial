﻿using System.Collections.Generic;

namespace BSSFileUpload.Common
{
    public interface IRFTASKRepository
    {
        RFTASK Add(RFTASK RFTASK);
    }
}
