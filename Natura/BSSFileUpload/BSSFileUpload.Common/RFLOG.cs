﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSSFileUpload.Common
{
    class RFLOG
    {
        public string USUARIO { get; set; }
        public string PROGRAMA { get; set; }
        public string ALERT { get; set; }
        public string EVENTO { get; set; }
        public string LKEY { get; set; }
        public string TXTSQL { get; set; }
        public string OPERACION { get; set; } 
    }
}
