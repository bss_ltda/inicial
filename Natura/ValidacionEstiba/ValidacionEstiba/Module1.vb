﻿Imports ADODB
Imports System.Configuration
Imports System.Threading
Module Module1
    Dim DB As New ADODB.Connection
    Dim fSql As String
    Dim vSql As String
    Dim lastSql As String = ""
    Dim idNum As String
    Dim bMuestraSQL As Boolean = False
    Dim Paso As String
    Dim tiempoIni As DateTime = DateTime.Now
    Dim regs As Long
    Sub Main()
        Dim aSubParam() As String
        'es llamado desde \Despachos\ValidacionEstiba_events.asp
        'Poner en E:\Sitios\Colombia\AplicacionConsola\ValidacionEstiba\ValidacionEstiba.exe
        Environment.GetCommandLineArgs()
        For Each parametro In Environment.GetCommandLineArgs()
            aSubParam = Split(parametro, "=")
            Select Case UCase(aSubParam(0))
                Case "ID"
                    idNum = aSubParam(1)
                Case "DSP"
                    If aSubParam(1) = "1" Then
                        bMuestraSQL = True
                    End If
            End Select
        Next
        If idNum <> "" Then
            DB.Open(My.Settings.InfoNatura)
            Try
                ValidaPlanillaDespachos()
                'Dim tiempoFinSQL As DateTime = DateTime.Now
                'Dim lleva As TimeSpan = tiempoFinSQL.Subtract(tiempoIni)
            Catch ex As Exception
                GuardaLog(ex)
            End Try
            DB.Close()
        End If
    End Sub
    Sub ValidaPlanillaDespachos()
        Dim rsPlanilla As ADODB.Recordset
        Dim rs1 As ADODB.Recordset
        Dim rs2 As ADODB.Recordset
        Dim rsMinuta As ADODB.Recordset
        Dim rsPedido As ADODB.Recordset
        Dim rsEmbalaje As ADODB.Recordset
        Dim rsTransp As ADODB.Recordset
        Dim rs As ADODB.Recordset
        'Actualiza Embalaje con los primeros 7 digitos
        'Esto se hace por Despachos\P_EstibaBatch y en Despachos\PistoleoEstiba
        'fSql = "Update DespachosPlanilla Set [EMBALAJE ] = left([EMBALAJE ],8) Where Id = " & idNum
        'ExecuteSQL(fSql)
        tiempoIni = Now()
        fSql = "UPDATE DespachosPlanilla set "
        fSql &= "  Estado1 = Case When Estado1 Is Null Then '' Else Estado1 End"
        fSql &= ", Estado2 = Case When Estado2 Is Null Then '' Else Estado2 End"
        fSql &= ", Estado3 = Case When Estado3 Is Null Then '' Else Estado3 End"
        fSql &= ", Estado4 = Case When Estado4 Is Null Then '' Else Estado4 End"
        fSql &= ", Estado5 = Case When Estado5 Is Null Then '' Else Estado5 End"
        fSql &= ", Fecha = getdate()"
        fSql &= ", NoCaja = 0"
        fSql &= ", CantCaja= 0  "
        fSql &= " Where  [Id ] = " & idNum
        Paso = "Paso 0"
        ExecuteSQL(fSql)

        'PedidosMinuta se borrar en cada cargue, entonces es el que delimita el conjunto de datos a actualizar
        fSql = " UPDATE DespachosPlanilla  SET  "
        fSql &= "   Pedido               = a.Numpedido  "
        '      fSql &= "   Pedido               = CASE WHEN b.PEDIDO_ATG IS NULL THEN a.Numpedido WHEN LTRIM(RTRIM(PEDIDO_ATG)) = '' THEN a.Numpedido ELSE b.PEDIDO_ATG END  "
        fSql &= " , PedidoATG            = CASE WHEN LTRIM(RTRIM(b.PEDIDO_ATG)) = '' THEN NULL ELSE b.PEDIDO_ATG END  "
        fSql &= " , Direccion            = b.DIRECCION "
        fSql &= " , TipoEntrega          = b.TIPO_DE_ENTREGA  "
        fSql &= " , DetalleEntrega       = b.DETALLE_ENTREGA "
        fSql &= " , FechaPrevistaEntrega = b.FECHA_PREVISTA_ENTREGA  "
        fSql &= " , RangoHoraEntrega     = b.RANGO_HORA_ENTREGA  "
        fSql &= " FROM DespachosPlanilla a INNeR JOIN DespachosMINUTA b ON a.Numpedido = b.Pedido "
        fSql &= " Where  a.[Id ] = " & idNum
        Paso = "Paso 1"
        ExecuteSQL(fSql)
        'Trae nueva transportadora si la encuentra digitada (Nueva Direccion se usa para la nueva transportadora)
        fSql = " UPDATE DespachosPedFac  SET Transportadora = b.NuevaDireccion "
        fSql &= " FROM DespachosPedFac a INNER JOIN DespachosCTransportadora b ON a.CodigoPedido = b.Pedido"
        Paso = "Paso 2"
        ExecuteSQL(fSql)
        'Actualiza Minuta con actualizaciones de direccion (que hacen por InfoNatura)
        fSql = " UPDATE DespachosMINUTA  SET "
        fSql &= "   Direccion    = b.NuevaDireccion"
        fSql &= " , Departamento = b.NuevoDep"
        fSql &= " , Ciudad       = b.NuevoMun "
        fSql &= " FROM DespachosMINUTA a INNER JOIN DespachosCDireccion b ON a.pedido = b.Pedido"
        Paso = "Paso 3"
        ExecuteSQL(fSql)
        fSql = " SELECT DISTINCT [Estiba] FROM DespachosPlanilla WHERE [Id ] = " & idNum
        rsPlanilla = ExecuteSQL(fSql)
        Do While Not rsPlanilla.EOF
            fSql = " UPDATE "
            fSql &= "    DespachosPlanilla  "
            fSql &= " SET "
            fSql &= "    NoCaja = i.RowNum "
            fSql &= " FROM "
            fSql &= "    ( "
            fSql &= "        SELECT "
            fSql &= "            x.idUnico "
            fSql &= "          , ROW_NUMBER() OVER(ORDER BY x.IdUnico) AS RowNum "
            fSql &= "        FROM "
            fSql &= "            DespachosPlanilla x "
            fSql &= "        WHERE "
            fSql &= "            [Id ] = " & idNum
            fSql &= "            And Estiba = " & rsPlanilla("Estiba").Value
            fSql &= "    ) "
            fSql &= "    AS i "
            fSql &= " WHERE "
            fSql &= "    [Id ] = " & idNum
            fSql &= "    And Estiba = " & rsPlanilla("Estiba").Value
            fSql &= "    And DespachosPlanilla.IdUnico = i.IdUnico "
            Paso = "Paso 4." & rsPlanilla("Estiba").Value
            ExecuteSQL(fSql)
            rsPlanilla.MoveNext()
        Loop
        rsPlanilla.Close()

        fSql = "UPDATE DespachosPlanilla SET Estado1= '', Estado2= 'OK', Estado3= '', Estado4= '', Estado5= '' "
        fSql &= " Where [Id ] = " & idNum
        Paso = "Paso 5"
        ExecuteSQL(fSql)

        fSql = sqlDespachosPlanillas()
        rsPlanilla = ExecuteSQL(fSql)
        While Not rsPlanilla.EOF
            fSql = "Select * From DespachosMinuta Where [EMBALAJE ] = '" & rsPlanilla("EMBALAJE").Value & "'"
            rsMinuta = ExecuteSQL(fSql)
            'Busca EMBALAJE, si no esta Alerta
            If rsMinuta.EOF Then
                fSql = "UPDATE DespachosPlanilla SET "
                fSql &= " Estado1 = Estado1 + 'NO EXISTE EMBALAJE' "
                fSql &= " Where [IdUnico] =  " & rsPlanilla("IdUnico").Value
                Paso = "Paso 6"
                ExecuteSQL(fSql)
            Else
                'Busca si el EMBALAJE esta en otra estiba
                fSql = "Select EMBALAJE From DespachosPlanilla "
                fSql &= " Where ( [EMBALAJE ] = '" & rsPlanilla("EMBALAJE").Value & "' "
                fSql &= "         AND ID = " & idNum & " "
                fSql &= "         AND Estado1 = 'OK' AND Estado2 = 'OK') or "
                fSql &= "       ( [EMBALAJE ] = '" & rsPlanilla("EMBALAJE").Value & "' AND Estado1 = 'OK' AND Estado2 = 'OK')"
                rsEmbalaje = ExecuteSQL(fSql)
                If Not rsEmbalaje.EOF Then
                    fSql = "UPDATE DespachosPlanilla SET "
                    fSql &= " Estado1 = Estado1 + 'EMBALAJE EN OTRA ESTIBA' "
                    fSql &= " Where [IdUnico] =  " & rsPlanilla("IdUnico").Value
                    Paso = "Paso 7"
                    ExecuteSQL(fSql)
                Else
                    While Not rsMinuta.EOF
                        'Busca el Pedidos.csv
                        fSql = "Select Persona, Replace(NombrePersona, '''', '' ) NombrePersona From DespachosPedFac "
                        fSql &= " Where [CodigoPedido] = '" & rsMinuta("PEDIDO").Value & "'"
                        rsPedido = ExecuteSQL(fSql)
                        If rsPedido.EOF Then
                            fSql = "UPDATE DespachosPlanilla SET "
                            fSql &= " Estado1 = Estado1 + 'NO EXISTE PEDIDO' "
                            fSql &= " Where [IdUnico] =  " & rsPlanilla("IdUnico").Value
                            Paso = "Paso 8"
                            ExecuteSQL(fSql)
                        Else
                            'Busca que el pedido sea de la transportadora en Pedidos.csv
                            'SELECT [PesoEstimado], [PesoReal] FROM DespachosPedFac
                            fSql = "Select * From DespachosPedFac "
                            fSql &= " Where [CodigoPedido] = '" & rsMinuta("PEDIDO").Value & "' "
                            fSql &= "   and Transportadora = '" & rsPlanilla("Transportadora").Value & "' "
                            rsTransp = ExecuteSQL(fSql)
                            If rsTransp.EOF Then
                                fSql = "UPDATE DespachosPlanilla SET "
                                fSql &= " Estado1 = Estado1 + 'TRANSPORTADORA INCORRECTA' "
                                fSql &= " Where [IdUnico] =  " & rsPlanilla("IdUnico").Value
                                Paso = "Paso 9"
                                ExecuteSQL(fSql)
                            End If
                            rsTransp.Close()
                            'Busca que el pedido no haya sido anulado (option por InfoNatura)
                            fSql = "SELECT Pedido  FROM DespachosAnulados WHERE Pedido = '" & rsMinuta("PEDIDO").Value & "'"
                            rs = ExecuteSQL(fSql)
                            If Not rs.EOF Then
                                fSql = "UPDATE DespachosPlanilla SET "
                                fSql &= " Estado1 = Estado1 + 'PEDIDO ANULADO', "
                                fSql &= " Estado2 = Estado2 + 'PEIDO ANULADO' "
                                fSql &= " Where "
                                fSql &= "   [IdUnico] =  " & rsPlanilla("IdUnico").Value
                                Paso = "Paso 10"
                                ExecuteSQL(fSql)
                            End If
                            rs.Close()
                        End If
                        If Not rsPedido.EOF Then
                            'Actualiza datos planilla. Persona y Nombre desde Pedidos.csv, 
                            fSql = "SELECT ISNULL (MAX (CantCaja), 0) + 1 AS ID FROM DespachosPlanilla "
                            fSql &= " where id =" & idNum & "AND NumPedido = '" & rsMinuta("PEDIDO").Value & "'"
                            rs = ExecuteSQL(fSql)
                            fSql = "UPDATE DespachosPlanilla SET  "
                            fSql &= "  Estado1        = Estado1 + 'OK' "
                            fSql &= ", CantCaja       = " & rs("ID").Value
                            fSql &= ", Numpedido      = '" & rsMinuta("PEDIDO").Value & "'"
                            fSql &= ", CodigoCN       = '" & rsPedido("Persona").Value & "'"
                            fSql &= ", NombreCN       = '" & rsMinuta("CONSULTORA").Value & "'"
                            fSql &= ", Direccion      = '" & rsMinuta("DIRECCION").Value & "'"
                            fSql &= ", Celular        = '" & rsMinuta("MOVIL").Value & "'"
                            fSql &= ", Telefono       = '" & rsMinuta("TEL").Value & "'"
                            fSql &= ", Ciudad         = '" & rsMinuta("CIUDAD").Value & "'"
                            fSql &= ", Departamento   = '" & rsMinuta("DEPARTAMENTO").Value & "'"
                            fSql &= ", NumMinuta      = '" & numi(rsMinuta("MINUTA").Value) & "'"
                            fSql &= " Where [IdUnico] =  " & rsPlanilla("IdUnico").Value
                            Paso = "Paso 11"
                            ExecuteSQL(fSql)
                            rs.Close()
                        End If
                        rsPedido.Close()
                        fSql = "SELECT Fecha, Transportadora, Estiba FROM DespachosPlanillaHistorico "
                        fSql &= " Where [EMBALAJE ] = '" & rsPlanilla("EMBALAJE").Value & "' AND Estado1 = 'OK' AND Estado2 = 'OK' AND Estado5=''"
                        rs = ExecuteSQL(fSql)
                        If Not rs.EOF Then
                            fSql = "UPDATE DespachosPlanilla SET "
                            fSql &= " Estado1 = Estado1 + 'EMBALAJE EN OTRA ESTIBA' "
                            fSql &= " Where [IdUnico] =  " & rsPlanilla("IdUnico").Value
                            Paso = "Paso 12"
                            ExecuteSQL(fSql)
                        End If
                        rs.Close()
                        rsMinuta.MoveNext()
                    End While
                End If
                rsEmbalaje.Close()
            End If
            rsMinuta.Close()
            rsPlanilla.MoveNext()
        End While
        rsPlanilla.Close()
        Dim RsConsul, RsConsul1 As ADODB.Recordset
        fSql = "SELECT NumPedido, ISNULL (MAX (CantCaja), 0) AS ID FROM DespachosPlanilla where id = " & idNum & " group by NumPedido "
        RsConsul = ExecuteSQL(fSql)
        While Not RsConsul.EOF
            fSql = "Select * From DespachosPlanilla "
            fSql &= " Where Id = " & idNum & "AND NumPedido = '" & RsConsul("NumPedido").Value & "' AND CantCaja < " & RsConsul("ID").Value & ""
            RsConsul1 = ExecuteSQL(fSql)
            While Not RsConsul1.EOF
                fSql = "update  DespachosPlanilla set "
                fSql &= " Estado1 = 'Seguimiento' "
                fSql &= " Where Id = " & idNum & " AND NumPedido = '" & RsConsul("NumPedido").Value & "' AND CantCaja < " & RsConsul("ID").Value & ""
                Paso = "Paso 13"
                ExecuteSQL(fSql)
                RsConsul1.MoveNext()
            End While
            RsConsul.MoveNext()
        End While
        RsConsul.Close()

        fSql = sqlDespachosPlanillas()
        rsPlanilla = ExecuteSQL(fSql)
        While Not rsPlanilla.EOF
            fSql = "Select Persona, NombrePersona From DespachosPedFac "
            fSql &= " Where [CodigoPedido] = '" & rsPlanilla("Numpedido").Value & "'"
            rsPedido = ExecuteSQL(fSql)
            fSql = "Select [EMBALAJE ] AS EMBALAJE From DespachosPlanilla "
            fSql &= " Where ( [EMBALAJE ] = '" & rsPlanilla("EMBALAJE").Value & "' "
            fSql &= "         AND ID = " & idNum & ") or "
            fSql &= "       ( [EMBALAJE ] = '" & rsPlanilla("EMBALAJE").Value & "' AND Estado1 = 'OK' AND Estado2 = 'OK')"
            rsEmbalaje = ExecuteSQL(fSql)
            If Not rsEmbalaje.EOF Then
                If Not rsPedido.EOF Then
                    fSql = "SELECT ISNULL  (COUNT(*), 0) AS total FROM DespachosMinuta "
                    fSql &= " Where PEDIDO = '" & rsPlanilla("Numpedido").Value & "'"
                    rs1 = ExecuteSQL(fSql)
                    fSql = "  SELECT ISNULL (MAX (CantCaja), 0) AS total FROM DespachosPlanilla "
                    fSql &= " Where Numpedido = '" & rsPlanilla("Numpedido").Value & "' And ID  = " & idNum & " AND Estado1 = 'OK'"
                    rs2 = ExecuteSQL(fSql)
                    If rs1("total").Value > rs2("total").Value Then
                        fSql = "UPDATE DespachosPlanilla SET Estado3 = 'PEDIDO INCOMPLETO' "
                        fSql &= " Where Id = " & idNum & " AND Numpedido = '" & rsPlanilla("Numpedido").Value & "' "
                        Paso = "Paso 14"
                        ExecuteSQL(fSql)
                        fSql = "UPDATE DespachosPlanilla SET Estado2 = 'ERROR' "
                        fSql &= " Where Id = " & idNum & " AND Numpedido = '" & rsPlanilla("Numpedido").Value & "' "
                        ExecuteSQL(fSql)
                    Else
                        fSql = "UPDATE DespachosPlanilla set Estado2 = 'OK', Estado3= 'OK' "
                        fSql &= " Where Id = " & idNum & " AND Numpedido = '" & rsPlanilla("Numpedido").Value & "' "
                        Paso = "Paso 15"
                        ExecuteSQL(fSql)
                    End If
                    rs1.Close()
                    rs2.Close()
                End If
                rsPedido.Close()
            End If
            rsEmbalaje.Close()
            rsPlanilla.MoveNext()
        End While
        rsPlanilla.Close()
        'PedidosMinuta se borrar en cada cargue, entonces es el que delimita el conjunto de datos a actualizar
        fSql = " UPDATE DespachosPlanilla  SET  "
        fSql &= "   Pedido               = a.Numpedido  "
        fSql &= " , PedidoATG            = CASE WHEN b.PEDIDO_ATG IS NULL THEN b.PEDIDO_ATG WHEN LTRIM(RTRIM(b.PEDIDO_ATG)) = '' THEN NULL ELSE b.PEDIDO_ATG END  "
        fSql &= " , Direccion            = b.DIRECCION "
        fSql &= " , TipoEntrega          = b.TIPO_DE_ENTREGA  "
        fSql &= " , DetalleEntrega       = b.DETALLE_ENTREGA "
        fSql &= " , FechaPrevistaEntrega = b.FECHA_PREVISTA_ENTREGA  "
        fSql &= " , RangoHoraEntrega     = b.RANGO_HORA_ENTREGA  "
        fSql &= " FROM DespachosPlanilla a INNeR JOIN DespachosMINUTA b ON a.Numpedido = b.Pedido "
        fSql &= " Where  a.[Id ] = " & idNum
        Paso = "Paso 16"
        ExecuteSQL(fSql)
        fSql = " UPDATE DespachosPlanilla SET  "
        fSql &= "   Peso = w.PesoEstimado "
        fSql &= " , PesoFinal = w.PesoReal "
        fSql &= " FROM DespachosPlanilla r Inner Join  "
        fSql &= " 	 DespachosPedFac W on r.Pedido = cast( w.CodigoPedido as int) "
        fSql &= " Where r.[Id ] = " & idNum
        Paso = "Paso 17"
        ExecuteSQL(fSql)
    End Sub

    Function ExecuteSQL(varSql As String) As ADODB.Recordset
        Try
            lastSql = varSql
            Dim tiempoIniSQL As DateTime = DateTime.Now
            If bMuestraSQL Then
                Console.WriteLine(Paso & " Ejecutando:")
                Console.WriteLine(varSql)
            End If
            ExecuteSQL = DB.Execute(varSql, regs)
            If bMuestraSQL Then
                Console.WriteLine("Registros actualizados " & regs)
                Dim tiempoFinSQL As DateTime = DateTime.Now
                Dim ts As TimeSpan = tiempoFinSQL.Subtract(tiempoIniSQL)
                Console.Write(Paso & vbTab & " Tomo " & vbTab & ts.ToString)
                Dim lleva As TimeSpan = tiempoFinSQL.Subtract(tiempoIni)
                Console.WriteLine(vbTab & "Lleva " & vbTab & lleva.ToString)
                Console.WriteLine("")
            End If
        Catch ex As Exception
            GuardaLog(ex)
            Return Nothing
        End Try
    End Function

    Sub GuardaLog(ex As Exception)
        Dim fSql, vSql As String
        dsp(ex.ToString)
        fSql = " INSERT INTO RFLOG( "
        vSql = " VALUES ( "
        fSql &= " USUARIO   , " : vSql = vSql & "'DESPACHOS', "
        fSql &= " PROGRAMA  , " : vSql = vSql & "'ValidacionEstibas', "
        fSql &= " ALERT     , " : vSql = vSql & "1, "
        fSql &= " EVENTO    , " : vSql = vSql & "'" & Left(Replace("<hr/>" & ex.Message.ToString(), "'", "''"), 20000) & "', "
        fSql &= " TXTSQL    ) " : vSql = vSql & "'" & Replace(lastSql, "'", "''") & "' ) "
        Try
            DB.Execute(fSql & vSql)
        Catch ex2 As Exception
        End Try
    End Sub

    Function numi(v As String) As String
        Dim i As Integer
        Dim s As String = ""
        For i = 1 To Len(v)
            If Asc(Mid(v, i, 1)) >= 48 And Asc(Mid(v, i, 1)) <= 57 Then
                s &= Mid(v, i, 1)
            End If
        Next
        Return s

    End Function


    Sub dsp(txt As String)
        Console.WriteLine(txt)
    End Sub

    Function sqlDespachosPlanillas()
        Dim fSql As String
        fSql = "      SELECT  [IdUnico] "
        fSql &= "      ,[Id ] AS Id"
        fSql &= "      ,[EMBALAJE ] as EMBALAJE"
        fSql &= "      ,[NoCaja] "
        fSql &= "      ,[CantCaja] "
        fSql &= "      ,[Numpedido] "
        fSql &= "      ,[CodigoCN] "
        fSql &= "      ,[NombreCN] "
        fSql &= "      ,[Direccion] "
        fSql &= "      ,[Celular] "
        fSql &= "      ,[Telefono] "
        fSql &= "      ,[Ciudad] "
        fSql &= "      ,[Departamento] "
        fSql &= "      ,[Fecha] "
        fSql &= "      ,[NumMinuta] "
        fSql &= "      ,[Peso] "
        fSql &= "      ,[Transportadora] "
        fSql &= "      ,[Estiba] "
        fSql &= "      ,[Estado1] "
        fSql &= "      ,[Estado2] "
        fSql &= "      ,[Estado3] "
        fSql &= "      ,[Estado4] "
        fSql &= "      ,[Estado5] "
        fSql &= "      ,[TipoDeCaja] "
        fSql &= "      ,[PesoFinal] "
        fSql &= "      ,[PoliticaEntrega] "
        fSql &= "      ,[FechaRegistro] "
        fSql &= "  From DespachosPlanilla Where ID = " & idNum
        Return fSql
    End Function
End Module
