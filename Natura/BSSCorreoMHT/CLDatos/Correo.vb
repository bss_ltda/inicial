﻿Public Class Correo
    Private datos As New ClsDatos
    Private Correo As New clsMHT
    Private data

    Public Sub Inicializador()
        Try
            If Not datos.AbrirConexion Then
                Exit Sub
            End If
            If AvisoLog() Then
                revisaTareasMHT()
            End If
            datos.CerrarConexion()
        Catch ex As Exception
            datos.WrtSqlError(datos.fSql, ex.StackTrace & "<br />" & ex.Message)
        End Try
    End Sub

    Sub revisaTareasMHT()
        Dim serverMHT As String
        Dim local As String = datos.local
        Dim resultado As String = ""
        Dim parametro(1) As String
        Dim dtRMAILX As DataTable
        Dim continuar As Boolean

        Console.WriteLine("Abriendo Conexión...")
        If Not datos.AbrirConexion Then
            Console.WriteLine("Error Abriendo Conexión " & datos.mensaje & vbCrLf & datos.Cadena)
            Exit Sub
        End If
        Console.WriteLine("Conexión..." & datos.conexion.State)
        datos.gsKeyWords = "revisaTareasMHT (1)"
        parametro(0) = local
        parametro(1) = "SMTPMHTSERVER"
        serverMHT = datos.ConsultarRFPARAMdt(parametro, 0)

        Console.WriteLine("SMTPMHTSERVER: " & serverMHT)
        Console.WriteLine("Consulta RMAILX...")
        dtRMAILX = datos.ConsultarRMAILXdt(parametro, 0)
        If dtRMAILX IsNot Nothing Then
            datos.gsKeyWords = "revisaTareasMHT (2)"
            If dtRMAILX.Rows(0).Item("TOT") > 50 Then
                Console.WriteLine("Es Mayor a 50...")
                datos.gsKeyWords = "revisaTareasMHT (3)"
                Console.WriteLine(IIf(dtRMAILX.Rows(0).Item("LCATALOGO") Is DBNull.Value, "", dtRMAILX.Rows(0).Item("LCATALOGO")))
                With Correo
                    .Inicializa()
                    .datos = datos
                    .catalogo = IIf(dtRMAILX.Rows(0).Item("LCATALOGO") Is DBNull.Value, "", dtRMAILX.Rows(0).Item("LCATALOGO"))
                    datos.gsKeyWords = "revisaTareasMHT (4)"
                    Console.WriteLine("Aviso Desborde")
                    .avisoDesborde("RMAILX")
                    datos.gsKeyWords = "Sale de avisoDesborde"
                End With

                'Me.EventLog1.WriteEntry("Mas de 50 Correos Pendientes.")
                Return
            End If
        End If
        dtRMAILX = datos.ConsultarRMAILXdt(parametro, 1)
        If dtRMAILX IsNot Nothing Then
            Console.WriteLine("Recorriendo RMAILX...")
            For Each row As DataRow In dtRMAILX.Rows
                datos.gsKeyWords = "revisaTareasMHT (5)"
                datos.gsKeyWords = "revisaTareasMHT, LID = " & row("LID")
                data = Nothing
                data = New RMAILX
                With data
                    .LENVIADO = "-1"
                    .LMSGERR = "En proceso"
                    .LID = row("LID")
                End With
                Console.WriteLine("Actualiza RMAILX -1 En Proceso...")
                continuar = datos.ActualizarRMAILXRow(data, 0)
                If continuar = False Then
                    Console.WriteLine("Error Actualizando " & datos.mensaje)
                Else
                    Console.WriteLine("RMAILX -1 Actualizado")
                End If
                Console.WriteLine("Inicializando envio...")
                Console.WriteLine(row("LCATALOGO"))
                With Correo
                    .Inicializa()
                    .datos = datos
                    If Not row("LCATALOGO") Is DBNull.Value Then
                        .catalogo = row("LCATALOGO")
                    End If
                    If Not row("LMOD") Is DBNull.Value Then
                        .Modulo = row("LMOD")
                    Else
                        .Modulo = ""
                    End If
                    .Asunto = row("LASUNTO")
                    If Not row("LENVIA") Is DBNull.Value Then
                        If Not row("LREF") Is DBNull.Value Then
                            .De = IIf(row("LENVIA") = "", row("LREF"), row("LENVIA"))
                        Else
                            .De = row("LENVIA")
                        End If
                    Else
                        If Not row("LREF") Is DBNull.Value Then
                            .De = row("LREF")
                        Else
                            .De = ""
                        End If
                    End If
                    Console.WriteLine("Para: " & row("LPARA"))
                    .Para = row("LPARA")
                    If Not row("LCOPIA") Is DBNull.Value Then
                        .CC = row("LCOPIA")
                    Else
                        .CC = ""
                    End If

                    .url = row("LCUERPO")

                    If InStr(.url.ToUpper, "HREF") <> 0 Then
                        parametro(0) = local
                        parametro(1) = "SMTPAVISOGRAL"
                        .url = serverMHT & datos.ConsultarRFPARAMdt(parametro, 0) & "?LID=" & row("LID")
                    Else
                        If InStr(.url.ToUpper, "HTTP") = 0 Then
                            If InStr(.url.ToUpper, ".ASP") = 0 Then
                                parametro(0) = local
                                parametro(1) = "SMTPAVISOGRAL"
                                .url = serverMHT & datos.ConsultarRFPARAMdt(parametro, 0) & "?LID=" & row("LID")
                            Else
                                .url = serverMHT & .url
                            End If
                        End If
                    End If

                    If Not row("LADJUNTOS") Is DBNull.Value Then
                        .Adjuntos = row("LADJUNTOS")
                    Else
                        .Adjuntos = ""
                    End If
                    resultado = .enviaMail()
                    If (resultado = "OK") Then
                        data = Nothing
                        data = New RMAILX
                        With data
                            .LENVIADO = "1"
                            .LMSGERR = "OK"
                            .LID = row("LID")
                        End With
                        Console.WriteLine("Actualizando RMAILX 1 OK...")
                        continuar = datos.ActualizarRMAILXRow(data, 0)
                        If continuar = False Then
                            Console.WriteLine("Error Actualizando RMAILX 1 OK..." & datos.mensaje)
                        End If
                    Else
                        data = Nothing
                        data = New RMAILX
                        With data
                            .LENVIADO = "-1"
                            .LMSGERR = resultado
                            .LID = row("LID")
                        End With
                        Console.WriteLine("Actualizando RMAILX -1 " & resultado & "..." & row("LID"))
                        continuar = datos.ActualizarRMAILXRow(data, 0)
                        If continuar = False Then
                            Console.WriteLine("Error Actualizando RMAILX -1 " & resultado & "..." & datos.mensaje)
                        Else
                            Console.WriteLine("RMAILX Actualizando")
                        End If
                    End If
                End With
            Next
        End If
        datos.CerrarConexion()
    End Sub

    Function AvisoLog() As Boolean
        Dim serverMHT As String
        Dim resultado As String = ""
        Dim parametro(1) As String
        Dim dtRFLOG As DataTable
        Dim continuar As Boolean

        Console.WriteLine("Abriendo Conexión...")
        If Not datos.AbrirConexion Then
            Console.WriteLine("Error Abriendo Conexión " & datos.mensaje & vbCrLf & datos.conexion.ConnectionString)
            Return False
        End If

        datos.gsKeyWords = "AvisoLog"
        parametro(0) = datos.local
        parametro(1) = "SMTPMHTSERVER"
        serverMHT = datos.ConsultarRFPARAMdt(parametro, 0)
        Console.WriteLine("SMTPMHTSERVER: " & serverMHT)
        Console.WriteLine("Consultando RFLOG...")
        dtRFLOG = datos.ConsultarRFLOGdt(parametro, 0)
        If dtRFLOG IsNot Nothing Then
            If dtRFLOG.Rows(0).Item("TOT") > 50 Then
                Console.WriteLine("RFLOG Mayor a 50")
                With Correo
                    .Inicializa()
                    .datos = datos
                    Console.WriteLine("Aviso Desborde RFLOG Mayor a 50")
                    .avisoDesborde("RFLOG")
                End With
                'Me.EventLog1.WriteEntry("Mas de 50 Correos Pendientes.")
                Return False
            End If
        End If

        dtRFLOG = datos.ConsultarRFLOGdt(parametro, 1)
        If dtRFLOG IsNot Nothing Then
            Console.WriteLine("Recorriendo RFLOG...")
            For Each row As DataRow In dtRFLOG.Rows
                With Correo
                    .Inicializa()
                    .datos = datos
                    .Modulo = "PGMERR"
                    .Asunto = row("PROGRAMA")
                    .De = "Error en Programa"
                    If Not row("ALERTTO") Is DBNull.Value Then
                        .Para = "BSS" & IIf(row("ALERTTO") <> "", "," & row("ALERTTO"), "")
                    Else
                        .Para = "BSS"
                    End If
                    parametro(0) = ""
                    parametro(1) = "SMTPAVISOERROR"
                    .url = serverMHT & datos.ConsultarRFPARAMdt(parametro, 0) & "?ID=" & row("ID")
                    resultado = .enviaMail()
                    Console.WriteLine("Enviando Correo... " & resultado)
                    If (resultado = "OK") Then
                        Data = Nothing
                        Data = New RFLOG
                        With Data
                            .ALERT = "2"
                            .ID = row("ID")
                        End With
                        Console.WriteLine("Actualizando RFLOG Alert 2")
                        continuar = datos.ActualizarRFLOGRow(Data, 0)
                        If continuar = False Then
                            Console.WriteLine("Error Actualizando RFLOG " & datos.mensaje)
                        End If
                    Else
                        Data = Nothing
                        Data = New RFLOG
                        With Data
                            .ALERT = "-1"
                            .ID = row("ID")
                        End With
                        Console.WriteLine("Actualizando RFLOG Alert -1")
                        continuar = datos.ActualizarRFLOGRow(Data, 0)
                        If continuar = False Then
                            Console.WriteLine("Error Actualizando RFLOG " & datos.mensaje)
                        End If
                    End If
                End With
            Next
        End If
        datos.CerrarConexion()
        Return True
    End Function

End Class
