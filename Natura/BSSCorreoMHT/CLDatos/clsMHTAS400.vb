﻿Public Class clsMHTAS400
    Public Modulo, IdMsg, Adjuntos, Asunto, De, Para, CC, url As String
    Private campoMail As String = "UEMAIL"
    Private mht As New Chilkat.Mht
    Private mailman As New Chilkat.MailMan
    Private email As New Chilkat.Email
    Private datos As New ClsDatosAS400
    Private data

    Function enviaMail() As String
        Dim resultado As String = ""
        Dim destBCC As String
        Dim parametro(1) As String
        Dim continuar As Boolean

        Console.WriteLine("Abriendo Conexión...")
        If Not datos.AbrirConexion Then
            Console.WriteLine("Error Abriendo Conexión " & datos.mensaje & vbCrLf & datos.conexionIBM.ConnectionString)
            Return datos.mensaje & vbCrLf & datos.conexionIBM.ConnectionString
        End If

        mailman.UnlockComponent("FMANRQ.CB40517_Le6wzL45oMlf")
        mht.UnlockComponent("FMANRQ.CB40517_Le6wzL45oMlf")

        parametro(0) = datos.local
        parametro(1) = "SMTPHOST"
        mailman.SmtpHost = datos.ConsultarRFPARAMdt(parametro, 0)
        Console.WriteLine("SmtpHost: " & mailman.SmtpHost)
        parametro(0) = datos.local
        parametro(1) = "SMTPAUTH"
        If datos.ConsultarRFPARAMdt(parametro, 0) <> "DIRECTO" Then
            parametro(0) = datos.local
            parametro(1) = "SMTPUSERNAME"
            mailman.SmtpUsername = datos.ConsultarRFPARAMdt(parametro, 0)
            Console.WriteLine("SMTPUSERNAME: " & mailman.SmtpUsername)
            parametro(0) = datos.local
            parametro(1) = "SMTPPASSWORD"
            mailman.SmtpPassword = datos.ConsultarRFPARAMdt(parametro, 0)
            Console.WriteLine("SMTPPASSWORD: " & mailman.SmtpPassword)
        End If
        resultado = correoMHT()
        Console.WriteLine("correoMHT: " & resultado)
        If resultado = "OK" Then
            If Adjuntos.Trim <> "" Then
                If Not Adjuntar() Then
                    Return "Error adjuntando archivos."
                End If
            End If
            email.FromName = IIf(De <> "", De, "Alertas " & My.Settings.EMP)
            parametro(0) = "CCNOT1"
            parametro(1) = "SMTPUSERMAIL"
            email.FromAddress = datos.ConsultarRFPARAMdt(parametro, 0)
            Console.WriteLine("SMTPUSERMAIL: " & email.FromAddress)
            destBCC = getBCC()
            Console.WriteLine("getBCC: " & destBCC)
            Destinatarios("TO", Para)
            If CC.Trim <> "" Then
                Destinatarios("CC", CC)
            End If
            Destinatarios("BCC", destBCC)
            email.Subject = Asunto
            Console.WriteLine("Enviando correo...")
            If mailman.SendEmail(email) Then
                resultado = "OK"
                Console.WriteLine("Correo: " & resultado)
            Else
                Console.WriteLine("Correo Error: " & mailman.LastErrorHtml)
                datos.WrtSqlError(IdMsg, mailman.LastErrorHtml)
                data = Nothing
                data = New RFPARAM
                With data
                    .Campo = "CCUDC2"
                    .Valor = "-1"
                    .CCCODE = "SMTPAUTH"
                End With
                continuar = datos.ActualizarRFPARAMRow(data, 0)
                If continuar = False Then

                End If
                resultado = "Error en Envio"
            End If
            mailman.CloseSmtpConnection()
        End If
        Return resultado

    End Function

    Sub Destinatarios(Tipo As String, Correos As String)
        Dim aDest() As String = Split(Correos, ",")
        Dim parametro(1) As String
        Dim dtRCAU As DataTable

        For Each dest In aDest
            Console.WriteLine("Destinatarios: " & dest)
            If InStr(dest, "@") = 0 Then
                'parametro(0) = campoMail
                parametro(0) = "UEMAIL"
                parametro(1) = dest.ToUpper.Trim
                dtRCAU = datos.ConsultarRCAUdt(parametro, 0)
                If dtRCAU IsNot Nothing Then
                    Console.WriteLine("Destinatarios: " & dtRCAU.Rows(0).Item("UNOM") & " - " & dtRCAU.Rows(0).Item("UEMAIL"))
                    Select Case Tipo
                        Case "TO"
                            email.AddTo(dtRCAU.Rows(0).Item("UNOM"), dtRCAU.Rows(0).Item("UEMAIL"))
                        Case "CC"
                            email.AddCC(dtRCAU.Rows(0).Item("UNOM"), dtRCAU.Rows(0).Item("UEMAIL"))
                        Case "BCC"
                            email.AddBcc(dtRCAU.Rows(0).Item("UNOM"), dtRCAU.Rows(0).Item("UEMAIL"))
                    End Select
                End If
            Else
                Select Case Tipo
                    Case "TO"
                        email.AddTo("", dest)
                    Case "CC"
                        email.AddCC("", dest)
                    Case "BCC"
                        email.AddBcc("", dest)
                End Select
            End If
        Next
    End Sub

    Function Adjuntar() As Boolean

        Dim aAdjuntos() As String = Split(Adjuntos, ",")
        For Each adjunto In aAdjuntos
            If email.AddFileAttachment(adjunto) = vbNullString Then
                datos.WrtSqlError(IdMsg, email.LastErrorHtml)
                Return False
            End If
        Next
        Return True

    End Function

    'correoMHT()	
    Function correoMHT() As String
        Dim emlStr As String

        mht.UseCids = 1
        Console.WriteLine(url.Replace(" ", ""))
        Try
            emlStr = mht.GetEML(url.Replace(" ", ""))
        Catch ex As Exception
            Console.WriteLine(ex.Message)
        End Try

        If (emlStr = vbNullString) Then
            datos.WrtSqlError(IdMsg, mht.LastErrorHtml)
            Console.WriteLine(mht.LastErrorHtml)
            Return "Error en MHT."
        End If
        If Not (email.SetFromMimeText(emlStr)) Then
            datos.WrtSqlError(IdMsg, email.LastErrorHtml)
            Console.WriteLine(email.LastErrorHtml)
            Return "Error en MimeText"
        End If
        Return "OK"

    End Function

    Public Function getBCC() As String
        Dim resultado As String = ""
        Dim dtRFPARAM As DataTable
        Dim parametro(1) As String

        campoMail = "UEMAIL"
        parametro(0) = Modulo
        Console.WriteLine("Consulta RFPARAM: " & "UEMAIL - " & Modulo)
        dtRFPARAM = datos.ConsultarRFPARAMdt2(parametro, 0)
        If dtRFPARAM IsNot Nothing Then
            resultado = dtRFPARAM.Rows(0).Item("CCDESC")
            Console.WriteLine("Resultado RFPARAM: " & resultado)
            If dtRFPARAM.Rows(0).Item("CCSDSC") <> "" Then
                campoMail = dtRFPARAM.Rows(0).Item("CCSDSC")
                Console.WriteLine("campoMail RFPARAM: " & campoMail)
            End If
        End If
        Return resultado.Trim

    End Function

    Sub avisoDesborde(Archivo As String)
        Dim parametro(1) As String
        Dim continuar As Boolean
        Dim resultado As String = ""
        datos.gsKeyWords &= " avisoDesborde (1) " & Archivo & " " & datos.conexionIBM.State()

        Console.WriteLine("Abriendo Conexión...")
        If Not datos.AbrirConexion Then
            Console.WriteLine("Error Abriendo Conexión " & datos.mensaje & vbCrLf & datos.conexionIBM.ConnectionString)
            Return
        End If

        parametro(0) = "CCUDC1"
        parametro(1) = "SMTPAUTH"
        resultado = datos.ConsultarRFPARAMdt(parametro, 0)

        datos.gsKeyWords = "avisoDesborde (2)"
        If CInt(resultado) < Hour(Now()) Then
            datos.gsKeyWords = "avisoDesborde (3)"
            data = Nothing
            data = New RFPARAM
            With data
                .Campo = "CCUDC1"
                .Valor = Hour(Now())
                .CCCODE = "SMTPAUTH"
            End With
            continuar = datos.ActualizarRFPARAMRow(data, 0)
            If continuar = False Then
                'ERROR DE ACTUALIZADO
            End If
        Else
            Return
        End If
        datos.gsKeyWords = "avisoDesborde (4)"
        mailman.UnlockComponent("FMANRQ.CB40517_Le6wzL45oMlf")
        mht.UnlockComponent("FMANRQ.CB40517_Le6wzL45oMlf")
        parametro(0) = datos.local
        parametro(1) = "SMTPHOST"
        mailman.SmtpHost = datos.ConsultarRFPARAMdt(parametro, 0)
        parametro(0) = datos.local
        parametro(1) = "SMTPAUTH"
        If datos.ConsultarRFPARAMdt(parametro, 0) <> "DIRECTO" Then
            parametro(0) = datos.local
            parametro(1) = "SMTPUSERNAME"
            mailman.SmtpUsername = datos.ConsultarRFPARAMdt(parametro, 0)
            parametro(0) = datos.local
            parametro(1) = "SMTPPASSWORD"
            mailman.SmtpPassword = datos.ConsultarRFPARAMdt(parametro, 0)
        End If
        datos.gsKeyWords = "avisoDesborde (5)"

        email.FromName = "Correo MHT " & My.Settings.EMP
        parametro(0) = "CCNOT1"
        parametro(1) = "SMTPUSERMAIL"
        email.FromAddress = datos.ConsultarRFPARAMdt(parametro, 0) '"admin.logistica@brinsa.com.co"
        Destinatarios("TO", "BSS")
        email.Subject = Asunto
        email.Body = "Desbordamiento de archivo " & Archivo
        If mailman.SendEmail(email) Then
            resultado = "OK"
        Else
            datos.WrtSqlError(IdMsg, mailman.LastErrorHtml)
            datos.gsKeyWords = "avisoDesborde (6)"
            data = Nothing
            data = New RFPARAM
            With data
                .Campo = "CCUDC2"
                .Valor = "-1"
                .CCCODE = "SMTPAUTH"
            End With
        End If
        mailman.CloseSmtpConnection()
    End Sub

    Sub Inicializa()
        Modulo = ""
        IdMsg = ""
        Adjuntos = ""
        Asunto = ""
        De = ""
        Para = ""
        CC = ""
        url = ""
    End Sub
End Class
