﻿Imports System.IO

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cbArchivos = New System.Windows.Forms.ComboBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'cbArchivos
        '
        Me.cbArchivos.FormattingEnabled = True
        Me.cbArchivos.Location = New System.Drawing.Point(103, 48)
        Me.cbArchivos.Name = "cbArchivos"
        Me.cbArchivos.Size = New System.Drawing.Size(187, 21)
        Me.cbArchivos.TabIndex = 0
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(308, 48)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(59, 21)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(449, 345)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.cbArchivos)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

        For Each arch In archivoList
            Me.cbArchivos.Items.Add(arch.Nombre)
        Next

    End Sub



    Friend WithEvents cbArchivos As ComboBox
    Friend WithEvents Button1 As Button


End Class
