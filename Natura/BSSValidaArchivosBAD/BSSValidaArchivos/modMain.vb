﻿
Imports System.IO

Module modMain

    Public Class Archivo
        Public Nombre As String
        Public Descripcion As String
        Public Columnas As Columna()
    End Class

    Public Class Columna
        Public Titulo As String
        Public Tipo As String
    End Class

    Public archivoList As List(Of Archivo)

    Sub CargaArchivos()
        Dim dataPath As String = System.Windows.Forms.Application.StartupPath & "\..\..\data\"
        Dim customerListPath = Path.GetFullPath(Path.Combine(dataPath, "archivos.xml"))

        'Dim customerListPath = "C:\Dropbox\AppBSS\VisualStudio\Natura\BSSValidaArchivos\BSSValidaArchivos\data\archivos.xml"
        Dim list = XDocument.Load(customerListPath).Root.Elements("Archivo")

        archivoList = (From ee In list
                       Select New Archivo With {
                            .Nombre = CStr(ee.<Nombre>.Value),
                            .Descripcion = CStr(ee.<Descripcion>.Value),
                            .Columnas = (
                                       From o In ee...<Columnas>...<Columna>
                                       Select New Columna With {
                                            .Titulo = CStr(o.<Titulo>.Value),
                                            .Tipo = CStr(o.<Tipo>.Value)}).ToArray()
                        }).ToList()

    End Sub

End Module
