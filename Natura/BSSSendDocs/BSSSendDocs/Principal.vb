﻿Imports System.IO
Imports System.Text
Imports CLDatos

Module Principal
    Private datos As New ClsDatosSendDocs
    Private data

    Sub Main()
        Dim aSubParam() As String
        Dim TRANSPORTADORA As String = ""
        Dim ARCHIVO As String = ""
        Dim ESTIBA As String = ""
        Dim IDLOG As String = ""

        Environment.GetCommandLineArgs()
        For Each parametro In Environment.GetCommandLineArgs()
            aSubParam = Split(parametro, "=")
            Select Case UCase(aSubParam(0))
                Case "TRANSPORTADORA"
                    TRANSPORTADORA = aSubParam(1)
                Case "ARCHIVO"
                    ARCHIVO = aSubParam(1)
                Case "ESTIBA"
                    ESTIBA = aSubParam(1)
                Case "IDLOG"
                    IDLOG = aSubParam(1)
            End Select
        Next
        Try
            SFTP(TRANSPORTADORA, ARCHIVO, ESTIBA, IDLOG)
        Catch ex As Exception
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\BSSSendDocsError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("Se encontro un error en BSSSendDocs.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try

    End Sub

    Sub SFTP(ByVal Transportadora As String, ByVal Archivo As String, ByVal Estiba As String, ByVal IDLOG As String)
        Dim parametro(1) As String
        Dim email As New Chilkat.Email
        Dim aParam() As String
        Dim mailman As New Chilkat.MailMan
        Dim mht As New Chilkat.Mht
        Dim success As Boolean
        aParam = Split(Command(), " ")
        If UBound(aParam) <> 1 Then
            ' End
        End If

        success = mailman.UnlockComponent("FMANRQ.CB40517_Le6wzL45oMlf")
        If success = False Then
            Console.WriteLine(mailman.LastErrorHtml)
            'rflog("mailman.UnlockComponent", mailman.LastErrorHtml, "mailman.UnlockComponent()", "1")
            If IDLOG <> "" Then
                data = Nothing
                data = New RFLOG
                With data
                    .ID = IDLOG
                    .ALERT = "1"
                    .MENSAJE = mailman.LastErrorHtml
                End With
                datos.ActualizarRFLOGRow(data, 1)
            End If
            Exit Sub
        End If
        success = mht.UnlockComponent("FMANRQ.CB40517_Le6wzL45oMlf")
        If success = False Then
            Console.WriteLine(mht.LastErrorHtml)
            'rflog("mht.UnlockComponent", mht.LastErrorHtml, "mht.UnlockComponent()", "1")
            If IDLOG <> "" Then
                data = Nothing
                data = New RFLOG
                With data
                    .ID = IDLOG
                    .ALERT = "1"
                    .MENSAJE = mht.LastErrorHtml
                End With
                datos.ActualizarRFLOGRow(data, 1)
            End If
            Exit Sub
        End If

        If Not datos.AbrirConexion() Then
            Exit Sub
        End If

        parametro(0) = ""
        parametro(1) = "SMTPHOST"
        mailman.SmtpHost = datos.ConsultarRFPARAMdt(parametro, 0)
        parametro(0) = ""
        parametro(1) = "SMTPUSERNAME"
        mailman.SmtpUsername = datos.ConsultarRFPARAMdt(parametro, 0)
        parametro(0) = ""
        parametro(1) = "SMTPPASSWORD"
        mailman.SmtpPassword = datos.ConsultarRFPARAMdt(parametro, 0)
        mailman.SmtpPort = 25
        mailman.StartTLS = 1
        mailman.SmtpSsl = False

        email.Subject = "Estiba No: " & Estiba & " Transportadora: " & Transportadora
        email.Body = "Estiba No: " & Estiba & " Transportadora: " & Transportadora
        email.From = "support@Natura.Net"
        email.FromAddress = "support@Natura.Net"
        email.FromName = "support@Natura.Net"

        If Transportadora = "ENVIA" Then
            email.AddTo("Asesor SAC Natura 2", "asesorsacnatura2@enviacolvanes.com.co")
            email.AddTo("Asesor SAC Natura 2", "asesorsacnatura1@enviacolvanes.com.co")
        End If

        If Transportadora = "DEPRISA" Then
            email.AddTo("Transportesnaturacol_6", "Transportesnaturacol_6@natura.net")
            email.AddTo("Transportesnaturacol_7", "Transportesnaturacol_7@natura.net")
        End If

        If Transportadora = "SERVIENTREGAS.A" Then
            email.AddTo("Luis Padilla", "Luis.Padillau@co.servientrega.com")
            email.AddTo("Jhon Camacho", "jhon.camacho@co.servientrega.com")
        End If

        If Transportadora = "INTERRAPIDISIMOS.A" Then
            email.AddTo("Interrapidisimo", "coord.natura@interrapidisimo.com")
        End If

        email.AddTo("Natura", "Transportesnaturacol_4@natura.net")
        email.AddCC("Criss Velandia", "CrisVelandia@natura.net")
        email.AddCC("Ivonne Perez", "IvonnePerez@natura.net")
        email.AddCC("Carol Morales", "carolmorales@natura.net")
        email.AddCC("Eduard Abril", "eduardabril@natura.net")
        email.AddCC("Despachos", "DespachosColombia@natura.net")
        email.AddCC("Alertas", "AlertasNatura@gmail.com")
        email.AddCC("Marlon Hurtado", "marlonhurtado.sonda@natura.net")
        email.AddBcc("Alertas Correo", "alertascorreo2015@gmail.com")
        email.AddTo("BSS", "jesus.santamaria@bssltda.com")



        '  To add file attachments to an email, call AddFileAttachment
        '  once for each file to be attached.  The method returns
        '  the content-type of the attachment if successful, otherwise
        '  returns cknull

        Dim contentType As String
        contentType = email.AddFileAttachment(My.Settings.CrpetRaiz & Archivo)
        If (contentType = vbNullString) Then
            Console.WriteLine(email.LastErrorText)
            Exit Sub
        End If
        success = mailman.SendEmail(email)

        If (success <> True) Then
            Console.WriteLine(mailman.LastErrorHtml)
            'rflog("mailman.SendEmail", mailman.LastErrorHtml, "mailman.SendEmail()", "1")
            If IDLOG <> "" Then
                data = Nothing
                data = New RFLOG
                With data
                    .ID = IDLOG
                    .ALERT = "1"
                    .MENSAJE = mailman.LastErrorHtml
                End With
                datos.ActualizarRFLOGRow(data, 1)
            End If
        Else
            Console.WriteLine("Enviado")
            If IDLOG <> "" Then
                data = Nothing
                data = New RFLOG
                With data
                    .ID = IDLOG
                    .MENSAJE = "OK"
                End With
                datos.ActualizarRFLOGRow(data, 0)
            End If
        End If
    End Sub

    Function rflog(ByVal OPERACION As String, ByVal EVENTO As String, ByVal TXTSQL As String, ByVal ALERT As String) As Boolean
        Dim continuar As Boolean
        data = Nothing
        data = New RFLOG
        With data
            .USUARIO = Net.Dns.GetHostName()
            .OPERACION = OPERACION
            .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
            .EVENTO = EVENTO
            .TXTSQL = TXTSQL
            .ALERT = ALERT
        End With
        continuar = datos.GuardarRFLOGRow(data)
        Return continuar
    End Function

End Module
