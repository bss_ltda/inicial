﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Text

Public Class ClsDatosSendDocs
    Private conexion As SqlConnection
    Private adapter As SqlDataAdapter = New SqlDataAdapter()
    Private builder As SqlCommandBuilder
    Private command As SqlCommand
    Private dt As DataTable
    Private WithEvents ds As DataSet
    Public fSql As String
    Private numFilas As Integer
    Public appPath As String = Path.GetFullPath(My.Application.Info.DirectoryPath & "\Log\")
    Private data
    Public mensaje As String

#Region "ACTUALIZAR"

    ''' <summary>
    ''' Actualiza la tabla RFLOG 
    ''' </summary>
    ''' <param name="datos">RFLOG</param>
    ''' <param name="tipo">0- SET MENSAJE = @MENSAJE WHERE ID = @ID</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 04/08/2016</remarks>
    Public Function ActualizarRFLOGRow(ByVal datos As RFLOG, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean
        Select Case tipo
            Case 0
                fSql = "UPDATE RFLOG SET "
                fSql &= " MENSAJE = '" & datos.MENSAJE & "'"
                fSql &= " WHERE ID = " & datos.ID
            Case 1
                fSql = "UPDATE RFLOG SET "
                fSql &= " MENSAJE = '" & datos.MENSAJE & "', "
                fSql &= " ALERT = " & datos.ALERT & ""
                fSql &= " WHERE ID = " & datos.ID
        End Select

        command = New SqlCommand
        command.Connection = conexion
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As SqlException
            continuarguardando = False
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo actualizar la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()

                data = Nothing
                data = New RFLOG
                With data
                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Actualizar RFLOG"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarRFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

#End Region

#Region "CONSULTAR"

    ''' <summary>
    ''' Consulta la tabla RFPARAM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por CCCODE(1) devuelve (0)</param>
    ''' <returns>Un String</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 04/05/2016</remarks>
    Public Function ConsultarRFPARAMdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As String
        dt = Nothing
        ds = Nothing
        Console.WriteLine(conexion.ConnectionString)
        Select Case tipoparametro
            Case 0
                If parametroBusqueda(0) = "" Then
                    parametroBusqueda(0) = "CCDESC"
                End If
                fSql = "USE InfoNatura SELECT " & parametroBusqueda(0) & " AS DATO FROM RFPARAM WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER('" & parametroBusqueda(1) & "')"
                Console.WriteLine(fSql)
        End Select
        adapter = New SqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey

        builder = New SqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        Return dt.Rows(0).Item(0)
    End Function

#End Region

#Region "FUNCIONES"

    Public Function AbrirConexion() As Boolean
        Dim abierta As Boolean = False
        Try
            ' Si el directorio no existe, crearlo
            If Not Directory.Exists(appPath) Then
                Directory.CreateDirectory(appPath)
            End If
            If conexion IsNot Nothing Then
                If conexion.State = ConnectionState.Open Then
                    abierta = True
                Else
                    If My.Settings.PRUEBAS = "SI" Then
                        conexion = New SqlConnection(My.Settings.BASEDATOSLOCAL)
                    Else
                        conexion = New SqlConnection(My.Settings.BASEDATOS)
                    End If

                    conexion.Open()
                    abierta = True
                End If
            Else
                If My.Settings.PRUEBAS = "SI" Then
                    conexion = New SqlConnection(My.Settings.BASEDATOSLOCAL)
                Else
                    conexion = New SqlConnection(My.Settings.BASEDATOS)
                End If

                conexion.Open()
                abierta = True
            End If
        Catch ex1 As SqlException
            abierta = False
            mensaje = ex1.InnerException.ToString & vbCrLf & ex1.Message
        Catch ex As Exception
            abierta = False
            mensaje = ex.Message
        Finally
            If abierta = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo realizar la conexión.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & "Conexion: " & conexion.ConnectionString & vbCrLf & mensaje)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End If
        End Try
        Return abierta
    End Function

    Public Function CerrarConexion() As Boolean
        Dim cerrada As Boolean = False
        Try
            conexion.Close()
            cerrada = True
        Catch ex1 As SqlException
            cerrada = False
            mensaje = ex1.Message
        Catch ex As Exception
            cerrada = False
            mensaje = ex.Message
        Finally
            If cerrada = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo Cerrar la conexión.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & "Conexion: " & conexion.ConnectionString)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End If
        End Try
        Return cerrada
    End Function

#End Region

#Region "GUARDAR"

    ''' <summary>
    ''' Guarda RFLOG
    ''' </summary>
    ''' <param name="datos">RFLOG</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 22/04/2016</remarks>
    Public Function GuardarRFLOGRow(ByVal datos As RFLOG) As Boolean
        Dim continuarguardandoRFLOG As Boolean
        fSql = "INSERT INTO RFLOG (USUARIO, OPERACION, PROGRAMA, EVENTO, TXTSQL, ALERT)"
        fSql &= " VALUES ('" & datos.USUARIO & "', '" & datos.OPERACION & "', '" & datos.PROGRAMA & "', '" & datos.EVENTO.Replace("'", "''") & "', '" & datos.TXTSQL.Replace("'", "''") & "', " & datos.ALERT & ")"

        command = New SqlCommand
        command.Connection = conexion
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardandoRFLOG = True
        Catch ex As SqlException
            continuarguardandoRFLOG = False
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardandoRFLOG = False
            mensaje = ex.Message
        Finally
            If continuarguardandoRFLOG = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End If
        End Try
        Return continuarguardandoRFLOG
    End Function

#End Region

End Class

Public Class RFLOG
    Public USUARIO As String
    Public OPERACION As String
    Public PROGRAMA As String
    Public EVENTO As String
    Public TXTSQL As String
    Public ALERT As String
    Public MENSAJE As String
    Public ID As String
End Class
