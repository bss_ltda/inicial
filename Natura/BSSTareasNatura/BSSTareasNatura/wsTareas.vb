﻿Imports System.IO
Imports CLDatos

Public Class wsTareas
    Dim wsNum As String
    Dim wsResult As String
    Private WithEvents m_timer As New Timers.Timer(30000)
    Public sEstado As String = ""
    Private datos As New ClsDatos


    Protected Overrides Sub OnStart(ByVal args() As String)
        Me.EventLog1.WriteEntry("Inicia")
        m_timer.Enabled = True
    End Sub

    Protected Overrides Sub OnStop()
        m_timer.Stop()
    End Sub

    Private Sub m_timer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles m_timer.Elapsed
        'Dim resultado As String = ""

        Try
            m_timer.Enabled = False
            revisaTareas()
        Catch ex As Exception
            Me.EventLog1.WriteEntry(datos.fSql & vbCrLf & ex.Message & vbCrLf & ex.StackTrace, EventLogEntryType.Error)
        End Try
        m_timer.Enabled = True
    End Sub

End Class
