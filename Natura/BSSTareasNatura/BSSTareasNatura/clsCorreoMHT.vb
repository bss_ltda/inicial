﻿Public Class clsCorreoMHT
    Public Modulo As String
    Public Referencia As String
    Public Asunto As String
    Public Remitente As String
    Public Destinatario As String
    Public ConCopiaA As String
    Public Mensaje As String
    Public Adjuntos As String
    Public Enviado As String
    Public Creado As String
    Public Actualizado As String
    Public DB As clsConexion


    Function EnviaCorreo()
        Dim IdMail
        Dim fSql, vSql As String

        IdMail = DB.CalcConsec("RMAILX", "99999999")
        fSql = " INSERT INTO RMAILX( "
        vSql = " VALUES ( "
        fSql = fSql & " LID       , " : vSql = vSql & " " & IdMail & ", "         '//8P0    ID
        fSql = fSql & " LMOD      , " : vSql = vSql & "'" & Modulo & "', "        '//25A   Modulo
        fSql = fSql & " LREF      , " : vSql = vSql & "'" & Referencia & "', "    '//25A   Referencia
        fSql = fSql & " LASUNTO   , " : vSql = vSql & "'" & Asunto & "', "        '//102A  Asunto
        fSql = fSql & " LENVIA    , " : vSql = vSql & "'" & Remitente & "', "     '//152A  Remitente
        fSql = fSql & " LPARA     , " : vSql = vSql & "'" & Destinatario & "', "  '//2002A Destinatario
        fSql = fSql & " LCOPIA    , " : vSql = vSql & "'" & ConCopiaA & "', "      '//2002A Con Copia A
        fSql = fSql & " LCUERPO   , " : vSql = vSql & "'" & Mensaje & "', "       '//10002AMensaje
        fSql = fSql & " LADJUNTOS ) " : vSql = vSql & "'" & Adjuntos & "') "      '//10002AAdjuntos
        DB.ExecuteSQL(fSql & vSql)
        EnviaCorreo = IdMail

    End Function

    Sub AddLine(txt)

        If txt = "" Then
            Mensaje = ""
        Else
            Mensaje = Mensaje & CStr(txt) & "<br/>"
        End If


    End Sub

    Function AvisarA()
        Dim rs As New ADODB.RecordSet
        Dim fSql As String
        Dim resultado As String = ""
        fSql = " SELECT CCNOT1 FROM ZCC WHERE CCTABL = 'FLAVISOS' AND CCCODE = '" & Modulo & "' "
        If DB.OpenRS(rs, fSql) Then
            resultado = rs("CCNOT1").Value
        End If
        rs.Close()
        rs = Nothing
        Return resultado

    End Function

    Sub Inicializa()
        Modulo = ""
        Referencia = ""
        Asunto = ""
        Remitente = ""
        Destinatario = ""
        ConCopiaA = ""
        Mensaje = ""
        Adjuntos = ""
        Enviado = ""
        Creado = ""
        Actualizado = ""
    End Sub


End Class
