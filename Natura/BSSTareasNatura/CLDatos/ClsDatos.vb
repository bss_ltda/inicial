﻿Imports System.Data.SqlClient
Imports System.Text
Imports System.IO

Public Class ClsDatos
    Public Cadena As String
    Private ReadOnly conexion As SqlConnection
    Private adapter As SqlDataAdapter = New SqlDataAdapter()
    Private trans As SqlTransaction
    Private builder As SqlCommandBuilder
    Private command As SqlCommand
    Private WithEvents ds As DataSet
    Public fSql As String
    Private numFilas As Integer
    Private continuarguardando As Boolean
    Private dt As DataTable
    Private appPath As String = Path.GetFullPath(My.Application.Info.DirectoryPath & "\Log\")
    Private data
    Public mensaje As String

    Sub New()
        If My.Settings.PRUEBAS = "SI" Then
            Cadena = My.Settings.BASEDATOSLOCAL
        Else
            Cadena = My.Settings.BASEDATOS
        End If
        Try
            conexion = New SqlConnection(Cadena)
            ' Si el directorio no existe, crearlo
            If Not Directory.Exists(appPath) Then
                Directory.CreateDirectory(appPath)
            End If
        Catch ex As Exception
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\CONEXIONError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo Establecer conexion. " & vbCrLf & ex.ToString)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try
    End Sub

#Region "ACTUALIZAR"

    ''' <summary>
    ''' Actualiza la tabla RFTASK 
    ''' </summary>
    ''' <param name="datos">RFTASK</param>
    ''' <param name="tipo">0- @BD - SET TLXMSG = @TLXMSG, STS1 = @STS1, TUPDDAT = NOW(), TMSG = @TMSG, TCMD = @TCMD WHERE TNUMREG = @TNUMREG</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 21/04/2016</remarks>
    Public Function ActualizarRFTASKRow(ByVal datos As RFTASK, ByVal tipo As Integer) As Boolean

        trans = conexion.BeginTransaction()
        Select Case tipo
            Case 0
                fSql = "USE " & datos.BD & " SET LANGUAGE 'English'; UPDATE RFTASK SET "
                fSql &= " TLXMSG = '" & datos.TLXMSG & "', "
                fSql &= " STS1 = " & datos.STS1 & ", "
                fSql &= " TUPDDAT = '" & datos.TUPDDAT & "', "
                fSql &= " TMSG = '" & datos.TMSG & "', "
                fSql &= " TCMD = '" & datos.TCMD & "'"
                fSql &= " WHERE TNUMREG = " & datos.TNUMREG
        End Select

        command = New SqlCommand
        command.Connection = conexion
        command.Transaction = trans
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As Data.SqlClient.SqlException
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFTASKError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo guardar en la tabla RFTASK.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.ToString
        Catch ex As Exception
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFTASKError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo guardar en la tabla RFTASK.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.ToString
        Finally
            If continuarguardando = True Then
                trans.Commit()
            Else
                data = Nothing
                data = New RFLOG
                With data
                    .bd = datos.BD
                    .USUARIO = System.Net.Dns.GetHostName()
                    .OPERACION = "Guardar RFTASK"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarRFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

#End Region

#Region "CONSULTAR"

    ''' <summary>
    ''' Consulta la tabla RFTASK con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- BD(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 21/04/2016</remarks>
    Public Function ConsultarRFTASKdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing
        
        Select Case tipoparametro
            Case 0
                fSql = "USE " & parametroBusqueda(0) & " SELECT * FROM  RFTASK WHERE STS1 = 0 AND THLD = 0 AND TFPROXIMA <= getDate()"
        End Select
        adapter = New SqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New SqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        
        Return dt
    End Function

#End Region

#Region "FUNCIONES"

    Public Function AbrirConexion() As Boolean
        Dim abierta As Boolean = False
        Try
            If conexion.State = ConnectionState.Open Then
                abierta = True
            Else
                conexion.Open()
                abierta = True
            End If
            Console.WriteLine("Conexión " & abierta & Cadena)
        Catch ex1 As SqlException
            abierta = False
            mensaje = ex1.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo establecer la Conexion.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex1.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Catch ex As Exception
            abierta = False
            mensaje = ex.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo establecer la Conexion.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try
        Return abierta
    End Function

    Public Function CerrarConexion() As Boolean
        Dim cerrada As Boolean = False
        Try
            conexion.Close()
            cerrada = True
        Catch ex1 As SqlException
            cerrada = False
            mensaje = ex1.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo detener la Conexion.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex1.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Catch ex As Exception
            cerrada = False
            mensaje = ex.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo detener la Conexion.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try
        Return cerrada
    End Function

#End Region

#Region "GUARDAR"

    ''' <summary>
    ''' Guarda RFLOG
    ''' </summary>
    ''' <param name="datos">RFLOG</param>
    ''' <returns>true si se guarda con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 04/05/2016</remarks>
    Public Function GuardarRFLOGRow(ByVal datos As RFLOG) As Boolean
        trans = conexion.BeginTransaction()
        Using sqlCommand As New SqlCommand()
            With sqlCommand
                .CommandText = "USE " & datos.BD & " INSERT INTO RFLOG (USUARIO, OPERACION, PROGRAMA, ALERT, EVENTO, TXTSQL) VALUES (" &
                    "'" & datos.usuario & "', " &
                    "'" & datos.OPERACION & "', " &
                    "'" & datos.programa & "', " &
                    datos.alert & ", " &
                    "'" & datos.evento & "', " &
                    "'" & datos.txtsql.Replace("'", "''") & "')"
                .Connection = conexion
                .Transaction = trans
            End With
            Try
                sqlCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As SqlException
                trans.Rollback()
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Catch ex As Exception
                trans.Rollback()
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Finally
                If continuarguardando = True Then
                    trans.Commit()
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

#End Region

End Class

Public Class RFTASK
    Public BD As String
    Public TLXMSG As String
    Public STS1 As String
    Public TUPDDAT As String
    Public TMSG As String
    Public TNUMREG As String
    Public TCMD As String
End Class

Public Class RFLOG
    Public BD As String
    Public usuario As String
    Public programa As String
    Public alert As String
    Public evento As String
    Public lkey As String
    Public txtsql As String
    Public OPERACION As String
End Class
