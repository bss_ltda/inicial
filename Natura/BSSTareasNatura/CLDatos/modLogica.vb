﻿Imports System.Globalization
Imports System.IO
Imports System.Data.SqlClient

Public Module modLogica
    Private datos As New ClsDatos
    Private data

    Sub revisaTareas()
        'Para no capturar el error durante el debug poner Si
        If My.Settings.PRUEBAS = "NO" Then
            Try
                EjecutaProceso()
            Catch ex As Exception
                WrtSqlError(datos.fSql, ex.Message & "<br />" & ex.StackTrace.Replace(" en ", "<br />"))
            End Try
        Else
            EjecutaProceso()
        End If
    End Sub

    Sub EjecutaProceso()
        Dim pgm, param, exe As String
        Dim sts As Integer
        Dim msgT As String
        Dim tLXmsg As String
        Dim Accion As String
        Dim basesDatos() As String
        Dim dtRFTASK As DataTable
        Dim parametro(0) As String
        Dim continuar As Boolean
        Dim mensajeError As String = ""

        Console.WriteLine("Abriendo Conexión...")
        If Not datos.AbrirConexion() Then
            Console.WriteLine("Error Abriendo Conexión " & datos.mensaje & vbCrLf & datos.Cadena)
            Exit Sub
        End If

        basesDatos = My.Settings.BASESDEDATOS.Split(",")
        Console.WriteLine("Recorriendo Bases de Datos...")
        For i = 0 To basesDatos.Length - 1
            Console.WriteLine("BD " & basesDatos(i))
            parametro(0) = basesDatos(i)
            Console.WriteLine("Consultando Tareas...")
            dtRFTASK = datos.ConsultarRFTASKdt(parametro, 0)
            If dtRFTASK IsNot Nothing Then
                For Each row As DataRow In dtRFTASK.Rows
                    Console.WriteLine("Recorriendo Tareas...")
                    Accion = UCase(row("TTIPO"))
                    If row("FWRKID") <> 0 Then
                        If Accion <> "ULTIMO" Then
                            Accion = "PAQUETE"
                        End If
                    Else
                        Accion = UCase(row("TTIPO"))
                    End If
                    Console.WriteLine("Case " & Accion)
                    Select Case Accion
                        Case UCase("Shell")
                            Console.WriteLine("Programa: " & row("TPGM"))
                            Console.WriteLine("parametros: " & row("TPRM"))
                            pgm = row("TPGM")
                            param = row("TPRM")
                            exe = pgm & " " & param.Replace("|", " ")
                            Console.WriteLine("Comando: " & exe)
                            sts = 1
                            msgT = "Comando Enviado."
                            tLXmsg = "EN PROCESO"
                            data = Nothing
                            data = New RFTASK
                            With data
                                .bd = basesDatos(i)
                                .TLXMSG = tLXmsg
                                .STS1 = sts
                                .TUPDDAT = Now().ToString("yyyy-MM-dd HH:mm:ss")
                                .TMSG = msgT
                                .TCMD = "" & exe & ""
                                .TNUMREG = row("TNUMREG")
                            End With
                            Console.WriteLine("Actualizando RFTASK STS1 = 1")
                            continuar = datos.ActualizarRFTASKRow(data, 0)
                            If continuar = True Then
                                Console.WriteLine("RFTASK Actualizado")
                                Try
                                    Console.WriteLine("Ejecutando: " & exe)
                                    Shell(exe, AppWinStyle.Hide, False, -1)
                                Catch ex As Exception
                                    Console.WriteLine("Error Ejecutando: " & exe & vbCrLf & ex.Message)
                                    sts = -1
                                    msgT = ex.Message.Replace("'", "''")
                                    tLXmsg = "ERROR"
                                    data = Nothing
                                    data = New RFTASK
                                    With data
                                        .bd = basesDatos(i)
                                        .TLXMSG = tLXmsg
                                        .STS1 = sts
                                        .TUPDDAT = Now().ToString("yyyy-MM-dd HH:mm:ss")
                                        .TMSG = msgT
                                        .TCMD = "" & exe & ""
                                        .TNUMREG = row("TNUMREG")
                                    End With
                                    Console.WriteLine("Actualizando RFTASK...")
                                    continuar = datos.ActualizarRFTASKRow(data, 0)
                                    If continuar = False Then
                                        Console.WriteLine("Error Actualizando RFTASK " & datos.mensaje)
                                        'ERROR DE ACTUALIZADO
                                        datos.CerrarConexion()
                                        Exit Sub
                                    Else
                                        Console.WriteLine("RFTASK Actualizado")
                                        data = Nothing
                                        data = New RFLOG
                                        With data
                                            .bd = basesDatos(i)
                                            .USUARIO = System.Net.Dns.GetHostName()
                                            .OPERACION = "Error Ejecutando " & exe
                                            .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                                            .EVENTO = ex.ToString
                                            .TXTSQL = exe
                                            .ALERT = 1
                                        End With
                                        datos.GuardarRFLOGRow(data)
                                    End If
                                End Try
                            Else
                                Console.WriteLine("Error Actualizando RFTASK STS1 = 1" & vbCrLf & datos.mensaje)
                                'ERROR DE ACTUALIZADO
                                datos.CerrarConexion()
                                Exit Sub
                            End If
                        Case UCase("Ultimo")
                            exe = "ImprimeDocumentos.exe " & row("FWRKID")
                            Console.WriteLine("Comando: " & exe)
                            sts = 1
                            msgT = "Paquete Enviado."
                            tLXmsg = "ENVIADO"
                            data = Nothing
                            data = New RFTASK
                            With data
                                .bd = basesDatos(i)
                                .TLXMSG = tLXmsg
                                .STS1 = sts
                                .TUPDDAT = Now().ToString("yyyy-MM-dd HH:mm:ss")
                                .TMSG = msgT
                                .TCMD = "" & exe & ""
                                .TNUMREG = row("TNUMREG")
                            End With
                            Console.WriteLine("Actualizando RFTASK STS1 = 1")
                            continuar = datos.ActualizarRFTASKRow(data, 0)
                            If continuar = False Then
                                Console.WriteLine("Error Actualizando RFTASK STS1 = 1" & vbCrLf & datos.mensaje)
                                'ERROR AL ACTUALIZAR
                                datos.CerrarConexion()
                                Exit Sub
                            Else
                                Console.WriteLine("RFTASK Actualizado")
                                Try
                                    Console.WriteLine("Ejecutando: " & exe)
                                    Shell(exe, AppWinStyle.Hide, False, -1)
                                Catch ex As Exception
                                    Console.WriteLine("Error Ejecutando: " & exe & vbCrLf & ex.Message)
                                    data = Nothing
                                    data = New RFLOG
                                    With data
                                        .bd = basesDatos(i)
                                        .USUARIO = System.Net.Dns.GetHostName()
                                        .OPERACION = "Error Ejecutando " & exe
                                        .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                                        .EVENTO = ex.ToString
                                        .TXTSQL = exe
                                        .ALERT = 1
                                    End With
                                    datos.GuardarRFLOGRow(data)
                                    'msgT = ex.Message.Replace("'", "''")
                                    'WrtSqlError(exe, msgT)
                                End Try
                            End If
                        Case UCase("Rutina")
                            Console.WriteLine("Programa: " & row("TPGM"))
                            pgm = row("TPGM")
                            'Ejecutable de rutinas con parametro TNUMREG de RFTASK
                            exe = pgm & " " & row("TNUMREG")
                            Console.WriteLine("Comando: " & exe)
                            sts = 1
                            msgT = "Comando Enviado."
                            tLXmsg = "EJECUTADA"
                            Try
                                Console.WriteLine("Ejecutando: " & exe)
                                Shell(exe, AppWinStyle.Hide, False, -1)
                            Catch ex As Exception
                                sts = -1
                                msgT = ex.Message.Replace("'", "''")
                                tLXmsg = "ERROR"
                                Console.WriteLine("Error Ejecutando: " & exe & vbCrLf & ex.Message)
                                data = Nothing
                                data = New RFLOG
                                With data
                                    .bd = basesDatos(i)
                                    .USUARIO = System.Net.Dns.GetHostName()
                                    .OPERACION = "Error Ejecutando " & exe
                                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                                    .EVENTO = ex.ToString
                                    .TXTSQL = exe
                                    .ALERT = 1
                                End With
                                datos.GuardarRFLOGRow(data)
                            End Try
                            data = Nothing
                            data = New RFTASK
                            With data
                                .bd = basesDatos(i)
                                .TLXMSG = tLXmsg
                                .STS1 = sts
                                .TUPDDAT = Now().ToString("yyyy-MM-dd HH:mm:ss")
                                .TMSG = msgT
                                .TCMD = "" & exe & ""
                                .TNUMREG = row("TNUMREG")
                            End With
                            Console.WriteLine("Actualizando RFTASK...")
                            continuar = datos.ActualizarRFTASKRow(data, 0)
                            If continuar = False Then
                                Console.WriteLine("Error Actualizando RFTASK..." & datos.mensaje)
                                'ERROR AL ACTUALIZAR
                                datos.CerrarConexion()
                                Exit Sub
                            Else
                                Console.WriteLine("RFTASK Actualizado")
                            End If
                        Case UCase("Java")

                        Case UCase("SFTP")
                            Console.WriteLine("Programa: " & row("TPGM"))
                            Console.WriteLine("Parametros: " & row("TPRM"))
                            pgm = row("TPGM")
                            param = row("TPRM")
                            exe = pgm & " " & param.Replace("|", " ") & " TAREA=" & row("TNUMREG")
                            Console.WriteLine("Comando: " & exe)
                            sts = 1
                            msgT = "Comando Enviado."
                            tLXmsg = "EN PROCESO"
                            data = Nothing
                            data = New RFTASK
                            With data
                                .bd = basesDatos(i)
                                .TLXMSG = tLXmsg
                                .STS1 = sts
                                .TUPDDAT = Now().ToString("yyyy-MM-dd HH:mm:ss")
                                .TMSG = msgT
                                .TCMD = "" & exe & ""
                                .TNUMREG = row("TNUMREG")
                            End With
                            Console.WriteLine("Actualiza Tarea")
                            continuar = datos.ActualizarRFTASKRow(data, 0)
                            If continuar = True Then
                                Console.WriteLine("RFTASK Actualizado")
                                Try
                                    Console.WriteLine("Ejecuta Programa: " & exe)
                                    Shell(exe, AppWinStyle.Hide, False, -1)
                                Catch ex As Exception
                                    Console.WriteLine("Error al ejecutar " & exe)
                                    sts = -1
                                    msgT = ex.Message.Replace("'", "''")
                                    tLXmsg = "ERROR"
                                    data = Nothing
                                    data = New RFTASK
                                    With data
                                        .bd = basesDatos(i)
                                        .TLXMSG = tLXmsg
                                        .STS1 = sts
                                        .TUPDDAT = Now().ToString("yyyy-MM-dd HH:mm:ss")
                                        .TMSG = msgT
                                        .TCMD = "" & exe & ""
                                        .TNUMREG = row("TNUMREG")
                                    End With
                                    Console.WriteLine("Actualizando RFTASK...")
                                    continuar = datos.ActualizarRFTASKRow(data, 0)
                                    If continuar = False Then
                                        Console.WriteLine("Error Actualizando RFTASK " & datos.mensaje)
                                        'ERROR DE ACTUALIZADO
                                        datos.CerrarConexion()
                                        Exit Sub
                                    Else
                                        Console.WriteLine("RFTASK Actualizado")
                                        data = Nothing
                                        data = New RFLOG
                                        With data
                                            .bd = basesDatos(i)
                                            .USUARIO = System.Net.Dns.GetHostName()
                                            .OPERACION = "Error Ejecutando " & exe
                                            .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                                            .EVENTO = ex.ToString
                                            .TXTSQL = exe
                                            .ALERT = 1
                                        End With
                                        datos.GuardarRFLOGRow(data)
                                    End If
                                End Try
                            Else
                                Console.WriteLine("Error Actualizando RFTASK " & datos.mensaje)
                                'ERROR DE ACTUALIZADO
                                datos.CerrarConexion()
                                Exit Sub
                            End If
                        Case UCase("Procedimiento")
                            Console.WriteLine("Programa: " & row("TPGM"))
                            Console.WriteLine("Parametros: " & row("TPRM"))
                            pgm = row("TPGM")
                            param = row("TPRM")
                            'Ejecutable de Procedimientos con parametros PROC PARAMS y TNUMREG de RFTASK
                            exe = pgm & " " & param & " TAREA=" & row("TNUMREG")
                            Console.WriteLine("Comando: " & exe)
                            sts = 1
                            msgT = "Comando Enviado."
                            tLXmsg = "EN PROCESO"
                            Try
                                Console.WriteLine("Ejecutando: " & exe)
                                Shell(exe, AppWinStyle.Hide, False, -1)
                            Catch ex As Exception
                                sts = -1
                                msgT = ex.Message.Replace("'", "''")
                                tLXmsg = "ERROR"
                                Console.WriteLine("Error Ejecutando: " & exe & vbCrLf & ex.Message)
                                data = Nothing
                                data = New RFLOG
                                With data
                                    .bd = basesDatos(i)
                                    .USUARIO = System.Net.Dns.GetHostName()
                                    .OPERACION = "Error Ejecutando " & exe
                                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                                    .EVENTO = ex.ToString
                                    .TXTSQL = exe
                                    .ALERT = 1
                                End With
                                datos.GuardarRFLOGRow(data)
                            End Try
                            data = Nothing
                            data = New RFTASK
                            With data
                                .bd = basesDatos(i)
                                .TLXMSG = tLXmsg
                                .STS1 = sts
                                .TUPDDAT = Now().ToString("yyyy-MM-dd HH:mm:ss")
                                .TMSG = msgT
                                .TCMD = "" & exe & ""
                                .TNUMREG = row("TNUMREG")
                            End With
                            Console.WriteLine("Actualizando RFTASK...")
                            continuar = datos.ActualizarRFTASKRow(data, 0)
                            If continuar = False Then
                                Console.WriteLine("Error Actualizando RFTASK..." & datos.mensaje)
                                'ERROR AL ACTUALIZAR
                                datos.CerrarConexion()
                                Exit Sub
                            Else
                                Console.WriteLine("RFTASK Actualizado")
                            End If
                    End Select
                Next
            Else
                Console.WriteLine("No hay Tareas para ejecutar")
            End If
        Next
        Console.WriteLine("FIN")
        datos.CerrarConexion()
    End Sub

    'Sub EjecutaProcedimiento()
    '    'Dim conn As New SqlConnection(My.Settings.BASEDATOS_ADO)
    '    'conn.Open()

    '    'Dim adaptador As SqlDataAdapter = New SqlDataAdapter()
    '    'Dim sql As SqlCommand = New SqlCommand("select * from Eventos where Estado='Enviado'", conn)
    '    'adaptador.SelectCommand = sql

    '    'sql = New SqlCommand("SP_CarteraSubirArchivos", conn)
    '    'sql.CommandTimeout = 0
    '    'sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
    '    'sql.CommandType = CommandType.StoredProcedure
    '    'sql.ExecuteNonQuery()


    '    'Dim tareas As DataSet = New DataSet
    '    'adaptador.Fill(tareas, "Eventos")
    '    'Dim dr As DataRow
    '    'For Each dr In tareas.Tables("Eventos").Rows
    '    '    fSql = "UPDATE Eventos SET Estado=@Estado Where id=@Trabajo"
    '    '    sql = New SqlCommand(fSql, conn)
    '    '    sql.Parameters.AddWithValue("@Estado", "Iniciado")
    '    '    sql.Parameters.AddWithValue("@Trabajo", dr("ID").ToString())
    '    '    Dim r As Integer = sql.ExecuteNonQuery()
    '    '    Dim aParams() As String = Split(dr("Parametros").ToString.Trim(), ",")
    '    '    Select Case dr("Macro").ToString().Trim()
    '    '        Case "Cartera"
    '    '        Case "Datacredito"
    '    '            sql = New SqlCommand("SP_DataCreditoSubirArchivos", conn)
    '    '            sql.CommandTimeout = 0
    '    '            sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
    '    '            sql.CommandType = CommandType.StoredProcedure
    '    '            sql.ExecuteNonQuery()
    '    '        Case "DatacreditoHistorico"
    '    '            sql = New SqlCommand("SP_DataCreditoSubirArchivos2", conn)
    '    '            sql.CommandTimeout = 0
    '    '            sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
    '    '            sql.CommandType = CommandType.StoredProcedure
    '    '            sql.ExecuteNonQuery()
    '    '        Case "Logistica1"
    '    '            sql = New SqlCommand("SP_LogisticaCargueArchivo1", conn)
    '    '            sql.CommandTimeout = 0
    '    '            sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
    '    '            sql.CommandType = CommandType.StoredProcedure
    '    '            sql.ExecuteNonQuery()
    '    '        Case "Logistica2"
    '    '            sql = New SqlCommand("SP_LogisticaCargueArchivo2", conn)
    '    '            sql.CommandTimeout = 0
    '    '            sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
    '    '            sql.CommandType = CommandType.StoredProcedure
    '    '            sql.ExecuteNonQuery()
    '    '        Case "Logistica3"
    '    '            sql = New SqlCommand("SP_LogisticaCargueArchivo3", conn)
    '    '            sql.CommandTimeout = 0
    '    '            sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
    '    '            sql.CommandType = CommandType.StoredProcedure
    '    '            sql.ExecuteNonQuery()
    '    '    End Select
    '    '    sql = New SqlCommand(fSql, conn)
    '    '    sql.Parameters.AddWithValue("@Estado", "Terminado")
    '    '    sql.Parameters.AddWithValue("@Trabajo", dr("ID").ToString())
    '    '    r = sql.ExecuteNonQuery()
    '    'Next

    'End Sub

    Sub WrtSqlError(ByVal sqls As String, ByVal Descrip As String)
        data = Nothing
        data = New RFLOG
        With data
            .USUARIO = System.Net.Dns.GetHostName()
            .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
            .ALERT = "1"
            .EVENTO = Replace(Descrip, "'", "''")
            .TXTSQL = sqls
        End With
        datos.GuardarRFLOGRow(data)
        'datos.CerrarConexion()
    End Sub


End Module
