﻿Imports System.IO
Module Module1
    Dim DB As New ADODB.Connection
    Dim fSql As String
    Dim vSql As String
    Sub Main(ByVal Parametros As String())
        PedidoChile(Parametros(0))
    End Sub

    Sub PedidoChile(qArchivo As String)
        Dim i As Integer
        Dim s As String
        Dim aCabecera() As String
        Dim aDatos() As String
        Dim bPrimera As Boolean

        DB.Open(My.Settings.CONEXION)

        bPrimera = True
        s = ""
        Using sr As New StreamReader(qArchivo)
            Do While Not sr.EndOfStream
                If bPrimera Then
                    aCabecera = Split(sr.ReadLine, ";")
                Else
                    aDatos = Split(sr.ReadLine, ";")
                    fSql = " INSERT INTO PedidoCSV( "
                    vSql = " VALUES ( "
                    fSql &= " NumeroPedido     , " : vSql &= "'" & aDatos(0) & "', "
                    fSql &= " Rut              , " : vSql &= "'" & aDatos(1) & "', "
                    fSql &= " DV               , " : vSql &= "'" & aDatos(2) & "', "
                    fSql &= " DireccionEntrega , " : vSql &= "'" & aDatos(3) & "', "
                    fSql &= " FormaPago        , " : vSql &= "'" & aDatos(4) & "', "
                    For i = 5 To UBound(aDatos)
                        PedidoChile2(fSql, vSql, aCabecera(i), aDatos(i))
                    Next
                End If
                bPrimera = False
            Loop
        End Using
        DB.Close()

    End Sub

    Sub PedidoChile2(ffSql As String, vvSql As String, Producto As String, Cantidad As String)
        Dim fSql As String
        Dim vSql As String
        If Trim(Cantidad) = "" Then
            Exit Sub
        End If
        If CDbl(Cantidad) = 0 Then
            Exit Sub
        End If
        fSql = ffSql
        vSql = vvSql
        fSql &= " CodigoProducto   , " : vSql &= "'" & Producto & "', "
        fSql &= " Cantidad         ) " : vSql &= " " & Cantidad & ")  "
        DB.Execute(fSql & vSql)
    End Sub



End Module
