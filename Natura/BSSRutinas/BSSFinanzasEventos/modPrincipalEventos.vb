﻿Imports CLDatos

Module modPrincipalEventos
    Dim datos As clsDatosFinanzasEventos


    'Publicado en C:\inetpub\wwwroot\Exe\BSSFinanzasEventos\BSSFinanzasEventos.exe


    Sub Main()

        bPruebas = (My.Settings.PRUEBAS = "SI")
        bDebug = (My.Settings.DEBUG = "SI")
        If Not bPruebas Then
            gsConnectionString = My.Settings.BASEDATOS
        Else
            gsConnectionString = My.Settings.BASEDATOSLOCAL
        End If
        LXLONG = My.Settings.LXLONG
        LXLONG_CAMPO = My.Settings.LXLONG_CAMPO

        SetAmbiente(Environment.GetCommandLineArgs())

        LOG_FILE = ""

        pgmAplicacion = My.Application.Info.AssemblyName
        pgmVersion = My.Application.Info.Version.ToString

        wrt_ini(LOG_FILE)

        datos = New clsDatosFinanzasEventos()
        NotepadPath = "C:\Program Files (x86)\Notepad++\notepad++.exe"

        If Not datos.AbrirConexion() Then
            Exit Sub
        End If

        Try
            ProcesoPrincipal()
            datos.EstadoProceso("Fin proceso")
        Catch ex As Exception
            dsp(ex.ToString())
            wrt(ex.ToString())
            dsp(datos.getLastSql())
            wrt(datos.getLastSql())
            If bDebug Then
                Notepad(LOG_FILE)
            End If
        End Try

        If bDebug Then
            Notepad(LOG_FILE)
        End If
    End Sub

    Sub ProcesoPrincipal()
        Dim ev As New clsFinanzasEventos

        With ev
            .datos = datos
            datos.setPalabraClave("Finanzas Eventos")
            datos.EstadoProceso("Inicio proceso")
            If .CargaArchivos() Then
                If .Eventos_123 = 4 Then
                    .NumeroEvento = "1"
                    .ImpuestosGERA = "1,4,7"
                    .ImpuestosSAP = "1,4,7"
                    .CampoVentas = "SalesBalance"
                    .TitCampoVentas = "Ventas"
                    .CampoVentasSAP = "Ventas"
                    .FinanzasEvento()

                    .NumeroEvento = "2"
                    .ImpuestosGERA = "1"
                    .ImpuestosSAP = "1"
                    .CampoVentas = "PartyReturnsValue"
                    .TitCampoVentas = "Dev_CN"
                    .CampoVentasSAP = "DevolucionCN"
                    .FinanzasEvento()

                    .NumeroEvento = "3"
                    .ImpuestosGERA = ""
                    .ImpuestosSAP = ""
                    .TitCampoVentas = "Nota_Credito"
                    .CampoVentasSAP = "NotaCredito"
                    .CampoVentas = "CreditBillsWithTaxBalance"
                    .FinanzasEvento()

                    datos.ActualizacionesProceso("FinanzasEventosDiferencias", .Periodo)
                End If

                If .Evento_9 = 4 Then
                    .FinanzasBancos()
                    datos.setPalabraClave("Finanzas Eventos")
                    datos.EstadoProceso("Fin proceso")
                End If
            End If

        End With

    End Sub


End Module
