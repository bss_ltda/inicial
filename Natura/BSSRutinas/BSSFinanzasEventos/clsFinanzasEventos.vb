﻿Imports CLDatos
Imports System.Text

Public Class clsFinanzasEventos
    Public NumeroEvento As String
    Public ImpuestosGERA As String
    Public ImpuestosSAP As String
    Public CampoVentas As String
    Public CampoVentasSAP As String
    Public TitCampoVentas As String
    Public Periodo As String
    Public Eventos_123 As Int16
    Public Evento_9 As Int16
    Public datos As clsDatosFinanzasEventos

    Dim Evento As String

    Sub FinanzasEvento()
        Dim dt As DataTable
        Dim aImpGERA() As String
        Dim aImpSAP() As String
        Dim data As FinanzasEventos123

        aImpGERA = Split(ImpuestosGERA, ",")
        aImpSAP = Split(ImpuestosSAP, ",")
        Evento = "Evento_" & NumeroEvento

        datos.setPalabraClave(Evento)
        datos.EstadoProceso("Procesando ")

        data = Nothing
        data = New FinanzasEventos123

        datos.sqlWhere.Clear()
        datos.sqlWhere.Add("Evento", datos.tit(Evento))
        datos.sqlWhere.Add("Periodo", Me.Periodo)
        'Periodo
        datos.ExecuteSQL(data.Delete(datos.sqlWhere))

        dt = datos.FinanzasEventosGERA("FinanzasEvento" & NumeroEvento, CampoVentas, TitCampoVentas, aImpGERA, aImpSAP)

        For Each dr As DataRow In dt.Rows
            data.Inicializa()
            For i = 1 To dt.Columns.Count - 1
                With data
                    .Periodo = Me.Periodo
                    .Fecha = dr("Fecha")
                    .Evento = datos.tit(Evento)
                    .Item = datos.tit(dt.Columns(i).ColumnName)
                    If Not IsDBNull(dr(i)) Then
                        .ValorGERA = dr(i)
                    End If
                    datos.ExecuteSQL(.Insert())
                End With
            Next
        Next

        dt = datos.FinanzasEventosSAP("EVENTO" & CInt(NumeroEvento).ToString("00"), "FinanzasEventosSAP", CampoVentasSAP, TitCampoVentas, aImpSAP)
        data.Inicializa()
        For Each dr As DataRow In dt.Rows
            data.Inicializa()
            For i = 1 To dt.Columns.Count - 1
                With data
                    If Not IsDBNull(dr(i)) Then
                        .ValorSAP = dr(i)
                        datos.sqlWhere.Clear()
                        datos.sqlWhere.Add("Periodo", Me.Periodo)
                        datos.sqlWhere.Add("Fecha", dr("Fechadoc"))
                        datos.sqlWhere.Add("Evento", datos.tit(Evento))
                        datos.sqlWhere.Add("Item", datos.tit(dt.Columns(i).ColumnName))
                        datos.ExecuteSQL(.Update(datos.sqlWhere))
                    End If
                End With
            Next
        Next

        datos.EstadoProceso("Fin Proceso ")

    End Sub

    Sub FinanzasBancos()

        datos.setPalabraClave("Evento 9")
        datos.EstadoProceso("Procesando ")
        If Not datos.FinanzasBancos(Me.Periodo) Then
            datos.ActualizacionesProceso("Proceso")
        End If
    End Sub
    Function CargaArchivos() As Boolean
        Dim dt As DataTable
        Dim sqlWhere As New sqlWhere
        Dim separador As String
        Dim resultSQL As Boolean = False
        Dim desde As String = ""
        Dim Eventos As New Dictionary(Of String, Byte)

        dt = datos.dtConsultasProceso("Periodo")
        'Si CCUDC2 = 1 , se debe retornar: "está en proceso"
        If datos.getRegistros() > 0 Then
            'If dt.Rows(0).Item("EnProceso") = 1 Then
            '    datos.setError("En Proceso")
            '    datos.ActualizacionesProceso("Proceso")
            '    Return False
            'End If
            Me.Periodo = dt.Rows(0).Item("Periodo").ToString
            datos.IdProceso = dt.Rows(0).Item("IdProceso")
            dsp(Me.Periodo)
        End If
        dt.Clear()

        Eventos_123 = 0
        Evento_9 = 0
        sqlWhere.Inicializa()
        sqlWhere.Where("FAPROCNUM", Me.Periodo, "")
        dt = datos.dtConsultasProceso("Archivos", sqlWhere.getWhere())

        For Each dr As DataRow In dt.Rows
            dsp(dr("Archivo").ToString)

            If dr("ProcesarArchivo") = 1 And Not (Eventos.ContainsKey(dr("Evento"))) Then
                Eventos.Add(dr("Evento"), 0)
                If Not bPruebas Then
                    desde = "\\" & dr("Archivo").ToString.Replace("C:", datos.dtConsultaDato("CarpetaArchivos"))
                Else
                    desde = dr("Archivo").ToString
                End If

                dsp(desde)
                datos.setPalabraClave(dr("Evento"))
                datos.EstadoProceso("Cargue de Archivos. ")
                Select Case dr("Separador")
                    Case 0
                        separador = ""
                    Case 8
                        separador = "\t"
                    Case Else
                        separador = Chr(dr("Separador"))
                End Select
                datos.setPalabraClave(dr("Evento"))
                'SELECT Codigo FROM TABLAMAESTRA WHERE TABLA = 'DIRECCION'
                If datos.TruncateTables("Finanzas" & Trim(dr("Sufijo"))) Then
                    If Not datos.BulkINSERT("Finanzas" & Trim(dr("Sufijo")), desde, dr("PrimeraFila"), separador, dr("Temporal")) Then
                        datos.ActualizacionesProceso("Proceso")
                        Return False
                    Else
                        Select Case dr("Evento")
                            Case "Evento1"
                                datos.ActualizacionesProceso("FechaEvento1")
                                Eventos_123 += 1
                            Case "Evento2"
                                datos.ActualizacionesProceso("FechaEvento2")
                                Eventos_123 += 1
                            Case "Evento3"
                                datos.ActualizacionesProceso("FechaEvento3")
                                Eventos_123 += 1
                            Case "Evento9"
                                datos.ActualizacionesProceso("FechaEvento9")
                                Eventos_123 += 1
                                Evento_9 += 1
                            Case "EventosCuentas"
                                'If ConvierteFinanzasEventosBancos() Then
                                '    If datos.ActualizacionesProceso("ValoresEventosCuentas") Then
                                '        datos.ActualizacionesProceso("ValoresEventosCuentasFecha")
                                '    End If
                                'End If
                                If datos.ActualizacionesProceso("ValoresEventosCuentas") Then
                                    datos.ActualizacionesProceso("ValoresEventosCuentasFecha")
                                    Evento_9 += 1
                                End If
                            Case "EventosSAP"
                                Evento_9 += 1
                                'If ConvierteFinanzasEventosSAP() Then
                                '    If datos.ActualizacionesProceso("ValoresEventosSAP") Then
                                '        datos.ActualizacionesProceso("ValoresEventosSAPFecha")
                                '    End If
                                'End If
                                If datos.ActualizacionesProceso("ValoresEventosSAP") Then
                                    datos.ActualizacionesProceso("ValoresEventosSAPFecha")
                                    Evento_9 += 1
                                End If
                            Case Else
                        End Select
                    End If
                    If Not datos.getResultadoSQL() Then
                        datos.ActualizacionesProceso("Proceso")
                        Return False
                    End If
                Else
                    datos.ActualizacionesProceso("Proceso")
                    Return False
                End If
            End If

        Next

        Return True

    End Function

    Function ConvierteFinanzasEventosSAP() As Boolean
        Dim dt As DataTable
        Dim aDatos() As String
        Dim vSql As New StringBuilder
        Dim i, j As Integer
        Dim sep As String = ""

        datos.TruncateTables("FinanzasEventosSAP")
        dt = datos.dtConsultasProceso("FinanzasEventosCampo")
        For Each dr As DataRow In dt.Rows
            vSql.Clear()
            aDatos = Split(dr("campo"), vbTab)
            sep = ""
            For i = 0 To UBound(aDatos)
                vSql.AppendFormat("{0}'{1}'", sep, IIf(IsDBNull(aDatos(i)), "", aDatos(i)))
                sep = ", "
            Next
            For j = i To 43
                vSql.Append(", ''")
            Next
            If Not datos.InsertFinanzasEventos("FinanzasEventosSAP", vSql.ToString) Then
                Return False
            End If
        Next
        Return True
    End Function

    Function ConvierteFinanzasEventosBancos() As Boolean
        Dim dt As DataTable
        Dim aDatos() As String
        Dim vSql As New StringBuilder
        Dim i, j As Integer
        Dim sep As String = ""

        datos.TruncateTables("FinanzasEventosCuentas")
        dt = datos.dtConsultasProceso("FinanzasEventosCampo")
        For Each dr As DataRow In dt.Rows
            vSql.Clear()
            If Not IsDBNull(dr("campo")) Then
                aDatos = Split(dr("campo"), vbTab)
                sep = ""
                For i = 0 To UBound(aDatos)
                    vSql.AppendFormat("{0}'{1}'", sep, IIf(IsDBNull(aDatos(i)), "", aDatos(i)))
                    sep = ", "
                Next
                For j = i To 11
                    vSql.Append(", ''")
                Next
                If Not datos.InsertFinanzasEventos("FinanzasEventosCuentas", vSql.ToString) Then
                    Return False
                End If
            Else
                datos.setError("Datos nulos en FinanzasEventosCuentas")
                Return False
            End If
        Next

        Return True
    End Function


End Class
