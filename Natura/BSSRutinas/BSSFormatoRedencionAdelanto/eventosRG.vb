﻿Imports iTextSharp.text.pdf
Imports iTextSharp.text

Public Class eventosRG
    Inherits PdfPageEventHelper

    Public Overrides Sub OnEndPage(writer As PdfWriter, document As Document)
        Try
            Dim pdfTab As New PdfPTable(3)
            Dim Titulo As New PdfPCell(New Paragraph("Reembolso de Gastos", FontFactory.GetFont("Arial", 14, Font.BOLD)))
            Dim logonatura As Image = Image.GetInstance(RG.LOGO)
            Dim logoredenci As Image = Image.GetInstance(RG.LOGOREDENCION)
            Dim percentage As Single = 0.0F
            Dim naturaimgCell As PdfPCell = New PdfPCell(logonatura)
            Dim redencionimgCell As PdfPCell = New PdfPCell(logoredenci)

            '===== TITULO =========
            Titulo.PaddingBottom = 5.0F
            Titulo.BorderWidth = 0
            Titulo.HorizontalAlignment = Element.ALIGN_CENTER
            Titulo.VerticalAlignment = Element.ALIGN_MIDDLE

            '===== ESCALANDO IMAGEN =========
            logonatura.ScalePercent(65)
            logoredenci.ScalePercent(55)

            naturaimgCell.BorderWidth = 0
            naturaimgCell.HorizontalAlignment = Element.ALIGN_LEFT
            redencionimgCell.BorderWidth = 0
            redencionimgCell.HorizontalAlignment = Element.ALIGN_RIGHT

            pdfTab.AddCell(naturaimgCell)
            pdfTab.AddCell(Titulo)
            pdfTab.AddCell(redencionimgCell)

            pdfTab.SetWidthPercentage(New Single() {100, 340, 100}, PageSize.LETTER)
            pdfTab.WriteSelectedRows(0, -1, 34.8, document.PageSize.Height - 40, writer.DirectContent)
        Catch ex As StackOverflowException
            Console.WriteLine(ex.StackTrace & vbCrLf & ex.Message)
        End Try
    End Sub

End Class
