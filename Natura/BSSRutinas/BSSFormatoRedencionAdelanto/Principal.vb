﻿Imports CLDatos

Module Principal
    Private datos As New ClsDatosBSSFormatoRedencionAdelanto

    Sub Main()
        Dim aSubParam() As String
        Dim NumeroDocumento As String = ""
        Dim PROCESO As String = ""

        Environment.GetCommandLineArgs()
        For Each parametro In Environment.GetCommandLineArgs()
            aSubParam = Split(parametro, "=")
            Select Case UCase(aSubParam(0))
                Case "DOC"
                    NumeroDocumento = datos.Ceros(Trim(aSubParam(1)), 8)
                Case "PROCESO"
                    PROCESO = aSubParam(1)
            End Select
        Next
        Select Case PROCESO
            Case "SA"
                SA.inicio(NumeroDocumento)
            Case "FR"
                FR.inicio(NumeroDocumento)
            Case "RTC"
                RTC.inicio(NumeroDocumento)
            Case "RG"
                RG.inicio(NumeroDocumento)
        End Select
    End Sub

End Module
