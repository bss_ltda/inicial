﻿Imports CLDatos
Imports iTextSharp.text
Imports System.IO
Imports System.Text
Imports iTextSharp.text.pdf

Module RG
    Private datos As New ClsDatosBSSFormatoRedencionAdelanto
    Private barcodes As Barcode
    Private dtBSSFFNBAL As DataTable
    Private data
    Private CARPETA_SITIO As String
    Private CARPETA_IMG As String
    Private NumeroDocumento As String
    Private PDF_Folder As String
    Private Firma_Folder As String
    Private LOG_File As String
    Public LOGO As String
    Public LOGOREDENCION As String
    Private ArchivoPDF As String
    Private document As Document
    Private PageCount As Integer
    Private dtCab As DataTable
    Private dtDet As DataTable
    Private dtNotas As DataTable
    Private totalvalor As Double
    Private totalcop As Double
    Private totalcambio As Double


    Public Sub inicio(ByVal Doc As String)
        Dim campoParam As String = ""
        Dim aSubParam() As String

        Try
            Environment.GetCommandLineArgs()
            For Each parametro In Environment.GetCommandLineArgs()
                aSubParam = Split(parametro, "=")
                Select Case UCase(aSubParam(0))
                    Case "DOC"
                        NumeroDocumento = datos.Ceros(Trim(aSubParam(1)), 8)
                End Select
            Next

            If Not datos.AbrirConexion() Then
                Return
            End If

            campoParam = LXLONG_CAMPO
            CARPETA_SITIO = datos.ConsultarRFPARAMString("SITIO_CARPETA", campoParam, "")
            CARPETA_IMG = datos.ConsultarRFPARAMString("CARPETA_IMG", campoParam, "")
            LOGO = datos.ConsultarRFPARAMString("LOGO", campoParam, "")
            LOGOREDENCION = datos.ConsultarRFPARAMString("LOGORG", campoParam, "")
            PDF_Folder = CARPETA_SITIO & "xm\SRG\docs\ReembolsoGastos\" & NumeroDocumento & "\"

            Try
                If Not Directory.Exists(PDF_Folder) Then
                    Directory.CreateDirectory(PDF_Folder)
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\DirectorioError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo crear Directorio." & vbCrLf & ex.Message)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            Firma_Folder = CARPETA_SITIO & "xm\SRG\firma\"
            Try
                If Not Directory.Exists(Firma_Folder) Then
                    Directory.CreateDirectory(Firma_Folder)
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\DirectorioError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo crear Directorio." & vbCrLf & ex.Message)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            LOG_File = PDF_Folder & NumeroDocumento & ".txt"
            datos.WrtTxtError(LOG_File, NumeroDocumento)
            Dim arguments As String() = Environment.GetCommandLineArgs()
            datos.WrtTxtError(LOG_File, String.Join(", ", arguments))
            If Not datos.CheckVer() Then
                datos.WrtSqlError(datos.mensaje, "")
                End
            End If

            GenerarPDF()
            datos.CerrarConexion()
            Return
        Catch ex As Exception
            datos.WrtTxtError(LOG_File, ex.Message.ToString)
            datos.CerrarConexion()
            Return
        End Try
    End Sub

    Private Sub GenerarPDF()
        Dim Resultado As Boolean = False
        Dim parametro(0) As String

        parametro(0) = NumeroDocumento
        dtCab = datos.ConsultarRCAUdt(parametro, 0)
        If dtCab Is Nothing Then
            datos.WrtTxtError(LOG_File, "Datos no encontrados.")
            Return
        End If
        dtDet = datos.ConsultarRFSRGLdt(parametro, 0)
        If dtDet Is Nothing Then
            datos.WrtTxtError(LOG_File, "Registro de Gastos no encontrado.")
            Return
        End If
        dtNotas = datos.ConsultarRFSRGLdt(parametro, 1)
        If dtNotas Is Nothing Then
            datos.WrtTxtError(LOG_File, "Gastos por cuenta contable no encontrados.")
            Return
        End If

        ArchivoPDF = PDF_Folder & NumeroDocumento & ".pdf"
        'ArchivoPDF = System.IO.Path.GetTempPath() + Guid.NewGuid().ToString() & ".pdf"
        ArmaPDF()

        If bPRUEBAS Then
            'EJECUTA
            Dim prc As Process = New Process()
            prc.StartInfo.FileName = ArchivoPDF
            prc.Start()
        End If
    End Sub

    Private Sub ArmaPDF()

        document = New Document(New Rectangle(288.0F, 144.0F), 36, 36, 110, 45)
        document.SetPageSize(PageSize.LETTER)
        Console.WriteLine(document.PageSize.Width.ToString)
        document.AddTitle("Reembolso de Gastos - " & NumeroDocumento)
        Dim es As eventosRG = New eventosRG()
        Dim pw As PdfWriter = PdfWriter.GetInstance(document, New FileStream(ArchivoPDF, FileMode.Create))
        pw.PageEvent = es
        document.Open()

        Dim unaTabla As PdfPTable = GenerarTabla()
        document.Add(unaTabla)

        Dim unaTabla2 As PdfPTable = GenerarTabla2() 'Gastos por cuenta contable
        document.Add(unaTabla2)

        Dim unaTabla3 As PdfPTable = GenerarTabla3() 'Registro de los Gastos
        document.Add(unaTabla3)
        Dim finTabla As PdfPTable = GenerarfinTabla()
        finTabla.SplitRows = True
        finTabla.SplitLate = False
        document.Add(finTabla)
        PageCount = pw.PageNumber

        document.Close()
    End Sub

    Private Function GenerarTabla() As PdfPTable
        Dim flag As Integer = 0
        Dim bm As Drawing.Bitmap = Nothing

        Console.WriteLine("Generando una tabla...")
        Dim tablaPadre As New PdfPTable(1)
        tablaPadre.SetWidthPercentage(New Single() {615}, PageSize.LETTER)
        tablaPadre.HorizontalAlignment = Element.ALIGN_CENTER
        tablaPadre.DefaultCell.BorderWidth = 0.5

        Dim unaTabla As New PdfPTable(6)
        unaTabla.HorizontalAlignment = Element.ALIGN_CENTER
        unaTabla.DefaultCell.BorderWidth = 0
        unaTabla.SetWidthPercentage(New Single() {90, 163, 90, 90, 80, 102}, PageSize.LETTER)
        unaTabla.DefaultCell.FixedHeight = 100.0F

        Dim celda As PdfPCell

        celda = New PdfPCell(New Paragraph("Datos de Colaborador", FontFactory.GetFont("Arial", 12, Font.BOLD)))
        celda.FixedHeight = 20
        celda.BackgroundColor = New BaseColor(215, 215, 215)
        celda.Colspan = 6
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Tipo", FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(dtCab.Rows(0).Item("UTIPO"), FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Código SAP", FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(Trim(dtCab.Rows(0).Item("UUSR")), FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Proceso", FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(datos.Ceros(Trim(dtCab.Rows(0).Item("SNUMDOC")), 4), FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Nombre", FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(Trim(dtCab.Rows(0).Item("UNOM")), FontFactory.GetFont("Arial", 10)))
        celda.Colspan = 5
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Empresa", FontFactory.GetFont("Arial", 10)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(Trim(dtCab.Rows(0).Item("UCATEG1")), FontFactory.GetFont("Arial", 10)))
        celda.Colspan = 2
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(""))
        celda.Colspan = 3
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Área", FontFactory.GetFont("Arial", 10)))
        celda.VerticalAlignment = Element.ALIGN_MIDDLE
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(Trim(dtCab.Rows(0).Item("UCATEG2")), FontFactory.GetFont("Arial", 10)))
        celda.Colspan = 2
        celda.VerticalAlignment = Element.ALIGN_MIDDLE
        unaTabla.AddCell(celda)

        bm = ClsDatosBSSFormatoRedencionAdelanto.Code39(NumeroDocumento.ToString, False, 20)
        'bm.Save("C:\Temp\barras.jpg")
        Dim img As Image = Image.GetInstance(datos.ConvertToByteArray(bm))
        Dim percentage As Single = 0.0F
        Dim codbarras As PdfPCell = New PdfPCell(img)
        codbarras.Colspan = 3
        codbarras.HorizontalAlignment = Element.ALIGN_CENTER
        codbarras.VerticalAlignment = Element.ALIGN_MIDDLE
        codbarras.FixedHeight = 25
        unaTabla.AddCell(codbarras)

        Dim celdapapa As New PdfPCell(unaTabla)
        celdapapa.BorderWidth = 0
        tablaPadre.AddCell(celdapapa)

        Return tablaPadre
    End Function

    Private Function GenerarTabla2() As PdfPTable
        Dim flag As Integer = 0

        Console.WriteLine("Generando una tabla2...")
        Dim tablaPadre As New PdfPTable(1)
        tablaPadre.SetWidthPercentage(New Single() {615}, PageSize.LETTER)
        tablaPadre.HorizontalAlignment = Element.ALIGN_CENTER
        tablaPadre.DefaultCell.BorderWidth = 0.5

        Dim unaTabla As New PdfPTable(4)
        unaTabla.HorizontalAlignment = Element.ALIGN_CENTER
        unaTabla.DefaultCell.BorderWidth = 0
        unaTabla.SetWidthPercentage(New Single() {135, 220, 130, 130}, PageSize.LETTER)
        unaTabla.DefaultCell.FixedHeight = 100.0F

        Dim celda As PdfPCell

        celda = New PdfPCell(New Paragraph(""))
        celda.FixedHeight = 1
        celda.Colspan = 4
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Gastos por Cuenta Contable", FontFactory.GetFont("Arial", 10, Font.BOLD)))
        celda.FixedHeight = 18
        celda.BorderWidth = 0
        celda.BackgroundColor = New BaseColor(215, 215, 215)
        celda.Colspan = 4
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(""))
        celda.FixedHeight = 15
        celda.Colspan = 4
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        Dim header0 As New PdfPCell(New Paragraph("Centro de Costo", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header0.PaddingBottom = 5
        header0.HorizontalAlignment = 1
        header0.VerticalAlignment = Element.ALIGN_MIDDLE

        Dim header1 As New PdfPCell(New Paragraph("Cuenta", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header1.PaddingBottom = 5
        header1.HorizontalAlignment = 1
        header1.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header2 As New PdfPCell(New Paragraph("Valor Neto", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header2.PaddingBottom = 5
        header2.HorizontalAlignment = 1
        header2.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header3 As New PdfPCell(New Paragraph("COP", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header3.PaddingBottom = 5
        header3.HorizontalAlignment = 1
        header3.VerticalAlignment = Element.ALIGN_MIDDLE

        header0.BorderWidth = 0.5
        header1.BorderWidth = 0.5
        header2.BorderWidth = 0.5
        header3.BorderWidth = 0.5

        unaTabla.AddCell(header0)
        unaTabla.AddCell(header1)
        unaTabla.AddCell(header2)
        unaTabla.AddCell(header3)
        unaTabla.HeaderRows = 1

        For Each row As DataRow In dtNotas.Rows
            celda = New PdfPCell(New Paragraph(IIf(row("DCENCOS") Is DBNull.Value, "", row("DCENCOS")), FontFactory.GetFont("Arial", 8)))
            celda.BorderWidth = 0
            If flag = 1 Then
                celda.BackgroundColor = New BaseColor(215, 215, 215)
            End If
            unaTabla.AddCell(celda)

            celda = New PdfPCell(New Paragraph(IIf(row("CUENTA") Is DBNull.Value, "", row("CUENTA")), FontFactory.GetFont("Arial", 8)))
            celda.BorderWidth = 0
            If flag = 1 Then
                celda.BackgroundColor = New BaseColor(215, 215, 215)
            End If
            unaTabla.AddCell(celda)

            celda = New PdfPCell(New Paragraph(FormatNumber(row("VALOR"), 2), FontFactory.GetFont("Arial", 8)))
            celda.BorderWidth = 0
            celda.HorizontalAlignment = Element.ALIGN_RIGHT
            If flag = 1 Then
                celda.BackgroundColor = New BaseColor(215, 215, 215)
            End If
            unaTabla.AddCell(celda)

            celda = New PdfPCell(New Paragraph(FormatNumber(row("VALORLOC"), 2), FontFactory.GetFont("Arial", 8)))
            celda.BorderWidth = 0
            celda.HorizontalAlignment = Element.ALIGN_RIGHT
            If flag = 1 Then
                celda.BackgroundColor = New BaseColor(215, 215, 215)
                flag = 0
            Else
                flag += 1
            End If
            unaTabla.AddCell(celda)
        Next

        celda = New PdfPCell(New Paragraph("TOTAL", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        celda.Colspan = 2
        celda.BorderWidth = 0
        celda.HorizontalAlignment = Element.ALIGN_RIGHT
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(FormatNumber(totalvalor.ToString, 2), FontFactory.GetFont("Arial", 8)))
        celda.BorderWidth = 0
        celda.HorizontalAlignment = Element.ALIGN_RIGHT
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(FormatNumber(totalcop.ToString, 2), FontFactory.GetFont("Arial", 8)))
        celda.BorderWidth = 0
        celda.HorizontalAlignment = Element.ALIGN_RIGHT
        unaTabla.AddCell(celda)

        Dim celdapapa As New PdfPCell(unaTabla)
        celdapapa.BorderWidth = 0
        tablaPadre.AddCell(celdapapa)

        Return tablaPadre
    End Function

    Private Function GenerarTabla3() As PdfPTable
        Dim flag As Integer = 0

        Console.WriteLine("Generando una tabla3...")
        Dim tablaPadre As New PdfPTable(1)
        tablaPadre.SetWidthPercentage(New Single() {615}, PageSize.LETTER)
        tablaPadre.HorizontalAlignment = Element.ALIGN_CENTER
        tablaPadre.DefaultCell.BorderWidth = 0.5

        Dim unaTabla As New PdfPTable(7)
        unaTabla.HorizontalAlignment = Element.ALIGN_CENTER
        unaTabla.DefaultCell.BorderWidth = 0
        unaTabla.SetWidthPercentage(New Single() {60, 158, 157, 70, 50, 60, 60}, PageSize.LETTER)
        unaTabla.DefaultCell.FixedHeight = 100.0F

        Dim celda As PdfPCell

        celda = New PdfPCell(New Paragraph(""))
        celda.FixedHeight = 15
        celda.Colspan = 7
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Registro de los Gastos", FontFactory.GetFont("Arial", 10, Font.BOLD)))
        celda.FixedHeight = 18
        celda.BackgroundColor = New BaseColor(215, 215, 215)
        celda.Colspan = 7
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(""))
        celda.FixedHeight = 15
        celda.Colspan = 7
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        Dim header1 As New PdfPCell(New Paragraph("Fecha Doc", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header1.PaddingBottom = 5
        header1.HorizontalAlignment = 1
        header1.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header2 As New PdfPCell(New Paragraph("Cuenta Contable", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header2.PaddingBottom = 5
        header2.HorizontalAlignment = 1
        header2.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header3 As New PdfPCell(New Paragraph("Descripción", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header3.PaddingBottom = 5
        header3.HorizontalAlignment = 1
        header3.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header4 As New PdfPCell(New Paragraph("Vlr. Neto", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header4.PaddingBottom = 5
        header4.HorizontalAlignment = 1
        header4.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header5 As New PdfPCell(New Paragraph("Moneda", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header5.PaddingBottom = 5
        header5.HorizontalAlignment = 1
        header5.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header6 As New PdfPCell(New Paragraph("Cambio", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header6.PaddingBottom = 5
        header6.HorizontalAlignment = 1
        header6.VerticalAlignment = Element.ALIGN_MIDDLE
        Dim header7 As New PdfPCell(New Paragraph("COP", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        header7.PaddingBottom = 5
        header7.HorizontalAlignment = 1
        header7.VerticalAlignment = Element.ALIGN_MIDDLE

        header1.BorderWidth = 0.5
        header2.BorderWidth = 0.5
        header3.BorderWidth = 0.5
        header4.BorderWidth = 0.5
        header5.BorderWidth = 0.5
        header6.BorderWidth = 0.5
        header7.BorderWidth = 0.5

        unaTabla.AddCell(header1)
        unaTabla.AddCell(header2)
        unaTabla.AddCell(header3)
        unaTabla.AddCell(header4)
        unaTabla.AddCell(header5)
        unaTabla.AddCell(header6)
        unaTabla.AddCell(header7)
        unaTabla.HeaderRows = 1

        totalvalor = 0
        totalcambio = 0
        totalcop = 0

        For Each row As DataRow In dtDet.Rows
            totalvalor += CDbl(row("DVALOR"))
            totalcambio += CDbl(row("DTRMLOC"))
            totalcop += CDbl(row("DVALORLOC"))
            celda = New PdfPCell(New Paragraph(CDate(row("DFECDOC")).ToString("dd-MMM-yy"), FontFactory.GetFont("Arial", 8)))
            celda.BorderWidth = 0
            celda.HorizontalAlignment = Element.ALIGN_CENTER
            If flag = 1 Then
                celda.BackgroundColor = New BaseColor(215, 215, 215)
            End If
            unaTabla.AddCell(celda)

            celda = New PdfPCell(New Paragraph(IIf(row("CUENTA") Is DBNull.Value, "", row("CUENTA")), FontFactory.GetFont("Arial", 6)))
            celda.BorderWidth = 0
            If flag = 1 Then
                celda.BackgroundColor = New BaseColor(215, 215, 215)
            End If
            unaTabla.AddCell(celda)

            celda = New PdfPCell(New Paragraph(row("DDESCRIP"), FontFactory.GetFont("Arial", 7)))
            celda.BorderWidth = 0
            If flag = 1 Then
                celda.BackgroundColor = New BaseColor(215, 215, 215)
            End If
            unaTabla.AddCell(celda)

            celda = New PdfPCell(New Paragraph(FormatNumber(row("DVALOR"), 2), FontFactory.GetFont("Arial", 7)))
            celda.BorderWidth = 0
            celda.HorizontalAlignment = Element.ALIGN_RIGHT
            If flag = 1 Then
                celda.BackgroundColor = New BaseColor(215, 215, 215)
            End If
            unaTabla.AddCell(celda)

            celda = New PdfPCell(New Paragraph(row("MONEDA"), FontFactory.GetFont("Arial", 7)))
            celda.BorderWidth = 0
            celda.HorizontalAlignment = Element.ALIGN_CENTER
            If flag = 1 Then
                celda.BackgroundColor = New BaseColor(215, 215, 215)
            End If
            unaTabla.AddCell(celda)

            celda = New PdfPCell(New Paragraph(FormatNumber(row("DTRMLOC"), 2), FontFactory.GetFont("Arial", 7)))
            celda.BorderWidth = 0
            celda.HorizontalAlignment = Element.ALIGN_RIGHT
            If flag = 1 Then
                celda.BackgroundColor = New BaseColor(215, 215, 215)
            End If
            unaTabla.AddCell(celda)

            celda = New PdfPCell(New Paragraph(FormatNumber(row("DVALORLOC"), 2), FontFactory.GetFont("Arial", 7)))
            celda.BorderWidth = 0
            celda.HorizontalAlignment = Element.ALIGN_RIGHT
            If flag = 1 Then
                celda.BackgroundColor = New BaseColor(215, 215, 215)
                flag = 0
            Else
                flag += 1
            End If
            unaTabla.AddCell(celda)
        Next

        celda = New PdfPCell(New Paragraph("TOTAL", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        celda.BorderWidth = 0
        celda.Colspan = 3
        celda.HorizontalAlignment = Element.ALIGN_RIGHT
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(FormatNumber(totalvalor.ToString, 2), FontFactory.GetFont("Arial", 8)))
        celda.BorderWidth = 0
        celda.HorizontalAlignment = Element.ALIGN_RIGHT
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(""))
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(FormatNumber(totalcambio.ToString, 2), FontFactory.GetFont("Arial", 8)))
        celda.BorderWidth = 0
        celda.HorizontalAlignment = Element.ALIGN_RIGHT
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(FormatNumber(totalcop.ToString, 2), FontFactory.GetFont("Arial", 8)))
        celda.BorderWidth = 0
        celda.HorizontalAlignment = Element.ALIGN_RIGHT
        unaTabla.AddCell(celda)

        unaTabla.SplitRows = True
        unaTabla.SplitLate = False

        Dim celdapapa As New PdfPCell(unaTabla)
        celdapapa.BorderWidth = 0
        tablaPadre.AddCell(celdapapa)

        Return tablaPadre
    End Function

    Private Function GenerarfinTabla() As PdfPTable
        Dim flag As Integer = 0

        Console.WriteLine("Generando una tabla3...")
        Dim tablaPadre As New PdfPTable(1)
        tablaPadre.SetWidthPercentage(New Single() {615}, PageSize.LETTER)
        tablaPadre.HorizontalAlignment = Element.ALIGN_CENTER
        tablaPadre.DefaultCell.BorderWidth = 0.5

        Dim unaTabla As New PdfPTable(4)
        unaTabla.HorizontalAlignment = Element.ALIGN_CENTER
        unaTabla.DefaultCell.BorderWidth = 0
        unaTabla.SetWidthPercentage(New Single() {200, 200, 107, 107}, PageSize.LETTER)
        unaTabla.DefaultCell.FixedHeight = 100.0F

        Dim celda As PdfPCell

        celda = New PdfPCell(New Paragraph(""))
        celda.FixedHeight = 15
        celda.Colspan = 4
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Su rendición de cuentas solo será valida con la entrega de esta planilla debidamente aprobada y con los comprobantes en adjunto.", FontFactory.GetFont("Arial", 8, Font.BOLD)))
        celda.FixedHeight = 16
        celda.BackgroundColor = New BaseColor(215, 215, 215)
        celda.Colspan = 4
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(""))
        celda.FixedHeight = 10
        celda.Colspan = 4
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        Dim texto As String = "Por medio del presente, declaro de manera expresa y bajo juramento que la información aquí  mencionada y los documentos anexos "
        texto &= "(de ser aplicable) son exactos, veraces e íntegros. Asimismo, declaro conocer que las eventuales irregularidades sobre estas "
        texto &= "declaraciones están sujetas a las sanciones previstas en los reglamentos internos, en el Código de Conducta de Natura y las leyes "
        texto &= "laborales locales. En el supuesto que no sea presentado el informe de gastos y sus respectivos sustentos hasta el día 20 del mes "
        texto &= "siguiente a la fecha de mi regreso a labores o de la utilización del anticipo conforme con lo descrito en la Norma de Gastos con Viajes, "
        texto &= "autorizo de manera expresa y libre, el descuento directo sin previo aviso en la planilla del valor total del anticipo recibido en tanto será considerado este monto como un préstamo a mi persona por parte de la compañía. Caso por cuenta de las leyes laborales locales no sea posible realizar el descuento del monto total en apenas un salario, estoy de acuerdo que Natura haga el descuento en cuotas."

        celda = New PdfPCell(New Paragraph(texto, FontFactory.GetFont("Arial", 8, BaseColor.RED)))
        celda.HorizontalAlignment = Element.ALIGN_CENTER
        celda.BorderWidth = 0
        celda.Colspan = 4
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(""))
        celda.FixedHeight = 10
        celda.Colspan = 4
        celda.BorderWidth = 0
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Firmas", FontFactory.GetFont("Arial", 10, Font.BOLD)))
        celda.FixedHeight = 17
        celda.BackgroundColor = New BaseColor(215, 215, 215)
        celda.Colspan = 4
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Nº de identificación del Colaborador: ", FontFactory.GetFont("Arial", 9)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(dtCab.Rows(0).Item("SSOLICITA").ToString, FontFactory.GetFont("Arial", 9)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Fecha: ", FontFactory.GetFont("Arial", 9)))
        unaTabla.AddCell(celda)
        Dim fec As String = ""

        If dtCab.Rows(0).Item("SFSOLICITA") IsNot DBNull.Value Then
            fec = CDate(dtCab.Rows(0).Item("SFSOLICITA")).ToString("dd-MMM-yy")
        End If
        celda = New PdfPCell(New Paragraph(fec, FontFactory.GetFont("Arial", 9)))
        unaTabla.AddCell(celda)

        If dtCab.Rows(0).Item("UUPLFILE1") Is DBNull.Value Then
            celda = New PdfPCell(New Paragraph("Nombre del Colaborador: ", FontFactory.GetFont("Arial", 9)))
            unaTabla.AddCell(celda)

            celda = New PdfPCell(New Paragraph(Trim(dtCab.Rows(0).Item("UNOM").ToString), FontFactory.GetFont("Arial", 9)))
            celda.Colspan = 3
            unaTabla.AddCell(celda)
        Else
            celda = New PdfPCell(New Paragraph("Nombre del Colaborador: ", FontFactory.GetFont("Arial", 9)))
            celda.VerticalAlignment = Element.ALIGN_MIDDLE
            celda.Rowspan = 2
            unaTabla.AddCell(celda)

            Dim firmacola As Image = Image.GetInstance(Firma_Folder & Trim(dtCab.Rows(0).Item("UUPLFILE1")))
            firmacola.ScalePercent(65)
            celda = New PdfPCell(firmacola)
            celda.Colspan = 3
            celda.FixedHeight = 52
            celda.VerticalAlignment = Element.ALIGN_MIDDLE
            celda.HorizontalAlignment = Element.ALIGN_CENTER
            unaTabla.AddCell(celda)

            celda = New PdfPCell(New Paragraph(Trim(dtCab.Rows(0).Item("UNOM").ToString), FontFactory.GetFont("Arial", 9)))
            celda.HorizontalAlignment = Element.ALIGN_CENTER
            celda.Colspan = 3
            unaTabla.AddCell(celda)
        End If

        celda = New PdfPCell(New Paragraph(""))
        celda.FixedHeight = 8
        celda.BackgroundColor = New BaseColor(215, 215, 215)
        celda.Colspan = 4
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Nº de identificación del Gestor: ", FontFactory.GetFont("Arial", 9)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph(dtCab.Rows(0).Item("SAPRUEBA").ToString, FontFactory.GetFont("Arial", 9)))
        unaTabla.AddCell(celda)

        celda = New PdfPCell(New Paragraph("Fecha: ", FontFactory.GetFont("Arial", 9)))
        unaTabla.AddCell(celda)
        Dim feca As String = ""

        If dtCab.Rows(0).Item("SFAPRUEBA") IsNot DBNull.Value Then
            feca = CDate(dtCab.Rows(0).Item("SFAPRUEBA")).ToString("dd-MMM-yy")
        End If
        celda = New PdfPCell(New Paragraph(feca, FontFactory.GetFont("Arial", 9)))
        unaTabla.AddCell(celda)

        If dtCab.Rows(0).Item("UUPLFILE1GESTOR") Is DBNull.Value Then
            celda = New PdfPCell(New Paragraph("Nombre del Gestor: ", FontFactory.GetFont("Arial", 9)))
            unaTabla.AddCell(celda)

            celda = New PdfPCell(New Paragraph(Trim(dtCab.Rows(0).Item("UNOMGESTOR").ToString), FontFactory.GetFont("Arial", 9)))
            celda.Colspan = 3
            unaTabla.AddCell(celda)
        Else
            celda = New PdfPCell(New Paragraph("Nombre del Gestor: ", FontFactory.GetFont("Arial", 9)))
            celda.Rowspan = 2
            celda.VerticalAlignment = Element.ALIGN_MIDDLE
            unaTabla.AddCell(celda)

            Dim firmages As Image = Image.GetInstance(Firma_Folder & Trim(dtCab.Rows(0).Item("UUPLFILE1GESTOR")))
            firmages.ScalePercent(65)
            celda = New PdfPCell(firmages)

            celda.Colspan = 3
            celda.FixedHeight = 52
            celda.VerticalAlignment = Element.ALIGN_MIDDLE
            celda.HorizontalAlignment = Element.ALIGN_CENTER
            unaTabla.AddCell(celda)

            celda = New PdfPCell(New Paragraph(Trim(dtCab.Rows(0).Item("UNOMGESTOR").ToString), FontFactory.GetFont("Arial", 9)))
            celda.HorizontalAlignment = Element.ALIGN_CENTER
            celda.Colspan = 3
            unaTabla.AddCell(celda)
        End If

        Dim celdapapa As New PdfPCell(unaTabla)
        celdapapa.BorderWidth = 0
        tablaPadre.AddCell(celdapapa)

        Return tablaPadre
    End Function

End Module
