﻿Imports System.IO
Imports System.Text
Imports CLDatos

Module SendDocs
    Private datos As New ClsDatosBSSSendDocs
    Private data
    Sub Main()
        Dim aSubParam() As String
        Dim TRANSPORTADORA As String = ""
        Dim ARCHIVO As String = ""
        Dim ESTIBA As String = ""
        Dim IDLOG As String = ""

        Environment.GetCommandLineArgs()
        For Each parametro In Environment.GetCommandLineArgs()
            aSubParam = Split(parametro, "=")
            Select Case UCase(aSubParam(0))
                Case "TRANSPORTADORA"
                    TRANSPORTADORA = aSubParam(1)
                Case "ARCHIVO"
                    ARCHIVO = aSubParam(1)
                Case "ESTIBA"
                    ESTIBA = aSubParam(1)
                Case "IDLOG"
                    IDLOG = aSubParam(1)
            End Select
        Next
        Try
            SFTP(TRANSPORTADORA, ARCHIVO, ESTIBA, IDLOG)
        Catch ex As Exception
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\BSSSendDocsError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("Se encontro un error en BSSSendDocs.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try
    End Sub

    Sub SFTP(ByVal Transportadora As String, ByVal Archivo As String, ByVal Estiba As String, ByVal IDLOG As String)
        Dim parametro(1) As String
        Dim email As New Chilkat.Email
        Dim mailman As New Chilkat.MailMan
        Dim mht As New Chilkat.Mht
        Dim success As Boolean
        Dim dtRFPARAM As DataTable

        success = mailman.UnlockComponent("FMANRQ.CB40517_Le6wzL45oMlf")
        If success = False Then
            Console.WriteLine(mailman.LastErrorHtml)
            If IDLOG <> "" Then
                data = Nothing
                data = New RFLOG
                With data
                    .ID = IDLOG
                    .ALERT = "1"
                    .MENSAJE = mailman.LastErrorHtml
                End With
                datos.ActualizarRFLOGRow(data, 1)
            End If
            Exit Sub
        End If
        success = mht.UnlockComponent("FMANRQ.CB40517_Le6wzL45oMlf")
        If success = False Then
            Console.WriteLine(mht.LastErrorHtml)
            If IDLOG <> "" Then
                data = Nothing
                data = New RFLOG
                With data
                    .ID = IDLOG
                    .ALERT = "1"
                    .MENSAJE = mht.LastErrorHtml
                End With
                datos.ActualizarRFLOGRow(data, 1)
            End If
            Exit Sub
        End If

        If Not datos.AbrirConexion() Then
            Exit Sub
        End If

        parametro(0) = ""
        parametro(1) = "SMTPHOST"
        mailman.SmtpHost = datos.ConsultarRFPARAMst(parametro, 0)
        parametro(0) = ""
        parametro(1) = "SMTPUSERNAME"
        mailman.SmtpUsername = datos.ConsultarRFPARAMst(parametro, 0)
        parametro(0) = ""
        parametro(1) = "SMTPPASSWORD"
        mailman.SmtpPassword = datos.ConsultarRFPARAMst(parametro, 0)
        mailman.SmtpPort = 25
        mailman.StartTLS = 1
        mailman.SmtpSsl = False

        email.Subject = "Estiba No: " & Estiba & " Transportadora: " & Transportadora
        email.Body = "Estiba No: " & Estiba & " Transportadora: " & Transportadora
        email.From = "support@Natura.Net"
        email.FromAddress = "support@Natura.Net"
        email.FromName = "support@Natura.Net"

        If Transportadora = "ENVIA" Then
            email.AddTo("Asesor SAC Natura 2", "asesorsacnatura2@enviacolvanes.com.co")
            email.AddTo("Asesor SAC Natura 2", "asesorsacnatura1@enviacolvanes.com.co")
        End If

        If Transportadora = "DEPRISA" Then
            email.AddTo("Transportesnaturacol_6", "Transportesnaturacol_6@natura.net")
            email.AddTo("Transportesnaturacol_7", "Transportesnaturacol_7@natura.net")
            email.AddTo("Inhouse NATURA", "inhousenatura@gmail.com")
        End If

        If Transportadora = "SERVIENTREGAS.A" Then
            email.AddTo("Luis Padilla", "Luis.Padillau@co.servientrega.com")
            email.AddTo("Jhon Camacho", "jhon.camacho@co.servientrega.com")
        End If

        If Transportadora = "INTERRAPIDISIMOS.A" Then
            email.AddTo("Interrapidisimo", "coord.natura@interrapidisimo.com")
        End If

        parametro(0) = "SENDDOCS"
        dtRFPARAM = datos.ConsultarRFPARAMdt(parametro, 0)
        If dtRFPARAM IsNot Nothing Then
            For Each row As DataRow In dtRFPARAM.Rows
                If row("CCCODE2") = "Natura" Then
                    email.AddTo(row("CCCODE2"), row("CCDESC"))
                ElseIf row("CCCODE2") = "Alertas Correo" Then
                    email.AddBcc(row("CCCODE2"), row("CCDESC"))
                Else
                    email.AddCC(row("CCCODE2"), row("CCDESC"))
                End If
            Next
        End If

        Dim contentType As String
        parametro(0) = ""
        parametro(1) = "CARPETARAIZ"
        contentType = email.AddFileAttachment(datos.ConsultarRFPARAMst(parametro, 0) & Archivo)
        If (contentType = vbNullString) Then
            Console.WriteLine(email.LastErrorText)
            Exit Sub
        End If
        success = mailman.SendEmail(email)

        If (success <> True) Then
            Console.WriteLine(mailman.LastErrorHtml)
            If IDLOG <> "" Then
                data = Nothing
                data = New RFLOG
                With data
                    .ID = IDLOG
                    .ALERT = "1"
                    .MENSAJE = mailman.LastErrorHtml
                End With
                datos.ActualizarRFLOGRow(data, 1)
            End If
        Else
            Console.WriteLine("Enviado")
            If IDLOG <> "" Then
                data = Nothing
                data = New RFLOG
                With data
                    .ID = IDLOG
                    .MENSAJE = "OK"
                End With
                datos.ActualizarRFLOGRow(data, 0)
            End If
        End If
    End Sub

End Module
