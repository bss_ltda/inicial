﻿Imports System.IO
Imports System.Text
Imports CLDatos

Module ScriptsFTP
    Private datos As New ClsDatosBSSScriptsFTP
    Private data
    Private success As Boolean
    Private dtZCC As DataTable
    Private dtRFPARAM As DataTable
    Private dtRFPARAMNatura As DataTable
    Private dtRFTASK As DataTable
    Private aHora As String
    Private Horas()
    Private continuar As Boolean
    Private FechaActual As Date
    Private nomArchivo As String
    Private hayArchivo As Boolean
    Private fprogramar As String
    Private TareaP As String
    Private horat As String = ""
    Private ftpNatura As New Chilkat.Ftp2()
    Private localFilename As String
    Private remoteFilename As String
    Private archivo As String
    Private thisFile As IO.FileInfo
    Private archivoCorreo As String = ""

    Sub Main()
        Dim aSubParam() As String
        Dim TAREA As String = ""
        Dim BANCO As String = ""
        Dim HORASIG As String = ""
        Dim HORAACT As String = ""

        Environment.GetCommandLineArgs()
        For Each parametro In Environment.GetCommandLineArgs()
            aSubParam = Split(parametro, "=")
            Select Case UCase(aSubParam(0))
                Case "TAREA"
                    TAREA = aSubParam(1)
                Case "BANCO"
                    BANCO = aSubParam(1)
                Case "HORASIG"
                    HORASIG = aSubParam(1)
                Case "ACT"
                    HORAACT = aSubParam(1)
            End Select
        Next
        Console.WriteLine(TAREA & ", " & BANCO & ", " & HORASIG)
        SFTP(TAREA, BANCO, HORASIG, HORAACT)
    End Sub

    Public Sub SFTP(ByVal TAREA As String, ByVal BANCO As String, ByVal HORASIG As String, ByVal HORAACT As String)
        Dim parametro(0) As String
        Console.WriteLine("Abriendo Conexión...")
        If Not datos.AbrirConexion() Then

            Exit Sub
        End If

        parametro(0) = BANCO
        dtZCC = datos.ConsultarZCCdt(parametro, 4)

        If BANCO.ToUpper.Contains("DAVIVIENDA") Then
            TareaP = TAREA
            SFTPDavivienda(TAREA, BANCO, HORASIG, HORAACT)
        Else
            SFTPBancoBogota(TAREA, BANCO, HORASIG, HORAACT)
        End If
    End Sub

    Sub sftpDelete(ByVal hostname As String, ByVal port As Long, ByVal user As String, ByVal pass As String, ByVal file As String, ByVal HORASIG As String)
        Dim sftp As New Chilkat.SFtp()

        success = sftp.UnlockComponent("FMANRQ.CB40517_Le6wzL45oMlf")
        If (success <> True) Then
            Console.WriteLine(sftp.LastErrorHtml)
            rftaskReprograma(TareaP, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
            correo("Davivienda", "Transferencia Davivienda FALLA", "Fecha de Reprograma: " & fprogramar & "<Br>Davivienda<Br>" & sftp.LastErrorHtml)
            rflog("sftp.UnlockComponent", sftp.LastErrorHtml, "sftp.UnlockComponent", "1")
            Exit Sub
        End If

        sftp.ConnectTimeoutMs = 5000
        sftp.IdleTimeoutMs = 10000

        Console.WriteLine("Connect " & hostname & " - " & port)
        success = sftp.Connect(hostname, port)
        If (success <> True) Then
            Console.WriteLine(sftp.LastErrorHtml)
            rftaskReprograma(TareaP, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
            correo("Davivienda", "Transferencia Davivienda FALLA", "Fecha de Reprograma: " & fprogramar & "<Br>Davivienda<Br>" & sftp.LastErrorHtml)
            rflog("sftp.Connect", sftp.LastErrorHtml, "sftp.Connect(" & hostname & ", " & port & ")", "1")
            Exit Sub
        Else
            Console.WriteLine("Connect ")
        End If
        Console.WriteLine("AuthenticatePw " & user & " - " & pass)
        success = sftp.AuthenticatePw(user, pass)
        If (success <> True) Then
            Console.WriteLine(sftp.LastErrorHtml)
            rftaskReprograma(TareaP, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
            correo("Davivienda", "Transferencia Davivienda FALLA", "Fecha de Reprograma: " & fprogramar & "<Br>Davivienda<Br>" & sftp.LastErrorHtml)
            rflog("sftp.AuthenticatePw", sftp.LastErrorHtml, "sftp.AuthenticatePw(" & user & ", " & pass & ")", "1")
            Exit Sub
        Else
            Console.WriteLine("AuthenticatePw ")
        End If
        success = sftp.InitializeSftp()
        If (success <> True) Then
            Console.WriteLine(sftp.LastErrorHtml)
            rftaskReprograma(TareaP, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
            correo("Davivienda", "Transferencia Davivienda FALLA", "Fecha de Reprograma: " & fprogramar & "<Br>Davivienda<Br>" & sftp.LastErrorHtml)
            rflog("sftp.InitializeSftp", sftp.LastErrorHtml, "sftp.InitializeSftp()", "1")
            Exit Sub
        End If
        success = sftp.RemoveFile(file)
        If (success <> True) Then
            Console.WriteLine(sftp.LastErrorHtml)
            rftaskReprograma(TareaP, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
            correo("Davivienda", "Transferencia Davivienda FALLA", "Fecha de Reprograma: " & fprogramar & "<Br>Davivienda<Br>" & sftp.LastErrorHtml)
            rflog("sftp.RemoveFile", sftp.LastErrorHtml, "sftp.RemoveFile(" & file & ")", "1")
            Exit Sub
        End If
        sftp.Disconnect()
        Console.WriteLine("File Deleted!")
    End Sub

    Function sftpDownload(ByVal hostname As String, ByVal port As Long, ByVal user As String, ByVal pass As String, ByVal localpath As String, ByVal HORASIG As String) As Boolean
        Dim sftpBanco As New Chilkat.SFtp()
        Dim handle As String
        Dim dirPath As String
        Dim remoteFilePath As String
        Dim localFilePath As String
        Dim fileObj As Chilkat.SFtpFile
        Dim dirListing As Chilkat.SFtpDir
        Dim i As Long
        Dim n As Long

        success = sftpBanco.UnlockComponent("FMANRQ.CB40517_Le6wzL45oMlf")
        If (success <> True) Then
            Console.WriteLine(sftpBanco.LastErrorHtml)
            rftaskReprograma(TareaP, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
            correo("Davivienda", "Transferencia Davivienda FALLA", "Fecha de Reprograma: " & fprogramar & "<Br>Davivienda<Br>" & sftpBanco.LastErrorHtml)
            rflog("sftpBanco.UnlockComponent", sftpBanco.LastErrorHtml, "sftpBanco.UnlockComponent()", "1")
            Return False
        End If

        sftpBanco.ConnectTimeoutMs = 5000
        sftpBanco.IdleTimeoutMs = 100003

        Console.WriteLine("Conectando...")
        success = sftpBanco.Connect(hostname, port)
        If (success <> True) Then
            Console.WriteLine(sftpBanco.LastErrorHtml)
            rftaskReprograma(TareaP, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
            correo("Davivienda", "Transferencia Davivienda FALLA", "Fecha de Reprograma: " & fprogramar & "<Br>Davivienda<Br>" & sftpBanco.LastErrorHtml)
            rflog("sftpBanco.Connect", sftpBanco.LastErrorHtml, "sftpBanco.Connect(" & hostname & ", " & port & ")", "1")
            Return False
        Else
            Console.WriteLine("Conectado")
        End If

        Console.WriteLine("Autenticando...")
        success = sftpBanco.AuthenticatePw(user, pass)
        If (success <> True) Then
            Console.WriteLine(sftpBanco.LastErrorHtml)
            rftaskReprograma(TareaP, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
            correo("Davivienda", "Transferencia Davivienda FALLA", "Fecha de Reprograma: " & fprogramar & "<Br>Davivienda<Br>" & sftpBanco.LastErrorHtml)
            rflog("sftpBanco.AuthenticatePw", sftpBanco.LastErrorHtml, "sftpBanco.AuthenticatePw(" & user & ", " & pass & ")", "1")
            Return False
        Else
            Console.WriteLine("Autenticado")
        End If

        Console.WriteLine("Inicializando...")
        success = sftpBanco.InitializeSftp()
        If (success <> True) Then
            Console.WriteLine(sftpBanco.LastErrorHtml)
            rftaskReprograma(TareaP, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
            correo("Davivienda", "Transferencia Davivienda FALLA", "Fecha de Reprograma: " & fprogramar & "<Br>Davivienda<Br>" & sftpBanco.LastErrorHtml)
            rflog("sftpBanco.InitializeSftp", sftpBanco.LastErrorHtml, "sftpBanco.InitializeSftp()", "1")
            Return False
        Else
            Console.WriteLine("Inicializado")
        End If

        dirPath = dtRFPARAM.Rows(0).Item("CCNOT1")
        handle = sftpBanco.OpenDir(dirPath)
        If (handle = vbNullString) Then
            Console.WriteLine(sftpBanco.LastErrorHtml)
            rftaskReprograma(TareaP, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
            correo("Davivienda", "Transferencia Davivienda FALLA", "Fecha de Reprograma: " & fprogramar & "<Br>Davivienda<Br>" & sftpBanco.LastErrorHtml)
            rflog("sftpBanco.OpenDir", sftpBanco.LastErrorHtml, "sftpBanco.OpenDir(" & dirPath & ")", "1")
            Return False
        End If

        dirListing = sftpBanco.ReadDir(handle)
        If (dirListing Is Nothing) Then
            Console.WriteLine(sftpBanco.LastErrorHtml)
            rftaskReprograma(TareaP, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
            correo("Davivienda", "Transferencia Davivienda FALLA", "Fecha de Reprograma: " & fprogramar & "<Br>Davivienda<Br>" & sftpBanco.LastErrorHtml)
            rflog("sftpBanco.ReadDir", sftpBanco.LastErrorHtml, "sftpBanco.ReadDir(" & handle & ")", "1")
            Return False
        End If

        n = dirListing.NumFilesAndDirs
        If (n = 0) Then
            Console.WriteLine("No entries found in this directory." & vbCrLf)
            rftaskReprograma(TareaP, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
            correo("Davivienda", "Tarea Reprogramada (No hay Archivo(s)) Davivienda", "Fecha de Reprograma: " & fprogramar)
            'correo("Davivienda", "Transferencia Davivienda FALLA", "Fecha de Reprograma: " & fprogramar & "<Br>Davivienda<Br>" & sftpBanco.LastErrorHtml)
            rflog("Descarga Archivos Davivienda", "No hay Archivos para descargar", "sftpBanco.Download", "0")
            Return False
        Else
            For i = 0 To n - 1
                fileObj = dirListing.GetFileObject(i)
                Console.WriteLine(fileObj.Filename & vbCrLf)
                remoteFilePath = dirPath & "/"
                remoteFilePath = remoteFilePath & fileObj.Filename
                localFilePath = localpath & "\" & fileObj.Filename
                Console.WriteLine(remoteFilePath & vbCrLf & localFilePath)
                success = sftpBanco.DownloadFileByName(remoteFilePath, localFilePath)
                If (success <> True) Then
                    Console.WriteLine(sftpBanco.LastErrorHtml)
                    rftaskReprograma(TareaP, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
                    correo("Davivienda", "Transferencia Davivienda FALLA", "Fecha de Reprograma: " & fprogramar & "<Br>Davivienda<Br>" & sftpBanco.LastErrorHtml)
                    rflog("sftpBanco.DownloadFileByName", sftpBanco.LastErrorHtml, "sftpBanco.DownloadFileByName(" & remoteFilePath & ", " & localFilePath & ")", "1")
                    Return False
                End If
            Next
        End If

        success = sftpBanco.CloseHandle(handle)
        If (success <> True) Then
            Console.WriteLine(sftpBanco.LastErrorHtml)
            rftaskReprograma(TareaP, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
            correo("Davivienda", "Transferencia Davivienda FALLA", "Fecha de Reprograma: " & fprogramar & "<Br>Davivienda<Br>" & sftpBanco.LastErrorHtml)
            rflog("sftpBanco.CloseHandle", sftpBanco.LastErrorHtml, "sftpBanco.CloseHandle(" & handle & ")", "1")
            Return False
        End If

        sftpBanco.Disconnect()
        Console.WriteLine("Download Success.")
        Return True
    End Function

    Function rflog(ByVal OPERACION As String, ByVal EVENTO As String, ByVal TXTSQL As String, ByVal ALERT As String) As Boolean
        data = Nothing
        data = New RFLOG
        With data
            .USUARIO = Net.Dns.GetHostName()
            .OPERACION = OPERACION
            .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
            .EVENTO = EVENTO
            .TXTSQL = TXTSQL
            .ALERT = ALERT
        End With
        continuar = datos.GuardarRFLOGRow(data)
        Return continuar
    End Function

    Function rftaskReprograma(ByVal tarea As String, ByVal parametros As String, ByVal HORASIG As String) As Boolean
        If Horas IsNot Nothing Then
            For i = 0 To Horas.Length - 1
                If Trim(Horas(i)) = HORASIG Then
                    If i = 0 Then
                        horat = Trim(Horas(Horas.Length - 1))
                    Else
                        horat = Trim(Horas(i - 1))
                    End If
                End If
            Next
        End If
        If Hour(Now) > CInt(horat) Then
            Return True
        Else
            fprogramar = Now().AddMinutes(10).ToString("yyyy-MM-dd HH:mm:00")
            data = Nothing
            data = New RFTASK
            With data
                .STS1 = 0
                .TMSG = "REPROGRAMADA"
                .TFPROXIMA = fprogramar
                .TNUMREG = tarea
                If parametros.Contains("HORASIG=") Then
                    Dim prm As String = parametros.Substring(parametros.IndexOf("HORASIG="), 10)
                    parametros = parametros.Replace(prm, "")
                End If
                .TPRM = parametros
            End With
            continuar = datos.ActualizarRFTASKRow(data, 2)
            Return continuar
        End If
    End Function

    Function correo(ByVal banco As String, ByVal asunto As String, ByVal cuerpo As String) As Boolean
        Dim aviso
        Dim parametro(1) As String

        parametro(0) = "DESTINATARIOS"
        parametro(1) = dtZCC.Rows(0).Item("CCCODE")
        dtRFPARAM = datos.ConsultarRFPARAMdt(parametro, 0)
        If dtRFPARAM IsNot Nothing Then
            aviso = New bssCorreoMHT
            With aviso
                .inicializa()
                .Modulo = "SFTP"
                .Referencia = "FTP " & banco
                .Asunto = asunto
                For Each row As DataRow In dtRFPARAM.Rows
                    .AddDestinatario(LXLONG_CAMPO)
                Next
                .AddLine(cuerpo)
                .EnviaCorreo()
            End With
        End If
        Return continuar
    End Function

    Sub WMSCOPED(ByVal FechaActual As Date, ByVal HORASIG As String, ByVal Horas() As String, ByVal banco As String)
        Dim tarea
        Dim appPath As String = Path.GetFullPath(My.Application.Info.DirectoryPath & "\BSSScriptsFTP.exe")

        tarea = New bssTarea
        With tarea
            .inicializa()
            .Categoria = "SFTP"
            Console.WriteLine("Subcategoria: " & dtZCC.Rows(0).Item("CCCODE"))
            .Subcategoria = dtZCC.Rows(0).Item("CCCODE")
            Console.WriteLine("Ruta Programa " & appPath)
            .Comando = appPath
            Console.WriteLine("Validando Fecha proxima tarea..." & fprogramar)
            If banco = "Davivienda" Then
                fprogramar = Now.ToString("yyyy-MM-dd") & " " & HORASIG & ":15:00"
            Else
                fprogramar = Now.ToString("yyyy-MM-dd") & " " & HORASIG & ":10:00"
            End If
            If Hour(FechaActual) > HORASIG Then
                fprogramar = CDate(fprogramar).AddDays(1)
            End If
            If banco = "Davivienda" Then
                .Programar = Convert.ToDateTime(fprogramar).ToString("yyyy-MM-dd HH:15:00")
                Console.WriteLine("Fecha proxima tarea..." & Convert.ToDateTime(fprogramar).ToString("yyyy-MM-dd HH:15:00"))
            Else
                .Programar = Convert.ToDateTime(fprogramar).ToString("yyyy-MM-dd HH:10:00")
                Console.WriteLine("Fecha proxima tarea..." & Convert.ToDateTime(fprogramar).ToString("yyyy-MM-dd HH:10:00"))
            End If
            .tipo = "SFTP"
            For i = 0 To Horas.Length - 1
                If Trim(Horas(i)) = HORASIG Then
                    If i = Horas.Length - 1 Then
                        horat = Trim(Horas(0))
                    Else
                        horat = Trim(Horas(i + 1))
                    End If
                End If
            Next
            .Parametros = "BANCO=" & dtZCC.Rows(0).Item("CCCODE") & " HORASIG=" & horat & " ACT=" & HORASIG
            .EnviaTarea()
        End With
    End Sub

    Sub captura(ByVal err As String, ByVal proceso As String)
        Dim sb As New Text.StringBuilder()
        Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\" & proceso & Now.ToString("ddMMyyyyHHmmss") & ".txt")
        sb.AppendLine(proceso & vbCrLf & err)
        sb.AppendLine(Now().ToString)
        sw.WriteLine(sb.ToString())
        sw.Close()
    End Sub

    Public Sub SFTPDavivienda(ByVal TAREA As String, ByVal BANCO As String, ByVal HORASIG As String, ByVal HORAACT As String)
        Dim port As Long
        Dim hostname As String
        Dim user As String
        Dim pass As String
        Dim parametro(2) As String

        hayArchivo = False

        Console.WriteLine("Abriendo Conexión...")
        If Not datos.AbrirConexion() Then
            Exit Sub
        End If

        Console.WriteLine("Consultando Parametros...")
        parametro(0) = "SFTP"
        parametro(1) = BANCO
        parametro(2) = BANCO
        dtRFPARAM = datos.ConsultarRFPARAMdt(parametro, 1)
        parametro(0) = "SFTP"
        parametro(1) = BANCO
        parametro(2) = "Natura"
        dtRFPARAMNatura = datos.ConsultarRFPARAMdt(parametro, 1)

        hostname = dtRFPARAM.Rows(0).Item("CCALTC")
        port = dtRFPARAM.Rows(0).Item("CCCODEN")
        user = dtRFPARAM.Rows(0).Item("CCDESC")
        pass = dtRFPARAM.Rows(0).Item("CCSDSC")

        Console.WriteLine("Validando Nombre de Archivo...")
        nomArchivo = dtRFPARAM.Rows(0).Item("CCCODE3")

        Console.WriteLine("Validando Horas")

        If dtZCC IsNot Nothing Then
            If dtZCC.Rows(0).Item("CCUDC1") = 1 Then
                Horas = Split(dtZCC.Rows(0).Item("CCDESC").Replace(" ", ""), ",")
                captura("Tarea: " & TAREA, "Consultar Tarea")
                parametro(0) = TAREA
                dtRFTASK = datos.ConsultarRFTASKdt(parametro, 0)
                If dtRFTASK IsNot Nothing Then
                    captura("Hay tarea " & "Tarea: " & TAREA, "Tareas")
                    aHora = HORAACT
                    captura("Hora: " & aHora & " Tarea: " & TAREA, "Tareas")
                    Console.WriteLine("Hora de Proceso """ & aHora & "")
                    FechaActual = Now()
                    If HORASIG <> "" Then
                        WMSCOPED(FechaActual, HORASIG, Horas, "Davivienda")
                    End If

                    Console.WriteLine("Fecha Actual """ & FechaActual.ToString("yyyy-MM-dd HH:mm:ss"))
                    Console.WriteLine("Actualizando Fecha ZCC CCSDSC: " & FechaActual.ToString("yyyyMMddHH"))

                    data = Nothing
                    data = New ZCC
                    With data
                        .CCCODE = dtZCC.Rows(0).Item("CCCODE")
                        .CCSDSC = FechaActual.ToString("yyyyMMddHH")
                    End With
                    continuar = datos.ActualizarZCCRow(data, 0)

                    If continuar = True Then
                        captura("Actualizó ZCC " & "Tarea: " & TAREA, "ZCC")
                        Console.WriteLine("Fecha ZCC Actualizada")
                        continuar = False
                        Console.WriteLine("Validando Hora Actual con Hora de Proceso... " & Hour(FechaActual) & " - " & aHora)
                        captura(Hour(FechaActual) & " - " & aHora & " Tarea: " & TAREA, "Validando Hora Actual con Hora de Proceso")
                        captura(Hour(FechaActual) & "=" & aHora & " Tarea: " & TAREA, "Hora-HoraValida")
                        Console.WriteLine("Validando Fecha ZCC (CCALTC) y Fecha Actual..." & dtZCC.Rows(0).Item("CCALTC") & " <> " & FechaActual.ToString("yyyyMMddHH"))
                        If dtZCC.Rows(0).Item("CCALTC") <> FechaActual.ToString("yyyyMMddHH") Then
                            Console.WriteLine("Fechas Validas.")
                            Console.WriteLine("Copiando Archivos de CarpetaFTP1 a CarpetaFTP2... " & dtRFPARAMNatura.Rows(0).Item("CCNOT1") & "-" & dtRFPARAMNatura.Rows(0).Item("CCNOT2"))
                            'BORRANDO DATOS CARPETA INFONATURA
                            Try
                                My.Computer.FileSystem.CopyDirectory(dtRFPARAMNatura.Rows(0).Item("CCNOT1"), dtRFPARAMNatura.Rows(0).Item("CCNOT2"), True) ' CarpetaFtp , CarpetaFtp2
                            Catch ex As Exception
                                rflog("Copiando Archivos de CarpetaFTP1 a CarpetaFTP2", ex.ToString, "My.Computer.FileSystem.CopyDirectory(" & dtRFPARAMNatura.Rows(0).Item("CCNOT1") & ", " & dtRFPARAMNatura.Rows(0).Item("CCNOT2") & ", True)", "1")
                            End Try
                            Console.WriteLine("Copiado(s).")
                            Console.WriteLine("Eliminando Archivos de CarpetaFTP1...")
                            For Each fichero As String In Directory.GetFiles(dtRFPARAMNatura.Rows(0).Item("CCNOT1")) 'CarpetaFtp
                                File.Delete(fichero)
                            Next
                            Console.WriteLine("Eliminado(s).")
                            'COPIANDO DATOS DE FTP DE DAVIVIENDA A INFONATURA

                            Console.WriteLine("Descargando Archivos de Banco a CarpetaFTP1 Local...")
                            continuar = sftpDownload(hostname, port, user, pass, dtRFPARAMNatura.Rows(0).Item("CCNOT1"), HORASIG)
                            If continuar = True Then
                                ' CONEXION A FTP HOSTING RED
                                captura("Archivos descargados " & "Tarea: " & TAREA, "Download")
                                success = ftpNatura.UnlockComponent("FMANRQ.CB40517_Le6wzL45oMlf")
                                If (success <> True) Then
                                    Console.WriteLine(ftpNatura.LastErrorHtml & vbCrLf)
                                    rftaskReprograma(TAREA, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
                                    captura("Fecha de Reprograma: " & fprogramar & " Tarea: " & TAREA, "Reprogramada")
                                    correo("Davivienda", "Transferencia Davivienda FALLA", "Fecha de Reprograma: " & fprogramar & "<Br>Natura<Br>" & ftpNatura.LastErrorHtml)
                                    rflog("ftpNatura.UnlockComponent", ftpNatura.LastErrorHtml, "ftpNatura.UnlockComponent('FMANRQ.CB40517_Le6wzL45oMlf') FTPNATURA", "1")
                                    datos.CerrarConexion()
                                    Exit Sub
                                End If
                                ftpNatura.Hostname = dtRFPARAMNatura.Rows(0).Item("CCALTC")
                                ftpNatura.Username = dtRFPARAMNatura.Rows(0).Item("CCDESC")
                                ftpNatura.Password = dtRFPARAMNatura.Rows(0).Item("CCSDSC")

                                Console.WriteLine("Conectando a FTP de Natura...")
                                success = ftpNatura.Connect()
                                If (success <> True) Then
                                    Console.WriteLine(ftpNatura.LastErrorHtml)
                                    rftaskReprograma(TAREA, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
                                    captura("Fecha de Reprograma: " & fprogramar & " Tarea: " & TAREA, "Reprogramada")
                                    correo("Davivienda", "Transferencia Davivienda FALLA", "Fecha de Reprograma: " & fprogramar & "<Br>Natura<Br>" & ftpNatura.LastErrorHtml)
                                    rflog("ftpNatura.Connect", ftpNatura.LastErrorHtml, "ftpNatura.Connect()", "1")
                                    datos.CerrarConexion()
                                    Exit Sub
                                Else
                                    Console.WriteLine("Conectado.")
                                End If
                                Console.WriteLine("Validando Archivos CarpetaFTP1 Local... " & dtRFPARAMNatura.Rows(0).Item("CCNOT1") & "," & nomArchivo & Now.ToString("_yyyyMMdd_") & aHora & "*.txt")
                                hayArchivo = False
                                Try
                                    For Each fichero As String In Directory.GetFiles(dtRFPARAMNatura.Rows(0).Item("CCNOT1"), nomArchivo & Now.ToString("_yyyyMMdd_") & aHora & "*.txt")
                                        captura(fichero & " Tarea: " & TAREA, "Fichero")
                                        archivo = fichero
                                        Console.WriteLine("Valida Nombre de Archivo " & fichero & " = " & archivo)
                                        If fichero = archivo Then
                                            Console.WriteLine("Archivo valido.")
                                            Console.WriteLine("Elimina Archivo de FTP Banco")
                                            thisFile = My.Computer.FileSystem.GetFileInfo(archivo)
                                            Console.WriteLine("sftpDelete: " & hostname & " - " & CStr(port) & " - " & CStr(dtRFPARAM.Rows(0).Item("CCDESC")) & " - " & CStr(dtRFPARAM.Rows(0).Item("CCSDSC")) & " - " & "Recibidos_Davivienda/" & thisFile.Name)
                                            sftpDelete(hostname, port, user, pass, "Recibidos_Davivienda/" & thisFile.Name, HORASIG)
                                            Console.WriteLine("Archivo Eliminado.")
                                            localFilename = fichero
                                            remoteFilename = thisFile.Name
                                            If thisFile.Length <> 0 Then
                                                Console.WriteLine("Subiendo a FTP Natura" & remoteFilename)
                                                success = ftpNatura.PutFile(localFilename, remoteFilename)
                                                If (success <> True) Then
                                                    Console.WriteLine(ftpNatura.LastErrorHtml & vbCrLf)
                                                    rftaskReprograma(TAREA, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
                                                    captura("Fecha de Reprograma: " & fprogramar & " Tarea: " & TAREA, "Reprogramada")
                                                    correo("Davivienda", "Transferencia Davivienda FALLA", "Fecha de Reprograma: " & fprogramar & "<Br>Natura<Br>" & ftpNatura.LastErrorHtml)
                                                    rflog("ftpNatura.PutFile", ftpNatura.LastErrorHtml, "ftpNatura.PutFile(" & localFilename & ", " & remoteFilename & ") NATURA", "1")
                                                    datos.CerrarConexion()
                                                    Exit Sub
                                                Else
                                                    Console.WriteLine("En FTP Natura.")
                                                    If archivoCorreo = "" Then
                                                        archivoCorreo = remoteFilename
                                                    Else
                                                        archivoCorreo &= " <Br> " & remoteFilename
                                                    End If
                                                    continuar = rflog("Paso a FTP Natura", "OK", "ftpNatura.PutFile(" & localFilename & ", " & remoteFilename & ") NATURA", "0")
                                                    If continuar = True Then
                                                        hayArchivo = True
                                                    End If
                                                    Try
                                                        Console.WriteLine("Copiando archivo " & remoteFilename & " a CarpetaFTP2...")
                                                        My.Computer.FileSystem.CopyFile(fichero, dtRFPARAMNatura.Rows(0).Item("CCNOT2") & "\" & remoteFilename, True)
                                                        Console.WriteLine("Archivo Copiado")
                                                    Catch ex As Exception
                                                        Console.WriteLine("Error en copia " & ex.Message)
                                                    End Try
                                                    Console.WriteLine("Actualizando Fecha en ZCC CCALTC " & FechaActual.ToString("yyyyMMddHH"))
                                                    data = Nothing
                                                    data = New ZCC
                                                    With data
                                                        .CCCODE = dtZCC.Rows(0).Item("CCCODE")
                                                        .CCALTC = FechaActual.ToString("yyyyMMddHH")
                                                    End With
                                                    continuar = datos.ActualizarZCCRow(data, 1)
                                                    If continuar = False Then
                                                        datos.CerrarConexion()
                                                        Exit Sub
                                                    Else
                                                        Console.WriteLine("Fecha ZCC CCALTC Actualizada")
                                                    End If
                                                End If
                                            Else
                                                rflog("Archivo en cero", "Tamaño de archivo Davivienda", "Tamaño: " & thisFile.Length & ", Archivo: " & thisFile.Name, "0")
                                                captura("Archivo en Cero " & "Tarea: " & TAREA & ", Archivo: " & thisFile.Name, "Tamaño de Archivo")
                                            End If
                                        End If
                                    Next
                                Catch ex As Exception
                                    Dim sb As New StringBuilder()
                                    Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\FicheroError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                                    sb.AppendLine("Fichero con error. " & vbCrLf & ex.Message)
                                    sb.AppendLine(Now().ToString)
                                    sw.WriteLine(sb.ToString())
                                    sw.Close()
                                End Try
                                ftpNatura.Disconnect()
                                Console.WriteLine("Creando Tarea...")
                                If hayArchivo = True Then
                                    Console.WriteLine("Enviando Correo...")
                                    If continuar = False Then
                                        correo("Davivienda", "Transferencia Davivienda ERROR", "Transferencia de archivo(s): <Br>" & archivoCorreo & "<Br> ERROR <Br> Fecha de transferencia: " & FechaActual & "<Br> <hr>Fecha Proxima Transferencia: " & fprogramar & "<hr>")
                                    Else
                                        If fprogramar = "" Then
                                            parametro(0) = dtZCC.Rows(0).Item("CCCODE")
                                            parametro(1) = "0"
                                            dtRFTASK = datos.ConsultarRFTASKdt(parametro, 1)
                                            If dtRFTASK IsNot Nothing Then
                                                fprogramar = Convert.ToDateTime(dtRFTASK.Rows(0).Item("TFPROXIMA")).ToString("yyyy-MM-dd HH:mm:ss")
                                            End If
                                        End If
                                        correo("Davivienda", "Transferencia Davivienda OK", "Transferencia de archivo(s): <Br>" & archivoCorreo & "<Br> OK <Br> Fecha de transferencia: " & FechaActual & "<Br> <hr>Fecha Proxima Transferencia: " & fprogramar & "<hr>")
                                    End If
                                Else
                                    Console.WriteLine("Horas: " & Horas.Length)
                                    For i = 0 To Horas.Length - 1
                                        If Trim(Horas(i)) = HORASIG Then
                                            If i = 0 Then
                                                horat = Trim(Horas(Horas.Length - 1))
                                            Else
                                                horat = Trim(Horas(i - 1))
                                            End If
                                        End If
                                    Next
                                    Console.WriteLine("Hora: " & horat)
                                    Console.WriteLine("Valida Horas: " & Hour(Now) & " > " & CInt(horat))
                                    If Hour(Now) > CInt(horat) Then

                                    Else
                                        rftaskReprograma(TAREA, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
                                        captura("Fecha de Reprograma: " & fprogramar & " Tarea: " & TAREA, "Reprogramada")
                                    End If
                                    Console.WriteLine("Enviando Correo...")
                                    correo("Davivienda", "Tarea Reprogramada (No hay Archivo(s)) Davivienda", "Fecha de Reprograma: " & fprogramar)
                                End If
                            Else
                                captura("No hay archivos " & "Tarea: " & TAREA, "Descargar Archivos Davivienda")
                            End If
                        End If
                    Else
                        datos.CerrarConexion()
                        Exit Sub
                    End If
                End If
            Else
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\ZCCError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("CCUDC1 ZCC es diferente de 1. " & vbCrLf & dtZCC.Rows(0).Item("CCUDC1"))
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End If
        Else
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\ZCCError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("Consulta ZCC no retorno datos. " & vbCrLf & datos.fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End If
        Console.WriteLine("Fin")
        datos.CerrarConexion()
    End Sub

    Public Sub SFTPBancoBogota(ByVal TAREA As String, ByVal BANCO As String, ByVal HORASIG As String, ByVal HORAACT As String)
        Dim ftpBanco As New Chilkat.Ftp2()
        Dim parametro(2) As String

        hayArchivo = False
        Console.WriteLine("Abriendo Conexión...")
        If Not datos.AbrirConexion() Then
            Exit Sub
        End If

        Console.WriteLine("Consultando Parametros...")
        parametro(0) = "SFTP"
        parametro(1) = BANCO
        parametro(2) = BANCO
        dtRFPARAM = datos.ConsultarRFPARAMdt(parametro, 1)
        parametro(0) = "SFTP"
        parametro(1) = BANCO
        parametro(2) = "Natura"
        dtRFPARAMNatura = datos.ConsultarRFPARAMdt(parametro, 1)

        Console.WriteLine("Validando Nombre de Archivo...")
        nomArchivo = dtRFPARAM.Rows(0).Item("CCCODE3")

        Console.WriteLine("Validando Horas")

        If dtZCC IsNot Nothing Then
            If dtZCC.Rows(0).Item("CCUDC1") = 1 Then
                Horas = Split(dtZCC.Rows(0).Item("CCDESC").Replace(" ", ""), ",")
                parametro(0) = TAREA
                dtRFTASK = datos.ConsultarRFTASKdt(parametro, 0)
                If dtRFTASK IsNot Nothing Then
                    success = ftpBanco.UnlockComponent("FMANRQ.CB40517_Le6wzL45oMlf")
                    If (success <> True) Then
                        Console.WriteLine(ftpBanco.LastErrorHtml)
                        rftaskReprograma(TAREA, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
                        correo("Banco de Bogota", "Transferencia Banco de Bogota FALLA", "Fecha de Reprograma: " & fprogramar & "<Br>Banco de Bogota<Br>" & ftpBanco.LastErrorHtml)
                        rflog("ftpBanco.UnlockComponent", ftpBanco.LastErrorHtml, "ftpBanco.UnlockComponent('FMANRQ.CB40517_Le6wzL45oMlf')", "1")
                        datos.CerrarConexion()
                        Exit Sub
                    End If
                    aHora = HORAACT
                    Console.WriteLine("Hora de Proceso """ & aHora & "")
                    FechaActual = Now()
                    If HORASIG <> "" Then
                        WMSCOPED(FechaActual, HORASIG, Horas, "Bogota")
                    End If

                    Console.WriteLine("Fecha Actual """ & FechaActual.ToString("yyyy-MM-dd HH:mm:ss"))
                    Console.WriteLine("Actualizando Fecha ZCC CCSDSC: " & FechaActual.ToString("yyyyMMddHH"))
                    data = Nothing
                    data = New ZCC
                    With data
                        .CCCODE = dtZCC.Rows(0).Item("CCCODE")
                        .CCSDSC = FechaActual.ToString("yyyyMMddHH")
                    End With
                    continuar = datos.ActualizarZCCRow(data, 0)
                    If continuar = True Then
                        Console.WriteLine("Fecha ZCC Actualizada")
                        continuar = False
                        Console.WriteLine("Validando Fecha ZCC (CCALTC) y Fecha Actual..." & dtZCC.Rows(0).Item("CCALTC") & " <> " & FechaActual.ToString("yyyyMMddHH"))
                        If dtZCC.Rows(0).Item("CCALTC") <> FechaActual.ToString("yyyyMMddHH") Then
                            Console.WriteLine("Fechas Validas.")
                            Console.WriteLine("Copiando Archivos de CarpetaFTP1 a CarpetaFTP2... " & dtRFPARAMNatura.Rows(0).Item("CCNOT1") & "-" & dtRFPARAMNatura.Rows(0).Item("CCNOT2"))
                            'BORRANDO DATOS CARPETA INFONATURA
                            Try
                                My.Computer.FileSystem.CopyDirectory(dtRFPARAMNatura.Rows(0).Item("CCNOT1"), dtRFPARAMNatura.Rows(0).Item("CCNOT2"), True) ' CarpetaFtp , CarpetaFtp2
                            Catch ex As Exception
                                rflog("Copiando Archivos de CarpetaFTP1 a CarpetaFTP2", ex.ToString, "My.Computer.FileSystem.CopyDirectory(" & dtRFPARAMNatura.Rows(0).Item("CCNOT1") & ", " & dtRFPARAMNatura.Rows(0).Item("CCNOT2") & ", True)", "1")
                            End Try
                            Console.WriteLine("Copiado(s).")
                            Console.WriteLine("Eliminando Archivos de CarpetaFTP1...")
                            For Each fichero As String In Directory.GetFiles(dtRFPARAMNatura.Rows(0).Item("CCNOT1")) 'CarpetaFtp
                                File.Delete(fichero)
                            Next
                            Console.WriteLine("Eliminado(s).")
                            'COPIANDO DATOS DE FTP DE BANCO BOGOTA A INFONATURA
                            ftpBanco.Passive = False
                            ftpBanco.Hostname = dtRFPARAM.Rows(0).Item("CCALTC")
                            ftpBanco.Username = dtRFPARAM.Rows(0).Item("CCDESC")
                            ftpBanco.Password = dtRFPARAM.Rows(0).Item("CCSDSC")
                            ftpBanco.Port = dtRFPARAM.Rows(0).Item("CCCODEN")
                            ftpBanco.AuthTls = True
                            ftpBanco.Ssl = False
                            Console.WriteLine("Conectando a FTP de Banco...")

                            success = ftpBanco.Connect()

                            If (success <> True) Then
                                rftaskReprograma(TAREA, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
                                correo("Banco de Bogota", "Transferencia Banco de Bogota FALLA", "Fecha de Reprograma: " & fprogramar & "<Br>Banco de Bogota<Br>" & ftpBanco.LastErrorHtml)
                                Console.WriteLine(ftpBanco.LastErrorHtml)
                                rflog("ftpBanco.Connect", ftpBanco.LastErrorHtml, "ftpBanco.Connect()", "1")
                                datos.CerrarConexion()
                                Exit Sub
                            End If

                            ftpBanco.Passive = True

                            Console.WriteLine("Descargando Archivos de Banco a CarpetaFTP1 Local...")
                            success = ftpBanco.DownloadTree(dtRFPARAMNatura.Rows(0).Item("CCNOT1")) 'CarpetaFtp

                            If (success <> True) Then
                                Console.WriteLine(ftpBanco.LastErrorHtml)
                                rftaskReprograma(TAREA, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
                                correo("Banco de Bogota", "Transferencia Banco de Bogota FALLA", "Fecha de Reprograma: " & fprogramar & "<Br>Banco de Bogota<Br>" & ftpBanco.LastErrorHtml)
                                rflog("ftpBanco.DownloadTree", ftpBanco.LastErrorHtml, "ftpBanco.DownloadTree(" & dtRFPARAMNatura.Rows(0).Item("CCNOT1") & ")", "1")
                                datos.CerrarConexion()
                                Exit Sub
                            Else
                                Console.WriteLine("Archivos Descargados")
                            End If
                            ' CONEXION A FTP HOSTING RED
                            success = ftpNatura.UnlockComponent("FMANRQ.CB40517_Le6wzL45oMlf")
                            If (success <> True) Then
                                Console.WriteLine(ftpNatura.LastErrorHtml & vbCrLf)
                                rftaskReprograma(TAREA, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
                                correo("Banco de Bogota", "Transferencia Banco de Bogota FALLA", "Fecha de Reprograma: " & fprogramar & "<Br>Natura<Br>" & ftpNatura.LastErrorHtml)
                                rflog("ftpNatura.UnlockComponent", ftpNatura.LastErrorHtml, "ftpNatura.UnlockComponent('FMANRQ.CB40517_Le6wzL45oMlf')", "1")
                                datos.CerrarConexion()
                                Exit Sub
                            End If
                            ftpNatura.Hostname = dtRFPARAMNatura.Rows(0).Item("CCALTC")
                            ftpNatura.Username = dtRFPARAMNatura.Rows(0).Item("CCDESC")
                            ftpNatura.Password = dtRFPARAMNatura.Rows(0).Item("CCSDSC")
                            Console.WriteLine("Conectando a FTP de Natura...")
                            success = ftpNatura.Connect()
                            If (success <> True) Then
                                Console.WriteLine(ftpNatura.LastErrorHtml)
                                rftaskReprograma(TAREA, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
                                correo("Banco de Bogota", "Transferencia Banco de Bogota FALLA", "Fecha de Reprograma: " & fprogramar & "<Br>Natura<Br>" & ftpNatura.LastErrorHtml)
                                rflog("ftpNatura.Connect", ftpNatura.LastErrorHtml, "ftpNatura.Connect()", "1")
                                datos.CerrarConexion()
                                Exit Sub
                            Else
                                Console.WriteLine("Conectado.")
                            End If
                            Console.WriteLine("Validando Archivos CarpetaFTP1 Local... " & dtRFPARAMNatura.Rows(0).Item("CCNOT1") & "\" & nomArchivo & aHora)
                            hayArchivo = False
                            Try
                                For Each fichero As String In Directory.GetFiles(dtRFPARAMNatura.Rows(0).Item("CCNOT1"), nomArchivo & aHora)
                                    archivo = dtRFPARAMNatura.Rows(0).Item("CCNOT1") & "\" & nomArchivo & aHora
                                    Console.WriteLine("Valida Nombre de Archivo " & fichero & " = " & archivo)
                                    If fichero = archivo Then
                                        Console.WriteLine("Archivo valido.")
                                        Console.WriteLine("Elimina Archivo de FTP Banco")
                                        thisFile = My.Computer.FileSystem.GetFileInfo(archivo)
                                        ftpBanco.DeleteRemoteFile(nomArchivo & Trim(aHora))
                                        If (success <> True) Then
                                            Console.WriteLine(ftpBanco.LastErrorHtml)
                                            rftaskReprograma(TAREA, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
                                            correo("Banco de Bogota", "Transferencia Banco de Bogota FALLA", "Fecha de Reprograma: " & fprogramar & "<Br>Banco de Bogota<Br>" & ftpBanco.LastErrorHtml)
                                            rflog("ftpBanco.DeleteRemoteFile", ftpBanco.LastErrorHtml, "ftpBanco.DeleteRemoteFile(" & nomArchivo & Trim(aHora) & ")", "1")
                                            data = Nothing
                                            datos.CerrarConexion()
                                            Exit Sub
                                        Else
                                            Console.WriteLine("Archivo Eliminado.")
                                        End If
                                        ftpBanco.Disconnect()
                                        localFilename = fichero
                                        remoteFilename = nomArchivo.Replace("-", "_") & Trim(aHora) & Now.ToString("_ddMMyyyy") & Trim(aHora) & "00.txt"
                                        If thisFile.Length <> 0 Then
                                            Console.WriteLine("Subiendo a FTP Natura" & remoteFilename)
                                            success = ftpNatura.PutFile(localFilename, remoteFilename)
                                            If (success <> True) Then
                                                Console.WriteLine(ftpNatura.LastErrorHtml & vbCrLf)
                                                rftaskReprograma(TAREA, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
                                                correo("Banco de Bogota", "Transferencia Banco de Bogota FALLA", "Fecha de Reprograma: " & fprogramar & "<Br>Natura<Br>" & ftpNatura.LastErrorHtml)
                                                rflog("ftpNatura.PutFile", ftpNatura.LastErrorHtml, "ftpNatura.PutFile(" & localFilename & ", " & remoteFilename & ") NATURA", "1")
                                                datos.CerrarConexion()
                                                Exit Sub
                                            Else
                                                Console.WriteLine("En FTP Natura.")
                                                If archivoCorreo = "" Then
                                                    archivoCorreo = remoteFilename
                                                Else
                                                    archivoCorreo &= " <Br> " & remoteFilename
                                                End If
                                                continuar = rflog("Paso a FTP Natura", "OK", "ftpNatura.PutFile(" & localFilename & ", " & remoteFilename & ") NATURA", "0")
                                                If continuar = True Then
                                                    hayArchivo = True
                                                End If
                                                Try
                                                    Console.WriteLine("Copiando archivo " & remoteFilename & " a CarpetaFTP2...")
                                                    My.Computer.FileSystem.CopyFile(fichero, dtRFPARAMNatura.Rows(0).Item("CCNOT2") & "\" & remoteFilename, True)
                                                    Console.WriteLine("Archivo Copiado")
                                                Catch ex As Exception
                                                    Console.WriteLine("Error en copia " & ex.Message)
                                                End Try
                                                Console.WriteLine("Actualizando Fecha en ZCC CCALTC " & FechaActual.ToString("yyyyMMddHH"))
                                                data = Nothing
                                                data = New ZCC
                                                With data
                                                    .CCCODE = dtZCC.Rows(0).Item("CCCODE")
                                                    .CCALTC = FechaActual.ToString("yyyyMMddHH")
                                                End With
                                                continuar = datos.ActualizarZCCRow(data, 1)
                                                If continuar = False Then
                                                    datos.CerrarConexion()
                                                    Exit Sub
                                                Else
                                                    Console.WriteLine("Fecha ZCC CCALTC Actualizada")
                                                End If
                                            End If
                                        Else
                                            rflog("Archivo en cero", "Tamaño de archivo", "Tamaño: " & thisFile.Length & ", Archivo: " & thisFile.Name, "0")
                                            captura("Archivo en Cero", "Tamaño de Archivo")
                                        End If
                                    End If
                                Next
                            Catch ex As Exception
                                Dim sb As New StringBuilder()
                                Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\FicheroError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                                sb.AppendLine("Fichero con error. " & vbCrLf & ex.Message)
                                sb.AppendLine(Now().ToString)
                                sw.WriteLine(sb.ToString())
                                sw.Close()
                            End Try

                            ftpNatura.Disconnect()
                            Console.WriteLine("Creando Tarea...")
                            If hayArchivo = True Then

                                Console.WriteLine("Enviando Correo...")
                                If continuar = False Then
                                    correo("Banco de Bogotá", "Transferencia Banco de Bogotá ERROR", "Transferencia de archivo(s): <Br>" & archivoCorreo & "<Br> ERROR" & "<Br>" & "Fecha de transferencia: " & FechaActual & "<Br> <hr>Fecha Proxima Transferencia: " & fprogramar & "<hr>")
                                Else
                                    If fprogramar = "" Then
                                        parametro(0) = dtZCC.Rows(0).Item("CCCODE")
                                        parametro(1) = "0"
                                        dtRFTASK = datos.ConsultarRFTASKdt(parametro, 1)
                                        If dtRFTASK IsNot Nothing Then
                                            fprogramar = Convert.ToDateTime(dtRFTASK.Rows(0).Item("TFPROXIMA")).ToString("yyyy-MM-dd HH:mm:ss")
                                        End If
                                    End If
                                    correo("Banco de Bogotá", "Transferencia Banco de Bogotá OK", "Transferencia de archivo(s): <Br>" & archivoCorreo & "<Br> OK" & "<Br>" & "Fecha de transferencia: " & FechaActual & "<Br> <hr>Fecha Proxima Transferencia: " & fprogramar & "<hr>")
                                End If
                            Else
                                Console.WriteLine("Horas: " & Horas.Length)
                                For i = 0 To Horas.Length - 1
                                    If Trim(Horas(i)) = HORASIG Then
                                        If i = 0 Then
                                            horat = Trim(Horas(Horas.Length - 1))
                                        Else
                                            horat = Trim(Horas(i - 1))
                                        End If
                                    End If
                                Next
                                Console.WriteLine("Hora: " & horat)
                                Console.WriteLine("Valida Horas: " & Hour(Now) & " > " & CInt(horat))
                                If Hour(Now) > CInt(horat) Then

                                Else
                                    rftaskReprograma(TAREA, dtRFTASK.Rows(0).Item("TPRM"), HORASIG)
                                End If
                                Console.WriteLine("Enviando Correo...")
                                correo("Banco de Bogotá", "Tarea Reprogramada (No hay Archivo(s)) Banco de Bogotá", "Fecha de Reprograma: " & fprogramar)
                            End If
                        End If
                    Else
                        datos.CerrarConexion()
                        Exit Sub
                    End If
                End If
            End If
        End If
        Console.WriteLine("Fin")
        datos.CerrarConexion()
    End Sub

End Module
