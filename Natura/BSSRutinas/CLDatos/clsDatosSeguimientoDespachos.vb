﻿Imports System.Text
Imports System.Globalization
Public Class clsDatosSeguimientoDespachos
    Private DB As clsDatosBSSdbMSSql

    Private datos As New Transp_Seguimiento

    Public Class campoCsv
        Public CampoTabla As String
        Public CampoArchivo As String
        Public Formato As String
        Public Columa As Int16
        Public Valor As String
    End Class

    Private camposList As List(Of campoCsv)

    Public Sub New(DB As clsDatosBSSdbMSSql)
        Me.DB = DB
    End Sub

    Public Function cargaDatos(Origen As String) As Boolean
        Dim dt As New DataTable
        Dim bPrimera As Boolean = True
        Dim Result As Boolean


        camposList = New List(Of campoCsv)

        dt = DB.getDataTable(String.Format("SELECT CCCODE2, CCCODE3, CCSDSC FROM RFPARAM WHERE CCTABL = 'BULKINSERT_TRANSP' AND CCCODE = '{0}'", Origen))
        For Each dr As DataRow In dt.Rows
            camposList.Add(New campoCsv With {.CampoTabla = dr("CCCODE2").ToString.Trim, .CampoArchivo = dr("CCCODE3").ToString.Trim, .Formato = dr("CCSDSC").ToString.Trim, .Columa = -1, .Valor = ""})
        Next
        dt.Clear()

        'SELECT CCALTC, CCSDSC, CCUDC3, CCUDC4 FROM RFPARAM WHERE CCTABL = 'UPLOADPARAM' AND CCCODE = 'Transp_SeguimientoDespachos' AND CCCODE2 = 'Servientrega'

        Dim tablaTmp As String = "Tmp" + Origen + DB.CalcConsec("BULKINSERT", 6)
        dt = DB.ImportaCSV(tablaTmp, "C:\AppData\Natura\Despachos\NaturaTransportadoras\Oficiales\Servientrega.txt")
        For Each dr As DataRow In dt.Rows
            If bPrimera Then
                If Not camposOK(dr("campo").ToString, vbTab) Then
                    Result = False
                    Exit For
                End If
                bPrimera = False
            Else
                GuardaRegistro(dr("campo").ToString, vbTab)
            End If
            Dim campos = dr("campo")

        Next
        DB.ExecuteSQL("DROP TABLE " + tablaTmp)

    End Function

    Function camposOK(campo As String, sep As String) As Boolean
        Dim aCols() As String = Split(campo, sep)
        Dim result As Boolean = True
        Dim titCol As String

        For i = 0 To UBound(aCols)
            titCol = Trim(aCols(i))
            Dim columna = From col In camposList
                          Where col.CampoArchivo = titCol
                          Select col
            For Each col In columna
                col.Columa = i
            Next
        Next
        Dim col2 = From col In camposList
                   Where col.Columa = -1
                   Select col
        For Each col In col2
            Console.WriteLine(col.CampoArchivo)
            result = False
        Next

        Return result

    End Function

    Sub GuardaRegistro(campo As String, sep As String)
        Dim aCols() As String = Split(campo, sep)
        Dim Pedido As String = ""
        Dim sepF As String = ""
        Dim vlrCol As String = ""
        Dim bIncluir As Boolean

        Dim fSql As New StringBuilder
        Dim vSql As New StringBuilder

        Dim provider As CultureInfo = CultureInfo.InvariantCulture

        datos.Inicializa()

        Dim columna = From col In camposList
                      Select col
        For Each col In columna
            col.Valor = aCols(col.Columa)
            If col.CampoTabla = "Pedido" Then
                If IsNumeric(aCols(col.Columa)) Then
                    Pedido = aCols(col.Columa)
                End If
            End If
        Next

        If IsDBNull(Pedido) Then
            Exit Sub
        End If
        If Pedido = "" Then
            Exit Sub
        End If

        fSql.Clear()
        vSql.Clear()
        If Pedido = "203" Then
            Console.WriteLine(Pedido)
        End If


        If DB.getDato("SELECT Pedido FROM Transp_Seguimiento WHERE Pedido = " + Pedido) <> "" Then
            'ACTUALIZA
        Else
            fSql.Append("INSERT INTO Transp_Seguimiento(")
            vSql.Append(" VALUES( ")
            columna = From col In camposList
                      Select col
            For Each col In columna
                bIncluir = True
                Select Case col.Formato
                    Case "CHAR"
                        If col.Valor <> "" Then
                            vlrCol = String.Format("'{0}'", col.Valor.Replace("'", ""))
                        Else
                            bIncluir = False
                        End If
                    Case "NUM"
                        If col.Valor <> "" Then
                            If IsNumeric(col.Valor) Then
                                vlrCol = col.Valor
                            Else
                                bIncluir = False
                            End If
                        Else
                            bIncluir = False
                        End If
                    Case "FEC1"
                        If col.Valor <> "" Then
                            vlrCol = String.Format("'{0}'", convierteFecha(col.Valor, "d/MM/yyyy"))
                        Else
                            bIncluir = False
                        End If
                End Select
                If bIncluir Then
                    fSql.AppendFormat("{0}{1}", sepF, col.CampoTabla)
                    vSql.AppendFormat("{0}{1}", sepF, vlrCol)
                    sepF = ", "
                End If
            Next
            fSql.Append(")")
            vSql.Append(")")
            DB.ExecuteSQL(fSql.ToString & vSql.ToString)

        End If

    End Sub



End Class
