﻿Public Class bssCorreoMHT
    Private datos As New ClsDatosBSSScriptsFTP
    Public Modulo
    Public Referencia
    Public Clave
    Public Asunto
    Public Remitente
    Public Destinatario
    Public ConCopiaA
    Public BCC
    Public Mensaje
    Public Adjuntos
    Public Enviado
    Public Creado
    Public Actualizado
    Private data

    Function EnviaCorreo()
        Dim mensajeError As String = ""

        If Not datos.AbrirConexion() Then
            EnviaCorreo = datos.mensaje
        End If

        data = Nothing
        data = New RMAILX
        With data
            .LMOD = Modulo                                          '//25A   Modulo
            .LREF = Referencia                                      '//25A   Referencia
            .LKEY = Clave                                           '//50A   Clave
            .LASUNTO = Asunto                                       '//102A  Asunto
            .LENVIA = Remitente                                     '//152A  Remitente
            .LPARA = Destinatario                                   '//2002A Destinatario
            .LCOPIA = ConCopiaA                                     '//2002A Con Copia A
            .LBCC = BCC                                             '//156A BCC
            .LCUERPO = Replace(Mensaje, "'", "''")                  '//10002AMensaje
            .LADJUNTOS = Adjuntos                                   '//10002AAdjuntos
        End With
        EnviaCorreo = datos.GuardarRMAILXRow(data)
    End Function

    Sub AddLine(txt)
        If txt = "" Then
            Mensaje = ""
        Else
            Mensaje = Mensaje & CStr(txt) & "<br/>"
        End If
    End Sub

    Sub AddUrl(href, txt)
        Dim s
        s = "<a href=""{HREF}"">{TXT}</a>"
        s = Replace(s, "{HREF}", href)
        s = Replace(s, "{TXT}", txt)
        Mensaje = Mensaje & "<hr/>" & s & "<hr/>"
    End Sub

    Sub AddDestinatario(eMail)
        If Destinatario = "" Then
            Destinatario = eMail
        Else
            Destinatario = Destinatario & "," & eMail
        End If
    End Sub

    Sub Destinos(Lista)
        Dim parametro(1) As String
        Dim dtRFPARAM As DataTable

        parametro(0) = "FLAVISOS"
        parametro(1) = Lista
        dtRFPARAM = datos.ConsultarRFPARAMdt(parametro, 0)
        If dtRFPARAM IsNot Nothing Then
            For Each row As DataRow In dtRFPARAM.Rows
                Select Case CInt(row("CCUDC1"))
                    Case 1
                        If Destinatario = "" Then
                            Destinatario = row("CCDESC")
                        Else
                            Destinatario = Destinatario & "," & row("CCDESC")
                        End If
                    Case 2
                        If ConCopiaA = "" Then
                            ConCopiaA = row("CCDESC")
                        Else
                            ConCopiaA = ConCopiaA & "," & row("CCDESC")
                        End If
                    Case 3
                        If BCC = "" Then
                            BCC = row("CCDESC")
                        Else
                            BCC = BCC & "," & row("CCDESC")
                        End If
                End Select
            Next
        End If
    End Sub

    Function AvisarA()
        Dim parametro(2) As String
        Dim dtZCC As DataTable

        parametro(0) = "CCNOT1"
        parametro(1) = "FLAVISOS"
        parametro(2) = Modulo
        dtZCC = datos.ConsultarZCCdt(parametro, 2)
        If dtZCC IsNot Nothing Then
            AvisarA = dtZCC.Rows(0).Item("CCNOT1")
        Else
            AvisarA = ""
        End If
    End Function

    Sub Inicializa()
        Modulo = ""
        Referencia = ""
        Asunto = ""
        Remitente = ""
        Destinatario = ""
        ConCopiaA = ""
        Mensaje = ""
        Adjuntos = ""
        Enviado = ""
        Creado = ""
        Actualizado = ""
    End Sub

End Class
