﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Public Class clsDatosBSSdbMSSql
    Public mensaje As String
    Public fSql As String
    Public appPath As String
    Public sqlWhere As Dictionary(Of String, String)

    Private conexion As SqlConnection
    Private adapter As SqlDataAdapter
    Private builder As SqlCommandBuilder
    Private command As SqlCommand

    Private numFilas As Integer
    Private data
    Private PalabraClave As String
    Private bResultSQL As Boolean
    Private lastSQL As String
    Private bPruebas As Boolean

    Private dt As DataTable
    Private ds As DataSet
    Private ConnectionString As String


    Public Sub New(ConexionPruebas As String, ConexionReal As String, Pruebas As String)
        If Pruebas = "SI" Then
            Me.ConnectionString = ConexionPruebas
            bPruebas = True
        Else
            Me.ConnectionString = ConexionReal
            bPruebas = False
        End If
        sqlWhere = New Dictionary(Of String, String)
        PalabraClave = ""
        lastSQL = ""
        appPath = Path.GetFullPath(My.Application.Info.DirectoryPath & "\Log\")
        adapter = New SqlDataAdapter()

    End Sub

    Public Function AbrirConexion() As Boolean
        Dim abierta As Boolean = False
        Try

            Console.WriteLine(IIf(bPruebas, "AMBIENTE DE PRUEBAS", "AMBIENTE REAL"))

            ' Si el directorio no existe, crearlo
            If Not Directory.Exists(appPath) Then
                Directory.CreateDirectory(appPath)
            End If

            If conexion IsNot Nothing Then
                If conexion.State = ConnectionState.Open Then
                    abierta = True
                Else
                    conexion = New SqlConnection(Me.ConnectionString)
                    conexion.Open()
                    abierta = True
                End If
            Else
                conexion = New SqlConnection(Me.ConnectionString)
                Console.WriteLine(conexion.ConnectionString)
                conexion.Open()
                abierta = True
            End If
            If abierta Then
                Console.WriteLine("Conexion abierta")
            End If
        Catch ex1 As SqlException
            abierta = False
            mensaje = ex1.InnerException.ToString & vbCrLf & ex1.Message
        Catch ex As Exception
            abierta = False
            mensaje = ex.Message
        Finally
            If abierta = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo realizar la conexión.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & "Conexion: " & conexion.ConnectionString & vbCrLf & mensaje)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End If
        End Try
        Return abierta
    End Function

    Public Function CerrarConexion() As Boolean
        Dim cerrada As Boolean = False
        Try
            conexion.Close()
            cerrada = True
        Catch ex1 As SqlException
            cerrada = False
            mensaje = ex1.Message
        Catch ex As Exception
            cerrada = False
            mensaje = ex.Message
        Finally
            If cerrada = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo Cerrar la conexión.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & "Conexion: " & conexion.ConnectionString)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End If
        End Try
        Return cerrada
    End Function

    Public Function ExecuteSQL(ByVal fSql As String) As Boolean
        lastSQL = fSql
        mensaje = ""
        numFilas = 0
        command = New SqlCommand
        command.Connection = conexion
        Try
            command.CommandText = fSql
            numFilas = command.ExecuteNonQuery()
            bResultSQL = True
        Catch ex As SqlException
            bResultSQL = False
            mensaje = ex.Message
        Catch ex As Exception
            bResultSQL = False
            mensaje = ex.Message
        Finally
            If bResultSQL = False Then
                Console.WriteLine(fSql)
                Console.WriteLine(mensaje)
                If bPruebas Then
                    Console.WriteLine("")
                    Console.WriteLine("Presion ENTER")
                    Console.ReadLine()
                End If

                data = Nothing
                    data = New RFLOG
                    With data
                        .USUARIO = Net.Dns.GetHostName()
                        .OPERACION = "Actualizar RFLOG"
                        .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                        .EVENTO = mensaje
                        .TXTSQL = fSql
                        .ALERT = 1
                        .LKEY = PalabraClave
                    End With
                    GuardarRFLOGRow(data)
                End If
        End Try
        Return bResultSQL

    End Function

    ''' <summary>
    ''' Devuelve datatable de la sentencia sql
    ''' </summary>
    ''' <param name="fSql"></param>
    ''' <returns></returns>
    Public Function getDataTable(ByVal fSql As String) As DataTable

        dt = Nothing
        ds = Nothing

        numFilas = 0
        mensaje = ""
        adapter = New SqlDataAdapter(fSql, conexion)
        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New SqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        Try
            numFilas = adapter.Fill(ds)
            bResultSQL = True
        Catch ex As SqlException
            bResultSQL = False
            mensaje = ex.Message
        Catch ex As Exception
            bResultSQL = False
            mensaje = ex.Message
        Finally
            If bResultSQL = False Then
                numFilas = -1
                data = Nothing
                data = New RFLOG
                With data
                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Actualizar RFLOG"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarRFLOGRow(data)
            End If
        End Try
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing

        Return dt
    End Function

    Public Function getDato(fSql As String) As String
        Dim dt As DataTable
        dt = getDataTable(fSql)
        If Not IsNothing(dt) Then
            Return dt.Rows(0).Item(0)
        End If

        Return ""

    End Function

    Public Function mkWhere(lstWhere As List(Of lstWhere), bTieneWhere As Boolean) As String
        Dim condiWhere = From condi In lstWhere
        Dim wSql As New StringBuilder
        Dim sep As String = ""

        For Each condi In condiWhere
            wSql.AppendFormat("{0}{1} = {2}{3}{2}", sep, condi.Campo, condi.Tipo, condi.Valor, condi.Tipo)
            sep = " AND "
        Next
        If wSql.Length > 0 Then
            If bTieneWhere Then
                wSql.Insert(0, " AND ")
            Else
                wSql.Insert(0, " WHERE ")
            End If
        End If
        Return wSql.ToString

    End Function

    Public Function getRegistros() As Integer
        Return numFilas
    End Function

    Public Function getLastSql() As String
        Return Me.lastSQL
    End Function

    Public Function getPruebas() As Boolean
        Return Me.bPruebas
    End Function

    Public Sub setPruebas(Pruebas As String)
        If Pruebas = "SI" Then
            Me.bPruebas = True
        Else
            Me.bPruebas = False
        End If
    End Sub


    Public Function getError() As String
        Return Replace(Me.mensaje, "'", "''")
    End Function

    Public Function getResultadoSQL() As Boolean
        Return bResultSQL
    End Function

    Public Sub setError(txt As String)
        Me.mensaje = txt
    End Sub


    Public Sub setPalabraClave(txt As String)
        Me.PalabraClave = txt
    End Sub

    Public Function tit(txt)
        tit = Replace(txt, "_", " ")
    End Function

    Public Function GuardarRFLOGRow(ByVal datos As RFLOG) As Boolean
        Dim continuarguardandoRFLOG As Boolean
        fSql = "INSERT INTO RFLOG (USUARIO, OPERACION, PROGRAMA, EVENTO, TXTSQL, ALERT, LKEY )"
        fSql &= " VALUES ('" & datos.USUARIO & "', '" & datos.OPERACION & "', '" & datos.PROGRAMA & "', '" & datos.EVENTO.Replace("'", "''") & "', '" & datos.TXTSQL.Replace("'", "''") & "', " & datos.ALERT & ", '" & datos.LKEY & "' )"

        command = New SqlCommand
        command.Connection = conexion
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardandoRFLOG = True
        Catch ex As SqlException
            continuarguardandoRFLOG = False
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardandoRFLOG = False
            mensaje = ex.Message
        Finally
            If continuarguardandoRFLOG = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End If
        End Try
        Return continuarguardandoRFLOG
    End Function

    Public Function ImportaCSV(Tabla As String, Archivo As String) As DataTable
        Dim fSql As New StringBuilder

        ExecuteSQL(String.Format("CREATE TABLE [{0}]([campo] [nvarchar](max) NULL) ", Tabla))

        fSql.Clear()
        fSql.AppendFormat("BULK INSERT [{0}]", Tabla)
        fSql.AppendFormat("   FROM '{0}'  ", Archivo)
        fSql.Append("   WITH ( FIRSTROW = 1, MAXERRORS = 0, ROWTERMINATOR = '\n', CODEPAGE = 'ACP') ")
        ExecuteSQL(fSql.ToString)

        Return getDataTable(String.Format("SELECT * FROM [{0}]", Tabla))

    End Function
    Public Function BulkINSERT(ByVal Tabla As String, ByVal Desde As String, ByVal PrimeraFila As Integer, Separador As String) As Boolean
        Dim fSql As New StringBuilder

        fSql.AppendFormat(" BULK INSERT {0}", Tabla)
        fSql.AppendFormat(" FROM  '{0}'", Desde)
        If Separador <> "" Then
            fSql.AppendFormat(" WITH ( FIRSTROW = {0}, MAXERRORS = 0, FIELDTERMINATOR = '{1}', ROWTERMINATOR = '\n', CODEPAGE = 'ACP') ", PrimeraFila, Separador)
        Else
            fSql.AppendFormat(" WITH ( FIRSTROW = {0}, MAXERRORS = 0, ROWTERMINATOR = '\n', CODEPAGE = 'ACP') ", PrimeraFila)
        End If

        Return ExecuteSQL(fSql.ToString())

    End Function

    Public Function CalcConsec(Secuencia As String, Tope As Int16) As String
        Dim fSql As New StringBuilder
        Dim result As String

        ExecuteSQL("BEGIN TRANSACTION")
        If getDato(String.Format("SELECT CCDESC FROM RFPARAM WHERE CCTABL = 'SECUENCE' AND CCCODE='{0}'", Secuencia)) = "" Then
            ExecuteSQL(String.Format("INSERT INTO RFPARAM(CCID, CCTABL, CCCODE, CCDESC ) VALUES( 'CC', 'SECUENCE', '{0}', '0' )", Secuencia))
        End If

        ExecuteSQL(String.Format("UPDATE RFPARAM SET CCCODEN = CAST( RTRIM(CCDESC) AS INT ) WHERE CCTABL = 'SECUENCE' AND CCCODE='{0}'", Secuencia))

        fSql.Clear()
        fSql.AppendFormat("UPDATE RFPARAM SET CCCODEN = CASE WHEN CCCODEN + 1 >= 1E{0} THEN 1 ELSE CCCODEN + 1 END,", Tope)
        fSql.AppendFormat("CCDESC = RIGHT( REPLICATE( '0', {0} ) + CAST((CASE WHEN CCCODEN + 1 >= 1E{0} THEN 1 ELSE CCCODEN + 1 END) AS VARCHAR({0}) ), {0} )", Tope)
        fSql.AppendFormat("WHERE CCTABL = 'SECUENCE' AND CCCODE='{0}'", Secuencia)
        ExecuteSQL(fSql.ToString)

        result = getDato(String.Format("SELECT CCDESC FROM RFPARAM WHERE CCTABL = 'SECUENCE' AND CCCODE='{0}'", Secuencia))

        ExecuteSQL("COMMIT TRANSACTION")

        Return result

    End Function
End Class
