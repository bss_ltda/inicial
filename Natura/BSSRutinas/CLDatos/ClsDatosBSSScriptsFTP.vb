﻿Imports System.Data.SqlClient
Imports System.Text
Imports System.IO

Public Class ClsDatosBSSScriptsFTP
    Private Cadena As String
    Private conexion As SqlConnection
    Private adapter As SqlDataAdapter = New SqlDataAdapter()
    Private builder As SqlCommandBuilder
    Private command As SqlCommand
    Private WithEvents ds As DataSet
    Public fSql As String
    Private numFilas As Integer
    Private dt As DataTable
    Public appPath As String = Path.GetFullPath(My.Application.Info.DirectoryPath & "\Log\")
    Private data
    Public mensaje As String

    Sub New()

        Cadena = gsConnectionString
        Try
            conexion = New SqlConnection(Cadena)
            ' Si el directorio no existe, crearlo
            If Not Directory.Exists(appPath) Then
                Directory.CreateDirectory(appPath)
            End If
        Catch ex As Exception
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\CONEXIONError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo Establecer conexion." & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try
    End Sub

#Region "ACTUALIZAR"

    ''' <summary>
    ''' Actualiza la tabla RFTASK 
    ''' </summary>
    ''' <param name="datos">RFTASK</param>
    ''' <param name="tipo">0- SET TPRM = @TPRM WHERE TNUMREG = @TNUMREG,
    ''' 1- SET STS1 = @STS1 WHERE TNUMREG = @TNUMREG,
    ''' 2- SET STS1 = @STS1, TMSG = @TMSG, TFPROXIMA = TFPROXIMA WHERE TNUMREG = @TNUMREG,
    ''' 3- SET LANGUAGE 'English'; UPDATE RFTASK SET STS1 = @STS1, TMSG = @TMSG, TFPROXIMA = @TFPROXIMA, TPRM = @TPRM  WHERE TNUMREG = @TNUMREG</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 22/04/2016</remarks>
    Public Function ActualizarRFTASKRow(ByVal datos As RFTASK, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean
        Select Case tipo
            Case 0
                fSql = "UPDATE RFTASK SET "
                fSql &= " TPRM = '" & datos.TPRM & "'"
                fSql &= " WHERE TNUMREG = " & datos.TNUMREG
            Case 1
                fSql = "UPDATE RFTASK SET "
                fSql &= " STS1 = '" & datos.STS1 & "'"
                fSql &= " WHERE TNUMREG = " & datos.TNUMREG
            Case 2
                fSql = "SET LANGUAGE 'English'; UPDATE RFTASK SET "
                fSql &= " STS1 = '" & datos.STS1 & "', "
                fSql &= " TMSG = '" & datos.TMSG & "', "
                fSql &= " TFPROXIMA = '" & datos.TFPROXIMA & "', "
                fSql &= " TPRM = '" & datos.TPRM & "'"
                fSql &= " WHERE TNUMREG = " & datos.TNUMREG
            Case 3
                fSql = "SET LANGUAGE 'English'; UPDATE RFTASK SET "
                fSql &= " STS1 = '" & datos.STS1 & "', "
                fSql &= " TMSG = '" & datos.TMSG & "', "
                fSql &= " TFPROXIMA = '" & datos.TFPROXIMA & "', "
                fSql &= " TPRM = '" & datos.TPRM & "' "
                fSql &= " WHERE TNUMREG = " & datos.TNUMREG
        End Select

        command = New SqlCommand
        command.Connection = conexion
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As SqlException
            continuarguardando = False
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\RFTASKError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo actualizar la tabla RFTASK.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()

                data = Nothing
                data = New RFLOG
                With data
                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Actualizar RFTASK"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarRFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla ZCC 
    ''' </summary>
    ''' <param name="datos">ZCC</param>
    ''' <param name="tipo">0- Actualiza CCSDSC, 1- Actualiza CCALTC</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 20/04/2016</remarks>
    Public Function ActualizarZCCRow(ByVal datos As ZCC, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean
        Select Case tipo
            Case 0
                fSql = "UPDATE ZCC SET CCSDSC = '" & datos.CCSDSC & "' WHERE CCTABL='ALERTAS' AND CCCODE='" & datos.CCCODE & "'"
            Case 1
                fSql = "UPDATE ZCC SET CCALTC = '" & datos.CCALTC & "' WHERE CCTABL='ALERTAS' AND CCCODE='" & datos.CCCODE & "'"
        End Select

        command = New SqlCommand
        command.Connection = conexion
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As SqlException
            continuarguardando = False
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\ZCCError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo actualizar la tabla ZCC.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()

                data = Nothing
                data = New RFLOG
                With data
                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Actualizar ZCC"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarRFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

#End Region

#Region "CONSULTAR"

    ''' <summary>
    ''' Consulta la tabla RFPARAM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por CCTABL(0) y CCCODE(1),
    ''' 1- Busca por CCTABL(0) - CCCODE(1) y CCODE2(2)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 20/04/2016</remarks>
    Public Function ConsultarRFPARAMdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT * FROM RFPARAM WHERE CCTABL = '" & parametroBusqueda(0) & "' AND CCCODE = '" & parametroBusqueda(1) & "'"
            Case 1
                fSql = "SELECT * FROM RFPARAM WHERE CCTABL = '" & parametroBusqueda(0) & "' AND CCCODE = '" & parametroBusqueda(1) & "' AND CCCODE2 = '" & parametroBusqueda(2) & "'"
        End Select
        adapter = New SqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey

        builder = New SqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing

        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla RFTASK con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por TNUMREG(0), 1- TSUBCAT(0) y STS1(1)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 22/04/2016</remarks>
    Public Function ConsultarRFTASKdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT TFPROXIMA, TPRM FROM RFTASK WHERE TNUMREG='" & parametroBusqueda(0) & "'"
            Case 1
                fSql = "SELECT TFPROXIMA FROM RFTASK WHERE TSUBCAT ='" & parametroBusqueda(0) & "' AND STS1 = " & parametroBusqueda(1) & ""
        End Select
        adapter = New SqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New SqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing

        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla ZCC con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por CCTABL='ALERTAS' AND CCCODE='FTPBANCOBOGOTA', 
    ''' 1- Busca por CCTABL(0) AND CCCODE(1),
    ''' 2- Busca por CCTABL(1) - CCCODE(2) devuelve (0),
    ''' 3- Busca por CCTABL='ALERTAS' AND CCCODE='FTPDAVIVIENDA',
    ''' 4- Busca por CCTABL='ALERTAS' AND CCCODE=(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 20/04/2016</remarks>
    Public Function ConsultarZCCdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT Z.*, DAY(getdate()) AS DOW FROM ZCC Z WHERE CCTABL='ALERTAS' AND CCCODE='FTPBANCOBOGOTA'"
            Case 1
                fSql = "SELECT CCNOT1 FROM ZCC WHERE CCTABL='" & parametroBusqueda(0) & "' AND CCCODE='" & parametroBusqueda(1) & "'"
            Case 2
                fSql = "SELECT " & parametroBusqueda(0) & " FROM ZCC WHERE CCTABL='" & parametroBusqueda(1) & "' AND CCCODE='" & parametroBusqueda(2) & "'"
            Case 3
                fSql = "SELECT Z.*, DAY(getdate()) AS DOW FROM ZCC Z WHERE CCTABL='ALERTAS' AND CCCODE='FTPDAVIVIENDA'"
            Case 4
                fSql = "SELECT Z.*, DAY(getdate()) AS DOW FROM ZCC Z WHERE CCTABL='ALERTAS' AND CCCODE='" & parametroBusqueda(0) & "'"
        End Select
        adapter = New SqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New SqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing

        Return dt
    End Function

#End Region

#Region "FUNCIONES"

    Public Function AbrirConexion() As Boolean
        Dim abierta As Boolean = False
        Try
            If conexion.State = ConnectionState.Open Then
                abierta = True
            Else
                conexion = New SqlConnection()
                conexion.Open()
                abierta = True
            End If
        Catch ex1 As SqlException
            mensaje = ex1.InnerException.ToString & vbCrLf & ex1.Message
        Catch ex As Exception
            mensaje = ex.InnerException.ToString & vbCrLf & ex.Message
        Finally
            If abierta = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo realizar la conexión.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & "Conexion: " & conexion.ConnectionString & vbCrLf & mensaje)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End If
        End Try
        Return abierta
    End Function

    Public Function CerrarConexion() As Boolean
        Dim cerrada As Boolean = False
        Try
            conexion.Close()
            cerrada = True
        Catch ex1 As SqlException
            mensaje = ex1.Message
        Catch ex As Exception
            mensaje = ex.Message
        Finally
            If cerrada = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo Cerrar la conexión.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & "Conexion: " & conexion.ConnectionString)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End If
        End Try
        Return cerrada
    End Function

#End Region

#Region "GUARDAR"

    ''' <summary>
    ''' Guarda RFLOG
    ''' </summary>
    ''' <param name="datos">RFLOG</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 22/04/2016</remarks>
    Public Function GuardarRFLOGRow(ByVal datos As RFLOG) As Boolean
        Dim continuarguardandoRFLOG As Boolean = False
        fSql = "INSERT INTO RFLOG (USUARIO, OPERACION, PROGRAMA, EVENTO, TXTSQL, ALERT)"
        fSql &= " VALUES ('" & datos.USUARIO & "', '" & datos.OPERACION & "', '" & datos.PROGRAMA & "', '" & datos.EVENTO.Replace("'", "''") & "', '" & datos.TXTSQL.Replace("'", "''") & "', " & datos.ALERT & ")"

        command = New SqlCommand
        command.Connection = conexion
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardandoRFLOG = True
        Catch ex As SqlException
            mensaje = ex.Message
        Catch ex As Exception
            mensaje = ex.Message
        Finally
            If continuarguardandoRFLOG = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End If
        End Try
        Return continuarguardandoRFLOG
    End Function

    ''' <summary>
    ''' Guarda RFTASK
    ''' </summary>
    ''' <param name="datos">RFTASK</param>
    ''' <returns>Boolean</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 22/04/2016</remarks>
    Public Function GuardarRFTASKRow(ByVal datos As RFTASK) As Boolean
        Dim continuarguardando As Boolean = False
        fSql = "SET LANGUAGE 'English'; INSERT INTO RFTASK (TCATEG, TSUBCAT, TLXMSG, TTIPO, TPGM, TPRM, TFPROXIMA)"
        fSql &= " VALUES ('" & datos.TCATEG & "', '" & datos.TSUBCAT & "', '" & datos.TLXMSG & "', '" & datos.TTIPO & "', '" & datos.TPGM & "', '" & datos.TPRM & "', '" & datos.TFPROXIMA & "')"

        command = New SqlCommand
        command.Connection = conexion
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As SqlException
            mensaje = ex.Message
        Catch ex As Exception
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\RFTASKError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla RFTASK.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()

                data = Nothing
                data = New RFLOG
                With data
                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Guardar RFTASK"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarRFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Guarda RMAILX
    ''' </summary>
    ''' <param name="datos">RMAILX</param>
    ''' <returns>LID Creado</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 22/04/2016</remarks>
    Public Function GuardarRMAILXRow(ByVal datos As RMAILX) As String
        Dim lid As String = ""

        fSql = " INSERT INTO RMAILX(LMOD, LREF, LKEY, LASUNTO, LENVIA, LPARA, LCOPIA, LBCC, LCUERPO, LADJUNTOS)"
        fSql &= " VALUES ("
        fSql &= "'" & datos.LMOD & "', "                            '//25A   Modulo
        fSql &= "'" & datos.LREF & "', "                            '//25A   Referencia
        If datos.LKEY IsNot Nothing Then
            fSql &= "'" & datos.LKEY & "', "                        '//50A   Clave
        Else
            fSql &= "'', "                                          '//50A   Clave
        End If
        fSql &= "'" & datos.LASUNTO & "', "                         '//102A  Asunto
        If datos.LENVIA IsNot Nothing Then
            fSql &= "'" & datos.LENVIA & "', "                      '//152A  Remitente
        Else
            fSql &= "'', "                                          '//152A  Remitente
        End If
        fSql &= "'" & datos.LPARA & "', "                           '//2002A Destinatario
        If datos.LCOPIA IsNot Nothing Then
            fSql &= "'" & datos.LCOPIA & "', "                      '//2002A Con Copia A
        Else
            fSql &= "'', "                                          '//2002A Con Copia A
        End If
        If datos.LBCC IsNot Nothing Then
            fSql &= "'" & datos.LBCC & "', "                        '//156A BCC
        Else
            fSql &= "'', "                                          '//156A BCC
        End If
        fSql &= "'" & datos.LCUERPO & "', "                         '//10002AMensaje
        If datos.LADJUNTOS IsNot Nothing Then
            fSql &= "'" & datos.LADJUNTOS & "') "                   '//10002AAdjuntos
        Else
            fSql &= "'') "                                          '//10002AAdjuntos
        End If

        command = New SqlCommand
        command.Connection = conexion
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            adapter = New SqlDataAdapter("SELECT @@IDENTITY", conexion)
            adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
            builder = New SqlCommandBuilder(adapter)
            dt = New DataTable()
            ds = New DataSet()
            numFilas = adapter.Fill(ds)
            If numFilas > 0 Then
                dt = ds.Tables(0)
            Else
                dt = Nothing
            End If
            If dt IsNot Nothing Then
                lid = dt.Rows(0).Item(0)
            Else
                lid = "-1"
            End If
        Catch ex As SqlException
            lid = "-1"
            mensaje = ex.Message
        Catch ex As Exception
            lid = "-1"
            mensaje = ex.Message
        Finally
            If lid = "-1" Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\RMAILXError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla RMAILX.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()

                data = Nothing
                data = New RFLOG
                With data
                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Guardar RMAILX"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarRFLOGRow(data)
            End If
        End Try
        Return lid
    End Function

#End Region

End Class
