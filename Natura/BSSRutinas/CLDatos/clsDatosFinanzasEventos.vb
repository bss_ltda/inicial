﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Text

Public Class clsDatosFinanzasEventos
    Public mensaje As String
    Public fSql As String
    Public sqlWhere As Dictionary(Of String, String)
    Public IdProceso As Integer

    Private conexion As SqlConnection
    Private adapter As SqlDataAdapter
    Private builder As SqlCommandBuilder
    Private command As SqlCommand

    Private numFilas As Integer
    Private data
    Private PalabraClave As String
    Private bResultSQL As Boolean
    Private lastSQL As String

    Private dt As DataTable
    Private ds As DataSet
    Private ConnectionString As String

    Public Sub New()

        Me.ConnectionString = gsConnectionString
        sqlWhere = New Dictionary(Of String, String)
        PalabraClave = ""
        lastSQL = ""
        IdProceso = -1
        adapter = New SqlDataAdapter()

    End Sub

#Region "BORRAR"
    ''' <summary>
    ''' Truncate a la tabla enviada
    ''' </summary>
    ''' <param name="tabla">Tabla que se va a truncar</param>
    ''' <returns>Booleano</returns>
    ''' <remarks>Autor: FM Fecha: 2016-11-08 09:49:47 | </remarks>
    Public Function TruncateTables(ByVal tabla As String) As Boolean

        fSql = " TRUNCATE TABLE " & tabla
        Return ExecuteSQL(fSql)

    End Function



#End Region

#Region "CONSULTAR"

    ''' <summary>
    ''' Consulta la tabla RFPARAM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por CCCODE(1) devuelve (0)</param>
    ''' <returns>Un String</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 04/05/2016</remarks>
    Public Function ConsultarRFPARAMst(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As String
        dt = Nothing
        ds = Nothing
        dbg(conexion.ConnectionString)
        Select Case tipoparametro
            Case 0
                If parametroBusqueda(0) = "" Then
                    parametroBusqueda(0) = "CCDESC"
                End If
                fSql = "USE InfoNatura SELECT " & parametroBusqueda(0) & " AS DATO FROM RFPARAM WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER('" & parametroBusqueda(1) & "')"
                dbg(fSql)
        End Select
        adapter = New SqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey

        builder = New SqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        Return dt.Rows(0).Item(0)
    End Function
    ''' <summary>
    ''' Para consultas que devuelven un solo dato sin condiciones en el Where
    ''' </summary>
    ''' <param name="Consulta"></param>
    ''' <returns></returns>
    Public Function dtConsultaDato(Consulta As String) As String
        Dim lstWhere As New List(Of lstWhere)
        Return dtConsultaDato(Consulta, lstWhere)
    End Function
    ''' <summary>
    ''' Para consultas que devuelven un solo dato
    ''' </summary>
    ''' <param name="Consulta"></param>
    ''' <param name="lstWhere"></param>
    ''' <returns></returns>
    Public Function dtConsultaDato(Consulta As String, ByVal lstWhere As List(Of lstWhere)) As String

        Select Case Consulta
            Case "CarpetaArchivos"
                fSql = "SELECT Codigo FROM TABLAMAESTRA WHERE TABLA = 'DIRECCION'"
            Case "Periodo"
                fSql = "SELECT Codigo FROM TABLAMAESTRA WHERE TABLA = 'DIRECCION'"
        End Select

        fSql &= mkWhere(lstWhere, fSql.Contains("WHERE"))

        Return getDato(fSql)

    End Function
    ''' <summary>
    ''' Para consultar sin condiciones en el Where
    ''' </summary>
    ''' <param name="Consulta"></param>
    ''' <returns></returns>
    Public Function dtConsultasProceso(Consulta As String) As DataTable
        Dim lstWhere As New List(Of lstWhere)
        Return dtConsultasProceso(Consulta, lstWhere)
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Consulta"></param>
    ''' <param name="lstWhere"></param>
    ''' <returns></returns>
    Public Function dtConsultasProceso(Consulta As String, ByVal lstWhere As List(Of lstWhere)) As DataTable
        Dim fSql As New StringBuilder
        Dim qSql As String
        fSql.Clear()

        dsp(Consulta)

        Select Case Consulta
            Case "Periodo"
                fSql.Append("SELECT CCNUM, CCUDC1, CCUDC2 As EnProceso, CCCODEN as Periodo, CCCODEN2 as IdProceso ")
                fSql.Append("FROM RFPARAM WHERE CCTABL = 'FinanzasEventos' AND CCCODE = 'Periodo'")
            Case "Archivos"
                fSql.Append(" SELECT FANUMREG, FACATEG01, FACATEG02 as Evento, CCCODE3 as Sufijo, FAPROCNUM, ")
                fSql.Append(" FADESCRIP As Archivo, FATITURL, ")
                fSql.Append(" CCUDC3 As PrimeraFila, CCUDC4 as Separador, CCUDC5 as Temporal, CCUDC9 as ProcesarArchivo, CCALTC as FormatoFecha  ")
                fSql.Append(" FROM RFADJUNTO INNER JOIN RFPARAM On ")
                fSql.Append(" RFADJUNTO.FACATEG01 = RFPARAM.CCCODE And RFADJUNTO.FACATEG02 = RFPARAM.CCCODE2 ")
                'En CCCODEN esta el periodo
                fSql.Append(" And RFADJUNTO.FAPROCNUM = RFPARAM.CCCODEN")
                fSql.Append(" WHERE ( CCTABL = 'UPLOADPARAM' AND CCCODE = 'FinanzasEventos' ) ")
                fSql.Append(" ORDER BY FANUMREG DESC")
            Case "CarpetaArchivos"
                fSql.Append("SELECT Codigo FROM TABLAMAESTRA WHERE TABLA = 'DIRECCION'")
            Case "FinanzasEventosCampo"
                fSql.Append("SELECT campo FROM FinanzasEventosCampo")
        End Select

        qSql = mkWhere(lstWhere, fSql.ToString.Contains("WHERE"))
        fSql.Replace("{0}", qSql)
        Return getDataTable(fSql.ToString)

    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Tabla"></param>
    ''' <param name="CampoVentas"></param>
    ''' <param name="TitCampoVentas"></param>
    ''' <param name="aImpGERA"></param>
    ''' <param name="aImpSAP"></param>
    ''' <returns></returns>
    Public Function FinanzasEventosGERA(ByVal Tabla As String, ByVal CampoVentas As String, ByVal TitCampoVentas As String, aImpGERA() As String, aImpSAP() As String) As DataTable
        Dim fSql As New StringBuilder
        Dim sep As String = vbLf

        fSql.Clear()

        fSql.AppendFormat("Select [Fecha], SUM( {0} ) As [{1}] ", ConvertFloat(CampoVentas), TitCampoVentas)
        For i = 0 To UBound(aImpGERA)
            If (aImpGERA(i) <> "") Then
                fSql.AppendFormat("{0}, SUM( {1}) As Impuesto_{2} ", vbLf, ConvertFloat("Tax" & aImpGERA(i)), aImpSAP(i))
            End If
        Next
        fSql.AppendFormat("{0} FROM {1}", vbLf, Tabla)
        fSql.AppendFormat("{0} GROUP BY Fecha", vbLf)

        Return getDataTable(fSql.ToString())

    End Function

    Public Function FinanzasBancos(ByVal Periodo As String) As Boolean
        Dim fSql As New StringBuilder

        'Entran: FinanzasEvento9, FinanzasEventosSAP, FinanzasEventosBancos
        fSql.Clear()
        fSql.AppendFormat("DELETE FROM FinanzasEventosBancos WHERE PERIODO = '{0}'", Periodo)
        If Not ExecuteSQL(fSql.ToString) Then Return False

        fSql.Clear()
        fSql.Append(" INSERT INTO FinanzasEventosBancos(PERIODO, DOCUMENT_DATE, RECEIVER, TIPO_BAJA, RECEIVER_BALANCE, PENALTY_INTEREST_BALANCE, TAX1, FACTOR, ORDEN )")
        fSql.Append(" SELECT")
        fSql.AppendFormat("  '{0}',", Periodo)
        fSql.Append("  RTRIM(DOCUMENT_DATE),")
        fSql.Append("  RECEIVER,")
        fSql.Append("  TIPO_BAJA,")
        fSql.AppendFormat("  SUM( {0} ) * CCUDC1 AS RECEIVER_BALANCE      ,", ConvertFloat("RECEIVER_BALANCE"))
        fSql.AppendFormat("  SUM({0})  * CCUDC1 AS PENALTY_INTEREST_BALANCE,", ConvertFloat("PENALTY_INTEREST_BALANCE"))
        fSql.AppendFormat("  SUM({0})  * CCUDC1 AS TAX1  ,", ConvertFloat("TAX1"))
        fSql.Append("  CCUDC1, CCCODEN")
        fSql.Append(" FROM")
        fSql.Append("  FinanzasEvento9 Inner Join")
        fSql.Append("  RFPARAM")
        fSql.Append("    On FinanzasEvento9.TIPO_BAJA = RFPARAM.CCCODE2")
        fSql.Append(" WHERE")
        fSql.Append("  CCTABL = 'FinanzasEventosRubros'")
        fSql.Append(" GROUP BY ")
        fSql.Append(" DOCUMENT_DATE, RECEIVER, TIPO_BAJA, CCUDC1, CCCODEN")
        If Not ExecuteSQL(fSql.ToString) Then Return False

        fSql.Clear()
        fSql.Append(" INSERT INTO FinanzasEventosBancos(PERIODO, DOCUMENT_DATE, RECEIVER, REFERENCIA, SALDO_REC, MULTA_INT, IMPUESTO1, ORDEN )")
        fSql.Append(" SELECT")
        fSql.AppendFormat("  '{0}',", Periodo)
        fSql.Append("  Fechadoc,")
        fSql.Append("  Recibidor,")
        fSql.Append("  MensNumRef,")
        fSql.AppendFormat("  {0} AS SaldoRec, ", ConvertFloat("SaldoRec2"))
        fSql.AppendFormat("  {0} AS MultaInt, ", ConvertFloat("MultaInt"))
        fSql.AppendFormat("  {0} AS Impuesto1, ", ConvertFloat("Impuesto1"))
        fSql.Append("  10")
        fSql.Append(" FROM")
        fSql.Append("  FinanzasEventosSAP ")
        fSql.Append(" WHERE ")
        fSql.Append("  Evento = 'EVENTO09' ")
        If Not ExecuteSQL(fSql.ToString) Then Return False

        fSql.Clear()
        fSql.Append(" INSERT INTO FinanzasEventosBancos(PERIODO, DOCUMENT_DATE, RECEIVER, REFERENCIA, IMPORTE_MD, ORDEN )  ")
        fSql.Append(" Select")
        fSql.AppendFormat("  '{0}',", Periodo)
        fSql.Append("  S.Fechadoc,")
        fSql.Append("  S.Recibidor,")
        fSql.Append("  C.Referencia,")
        fSql.Append("  C.ImporteMD, ")
        fSql.Append("  20")
        fSql.Append(" From")
        fSql.Append("  FinanzasEventosSAP S Inner Join")
        fSql.Append("  FinanzasEventosCuentas C")
        fSql.Append("    On S.MensNumRef = C.Referencia")
        fSql.Append(" Where")
        fSql.Append("  S.Evento = 'EVENTO09' ")
        If Not ExecuteSQL(fSql.ToString) Then Return False

        fSql.Clear()
        fSql.AppendFormat("DELETE FROM FinanzasEventosBancosResu WHERE PERIODO = '{0}'", Periodo)
        If Not ExecuteSQL(fSql.ToString) Then Return False

        fSql.Clear()
        fSql.Append(" INSERT INTO FinanzasEventosBancosResu(PERIODO, DOCUMENT_DATE, RECEIVER, RECEIVER_BALANCE, SALDO_REC, IMPORTE_MD, PENALTY_INTEREST_BALANCE, MULTA_INT, TAX1, IMPUESTO1 )")
        fSql.Append(" Select")
        fSql.AppendFormat("  '{0}',", Periodo)
        fSql.Append("      DOCUMENT_DATE,")
        fSql.Append("      RECEIVER,")
        fSql.Append("  IsNull(Sum(RECEIVER_BALANCE), 0 ) As  RECEIVER_BALANCE,")
        fSql.Append("  IsNull(Sum(SALDO_REC), 0 ) As  SALDO_REC,")
        fSql.Append("  IsNull(Sum(IMPORTE_MD), 0 ) As  IMPORTE_MD,")
        fSql.Append("  IsNull(Sum(PENALTY_INTEREST_BALANCE), 0 ) As   PENALTY_INTEREST_BALANCE,")
        fSql.Append("  IsNull(Sum(MULTA_INT), 0 ) As  MULTA_INT,")
        fSql.Append("  IsNull(Sum(TAX1), 0 ) As  TAX1,")
        fSql.Append("  IsNull(Sum(IMPUESTO1), 0 ) As  IMPUESTO1")
        fSql.Append(" From")
        fSql.Append("  FinanzasEventosBancos")
        fSql.Append(" Group By")
        fSql.Append("  DOCUMENT_DATE, RECEIVER")
        If Not ExecuteSQL(fSql.ToString) Then Return False

        fSql.Clear()
        fSql.Append(" UPDATE [FinanzasEventosBancosResu]")
        fSql.Append("   SET ")
        fSql.Append("       [STS1] = CASE WHEN [RECEIVER_BALANCE] <> [SALDO_REC] THEN 1 ELSE 0 END")
        fSql.Append("      ,[STS2] = CASE WHEN [RECEIVER_BALANCE] <> [IMPORTE_MD] THEN 1 ELSE 0 END")
        fSql.Append("      ,[STS3] = CASE WHEN [PENALTY_INTEREST_BALANCE] <> [MULTA_INT] THEN 1 ELSE 0 END")
        fSql.Append("      ,[STS4] = CASE WHEN [TAX1] <> [IMPUESTO1] THEN 1 ELSE 0 END")
        fSql.AppendFormat(" WHERE PERIODO = '{0}'", Periodo)
        If Not ExecuteSQL(fSql.ToString) Then Return False

        fSql.Clear()
        fSql.Append("UPDATE [FinanzasEventosBancosResu]")
        fSql.Append("   SET [STS5] = [STS1] + [STS2] + [STS3] + [STS4]")
        fSql.AppendFormat(" WHERE PERIODO = '{0}'", Periodo)
        Return ExecuteSQL(fSql.ToString)

    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Evento"></param>
    ''' <param name="Tabla"></param>
    ''' <param name="CampoVentasSAP"></param>
    ''' <param name="TitCampoVentas"></param>
    ''' <param name="aImpSAP"></param>
    ''' <returns></returns>
    Public Function FinanzasEventosSAP(ByVal Evento As String, ByVal Tabla As String, ByVal CampoVentasSAP As String, ByVal TitCampoVentas As String, aImpSAP() As String) As DataTable
        Dim fSql As New StringBuilder
        fSql.Clear()
        fSql.AppendFormat("Select  Fechadoc, SUM( {0}) as [{1}] ", ConvertFloat(CampoVentasSAP), TitCampoVentas)
        For i = 0 To UBound(aImpSAP)
            If (aImpSAP(i) <> "") Then
                fSql.AppendFormat("{0}, SUM( {1}) as Impuesto_{2} ", vbLf, ConvertFloat("Impuesto" & aImpSAP(i)), aImpSAP(i))
            End If
        Next
        fSql.AppendFormat("{0} FROM {1}", vbLf, Tabla)
        fSql.AppendFormat("{0} WHERE Evento = '{1}'", vbLf, Evento)
        fSql.AppendFormat("{0} GROUP BY Fechadoc", vbLf)

        Return getDataTable(fSql.ToString())

    End Function

    Function ConvertFloat(valor As String) As String
        Return String.Format("CONVERT(FLOAT, Replace([{0}], ',', '.') )", valor.ToString)
    End Function

    Function ConvertFecha(Fecha As String) As String
        Dim fsql As New StringBuilder

        fsql.Append("  CONVERT( nvarchar(30), ")
        fsql.Append("           CONVERT(datetime, ")
        fsql.AppendFormat("           RTrim(Replace( Replace( Replace( Replace( Upper({0}), 'A', '' ), 'M', '' ), 'P', '' ), '.', '' )), 103), 112) ", Fecha)
        Return fsql.ToString()

    End Function

#End Region

#Region "FUNCIONES"

    Public Function AbrirConexion() As Boolean
        Dim abierta As Boolean = False
        Try
            If conexion IsNot Nothing Then
                If conexion.State = ConnectionState.Open Then
                    abierta = True
                Else
                    conexion = New SqlConnection(gsConnectionString)

                    conexion.Open()
                    abierta = True
                End If
            Else

                conexion = New SqlConnection(gsConnectionString)
                wrt(conexion.ConnectionString)
                conexion.Open()
                abierta = True
            End If

            If abierta Then
                Consultacadena()
                wrt("Conexion abierta.")
                wrt(pgmVersion)
                dsp(conexion.ConnectionString)
            End If
        Catch ex1 As SqlException
            abierta = False
            mensaje = ex1.InnerException.ToString & vbCrLf & ex1.Message
        Catch ex As Exception
            abierta = False
            mensaje = ex.Message
        Finally
            If abierta = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo realizar la conexión.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & "Conexion: " & conexion.ConnectionString & vbCrLf & mensaje)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End If
        End Try
        Return abierta
    End Function

    Sub Consultacadena()
        Dim cadenanueva As String = ""
        Dim esta As Boolean = True
        fSql = "SELECT " & LXLONG_CAMPO & " AS Valor, CCCODE2 AS Parametro FROM RFPARAM WHERE CCTABL = 'LXLONG' AND CCSDSC = 'CONN.NET'"
        getDataTable(fSql)

        For Each row As DataRow In dt.Rows
            cadenanueva &= Trim(row("Parametro")) & "=" & Trim(row("Valor"))
            If Not gsConnectionString.ToUpper.Contains(UCase(Trim(row("Parametro")) & "=" & Trim(row("Valor")))) Then
                esta = False
            End If
            cadenanueva &= ";"
        Next

        If Not esta Then
            gsConnectionString = cadenanueva
            conexion.Close()
            conexion.ConnectionString = gsConnectionString
            conexion.Open()
        End If
    End Sub

    Public Function CerrarConexion() As Boolean
        Dim cerrada As Boolean = False
        Try
            conexion.Close()
            cerrada = True
        Catch ex1 As SqlException
            cerrada = False
            mensaje = ex1.Message
        Catch ex As Exception
            cerrada = False
            mensaje = ex.Message
        Finally
            If cerrada = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo Cerrar la conexión.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & "Conexion: " & conexion.ConnectionString)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End If
        End Try
        Return cerrada
    End Function

    Public Function ExecuteSQL(ByVal fSql As String) As Boolean
        lastSQL = fSql
        mensaje = ""
        numFilas = 0
        command = New SqlCommand
        command.Connection = conexion
        Try
            command.CommandText = fSql
            numFilas = command.ExecuteNonQuery()
            bResultSQL = True
        Catch ex As SqlException
            bResultSQL = False
            mensaje = ex.Message
        Catch ex As Exception
            bResultSQL = False
            mensaje = ex.Message
        Finally
            If bResultSQL = False Then
                dbg(mensaje)
                data = Nothing
                data = New RFLOG
                With data

                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Actualizar RFLOG"
                    .PROGRAMA = pgmAplicacion & "- V" & pgmVersion
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                    .LKEY = PalabraClave
                    .IDPROCESO = IdProceso
                End With
                GuardarRFLOGRow(data)
                Try
                    wrt(String.Format("SQL = [ {0}{1}{0} ]", vbCrLf, lastSQL))
                    wrt(mensaje)
                    conexion.Close()
                    If bDebug Then
                        Notepad(LOG_FILE)
                    End If
                Catch ex As Exception

                End Try
                Environment.Exit(-1)
            End If
        End Try
        Return bResultSQL

    End Function

    ''' <summary>
    ''' Devuelve datatable de la sentencia sql
    ''' </summary>
    ''' <param name="fSql"></param>
    ''' <returns></returns>
    Public Function getDataTable(ByVal fSql As String) As DataTable

        dt = Nothing
        ds = Nothing

        numFilas = 0
        mensaje = ""
        adapter = New SqlDataAdapter(fSql, conexion)
        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New SqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        Try
            numFilas = adapter.Fill(ds)
            bResultSQL = True
        Catch ex As SqlException
            bResultSQL = False
            mensaje = ex.Message
        Catch ex As Exception
            bResultSQL = False
            mensaje = ex.Message
        Finally
            If bResultSQL = False Then
                numFilas = -1
                data = Nothing
                data = New RFLOG
                With data
                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Actualizar RFLOG"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarRFLOGRow(data)
            End If
        End Try
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing

        Return dt
    End Function

    Public Function getCamposTabla(ByVal Tabla As String) As DataTable

        dt = Nothing
        ds = Nothing

        numFilas = 0
        mensaje = ""
        adapter = New SqlDataAdapter(String.Format("SELECT * FROM {0} WHERE 1=0", Tabla), conexion)
        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New SqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        Try
            numFilas = adapter.Fill(ds)
            bResultSQL = True
        Catch ex As SqlException
            bResultSQL = False
            mensaje = ex.Message
        Catch ex As Exception
            bResultSQL = False
            mensaje = ex.Message
        Finally
            If bResultSQL = False Then
                numFilas = -1
                data = Nothing
                data = New RFLOG
                With data
                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Actualizar RFLOG"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarRFLOGRow(data)
            End If
        End Try
        dt = ds.Tables(0)
        adapter = Nothing
        builder = Nothing

        Return dt
    End Function


    Public Function getDato(fSql As String) As String
        Dim dt As DataTable
        dt = getDataTable(fSql)
        If Not IsDBNull(dt) Then
            Return dt.Rows(0).Item(0)
        End If

        Return ""

    End Function

    Public Function mkWhere(lstWhere As List(Of lstWhere), bTieneWhere As Boolean) As String
        Dim condiWhere = From condi In lstWhere
        Dim wSql As New StringBuilder
        Dim sep As String = ""

        For Each condi In condiWhere
            wSql.AppendFormat("{0}{1} = {2}{3}{2}", sep, condi.Campo, condi.Tipo, condi.Valor, condi.Tipo)
            sep = " AND "
        Next
        If wSql.Length > 0 Then
            If bTieneWhere Then
                wSql.Insert(0, " AND ")
            Else
                wSql.Insert(0, " WHERE ")
            End If
        End If
        Return wSql.ToString

    End Function

    Public Function getRegistros() As Integer
        Return numFilas
    End Function

    Public Function getLastSql() As String
        Return Me.lastSQL
    End Function

    Public Function getError() As String
        Return Replace(Me.mensaje, "'", "''")
    End Function

    Public Function getResultadoSQL() As Boolean
        Return bResultSQL
    End Function

    Public Sub setError(txt As String)
        Me.mensaje = txt
    End Sub

    Public Sub setPalabraClave(txt As String)
        Me.PalabraClave = txt
    End Sub

    Public Function tit(txt)
        tit = Replace(txt, "_", " ")
    End Function
#End Region

#Region "GUARDAR"

    ''' <summary>
    ''' Guarda RFLOG
    ''' </summary>
    ''' <param name="datos">RFLOG</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 22/04/2016</remarks>
    Public Function GuardarRFLOGRow(ByVal datos As RFLOG) As Boolean
        Dim continuarguardandoRFLOG As Boolean
        Dim fSql, vSql As String

        fSql = " INSERT INTO RFLOG( "
        vSql = " VALUES ( "
        fSql = fSql & " OPERACION , " : vSql = vSql & "'" & datos.OPERACION & "', "
        fSql = fSql & " USUARIO   , " : vSql = vSql & "'" & datos.USUARIO & "', "
        fSql = fSql & " PROGRAMA  , " : vSql = vSql & "'" & datos.PROGRAMA & "', "
        fSql = fSql & " EVENTO    , " : vSql = vSql & "'" & datos.EVENTO.Replace("'", "''") & "', "
        fSql = fSql & " TXTSQL    , " : vSql = vSql & "'" & datos.TXTSQL.Replace("'", "''") & "', "
        fSql = fSql & " ALERT     , " : vSql = vSql & " " & datos.ALERT & ", "
        fSql = fSql & " LKEY      , " : vSql = vSql & "'" & datos.LKEY & "', "
        fSql = fSql & " IDPROCESO ) " : vSql = vSql & " " & datos.IDPROCESO & ") "

        command = New SqlCommand
        command.Connection = conexion
        Try
            command.CommandText = fSql & vSql
            command.ExecuteNonQuery()
            continuarguardandoRFLOG = True
        Catch ex As SqlException
            continuarguardandoRFLOG = False
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardandoRFLOG = False
            mensaje = ex.Message
        Finally
            If continuarguardandoRFLOG = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End If
        End Try
        Return continuarguardandoRFLOG
    End Function

    Public Function BulkINSERT(ByVal Tabla As String, ByVal Desde As String, ByVal PrimeraFila As Integer, Separador As String, Temporal As String) As Boolean
        Dim fSql As New StringBuilder
        Dim datos As New DataTable
        Dim dt As New DataTable
        Dim result As Boolean = True
        Dim i As Integer = 0
        If Temporal = "1" Then
            If ExecuteSQL("TRUNCATE TABLE FinanzasEventosWrk") And ExecuteSQL(String.Format("TRUNCATE TABLE {0}", Tabla)) Then
                fSql.Clear()
                fSql.Append(" BULK INSERT FinanzasEventosWrk")
                fSql.AppendFormat(" FROM  '{0}'", Desde)
                fSql.AppendFormat(" WITH ( FIRSTROW = {0}, MAXERRORS = 0, ROWTERMINATOR = '\n', CODEPAGE = 'ACP') ", PrimeraFila)
                If ExecuteSQL(fSql.ToString()) Then
                    datos = getDataTable("SELECT * FROM FinanzasEventosWrk")
                    dt = getCamposTabla(Tabla)
                    For Each dr As DataRow In datos.Rows
                        If Tabla = "FinanzasEvento1" Then
                            i += 1
                            If i Mod 10000 = 0 Then
                                EstadoProceso("Cargue de Archivos. Registros: " & i.ToString)
                                dsp(i)
                            End If
                        End If
                        result = result And InsertaRegistro(Tabla, dr, dt, Separador)
                    Next
                End If
            End If
        Else
            If ExecuteSQL(String.Format("TRUNCATE TABLE {0}", Tabla)) Then
                fSql.AppendFormat(" BULK INSERT {0}", Tabla)
                fSql.AppendFormat(" FROM  '{0}'", Desde)
                If Separador <> "" Then
                    fSql.AppendFormat(" WITH ( FIRSTROW = {0}, MAXERRORS = 0, FIELDTERMINATOR = '{1}', ROWTERMINATOR = '\n', CODEPAGE = 'ACP') ", PrimeraFila, Separador)
                Else
                    fSql.AppendFormat(" WITH ( FIRSTROW = {0}, MAXERRORS = 0, ROWTERMINATOR = '\n', CODEPAGE = 'ACP') ", PrimeraFila)
                End If
            End If
            result = ExecuteSQL(fSql.ToString())
        End If
        Return result

    End Function

    Function InsertaRegistro(Tabla As String, dr As DataRow, dt As DataTable, ByVal Separador As String) As Boolean
        Dim i As Integer = 0
        Dim fSql As New StringBuilder
        Dim vSql As New StringBuilder
        Dim sep As String = ""
        If Separador = "\t" Then
            Separador = vbTab
        End If
        Dim aCols() As String = Split(dr("campo").ToString, Separador)
        If UBound(aCols) < dt.Columns.Count - 2 Then
            Return True
        End If
        fSql.AppendFormat(" INSERT INTO {0}(", Tabla)
        vSql.Append(" VALUES( ")
        For i = 0 To UBound(aCols)
            fSql.AppendFormat("{0}{1}", sep, dt.Columns(i)) : vSql.AppendFormat("{0}'{1}'", sep, aCols(i))
            sep = ", "
        Next
        fSql.Append(" )")
        vSql.Append(" )")
        Return ExecuteSQL(fSql.ToString & vSql.ToString)

    End Function

    Public Function ActualizacionesProceso(Actualizacion As String) As Boolean
        Return ActualizacionesProceso(Actualizacion, "")
    End Function
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="Actualizacion"></param>
    ''' <returns></returns>
    Public Function ActualizacionesProceso(Actualizacion As String, Param As String) As Boolean
        Dim fSql As New StringBuilder
        fSql.Clear()
        dbg(Actualizacion)
        Select Case Actualizacion
            Case "Proceso"
                dbg(Me.PalabraClave & ": " & getError())
                fSql.AppendFormat(" UPDATE RFPARAM SET CCNOTE = '{0} v{1}<br>{2}{3}{4}'", pgmAplicacion, pgmVersion, Me.PalabraClave, IIf(Me.PalabraClave <> "", "<BR>", ""), getError())
                fSql.Append(", CCUDC2 = 0, CCUDTS = getDate() WHERE CCTABL = 'FinanzasEventos' AND CCCODE = 'Periodo'")
            Case "Inicio proceso"
                dbg(Me.PalabraClave & ": " & getError())
                fSql.AppendFormat(" UPDATE RFPARAM SET CCNOTE = '{0}{1}{2}'", Me.PalabraClave, IIf(Me.PalabraClave <> "", "<BR>", ""), getError())
                fSql.Append(", CCUDC2 = 1, CCUDTS = getDate() WHERE CCTABL = 'FinanzasEventos' AND CCCODE = 'Periodo'")
            Case "Fin proceso"
                dbg(Me.PalabraClave & ": " & getError())
                fSql.AppendFormat(" UPDATE RFPARAM SET CCNOTE = '{0}{1}{2}'", Me.PalabraClave, IIf(Me.PalabraClave <> "", "<BR>", ""), getError())
                fSql.Append(", CCUDC2 = 0, CCUDTS = getDate() WHERE CCTABL = 'FinanzasEventos' AND CCCODE = 'Periodo'")
            Case "FechaEvento1" 'Quitar am pm de la fecha
                fSql.Append("UPDATE FinanzasEvento1 SET ")
                fSql.AppendFormat("  Fecha = {0}", ConvertFecha("Fecha"))
                fSql.Append(", [SalesBalance] = Replace( Replace( [SalesBalance], '.', '' ), ',', '.' ) ")
                fSql.Append(", [Tax1] = Replace( Replace( [Tax1]    , '.', '' ), ',', '.' ) ")
                fSql.Append(", [Tax2] = Replace( Replace( [Tax2]    , '.', '' ), ',', '.' ) ")
                fSql.Append(", [Tax3] = Replace( Replace( [Tax3]    , '.', '' ), ',', '.' ) ")
                fSql.Append(", [Tax4] = Replace( Replace( [Tax4]    , '.', '' ), ',', '.' ) ")
                fSql.Append(", [Tax5] = Replace( Replace( [Tax5]    , '.', '' ), ',', '.' ) ")
                fSql.Append(", [Tax6] = Replace( Replace( [Tax6]    , '.', '' ), ',', '.' ) ")
                fSql.Append(", [Tax7] = Replace( Replace( [Tax7]    , '.', '' ), ',', '.' ) ")
            Case "FechaEvento2"
                fSql.AppendFormat("UPDATE FinanzasEvento2 SET Fecha = {0}", ConvertFecha("Fecha"))
            Case "FechaEvento3"
                fSql.AppendFormat("UPDATE FinanzasEvento3 SET Fecha = {0}", ConvertFecha("Fecha"))
            Case "ValoresEventosSAP"
                fSql.Append("UPDATE FinanzasEventosSAP SET")
                fSql.Append("  [Fechadoc]     = Right( REPLICATE('0', 8 ) + rtrim([Fechadoc]), 8 ) ")
                fSql.Append(", [Fecont]       = Right( REPLICATE('0', 8 ) + rtrim([Fecont]), 8 ) ")
                fSql.Append(", [Ventas]       = Replace( Replace( ltrim(rtrim([Ventas]       )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [TsServic]     = Replace( Replace( ltrim(rtrim([TsServic]     )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [SldEntr]      = Replace( Replace( ltrim(rtrim([SldEntr]      )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [DvEntrg]      = Replace( Replace( ltrim(rtrim([DvEntrg]      )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [Impuesto1]    = Replace( Replace( ltrim(rtrim([Impuesto1]    )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [Impuesto2]    = Replace( Replace( ltrim(rtrim([Impuesto2]    )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [Impuesto3]    = Replace( Replace( ltrim(rtrim([Impuesto3]    )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [Impuesto4]    = Replace( Replace( ltrim(rtrim([Impuesto4]    )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [Impuesto5]    = Replace( Replace( ltrim(rtrim([Impuesto5]    )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [Impuesto6]    = Replace( Replace( ltrim(rtrim([Impuesto6]    )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [Impuesto7]    = Replace( Replace( ltrim(rtrim([Impuesto7]    )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [Impuesto8]    = Replace( Replace( ltrim(rtrim([Impuesto8]    )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [Impuesto9]    = Replace( Replace( ltrim(rtrim([Impuesto9]    )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [Impuesto10]   = Replace( Replace( ltrim(rtrim([Impuesto10]   )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [cliente]      = Replace( Replace( ltrim(rtrim([cliente]      )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [VentasNCN]    = Replace( Replace( ltrim(rtrim([VentasNCN]    )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [MonedaPar]    = Replace( Replace( ltrim(rtrim([MonedaPar]    )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [Motivo]       = Replace( Replace( ltrim(rtrim([Motivo]       )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [NroFat]       = Replace( Replace( ltrim(rtrim([NroFat]       )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [NroOcor]      = Replace( Replace( ltrim(rtrim([NroOcor]      )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [Pedido]       = Replace( Replace( ltrim(rtrim([Pedido]       )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [DevolucionCN] = Replace( Replace( ltrim(rtrim([DevolucionCN] )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [EstNota]      = Replace( Replace( ltrim(rtrim([EstNota]      )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [MultaInt]     = Replace( Replace( ltrim(rtrim([MultaInt]     )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [SaldoRec]     = Replace( Replace( ltrim(rtrim([SaldoRec]     )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [NotaCredito]  = Replace( Replace( ltrim(rtrim([NotaCredito]  )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [SaldoBj]      = Replace( Replace( ltrim(rtrim([SaldoBj]      )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [ProvInt]      = Replace( Replace( ltrim(rtrim([ProvInt]      )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [SaldoRec2]    = Replace( Replace( ltrim(rtrim([SaldoRec2]    )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [Cotizacion]   = Replace( Replace( ltrim(rtrim([Cotizacion]   )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [TitRevPA]     = Replace( Replace( ltrim(rtrim([TitRevPA]     )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [TitRevLC]     = Replace( Replace( ltrim(rtrim([TitRevLC]     )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [TitExPA]      = Replace( Replace( ltrim(rtrim([TitExPA]      )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [titExLC]      = Replace( Replace( ltrim(rtrim([titExLC]      )), '.', '' ), ',', '.' ) ")
                fSql.Append(", [Recibidor]    = Replace( Replace( ltrim(rtrim([Recibidor]    )), '.', '' ), ',', '.' ) ")
            Case "ValoresEventosSAPFecha"
                fSql.Append("UPDATE FinanzasEventosSAP SET")
                fSql.Append("  [Fechadoc]     = SUBSTRING(Fechadoc, 5, 4 ) + SUBSTRING(Fechadoc, 3, 2 ) + SUBSTRING(Fechadoc, 1, 2 )")
                fSql.Append(", [Fecont]       = SUBSTRING(Fecont, 5, 4 ) + SUBSTRING(Fecont, 3, 2 ) + SUBSTRING(Fecont, 1, 2 ) ")
            Case "ValoresEventosCuentas"
                fSql.Append("UPDATE FinanzasEventosCuentas SET")
                fSql.Append("  [FeContable]     = Replace( ltrim(rtrim([FeContable])), '.', '' ) ")
                fSql.Append(", [FechaDoc]       = Replace( ltrim(rtrim([FechaDoc])), '.', '' ) ")
                fSql.Append(", [ImporteMD]       = Replace( Replace( ltrim(rtrim([ImporteMD]))       , '.', '' ), ',', '.' ) ")
            Case "ValoresEventosCuentasFecha"
                fSql.Append("UPDATE FinanzasEventosCuentas SET")
                fSql.Append("  [FeContable]     = SUBSTRING(FeContable, 5, 4 ) + SUBSTRING(FeContable, 3, 2 ) + SUBSTRING(FeContable, 1, 2 )")
                fSql.Append(", [FechaDoc]       = SUBSTRING(FechaDoc, 5, 4 ) + SUBSTRING(FechaDoc, 3, 2 ) + SUBSTRING(FechaDoc, 1, 2 ) ")
            Case "FechaEvento9"
                fSql.Append("UPDATE FinanzasEvento9 SET ")
                fSql.Append("  DOCUMENT_DATE  = ")
                fSql.Append("  CONVERT( nvarchar(30), ")
                fSql.Append("           CONVERT(datetime, ")
                fSql.Append("           RTrim(Replace( Replace( Replace( Replace( Upper(DOCUMENT_DATE), 'A', '' ), 'M', '' ), 'P', '' ), '.', '' )), 103), 112) ")
                fSql.Append(", TIPO_BAJA = CASE WHEN TIPO_BAJA = 'DÃ©bito' THEN 'Debito' ")
                fSql.Append("                 WHEN TIPO_BAJA = 'CrÃ©dito' THEN 'Credito' ")
                fSql.Append("                 WHEN TIPO_BAJA = 'TÃ­tulo' THEN 'Titulo' ")
                fSql.Append("                 ELSE TIPO_BAJA END ")
                fSql.Append(", [RECEIVER_BALANCE] = Replace( Replace( [RECEIVER_BALANCE]    , '.', '' ), ',', '.' ) ")
                fSql.Append(", [PENALTY_INTEREST_BALANCE] = Replace( Replace( [PENALTY_INTEREST_BALANCE]    , '.', '' ), ',', '.' ) ")
                fSql.Append(", [LOSS_RECOVERY_BALANCE] = Replace( Replace( [LOSS_RECOVERY_BALANCE]    , '.', '' ), ',', '.' ) ")
                fSql.Append(", [Tax1] = Replace( Replace( [Tax1]    , '.', '' ), ',', '.' ) ")
                fSql.Append(", [Tax2] = Replace( Replace( [Tax2]    , '.', '' ), ',', '.' ) ")
                fSql.Append(", [Tax3] = Replace( Replace( [Tax3]    , '.', '' ), ',', '.' ) ")
                fSql.Append(", [Tax4] = Replace( Replace( [Tax4]    , '.', '' ), ',', '.' ) ")
                fSql.Append(", [Tax5] = Replace( Replace( [Tax5]    , '.', '' ), ',', '.' ) ")
                fSql.Append(", [Tax6] = Replace( Replace( [Tax6]    , '.', '' ), ',', '.' ) ")
            Case "FinanzasEventosDiferencias"
                fSql.Append(" UPDATE FinanzasEventos123 SET  ")
                fSql.Append(" Diferencia = ValorGERA - ValorSAP ")
                fSql.AppendFormat("WHERE Periodo = '{0}'", Param)

        End Select
        bResultSQL = ExecuteSQL(fSql.ToString)
        Return bResultSQL

    End Function

    Function InsertFinanzasEventos(Actualizacion As String, Optional ByVal vSql As String = "") As Boolean
        Dim fSql As New StringBuilder
        fSql.Clear()
        Select Case Actualizacion
            Case "FinanzasEventosSAP"
                fSql.AppendFormat(" INSERT INTO  FinanzasEventosSAP VALUES( {0} ) ", vSql)
            Case "FinanzasEventosCuentas"
                fSql.AppendFormat(" INSERT INTO  FinanzasEventosCuentas VALUES( {0} ) ", vSql)
        End Select
        bResultSQL = ExecuteSQL(fSql.ToString)
        Return bResultSQL

    End Function
    Sub EstadoProceso(txtEstado As String)

        setError(txtEstado)
        ActualizacionesProceso("Proceso")
    End Sub

#End Region

End Class
