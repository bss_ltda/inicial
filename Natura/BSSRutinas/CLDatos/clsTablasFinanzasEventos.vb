﻿Public Class FinanzasEventos123
    Public IdEvento As String     'P
    Public Periodo As String      'A
    Public Fecha As String        'A
    Public Evento As String       'A
    Public Item As String         'A
    Public ValorGERA As String    'P
    Public ValorSAP As String     'P
    Public Diferencia As String   'P
    Public Observacion As String  'A

    Dim tipoDato As New Dictionary(Of String, String)
    Dim pair As KeyValuePair(Of String, String)

    Public Sub New()

        With tipoDato
            .Add("IdEvento", "")
            .Add("Periodo", "'")
            .Add("Fecha", "'")
            .Add("Evento", "'")
            .Add("Item", "'")
            .Add("ValorGERA", "")
            .Add("ValorSAP", "")
            .Add("Diferencia", "")
            .Add("Observacion", "'")
        End With
        Inicializa()

    End Sub

    Function Insert(Optional ByVal FromSelect As String = "") As String
        Dim sql As New clsMakeSQL

        With sql
            .InsertInto("FinanzasEventos123", FromSelect)
            .Values("IdEvento", IdEvento, "")
            .Values("Periodo", Periodo, "'")
            .Values("Fecha", Fecha, "'")
            .Values("Evento", Evento, "'")
            .Values("Item", Item, "'")
            .Values("ValorGERA", ValorGERA, "")
            .Values("ValorSAP", ValorSAP, "")
            .Values("Diferencia", Diferencia, "")
            .Values("Observacion", Observacion, "'")
            Return .getSQL()
        End With

    End Function

    Function Update(camposWhere As Dictionary(Of String, String)) As String
        Dim sql As New clsMakeSQL

        With sql
            .Update("FinanzasEventos123")
            .Values("IdEvento", IdEvento, "")
            .Values("Periodo", Periodo, "'")
            .Values("Fecha", Fecha, "'")
            .Values("Evento", Evento, "'")
            .Values("Item", Item, "'")
            .Values("ValorGERA", ValorGERA, "")
            .Values("ValorSAP", ValorSAP, "")
            .Values("Diferencia", Diferencia, "")
            .Values("Observacion", Observacion, "'")
            For Each pair In camposWhere
                .Where(pair.Key, pair.Value, tipoDato.Item(pair.Key))
            Next
            Return .getSQL()
        End With

    End Function

    Function Delete(Campo As String, Valor As String) As String
        Dim sql As New clsMakeSQL

        With sql
            .Delete("FinanzasEventos123")
            .Where(Campo, Valor, tipoDato.Item(Campo))
            Return .getSQL()
        End With

    End Function
    Function Delete(camposWhere As Dictionary(Of String, String)) As String
        Dim sql As New clsMakeSQL
        With sql
            .Delete("FinanzasEventos123")
            For Each pair In camposWhere
                .Where(pair.Key, pair.Value, tipoDato.Item(pair.Key))
            Next
            Return .getSQL()
        End With
    End Function

    Public Sub Inicializa()
        IdEvento = ""
        Periodo = ""
        Fecha = ""
        Evento = ""
        Item = ""
        ValorGERA = ""
        ValorSAP = ""
        Diferencia = ""
        Observacion = ""
    End Sub
End Class