﻿Imports System.Globalization
Imports System.IO
Imports System.Text
Public Module modUtilBSS

    Public bPruebas As Boolean
    Public bDebug As Boolean
    Public gsConnectionString As String
    Public LXLONG As String = "LXLONG"
    Public LXLONG_CAMPO As String = "CCNOT1"
    Public AMBIENTE As String
    Public LOG_FILE As String
    Public appPath As String
    Public NotepadPath As String
    Public pgmVersion As String
    Public pgmAplicacion As String

    Function convierteFecha(fecString As String, formato As String) As String
        Dim parsedDate As Date
        Dim aFec() As String = Split(fecString, " ")

        DateTime.TryParseExact(aFec(0), formato, Nothing, DateTimeStyles.None, parsedDate)

        Return parsedDate.ToString("yyyy-MM-dd")
    End Function

    Sub SetAmbiente(ByVal parametros() As String)
        Dim aSubParam() As String
        Dim sep As String = ""
        aSubParam = gsConnectionString.Split(";")
        For Each parametro In aSubParam
            aSubParam = Split(parametro, "=")
            Select Case UCase(aSubParam(0))
                Case "DATABASE", "DEFAULT COLLECTION", "DEFAULTCOLLECTION"
                    AMBIENTE = aSubParam(1)
            End Select
        Next
        For Each parametro In parametros
            aSubParam = Split(parametro, "=")
            Select Case UCase(aSubParam(0))
                Case "LIB"
                    AMBIENTE = aSubParam(1)
            End Select
        Next
        aSubParam = gsConnectionString.Split(";")
        gsConnectionString = ""
        For Each parametro In aSubParam
            aSubParam = Split(parametro, "=")
            Select Case UCase(aSubParam(0))
                Case "DATABASE", "DEFAULT COLLECTION", "DEFAULTCOLLECTION"
                    gsConnectionString &= sep & aSubParam(0) & "=" & AMBIENTE
                Case Else
                    gsConnectionString &= sep & parametro
            End Select
            sep = ";"
        Next
    End Sub


    Sub dsp(txt)
        Console.WriteLine("")
        Console.WriteLine(txt.ToString)
    End Sub

    Sub dbg(txt)
        If bDebug Then
            wrt(txt)
            dsp(txt)
        End If
    End Sub

    Sub wrt_ini(ByVal archivo As String)
        If archivo = "" Then
            archivo = "LOG-" & DateTime.Today.ToString("dd-MMM-yyyy") & ".txt"
        End If
        appPath = Path.GetFullPath(My.Application.Info.DirectoryPath & "\Log\")
        If Not Directory.Exists(appPath) Then
            Directory.CreateDirectory(appPath)
        End If
        LOG_FILE = appPath & archivo
        If File.Exists(LOG_FILE) Then
            File.Delete(LOG_FILE)
        End If
    End Sub
    Sub wrt(txt)

        Dim sb As New StringBuilder()
        'Dim sw As StreamWriter = New StreamWriter(LOG_FILE)
        sb.AppendLine(txt)
        Using sw As StreamWriter = File.AppendText(LOG_FILE)
            sw.WriteLine(sb.ToString())
        End Using

    End Sub

    Sub Notepad(archivo As String)
        Shell(String.Format("""{0}"" ""{1}""", NotepadPath, archivo), AppWinStyle.MaximizedFocus, False)
    End Sub
End Module
