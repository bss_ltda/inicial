﻿Imports System.Text

Public Class clsMakeSQL
    Dim fSql As New StringBuilder
    Dim vSql As New StringBuilder
    Dim whereSql As New StringBuilder
    Dim sepCampo As String
    Dim sepWhere As String
    Dim bInsert As Boolean
    Dim FromSelect As String
    Dim groupBY As Dictionary(Of String, String)

    Public Sub New()
        fSql.Clear()
        vSql.Clear()
        whereSql.Clear()
        sepCampo = ""
        sepWhere = ""
        groupBY = New Dictionary(Of String, String)
    End Sub

    Sub InsertInto(Tabla As String, Optional ByVal FromSelect As String = "")
        Me.FromSelect = FromSelect
        fSql.AppendFormat(" INSERT INTO {0}( ", Tabla)

        If Me.FromSelect <> "" Then
            vSql.Append(" SELECT ")
        Else
            vSql.Append(" VALUES( ")
        End If

        bInsert = True

    End Sub

    Sub Values(Campo As String, Valor As String, Tipo As String)
        If Valor <> "" Then
            If bInsert Then
                fSql.AppendFormat("{0} {1} ", sepCampo, Campo)
                vSql.AppendFormat("{0} '{1}' ", sepCampo, Valor)
                If Me.FromSelect <> "" Then
                    paraGroupBy(Valor)
                End If
            Else
                ' CAMPO = 'VALOR'
                fSql.AppendFormat("{0} {1} = {3}{2}{3} ", sepCampo, Campo, Valor, Tipo)
            End If
            sepCampo = ","
        End If

    End Sub
    Sub Where(Campo As String, Valor As String, Tipo As String)

        If Valor <> "" Then
            ' AND CAMPO = 'VALOR'
            whereSql.AppendFormat("{0} {1} = {3}{2}{3} ", sepWhere, Campo, Valor, Tipo)
            sepWhere = " AND "
        End If

    End Sub

    Sub Update(Tabla As String)
        fSql.Clear()
        vSql.Clear()
        whereSql.Clear()
        fSql.AppendFormat(" UPDATE {0} SET ", Tabla)
        bInsert = False
    End Sub

    Sub Delete(Tabla As String)
        fSql.Clear()
        vSql.Clear()
        whereSql.Clear()
        fSql.AppendFormat(" DELETE FROM {0} ", Tabla)
        bInsert = False
    End Sub

    Function getSQL()
        Dim locSQL As New StringBuilder
        locSQL.Clear()

        If bInsert Then
            If Me.FromSelect <> "" Then
                If whereSql.Length > 0 Then
                    locSQL.AppendFormat("{0} {1} WHERE {2} )", fSql.ToString, vSql.ToString, whereSql.ToString)
                Else
                    locSQL.AppendFormat("{0} {1} )", fSql.ToString, vSql.ToString)
                End If
            Else
                locSQL.AppendFormat("{0} ) {1} )", fSql.ToString, vSql.ToString)
            End If
            If groupBY.Count > 0 Then
                Dim pair As KeyValuePair(Of String, String)
                Dim sep As String = ""
                For Each pair In groupBY
                    locSQL.Append(" GROUP BY ")
                    locSQL.AppendFormat("{0} {1}", sep, pair.Key)
                    sep = ","
                Next
            End If
        Else
            If whereSql.Length > 0 Then
                locSQL.AppendFormat("{0} WHERE {1} ", fSql.ToString, whereSql.ToString)
            Else
                locSQL.Append(fSql.ToString)
            End If
        End If

        Return locSQL.ToString

    End Function

    Sub paraGroupBy(valor As String)
        Dim aAgregadas() As String = {"SUM(", "AVG(", "MAX(", "MIN(", "COUNT(", "STDEV(", "STDEVP(", "VAR(", "VARP("}
        For Each func In aAgregadas
            If InStr(valor.ToUpper(), func) = 0 Then
                groupBY.Add(valor, valor)
            End If
        Next
    End Sub
End Class

Public Class lstWhere
    Public Campo As String
    Public Valor As String
    Public Tipo As String
End Class

Public Class sqlWhere
    Dim lstWhere = New List(Of lstWhere)
    Public Sub Inicializa()
        lstWhere.clear()
    End Sub

    Public Sub Where(Campo As String, Valor As String, Tipo As String)
        lstWhere.Add(New lstWhere With {.Campo = Campo, .Valor = Valor, .Tipo = Tipo})
    End Sub

    Public Function getWhere() As List(Of lstWhere)
        Return lstWhere
    End Function

End Class