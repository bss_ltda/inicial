﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Text
Imports iTextSharp.text.pdf
Imports System.Drawing

Public Class ClsDatosBSSFormatoRedencionAdelanto
    Private command As New SqlCommand
    Public ReadOnly conexion As New SqlConnection()
    Private builder As SqlCommandBuilder
    Private adapter As SqlDataAdapter = New SqlDataAdapter()
    Private dt As DataTable
    Private ds As DataSet
    Public fSql As String
    Private numFilas As Integer
    Public appPath As String = Path.GetFullPath(My.Application.Info.DirectoryPath & "\Log\")
    Public mensaje As String
    Private data
    Private gsAppPath As String
    Private gsAppName As String
    Private gsAppVersion As String

#Region "CONSULTAR"

    ''' <summary>
    ''' Consulta la tabla RCAU con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- RFSRGH.SNUMDOC(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 08/06/2016</remarks>
    Public Function ConsultarRCAUdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing

        Select Case tipoparametro
            Case 0
                fSql = " SELECT RCAU.UTIPO, RCAU.UNOM, RCAU.UCATEG1, RCAU.UCATEG2, RCAU.UUSR, RCAU.UCATEG3, RFSRGH.SCENCCOST,   "
                fSql &= " (SELECT CCDESC   "
                fSql &= " FROM RFPARAM   "
                fSql &= " WHERE RFPARAM.CCTABL = 'CENTROCOSTO' And RFPARAM.CCCODE = RFSRGH.SCENCCOST) AS CENTROCOSTO,   "
                fSql &= " (SELECT RFPARAM.CCDESC   "
                fSql &= " FROM RFPARAM   "
                fSql &= " WHERE RFPARAM.CCTABL = 'ORIGEN' And RFPARAM.CCCODE = RFSRGH.SORIGEN) As SORIGEN,   "
                fSql &= " (SELECT RFPARAM.CCDESC   "
                fSql &= " FROM  RFPARAM   "
                fSql &= " WHERE RFPARAM.CCTABL = 'DESTINO' And RFPARAM.CCCODE = RFSRGH.SDESTINO) As SDESTINO,   "
                fSql &= " (SELECT RFPARAM.CCDESC   "
                fSql &= " FROM RFPARAM   "
                fSql &= " WHERE RFPARAM.CCTABL = 'LOCALIDAD'  "
                fSql &= " And RFPARAM.CCCODE = RFSRGH.SLOCALIDAD) As SLOCALIDAD,   "
                fSql &= " RFSRGH.SFSALIDA, RFSRGH.SFLLEGADA, RFSRGH.SNUMDOC, RFSRGH.SVALORLOC, RFSRGH.SFECDOC, RCAU.UUPLFILE1,  "
                fSql &= " DateDiff(day, RFSRGH.SFSALIDA, RFSRGH.SFLLEGADA) As DIAS,  "
                fSql &= " RFSRGH.SJUSTIFIC, AP.UNOM As UNOMGESTOR, AP.UUPLFILE1 As UUPLFILE1GESTOR,  "
                fSql &= " RFSRGH.SFSOLICITA, RFSRGH.SFAPRUEBA, RFSRGH.SSOLICITA, RFSRGH.SAPRUEBA  "
                fSql &= " FROM RCAU Inner Join RFSRGH On RCAU.UUSR = RFSRGH.SSOLICITA Inner Join RCAU AP On RFSRGH.SAPRUEBA = AP.UUSR   "
                fSql &= String.Format(" WHERE RFSRGH.SNUMDOC = {0} ", parametroBusqueda(0))
        End Select
        adapter = New SqlDataAdapter(fSql, conexion)
        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New SqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla RFSRGL con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- DNUMDOC(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 08/06/2016</remarks>
    Public Function ConsultarRFSRGLdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing

        Select Case tipoparametro
            Case 0
                fSql = " SELECT DNUMDOC, DNUMLIN, DFECDOC, "
                fSql &= " (SELECT RTRIM(CCDESC) + ' - ' + DCUENTA "
                fSql &= " FROM RFPARAM  "
                fSql &= " WHERE (CCTABL = 'CUENTA') AND (CCCODE = RFSRGL.DCUENTA)) AS CUENTA,  "
                fSql &= " DCUENTA, DPROV, DDESCRIP, CAST(DVALOR AS Float) AS DVALOR,  "
                fSql &= " DMONEDA,  "
                fSql &= " (SELECT RTRIM(CCALTC)  "
                fSql &= " FROM RFPARAM AS RFPARAM_1  "
                fSql &= " WHERE (CCTABL = 'MONEDA') AND (CCCODE = RFSRGL.DMONEDA)) AS MONEDA,  "
                fSql &= " DTRMUSD, CAST(DTRMLOC AS Float) AS DTRMLOC, CAST(DVALORLOC AS Float) AS DVALORLOC,  "
                fSql &= " DNUMUPL, FFLAG, STS1, STS2, STS3, STS4, STS5, DCRTUSR, DCRTDAT, DUPDDAT, DIMPUESTOS, DREFER  "
                fSql &= " FROM RFSRGL  "
                fSql &= String.Format(" WHERE DNUMDOC = {0} ", parametroBusqueda(0))
                fSql &= " ORDER BY DFECDOC "
                'fSql = " SELECT DNUMDOC, DNUMLIN, DFECDOC, "
                'fSql &= " (SELECT RTRIM(CCDESC) + ' - ' + DCUENTA "
                'fSql &= " FROM RFPARAM  "
                'fSql &= " WHERE (CCTABL = 'CUENTA') AND (CCCODE = RFSRGL.DCUENTA) AND CCCODE2 =  "
                'fSql &= " (SELECT UTIPO  "
                'fSql &= " FROM RCAU WHERE UUSR =  "
                'fSql &= " (SELECT SSOLICITA  FROM RFSRGH  "
                'fSql &= String.Format(" WHERE SNUMDOC = {0} ", parametroBusqueda(0))
                'fSql &= "))) AS CUENTA,  "
                'fSql &= " DCUENTA, DPROV, DDESCRIP, CAST(DVALOR AS Float) AS DVALOR,  "
                'fSql &= " DMONEDA,  "
                'fSql &= " (SELECT RTRIM(CCALTC)  "
                'fSql &= " FROM RFPARAM AS RFPARAM_1  "
                'fSql &= " WHERE (CCTABL = 'MONEDA') AND (CCCODE = RFSRGL.DMONEDA)) AS MONEDA,  "
                'fSql &= " DTRMUSD, CAST(DTRMLOC AS Float) AS DTRMLOC, CAST(DVALORLOC AS Float) AS DVALORLOC,  "
                'fSql &= " DNUMUPL, FFLAG, STS1, STS2, STS3, STS4, STS5, DCRTUSR, DCRTDAT, DUPDDAT, DIMPUESTOS, DREFER  "
                'fSql &= " FROM RFSRGL  "
                'fSql &= String.Format(" WHERE DNUMDOC = {0} ", parametroBusqueda(0))
                'fSql &= " ORDER BY DFECDOC "
            Case 1
                fSql = " SELECT DCUENTA, DCENCOS, "
                fSql &= " (SELECT RTRIM(CCDESC) + ' - ' + DCUENTA   "
                fSql &= " FROM RFPARAM  "
                fSql &= " WHERE (CCTABL = 'CUENTA') AND (CCCODE = RFSRGL.DCUENTA)) AS CUENTA,  "
                fSql &= " Sum(CAST(DVALOR AS FLOAT)) AS VALOR,  "
                fSql &= " Sum(CAST(DVALORLOC AS FLOAT)) AS VALORLOC  "
                fSql &= " FROM RFSRGL  "
                fSql &= String.Format(" WHERE DNUMDOC = {0} ", parametroBusqueda(0))
                fSql &= " GROUP BY DCUENTA, DCENCOS  "
                fSql &= " ORDER BY DCUENTA "
                'fSql = " SELECT DCUENTA, DCENCOS, "
                'fSql &= " (SELECT RTRIM(CCDESC) + ' - ' + DCUENTA   "
                'fSql &= " FROM RFPARAM  "
                'fSql &= " WHERE (CCTABL = 'CUENTA') AND (CCCODE = RFSRGL.DCUENTA) AND CCCODE2 =  "
                'fSql &= " (SELECT UTIPO  "
                'fSql &= " FROM RCAU  "
                'fSql &= " WHERE UUSR =  "
                'fSql &= " (SELECT SSOLICITA  "
                'fSql &= " FROM RFSRGH  "
                'fSql &= String.Format(" WHERE SNUMDOC = {0} ", parametroBusqueda(0))
                'fSql &= " ))) AS CUENTA,  "
                'fSql &= " Sum(CAST(DVALOR AS FLOAT)) AS VALOR,  "
                'fSql &= " Sum(CAST(DVALORLOC AS FLOAT)) AS VALORLOC  "
                'fSql &= " FROM RFSRGL  "
                'fSql &= String.Format(" WHERE DNUMDOC = {0} ", parametroBusqueda(0))
                'fSql &= " GROUP BY DCUENTA, DCENCOS  "
                'fSql &= " ORDER BY DCUENTA "
        End Select
        adapter = New SqlDataAdapter(fSql, conexion)
        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New SqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla RFPARAM con un determinado parametro de busqueda.Retorna un String
    ''' </summary>
    ''' <param name="prm">parametro de Busqueda</param>
    ''' <param name="Campo"></param>
    ''' <returns>Un String</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 08/06/2016</remarks>
    Public Function ConsultarRFPARAMString(ByVal prm As String, ByVal Campo As String, ByVal cctabl As String) As String
        dt = Nothing
        ds = Nothing

        If Campo = "" Then
            Campo = "CCNOT1"
        End If
        If cctabl = "" Then
            cctabl = "LXLONG"
        End If
        fSql = "SELECT " & Campo & " AS DATO "
        fSql &= " FROM RFPARAM "
        fSql &= " WHERE CCTABL='" & cctabl & "' AND UPPER(CCCODE) = UPPER('" & prm & "')"

        adapter = New SqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New SqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        Return dt.Rows(0).Item("DATO")
    End Function

    ''' <summary>
    ''' Consulta la tabla ZCCL01 con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- CCCODE(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 08/06/2016</remarks>
    Public Function ConsultarZCCL01dt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT CCSDSC, CCUDC1, CCNOT2, CCDESC  FROM ZCCL01 WHERE CCTABL='RFVBVER' AND UPPER(CCCODE) = '" & parametroBusqueda(0) & "'"
        End Select
        adapter = New SqlDataAdapter(fSql, conexion)
        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New SqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        Return dt
    End Function

#End Region

#Region "FUNCIONES"

    Public Function AbrirConexion() As Boolean
        Dim abierta As Boolean = False
        Try
            If Not Directory.Exists(appPath) Then
                Directory.CreateDirectory(appPath)
            End If
            If conexion.State = ConnectionState.Open Then
                abierta = True
            Else
                conexion.ConnectionString = gsConnectionString
                conexion.Open()
                abierta = True
            End If
        Catch ex1 As SqlException
            mensaje = ex1.Message
        Catch ex As Exception
            mensaje = ex.Message
        Finally
            If abierta = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo establecer Conexion." & vbCrLf & mensaje)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End If
        End Try
        Return abierta
    End Function

    Public Function CerrarConexion() As Boolean
        Dim cerrada As Boolean = False
        Try
            conexion.Close()
            cerrada = True
        Catch ex1 As SqlException
            mensaje = ex1.Message
        Catch ex As Exception
            mensaje = ex.Message
        Finally
            If cerrada = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo finalizar Conexion." & vbCrLf & mensaje)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End If
        End Try
        Return cerrada
    End Function

    Public Function Ceros(val As String, c As Integer)
        Dim result As String
        Dim sCeros As String = New String("0", c)
        result = Right(sCeros & val, c)
        Return result
    End Function

    Public Sub WrtTxtError(Archivo As String, ByVal Texto As String)
        Dim thisFile As FileInfo
        Try
            Using w As StreamWriter = File.AppendText(Archivo)
                Log(Texto, w)
            End Using
        Catch ex As Exception
            thisFile = My.Computer.FileSystem.GetFileInfo(Archivo)
            ' Si el directorio no existe, crearlo
            If Not Directory.Exists(thisFile.Directory.ToString) Then
                Directory.CreateDirectory(thisFile.Directory.ToString)
            End If
            Try
                Using w As StreamWriter = File.AppendText(Archivo)
                    Log(Texto, w)
                End Using
            Catch ex1 As Exception

            End Try
        End Try
    End Sub

    Public Sub Log(logMessage As String, w As TextWriter)
        w.Write(vbCrLf + "Entrada de Log : ")
        w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString())
        w.WriteLine("  :{0}", logMessage)
        w.WriteLine(" ")
    End Sub

    Public Function CheckVer() As Boolean
        Dim lVer As String
        Dim msg As String
        Dim NomApp As String
        Dim App As clsApp = New clsApp(Reflection.Assembly.GetExecutingAssembly)
        Dim Resultado As Boolean = True
        Dim param(0) As String
        Dim dtZCCL01 As DataTable

        gsAppPath = App.Path
        gsAppName = App.EXEName
        gsAppVersion = App.Major.ToString & "." & App.Minor.ToString & "." & App.Revision.ToString

        lVer = "No hay registro"
        param(0) = gsAppName.ToUpper
        dtZCCL01 = ConsultarZCCL01dt(param, 0)
        If dtZCCL01 IsNot Nothing Then
            lVer = Trim(dtZCCL01.Rows(0).Item("CCSDSC"))
            NomApp = dtZCCL01.Rows(0).Item("CCDESC")
            If InStr(lVer, "*NOCHK") > 0 Then
                Resultado = True
            ElseIf InStr(lVer, "(" & gsAppVersion & ")") > 0 Then
                Resultado = True
            End If
            If Resultado And dtZCCL01.Rows(0).Item("CCUDC1") = 0 Then
                msg = "La aplicacion " & NomApp & " no se puede usar en este momento." & vbCr
                msg = msg & "Razon: " & vbCr
                msg = msg & "=========================================" & vbCr
                If Trim(dtZCCL01.Rows(0).Item("CCNOT2")) = "" Then
                    msg = msg & "Aplicacion en Mantenimiento." & vbCr
                Else
                    msg = msg & dtZCCL01.Rows(0).Item("CCNOT2") & vbCr
                End If
                msg = msg & "=========================================" & vbCr
                Return False
            End If
        End If

        If Not Resultado Then
            msg = "Aplicacion " & App.EXEName.ToUpper & vbCr &
                   "=========================================" & vbCr &
                   "Versión incorrecta del programa." & vbCr &
                   "Versión Registrada: " & lVer & vbCr &
                   "Versión Actual: " & "(" & gsAppVersion & ")"
            mensaje = msg
        End If
        Return Resultado
    End Function

    Public Sub WrtSqlError(sql As String, Descrip As String)
        data = Nothing
        data = New RFLOG
        With data
            .USUARIO = Net.Dns.GetHostName()
            .PROGRAMA = Reflection.Assembly.GetExecutingAssembly.FullName
            .ALERT = "1"
            .EVENTO = Descrip
            .TXTSQL = sql
        End With
        GuardarRFLOGRow(data)
    End Sub

    Public Shared Function Code39(ByVal _code As String, Optional ByVal PrintTextInCode As Boolean = False, Optional ByVal Height As Single = 0, Optional ByVal GenerateChecksum As Boolean = False, Optional ByVal ChecksumText As Boolean = False) As Bitmap
        If _code.Trim = "" Then
            Return Nothing
        Else
            Dim barcode As New Barcode39
            barcode.StartStopText = True
            barcode.GenerateChecksum = GenerateChecksum
            barcode.ChecksumText = ChecksumText

            If Height <> 0 Then barcode.BarHeight = Height
            barcode.Code = _code
            Try
                Dim bm As New Bitmap(barcode.CreateDrawingImage(Color.Black, Color.White))
                If PrintTextInCode = False Then
                    Return bm
                Else
                    Dim bmT As Image
                    bmT = New Bitmap(bm.Width, bm.Height + 14)
                    Dim g As Graphics = Graphics.FromImage(bmT)
                    g.FillRectangle(New SolidBrush(Color.White), 0, 0, bm.Width, bm.Height + 14)

                    Dim drawFont As New Font("Arial", 8)
                    Dim drawBrush As New SolidBrush(Color.Black)

                    Dim stringSize As New SizeF
                    stringSize = g.MeasureString(_code, drawFont)
                    Dim xCenter As Single = (bm.Width - stringSize.Width) / 2
                    Dim x As Single = xCenter
                    Dim y As Single = bm.Height

                    Dim drawFormat As New StringFormat
                    drawFormat.FormatFlags = StringFormatFlags.NoWrap

                    g.DrawImage(bm, 0, 0)
                    g.DrawString(_code, drawFont, drawBrush, x, y, drawFormat)

                    Return bmT
                End If

            Catch ex As Exception
                Throw New Exception("Error generating code39 barcode. Desc:" & ex.Message)
            End Try
        End If
    End Function

    Public Function ConvertToByteArray(ByVal value As Bitmap) As Byte()
        Dim bitmapBytes As Byte()

        Using stream As New MemoryStream
            Try
                value.Save(stream, Imaging.ImageFormat.Jpeg)
            Catch ex As Exception

            End Try
            bitmapBytes = stream.ToArray
        End Using
        Return bitmapBytes

    End Function

#End Region

#Region "GUARDAR"

    ''' <summary>
    ''' Guarda RFLOG
    ''' </summary>
    ''' <param name="datos">RFLOG</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 08/06/2016</remarks>
    Public Function GuardarRFLOGRow(ByVal datos As RFLOG) As Boolean
        Dim continuarguardando As Boolean = False
        fSql = "INSERT INTO RFLOG (USUARIO, OPERACION, PROGRAMA, EVENTO, TXTSQL, ALERT)"
        fSql &= " VALUES ('" & datos.USUARIO & "', '" & datos.OPERACION & "', '" & datos.PROGRAMA & "', '" & datos.EVENTO.Replace("'", "''") & "', '" & datos.TXTSQL.Replace("'", "''") & "', " & datos.ALERT & ")"

        command = New SqlCommand
        command.Connection = conexion
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As SqlException
            mensaje = ex.Message
        Catch ex As Exception
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End If
        End Try
        Return continuarguardando
    End Function

#End Region

End Class
