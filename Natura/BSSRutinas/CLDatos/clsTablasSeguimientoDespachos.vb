﻿Public Class Transp_Seguimiento
    Public Pedido As String                         'Double
    Public CodTransportadora As String              'Double
    Public Transportadora As String                 'VarWChar
    Public FechaDespacho As String                  'VarWChar
    Public TipoDespacho As String                   'VarWChar
    Public CuentaContableGERA As String             'VarWChar
    Public DescripcionCuentaGERA As String          'VarWChar
    Public CentroCostoSAP As String                 'VarWChar
    Public CuentaContableSAP As String              'VarWChar
    Public NumeroGuia As String                     'VarWChar
    Public CodigoConsultora As String               'Double
    Public Destinatario As String                   'VarWChar
    Public Direccion As String                      'VarWChar
    Public CiudadDestino As String                  'VarWChar
    Public Departamento As String                   'VarWChar
    Public CodigoDaneCiudad As String               'VarWChar
    Public Gerencia As String                       'VarWChar
    Public Sector As String                         'VarWChar
    Public Celular As String                        'VarWChar
    Public Telefono As String                       'VarWChar
    Public Cajas As String                          'Double
    Public Regional As String                       'VarWChar
    Public TiempoEntrega As String                  'Double
    Public Estado As String                         'VarWChar
    Public FechaPrevistaEntrega As String           'VarWChar
    Public DiasAtraso As String                     'Double
    Public FechaEntrega As String                   'VarWChar
    Public Cumplimiento As String                   'Double
    Public TipoNovedad As String                    'VarWChar
    Public FechaNovedad As String                   'VarWChar
    Public Observacion As String                    'VarWChar
    Public Solucion As String                       'VarWChar
    Public Solucion1 As String                      'VarWChar
    Public Tarifas As String                        'VarWChar

    Dim tipoDato As New Dictionary(Of String, String)
    Dim pair As KeyValuePair(Of String, String)

    Public Sub New()

        With tipoDato
            .Add("Pedido", "")
            .Add("CodTransportadora", "")
            .Add("Transportadora", "")
            .Add("FechaDespacho", "")
            .Add("TipoDespacho", "")
            .Add("CuentaContableGERA", "")
            .Add("DescripcionCuentaGERA", "")
            .Add("CentroCostoSAP", "")
            .Add("CuentaContableSAP", "")
            .Add("NumeroGuia", "")
            .Add("CodigoConsultora", "")
            .Add("Destinatario", "")
            .Add("Direccion", "")
            .Add("CiudadDestino", "")
            .Add("Departamento", "")
            .Add("CodigoDaneCiudad", "")
            .Add("Gerencia", "")
            .Add("Sector", "")
            .Add("Celular", "")
            .Add("Telefono", "")
            .Add("Cajas", "")
            .Add("Regional", "")
            .Add("TiempoEntrega", "")
            .Add("Estado", "")
            .Add("FechaPrevistaEntrega", "")
            .Add("DiasAtraso", "")
            .Add("FechaEntrega", "")
            .Add("Cumplimiento", "")
            .Add("TipoNovedad", "")
            .Add("FechaNovedad", "")
            .Add("Observacion", "")
            .Add("Solucion", "")
            .Add("Solucion1", "")
            .Add("Tarifas", "")
        End With
        Inicializa()

    End Sub


    Function Insert() As String
        Dim sql As New clsMakeSQL

        With sql
            .InsertInto("Transp_Seguimiento")
            .Values("Pedido", Pedido, "")
            .Values("CodTransportadora", CodTransportadora, "")
            .Values("Transportadora", Transportadora, "'")
            .Values("FechaDespacho", FechaDespacho, "'")
            .Values("TipoDespacho", TipoDespacho, "'")
            .Values("CuentaContableGERA", CuentaContableGERA, "'")
            .Values("DescripcionCuentaGERA", DescripcionCuentaGERA, "'")
            .Values("CentroCostoSAP", CentroCostoSAP, "'")
            .Values("CuentaContableSAP", CuentaContableSAP, "'")
            .Values("NumeroGuia", NumeroGuia, "'")
            .Values("CodigoConsultora", CodigoConsultora, "")
            .Values("Destinatario", Destinatario, "'")
            .Values("Direccion", Direccion, "'")
            .Values("CiudadDestino", CiudadDestino, "'")
            .Values("Departamento", Departamento, "'")
            .Values("CodigoDaneCiudad", CodigoDaneCiudad, "'")
            .Values("Gerencia", Gerencia, "'")
            .Values("Sector", Sector, "'")
            .Values("Celular", Celular, "'")
            .Values("Telefono", Telefono, "'")
            .Values("Cajas", Cajas, "")
            .Values("Regional", Regional, "'")
            .Values("TiempoEntrega", TiempoEntrega, "")
            .Values("Estado", Estado, "'")
            .Values("FechaPrevistaEntrega", FechaPrevistaEntrega, "'")
            .Values("DiasAtraso", DiasAtraso, "")
            .Values("FechaEntrega", FechaEntrega, "'")
            .Values("Cumplimiento", Cumplimiento, "")
            .Values("TipoNovedad", TipoNovedad, "'")
            .Values("FechaNovedad", FechaNovedad, "'")
            .Values("Observacion", Observacion, "'")
            .Values("Solucion", Solucion, "'")
            .Values("Solucion1", Solucion1, "'")
            .Values("Tarifas", Tarifas, "'")
            Return .getSQL()
        End With

    End Function

    Function Update(lstWhere As List(Of lstWhere)) As String
        Dim sql As New clsMakeSQL
        Dim condiWhere = From condi In lstWhere

        With sql
            .Update("Transp_Seguimiento")
            .Values("Pedido", Pedido, "")
            .Values("CodTransportadora", CodTransportadora, "")
            .Values("Transportadora", Transportadora, "'")
            .Values("FechaDespacho", FechaDespacho, "'")
            .Values("TipoDespacho", TipoDespacho, "'")
            .Values("CuentaContableGERA", CuentaContableGERA, "'")
            .Values("DescripcionCuentaGERA", DescripcionCuentaGERA, "'")
            .Values("CentroCostoSAP", CentroCostoSAP, "'")
            .Values("CuentaContableSAP", CuentaContableSAP, "'")
            .Values("NumeroGuia", NumeroGuia, "'")
            .Values("CodigoConsultora", CodigoConsultora, "")
            .Values("Destinatario", Destinatario, "'")
            .Values("Direccion", Direccion, "'")
            .Values("CiudadDestino", CiudadDestino, "'")
            .Values("Departamento", Departamento, "'")
            .Values("CodigoDaneCiudad", CodigoDaneCiudad, "'")
            .Values("Gerencia", Gerencia, "'")
            .Values("Sector", Sector, "'")
            .Values("Celular", Celular, "'")
            .Values("Telefono", Telefono, "'")
            .Values("Cajas", Cajas, "")
            .Values("Regional", Regional, "'")
            .Values("TiempoEntrega", TiempoEntrega, "")
            .Values("Estado", Estado, "'")
            .Values("FechaPrevistaEntrega", FechaPrevistaEntrega, "'")
            .Values("DiasAtraso", DiasAtraso, "")
            .Values("FechaEntrega", FechaEntrega, "'")
            .Values("Cumplimiento", Cumplimiento, "")
            .Values("TipoNovedad", TipoNovedad, "'")
            .Values("FechaNovedad", FechaNovedad, "'")
            .Values("Observacion", Observacion, "'")
            .Values("Solucion", Solucion, "'")
            .Values("Solucion1", Solucion1, "'")
            .Values("Tarifas", Tarifas, "'")
            For Each condi In condiWhere
                .Where(condi.Campo, condi.Valor, condi.Tipo)
            Next
            Return .getSQL()
        End With

    End Function

    Function Delete(lstWhere As List(Of lstWhere)) As String
        Dim sql As New clsMakeSQL
        Dim condiWhere = From condi In lstWhere

        With sql
            .Delete("Transp_Seguimiento")
            For Each condi In condiWhere
                .Where(condi.Campo, condi.Valor, condi.Tipo)
            Next
            Return .getSQL()
        End With

    End Function

    Public Sub Inicializa()
        Pedido = ""
        CodTransportadora = ""
        Transportadora = ""
        FechaDespacho = ""
        TipoDespacho = ""
        CuentaContableGERA = ""
        DescripcionCuentaGERA = ""
        CentroCostoSAP = ""
        CuentaContableSAP = ""
        NumeroGuia = ""
        CodigoConsultora = ""
        Destinatario = ""
        Direccion = ""
        CiudadDestino = ""
        Departamento = ""
        CodigoDaneCiudad = ""
        Gerencia = ""
        Sector = ""
        Celular = ""
        Telefono = ""
        Cajas = ""
        Regional = ""
        TiempoEntrega = ""
        Estado = ""
        FechaPrevistaEntrega = ""
        DiasAtraso = ""
        FechaEntrega = ""
        Cumplimiento = ""
        TipoNovedad = ""
        FechaNovedad = ""
        Observacion = ""
        Solucion = ""
        Solucion1 = ""
        Tarifas = ""
    End Sub
End Class
