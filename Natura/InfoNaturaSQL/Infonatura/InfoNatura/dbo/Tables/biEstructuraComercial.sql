﻿CREATE TABLE [dbo].[biEstructuraComercial](
	[codSector] [int] NOT NULL,
	[Cargo] [nvarchar](10) NULL,
	[codGV] [int] NULL,
	[Gerencia] [nvarchar](255) NULL,
	[Sector] [nvarchar](255) NULL,
	[OndaDeCierre] [int] NULL,
	[codResponsable] [int] NULL,
	[NombreResponsable] [nvarchar](255) NULL,
	[CorreoCorporativo] [nvarchar](255) NULL,
	[Empresa] [nvarchar](255) NULL,
	[porcPoblacion] [float] NULL,
	[CorreoBackup] [nvarchar](255) NULL,
	[CorreoTemporal] [nvarchar](255) NULL,
	[GV_AA] [nvarchar](255) NULL,
	[DireccionDeEntrega] [nvarchar](255) NULL,
	[Telefono] [nvarchar](50) NULL,
	[CiudadResidencia] [nvarchar](150) NULL,
	[CiudadBaseSector] [nvarchar](150) NULL,
	[CumpleA] [nvarchar](50) NULL,
	[Observaciones] [nvarchar](255) NULL,
	[DifBonoCartera] [smallint] NULL,
	[CNo] [smallint] NULL,
	[GRs] [smallint] NULL,
	[PorcBonoMorosidad42] [float] NULL,
	[MultipUtilizado] [float] NULL,
	[ParaPagoPuntosInac3] [float] NULL,
 CONSTRAINT [PK_biEstucturaComercial] PRIMARY KEY CLUSTERED 
(
	[codSector] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]