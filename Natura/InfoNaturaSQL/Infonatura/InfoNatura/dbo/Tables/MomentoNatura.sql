﻿CREATE TABLE [dbo].[MomentoNatura](
	[IdMomentoNat] [int] IDENTITY(1,1) NOT NULL,
	[Dispositivo] [nvarchar](50) NULL,
	[TamPantalla] [nvarchar](50) NULL,
	[Navegador] [nvarchar](50) NULL,
	[Ip] [nvarchar](20) NULL,
	[Fecha] [datetime] NULL,
	[FechaUpd] [datetime] NULL,
	[Finalizado] [nvarchar](2) NULL,
 CONSTRAINT [PK__MomentoN__81421E7F6F8A7843] PRIMARY KEY CLUSTERED 
(
	[IdMomentoNat] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF__MomentoNa__Fecha__7172C0B5]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[MomentoNatura] ADD  CONSTRAINT [DF__MomentoNa__Fecha__7172C0B5]  DEFAULT (getdate()) FOR [Fecha]
GO
/****** Object:  Default [DF__MomentoNa__Final__7266E4EE]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[MomentoNatura] ADD  CONSTRAINT [DF__MomentoNa__Final__7266E4EE]  DEFAULT (N'NO') FOR [Finalizado]