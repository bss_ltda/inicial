﻿CREATE TABLE [dbo].[CTCampResultC](
	[IdCTCampResultC] [int] IDENTITY(1,1) NOT NULL,
	[IdCTCampaign] [int] NULL,
	[CodGer] [int] NULL,
	[Gerencia] [nvarchar](255) NULL,
	[CodSector] [int] NOT NULL,
	[Sector] [nvarchar](255) NULL,
	[Grupo] [float] NULL,
	[Nombre] [nvarchar](255) NULL,
	[Dsiponibles] [nvarchar](255) NULL,
	[NumActivas] [int] NULL,
	[RepiqueGrupoDirecto] [int] NULL,
	[RepiqueCNOF] [int] NULL,
	[Nivel] [nvarchar](255) NULL,
	[Actividad] [float] NULL,
	[RepiqueTotal] [float] NULL,
	[PorcActividadGer] [float] NULL,
	[PorcRepiqueGer] [float] NULL,
	[PorcRepiqueCalc] [float] NULL,
	[CumpleActividad] [nchar](10) NULL,
	[CumpleRepique] [nchar](10) NULL,
	[GanaCamp] [nchar](10) NULL,
	[Color] [nchar](50) NULL,
 CONSTRAINT [PK_CTCampResultC] PRIMARY KEY CLUSTERED 
(
	[IdCTCampResultC] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]