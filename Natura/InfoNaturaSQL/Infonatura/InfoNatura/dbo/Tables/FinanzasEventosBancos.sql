﻿CREATE TABLE [dbo].[FinanzasEventosBancos](
	[PERIODO] [nchar](6) NULL,
	[DOCUMENT_DATE] [nchar](8) NULL,
	[RECEIVER] [nvarchar](50) NULL,
	[ORDEN] [smallint] NULL,
	[TIPO_BAJA] [nvarchar](50) NULL,
	[FACTOR] [smallint] NULL,
	[REFERENCIA] [nvarchar](50) NULL,
	[RECEIVER_BALANCE] [float] NULL,
	[SALDO_REC] [float] NULL,
	[IMPORTE_MD] [float] NULL,
	[PENALTY_INTEREST_BALANCE] [float] NULL,
	[MULTA_INT] [float] NULL,
	[TAX1] [float] NULL,
	[IMPUESTO1] [float] NULL,
	[STS1] [bit] NOT NULL,
	[STS2] [bit] NOT NULL,
	[STS3] [bit] NOT NULL,
	[STS4] [bit] NOT NULL,
	[STS5] [bit] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Default [DF_FinanzasEventosBancos_FACTOR]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasEventosBancos] ADD  CONSTRAINT [DF_FinanzasEventosBancos_FACTOR]  DEFAULT ((1)) FOR [FACTOR]
GO
/****** Object:  Default [DF_FinanzasEventosBancos_STS1]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasEventosBancos] ADD  CONSTRAINT [DF_FinanzasEventosBancos_STS1]  DEFAULT ((0)) FOR [STS1]
GO
/****** Object:  Default [DF_FinanzasEventosBancos_STS2]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasEventosBancos] ADD  CONSTRAINT [DF_FinanzasEventosBancos_STS2]  DEFAULT ((0)) FOR [STS2]
GO
/****** Object:  Default [DF_FinanzasEventosBancos_STS3]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasEventosBancos] ADD  CONSTRAINT [DF_FinanzasEventosBancos_STS3]  DEFAULT ((0)) FOR [STS3]
GO
/****** Object:  Default [DF_FinanzasEventosBancos_STS4]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasEventosBancos] ADD  CONSTRAINT [DF_FinanzasEventosBancos_STS4]  DEFAULT ((0)) FOR [STS4]
GO
/****** Object:  Default [DF_FinanzasEventosBancos_STS5]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasEventosBancos] ADD  CONSTRAINT [DF_FinanzasEventosBancos_STS5]  DEFAULT ((0)) FOR [STS5]