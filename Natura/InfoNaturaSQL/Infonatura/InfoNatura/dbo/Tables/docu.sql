﻿CREATE TABLE [dbo].[docu](
	[docApp] [nchar](30) NULL,
	[docMod] [nchar](30) NULL,
	[docClave] [nvarchar](50) NOT NULL,
	[docOp] [nchar](50) NOT NULL,
	[docTitulo] [nvarchar](50) NULL,
	[docDescripcion] [nvarchar](500) NULL,
	[docEmbebido] [nvarchar](250) NULL,
	[docTipo] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_docu] PRIMARY KEY CLUSTERED 
(
	[docClave] ASC,
	[docOp] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_docu_docTipo]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[docu] ADD  CONSTRAINT [DF_docu_docTipo]  DEFAULT ('document') FOR [docTipo]