﻿CREATE TABLE [dbo].[DespachosPlanillaCopia](
	[IdUnico] [int] IDENTITY(1,1) NOT NULL,
	[Id ] [float] NULL,
	[EMBALAJE ] [nvarchar](255) NULL,
	[NoCaja] [float] NULL,
	[CantCaja] [float] NULL,
	[Numpedido] [float] NULL,
	[CodigoCN] [float] NULL,
	[NombreCN] [nvarchar](255) NULL,
	[Direccion] [nvarchar](255) NULL,
	[Celular] [nvarchar](255) NULL,
	[Telefono] [nvarchar](255) NULL,
	[Ciudad] [nvarchar](255) NULL,
	[Departamento] [nvarchar](255) NULL,
	[Fecha] [datetime] NULL,
	[NumMinuta] [float] NULL,
	[Peso] [float] NULL,
	[Transportadora] [nvarchar](255) NULL,
	[Estiba] [nvarchar](255) NULL,
	[Estado1] [nvarchar](255) NULL,
	[Estado2] [nvarchar](255) NULL,
	[Estado3] [nvarchar](255) NULL,
	[Estado4] [nvarchar](255) NULL,
	[Estado5] [nvarchar](255) NULL,
 CONSTRAINT [PK_DespachosPlanillaCopia] PRIMARY KEY CLUSTERED 
(
	[IdUnico] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF__Despachos__Fecha__0C06BB60]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlanillaCopia] ADD  DEFAULT (getdate()) FOR [Fecha]