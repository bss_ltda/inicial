﻿CREATE TABLE [dbo].[RMAILX](
	[LID] [bigint] IDENTITY(1,1) NOT NULL,
	[LMOD] [nvarchar](25) NULL,
	[LREF] [nvarchar](50) NULL,
	[LASUNTO] [nvarchar](100) NULL,
	[LENVIA] [nvarchar](150) NULL,
	[LPARA] [nvarchar](2000) NULL,
	[LCOPIA] [nvarchar](2000) NULL,
	[LBCC] [nvarchar](150) NULL,
	[LCUERPO] [nvarchar](max) NULL,
	[LADJUNTOS] [nvarchar](max) NULL,
	[LURL] [nvarchar](2000) NULL,
	[LTITURL] [nvarchar](250) NULL,
	[LENVIADO] [decimal](1, 0) NOT NULL,
	[LSTS01] [decimal](1, 0) NOT NULL,
	[LSTS02] [decimal](1, 0) NOT NULL,
	[LSTS03] [decimal](1, 0) NOT NULL,
	[LSTS04] [decimal](1, 0) NOT NULL,
	[LSTS05] [decimal](1, 0) NOT NULL,
	[LCRTDAT] [datetime] NOT NULL,
	[LUPDDAT] [datetime] NULL,
	[LMSGERR] [nvarchar](max) NULL,
	[LKEY] [nvarchar](50) NULL,
	[LCATALOGO] [nchar](20) NULL,
	[LSMTPHOST] [nvarchar](250) NULL,
	[LSMTPUSERNAME] [nvarchar](250) NULL,
	[LSMTPPASSWORD] [nvarchar](250) NULL,
	[LFPROXIMA] [datetime] NULL,
	[LSQL] [nvarchar](max) NULL,
	[LNUMREENVIO] [smallint] NULL,
 CONSTRAINT [PK__RMAILX__C65557210B129727] PRIMARY KEY CLUSTERED 
(
	[LID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_RMAILX_LENVIADO_1]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RMAILX] ADD  CONSTRAINT [DF_RMAILX_LENVIADO_1]  DEFAULT ((0)) FOR [LENVIADO]
GO
/****** Object:  Default [DF_RMAILX_LSTS01]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RMAILX] ADD  CONSTRAINT [DF_RMAILX_LSTS01]  DEFAULT ((0)) FOR [LSTS01]
GO
/****** Object:  Default [DF_RMAILX_LSTS02]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RMAILX] ADD  CONSTRAINT [DF_RMAILX_LSTS02]  DEFAULT ((0)) FOR [LSTS02]
GO
/****** Object:  Default [DF_RMAILX_LSTS03]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RMAILX] ADD  CONSTRAINT [DF_RMAILX_LSTS03]  DEFAULT ((0)) FOR [LSTS03]
GO
/****** Object:  Default [DF_RMAILX_LSTS04]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RMAILX] ADD  CONSTRAINT [DF_RMAILX_LSTS04]  DEFAULT ((0)) FOR [LSTS04]
GO
/****** Object:  Default [DF_RMAILX_LSTS05]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RMAILX] ADD  CONSTRAINT [DF_RMAILX_LSTS05]  DEFAULT ((0)) FOR [LSTS05]
GO
/****** Object:  Default [DF_RMAILX_LCRTDAT]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RMAILX] ADD  CONSTRAINT [DF_RMAILX_LCRTDAT]  DEFAULT (getdate()) FOR [LCRTDAT]
GO
/****** Object:  Default [DF_RMAILX_LUPDDAT]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RMAILX] ADD  CONSTRAINT [DF_RMAILX_LUPDDAT]  DEFAULT (getdate()) FOR [LUPDDAT]
GO
/****** Object:  Default [DF_RMAILX_LNUMREENVIO]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RMAILX] ADD  CONSTRAINT [DF_RMAILX_LNUMREENVIO]  DEFAULT ((0)) FOR [LNUMREENVIO]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Fecha en la que se ejecutará el proceso de correo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'RMAILX', @level2type=N'COLUMN',@level2name=N'LFPROXIMA'