﻿CREATE TABLE [dbo].[FinanzasAcumY](
	[Tercero] [nvarchar](255) NULL,
	[CentroCosto] [nvarchar](255) NULL,
	[SubCentroCosto] [nvarchar](255) NULL,
	[Proyecto] [nvarchar](255) NULL,
	[AcumActualY] [float] NULL,
	[AcumPptoY] [float] NULL,
	[AcumAnteriorY] [float] NULL,
	[AcumROY] [float] NULL
) ON [PRIMARY]