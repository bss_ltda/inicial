﻿CREATE TABLE [dbo].[TransportadorasFletes](
	[Transportadora] [nvarchar](255) NULL,
	[Cod Mun] [nvarchar](255) NULL,
	[Cod Dep] [nvarchar](255) NULL,
	[Departamento] [nvarchar](255) NULL,
	[Municipio] [nvarchar](255) NULL,
	[Regional] [nvarchar](255) NULL,
	[Ruta] [nvarchar](255) NULL,
	[Clase] [nvarchar](255) NULL,
	[Dias Promesa de entrega] [float] NULL,
	[Frecuencia] [nvarchar](255) NULL,
	[Tarifa de 1kg a 10kg Terrestre] [float] NULL,
	[Tarifa de Kilo adicional Terrestre] [float] NULL,
	[Tarifa de 1kg a 10kg1 Aereo] [float] NULL,
	[Tarifa de Kilo adicional1 Aereo] [float] NULL,
	[Cubrimiento] [nvarchar](255) NULL
) ON [PRIMARY]