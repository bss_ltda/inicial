﻿CREATE TABLE [dbo].[GeraPDF](
	[IdGeraPDF] [int] IDENTITY(1,1) NOT NULL,
	[CodigoPersona] [float] NULL,
	[Cedula_Ciudadania] [int] NULL,
	[Nombre] [nvarchar](90) NULL,
	[Fecha_Primer_Pedido] [datetime] NULL,
	[Tipo] [nvarchar](50) NULL,
	[FechaCreacion] [datetime] NULL,
	[FechaActualizacion] [datetime] NULL,
	[FechaProgramado] [datetime] NULL,
	[FechaEnviado] [datetime] NULL,
	[URL_PDF] [nvarchar](200) NULL,
	[Mensaje] [nvarchar](300) NULL,
	[STS01] [smallint] NOT NULL,
	[STS02] [smallint] NOT NULL,
	[STS03] [smallint] NOT NULL,
	[STS04] [smallint] NOT NULL,
	[STS05] [smallint] NOT NULL,
	[Correo] [nvarchar](80) NULL,
	[IP] [nvarchar](20) NULL,
	[Navegador] [nvarchar](50) NULL,
	[Dispositivo] [nvarchar](50) NULL,
	[TamPantalla] [nvarchar](50) NULL,
	[FechaFinaliza] [datetime] NULL,
 CONSTRAINT [PK_GeraPDF] PRIMARY KEY CLUSTERED 
(
	[IdGeraPDF] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_GeraPDF_STS01]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[GeraPDF] ADD  CONSTRAINT [DF_GeraPDF_STS01]  DEFAULT ((0)) FOR [STS01]
GO
/****** Object:  Default [DF_GeraPDF_STS02]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[GeraPDF] ADD  CONSTRAINT [DF_GeraPDF_STS02]  DEFAULT ((0)) FOR [STS02]
GO
/****** Object:  Default [DF_GeraPDF_STS03]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[GeraPDF] ADD  CONSTRAINT [DF_GeraPDF_STS03]  DEFAULT ((0)) FOR [STS03]
GO
/****** Object:  Default [DF_GeraPDF_STS04]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[GeraPDF] ADD  CONSTRAINT [DF_GeraPDF_STS04]  DEFAULT ((0)) FOR [STS04]
GO
/****** Object:  Default [DF_GeraPDF_STS05]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[GeraPDF] ADD  CONSTRAINT [DF_GeraPDF_STS05]  DEFAULT ((0)) FOR [STS05]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'En Proceso' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GeraPDF', @level2type=N'COLUMN',@level2name=N'STS01'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Creado' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GeraPDF', @level2type=N'COLUMN',@level2name=N'STS02'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Deuda Vencida' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GeraPDF', @level2type=N'COLUMN',@level2name=N'STS03'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Deuda Sin Vencer' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'GeraPDF', @level2type=N'COLUMN',@level2name=N'STS04'