﻿CREATE TABLE [dbo].[RCAAU](
	[AUNUMMNU] [bigint] NOT NULL,
	[AUNUMAPP] [decimal](5, 0) NOT NULL,
	[AUID] [decimal](3, 0) NOT NULL,
	[AUAPP] [nvarchar](15) NOT NULL,
	[AUMOD] [nvarchar](15) NOT NULL,
	[AUUSER] [nvarchar](25) NOT NULL,
	[AUIDMENU] [bigint] NOT NULL
) ON [PRIMARY]