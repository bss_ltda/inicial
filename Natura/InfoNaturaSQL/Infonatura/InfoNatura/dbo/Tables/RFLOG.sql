﻿CREATE TABLE [dbo].[RFLOG](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[OPERACION] [nvarchar](100) NULL,
	[FECHA] [datetime] NOT NULL,
	[USUARIO] [nvarchar](500) NULL,
	[PROGRAMA] [nvarchar](500) NULL,
	[EVENTO] [varchar](max) NULL,
	[TXTSQL] [varchar](max) NULL,
	[MENSAJE] [varchar](max) NULL,
	[ALERT] [decimal](1, 0) NOT NULL,
	[IDSESION] [decimal](8, 0) NULL,
	[FECHAWIN] [datetime] NULL,
	[ALERTTO] [nvarchar](150) NULL,
	[LKEY] [nchar](30) NULL,
	[IDPROCESO] [bigint] NOT NULL,
 CONSTRAINT [PK__RFLOG__3214EC272DD1C37F] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_RFLOG_FECHA]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RFLOG] ADD  CONSTRAINT [DF_RFLOG_FECHA]  DEFAULT (getdate()) FOR [FECHA]
GO
/****** Object:  Default [DF_RFLOG_ALERT]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RFLOG] ADD  CONSTRAINT [DF_RFLOG_ALERT]  DEFAULT ((0)) FOR [ALERT]
GO
/****** Object:  Default [DF_RFLOG_IDPROCESO]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RFLOG] ADD  CONSTRAINT [DF_RFLOG_IDPROCESO]  DEFAULT ((-1)) FOR [IDPROCESO]
GO
CREATE NONCLUSTERED INDEX [RFLOG_IDPROCESO] ON [dbo].[RFLOG] 
(
	[IDPROCESO] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]