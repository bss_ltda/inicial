﻿CREATE TABLE [dbo].[RequridoRqr](
	[Item] [nvarchar](255) NULL,
	[Articulo] [nvarchar](255) NULL,
	[Descripcion] [nvarchar](255) NULL,
	[Referencia  1] [nvarchar](255) NULL,
	[Referencia  2] [nvarchar](255) NULL,
	[Fecha de Expiracion] [float] NULL,
	[Case Number] [nvarchar](255) NULL,
	[Cajas] [float] NULL,
	[Unidades] [nvarchar](255) NULL,
	[Observaciones] [nvarchar](255) NULL,
	[Estado1] [nvarchar](255) NULL,
	[Estado2] [nvarchar](255) NULL
) ON [PRIMARY]