﻿CREATE TABLE [dbo].[fofiBaseLiquidacion](
	[IdBaseLiq] [bigint] IDENTITY(1,1) NOT NULL,
	[Ciclo] [float] NOT NULL,
	[GerenciaDeVentas] [nvarchar](255) NULL,
	[codSector] [float] NULL,
	[Sector] [nvarchar](255) NULL,
	[NombreResponsable] [nvarchar](255) NULL,
	[Cedula] [float] NULL,
	[CNOs] [float] NULL,
	[DispFinales] [float] NULL,
	[RangoDisponibles] [float] NULL,
	[porcPoblacion] [float] NULL,
	[Movilidad] [money] NULL,
	[ReunionDeCiclo] [money] NULL,
	[EncuentrosRefrigerio] [money] NULL,
	[Opcional01] [money] NULL,
	[Opcional02] [money] NULL,
	[Opcional03] [money] NULL,
	[Opcional04] [money] NULL,
	[Opcional05] [money] NULL,
	[Total ] [money] NULL,
 CONSTRAINT [PK_fofiBaseLiquidacion] PRIMARY KEY CLUSTERED 
(
	[IdBaseLiq] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]