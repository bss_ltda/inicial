﻿CREATE TABLE [dbo].[RevistasTracking](
	[NumeroGuia] [nvarchar](255) NOT NULL,
	[idTransportadora] [int] NOT NULL,
	[CodConsultora] [int] NOT NULL,
	[CodigoPostal] [nvarchar](255) NULL,
	[Ciudad] [nvarchar](255) NULL,
	[Depto] [nvarchar](255) NULL,
	[Direccion] [nvarchar](255) NULL,
	[Telefono] [nvarchar](255) NULL,
	[Celular] [nvarchar](255) NULL,
	[FechaDespacho] [nvarchar](255) NULL,
	[Ciclo] [nvarchar](255) NULL,
	[Estado] [nvarchar](255) NULL,
	[Novedad1] [nvarchar](255) NULL,
 CONSTRAINT [PK_RevistasTracking] PRIMARY KEY CLUSTERED 
(
	[NumeroGuia] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]