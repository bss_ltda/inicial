﻿CREATE TABLE [dbo].[EventosExcel](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Macro] [nchar](50) NOT NULL,
	[Sentencia] [varchar](500) NULL,
	[FechaEjecucion] [datetime] NULL,
	[Estado] [nchar](50) NULL
) ON [PRIMARY]