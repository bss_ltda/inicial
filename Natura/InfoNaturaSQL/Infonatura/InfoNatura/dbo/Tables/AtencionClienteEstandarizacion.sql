﻿CREATE TABLE [dbo].[AtencionClienteEstandarizacion](
	[Id] [bigint] NOT NULL,
	[Grupo] [nvarchar](255) NULL,
	[IdTipo] [int] NULL,
	[Tipo] [nvarchar](255) NULL,
	[MotivoReclamo] [nvarchar](1500) NULL,
	[MotivoReclamo2] [nvarchar](max) NULL,
	[Casuistica] [nvarchar](1500) NULL,
	[Casuistica2] [nvarchar](1500) NULL,
	[TiempoDeAtencion] [nvarchar](1500) NULL,
	[TenerEnCuenta] [nvarchar](1500) NULL,
	[PreguntasARealizar] [nvarchar](max) NULL,
	[PlantillaObservacionesGera] [nvarchar](max) NULL,
 CONSTRAINT [PK_AtencionClienteEstandarizacion] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]