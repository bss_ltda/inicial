﻿CREATE TABLE [dbo].[MarcosPlanilla](
	[CodConsultora] [nchar](30) NULL,
	[Nombre] [nvarchar](255) NULL,
	[Sector] [nchar](30) NULL,
	[SectorN] [nvarchar](255) NULL,
	[Gerencia] [nchar](30) NULL,
	[Onda] [nvarchar](255) NULL,
	[Correo] [nvarchar](255) NULL,
	[Telefono] [nvarchar](255) NULL,
	[Ingreso] [datetime] NULL,
	[Ano] [nvarchar](255) NULL,
	[GanoAnterios] [nvarchar](255) NULL,
	[PuntajeHabilitador] [float] NULL,
	[MarcoGanar] [nvarchar](255) NULL,
	[FacturacionAcum] [money] NULL,
	[FaturacionNece] [money] NULL,
	[FacturacionFalta] [money] NULL,
	[PromFacturacion] [money] NULL,
	[PuntosActual] [float] NULL,
	[PuntosAnteriores] [float] NULL,
	[SumPuntos] [float] NULL,
	[Status] [nvarchar](255) NULL,
	[ConceptoNecesario] [nvarchar](255) NULL,
	[UltimoConcepto] [nvarchar](255) NULL,
	[Ciclo] [float] NULL,
	[Notas1] [nvarchar](255) NULL,
	[Notas2] [nvarchar](255) NULL,
	[Notas3] [nvarchar](255) NULL,
	[Notas4] [nvarchar](255) NULL,
	[Notas5] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Default [DF_MarcosPlanilla_FacturacionAcum]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[MarcosPlanilla] ADD  CONSTRAINT [DF_MarcosPlanilla_FacturacionAcum]  DEFAULT ((0)) FOR [FacturacionAcum]
GO
/****** Object:  Default [DF_MarcosPlanilla_FaturacionNece]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[MarcosPlanilla] ADD  CONSTRAINT [DF_MarcosPlanilla_FaturacionNece]  DEFAULT ((0)) FOR [FaturacionNece]
GO
/****** Object:  Default [DF_MarcosPlanilla_FacturacionFalta]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[MarcosPlanilla] ADD  CONSTRAINT [DF_MarcosPlanilla_FacturacionFalta]  DEFAULT ((0)) FOR [FacturacionFalta]
GO
/****** Object:  Default [DF_MarcosPlanilla_PromFacturacion]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[MarcosPlanilla] ADD  CONSTRAINT [DF_MarcosPlanilla_PromFacturacion]  DEFAULT ((0)) FOR [PromFacturacion]
GO
/****** Object:  Default [DF_MarcosPlanilla_PuntosActual]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[MarcosPlanilla] ADD  CONSTRAINT [DF_MarcosPlanilla_PuntosActual]  DEFAULT ((0)) FOR [PuntosActual]
GO
/****** Object:  Default [DF_MarcosPlanilla_PuntosAnteriores]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[MarcosPlanilla] ADD  CONSTRAINT [DF_MarcosPlanilla_PuntosAnteriores]  DEFAULT ((0)) FOR [PuntosAnteriores]