﻿CREATE TABLE [dbo].[CTCampaignManagement](
	[IdCTCampaignManagement] [int] IDENTITY(1,1) NOT NULL,
	[IdCTCampaign] [int] NOT NULL,
	[Codigo] [nchar](10) NOT NULL,
	[Gerencia] [nvarchar](50) NOT NULL,
	[PorcentajeActividad] [smallint] NULL,
	[PorcentajeRepique] [smallint] NULL,
 CONSTRAINT [PK_CTCampaignManagement] PRIMARY KEY CLUSTERED 
(
	[IdCTCampaignManagement] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_CTCampaignManagement_CTCampaign]    Script Date: 09/13/2018 18:30:19 ******/
ALTER TABLE [dbo].[CTCampaignManagement]  WITH CHECK ADD  CONSTRAINT [FK_CTCampaignManagement_CTCampaign] FOREIGN KEY([IdCTCampaign])
REFERENCES [dbo].[CTCampaign] ([IdCTCampaign])
GO

ALTER TABLE [dbo].[CTCampaignManagement] CHECK CONSTRAINT [FK_CTCampaignManagement_CTCampaign]