﻿CREATE TABLE [dbo].[RevistasDomina](
	[Guia] [nvarchar](255) NULL,
	[Cuenta] [nvarchar](255) NULL,
	[Destinatario] [nvarchar](255) NULL,
	[Complemento] [nvarchar](255) NULL,
	[Direccion] [nvarchar](255) NULL,
	[Destino] [nvarchar](255) NULL,
	[DeptoDest] [nvarchar](255) NULL,
	[Posicion] [nvarchar](255) NULL,
	[Estado] [nvarchar](255) NULL,
	[FechaGestion] [nvarchar](255) NULL
) ON [PRIMARY]