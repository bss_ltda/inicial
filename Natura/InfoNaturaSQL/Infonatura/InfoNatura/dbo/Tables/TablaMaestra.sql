﻿CREATE TABLE [dbo].[TablaMaestra](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Tabla] [nchar](20) NOT NULL,
	[Codigo] [nchar](10) NOT NULL,
	[CodNum] [int] NOT NULL,
	[Descripcion] [nvarchar](50) NULL,
	[DescCorta] [nvarchar](20) NULL,
	[Nota1] [nvarchar](150) NULL,
	[Nota2] [nvarchar](150) NULL,
	[UsrCode1] [smallint] NOT NULL,
	[UsrCode2] [smallint] NOT NULL,
	[UsrCode3] [smallint] NOT NULL,
	[UsrCode4] [smallint] NOT NULL,
	[UsrCode5] [smallint] NOT NULL,
	[Fecha1] [datetime] NULL,
 CONSTRAINT [PK_TablaMaestra] PRIMARY KEY CLUSTERED 
(
	[Tabla] ASC,
	[Codigo] ASC,
	[CodNum] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_TablaMaestra_Codigo]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[TablaMaestra] ADD  CONSTRAINT [DF_TablaMaestra_Codigo]  DEFAULT ('') FOR [Codigo]
GO
/****** Object:  Default [DF_TablaMaestra_CodNum]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[TablaMaestra] ADD  CONSTRAINT [DF_TablaMaestra_CodNum]  DEFAULT ((0)) FOR [CodNum]
GO
/****** Object:  Default [DF_TablaMaestra_UsrCode1]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[TablaMaestra] ADD  CONSTRAINT [DF_TablaMaestra_UsrCode1]  DEFAULT ((0)) FOR [UsrCode1]
GO
/****** Object:  Default [DF_TablaMaestra_UsrCode2]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[TablaMaestra] ADD  CONSTRAINT [DF_TablaMaestra_UsrCode2]  DEFAULT ((0)) FOR [UsrCode2]
GO
/****** Object:  Default [DF_TablaMaestra_UsrCode3]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[TablaMaestra] ADD  CONSTRAINT [DF_TablaMaestra_UsrCode3]  DEFAULT ((0)) FOR [UsrCode3]
GO
/****** Object:  Default [DF_TablaMaestra_UsrCode4]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[TablaMaestra] ADD  CONSTRAINT [DF_TablaMaestra_UsrCode4]  DEFAULT ((0)) FOR [UsrCode4]
GO
/****** Object:  Default [DF_TablaMaestra_UsrCode5]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[TablaMaestra] ADD  CONSTRAINT [DF_TablaMaestra_UsrCode5]  DEFAULT ((0)) FOR [UsrCode5]