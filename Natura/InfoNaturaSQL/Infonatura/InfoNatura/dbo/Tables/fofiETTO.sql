﻿CREATE TABLE [dbo].[fofiETTO](
	[IdEtto] [bigint] IDENTITY(1,1) NOT NULL,
	[SPCCiclo] [float] NULL,
	[Ciclo] [float] NULL,
	[Gerencia] [nvarchar](255) NULL,
	[CodigoSector] [float] NULL,
	[Sector] [nvarchar](255) NULL,
	[Objetivo] [float] NULL,
	[Real] [float] NULL,
	[Tope] [float] NULL,
	[Observaciones] [nvarchar](255) NULL,
	[CRTUSR] [nvarchar](50) NULL,
	[CRTDAT] [datetime] NOT NULL,
	[UPDUSR] [nvarchar](50) NULL,
	[UPDDAT] [datetime] NULL,
 CONSTRAINT [PK_fofiETTO] PRIMARY KEY CLUSTERED 
(
	[IdEtto] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_fofiETTO_CRTDAT]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[fofiETTO] ADD  CONSTRAINT [DF_fofiETTO_CRTDAT]  DEFAULT (getdate()) FOR [CRTDAT]