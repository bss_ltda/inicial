﻿CREATE TABLE [dbo].[FinanzasEventos123](
	[IdEvento] [int] IDENTITY(1,1) NOT NULL,
	[Periodo] [nchar](6) NULL,
	[Fecha] [nchar](8) NULL,
	[Evento] [nvarchar](50) NULL,
	[Item] [nvarchar](50) NULL,
	[ValorGERA] [float] NOT NULL,
	[ValorSAP] [float] NOT NULL,
	[Diferencia] [float] NOT NULL,
	[Observacion] [nvarchar](500) NULL,
	[CRTDAT] [datetime] NOT NULL,
 CONSTRAINT [PK_FinanzasEventos123] PRIMARY KEY CLUSTERED 
(
	[IdEvento] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_FinanzasEventos123_ValorGERA]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasEventos123] ADD  CONSTRAINT [DF_FinanzasEventos123_ValorGERA]  DEFAULT ((0)) FOR [ValorGERA]
GO
/****** Object:  Default [DF_FinanzasEventos123_ValorSAP]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasEventos123] ADD  CONSTRAINT [DF_FinanzasEventos123_ValorSAP]  DEFAULT ((0)) FOR [ValorSAP]
GO
/****** Object:  Default [DF_FinanzasEventos123_Diferencia]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasEventos123] ADD  CONSTRAINT [DF_FinanzasEventos123_Diferencia]  DEFAULT ((0)) FOR [Diferencia]
GO
/****** Object:  Default [DF_FinanzasEventos123_CRTDAT]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasEventos123] ADD  CONSTRAINT [DF_FinanzasEventos123_CRTDAT]  DEFAULT (getdate()) FOR [CRTDAT]