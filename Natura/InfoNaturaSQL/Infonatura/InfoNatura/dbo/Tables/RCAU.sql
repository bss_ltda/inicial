﻿CREATE TABLE [dbo].[RCAU](
	[USTS] [nvarchar](1) NOT NULL,
	[UTIPO] [nvarchar](10) NOT NULL,
	[UUSR] [nvarchar](25) NOT NULL,
	[UUSRBOL] [nvarchar](10) NULL,
	[UUSRABR] [nvarchar](4) NULL,
	[UMOD] [nvarchar](15) NOT NULL,
	[UPLANT] [nvarchar](2) NULL,
	[UWHS] [nvarchar](2) NULL,
	[UGRP] [decimal](3, 0) NULL,
	[UTRA] [decimal](8, 0) NULL,
	[UVEND] [decimal](8, 0) NULL,
	[UPASS] [nvarchar](20) NOT NULL,
	[UNOM] [nvarchar](40) NOT NULL,
	[UEMAIL] [nvarchar](60) NOT NULL,
	[UFECCAD] [datetime] NULL,
	[UDIAS] [decimal](3, 0) NULL,
	[UUSRR1] [nvarchar](10) NULL,
	[UUSRR2] [nvarchar](10) NULL,
	[UUSRR3] [nvarchar](10) NULL,
	[UUSRR4] [nvarchar](10) NULL,
	[UUSRR5] [nvarchar](10) NULL,
	[UCATEG1] [nvarchar](10) NULL,
	[UCATEG2] [nvarchar](10) NULL,
	[UCATEG3] [nvarchar](10) NULL,
	[UCATEG4] [nvarchar](10) NULL,
	[UCATEG5] [nvarchar](10) NULL,
	[UUEN] [nvarchar](15) NULL,
	[UANILL] [nvarchar](100) NULL,
	[USFCNS] [nvarchar](100) NULL,
	[UBODS] [nvarchar](150) NULL,
	[UQRY] [nvarchar](max) NULL,
	[UTU] [decimal](3, 0) NULL,
	[UPRF400] [nchar](10) NULL,
	[UPERFIL] [nchar](15) NULL,
	[UTOKEN] [nvarchar](50) NULL,
 CONSTRAINT [PK_RCAU] PRIMARY KEY CLUSTERED 
(
	[UUSR] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_RCAU_UUSRBOL]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAU] ADD  CONSTRAINT [DF_RCAU_UUSRBOL]  DEFAULT ('') FOR [UUSRBOL]
GO
/****** Object:  Default [DF_RCAU_UUSRABR]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAU] ADD  CONSTRAINT [DF_RCAU_UUSRABR]  DEFAULT ('') FOR [UUSRABR]
GO
/****** Object:  Default [DF_RCAU_UPASS]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAU] ADD  CONSTRAINT [DF_RCAU_UPASS]  DEFAULT ('') FOR [UPASS]
GO
/****** Object:  Default [DF_RCAU_UNOM]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAU] ADD  CONSTRAINT [DF_RCAU_UNOM]  DEFAULT ('') FOR [UNOM]
GO
/****** Object:  Default [DF_RCAU_UEMAIL]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAU] ADD  CONSTRAINT [DF_RCAU_UEMAIL]  DEFAULT ('') FOR [UEMAIL]
GO
/****** Object:  Default [DF_RCAU_UTU]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAU] ADD  CONSTRAINT [DF_RCAU_UTU]  DEFAULT ((0)) FOR [UTU]