﻿CREATE TABLE [dbo].[CorreoConfig](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[IdCamp] [bigint] NULL,
	[IdAdjunto] [bigint] NULL,
	[Codigo] [nchar](50) NULL,
	[Nombre] [nvarchar](150) NULL,
	[Correo] [nvarchar](150) NULL,
	[Categ01] [nvarchar](50) NULL,
	[Categ02] [nvarchar](50) NULL,
	[Categ03] [nvarchar](50) NULL,
	[Titulo] [nvarchar](150) NULL,
	[Asunto] [nvarchar](150) NULL,
	[Referencia] [nvarchar](150) NULL,
	[Remitente] [nvarchar](150) NULL,
	[Enlace] [nvarchar](250) NULL,
	[Imagen] [nvarchar](250) NULL,
	[Cabecera] [nvarchar](max) NULL,
	[Cuerpo1] [nvarchar](max) NULL,
	[Cuerpo2] [nvarchar](max) NULL,
	[Pie1] [nvarchar](max) NULL,
	[Pie2] [nvarchar](max) NULL,
	[Pie3] [nvarchar](max) NULL,
	[Pie4] [nvarchar](max) NULL,
	[Plantilla] [nvarchar](50) NULL,
	[Mensaje] [nvarchar](max) NULL,
	[FechaEjecucion] [datetime] NULL,
	[Tarea] [bigint] NULL,
	[Sts1] [decimal](1, 0) NOT NULL,
	[Sts2] [decimal](1, 0) NOT NULL,
	[Sts3] [decimal](1, 0) NOT NULL,
	[Sts4] [decimal](1, 0) NOT NULL,
	[Sts5] [decimal](1, 0) NOT NULL,
 CONSTRAINT [PK_CorreoConfig] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_CorreoConfig_Sts1]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[CorreoConfig] ADD  CONSTRAINT [DF_CorreoConfig_Sts1]  DEFAULT ((0)) FOR [Sts1]
GO
/****** Object:  Default [DF_CorreoConfig_Sts11]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[CorreoConfig] ADD  CONSTRAINT [DF_CorreoConfig_Sts11]  DEFAULT ((0)) FOR [Sts2]
GO
/****** Object:  Default [DF_CorreoConfig_Sts11_1]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[CorreoConfig] ADD  CONSTRAINT [DF_CorreoConfig_Sts11_1]  DEFAULT ((0)) FOR [Sts3]
GO
/****** Object:  Default [DF_CorreoConfig_Sts12]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[CorreoConfig] ADD  CONSTRAINT [DF_CorreoConfig_Sts12]  DEFAULT ((0)) FOR [Sts4]
GO
/****** Object:  Default [DF_CorreoConfig_Sts13]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[CorreoConfig] ADD  CONSTRAINT [DF_CorreoConfig_Sts13]  DEFAULT ((0)) FOR [Sts5]