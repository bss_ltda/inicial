﻿CREATE TABLE [dbo].[Seguimiento](
	[IdSeguimiento] [int] IDENTITY(1,1) NOT NULL,
	[IdPedido] [varchar](50) NULL,
	[Seguimiento] [varchar](500) NULL,
	[Fecha] [varchar](50) NULL,
 CONSTRAINT [PK_Seguimiento] PRIMARY KEY CLUSTERED 
(
	[IdSeguimiento] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]