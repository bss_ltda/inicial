﻿CREATE TABLE [dbo].[AuditoriaNaturaSaveCSV](
	[IdProceso] [int] IDENTITY(1,1) NOT NULL,
	[FechaProceso] [datetime] NOT NULL,
	[UltimaFechaProceso] [datetime] NULL,
	[NombreArchivo] [varchar](1000) NOT NULL,
	[RutaArchivo] [varchar](2000) NOT NULL,
	[DescripcionProceso] [varchar](2000) NOT NULL,
	[Estado] [int] NOT NULL,
	[EstadoDescarga] [bit] NOT NULL,
	[EstadoCodificacion] [bit] NOT NULL,
	[EstadoBulk] [bit] NOT NULL,
	[ProcesoInterno] [bigint] NULL,
	[Categoria01] [nvarchar](50) NULL,
	[Categoria02] [nvarchar](50) NULL,
	[Categoria03] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdProceso] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_Estado_Auditoria]    Script Date: 09/13/2018 18:30:19 ******/
ALTER TABLE [dbo].[AuditoriaNaturaSaveCSV]  WITH CHECK ADD  CONSTRAINT [FK_Estado_Auditoria] FOREIGN KEY([Estado])
REFERENCES [dbo].[EstadoAuditoriaSaveCSV] ([IdEstado])
GO

ALTER TABLE [dbo].[AuditoriaNaturaSaveCSV] CHECK CONSTRAINT [FK_Estado_Auditoria]
GO
/****** Object:  Default [DF_AuditoriaNaturaSaveCSV_FAPROCNUM]    Script Date: 09/13/2018 18:30:19 ******/
ALTER TABLE [dbo].[AuditoriaNaturaSaveCSV] ADD  CONSTRAINT [DF_AuditoriaNaturaSaveCSV_FAPROCNUM]  DEFAULT ((0)) FOR [ProcesoInterno]