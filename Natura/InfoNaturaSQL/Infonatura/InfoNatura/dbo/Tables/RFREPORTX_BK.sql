﻿CREATE TABLE [dbo].[RFREPORTX_BK](
	[XID] [int] NOT NULL,
	[XURL] [varchar](1000) NULL,
	[XXLS] [varchar](1000) NULL,
	[XSQL] [varchar](8000) NULL,
	[XTIT] [varchar](8000) NULL,
	[XSTS] [smallint] NULL,
	[XMSG] [varchar](2000) NULL,
	[CRTUSR] [nchar](20) NULL,
	[CRTDAT] [timestamp] NULL
) ON [PRIMARY]