﻿CREATE TABLE [dbo].[DespachosPlanilla](
	[IdUnico] [int] IDENTITY(1,1) NOT NULL,
	[Id ] [float] NULL,
	[EMBALAJE ] [nvarchar](255) NULL,
	[NoCaja] [float] NULL,
	[CantCaja] [float] NULL,
	[Numpedido] [float] NULL,
	[CodigoCN] [float] NULL,
	[NombreCN] [nvarchar](255) NULL,
	[Direccion] [nvarchar](255) NULL,
	[Celular] [nvarchar](255) NULL,
	[Telefono] [nvarchar](255) NULL,
	[Ciudad] [nvarchar](255) NULL,
	[Departamento] [nvarchar](255) NULL,
	[Fecha] [datetime] NULL,
	[NumMinuta] [float] NULL,
	[Peso] [float] NULL,
	[Transportadora] [nvarchar](255) NULL,
	[Estiba] [nvarchar](255) NULL,
	[Estado1] [nvarchar](255) NULL,
	[Estado2] [nvarchar](255) NULL,
	[Estado3] [nvarchar](255) NULL,
	[Estado4] [nvarchar](255) NULL,
	[Estado5] [nvarchar](255) NULL,
	[TipoDeCaja] [nvarchar](50) NULL,
	[PesoFinal] [float] NULL,
	[PoliticaEntrega] [nvarchar](50) NULL,
	[FechaRegistro] [timestamp] NOT NULL,
 CONSTRAINT [PK_DespachosPlanilla] PRIMARY KEY CLUSTERED 
(
	[IdUnico] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF__Despachos__Fecha__546180BB]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlanilla] ADD  DEFAULT (getdate()) FOR [Fecha]
GO
/****** Object:  Default [DF_DespachosPlanilla_TipoDeCaja]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlanilla] ADD  CONSTRAINT [DF_DespachosPlanilla_TipoDeCaja]  DEFAULT ('NA') FOR [TipoDeCaja]
GO
/****** Object:  Default [DF_DespachosPlanilla_PesoFinal]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlanilla] ADD  CONSTRAINT [DF_DespachosPlanilla_PesoFinal]  DEFAULT ((0)) FOR [PesoFinal]
GO
/****** Object:  Default [DF_DespachosPlanilla_PoliticaEntrega]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlanilla] ADD  CONSTRAINT [DF_DespachosPlanilla_PoliticaEntrega]  DEFAULT ('NA') FOR [PoliticaEntrega]