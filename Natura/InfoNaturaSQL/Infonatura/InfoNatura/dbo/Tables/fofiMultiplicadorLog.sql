﻿CREATE TABLE [dbo].[fofiMultiplicadorLog](
	[idMult] [int] NOT NULL,
	[cargo] [nchar](2) NULL,
	[disp] [int] NOT NULL,
	[grupo] [int] NOT NULL,
	[multiplicador] [int] NULL,
	[fecha] [datetime] NULL
) ON [PRIMARY]