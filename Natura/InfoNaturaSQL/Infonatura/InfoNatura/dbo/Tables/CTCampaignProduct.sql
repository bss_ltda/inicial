﻿CREATE TABLE [dbo].[CTCampaignProduct](
	[IdCTCampaignProduct] [int] IDENTITY(1,1) NOT NULL,
	[IdCTCampaign] [int] NOT NULL,
	[CodigoProducto] [nchar](10) NOT NULL,
	[Descripcion] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_CTCampaignProduct] PRIMARY KEY CLUSTERED 
(
	[IdCTCampaignProduct] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_CTCampaignProduct_CTCampaign]    Script Date: 09/13/2018 18:30:19 ******/
ALTER TABLE [dbo].[CTCampaignProduct]  WITH CHECK ADD  CONSTRAINT [FK_CTCampaignProduct_CTCampaign] FOREIGN KEY([IdCTCampaign])
REFERENCES [dbo].[CTCampaign] ([IdCTCampaign])
GO

ALTER TABLE [dbo].[CTCampaignProduct] CHECK CONSTRAINT [FK_CTCampaignProduct_CTCampaign]