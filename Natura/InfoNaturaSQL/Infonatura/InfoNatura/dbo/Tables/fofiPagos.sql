﻿CREATE TABLE [dbo].[fofiPagos](
	[IdPago] [bigint] IDENTITY(1,1) NOT NULL,
	[AAAA] [decimal](4, 0) NULL,
	[Ciclo] [decimal](6, 0) NOT NULL,
	[Sector] [int] NOT NULL,
	[FechaPago] [date] NULL,
	[GerenciaVentas] [nvarchar](255) NOT NULL,
	[RegPorGerencia] [int] NULL,
	[SectorNombre] [nvarchar](255) NOT NULL,
	[SectorResponsable] [nvarchar](255) NOT NULL,
	[Cargo] [nchar](10) NULL,
	[DifBonoCartera] [decimal](1, 0) NULL,
	[CNOs] [decimal](3, 0) NULL,
	[DispFinalObj] [float] NULL,
	[DispFinalReal] [float] NULL,
	[ActivaObj] [float] NULL,
	[ActivaReal] [float] NULL,
	[Inactivas3Obj] [float] NULL,
	[Inactivas3Real] [float] NULL,
	[PorcCumplDisp] [float] NULL,
	[PorcCumplactivas] [float] NULL,
	[DifInact3] [float] NULL,
	[PuntosDis] [float] NULL,
	[PuntosAct] [float] NULL,
	[I3Menor200Disp] [float] NULL,
	[I3Menor400Disp] [float] NULL,
	[I3Mayor400Disp] [float] NULL,
	[PuntosInac3] [float] NULL,
	[PuntosTotales] [float] NULL,
	[MultipUtilizado] [float] NULL,
	[FranjaPremiacion] [float] NULL,
	[Pago] [float] NULL,
	[Morosidad42Dias] [float] NULL,
	[PorcBonoMorosidad42] [float] NULL,
	[Morosidad63DIAS] [float] NULL,
	[BonoCicloAnterior] [float] NULL,
	[PorcMorosidad42CicloAnterior] [float] NULL,
	[BonoMorosidad63] [float] NULL,
	[TotalGanado] [float] NULL,
	[TotalPagado] [float] NULL,
	[FranjaDisponibles] [nvarchar](255) NULL,
	[FranjaActivas] [nvarchar](255) NULL,
	[FranjaInactivas3] [nvarchar](255) NULL,
	[FranjaTotal] [nvarchar](255) NULL,
	[FranjaDispMultip] [nvarchar](255) NULL,
	[BonoMorosidad42d] [float] NULL,
	[ActividadObjC5] [nvarchar](255) NULL,
	[ActividadRealC5] [nvarchar](255) NULL,
	[BonoActivC5] [nvarchar](255) NULL,
	[Empresa] [nvarchar](50) NOT NULL,
	[Batch] [decimal](8, 0) NULL,
	[STS1] [decimal](1, 0) NULL,
	[STS2] [decimal](1, 0) NULL,
	[STS3] [decimal](1, 0) NULL,
	[STS4] [decimal](1, 0) NULL,
	[STS5] [decimal](1, 0) NULL,
	[Actualizado] [datetime] NULL,
	[Usuario] [nchar](25) NULL,
 CONSTRAINT [PK_fofiPagos_1] PRIMARY KEY CLUSTERED 
(
	[Ciclo] ASC,
	[Sector] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_fofiPagos_GerenciaVentas]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[fofiPagos] ADD  CONSTRAINT [DF_fofiPagos_GerenciaVentas]  DEFAULT (N'NO EXISTE') FOR [GerenciaVentas]
GO
/****** Object:  Default [DF_fofiPagos_Sector1]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[fofiPagos] ADD  CONSTRAINT [DF_fofiPagos_Sector1]  DEFAULT (N'NO EXISTE') FOR [SectorNombre]
GO
/****** Object:  Default [DF_fofiPagos_GerentedeRelaciones]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[fofiPagos] ADD  CONSTRAINT [DF_fofiPagos_GerentedeRelaciones]  DEFAULT (N'NO EXISTE') FOR [SectorResponsable]
GO
/****** Object:  Default [DF_fofiPagos_DifBonoCartera]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[fofiPagos] ADD  CONSTRAINT [DF_fofiPagos_DifBonoCartera]  DEFAULT ((0)) FOR [DifBonoCartera]
GO
/****** Object:  Default [DF_fofiPagos_CNOs]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[fofiPagos] ADD  CONSTRAINT [DF_fofiPagos_CNOs]  DEFAULT ((0)) FOR [CNOs]
GO
/****** Object:  Default [DF_fofiPagos_Empresa]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[fofiPagos] ADD  CONSTRAINT [DF_fofiPagos_Empresa]  DEFAULT (N'NO EXISTE') FOR [Empresa]