﻿CREATE TABLE [dbo].[MarcosConceptos](
	[Gerente] [float] NULL,
	[Sector] [float] NULL,
	[Sector1] [nvarchar](255) NULL,
	[Gerente2] [nvarchar](255) NULL,
	[Gerencia] [nvarchar](255) NULL,
	[Participa] [nvarchar](255) NULL,
	[Novedad] [nvarchar](255) NULL,
	[Concepto] [nvarchar](255) NULL,
	[Ciclo] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Default [DF_MarcosConceptos_Novedad]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[MarcosConceptos] ADD  CONSTRAINT [DF_MarcosConceptos_Novedad]  DEFAULT ('') FOR [Novedad]