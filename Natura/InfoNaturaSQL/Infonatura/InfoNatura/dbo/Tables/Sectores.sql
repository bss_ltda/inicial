﻿CREATE TABLE [dbo].[Sectores](
	[CodGerencia] [nchar](30) NOT NULL,
	[Sector] [nchar](30) NOT NULL,
	[SectorNombre] [nvarchar](255) NULL,
 CONSTRAINT [PK_Sectores] PRIMARY KEY CLUSTERED 
(
	[CodGerencia] ASC,
	[Sector] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]