﻿CREATE TABLE [dbo].[FinanzasComentarios](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Mes] [nvarchar](255) NULL,
	[Centro] [nvarchar](255) NULL,
	[Subentro] [nvarchar](255) NULL,
	[Proyecto] [nvarchar](255) NULL,
	[VarMensual] [float] NULL,
	[ComentariosMes] [nvarchar](255) NULL,
	[ComentariosAño] [nvarchar](255) NULL,
	[Usr] [nvarchar](255) NULL
) ON [PRIMARY]