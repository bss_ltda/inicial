﻿CREATE TABLE [dbo].[fofiMultiplicador](
	[idMult] [int] IDENTITY(1,1) NOT NULL,
	[cargo] [nchar](2) NULL,
	[disp] [int] NOT NULL,
	[grupo] [int] NOT NULL,
	[multiplicador] [int] NULL,
 CONSTRAINT [PK_fofiMultiplicador] PRIMARY KEY CLUSTERED 
(
	[idMult] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]