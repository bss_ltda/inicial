﻿CREATE TABLE [dbo].[geraCNOsW](
	[CodigoEstructuraPadre] [int] NULL,
	[EstructuraPadre] [nvarchar](255) NULL,
	[CodigoEstructura] [int] NULL,
	[Estructura] [nvarchar](255) NULL,
	[Grupo] [int] NOT NULL,
	[Codigo] [int] NULL,
	[ResponsableGrupo] [nvarchar](255) NULL,
	[Nivel] [nvarchar](255) NULL,
	[Disponibles] [nvarchar](255) NULL,
	[Activas] [nvarchar](255) NULL,
	[DisponiblesGrupoDirecto] [nvarchar](255) NULL,
	[DisponiblesGrupoCnof] [nvarchar](255) NULL,
	[ActivasGrupoDirecto] [nvarchar](255) NULL,
	[ActivasFrecuentesGrupoDirecto] [nvarchar](255) NULL,
	[ActivasGrupoCnof] [nvarchar](255) NULL,
	[PorcActividad] [nvarchar](255) NULL,
	[PorcActividadFrecuenteGrupoDirecto] [nvarchar](255) NULL,
	[SaldoDelGrupo] [nvarchar](255) NULL,
	[CantCNIFEnElGrupo] [nvarchar](255) NULL,
	[GananciaXDisponiblesActividad] [nvarchar](255) NULL,
	[GananciaXSaldoDelGrupo] [nvarchar](255) NULL,
	[ActivasGruposRelacionadosCNOs] [nvarchar](255) NULL,
	[GananciaGruposRelacionadosCNOs] [nvarchar](255) NULL,
	[BonoCno] [nvarchar](255) NULL,
	[TotalGanancia] [nvarchar](255) NULL,
	[Observaciones] [nvarchar](255) NULL,
	[GruposRelacionadosCNOs] [nvarchar](255) NULL,
	[GruposRelacionadosCNOFs] [nvarchar](255) NULL,
	[ProductividadXActivas] [nvarchar](255) NULL,
	[RecetaDelGrupo] [nvarchar](255) NULL,
 CONSTRAINT [PK_geraCNOsW] PRIMARY KEY CLUSTERED 
(
	[Grupo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]