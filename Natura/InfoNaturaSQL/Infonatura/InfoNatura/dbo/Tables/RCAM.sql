﻿CREATE TABLE [dbo].[RCAM](
	[ANUMREG] [bigint] IDENTITY(1,1) NOT NULL,
	[AID] [decimal](3, 0) NOT NULL,
	[ALVL] [decimal](3, 0) NULL,
	[AORD] [decimal](3, 0) NULL,
	[AAPP] [nvarchar](15) NOT NULL,
	[AMOD] [nvarchar](15) NOT NULL,
	[AGRP] [decimal](3, 0) NULL,
	[APARENT] [decimal](3, 0) NOT NULL,
	[ATIT] [nvarchar](100) NOT NULL,
	[ALINK] [nvarchar](100) NULL,
	[ACLASS] [nvarchar](20) NULL,
	[AISET] [nvarchar](100) NULL,
	[APARM1] [nvarchar](10) NULL,
	[APARM2] [nvarchar](10) NULL,
	[APARM3] [nvarchar](10) NULL,
	[APARM4] [nvarchar](10) NULL,
	[APARM5] [nvarchar](10) NULL,
	[ADSPORD] [decimal](3, 0) NULL,
	[ATABS] [decimal](3, 0) NULL,
	[ADESC] [nvarchar](4000) NULL,
	[AHLPURL] [nvarchar](2000) NULL,
	[AKEY] [nvarchar](100) NULL,
	[AREGPAR] [bigint] NULL,
	[ASUBM] [decimal](1, 0) NULL,
 CONSTRAINT [PK__RCAM__75A05C3E64ECEE3F] PRIMARY KEY CLUSTERED 
(
	[ANUMREG] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_RCAM_AID]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAM] ADD  CONSTRAINT [DF_RCAM_AID]  DEFAULT ((0)) FOR [AID]
GO
/****** Object:  Default [DF_RCAM_ALVL]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAM] ADD  CONSTRAINT [DF_RCAM_ALVL]  DEFAULT ((0)) FOR [ALVL]
GO
/****** Object:  Default [DF_RCAM_AORD]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAM] ADD  CONSTRAINT [DF_RCAM_AORD]  DEFAULT ((0)) FOR [AORD]
GO
/****** Object:  Default [DF_RCAM_AAPP]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAM] ADD  CONSTRAINT [DF_RCAM_AAPP]  DEFAULT ('') FOR [AAPP]
GO
/****** Object:  Default [DF_RCAM_AMOD]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAM] ADD  CONSTRAINT [DF_RCAM_AMOD]  DEFAULT ('') FOR [AMOD]
GO
/****** Object:  Default [DF_RCAM_AGRP]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAM] ADD  CONSTRAINT [DF_RCAM_AGRP]  DEFAULT ((0)) FOR [AGRP]
GO
/****** Object:  Default [DF_RCAM_APARENT]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAM] ADD  CONSTRAINT [DF_RCAM_APARENT]  DEFAULT ((0)) FOR [APARENT]
GO
/****** Object:  Default [DF_RCAM_ATIT]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAM] ADD  CONSTRAINT [DF_RCAM_ATIT]  DEFAULT ('') FOR [ATIT]
GO
/****** Object:  Default [DF_RCAM_ALINK]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAM] ADD  CONSTRAINT [DF_RCAM_ALINK]  DEFAULT ('') FOR [ALINK]
GO
/****** Object:  Default [DF_RCAM_ACLASS]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAM] ADD  CONSTRAINT [DF_RCAM_ACLASS]  DEFAULT ('') FOR [ACLASS]
GO
/****** Object:  Default [DF_RCAM_AISET]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAM] ADD  CONSTRAINT [DF_RCAM_AISET]  DEFAULT ('') FOR [AISET]
GO
/****** Object:  Default [DF_RCAM_APARM1]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAM] ADD  CONSTRAINT [DF_RCAM_APARM1]  DEFAULT ('') FOR [APARM1]
GO
/****** Object:  Default [DF_RCAM_APARM2]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAM] ADD  CONSTRAINT [DF_RCAM_APARM2]  DEFAULT ('') FOR [APARM2]
GO
/****** Object:  Default [DF_RCAM_APARM3]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAM] ADD  CONSTRAINT [DF_RCAM_APARM3]  DEFAULT ('') FOR [APARM3]
GO
/****** Object:  Default [DF_RCAM_APARM4]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAM] ADD  CONSTRAINT [DF_RCAM_APARM4]  DEFAULT ('') FOR [APARM4]
GO
/****** Object:  Default [DF_RCAM_APARM5]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAM] ADD  CONSTRAINT [DF_RCAM_APARM5]  DEFAULT ('') FOR [APARM5]
GO
/****** Object:  Default [DF_RCAM_ADESC]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAM] ADD  CONSTRAINT [DF_RCAM_ADESC]  DEFAULT ('') FOR [ADESC]
GO
/****** Object:  Default [DF_RCAM_AHLPURL]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAM] ADD  CONSTRAINT [DF_RCAM_AHLPURL]  DEFAULT ('') FOR [AHLPURL]
GO
/****** Object:  Default [DF_RCAM_AKEY]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAM] ADD  CONSTRAINT [DF_RCAM_AKEY]  DEFAULT ('') FOR [AKEY]
GO
/****** Object:  Default [DF_RCAM_AREGPAR]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAM] ADD  CONSTRAINT [DF_RCAM_AREGPAR]  DEFAULT ((0)) FOR [AREGPAR]
GO
/****** Object:  Default [DF_RCAM_ASUBM]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAM] ADD  CONSTRAINT [DF_RCAM_ASUBM]  DEFAULT ((0)) FOR [ASUBM]