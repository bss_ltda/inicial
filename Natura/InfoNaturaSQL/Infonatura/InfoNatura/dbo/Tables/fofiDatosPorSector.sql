﻿CREATE TABLE [dbo].[fofiDatosPorSector](
	[Ciclo] [decimal](6, 0) NOT NULL,
	[Sector] [int] NOT NULL,
	[Gerencia] [nvarchar](255) NULL,
	[SectorNombre] [nvarchar](255) NULL,
	[CNOs] [smallint] NULL,
	[PorcMorosidad42CicloAnterior] [float] NULL,
	[BonoCicloAnterior] [float] NULL,
	[Batch] [bigint] NULL,
 CONSTRAINT [PK_fofiDatosPorSector] PRIMARY KEY CLUSTERED 
(
	[Ciclo] ASC,
	[Sector] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]