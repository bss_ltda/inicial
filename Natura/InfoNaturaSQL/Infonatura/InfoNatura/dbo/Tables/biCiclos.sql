﻿CREATE TABLE [dbo].[biCiclos](
	[Ciclo] [decimal](6, 0) NOT NULL,
	[CicloYear] [decimal](4, 0) NULL,
	[CicloNumero] [decimal](2, 0) NULL,
	[CicloF1] [nchar](10) NULL,
	[CicloF2] [nchar](10) NULL,
	[Ciclo42Dias] [decimal](6, 0) NULL,
	[Ciclo63Dias] [decimal](6, 0) NULL,
	[Ciclo42DiasFofi] [decimal](6, 0) NULL,
	[Ciclo63DiasFofi] [decimal](6, 0) NULL,
	[CicloFofi] [decimal](1, 0) NOT NULL,
 CONSTRAINT [PK_biCiclos] PRIMARY KEY CLUSTERED 
(
	[Ciclo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_biCiclos_CicloFofi]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[biCiclos] ADD  CONSTRAINT [DF_biCiclos_CicloFofi]  DEFAULT ((0)) FOR [CicloFofi]