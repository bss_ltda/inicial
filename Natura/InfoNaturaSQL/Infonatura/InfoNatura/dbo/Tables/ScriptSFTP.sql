﻿CREATE TABLE [dbo].[ScriptSFTP](
	[IdScriptSFTP] [int] IDENTITY(1,1) NOT NULL,
	[Banco] [nvarchar](150) NULL,
	[NombreArchivo] [nvarchar](150) NULL,
	[TamArchivo] [int] NULL,
	[RutaArchivo] [nvarchar](250) NULL,
	[FechaDescargado] [datetime] NULL,
	[FechaRevisado] [datetime] NULL,
	[FechaEnGera] [datetime] NULL,
	[Estado] [nvarchar](50) NULL,
	[STS01] [smallint] NULL,
	[STS02] [smallint] NULL,
	[STS03] [smallint] NULL,
	[STS04] [smallint] NULL,
	[STS05] [smallint] NULL,
	[Mensaje] [nvarchar](600) NULL,
 CONSTRAINT [PK_ScriptSFTP] PRIMARY KEY CLUSTERED 
(
	[IdScriptSFTP] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_ScriptSFTP_STS01]    Script Date: 09/13/2018 18:30:17 ******/
ALTER TABLE [dbo].[ScriptSFTP] ADD  CONSTRAINT [DF_ScriptSFTP_STS01]  DEFAULT ((0)) FOR [STS01]
GO
/****** Object:  Default [DF_ScriptSFTP_STS02]    Script Date: 09/13/2018 18:30:17 ******/
ALTER TABLE [dbo].[ScriptSFTP] ADD  CONSTRAINT [DF_ScriptSFTP_STS02]  DEFAULT ((0)) FOR [STS02]
GO
/****** Object:  Default [DF_ScriptSFTP_STS03]    Script Date: 09/13/2018 18:30:17 ******/
ALTER TABLE [dbo].[ScriptSFTP] ADD  CONSTRAINT [DF_ScriptSFTP_STS03]  DEFAULT ((0)) FOR [STS03]
GO
/****** Object:  Default [DF_ScriptSFTP_STS04]    Script Date: 09/13/2018 18:30:17 ******/
ALTER TABLE [dbo].[ScriptSFTP] ADD  CONSTRAINT [DF_ScriptSFTP_STS04]  DEFAULT ((0)) FOR [STS04]
GO
/****** Object:  Default [DF_ScriptSFTP_STS05]    Script Date: 09/13/2018 18:30:17 ******/
ALTER TABLE [dbo].[ScriptSFTP] ADD  CONSTRAINT [DF_ScriptSFTP_STS05]  DEFAULT ((0)) FOR [STS05]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tamaño en bytes' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ScriptSFTP', @level2type=N'COLUMN',@level2name=N'TamArchivo'