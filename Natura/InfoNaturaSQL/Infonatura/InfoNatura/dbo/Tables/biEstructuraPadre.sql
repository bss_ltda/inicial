﻿CREATE TABLE [dbo].[biEstructuraPadre](
	[codGerencia] [float] NOT NULL,
	[EstructuraPadre] [nvarchar](255) NULL,
	[Gerencia] [nvarchar](50) NULL,
 CONSTRAINT [PK_aEstructuraPadre] PRIMARY KEY CLUSTERED 
(
	[codGerencia] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]