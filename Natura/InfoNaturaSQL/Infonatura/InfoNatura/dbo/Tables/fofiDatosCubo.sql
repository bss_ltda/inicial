﻿CREATE TABLE [dbo].[fofiDatosCubo](
	[datoSector] [float] NOT NULL,
	[SectorCubo] [nvarchar](150) NULL,
	[ActivaMeta] [float] NULL,
	[ActivaRealizado] [float] NULL,
	[DisponibleMeta] [float] NULL,
	[DisponibleRealizado] [float] NULL,
	[Inactiva3Meta] [float] NULL,
	[Inactiva3Realizado] [float] NULL,
	[FacturacionMeta] [float] NULL,
	[FacturacionRealizado] [float] NULL,
	[SaldoDisponibleMeta] [float] NULL,
	[SaldoDisponibleRealizado] [float] NULL,
	[ActivaFrecuenteMeta] [float] NULL,
	[ActivaFrecuenteRealizado] [float] NULL,
	[IndisponibleRealizado] [float] NULL,
	[CadastroRealizado] [float] NULL,
	[Inactiva6Realizado] [float] NULL,
	[ActividadMeta] [float] NULL
) ON [PRIMARY]