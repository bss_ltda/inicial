﻿CREATE TABLE [dbo].[RevistasDeprisa](
	[CantCajas] [nvarchar](255) NULL,
	[Referencia] [nvarchar](255) NULL,
	[Guia] [nvarchar](255) NULL,
	[NombreDestinatario] [nvarchar](255) NULL,
	[DireccionDestinatario] [nvarchar](255) NULL,
	[CiudadDestino] [nvarchar](255) NULL,
	[CodigoPostal] [nvarchar](255) NULL,
	[DepartamentoDestino] [nvarchar](255) NULL,
	[CelDestinatario] [nvarchar](255) NULL,
	[Servicio] [nvarchar](255) NULL,
	[EstadoDelEnvio] [nvarchar](255) NULL,
	[FechaDespacho] [nvarchar](255) NULL,
	[FechaEntrega] [nvarchar](255) NULL,
	[HoraDeEntrega] [nvarchar](255) NULL,
	[DiasEnEntrega] [nvarchar](255) NULL,
	[PromesaDeEntrega] [nvarchar](255) NULL,
	[Cumple] [nvarchar](255) NULL,
	[Responsable] [nvarchar](255) NULL,
	[Observaciones] [nvarchar](255) NULL,
	[NomReceptor] [nvarchar](255) NULL,
	[NitReceptor] [nvarchar](255) NULL,
	[D1] [nvarchar](255) NULL,
	[D2] [nvarchar](255) NULL,
	[D3] [nvarchar](255) NULL,
	[D4] [nvarchar](255) NULL,
	[D5] [nvarchar](255) NULL,
	[D] [nvarchar](255) NULL,
	[Valor] [nvarchar](255) NULL
) ON [PRIMARY]