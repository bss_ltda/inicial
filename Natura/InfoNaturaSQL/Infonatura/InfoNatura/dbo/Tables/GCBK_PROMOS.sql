﻿CREATE TABLE [dbo].[GCBK_PROMOS](
	[PRMID] [float] NULL,
	[PRMCL] [varchar](100) NULL,
	[PRMDESC] [varchar](100) NULL,
	[PRMFLAG] [float] NULL,
	[PRMSKU] [float] NULL,
	[PRMCATE] [varchar](100) NULL,
	[PRMUSR] [varchar](100) NULL,
	[PRMFEC] [timestamp] NOT NULL,
	[PRMITEMID] [int] IDENTITY(1,1) NOT NULL
) ON [PRIMARY]