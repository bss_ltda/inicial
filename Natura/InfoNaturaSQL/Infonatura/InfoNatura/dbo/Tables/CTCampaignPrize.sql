﻿CREATE TABLE [dbo].[CTCampaignPrize](
	[IdCTCampaignPrize] [int] IDENTITY(1,1) NOT NULL,
	[IdCTCampaign] [int] NOT NULL,
	[Premio] [nvarchar](255) NOT NULL,
	[Puntos] [smallint] NULL,
	[MinimoPedidos] [smallint] NULL,
	[Ciclos] [smallint] NULL,
	[Acumulado] [nchar](1) NULL,
	[Color] [nchar](10) NULL,
 CONSTRAINT [PK_CTCampaignPrize] PRIMARY KEY CLUSTERED 
(
	[IdCTCampaignPrize] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_CTCampaignPrize_CTCampaign]    Script Date: 09/13/2018 18:30:19 ******/
ALTER TABLE [dbo].[CTCampaignPrize]  WITH CHECK ADD  CONSTRAINT [FK_CTCampaignPrize_CTCampaign] FOREIGN KEY([IdCTCampaign])
REFERENCES [dbo].[CTCampaign] ([IdCTCampaign])
GO

ALTER TABLE [dbo].[CTCampaignPrize] CHECK CONSTRAINT [FK_CTCampaignPrize_CTCampaign]