﻿CREATE TABLE [dbo].[TransportadorasNovedades](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Pedido] [varchar](50) NULL,
	[Consultora] [varchar](50) NULL,
	[Novedad] [varchar](500) NULL,
	[usuario] [varchar](50) NULL,
	[Fecha] [timestamp] NULL,
 CONSTRAINT [PK_TransportadorasNovedades] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]