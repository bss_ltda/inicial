﻿CREATE TABLE [dbo].[CTCampaignCycle](
	[IdCTCampaignCycle] [int] IDENTITY(1,1) NOT NULL,
	[IdCTCampaign] [int] NOT NULL,
	[IdCTCampaignPrize] [int] NULL,
	[Ciclo] [nchar](7) NOT NULL,
 CONSTRAINT [PK_CTCampaignCycle] PRIMARY KEY CLUSTERED 
(
	[IdCTCampaignCycle] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_CTCampaignCycle_CTCampaign]    Script Date: 09/13/2018 18:30:19 ******/
ALTER TABLE [dbo].[CTCampaignCycle]  WITH CHECK ADD  CONSTRAINT [FK_CTCampaignCycle_CTCampaign] FOREIGN KEY([IdCTCampaign])
REFERENCES [dbo].[CTCampaign] ([IdCTCampaign])
GO

ALTER TABLE [dbo].[CTCampaignCycle] CHECK CONSTRAINT [FK_CTCampaignCycle_CTCampaign]