﻿CREATE TABLE [dbo].[FinanzasAhorro_Timming](
	[Mes] [nvarchar](255) NULL,
	[Centro] [nvarchar](255) NULL,
	[Subentro] [nvarchar](255) NULL,
	[Proyecto] [nvarchar](255) NULL,
	[ComentariosMes] [nvarchar](1000) NULL,
	[AhorrosMes] [float] NULL,
	[TimmingMes] [float] NULL,
	[ComentariosAño] [nvarchar](1000) NULL,
	[AhorroAño] [float] NULL,
	[TimmigAño] [float] NULL,
	[Usr] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Default [DF_Proyecto]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasAhorro_Timming] ADD  CONSTRAINT [DF_Proyecto]  DEFAULT ('') FOR [Proyecto]