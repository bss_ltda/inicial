﻿CREATE TABLE [dbo].[FinanzasResu](
	[Mes] [smallint] NULL,
	[Tercero] [nvarchar](255) NULL,
	[CentroCosto] [nvarchar](255) NULL,
	[SubCentroCosto] [nvarchar](255) NULL,
	[Proyecto] [nvarchar](255) NULL,
	[Actual] [float] NULL,
	[Ppto] [float] NULL,
	[Anterior] [float] NULL,
	[RO] [float] NULL,
	[AcumActual] [float] NULL,
	[AcumPpto] [float] NULL,
	[AcumAnterior] [float] NULL,
	[AcumRO] [float] NULL,
	[AcumActualY] [float] NULL,
	[AcumPptoY] [float] NULL,
	[AcumAnteriorY] [float] NULL,
	[AcumROY] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Default [DF_prueba_valor0]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasResu] ADD  CONSTRAINT [DF_prueba_valor0]  DEFAULT ((0)) FOR [Actual]
GO
/****** Object:  Default [DF_prueba_valor1]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasResu] ADD  CONSTRAINT [DF_prueba_valor1]  DEFAULT ((0)) FOR [Ppto]
GO
/****** Object:  Default [DF_prueba_valor2]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasResu] ADD  CONSTRAINT [DF_prueba_valor2]  DEFAULT ((0)) FOR [Anterior]
GO
/****** Object:  Default [DF_prueba_valor3]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasResu] ADD  CONSTRAINT [DF_prueba_valor3]  DEFAULT ((0)) FOR [RO]
GO
/****** Object:  Default [DF_prueba_valor4]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasResu] ADD  CONSTRAINT [DF_prueba_valor4]  DEFAULT ((0)) FOR [AcumActual]
GO
/****** Object:  Default [DF_prueba_valor5]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasResu] ADD  CONSTRAINT [DF_prueba_valor5]  DEFAULT ((0)) FOR [AcumPpto]
GO
/****** Object:  Default [DF_prueba_valor6]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasResu] ADD  CONSTRAINT [DF_prueba_valor6]  DEFAULT ((0)) FOR [AcumAnterior]
GO
/****** Object:  Default [DF_prueba_valor7]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasResu] ADD  CONSTRAINT [DF_prueba_valor7]  DEFAULT ((0)) FOR [AcumRO]
GO
/****** Object:  Default [DF_prueba_valor8]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasResu] ADD  CONSTRAINT [DF_prueba_valor8]  DEFAULT ((0)) FOR [AcumActualY]
GO
/****** Object:  Default [DF_prueba_valor9]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasResu] ADD  CONSTRAINT [DF_prueba_valor9]  DEFAULT ((0)) FOR [AcumPptoY]
GO
/****** Object:  Default [DF_prueba_valor10]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasResu] ADD  CONSTRAINT [DF_prueba_valor10]  DEFAULT ((0)) FOR [AcumAnteriorY]
GO
/****** Object:  Default [DF_prueba_valor11]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasResu] ADD  CONSTRAINT [DF_prueba_valor11]  DEFAULT ((0)) FOR [AcumROY]