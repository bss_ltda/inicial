﻿CREATE TABLE [dbo].[DespachosPlanillaDiaN](
	[NoPlanilla] [int] IDENTITY(1,1) NOT NULL,
	[Transportadora] [nvarchar](255) NULL,
	[Embalajes] [int] NULL,
	[Estibas] [int] NULL,
	[Pedidos] [int] NULL,
	[Cajas] [int] NULL,
	[Fecha] [datetime] NULL,
 CONSTRAINT [PK_DespachosPlanillaDiaN] PRIMARY KEY CLUSTERED 
(
	[NoPlanilla] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_DespachosPlanillaDiaN_Fecha]    Script Date: 09/13/2018 18:30:17 ******/
ALTER TABLE [dbo].[DespachosPlanillaDiaN] ADD  CONSTRAINT [DF_DespachosPlanillaDiaN_Fecha]  DEFAULT (getdate()) FOR [Fecha]