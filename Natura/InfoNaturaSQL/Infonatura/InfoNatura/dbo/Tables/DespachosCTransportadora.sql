﻿CREATE TABLE [dbo].[DespachosCTransportadora](
	[Id_Cambio] [int] NULL,
	[Pedido] [varchar](100) NULL,
	[DireccionAnterior] [varchar](100) NULL,
	[NuevaDireccion] [varchar](100) NULL,
	[Usr] [varchar](100) NULL,
	[Fecha] [datetime] NULL
) ON [PRIMARY]