﻿CREATE TABLE [dbo].[CorreoDatos](
	[IdDatos] [bigint] IDENTITY(1,1) NOT NULL,
	[IdCorreo] [bigint] NULL,
	[Codigo] [nchar](50) NULL,
	[Nombre] [nvarchar](150) NULL,
	[Correo] [nvarchar](150) NULL,
	[ColumnaA] [nvarchar](250) NULL,
	[ColumnaB] [nvarchar](250) NULL,
	[ColumnaC] [nvarchar](250) NULL,
	[ColumnaD] [nvarchar](250) NULL,
	[ColumnaE] [nvarchar](250) NULL,
	[ColumnaF] [nvarchar](250) NULL,
	[ColumnaG] [nvarchar](250) NULL,
	[ColumnaH] [nvarchar](250) NULL,
	[ColumnaI] [nvarchar](250) NULL,
	[ColumnaJ] [nvarchar](250) NULL,
	[ColumnaK] [nvarchar](250) NULL,
	[ColumnaL] [nvarchar](250) NULL,
	[ColumnaM] [nvarchar](250) NULL,
	[ColumnaN] [nvarchar](250) NULL,
	[ColumnaO] [nvarchar](250) NULL,
	[ColumnaP] [nvarchar](250) NULL,
	[ColumnaQ] [nvarchar](250) NULL,
	[ColumnaR] [nvarchar](250) NULL,
	[ColumnaS] [nvarchar](250) NULL,
	[ColumnaT] [nvarchar](250) NULL,
	[Sts1] [decimal](1, 0) NOT NULL,
	[Sts2] [decimal](1, 0) NOT NULL,
	[Sts3] [decimal](1, 0) NOT NULL,
	[Sts4] [decimal](1, 0) NOT NULL,
	[Sts5] [decimal](1, 0) NOT NULL,
	[Mensaje] [nvarchar](max) NULL,
	[Fecha] [datetime] NOT NULL,
 CONSTRAINT [PK_CorreoDatos] PRIMARY KEY CLUSTERED 
(
	[IdDatos] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_CorreoDatos_Sts1]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[CorreoDatos] ADD  CONSTRAINT [DF_CorreoDatos_Sts1]  DEFAULT ((0)) FOR [Sts1]
GO
/****** Object:  Default [DF_CorreoDatos_Sts2]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[CorreoDatos] ADD  CONSTRAINT [DF_CorreoDatos_Sts2]  DEFAULT ((0)) FOR [Sts2]
GO
/****** Object:  Default [DF_CorreoDatos_Sts3]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[CorreoDatos] ADD  CONSTRAINT [DF_CorreoDatos_Sts3]  DEFAULT ((0)) FOR [Sts3]
GO
/****** Object:  Default [DF_CorreoDatos_Sts4]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[CorreoDatos] ADD  CONSTRAINT [DF_CorreoDatos_Sts4]  DEFAULT ((0)) FOR [Sts4]
GO
/****** Object:  Default [DF_CorreoDatos_Sts5]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[CorreoDatos] ADD  CONSTRAINT [DF_CorreoDatos_Sts5]  DEFAULT ((0)) FOR [Sts5]
GO
/****** Object:  Default [DF_CorreoDatos_Fecha]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[CorreoDatos] ADD  CONSTRAINT [DF_CorreoDatos_Fecha]  DEFAULT (getdate()) FOR [Fecha]