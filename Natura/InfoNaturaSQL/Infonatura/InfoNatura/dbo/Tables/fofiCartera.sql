﻿CREATE TABLE [dbo].[fofiCartera](
	[Ciclo] [decimal](6, 0) NOT NULL,
	[Sector] [float] NOT NULL,
	[GV] [decimal](1, 0) NULL,
	[Cargo] [nchar](10) NULL,
	[Gerencia] [nvarchar](255) NULL,
	[SectorNombre] [nvarchar](255) NULL,
	[Venta42] [float] NULL,
	[Saldo42] [float] NULL,
	[Cobranza42] [float] NULL,
	[SaldoCobranza42] [float] NULL,
	[Venta63] [float] NULL,
	[Saldo63] [float] NULL,
	[Cobranza63] [float] NULL,
	[SaldoCobranza63] [float] NULL,
	[Batch] [bigint] NULL,
 CONSTRAINT [PK_fofiCartera] PRIMARY KEY CLUSTERED 
(
	[Ciclo] ASC,
	[Sector] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]