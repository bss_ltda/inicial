﻿CREATE TABLE [dbo].[GCBK_CICLOS](
	[CNUMREG] [float] NOT NULL,
	[CLID] [varchar](100) NOT NULL,
	[CLFECINI] [varchar](100) NULL,
	[CLFECFIN] [varchar](100) NULL,
	[CLESPECIAL] [varchar](100) NULL,
	[CLPORTADA] [varchar](100) NULL,
	[CLAPERTURA] [varchar](100) NULL,
	[CL3PORTADA] [varchar](100) NULL,
	[CL4PORTADA] [varchar](100) NULL,
	[CLCROSS] [varchar](100) NULL,
	[CLNOTAS] [varchar](500) NULL,
	[CLUSR] [varchar](100) NULL,
	[CLFEC] [timestamp] NULL
) ON [PRIMARY]