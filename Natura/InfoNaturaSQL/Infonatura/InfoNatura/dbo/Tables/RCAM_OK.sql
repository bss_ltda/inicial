﻿CREATE TABLE [dbo].[RCAM_OK](
	[AID] [decimal](3, 0) NOT NULL,
	[ALVL] [decimal](2, 0) NOT NULL,
	[AORD] [decimal](2, 0) NOT NULL,
	[AAPP] [nvarchar](15) NOT NULL,
	[AMOD] [nvarchar](30) NOT NULL,
	[AGRP] [decimal](3, 0) NOT NULL,
	[APARENT] [decimal](3, 0) NOT NULL,
	[ATIT] [nvarchar](100) NOT NULL,
	[ALINK] [nvarchar](100) NOT NULL,
	[ACLASS] [nvarchar](20) NOT NULL,
	[AISET] [nvarchar](100) NOT NULL,
	[APARM1] [nvarchar](10) NOT NULL,
	[APARM2] [nvarchar](10) NOT NULL,
	[APARM3] [nvarchar](10) NOT NULL,
	[APARM4] [nvarchar](10) NOT NULL,
	[APARM5] [nvarchar](10) NOT NULL,
	[ADESC] [nvarchar](max) NOT NULL
) ON [PRIMARY]