﻿CREATE TABLE [dbo].[fofiPlanDeCarrera](
	[IdPlanCarrera] [bigint] IDENTITY(1,1) NOT NULL,
	[Ciclo] [decimal](6, 0) NULL,
	[codSector] [int] NULL,
	[Sector] [nvarchar](255) NULL,
	[Fact] [float] NULL,
	[Premio] [float] NULL,
	[InicComp] [float] NULL,
	[ETTO] [float] NULL,
	[TotalPuntos] [float] NULL,
	[ConceptoParcial] [nvarchar](50) NULL,
	[Batch] [bigint] NULL,
	[Actualizado] [datetime] NULL,
	[Usuario] [nchar](25) NULL,
 CONSTRAINT [PK_fofiPlanDeCarrera] PRIMARY KEY CLUSTERED 
(
	[IdPlanCarrera] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]