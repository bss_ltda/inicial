﻿CREATE TABLE [dbo].[TransportadoraCorreo](
	[idCorreo] [int] IDENTITY(1,1) NOT NULL,
	[Operacion] [nvarchar](50) NULL,
	[idTransportadora] [int] NULL,
	[TipoDestinatario] [nvarchar](10) NULL,
	[Nombre] [nvarchar](50) NULL,
	[Correo] [nvarchar](150) NULL,
 CONSTRAINT [PK_TransportadoraCorreo] PRIMARY KEY CLUSTERED 
(
	[idCorreo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]