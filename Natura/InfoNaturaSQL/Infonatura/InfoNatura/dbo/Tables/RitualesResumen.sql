﻿CREATE TABLE [dbo].[RitualesResumen](
	[Ciclo] [int] NULL,
	[Gerencia] [varchar](50) NULL,
	[CodSector] [varchar](50) NULL,
	[Sector] [varchar](50) NULL,
	[GrupoCNO] [varchar](50) NULL,
	[ValorLiquido] [float] NOT NULL,
	[Pedidos] [int] NULL,
	[CantCNs] [int] NULL,
	[ValorPersona] [float] NULL
) ON [PRIMARY]