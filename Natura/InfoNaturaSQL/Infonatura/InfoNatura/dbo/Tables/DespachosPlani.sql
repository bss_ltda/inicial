﻿CREATE TABLE [dbo].[DespachosPlani](
	[Estiba1] [nvarchar](255) NULL,
	[Estiba2] [nvarchar](255) NULL,
	[Estiba3] [nvarchar](255) NULL,
	[Estiba4] [nvarchar](255) NULL,
	[Estiba5] [nvarchar](255) NULL,
	[Estiba6] [nvarchar](255) NULL,
	[Estiba7] [nvarchar](255) NULL,
	[Estiba8] [nvarchar](255) NULL,
	[Estiba9] [nvarchar](255) NULL,
	[Estiba10] [nvarchar](255) NULL,
	[Estiba11] [nvarchar](255) NULL,
	[Estiba12] [nvarchar](255) NULL,
	[Estiba13] [nvarchar](255) NULL,
	[Estiba14] [nvarchar](255) NULL,
	[Estiba15] [nvarchar](255) NULL,
	[Estiba16] [nvarchar](255) NULL,
	[Estiba17] [nvarchar](255) NULL,
	[Estiba18] [nvarchar](255) NULL,
	[Estiba19] [nvarchar](255) NULL,
	[Estiba20] [nvarchar](255) NULL,
	[Estiba21] [nvarchar](255) NULL,
	[Estiba22] [nvarchar](255) NULL,
	[Estiba23] [nvarchar](255) NULL,
	[Estiba24] [nvarchar](255) NULL,
	[Estiba25] [nvarchar](255) NULL,
	[Estiba26] [nvarchar](255) NULL,
	[Estiba27] [nvarchar](255) NULL,
	[Estiba28] [nvarchar](255) NULL,
	[Estiba29] [nvarchar](255) NULL,
	[Estiba30] [nvarchar](255) NULL,
	[Estiba31] [nvarchar](255) NULL,
	[Estiba32] [nvarchar](255) NULL,
	[Estiba33] [nvarchar](255) NULL,
	[Estiba34] [nvarchar](255) NULL,
	[Estiba35] [nvarchar](255) NULL,
	[Estiba36] [nvarchar](255) NULL,
	[Estiba37] [nvarchar](255) NULL,
	[Estiba38] [nvarchar](255) NULL,
	[Estiba39] [nvarchar](255) NULL,
	[Estiba40] [nvarchar](255) NULL,
	[Estiba41] [nvarchar](255) NULL,
	[Estiba42] [nvarchar](255) NULL,
	[Estiba43] [nvarchar](255) NULL,
	[Estiba44] [nvarchar](255) NULL,
	[Estiba45] [nvarchar](255) NULL,
	[Estiba46] [nvarchar](255) NULL,
	[Estiba47] [nvarchar](255) NULL,
	[Estiba48] [nvarchar](255) NULL,
	[Estiba49] [nvarchar](255) NULL,
	[Estiba50] [nvarchar](255) NULL,
	[Estiba51] [nvarchar](255) NULL,
	[Estiba52] [nvarchar](255) NULL,
	[Estiba53] [nvarchar](255) NULL,
	[Estiba54] [nvarchar](255) NULL,
	[Estiba55] [nvarchar](255) NULL,
	[Estiba56] [nvarchar](255) NULL,
	[Estiba57] [nvarchar](255) NULL,
	[Estiba58] [nvarchar](255) NULL,
	[Estiba59] [nvarchar](255) NULL,
	[Estiba60] [nvarchar](255) NULL,
	[Transportadora] [nvarchar](255) NULL,
	[grupo] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba1]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba1]  DEFAULT (' ') FOR [Estiba1]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba2]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba2]  DEFAULT (' ') FOR [Estiba2]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba3]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba3]  DEFAULT (' ') FOR [Estiba3]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba4]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba4]  DEFAULT (' ') FOR [Estiba4]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba5]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba5]  DEFAULT (' ') FOR [Estiba5]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba6]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba6]  DEFAULT (' ') FOR [Estiba6]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba7]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba7]  DEFAULT (' ') FOR [Estiba7]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba8]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba8]  DEFAULT (' ') FOR [Estiba8]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba9]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba9]  DEFAULT (' ') FOR [Estiba9]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba10]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba10]  DEFAULT (' ') FOR [Estiba10]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba11]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba11]  DEFAULT (' ') FOR [Estiba11]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba12]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba12]  DEFAULT (' ') FOR [Estiba12]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba13]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba13]  DEFAULT (' ') FOR [Estiba13]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba14]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba14]  DEFAULT (' ') FOR [Estiba14]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba15]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba15]  DEFAULT (' ') FOR [Estiba15]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba16]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba16]  DEFAULT (' ') FOR [Estiba16]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba17]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba17]  DEFAULT (' ') FOR [Estiba17]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba18]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba18]  DEFAULT (' ') FOR [Estiba18]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba19]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba19]  DEFAULT (' ') FOR [Estiba19]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba20]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba20]  DEFAULT (' ') FOR [Estiba20]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba21]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba21]  DEFAULT (' ') FOR [Estiba21]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba22]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba22]  DEFAULT (' ') FOR [Estiba22]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba23]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba23]  DEFAULT (' ') FOR [Estiba23]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba24]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba24]  DEFAULT (' ') FOR [Estiba24]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba25]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba25]  DEFAULT (' ') FOR [Estiba25]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba26]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba26]  DEFAULT (' ') FOR [Estiba26]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba27]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba27]  DEFAULT (' ') FOR [Estiba27]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba28]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba28]  DEFAULT (' ') FOR [Estiba28]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba29]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba29]  DEFAULT (' ') FOR [Estiba29]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba30]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba30]  DEFAULT (' ') FOR [Estiba30]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba31]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba31]  DEFAULT (' ') FOR [Estiba31]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba32]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba32]  DEFAULT (' ') FOR [Estiba32]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba33]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba33]  DEFAULT (' ') FOR [Estiba33]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba34]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba34]  DEFAULT (' ') FOR [Estiba34]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba35]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba35]  DEFAULT (' ') FOR [Estiba35]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba36]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba36]  DEFAULT (' ') FOR [Estiba36]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba37]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba37]  DEFAULT (' ') FOR [Estiba37]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba38]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba38]  DEFAULT (' ') FOR [Estiba38]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba39]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba39]  DEFAULT (' ') FOR [Estiba39]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba40]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba40]  DEFAULT (' ') FOR [Estiba40]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba41]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba41]  DEFAULT (' ') FOR [Estiba41]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba42]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba42]  DEFAULT (' ') FOR [Estiba42]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba43]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba43]  DEFAULT (' ') FOR [Estiba43]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba44]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba44]  DEFAULT (' ') FOR [Estiba44]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba45]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba45]  DEFAULT (' ') FOR [Estiba45]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba46]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba46]  DEFAULT (' ') FOR [Estiba46]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba47]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba47]  DEFAULT (' ') FOR [Estiba47]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba48]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba48]  DEFAULT (' ') FOR [Estiba48]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba49]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba49]  DEFAULT (' ') FOR [Estiba49]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba50]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba50]  DEFAULT (' ') FOR [Estiba50]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba51]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba51]  DEFAULT (' ') FOR [Estiba51]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba52]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba52]  DEFAULT (' ') FOR [Estiba52]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba53]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba53]  DEFAULT (' ') FOR [Estiba53]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba54]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba54]  DEFAULT (' ') FOR [Estiba54]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba55]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba55]  DEFAULT (' ') FOR [Estiba55]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba56]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba56]  DEFAULT (' ') FOR [Estiba56]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba57]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba57]  DEFAULT (' ') FOR [Estiba57]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba58]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba58]  DEFAULT (' ') FOR [Estiba58]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba59]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba59]  DEFAULT (' ') FOR [Estiba59]
GO
/****** Object:  Default [DF_DespachosPlani_Estiba60]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachosPlani] ADD  CONSTRAINT [DF_DespachosPlani_Estiba60]  DEFAULT (' ') FOR [Estiba60]