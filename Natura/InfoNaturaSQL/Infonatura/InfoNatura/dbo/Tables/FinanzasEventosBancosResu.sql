﻿CREATE TABLE [dbo].[FinanzasEventosBancosResu](
	[PERIODO] [nchar](6) NULL,
	[DOCUMENT_DATE] [nchar](8) NULL,
	[RECEIVER] [nvarchar](50) NULL,
	[RECEIVER_BALANCE] [float] NOT NULL,
	[SALDO_REC] [float] NOT NULL,
	[IMPORTE_MD] [float] NOT NULL,
	[PENALTY_INTEREST_BALANCE] [float] NOT NULL,
	[MULTA_INT] [float] NOT NULL,
	[TAX1] [float] NOT NULL,
	[IMPUESTO1] [float] NOT NULL,
	[STS1] [smallint] NOT NULL,
	[STS2] [smallint] NOT NULL,
	[STS3] [smallint] NOT NULL,
	[STS4] [smallint] NOT NULL,
	[STS5] [smallint] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Default [DF_FinanzasEventosBancosResu_RECEIVER_BALANCE]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasEventosBancosResu] ADD  CONSTRAINT [DF_FinanzasEventosBancosResu_RECEIVER_BALANCE]  DEFAULT ((0)) FOR [RECEIVER_BALANCE]
GO
/****** Object:  Default [DF_FinanzasEventosBancosResu_SALDO_REC]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasEventosBancosResu] ADD  CONSTRAINT [DF_FinanzasEventosBancosResu_SALDO_REC]  DEFAULT ((0)) FOR [SALDO_REC]
GO
/****** Object:  Default [DF_FinanzasEventosBancosResu_IMPORTE_MD]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasEventosBancosResu] ADD  CONSTRAINT [DF_FinanzasEventosBancosResu_IMPORTE_MD]  DEFAULT ((0)) FOR [IMPORTE_MD]
GO
/****** Object:  Default [DF_FinanzasEventosBancosResu_PENALTY_INTEREST_BALANCE]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasEventosBancosResu] ADD  CONSTRAINT [DF_FinanzasEventosBancosResu_PENALTY_INTEREST_BALANCE]  DEFAULT ((0)) FOR [PENALTY_INTEREST_BALANCE]
GO
/****** Object:  Default [DF_FinanzasEventosBancosResu_MULTA_INT]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasEventosBancosResu] ADD  CONSTRAINT [DF_FinanzasEventosBancosResu_MULTA_INT]  DEFAULT ((0)) FOR [MULTA_INT]
GO
/****** Object:  Default [DF_FinanzasEventosBancosResu_TAX1]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasEventosBancosResu] ADD  CONSTRAINT [DF_FinanzasEventosBancosResu_TAX1]  DEFAULT ((0)) FOR [TAX1]
GO
/****** Object:  Default [DF_FinanzasEventosBancosResu_IMPUESTO1]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasEventosBancosResu] ADD  CONSTRAINT [DF_FinanzasEventosBancosResu_IMPUESTO1]  DEFAULT ((0)) FOR [IMPUESTO1]
GO
/****** Object:  Default [DF_FinanzasEventosBancosResu_STS1]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasEventosBancosResu] ADD  CONSTRAINT [DF_FinanzasEventosBancosResu_STS1]  DEFAULT ((0)) FOR [STS1]
GO
/****** Object:  Default [DF_FinanzasEventosBancosResu_STS2]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasEventosBancosResu] ADD  CONSTRAINT [DF_FinanzasEventosBancosResu_STS2]  DEFAULT ((0)) FOR [STS2]
GO
/****** Object:  Default [DF_FinanzasEventosBancosResu_STS3]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasEventosBancosResu] ADD  CONSTRAINT [DF_FinanzasEventosBancosResu_STS3]  DEFAULT ((0)) FOR [STS3]
GO
/****** Object:  Default [DF_FinanzasEventosBancosResu_STS4]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasEventosBancosResu] ADD  CONSTRAINT [DF_FinanzasEventosBancosResu_STS4]  DEFAULT ((0)) FOR [STS4]
GO
/****** Object:  Default [DF_FinanzasEventosBancosResu_STS5]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasEventosBancosResu] ADD  CONSTRAINT [DF_FinanzasEventosBancosResu_STS5]  DEFAULT ((0)) FOR [STS5]