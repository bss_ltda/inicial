﻿CREATE TABLE [dbo].[CTCampResultB](
	[IdCTCampResultB] [int] IDENTITY(1,1) NOT NULL,
	[IdCTCampaign] [int] NULL,
	[Persona] [numeric](18, 0) NULL,
	[PedidosLogrados] [int] NULL,
	[ValorTotal] [float] NULL,
	[PuntosLogrados] [int] NULL,
	[Premio] [nvarchar](255) NOT NULL,
	[PuntosParaPremio] [smallint] NULL,
	[MinimoPedidos] [smallint] NULL,
	[StatusPedido] [nvarchar](50) NULL,
	[Accion] [nvarchar](50) NULL,
	[Color] [nchar](50) NULL,
 CONSTRAINT [PK_CTCampResultB] PRIMARY KEY CLUSTERED 
(
	[IdCTCampResultB] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]