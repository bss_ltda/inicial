﻿CREATE TABLE [dbo].[Login](
	[Cedula] [varchar](50) NULL,
	[Nombre] [varchar](50) NULL,
	[Rol] [varchar](50) NULL,
	[Correo] [varchar](50) NULL,
	[Clave] [varchar](50) NULL
) ON [PRIMARY]