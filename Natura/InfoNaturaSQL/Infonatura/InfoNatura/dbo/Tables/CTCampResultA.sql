﻿CREATE TABLE [dbo].[CTCampResultA](
	[IdCTCampResultA] [int] IDENTITY(1,1) NOT NULL,
	[IdCTCampaign] [int] NOT NULL,
	[Codigo_Revendedor] [int] NULL,
	[PuntosLogrados] [int] NULL,
	[Facturacion] [float] NULL,
	[Premio] [nvarchar](80) NOT NULL,
	[PuntosParaPremio] [smallint] NULL,
	[CiclosCompra] [smallint] NULL,
	[CiclosPremio] [smallint] NULL,
	[StatusPedido] [nvarchar](50) NULL,
	[Accion] [nvarchar](50) NULL,
	[Color] [nchar](50) NULL,
 CONSTRAINT [PK_CTCampResultA] PRIMARY KEY CLUSTERED 
(
	[IdCTCampResultA] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]