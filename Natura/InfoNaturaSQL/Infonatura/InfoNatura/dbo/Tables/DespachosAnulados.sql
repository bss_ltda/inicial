﻿CREATE TABLE [dbo].[DespachosAnulados](
	[IdCambio] [int] IDENTITY(1,1) NOT NULL,
	[Pedido] [varchar](100) NULL,
	[Usuario] [varchar](100) NULL,
	[Fecha] [datetime] NOT NULL,
 CONSTRAINT [PK_DespachosAnulados1] PRIMARY KEY CLUSTERED 
(
	[IdCambio] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]