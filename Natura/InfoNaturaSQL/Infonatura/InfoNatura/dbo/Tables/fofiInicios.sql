﻿CREATE TABLE [dbo].[fofiInicios](
	[iniCiclo] [float] NULL,
	[iniCodSector] [float] NULL,
	[iniObjetivo] [float] NULL,
	[iniReal] [float] NULL,
	[CRTDAT] [datetime] NOT NULL,
	[UPDUSR] [nvarchar](50) NULL,
	[UPDDAT] [datetime] NULL
) ON [PRIMARY]