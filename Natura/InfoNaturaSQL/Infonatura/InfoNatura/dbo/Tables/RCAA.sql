﻿CREATE TABLE [dbo].[RCAA](
	[ASTS] [nvarchar](1) NOT NULL,
	[AAPP] [nvarchar](15) NOT NULL,
	[AMOD] [nvarchar](15) NOT NULL,
	[ADESC] [nvarchar](60) NOT NULL,
	[AUSR] [nvarchar](25) NOT NULL,
	[AAPPMNU] [nvarchar](15) NOT NULL,
	[AOPCMNU] [nvarchar](200) NOT NULL,
	[AURL] [nvarchar](200) NOT NULL,
	[ACC00] [nvarchar](1) NOT NULL,
	[ACC01] [nvarchar](1) NOT NULL,
	[ACC02] [nvarchar](1) NOT NULL,
	[ACC03] [nvarchar](1) NOT NULL,
	[ACC04] [nvarchar](1) NOT NULL,
	[ACC05] [nvarchar](1) NOT NULL,
	[ACC06] [nvarchar](1) NOT NULL,
	[ACC07] [nvarchar](1) NOT NULL,
	[ACC08] [nvarchar](1) NOT NULL,
	[ACC09] [nvarchar](1) NOT NULL,
	[ACC10] [nvarchar](1) NOT NULL,
	[ACC11] [nvarchar](1) NOT NULL,
	[ACC12] [nvarchar](1) NOT NULL,
	[ACC13] [nvarchar](1) NOT NULL,
	[ACC14] [nvarchar](1) NOT NULL,
	[ACC15] [nvarchar](1) NOT NULL,
	[ACC16] [nvarchar](1) NOT NULL,
	[ACC17] [nvarchar](1) NOT NULL,
	[ACC18] [nvarchar](1) NOT NULL,
	[ACC19] [nvarchar](1) NOT NULL,
	[ACC20] [nvarchar](1) NOT NULL,
	[AAUT] [nvarchar](10) NOT NULL,
	[AUPD] [datetime] NOT NULL,
	[ADEFAULT] [decimal](1, 0) NOT NULL,
	[ASTYLE] [nvarchar](100) NOT NULL,
	[ABOL] [decimal](3, 0) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Default [DF_RCAA_ADESC]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_ADESC]  DEFAULT ('') FOR [ADESC]
GO
/****** Object:  Default [DF_RCAA_AAPPMNU]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_AAPPMNU]  DEFAULT ('') FOR [AAPPMNU]
GO
/****** Object:  Default [DF_RCAA_AOPCMNU]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_AOPCMNU]  DEFAULT ('') FOR [AOPCMNU]
GO
/****** Object:  Default [DF_RCAA_AURL]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_AURL]  DEFAULT ('') FOR [AURL]
GO
/****** Object:  Default [DF_RCAA_ACC00]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_ACC00]  DEFAULT ('') FOR [ACC00]
GO
/****** Object:  Default [DF_RCAA_ACC01]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_ACC01]  DEFAULT ('') FOR [ACC01]
GO
/****** Object:  Default [DF_RCAA_ACC02]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_ACC02]  DEFAULT ('') FOR [ACC02]
GO
/****** Object:  Default [DF_RCAA_ACC03]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_ACC03]  DEFAULT ('') FOR [ACC03]
GO
/****** Object:  Default [DF_RCAA_ACC04]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_ACC04]  DEFAULT ('') FOR [ACC04]
GO
/****** Object:  Default [DF_RCAA_ACC05]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_ACC05]  DEFAULT ('') FOR [ACC05]
GO
/****** Object:  Default [DF_RCAA_ACC06]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_ACC06]  DEFAULT ('') FOR [ACC06]
GO
/****** Object:  Default [DF_RCAA_ACC07]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_ACC07]  DEFAULT ('') FOR [ACC07]
GO
/****** Object:  Default [DF_RCAA_ACC08]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_ACC08]  DEFAULT ('') FOR [ACC08]
GO
/****** Object:  Default [DF_RCAA_ACC09]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_ACC09]  DEFAULT ('') FOR [ACC09]
GO
/****** Object:  Default [DF_RCAA_ACC10]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_ACC10]  DEFAULT ('') FOR [ACC10]
GO
/****** Object:  Default [DF_RCAA_ACC11]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_ACC11]  DEFAULT ('') FOR [ACC11]
GO
/****** Object:  Default [DF_RCAA_ACC12]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_ACC12]  DEFAULT ('') FOR [ACC12]
GO
/****** Object:  Default [DF_RCAA_ACC13]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_ACC13]  DEFAULT ('') FOR [ACC13]
GO
/****** Object:  Default [DF_RCAA_ACC14]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_ACC14]  DEFAULT ('') FOR [ACC14]
GO
/****** Object:  Default [DF_RCAA_ACC15]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_ACC15]  DEFAULT ('') FOR [ACC15]
GO
/****** Object:  Default [DF_RCAA_ACC16]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_ACC16]  DEFAULT ('') FOR [ACC16]
GO
/****** Object:  Default [DF_RCAA_ACC17]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_ACC17]  DEFAULT ('') FOR [ACC17]
GO
/****** Object:  Default [DF_RCAA_ACC18]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_ACC18]  DEFAULT ('') FOR [ACC18]
GO
/****** Object:  Default [DF_RCAA_ACC19]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_ACC19]  DEFAULT ('') FOR [ACC19]
GO
/****** Object:  Default [DF_RCAA_ACC20]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_ACC20]  DEFAULT ('') FOR [ACC20]
GO
/****** Object:  Default [DF_RCAA_AAUT]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_AAUT]  DEFAULT ('') FOR [AAUT]
GO
/****** Object:  Default [DF_RCAA_AUPD]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_AUPD]  DEFAULT (getdate()) FOR [AUPD]
GO
/****** Object:  Default [DF_RCAA_ADEFAULT]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_ADEFAULT]  DEFAULT ((0)) FOR [ADEFAULT]
GO
/****** Object:  Default [DF_RCAA_ASTYLE]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_ASTYLE]  DEFAULT ('') FOR [ASTYLE]
GO
/****** Object:  Default [DF_RCAA_ABOL]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAA] ADD  CONSTRAINT [DF_RCAA_ABOL]  DEFAULT ((0)) FOR [ABOL]