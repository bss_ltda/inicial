﻿CREATE TABLE [dbo].[FinanzasResuTot](
	[Mes] [smallint] NULL,
	[Tercero] [nvarchar](255) NULL,
	[CentroCosto] [nvarchar](255) NULL,
	[SubCentroCosto] [nvarchar](255) NULL,
	[Proyecto] [nvarchar](255) NULL,
	[Actual] [float] NULL,
	[Ppto] [float] NULL,
	[Anterior] [float] NULL,
	[RO] [float] NULL,
	[AcumActual] [float] NULL,
	[AcumPpto] [float] NULL,
	[AcumAnterior] [float] NULL,
	[AcumRO] [float] NULL,
	[AcumActualY] [float] NULL,
	[AcumPptoY] [float] NULL,
	[AcumAnteriorY] [float] NULL,
	[AcumROY] [float] NULL
) ON [PRIMARY]
GO
/****** Object:  Default [DF_prueba_valor0A]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasResuTot] ADD  CONSTRAINT [DF_prueba_valor0A]  DEFAULT ((0)) FOR [Actual]
GO
/****** Object:  Default [DF_prueba_valor1A]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasResuTot] ADD  CONSTRAINT [DF_prueba_valor1A]  DEFAULT ((0)) FOR [Ppto]
GO
/****** Object:  Default [DF_prueba_valor2A]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasResuTot] ADD  CONSTRAINT [DF_prueba_valor2A]  DEFAULT ((0)) FOR [Anterior]
GO
/****** Object:  Default [DF_prueba_valor3A]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasResuTot] ADD  CONSTRAINT [DF_prueba_valor3A]  DEFAULT ((0)) FOR [RO]
GO
/****** Object:  Default [DF_prueba_valor4A]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasResuTot] ADD  CONSTRAINT [DF_prueba_valor4A]  DEFAULT ((0)) FOR [AcumActual]
GO
/****** Object:  Default [DF_prueba_valor5A]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasResuTot] ADD  CONSTRAINT [DF_prueba_valor5A]  DEFAULT ((0)) FOR [AcumPpto]
GO
/****** Object:  Default [DF_prueba_valor6A]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasResuTot] ADD  CONSTRAINT [DF_prueba_valor6A]  DEFAULT ((0)) FOR [AcumAnterior]
GO
/****** Object:  Default [DF_prueba_valor7A]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasResuTot] ADD  CONSTRAINT [DF_prueba_valor7A]  DEFAULT ((0)) FOR [AcumRO]
GO
/****** Object:  Default [DF_prueba_valor8A]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasResuTot] ADD  CONSTRAINT [DF_prueba_valor8A]  DEFAULT ((0)) FOR [AcumActualY]
GO
/****** Object:  Default [DF_prueba_valor9A]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasResuTot] ADD  CONSTRAINT [DF_prueba_valor9A]  DEFAULT ((0)) FOR [AcumPptoY]
GO
/****** Object:  Default [DF_prueba_valor10A]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasResuTot] ADD  CONSTRAINT [DF_prueba_valor10A]  DEFAULT ((0)) FOR [AcumAnteriorY]
GO
/****** Object:  Default [DF_prueba_valor11A]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[FinanzasResuTot] ADD  CONSTRAINT [DF_prueba_valor11A]  DEFAULT ((0)) FOR [AcumROY]