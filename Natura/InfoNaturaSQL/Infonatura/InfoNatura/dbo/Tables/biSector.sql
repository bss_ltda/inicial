﻿CREATE TABLE [dbo].[biSector](
	[CodGerencia] [nchar](30) NOT NULL,
	[CodSector] [nchar](30) NOT NULL,
	[Sector] [nvarchar](50) NULL,
 CONSTRAINT [PK_Sector] PRIMARY KEY CLUSTERED 
(
	[CodGerencia] ASC,
	[CodSector] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  ForeignKey [FK_biSector_biGerencia]    Script Date: 09/13/2018 18:30:19 ******/
ALTER TABLE [dbo].[biSector]  WITH CHECK ADD  CONSTRAINT [FK_biSector_biGerencia] FOREIGN KEY([CodGerencia])
REFERENCES [dbo].[biGerencia] ([CodGerencia])
GO

ALTER TABLE [dbo].[biSector] CHECK CONSTRAINT [FK_biSector_biGerencia]