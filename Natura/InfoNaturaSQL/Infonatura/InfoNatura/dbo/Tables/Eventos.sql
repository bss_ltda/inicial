﻿CREATE TABLE [dbo].[Eventos](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[Macro] [nchar](50) NOT NULL,
	[Subcategoria] [nvarchar](50) NULL,
	[Parametros] [varchar](500) NULL,
	[FechaCreacion] [datetime] NULL,
	[FechaEjecucion] [datetime] NULL,
	[Estado] [nchar](50) NULL,
	[Notas] [nvarchar](250) NULL,
	[Mensajes] [nvarchar](max) NULL,
	[Usuario] [nchar](25) NULL,
	[Sts1] [decimal](1, 0) NOT NULL,
	[Sts2] [decimal](1, 0) NOT NULL,
	[Sts3] [decimal](1, 0) NOT NULL,
	[Sts4] [decimal](1, 0) NOT NULL,
	[Sts5] [decimal](1, 0) NOT NULL,
	[Url] [nvarchar](150) NULL,
	[UrlTit] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Default [DF_Eventos_Parametros]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[Eventos] ADD  CONSTRAINT [DF_Eventos_Parametros]  DEFAULT ('') FOR [Parametros]
GO
/****** Object:  Default [DF_Eventos_FechaCreacion]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[Eventos] ADD  CONSTRAINT [DF_Eventos_FechaCreacion]  DEFAULT (getdate()) FOR [FechaCreacion]
GO
/****** Object:  Default [DF_Eventos_Estado]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[Eventos] ADD  CONSTRAINT [DF_Eventos_Estado]  DEFAULT ('') FOR [Estado]
GO
/****** Object:  Default [DF_Eventos_Sts1]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[Eventos] ADD  CONSTRAINT [DF_Eventos_Sts1]  DEFAULT ((0)) FOR [Sts1]
GO
/****** Object:  Default [DF_Eventos_Sts2]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[Eventos] ADD  CONSTRAINT [DF_Eventos_Sts2]  DEFAULT ((0)) FOR [Sts2]
GO
/****** Object:  Default [DF_Eventos_Sts3]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[Eventos] ADD  CONSTRAINT [DF_Eventos_Sts3]  DEFAULT ((0)) FOR [Sts3]
GO
/****** Object:  Default [DF_Eventos_Sts4]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[Eventos] ADD  CONSTRAINT [DF_Eventos_Sts4]  DEFAULT ((0)) FOR [Sts4]
GO
/****** Object:  Default [DF_Eventos_Sts5]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[Eventos] ADD  CONSTRAINT [DF_Eventos_Sts5]  DEFAULT ((0)) FOR [Sts5]