﻿CREATE TABLE [dbo].[LibPed7Millon](
	[FechaPedido] [nvarchar](255) NULL,
	[Pedido] [float] NULL,
	[CodPersona] [float] NULL,
	[NombreRevendedor] [nvarchar](255) NULL,
	[EstructuraPadre] [nvarchar](255) NULL,
	[Estructura] [nvarchar](255) NULL,
	[Estado] [varchar](6) NOT NULL,
	[DeudaTotal] [float] NULL
) ON [PRIMARY]