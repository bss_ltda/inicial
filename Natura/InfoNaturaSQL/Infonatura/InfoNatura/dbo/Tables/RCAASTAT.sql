﻿CREATE TABLE [dbo].[RCAASTAT](
	[AAPP] [nvarchar](15) NOT NULL,
	[AMOD] [nvarchar](15) NOT NULL,
	[ADESC] [nvarchar](150) NOT NULL,
	[AUSR] [nvarchar](25) NOT NULL,
	[AURL] [nvarchar](2000) NOT NULL,
	[ADATE] [datetime] NOT NULL,
	[AIPADDR] [nvarchar](25) NOT NULL,
	[ARESOL] [nvarchar](50) NULL,
	[ADEVICE] [nvarchar](50) NULL,
	[ANAVIGATOR] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Default [DF_RCAASTAT_AAPP]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAASTAT] ADD  CONSTRAINT [DF_RCAASTAT_AAPP]  DEFAULT ('') FOR [AAPP]
GO
/****** Object:  Default [DF_RCAASTAT_AMOD]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAASTAT] ADD  CONSTRAINT [DF_RCAASTAT_AMOD]  DEFAULT ('') FOR [AMOD]
GO
/****** Object:  Default [DF_RCAASTAT_ADESC]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAASTAT] ADD  CONSTRAINT [DF_RCAASTAT_ADESC]  DEFAULT ('') FOR [ADESC]
GO
/****** Object:  Default [DF_RCAASTAT_AUSR]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAASTAT] ADD  CONSTRAINT [DF_RCAASTAT_AUSR]  DEFAULT ('') FOR [AUSR]
GO
/****** Object:  Default [DF_RCAASTAT_AURL]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAASTAT] ADD  CONSTRAINT [DF_RCAASTAT_AURL]  DEFAULT ('') FOR [AURL]
GO
/****** Object:  Default [DF_RCAASTAT_ADATE]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RCAASTAT] ADD  CONSTRAINT [DF_RCAASTAT_ADATE]  DEFAULT (getdate()) FOR [ADATE]