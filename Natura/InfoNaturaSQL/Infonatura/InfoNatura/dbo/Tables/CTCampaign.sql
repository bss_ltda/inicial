﻿CREATE TABLE [dbo].[CTCampaign](
	[IdCTCampaign] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [nvarchar](10) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[Descripcion] [nvarchar](2000) NOT NULL,
	[Ciclo] [nchar](7) NULL,
	[Puntos] [smallint] NULL,
	[Archivar] [nchar](1) NOT NULL,
	[Fecha] [datetime] NULL,
 CONSTRAINT [PK_CTCampaign] PRIMARY KEY CLUSTERED 
(
	[IdCTCampaign] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_CTCampaign_Archivar]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[CTCampaign] ADD  CONSTRAINT [DF_CTCampaign_Archivar]  DEFAULT ('0') FOR [Archivar]
GO
/****** Object:  Default [DF_CTCampaign_Fecha]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[CTCampaign] ADD  CONSTRAINT [DF_CTCampaign_Fecha]  DEFAULT (getdate()) FOR [Fecha]