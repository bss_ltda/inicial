﻿CREATE TABLE [dbo].[GCSKU](
	[SKLINEA] [varchar](100) NULL,
	[SKCATEGORIA] [varchar](100) NULL,
	[SKID] [float] NOT NULL,
	[SKDESC] [varchar](100) NULL,
	[SKCPC1] [float] NULL,
	[SKCPC2] [float] NULL,
	[SKCPC3] [float] NULL,
	[SKCPC4] [float] NULL,
	[SKCSP1] [float] NULL,
	[SKCSP2] [float] NULL,
	[SKCSP3] [float] NULL,
	[SKCSP4] [float] NULL,
	[SKCRC1] [float] NULL,
	[SKCRC2] [float] NULL,
	[SKCRC3] [float] NULL,
	[SKCRC4] [float] NULL,
	[SKCSR1] [float] NULL,
	[SKCSR2] [float] NULL,
	[SKCSR3] [float] NULL,
	[SKCSR4] [float] NULL,
	[SKACTP] [float] NULL,
	[SKACTR] [float] NULL,
 CONSTRAINT [PK_GCSKU] PRIMARY KEY CLUSTERED 
(
	[SKID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]