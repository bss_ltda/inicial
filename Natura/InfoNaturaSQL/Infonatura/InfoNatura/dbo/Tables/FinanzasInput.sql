﻿CREATE TABLE [dbo].[FinanzasInput](
	[Id] [int] NOT NULL,
	[TipoDato] [smallint] NULL,
	[Mes] [smallint] NULL,
	[Detalle] [nvarchar](255) NULL,
	[Tercero] [float] NULL,
	[TerceroNombre] [nvarchar](255) NULL,
	[Saldo] [float] NULL,
	[CentroCosto] [smallint] NULL,
	[SubCentroCosto] [smallint] NULL,
	[Proyecto] [smallint] NULL,
	[CentroCostoNombre] [nvarchar](255) NULL,
	[SubCentroCostoNombre] [nvarchar](255) NULL,
	[ProyectoNombre] [nvarchar](255) NULL
) ON [PRIMARY]