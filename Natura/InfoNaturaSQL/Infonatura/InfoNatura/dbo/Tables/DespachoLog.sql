﻿CREATE TABLE [dbo].[DespachoLog](
	[Transportadora] [nvarchar](255) NULL,
	[Estiba] [nvarchar](255) NULL,
	[Caja] [nvarchar](255) NULL,
	[Fecha] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Default [DF_DespachoLog_Fecha]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[DespachoLog] ADD  CONSTRAINT [DF_DespachoLog_Fecha]  DEFAULT (getdate()) FOR [Fecha]