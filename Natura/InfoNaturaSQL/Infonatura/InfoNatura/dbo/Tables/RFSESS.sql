﻿CREATE TABLE [dbo].[RFSESS](
	[IDSESS] [decimal](6, 0) NOT NULL,
	[TIPO] [nvarchar](25) NULL,
	[PARAM] [nvarchar](25) NULL,
	[VALOR] [nvarchar](2500) NULL,
	[CRTDAT] [datetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Default [DF_RFSESS_CRTDAT]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[RFSESS] ADD  CONSTRAINT [DF_RFSESS_CRTDAT]  DEFAULT (getdate()) FOR [CRTDAT]