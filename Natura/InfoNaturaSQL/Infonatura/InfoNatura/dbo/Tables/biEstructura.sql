﻿CREATE TABLE [dbo].[biEstructura](
	[codGerencia] [float] NULL,
	[codSector] [float] NOT NULL,
	[Estructura] [nvarchar](255) NULL,
	[EstructuraResponsable] [nvarchar](150) NULL,
	[CorreoElectronico] [nvarchar](150) NULL,
 CONSTRAINT [PK_aEstructura] PRIMARY KEY CLUSTERED 
(
	[codSector] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]