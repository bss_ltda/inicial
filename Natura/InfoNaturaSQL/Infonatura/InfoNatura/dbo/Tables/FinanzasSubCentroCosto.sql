﻿CREATE TABLE [dbo].[FinanzasSubCentroCosto](
	[CentroCosto] [float] NULL,
	[SubCentroCosto] [float] NULL,
	[SubCentroCostoNombre] [nvarchar](255) NULL
) ON [PRIMARY]