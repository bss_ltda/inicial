﻿CREATE TABLE [dbo].[fofiBaseLiquiTit](
	[Batch] [int] NOT NULL,
	[Ciclo] [nvarchar](255) NULL,
	[Sector] [nvarchar](255) NULL,
	[GerenciaVentas] [nvarchar](255) NULL,
	[SectorNombre] [nvarchar](255) NULL,
	[NombreResponsable] [nvarchar](255) NULL,
	[Cedula] [nvarchar](255) NULL,
	[Campo05] [nvarchar](255) NULL,
	[Campo06] [nvarchar](255) NULL,
	[Campo07] [nvarchar](255) NULL,
	[Campo08] [nvarchar](255) NULL,
	[Campo09] [nvarchar](255) NULL,
	[Campo10] [nvarchar](255) NULL,
	[Campo11] [nvarchar](255) NULL,
	[Campo12] [nvarchar](255) NULL,
	[Campo13] [nvarchar](255) NULL,
	[Campo14] [nvarchar](255) NULL,
	[Campo15] [nvarchar](255) NULL,
	[Campo16] [nvarchar](255) NULL,
	[Campo17] [nvarchar](255) NULL,
	[Campo18] [nvarchar](255) NULL,
	[Campo19] [nvarchar](255) NULL,
	[Campo20] [nvarchar](255) NULL,
 CONSTRAINT [PK_fofiBaseLiquiTit] PRIMARY KEY CLUSTERED 
(
	[Batch] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_fofiBaseLiquiTit_Ciclo]    Script Date: 09/13/2018 18:30:17 ******/
ALTER TABLE [dbo].[fofiBaseLiquiTit] ADD  CONSTRAINT [DF_fofiBaseLiquiTit_Ciclo]  DEFAULT ('Ciclo') FOR [Ciclo]
GO
/****** Object:  Default [DF_fofiBaseLiquiTit_Sector]    Script Date: 09/13/2018 18:30:17 ******/
ALTER TABLE [dbo].[fofiBaseLiquiTit] ADD  CONSTRAINT [DF_fofiBaseLiquiTit_Sector]  DEFAULT ('Código Sector') FOR [Sector]
GO
/****** Object:  Default [DF_fofiBaseLiquiTit_GerenciaVentas]    Script Date: 09/13/2018 18:30:17 ******/
ALTER TABLE [dbo].[fofiBaseLiquiTit] ADD  CONSTRAINT [DF_fofiBaseLiquiTit_GerenciaVentas]  DEFAULT ('Gerencia de Ventas') FOR [GerenciaVentas]
GO
/****** Object:  Default [DF_fofiBaseLiquiTit_SectorNombre]    Script Date: 09/13/2018 18:30:17 ******/
ALTER TABLE [dbo].[fofiBaseLiquiTit] ADD  CONSTRAINT [DF_fofiBaseLiquiTit_SectorNombre]  DEFAULT ('Sector') FOR [SectorNombre]
GO
/****** Object:  Default [DF_fofiBaseLiquiTit_NombreResponsable]    Script Date: 09/13/2018 18:30:17 ******/
ALTER TABLE [dbo].[fofiBaseLiquiTit] ADD  CONSTRAINT [DF_fofiBaseLiquiTit_NombreResponsable]  DEFAULT ('Gerente de Relaciones') FOR [NombreResponsable]
GO
/****** Object:  Default [DF_fofiBaseLiquiTit_Cedula]    Script Date: 09/13/2018 18:30:17 ******/
ALTER TABLE [dbo].[fofiBaseLiquiTit] ADD  CONSTRAINT [DF_fofiBaseLiquiTit_Cedula]  DEFAULT ('Cedula') FOR [Cedula]