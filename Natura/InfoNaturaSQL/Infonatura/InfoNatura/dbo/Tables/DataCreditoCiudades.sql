﻿CREATE TABLE [dbo].[DataCreditoCiudades](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[DEPARTAMENTO] [nvarchar](255) NULL,
	[CIUDAD] [nvarchar](255) NULL,
	[DPTO_DANE] [nvarchar](255) NULL,
	[CIUDAD_DANE] [nvarchar](255) NULL
) ON [PRIMARY]