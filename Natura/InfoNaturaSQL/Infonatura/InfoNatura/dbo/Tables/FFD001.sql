﻿CREATE TABLE [dbo].[FFD001](
	[XTable] [nvarchar](50) NOT NULL,
	[XName] [nvarchar](50) NOT NULL,
	[XValue] [nvarchar](max) NULL,
	[XType] [int] NULL,
	[XTypeD] [nvarchar](50) NOT NULL,
	[XDefinedSize] [int] NULL,
	[XNumericScale] [int] NULL
) ON [PRIMARY]