﻿CREATE TABLE [dbo].[TranspEntrega](
	[idEntrega] [int] IDENTITY(1,1) NOT NULL,
	[Ciclo] [decimal](6, 0) NULL,
	[idTransportadora] [int] NULL,
	[Transportadora] [nvarchar](255) NULL,
	[FechaDespacho] [datetime] NULL,
	[TipoDespacho] [nvarchar](255) NULL,
	[CuentaGERA] [float] NULL,
	[DescripcionCuenta] [nvarchar](255) NULL,
	[CtoCostoContable] [nvarchar](255) NULL,
	[Pedido] [int] NULL,
	[NumeroGuia] [nvarchar](255) NULL,
	[CN] [float] NULL,
	[Destinatario] [nvarchar](255) NULL,
	[Direccion] [nvarchar](255) NULL,
	[CiudadDANE] [nvarchar](255) NULL,
	[CiudadDestino] [nvarchar](255) NULL,
	[Departamento] [nvarchar](255) NULL,
	[idGerencia] [nchar](10) NULL,
	[Gerencia] [nvarchar](255) NULL,
	[idSector] [nchar](10) NULL,
	[Sector] [nvarchar](255) NULL,
	[Celular] [nvarchar](25) NULL,
	[Telefono] [nvarchar](25) NULL,
	[Cajas] [float] NULL,
	[Regional] [nvarchar](255) NULL,
	[TiempoEntrega] [float] NULL,
	[Estado] [nvarchar](255) NULL,
	[EstadoReal] [nvarchar](255) NULL,
	[FechaPrevistaEntrega] [datetime] NULL,
	[DiasAtraso] [float] NULL,
	[FechaEntrega] [datetime] NULL,
	[Cumplimiento] [float] NULL,
	[TipoNovedad] [nvarchar](255) NULL,
	[FechaNovedad] [datetime] NULL,
	[Novedades] [nvarchar](max) NULL,
	[Tarifas] [nvarchar](255) NULL,
	[Alertas] [nvarchar](max) NULL,
	[ConNovedad] [bit] NULL,
	[idTrasnportadoraT] [int] NULL,
	[FechaCargueTransp] [datetime] NULL,
 CONSTRAINT [PK_TranspSeguimiento] PRIMARY KEY CLUSTERED 
(
	[idEntrega] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_TranspEntrega_FechaCargueTransp]    Script Date: 09/13/2018 18:30:18 ******/
ALTER TABLE [dbo].[TranspEntrega] ADD  CONSTRAINT [DF_TranspEntrega_FechaCargueTransp]  DEFAULT (getdate()) FOR [FechaCargueTransp]