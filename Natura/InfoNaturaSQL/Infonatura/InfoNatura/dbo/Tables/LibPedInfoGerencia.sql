﻿CREATE TABLE [dbo].[LibPedInfoGerencia](
	[Pedido] [float] NULL,
	[CodPersona] [float] NULL,
	[NombreRevendedor] [nvarchar](255) NULL,
	[TipoPendencia] [nvarchar](255) NULL,
	[EstructuraPadre] [nvarchar](255) NULL,
	[Estructura] [nvarchar](255) NULL
) ON [PRIMARY]