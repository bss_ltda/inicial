﻿-- =============================================
-- Author:		FM
-- Create date: 2018-04-30
-- Description:	Quitar caracteres no numericos
-- =============================================
CREATE PROCEDURE [dbo].[SP_ETL_CNOs]

-- EXEC [dbo].[SP_ETL_CNOs]


AS
BEGIN
	SET NOCOUNT ON;
	SET ANSI_NULLS ON;
	SET QUOTED_IDENTIFIER ON;

BEGIN TRY
	DECLARE @ETL VARCHAR(100) = 'SP_ETL_CNOs';
	DECLARE @SQL_BULK VARCHAR(MAX);
	DECLARE @mensajeProceso AS VARCHAR(MAX);


	SET @mensajeProceso = 'Sin Procesar el SP';

	TRUNCATE TABLE geraCNOsW;

    DECLARE @rutaArchivo varchar(255) = '\\10.21.3.15\xm\CNOs\CNOs.txt'
	--set @rutaArchivo = 'D:\Sitios\xm\CNOs\CNOs.txt'
	--DECLARE @rutaArchivo varchar(255) = 'D:\Natura\SR18-003B Seguimiento de Campañas\geraCNOs1.csv'

	SET @SQL_BULK = 'BULK INSERT geraCNOsW FROM ''' + @rutaArchivo + ''' WITH
        (
        FIRSTROW = 2,
        FIELDTERMINATOR = ''\t'',
        ROWTERMINATOR = ''\n'',
		CODEPAGE = ''ACP''
        )'

		PRINT @SQL_BULK

	EXEC (@SQL_BULK);
	SET @mensajeProceso = 'Actualizando...';
UPDATE GeraCNOs SET 
      CodigoEstructuraPadre                        = w.CodigoEstructuraPadre
    , EstructuraPadre                              = w.EstructuraPadre
    , CodigoEstructura                             = w.CodigoEstructura
    , Estructura                                   = w.Estructura
    , Grupo                                        = w.Grupo
    , Codigo                                       = w.Codigo
    , ResponsableGrupo                             = w.ResponsableGrupo
    , Nivel                                        = w.Nivel
    , Disponibles                                  = w.Disponibles
    , Activas                                      = w.Activas
    , DisponiblesGrupoDirecto                      = w.DisponiblesGrupoDirecto
    , DisponiblesGrupoCnof                         = w.DisponiblesGrupoCnof
    , ActivasGrupoDirecto                          = w.ActivasGrupoDirecto
    , ActivasFrecuentesGrupoDirecto                = w.ActivasFrecuentesGrupoDirecto
    , ActivasGrupoCnof                             = w.ActivasGrupoCnof
    , PorcActividad                                = dbo.PreparaNumero(w.PorcActividad, ',' )
    , PorcActividadFrecuenteGrupoDirecto           = dbo.PreparaNumero(w.PorcActividadFrecuenteGrupoDirecto, ',' )
    , SaldoDelGrupo                                = w.SaldoDelGrupo
    , CantCNIFEnElGrupo                            = w.CantCNIFEnElGrupo
    , GananciaXDisponiblesActividad                = dbo.PreparaNumero(w.GananciaXDisponiblesActividad, ',' )
    , GananciaXSaldoDelGrupo                       = dbo.PreparaNumero(w.GananciaXSaldoDelGrupo, ',' )
    , ActivasGruposRelacionadosCNOs                = w.ActivasGruposRelacionadosCNOs
    , GananciaGruposRelacionadosCNOs               = dbo.PreparaNumero(w.GananciaGruposRelacionadosCNOs, ',' )
    , BonoCno                                      = w.BonoCno
    , TotalGanancia                                = dbo.PreparaNumero(w.TotalGanancia, ',' )
    , Observaciones                                = w.Observaciones
    , GruposRelacionadosCNOs                       = w.GruposRelacionadosCNOs
    , GruposRelacionadosCNOFs                      = w.GruposRelacionadosCNOFs
    , ProductividadXActivas                        = dbo.PreparaNumero(w.ProductividadXActivas, ',' )
    , RecetaDelGrupo                               = dbo.PreparaNumero(w.RecetaDelGrupo, ',' )   
FROM GeraCNOs r Inner Join GeraCNOsW w on r.Grupo = w.Grupo ;
SET @mensajeProceso = 'Creando...';
INSERT INTO GeraCNOs ( 
      CodigoEstructuraPadre
    , EstructuraPadre
    , CodigoEstructura
    , Estructura
    , Grupo
    , Codigo
    , ResponsableGrupo
    , Nivel
    , Disponibles
    , Activas
    , DisponiblesGrupoDirecto
    , DisponiblesGrupoCnof
    , ActivasGrupoDirecto
    , ActivasFrecuentesGrupoDirecto
    , ActivasGrupoCnof
    , PorcActividad
    , PorcActividadFrecuenteGrupoDirecto
    , SaldoDelGrupo
    , CantCNIFEnElGrupo
    , GananciaXDisponiblesActividad
    , GananciaXSaldoDelGrupo
    , ActivasGruposRelacionadosCNOs
    , GananciaGruposRelacionadosCNOs
    , BonoCno
    , TotalGanancia
    , Observaciones
    , GruposRelacionadosCNOs
    , GruposRelacionadosCNOFs
    , ProductividadXActivas
    , RecetaDelGrupo
)
SELECT 
      w.CodigoEstructuraPadre
    , w.EstructuraPadre
    , w.CodigoEstructura
    , w.Estructura
    , w.Grupo
    , w.Codigo
    , w.ResponsableGrupo
    , w.Nivel
    , w.Disponibles
    , w.Activas
    , w.DisponiblesGrupoDirecto
    , w.DisponiblesGrupoCnof
    , w.ActivasGrupoDirecto
    , w.ActivasFrecuentesGrupoDirecto
    , w.ActivasGrupoCnof
    , dbo.PreparaNumero(w.PorcActividad, ',' )
    , dbo.PreparaNumero(w.PorcActividadFrecuenteGrupoDirecto, ',' )
    , w.SaldoDelGrupo
    , w.CantCNIFEnElGrupo
    , dbo.PreparaNumero(w.GananciaXDisponiblesActividad, ',' )
    , dbo.PreparaNumero(w.GananciaXSaldoDelGrupo, ',' )
    , w.ActivasGruposRelacionadosCNOs
    , dbo.PreparaNumero(w.GananciaGruposRelacionadosCNOs, ',' )
    , w.BonoCno
    , dbo.PreparaNumero(w.TotalGanancia, ',' )
    , w.Observaciones
    , w.GruposRelacionadosCNOs
    , w.GruposRelacionadosCNOFs
    , dbo.PreparaNumero(w.ProductividadXActivas, ',' )
    , dbo.PreparaNumero(w.RecetaDelGrupo, ',' )
 FROM GeraCNOsW w Left Join GeraCNOs r on w.Grupo = r.Grupo
 WHERE r.Grupo Is Null ;
 
	 -- UPDATE geraCNOs SET  
	 --  PorcActividad                        = dbo.PreparaNumero(PorcActividad                       , ',' )
	 --, PorcActividadFrecuenteGrupoDirecto   = dbo.PreparaNumero(PorcActividadFrecuenteGrupoDirecto  , ',' )
	 --, GananciaXDisponiblesActividad        = dbo.PreparaNumero(GananciaXDisponiblesActividad       , ',' )
	 --, GananciaXSaldoDelGrupo               = dbo.PreparaNumero(GananciaXSaldoDelGrupo              , ',' )
	 --, GananciaGruposRelacionadosCNOs       = dbo.PreparaNumero(GananciaGruposRelacionadosCNOs      , ',' )
	 --, TotalGanancia                        = dbo.PreparaNumero(TotalGanancia                       , ',' )
	 --, ProductividadXActivas                = dbo.PreparaNumero(ProductividadXActivas               , ',' )
	 --, RecetaDelGrupo                       = dbo.PreparaNumero(RecetaDelGrupo                      , ',' );	

	UPDATE RFPARAM SET 
	    CCMNDT = GETDATE()
	  , CCUDC2 = 1
	  , CCNOT2 = @rutaArchivo
	  , CCNOTE = 'OK'
	WHERE CCTABL = 'ETL_FTP' AND CCCODE = 'gera' AND CCCODE2 = @ETL ;

SELECT 'OK' AS 'MensajeProceso';
END TRY
BEGIN CATCH 
	SET @mensajeProceso = 'Se ha generado un error en la Estructura del BULK-INSERT: Linea#:' + CAST (ERROR_LINE() AS VARCHAR) + ' - Mensaje de Error:' + ERROR_MESSAGE();				
	UPDATE RFPARAM SET 
	    CCMNDT = GETDATE()
	  , CCUDC2 = -1
	  , CCNOT2 = @rutaArchivo	  
	  , CCNOTE = @mensajeProceso
	WHERE CCTABL = 'ETL_FTP' AND CCCODE = 'gera' AND CCCODE2 = @ETL;	
	SELECT @mensajeProceso AS 'MensajeProceso';
END CATCH
END