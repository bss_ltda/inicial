﻿-- =============================================
-- Author:		FM
-- Create date: 2018-09-03
-- Description:	ETL REVISTAS
-- =============================================
CREATE PROCEDURE [dbo].[SP_ETL_RevistasEnvia]
AS
BEGIN
	SET NOCOUNT ON;
BEGIN TRY

	DECLARE @SQL_BULK VARCHAR(MAX);
	DECLARE @mensajeProceso AS VARCHAR(MAX);
	DECLARE @ID_TRANSP AS int;

	SET @mensajeProceso = 'Sin Procesar el SP';
	SET @ID_TRANSP = 123665;

	TRUNCATE TABLE RevistasEnvia;
	BULK INSERT RevistasEnvia FROM '\\10.21.3.15\xm\Revistas\Envia.txt' WITH
        (
        FIRSTROW = 2,
        DATAFILETYPE ='widechar',
        FIELDTERMINATOR = '\t',
        ROWTERMINATOR = '\n',
		CODEPAGE = 'ACP'
        )

	UPDATE RevistasEnvia SET Guia = REPLACE(Guia, '''', '' );

	UPDATE RevistasTracking SET 
		  CodConsultora              = w.CodDeCn  
		, Ciudad                     = w.CiudadEntrega  
		, Depto                      = w.RegionEntrega 
		, Direccion                  = w.Direccion 
		, Telefono                   = w.TelResidencial  
		, Celular                    = w.TelMovil 
		, FechaDespacho              = w.FechaDespacho  
		, Ciclo                      = w.CicloActual  
		, Estado                     = w.Estado
		, Novedad1                   = w.Novedad
	FROM RevistasTracking r Inner Join 
		 RevistasEnvia w on r.IdTransportadora = @ID_TRANSP and r.NumeroGuia = w.Guia  ;


	INSERT INTO RevistasTracking ( 
		  NumeroGuia
		, IdTransportadora
		, CodConsultora
		, Ciudad
		, Depto
		, Direccion
		, Telefono
		, Celular
		, FechaDespacho
		, Ciclo
		, Estado
		, Novedad1
	)
	SELECT 
		  w.Guia 
		, @ID_TRANSP
		, w.CodDeCn
		, w.CiudadEntrega
		, w.RegionEntrega
		, w.Direccion
		, w.TelResidencial
		, w.TelMovil
		, w.FechaDespacho
		, w.CicloActual
		, w.Estado
		, w.Novedad
	 FROM RevistasEnvia w Left Join RevistasTracking r on r.IdTransportadora = @ID_TRANSP and w.Guia  = r.NumeroGuia
	 WHERE r.NumeroGuia Is Null ;


	SELECT 'OK' AS 'MensajeProceso';
	END TRY
	BEGIN CATCH 
		SET @mensajeProceso = 'Se ha generado un error en la Estructura del BULK-INSERT: Linea#:' + CAST (ERROR_LINE() AS VARCHAR) + ' - Mensaje de Error:' + ERROR_MESSAGE();				
		SELECT @mensajeProceso AS 'MensajeProceso';
	END CATCH
END