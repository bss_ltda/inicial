﻿-- =============================================
-- Author:		FM
-- Create date: 2018-02-22
-- Description:	ETL de revendedoras de archivos csv 
-- =============================================
CREATE PROCEDURE [dbo].[SP_ETL_Obtener_Directorios]
AS
BEGIN
SET NOCOUNT ON;

DECLARE @mensajeProceso AS VARCHAR(100);
DECLARE @fechaProceso AS DATETIME;

SET @mensajeProceso = 'Sin Procesar el SP';

BEGIN TRY
	
	SELECT
		CCNUM  AS NumID
		,CCNOT1 AS Carpeta
		,CCNOT2 AS Procedimiento
	FROM
		RFPARAM
	WHERE
		CCTABL     = 'ETL_WACTHER'
		AND CCUDC1 = 1
		
	SET @mensajeProceso = 'Consulta de Directorios PARAM realizada de Manera Correcta.';
	SELECT @mensajeProceso AS 'MensajeProceso';	
	
END TRY
BEGIN CATCH 
	SET @mensajeProceso = 'Se ha generado un Error en la Consulta de Directorios PARAM:' + ERROR_LINE() + ' - Mensaje de Error:' + ERROR_MESSAGE();		
	SELECT @mensajeProceso AS 'MensajeProceso';
END CATCH

END