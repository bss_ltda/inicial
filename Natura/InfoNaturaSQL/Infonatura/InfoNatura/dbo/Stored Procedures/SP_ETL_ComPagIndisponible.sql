﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_ETL_ComPagIndisponible]

-- SELECT * FROM geraComPagIndisponible

-- EXEC [dbo].[SP_ETL_ComPagIndisponible] 'D:\CSV\Consulta_Q844_P267340_20180614085638630.csv',15

@rutaArchivo varchar(500),
@idLogAuditoria int	
AS
BEGIN
	SET NOCOUNT ON;	
BEGIN TRY
	DECLARE @ETL VARCHAR(100) = 'SP_ETL_ComPagIndisponible';
	DECLARE @SQL_BULK VARCHAR(MAX);
	DECLARE @mensajeProceso AS VARCHAR(MAX);
	SET @mensajeProceso = 'Sin Procesar el SP';		
	
	SET @rutaArchivo = REPLACE( @rutaArchivo, 'D:\Sitios\', '\\10.21.3.15\' )
	INSERT INTO RFLOG( OPERACION, EVENTO ) VALUES('BULKINSERT', @rutaArchivo)
	
	

TRUNCATE TABLE geraComPagIndisponible;


SET @SQL_BULK = 'BULK INSERT geraComPagIndisponible FROM ''' + @rutaArchivo + ''' WITH
        (
        FIRSTROW = 2,
        FIELDTERMINATOR = '';'',
        ROWTERMINATOR = ''\n'',
		CODEPAGE = ''ACP''
        )'

		PRINT @SQL_BULK

EXEC (@SQL_BULK);  
     
	UPDATE geraComPagIndisponible
	SET    CicloActual           = (dbo.preparCiclo(CicloActual))
	     , CicloPrimerPedido     = (dbo.preparCiclo(CicloPrimerPedido) )
	     , PromedioDeDiasDeRetraso = (SELECT Data FROM Split(PromedioDeDiasDeRetraso, ',') WHERE Indice = 1)


EXEC [dbo].[SP_ETL_LOG] @ETL, @rutaArchivo, @idLogAuditoria, 1, 'OK'

SELECT 'OK' AS 'MensajeProceso';
        
END TRY
BEGIN CATCH 
	SET @mensajeProceso = 'Se ha generado un error en la Estructura del BULK-INSERT: Linea#:' + CAST (ERROR_LINE() AS VARCHAR) + ' - Mensaje de Error:' + ERROR_MESSAGE();				
	EXEC [dbo].[SP_ETL_LOG] @ETL, @rutaArchivo, @idLogAuditoria, -1, @mensajeProceso
	SELECT @mensajeProceso AS 'MensajeProceso';
END CATCH

END