﻿CREATE PROCEDURE [dbo].[SP_DataCreditoSubirArchivos] (@Id BigInt)
AS
BEGIN
	SET NOCOUNT ON;
truncate table DataCreditoPersonas;
truncate table DataCreditoTítulos;
DECLARE @Name nvarchar(50)
SELECT @Name = Codigo FROM TABLAMAESTRA WHERE TABLA = 'DIRECCION';	
Declare @comando nvarchar(400)

--insert into DataCreditoTítulos SELECT * FROM OPENROWSET('Microsoft.Jet.OLEDB.4.0','Excel 8.0;HDR=Yes;Database=D:\InfoNatura\Datos\Datacredito\Consulta.xls', 'SELECT * FROM [Títulos$]')  -->


     
set @comando = 'BULK INSERT DataCreditoTítulos FROM ''\\'+ @Name +'\InfoNaturaDatos\DataCredito\Consulta.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
EXEC sp_executesql @comando

UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Consulta' WHERE Id=@Id
set @comando = 'BULK INSERT DataCreditoPersonas FROM ''\\'+ @Name +'\InfoNaturaDatos\DataCredito\Personas1.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
EXEC sp_executesql @comando

UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Personas1' WHERE Id=@Id
set @comando = 'BULK INSERT DataCreditoPersonas FROM ''\\'+ @Name +'\InfoNaturaDatos\DataCredito\Personas2.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
EXEC sp_executesql @comando

      UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Personas2' WHERE Id=@Id
set @comando = 'BULK INSERT DataCreditoPersonas FROM ''\\'+ @Name +'\InfoNaturaDatos\DataCredito\Personas3.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
EXEC sp_executesql @comando

UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Personas3' WHERE Id=@Id
set @comando = 'BULK INSERT DataCreditoPersonas FROM ''\\'+ @Name +'\InfoNaturaDatos\DataCredito\Personas4.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
EXEC sp_executesql @comando

UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Personas4' WHERE Id=@Id
set @comando = 'BULK INSERT DataCreditoPersonas FROM ''\\'+ @Name +'\InfoNaturaDatos\DataCredito\Personas5.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
EXEC sp_executesql @comando

UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Personas5' WHERE Id=@Id
set @comando = 'BULK INSERT DataCreditoPersonas FROM ''\\'+ @Name +'\InfoNaturaDatos\DataCredito\Personas6.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
EXEC sp_executesql @comando

 UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Personas6' WHERE Id=@Id
delete from DataCreditoBTS
set @comando = 'BULK INSERT DataCreditoBTS FROM ''\\'+ @Name +'\InfoNaturaDatos\DataCredito\COBRO.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
EXEC sp_executesql @comando


truncate table [DataCreditoPlanilla] 
INSERT INTO [InfoNatura].[dbo].[DataCreditoPlanilla]
           ([Codigo]
           ,[Tipo de documento]
           ,[Noidentificación]
           ,[Numero de obligacion]
           ,[Nombre completo]
           ,[Situacion del titular]
           ,[Fecha apertura]
           ,[Fecha vencimiento]
           ,[Responsable]
           ,[Tipo de Obligacion]
           ,[Termino del Contrato]
           ,[Forma de pago]
           ,[Peridicidad]
           ,[Novedad]
           ,[Estado Origen de la cuenta]
           ,[Fecha estado origen]
           ,[Estadode la cuenta]
           ,[Fecha estado de la cuenta]
           ,[Adjetivo]
           ,[FechaAdjetivo]
           ,[Tipo de moneda]
           ,[Tipo de garantía]
           ,[Calificacion]
           ,[Edad de mora]
           ,[Valor inicial]
           ,[Valor saldo deuda]
           ,[Valor disponible]
           ,[Valor Cuota mes]
           ,[Valor saldo en  mora]
           ,[Total Cuotas]
           ,[Cuotas Canceladas]
           ,[cuotas en mora]
           ,[Clausula de permanencia]
           ,[Fecha clausula de permanencia]
           ,[Fecha limite de pago]
           ,[Fecha de pago]
           ,[Oficina de radicacion]
           ,[Ciudad de radicación]
           ,[Codigo DANE ciudad de radicacion]
           ,[Ciudad residencia]
           ,[Codigo DANE ciudad de residencia]
           ,[Deparatamento de residencia]
           ,[Direccion de residencia]
           ,[Telefono de residencia]
           ,[Ciudad laboral]
           ,[Codigo DANE ciudad laboral]
           ,[Deparatamento laboral]
           ,[Direccion Laboral]
           ,[Telefono Laboral]
           ,[Ciudad de correspondencia]
           ,[Codigo DANE ciudad correspondencia]
           ,[Deparatamento Correspondencia]
           ,[Direccion Correspondencia]
           ,[Correo electronico]
           ,[Numero Celular])
     Select CódigoPersona,'','',NúmeroPedido,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','' from DataCreditoTítulos 
 UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Insert1' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Tipo de documento]='1', [Situacion del titular]='0', Responsable='00',[Tipo de Obligacion]='2',[Termino del Contrato]='2',[Forma de pago]='0',Peridicidad='9',[Estado Origen de la cuenta]='0',Adjetivo='0',FechaAdjetivo='00000000', [Tipo de moneda]='1', [Tipo de garantía]='2', Calificacion='  ',[Valor disponible]='0000000000',[Total Cuotas]='000',[Cuotas Canceladas]='0000',[cuotas en mora]='000',[Clausula de permanencia]='000',[Fecha clausula de permanencia]='00000000', [Fecha estado de la cuenta]=  CONVERT(CHAR(10),GetDate (),102)

--DataCreditoPersonas
UPDATE  DataCreditoPersonas SET CodigoRevendedor=REPLACE(CodigoRevendedor,'.','');
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update1' WHERE Id=@Id
UPDATE DataCreditoPlanilla SET Noidentificación = [Cédula de Ciudadanía/NIT] FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CodigoRevendedor = P.Codigo
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update2' WHERE Id=@Id
UPDATE  DataCreditoPlanilla SET Noidentificación=REPLACE(Noidentificación,'.','');
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update3' WHERE Id=@Id
UPDATE DataCreditoPlanilla SET [Correo electronico] = EMail  FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update4' WHERE Id=@Id
UPDATE DataCreditoPlanilla SET [Numero Celular]  = TelMovil   FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update5' WHERE Id=@Id
UPDATE DataCreditoPlanilla SET [Telefono Laboral]  = TelResidencial    FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update6' WHERE Id=@Id
UPDATE DataCreditoPlanilla SET [Telefono de residencia]  = TelResidencial    FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update7' WHERE Id=@Id
UPDATE DataCreditoPlanilla SET [Telefono de residencia]  = TelResidencial    FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update8' WHERE Id=@Id
UPDATE DataCreditoPlanilla SET [Ciudad de radicación]  =CiudadResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'update 9' WHERE Id=@Id
UPDATE DataCreditoPlanilla SET [Ciudad de correspondencia]  = CiudadResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 10' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Ciudad residencia]  = CiudadResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 11' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Ciudad laboral]  = CiudadResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 12' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Deparatamento Correspondencia]  = RegionResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 13' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Deparatamento de residencia]  = RegionResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 14' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Deparatamento laboral]  = RegionResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 15' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Direccion Correspondencia]  =calleResidencial + '-' +  ComplementoResidencial   FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 16' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Direccion Laboral]  = calleResidencial + '-' +  ComplementoResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 17' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Direccion de residencia]  = calleResidencial + '-' +  ComplementoResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 18' WHERE Id=@Id

--DataCreditoTítulos

UPDATE DataCreditoPlanilla SET [Nombre completo] =Nombre  FROM dbo.DataCreditoTítulos AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CódigoPersona = P.Codigo 
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update19' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Fecha apertura] =CONVERT(CHAR(10), FechaPedido , 102)  FROM dbo.DataCreditoTítulos AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CódigoPersona = P.Codigo and R.NúmeroPedido=P.[Numero de obligacion]
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update20' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Fecha vencimiento] =CONVERT(CHAR(10),FechaVencimiento,102)  FROM dbo.DataCreditoTítulos AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CódigoPersona = P.Codigo and R.NúmeroPedido=P.[Numero de obligacion]
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 21' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Fecha estado origen] =CONVERT(CHAR(10),FechaPedido, 102)  FROM dbo.DataCreditoTítulos AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CódigoPersona = P.Codigo and R.NúmeroPedido=P.[Numero de obligacion]
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 22' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Valor inicial] =ValorTítulo  FROM dbo.DataCreditoTítulos AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CódigoPersona = P.Codigo and R.NúmeroPedido=P.[Numero de obligacion]
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 23' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Valor saldo deuda] =ValorTítulo  FROM dbo.DataCreditoTítulos AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CódigoPersona = P.Codigo and R.NúmeroPedido=P.[Numero de obligacion]
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 24' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Valor Cuota mes] =[Saldo actualizado]  FROM dbo.DataCreditoTítulos AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CódigoPersona = P.Codigo and R.NúmeroPedido=P.[Numero de obligacion]
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 25' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Fecha limite de pago] =CONVERT(CHAR(10),FechaVencimiento,102)  FROM dbo.DataCreditoTítulos AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CódigoPersona = P.Codigo and R.NúmeroPedido=P.[Numero de obligacion]
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 26' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Oficina de radicacion] =Estructura  FROM dbo.DataCreditoTítulos AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CódigoPersona = P.Codigo and R.NúmeroPedido=P.[Numero de obligacion]
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 27' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Estadode la cuenta]='01'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update *1' WHERE Id=@Id

--DataCreditoBTS
UPDATE DataCreditoPlanilla SET Novedad ='0'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update *2' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET Novedad =[Dia Mora]  FROM dbo.DataCreditoBTS  AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.Cedula = P.Noidentificación and R.CodigoPedido = P.[Numero de obligacion]
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update *3' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Edad de mora] =Novedad 
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update *4' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET Novedad ='01' WHERE Novedad >=0 AND  Novedad <=59
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update *5' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET Novedad ='07' WHERE Novedad >=60 AND  Novedad <=89
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update *6' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET Novedad ='08' WHERE Novedad >=90 AND  Novedad <=119
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update *7' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET Novedad ='09' WHERE Novedad >=120
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 28' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Valor saldo en  mora] =[Valor actualizado total]  FROM dbo.DataCreditoBTS  AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.Cedula = P.Noidentificación AND P.[Numero de obligacion]=R.CodigoPedido
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 29' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Fecha de pago] =CONVERT(CHAR(10),[Fecha Pago],102)  FROM dbo.DataCreditoBTS  AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.Cedula = P.Noidentificación AND P.[Numero de obligacion]=R.CodigoPedido
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 30' WHERE Id=@Id

--DataCreditoCiudades 

UPDATE DataCreditoPlanilla SET [Codigo DANE ciudad de radicacion] =CIUDAD_DANE fROM dbo.DataCreditoCiudades  AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CIUDAD = P.[Ciudad de radicación]
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 31' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Codigo DANE ciudad de residencia] =CIUDAD_DANE fROM dbo.DataCreditoCiudades  AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CIUDAD = P.[Ciudad residencia]
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 32' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Codigo DANE ciudad laboral] =CIUDAD_DANE fROM dbo.DataCreditoCiudades  AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CIUDAD = P.[Ciudad laboral]
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 33' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Codigo DANE ciudad correspondencia] =CIUDAD_DANE fROM dbo.DataCreditoCiudades  AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CIUDAD = P.[Ciudad de correspondencia]
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 34' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Fecha de pago] =CONVERT(CHAR(10),FechaVencimiento,102)  FROM dbo.DataCreditoTítulos AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CódigoPersona = P.Codigo and P.[Fecha de pago]=''
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 35' WHERE Id=@Id

UPDATE  DataCreditoPlanilla SET [Fecha apertura]=REPLACE([Fecha apertura],'.','');
UPDATE  DataCreditoPlanilla SET [Fecha vencimiento]=REPLACE([Fecha vencimiento],'.','');
UPDATE  DataCreditoPlanilla SET [Fecha de pago]=REPLACE( [Fecha de pago],'.','');
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 36' WHERE Id=@Id

UPDATE  DataCreditoPlanilla SET [Fecha estado origen]=REPLACE([Fecha estado origen],'.','');
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 37' WHERE Id=@Id

UPDATE  DataCreditoPlanilla SET [Fecha estado de la cuenta]=REPLACE([Fecha estado de la cuenta],'.','');
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 38' WHERE Id=@Id

UPDATE  DataCreditoPlanilla SET [Fecha limite de pago]=REPLACE([Fecha limite de pago],'.','');
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 39' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Valor saldo en  mora]  =0  where [Valor saldo en  mora] ='';
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 40' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Estadode la cuenta]='02' where [Edad de mora] >=60;
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Update 41' WHERE Id=@Id

END