﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_LibPedInfoGerencias]
AS

DECLARE @result varchar(100) = '1'

BEGIN


BEGIN TRY 

   TRUNCATE TABLE LibPedInfoGerencia

    
	INSERT INTO LibPedInfoGerencia

	SELECT       [Pedido]
               , [CodPersona]
	           , [NombreRevendedor]
	           , [TipoPendencia]
	           , [EstructuraPadre]
	           , [Estructura]
    FROM         [InfoNatura].[dbo].[geraLibPedConPendencia]
	WHERE        TipoPendencia NOT IN('Credito', 'Situacion del registro')

      
			   			   
			   INSERT INTO LibPedEventos
			   SELECT 'Sp_LibPedInfoGerencias', 1, GETDATE()
			   
			   UPDATE rfparam SET 
					  CCNOT1 = 'Termino'
					, CCUDC1 = 1
					, CCMNDT = GETDATE()
 
				WHERE CCTABL = 'LIBPEDLOG' AND CCCODE = 'Sp_LibPedInfoGerencias' 
	 END TRY 

	 BEGIN CATCH 
	     INSERT INTO LibPedEventos
         SELECT 'Sp_LibPedInfoGerencias', '0', GETDATE()
         
           UPDATE rfparam SET 
				  CCNOT1 = 'No encontro datos con la condicion'
				, CCUDC1 = 0
				, CCMNDT = GETDATE() 
           WHERE CCTABL = 'LIBPEDLOG' AND CCCODE = 'Sp_LibPedInfoGerencias' 
         
	 END CATCH


END