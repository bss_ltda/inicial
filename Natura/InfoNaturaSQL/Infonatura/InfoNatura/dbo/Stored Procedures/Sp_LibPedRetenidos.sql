﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_LibPedRetenidos]

   -- EXEC [dbo].[Sp_LibPedRetenidos]

AS

DECLARE @err_message VARCHAR(100)
DECLARE @result varchar(100) = 1

BEGIN

BEGIN TRY

TRUNCATE TABLE LibPedRetenidos



               IF EXISTS( SELECT       a.FechaPedido
									 , a.Pedido
									 , a.CodPersona
									 , a.NombreRevendedor
									 , a.EstructuraPadre
		      						 , a.Estructura	
									 , RTRIM('ABONO $') + ' ' + RTRIM(CONVERT(bigint,(CupoTolerancia + CreditoTLAExcedido) + (2 * 4900) )) AS Abono 
				FROM                   geraLibPedConPendencia a
				INNER JOIN             geraLibPedComportPago b
				ON                     a.CodPersona = b.CodigoConsultora    
				AND                    a.TipoPendencia in ('Credito', 'Situacion del registro')
				AND                    CupoTolerancia + CreditoTLAExcedido IS NOT NULL)


				 BEGIN 

				       INSERT INTO LibPedRetenidos

							 SELECT       a.FechaPedido
												 , a.Pedido
												 , a.CodPersona
												 , a.NombreRevendedor
												 , a.EstructuraPadre
		      									 , a.Estructura	
												 , RTRIM('ABONO $') + ' ' + RTRIM(CONVERT(bigint,(CupoTolerancia + CreditoTLAExcedido) + (2 * 4900) )) AS Abono 
							--INTO				   LibPedRetenidos
							FROM                   geraLibPedConPendencia a
							INNER JOIN             geraLibPedComportPago b
							ON                     a.CodPersona = b.CodigoConsultora    
							AND                    a.TipoPendencia in ('Credito', 'Situacion del registro')
							AND                    CupoTolerancia + CreditoTLAExcedido IS NOT NULL
               
			   
			   PRINT @result

			   
			   INSERT INTO LibPedEventos

			   SELECT 'Sp_LibPedRetenidos', @result, GETDATE()
			    
			   
			   UPDATE rfparam SET 
					  CCNOT1 = 'Termino'
					, CCUDC1 = 1
					, CCMNDT = GETDATE()
 
				WHERE CCTABL = 'LIBPEDLOG' AND CCCODE = 'Sp_LibPedRetenidos' 
    END


	 ELSE
	    BEGIN
	          SET @err_message = '0'
              RAISERROR (@err_message, 11,1)
        END

	 END TRY 

	 BEGIN CATCH 
	     INSERT INTO LibPedEventos
         SELECT 'Sp_LibPedRetenidos', '0', GETDATE()

		 UPDATE rfparam SET 
					  CCNOT1 = 'No hay resgistros'
					, CCUDC1 = 0
					, CCMNDT = GETDATE()
 
				WHERE CCTABL = 'LIBPEDLOG' AND CCCODE = 'Sp_LibPedRetenidos' 

	 END CATCH

END