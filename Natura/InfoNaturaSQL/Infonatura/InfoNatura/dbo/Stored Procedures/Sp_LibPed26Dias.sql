﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_LibPed26Dias]

   -- exec [dbo].[Sp_LibPed26Dias] 

AS

DECLARE @err_message VARCHAR(100)
DECLARE @result VARCHAR(20) = '1'

BEGIN 


     BEGIN TRY

	    TRUNCATE TABLE LibPed26Dias



IF EXISTS(SELECT       a.FechaPedido
					 , a.Pedido
					 , a.CodPersona
					 , a.NombreRevendedor
					 , a.EstructuraPadre 
					 , a.Estructura		   

FROM                   geraLibPedConPendencia a

INNER JOIN             geraLibPedComportPago b
  	   ON              a.CodPersona = b.CodigoConsultora

WHERE                  a.TotalPagar + a.DeudaYSaldoCapital >= '1000000'
AND                    b.PromedioDeDiasDeRetraso > '26'
AND                    a.CreditoTLAExcedido >= '100%'
AND                    a.CtdTitulosPendientes > '3'
AND                    a.CtdTitulosVencidos > '0'
AND                    a.TipoPendencia IN ('Credito', 'Situacion del registro'))
	  BEGIN 
	           
			   INSERT INTO             LibPed15Dias

				SELECT                 a.FechaPedido
									 , a.Pedido
									 , a.CodPersona
									 , a.NombreRevendedor
									 , a.EstructuraPadre
		      						 , a.Estructura	
									 , RTRIM('ABONO $') + ' ' + RTRIM(CONVERT(float,(CupoTolerancia + CreditoTLAExcedido) + (6 + 2 * 4900))) AS Abono
				FROM                   geraLibPedConPendencia a
				INNER JOIN             geraLibPedComportPago b
				ON                     a.CodPersona = b.CodigoConsultora               
				WHERE                  a.TotalPagar + a.DeudaYSaldoCapital >= '1000000'
				AND                    b.PromedioDeDiasDeRetraso > '26'
				AND                    a.CreditoTLAExcedido >= '100%'
				AND                    a.CtdTitulosPendientes > '3'
				AND                    a.CtdTitulosVencidos > '0'
				AND                    a.TipoPendencia in ('Credito', 'Situacion del registro')

                 
			  PRINT @result

			   
			   INSERT INTO LibPedEventos

			   SELECT 'Sp_LibPed26Dias', @result, GETDATE()
			   
			   UPDATE rfparam SET 
				  CCNOT1 = 'Termino'
				, CCUDC1 = 1
				, CCMNDT = GETDATE() 
           WHERE CCTABL = 'LIBPEDLOG' AND CCCODE = 'Sp_LibPed26Dias'

    END


	 ELSE
	    BEGIN
	          SET @err_message = '0'
              RAISERROR (@err_message, 11,1)
        END

	 END TRY 

	 BEGIN CATCH 
	     INSERT INTO LibPedEventos
         SELECT 'Sp_LibPed26Dias', '0', GETDATE()
         
         UPDATE rfparam SET 
				  CCNOT1 = 'No encontro datos con la condicion'
				, CCUDC1 = 0
				, CCMNDT = GETDATE() 
           WHERE CCTABL = 'LIBPEDLOG' AND CCCODE = 'Sp_LibPed26Dias'
	 END CATCH

     

END