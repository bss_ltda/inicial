﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_ETL_EstructuraComercial]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	
SET ANSI_NULLS ON;

SET QUOTED_IDENTIFIER ON;


BEGIN TRY


	DECLARE @SQL_BULK VARCHAR(MAX);
	DECLARE @mensajeProceso AS VARCHAR(MAX);


	SET @mensajeProceso = 'Sin Procesar el SP';

	TRUNCATE TABLE biEstructuraComercialW;

	--DECLARE @path varchar(255) = 'D:\Sitios\xm\EstructuraComercial\Estructura.txt'
	DECLARE @path varchar(255) = '\\10.21.3.15\xm\EstructuraComercial\Estructura.txt'
	

	SET @SQL_BULK = 'BULK INSERT biEstructuraComercialW FROM ''' + @path + ''' WITH
        (
        FIRSTROW = 2,
        DATAFILETYPE =''widechar'',
        FIELDTERMINATOR = ''\t'',
        ROWTERMINATOR = ''\n'',
		CODEPAGE = ''ACP''
        )'
--print @SQL_BULK
--print 'Importando ' + @path;
EXEC (@SQL_BULK);

UPDATE geraEstructuraComercial SET 
        codSector              =      w.CodSector
      , Cargo                  =      Case When left( w.CodSector, 1) <> '7' Then 'GV' When w.CodSector = w.CodGV Then 'GV' Else 'GR' end
      , codGV                  =      w.CodGV
      , Gerencia               =      w.GV
      , Sector                 =      w.Sector
      , OndaDeCierre           =      w.OndaDeCierre
      , codResponsable         =      w.CodResponsable
      , NombreResponsable      =      w.NombreResponsable
      , CorreoCorporativo      =      w.CorreoCorporativo
      , Empresa                =      'Natura'
      , DireccionDeEntrega     =      w.DireccionDeEntrega
      , Telefono               =      w.Telefono
      , CiudadResidencia       =      w.CiudadResidencia
      , CiudadBaseSector       =      rtrim(w.CiudadBaseSector)
      , CumpleA                =      w.CumpleA
      , Observaciones          =      w.Observaciones
FROM geraEstructuraComercial r Inner Join biEstructuraComercialw w on r.[codSector] = w.[CodSector] ;

INSERT INTO geraEstructuraComercial (
        codSector             
      , Cargo                 
      , codGV                 
      , Gerencia              
      , Sector                
      , OndaDeCierre          
      , codResponsable        
      , NombreResponsable     
      , CorreoCorporativo     
      , Empresa               
      , DireccionDeEntrega    
      , Telefono              
      , CiudadResidencia      
      , CiudadBaseSector      
      , CumpleA               
      , Observaciones         )
SELECT 
	   w.CodSector	  
	 , Case When left( w.CodSector, 1) <> '7' Then 'GV' When w.CodSector = w.CodGV Then 'GV' Else 'GR' end
	 , w.CodGV		   		   
	 , w.GV
	 , w.Sector
	 , w.OndaDeCierre
	 , w.CodResponsable
	 , w.NombreResponsable
	 , w.CorreoCorporativo
	 , 'Natura'
	 , w.DireccionDeEntrega
	 , w.Telefono
	 , w.CiudadResidencia
	 , rtrim(w.CiudadBaseSector)
	 , w.CumpleA
	 , w.Observaciones 
FROM biEstructuraComercialW w
WHERE w.[CodSector] In( 
SELECT w.CodSector	  
FROM biEstructuraComercialW w Left Join geraEstructuraComercial r on w.[codSector] = r.[CodSector] 
WHERE r.[CodSector]  IS NULL ) ;	 

UPDATE RFPARAM SET 
  CCNOT2 = 'Actualizados ' + (SELECT RTRIM(CAST( COUNT(*) AS CHAR ))  FROM biEstructuraComercialW ) + ' Registros'
, CCUDTS = GETDATE()
WHERE CCTABL = 'ETL_WACTHER' AND CCCODE = 'ESTRUCTURA COMERCIAL';

SELECT 'OK' AS 'MensajeProceso';
END TRY
BEGIN CATCH 
	SET @mensajeProceso = 'Se ha generado un error en la Estructura del BULK-INSERT: Linea#:' + CAST (ERROR_LINE() AS VARCHAR) + ' - Mensaje de Error:' + ERROR_MESSAGE();				
	SELECT @mensajeProceso AS 'MensajeProceso';
END CATCH
END