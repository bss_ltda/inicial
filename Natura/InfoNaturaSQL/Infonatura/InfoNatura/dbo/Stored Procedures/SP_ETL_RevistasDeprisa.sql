﻿-- =============================================
-- Author:		FM
-- Create date: 2018-09-03
-- Description:	ETL REVISTAS
-- =============================================
CREATE PROCEDURE [dbo].[SP_ETL_RevistasDeprisa]
AS
BEGIN
	SET NOCOUNT ON;
BEGIN TRY

	DECLARE @SQL_BULK VARCHAR(MAX);
	DECLARE @mensajeProceso AS VARCHAR(MAX);
	DECLARE @ID_TRANSP AS int;

	SET @mensajeProceso = 'Sin Procesar el SP';
	SET @ID_TRANSP = 349739;

	TRUNCATE TABLE RevistasDeprisa;
	BULK INSERT RevistasDeprisa FROM '\\10.21.3.15\xm\Revistas\Deprisa.txt' WITH
        (
        FIRSTROW = 2,
        DATAFILETYPE ='widechar',
        FIELDTERMINATOR = '\t',
        ROWTERMINATOR = '\n',
		CODEPAGE = 'ACP'
        )


	UPDATE RevistasTracking SET 
		  CodConsultora              = LTRIM(RTRIM(SUBSTRING( w.Referencia, 1, CHARINDEX('/', w.Referencia) -1 )))
		, Ciudad                     = w.CiudadDestino  
		, CodigoPostal               = w.CodigoPostal  
		, Depto                      = w.DepartamentoDestino 
		, Direccion                  = w.DireccionDestinatario 
		, Celular                    = w.CelDestinatario 
		, FechaDespacho              = w.FechaDespacho  
		, Ciclo                      = RTRIM( SUBSTRING( w.Referencia, CHARINDEX('/', w.Referencia) + 1, 50  ) ) 
		, Estado                     = w.EstadoDelEnvio 
		, Novedad1                   = w.Observaciones 
	FROM RevistasTracking r Inner Join 
		 RevistasDeprisa w on r.IdTransportadora = @ID_TRANSP and r.NumeroGuia = w.Guia  ;


	INSERT INTO RevistasTracking ( 
		  NumeroGuia
		, IdTransportadora
		, CodConsultora
		, CodigoPostal
		, Ciudad
		, Depto
		, Direccion
		, Telefono
		, Celular
		, FechaDespacho
		, Ciclo
		, Estado
		, Novedad1
	)
	SELECT 
		  w.Guia 
		, @ID_TRANSP
		, LTRIM(RTRIM(SUBSTRING( w.Referencia, 1, CHARINDEX('/', w.Referencia) -1 )))
		, W.CodigoPostal		
		, w.CiudadDestino
		, w.DepartamentoDestino
		, w.DireccionDestinatario
		, w.CelDestinatario
		, w.CelDestinatario
		, w.FechaDespacho
		, RTRIM( SUBSTRING( w.Referencia, CHARINDEX('/', w.Referencia) + 1, 50  ) ) 
		, w.EstadoDelEnvio
		, w.Observaciones
	 FROM RevistasDeprisa w Left Join RevistasTracking r on r.IdTransportadora = @ID_TRANSP and w.Guia  = r.NumeroGuia
	 WHERE r.NumeroGuia Is Null ;


	SELECT 'OK' AS 'MensajeProceso';
	END TRY
	BEGIN CATCH 
		SET @mensajeProceso = 'Se ha generado un error en la Estructura del BULK-INSERT: Linea#:' + CAST (ERROR_LINE() AS VARCHAR) + ' - Mensaje de Error:' + ERROR_MESSAGE();				
		SELECT @mensajeProceso AS 'MensajeProceso';
	END CATCH
END