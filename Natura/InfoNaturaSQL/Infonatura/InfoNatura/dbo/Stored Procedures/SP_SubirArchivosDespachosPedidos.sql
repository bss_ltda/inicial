﻿CREATE PROCEDURE [dbo].[SP_SubirArchivosDespachosPedidos] (@Id BigInt, @Adj BigInt)

AS
BEGIN

	SET NOCOUNT ON;

DECLARE @tempDATA VARCHAR(255)
DECLARE @Faltantes VARCHAR(255) = ''
DECLARE @MenNulo VARCHAR(255) = ''
DECLARE @Name nvarchar(50)
DECLARE @Ruta nvarchar(2000)
DECLARE @Paso nvarchar(50)
DECLARE @IDD FLOAT
SELECT @Name = Codigo FROM TABLAMAESTRA WHERE TABLA = 'DIRECCION';	
SELECT @Ruta = FADESCRIP FROM RFADJUNTO WHERE FANUMREG = @Adj;
Declare @comando nvarchar(400)

BEGIN TRY

truncate table DespachosPedidos;
truncate table DespachosPedidosWrk;
UPDATE [InfoNatura].[dbo].[Eventos] SET [Mensajes] = NULL WHERE Id=@Id

DELETE FROM DespachosPedidos
DELETE FROM DespachosPedidosWrk

set @Ruta = REPLACE(@Ruta,'C:',@Name)

set @comando = 'BULK INSERT DespachosPedidosWrk FROM ''\\' + @Ruta + ''' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'

set @Paso = 'Despachos Pedidos'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id
EXEC sp_executesql @comando

update DespachosPedidosWrk set PESO_FINAL = REPLACE(peso_final, ',' , '.')

insert into DespachosPedidos select * from despachospedidoswrk


DECLARE DespachosPedidosCur CURSOR FOR 
SELECT dp.TIPO_CAJA 
FROM DespachosPedidos dp 
LEFT JOIN RFPARAM r on r.CCTABL = 'DESPACHOS' AND r.CCCODE = 'TIPO_CAJA' AND dp.TIPO_CAJA = r.CCCODE2 
WHERE  r.CCDESC IS NULL
GROUP BY dp.TIPO_CAJA;
OPEN DespachosPedidosCur
FETCH NEXT FROM DespachosPedidosCur INTO @tempDATA
WHILE ( @@FETCH_STATUS = 0 )
	BEGIN		
		IF @Faltantes = ''		
		SET @Faltantes = '<strong>TIPO CAJA</strong><br>' + @tempDATA
		ELSE
		SET @Faltantes += ', ' + @tempDATA
		FETCH NEXT FROM DespachosPedidosCur INTO @tempDATA
	END
SELECT @MenNulo = [Mensajes] FROM Eventos WHERE id = @Id AND [Mensajes] Is NOT NULL;
IF @MenNulo = ''
	UPDATE Eventos SET [Mensajes] = @Faltantes + '<br>' WHERE id = @Id
ELSE
	UPDATE Eventos SET [Mensajes] = @MenNulo + @Faltantes + '<br>' WHERE id = @Id
CLOSE DespachosPedidosCur 
DEALLOCATE DespachosPedidosCur

SET @Faltantes = ''
SET @MenNulo = ''
DECLARE DespachosPedidosCur2 CURSOR FOR 
SELECT dp.CD_POL_ENTREGA 
FROM DespachosPedidos dp 
LEFT JOIN RFPARAM r on r.CCTABL = 'DESPACHOS' AND r.CCCODE = 'CD_POL_ENTREGA' AND dp.CD_POL_ENTREGA = r.CCCODE2 
WHERE  r.CCDESC IS NULL
GROUP BY dp.CD_POL_ENTREGA;
OPEN DespachosPedidosCur2
FETCH NEXT FROM DespachosPedidosCur2 INTO @tempDATA
WHILE ( @@FETCH_STATUS = 0 )
	BEGIN		
		IF @Faltantes = ''
		SET @Faltantes = '<strong>POLITICA ENTREGA</strong><br>' + @tempDATA
		ELSE
		SET @Faltantes += ', ' + @tempDATA
		FETCH NEXT FROM DespachosPedidosCur2 INTO @tempDATA
	END
SELECT @MenNulo = [Mensajes] FROM Eventos WHERE id = @Id AND [Mensajes] Is NOT NULL;
IF @MenNulo = ''
	UPDATE Eventos SET [Mensajes] = @Faltantes + '<br>' WHERE id = @Id
ELSE
	UPDATE Eventos SET [Mensajes] = @MenNulo + @Faltantes + '<br>' WHERE id = @Id
CLOSE DespachosPedidosCur2 
DEALLOCATE DespachosPedidosCur2
	
IF (@Faltantes = '')
	BEGIN
		UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Terminado' WHERE Id=@Id;
		UPDATE [InfoNatura].[dbo].[RFADJUNTO] SET [STS02] = 1 WHERE FANUMREG=@Adj;
	END	
ELSE
	UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Error Parametrización' WHERE Id=@Id
END TRY
BEGIN CATCH
	DECLARE @ErrorNumber INT = ERROR_NUMBER();
	DECLARE @ErrorLine INT = ERROR_LINE();
	DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
	DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
	DECLARE @ErrorState INT = ERROR_STATE();
	DECLARE @ErrorProcedure INT = ERROR_PROCEDURE();

	PRINT 'Actual error number: ' + CAST(@ErrorNumber AS VARCHAR(10)) + '.';
	PRINT 'Actual line number: ' + CAST(@ErrorLine AS VARCHAR(10)) + '.';
	PRINT 'Paso: ' + @Paso + '.';
	PRINT 'Mensaje Error: ' + @ErrorMessage + '.';
	PRINT  @ErrorSeverity ;
	PRINT  @ErrorState ;

	RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
	
	UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Error ' + @Paso, [Mensajes] = @ErrorMessage + CAST(@ErrorLine AS VARCHAR(10)) WHERE Id=@Id
END CATCH;
END