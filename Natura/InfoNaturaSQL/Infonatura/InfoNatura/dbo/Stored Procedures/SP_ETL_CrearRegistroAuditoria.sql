﻿-- =============================================
-- Author:		FM
-- Create date: 2018-02-22
-- Description:	ETL de revendedoras de archivos csv 
-- =============================================
CREATE PROCEDURE [dbo].[SP_ETL_CrearRegistroAuditoria]
	@nombreArchivo AS VARCHAR(200),	
	@rutaArchivo AS VARCHAR(2000),
	@descripcionProceso AS VARCHAR(2000),
	@estadoProceso AS INT,
	@estadoDescarga AS BIT,
	@estadoCodificacion AS BIT,
	@estadoBulk AS BIT	
AS
BEGIN
SET NOCOUNT ON;

DECLARE @mensajeProceso AS VARCHAR(100);
DECLARE @fechaProceso AS DATETIME;

SET @mensajeProceso = 'Sin Procesar el SP';
SET @fechaProceso = GETDATE();

BEGIN TRY
	
	INSERT INTO [InfoNatura].[dbo].[AuditoriaNaturaSaveCSV]
           ([FechaProceso]
           ,[UltimaFechaProceso]
           ,[NombreArchivo]
           ,[RutaArchivo]
           ,[DescripcionProceso]
           ,[Estado]
           ,[EstadoDescarga]
           ,[EstadoCodificacion]
           ,[EstadoBulk])
     VALUES
           (@fechaProceso
           , NULL
           ,@nombreArchivo
           ,@rutaArchivo
           ,@descripcionProceso
           ,@estadoProceso
           ,@estadoDescarga
           ,@estadoCodificacion
           ,@estadoBulk)
	
	SET @mensajeProceso = 'Se ha realizado el Registro de Auditoria de Manera Correcta. Numero de Id [' + CAST ((SELECT IdProceso FROM AuditoriaNaturaSaveCSV WHERE [FechaProceso] = @fechaProceso AND [NombreArchivo] = @nombreArchivo) AS VARCHAR) + ']';
	SELECT @mensajeProceso AS 'MensajeProceso';	
	
END TRY
BEGIN CATCH 
	SET @mensajeProceso = 'Se ha generado un Error Registrarndo Auditoria Linea:' + ERROR_LINE() + ' - Mensaje de Error:' + ERROR_MESSAGE();		
	SELECT @mensajeProceso AS 'MensajeProceso';
END CATCH

END