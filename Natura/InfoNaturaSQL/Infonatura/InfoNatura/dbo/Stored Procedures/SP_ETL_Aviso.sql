﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_ETL_Aviso] 
	@ETL varchar(255)	
AS
BEGIN
	SET NOCOUNT ON;
	
INSERT INTO RMAILX(
        LMOD
      , LREF
      , LASUNTO
      , LPARA
      , LCUERPO
      , LCATALOGO
    )
    VALUES(
        'SP_ETL'
      , 'ETL Archivos CSV'
      , @ETL
      , 'BSS'
      , 'http://Localhost/Tecno/ETL/SP_ETL.asp?ETL='+ @ETL
      , 'USE InfoNatura ');      		

END