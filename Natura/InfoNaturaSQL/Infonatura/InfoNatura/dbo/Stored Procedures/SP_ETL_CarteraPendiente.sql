﻿-- =============================================
-- Author:		JASC
-- Create date: 20-04-2018
-- Description:	Bulk Insert Archivo geraCarteraPendiente
-- =============================================
CREATE PROCEDURE [dbo].[SP_ETL_CarteraPendiente]
@rutaArchivo varchar(255),
@idLogAuditoria int	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here	
	
SET ANSI_NULLS ON;

SET QUOTED_IDENTIFIER ON;


BEGIN TRY
	DECLARE @ETL VARCHAR(100);
	DECLARE @SQL_BULK VARCHAR(MAX);
	DECLARE @mensajeProceso AS VARCHAR(MAX);
	SET @mensajeProceso = 'Sin Procesar el SP';		
	SET @ETL = 'SP_ETL_CarteraPendiente';

	TRUNCATE TABLE geraCarteraPendiente;

	SET @rutaArchivo = REPLACE( @rutaArchivo, 'D:\Sitios\', '\\10.21.3.15\' )
	--set @path = 'D:\Sitios\xm\FTP_CSV\CarteraPendiente\ConsultaTitulo.csv'
	--SET @path = REPLACE( @path, 'D:\Sitios\', '\\10.21.3.15\' )
	--D:\Sitios\xm\FTP_CSV\CarteraPendiente\ConsultaTitulo_2d06e513-d5ec-42c8-81bf-907a8890245c.csv

	--SET @path = 'C:\Temp\CarteraTitulo.csv'

	SET @SQL_BULK = 'BULK INSERT geraCarteraPendiente FROM ''' + @rutaArchivo + ''' WITH
        (
        FIRSTROW = 2,
        DATAFILETYPE =''widechar'',
        FIELDTERMINATOR = '';'',
        ROWTERMINATOR = ''\n'',
		CODEPAGE = ''ACP''
        )'
        
        SET @mensajeProceso = 'Importando ' + @rutaArchivo;
--print @SQL_BULK
--print 'Importando ' + @path;
EXEC (@SQL_BULK);
SET @mensajeProceso = 'Actualizando...';

	 UPDATE geraCarteraPendiente SET 
	   CodigoPersona =  Replace( CodigoPersona, '.', '' )  
	 , Negociacion = Replace( Negociacion, '.', '' ) 
	 , ValorTitulo = Replace( ValorTitulo, '.', '' )
	 , SaldoPrincipal = Replace( SaldoPrincipal, '.', '' )    
	 , SaldoFinanciero = Replace( SaldoFinanciero, '.', '' )    
	 , SaldoTotal = Replace( SaldoTotal, '.', '' )   
	 , SaldoActualizado = Replace( SaldoActualizado, '.', '' )  
	 , NumeroFactura = Replace( NumeroFactura, '.', '' )
	 , CodEstructura = Replace( CodEstructura, '.', '' )
	 , CodEstructuraPadre = Replace( CodEstructuraPadre, '.', '' )
	 	 
	UPDATE RFPARAM SET 
	    CCMNDT = GETDATE()
	  , CCCODEN = @idLogAuditoria
	  , CCUDC2 = 1
	  , CCNOT2 = @rutaArchivo	  
	  , CCNOTE = 'OK'
	WHERE CCTABL = 'ETL_FTP' AND CCCODE = 'gera' AND CCCODE2 = @ETL ;
	
	SELECT 'OK' AS 'MensajeProceso';
END TRY
BEGIN CATCH 
	SET @mensajeProceso = 'LINEA: ' + CAST (ERROR_LINE() AS VARCHAR) + ' - ERROR:' + ERROR_MESSAGE();				
	UPDATE RFPARAM SET 
	    CCMNDT = GETDATE()
	  , CCCODEN = @idLogAuditoria
	  , CCUDC2 = -1
	  , CCNOT2 = @rutaArchivo	  
	  , CCNOTE = @mensajeProceso
	WHERE CCTABL = 'ETL_FTP' AND CCCODE = 'gera' AND CCCODE2 = @ETL;
	
	EXEC SP_ETL_Aviso @ETL
	
	
	SELECT @mensajeProceso AS 'MensajeProceso';
	
END CATCH
END