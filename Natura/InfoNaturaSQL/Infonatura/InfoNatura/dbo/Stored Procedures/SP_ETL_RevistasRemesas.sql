﻿-- =============================================
-- Author:		FM
-- Create date: 2018-09-03
-- Description:	ETL REVISTAS
-- =============================================
CREATE PROCEDURE [dbo].[SP_ETL_RevistasRemesas]
AS
BEGIN
	SET NOCOUNT ON;
BEGIN TRY

	DECLARE @SQL_BULK VARCHAR(MAX);
	DECLARE @mensajeProceso AS VARCHAR(MAX);
	DECLARE @ID_TRANSP AS int;

	SET @mensajeProceso = 'Sin Procesar el SP';
	SET @ID_TRANSP = 373668;

	TRUNCATE TABLE RevistasRemesas;
	BULK INSERT RevistasRemesas FROM '\\10.21.3.15\xm\Revistas\Remesas.txt' WITH
        (
        FIRSTROW = 2,
        DATAFILETYPE ='widechar',
        FIELDTERMINATOR = '\t',
        ROWTERMINATOR = '\n',
		CODEPAGE = 'ACP'
        )


	UPDATE RevistasTracking SET 
		  CodConsultora              = w.CodigoONumeroDeConsultora  
		, Ciudad                     = w.Destino  
		, Depto                      = w.Departamento 
		, Direccion                  = w.DireccionDestinatario 
		, Telefono                   = w.TelefonoDestinatario  
		, Celular                    = w.TelefonoDestinatario 
		, FechaDespacho              = w.FechaDeEntrega  
--		, Ciclo                      = w.Ciclo 
		, Estado                     = w.Estado 
		, Novedad1                   = w.Novedades 
	FROM RevistasTracking r Inner Join 
		 RevistasRemesas w on r.IdTransportadora = @ID_TRANSP and r.NumeroGuia = w.NumeroDeGuia  ;


	INSERT INTO RevistasTracking ( 
		  NumeroGuia
		, IdTransportadora
		, CodConsultora
		, Ciudad
		, Depto
		, Direccion
		, Telefono
		, Celular
		, FechaDespacho
--		, Ciclo
		, Estado
		, Novedad1
	)
	SELECT 
		  w.NumeroDeGuia 
		, @ID_TRANSP
		, w.CodigoONumeroDeConsultora
		, w.Destino
		, w.Departamento
		, w.DireccionDestinatario
		, w.TelefonoDestinatario
		, w.TelefonoDestinatario
		, w.FechaDeEntrega
--		, w.Ciclo
		, w.Estado
		, w.Novedades
	 FROM RevistasRemesas w Left Join RevistasTracking r on r.IdTransportadora = @ID_TRANSP and w.NumeroDeGuia  = r.NumeroGuia
	 WHERE r.NumeroGuia Is Null ;


	SELECT 'OK' AS 'MensajeProceso';
	END TRY
	BEGIN CATCH 
		SET @mensajeProceso = 'Se ha generado un error en la Estructura del BULK-INSERT: Linea#:' + CAST (ERROR_LINE() AS VARCHAR) + ' - Mensaje de Error:' + ERROR_MESSAGE();				
		SELECT @mensajeProceso AS 'MensajeProceso';
	END CATCH
END