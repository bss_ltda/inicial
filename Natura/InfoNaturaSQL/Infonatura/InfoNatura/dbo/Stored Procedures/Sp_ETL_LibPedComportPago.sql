﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_ETL_LibPedComportPago]
-- EXEC [dbo].[Sp_ETL_LibPedComportPago]
-- SELECT * FROM geraLibPedComportPago
AS
BEGIN
	SET NOCOUNT ON;	
	
	
BEGIN TRY
	
	TRUNCATE TABLE geraLibPedComportPago
	
     --  Estado 1 Disponible
     --  Estado 0 Indisponible

     -- COMPORTAMIENTO DE PAGO DISPONIBLE
     
     INSERT INTO geraLibPedComportPago ( 
      CodigoConsultora
    , NombreConsultora
    , ApellidoConsultora
    , NumeroDocumento
    , LimiteDeCreditoActual
    , CreditoActualDisponible
    , PerfilDeCreditoActual
    , PromedioDeDiasDeRetraso
    , CuantPedidosUtilizadosEnCalculoDelPromedio
    , SituacionComercial
    , CiclosInactividad
    , CicloActual
    , CantCiclosActivos
    , CicloPrimerPedido
    , FechaPrimerPedido
    , FechaRegistroIntencionDeReventa
    , CicloReativacion
    , CicloCesamiento
    , CodigoGrupo
    , CodigoSector
    , Sector
    , CodigoGerenciaDeVentas
    , GerenciaDeVentas
    , CiudadConsultora
    , DireccionResidencialConsultora
    , TelefonoResidencialConsultora
    , TelefonoMovilConsultora
    , Estado
)
SELECT 
      w.CodigoConsultora
    , w.NombreConsultora
    , w.ApellidoConsultora
    , w.NumeroDocumento
    , w.LimiteDeCreditoActual
    , w.CreditoActualDisponible
    , w.PerfilDeCreditoActual
    , PromedioDeDiasDeRetraso = (dbo.PreparaNumero(PromedioDeDiasDeRetraso,''))
    , w.CantPedidosCalc
    , w.SituacionComercial
    , w.CiclosInactividad
    , w.CicloActual
    , w.CantCiclosActivos
    , w.CicloPrimerPedido
    , w.FechaPrimerPedido
    , w.FecRegIntencionDeReventa
    , w.CicloReativacion
    , w.CicloCesamiento
    , w.CodigoGrupo
    , w.CodigoSector
    , w.Sector
    , w.CodigoGerenciaDeVentas
    , w.GerenciaDeVentas
    , w.Ciudad
    , w.Direccion
    , w.Telefono
    , w.TelefonoMovil
    , '0'
 FROM geraComPagDisponible w


  -- Comportamiento de pago Indisponible
  
  INSERT INTO geraLibPedComportPago ( 
      CodigoConsultora
    , NombreConsultora
    , ApellidoConsultora
    , NumeroDocumento
    , LimiteDeCreditoActual
    , CreditoActualDisponible
    , PerfilDeCreditoActual
    , PromedioDeDiasDeRetraso
    , CuantPedidosUtilizadosEnCalculoDelPromedio
    , SituacionComercial
    , CiclosInactividad
    , CicloActual
    , CantCiclosActivos
    , CicloPrimerPedido
    , FechaPrimerPedido
    , FechaRegistroIntencionDeReventa
    , CicloReativacion
    , CicloCesamiento
    , CodigoGrupo
    , CodigoSector
    , Sector
    , CodigoGerenciaDeVentas
    , GerenciaDeVentas
    , CiudadConsultora
    , DireccionResidencialConsultora
    , TelefonoResidencialConsultora
    , TelefonoMovilConsultora
    , Estado
)
SELECT 
      w.CodigoConsultora
    , w.NombreConsultora
    , w.ApellidoConsultora
    , w.NumeroDocumento
    , w.LimiteDeCreditoActual
    , w.CreditoActualDisponible
    , w.PerfilDeCreditoActual
    , PromedioDeDiasDeRetraso = (dbo.PreparaNumero(PromedioDeDiasDeRetraso,''))
    , w.CantPedidosCalc
    , w.SituacionComercial
    , w.CiclosInactividad
    , w.CicloActual
    , w.CantCiclosActivos
    , w.CicloPrimerPedido
    , w.FechaPrimerPedido
    , w.FecRegIntencionDeReventa
    , w.CicloReativacion
    , w.CicloCesamiento
    , w.CodigoGrupo
    , w.CodigoSector
    , w.Sector
    , w.CodigoGerenciaDeVentas
    , w.GerenciaDeVentas
    , w.Ciudad
    , w.Direccion
    , w.Telefono
    , w.TelefonoMovil
    , '1'
 FROM geraComPagIndisponible w
 
               EXEC Sp_LibPedTolerancia
			   
			   INSERT INTO LibPedEventos

			   SELECT 'Sp_ETL_LibPedComportPago', '1', GETDATE()
			    
			   
			   UPDATE rfparam SET 
					  CCNOT1 = 'Termino'
					, CCUDC1 = 1
					, CCMNDT = GETDATE()
 
				WHERE CCTABL = 'LIBPEDLOG' AND CCCODE = 'Sp_ETL_LibPedComportPago'

END TRY

BEGIN CATCH

     		   
			   INSERT INTO LibPedEventos

			   SELECT 'Sp_ETL_LibPedComportPago', '0', GETDATE()
			    
			   
			   UPDATE rfparam SET 
					  CCNOT1 = 'Termino'
					, CCUDC1 = 0
					, CCMNDT = GETDATE()
 
				WHERE CCTABL = 'LIBPEDLOG' AND CCCODE = 'Sp_ETL_LibPedComportPago'

END CATCH

END