﻿-- =============================================
-- Author:		FM
-- Create date: 2018-02-22
-- Description:	ETL de revendedoras de archivos csv 
-- =============================================
CREATE PROCEDURE [dbo].[SP_ETL_ActualizarRegistroAuditoria]
	@idProceso AS INT,
	@nombreArchivo AS VARCHAR(200),	
	@rutaArchivo AS VARCHAR(2000),
	@descripcionProceso AS VARCHAR(2000),
	@estadoProceso AS INT,
	@estadoDescarga AS BIT,
	@estadoCodificacion AS BIT,
	@estadoBulk AS BIT	
AS
BEGIN
SET NOCOUNT ON;

DECLARE @mensajeProceso AS VARCHAR(100);
DECLARE @fechaProceso AS DATETIME;

SET @mensajeProceso = 'Sin Procesar el SP';
SET @fechaProceso = GETDATE();

BEGIN TRY

	IF @idProceso = 0
	BEGIN
		SET @idProceso = (SELECT idProceso FROM AuditoriaNaturaSaveCSV WHERE nombreArchivo = @nombreArchivo AND rutaArchivo = @rutaArchivo)
	END	
	
	UPDATE [InfoNatura].[dbo].[AuditoriaNaturaSaveCSV]
		SET [UltimaFechaProceso] = @fechaProceso
			,[NombreArchivo] = @nombreArchivo
			,[RutaArchivo] = @rutaArchivo
			,[DescripcionProceso] = @descripcionProceso
			,[Estado] = @estadoProceso
			,[EstadoDescarga] = @estadoDescarga
			,[EstadoCodificacion] = @estadoCodificacion
			,[EstadoBulk] = @estadoBulk
	WHERE idProceso = @idProceso;	
		
	SET @mensajeProceso = 'Se ha Actualizado el Registro de Auditoria de Manera Correcta.';
	SELECT @mensajeProceso AS 'MensajeProceso';	
	
END TRY
BEGIN CATCH 
	SET @mensajeProceso = 'Se ha generado un Error Actualizado Auditoria Linea:' + ERROR_LINE() + ' - Mensaje de Error:' + ERROR_MESSAGE();		
	SELECT @mensajeProceso AS 'MensajeProceso';
END CATCH

END