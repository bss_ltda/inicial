﻿CREATE PROCEDURE [dbo].[SP_LogisticaSubirArchivos] (@Id BigInt)
AS
BEGIN
	SET NOCOUNT ON;
truncate table TrasportadoraEnvia;
truncate table TrasportadoraInter;
truncate table Trasportadoras;
truncate table TrasportadoraServientrega;
truncate table TrasportadorasRprt;

   BULK INSERT TrasportadoraEnvia 
   FROM '\\10.21.2.15\InfoNaturaDatos\Trasportadoras\TrasportadoraEnvia.csv'
  
   WITH 
    ( 
        FIRSTROW = 2, 
        MAXERRORS = 0, 
        FIELDTERMINATOR = ',', 
        ROWTERMINATOR = '\n' 
    )
   UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'TrasportadoraEnvia' WHERE Id=@Id

   BULK INSERT TrasportadoraInter 
   FROM '\\10.21.2.15\InfoNaturaDatos\Trasportadoras\TrasportadoraInter.csv'
  
   WITH 
    ( 
        FIRSTROW = 2, 
        MAXERRORS = 0, 
        FIELDTERMINATOR = ',', 
        ROWTERMINATOR = '\n' 
    )
   UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'TrasportadoraInter' WHERE Id=@Id


   BULK INSERT TrasportadoraServientrega 
   FROM '\\10.21.2.15\InfoNaturaDatos\Trasportadoras\TrasportadoraServientrega.csv'
  
   WITH 
    ( 
        FIRSTROW = 2, 
        MAXERRORS = 0, 
        FIELDTERMINATOR = ',', 
        ROWTERMINATOR = '\n' 
    )
   UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'TrasportadoraServientrega' WHERE Id=@Id


   BULK INSERT Trasportadoras 
   FROM '\\10.21.2.15\InfoNaturaDatos\Trasportadoras\Trasportadoras.csv'
  
   WITH 
    ( 
        FIRSTROW = 2, 
        MAXERRORS = 0, 
        FIELDTERMINATOR = ',', 
        ROWTERMINATOR = '\n' 
    )
   UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Trasportadoras' WHERE Id=@Id


INSERT INTO [InfoNatura].[dbo].[TrasportadorasRprt]
           ([Transportadora]
           ,[CodigoPedido]
           ,[Guía]
           ,[Estado]
           ,[Fecha]
           ,[Novedad]
           ,[Persona]
           ,[NombrePersona]
           ,[Fecha1]
           ,[PrevisiónEntrega]
           ,[Días de Retraso]
           ,[Ciclo Captación]
           ,[Dirección Entrega]
           ,[MUNICIPIOEntrega]
           ,[DEPARTAMIENTOEntrega]
           ,[EstructuraPadre]
           ,[Estructura])     
     (SELECT  ISNULL([Transportadora] , ' ' ) 
      ,ISNULL([CodigoPedido] , ' ' ) 
      , ' ' 
      , ' ' 
      , ' ' 
      , ' ' 
      ,ISNULL([Persona] , ' ' ) 
      ,ISNULL([NombrePersona] , ' ' ) 
      ,ISNULL([Fecha] , ' ' ) 
      ,ISNULL([PrevisiónEntrega] , ' ' ) 
      , ' ' 
      ,ISNULL([Ciclo Captación] , ' ' ) 
      ,ISNULL([VIA PRINCIPALEntrega] , ' ' ) 
      ,ISNULL([MUNICIPIOEntrega] , ' ' )
      ,ISNULL([DEPARTAMIENTOEntrega] , ' ' )
      ,ISNULL([EstructuraPadre] , ' ' )
      ,ISNULL([Estructura] , ' ' )
       FROM [InfoNatura].[dbo].Trasportadoras);

UPDATE TrasportadoraEnvia SET Numero_Guia = REPLACE (Numero_Guia,'  ', '');

UPDATE TrasportadorasRprt SET [Estado] = [Estado_Actual] from  TrasportadoraEnvia as R INNER JOIN TrasportadorasRprt AS P ON R.Numero_Pedido = P.CodigoPedido  ;
UPDATE TrasportadorasRprt SET [Estado] = [ESTADO ENVIO] from  TrasportadoraServientrega as R INNER JOIN TrasportadorasRprt AS P ON R.PEDIDO = P.CodigoPedido  ;
UPDATE TrasportadorasRprt SET [Estado] = [Estado Gestion] from  TrasportadoraInter as R INNER JOIN TrasportadorasRprt AS P ON R.Contenido = P.CodigoPedido  ;

UPDATE TrasportadorasRprt SET [Fecha] = [Fecha_Novedad1] from  TrasportadoraEnvia as R INNER JOIN TrasportadorasRprt AS P ON R.Numero_Pedido = P.CodigoPedido  ;
UPDATE TrasportadorasRprt SET [Fecha] = CONVERT(FLOAT, convert(datetime,[NOVEDAD 2]))  from  TrasportadoraServientrega as R INNER JOIN TrasportadorasRprt AS P ON R.PEDIDO = P.CodigoPedido  ;
--UPDATE TrasportadorasRprt SET [Fecha] = [Guia] from  TrasportadoraInter as R INNER JOIN TrasportadorasRprt AS P ON R.Contenido = P.CodigoPedido  ;

UPDATE TrasportadorasRprt SET [Novedad] = [Observaciones Novedades] from  TrasportadoraEnvia as R INNER JOIN TrasportadorasRprt AS P ON R.Numero_Pedido = P.CodigoPedido  ;
UPDATE TrasportadorasRprt SET [Novedad] = [NOVEDAD 1] from  TrasportadoraServientrega as R INNER JOIN TrasportadorasRprt AS P ON R.PEDIDO = P.CodigoPedido  ;
UPDATE TrasportadorasRprt SET [Novedad] = [Guia] from  TrasportadoraInter as R INNER JOIN TrasportadorasRprt AS P ON R.Contenido = P.CodigoPedido  ;


END