﻿-- =============================================
-- Author:		FM
-- Create date: 2018-03
-- Description:	ETL datos de servientrega para seguimiento entregas
-- =============================================
CREATE PROCEDURE  [dbo].[SP_ETL_TranspServientrega]
	@rutaArchivo varchar(255),
    @idLogAuditoria int		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
BEGIN TRY

	DECLARE @SQL_BULK VARCHAR(MAX);
	DECLARE @mensajeProceso AS VARCHAR(MAX);


	SET @mensajeProceso = 'Sin Procesar el SP';

	TRUNCATE TABLE TranspServientrega;

	SET @SQL_BULK = 'BULK INSERT TranspServientrega FROM ''' + @rutaArchivo + ''' WITH
        (
        FIRSTROW = 2,
        DATAFILETYPE =''widechar'',
        FIELDTERMINATOR = '','',
        ROWTERMINATOR = ''\n'',
		CODEPAGE = ''ACP''
        )'
	EXEC (@SQL_BULK);


	UPDATE TranspServientrega Set NumeroGuia = Replace(NumeroGuia, '"', '' );

	UPDATE TranspEntrega SET 
		  FechaDespacho                  = convert(datetime, w.FechaEnvio, 105) 
		, TipoDespacho                   = w.NombreMedtrans
		, Pedido                         = Case When ISNUMERIC( w.NumeroDocumentoCliente ) = 1 Then w.NumeroDocumentoCliente Else Null End
		, NumeroGuia                      = w.NumeroGuia
		, Destinatario                   = w.DestinaGuia
		, Direccion                      = w.DireccionDestinaGuia
		, CiudadDestino                  = w.CiudadDestino
		, Departamento                   = w.Departamento
		, Cajas                          = Case When ISNUMERIC( w.NumeroPiezas ) = 1 Then w.NumeroPiezas Else Null End
		, Regional                       = w.NombreRegional
		, Estado                         = w.NombreEstadoEnvio
		, FechaEntrega                   = convert(datetime, w.FechaEntrega, 105)
		, TipoNovedad                    = w.Novedad
		, FechaNovedad                   = Case When ISNUMERIC( lEFT(w.FechaNovedad, 2 ) ) = 1 Then convert(datetime, w.FechaNovedad, 105) Else Null End
		, FechaCargueTransp              = getDate()
	FROM TranspEntrega r Inner Join TranspServientrega w on r.idTransportadora = 100006 And r.NumeroGuia = w.NumeroGuia ;

	INSERT INTO TranspEntrega ( 
		  idTransportadora
		, FechaDespacho     
		, TipoDespacho      
		, Pedido            
		, NumeroGuia              
		, Destinatario      
		, Direccion         
		, CiudadDestino     
		, Departamento      
		, Cajas
		, Regional          
		, Estado            
		, FechaEntrega      
		, TipoNovedad       
		, FechaNovedad      
	)
	SELECT 
		  100006
		, convert(datetime, w.FechaEnvio, 105) 
		, w.NombreMedtrans
		, Case When ISNUMERIC( w.NumeroDocumentoCliente ) = 1 Then w.NumeroDocumentoCliente Else Null End
		, w.NumeroGuia 
		, w.DestinaGuia
		, w.DireccionDestinaGuia
		, w.CiudadDestino
		, w.Departamento
		, Case When ISNUMERIC( w.NumeroPiezas ) = 1 Then w.NumeroPiezas Else Null End
		, w.NombreRegional
		, w.NombreEstadoEnvio
		, convert(datetime, w.FechaEntrega, 105)
		, w.Novedad
		, Case When ISNUMERIC( lEFT(w.FechaNovedad, 2 ) ) = 1 Then convert(datetime, w.FechaNovedad, 105) Else Null End
	 FROM TranspServientrega w Left Join TranspEntrega r on r.idTransportadora = 100006 And r.NumeroGuia = w.NumeroGuia
	 WHERE r.NumeroGuia Is Null ;
	 
	UPDATE AuditoriaNaturaSaveCSV SET 
	  UltimaFechaProceso = GETDATE()
	, ProcesoInterno = 1
	, DescripcionProceso = 'ETL Aplicado'	 
	WHERE IdProceso = @idLogAuditoria;
	 

	SELECT 'OK' AS 'MensajeProceso';
	END TRY
	BEGIN CATCH 
		SET @mensajeProceso = 'Se ha generado un error en la Estructura del BULK-INSERT: Linea#:' + CAST (ERROR_LINE() AS VARCHAR) + ' - Mensaje de Error:' + ERROR_MESSAGE();				
		SELECT @mensajeProceso AS 'MensajeProceso';
	END CATCH
END