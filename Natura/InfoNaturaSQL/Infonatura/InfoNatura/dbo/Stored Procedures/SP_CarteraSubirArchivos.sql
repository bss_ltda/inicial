﻿CREATE PROCEDURE  [dbo].[SP_CarteraSubirArchivos] (@Id BigInt)
AS
BEGIN
	SET NOCOUNT ON;

	declare @periodo varchar(100); 
	DECLARE @Name nvarchar(50)
	SELECT @Name = Codigo FROM TABLAMAESTRA WHERE TABLA = 'DIRECCION';	
	Declare @comando nvarchar(400)
	
	BEGIN TRY
	
	set @periodo= 'Primer Mes'
	truncate  table CarteraTítulos;
	set @comando = 'BULK INSERT CarteraTítulos FROM ''\\'+ @Name +'\InfoNaturaDatos\IndiceMorosidad\Mes1.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
	
	UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @periodo WHERE Id=@Id
	EXEC sp_executesql @comando
	delete from CarteraTítulos where Situación='Anulado' 
	
	update CarteraTítulos set  [Saldo Total] =  REPLACE([Saldo Total], '.', '') 
	update CarteraTítulos set  [Saldo Total] =  REPLACE([Saldo Total], ',00', '') 
	update CarteraTítulos set  [Saldo Total] =  REPLACE([Saldo Total], '$', '') 
	update CarteraTítulos set  [Saldo Total] =  REPLACE([Saldo Total], '-', '0') 
	
	update CarteraTítulos set  ValorTítulo =  REPLACE(ValorTítulo, '.', '') 
	update CarteraTítulos set  ValorTítulo =  REPLACE(ValorTítulo, ',00', '') 
	update CarteraTítulos set  ValorTítulo =  REPLACE(ValorTítulo, '$', '') 
    exec SP_CarteraTitulosInd @periodo 


	
	set @periodo= 'Segundo Mes'
	truncate  table CarteraTítulos;
    set @comando = 'BULK INSERT CarteraTítulos FROM ''\\'+ @Name +'\InfoNaturaDatos\IndiceMorosidad\Mes2.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
    UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @periodo WHERE Id=@Id
EXEC sp_executesql @comando
	update CarteraTítulos set  [Saldo Total] =  REPLACE([Saldo Total], '.', '') 
	update CarteraTítulos set  [Saldo Total] =  REPLACE([Saldo Total], ',00', '') 
	update CarteraTítulos set  [Saldo Total] =  REPLACE([Saldo Total], '$', '') 
	update CarteraTítulos set  [Saldo Total] =  REPLACE([Saldo Total], '-', '0') 

	update CarteraTítulos set  ValorTítulo =  REPLACE(ValorTítulo, '.', '') 
	update CarteraTítulos set  ValorTítulo =  REPLACE(ValorTítulo, ',00', '') 
	update CarteraTítulos set  ValorTítulo =  REPLACE(ValorTítulo, '$', '') 
    exec SP_CarteraTitulosInd @periodo 

	
	
	set @periodo= 'Tercer Mes'
	truncate  table CarteraTítulos;
set @comando = 'BULK INSERT CarteraTítulos FROM ''\\'+ @Name +'\InfoNaturaDatos\IndiceMorosidad\Mes3.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @periodo WHERE Id=@Id
EXEC sp_executesql @comando
	update CarteraTítulos set  [Saldo Total] =  REPLACE([Saldo Total], '.', '') 
	update CarteraTítulos set  [Saldo Total] =  REPLACE([Saldo Total], ',00', '') 
	update CarteraTítulos set  [Saldo Total] =  REPLACE([Saldo Total], '$', '') 
	update CarteraTítulos set  [Saldo Total] =  REPLACE([Saldo Total], '-', '0') 

 	update CarteraTítulos set  ValorTítulo =  REPLACE(ValorTítulo, '.', '') 
	update CarteraTítulos set  ValorTítulo =  REPLACE(ValorTítulo, ',00', '') 
	update CarteraTítulos set  ValorTítulo =  REPLACE(ValorTítulo, '$', '') 
    exec SP_CarteraTitulosInd @periodo 
	
	

	set @periodo= 'Cuarto Mes'
	truncate  table CarteraTítulos;
set @comando = 'BULK INSERT CarteraTítulos FROM ''\\'+ @Name +'\InfoNaturaDatos\IndiceMorosidad\Mes4.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @periodo WHERE Id=@Id
EXEC sp_executesql @comando
	update CarteraTítulos set  [Saldo Total] =  REPLACE([Saldo Total], '.', '') 
	update CarteraTítulos set  [Saldo Total] =  REPLACE([Saldo Total], ',00', '') 
	update CarteraTítulos set  [Saldo Total] =  REPLACE([Saldo Total], '$', '') 
	update CarteraTítulos set  [Saldo Total] =  REPLACE([Saldo Total], '-', '0') 

	update CarteraTítulos set  ValorTítulo =  REPLACE(ValorTítulo, '.', '') 
	update CarteraTítulos set  ValorTítulo =  REPLACE(ValorTítulo, ',00', '') 
	update CarteraTítulos set  ValorTítulo =  REPLACE(ValorTítulo, '$', '') 
    exec SP_CarteraTitulosInd @periodo 

	

	set @periodo= 'Quinto Mes'
	truncate  table CarteraTítulos;
set @comando = 'BULK INSERT CarteraTítulos FROM ''\\'+ @Name +'\InfoNaturaDatos\IndiceMorosidad\Mes5.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @periodo WHERE Id=@Id
EXEC sp_executesql @comando
	update CarteraTítulos set  [Saldo Total] =  REPLACE([Saldo Total], '.', '') 
	update CarteraTítulos set  [Saldo Total] =  REPLACE([Saldo Total], ',00', '') 
	update CarteraTítulos set  [Saldo Total] =  REPLACE([Saldo Total], '$', '') 
	update CarteraTítulos set  [Saldo Total] =  REPLACE([Saldo Total], '-', '0') 

	update CarteraTítulos set  ValorTítulo =  REPLACE(ValorTítulo, '.', '') 
	update CarteraTítulos set  ValorTítulo =  REPLACE(ValorTítulo, ',00', '') 
	update CarteraTítulos set  ValorTítulo =  REPLACE(ValorTítulo, '$', '') 
    exec SP_CarteraTitulosInd @periodo 

	

	set @periodo= 'Sexto Mes'
	truncate  table CarteraTítulos;
set @comando = 'BULK INSERT CarteraTítulos FROM ''\\'+ @Name +'\InfoNaturaDatos\IndiceMorosidad\Mes6.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @periodo WHERE Id=@Id
EXEC sp_executesql @comando
	update CarteraTítulos set  [Saldo Total] =  REPLACE([Saldo Total], '.', '') 
	update CarteraTítulos set  [Saldo Total] =  REPLACE([Saldo Total], ',00', '') 
	update CarteraTítulos set  [Saldo Total] =  REPLACE([Saldo Total], '$', '') 
	update CarteraTítulos set  [Saldo Total] =  REPLACE([Saldo Total], '-', '0') 

	update CarteraTítulos set  ValorTítulo =  REPLACE(ValorTítulo, '.', '') 
	update CarteraTítulos set  ValorTítulo =  REPLACE(ValorTítulo, ',00', '') 
	update CarteraTítulos set  ValorTítulo =  REPLACE(ValorTítulo, '$', '') 
    exec SP_CarteraTitulosInd @periodo

	
set @periodo= 'Inicios'
	truncate  table  CarteraInicios;	 
	set @comando = 'BULK INSERT CarteraInicios FROM ''\\'+ @Name +'\InfoNaturaDatos\IndiceMorosidad\Inicios.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
	UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @periodo WHERE Id=@Id
EXEC sp_executesql @comando

	
set @periodo= 'Agencias'
    truncate  table  CarteraAgenciasDeCobro; 
set @comando = 'BULK INSERT CarteraAgenciasDeCobro FROM ''\\'+ @Name +'\InfoNaturaDatos\IndiceMorosidad\Agencias.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @periodo WHERE Id=@Id
EXEC sp_executesql @comando
	UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'fin de Archivos' WHERE Id=@Id
	delete from CarteraTítulosInd where Situación='Anulado' 
	delete from CarteraTítulos where Situación='Anulado' 
END TRY
BEGIN CATCH
EXECUTE usp_GetErrorInfo @IdE=@Id, @PasoE=@periodo;

	--DECLARE @ErrorNumber INT = ERROR_NUMBER();
 --   DECLARE @ErrorLine INT = ERROR_LINE();
 --   DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
 --   DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
 --   DECLARE @ErrorState INT = ERROR_STATE();
 --   DECLARE @ErrorProcedure INT = ERROR_PROCEDURE();

 --   PRINT 'Actual error number: ' + CAST(@ErrorNumber AS VARCHAR(10)) + '.';
 --   PRINT 'Actual line number: ' + CAST(@ErrorLine AS VARCHAR(10)) + '.';
 --   PRINT 'Paso: ' + @Periodo + '.';

 --   RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
    
 --   UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Error ' + @periodo, [Mensajes] = @ErrorMessage + CAST(@ErrorLine AS VARCHAR(10)) WHERE Id=@Id
END CATCH;
END