﻿CREATE PROCEDURE [dbo].[SP_UnlockMacro42Dias]
AS
BEGIN
	SET NOCOUNT ON;

DECLARE @id int
DECLARE @Paso nvarchar(50)

BEGIN TRY

--===================== ACTUALIZANDO RFPARAM ==========================
set @Paso = 'RFPARAM'
UPDATE [InfoNatura].[dbo].[RFPARAM] SET [CCUDC1] = 0 WHERE CCTABL = 'PROCEDIMIENTOS' AND CCCODE = 'SP_SubirArchivos42Dias';

--=====================================================================

--======================= ACTUALIZANDO EVENTOS ========================

set @Paso = 'Eventos'
SELECT @id = Max(id) FROM Eventos WHERE Macro = 'CDias' ;
UPDATE [InfoNatura].[dbo].[Eventos] SET Sts1 = 1, Sts2 = 1, Sts3 = 1 WHERE Id = @id
UPDATE [InfoNatura].[dbo].[Eventos] SET [Estado] = 'Desbloqueada', [Mensajes] = 'Macro desbloqueada con exito' WHERE Id = @Id

--=====================================================================

END TRY
BEGIN CATCH
	DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();
    DECLARE @ErrorProcedure INT = ERROR_PROCEDURE();

    PRINT 'Actual error number: ' + CAST(@ErrorNumber AS VARCHAR(10)) + '.';
    PRINT 'Actual line number: ' + CAST(@ErrorLine AS VARCHAR(10)) + '.';
    
    RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
    
    UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Error ' + @Paso, [Mensajes] = @ErrorMessage + CAST(@ErrorLine AS VARCHAR(10)) WHERE Id = @Id
END CATCH;
END