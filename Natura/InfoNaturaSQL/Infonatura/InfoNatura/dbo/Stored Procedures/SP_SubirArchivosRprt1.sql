﻿CREATE PROCEDURE [dbo].[SP_SubirArchivosRprt1] (@Id BigInt)
AS
BEGIN
	SET NOCOUNT ON;
DECLARE @Name nvarchar(50)
SELECT @Name = Codigo FROM TABLAMAESTRA WHERE TABLA = 'DIRECCION';	
Declare @comando nvarchar(400)

UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'CobroInterno' WHERE Id=@Id
truncate table  RCartraCobroInterno
set @comando = 'BULK INSERT RCartraCobroInterno FROM ''\\'+ @Name +'\InfoNaturaDatos\ReporteCartera\COBRO.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
EXEC sp_executesql @comando
      
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Deudas' WHERE Id=@Id
truncate table  Títulos
set @comando = 'BULK INSERT Títulos FROM ''\\'+ @Name +'\InfoNaturaDatos\ReporteCartera\Deudas.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'

EXEC sp_executesql @comando

      
        delete from RCarteraAgenciasDeCobro
set @comando = 'BULK INSERT RCarteraAgenciasDeCobro FROM ''\\'+ @Name +'\InfoNaturaDatos\ReporteCartera\Agencias.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
EXEC sp_executesql @comando

TRUNCATE TABLE [RCartraPlanilla]
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'CobroInterno' WHERE Id=@Id
INSERT INTO [InfoNatura].[dbo].[RCartraPlanilla]
           ([Primer Pedido]
           ,[Fecha primer pedido]
           ,[CodigoPedido]
           ,[Factura]
           ,[Perfil consultor Cartera]
           ,[Codig]
           ,[NombrePersona]
           ,[Cedula]
           ,[Puntaje Credito]
           ,[Valor Original]
           ,[Intereses]
           ,[Cuota administrativa]
           ,[Abono Pago]
           ,[Valor actualizado del pedido]
           ,[Valor actualizado total]
           ,[SituaciónComercial]
           ,[FechaAutFacturación]
           ,[FechaEntrega]
           ,[FechaVencimiento]
           ,[Dia Mora]
           ,[Rango Mora]
           ,[Fecha Pago]
           ,[Entidad de pago]
           ,[Ciclo]
           ,[CicloInd]
           ,[Ciclo de facturacion del pedido]
           ,[PlanPago]
           ,[VIA PRINCIPAL]
           ,[Complemento]
           ,[BARRIO]
           ,[MUNICIPIO]
           ,[DEPARTAMIENTO]
           ,[Telefono residencial]
           ,[Celular]
           ,[DescripciónModeloComercial]
           ,[CodEstructuraPadre]
           ,[EstructuraPadre]
		   ,[CodEstructura]     
           ,[Estructura]  
           ,[Usuario de Creación]
           ,[Usuario de Finalización]
           ,[NumeroDias]
           ,[CaptacionRestrita]
           ,[Correo Electronico]
           ,[Referencias personales 1]
           ,[Referencia Telefono 1]
           ,[Referencias personales 2]
           ,[Referencia Telefono 2])(SELECT ISNULL([Primer Pedido] , ' ' )
      ,ISNULL([Fecha primer pedido] , ' ' )
      ,ISNULL([CodigoPedido] , ' ' )
      ,ISNULL([Factura] , ' ' )
      ,ISNULL([Perfil consultor Cartera] , ' ' )
      ,ISNULL([Codig] , ' ' )
      ,ISNULL([NombrePersona] , ' ' )
      ,ISNULL([Cedula] , ' ' )
      ,ISNULL([Puntaje Credito] , ' ' )
      ,ISNULL([Valor Original] , ' ' )
      ,ISNULL([Intereses] , ' ' )
      ,ISNULL([Cuota administrativa] , ' ' )
      ,ISNULL([Abono Pago] , ' ' )
      ,ISNULL([Valor actualizado del pedido] , ' ' )
      ,ISNULL([Valor actualizado total] , ' ' )
      ,ISNULL([SituaciónComercial] , ' ' )
      ,ISNULL([FechaAutFacturación] , ' ' )
      ,ISNULL([FechaEntrega] , ' ' )
      ,ISNULL([FechaVencimiento] , ' ' )
      ,ISNULL([Dia Mora] , ' ' )
      ,ISNULL([Rango Mora] , ' ' )
      ,ISNULL([Fecha Pago] , ' ' )
      ,ISNULL([Entidad de pago] , ' ' )
      ,ISNULL([Ciclo] , ' ' )
      ,ISNULL([CicloInd] , ' ' )
      ,ISNULL([Ciclo de facturacion del pedido] , ' ' )
      ,ISNULL([PlanPago] , ' ' )
      ,ISNULL([VIA PRINCIPAL] , ' ' )
      ,ISNULL([Complemento] , ' ' )
      ,ISNULL([BARRIO] , ' ' )
      ,ISNULL([MUNICIPIO] , ' ' )
      ,ISNULL([DEPARTAMIENTO] , ' ' )
      ,ISNULL([Telefono residencial] , ' ' )
      ,ISNULL([Celular] , ' ' )
      ,ISNULL([DescripciónModeloComercial] , ' ' )
      ,' '
      ,ISNULL([EstructuraPadre] , ' ' )
      ,' '
      ,ISNULL([Estructura] , ' ' )
      ,ISNULL([Usuario de Creación] , ' ' )
      ,ISNULL([Usuario de Finalización] , ' ' )
      ,ISNULL([NumeroDias] , ' ' )
      ,ISNULL([CaptacionRestrita] , ' ' )
      ,ISNULL([Correo Electronico] , ' ' )
      ,ISNULL([Referencias personales 1] , ' ' )
      ,ISNULL([Referencia Telefono 1] , ' ' )
      ,ISNULL([Referencias personales 2] , ' ' )
      ,ISNULL([Referencia Telefono 2] , ' ' )
   FROM [InfoNatura].[dbo].[RCartraCobroInterno]);
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Cambio Estructura' WHERE Id=@Id
--UPDATE RCartraPlanilla set [Tipo de Cartera] = 'CAMBIO', [EstructuraPadre] = GerenciaActual from RCartraPlanilla INNER JOIN GerenciasNE on Estructura LIKE Sector + '%' 
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Agencias de Cobro' WHERE Id=@Id
UPDATE RCartraPlanilla set [Agencia de Cobranza] = [Empresa de cobranza] from  RCarteraAgenciasDeCobro as R INNER JOIN RCartraPlanilla AS P ON R.Sector like '%' + P.Estructura  + '%'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'TipoCartera' WHERE Id=@Id
UPDATE RCartraPlanilla seT [Tipo de Cartera] = 'Pre-Juridica < 180 dias' 
UPDATE RCartraPlanilla seT [Tipo de Cartera] = 'Pre-Juridica > 180 dias' where RCartraPlanilla.[Dia Mora] > 180 
UPDATE  RCartraPlanilla set [Agencia de Cobranza] = 'ASERFINC' where [Tipo de Cartera] = 'Pre-Juridica > 180 dias'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Terminado' WHERE Id=@Id
UPDATE RCartraPlanilla set [CodEstructuraPadre] = ' '
UPDATE RCartraPlanilla set [CodEstructura] = ' ' 
--UPDATE RCartraPlanilla set [EstructuraPadre] = ' ' 
--UPDATE RCartraPlanilla set [Estructura] = ' ' 
UPDATE RCartraPlanilla set [CodEstructuraPadre] = [Cód Estructura Padre] from  RCartraPlanilla as R INNER JOIN Títulos AS P ON R.CodigoPedido = P.NúmeroPedido
UPDATE RCartraPlanilla set [CodEstructura] = [Cód Estructura] from  RCartraPlanilla as R INNER JOIN Títulos AS P ON R.CodigoPedido =  P.NúmeroPedido  
--UPDATE RCartraPlanilla set [EstructuraPadre] = [Estructura Padre] from  RCartraPlanilla as R INNER JOIN Títulos AS P ON R.CodigoPedido =  P.NúmeroPedido  
--UPDATE RCartraPlanilla set [Estructura] = [EstructuraN] from  RCartraPlanilla as R INNER JOIN Títulos AS P ON R.CodigoPedido = P.NúmeroPedido  
UPDATE RCartraPlanilla set [Valor actualizado del pedido]= REPLACE ([Valor actualizado del pedido], '.00', '')
UPDATE RCartraPlanilla set [Valor actualizado total] = REPLACE([Valor actualizado total],'.00','')
UPDATE RCartraPlanilla set [EstructuraPadre] = [NombreGerencia] from  RCartraPlanilla as R INNER JOIN CarteraGerencias AS P ON R.[CodEstructuraPadre] =  P.[Cód Estructura Padre]  
DELETE  from RCartraPlanilla WHERE [EstructuraPadre] = 'GERENCIA COLABORADORES NATURA'
DELETE  from RCartraPlanilla WHERE [EstructuraPadre] = 'GERENCIA VENTAS INSTITUCIONAL'
END