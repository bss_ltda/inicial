﻿CREATE PROCEDURE [dbo].[SP_SubirArchivosFnzRO] (@Id BigInt)
AS
BEGIN
SET NOCOUNT ON;
DECLARE @Name nvarchar(50)
DECLARE @Archivo nvarchar(50)
SELECT @Name = Codigo FROM TABLAMAESTRA WHERE TABLA = 'DIRECCION';	
SELECT @Archivo = CCSDSC FROM ZCC WHERE CCTABL = 'ARCHIVOS' AND CCCODE = 'FINANZAS' AND CCDESC = 'RO';
Declare @comando nvarchar(400)

Delete From FinanzasDatos Where TipoDato = '4';
	
	set @comando = 'BULK INSERT FinanzasDatos FROM ''\\'+ @Name +'\InfoNaturaDatos\Finanzas\'+ @Archivo +'.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'')'
	EXEC sp_executesql @comando
	UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Pesos' WHERE Id=@Id

	
END