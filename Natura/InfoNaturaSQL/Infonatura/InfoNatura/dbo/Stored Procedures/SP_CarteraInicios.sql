﻿CREATE PROCEDURE [dbo].[SP_CarteraInicios]  (@Gerencia  varchar(100), @MesPlanilla varchar(100))

AS
BEGIN

	SET NOCOUNT ON;
SELECT  (SUM (CONVERT(float,[Saldo Total]))/SUM (CONVERT(float,ValorTítulo))) as procentaje FROM CarteraTítulosInd where [Cód Estructura Padre] =@Gerencia and MesPlanilla=@MesPlanilla and Inicion='si';

END