﻿-- =============================================
-- Author:		J.A.S.C
-- Create date: 2018-08-04
-- Description:	Cargue Pedidos Chile
-- =============================================
CREATE PROCEDURE [dbo].[SP_ETL_NotaPedido]
@rutaArchivo varchar(255),
@idLogAuditoria int	
AS
BEGIN	
	SET NOCOUNT ON;
	DECLARE @MensajeProceso AS VARCHAR(MAX);	
	INSERT INTO RFLOG( OPERACION, EVENTO ) VALUES('BULKINSERT', @rutaArchivo)	
	SET @MensajeProceso = 'SP Sin procesar';
	--SET @MensajeProceso =  	
	EXEC @MensajeProceso = ShowroomCL.dbo.SP_ETL_NotaPedido @rutaArchivo=@rutaArchivo
	SELECT @MensajeProceso AS 'MensajeProceso'
END