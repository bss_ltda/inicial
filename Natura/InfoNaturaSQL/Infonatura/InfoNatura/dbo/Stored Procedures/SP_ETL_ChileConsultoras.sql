﻿-- =============================================
-- Author:		J.A.S.C
-- Create date: 2018-08-04
-- Description:	Cargue Pedidos Chile
-- =============================================
CREATE PROCEDURE [dbo].[SP_ETL_ChileConsultoras]
@rutaArchivo varchar(255),
@idLogAuditoria int	
AS
BEGIN	
	SET NOCOUNT ON;
	DECLARE @ID_PARAM AS bigint;
	INSERT INTO RFLOG( OPERACION, EVENTO ) VALUES('BULKINSERT', @rutaArchivo)		 
	SET @ID_PARAM =  ( SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY] ); 	
	
	DECLARE @MensajeProceso AS VARCHAR(MAX);	
	SET @MensajeProceso = 'SP Sin procesar';
	--SET @MensajeProceso =  	
	EXEC @MensajeProceso = ShowroomCL.dbo.SP_ETL_ChileConsultoras @rutaArchivo=@rutaArchivo, @ID_PARAM=@ID_PARAM ;	
	UPDATE RFLOG SET MENSAJE = @MensajeProceso WHERE ID = @ID_PARAM;
	SELECT 'aqui ' + @MensajeProceso AS 'MensajeProceso'
END