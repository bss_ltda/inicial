﻿CREATE PROCEDURE [dbo].[SP_SubirArchivos42Dias] (@Id BigInt)
AS
BEGIN
	SET NOCOUNT ON;

BEGIN TRY
DECLARE @Name nvarchar(50)
DECLARE @Paso nvarchar(50)
SELECT @Name = Codigo FROM TABLAMAESTRA WHERE TABLA = 'DIRECCION';	
Declare @comando nvarchar(400)

DECLARE @VCtrl int
SELECT @VCtrl = CCUDC1 FROM RFPARAM WHERE CCTABL = 'PROCEDIMIENTOS' AND CCCODE = 'SP_SubirArchivos42Dias';

IF  @VCtrl = 0
BEGIN
	UPDATE [InfoNatura].[dbo].[RFPARAM] SET [CCUDC1] = 1 WHERE CCTABL = 'PROCEDIMIENTOS' AND CCCODE = 'SP_SubirArchivos42Dias'
	truncate table C42CarteraVencida;
	truncate table C42Ondas;
	truncate table C42PrimerPedido;
	truncate table C42Reveendedores;
	truncate table C42Titulos; -- No Vencida
	truncate table C42Planilla; 
	truncate table C42Ventas; 

	set @comando = 'BULK INSERT C42CarteraVencida FROM ''\\'+ @Name +'\InfoNaturaDatos\42Dias\Cartera.csv'' WITH 
	( 
	FIRSTROW = 2, 
	MAXERRORS = 0, 
	FIELDTERMINATOR = '';'',
	ROWTERMINATOR = ''\n'',
	CODEPAGE = ''ACP''
	)'
	set @Paso = 'C42Cartera'
	UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id
	EXEC (@comando)

	set @comando = 'BULK INSERT C42Reveendedores FROM ''\\'+ @Name +'\InfoNaturaDatos\42Dias\Revendedores.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
	set @Paso = 'C42Reveendedores'
	UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id
	EXEC (@comando)

	set @comando = 'BULK INSERT C42Ondas FROM ''\\'+ @Name +'\InfoNaturaDatos\42Dias\Ondas.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
	set @Paso = 'C42Ondas'
	UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id
	EXEC (@comando)

	set @comando = 'BULK INSERT C42PrimerPedido FROM ''\\'+ @Name +'\InfoNaturaDatos\42Dias\PrimerPedido.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
	set @Paso = 'C42PrimerPedido'
	UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id
	EXEC (@comando)

	UPDATE  C42PrimerPedido SET CodigoRevendedor=REPLACE(CodigoRevendedor,'.',''); 
	
	set @comando = 'BULK INSERT C42Ventas FROM ''\\'+ @Name +'\InfoNaturaDatos\42Dias\Ventas.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
	set @Paso = 'C42Ventas'	
	UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id
	EXEC (@comando)

	truncate table [C42Planilla]
	set @Paso = 'C42CarteraVencida DiaMora=0'
	UPDATE C42CarteraVencida set DIA_MORA = '0' where  DIA_MORA IS NULL          

			
	 INSERT INTO [InfoNatura].[dbo].[C42Planilla]
			   ([CodigoPedido]
			   ,[Persona]
			   ,[NombrePersona]
			   ,[CicloIndicador]
			   ,ValorTotal
			   ,CodEstructuraPadre
			   ,[EstructuraPadre]
			   ,[Estructura]
			   ,CodEstructura)
		 SELECT 
				C42Ventas.CodigoPedido,  
				C42Ventas.Persona,
				C42Ventas.NombrePersona,
				C42Ventas.CicloIndicador,
				C42Ventas.ValorTotal,
				C42Ventas.[Cód Estructura Padre],
				C42Ventas.EstructuraPadre,
				C42Ventas.[ESTRUCTURA],
				C42Ventas.CodEstructura 
		 FROM  C42Ventas 


	--
	UPDATE C42Planilla set Deuda = '0' where  Deuda IS NULL	
	UPDATE C42Planilla set [CodigoGrupo] = '0' where  [CodigoGrupo] IS NULL          
	UPDATE C42Planilla set Celular = '' where  Celular IS NULL          
	UPDATE C42Planilla set ValorTotal = '0' where  ValorTotal IS NULL          
	UPDATE C42Planilla set PrimerPedido = 'NA' where  PrimerPedido IS NULL          
	UPDATE C42Planilla set Onda = '' where  Onda IS NULL          
	UPDATE C42Planilla set deuda = VALOR_ACTUALIZADO_DEL_PEDIDO from C42CarteraVencida as R INNER JOIN C42Planilla AS P ON   R.[CODIGO_PEDIDO] = P.[CodigoPedido]
	UPDATE C42Planilla set [DiaMora] = DIA_MORA from C42CarteraVencida as R INNER JOIN C42Planilla AS P ON   R.[CODIGO_PEDIDO] = P.[CodigoPedido]
	UPDATE [C42Planilla] SET  Celular = TelMovil   FROM dbo.C42Reveendedores AS R INNER JOIN dbo.C42Planilla AS P ON R.[CodigoRevendedor] = P.[Persona]
	UPDATE [C42Planilla] SET [CodigoGrupo]  = CodGrupo   FROM dbo.C42Reveendedores AS R INNER JOIN dbo.C42Planilla AS P ON R.[CodigoRevendedor] = P.[Persona]
	UPDATE [C42Planilla] SET PrimerPedido = CicloPrimerPedido FROM dbo.C42PrimerPedido AS R INNER JOIN dbo.[C42Planilla] AS P ON P.Persona = R.CodigoRevendedor
	UPDATE [C42Planilla]  SET  PrimerPedido = 'NO' Where  CicloIndicador <> PrimerPedido
	UPDATE [C42Planilla]  SET  C42Planilla.PrimerPedido = 'SI' Where CicloIndicador = PrimerPedido

	UPDATE [C42Planilla] SET Onda = R.CodEstructura FROM dbo.C42Ventas AS R INNER JOIN dbo.[C42Planilla] AS P ON P.CodigoPedido = R.CodigoPedido
	UPDATE [C42Planilla]  SET  Onda = REPLACE (Onda,'.','') 
	UPDATE [C42Planilla] SET C42Planilla.Onda = R.ONDA FROM dbo.C42Ondas AS R INNER JOIN dbo.[C42Planilla] AS P ON P.Onda = CAST(R.CodigoSector as nvarchar)
	UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Terminado', [Mensajes] = NULL WHERE Id=@Id

	UPDATE [InfoNatura].[dbo].[RFPARAM] SET [CCUDC1] = 0 WHERE CCTABL = 'PROCEDIMIENTOS' AND CCCODE = 'SP_SubirArchivos42Dias'
END
ELSE
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Cancelado', [Mensajes] = 'Procedimiento ya enviado' WHERE Id=@Id
END TRY
BEGIN CATCH    
    UPDATE [InfoNatura].[dbo].[Eventos] 
    SET [FechaEjecucion] = GetDate(),[Estado] = 'Error ' + @Paso, 
    [Mensajes] = 'Se ha generado un error en la Estructura del BULK-INSERT: Linea:' + CAST (ERROR_LINE() AS VARCHAR) + ' - Mensaje de Error:' + ERROR_MESSAGE() WHERE Id=@Id
END CATCH;
END