﻿CREATE PROCEDURE [dbo].[SP_SubirArchivosCifin] (@Id BigInt)

AS
BEGIN

	SET NOCOUNT ON;


DECLARE @Name nvarchar(50)
DECLARE @Paso nvarchar(50)
DECLARE @IDD FLOAT
SELECT @Name = Codigo FROM TABLAMAESTRA WHERE TABLA = 'DIRECCION';	
Declare @comando nvarchar(400)

--insert into  CifinPlanillahistoricos select * from CifinPlanilla
BEGIN TRY

truncate table CifinPlanilla;
truncate table CifinPersonas;
set @comando = 'BULK INSERT CifinPersonas FROM ''\\'+ @Name +'\InfoNaturaDatos\Cifin\Personas1.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
set @Paso = 'Personas 1 - Prox: Personas 2'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id
EXEC sp_executesql @comando

set @comando = 'BULK INSERT CifinPersonas FROM ''\\'+ @Name +'\InfoNaturaDatos\Cifin\Personas2.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
set @Paso = 'Personas2 - Prox: Personas 3'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id
EXEC sp_executesql @comando

set @comando = 'BULK INSERT CifinPersonas FROM ''\\'+ @Name +'\InfoNaturaDatos\Cifin\Personas3.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
set @Paso = 'Personas3 - Prox: Personas 4'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id
EXEC sp_executesql @comando

set @comando = 'BULK INSERT CifinPersonas FROM ''\\'+ @Name +'\InfoNaturaDatos\Cifin\Personas4.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
set @Paso = 'Personas4 - Prox: Personas 5'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id
EXEC sp_executesql @comando

set @comando = 'BULK INSERT CifinPersonas FROM ''\\'+ @Name +'\InfoNaturaDatos\Cifin\Personas5.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
set @Paso = 'Personas5 - Prox: Personas 6'
EXEC sp_executesql @comando
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id

set @comando = 'BULK INSERT CifinPersonas FROM ''\\'+ @Name +'\InfoNaturaDatos\Cifin\Personas6.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
set @Paso = 'Personas6 - Prox: Cobro'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id
EXEC sp_executesql @comando

truncate table CifinCobro;
Set Language 'Spanish';
set @comando = 'BULK INSERT CifinCobro FROM ''\\'+ @Name +'\InfoNaturaDatos\Cifin\Cobro.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
set @Paso = 'Cobro  - Prox: Vencimientos'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id
EXEC sp_executesql @comando

truncate table CifinVencimiento;
Set Language 'Spanish';
set @comando = 'BULK INSERT CifinVencimiento FROM ''\\'+ @Name +'\InfoNaturaDatos\Cifin\Vencimientos.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
set @Paso = 'Vencimientos'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id
EXEC sp_executesql @comando

set @Paso = 'Actualiza Personas'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id



UPDATE  CifinPersonas SET CiudadResidencial =REPLACE(CiudadResidencial,'Á','A');
UPDATE  CifinPersonas SET CiudadResidencial =REPLACE(CiudadResidencial,'É','E');
UPDATE  CifinPersonas SET CiudadResidencial =REPLACE(CiudadResidencial,'Í','I');
UPDATE  CifinPersonas SET CiudadResidencial =REPLACE(CiudadResidencial,'Ó','O');
UPDATE  CifinPersonas SET CiudadResidencial =REPLACE(CiudadResidencial,'Ú','U');

UPDATE  CifinPersonas SET RegionResidencial =REPLACE(RegionResidencial,'Á','A');
UPDATE  CifinPersonas SET RegionResidencial =REPLACE(RegionResidencial,'É','E');
UPDATE  CifinPersonas SET RegionResidencial =REPLACE(RegionResidencial,'Í','I');
UPDATE  CifinPersonas SET RegionResidencial =REPLACE(RegionResidencial,'Ó','O');
UPDATE  CifinPersonas SET RegionResidencial =REPLACE(RegionResidencial,'Ú','U');



SELECT @IDD = (ISNULL (MAX (ID), 0)) + 1 FROM CifinPlanillaHistoricos 


delete from CifinVencimiento where  [Cód Estructura Padre] = '700000031'
delete from CifinVencimiento where  [Cód Estructura Padre] = '700000172'


delete from CifinCobro where  DESCRIPCION_MODELO_COMERCIAL = 'COLABORADOR CN'

set @Paso = 'Positivas Negativas Pendientes'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id

exec SP_CifinPositivas @ID=@IDD;
exec CifinNegativas @ID=@IDD;
exec CifinPedientes @ID=@IDD;

set @Paso = 'Planilla CIFIN'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id

delete t1 from CifinPlanilla t1 inner join CifinPlanillaHistoricos t2 on t2.[NUMERO OBLIGACION] = t1.[NUMERO OBLIGACION] and t2.ID = (select top 1 ID FROM CifinPlanillaHistoricos where FECHA_GENERACION = (select MAX(FECHA_GENERACION)FROM CifinPlanillaHistoricos)) AND t2.Reporte = 'POSITIVO'
   
insert into  CifinPlanillahistoricos select * from CifinPlanilla
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Terminado', [Mensajes] = NULL WHERE Id=@Id
END TRY
BEGIN CATCH
	DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();
    DECLARE @ErrorProcedure INT = ERROR_PROCEDURE();

    PRINT 'Actual error number: ' + CAST(@ErrorNumber AS VARCHAR(10)) + '.';
    PRINT 'Actual line number: ' + CAST(@ErrorLine AS VARCHAR(10)) + '.';
    PRINT 'Paso: ' + @Paso + '.';
	PRINT 'Mensaje Error: ' + @ErrorMessage + '.';
	PRINT  @ErrorSeverity ;
	PRINT  @ErrorState ;

    RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
    
    UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Error ' + @Paso, [Mensajes] = @ErrorMessage + CAST(@ErrorLine AS VARCHAR(10)) WHERE Id=@Id
END CATCH;
END