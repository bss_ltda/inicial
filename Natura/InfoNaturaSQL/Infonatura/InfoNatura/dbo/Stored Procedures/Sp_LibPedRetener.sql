﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_LibPedRetener]


-- EXEC [dbo].[Sp_LibPedRetener]

AS

DECLARE @err_message VARCHAR(100) 

BEGIN

BEGIN TRY

       --5721 Valor punto consultora
       --      a.TotalPagar + a.DeudaYSaldoCapital >= 1000000
		-- b.PromedioDeDiasDeRetraso > 15
		--  a.CreditoTLAExcedido > 100
		--  a.CtdTitulosPendientes >= 3
		--  a.CtdTitulosVencidos > 0
       
	   TRUNCATE TABLE LibPedRetenidos
	   TRUNCATE TABLE LibPedMasivos
	   
	   IF EXISTS(SELECT           a.FechaPedido
								, a.Pedido
								, a.CodPersona
								, a.NombreRevendedor
								, a.EstructuraPadre
		      					, a.Estructura	
								, a.TipoPendencia								
		FROM                   geraLibPedConPendencia a
		INNER JOIN             geraLibPedComportPago b
		ON                     a.CodPersona = b.CodigoConsultora               
		WHERE                  a.TotalPagar + a.DeudaYSaldoCapital >= 1000000
		AND                    b.PromedioDeDiasDeRetraso > 15
		AND                    a.CreditoTLAExcedido > 100
		AND                    a.CtdTitulosPendientes >= 3
		AND                    a.CtdTitulosVencidos > 0
		AND                    a.TipoPendencia  LIKE '%Crédito%')

		           BEGIN
				       
					  
				       INSERT INTO             LibPedRetenidos

							SELECT                 a.FechaPedido
												 , a.Pedido
												 , a.CodPersona
												 , a.NombreRevendedor
												 , a.EstructuraPadre
		      									 , a.Estructura	
												 , RTRIM('ABONO $') + ' ' + RTRIM(CONVERT(bigint,(CupoTolerancia + CreditoTLAExcedido) + (6 + 2 * 5721))) AS Abono
												 , 'Pedidos con > 15 dias' 
							FROM                   geraLibPedConPendencia a							
							INNER JOIN             geraLibPedComportPago b
							ON                     a.CodPersona = b.CodigoConsultora               
							WHERE                  a.TotalPagar + a.DeudaYSaldoCapital >= 1000000
							AND                    b.PromedioDeDiasDeRetraso > 15
							AND                    a.CreditoTLAExcedido > 100
							AND                    a.CtdTitulosPendientes >= 3
							AND                    a.CtdTitulosVencidos > 0
							AND                    a.TipoPendencia  LIKE '%Crédito%'


					 
			   
						   INSERT INTO LibPedEventos

						   SELECT 'Retenidos > 15 dias', '1', GETDATE()
			    

				  END

-- SI NINGUNO CUMPLE CON LOS PARAMETROS
	   
	              IF EXISTS(SELECT                 [Pedido]
												  ,[CodPersona]
												  ,[NombreRevendedor]
												  ,[Puntos]
												  ,[Ciclo]
												  ,[FechaPedido]
												  ,[TotalPagar]
												  ,[TipoPendencia]
												  ,[DeudaYSaldoCapital]
												  ,[PorUsoCreditoPedido]
												  ,[CreditoExcedido]
												  ,[CreditoTLAExcedido]
												  ,[ValorDeLosTitulosVencidos]
												  ,[CantidadDiasRetraso]
												  ,[PedidosEnRetraso]
												  ,[CtdTitulosPendientes]
												  ,[CtdTitulosVencidos]
												  ,[EstructuraPadre]
												  ,[Estructura]
												  ,[TelResidencial]
												  ,[TelMovil]
												  ,[PerfilesActivos]
												  ,[PerfilesInactivos]
												  ,[CicloUltimoPedidoFacturado]
												  ,[PerfilCredito]
												  ,[Otrasinformaciones]			            
							FROM                   geraLibPedConPendencia a
							INNER JOIN             geraLibPedComportPago b
							ON                     a.CodPersona = b.CodigoConsultora               
							WHERE                  a.TotalPagar + a.DeudaYSaldoCapital < 1000000
							AND                    a.CantidadDiasRetraso < 15
							AND                    a.CtdTitulosPendientes < 3
							AND                    a.CtdTitulosVencidos <= 0
							AND                    a.TipoPendencia LIKE '%Crédito%')

			BEGIN
		 
		                    INSERT INTO             LibPedMasivos

							SELECT                 '1'
							                      ,'Cumple con Politicas Cartera'
							                      ,[Pedido]
												  ,[CodPersona]
												  ,[NombreRevendedor]
												  ,[Puntos]
												  ,[Ciclo]
												  ,[FechaPedido]
												  ,[TotalPagar]
												  ,[TipoPendencia]
												  ,[DeudaYSaldoCapital]
												  ,[PorUsoCreditoPedido]
												  ,[CreditoExcedido]
												  ,[CreditoTLAExcedido]
												  ,[ValorDeLosTitulosVencidos]
												  ,[CantidadDiasRetraso]
												  ,[PedidosEnRetraso]
												  ,[CtdTitulosPendientes]
												  ,[CtdTitulosVencidos]
												  ,[EstructuraPadre]
												  ,[Estructura]
												  ,[TelResidencial]
												  ,[TelMovil]
												  ,[PerfilesActivos]
												  ,[PerfilesInactivos]
												  ,[CicloUltimoPedidoFacturado]
												  ,[PerfilCredito]
												  ,[Otrasinformaciones]			            
							FROM                   geraLibPedConPendencia a
							INNER JOIN             geraLibPedComportPago b
							ON                     a.CodPersona = b.CodigoConsultora               
							WHERE                  a.TotalPagar + a.DeudaYSaldoCapital < 1000000
							AND                    a.CantidadDiasRetraso < 15
							AND                    a.CtdTitulosPendientes < 3
							AND                    a.CtdTitulosVencidos <= 0
							AND                    a.TipoPendencia  LIKE '%Crédito%'
		      
	  END		     

----  SI EXISTEN PEDIDOS A RETENER > 26 DIAS

	   IF EXISTS(SELECT           a.FechaPedido
								, a.Pedido
								, a.CodPersona
								, a.NombreRevendedor
								, a.EstructuraPadre
		      					, a.Estructura	
								, a.TipoPendencia								
		FROM                   geraLibPedConPendencia a
		INNER JOIN             geraLibPedComportPago b
		ON                     a.CodPersona = b.CodigoConsultora               
		WHERE                  a.TotalPagar + a.DeudaYSaldoCapital < 1000000
		AND                    b.PromedioDeDiasDeRetraso > 26
		AND                    a.CreditoTLAExcedido > 100
		AND                    a.CtdTitulosPendientes >= 3
		AND                    a.CtdTitulosVencidos > 0
		AND                    a.TipoPendencia  LIKE '%Crédito%')

		           BEGIN
				       
					   INSERT INTO             LibPedRetenidos

							SELECT                 a.FechaPedido
												 , a.Pedido
												 , a.CodPersona
												 , a.NombreRevendedor
												 , a.EstructuraPadre
		      									 , a.Estructura	
												 , RTRIM('ABONO $') + ' ' + RTRIM(CONVERT(bigint,(CupoTolerancia + CreditoTLAExcedido) + (6 + 2 * 5721))) AS Abono
												 , 'Pedidos con > 26 dias' 
							FROM                   geraLibPedConPendencia a							
							INNER JOIN             geraLibPedComportPago b
							ON                     a.CodPersona = b.CodigoConsultora               
							WHERE                  a.TotalPagar + a.DeudaYSaldoCapital < 1000000
							AND                    b.PromedioDeDiasDeRetraso > 26
							AND                    a.CreditoTLAExcedido > 100
							AND                    a.CtdTitulosPendientes >= 3
							AND                    a.CtdTitulosVencidos > 0
							AND                    a.TipoPendencia  LIKE '%Crédito%'


					 
			   
						   INSERT INTO LibPedEventos

						   SELECT 'Retenidos > 26 dias', '1', GETDATE()
			    

				  END

	-- LIBERAR MASIVOS > 26 dias


                  IF EXISTS(SELECT                 [Pedido]
												  ,[CodPersona]
												  ,[NombreRevendedor]
												  ,[Puntos]
												  ,[Ciclo]
												  ,[FechaPedido]
												  ,[TotalPagar]
												  ,[TipoPendencia]
												  ,[DeudaYSaldoCapital]
												  ,[PorUsoCreditoPedido]
												  ,[CreditoExcedido]
												  ,[CreditoTLAExcedido]
												  ,[ValorDeLosTitulosVencidos]
												  ,[CantidadDiasRetraso]
												  ,[PedidosEnRetraso]
												  ,[CtdTitulosPendientes]
												  ,[CtdTitulosVencidos]
												  ,[EstructuraPadre]
												  ,[Estructura]
												  ,[TelResidencial]
												  ,[TelMovil]
												  ,[PerfilesActivos]
												  ,[PerfilesInactivos]
												  ,[CicloUltimoPedidoFacturado]
												  ,[PerfilCredito]
												  ,[Otrasinformaciones]			            
							FROM                   geraLibPedConPendencia a
							INNER JOIN             geraLibPedComportPago b
							ON                     a.CodPersona = b.CodigoConsultora               
							WHERE                  a.TotalPagar + a.DeudaYSaldoCapital < 1000000
							AND                    a.CantidadDiasRetraso < 26
							AND                    a.CtdTitulosPendientes < 3
							AND                    a.CtdTitulosVencidos <= 0
							AND                    a.TipoPendencia  LIKE '%Crédito%')

			BEGIN
		 
		          INSERT INTO             LibPedMasivos

							SELECT                 '1'
							                      ,'Cumple con Politicas Cartera'
							                      ,[Pedido]
												  ,[CodPersona]
												  ,[NombreRevendedor]
												  ,[Puntos]
												  ,[Ciclo]
												  ,[FechaPedido]
												  ,[TotalPagar]
												  ,[TipoPendencia]
												  ,[DeudaYSaldoCapital]
												  ,[PorUsoCreditoPedido]
												  ,[CreditoExcedido]
												  ,[CreditoTLAExcedido]
												  ,[ValorDeLosTitulosVencidos]
												  ,[CantidadDiasRetraso]
												  ,[PedidosEnRetraso]
												  ,[CtdTitulosPendientes]
												  ,[CtdTitulosVencidos]
												  ,[EstructuraPadre]
												  ,[Estructura]
												  ,[TelResidencial]
												  ,[TelMovil]
												  ,[PerfilesActivos]
												  ,[PerfilesInactivos]
												  ,[CicloUltimoPedidoFacturado]
												  ,[PerfilCredito]
												  ,[Otrasinformaciones]			            
							FROM                   geraLibPedConPendencia a
							INNER JOIN             geraLibPedComportPago b
							ON                     a.CodPersona = b.CodigoConsultora               
							WHERE                  a.TotalPagar + a.DeudaYSaldoCapital < 1000000
							AND                    a.CantidadDiasRetraso < 26
							AND                    a.CtdTitulosPendientes < 3
							AND                    a.CtdTitulosVencidos <= 0
							AND                    a.TipoPendencia  LIKE '%Crédito%'
		      
	  END		     
	

	
	-- CONSULTA CON PERFIL E


	   IF EXISTS(SELECT       a.FechaPedido
							, a.Pedido
							, a.CodPersona
							, a.NombreRevendedor
							, a.EstructuraPadre
							, a.Estructura		   

	FROM                      geraLibPedConPendencia a

	INNER JOIN                geraLibPedComportPago b
 			ON                a.CodPersona = b.CodigoConsultora

		 WHERE                a.PerfilCredito = 'Clase de TL (E)'
					AND       b.CantCiclosActivos <= 4
					AND       a.TipoPendencia  LIKE '%Crédito%')

		BEGIN

		      INSERT INTO LibPedRetenidos
		     
			     SELECT       a.FechaPedido
							, a.Pedido
							, a.CodPersona
							, a.NombreRevendedor
							, a.EstructuraPadre
							, a.Estructura
							, RTRIM('ABONO $') + ' ' + RTRIM(CONVERT(bigint,(CupoTolerancia + CreditoTLAExcedido) + (6 + 2 * 5721))) AS Abono
							, 'Pedidos con Perfil de credito E'    

	FROM                      geraLibPedConPendencia a

	INNER JOIN                geraLibPedComportPago b
 			ON                a.CodPersona = b.CodigoConsultora

		 WHERE                a.PerfilCredito = 'Clase de TL (E)'
					AND       b.CantCiclosActivos <= 4
					AND       a.TipoPendencia  LIKE '%Crédito%'
		    
		END


	-- pedidos que superan los 7 millones


	   IF EXISTS(SELECT       a.FechaPedido
							, a.Pedido
							, a.CodPersona
							, a.NombreRevendedor
							, a.EstructuraPadre
							, a.Estructura		   

	FROM                      geraLibPedConPendencia a

	INNER JOIN                geraLibPedComportPago b
 			ON                a.CodPersona = b.CodigoConsultora

		 WHERE                a.TotalPagar + a.DeudaYSaldoCapital >= 7000000
		   AND                a.TipoPendencia  LIKE '%Crédito%')

		BEGIN

		      INSERT INTO LibPedRetenidos
		     
			     SELECT       a.FechaPedido
							, a.Pedido
							, a.CodPersona
							, a.NombreRevendedor
							, a.EstructuraPadre
							, a.Estructura
							, RTRIM('ABONO $') + ' ' + RTRIM(CONVERT(bigint,(CupoTolerancia + CreditoTLAExcedido) + (6 + 2 * 5721))) AS Abono
							, 'Pedidos con saldo mayo a 7 millones' 	   

	FROM                      geraLibPedConPendencia a

	INNER JOIN                geraLibPedComportPago b
 			ON                a.CodPersona = b.CodigoConsultora

		 WHERE                a.TotalPagar + a.DeudaYSaldoCapital >= 7000000
		   AND                a.TipoPendencia LIKE '%Crédito%'
		    
		END

		INSERT INTO LibPedEventos
			   SELECT 'Sp_LibPedRetener', 1, GETDATE()

			   UPDATE rfparam SET 
					  CCNOT1 = 'Termino'
					, CCUDC1 = 1
					, CCMNDT = GETDATE()
 
				WHERE CCTABL = 'LIBPEDLOG' AND CCCODE = 'Sp_LibPedRetener'

END TRY
    
	    

BEGIN CATCH
      INSERT INTO LibPedEventos
			   SELECT 'Sp_LibPedRetener', 0, GETDATE()

			   UPDATE rfparam SET 
					  CCNOT1 = 'Error'
					, CCUDC1 = 0
					, CCMNDT = GETDATE()
 
				WHERE CCTABL = 'LIBPEDLOG' AND CCCODE = 'Sp_LibPedRetener' 
END CATCH



END