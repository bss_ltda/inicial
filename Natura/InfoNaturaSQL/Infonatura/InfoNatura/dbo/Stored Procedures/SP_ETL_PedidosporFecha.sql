﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_ETL_PedidosporFecha]
@rutaArchivo varchar(255),
@idLogAuditoria int	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SET @rutaArchivo = REPLACE( @rutaArchivo, 'D:\Sitios\', '\\10.21.3.15\' )
	INSERT INTO RFLOG( OPERACION, EVENTO ) VALUES('BULKINSERT', @rutaArchivo)
	
	--\\10.21.3.15\xm\FTP_CSV\CarteraPendiente
	--D:\Sitios\xm\FTP_CSV\CarteraPendiente\ConsultaTitulo_78afeb01-43.csv

--TRUNCATE TABLE geraCarteraPendienteW;


--DECLARE @rutaArchivo varchar(255) = '\\10.21.3.15\xm\FTP_CSV\CarteraPendiente\carteraP.csv'

--DECLARE @SQL_BULK VARCHAR(MAX);
--SET @SQL_BULK = 'BULK INSERT geraCarteraPendienteW FROM ''' + @rutaArchivo + ''' WITH
--        (
--        FIRSTROW = 2,
--        FIELDTERMINATOR = ''|'',
--        ROWTERMINATOR = ''\n'',
--		CODEPAGE = ''ACP''
--        )'
--print @SQL_BULK
--print 'Importando ' + @rutaArchivo;
--EXEC (@SQL_BULK);
;



END