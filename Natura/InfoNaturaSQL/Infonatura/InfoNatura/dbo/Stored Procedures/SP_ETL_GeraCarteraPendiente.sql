﻿-- =============================================
-- Author:		JASC
-- Create date: 20-04-2018
-- Description:	Bulk Insert Archivo geraCarteraPendiente
-- =============================================
CREATE PROCEDURE [dbo].[SP_ETL_GeraCarteraPendiente]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here	
	
SET ANSI_NULLS ON;

SET QUOTED_IDENTIFIER ON;


BEGIN TRY
	DECLARE @SQL_BULK VARCHAR(MAX);
	DECLARE @mensajeProceso AS VARCHAR(MAX);
	SET @mensajeProceso = 'Sin Procesar el SP';

	TRUNCATE TABLE geraCarteraPendienteW;

	DECLARE @path varchar(255)
set @path = 'D:\Sitios\xm\FTP_CSV\CarteraPendiente\ConsultaTitulo.csv'
	SET @path = REPLACE( @path, 'D:\Sitios\', '\\10.21.3.15\' )
		--D:\Sitios\xm\FTP_CSV\CarteraPendiente\ConsultaTitulo_2d06e513-d5ec-42c8-81bf-907a8890245c.csv

	--SET @path = 'C:\Temp\CarteraTitulo.csv'

	SET @SQL_BULK = 'BULK INSERT geraCarteraPendienteW FROM ''' + @path + ''' WITH
        (
        FIRSTROW = 2,
        DATAFILETYPE =''widechar'',
        FIELDTERMINATOR = '';'',
        ROWTERMINATOR = ''\n'',
		CODEPAGE = ''ACP''
        )'
        
        SET @mensajeProceso = 'Importando ' + @path;
--print @SQL_BULK
--print 'Importando ' + @path;
EXEC (@SQL_BULK);
SET @mensajeProceso = 'Actualizando...';
UPDATE geraCarteraPendiente SET				
       Titulo						=      w.Titulo					
      ,Cuota						=      w.Cuota					
      ,NumeroPedido					=      w.NumeroPedido				
      ,CicloCaptacion				=      w.CicloCaptacion			
      ,CicloIndicador				=      w.CicloIndicador			
      ,Grupo						=      w.Grupo					
      ,NumeroFactura				=      Replace( w.NumeroFactura, '.', '' )			
      ,FechaPedido					=      convert(datetime, w.FechaPedido , 105)				
      ,FechaVencimientoOriginal		=      convert(datetime, w.FechaVencimientoOriginal	, 105)
      ,FechaVencimiento				=      convert(datetime, w.FechaVencimiento	, 105)			
      ,ValorTitulo					=      Replace( w.ValorTitulo, '.', '' )				
      ,SaldoPrincipal				=      Replace( w.SaldoPrincipal, '.', '' )			
      ,SaldoFinanciero				=      Replace( w.SaldoFinanciero, '.', '' )			
      ,SaldoTotal					=      Replace( w.SaldoTotal, '.', '' )				
      ,SaldoActualizado				=      Replace( w.SaldoActualizado, '.', '' )			
      ,Situacion					=      w.Situacion				
	  ,DiasDeRetraso				=	   w.DiasDeRetraso			
	  ,ControlDeEnvioLaPerdida		=	   w.ControlDeEnvioLaPerdida	
	  ,FechaDeEnvioLaPerdida		=	   case when w.FechaDeEnvioLaPerdida is null then null else convert(datetime, w.FechaDeEnvioLaPerdida , 105) end
	  ,FechaSaldo					=	   convert(datetime, w.FechaSaldo , 105)
	  ,DataSaldoCorregido			=	   w.DataSaldoCorregido		
	  ,FechaLiquidacion				=	   case when  w.FechaLiquidacion is null then null else convert(datetime, w.FechaLiquidacion , 105) end
	  ,EntidadConciliacion			=	   w.EntidadConciliacion		
	  ,Negociacion					=	   w.Negociacion				
	  ,FaseDelCobro					=	   w.FaseDelCobro				
	  ,CodEstructuraPadre			=	   Replace( w.CodEstructuraPadre, '.', '' )
	  ,EstructuraPadre				=	   w.EstructuraPadre			
	  ,CodEstructura				=	   Replace( w.CodEstructura, '.', '' )
	  ,Estructura					=	   w.Estructura				
	  ,CodigoPersona				=	   Replace( w.CodigoPersona, '.', '' )
	  ,Nombre						=	   w.Nombre					
	  ,Direccion					=	   w.Direccion				
	  ,TelefonoResidencial			=	   w.TelefonoResidencial		
	  ,TelefonoMovil				=	   w.TelefonoMovil			
	  ,CorreoElectronicoPersonal	=	   w.CorreoElectronicoPersonal
	  ,EntidadDeCredito				=	   w.EntidadDeCredito			
	  ,EntidadDeCobro				=	   w.EntidadDeCobro			
	  ,CodPlanRecibimiento			=	   w.CodPlanRecibimiento		
	  ,PlanRecibimiento				=	   w.PlanRecibimiento			
	  ,CodModeloComercial			=	   w.CodModeloComercial		
	  ,ModeloComercial				=	   w.ModeloComercial			
	  ,Contestado					=	   w.Contestado				
	  ,Nsu							=	   w.Nsu						
FROM geraCarteraPendiente r Inner Join geraCarteraPendienteW w on r.[Titulo] = w.[Titulo] ;
SET @mensajeProceso = 'Creando...';
INSERT INTO geraCarteraPendiente (       					
       Titulo						
      ,Cuota						
      ,NumeroPedido					
      ,CicloCaptacion				
      ,CicloIndicador				
      ,Grupo						
      ,NumeroFactura				
      ,FechaPedido					
      ,FechaVencimientoOriginal		
      ,FechaVencimiento				
      ,ValorTitulo					
      ,SaldoPrincipal				
      ,SaldoFinanciero				
      ,SaldoTotal					
      ,SaldoActualizado				
      ,Situacion					
	  ,DiasDeRetraso				
	  ,ControlDeEnvioLaPerdida		
	  ,FechaDeEnvioLaPerdida		
	  ,FechaSaldo					
	  ,DataSaldoCorregido			
	  ,FechaLiquidacion				
	  ,EntidadConciliacion			
	  ,Negociacion					
	  ,FaseDelCobro					
	  ,CodEstructuraPadre			
	  ,EstructuraPadre				
	  ,CodEstructura				
	  ,Estructura					
	  ,CodigoPersona				
	  ,Nombre						
	  ,Direccion					
	  ,TelefonoResidencial			
	  ,TelefonoMovil				
	  ,CorreoElectronicoPersonal	
	  ,EntidadDeCredito				
	  ,EntidadDeCobro				
	  ,CodPlanRecibimiento			
	  ,PlanRecibimiento				
	  ,CodModeloComercial			
	  ,ModeloComercial				
	  ,Contestado					
	  ,Nsu)
SELECT			
	  w.Titulo					
	 ,w.Cuota					
	 ,w.NumeroPedido				
	 ,w.CicloCaptacion			
	 ,w.CicloIndicador			
	 ,w.Grupo					
	 ,Replace( w.NumeroFactura, '.', '' )			
	 ,convert(datetime, w.FechaPedido , 105)				
	 ,convert(datetime, w.FechaVencimientoOriginal	, 105)
	 ,convert(datetime, w.FechaVencimiento	, 105)			
	 ,Replace( w.ValorTitulo, '.', '' )			
	 ,Replace( w.SaldoPrincipal, '.', '' )			
	 ,Replace( w.SaldoFinanciero, '.', '' )		
	 ,Replace( w.SaldoTotal, '.', '' )				
	 ,Replace( w.SaldoActualizado, '.', '' )			
	 ,w.Situacion				
	 ,w.DiasDeRetraso			
	 ,w.ControlDeEnvioLaPerdida	
	 ,case when w.FechaDeEnvioLaPerdida is null then null else convert(datetime, w.FechaDeEnvioLaPerdida , 105) end
	 ,convert(datetime, w.FechaSaldo , 105)
	 ,w.DataSaldoCorregido		
	 ,case when w.FechaLiquidacion is null then null else convert(datetime, w.FechaLiquidacion , 105) end
	 ,w.EntidadConciliacion
	 ,w.Negociacion				
	 ,w.FaseDelCobro				
	 ,Replace( w.CodEstructuraPadre, '.', '' )
	 ,w.EstructuraPadre			
	 ,Replace( w.CodEstructura, '.', '' )
	 ,w.Estructura				
	 ,Replace( w.CodigoPersona, '.', '' )
	 ,w.Nombre					
	 ,w.Direccion				
	 ,w.TelefonoResidencial		
	 ,w.TelefonoMovil			
	 ,w.CorreoElectronicoPersonal
	 ,w.EntidadDeCredito			
	 ,w.EntidadDeCobro			
	 ,w.CodPlanRecibimiento		
	 ,w.PlanRecibimiento			
	 ,w.CodModeloComercial		
	 ,w.ModeloComercial			
	 ,w.Contestado				
	 ,w.Nsu
FROM geraCarteraPendienteW w
WHERE w.[Titulo] In( 
SELECT w.Titulo	  
FROM geraCarteraPendienteW w Left Join geraCarteraPendiente r on w.[Titulo] = r.[Titulo] 
WHERE r.[Titulo]  IS NULL ) ;	 

SELECT 'OK' AS 'MensajeProceso';
END TRY
BEGIN CATCH 
	SET @mensajeProceso = 'ERROR: Linea:' + CAST (ERROR_LINE() AS VARCHAR) + ' - Mensaje de Error:' + ERROR_MESSAGE();				
	SELECT @mensajeProceso AS 'MensajeProceso';
END CATCH
END