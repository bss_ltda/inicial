﻿-- =============================================
-- Author:		fm
-- Create date: 2018-05-28
-- Description:	Log procesos ETL
-- =============================================
CREATE PROCEDURE [dbo].[SP_ETL_LOG]
@ETL varchar(255),
@rutaArchivo varchar(500),
@idLogAuditoria int	,
@Resultado decimal(1, 0)	,
@mensajeProceso AS VARCHAR(MAX)
AS
BEGIN

	IF NOT EXISTS(SELECT CCNUM FROM RFPARAM WHERE CCTABL = 'ETL_FTP' AND CCCODE2 = @ETL)
	INSERT INTO RFPARAM(CCID, CCTABL,    CCCODE, CCCODE2, CCDESC )
                 VALUES('CC', 'ETL_FTP', 'gera', @ETL,    @ETL   )

	UPDATE RFPARAM SET 
	    CCMNDT = GETDATE()
	  , CCCODEN = @idLogAuditoria
	  , CCUDC2 = @Resultado
	  , CCNOT2 = @rutaArchivo
	  , CCNOTE = @mensajeProceso
	WHERE CCTABL = 'ETL_FTP' AND CCCODE = 'gera' AND CCCODE2 = @ETL ;
	
	IF @Resultado <> 1
		EXEC [dbo].[SP_ETL_Aviso] @ETL
	

END