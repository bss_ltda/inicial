﻿-- =============================================
-- Author:		FM
-- Create date: 2018-09-03
-- Description:	ETL REVISTAS
-- =============================================
CREATE PROCEDURE [dbo].[SP_ETL_RevistasInterrapidisimo]
AS
BEGIN
	SET NOCOUNT ON;
BEGIN TRY

	DECLARE @SQL_BULK VARCHAR(MAX);
	DECLARE @mensajeProceso AS VARCHAR(MAX);
	DECLARE @ID_TRANSP AS int;

	SET @mensajeProceso = 'Sin Procesar el SP';
	SET @ID_TRANSP = 221697;

	TRUNCATE TABLE RevistasInterrapidisimo;
	BULK INSERT RevistasInterrapidisimo FROM '\\10.21.3.15\xm\Revistas\Interrapidisimo.txt' WITH
        (
        FIRSTROW = 2,
        DATAFILETYPE ='widechar',
        FIELDTERMINATOR = '\t',
        ROWTERMINATOR = '\n',
		CODEPAGE = 'ACP'
        )


	UPDATE RevistasTracking SET 
		  CodConsultora              = w.IdDestinatario  
		, Ciudad                     = LTRIM(RTRIM(SUBSTRING( w.CiudadDestino, 1, CHARINDEX('/', w.CiudadDestino) -1 )))
		, Depto                      = w.DepartamentoDestino 
		, Direccion                  = w.DireccionDestinatario 
--		, Telefono                   = w.TelResidencia 
--		, Celular                    = w.Celular
		, FechaDespacho              = w.FechaEntrega  
		, Ciclo                      = w.OrdenPedido  
		, Estado                     = w.Estado
		, Novedad1                   = w.EstadoController 
	FROM RevistasTracking r Inner Join 
		 RevistasInterrapidisimo w on r.IdTransportadora = @ID_TRANSP and r.NumeroGuia = w.NumeroGuia   ;


	INSERT INTO RevistasTracking ( 
		  NumeroGuia
		, IdTransportadora
		, CodConsultora
		, Ciudad
		, Depto
		, Direccion
--		, Telefono
--		, Celular
		, FechaDespacho
		, Ciclo
		, Estado
		, Novedad1
	)
	SELECT 
		  w.NumeroGuia  
		, @ID_TRANSP
		, w.IdDestinatario
		, LTRIM(RTRIM(SUBSTRING( w.CiudadDestino, 1, CHARINDEX('/', w.CiudadDestino) -1 )))
		, w.DepartamentoDestino
		, w.DireccionDestinatario
--		, w.TelResidencia
--		, w.Celular
		, w.FechaEntrega
		, w.OrdenPedido
		, w.Estado
		, w.EstadoController
	 FROM RevistasInterrapidisimo w Left Join RevistasTracking r on r.IdTransportadora = @ID_TRANSP and w.NumeroGuia   = r.NumeroGuia
	 WHERE r.NumeroGuia Is Null ;


	SELECT 'OK' AS 'MensajeProceso';
	END TRY
	BEGIN CATCH 
		SET @mensajeProceso = 'Se ha generado un error en la Estructura del BULK-INSERT: Linea#:' + CAST (ERROR_LINE() AS VARCHAR) + ' - Mensaje de Error:' + ERROR_MESSAGE();				
		SELECT @mensajeProceso AS 'MensajeProceso';
	END CATCH
END