﻿-- =============================================
-- Author:		Fredy Manrique
-- Create date: 2017-04-26 22:18:57
-- Update date: 2017-10-11 08:09:06
-- Description:	Genera Productividad de Rituales
-- =============================================
CREATE PROCEDURE [dbo].[RitualesProcesa]
	(@Ciclo BigInt)
AS
BEGIN 

BEGIN TRY

	SET NOCOUNT ON;
	Declare @comando nvarchar(400)	
	Declare @Desde nvarchar(400)	
	Declare @Estado int
	Declare @Paso nvarchar(100)	
	
	SELECT @Estado = CCUDC2
	FROM RFPARAM
	WHERE ( CCTABL = 'ProductividadRituales' AND CCCODE = 'Ciclo' )

	truncate table RitualesAsistentes; 
	truncate table RitualesPedidos; 

	DELETE FROM RitualesResumen WHERE Ciclo = @Ciclo; 
	
	SELECT TOP 1 @Desde = replace( [FADESCRIP], 'C:\', '\\10.21.3.15\' )  
	FROM rfadjunto WHERE FACATEG01 = 'Rituales' AND FACATEG02 = 'Asistentes' AND FAPROCNUM = @Ciclo
	ORDER BY FANUMREG DESC
	
	SET @Paso = 'RitualesAsistentes'
	SET @Comando = 'BULK INSERT RitualesAsistentes FROM ''' + @Desde + ''' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR='';'', ROWTERMINATOR=''\n'' )'
	PRINT @comando
	EXEC sp_executesql @comando
	
	SET @Paso = 'RitualesPedidos'
	SELECT TOP 1 @Desde = replace( [FADESCRIP], 'C:\', '\\10.21.3.15\' )  
	FROM rfadjunto WHERE FACATEG01 = 'Rituales' AND FACATEG02 = 'Pedidos' AND FAPROCNUM = @Ciclo
	ORDER BY FANUMREG DESC
	
	SET @Comando = 'BULK INSERT RitualesPedidos FROM ''' + @Desde + ''' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR='';'', ROWTERMINATOR=''\n'' )'
	PRINT @comando
	EXEC sp_executesql @comando

	SET @Paso = 'RitualesResumen'
	INSERT INTO [RitualesResumen]
           ([Ciclo]
           ,[Gerencia]
           ,[CodSector]
           ,[Sector]
           ,[GrupoCNO]
           ,[ValorLiquido]
           ,[Pedidos]
           ,[CantCNs]
           )
	SELECT
		@Ciclo
	  , Gerencia
	  , CodSector
	  , Sector
	  , GrupoCNO
	  , ISNULL(AVG(ValorLiquido), 0 ) AS ValorLiquido
	  , COUNT(DISTINCT Persona)       AS Pedidos
	  , COUNT(DISTINCT CODCN)         AS CantCNs 
	FROM
		RitualesAsistentes
		LEFT JOIN
			(
			
				Select
				  p.Persona,
				  Sum(p.ValorLiquido) as ValorLiquido,
				  Max(p.Pedido) as Pedido
				From
				  RitualesPedidos p
				Group By
				  p.Persona			
			
			)RitualesPedidosResu
			ON
				RitualesAsistentes.CodCN = RitualesPedidosResu.Persona
	WHERE
		Gerencia IS NOT NULL
	GROUP BY
		Gerencia
	  , CodSector
	  , Sector
	  , GrupoCNO
	
	UPDATE RFPARAM SET CCUDC2 = 0 , CCUDTS = GETDATE(), CCNOTE = 'Proceso Ejecutado'
	WHERE ( CCTABL = 'ProductividadRituales' AND CCCODE = 'Ciclo' ) 

	SELECT CCCODE, CCCODEN, CCNOT1, CCUDC1, CCNOTE, CCNUM, CCUDTS, CCCODEN2 
	FROM RFPARAM
	WHERE ( CCTABL = 'ProductividadRituales' AND CCCODE = 'Ciclo' ) 
END TRY
BEGIN CATCH
	DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();
    DECLARE @ErrorProcedure INT = ERROR_PROCEDURE();

    PRINT 'Actual error number: ' + CAST(@ErrorNumber AS VARCHAR(10)) + '.';
    PRINT 'Actual line number: ' + CAST(@ErrorLine AS VARCHAR(10)) + '.';
    --PRINT 'Paso: ' + @Paso + '.';
    
	UPDATE RFPARAM SET CCUDC2 = 0 , CCUDTS = GETDATE(), CCNOTE = 'Ejecucion:' +  CONVERT(VARCHAR(30),GETDATE(), 20   ) +'<br>Paso: ' + @Paso + '<br>' + @ErrorMessage
	WHERE ( CCTABL = 'ProductividadRituales' AND CCCODE = 'Ciclo' ) 
    
SELECT  
        ERROR_NUMBER() AS ErrorNumber  
        ,ERROR_SEVERITY() AS ErrorSeverity  
        ,ERROR_STATE() AS ErrorState  
        ,ERROR_PROCEDURE() AS ErrorProcedure  
        ,ERROR_LINE() AS ErrorLine  
        ,ERROR_MESSAGE() AS ErrorMessage; 
        
    RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
        
END CATCH;
END