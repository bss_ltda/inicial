﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_LibPed7Millon]
AS

DECLARE @err_message VARCHAR(100)
DECLARE @result VARCHAR(50) = '1'

BEGIN


 BEGIN TRY

TRUNCATE TABLE LibPed7Millon

IF EXISTS(SELECT      a.FechaPedido
					, a.Pedido
					, a.CodPersona
					, a.NombreRevendedor
					, a.EstructuraPadre
					, a.Estructura		   

		FROM        geraLibPedConPendencia a

		INNER JOIN  geraLibPedComportPago b
				ON  a.CodPersona = b.CodigoConsultora

		WHERE         a.TotalPagar + a.DeudaYSaldoCapital >= '7000000')

	  BEGIN 

	  INSERT INTO LibPed7Millon
       
	      SELECT      a.FechaPedido
					, a.Pedido
					, a.CodPersona
					, a.NombreRevendedor
					, a.EstructuraPadre
					, a.Estructura		   
					, 'Alerta' AS 'Estado'
					, a.TotalPagar + a.DeudaYSaldoCapital 'DeudaTotal'
		FROM        geraLibPedConPendencia a	      

		WHERE       a.TotalPagar + a.DeudaYSaldoCapital >= '7000000'

               PRINT @result
			   			   
			   INSERT INTO LibPedEventos

			   SELECT 'Sp_LibPed7Millon', @result, GETDATE()
			   
			   UPDATE rfparam SET 
				  CCNOT1 = 'Termino'
				, CCUDC1 = 1
				, CCMNDT = GETDATE() 
             WHERE CCTABL = 'LIBPEDLOG' AND CCCODE = 'Sp_LibPed7Millon'

    END


	 ELSE
	    BEGIN
	          SET @err_message = '0'
              RAISERROR (@err_message, 11,1)
        END

	 END TRY 

	 BEGIN CATCH 
	     INSERT INTO LibPedEventos
         SELECT 'Sp_LibPed7Millon', '0', GETDATE()
         
         UPDATE rfparam SET 
				  CCNOT1 = 'No encontro datos con la condicion'
				, CCUDC1 = 0
				, CCMNDT = GETDATE() 
           WHERE CCTABL = 'LIBPEDLOG' AND CCCODE = 'Sp_LibPed7Millon'
         
	 END CATCH


END