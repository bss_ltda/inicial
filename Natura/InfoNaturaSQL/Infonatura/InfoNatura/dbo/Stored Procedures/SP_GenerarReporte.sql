﻿CREATE PROCEDURE [dbo].[SP_GenerarReporte] (@Dias nvarchar(255), @Mora nvarchar (255), @Gerencia nvarchar (255), @cilo1 nvarchar (255), @ciclo2 nvarchar(255), @reporte nvarchar(255) ,@agencia nvarchar(255)) 
AS
BEGIN
	SET NOCOUNT ON;
truncate table RCartraPlanillaTemp;
insert into RCartraPlanillaTemp Select * from RCartraPlanilla where RCartraPlanilla.[Dia Mora]>@Dias and RCartraPlanilla.[Valor actualizado del pedido] > @Mora and RCartraPlanilla.EstructuraPadre like '%'+ @Gerencia + '%'  and Ciclo between @cilo1 and @ciclo2 and RCartraPlanilla.[Tipo de Cartera] = @reporte and RCartraPlanilla.[Agencia de Cobranza]=@agencia
END