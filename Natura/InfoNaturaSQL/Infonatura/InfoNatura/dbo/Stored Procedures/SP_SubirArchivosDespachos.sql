﻿CREATE PROCEDURE[dbo].[SP_SubirArchivosDespachos] (@Id BigInt)
AS
BEGIN
SET NOCOUNT ON;
DECLARE @Name nvarchar(50)
DECLARE @Sobrante nvarchar(50)
DECLARE @Pedidos nvarchar(50)
DECLARE @Minuta nvarchar(50)
SELECT @Name = Codigo FROM TABLAMAESTRA WHERE TABLA = 'DIRECCION';	
SELECT @Pedidos = CCSDSC FROM ZCC WHERE CCTABL = 'ARCHIVOS' AND CCCODE = 'DESPACHOS' AND CCDESC = 'Pedidos Facturdado';
SELECT @Minuta = CCSDSC FROM ZCC WHERE CCTABL = 'ARCHIVOS' AND CCCODE = 'DESPACHOS' AND CCDESC = 'Minuta';
SELECT @Sobrante = CCSDSC FROM ZCC WHERE CCTABL = 'ARCHIVOS' AND CCCODE = 'DESPACHOS' AND CCDESC = 'Sobrantes';

Declare @comando nvarchar(400)
truncate table RequeridoPlanilla;
truncate table DespachosMinuta;

-- BULK INSERT DespachosMinuta FROM '\\10.21.3.15\InfoNaturaDatos\Despachos\Minuta.csv' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = ';', ROWTERMINATOR = '\n',CODEPAGE = 'ACP'
	set @comando = 'BULK INSERT DespachosMinuta FROM ''\\'+ @Name +'\InfoNaturaDatos\Despachos\'+ @Minuta +'.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
	EXEC sp_executesql @comando
	UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Minuta' WHERE Id=@Id


	Truncate table DespachosPedFac;  
	set @comando = 'BULK INSERT DespachosPedFac FROM ''\\'+ @Name +'\InfoNaturaDatos\Despachos\'+ @Pedidos +'.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
	EXEC sp_executesql @comando
	UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Pedidos' WHERE Id=@Id

	UPDATE DespachosPedFac SET Transportadora = REPLACE(Transportadora, 'Á', 'A'  )
	UPDATE DespachosPedFac SET Transportadora = REPLACE(Transportadora, 'É', 'E'  )
	UPDATE DespachosPedFac SET Transportadora = REPLACE(Transportadora, 'Í', 'I'  )
	UPDATE DespachosPedFac SET Transportadora = REPLACE(Transportadora, 'Ó', 'O'  )
	UPDATE DespachosPedFac SET Transportadora = REPLACE(Transportadora, 'Ú', 'U'  )
	
	UPDATE DespachosPedFac SET Transportadora = REPLACE(Transportadora, 'á', 'a'  )
	UPDATE DespachosPedFac SET Transportadora = REPLACE(Transportadora, 'é', 'e'  )
	UPDATE DespachosPedFac SET Transportadora = REPLACE(Transportadora, 'í', 'i'  )
	UPDATE DespachosPedFac SET Transportadora = REPLACE(Transportadora, 'ó', 'o'  )
	UPDATE DespachosPedFac SET Transportadora = REPLACE(Transportadora, 'ú', 'u'  )
END