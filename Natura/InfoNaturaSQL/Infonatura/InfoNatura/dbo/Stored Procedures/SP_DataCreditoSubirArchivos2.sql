﻿CREATE PROCEDURE [dbo].[SP_DataCreditoSubirArchivos2] (@Id BigInt)

AS
BEGIN

	SET NOCOUNT ON;

truncate table [DataCreditoTítulos] 
DECLARE @Name nvarchar(50)
DECLARE @Paso nvarchar(50)
SELECT @Name = Codigo FROM TABLAMAESTRA WHERE TABLA = 'DIRECCION';	
Declare @comando nvarchar(400)

BEGIN TRY

truncate table DataCreditoPersonas;
set @comando = 'BULK INSERT DataCreditoPersonas FROM ''\\'+ @Name +'\InfoNaturaDatos\DataCredito\Personas1.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
set @Paso = 'Personas 1 - Prox: Personas 2'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id
EXEC sp_executesql @comando

set @comando = 'BULK INSERT DataCreditoPersonas FROM ''\\'+ @Name +'\InfoNaturaDatos\DataCredito\Personas2.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
set @Paso = 'Personas 2 - Prox: Personas 3'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id
EXEC sp_executesql @comando

set @comando = 'BULK INSERT DataCreditoPersonas FROM ''\\'+ @Name +'\InfoNaturaDatos\DataCredito\Personas3.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
set @Paso = 'Personas 3 - Prox: Personas 4'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id
EXEC sp_executesql @comando

set @comando = 'BULK INSERT DataCreditoPersonas FROM ''\\'+ @Name +'\InfoNaturaDatos\DataCredito\Personas4.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
set @Paso = 'Personas 4 - Prox: Personas 5'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id
EXEC sp_executesql @comando

set @comando = 'BULK INSERT DataCreditoPersonas FROM ''\\'+ @Name +'\InfoNaturaDatos\DataCredito\Personas5.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
set @Paso = 'Personas 5 - Prox: Personas 6'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id
EXEC sp_executesql @comando

set @comando = 'BULK INSERT DataCreditoPersonas FROM ''\\'+ @Name +'\InfoNaturaDatos\DataCredito\Personas6.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
set @Paso = 'Personas 6 - Prox: BST'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id
EXEC sp_executesql @comando

truncate table DataCreditoBTS;

set @comando = 'BULK INSERT DataCreditoBTS FROM ''\\'+ @Name +'\InfoNaturaDatos\DataCredito\COBRO.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
set @Paso = 'BST'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id
EXEC sp_executesql @comando

truncate table [DataCreditoPlanilla] 
INSERT INTO [InfoNatura].[dbo].[DataCreditoPlanilla]
           ([Codigo]
           ,[Tipo de documento]
           ,[Noidentificación]
           ,[Numero de obligacion]
           ,[Nombre completo]
           ,[Situacion del titular]
           ,[Fecha apertura]
           ,[Fecha vencimiento]
           ,[Responsable]
           ,[Tipo de Obligacion]
           ,[Termino del Contrato]
           ,[Forma de pago]
           ,[Peridicidad]
           ,[Novedad]
           ,[Estado Origen de la cuenta]
           ,[Fecha estado origen]
           ,[Estadode la cuenta]
           ,[Fecha estado de la cuenta]
           ,[Adjetivo]
           ,[FechaAdjetivo]
           ,[Tipo de moneda]
           ,[Tipo de garantía]
           ,[Calificacion]
           ,[Edad de mora]
           ,[Valor inicial]
           ,[Valor saldo deuda]
           ,[Valor disponible]
           ,[Valor Cuota mes]
           ,[Valor saldo en  mora]
           ,[Total Cuotas]
           ,[Cuotas Canceladas]
           ,[cuotas en mora]
           ,[Clausula de permanencia]
           ,[Fecha clausula de permanencia]
           ,[Fecha limite de pago]
           ,[Fecha de pago]
           ,[Oficina de radicacion]
           ,[Ciudad de radicación]
           ,[Codigo DANE ciudad de radicacion]
           ,[Ciudad residencia]
           ,[Codigo DANE ciudad de residencia]
           ,[Deparatamento de residencia]
           ,[Direccion de residencia]
           ,[Telefono de residencia]
           ,[Ciudad laboral]
           ,[Codigo DANE ciudad laboral]
           ,[Deparatamento laboral]
           ,[Direccion Laboral]
           ,[Telefono Laboral]
           ,[Ciudad de correspondencia]
           ,[Codigo DANE ciudad correspondencia]
           ,[Deparatamento Correspondencia]
           ,[Direccion Correspondencia]
           ,[Correo electronico]
           ,[Numero Celular])
     Select '','','',CodigoPedido,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','' from DataCreditoBTS where  [Valor actualizado del pedido]>10000
UPDATE DataCreditoPlanilla SET [Codigo]= Codig  FROM dbo.DataCreditoBTS AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CodigoPedido = P.[Numero de obligacion]
 UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = '1' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Tipo de documento]='1', [Situacion del titular]='0', Responsable='00',[Tipo de Obligacion]='2',[Termino del Contrato]='2',[Forma de pago]='0',Peridicidad='9',[Estado Origen de la cuenta]='0',Adjetivo='0',FechaAdjetivo='00000000', [Tipo de moneda]='1', [Tipo de garantía]='2', Calificacion='  ',[Valor disponible]='0000000000',[Total Cuotas]='000',[Cuotas Canceladas]='0000',[cuotas en mora]='000',[Clausula de permanencia]='000',[Fecha clausula de permanencia]='00000000', [Fecha estado de la cuenta]=  CONVERT(CHAR(10),GetDate (),111)
 UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = '2' WHERE Id=@Id

--DataCreditoPersonas
UPDATE  DataCreditoPersonas SET CodigoRevendedor=REPLACE(CodigoRevendedor,'.','');
 UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = '3' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET Noidentificación = [Cédula de Ciudadanía/NIT] FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CodigoRevendedor = P.Codigo
 UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = '4' WHERE Id=@Id

UPDATE  DataCreditoPlanilla SET Noidentificación=REPLACE(Noidentificación,'.','');
 UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = '5' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Correo electronico] = EMail  FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
 UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = '6' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Numero Celular]  = TelMovil   FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
 UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = '7' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Telefono Laboral]  = TelResidencial    FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
 UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = '8' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Telefono de residencia]  = TelResidencial    FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
 UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = '9' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Telefono de residencia]  = TelResidencial    FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
 UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = '10' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Ciudad de radicación]  =CiudadResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
 UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = '11' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Ciudad de correspondencia]  = CiudadResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
 UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = '12' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Ciudad residencia]  = CiudadResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
 UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = '13' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Ciudad laboral]  = CiudadResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
 UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = '14' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Deparatamento Correspondencia]  = RegionResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
 UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = '15' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Deparatamento de residencia]  = RegionResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
 UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = '16' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Deparatamento laboral]  = RegionResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
 UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = '17' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Direccion Correspondencia]  =calleResidencial + '-' + ComplementoResidencial   FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
 UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = '18' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Direccion Laboral]  = calleResidencial + '-' +  ComplementoResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
 UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = '19' WHERE Id=@Id

UPDATE DataCreditoPlanilla SET [Direccion de residencia]  = calleResidencial + '-' +  ComplementoResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
 UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = '20' WHERE Id=@Id

--DataCreditoTítulos
UPDATE DataCreditoPlanilla SET [Nombre completo] =NombrePersona  FROM dbo.DataCreditoBTS AS R iNNER JOIN dbo.DataCreditoPlanilla AS P ON  R.Cedula = P.Noidentificación and R.CodigoPedido = P.[Numero de obligacion]
UPDATE DataCreditoPlanilla SET [Fecha apertura] =CONVERT(VARCHAR(10), FechaAutFacturación ,111)  FROM dbo.DataCreditoBTS AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON  R.Cedula = P.Noidentificación and R.CodigoPedido = P.[Numero de obligacion]
UPDATE DataCreditoPlanilla SET [Fecha vencimiento] =CONVERT(VARCHAR(10),FechaVencimiento ,111) FROM dbo.DataCreditoBTS AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON  R.Cedula = P.Noidentificación and R.CodigoPedido = P.[Numero de obligacion]
UPDATE DataCreditoPlanilla SET [Fecha estado origen] =CONVERT(VARCHAR(10),FechaAutFacturación,111)  FROM dbo.DataCreditoBTS AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON  R.Cedula = P.Noidentificación and R.CodigoPedido = P.[Numero de obligacion]
UPDATE DataCreditoPlanilla SET [Valor inicial] =[Valor Original] FROM dbo.DataCreditoBTS AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON  R.Cedula = P.Noidentificación and R.CodigoPedido = P.[Numero de obligacion]
UPDATE DataCreditoPlanilla SET [Valor saldo deuda] =[Valor Original] FROM dbo.DataCreditoBTS AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON  R.Cedula = P.Noidentificación and R.CodigoPedido = P.[Numero de obligacion]
UPDATE DataCreditoPlanilla SET [Valor Cuota mes] =[Valor actualizado total]  FROM dbo.DataCreditoBTS AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON  R.Cedula = P.Noidentificación and R.CodigoPedido = P.[Numero de obligacion]
UPDATE DataCreditoPlanilla SET [Fecha limite de pago] =CONVERT(CHAR(10),[Fecha vencimiento],111)  FROM dbo.DataCreditoBTS AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON  R.Cedula = P.Noidentificación and R.CodigoPedido = P.[Numero de obligacion]
UPDATE DataCreditoPlanilla SET [Oficina de radicacion] =Estructura  FROM dbo.DataCreditoBTS AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON  R.Cedula = P.Noidentificación and R.CodigoPedido = P.[Numero de obligacion]

UPDATE DataCreditoPlanilla SET [Estadode la cuenta]='01'
--DataCreditoBTS
UPDATE DataCreditoPlanilla SET Novedad =0
UPDATE DataCreditoPlanilla SET Novedad =[Dia Mora]  FROM dbo.DataCreditoBTS  AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.Cedula = P.Noidentificación and R.CodigoPedido = P.[Numero de obligacion]
UPDATE DataCreditoPlanilla SET [Edad de mora] =Novedad 
UPDATE DataCreditoPlanilla SET Novedad ='01' WHERE Novedad >=0  AND  Novedad <=59
UPDATE DataCreditoPlanilla SET Novedad ='07' WHERE Novedad >=60 AND  Novedad <=89
UPDATE DataCreditoPlanilla SET Novedad ='08' WHERE Novedad >=90 AND  Novedad <=119
UPDATE DataCreditoPlanilla SET Novedad ='09' WHERE Novedad >=120

UPDATE DataCreditoPlanilla SET [Valor saldo en  mora] =[Valor actualizado total]  FROM dbo.DataCreditoBTS  AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.Cedula = P.Noidentificación AND P.[Numero de obligacion]=R.CodigoPedido
UPDATE DataCreditoPlanilla SET [Fecha de pago] =CONVERT(CHAR(10),[Fecha Pago],111)  FROM dbo.DataCreditoBTS  AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.Cedula = P.Noidentificación AND P.[Numero de obligacion]=R.CodigoPedido

--DataCreditoCiudades 

UPDATE DataCreditoPlanilla SET [Codigo DANE ciudad de radicacion] =CIUDAD_DANE fROM dbo.DataCreditoCiudades  AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CIUDAD = P.[Ciudad de radicación]
UPDATE DataCreditoPlanilla SET [Codigo DANE ciudad de residencia] =CIUDAD_DANE fROM dbo.DataCreditoCiudades  AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CIUDAD = P.[Ciudad residencia]
UPDATE DataCreditoPlanilla SET [Codigo DANE ciudad laboral] =CIUDAD_DANE fROM dbo.DataCreditoCiudades  AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CIUDAD = P.[Ciudad laboral]
UPDATE DataCreditoPlanilla SET [Codigo DANE ciudad correspondencia] =CIUDAD_DANE fROM dbo.DataCreditoCiudades  AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CIUDAD = P.[Ciudad de correspondencia]
UPDATE DataCreditoPlanilla SET [Fecha de pago] =CONVERT(CHAR(10),FechaVencimiento,111)  FROM dbo.DataCreditoBTS AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.Cedula = P.Noidentificación and P.[Fecha de pago]=''


UPDATE  DataCreditoPlanilla SET [Fecha apertura]=REPLACE([Fecha apertura],'-','');
UPDATE  DataCreditoPlanilla SET [Fecha vencimiento]=REPLACE([Fecha vencimiento],'-','');
UPDATE  DataCreditoPlanilla SET [Fecha de pago]=REPLACE( [Fecha de pago],'-','');
UPDATE  DataCreditoPlanilla SET [Fecha estado origen]=REPLACE([Fecha estado origen],'-','');
UPDATE  DataCreditoPlanilla SET [Fecha estado de la cuenta]=REPLACE([Fecha estado de la cuenta],'-','');
UPDATE  DataCreditoPlanilla SET [Fecha limite de pago]=REPLACE([Fecha limite de pago],'-','');


UPDATE  DataCreditoPlanilla SET [Fecha apertura]=REPLACE([Fecha apertura],'.','');
UPDATE  DataCreditoPlanilla SET [Fecha vencimiento]=REPLACE([Fecha vencimiento],'.','');
UPDATE  DataCreditoPlanilla SET [Fecha de pago]=REPLACE( [Fecha de pago],'.','');
UPDATE  DataCreditoPlanilla SET [Fecha estado origen]=REPLACE([Fecha estado origen],'.','');
UPDATE  DataCreditoPlanilla SET [Fecha estado de la cuenta]=REPLACE([Fecha estado de la cuenta],'.','');
UPDATE  DataCreditoPlanilla SET [Fecha limite de pago]=REPLACE([Fecha limite de pago],'.','');


UPDATE  DataCreditoPlanilla SET [Fecha apertura]=REPLACE([Fecha apertura],'/','');
UPDATE  DataCreditoPlanilla SET [Fecha vencimiento]=REPLACE([Fecha vencimiento],'/','');
UPDATE  DataCreditoPlanilla SET [Fecha de pago]=REPLACE( [Fecha de pago],'/','');
UPDATE  DataCreditoPlanilla SET [Fecha estado origen]=REPLACE([Fecha estado origen],'/','');
UPDATE  DataCreditoPlanilla SET [Fecha estado de la cuenta]=REPLACE([Fecha estado de la cuenta],'/','');
UPDATE  DataCreditoPlanilla SET [Fecha limite de pago]=REPLACE([Fecha limite de pago],'/','');


UPDATE DataCreditoPlanilla SET [Valor saldo en  mora]  =0  where [Valor saldo en  mora] ='';
UPDATE DataCreditoPlanilla SET [Estadode la cuenta]='02' where [Edad de mora] >=60;
END TRY
BEGIN CATCH
	DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();
    DECLARE @ErrorProcedure INT = ERROR_PROCEDURE();

    PRINT 'Actual error number: ' + CAST(@ErrorNumber AS VARCHAR(10)) + '.';
    PRINT 'Actual line number: ' + CAST(@ErrorLine AS VARCHAR(10)) + '.';
    PRINT 'Paso: ' + @Paso + '.';
	PRINT 'Mensaje Error: ' + @ErrorMessage + '.';
	PRINT  @ErrorSeverity ;
	PRINT  @ErrorState ;

    RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
    
    UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Error ' + @Paso, [Mensajes] = @ErrorMessage + CAST(@ErrorLine AS VARCHAR(10)) WHERE Id=@Id
END CATCH;
END