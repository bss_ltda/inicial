﻿-- =============================================
-- Author:		FM
-- Create date: 2018-03
-- Description:	ETL datos de servientrega para seguimiento entregas
-- =============================================
CREATE PROCEDURE  [dbo].[SP_ETL_TranspEnvia]
	@rutaArchivo varchar(255),
    @idLogAuditoria int		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
BEGIN TRY

	DECLARE @SQL_BULK VARCHAR(MAX);
	DECLARE @mensajeProceso AS VARCHAR(MAX);
--	DECLARE @rutaArchivo AS VARCHAR(255);


	SET @mensajeProceso = 'Sin Procesar el SP';

--	SET @rutaArchivo = 'C:\Temp\ENVIA.txt';

	TRUNCATE TABLE TranspEnvia;

	SET @SQL_BULK = 'BULK INSERT TranspEnvia FROM ''' + @rutaArchivo + ''' WITH
        (
        FIRSTROW = 2,
        DATAFILETYPE =''widechar'',
        FIELDTERMINATOR = ''\t'',
        ROWTERMINATOR = ''\n'',
		CODEPAGE = ''ACP''
        )'
	EXEC (@SQL_BULK);

	
	UPDATE TranspEnvia SET 
	  Guia             = Replace( Guia            , '''', '' )
	, Numero_Documento = Replace( Numero_Documento, ' ', '' )
	, Fec_Produccion   = Replace( Fec_Produccion  , '''', '' )
	, Fec_Entrega      = Replace( Fec_Entrega     , '''', '' )
	, Hora             = Replace( Hora            , '''', '' )
	, Fec_Novedad      = Replace( Fec_Novedad     , '''', '' ) ;


	UPDATE TranspEntrega SET 
		  FechaDespacho                  = convert(datetime, w.Fec_Produccion, 105) 
		, TipoDespacho                   = w.Servicio_Utilizado
		, Pedido                         = w.Numero_Documento
		, NumeroGuia                      = w.Guia
		, Destinatario                   = w.Nombre_Destinatario
		, Direccion                      = w.Direccion_Destinatario
		, CiudadDestino                  = w.Ciudad_Destino
		, Departamento                   = w.Nom_Departamento
		, Cajas                          = w.Num_Unidades 
		, Regional                       = w.Regional_Destino
		, Estado                         = w.Estado_Actual
		, FechaEntrega                   = convert(datetime, w.Fec_Entrega, 105)
		, TipoNovedad                    = w.Des_Novedad
		, FechaNovedad                   = convert(datetime, w.Fec_Novedad, 105)
		, FechaCargueTransp              = getDate()
	FROM TranspEntrega r Inner Join TranspEnvia w on r.idTransportadora = 123665 And r.NumeroGuia = w.Guia ;

	INSERT INTO TranspEntrega ( 
		  idTransportadora
		, FechaDespacho     
		, TipoDespacho      
		, Pedido            
		, NumeroGuia              
		, Destinatario      
		, Direccion         
		, CiudadDestino     
		, Departamento      
		, Cajas
		, Regional          
		, Estado            
		, FechaEntrega      
		, TipoNovedad       
		, FechaNovedad      
	)
	SELECT 
		  123665
		, convert(datetime, w.Fec_Produccion, 105) 
		, w.Servicio_Utilizado
		, w.Numero_Documento
		, w.Guia
		, w.Nombre_Destinatario
		, w.Direccion_Destinatario
		, w.Ciudad_Destino
		, w.Nom_Departamento
		, w.Num_Unidades 
		, w.Regional_Destino
		, w.Estado_Actual
		, convert(datetime, w.Fec_Entrega, 105)
		, w.Des_Novedad
		, convert(datetime, w.Fec_Novedad, 105)
	 FROM TranspEnvia w Left Join TranspEntrega r on r.idTransportadora = 123665 And r.NumeroGuia = w.Guia
	 WHERE r.NumeroGuia Is Null ;

INSERT INTO [LogSP]([categoria], [mensaje] )
VALUES( 'SP_ETL_TranspServientrega', 'sale' );

	SELECT 'OK' AS 'MensajeProceso';
	END TRY
	BEGIN CATCH 
		SET @mensajeProceso = 'Se ha generado un error en la Estructura del BULK-INSERT: Linea#:' + CAST (ERROR_LINE() AS VARCHAR) + ' - Mensaje de Error:' + ERROR_MESSAGE();				
		SELECT @mensajeProceso AS 'MensajeProceso';
	END CATCH
END