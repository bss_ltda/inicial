﻿CREATE PROCEDURE [dbo].[SP_DataCreditoDatos]
AS
BEGIN
	SET NOCOUNT ON;
delete from [DataCreditoPlanilla]
INSERT INTO [InfoNatura].[dbo].[DataCreditoPlanilla]
           ([Codigo]
           ,[Tipo de documento]
           ,[Noidentificación]
           ,[Numero de obligacion]
           ,[Nombre completo]
           ,[Situacion del titular]
           ,[Fecha apertura]
           ,[Fecha vencimiento]
           ,[Responsable]
           ,[Tipo de Obligacion]
           ,[Termino del Contrato]
           ,[Forma de pago]
           ,[Peridicidad]
           ,[Novedad]
           ,[Estado Origen de la cuenta]
           ,[Fecha estado origen]
           ,[Estadode la cuenta]
           ,[Fecha estado de la cuenta]
           ,[Adjetivo]
           ,[FechaAdjetivo]
           ,[Tipo de moneda]
           ,[Tipo de garantía]
           ,[Calificacion]
           ,[Edad de mora]
           ,[Valor inicial]
           ,[Valor saldo deuda]
           ,[Valor disponible]
           ,[Valor Cuota mes]
           ,[Valor saldo en  mora]
           ,[Total Cuotas]
           ,[Cuotas Canceladas]
           ,[cuotas en mora]
           ,[Clausula de permanencia]
           ,[Fecha clausula de permanencia]
           ,[Fecha limite de pago]
           ,[Fecha de pago]
           ,[Oficina de radicacion]
           ,[Ciudad de radicación]
           ,[Codigo DANE ciudad de radicacion]
           ,[Ciudad residencia]
           ,[Codigo DANE ciudad de residencia]
           ,[Deparatamento de residencia]
           ,[Direccion de residencia]
           ,[Telefono de residencia]
           ,[Ciudad laboral]
           ,[Codigo DANE ciudad laboral]
           ,[Deparatamento laboral]
           ,[Direccion Laboral]
           ,[Telefono Laboral]
           ,[Ciudad de correspondencia]
           ,[Codigo DANE ciudad correspondencia]
           ,[Deparatamento Correspondencia]
           ,[Direccion Correspondencia]
           ,[Correo electronico]
           ,[Numero Celular])
     Select CódigoPersona,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','' from DataCreditoTítulos 
 
UPDATE DataCreditoPlanilla SET [Tipo de documento]='1', [Situacion del titular]='0', Responsable='00',[Tipo de Obligacion]='2',[Termino del Contrato]='2',[Forma de pago]='0',Peridicidad='9',[Estado Origen de la cuenta]='0',Adjetivo='0',FechaAdjetivo='00000000', [Tipo de moneda]='1', [Tipo de garantía]='2', Calificacion='  ',[Valor disponible]='0000000000',[Total Cuotas]='000',[Cuotas Canceladas]='0000',[cuotas en mora]='000',[Clausula de permanencia]='000',[Fecha clausula de permanencia]='00000000', [Fecha estado de la cuenta]=  CONVERT(CHAR(10),GetDate (),102)

--DataCreditoPersonas
UPDATE  DataCreditoPersonas SET CodigoRevendedor=REPLACE(CodigoRevendedor,'.','');
UPDATE DataCreditoPlanilla SET Noidentificación = [Cédula de Ciudadanía/NIT] FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CodigoRevendedor = P.Codigo
UPDATE  DataCreditoPlanilla SET Noidentificación=REPLACE(Noidentificación,'.','');
UPDATE DataCreditoPlanilla SET [Correo electronico] = EMail  FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE DataCreditoPlanilla SET [Numero Celular]  = TelMovil   FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE DataCreditoPlanilla SET [Telefono Laboral]  = TelResidencial    FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE DataCreditoPlanilla SET [Telefono de residencia]  = TelResidencial    FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE DataCreditoPlanilla SET [Telefono de residencia]  = TelResidencial    FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE DataCreditoPlanilla SET [Ciudad de radicación]  =CiudadResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE DataCreditoPlanilla SET [Ciudad de correspondencia]  = CiudadResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE DataCreditoPlanilla SET [Ciudad residencia]  = CiudadResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE DataCreditoPlanilla SET [Ciudad laboral]  = CiudadResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE DataCreditoPlanilla SET [Deparatamento Correspondencia]  = RegionResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE DataCreditoPlanilla SET [Deparatamento de residencia]  = RegionResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE DataCreditoPlanilla SET [Deparatamento laboral]  = RegionResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE DataCreditoPlanilla SET [Direccion Correspondencia]  = CalleResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE DataCreditoPlanilla SET [Direccion Laboral]  = CalleResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación
UPDATE DataCreditoPlanilla SET [Direccion de residencia]  = CalleResidencial FROM dbo.DataCreditoPersonas AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.[Cédula de Ciudadanía/NIT] = P.Noidentificación

--DataCreditoTítulos

UPDATE DataCreditoPlanilla SET [Nombre completo] =Nombre  FROM dbo.DataCreditoTítulos AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CódigoPersona = P.Codigo 
UPDATE DataCreditoPlanilla SET [Numero de obligacion]= NúmeroPedido  FROM dbo.DataCreditoTítulos AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CódigoPersona = P.Codigo 
UPDATE DataCreditoPlanilla SET [Fecha apertura] =CONVERT(CHAR(10), FechaPedido , 102)  FROM dbo.DataCreditoTítulos AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CódigoPersona = P.Codigo 
UPDATE DataCreditoPlanilla SET [Fecha vencimiento] =FechaVencimiento  FROM dbo.DataCreditoTítulos AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CódigoPersona = P.Codigo 
UPDATE DataCreditoPlanilla SET [Fecha estado origen] =CONVERT(CHAR(10),FechaPedido, 102)  FROM dbo.DataCreditoTítulos AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CódigoPersona = P.Codigo 
UPDATE DataCreditoPlanilla SET [Valor inicial] =ValorTítulo  FROM dbo.DataCreditoTítulos AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CódigoPersona = P.Codigo 
UPDATE DataCreditoPlanilla SET [Valor saldo deuda] =ValorTítulo  FROM dbo.DataCreditoTítulos AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CódigoPersona = P.Codigo 
UPDATE DataCreditoPlanilla SET [Valor Cuota mes] =[Saldo actualizado]  FROM dbo.DataCreditoTítulos AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CódigoPersona = P.Codigo 
UPDATE DataCreditoPlanilla SET [Fecha limite de pago] =CONVERT(CHAR(10),FechaVencimiento,102)  FROM dbo.DataCreditoTítulos AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CódigoPersona = P.Codigo 
UPDATE DataCreditoPlanilla SET [Oficina de radicacion] =Estructura  FROM dbo.DataCreditoTítulos AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CódigoPersona = P.Codigo 


--DataCreditoBTS
UPDATE DataCreditoPlanilla SET Novedad =0
UPDATE DataCreditoPlanilla SET Novedad =[Dia Mora]  FROM dbo.DataCreditoBTS  AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.Cedula = P.Noidentificación and R.CodigoPedido = P.[Numero de obligacion]
UPDATE DataCreditoPlanilla SET [Edad de mora] =Novedad 
UPDATE DataCreditoPlanilla SET Novedad ='01' WHERE Novedad >=0 AND  Novedad <=59
UPDATE DataCreditoPlanilla SET Novedad ='07' WHERE Novedad >=60 AND  Novedad <=89
UPDATE DataCreditoPlanilla SET Novedad ='08' WHERE Novedad >=90 AND  Novedad <=119
UPDATE DataCreditoPlanilla SET Novedad ='09' WHERE Novedad >=120

UPDATE DataCreditoPlanilla SET [Valor saldo en  mora] =[Valor actualizado total]  FROM dbo.DataCreditoBTS  AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.Cedula = P.Noidentificación AND P.[Numero de obligacion]=R.CodigoPedido
UPDATE DataCreditoPlanilla SET [Fecha de pago] =[Fecha Pago]  FROM dbo.DataCreditoBTS  AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.Cedula = P.Noidentificación AND P.[Numero de obligacion]=R.CodigoPedido

--DataCreditoCiudades 

UPDATE DataCreditoPlanilla SET [Codigo DANE ciudad de radicacion] =CIUDAD_DANE fROM dbo.DataCreditoCiudades  AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CIUDAD = P.[Ciudad de radicación]
UPDATE DataCreditoPlanilla SET [Codigo DANE ciudad de residencia] =CIUDAD_DANE fROM dbo.DataCreditoCiudades  AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CIUDAD = P.[Ciudad residencia]
UPDATE DataCreditoPlanilla SET [Codigo DANE ciudad laboral] =CIUDAD_DANE fROM dbo.DataCreditoCiudades  AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CIUDAD = P.[Ciudad laboral]
UPDATE DataCreditoPlanilla SET [Codigo DANE ciudad correspondencia] =CIUDAD_DANE fROM dbo.DataCreditoCiudades  AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CIUDAD = P.[Ciudad de correspondencia]
UPDATE DataCreditoPlanilla SET [Fecha de pago] =FechaVencimiento  FROM dbo.DataCreditoTítulos AS R INNER JOIN dbo.DataCreditoPlanilla AS P ON R.CódigoPersona = P.Codigo and P.[Fecha de pago]=''

UPDATE  DataCreditoPlanilla SET [Fecha apertura]=REPLACE([Fecha apertura],'.','');
UPDATE  DataCreditoPlanilla SET [Fecha estado origen]=REPLACE([Fecha estado origen],'.','');
UPDATE  DataCreditoPlanilla SET [Fecha estado de la cuenta]=REPLACE([Fecha estado de la cuenta],'.','');
UPDATE  DataCreditoPlanilla SET [Fecha limite de pago]=REPLACE([Fecha limite de pago],'.','');
UPDATE DataCreditoPlanilla SET [Valor saldo en  mora]  =0  where [Valor saldo en  mora] =''

END