﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_LibPedTolerancia]
AS
BEGIN
	
	IF EXISTS(SELECT PerfilDeCreditoActual FROM geraLibPedComportPago WHERE PerfilDeCreditoActual = 'Clase de TL (A)')
	    
		 BEGIN 
		     
			 UPDATE geraLibPedComportPago SET Tolerancia = 400 WHERE PerfilDeCreditoActual = 'Clase de TL (A)'

		 END 

	IF EXISTS(SELECT PerfilDeCreditoActual FROM geraLibPedComportPago WHERE PerfilDeCreditoActual = 'Clase de TL (B)')
	    
		 BEGIN 
		     
			 UPDATE geraLibPedComportPago SET Tolerancia = 300 WHERE PerfilDeCreditoActual = 'Clase de TL (B)'

		 END

	IF EXISTS(SELECT PerfilDeCreditoActual FROM geraLibPedComportPago WHERE PerfilDeCreditoActual = 'Clase de TL (C)')
	    
		 BEGIN 
		     
			 UPDATE geraLibPedComportPago SET Tolerancia = 200 WHERE PerfilDeCreditoActual = 'Clase de TL (C)'

		 END

	IF EXISTS(SELECT PerfilDeCreditoActual FROM geraLibPedComportPago WHERE PerfilDeCreditoActual = 'Clase de TL (D)')
	    
		 BEGIN 
		     
			 UPDATE geraLibPedComportPago SET Tolerancia = 100 WHERE PerfilDeCreditoActual = 'Clase de TL (D)'

		 END

	IF EXISTS(SELECT PerfilDeCreditoActual FROM geraLibPedComportPago WHERE PerfilDeCreditoActual = 'Clase de TL (E)')
	    
		 BEGIN 
		     
			 UPDATE geraLibPedComportPago SET Tolerancia = 50 WHERE PerfilDeCreditoActual = 'Clase de TL (E)'

		 END


	UPDATE geraLibPedComportPago SET CupoTolerancia = CreditoActualDisponible * (1 + Tolerancia)

END