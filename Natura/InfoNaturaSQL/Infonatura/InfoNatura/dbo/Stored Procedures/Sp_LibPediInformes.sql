﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_LibPediInformes]
	 
	-- EXEC [dbo].[Sp_LibPediInformes]
AS
BEGIN
        
		TRUNCATE TABLE LibPedEventos
       
	    EXEC Sp_LibPedInfoGerencias	
		EXEC Sp_ETL_LibPedComportPago
		EXEC Sp_LibPedRetener
	
	     
END