﻿-- =============================================
-- Author:		FM
-- Create date: 2018-02-22
-- Description:	ETL de revendedoras de archivos csv 
-- =============================================
CREATE PROCEDURE [dbo].[SP_ETL_Revendedoras]
	@rutaArchivo varchar(255),
    @idLogAuditoria int		
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;
DECLARE @MensajeProceso AS VARCHAR(MAX);	
BEGIN TRY
DECLARE @ETL VARCHAR(100) = 'SP_ETL_Revendedoras';
SET @rutaArchivo = REPLACE( @rutaArchivo, 'D:\Sitios\', '\\10.21.3.15\' )

--SET @rutaArchivo = 'D:\temp\revendedoras.csv'

DECLARE @ID_PARAM AS bigint;
INSERT INTO RFLOG( OPERACION, EVENTO ) VALUES('BULKINSERT', @rutaArchivo)		 
SET @ID_PARAM =  ( SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY] ); 	


TRUNCATE TABLE geraRevendedorasW;
DECLARE @SQL_BULK VARCHAR(MAX);
DECLARE @TABLA_TMP VARCHAR(100);
SET @TABLA_TMP = 'geraRevW' + REPLACE( CONVERT(VARCHAR(12),GETDATE(),114), ':', '' );

SET @SQL_BULK = 'SELECT * INTO ' + @TABLA_TMP  + ' FROM geraRevendedorasW';
EXEC (@SQL_BULK);

--DECLARE @path varchar(255) = 'D:\Natura\gera\revendedoras\Consulta_Q554_P318531_20180215072054050.csv'


SET @SQL_BULK = 'BULK INSERT ' + @TABLA_TMP + ' FROM ''' + @rutaArchivo + ''' WITH
        (
        FIRSTROW = 2,
        FIELDTERMINATOR = '';'',
        ROWTERMINATOR = ''\n'',
		CODEPAGE = ''ACP''
        )'
--print @SQL_BULK
--print 'Importando ' + @rutaArchivo;
EXEC (@SQL_BULK);

SET @SQL_BULK = '
UPDATE ' + @TABLA_TMP + ' SET 
  Codigo_Revendedor = Replace( Replace( Codigo_Revendedor, ''.'' , '''' ),  '','', '''')';
EXEC (@SQL_BULK);

SET @SQL_BULK = '
UPDATE [dbo].[geraRevendedoras] SET 
            [Nombre]                               =  w.[Nombre]
           ,[Cod_Grupo]                            =  w.[Cod_Grupo]
           ,[Situacion_Registro]                   =  w.[Situacion_Registro]
           ,[Situacion_Entrenamiento]              =  w.[Situacion_Entrenamiento]
           ,[Permite_LaDivulgacionDeSusDatos]      =  w.[Permite_LaDivulgacionDeSusDatos]
           ,[Cedula_Ciudadania]                    =  w.[Cedula_Ciudadania]
           ,[Cumpleanos]                           =  w.[Cumpleanos]
           ,[Ciclo_Actual]                         =  w.[Ciclo_Actual]
           ,[Ciclo_Primer_Pedido]                  =  w.[Ciclo_Primer_Pedido]
           ,[Ciclo_Reactivacion]                   =  w.[Ciclo_Reactivacion]
           ,[Ciclo_Cessamiento]                    =  w.[Ciclo_Cessamiento]
           ,[Situacion]                            =  w.[Situacion]
           ,[Fecha_Registro_Intencion_Reventa]     =  case when w.[Fecha_Registro_Intencion_Reventa] is null then null else  convert(date, left(w.[Fecha_Registro_Intencion_Reventa], 10 ), 103 ) end
           ,[Ciclos_Inactividad]                   =  w.[Ciclos_Inactividad]
           ,[Nivel]                                =  w.[Nivel]           
           ,[Fecha_Primer_Pedido]                  =  case when w.[Fecha_Primer_Pedido] is null then null else  convert(date, left(w.[Fecha_Primer_Pedido], 10 ), 103 ) end
           ,[TiempodeCasa]                         =  w.[TiempodeCasa]
           ,[Gerencia_Comercial]                   =  w.[Gerencia_Comercial]
           ,[Cod_Gerencia_Comercial]               =  w.[Cod_Gerencia_Comercial]
           ,[Estructura_Comercial]                 =  w.[Estructura_Comercial]
           ,[Cod_Estructura_Comercial]             =  w.[Cod_Estructura_Comercial]
           ,[Equipo]                               =  w.[Equipo]
           ,[Origen_Registro]                      =  w.[Origen_Registro]
           ,[Bloqueado]                            =  w.[Bloqueado]
           ,[Motivo_Bloqueo]                       =  w.[Motivo_Bloqueo]
           ,[Tel_Residencial]                      =  w.[Tel_Residencial]
           ,[Tel_Comercial]                        =  w.[Tel_Comercial]
           ,[Tel_Movil]                            =  w.[Tel_Movil]
           ,[Acepta_SMS]                           =  w.[Acepta_SMS]
           ,[EMail]                                =  w.[EMail]
           ,[Departamento_Residencial]             =  w.[Departamento_Residencial]
           ,[Municipio_Residencial]                =  w.[Municipio_Residencial]
           ,[Barrio_Residencial]                   =  w.[Barrio_Residencial]
           ,[Via_Principal_Residencial]            =  w.[Via_Principal_Residencial]
           ,[Numero_Residencial]                   =  w.[Numero_Residencial]
           ,[Complemento_Residencial]              =  w.[Complemento_Residencial]
           ,[Departamento_Comercial]               =  w.[Departamento_Comercial]
           ,[Municipio_Comercial]                  =  w.[Municipio_Comercial]
           ,[Barrio_Comercial]                     =  w.[Barrio_Comercial]
           ,[Via_Principal_Comercial]              =  w.[Via_Principal_Comercial]
           ,[Numero_Comercial]                     =  w.[Numero_Comercial]
           ,[Complemento_Comercial]                =  w.[Complemento_Comercial]
           ,[Departamento_Entrega]                 =  w.[Departamento_Entrega]
           ,[Municipio_Entrega]                    =  w.[Municipio_Entrega]
           ,[Barrio_Entrega]                       =  w.[Barrio_Entrega]
           ,[Via_Principal_Entrega]                =  w.[Via_Principal_Entrega]
           ,[Numero_Entrega]                       =  w.[Numero_Entrega]
           ,[Complemento_Entrega]                  =  w.[Complemento_Entrega]
           ,[Departamento_Alternativo]             =  w.[Departamento_Alternativo]
           ,[Municipio_Alternativo]                =  w.[Municipio_Alternativo]
           ,[Barrio_Alternativo]                   =  w.[Barrio_Alternativo]
           ,[Via_Principal_Alternativo]            =  w.[Via_Principal_Alternativo]
           ,[Numero_Alternativo]                   =  w.[Numero_Alternativo]
           ,[Complemento_Alternativo]              =  w.[Complemento_Alternativo]
           ,[Qtd_Conf_Obligatoria]                 =  w.[Qtd_Conf_Obligatoria]
           ,[Qtd_Conf_Obligatoria_Atribuida]       =  w.[Qtd_Conf_Obligatoria_Atribuida]
           ,[Tipo_Conf_Obligatoria]                =  w.[Tipo_Conf_Obligatoria]
           ,[Usuario_Conf_Obligatoria]             =  w.[Usuario_Conf_Obligatoria]
           ,[Fecha_Hora_Conf_Oblig]                =  case when w.[Fecha_Hora_Conf_Oblig] is null then null else  convert(date, left(w.[Fecha_Hora_Conf_Oblig], 10 ), 103 ) end
           ,[Registro_con_datos_expirados]         =  w.[Registro_con_datos_expirados]            
           ,[Actualizado]                          =  GETDATE()
		   FROM geraRevendedoras r Inner Join ' + @TABLA_TMP + ' w on r.[Codigo_Revendedor] = w.[Codigo_Revendedor] ';

EXEC (@SQL_BULK);

SET @SQL_BULK = '
INSERT INTO [dbo].[geraRevendedoras]
           ([Codigo_Revendedor]
           ,[Nombre]
           ,[Cod_Grupo]
           ,[Situacion_Registro]
           ,[Situacion_Entrenamiento]
           ,[Permite_LaDivulgacionDeSusDatos]
           ,[Cedula_Ciudadania]
           ,[Cumpleanos]
           ,[Ciclo_Actual]
           ,[Ciclo_Primer_Pedido]
           ,[Ciclo_Reactivacion]
           ,[Ciclo_Cessamiento]
           ,[Situacion]
           ,[Fecha_Registro_Intencion_Reventa]
           ,[Ciclos_Inactividad]
           ,[Nivel]
           ,[Fecha_Primer_Pedido]
           ,[TiempodeCasa]
           ,[Gerencia_Comercial]
           ,[Cod_Gerencia_Comercial]
           ,[Estructura_Comercial]
           ,[Cod_Estructura_Comercial]
           ,[Equipo]
           ,[Origen_Registro]
           ,[Bloqueado]
           ,[Motivo_Bloqueo]
           ,[Tel_Residencial]
           ,[Tel_Comercial]
           ,[Tel_Movil]
           ,[Acepta_SMS]
           ,[EMail]
           ,[Departamento_Residencial]
           ,[Municipio_Residencial]
           ,[Barrio_Residencial]
           ,[Via_Principal_Residencial]
           ,[Numero_Residencial]
           ,[Complemento_Residencial]
           ,[Departamento_Comercial]
           ,[Municipio_Comercial]
           ,[Barrio_Comercial]
           ,[Via_Principal_Comercial]
           ,[Numero_Comercial]
           ,[Complemento_Comercial]
           ,[Departamento_Entrega]
           ,[Municipio_Entrega]
           ,[Barrio_Entrega]
           ,[Via_Principal_Entrega]
           ,[Numero_Entrega]
           ,[Complemento_Entrega]
           ,[Departamento_Alternativo]
           ,[Municipio_Alternativo]
           ,[Barrio_Alternativo]
           ,[Via_Principal_Alternativo]
           ,[Numero_Alternativo]
           ,[Complemento_Alternativo]
           ,[Qtd_Conf_Obligatoria]
           ,[Qtd_Conf_Obligatoria_Atribuida]
           ,[Tipo_Conf_Obligatoria]
           ,[Usuario_Conf_Obligatoria]
           ,[Fecha_Hora_Conf_Oblig]
           ,[Registro_con_datos_expirados])
     SELECT
            w.[Codigo_Revendedor]
           ,w.[Nombre]
           ,w.[Cod_Grupo]
           ,w.[Situacion_Registro]
           ,w.[Situacion_Entrenamiento]
           ,w.[Permite_LaDivulgacionDeSusDatos]
           ,w.[Cedula_Ciudadania]
           ,w.[Cumpleanos]
           ,w.[Ciclo_Actual]
           ,w.[Ciclo_Primer_Pedido]
           ,w.[Ciclo_Reactivacion]
           ,w.[Ciclo_Cessamiento]
           ,w.[Situacion]
			, case when w.[Fecha_Registro_Intencion_Reventa] is null then null else  convert(date, left(w.[Fecha_Registro_Intencion_Reventa], 10 ), 103 ) end
           ,w.[Ciclos_Inactividad]
           ,w.[Nivel]           
		   , case when w.[Fecha_Primer_Pedido] is null then null else  convert(date, left(w.[Fecha_Primer_Pedido], 10 ), 103 ) end
           ,w.[TiempodeCasa]
           ,w.[Gerencia_Comercial]
           ,w.[Cod_Gerencia_Comercial]
           ,w.[Estructura_Comercial]
           ,w.[Cod_Estructura_Comercial]
           ,w.[Equipo]
           ,w.[Origen_Registro]
           ,w.[Bloqueado]
           ,w.[Motivo_Bloqueo]
           ,w.[Tel_Residencial]
           ,w.[Tel_Comercial]
           ,w.[Tel_Movil]
           ,w.[Acepta_SMS]
           ,w.[EMail]
           ,w.[Departamento_Residencial]
           ,w.[Municipio_Residencial]
           ,w.[Barrio_Residencial]
           ,w.[Via_Principal_Residencial]
           ,w.[Numero_Residencial]
           ,w.[Complemento_Residencial]
           ,w.[Departamento_Comercial]
           ,w.[Municipio_Comercial]
           ,w.[Barrio_Comercial]
           ,w.[Via_Principal_Comercial]
           ,w.[Numero_Comercial]
           ,w.[Complemento_Comercial]
           ,w.[Departamento_Entrega]
           ,w.[Municipio_Entrega]
           ,w.[Barrio_Entrega]
           ,w.[Via_Principal_Entrega]
           ,w.[Numero_Entrega]
           ,w.[Complemento_Entrega]
           ,w.[Departamento_Alternativo]
           ,w.[Municipio_Alternativo]
           ,w.[Barrio_Alternativo]
           ,w.[Via_Principal_Alternativo]
           ,w.[Numero_Alternativo]
           ,w.[Complemento_Alternativo]
           ,w.[Qtd_Conf_Obligatoria]
           ,w.[Qtd_Conf_Obligatoria_Atribuida]
           ,w.[Tipo_Conf_Obligatoria]
           ,w.[Usuario_Conf_Obligatoria]
			, case when w.[Fecha_Hora_Conf_Oblig] is null then null else  convert(date, left(w.[Fecha_Hora_Conf_Oblig], 10 ), 103 ) end
           ,w.[Registro_con_datos_expirados]
            
		   FROM ' + @TABLA_TMP + ' w Left Join geraRevendedoras r on w.[Codigo_Revendedor] = r.[Codigo_Revendedor]
		   WHERE r.[Codigo_Revendedor] Is Null ';

EXEC (@SQL_BULK) 

SET @SQL_BULK = '
UPDATE [ShowroomDes].[dbo].[Consultora] SET 
            [Nombre]                               =  w.[Nombre]
           ,[CodGrupo]                             =  w.[Cod_Grupo]
           ,[Cedula]                               =  w.[Cedula_Ciudadania]
           ,[CicloActual]                          =  w.[Ciclo_Actual]
           ,[CicloPrimerPedido]                    =  w.[Ciclo_Primer_Pedido]
           ,[Situacion]                            =  w.[Situacion]
           ,[CodigoEstructuraComercial]            =  w.[Cod_Estructura_Comercial]
           ,[EstructuraComercial]                  =  w.[Estructura_Comercial]
           ,[EMail]                                =  w.[EMail]           
           ,[Actualizado]						   =  GETDATE()
		   FROM [ShowroomDes].[dbo].[Consultora] r Inner Join ' + @TABLA_TMP + ' w on r.[CodigoRevendedor] = w.[Codigo_Revendedor] ';

EXEC (@SQL_BULK) ;

SET @SQL_BULK = '
INSERT INTO [ShowroomDes].[dbo].[Consultora](
		    [CodigoRevendedor] 
		   ,[Nombre]                               
           ,[CodGrupo]                             
           ,[Cedula]                               
           ,[CicloActual]                          
           ,[CicloPrimerPedido]                    
           ,[Situacion]                            
           ,[CodigoEstructuraComercial]            
           ,[EstructuraComercial]                  
           ,[EMail] )
SELECT
			 w.[Codigo_Revendedor]
           , w.[Nombre]		   
           , w.[Cod_Grupo]
           , w.[Cedula_Ciudadania]
           , w.[Ciclo_Actual]
           , w.[Ciclo_Primer_Pedido]
           , w.[Situacion]
           , w.[Cod_Estructura_Comercial]
           , w.[Estructura_Comercial]
           , w.[EMail]                       
		   FROM ' + @TABLA_TMP + ' w Left Join [ShowroomDes].[dbo].[Consultora] r on w.[Codigo_Revendedor] = r.[CodigoRevendedor]
		   WHERE r.[CodigoRevendedor] Is Null ';
    EXEC (@SQL_BULK) ;

	SET @SQL_BULK = 'UPDATE RFLOG SET ' +
					'  MENSAJE = ( SELECT [Gerencia_Comercial] + '' '' + RTRIM(CAST( Count(*) AS VARCHAR )) FROM ' + @TABLA_TMP + '	GROUP BY [Gerencia_Comercial])' +
					', LKEY = ( SELECT MAX(Cod_Gerencia_Comercial) FROM ' + @TABLA_TMP + ' ) ' +
					', IDPROCESO = ( SELECT COUNT(*) FROM ' + @TABLA_TMP + ' ) ' +
					' WHERE ID = ' + CAST( @ID_PARAM AS VARCHAR);					
	EXEC (@SQL_BULK) ;

	DECLARE @Gerencia INT;
	SELECT @Gerencia = CAST( LKEY AS INT ) FROM RFLOG WHERE ID = @ID_PARAM;
	
	UPDATE geraEstructuraComercial SET 
	  ConsultorasActualizado = GETDATE()
	, ConsultorasTotal = (SELECT IDPROCESO FROM RFLOG WHERE ID = @ID_PARAM)
	WHERE Cargo = 'GV' AND codGV = @Gerencia
	
	UPDATE geraEstructuraComercial SET 
	GRs = TotCNs
    FROM geraEstructuraComercial  e Inner Join (
		SELECT Cod_Estructura_Comercial, COUNT(*) TotCNs FROM geraRevendedoras
		WHERE Cod_Gerencia_Comercial = @Gerencia
		GROUP BY Cod_Estructura_Comercial ) c
		on c.Cod_Estructura_Comercial = e.codSector
	WHERE Cargo = 'GR' AND e.codGV = @Gerencia;

	SET @SQL_BULK = 'DROP TABLE ' + @TABLA_TMP;
	EXEC (@SQL_BULK);
	
	EXEC [dbo].[SP_ETL_LOG] @ETL, @rutaArchivo, @idLogAuditoria, 1, 'OK'	
		
	SELECT @MensajeProceso = MENSAJE FROM RFLOG WHERE ID = @ID_PARAM;
	SELECT @MensajeProceso AS 'MensajeProceso';
	
END TRY
BEGIN CATCH
	SET @MensajeProceso = 'Se a generado un error: Linea: ' + CAST(ERROR_LINE() AS VARCHAR) + ' - Mensaje de error: ' + ERROR_MESSAGE();
	EXEC [dbo].[SP_ETL_LOG] @ETL, @rutaArchivo, @idLogAuditoria, -1, @mensajeProceso	
	UPDATE RFLOG SET MENSAJE = @MensajeProceso WHERE ID = @ID_PARAM;	
	SELECT @MensajeProceso AS 'MensajeProceso'
END CATCH	

END