﻿CREATE PROCEDURE [dbo].[SP_SubirCorreoDatos] (@IdCorreo BigInt)

AS
BEGIN

	SET NOCOUNT ON;

DECLARE @tempDATA VARCHAR(255)
DECLARE @correonovalido VARCHAR(255) = ''
DECLARE @MenNulo VARCHAR(255) = ''
DECLARE @Name nvarchar(50)
DECLARE @Ruta nvarchar(2000)
DECLARE @Paso nvarchar(50)
DECLARE @Adjunto nvarchar(50)
DECLARE @IDD FLOAT
SELECT @Name = Codigo FROM TABLAMAESTRA WHERE TABLA = 'DIRECCION';
	
--SELECT @Ruta = FADESCRIP, @IdCorreo = FAPROCNUM FROM RFADJUNTO WHERE FANUMREG = @Adj;
Declare @comando nvarchar(400)

Select
  @Ruta = A.FADESCRIP,
  @Adjunto = FAPROCNUM
From
  CorreoConfig C Inner Join
  RFADJUNTO A On C.Categ01 = A.FACATEG01 And C.Categ02 = A.FACATEG02 And
    C.Id = A.FAPROCNUM
    WHERE
    Id = @IdCorreo

BEGIN TRY

truncate table CorreoDatosWrk;
--UPDATE [InfoNatura].[dbo].[CorreoConfig] SET [Mensaje] = NULL WHERE Id=@Id

DELETE FROM CorreoDatosWrk
set @Ruta = REPLACE(@Ruta,'C:',@Name)

set @comando = 'BULK INSERT CorreoDatosWrk FROM ''\\' + @Ruta + ''' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
--set @Paso = 'Correo Datos Wrk'
UPDATE [InfoNatura].[dbo].[CorreoConfig] SET [FechaEjecucion] = GetDate() WHERE Id=@IdCorreo
EXEC sp_executesql @comando

--set @correonovalido = ''
--set @correonovalido = select dbo.fn_BuscaEmail("cualquieremail@cualquierdominio.com") 

--select codigo, dbo.fn_BuscaEmail(Correo) as correovalido from CorreoDatosWrk

DECLARE ValidaCorreoCur CURSOR FOR 
SELECT Replace(Codigo,' ','') 
FROM CorreoDatosWrk
WHERE dbo.fn_BuscaEmail(Correo) IS NULL;
OPEN ValidaCorreoCur
FETCH NEXT FROM ValidaCorreoCur INTO @tempDATA
WHILE ( @@FETCH_STATUS = 0 )
	BEGIN		
		IF @correonovalido = ''
		SET @correonovalido = '<strong>CÓDIGO CORREOS NO VALIDOS</strong><br>' + @tempDATA
		ELSE
		SET @correonovalido += ' ' + @tempDATA
		FETCH NEXT FROM ValidaCorreoCur INTO @tempDATA
	END
SELECT @MenNulo = [Mensaje] FROM CorreoConfig WHERE id = @IdCorreo AND [Mensaje] Is NOT NULL;
IF @MenNulo = ''
	UPDATE CorreoConfig SET [Mensaje] = @correonovalido + '<br>' WHERE id = @IdCorreo
ELSE
	UPDATE CorreoConfig SET [Mensaje] = @MenNulo + @correonovalido + '<br>' WHERE id = @IdCorreo
CLOSE ValidaCorreoCur 
DEALLOCATE ValidaCorreoCur

IF @correonovalido = ''
	BEGIN
		DELETE FROM CorreoDatos WHERE IdCorreo= @IdCorreo;

		INSERT INTO CorreoDatos 
		(IdCorreo, Codigo,Nombre,Correo,ColumnaA,ColumnaB,ColumnaC,ColumnaD,ColumnaE,ColumnaF,ColumnaG,ColumnaH,ColumnaI,ColumnaJ,ColumnaK,ColumnaL,ColumnaM,ColumnaN,ColumnaO,ColumnaP,ColumnaQ,ColumnaR,ColumnaS,ColumnaT) 
		(SELECT @IdCorreo, * FROM CorreoDatosWrk);
		UPDATE [InfoNatura].[dbo].[CorreoConfig] SET [FechaEjecucion] = GetDate(),[Mensaje] = 'OK', [IdAdjunto] = @Adjunto WHERE Id=@IdCorreo;		
	END	

END TRY
BEGIN CATCH
	DECLARE @ErrorNumber INT = ERROR_NUMBER();
	DECLARE @ErrorLine INT = ERROR_LINE();
	DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
	DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
	DECLARE @ErrorState INT = ERROR_STATE();
	DECLARE @ErrorProcedure INT = ERROR_PROCEDURE();

	PRINT 'Actual error number: ' + CAST(@ErrorNumber AS VARCHAR(10)) + '.';
	PRINT 'Actual line number: ' + CAST(@ErrorLine AS VARCHAR(10)) + '.';
	PRINT 'Paso: ' + @Paso + '.';
	PRINT 'Mensaje Error: ' + @ErrorMessage + '.';
	PRINT  @ErrorSeverity ;
	PRINT  @ErrorState ;

	RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
	
	UPDATE [InfoNatura].[dbo].[CorreoConfig] SET [FechaEjecucion] = GetDate(),[Mensaje] = 'Error ' + @Paso + ' - ' + @ErrorMessage + CAST(@ErrorLine AS VARCHAR(10)) WHERE Id=@IdCorreo
END CATCH;
END