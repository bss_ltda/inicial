﻿CREATE PROCEDURE [dbo].[SP_CifinPositivas] (@ID AS float) AS

BEGIN
DELETE FROM  CifinVencimiento WHERE Situación = 'Anulado'
DELETE FROM  [CifinPlanilla] WHERE REPORTE = 'POSITIVO'
INSERT INTO [InfoNatura].[dbo].[CifinPlanilla]
           ( ID
           ,REPORTE
           ,[TIPO IDENT]
           ,[Nº IDENTIFICACION]
           ,[CODIGO]
           ,[NOMBRE TERCERO]
           ,[RESERVADO]
           ,[FECHA LIMITE DE PAGO]
           ,[NUMERO OBLIGACION]
           ,[CÓDIGO SUCURSAL]
           ,[CALIDAD]
           ,[CALIFICACIÓN]
           ,[ESTADO DEL TITULAR]
           ,[ESTADO DE OBLIGACION]
           ,[EDAD DE MORA]
           ,[AÑOS EN MORA]
           ,[FECHA DE CORTE]
           ,[FECHA INICIO  ]
           ,[FECHA TERMINACIÓN]
           ,[FECHA DE EXIGIBILIDAD]
           ,[FECHA DE PRESCRIPCION]
           ,[FECHA DE PAGO]
           ,[MODO EXTINCIÓN]
           ,[TIPO DE PAGO]
           ,[PERIODICIDAD]
           ,[PROBABILIDAD DE NO PAGO]
           ,[NÚMERO DE CUOTAS PAGADAS]
           ,[NÚMERO DE CUOTAS PACTADAS]
           ,[CUOTAS EN MORA]
           ,[VALOR INICIAL]
           ,[VALOR DE MORA]
           ,[VALOR DEL SALDO]
           ,[VALOR DE LA CUOTA]
           ,[VALOR DE CARGO FIJO]
           ,[LÍNEA DE CRÉDITO]
           ,[CLÁUSULA DE PERMANENCIA]
           ,[TIPO DE CONTRATO]
           ,[ESTADO DE CONTRATO]
           ,[TERMINO O VIGENCIA DEL CONTRATO]
           ,[NUMERO DE MESES DEL CONTRATO]
           ,[NATURALEZA JURÍDICA]
           ,[MODALIDAD DE CRÉDITO]
           ,[TIPO DE MONEDA]
           ,[TIPO DE GARANTÍA]
           ,[VALOR DE LA GARANTÍA]
           ,[OBLIGACION REESTRUCTURADA]
           ,[NATURALEZA DE LA REESTRUCTURACIÓN]
           ,[NÚMERO DE REESTRUCTURACIONES]
           ,[CLASE  TARJETA]
           ,[NO DE CHEQUES DEVUELTOS]
           ,[CATEGORÍA SERVICIOS ]
           ,[PLAZO ]
           ,[DÍAS DE CARTERA]
           ,[TIPO DE CUENTA]
           ,[CUPO SOBREGIRO]
           ,[DIAS AUTORIZADOS]
           ,[DIRECCION CASA DEL TERCERO]
           ,[TELEFONO CASA DEL TERCERO]
           ,[CODIGO CIUDAD CASA DEL TERCERO]
           ,[CIUDAD CASA DEL TERCERO]
           ,[CODIGO DEPARTAMENTO DEL TERCERO]
           ,[DEPARTAMENTO CASA DEL TERCERO]
           ,[NOMBRE EMPRESA]
           ,[DIRECCION DE LA EMPRESA]
           ,[TELEFONO DE LA EMPRESA]
           ,[CODIGO CIUDAD EMPRESA DEL TERCERO]
           ,[CIUDAD EMPRESA DEL TERCERO]
           ,[CODIGO DEPARTAMENTO EMPRESA DEL TERCERO]
           ,[DEPARTAMENTO EMPRESA DEL TERCERO]
           ,[FECHA_GENERACION])
       Select
			@ID
		   , 'POSITIVO'
           ,'01'
           ,''
           ,[CódigoPersona]
           ,[Nombre]
           ,''
           , CONVERT(CHAR(10),[FechaVencimiento],111)
           ,[NúmeroPedido]
           ,'000001'
           ,'P'
           ,''
           ,''
           ,'02'
           ,'0'
           ,''
           , CONVERT(CHAR(10),getdate(),111) 
           , CONVERT(CHAR(10),FechaPedido,111)
           ,''
           ,''
           ,''
           ,CONVERT(CHAR(10),FechaLiquidación,111)
           ,'01'
           ,'01'
           ,'04'
           ,''
           ,'001'
           ,'001'
           ,'000'
           ,[ValorTítulo]
           ,'0'
           ,'0'
           ,'0'
           ,''
           ,'26'
           ,''
           ,'001'
           ,'001'
           ,'02'
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,'02'
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,''
           ,CONVERT(CHAR(10),getdate(),111)
           from CifinVencimiento t1 
    where  not exists(select null from CifinCobro t2 where t2.CODIGO_PEDIDO = t1.NúmeroPedido)  
   
	UPDATE  CifinPlanilla SET 
	  [FECHA DE CORTE]        = REPLACE([FECHA DE CORTE],'/','')
	, [FECHA LIMITE DE PAGO]  = REPLACE([FECHA LIMITE DE PAGO],'/','')
	, [FECHA INICIO  ]        = REPLACE( [FECHA INICIO  ],'/','')
	, [FECHA TERMINACIÓN]     = REPLACE([FECHA TERMINACIÓN],'/','')
	, [FECHA DE EXIGIBILIDAD] = REPLACE([FECHA DE EXIGIBILIDAD],'/','')
	, [FECHA DE PRESCRIPCION] = REPLACE([FECHA DE PRESCRIPCION],'/','')
	, [FECHA DE PAGO]         = REPLACE( REPLACE([FECHA DE PAGO],'/',''),'-','') ;


   UPDATE  CifinPersonas SET CodigoRevendedor=REPLACE(CodigoRevendedor,'.',''); 
   
   UPDATE CifinPlanilla SET 
     [Nº IDENTIFICACION]              = [Cédula de Ciudadanía/NIT]                                 
   , [DIRECCION CASA DEL TERCERO]     = calleResidencial + '-' + ComplementoResidencial  
   , [CIUDAD CASA DEL TERCERO]        = CiudadResidencial                                  
   , [DEPARTAMENTO CASA DEL TERCERO]  = RegionResidencial                            
   , [TELEFONO CASA DEL TERCERO]      = TelResidencial                                   
   FROM dbo.CifinPersonas AS R INNER JOIN dbo.CifinPlanilla AS P ON R.CodigoRevendedor = P.Codigo;
      
   UPDATE CIFINPLANILLA SET [DEPARTAMENTO CASA DEL TERCERO] = 'BOGOTA DISTRITO CA'
   WHERE [DEPARTAMENTO CASA DEL TERCERO] = 'CUNDINAMARCA' AND [CIUDAD CASA DEL TERCERO]   = 'BOGOTA'
---	UPDATE CifinPlanilla SET [CODIGO CIUDAD CASA DEL TERCERO] =CIUDAD_DANE fROM dbo.DataCreditoCiudades  AS R INNER JOIN dbo.CifinPlanilla AS P ON R.CIUDAD = P.[CIUDAD CASA DEL TERCERO] AND R.DEPARTAMENTO = P.[DEPARTAMENTO CASA DEL TERCERO]
---	UPDATE CifinPlanilla SET [CODIGO DEPARTAMENTO DEL TERCERO] =DPTO_DANE fROM dbo.DataCreditoCiudades  AS R INNER JOIN dbo.CifinPlanilla AS P ON  R.CIUDAD = P.[CIUDAD CASA DEL TERCERO] AND R.DEPARTAMENTO = P.[DEPARTAMENTO CASA DEL TERCERO]
	UPDATE CifinPlanilla SET [CODIGO CIUDAD CASA DEL TERCERO] =cmun fROM dbo.CifinCiudades  AS R INNER JOIN dbo.CifinPlanilla AS P ON R.mun = P.[CIUDAD CASA DEL TERCERO] AND R.dep = P.[DEPARTAMENTO CASA DEL TERCERO]
	UPDATE CifinPlanilla SET [CODIGO DEPARTAMENTO DEL TERCERO] =cdep fROM dbo.CifinCiudades  AS R INNER JOIN dbo.CifinPlanilla AS P ON  R.dep = P.[DEPARTAMENTO CASA DEL TERCERO]


--	UPDATE CifinPlanilla SET [VALOR INICIAL] = (round(([VALOR INICIAL]/1000), 0))
--	UPDATE CifinPlanilla SET [VALOR DE MORA] = (round(([VALOR DE MORA]/1000), 0))
--	UPDATE CifinPlanilla SET [VALOR DEL SALDO] = (round(([VALOR DEL SALDO]/1000), 0))
--	UPDATE CifinPlanilla set [VALOR DE LA CUOTA] = (round(([VALOR DEL SALDO]/1000), 0))
END