﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- EXEC [dbo].[Sp_ETL_RankingVentas] 'D:\Natura\SR18-003B Seguimiento de Campañas\ConsultaRankingVendas.csv',12
-- EXEC [dbo].[Sp_ETL_RankingVentas] 'D:\Sitios\xm\FTP_CSV\RankingVentas\ConsultaRankingVendas_d1165528-7148-4fde-8616-c1bf1114a3bd.csv', 1
-- =============================================
CREATE PROCEDURE [dbo].[Sp_ETL_RankingVentas]

@rutaArchivo varchar(255),
@idLogAuditoria int	
AS
BEGIN
	SET NOCOUNT ON;	
BEGIN TRY
	DECLARE @ETL VARCHAR(100) = 'Sp_ETL_RankingVentas';
	DECLARE @SQL_BULK VARCHAR(MAX);
	DECLARE @mensajeProceso AS VARCHAR(MAX);
	SET @mensajeProceso = 'Sin Procesar el SP';		

	
	
	--SET @rutaArchivo = REPLACE( @rutaArchivo, 'D:\Sitios\', '\\10.21.3.15\' )
	INSERT INTO RFLOG( OPERACION, EVENTO ) VALUES('BULKINSERT', @rutaArchivo)
	
	--\\10.21.3.15\xm\FTP_CSV\CarteraPendiente
	--D:\Sitios\xm\FTP_CSV\CarteraPendiente\ConsultaTitulo_78afeb01-43.csv

TRUNCATE TABLE geraRankingVentas;


--DECLARE @rutaArchivo varchar(255) = '\\10.21.3.15\xm\FTP_CSV\PedidosCertificados\ConsultaPedidos_5e467c4b-2224-4c1a-ae04-ce4bea93f912.csv'

SET @SQL_BULK = 'BULK INSERT geraRankingVentas FROM ''' + @rutaArchivo + ''' WITH
        (
        FIRSTROW = 2,
        FIELDTERMINATOR = '';'',
        ROWTERMINATOR = ''\n'',
		CODEPAGE = ''ACP''
        )'

		PRINT @SQL_BULK

EXEC (@SQL_BULK); 
     
	 UPDATE geraRankingVentas
	 SET    CicloCaptacion      = dbo.preparCiclo(CicloCaptacion)
		  , CicloFacturacion    = dbo.preparCiclo(CicloFacturacion)
		  , Facturacion         = dbo.PreparaNumero( Facturacion  , ',' )
		  , PrecioLista         = dbo.PreparaNumero( PrecioLista  , ',' )
		  , PrecioCN            = dbo.PreparaNumero( PrecioCN     , ',' )

     UPDATE RFPARAM SET 
	    CCMNDT = GETDATE()
	  , CCCODEN = @idLogAuditoria
	  , CCUDC2 = 1
	  , CCNOT2 = @rutaArchivo
	  , CCNOTE = 'OK'
	WHERE CCTABL = 'ETL_FTP' AND CCCODE = 'gera' AND CCCODE2 = @ETL ;

	
SELECT 'OK' AS 'MensajeProceso';
        
END TRY
BEGIN CATCH 
	SET @mensajeProceso = 'Se ha generado un error en la Estructura del BULK-INSERT: Linea#:' + CAST (ERROR_LINE() AS VARCHAR) + ' - Mensaje de Error:' + ERROR_MESSAGE();				
	UPDATE RFPARAM SET 
	    CCMNDT = GETDATE()
	  , CCUDC2 = -1
	  , CCNOT2 = @rutaArchivo	  
	  , CCNOTE = @mensajeProceso
	WHERE CCTABL = 'ETL_FTP' AND CCCODE = 'gera' AND CCCODE2 = @ETL;	
	SELECT @mensajeProceso AS 'MensajeProceso';
END CATCH
  
END