﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Sp_LibPebMasivo]

   -- [dbo].[Sp_LibPebMasivo]

AS

DECLARE @result varchar(100) = '1'

BEGIN


BEGIN TRY 

   TRUNCATE TABLE LibPedMasivos

    
	INSERT INTO LibPedMasivos

SELECT     [Pedido]
		  ,[CodPersona]
		  ,[NombreRevendedor]
		  ,[Puntos]
		  ,[Ciclo]
		  ,[FechaPedido]
		  ,[TotalPagar]
		  ,[TipoPendencia]
		  ,[DeudaYSaldoCapital]
		  ,[PorUsoCreditoPedido]
		  ,[CreditoExcedido]
		  ,[CreditoTLAExcedido]
		  ,[ValorDeLosTitulosVencidos]
		  ,[CantidadDiasRetraso]
		  ,[PedidosEnRetraso]
		  ,[CtdTitulosPendientes]
		  ,[CtdTitulosVencidos]
		  ,[EstructuraPadre]
		  ,[Estructura]
		  ,[TelResidencial]
		  ,[TelMovil]
		  ,[PerfilesActivos]
		  ,[PerfilesInactivos]
		  ,[CicloUltimoPedidoFacturado]
		  ,[PerfilCredito]
		  ,[Otrasinformaciones]
FROM     geraLibPedConPendencia

     PRINT @result
			   			   
			   INSERT INTO LibPedEventos
			   SELECT 'Sp_LibPebMasivo', @result, GETDATE()
			   
			   UPDATE rfparam SET 
					  CCNOT1 = 'Termino'
					, CCUDC1 = 1
					, CCMNDT = GETDATE()
 
				WHERE CCTABL = 'LIBPEDLOG' AND CCCODE = 'Sp_LibPebMasivo' 
	 END TRY 

	 BEGIN CATCH 
	     INSERT INTO LibPedEventos
         SELECT 'Sp_LibPebMasivo', '0', GETDATE()
         
           UPDATE rfparam SET 
				  CCNOT1 = 'No encontro datos con la condicion'
				, CCUDC1 = 0
				, CCMNDT = GETDATE() 
           WHERE CCTABL = 'LIBPEDLOG' AND CCCODE = 'Sp_LibPebMasivo' 
         
	 END CATCH


END