﻿-- =============================================
-- Author:		FM,AS
-- Create date: 2018-05-28
-- Description:	Genera resulatados Seguimiento Campañas de Categoria
-- EXEC [dbo].[SP_CTResultA] 35
-- =============================================
CREATE PROCEDURE [dbo].[SP_CTResultA] 
	@IdCTCampaign int
AS
BEGIN
BEGIN TRY
DECLARE @mensajeProceso AS VARCHAR(MAX);
DECLARE @tot_ciclos AS smallint;

SET @mensajeProceso = 'Sin Procesar el SP';

UPDATE CTCampaignPrize
   SET Ciclos = ( SELECT Count(*) FROM CTCampaignPrize WHERE IdCTCampaign = @IdCTCampaign ) 
WHERE IdCTCampaign = @IdCTCampaign 

TRUNCATE TABLE CTCampResultA
SET @mensajeProceso = 'Insertando CTCampResultA';
INSERT INTO CTCampResultA( IdCTCampaign, Codigo_Revendedor, PuntosLogrados, Facturacion, Premio, PuntosParaPremio, CiclosCompra, CiclosPremio )
SELECT
    p.IdCTCampaign
  , v.CodConsultora
  , SUM(CONVERT(INT,v.CantPontos))    Puntos
  , SUM(CONVERT(FLOAT,v.Facturacion)) Facturacion
  , z.Premio
  , z.Puntos                AS PuntosPremio
  , COUNT(DISTINCT c.Ciclo) AS Count_Ciclo
  , z.Ciclos
FROM
    geraRankingVentas v
    INNER JOIN
        CTCampaignProduct p
        ON
            p.CodigoProducto = v.CodigoProducto
    INNER JOIN
        CTCampaignPrize z
        ON
            z.IdCTCampaign = p.IdCTCampaign
    INNER JOIN
        CTCampaignCycle c
        ON
            c.IdCTCampaign = z.IdCTCampaign
            AND c.Ciclo         = CONVERT(INT,v.CicloCaptacion)
WHERE
    p.IdCTCampaign = @IdCTCampaign
    AND v.Tipo     = 'Venta'
    AND v.CicloCaptacion IN
    (
        SELECT DISTINCT
            CONVERT(nvarchar,c.Ciclo)
        FROM
            CTCampaignPrize z
            INNER JOIN
                CTCampaignCycle c
                ON
                    c.IdCTCampaign = z.IdCTCampaign
        WHERE
            z.IdCTCampaign = @IdCTCampaign
    )
GROUP BY
    p.IdCTCampaign
  , v.CodConsultora
  , z.Premio
  , z.Puntos
  , v.NombreConsultora
  , z.Ciclos
ORDER BY
    v.CodConsultora


	SET @mensajeProceso = 'Actualizando CTCampResultA';
UPDATE CTCampResultA SET 
  StatusPedido = CASE WHEN CiclosCompra < CiclosPremio THEN 'Pasar Pedido' ELSE 'OK Pedido' END
, Accion = CASE WHEN PuntosLogrados >= PuntosParaPremio THEN 'Ganadora Premio' 
                WHEN CiclosCompra < CiclosPremio THEN 'Pasar Pedido, ' + CONVERT(nvarchar, PuntosParaPremio - PuntosLogrados ) + ' Pts Faltantes'
				ELSE CONVERT(nvarchar, PuntosParaPremio - PuntosLogrados ) + ' Pts Faltantes' END
, Color = CASE WHEN PuntosLogrados >= PuntosParaPremio THEN 'rgba(220, 254, 158, 0.63)' 
                WHEN CiclosCompra < CiclosPremio THEN 'rgba(254, 158, 172, 0.23)'
				ELSE 'rgba(253, 189, 154, 0.23)' END

WHERE
	IdCTCampaign = @IdCTCampaign

SELECT 'OK' AS 'MensajeProceso';
END TRY
BEGIN CATCH 
	SET @mensajeProceso = 'Se ha generado un error en la Estructura del BULK-INSERT: Linea#:' + CAST (ERROR_LINE() AS VARCHAR) + ' - Mensaje de Error:' + ERROR_MESSAGE();				
		
	SELECT @mensajeProceso AS 'MensajeProceso';
END CATCH
END