﻿-- =============================================
-- Author:		FM,AS
-- Create date: 2018-05-28
-- Description:	Genera resulatados Seguimiento Campañas de Categoria
-- EXEC [dbo].[SP_CTResultB] 33
-- =============================================

ALTER PROCEDURE [dbo].[SP_CTResultB] 
	@IdCTCampaign int
AS
BEGIN
BEGIN TRY
DECLARE @mensajeProceso AS VARCHAR(MAX);
DECLARE @tot_ciclos AS smallint;

SET @mensajeProceso = 'Sin Procesar el SP';

UPDATE CTCampaignPrize
   SET Ciclos = ( SELECT Count(*) FROM CTCampaignPrize WHERE IdCTCampaign = @IdCTCampaign ) 
WHERE IdCTCampaign = @IdCTCampaign 

TRUNCATE TABLE CTCampResultB
SET @mensajeProceso = 'Insertando CTCampResultB';

INSERT INTO CTCampResultB
           ([IdCTCampaign]
           ,[Persona]
           ,[PedidosLogrados]
           ,[ValorTotal]
           ,[PuntosLogrados]
           ,[Premio]
           ,[PuntosParaPremio]
           ,[MinimoPedidos] )
SELECT
         @IdCTCampaign AS IdCTCampaign
       , p.Persona
       , Count(DISTINCT p.CodigoPedido)   AS PedidosLogrados
       , Sum(CONVERT(FLOAT,p.ValorTotal))    ValorTotal
       , Sum(CONVERT(smallint,p.Puntos))     PuntosLogrados
       , z.Premio
       , z.Puntos AS PuntosParaPremio
       , z.MinimoPedidos
FROM
         geraPedidosCamp p
       , CTCampaignPrize z
WHERE
         p.CicloCaptacion IN
         (
                SELECT
                       c.Ciclo
                FROM
                       CTCampaignCycle c
                WHERE
                       c.IdCTCampaign = @IdCTCampaign
         )
         AND z.IdCTCampaign = @IdCTCampaign
GROUP BY
         p.Persona
       , z.Premio
       , z.Puntos
       , z.MinimoPedidos
       

SET @mensajeProceso = 'Actualizando CTCampResultA';
UPDATE
       CTCampResultB
SET    StatusPedido =
       CASE
              WHEN PedidosLogrados < MinimoPedidos
                     THEN 'Pasar Pedido'
                     ELSE 'OK Pedido'
       END
     , Accion = CASE
                WHEN PuntosLogrados >= PuntosParaPremio
                     THEN 'Ganadora Premio'
                WHEN PedidosLogrados < MinimoPedidos
                     THEN 'Pasar Pedido, ' + CONVERT(nvarchar, PuntosParaPremio - PuntosLogrados ) + ' Pts Faltantes'
                     ELSE CONVERT(nvarchar, PuntosParaPremio - PuntosLogrados ) + ' Pts Faltantes'
       END
WHERE
       IdCTCampaign = @IdCTCampaign
       
SELECT 'OK' AS 'MensajeProceso';
END TRY
BEGIN CATCH 
	SET @mensajeProceso = 'Se ha generado un error en la Estructura del BULK-INSERT: Linea#:' + CAST (ERROR_LINE() AS VARCHAR) + ' - Mensaje de Error:' + ERROR_MESSAGE();				
		
	SELECT @mensajeProceso AS 'MensajeProceso';
END CATCH
END



