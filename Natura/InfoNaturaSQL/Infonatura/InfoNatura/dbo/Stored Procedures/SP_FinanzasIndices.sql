﻿CREATE PROCEDURE [dbo].[SP_FinanzasIndices]
AS
BEGIN

TRUNCATE TABLE [FinanzasResu]
TRUNCATE TABLE [FinanzasResuTot]

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumAnterior]) 
(SELECT   1 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 1 AND TipoDato = 3 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumAnterior]) 
(SELECT   2 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 2 AND TipoDato = 3 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumAnterior]) 
(SELECT   3 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 3  AND TipoDato = 3 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumAnterior]) 
(SELECT   4 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 4 AND TipoDato = 3 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumAnterior]) 
(SELECT   5 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 5 AND TipoDato = 3 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumAnterior]) 
(SELECT   6 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 6 AND TipoDato = 3 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumAnterior]) 
(SELECT   7 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 7 AND TipoDato = 3
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumAnterior]) 
(SELECT   8 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 8 AND TipoDato = 3 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumAnterior]) 
(SELECT   9 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 9 AND TipoDato = 3
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumAnterior]) 
(SELECT   10 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 10 AND TipoDato = 3
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumAnterior]) 
(SELECT   11 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 11 AND TipoDato = 3 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumAnterior]) 
(SELECT   12 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 12 AND TipoDato = 3
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 







INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumRO]) 
(SELECT   1 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 1 AND TipoDato = 4 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumRO]) 
(SELECT   2 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 2 AND TipoDato = 4 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumRO]) 
(SELECT   3 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 3  AND TipoDato = 4 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumRO]) 
(SELECT   4 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 4 AND TipoDato = 4 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumRO]) 
(SELECT   5 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 5 AND TipoDato = 4 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumRO]) 
(SELECT   6 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 6 AND TipoDato = 4 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumRO]) 
(SELECT   7 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 7 AND TipoDato = 4
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumRO]) 
(SELECT   8 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 8 AND TipoDato = 4 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumRO]) 
(SELECT   9 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 9 AND TipoDato = 4
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumRO]) 
(SELECT   10 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 10 AND TipoDato = 4
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumRO]) 
(SELECT   11 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 11 AND TipoDato = 4 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumRO]) 
(SELECT   12 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 12 AND TipoDato = 4
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 










INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumPpto]) 
(SELECT   1 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 1 AND TipoDato = 1 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumPpto]) 
(SELECT   2 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 2 AND TipoDato =1 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumPpto]) 
(SELECT   3 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 3  AND TipoDato = 1 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumPpto]) 
(SELECT   4 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 4 AND TipoDato = 1 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumPpto]) 
(SELECT   5 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 5 AND TipoDato = 1 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumPpto]) 
(SELECT   6 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 6 AND TipoDato = 1 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumPpto]) 
(SELECT   7 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 7 AND TipoDato = 1
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumPpto]) 
(SELECT   8 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 8 AND TipoDato = 1 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumPpto]) 
(SELECT   9 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 9 AND TipoDato = 1
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumPpto]) 
(SELECT   10 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 10 AND TipoDato = 1
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumPpto]) 
(SELECT   11 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 11 AND TipoDato = 1 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumPpto]) 
(SELECT   12 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 12 AND TipoDato = 1
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 



INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumActual]) 
(SELECT   1 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 1 AND TipoDato = 2
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumActual]) 
(SELECT   2 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 2 AND TipoDato =2 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumActual]) 
(SELECT   3 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 3  AND TipoDato = 2 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumActual]) 
(SELECT   4 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 4 AND TipoDato = 2 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumActual]) 
(SELECT   5 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 5 AND TipoDato = 2 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumActual]) 
(SELECT   6 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 6 AND TipoDato = 2 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumActual]) 
(SELECT   7 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 7 AND TipoDato = 2
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumActual]) 
(SELECT   8 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 8 AND TipoDato = 2
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumActual]) 
(SELECT   9 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 9 AND TipoDato = 2
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumActual]) 
(SELECT   10 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 10 AND TipoDato = 2
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumActual]) 
(SELECT   11 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 11 AND TipoDato = 2 
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]( [Mes],[Tercero] ,[CentroCosto],[SubCentroCosto] ,[Proyecto] ,[AcumActual]) 
(SELECT   12 ,[Tercero] ,[CentroCosto] ,[SubCentroCosto] ,[Proyecto] ,Sum([Saldo]) Acum_Actual  
FROM [InfoNatura].[dbo].[FinanzasDatos] WHERE Mes < = 12 AND TipoDato = 2
GROUP BY [TipoDato] , [CentroCosto] ,[Tercero] ,[SubCentroCosto] ,[Proyecto]) 


INSERT INTO [InfoNatura].[dbo].[FinanzasResu]
           ([Mes]
           ,[CentroCosto]
           ,[SubCentroCosto]
           ,[Proyecto]
           ,[Anterior])
         
     
           (SELECT 
	[Mes]
      ,[CentroCosto]
      ,[SubCentroCosto]
      ,[Proyecto]
      ,Sum([Saldo]) Acum_Actual 

  FROM [InfoNatura].[dbo].[FinanzasDatos]	WHERE TipoDato = 3
  GROUP BY  [Mes]
      
      ,[CentroCosto]
      ,[SubCentroCosto]
      ,[Proyecto])

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]
           ([Mes]
           ,[CentroCosto]
           ,[SubCentroCosto]
           ,[Proyecto]
           ,[Actual])
         
     
           (SELECT 
	[Mes]
      ,[CentroCosto]
      ,[SubCentroCosto]

      ,[Proyecto]
      ,Sum([Saldo]) Acum_Actual 

  FROM [InfoNatura].[dbo].[FinanzasDatos]	WHERE TipoDato = 2
  GROUP BY  [Mes]
      
      ,[CentroCosto]
      ,[SubCentroCosto]
      ,[Proyecto])
	  

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]
           ([Mes]
           ,[CentroCosto]
           ,[SubCentroCosto]
           ,[Proyecto]
           ,[Ppto])
         
     
           (SELECT 
	[Mes]
      ,[CentroCosto]
      ,[SubCentroCosto]
      ,[Proyecto]
      ,Sum([Saldo]) Acum_Actual 

  FROM [InfoNatura].[dbo].[FinanzasDatos]	WHERE TipoDato = 1
  GROUP BY  [Mes]
     
      ,[CentroCosto]
      ,[SubCentroCosto]
      ,[Proyecto])

INSERT INTO [InfoNatura].[dbo].[FinanzasResu]
           ([Mes]
           ,[CentroCosto]
           ,[SubCentroCosto]
           ,[Proyecto]
           ,[RO])
         
     
           (SELECT 
	[Mes]
      ,[CentroCosto]
      ,[SubCentroCosto]
      ,[Proyecto]
      ,Sum([Saldo]) Acum_Actual 

  FROM [InfoNatura].[dbo].[FinanzasDatos]	WHERE TipoDato = 4
  GROUP BY  [Mes]
      
      ,[CentroCosto]
      ,[SubCentroCosto]
      ,[Proyecto])

END