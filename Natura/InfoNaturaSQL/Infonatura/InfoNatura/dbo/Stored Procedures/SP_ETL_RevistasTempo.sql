﻿-- =============================================
-- Author:		FM
-- Create date: 2018-09-03
-- Description:	ETL REVISTAS
-- =============================================
CREATE PROCEDURE [dbo].[SP_ETL_RevistasTempo]
AS
BEGIN
	SET NOCOUNT ON;
BEGIN TRY

	DECLARE @SQL_BULK VARCHAR(MAX);
	DECLARE @mensajeProceso AS VARCHAR(MAX);
	DECLARE @ID_TRANSP AS int;

	SET @mensajeProceso = 'Sin Procesar el SP';
	SET @ID_TRANSP = 100006;

	TRUNCATE TABLE RevistasServientrega;
	BULK INSERT RevistasServientrega FROM '\\10.21.3.15\xm\Revistas\Servientrega.txt' WITH
        (
        FIRSTROW = 2,
        DATAFILETYPE ='widechar',
        FIELDTERMINATOR = '\t',
        ROWTERMINATOR = '\n',
		CODEPAGE = 'ACP'
        )


	UPDATE RevistasTracking SET 
		  CodConsultora              = w.CodCN 
		, Ciudad                     = w.CiudadEntrega 
		, Depto                      = w.Depto
		, Direccion                  = w.Direccion
		, Telefono                   = w.TelResidencia 
		, Celular                    = w.Celular
		, FechaDespacho              = w.Fechadespacho 
		, Ciclo                      = w.Ciclo 
		, Estado                     = w.Estado
		, Novedad1                   = w.Novedad1
	FROM RevistasTracking r Inner Join 
		 RevistasServientrega w on r.IdTransportadora = @ID_TRANSP and r.NumeroGuia = w.NumeroDeGuia  ;


	INSERT INTO RevistasTracking ( 
		  NumeroGuia
		, IdTransportadora
		, CodConsultora
		, Ciudad
		, Depto
		, Direccion
		, Telefono
		, Celular
		, FechaDespacho
		, Ciclo
		, Estado
		, Novedad1
	)
	SELECT 
		  w.NumeroDeGuia 
		, @ID_TRANSP
		, w.CodCN
		, w.CiudadEntrega
		, w.Depto
		, w.Direccion
		, w.TelResidencia
		, w.Celular
		, w.FechaDespacho
		, w.Ciclo
		, w.Estado
		, w.Novedad1
	 FROM RevistasServientrega w Left Join RevistasTracking r on r.IdTransportadora = @ID_TRANSP and w.NumeroDeGuia  = r.NumeroGuia
	 WHERE r.NumeroGuia Is Null ;


	SELECT 'OK' AS 'MensajeProceso';
	END TRY
	BEGIN CATCH 
		SET @mensajeProceso = 'Se ha generado un error en la Estructura del BULK-INSERT: Linea#:' + CAST (ERROR_LINE() AS VARCHAR) + ' - Mensaje de Error:' + ERROR_MESSAGE();				
		SELECT @mensajeProceso AS 'MensajeProceso';
	END CATCH
END