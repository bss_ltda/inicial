﻿CREATE PROCEDURE [dbo].[SP_SubirArchivosRequerido] (@Id BigInt)
AS

BEGIN
SET NOCOUNT ON;
DECLARE @Name nvarchar(50)
DECLARE @Requerido nvarchar(50)
DECLARE @Pesos nvarchar(50)
DECLARE @Linea nvarchar(50)
SELECT @Name = Codigo FROM TABLAMAESTRA WHERE TABLA = 'DIRECCION';	
SELECT @Requerido = CCSDSC FROM ZCC WHERE CCTABL = 'ARCHIVOS' AND CCCODE = 'REQUERIDO' AND CCDESC = 'Requerido';
SELECT @Pesos = CCSDSC FROM ZCC WHERE CCTABL = 'ARCHIVOS' AND CCCODE = 'REQUERIDO' AND CCDESC = 'Pesos';
SELECT @Linea = CCSDSC FROM ZCC WHERE CCTABL = 'ARCHIVOS' AND CCCODE = 'REQUERIDO' AND CCDESC = 'Linea Separacion';
Declare @comando nvarchar(400)
truncate table RequridoRqr;
truncate table RequeridoPlanilla;
	set @comando = 'BULK INSERT RequridoRqr FROM ''\\'+ @Name +'\InfoNaturaDatos\Requerido\'+ @Requerido +'.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'')'
	EXEC sp_executesql @comando
	UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Pesos' WHERE Id=@Id


	truncate table RequeridoPesos;  
	set @comando = 'BULK INSERT RequeridoPesos FROM ''\\'+ @Name +'\InfoNaturaDatos\Requerido\'+ @Pesos +'.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'')'
	EXEC sp_executesql @comando
	UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Requerido' WHERE Id=@Id

	truncate table RequeridoLineaSeparacion;
 	set @comando = 'BULK INSERT RequeridoLineaSeparacion FROM ''\\'+ @Name +'\InfoNaturaDatos\Requerido\'+ @Linea +'.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'')'
	EXEC sp_executesql @comando
	UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Linea' WHERE Id=@Id;

END