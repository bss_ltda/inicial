﻿CREATE PROCEDURE [dbo].[SP_SubirArchivosPremios] (@Id BigInt)
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY
	DECLARE @Name nvarchar(50)
	DECLARE @Paso nvarchar(50)
	SELECT @Name = Codigo FROM TABLAMAESTRA WHERE TABLA = 'DIRECCION';	
	Declare @comando nvarchar(400)
	truncate table PremiosCobroInterno;
	truncate table PremiosDiaMora; 
	truncate table PremiosOndas;
	truncate table PremiosPrimerPedido;
	truncate table PremiosVentas;
	truncate table [PremiosPlanilla];

		set @comando = 'BULK INSERT PremiosCobroInterno FROM ''\\'+ @Name +'\InfoNaturaDatos\Premios\COBRO.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'')'
		set @Paso = 'PremiosCobroInterno'
		UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id
		EXEC sp_executesql @comando
			 
		set @comando = 'BULK INSERT PremiosDiaMora FROM ''\\'+ @Name +'\InfoNaturaDatos\Premios\DiasMora.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'')'
		set @Paso = 'PremiosDiaMora'
		UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id
		EXEC sp_executesql @comando
		
 		set @comando = 'BULK INSERT PremiosOndas FROM ''\\'+ @Name +'\InfoNaturaDatos\Premios\Ondas.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'')'
 		set @Paso = 'PremiosOndas'
		UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id
		EXEC sp_executesql @comando
		
    	set @comando = 'BULK INSERT PremiosPrimerPedido FROM ''\\'+ @Name +'\InfoNaturaDatos\Premios\PrimerPedido.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'')'
		set @Paso = 'PremiosPrimerPedido'
		UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id
		EXEC sp_executesql @comando
			
		set @comando = 'BULK INSERT PremiosVentas FROM ''\\'+ @Name +'\InfoNaturaDatos\Premios\Ventas.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'')'
		set @Paso = 'PremiosVentas'
		UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id
		EXEC sp_executesql @comando
	set @Paso = 'PremiosPlanilla'
INSERT INTO [InfoNatura].[dbo].[PremiosPlanilla]
           ([CodigoPedido]
           ,[SituaciónFiscal]
           ,[Factura]
           ,[Persona]
           ,[NombrePersona]
           ,[CodigoGrupo]
           ,[Orden del Pedido]
           ,[Puntos]
           ,[CantidadÍtems]
           ,[ValorTotal]
           ,[ValorTabla]
           ,[ValorPracticado]
           ,[ValorLíquido]
           ,[ValorProductosRegulares]
           ,[MedioCaptación]
           ,[SituaciónComercial]
           ,[BuscarPedido_ExcelDetalheSituacaoComercial]
           ,[SituacionIntegracionExterna]
           ,[Fecha]
           ,[Fecha Marketing]
           ,[PrevisiónEntrega]
           ,[FechaEntrega]
           ,[FechaAutFacturación]
           ,[Ciclo Captación]
           ,[SubCiclo]
           ,[Ciclo Marketing]
           ,[CicloInd]
           ,[CicloAnulacion]
           ,[CaptacionRestrita]
           ,[Dia do Ciclo]
           ,[PlanPago]
           ,[VIA PRINCIPAL]
           ,[Complemento]
           ,[BARRIO]
           ,[MUNICIPIO]
           ,[DEPARTAMIENTO]
           ,[Código Postal]
           ,[Referencia]
           ,[VIA PRINCIPALEntrega]
           ,[ComplementoEntrega]
           ,[BARRIOEntrega]
           ,[MUNICIPIOEntrega]
           ,[DEPARTAMIENTOEntrega]
           ,[Código Postal Entrega]
           ,[ReferenciaEntrega]
           ,[Telefono]
           ,[ModeloComercial]
           ,[DescripciónModeloComercial]
           ,[Cód Estructura Padre]
           ,[EstructuraPadre]
           ,[Cód Estructura]
           ,[Estructura]
           ,[Sector de Ventas]
           ,[CEL_SEC]
           ,[Cód Usuario Creación]
           ,[Usuario de Creación]
           ,[Cód Usuario de Finalización]
           ,[Usuario de Finalización]
           ,[Cod Transportadora]
           ,[Transportadora]
           ,[Volumen]
           ,[Peso Estimado]
           ,[Peso Real]
           ,[Lote Separación]
           ,[Minuta]
           ,[PedidoConsolidado]) select * from PremiosVentas 
set @Paso = 'Eliminando PremiosPlanilla Anulados'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id   
delete from PremiosPlanilla where SituaciónComercial = 'Anulado' 
UPDATE PremiosPlanilla  SET [Ciclo Captación] = CONVERT(VARCHAR(10),[Ciclo Captación] , 102)
UPDATE PremiosPlanilla  SET SaldoCapital = 0
UPDATE PremiosPlanilla  SET DiaMora = 0
UPDATE PremiosPlanilla  SET SaldoCapital = [Valor actualizado del pedido] FROM PremiosPlanilla AS R INNER JOIN dbo.PremiosCobroInterno AS P ON R.CodigoPedido = P.CodigoPedido
UPDATE PremiosPlanilla  SET DiaMora = DATEDIFF(day, FechaVencimiento, FechaSaldo) from  PremiosPlanilla AS R INNER JOIN  dbo.PremiosDiaMora AS P ON R.CodigoPedido = P.NúmeroPedido
UPDATE PremiosPlanilla SET PrimerPedido ='0'
--UPDATE PremiosPlanilla SET PrimerCiclo = 'NOFILTRA'
UPDATE PremiosPlanilla SET PrimerCiclo = [Ciclo Primer Pedido] FROM dbo.PremiosPlanilla AS R INNER JOIN dbo.PremiosPrimerPedido AS P ON R.Persona = P.CodigoRevendedor
UPDATE PremiosPlanilla  SET [PrimerCiclo] = CONVERT(VARCHAR(10),[PrimerCiclo] , 102)
update PremiosPlanilla set PrimerPedido = '1' where PrimerCiclo = [Ciclo Captación];
update PremiosPlanilla set PrimerPedido = '0' where PrimerCiclo <> [Ciclo Captación];
update PremiosOndas set  SECTOR =  REPLACE(SECTOR, ';', '') FROM [InfoNatura].[dbo].[PremiosOndas]
update PremiosPlanilla set  [Cód Estructura] =  REPLACE([Cód Estructura], '.', '') FROM [InfoNatura].[dbo].[PremiosOndas]
update PremiosPlanilla set Onda = ONDAS FROM PremiosPlanilla as R INNER JOIN PremiosOndas AS P ON p.[Codigo Sector]=r.[Cód Estructura];
UPDATE PremiosPlanilla  SET DiaMora = 0 WHERE DiaMora < 1
update PremiosPlanilla set PedidosMora = '1' where PrimerPedido = '1' and SaldoCapital <> '0'
UPDATE PremiosPlanilla SET [SaldoCapital1] = 0
UPDATE PremiosPlanilla SET [SaldoCapital1] = SaldoCapital
UPDATE PremiosPlanilla SET [SaldoCapital1] = ValorTotal WHERE DiaMora <> '0' and SaldoCapital = 0
set @Paso = 'Terminado'
UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = @Paso WHERE Id=@Id   


DECLARE @Ciclo nvarchar(10)
DECLARE @CicloInd nvarchar(10)
Select  @Ciclo = RTrim(MAX(CicloInd )), @CicloInd = RTrim(MAX(CicloInd))FROM PremiosPlanilla
set @Ciclo = REPLACE( @Ciclo, '/', '' )
set @Ciclo = Case When LEN( @Ciclo ) = 6 Then RIGHT( @Ciclo, 4 ) + LEFT( @Ciclo, 2 ) Else RIGHT( @Ciclo, 4 ) + '0' + LEFT( @Ciclo, 1 ) End

--Para informacion necesario en pagos Fofi
DELETE  FROM C42InfoEstructura Where Ciclo = @Ciclo

INSERT INTO C42InfoEstructura
SELECT 
 Case When @CicloInd = [CicloInd] Then '42' Else '63' End as  [CDias],
 @Ciclo                           as  [Ciclo],
 [CicloInd]                       as  [CicloInd],
 [Cód Estructura Padre]           as  [CodEstructuraPadre],
 [EstructuraPadre]                as  [EstructuraPadre],
 [Cód Estructura]                 as  [CodEstructura],
 [Estructura]                     as  [Estructura],
 sum([ValorTotal]               ) as  [ValorTotal],
 sum([ValorTabla]               ) as  [ValorTabla],
 sum([ValorPracticado]          ) as  [ValorPracticado],
 sum([ValorLíquido]             ) as  [ValorLíquido],
 sum([ValorProductosRegulares]  ) as  [ValorProductosRegulares],
 sum([SaldoCapital]             ) as  [SaldoCapital],
 sum([SaldoCapital1]            ) as  [SaldoCapital1] 
FROM PremiosPlanilla
GROUP BY [CicloInd],
 [Cód Estructura Padre],
 [EstructuraPadre],
 [Cód Estructura],
 [Estructura]   

UPDATE C42InfoEstructura set CicloInd = REPLACE( CicloInd, '/', '' ) Where Ciclo = @Ciclo
UPDATE C42InfoEstructura set CicloInd = Case When LEN( RTRIM(CicloInd) ) = 6 
										     Then RIGHT( RTRIM(CicloInd), 4 ) + LEFT( CicloInd, 2 ) 
										     Else RIGHT( RTRIM(CicloInd), 4 ) + '0' + LEFT( CicloInd, 1 ) End
Where Ciclo = @Ciclo


 END TRY
BEGIN CATCH
	DECLARE @ErrorNumber INT = ERROR_NUMBER();
    DECLARE @ErrorLine INT = ERROR_LINE();
    DECLARE @ErrorMessage NVARCHAR(4000) = ERROR_MESSAGE();
    DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
    DECLARE @ErrorState INT = ERROR_STATE();
    DECLARE @ErrorProcedure INT = ERROR_PROCEDURE();

    PRINT 'Actual error number: ' + CAST(@ErrorNumber AS VARCHAR(10)) + '.';
    PRINT 'Actual line number: ' + CAST(@ErrorLine AS VARCHAR(10)) + '.';
    PRINT 'Paso: ' + @Paso + '.';

    RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
    
    UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Error ' + @Paso, [Mensajes] = @ErrorMessage + CAST(@ErrorLine AS VARCHAR(10)) WHERE Id=@Id
END CATCH;     
END