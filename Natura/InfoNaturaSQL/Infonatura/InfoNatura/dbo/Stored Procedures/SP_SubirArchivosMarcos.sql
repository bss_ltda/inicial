﻿CREATE PROCEDURE [dbo].[SP_SubirArchivosMarcos] (@Id BigInt, @Ciclo varchar(8))

AS
BEGIN

	SET NOCOUNT ON;


DECLARE @Name nvarchar(50)
DECLARE @SepCampo nchar(1)
DECLARE @CargaConceptos  nchar(1)
DECLARE @PrimerCiclo  nchar(4)

set @PrimerCiclo =  LEFT( @Ciclo, 4 ) +  '01' 
set @SepCampo = ';'
set @CargaConceptos = 'Y'

SELECT @Name = Codigo FROM TABLAMAESTRA WHERE TABLA = 'DIRECCION';	
Declare @comando nvarchar(400)

delete [MarcosFacturacion] where Ciclo = @Ciclo 
              
set @comando = 'BULK INSERT [MarcosFacturacion] FROM ''\\'+ @Name + '\InfoNaturaDatos\Marcos\Facturacion' + @Ciclo + '.csv''' +
               ' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = ''' + @SepCampo + ''', ROWTERMINATOR = ''\n'', CODEPAGE = ''ACP'')'
--PRINT    @comando            
EXEC sp_executesql @comando

UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Ok Facturacion' WHERE Id=@Id

If @CargaConceptos = 'Y' 
BEGIN
	delete MarcosConceptos where Ciclo = @Ciclo 
	set @comando = 'BULK INSERT MarcosConceptos FROM ''\\'+ @Name +'\InfoNaturaDatos\Marcos\Conceptos' + @Ciclo + '.csv''' +
				   ' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = ''' + @SepCampo + ''',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
	EXEC sp_executesql @comando
END

UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Ok Conceptos' WHERE Id=@Id

update [InfoNatura].[dbo].[MarcosFacturacion] set [Objetivo2] = '' where  [Objetivo2] IS NULL
update [InfoNatura].[dbo].[MarcosFacturacion] set [Gerencia] = '' where  [Gerencia] IS NULL
update [InfoNatura].[dbo].[MarcosFacturacion] set [GerenciaV] = '' where  [GerenciaV] IS NULL
--update [InfoNatura].[dbo].[MarcosConceptos] set Novedad   = '' where  Novedad IS NULL
UPDATE MarcosFacturacion SET Real2 = REPLACE (Real2,'$','')
UPDATE MarcosFacturacion SET Real2 = REPLACE (Real2,'.','')
UPDATE MarcosFacturacion SET Real2 = REPLACE (Real2,' ','')
UPDATE MarcosFacturacion SET Real2 = REPLACE (Real2,'-','0')

delete from [MarcosPlanilla] where Ciclo = @Ciclo

INSERT INTO [InfoNatura].[dbo].[MarcosPlanilla]
           ([CodConsultora]            
           ,[Nombre]                   
           ,[Sector]                   
           ,[SectorN]                  
           ,[Gerencia]                 
           ,[Onda]                     
           ,[Correo]                   
           ,[Telefono]                 
           ,[Ingreso]                  
           ,[GanoAnterios]             
           ,[Ciclo] )                  
     SELECT 
            Codigo,  
	        Responsable,
	        SectorCod,
	        Sector,
	        Gerencia,
	        Onda,
	        Correo,
	        Telefono,
	        Ingreso,
	        MarcoAnterior,
	        @Ciclo 
	 FROM MarcosGrs 

UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Creo Planilla' WHERE Id=@Id

UPDATE[InfoNatura].[dbo].MarcosPlanilla  set Ano =  YEAR(Ingreso) where  Ciclo = @Ciclo 

UPDATE[InfoNatura].[dbo].MarcosPlanilla  set PuntajeHabilitador =  20 where  ano > = 2014 and Ciclo = @Ciclo 

UPDATE[InfoNatura].[dbo].MarcosPlanilla  set PuntajeHabilitador =  20 where  ano < = 2014 and GanoAnterios = 'SI' and Ciclo = @Ciclo 

UPDATE[InfoNatura].[dbo].MarcosPlanilla  set PuntajeHabilitador =  12 where  ano < = 2014 and GanoAnterios = 'NO' and Ciclo = @Ciclo 

--UPDATE[InfoNatura].[dbo].MarcosPlanilla  set FaturacionNece = (select CCNOT1 from ZCC where ZCC.[CCSDSC] = MarcosPlanilla.MarcoGanar AND [CCTABL] = 'MARCOS' AND [CCCODE]= 'PREMIOS')  where  Ciclo = @Ciclo

UPDATE[InfoNatura].[dbo].MarcosPlanilla  set FacturacionAcum = (
	select ISNULL(SUM(convert(float,Real2)), 0) 
	from MarcosFacturacion 
	where MarcosFacturacion.Sector = MarcosPlanilla.Sector  
	      and  MarcosFacturacion.Ciclo Between @PrimerCiclo and @Ciclo )
WHERE  Ciclo = @Ciclo 

update[InfoNatura].[dbo].MarcosPlanilla set FacturacionAcum = 0 where  FacturacionAcum IS NULL

UPDATE[InfoNatura].[dbo].MarcosPlanilla  SET 
MarcoGanar =  
      CASE 
         WHEN [FacturacionAcum] <= 1500000000 THEN 'PULSERA'
         WHEN [FacturacionAcum] <= 2200000000 THEN 'COLLAR'
         WHEN [FacturacionAcum] <= 3600000000 THEN 'ARETES'
      END, 
[FaturacionNece] =  
      CASE 
         WHEN [FacturacionAcum] <= 1500000000 THEN 1500000000
         WHEN [FacturacionAcum] <= 2200000000 THEN 2200000000
         WHEN [FacturacionAcum] <= 3600000000 THEN 3600000000
      END
where  Ciclo = @Ciclo 
      




UPDATE [InfoNatura].[dbo].MarcosPlanilla  set FacturacionFalta  =  FaturacionNece - FacturacionAcum  where  Ciclo = @Ciclo 

UPDATE [InfoNatura].[dbo].MarcosPlanilla  set FacturacionFalta  =  0 where  Ciclo = @Ciclo AND (FaturacionNece - FacturacionAcum )<0

DECLARE @Tot int
SELECT @Tot = RIGHT( @Ciclo, 2 )

DECLARE @Tot1 int
SELECT @Tot1 =  17 - @Tot


DECLARE @AnoAnt int
DECLARE @CicloAnt nvarchar(6)

SELECT @AnoAnt = LEFT( @Ciclo, 4 )  - 1 
 
IF RIGHT( @Ciclo, 2 ) = '01' 
BEGIN
  SELECT @CicloAnt =  cast(@AnoAnt as varchar) + '17'
END
ELSE

SELECT @CicloAnt = @Ciclo - 1
SELECT @Tot1 =  17 - @Tot

UPDATE [InfoNatura].[dbo].MarcosPlanilla  set PromFacturacion  = FacturacionFalta/@Tot1   where  Ciclo = @Ciclo 
UPDATE [InfoNatura].[dbo].MarcosPlanilla set Notas1 = @CicloAnt  where  Ciclo = @Ciclo 
UPDATE [InfoNatura].[dbo].MarcosPlanilla  set UltimoConcepto = ( 
	select Concepto 
	from MarcosConceptos 
	where 	MarcosConceptos.Sector = MarcosPlanilla.Sector 
			and MarcosConceptos.Ciclo = @CicloAnt )
WHERE  Ciclo = @Ciclo 

UPDATE [InfoNatura].[dbo].MarcosPlanilla  set PuntosActual = (
	select [CCDESC] 
	from ZCC 
	where 	ZCC.[CCSDSC] = MarcosPlanilla.UltimoConcepto 
			AND [CCTABL] = 'MARCOS' AND [CCCODE]= 'PUNTAJE')  
WHERE  Ciclo = @Ciclo

UPDATE [InfoNatura].[dbo].MarcosPlanilla set PuntosActual = '0' where  PuntosActual IS NULL

UPDATE [InfoNatura].[dbo].MarcosPlanilla  set PuntosAnteriores = (
	select SUM(convert(float,PuntosActual) ) 
	from MarcosPlanilla as MC 
	where 	MC.Sector = MarcosPlanilla.Sector 
			and LEFT( MC.Ciclo, 4 ) = (LEFT( @Ciclo, 4 )-1))  
WHERE  Ciclo = @Ciclo 

update [InfoNatura].[dbo].MarcosPlanilla set PuntosAnteriores = '0' where  PuntosAnteriores IS NULL

UPDATE [InfoNatura].[dbo].MarcosPlanilla  set PuntosAnteriores = PuntosAnteriores + ( 
	select (convert(float,MarcosGrs.PuntosAnteriores) ) 
	from MarcosGrs 
	where MarcosGrs.SectorCod = MarcosPlanilla.Sector)  
WHERE  Ciclo = @Ciclo 

UPDATE [InfoNatura].[dbo].MarcosPlanilla  set SumPuntos = PuntosAnteriores + PuntosActual  where  Ciclo = @Ciclo 

UPDATE [InfoNatura].[dbo].MarcosPlanilla  set Status = 'HABILITADO'  where  Ciclo = @Ciclo  AND SumPuntos >= PuntajeHabilitador

UPDATE [InfoNatura].[dbo].MarcosPlanilla  set Status = 'NO HABILITADO'  where  Ciclo = @Ciclo  AND SumPuntos < PuntajeHabilitador


UPDATE[InfoNatura].[dbo].MarcosPlanilla  set Notas2 = '0'  where  Ciclo = @Ciclo  AND  PuntajeHabilitador - PuntosAnteriores > = 0
UPDATE[InfoNatura].[dbo].MarcosPlanilla  set Notas2 = PuntajeHabilitador - PuntosAnteriores  where  Ciclo = @Ciclo  AND   PuntajeHabilitador - PuntosAnteriores < 0
update[InfoNatura].[dbo].MarcosPlanilla set Notas2 = '0' where  Notas2 IS NULL
update[InfoNatura].[dbo].MarcosPlanilla set Notas2 = '0' where  Notas2 < 0

UPDATE[InfoNatura].[dbo].MarcosPlanilla  set ConceptoNecesario = ( 
	select [CCSDSC] 
	from ZCC 
	where	ZCC.[CCDESC] = MarcosPlanilla.Notas2 
			AND [CCTABL] = 'MARCOS' 
			AND [CCCODE]= 'PUNTAJE2')  
WHERE  Ciclo = @Ciclo

update[InfoNatura].[dbo].MarcosPlanilla set ConceptoNecesario = '' where  ConceptoNecesario IS NULL
update[InfoNatura].[dbo].MarcosPlanilla set UltimoConcepto = '' where  UltimoConcepto IS NULL

--update MarcosPlanilla set Correo = 'javiergarzon@natura.net'



END