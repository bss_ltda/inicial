﻿CREATE PROCEDURE [dbo].[SP_CarteraIndiceMorosidadInicios](@MesPlanilla varchar(100))

AS
BEGIN
	SET NOCOUNT ON;
SELECt (SUM (CONVERT(float,[Saldo Total]))/SUM (CONVERT(float,ValorTítulo))) as procentaje FROM CarteraTítulosInd WHERE MesPlanilla =@MesPlanilla and Inicion='si' ;

END