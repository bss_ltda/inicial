﻿CREATE PROCEDURE [dbo].[SP_SubirArchivosRprt3] (@Id BigInt)
AS
BEGIN
	SET NOCOUNT ON;
  delete from RCarteraAgenciasDeCobro
BULK INSERT RCarteraAgenciasDeCobro
  FROM '\\10.21.2.15\InfoNaturaDatos\ReporteCartera\Agencias.csv'
   WITH
     (
		FIRSTROW = 2,
        FIELDTERMINATOR =',',
        ROWTERMINATOR = '\n',
        FIRE_TRIGGERS
      );
END