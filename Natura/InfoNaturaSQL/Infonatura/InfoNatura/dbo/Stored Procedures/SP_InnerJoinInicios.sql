﻿CREATE PROCEDURE [dbo].[SP_InnerJoinInicios]

AS
BEGIN
	SET NOCOUNT ON;
UPDATE CarteraTítulosInd SET CicloPrimerPedido = [Ciclo Primer Pedido] FROM dbo.CarteraTítulosInd AS R INNER JOIN dbo.CarteraInicios AS P ON R.CódigoPersona = P.CodigoRevendedor
update CarteraTítulosInd set Inicion = 'No' 
update CarteraTítulosInd set Inicion = 'Si' where CicloPrimerPedido = CicloCaptacion;
UPDATE CarteraTítulosInd SET Agencia = [Empresa de cobranza] FROM dbo.CarteraTítulosInd AS R INNER JOIN dbo.CarteraAgenciasDeCobro AS P ON R.Estructura = P.Sector
UPDATE CarteraTítulosInd SET Agencia = 'SISTEMCOBRO'
END