﻿CREATE PROCEDURE [dbo].[CifinNegativas] (@ID AS float) AS
BEGIN

	SET NOCOUNT ON;

DELETE FROM  [CifinPlanilla] WHERE REPORTE = 'NEGATIVO'
INSERT INTO [InfoNatura].[dbo].[CifinPlanilla]
           (
           ID
           , REPORTE										---1
           ,[TIPO IDENT]								---2
           ,[Nº IDENTIFICACION]							---3
           ,[CODIGO]									---4
           ,[NOMBRE TERCERO]							---5
           ,[RESERVADO]									---6
           ,[FECHA LIMITE DE PAGO]						---7
           ,[NUMERO OBLIGACION]							---8	
           ,[CÓDIGO SUCURSAL]							---9
           ,[CALIDAD]									---10
           ,[CALIFICACIÓN]								---11
           ,[ESTADO DEL TITULAR]						---12
           ,[ESTADO DE OBLIGACION]						---13
           ,[EDAD DE MORA]								---14
           ,[AÑOS EN MORA]								---15
           ,[FECHA DE CORTE]							---16
           ,[FECHA INICIO  ]							---17
           ,[FECHA TERMINACIÓN]							---18
           ,[FECHA DE EXIGIBILIDAD]						---19
           ,[FECHA DE PRESCRIPCION]						---20
           ,[FECHA DE PAGO]								---21
           ,[MODO EXTINCIÓN]							---22
           ,[TIPO DE PAGO]								---23
           ,[PERIODICIDAD]								---24
           ,[PROBABILIDAD DE NO PAGO]					---25
           ,[NÚMERO DE CUOTAS PAGADAS]					---26
           ,[NÚMERO DE CUOTAS PACTADAS]					---27
           ,[CUOTAS EN MORA]							---28
           ,[VALOR INICIAL]								---29
           ,[VALOR DE MORA]								---30
           ,[VALOR DEL SALDO]							---31
           ,[VALOR DE LA CUOTA]							---32
           ,[VALOR DE CARGO FIJO]						---33
           ,[LÍNEA DE CRÉDITO]							---34
           ,[CLÁUSULA DE PERMANENCIA]					---35
           ,[TIPO DE CONTRATO]							---36
           ,[ESTADO DE CONTRATO]						---37
           ,[TERMINO O VIGENCIA DEL CONTRATO]			---38
           ,[NUMERO DE MESES DEL CONTRATO]				---39
           ,[NATURALEZA JURÍDICA]						---40
           ,[MODALIDAD DE CRÉDITO]						---41
           ,[TIPO DE MONEDA]							---42
           ,[TIPO DE GARANTÍA]							---43
           ,[VALOR DE LA GARANTÍA]						---44
           ,[OBLIGACION REESTRUCTURADA]					---45
           ,[NATURALEZA DE LA REESTRUCTURACIÓN]			---46
           ,[NÚMERO DE REESTRUCTURACIONES]				---47
           ,[CLASE  TARJETA]							---48
           ,[NO DE CHEQUES DEVUELTOS]					---49
           ,[CATEGORÍA SERVICIOS ]						---50
           ,[PLAZO ]									---51
           ,[DÍAS DE CARTERA]							---52
           ,[TIPO DE CUENTA]							---53
           ,[CUPO SOBREGIRO]							---54
           ,[DIAS AUTORIZADOS]							---55
           ,[DIRECCION CASA DEL TERCERO]				---56
           ,[TELEFONO CASA DEL TERCERO]					---57
           ,[CODIGO CIUDAD CASA DEL TERCERO]			---58	
           ,[CIUDAD CASA DEL TERCERO]					---59
           ,[CODIGO DEPARTAMENTO DEL TERCERO]			---60
           ,[DEPARTAMENTO CASA DEL TERCERO]				---61
           ,[NOMBRE EMPRESA]							---62
           ,[DIRECCION DE LA EMPRESA]					---63
           ,[TELEFONO DE LA EMPRESA]					---64
           ,[CODIGO CIUDAD EMPRESA DEL TERCERO]			---65
           ,[CIUDAD EMPRESA DEL TERCERO]				---66
           ,[CODIGO DEPARTAMENTO EMPRESA DEL TERCERO]	---67
           ,[DEPARTAMENTO EMPRESA DEL TERCERO]			---68
           ,[FECHA_GENERACION])							---69
       Select
		    @ID
		   ,'NEGATIVO'									---1
           ,'01'										---2
           ,''											---3
           ,[CODIGO]									---4
           ,[NOMBRE_PERSONA]							---5
           ,''											---6
           , CONVERT(CHAR(10),[FECHA_VENCIMIENTO],111)	---7
           ,[CODIGO_PEDIDO]								---8
           ,'000001'									---9
           ,'P'											---10
           ,''											---11
           ,''											---12
           ,'01'										---13
           ,'0'											---14
           ,''											---15
           , CONVERT(CHAR(10),getdate(),111)			---17
           , CONVERT(CHAR(10),FECHA_AUT_FACTURACION,111)			---16
           ,''											---18
           ,''											---19
           ,''											---20
           ,''											---21
           ,''										    ---22
           ,''										---23
           ,'04'										---24	
           ,''											---25
           ,'000'										---26
           ,'001'										---27
           ,'001'										---28
           ,[VALOR_ORIGINAL]							---29
           ,VALOR_ACTUALIZADO_TOTAL						---30
           ,VALOR_ACTUALIZADO_TOTAL	     				---31
           ,'0'											---32
           ,''											---33
           ,'26'										---34
           ,''											---35
           ,'001'										---36
           ,'001'										---37	
           ,'02'										---38
           ,''											---39
           ,''											---40
           ,''											---41
           ,''											---42
           ,''											---43
           ,''											---44
           ,'02'										---45
           ,''											---46
           ,''											---47
           ,''											---48
           ,''											---49
           ,''											---50
           ,''											---51
           ,''											---52
           ,''											---53
           ,''											---54
           ,''											---55
           ,''											---56
           ,''											---57
           ,''											---58
           ,''											---59
           ,''											---60
           ,''											---61
           ,''											---62
           ,''											---63
           ,''											---64
           ,''											---65
           ,''											---66	
           ,''											---67
           ,''											---68
           ,CONVERT(CHAR(10),getdate(),111)										---69
           from CifinCobro t1
           where  CONVERT(CHAR(10),[FECHA_AUT_FACTURACION],111) > CONVERT(CHAR(10),'2012/01/01',111) 
                  AND  VALOR_ACTUALIZADO_TOTAL >10000
				  AND NOT EXISTS (SELECT null from CifinExcluidas t2 where t2.codigo = t1.CEDULA) 
				  AND NOT EXISTS (SELECT null from CifinExcluidasoB t2 where t2.codigo = t1.CODIGO_PEDIDO);
				  
UPDATE CifinPlanilla SET [EDAD DE MORA] ='0' 
UPDATE CifinPlanilla SET [EDAD DE MORA] =[Dia_Mora]  
       FROM dbo.CifinCobro  AS R INNER JOIN dbo.CifinPlanilla AS P ON R.Codigo_Pedido = P.[Numero obligacion];

UPDATE CifinPlanilla SET [EDAD DE MORA] = 
       CASE WHEN [EDAD DE MORA] < 60    THEN 00
            WHEN [EDAD DE MORA] < 90    THEN 02
            WHEN [EDAD DE MORA] < 120   THEN 03
            WHEN [EDAD DE MORA] < 150   THEN 04
            WHEN [EDAD DE MORA] < 180   THEN 05
            WHEN [EDAD DE MORA] < 210   THEN 06
            WHEN [EDAD DE MORA] < 240   THEN 07
            WHEN [EDAD DE MORA] < 270   THEN 08
            WHEN [EDAD DE MORA] < 300   THEN 09
            WHEN [EDAD DE MORA] < 330   THEN 10
            WHEN [EDAD DE MORA] < 360   THEN 11
            WHEN [EDAD DE MORA] < 540   THEN 12
            WHEN [EDAD DE MORA] < 730   THEN 13
            ELSE 14 END;
                   
--UPDATE CifinPlanilla SET [EDAD DE MORA] ='00' WHERE [EDAD DE MORA] >=0  AND  [EDAD DE MORA] <=59
--UPDATE CifinPlanilla SET [EDAD DE MORA] ='02' WHERE [EDAD DE MORA] >=60  AND  [EDAD DE MORA] <=89
--UPDATE CifinPlanilla SET [EDAD DE MORA] ='03' WHERE [EDAD DE MORA] >=90  AND  [EDAD DE MORA] <=119
--UPDATE CifinPlanilla SET [EDAD DE MORA] ='04' WHERE [EDAD DE MORA] >=120  AND  [EDAD DE MORA] <=149
--UPDATE CifinPlanilla SET [EDAD DE MORA] ='05' WHERE [EDAD DE MORA] >=150 AND  [EDAD DE MORA] <=179
--UPDATE CifinPlanilla SET [EDAD DE MORA] ='06' WHERE [EDAD DE MORA] >=180  AND  [EDAD DE MORA] <=209
--UPDATE CifinPlanilla SET [EDAD DE MORA] ='07' WHERE [EDAD DE MORA] >=210  AND  [EDAD DE MORA] <=239
--UPDATE CifinPlanilla SET [EDAD DE MORA] ='08' WHERE [EDAD DE MORA] >=240  AND  [EDAD DE MORA] <=269
--UPDATE CifinPlanilla SET [EDAD DE MORA] ='09' WHERE [EDAD DE MORA] >=270  AND  [EDAD DE MORA] <=299
--UPDATE CifinPlanilla SET [EDAD DE MORA] ='10' WHERE [EDAD DE MORA] >=300  AND  [EDAD DE MORA] <=329
--UPDATE CifinPlanilla SET [EDAD DE MORA] ='11' WHERE [EDAD DE MORA] >=330  AND  [EDAD DE MORA] <=359
--UPDATE CifinPlanilla SET [EDAD DE MORA] ='12' WHERE [EDAD DE MORA] >=360  AND  [EDAD DE MORA] <=539
--UPDATE CifinPlanilla SET [EDAD DE MORA] ='13' WHERE [EDAD DE MORA] >=540  AND  [EDAD DE MORA] <=729
--UPDATE CifinPlanilla SET [EDAD DE MORA] ='14' WHERE [EDAD DE MORA] >=730 


	UPDATE  CifinPlanilla SET 
	  [FECHA DE CORTE]        = REPLACE([FECHA DE CORTE],'/','')
	, [FECHA LIMITE DE PAGO]  = REPLACE([FECHA LIMITE DE PAGO],'/','')
	, [FECHA INICIO  ]        = REPLACE( [FECHA INICIO  ],'/','')
	, [FECHA TERMINACIÓN]     = REPLACE([FECHA TERMINACIÓN],'/','')
	, [FECHA DE EXIGIBILIDAD] = REPLACE([FECHA DE EXIGIBILIDAD],'/','')
	, [FECHA DE PRESCRIPCION] = REPLACE([FECHA DE PRESCRIPCION],'/','')
	, [FECHA DE PAGO]         = REPLACE([FECHA DE PAGO],'/','');

UPDATE  CifinPersonas SET CiudadResidencial =REPLACE(CiudadResidencial,'Á','A');
UPDATE  CifinPersonas SET CiudadResidencial =REPLACE(CiudadResidencial,'É','E');
UPDATE  CifinPersonas SET CiudadResidencial =REPLACE(CiudadResidencial,'Í','I');
UPDATE  CifinPersonas SET CiudadResidencial =REPLACE(CiudadResidencial,'Ó','O');
UPDATE  CifinPersonas SET CiudadResidencial =REPLACE(CiudadResidencial,'Ú','U');

UPDATE  CifinPersonas SET RegionResidencial =REPLACE(RegionResidencial,'Á','A');
UPDATE  CifinPersonas SET RegionResidencial =REPLACE(RegionResidencial,'É','E');
UPDATE  CifinPersonas SET RegionResidencial =REPLACE(RegionResidencial,'Í','I');
UPDATE  CifinPersonas SET RegionResidencial =REPLACE(RegionResidencial,'Ó','O');
UPDATE  CifinPersonas SET RegionResidencial =REPLACE(RegionResidencial,'Ú','U');

UPDATE  CifinCiudades SET dep =REPLACE(dep,'Á','A');
UPDATE  CifinCiudades SET dep =REPLACE(dep,'É','E');
UPDATE  CifinCiudades SET dep =REPLACE(dep,'Í','I');
UPDATE  CifinCiudades SET dep =REPLACE(dep,'Ó','O');
UPDATE  CifinCiudades SET dep =REPLACE(dep,'Ú','U');

UPDATE  CifinCiudades SET mun =REPLACE(mun,'Á','A');
UPDATE  CifinCiudades SET mun =REPLACE(mun,'É','E');
UPDATE  CifinCiudades SET mun =REPLACE(mun,'Í','I');
UPDATE  CifinCiudades SET mun =REPLACE(mun,'Ó','O');
UPDATE  CifinCiudades SET mun =REPLACE(mun,'Ú','U');


   UPDATE  CifinPersonas SET CodigoRevendedor=REPLACE(CodigoRevendedor,'.',''); 
   UPDATE CifinPlanilla SET 
     [Nº IDENTIFICACION]              = [Cédula de Ciudadanía/NIT]                                 
   , [DIRECCION CASA DEL TERCERO]     = calleResidencial + '-' + ComplementoResidencial   
   , [CIUDAD CASA DEL TERCERO]        = CiudadResidencial                                   
   , [DEPARTAMENTO CASA DEL TERCERO]  = RegionResidencial                             
   , [TELEFONO CASA DEL TERCERO]      = TelResidencial                                    
   FROM dbo.CifinPersonas AS R INNER JOIN dbo.CifinPlanilla AS P ON R.CodigoRevendedor = P.Codigo;
      
UPDATE CIFINPLANILLA SET [DEPARTAMENTO CASA DEL TERCERO] = 'BOGOTA DISTRITO CA'
WHERE [DEPARTAMENTO CASA DEL TERCERO] = 'CUNDINAMARCA' AND [CIUDAD CASA DEL TERCERO]   = 'BOGOTA'
	UPDATE CifinPlanilla SET [CODIGO CIUDAD CASA DEL TERCERO] =cmun fROM dbo.CifinCiudades  AS R INNER JOIN dbo.CifinPlanilla AS P ON R.mun = P.[CIUDAD CASA DEL TERCERO] AND R.dep = P.[DEPARTAMENTO CASA DEL TERCERO]
	UPDATE CifinPlanilla SET [CODIGO DEPARTAMENTO DEL TERCERO] =cdep fROM dbo.CifinCiudades  AS R INNER JOIN dbo.CifinPlanilla AS P ON   R.dep = P.[DEPARTAMENTO CASA DEL TERCERO]

	UPDATE CifinPlanilla SET [VALOR INICIAL] = (round(([VALOR INICIAL]/1000), 0))
	UPDATE CifinPlanilla SET [VALOR DE MORA] = (round(([VALOR DE MORA]/1000), 0))
	UPDATE CifinPlanilla SET [VALOR DEL SALDO] = (round(([VALOR DEL SALDO]/1000), 0))
UPDATE CifinPlanilla set [VALOR DE LA CUOTA] = (round(([VALOR DEL SALDO]/1000), 0))
END