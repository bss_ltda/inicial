﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_ETL_PedidosCertificados]
@rutaArchivo varchar(255),
@idLogAuditoria int	
AS
BEGIN
	SET NOCOUNT ON;	
BEGIN TRY
	DECLARE @ETL VARCHAR(100) = 'SP_ETL_PedidosCertificados';
	DECLARE @SQL_BULK VARCHAR(MAX);
	DECLARE @mensajeProceso AS VARCHAR(MAX);
	SET @mensajeProceso = 'Sin Procesar el SP';		
	
	SET @rutaArchivo = REPLACE( @rutaArchivo, 'D:\Sitios\', '\\10.21.3.15\' )
	INSERT INTO RFLOG( OPERACION, EVENTO ) VALUES('BULKINSERT', @rutaArchivo)
	
	--\\10.21.3.15\xm\FTP_CSV\CarteraPendiente
	--D:\Sitios\xm\FTP_CSV\CarteraPendiente\ConsultaTitulo_78afeb01-43.csv

TRUNCATE TABLE geraPedidosCertificados;


--DECLARE @rutaArchivo varchar(255) = '\\10.21.3.15\xm\FTP_CSV\PedidosCertificados\ConsultaPedidos_5e467c4b-2224-4c1a-ae04-ce4bea93f912.csv'

SET @SQL_BULK = 'BULK INSERT geraPedidosCertificados FROM ''' + @rutaArchivo + ''' WITH
        (
        FIRSTROW = 2,
        FIELDTERMINATOR = '';'',
        ROWTERMINATOR = ''\n'',
		CODEPAGE = ''ACP''
        )'
        
EXEC (@SQL_BULK);        

	UPDATE RFPARAM SET 
	    CCMNDT = GETDATE()
	  , CCCODEN = @idLogAuditoria
	  , CCUDC2 = 1
	  , CCNOT2 = @rutaArchivo
	  , CCNOTE = 'OK'
	WHERE CCTABL = 'ETL_FTP' AND CCCODE = 'gera' AND CCCODE2 = @ETL ;


SELECT 'OK' AS 'MensajeProceso';

END TRY
BEGIN CATCH 
	SET @mensajeProceso = 'LINEA: ' + CAST (ERROR_LINE() AS VARCHAR) + ' - ERROR:' + ERROR_MESSAGE();				

	UPDATE RFPARAM SET 
	    CCMNDT = GETDATE()
	  , CCCODEN = @idLogAuditoria
	  , CCUDC2 = -1
	  , CCNOT2 = @rutaArchivo	  
	  , CCNOTE = @mensajeProceso
	WHERE CCTABL = 'ETL_FTP' AND CCCODE = 'gera' AND CCCODE2 = @ETL;
	
	EXEC SP_ETL_Aviso @ETL;
	
	SELECT @mensajeProceso AS 'MensajeProceso';
END CATCH

END