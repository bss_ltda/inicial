﻿CREATE PROCEDURE [dbo].[SP_SubirArchivosFnzActual] (@Id BigInt)
AS
BEGIN
SET NOCOUNT ON;
DECLARE @Name nvarchar(50)
DECLARE @Archivo nvarchar(50)
SELECT @Name = Codigo FROM TABLAMAESTRA WHERE TABLA = 'DIRECCION';	
SELECT @Archivo = CCSDSC FROM ZCC WHERE CCTABL = 'ARCHIVOS' AND CCCODE = 'FINANZAS' AND CCDESC = 'Actual';
Declare @comando nvarchar(400)


DECLARE @Archivo1 nvarchar(50)
SELECT @Archivo1= CCSDSC FROM ZCC WHERE CCTABL = 'ARCHIVOS' AND CCCODE = 'FINANZAS' AND CCDESC = 'RO';


	Delete From FinanzasDatos Where TipoDato = '2';
	Delete From FinanzasDatos Where TipoDato = '4';
	
	set @comando = 'BULK INSERT FinanzasDatos FROM ''\\'+ @Name +'\InfoNaturaDatos\Finanzas\'+ @Archivo1 +'.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'')'
	EXEC sp_executesql @comando
	UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Pesos' WHERE Id=@Id

	set @comando = 'BULK INSERT FinanzasDatos FROM ''\\'+ @Name +'\InfoNaturaDatos\Finanzas\'+ @Archivo +'.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'')'
	EXEC sp_executesql @comando
	UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Pesos' WHERE Id=@Id
 exec SP_FinanzasIndices
	
END