﻿CREATE PROCEDURE [dbo].[SP_SubirArchivosTrasportadora] (@Id BigInt)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @Name nvarchar(50)
	DECLARE @Sobrante nvarchar(50)
	DECLARE @Pedidos nvarchar(50)
	DECLARE @Minuta nvarchar(50)
	SELECT @Name = Codigo FROM TABLAMAESTRA WHERE TABLA = 'DIRECCION';	

	Declare @comando nvarchar(400)
	TRUNCATE TABLE TransportadoraPlanilla
	TRUNCATE TABLE TransportadorasEnvia
	set @comando = 'BULK INSERT TransportadorasEnvia FROM ''\\'+ @Name +'\InfoNaturaDatos\Transportadoras\ENVIA.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
	EXEC sp_executesql @comando

	TRUNCATE TABLE TransportadorasInter
	set @comando = 'BULK INSERT TransportadorasInter FROM ''\\'+ @Name +'\InfoNaturaDatos\Transportadoras\INTERRAPIDISIMO.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
	EXEC sp_executesql @comando

	TRUNCATE TABLE TransportadoraServi
	set @comando = 'BULK INSERT TransportadoraServi FROM ''\\'+ @Name +'\InfoNaturaDatos\Transportadoras\SERVIENTREGA.csv'' WITH ( FIRSTROW = 2, MAXERRORS = 0, FIELDTERMINATOR = '';'',ROWTERMINATOR = ''\n'',CODEPAGE = ''ACP'')'
	EXEC sp_executesql @comando

	INSERT INTO [InfoNatura].[dbo].[TransportadoraPlanilla]
           (
           [CTRANSPORTADORA]
           ,[TRANSPORTADORA]
           ,[TIPO DE DESPACHO]
           ,[PEDIDO]
           ,[COD DE CONSULTORA]
           ,[DESTINATARIO]
           ,[DIRECCION]
           ,[CIUDAD DESTINO]
           ,[DEPARTAMENTO]
           ,[Llave]
           ,[GERENCIA]
           ,[SECTOR]
           ,[CELULAR]
           ,[TELEFONO]
           )
     SELECT
             [Cod Transportadora]
			,Transportadora
			,ModeloComercial
			,CodigoPedido
			,Persona
			,NombrePersona
			,[VIA PRINCIPAL]+[COMPLEMENTO]+[REFERENCIA]+[BARRIO]
			,MUNICIPIO
			,DEPARTAMIENTO
			,Transportadora+MUNICIPIO+DEPARTAMIENTO
			,EstructuraPadre
			,Estructura
			,Teléfono
			,Teléfono
     FROM 
			TransportadorasPedidos
     
  UPDATE [TransportadoraPlanilla] SET regional = P.REGIONAL  from [TransportadorasFletes] as R INNER JOIN [TransportadoraPlanilla] AS P ON  UPPER(R.Municipio) = UPPER(P.[CIUDAD DESTINO])   AND UPPER(R.[DEPARTAMENTO]) = UPPER(P.Departamento) AND UPPER(R.[TRANSPORTADORA])= UPPER(P.Transportadora)
     
   UPDATE [TransportadoraPlanilla] SET [FECHA DESPACHO] = Fec_Produccion  from [TransportadorasEnvia] as R INNER JOIN [TransportadoraPlanilla] AS P ON  R.Numero_Documento = P.PEDIDO
-- UPDATE [TransportadoraPlanilla] SET [FECHA DESPACHO] = Fecha_de_venta  from [TransportadorasInter] as R INNER JOIN [TransportadoraPlanilla] AS P ON  R.Contenido = P.PEDIDO
   UPDATE [TransportadoraPlanilla] SET [FECHA DESPACHO] = [FECHA ENVIO]  from [TransportadoraServi] as R INNER JOIN [TransportadoraPlanilla] AS P ON  R.[NUMERO DOCUMENTO CLIENTE] = P.PEDIDO
  
   UPDATE [TransportadoraPlanilla] SET [Cta Cont Gera] = Numero_Documento  from [TransportadorasEnvia] as R INNER JOIN [TransportadoraPlanilla] AS P ON R.Numero_Documento = P.PEDIDO and LEN(R.Numero_Documento) < = 4
   UPDATE [TransportadoraPlanilla] SET [Cta Cont Gera] = '2222'  from [TransportadorasEnvia] as R INNER JOIN [TransportadoraPlanilla] AS P ON R.Numero_Documento = P.PEDIDO and LEN(R.Numero_Documento) > = 4
 
-- UPDATE [TransportadoraPlanilla] SET [Cta Cont Gera] = Contenido  from [TransportadorasInter] as R INNER JOIN [TransportadoraPlanilla] AS P ON R.Contenido = P.PEDIDO and LEN(R.Contenido) < = 4
-- UPDATE [TransportadoraPlanilla] SET [Cta Cont Gera] = '2222'  from [TransportadorasInter] as R INNER JOIN [TransportadoraPlanilla] AS P ON R.Contenido = P.PEDIDO and LEN(R.Contenido) > = 4


-- UPDATE [TransportadoraPlanilla] SET [Cta Cont Gera] = [NUMERO DOCUMENTO CLIENTE]  from [TransportadoraServi] as R INNER JOIN [TransportadoraPlanilla] AS P ON R.[NUMERO DOCUMENTO CLIENTE] = P.PEDIDO and LEN(R.[NUMERO DOCUMENTO CLIENTE]) < = 4 
 --UPDATE [TransportadoraPlanilla] SET [Cta Cont Gera] = '2222'  from [TransportadoraServi] as R INNER JOIN [TransportadoraPlanilla] AS P ON R.[NUMERO DOCUMENTO CLIENTE] = P.PEDIDO and LEN(R.[NUMERO DOCUMENTO CLIENTE]) > = 4
  
    
   --UPDATE [TransportadoraPlanilla] SET GUIA = P.Guia  from [TransportadorasEnvia] as R INNER JOIN [TransportadoraPlanilla] AS P ON  R.Numero_Documento = P.PEDIDO  
 --UPDATE [TransportadoraPlanilla] SET GUIA = Numero_de_Guia  from [TransportadorasInter] as R INNER JOIN [TransportadoraPlanilla] AS P ON  R.Contenido = P.PEDIDO
   --UPDATE [TransportadoraPlanilla] SET GUIA = [NUMERO GUIA]  from [TransportadoraServi] as R INNER JOIN [TransportadoraPlanilla] AS P ON  R.[NUMERO DOCUMENTO CLIENTE] = P.PEDIDO
 
 
 --SELECT convert(datetime, '23-10-2016', 105) -- dd-mm-yyy
 --UPDATE [TransportadoraPlanilla] SET [FECHA PREVISTA DE ENTREGA] = DiasLaborales(convert(datetime,[FECHA DESPACHO],105),6)
  
 
END