﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetErrorInfo] (@IdE BigInt, @PasoE nvarchar(50))
AS 

SELECT  
    ERROR_NUMBER() AS ErrorNumber  
    ,ERROR_SEVERITY() AS ErrorSeverity  
    ,ERROR_STATE() AS ErrorState  
    ,ERROR_PROCEDURE() AS ErrorProcedure  
    ,ERROR_LINE() AS ErrorLine  
    ,ERROR_MESSAGE() AS ErrorMessage;  

	--UPDATE ChunkData SET ChunkName = ERROR_MESSAGE() 
	
	UPDATE [InfoNatura].[dbo].[Eventos] SET [FechaEjecucion] = GetDate(),[Estado] = 'Error ' + @PasoE, [Mensajes] = ERROR_MESSAGE() + ' ' + CAST(ERROR_LINE() AS VARCHAR(10)) WHERE Id=@IdE