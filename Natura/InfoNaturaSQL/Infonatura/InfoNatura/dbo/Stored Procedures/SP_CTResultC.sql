﻿-- =============================================
-- Author:		FM,AS
-- Create date: 2018-05-28
-- Description:	Genera resulatados Seguimiento Campañas de Categoria
-- EXEC [dbo].[SP_CTResultC] 37
-- =============================================

CREATE PROCEDURE [dbo].[SP_CTResultC] 
	@IdCTCampaign int
AS
BEGIN
BEGIN TRY
DECLARE @mensajeProceso AS VARCHAR(MAX);
DECLARE @tot_ciclos AS smallint;
DECLARE @Ciclo AS nchar(10);
DECLARE @SQL AS VARCHAR(MAX);

SET @mensajeProceso = 'Sin Procesar el SP';
SET @Ciclo = ( SELECT Ciclo FROM CTCampaign WHERE IdCTCampaign = @IdCTCampaign ) ;

truncate table [dbo].[CTCampResultC]                   

UPDATE geraCNOs SET 
      GruposRelacionadosCNOFs = Replace( GruposRelacionadosCNOFs, '-', ',' )
    , RepiqueCNOF = 0


DECLARE @GruposRelacionadosCNOFs AS VARCHAR(255)
DECLARE @Grupo AS INT
DECLARE Datos CURSOR FOR SELECT Grupo, GruposRelacionadosCNOFs 
						 FROM geraCNOs
						 WHERE GruposRelacionadosCNOFs IS NOT NULL
OPEN Datos
FETCH NEXT FROM Datos INTO @Grupo, @GruposRelacionadosCNOFs
WHILE @@fetch_status = 0
BEGIN
   SET @SQL = 'UPDATE geraCNOs SET RepiqueCNOF = ( ' +
				' SELECT COUNT(*) AS TotPes FROM (               ' +
				' SELECT                                         ' +
				'   Count(DISTINCT p.CodigoPedido) AS TotPeds,   ' +
				'   p.Persona                                    ' +
				' FROM                                           ' +
				'   geraPedidosCamp p                            ' +
				' WHERE                                          ' +
				'   p.CodigoGrupo IN( ' + @GruposRelacionadosCNOFs + ' ) AND ' +
				'   p.CicloCaptacion = ' + @Ciclo + 
				' GROUP BY                                       ' +
				'   p.CodigoGrupo,                               ' +
				'   p.Persona,                                   ' +
				'   p.CicloCaptacion                             ' +
				' HAVING                                         ' +
				'   Count(DISTINCT p.CodigoPedido) > 1           ' +
				' ) T )                                          ' +
				' WHERE Grupo = ' + CONVERT( CHAR, @Grupo )

    --PRINT @SQL
	EXEC (@SQL);  
   FETCH NEXT FROM Datos INTO @Grupo, @GruposRelacionadosCNOFs
END
CLOSE Datos
DEALLOCATE Datos  


INSERT INTO [dbo].[CTCampResultC]                                
           ([IdCTCampaign]                                       -- 01
           ,[CodGer]                                             -- 02
           ,[Gerencia]                                           -- 03
           ,[CodSector]                                          -- 04
           ,[Sector]                                             -- 05
           ,[Grupo]                                              -- 06
           ,[Nombre]                                             -- 07
           ,[Dsiponibles]                                        -- 08
           ,[NumActivas]                                         -- 09
           ,[RepiqueGrupoDirecto]                                -- 10
           ,[RepiqueCNOF]                            -- 11
           ,[Nivel]                                              -- 12
           ,[PorcActividadGer]                                   -- 13
           ,[PorcRepiqueGer] )                                   -- 14
SELECT
  g.IdCTCampaign AS IdCTCampaign,                                -- 01
  cno.CodigoEstructuraPadre AS CodGer,                           -- 02
  cno.EstructuraPadre AS Gerencia,                               -- 03
  cno.CodigoEstructura AS CodSector,                             -- 04
  cno.Estructura AS Sector,                                      -- 05
  p.CodigoGrupo AS Grupo,                                        -- 06
  cno.ResponsableGrupo AS Nombre,                                -- 07
  cno.Disponibles AS Dsiponibles,                                -- 08
  Max(cno.Activas) AS NumActivas,				                     -- 09
  Count(DISTINCT p.CodigoPedido) AS RepiqueGrupoDirecto,         -- 10
  max(cno.[RepiqueCNOF]),                                   -- 11
  cno.Nivel,                                                     -- 12
  g.PorcentajeActividad as PorcActividadGer,                     -- 13
  g.PorcentajeRepique as PorcRepiqueGer                          -- 14
FROM                                                             
  geraPedidosCamp p
  INNER JOIN geraCNOs cno ON cno.Grupo = p.CodigoGrupo
  INNER JOIN CTCampaignManagement g ON g.Codigo = cno.CodigoEstructuraPadre
WHERE
  p.CicloCaptacion = @Ciclo AND
  g.IdCTCampaign = @IdCTCampaign
GROUP BY
  cno.CodigoEstructuraPadre,
  cno.EstructuraPadre,
  cno.CodigoEstructura,
  cno.Estructura,
  p.CodigoGrupo,
  cno.ResponsableGrupo,
  cno.Disponibles,
  cno.Nivel,
  p.CicloCaptacion,
  g.IdCTCampaign,
  g.PorcentajeActividad,
  g.PorcentajeRepique


UPDATE CTCampResultC SET 
  Actividad = Convert( int, NumActivas / Convert( float, Dsiponibles ) * 100 )
, RepiqueTotal = RepiqueGrupoDirecto + RepiqueCNOF
, PorcRepiqueCalc = Convert( INT, ((RepiqueGrupoDirecto + RepiqueCNOF) / NumActivas) * 100 )
WHERE IdCTCampaign = @IdCTCampaign

UPDATE CTCampResultC SET 
  CumpleActividad = Case When Actividad >= PorcActividadGer Then 'SI' Else 'No' End
, CumpleRepique = Case When PorcRepiqueCalc >= PorcRepiqueGer Then 'SI' Else 'No' End
WHERE IdCTCampaign = @IdCTCampaign

UPDATE CTCampResultC SET 
  GanaCamp = Case When CumpleActividad = 'Si' And CumpleRepique = 'Si' Then 'Si' Else 'No' End
WHERE IdCTCampaign = @IdCTCampaign



SELECT 'OK' AS 'MensajeProceso';
END TRY
BEGIN CATCH 
	SET @mensajeProceso = 'Se ha generado un error en la Estructura del BULK-INSERT: Linea#:' + CAST (ERROR_LINE() AS VARCHAR) + ' - Mensaje de Error:' + ERROR_MESSAGE();				
		
	SELECT @mensajeProceso AS 'MensajeProceso';
END CATCH
END