﻿CREAte VIEW [dbo].[CifinPlanillaHistoricosV] as
SELECT [ID]                                        AS ID
      ,[Reporte]                                   AS Reporte
      ,[TIPO IDENT]                                AS TIPO_IDENT
      ,[Nº IDENTIFICACION]                         AS N_IDENTIFICACION
      ,[CODIGO]                                    AS CODIGO
      ,[NOMBRE TERCERO]                            AS NOMBRE_TERCERO
      ,[RESERVADO]                                 AS RESERVADO
      ,[FECHA LIMITE DE PAGO]                      AS FECHA_LIMITE_DE_PAGO
      ,[NUMERO OBLIGACION]                         AS NUMERO_OBLIGACION
      ,[CÓDIGO SUCURSAL]                           AS CODIGO_SUCURSAL
      ,[CALIDAD]                                   AS CALIDAD
      ,[CALIFICACIÓN]                              AS CALIFICACION
      ,[ESTADO DEL TITULAR]                        AS ESTADO_DEL_TITULAR
      ,[ESTADO DE OBLIGACION]                      AS ESTADO_DE_OBLIGACION
      ,[EDAD DE MORA]                              AS EDAD_DE_MORA
      ,[AÑOS EN MORA]                              AS AÑOS_EN_MORA
      ,[FECHA DE CORTE]                            AS FECHA_DE_CORTE
      ,[FECHA INICIO  ]                            AS FECHA_INICIO
      ,[FECHA TERMINACIÓN]                         AS FECHA_TERMINACION
      ,[FECHA DE EXIGIBILIDAD]                     AS FECHA_DE_EXIGIBILIDAD
      ,[FECHA DE PRESCRIPCION]                     AS FECHA_DE_PRESCRIPCION
      ,[FECHA DE PAGO]                             AS FECHA_DE_PAGO
      ,[MODO EXTINCIÓN]                            AS MODO_EXTINCION
      ,[TIPO DE PAGO]                              AS TIPO_DE_PAGO
      ,[PERIODICIDAD]                              AS PERIODICIDAD
      ,[PROBABILIDAD DE NO PAGO]                   AS PROBABILIDAD_DE_NO_PAGO
      ,[NÚMERO DE CUOTAS PAGADAS]                  AS NUMERO_DE_CUOTAS_PAGADAS
      ,[NÚMERO DE CUOTAS PACTADAS]                 AS NUMERO_DE_CUOTAS_PACTADAS
      ,[CUOTAS EN MORA]                            AS CUOTAS_EN_MORA
      ,[VALOR INICIAL]                             AS VALOR_INICIAL
      ,[VALOR DE MORA]                             AS VALOR_DE_MORA
      ,[VALOR DEL SALDO]                           AS VALOR_DEL_SALDO
      ,[VALOR DE LA CUOTA]                         AS VALOR_DE_LA_CUOTA
      ,[VALOR DE CARGO FIJO]                       AS VALOR_DE_CARGO_FIJO
      ,[LÍNEA DE CRÉDITO]                          AS LINEA_DE_CREDITO
      ,[CLÁUSULA DE PERMANENCIA]                   AS CLAUSULA_DE_PERMANENCIA
      ,[TIPO DE CONTRATO]                          AS TIPO_DE_CONTRATO
      ,[ESTADO DE CONTRATO]                        AS ESTADO_DE_CONTRATO
      ,[TERMINO O VIGENCIA DEL CONTRATO]           AS TERMINO_O_VIGENCIA_DEL_CONTRATO
      ,[NUMERO DE MESES DEL CONTRATO]              AS NUMERO_DE_MESES_DEL_CONTRATO
      ,[NATURALEZA JURÍDICA]                       AS NATURALEZA_JURÍDICA
      ,[MODALIDAD DE CRÉDITO]                      AS MODALIDAD_DE_CRÉDITO
      ,[TIPO DE MONEDA]                            AS TIPO_DE_MONEDA
      ,[TIPO DE GARANTÍA]                          AS TIPO_DE_GARANTÍA
      ,[VALOR DE LA GARANTÍA]                      AS VALOR_DE_LA_GARANTÍA
      ,[OBLIGACION REESTRUCTURADA]                 AS OBLIGACION_REESTRUCTURADA
      ,[NATURALEZA DE LA REESTRUCTURACIÓN]         AS NATURALEZA_DE_LA_REESTRUCTURACION
      ,[NÚMERO DE REESTRUCTURACIONES]              AS NUMERO_DE_REESTRUCTURACIONES
      ,[CLASE  TARJETA]                            AS CLASE__TARJETA
      ,[NO DE CHEQUES DEVUELTOS]                   AS NO_DE_CHEQUES_DEVUELTOS
      ,[CATEGORÍA SERVICIOS ]                      AS CATEGORIA_SERVICIOS
      ,[PLAZO ]                                    AS PLAZO
      ,[DÍAS DE CARTERA]                           AS DÍAS_DE_CARTERA
      ,[TIPO DE CUENTA]                            AS TIPO_DE_CUENTA
      ,[CUPO SOBREGIRO]                            AS CUPO_SOBREGIRO
      ,[DIAS AUTORIZADOS]                          AS DIAS_AUTORIZADOS
      ,[DIRECCION CASA DEL TERCERO]                AS DIRECCION_CASA_DEL_TERCERO
      ,[TELEFONO CASA DEL TERCERO]                 AS TELEFONO_CASA_DEL_TERCERO
      ,[CODIGO CIUDAD CASA DEL TERCERO]            AS CODIGO_CIUDAD_CASA_DEL_TERCERO
      ,[CIUDAD CASA DEL TERCERO]                   AS CIUDAD_CASA_DEL_TERCERO
      ,[CODIGO DEPARTAMENTO DEL TERCERO]           AS CODIGO_DEPARTAMENTO_DEL_TERCERO
      ,[DEPARTAMENTO CASA DEL TERCERO]             AS DEPARTAMENTO_CASA_DEL_TERCERO
      ,[NOMBRE EMPRESA]                            AS NOMBRE_EMPRESA
      ,[DIRECCION DE LA EMPRESA]                   AS DIRECCION_DE_LA_EMPRESA
      ,[TELEFONO DE LA EMPRESA]                    AS TELEFONO_DE_LA_EMPRESA
      ,[CODIGO CIUDAD EMPRESA DEL TERCERO]         AS CODIGO_CIUDAD_EMPRESA_DEL_TERCERO
      ,[CIUDAD EMPRESA DEL TERCERO]                AS CIUDAD_EMPRESA_DEL_TERCERO
      ,[CODIGO DEPARTAMENTO EMPRESA DEL TERCERO]   AS CODIGO_DEPARTAMENTO_EMPRESA_DEL_TERCERO
      ,[DEPARTAMENTO EMPRESA DEL TERCERO]          AS DEPARTAMENTO_EMPRESA_DEL_TERCERO
      ,[FECHA_GENERACION]                          AS FECHA_GENERACION
  FROM [InfoNatura].[dbo].[CifinPlanillaHistoricos]