﻿create view [dbo].[DespachosPlaniH] as 
SELECT Transportadora, Estiba , CAST([Fecha] AS DATE) Fecha, Sum (CantCaja) As Cajas
--YEAR(Fecha) * 10000 +  Month(Fecha) * 100 + Day(Fecha) Fecha
FROM DespachosPlanillaHistorico
Where [Estado1] = N'OK' 
  and [Estado2] = N'OK' 
  And Estado5 = ''
GROUP BY Transportadora, Estiba, CAST([Fecha] AS DATE)
--YEAR(Fecha) , Month(Fecha), Day(Fecha)