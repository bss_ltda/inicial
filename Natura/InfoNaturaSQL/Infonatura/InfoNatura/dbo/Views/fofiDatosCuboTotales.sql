﻿CREATE View [dbo].[fofiDatosCuboTotales] as (
SELECT 'Total' as Total
, sum(ActivaMeta  ) AS ActivaMeta
, sum(ActivaRealizado  ) AS ActivaRealizado
, sum(DisponibleMeta  ) AS DisponibleMeta
, sum(DisponibleRealizado  ) AS DisponibleRealizado
, sum(Inactiva3Meta  ) AS Inactiva3Meta
, sum(Inactiva3Realizado  ) AS Inactiva3Realizado
, sum(FacturacionMeta  ) AS FacturacionMeta
, sum(FacturacionRealizado  ) AS FacturacionRealizado
, sum(SaldoDisponibleMeta  ) AS SaldoDisponibleMeta
, sum(SaldoDisponibleRealizado  ) AS SaldoDisponibleRealizado
, sum(ActivaFrecuenteMeta  ) AS ActivaFrecuenteMeta
, sum(ActivaFrecuenteRealizado  ) AS ActivaFrecuenteRealizado
, sum(IndisponibleRealizado  ) AS IndisponibleRealizado
, sum(CadastroRealizado  ) AS CadastroRealizado
, sum(Inactiva6Realizado  ) AS Inactiva6Realizado
, sum(ActividadMeta  ) AS ActividadMeta
  FROM fofiDatosCubo 
  where SectorCubo NOT LIKE '%GERENCIA%'  AND  datoSector <> 700000000 )