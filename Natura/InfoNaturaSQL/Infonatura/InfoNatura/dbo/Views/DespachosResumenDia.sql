﻿create view [dbo].[DespachosResumenDia] as 
SELECT Transportadora, 
Count( distinct [EMBALAJE ] ) AS Embalajes, 
Count( distinct Estiba ) AS Estibas, 
Count( distinct Numpedido ) AS Pedidos,
SUM( CantCaja ) as Cajas 
FROM DespachosPlanilla
WHERE ( Estado1 = 'OK' and Estado2 = 'OK' and Estado5 = '' )
GROUP BY Transportadora