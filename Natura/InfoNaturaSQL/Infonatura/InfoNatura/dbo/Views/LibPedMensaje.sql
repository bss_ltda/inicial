﻿CREATE VIEW [dbo].[LibPedMensaje] AS
SELECT  'Mensaje' as Validacion, TelefonoMovil AS Telefono, RTRIM(CCNOT1)+' '+RTRIM(LibPedRetenidos.Abono) AS Mensaje
FROM RFPARAM,
LibPedRetenidos
INNER JOIN geraComPagDisponible
ON LibPedRetenidos.CodPersona = geraComPagDisponible.CodigoConsultora
WHERE CCTABL = 'LIBPEDMENSAJE'