﻿create view [dbo].[DespachosPlanillaD] as
SELECT Transportadora, Estiba , CAST([Fecha] AS DATE) Fecha, Sum (CantCaja) As Cajas
FROM DespachosPlanilla
Where ( Estado1 = 'OK' and Estado2 = 'OK' and Estado5 = '' )
GROUP BY Transportadora, Estiba, CAST([Fecha] AS DATE)