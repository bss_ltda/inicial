﻿CREATE VIEW [dbo].[DesapachosEstibas] as
SELECT Transportadora, Estiba, CONVERT (decimal(4,0), left(Estiba, 4 )) AS Estibas 
FROM DespachosPlanilla
GROUP BY Transportadora, Estiba