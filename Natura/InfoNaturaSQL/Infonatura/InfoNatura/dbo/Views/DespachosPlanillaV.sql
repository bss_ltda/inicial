﻿Create View [dbo].[DespachosPlanillaV] as
Select
  [IdUnico]                  IdUnico,
  [Id ]                      Id ,
  [EMBALAJE ]                EMBALAJE ,
  [NoCaja]                   NoCaja,
  [CantCaja]                 CantCaja,
  [Numpedido]                Numpedido,
  [CodigoCN]                 CodigoCN,
  [NombreCN]                 NombreCN,
  [Direccion]                Direccion,
  [Celular]                  Celular,
  [Telefono]                 Telefono,
  [Ciudad]                   Ciudad,
  [Departamento]             Departamento,
  [Fecha]                    Fecha,
  [NumMinuta]                NumMinuta,
  [Peso]                     Peso,
  [Transportadora]           Transportadora,
  [Estiba]                   Estiba,
  [Estado1]                  Estado1,
  [Estado2]                  Estado2,
  [Estado4]                  Estado4,
  [Estado5]                  Estado5,
  [TipoDeCaja]               TipoDeCaja,
  [PesoFinal]                PesoFinal,
  [PoliticaEntrega]          PoliticaEntrega
From
  DespachosPlanilla