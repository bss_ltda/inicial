﻿create view [dbo].[DespachosPlanillaH2] as 
SELECT Transportadora, CAST([Fecha] AS DATE) as Fecha, 
Count( distinct [EMBALAJE ] ) AS Embalajes, 
Count( distinct Estiba ) AS Estibas, 
Count( distinct Numpedido ) AS Pedidos,
ISNULL( SUM( CantCaja ), 0 ) as Cajas 
FROM DespachosPlanillaHistorico
GROUP BY Transportadora, CAST([Fecha] AS DATE)