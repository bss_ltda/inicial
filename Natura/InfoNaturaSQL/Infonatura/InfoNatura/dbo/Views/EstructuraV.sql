﻿Create View [dbo].[EstructuraV] as
Select
  gv.codGerencia as codGV,
  gv.EstructuraPadre GV,
  gv.Gerencia,
  gr.codSector codGR,
  gr.Estructura GR,
  gr.EstructuraResponsable Responsable,
  gr.CorreoElectronico CorreoElectronico
From
  biEstructuraPadre gv Inner Join
  biEstructura gr
    On gv.codGerencia = gr.codGerencia