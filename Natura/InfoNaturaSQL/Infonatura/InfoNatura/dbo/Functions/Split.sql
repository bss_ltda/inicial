﻿CREATE FUNCTION [dbo].[Split]
(
    @RowData NVARCHAR(MAX) ,
    @SplitOn NVARCHAR(5)
)
RETURNS @ReturnValue TABLE (Indice INT, Data NVARCHAR(MAX) ) AS
    BEGIN
        DECLARE @Counter INT
        SET @Counter = 1
        WHILE ( CHARINDEX(@SplitOn, @RowData) > 0 )
        BEGIN
            INSERT  INTO @ReturnValue (Indice, data )
                SELECT  Indice = @Counter, Data = LTRIM(RTRIM(SUBSTRING(@RowData, 1, CHARINDEX(@SplitOn, @RowData) - 1)))
            SET @RowData = SUBSTRING(@RowData, CHARINDEX(@SplitOn, @RowData) + 1, LEN(@RowData))
            SET @Counter = @Counter + 1
        END
        INSERT INTO @ReturnValue ( Indice, data )
        SELECT Indice = @Counter , Data = LTRIM(RTRIM(@RowData))

        RETURN
    END;