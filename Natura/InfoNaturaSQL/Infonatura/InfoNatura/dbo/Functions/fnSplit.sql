﻿CREATE FUNCTION [dbo].[fnSplit](
    @sParametroList  VARCHAR(MAX) -- List of delimited items
  , @Num INT-- delimiter that separates items
) RETURNS @ListaTemporal TABLE (item VARCHAR(MAX))
BEGIN
DECLARE @sItem VARCHAR(8000)
DECLARE @sItem1 VARCHAR(8000)
DECLARE @cDelimitador VARCHAR(80)
SET @cDelimitador = '-'
DECLARE @NUMERO INT
DECLARE @var INT
SET @NUMERO=1
declare @sParametroList1 VARCHAR(MAX)
set @sParametroList1 = @sParametroList
WHILE CHARINDEX(@cDelimitador,@sParametroList,0) <> 0
BEGIN
SELECT
@sItem=RTRIM(LTRIM(SUBSTRING(@sParametroList,1,
CHARINDEX(@cDelimitador,@sParametroList,0)-1))),
@sParametroList=RTRIM(LTRIM(SUBSTRING(@sParametroList,
CHARINDEX(@cDelimitador,@sParametroList,0)
+LEN(@cDelimitador),LEN(@sParametroList))))
IF (@sItem) ='' begin
set @Num=@Num+1
end 
IF LEN(@sItem) > 0 
SET @NUMERO=@NUMERO+1
END


if(@NUMERO>4 and @Num>1 )begin
set @var=@NUMERO-4
set @Num=@Num+@var
end


SET @NUMERO=1
WHILE CHARINDEX(@cDelimitador,@sParametroList1,0) <> 0
BEGIN
SELECT
@sItem1=RTRIM(LTRIM(SUBSTRING(@sParametroList1,1,
CHARINDEX(@cDelimitador,@sParametroList1,0)-1))),
@sParametroList1=RTRIM(LTRIM(SUBSTRING(@sParametroList1,
CHARINDEX(@cDelimitador,@sParametroList1,0)
+LEN(@cDelimitador),LEN(@sParametroList1))))

IF LEN(@sItem1) > 0 AND @NUMERO=@Num
INSERT INTO @ListaTemporal SELECT @sItem1
SET @NUMERO=@NUMERO+1
END
IF LEN(@sParametroList1) > 0 AND @NUMERO=@Num
INSERT INTO @ListaTemporal SELECT @sParametroList1 -- Coloca luego del último elemento
RETURN
END