﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[DiasLaborales]
(
    @Start datetime, @NumOfDays integer
)
RETURNS datetime
AS 
BEGIN
DECLARE @NextDay datetime
DECLARE @Counter int

IF @NumOfDays>0 
	 BEGIN
	        SET @Counter=1
	        -- Drop the time part and initialize the Return Variable
	        
	        SET @NextDay=CONVERT(datetime ,(CONVERT(char(10),@Start,102)),102 )
	        
	        WHILE @Counter <= @NumOfDays
		        BEGIN
			        SET @NextDay = dbo.fNextWorkingDay(@NextDay)
			        SET @Counter=@Counter+1
		        END
	 END
ELSE
	 BEGIN
	        SET @Counter=ABS(@NumOfDays)
	        -- Drop the time part and initialize the Return Variable
	        
	        SET @NextDay=CONVERT(datetime ,(CONVERT(char(10),@Start,102)),102 )
	        
	        WHILE @Counter>0
		        BEGIN
			        SET @NextDay = dbo.fPreviousWorkingDay(@NextDay)
			        SET @Counter=@Counter-1
		        END
	 END

RETURN @NextDay
 
END