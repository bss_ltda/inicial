﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[preparCiclo]
(
     
	 @Dato varchar(255)

)
RETURNS  varchar(255)
AS
BEGIN
    
	DECLARE @Resultado VARCHAR(255)
	
	IF CHARINDEX('/', @Dato, 3) = 3
	   
	   Return SUBSTRING(@Dato, 4, 4) + SUBSTRING(@DATO, 1, 2)

	Else 
      
	   SET @Resultado = REPLACE (REPLACE(@DATO,'-','') ,'/','') 


	IF CAST(SUBSTRING(@Resultado, 1, 4) AS int) < 2000

	   SET @Resultado = SUBSTRING(@Resultado, 3, 4) + SUBSTRING(@Resultado, 1, 2)

	   RETURN @Resultado
     
	   


END