﻿CREATE FUNCTION [dbo].[PreparaNumero] (@Dato varchar(255), @sepMiles char(1))  
RETURNS varchar(255)  
WITH EXECUTE AS CALLER  
AS  
BEGIN
	 SET @Dato = REPLACE( @Dato, '$-', '0' );
	 SET @Dato = REPLACE( @Dato, '$', '' );
	 SET @Dato = REPLACE( @Dato, '%', '' );
	 SET @Dato = REPLACE( @Dato, @sepMiles, '' );
	 WHILE @Dato LIKE '% %'  
	   BEGIN  
		  SET @Dato = REPLACE( @Dato, ' ', '' );	 
	   END;  
	 SET @Dato = REPLACE( @Dato, '-', '0' );
	 
     RETURN(@Dato);  
END;