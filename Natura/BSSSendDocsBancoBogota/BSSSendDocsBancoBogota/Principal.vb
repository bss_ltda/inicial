﻿Imports CLDatos

Module Principal

    Sub Main()
        Dim aSubParam() As String
        Dim TAREA As String = ""
        Dim BANCO As String = ""
        Dim HORASIG As String = ""
        Dim HORAACT As String = ""

        Environment.GetCommandLineArgs()
        For Each parametro In Environment.GetCommandLineArgs()
            aSubParam = Split(parametro, "=")
            Select Case UCase(aSubParam(0))
                Case "TAREA"
                    TAREA = aSubParam(1)
                Case "BANCO"
                    BANCO = aSubParam(1)
                Case "HORASIG"
                    HORASIG = aSubParam(1)
                Case "ACT"
                    HORAACT = aSubParam(1)
            End Select
        Next
        Console.WriteLine(TAREA & ", " & BANCO & ", " & HORASIG)
        SFTP(TAREA, BANCO, HORASIG, HORAACT)
    End Sub

End Module
