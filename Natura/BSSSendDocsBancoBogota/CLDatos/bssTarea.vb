﻿Public Class bssTarea
    Private datos As New ClsDatos
    Public Categoria
    Public Subcategoria
    Public Comando
    Public Programar        'Ejecuta la tarea a la hora programada
    Public Parametros
    Public tipo
    Private data

    Function EnviaTarea()
        Dim continuar As Boolean
        Dim mensajeError As String = ""

        Console.WriteLine("Abriendo Conexión para Tarea...")
        If Not datos.AbrirConexion() Then
            Console.WriteLine("Error Abriendo Conexión para Tarea " & datos.mensaje)
            EnviaTarea = datos.mensaje
        End If
        Console.WriteLine("Conexión para Tarea Abierta...")
        data = Nothing
        data = New RFTASK
        With data
            .TPRM = Parametros
            .TCATEG = Categoria
            .TSUBCAT = Subcategoria
            .TLXMSG = "PROGRAMADA"
            .TTIPO = tipo
            .TPGM = Comando
            .TFPROXIMA = Programar
        End With
        Console.WriteLine("Guardando RFTASK...")
        continuar = datos.GuardarRFTASKRow(data)
        If continuar = True Then
            Console.WriteLine("RFTASK Guardado")
        Else
            Console.WriteLine("Error Guardando RFTASK " & datos.mensaje)
            'ERROR DE GUARDADO
        End If
        EnviaTarea = continuar
    End Function

    Sub inicializa()
        Categoria = ""
        Subcategoria = ""
        Comando = ""
        Programar = ""
        Parametros = ""
        tipo = ""
    End Sub

End Class
