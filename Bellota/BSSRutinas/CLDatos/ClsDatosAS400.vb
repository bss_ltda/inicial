﻿Imports IBM.Data.DB2.iSeries
Imports System.IO
Imports System.Text

Public Class ClsDatosAS400
    Public conexionIBM As iDB2Connection
    Private adapterIBM As iDB2DataAdapter = New iDB2DataAdapter()
    Private builderIBM As iDB2CommandBuilder
    Private commandIBM As iDB2Command
    Private numFilas As Integer
    Private dt As DataTable
    Private WithEvents ds As DataSet
    Private appPath As String = Path.GetFullPath(My.Application.Info.DirectoryPath & "\Log\")
    Public mensaje As String
    Private data
    Public dat
    Public fSql As String
    Public DB2_DATEDEC As String = " ( YEAR( NOW() ) * 10000 + MONTH( NOW()  ) * 100 + DAY( NOW() ) )"
    Public DB2_TIMEDEC As String = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    Public gsKeyWords As String = ""

#Region "ACTUALIZAR"

    ''' <summary>
    ''' Actualiza la tabla BFCIM 
    ''' </summary>
    ''' <param name="datos">BFCIM</param>
    ''' <param name="tipo">0- SET IFLAG = @IFLAG WHERE INMLOT = @INMLOT,
    ''' 1- SET IFLAG = IFLAG + 2 WHERE INMLOT = @INMLOT</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 27/04/2016</remarks>
    Public Function ActualizarBFCIMRow(ByVal datos As BFCIM, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean = False
        Select Case tipo
            Case 0
                fSql = " UPDATE BFCIM SET "
                fSql &= " IFLAG = " & datos.IFLAG & "  "
                fSql &= " WHERE INMLOT ='" & datos.INMLOT & "' "
            Case 1
                fSql = " UPDATE BFCIM SET "
                fSql &= " IFLAG = IFLAG + 2 "
                fSql &= " WHERE INMLOT ='" & datos.INMLOT & "' "
        End Select

        commandIBM = New iDB2Command
        commandIBM.Connection = conexionIBM
        Try
            commandIBM.CommandText = fSql
            commandIBM.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As iDB2Exception
            mensaje = ex.Message
        Catch ex As Exception
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFCIMAS400Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo actualizar la tabla BFCIM AS400.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()

                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Actualizar BFCIM AS400"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                    .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                End With
                dat.GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla BFDESPA 
    ''' </summary>
    ''' <param name="datos">BFDESPA</param>
    ''' <param name="tipo">0- SET DFLAG = DFLAG + 2 WHERE DBATCH = @DBATCH AND DPEDID = @DPEDID AND DLINEA = @DLINEA</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 27/04/2016</remarks>
    Public Function ActualizarBFDESPARow(ByVal datos As BFDESPA, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean = False
        Select Case tipo
            Case 0
                fSql = " UPDATE BFDESPA SET "
                fSql &= " DFLAG = DFLAG + 2 "
                fSql &= " WHERE DBATCH = " & datos.DBATCH & " AND DPEDID = " & datos.DPEDID & " AND DLINEA = " & datos.DLINEA & " "
        End Select

        commandIBM = New iDB2Command
        commandIBM.Connection = conexionIBM
        Try
            commandIBM.CommandText = fSql
            commandIBM.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As iDB2Exception
            mensaje = ex.Message
        Catch ex As Exception
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFDESPAAS400Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo actualizar la tabla BFDESPA AS400.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()

                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Actualizar BFDESPA AS400"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                    .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                End With
                dat.GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

#End Region

#Region "CONSULTAR"

    ''' <summary>
    ''' Consulta la tabla BFCIM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca donde IFLAG in (1), 0- Busca donde IFLAG in (2,7)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 27/04/2016</remarks>
    Public Function ConsultarBFCIMdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing
        If conexionIBM IsNot Nothing Then
            Select Case tipoparametro
                Case 0
                    fSql = "SELECT IFLAG, INMLOT, INERROR FROM BFCIM WHERE IFLAG IN( 1 )"
                Case 1
                    fSql = "SELECT IFLAG, INMLOT, INERROR FROM BFCIM WHERE IFLAG IN( 2, 7 )"
            End Select
            adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)
            adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
            builderIBM = New iDB2CommandBuilder(adapterIBM)
            dt = New DataTable()
            ds = New DataSet()
            Try
                numFilas = adapterIBM.Fill(ds)
                If numFilas > 0 Then
                    dt = ds.Tables(0)
                Else
                    dt = Nothing
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFCIMAS400Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo consultar en la tabla BFCIM AS400.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            adapterIBM = Nothing
            builderIBM = Nothing
        Else
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFCIMAS400Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No existe conexion AS400" & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End If
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla BFDESPA con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca donde DFLAG in (1), 1- Busca donde DFLAG in (2,7)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 27/04/2016</remarks>
    Public Function ConsultarBFDESPAdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing
        If conexionIBM IsNot Nothing Then
            Select Case tipoparametro
                Case 0
                    fSql = "SELECT DFLAG, DERROR, DDESER, DBATCH, DPEDID, DLINEA FROM BFDESPA WHERE DFLAG IN( 1 )"
                Case 1
                    fSql = "SELECT DFLAG, DERROR, DDESER, DBATCH, DPEDID, DLINEA FROM BFDESPA WHERE DFLAG IN( 2, 7 )"
            End Select
            adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)
            adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
            builderIBM = New iDB2CommandBuilder(adapterIBM)
            dt = New DataTable()
            ds = New DataSet()
            Try
                numFilas = adapterIBM.Fill(ds)
                If numFilas > 0 Then
                    dt = ds.Tables(0)
                Else
                    dt = Nothing
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFDESPAAS400Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo consultar en la tabla BFDESPA AS400.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            adapterIBM = Nothing
            builderIBM = Nothing
        Else
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFDESPAAS400Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No existe conexion AS400" & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End If
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla FSO con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por SORD(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarFSOdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing
        If conexionIBM IsNot Nothing Then
            Select Case tipoparametro
                Case 0
                    fSql = "SELECT SQREQ, SQFIN FROM FSO WHERE SORD =" & parametroBusqueda(0)
            End Select
            adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)

            adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
            builderIBM = New iDB2CommandBuilder(adapterIBM)
            dt = New DataTable()
            ds = New DataSet()
            Try
                numFilas = adapterIBM.Fill(ds)
                If numFilas > 0 Then
                    dt = ds.Tables(0)
                Else
                    dt = Nothing
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\FSOAS400Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo consultar en la tabla FSO AS400.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try

            adapterIBM = Nothing
            builderIBM = Nothing
        Else
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\FSOAS400Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No existe conexion AS400" & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End If
        Return dt
    End Function

#End Region

#Region "FUNCIONES"

    Public Function AbrirConexion(ByVal cadena As String, ByRef proceso As String) As Boolean
        Dim abierta As Boolean = False
        Try
            proceso &= vbCrLf & "Validando Conexion abierta AS400"
            Console.WriteLine("Validando Conexion abierta AS400")
            If conexionIBM IsNot Nothing Then
                proceso &= vbCrLf & "Validando estado Conexion AS400"
                If conexionIBM.State = ConnectionState.Open Then
                    abierta = True
                    Console.WriteLine("Conexion AS400 Existente Abierta")
                Else
                    conexionIBM = New iDB2Connection

                    proceso &= vbCrLf & "Abriendo Conexion AS400 1"
                    Console.WriteLine("Abriendo Conexion AS400")
                    conexionIBM.ConnectionString = cadena
                    conexionIBM.Open()
                    Console.WriteLine("Conexion AS400 Abierta")
                    proceso &= vbCrLf & "Conexion AS400 Abierta"
                    abierta = True
                End If
            Else
                conexionIBM = New iDB2Connection

                proceso &= vbCrLf & "Asignando Cadena Conexion AS400" & vbCrLf & cadena
                Console.WriteLine("Abriendo Conexion AS400")
                conexionIBM.ConnectionString = cadena
                proceso &= vbCrLf & "Abriendo Conexion AS400" & vbCrLf & cadena
                conexionIBM.Open()
                Console.WriteLine("Conexion AS400 Abierta")
                proceso &= vbCrLf & "Conexion AS400 Abierta"
                abierta = True
            End If

        Catch ex3 As iDB2DCFunctionErrorException
            abierta = False
            mensaje = ex3.InnerException.ToString & vbCrLf & cadena & vbCrLf & ex3.Message
        Catch ex4 As AccessViolationException
            abierta = False
            mensaje = ex4.InnerException.ToString & vbCrLf & cadena & vbCrLf & ex4.Message
        Catch ex2 As iDB2InvalidConnectionStringException
            abierta = False
            mensaje = ex2.InnerException.ToString & vbCrLf & cadena & vbCrLf & ex2.Message
        Catch ex1 As iDB2Exception
            abierta = False
            mensaje = ex1.MessageDetails & vbCrLf & cadena & vbCrLf & ex1.Message
        Catch ex As Exception
            abierta = False
            mensaje = ex.ToString & ex.Message & vbCrLf & cadena
        End Try

        If abierta = False Then
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionAS400Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo realizar la conexión AS400.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()

            data = Nothing
            data = New BFLOG
            With data
                .USUARIO = Net.Dns.GetHostName()
                .OPERACION = "Abrir Conexión AS400"
                .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                .EVENTO = mensaje
                .TXTSQL = cadena
                .ALERT = 1
            End With
            dat.GuardarBFLOGRow(data)
        End If

        Return abierta
    End Function

    Public Function CerrarConexion() As Boolean
        Dim cerrada As Boolean = False
        Try
            conexionIBM.Close()
            cerrada = True
        Catch ex1 As iDB2Exception
            cerrada = False
            mensaje = ex1.ToString
        Catch ex As Exception
            cerrada = False
            mensaje = ex.ToString
        End Try
        Return cerrada
    End Function

    Public Sub LlamaPrograma(ByVal PGM As String, ByVal Batch As String)
        Dim err As Boolean = False
        Dim cmd As iDB2Command = New iDB2Command()
        cmd.Connection = conexionIBM
        fSql = String.Format("Call " & PGM & "('{0}')", Batch)
        cmd.CommandText = fSql
        cmd.CommandType = CommandType.Text
        Try
            cmd.ExecuteReader()
        Catch ex As iDB2Exception
            err = True
            mensaje = "No se pudo llamar al programa." & vbCrLf & ex.Message
        Catch ex1 As Exception
            err = True
            mensaje = "No se pudo llamar al programa." & vbCrLf & ex1.Message
        End Try
        If err = True Then
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ProgramaError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine(mensaje)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            data = Nothing
            data = New BFLOG
            With data
                .USUARIO = Net.Dns.GetHostName()
                .OPERACION = "Llamar Programa AS400"
                .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                .EVENTO = mensaje
                .TXTSQL = fSql
                .ALERT = 1
            End With
            dat.GuardarBFLOGRow(data)
        End If
    End Sub

#End Region

#Region "GUARDAR"

    ''' <summary>
    ''' Guarda BFCIM
    ''' </summary>
    ''' <param name="datos">BFCIM</param>
    ''' <param name="tipo">0- (IFUENTE, IBATCH, INID, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREAS, INREF, INLPGM, INPROD, INWHSE, INLOT, INLOC, INQTY, INCBY, INCDT, INCTM)</param>
    ''' <returns>true si se guarda con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function GuardarBFCIMRow(ByVal datos As BFCIM, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean = False
        Using sqlCommand As New iDB2Command()
            With sqlCommand
                Select Case tipo
                    Case 0
                        fSql = "INSERT INTO BFCIM (IFUENTE, IBATCH, INLINE, INID, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREAS, INREF, INPROD, INWHSE, INLOT, INLOC, INQTY, INCBY, INLDT, INLTM, INCDT, INCTM) VALUES (" &
                         "'" & datos.IFUENTE & "', " &
                        datos.IBATCH & ", " &
                        "'" & datos.INLINE & "', " &
                        "'" & datos.INID & "', " &
                        "'" & datos.INTRAN & "', " &
                        datos.INCTNR & ", " &
                        "'" & datos.INMFGR & "', " &
                        datos.INDATE & ", " &
                        datos.INTRN & ", " &
                        "'" & datos.INMLOT & "', " &
                        "'" & datos.INREAS & "', " &
                        datos.INREF & ", " &
                        "'" & datos.INPROD & "', " &
                        "'" & datos.INWHSE & "', " &
                        "'" & datos.INLOT & "', " &
                        "'" & datos.INLOC & "', " &
                        datos.INQTY & ", " &
                        "'" & datos.INCBY & "', " &
                        datos.INLDT & ", " &
                        datos.INLTM & ", " &
                        datos.INCDT & ", " &
                        datos.INCTM & ")"
                    Case 1
                        fSql = "INSERT INTO BFCIM (IFUENTE, IBATCH, INID, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREAS, INREF, INPROD, INWHSE, INLOT, INLOC, INQTY, INCBY, INLDT, INLTM, INCDT, INCTM) VALUES (" &
                         "'" & datos.IFUENTE & "', " &
                        datos.IBATCH & ", " &
                        "'" & datos.INID & "', " &
                        "'" & datos.INTRAN & "', " &
                        "'" & datos.INCTNR & "', " &
                        "'" & datos.INMFGR & "', " &
                        datos.INDATE & ", " &
                        datos.INTRN & ", " &
                        "'" & datos.INMLOT & "', " &
                        "'" & datos.INREAS & "', " &
                        datos.INREF & ", " &
                        "'" & datos.INPROD & "', " &
                        "'" & datos.INWHSE & "', " &
                        "'" & datos.INLOT & "', " &
                        "'" & datos.INLOC & "', " &
                        datos.INQTY & ", " &
                        "'" & datos.INCBY & "', " &
                        datos.INLDT & ", " &
                        datos.INLTM & ", " &
                        datos.INCDT & ", " &
                        datos.INCTM & ")"
                    Case 2
                        fSql = "INSERT INTO BFCIM (IFUENTE, IBATCH, INID, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREF, INPROD, INLOT, INLOC, INQTY, INCBY, INLDT, INLTM, INCDT, INCTM, INUMERO, INCOST, INWHSE, INFACTU) VALUES (" &
                        "'" & datos.IFUENTE & "', " &
                        datos.IBATCH & ", " &
                        "'" & datos.INID & "', " &
                        "'" & datos.INTRAN & "', " &
                        "'" & datos.INCTNR & "', " &
                        "'" & datos.INMFGR & "', " &
                        datos.INDATE & ", " &
                        datos.INTRN & ", " &
                        "'" & datos.INMLOT & "', " &
                        datos.INREF & ", " &
                        "'" & datos.INPROD & "', " &
                        "'" & datos.INLOT & "', " &
                        "'" & datos.INLOC & "', " &
                        datos.INQTY & ", " &
                        "'" & datos.INCBY & "', " &
                        datos.INLDT & ", " &
                        datos.INLTM & ", " &
                        datos.INCDT & ", " &
                        datos.INCTM & ", " &
                        datos.INUMERO & ", " &
                        datos.INCOST & ", " &
                        "'" & datos.INWHSE & "', " &
                        "'" & datos.INFACTU & "')"
                    Case 3
                        fSql = "INSERT INTO BFCIM (IFUENTE, INID, IBATCH, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREAS, INREF, INPROD, INWHSE, INLOT, INLOC, INQTY, INCBY, INCDT, INCTM) VALUES (" &
                        "'" & datos.IFUENTE & "', " &
                        "'" & datos.INID & "', " &
                        datos.IBATCH & ", " &
                        "'" & datos.INTRAN & "', " &
                        datos.INCTNR & ", " &
                        "'" & datos.INMFGR & "', " &
                        datos.INDATE & ", " &
                        datos.INTRN & ", " &
                        "'" & datos.INMLOT & "', " &
                        "'" & datos.INREAS & "', " &
                        datos.INREF & ", " &
                        "'" & datos.INPROD & "', " &
                        "'" & datos.INWHSE & "', " &
                        "'" & datos.INLOT & "', " &
                        "'" & datos.INLOC & "', " &
                        datos.INQTY & ", " &
                        "'" & datos.INCBY & "', " &
                        datos.INCDT & ", " &
                        datos.INCTM & ")"
                    Case 4
                        fSql = "INSERT INTO BFCIM (IFUENTE, IBATCH, INID, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREAS, INREF, INLPGM, INPROD, INWHSE, INLOT, INLOC, INQTY, INCBY, INCDT, INCTM) VALUES ("
                        fSql &= "'" & datos.IFUENTE & "', "
                        fSql &= datos.IBATCH & ", "
                        fSql &= "'" & datos.INID & "', "
                        fSql &= "'" & datos.INTRAN & "', "
                        fSql &= datos.INCTNR & ", "
                        fSql &= "'" & datos.INMFGR & "', "
                        fSql &= datos.INDATE & ", "
                        fSql &= datos.INTRN & ", "
                        fSql &= "'" & datos.INMLOT & "', "
                        fSql &= "'" & datos.INREAS & "', "
                        fSql &= datos.INREF & ", "
                        fSql &= "'" & datos.INLPGM & "', "
                        fSql &= "'" & datos.INPROD & "', "
                        fSql &= "'" & datos.INWHSE & "', "
                        fSql &= "'" & datos.INLOT & "', "
                        fSql &= "'" & datos.INLOC & "', "
                        fSql &= datos.INQTY & ", "
                        fSql &= "'" & datos.INCBY & "', "
                        fSql &= datos.INCDT & ", "
                        fSql &= datos.INCTM & ")"
                    Case 5
                        fSql = "INSERT INTO BFCIM (IFUENTE, IBATCH, INID, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREAS, INREF, INPROD, INWHSE, INLOT, INLOC, INQTY, INCBY, INCDT, INCTM) VALUES ("
                        fSql &= "'" & datos.IFUENTE & "', "
                        fSql &= datos.IBATCH & ", "
                        fSql &= "'" & datos.INID & "', "
                        fSql &= "'" & datos.INTRAN & "', "
                        fSql &= datos.INCTNR & ", "
                        fSql &= "'" & datos.INMFGR & "', "
                        fSql &= datos.INDATE & ", "
                        fSql &= datos.INTRN & ", "
                        fSql &= "'" & datos.INMLOT & "', "
                        fSql &= "'" & datos.INREAS & "', "
                        fSql &= datos.INREF & ", "
                        fSql &= "'" & datos.INPROD & "', "
                        fSql &= "'" & datos.INWHSE & "', "
                        fSql &= "'" & datos.INLOT & "', "
                        fSql &= "'" & datos.INLOC & "', "
                        fSql &= datos.INQTY & ", "
                        fSql &= "'" & datos.INCBY & "', "
                        fSql &= datos.INCDT & ", "
                        fSql &= datos.INCTM & ")"
                End Select

                .CommandText = fSql
                .Connection = conexionIBM
            End With
            Try
                sqlCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As iDB2Exception
                continuarguardando = False
                mensaje = ex.Message
            Catch ex As Exception
                continuarguardando = False
                mensaje = ex.Message
            Finally
                If continuarguardando = False Then
                    Dim sb As New StringBuilder()
                    Dim sw As StreamWriter = New StreamWriter(appPath & "\BFCIMAS400Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                    sb.AppendLine("No se pudo guardar en la tabla BFCIMAS400.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                    sb.AppendLine(Now().ToString)
                    sw.WriteLine(sb.ToString())
                    sw.Close()

                    data = Nothing
                    data = New BFLOG
                    With data
                        .USUARIO = Net.Dns.GetHostName()
                        .OPERACION = "Guardar BFCIM AS400"
                        .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                        .EVENTO = mensaje
                        .TXTSQL = fSql
                        .ALERT = 1
                    End With
                    dat.GuardarBFLOGRow(data)
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Guarda BFDESPA
    ''' </summary>
    ''' <param name="datos">BFDESPA</param>
    ''' <returns>true si se guarda con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function GuardarBFDESPARow(ByVal datos As BFDESPA) As Boolean
        Dim continuarguardando As Boolean
        Using ibmCommand As New iDB2Command()
            With ibmCommand
                fSql = "INSERT INTO BFDESPA (DBATCH, DPEDID, DLINEA, DPRODU, DCANTI, DBODEG, DLOCAL, DLOTE, DUSER, DFLAG) VALUES ("
                fSql &= "'" & datos.DBATCH & "', "
                fSql &= datos.DPEDID & ", "
                fSql &= "'" & datos.DLINEA & "', "
                fSql &= "'" & datos.DPRODU & "', "
                fSql &= datos.DCANTI & ", "
                fSql &= "'" & datos.DBODEG & "', "
                fSql &= "'" & datos.DLOCAL & "', "
                fSql &= "'" & datos.DLOTE & "', "
                fSql &= "'" & datos.DUSER & "', "
                fSql &= datos.DFLAG & ")"

                .CommandText = fSql
                .Connection = conexionIBM
            End With
            Try
                ibmCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As iDB2Exception
                continuarguardando = False
                mensaje = ex.Message
            Catch ex As Exception
                continuarguardando = False
                mensaje = ex.Message
            Finally
                If continuarguardando = False Then
                    Dim sb As New StringBuilder()
                    Dim sw As StreamWriter = New StreamWriter(appPath & "\BFDESPAAS400Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                    sb.AppendLine("No se pudo guardar en la tabla BFDESPAAS400.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                    sb.AppendLine(Now().ToString)
                    sw.WriteLine(sb.ToString())
                    sw.Close()

                    data = Nothing
                    data = New BFLOG
                    With data
                        .USUARIO = Net.Dns.GetHostName()
                        .OPERACION = "Guardar BFDESPA AS400"
                        .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                        .EVENTO = mensaje
                        .TXTSQL = fSql
                        .ALERT = 1
                        .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                    End With
                    dat.GuardarBFLOGRow(data)
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

#End Region

End Class
