﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports System.Text

Public Class ClsDatos
    Private conexion As MySqlConnection
    Private adapter As MySqlDataAdapter = New MySqlDataAdapter()
    Private builder As MySqlCommandBuilder
    Private command As MySqlCommand
    Private ds As DataSet
    Private dt As DataTable
    Public fSql As String
    Private numFilas As Integer
    Public appPath As String = Path.GetFullPath(My.Application.Info.DirectoryPath & "\Log\")
    Public mensaje As String
    Private data
    Public gsKeyWords As String = ""

#Region "ACTUALIZAR"

    ''' <summary>
    ''' Actualiza la tabla sequence_data 
    ''' </summary>
    ''' <param name="seq_name">seq_name</param>
    ''' <param name="tipo">0- SET sequence_cur_value = If((sequence_cur_value + sequence_increment) > sequence_max_value,If(sequence_cycle = True,sequence_min_value,NULL),sequence_cur_value +sequence_increment) WHERE sequence_name = @seq_name</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 15/04/2016</remarks>
    Public Function Actualizarsequence_dataRow(ByVal seq_name As String, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean

        Select Case tipo
            Case 0
                fSql = "UPDATE sequence_data SET sequence_cur_value = If((sequence_cur_value + sequence_increment) > sequence_max_value,If(sequence_cycle = True,sequence_min_value,NULL),sequence_cur_value +sequence_increment) WHERE sequence_name = '" & seq_name & "'"
        End Select

        command = New MySqlCommand()
        command.Connection = conexion
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As MySqlException
            continuarguardando = False
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\sequence_dataError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla sequence_data.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()

                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Guardar sequence_data"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla BFCIM 
    ''' </summary>
    ''' <param name="datos">BFCIM</param>
    ''' <param name="tipo">0- SET IFLAG = @IFLAG, INERROR = @INERROR WHERE INMLOT = @INMLOT,
    ''' 1- SET IFLAG = IFLAG + 2 WHERE INMLOT = @INMLOT,
    ''' 2- SET IFLAG = @IFLAG WHERE IBATCH = @IBATCH AND INTRN = @INTRN,
    ''' 3- SET IFLAG = @IFLAG WHERE IBATCH = @IBATCH</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function ActualizarBFCIMRow(ByVal datos As BFCIM, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean = False

        Select Case tipo
            Case 0
                fSql = " UPDATE BFCIM SET "
                fSql &= " IFLAG = " & datos.IFLAG & ", "
                fSql &= " INERROR = '" & datos.INERROR & "'"
                fSql &= " WHERE INMLOT ='" & datos.INMLOT & "' "
            Case 1
                fSql = " UPDATE BFCIM SET "
                fSql &= " IFLAG = IFLAG + 2 "
                fSql &= " WHERE INMLOT ='" & datos.INMLOT & "' "
            Case 2
                fSql = " UPDATE BFCIM SET "
                fSql &= " IFLAG = " & datos.IFLAG
                fSql &= " WHERE IBATCH =" & datos.IBATCH
                fSql &= " AND INTRN = " & datos.INTRN
            Case 3
                fSql = " UPDATE BFCIM SET "
                fSql &= " IFLAG = " & datos.IFLAG
                fSql &= " WHERE IBATCH =" & datos.IBATCH
        End Select

        command = New MySqlCommand
        If Not conexion.State = ConnectionState.Open Then
            data = Nothing
            data = New BFLOG
            With data
                .USUARIO = Net.Dns.GetHostName()
                .OPERACION = "Actualizar BFCIM"
                .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                .EVENTO = "Conexion Cerrada"
                .TXTSQL = fSql
                .ALERT = 1
                .LKEY = " BATCH=" & datos.IBATCH & " IMLOT=" & datos.INMLOT
            End With
            GuardarBFLOGRow(data)
            Return continuarguardando
        End If
        command.Connection = conexion

        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As MySqlException
            continuarguardando = False
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFCIMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo actualizar la tabla BFCIM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()

                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Actualizar BFCIM"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                    .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                End With
                GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla BFCIMWS
    ''' </summary>
    ''' <param name="datos">BFCIMWS</param>
    ''' <param name="tipo">0- SET IFLAG2 = @IFLAG2 WHERE IBATCH = @IBATCH AND INTRN = @INTRN,
    ''' 1- SET IFLAG2 = @IFLAG2 WHERE IBATCH = @IBATCH</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function ActualizarBFCIMWSRow(ByVal datos As BFCIMWS, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean = False
        Select Case tipo
            Case 0
                fSql = " UPDATE BFCIMWS SET "
                fSql &= " IFLAG2 = " & datos.IFLAG2
                fSql &= " WHERE IBATCH =" & datos.IBATCH
                fSql &= " AND INTRN = " & datos.INTRN
            Case 1
                fSql = " UPDATE BFCIMWS SET "
                fSql &= " IFLAG2 = " & datos.IFLAG2
                fSql &= " WHERE IBATCH =" & datos.IBATCH
                fSql &= " AND IFLAG2 = 1"
        End Select

        command = New MySqlCommand
        command.Connection = conexion
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As MySqlException
            mensaje = ex.Message
        Catch ex As Exception
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFCIMWSError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo actualizar la tabla BFCIMWS.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()

                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Actualizar BFCIMWS"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                    .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                End With
                GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla BFDESPAWS
    ''' </summary>
    ''' <param name="datos">BFDESPAWS</param>
    ''' <param name="tipo">0- SET DFLAG2 = @DFLAG2 WHERE DBATCH = @DBATCH AND DPEDID = @DPEDID AND DLINEA = @DLINEA,
    ''' 1- SET DFLAG2 = @DFLAG2 WHERE DBATCH = @DBATCH</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function ActualizarBFDESPAWSRow(ByVal datos As BFDESPAWS, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean
        Select Case tipo
            Case 0
                fSql = " UPDATE BFDESPAWS SET "
                fSql &= " DFLAG2 = " & datos.DFLAG2
                fSql &= " WHERE DBATCH =" & datos.DBATCH
                fSql &= " AND DPEDID = " & datos.DPEDID
                fSql &= " AND DLINEA =" & datos.DLINEA
            Case 1
                fSql = " UPDATE BFDESPAWS SET "
                fSql &= " DFLAG2 = " & datos.DFLAG2
                fSql &= " WHERE DBATCH =" & datos.DBATCH
        End Select

        command = New MySqlCommand
        command.Connection = conexion
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As MySqlException
            continuarguardando = False
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFDESPAWSError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo actualizar la tabla BFDESPAWS.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()

                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Actualizar BFDESPAWS"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                    .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                End With
                GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla BFDESPA 
    ''' </summary>
    ''' <param name="datos">BFDESPA</param>
    ''' <param name="tipo">0- SET DFLAG = @DFLAG, DERROR = @DERROR, DDESER = @DDESER WHERE DBATCH = @DBATCH AND DPEDID = @DPEDID AND DLINEA = @DLINEA </param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function ActualizarBFDESPARow(ByVal datos As BFDESPA, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean
        Select Case tipo
            Case 0
                fSql = " UPDATE BFDESPA SET "
                fSql &= " DFLAG = " & datos.DFLAG & ", "
                fSql &= " DERROR = " & datos.DERROR & ", "
                fSql &= " DDESER = '" & datos.DDESER & "'"
                fSql &= " WHERE DBATCH = " & datos.DBATCH & " AND DPEDID = " & datos.DPEDID & " AND DLINEA = " & datos.DLINEA & " "
        End Select

        command = New MySqlCommand
        command.Connection = conexion
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As MySqlException
            continuarguardando = False
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFDESPAError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo actualizar la tabla BFDESPA.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()

                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Actualizar BFDESPA"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                    .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                End With
                GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla BFPARAM
    ''' </summary>
    ''' <param name="datos">BFPARAM</param>
    ''' <param name="tipo">0- SET CCNOT1 = @CCNOT1, CCMNDT = @CCMNDT WHERE CCNUM = @CCNUM,
    ''' 1- SET CCNOT1 = @CCNOT1, CCENDT = @CCENDT WHERE CCTABL = @CCTABL AND CCCODE = @CCCODE AND CCCODE2 = @CCCODE2,
    ''' 2- SET CCUDTS = @CCUDTS, CCDESC = @CCDESC, CCNOT2 = @CCNOT2, CCNOTE = @CCNOTE, CCENDT = @CCENDT WHERE CCTABL = @CCTABL AND CCCODE = @CCCODE,
    ''' 3- SET CCUDTS = @CCUDTS, CCMNDT = @CCMNDT WHERE CCTABL = @CCTABL AND CCCODE = @CCCODE,
    ''' 4- SET CCUDTS = @CCUDTS, CCNOT1 = @CCNOT1 WHERE CCTABL = @CCTABL AND CCCODE = @CCCODE,
    ''' 5- SET CCUDTS = @CCUDTS, CCUDV1 = @CCUDV1, CCNOT1 = @CCNOT1 WHERE CCTABL = @CCTABL AND CCCODE = @CCCODE,
    ''' 6- SET CCUDTS = @CCUDTS WHERE CCTABL = @CCTABL AND CCCODE = @CCCODE, 
    ''' 7- SET CCUDC2 = @CCUDC2, CCMNDT = NOW() WHERE CCNUM = @CCNUM</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function ActualizarBFPARAMRow(ByVal datos As BFPARAM, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean
        Select Case tipo
            Case 0
                fSql = "UPDATE BFPARAM SET CCNOT1 = '" & datos.CCNOT1 & "'"
                fSql &= ", CCMNDT = NOW()"     '//Z     CCENDT
                fSql &= " WHERE CCNUM = " & datos.CCNUM
            Case 1
                'fSql = "UPDATE BFPARAM SET CCNOT1 = '" & datos.CCNOT1 & "'"
                fSql = "UPDATE BFPARAM SET "
                fSql &= " CCENDT = '" & datos.CCENDT & "' "
                fSql &= " WHERE CCTABL = '" & datos.CCTABL & "' "
                fSql &= " AND CCCODE = '" & datos.CCCODE & "' "
                fSql &= " AND CCCODE2 = '" & datos.CCCODE2 & "' "
            Case 2
                fSql = " UPDATE BFPARAM SET "
                fSql &= " CCUDTS = '" & datos.CCUDTS & "' "
                fSql &= ", CCDESC = '" & datos.CCDESC & "' "
                fSql &= ", CCNOT2 = '" & datos.CCNOT2 & "' "
                fSql &= ", CCNOTE = '" & datos.CCNOTE & "' "
                fSql &= ", CCENDT = '" & datos.CCENDT & "' "
                fSql &= " WHERE CCTABL = '" & datos.CCTABL & "' AND CCCODE = '" & datos.CCCODE & "'"
            Case 3
                fSql = " UPDATE BFPARAM SET "
                fSql &= " CCUDTS = '" & datos.CCUDTS & "' "
                fSql &= ", CCMNDT = NOW()"
                fSql &= " WHERE CCTABL = '" & datos.CCTABL & "' AND CCCODE = '" & datos.CCCODE & "'"
            Case 4
                fSql = " UPDATE BFPARAM SET "
                fSql &= " CCUDTS = '" & datos.CCUDTS & "'"
                fSql &= ", CCNOT1    ='" & datos.CCNOT1 & "'"
                fSql &= " WHERE CCTABL = '" & datos.CCTABL & "' AND CCCODE = '" & datos.CCCODE & "'"
            Case 5
                fSql = " UPDATE BFPARAM SET "
                fSql &= " CCUDTS = '" & datos.CCUDTS & "'"
                fSql &= ", CCUDV1    = " & datos.CCUDV1 & " "
                fSql &= ", CCNOT1    ='" & datos.CCNOT1 & "' "
                fSql &= " WHERE CCTABL = '" & datos.CCTABL & "' AND CCCODE = '" & datos.CCCODE & "'"
            Case 6
                fSql = " UPDATE BFPARAM SET "
                fSql &= " CCUDTS = '" & datos.CCUDTS & "'"
                fSql &= " WHERE CCTABL = '" & datos.CCTABL & "' AND CCCODE = '" & datos.CCCODE & "'"
            Case 7
                fSql = " UPDATE BFPARAM SET "
                fSql &= " CCUDC2 = '" & datos.CCUDC2 & "', "
                fSql &= " CCNOT1 = '" & datos.CCNOT1 & "', "
                fSql &= " CCMNDT = NOW()"
                fSql &= " WHERE CCNUM = " & datos.CCNUM
            Case 8
                fSql = " UPDATE BFPARAM SET "
                fSql &= " CCUDC2 = '" & datos.CCUDC2 & "', "
                fSql &= " CCNOT1 = '" & datos.CCNOT1 & "', "
                fSql &= " CCMNDT = NOW(), "
                fSql &= " CCENDT = NOW()"
                fSql &= " WHERE CCNUM = " & datos.CCNUM
        End Select

        command = New MySqlCommand
        command.Connection = conexion
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As MySqlException
            continuarguardando = False
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFPARAMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo actualizar la tabla BFPARAM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()

                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Actualizar BFPARAM"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                    .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                End With
                GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla BFREPCONC 
    ''' </summary>
    ''' <param name="datos">BFREPCONC</param>
    ''' <param name="tipo">0- SET NUMREGISTO = NUMREGISTO, PROCESADO = 1 WHERE CNUMCON = CNUMCON AND NUMREGISTO = 0</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 15/04/2016</remarks>
    Public Function ActualizarBFREPCONCRow(ByVal datos As BFREPCONC, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean
        Select Case tipo
            Case 0
                fSql = "UPDATE BFREPCONC"
                fSql &= " SET NUMREGISTO = " & datos.NUMREGISTO & ", PROCESADO = 1 "
                fSql &= " WHERE CNUMCON = " & datos.CNUMCON & " AND NUMREGISTO = 0 "
        End Select

        command = New MySqlCommand()
        command.Connection = conexion
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex1 As MySqlException
            continuarguardando = False
            Dim sb As New StringBuilder()
            mensaje = ex1.Message
        Catch ex As Exception
            continuarguardando = False
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFREPCONCError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo actualizar en la tabla BFREPCONC.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()

                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Actualizar BFREPCONC"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarBFLOGRow(data)
            End If

        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla BFREPCONC D
    ''' </summary>
    ''' <param name="datos">BFREPCONCD</param>
    ''' <param name="tipo">0- SET NUMREGISTO = NUMREGISTO, PROCESADO = 1 WHERE CNUMCON = CNUMCON AND NUMREGISTO = 0</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 15/04/2016</remarks>
    Public Function ActualizarBFREPCONCDRow(ByVal datos As BFREPCONCD, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean
        Select Case tipo
            Case 0
                fSql = "UPDATE BFREPCONCD  SET NUMREGISTO = " & datos.NUMREGISTO & ", PROCESADO = 1 where CNUMREP = " & datos.CNUMREP & " AND CNUMCON = " & datos.CNUMCON & " AND NUMREGISTO = 0"
        End Select

        command = New MySqlCommand()
        command.Connection = conexion
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As MySqlException
            continuarguardando = False
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFREPCONCDError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla BFREPCONCD.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()

                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Actualizar BFREPCONCD"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarBFLOGRow(data)
            End If

        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla BFREP
    ''' </summary>
    ''' <param name="datos">BFREP</param>
    ''' <param name="tipo">0- SET TQRECB = @TQRECB, TQREO = @TQREO WHERE TORD = @TORD,
    ''' 1- SET TQREQACEB = TQREQACEB + @TQREQACEB WHERE TNUMREP = @TNUMREP,| 
    ''' 2- SET TQREDB = TQREDB +  @TQREDB WHERE TNUMREP = @TNUMREP,| 
    ''' 3- SET TRPODACEB = TRPODACEB + @TRPODACEB WHERE TNUMREP = @TNUMREP,| 
    ''' 4- SET TQREC = TQREC + @TQREC WHERE TNUMREP = @TNUMREP</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function ActualizarBFREPRow(ByVal datos As BFREP, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean
        Select Case tipo
            Case 0
                fSql = "UPDATE BFREP SET TQRECB = " & datos.TQRECB
                fSql &= ", TQREO = " & datos.TQREO
                fSql &= " WHERE TORD = " & datos.TORD
            Case 1
                fSql = "UPDATE BFREP SET TQREQACEB = TQREQACEB + " & datos.TQREQACEB & " WHERE TNUMREP = " & datos.TNUMREP
            Case 2
                fSql = "UPDATE BFREP SET TQREDB = TQREDB +  " & datos.TQREDB & " WHERE TNUMREP = " & datos.TNUMREP
            Case 3
                fSql = "UPDATE BFREP SET TRPODACEB = TRPODACEB + " & datos.TRPODACEB & " WHERE TNUMREP = " & datos.TNUMREP
            Case 4
                fSql = "UPDATE BFREP SET TQREC = TQREC + " & datos.TQREC & " WHERE TNUMREP = " & datos.TNUMREP
        End Select

        command = New MySqlCommand
        command.Connection = conexion
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As MySqlException
            continuarguardando = False
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFREPError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo actualizar la tabla BFREP.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()

                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Actualizar BFREP"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                    .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                End With
                GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla BFREPD
    ''' </summary>
    ''' <param name="datos">BFREPD</param>
    ''' <param name="tipo">0- SET TQREC = TQREC + @TQREC WHERE TNUMREP = @TNUMREP</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function ActualizarBFREPDRow(ByVal datos As BFREPD, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean
        Select Case tipo
            Case 0
                fSql = "UPDATE BFREPD SET TQREC = TQREC + " & datos.TQREC & " WHERE TNUMREP = " & datos.TNUMREP
        End Select

        command = New MySqlCommand
        command.Connection = conexion
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As MySqlException
            continuarguardando = False
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFREPDError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo actualizar la tabla BFREPD.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()

                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Actualizar BFREPD"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                    .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                End With
                GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla BFREPACE
    ''' </summary>
    ''' <param name="datos">BFREPACE</param>
    ''' <param name="tipo">0- SET CENVIADO = @CENVIADO WHERE WRKID = @WRKID</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function ActualizarBFREPACERow(ByVal datos As BFREPACE, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean
        Select Case tipo
            Case 0
                fSql = "UPDATE BFREPACE SET CENVIADO = " & datos.CENVIADO & " WHERE WRKID = " & datos.WRKID
        End Select

        command = New MySqlCommand
        command.Connection = conexion
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As MySqlException
            continuarguardando = False
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFREPACEError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo actualizar la tabla BFREPACE.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()

                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Actualizar BFREPACE"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                    .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                End With
                GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla BFREPTURRJ
    ''' </summary>
    ''' <param name="datos">BFREPTURRJ</param>
    ''' <param name="tipo">0- SET RENVIADO = @RENVIADO WHERE RWRKID = @RWRKID</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function ActualizarBFREPTURRJRow(ByVal datos As BFREPTURRJ, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean
        Select Case tipo
            Case 0
                fSql = "UPDATE BFREPTURRJ SET RENVIADO = " & datos.RENVIADO & " WHERE RWRKID = " & datos.RWRKID
        End Select

        command = New MySqlCommand
        command.Connection = conexion
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As MySqlException
            continuarguardando = False
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFREPTURRJError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo actualizar la tabla BFREPTURRJ.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()

                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Actualizar BFREPTURRJ"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                    .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                End With
                GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla BFTASK 
    ''' </summary>
    ''' <param name="datos">BFREPCONC</param>
    ''' <param name="tipo">0- SET STS2 = @STS2, TMSG = @TMSG WHERE TNUMREG = @TNUMREG</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 15/04/2016</remarks>
    Public Function ActualizarBFTASKRow(ByVal datos As BFTASK, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean
        Select Case tipo
            Case 0
                fSql = "UPDATE BFTASK"
                fSql &= " SET STS2 = " & datos.STS2 & ", TMSG = '" & datos.TMSG & "'"
                fSql &= " WHERE TNUMREG = " & datos.TNUMREG
        End Select

        command = New MySqlCommand()
        command.Connection = conexion
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As MySqlException
            continuarguardando = False
            Dim sb As New StringBuilder()
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFREPCONCError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla BFREPCONC.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()

                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Actualizar BFREPCONC"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarBFLOGRow(data)
            End If

        End Try
        Return continuarguardando
    End Function

#End Region

#Region "CONSULTAR"

    ''' <summary>
    ''' Consulta la tabla BFCIM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca donde IFLAG in (1,2),
    ''' 1- Busca por IFLAG(0) e IFUENTE(1),
    ''' 2- Busca por IFLAG(0) agrupado por IBATCH,
    ''' 3- Busca por IFLAG(0) e IFUENTE(1) agrupado por IBATCH</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarBFCIMdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing
        If conexion IsNot Nothing Then
            Select Case tipoparametro
                Case 0
                    fSql = "SELECT IFLAG, INMLOT FROM BFCIM WHERE IFLAG IN( 1, 2 )"
                Case 1
                    fSql = "SELECT * FROM BFCIM WHERE IFLAG = " & parametroBusqueda(0) & " AND IFUENTE = '" & parametroBusqueda(1) & "'"
                Case 2
                    fSql = "SELECT * FROM BFCIM WHERE IFLAG = " & parametroBusqueda(0) & " GROUP BY IBATCH"
                Case 3
                    fSql = "SELECT * FROM BFCIM WHERE IFLAG = " & parametroBusqueda(0) & " AND IFUENTE = '" & parametroBusqueda(1) & "' GROUP BY IBATCH"
            End Select
            adapter = New MySqlDataAdapter(fSql, conexion)
            adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
            builder = New MySqlCommandBuilder(adapter)
            dt = New DataTable()
            ds = New DataSet()
            Try
                numFilas = adapter.Fill(ds)
                If numFilas > 0 Then
                    dt = ds.Tables(0)
                Else
                    dt = Nothing
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFCIMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo consultar en la tabla BFCIM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            adapter = Nothing
            builder = Nothing
        Else
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFCIMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No existe conexión" & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End If
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla BFCIMWS con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca donde IFLAG2(0),
    ''' 1- Busca donde IFLAG2(0) agrupado por IBATCH</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarBFCIMWSdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing
        If conexion IsNot Nothing Then
            Select Case tipoparametro
                Case 0
                    fSql = "SELECT * FROM BFCIMWS WHERE IFLAG2 = " & parametroBusqueda(0)
                Case 1
                    fSql = "SELECT * FROM BFCIMWS WHERE IFLAG2 = " & parametroBusqueda(0) & " GROUP BY IBATCH"
            End Select
            adapter = New MySqlDataAdapter(fSql, conexion)

            adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
            builder = New MySqlCommandBuilder(adapter)
            dt = New DataTable()
            ds = New DataSet()
            Try
                numFilas = adapter.Fill(ds)
                If numFilas > 0 Then
                    dt = ds.Tables(0)
                Else
                    dt = Nothing
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFCIMWSError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo consultar en la tabla BFCIMWS.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            adapter = Nothing
            builder = Nothing
        Else
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFCIMWSError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No existe conexión" & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End If
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla BFDESPAWS con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por DFLAG2(0),
    ''' 1- Busca por DFLAG2(0) agrupado por DBATCH</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarBFDESPAWSdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing
        If conexion IsNot Nothing Then
            Select Case tipoparametro
                Case 0
                    fSql = "SELECT * FROM BFDESPAWS WHERE DFLAG2 = " & parametroBusqueda(0)
                Case 1
                    fSql = "SELECT * FROM BFDESPAWS WHERE DFLAG2 = " & parametroBusqueda(0) & " GROUP BY DBATCH"
            End Select
            adapter = New MySqlDataAdapter(fSql, conexion)

            adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
            builder = New MySqlCommandBuilder(adapter)
            dt = New DataTable()
            ds = New DataSet()
            Try
                numFilas = adapter.Fill(ds)
                If numFilas > 0 Then
                    dt = ds.Tables(0)
                Else
                    dt = Nothing
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFDESPAWSError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo consultar en la tabla BFDESPAWS.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            adapter = Nothing
            builder = Nothing
        Else
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFDESPAWSError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No existe conexión" & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End If
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla BFPARAM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por CCTABL = 'LXLONG' y CCCODE(0), 
    ''' 1- Busca por CCCODE(0), 
    ''' 2- Busca por CCTABL(0) y CCCODE(1),
    ''' 3- Busca por CCTABL(0) y CCCODE(%1%)
    ''' 4- Busca por CCTABL(0) - CCCODE2(1) y CCUDC2(2)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 15/04/2016</remarks>
    Public Function ConsultarBFPARAMdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing
        If conexion IsNot Nothing Then
            Select Case tipoparametro
                Case 0
                    fSql = " SELECT CCDESC FROM BFPARAM WHERE CCTABL = '" & parametroBusqueda(0) & "' AND CCCODE2 = '" & parametroBusqueda(1) & "' "
                Case 1
                    fSql = "SELECT * FROM BFPARAM WHERE CCTABL = '" & parametroBusqueda(0) & "' AND CCCODE = '" & parametroBusqueda(1) & "' AND CCCODE2 = '" & parametroBusqueda(2) & "' "
                Case 2
                    fSql = "SELECT * FROM BFPARAM WHERE CCTABL = '" & parametroBusqueda(0) & "' AND CCCODE = '" & parametroBusqueda(1) & "' "
                Case 3
                    fSql = "SELECT CCDESC FROM BFPARAM WHERE CCTABL = '" & parametroBusqueda(0) & "' AND CCCODE = '" & parametroBusqueda(1) & "' "
                Case 4
                    fSql = "SELECT * FROM BFPARAM WHERE CCTABL = '" & parametroBusqueda(0) & "' AND CCCODE LIKE '%" & parametroBusqueda(1) & "%'"
                Case 5
                    fSql = "SELECT * FROM BFPARAM WHERE CCTABL = '" & parametroBusqueda(0) & "' AND CCCODE2 = '" & parametroBusqueda(1) & "' AND CCUDC2 = '" & parametroBusqueda(2) & "'"
            End Select
            adapter = New MySqlDataAdapter(fSql, conexion)
            adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
            builder = New MySqlCommandBuilder(adapter)
            dt = New DataTable()
            ds = New DataSet()
            Try
                numFilas = adapter.Fill(ds)
                If numFilas > 0 Then
                    dt = ds.Tables(0)
                Else
                    dt = Nothing
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFPARAMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo consultar en la tabla BFPARAM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            adapter = Nothing
            builder = Nothing
        Else
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFPARAMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No existe conexión" & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End If
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla BFREPCONC con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- CNUMREP(0) y PROCESADO=0 </param>    
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 15/04/2016</remarks>
    Public Function ConsultarBFREPCONCdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing
        If conexion IsNot Nothing Then
            Select Case tipoparametro
                Case 0
                    fSql = "SELECT * FROM BFREPCONC WHERE CNUMREP = " & parametroBusqueda(0) & " AND PROCESADO = 0"
            End Select
            adapter = New MySqlDataAdapter(fSql, conexion)

            adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
            builder = New MySqlCommandBuilder(adapter)
            dt = New DataTable()
            ds = New DataSet()
            Try
                numFilas = adapter.Fill(ds)
                If numFilas > 0 Then
                    dt = ds.Tables(0)
                Else
                    dt = Nothing
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFREPCONCError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo consultar en la tabla BFREPCONC.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            adapter = Nothing
            builder = Nothing
        Else
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFREPCONCError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No existe conexión" & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End If
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla BFREPCONCD con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- CNUMREP(0) y PROCESADO=0 </param>    
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 15/04/2016</remarks>
    Public Function ConsultarBFREPCONCDdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing
        If conexion IsNot Nothing Then
            Select Case tipoparametro
                Case 0
                    fSql = "SELECT * FROM BFREPCONCD WHERE CNUMREP = " & parametroBusqueda(0) & " AND PROCESADO = 0"
            End Select
            adapter = New MySqlDataAdapter(fSql, conexion)

            adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
            builder = New MySqlCommandBuilder(adapter)
            dt = New DataTable()
            ds = New DataSet()
            Try
                numFilas = adapter.Fill(ds)
                If numFilas > 0 Then
                    dt = ds.Tables(0)
                Else
                    dt = Nothing
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFREPCONCDError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo consultar en la tabla BFREPCONCD.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            adapter = Nothing
            builder = Nothing
        Else
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFREPCONCDError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No existe conexión" & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End If
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla BFREP con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por TSTS != 2 agrupado por TORD,
    ''' 1- Busca por TORD(0),
    ''' 2- Busca por RENVIADO = 0,
    ''' 3- Busca donde TQREP != TQREC</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarBFREPdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing
        If conexion IsNot Nothing Then
            Select Case tipoparametro
                Case 0
                    fSql = "SELECT TORD FROM BFREP WHERE TSTS <> 2 GROUP BY TORD "
                Case 1
                    fSql = "SELECT * FROM BFREP INNER JOIN BFREPP ON TPROD = PPROD WHERE TORD ='" & parametroBusqueda(0) & "'"
                Case 2
                    fSql = "SELECT * FROM BFREP INNER JOIN BFREPP ON TPROD = PPROD INNER JOIN BFREPTURRJ ON TNUMREP = RNUMREP WHERE RENVIADO = 0"
                Case 3
                    fSql = "SELECT * FROM BFREP INNER JOIN BFREPP ON TPROD = PPROD WHERE TQREP <> TQREC "
            End Select
            adapter = New MySqlDataAdapter(fSql, conexion)

            adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
            builder = New MySqlCommandBuilder(adapter)
            dt = New DataTable()
            ds = New DataSet()
            Try
                numFilas = adapter.Fill(ds)
                If numFilas > 0 Then
                    dt = ds.Tables(0)
                Else
                    dt = Nothing
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFREPError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo consultar en la tabla BFREP.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            adapter = Nothing
            builder = Nothing
        Else
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFREPError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No existe conexión" & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End If
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla BFREPD con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca donde TSTS != 2 agrupado por TORD,
    ''' 1- Busca donde TQREP mayor TQREC</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarBFREPDdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing
        If conexion IsNot Nothing Then
            Select Case tipoparametro
                Case 0
                    fSql = "SELECT TORD FROM BFREPD WHERE TSTS <> 2 GROUP BY TORD "
                Case 1
                    fSql = "SELECT * FROM BFREPD INNER JOIN BFREPP ON TPROD = PPROD WHERE TQREP <> TQREC"
            End Select
            adapter = New MySqlDataAdapter(fSql, conexion)

            adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
            builder = New MySqlCommandBuilder(adapter)
            dt = New DataTable()
            ds = New DataSet()
            Try
                numFilas = adapter.Fill(ds)
                If numFilas > 0 Then
                    dt = ds.Tables(0)
                Else
                    dt = Nothing
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFREPDError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo consultar en la tabla BFREPD.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            adapter = Nothing
            builder = Nothing
        Else
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFREPDError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No existe conexión" & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End If

        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla BFREPACE con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca donde CENVIADO = 0 y CTIPO(0),
    ''' 1- Busca por CORDEN(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarBFREPACEdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing
        If conexion IsNot Nothing Then
            Select Case tipoparametro
                Case 0
                    fSql = "SELECT * FROM BFREPACE INNER JOIN BFREPP ON CPRODPT = PPROD WHERE CENVIADO = 0 AND CTIPO = '" & parametroBusqueda(0) & "'"
                Case 1
                    fSql = "SELECT * FROM BFREPACE INNER JOIN BFREPP ON CPRODPT  = PPROD WHERE CORDEN =" & parametroBusqueda(0)
            End Select
            adapter = New MySqlDataAdapter(fSql, conexion)

            adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
            builder = New MySqlCommandBuilder(adapter)
            dt = New DataTable()
            ds = New DataSet()
            Try
                numFilas = adapter.Fill(ds)
                If numFilas > 0 Then
                    dt = ds.Tables(0)
                Else
                    dt = Nothing
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFREPACEError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo consultar en la tabla BFREPACE.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            adapter = Nothing
            builder = Nothing
        Else
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFREPACEError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No existe conexión" & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End If
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla CIC con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por ICPROD(0) e ICFAC = IC</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarCICdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing
        If conexion IsNot Nothing Then
            Select Case tipoparametro
                Case 0
                    fSql = "SELECT * FROM CIC WHERE ICPROD = '" & parametroBusqueda(0) & "' AND ICFAC = 'IC'"
            End Select
            adapter = New MySqlDataAdapter(fSql, conexion)

            adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
            builder = New MySqlCommandBuilder(adapter)
            dt = New DataTable()
            ds = New DataSet()
            Try
                numFilas = adapter.Fill(ds)
                If numFilas > 0 Then
                    dt = ds.Tables(0)
                Else
                    dt = Nothing
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\CICError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo consultar en la tabla CIC.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            adapter = Nothing
            builder = Nothing
        Else
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\CICError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No existe conexión" & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End If
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla sequence_data con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por sequence_name(0) devuelve sequence_cur_value, 1- Busca por sequence_name(0) devuelve sequence_max_value</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 15/04/2016</remarks>
    Public Function Consultarsequence_datadt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing
        If conexion IsNot Nothing Then
            Select Case tipoparametro
                Case 0
                    fSql = "SELECT sequence_cur_value FROM sequence_data WHERE sequence_name = '" & parametroBusqueda(0) & "'"
                Case 1
                    fSql = "SELECT sequence_max_value FROM SEQUENCE_DATA WHERE sequence_name = '" & parametroBusqueda(0) & "'"
            End Select
            adapter = New MySqlDataAdapter(fSql, conexion)
            adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
            builder = New MySqlCommandBuilder(adapter)
            dt = New DataTable()
            ds = New DataSet()
            Try
                numFilas = adapter.Fill(ds)
                If numFilas > 0 Then
                    dt = ds.Tables(0)
                Else
                    dt = Nothing
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\SEQUENCE_DATAError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo consultar en la tabla SEQUENCE_DATA.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try
            adapter = Nothing
            builder = Nothing
        Else
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\SEQUENCE_DATAError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No existe conexión" & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End If
        Return dt
    End Function

#End Region

#Region "FUNCIONES"

    ''' <summary>
    ''' Arma cadena de conexion de AS400
    ''' </summary>
    ''' <returns>String con cadena</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 29-04-2016</remarks>
    Public Function CadenaConexionAS400() As String
        Dim cadenaas400 As String = "DataSource={SERVER};UserID={USER};Password={PASSWORD};LibraryList={LIBRARYLIST};SchemaSearchList={LIBRARYLIST};Naming=System"
        'Dim cadenaas400 As String = "Data Source={SERVER};Password={PASSWORD};User ID={USER};LibraryList={LIBRARYLIST};DefaultCollection={DEFAULT}"

        Dim parametro(1) As String
        Dim dtBFPARAM As DataTable

        parametro(0) = "LXLONG"
        parametro(1) = "AS400"
        dtBFPARAM = ConsultarBFPARAMdt(parametro, 4)
        If dtBFPARAM IsNot Nothing Then
            For Each row As DataRow In dtBFPARAM.Rows
                If row("CCCODE") = "AS400_SERVER" Then
                    If My.Settings.PRUEBAS = "SI" Then
                        cadenaas400 = cadenaas400.Replace("{SERVER}", row("CCDESC"))
                    Else
                        cadenaas400 = cadenaas400.Replace("{SERVER}", row("CCNOT1"))
                    End If
                ElseIf row("CCCODE") = "AS400_USER" Then
                    If My.Settings.PRUEBAS = "SI" Then
                        cadenaas400 = cadenaas400.Replace("{USER}", row("CCDESC"))
                    Else
                        cadenaas400 = cadenaas400.Replace("{USER}", row("CCNOT1"))
                    End If
                ElseIf row("CCCODE") = "AS400_PASS" Then
                    If My.Settings.PRUEBAS = "SI" Then
                        cadenaas400 = cadenaas400.Replace("{PASSWORD}", row("CCDESC"))
                    Else
                        cadenaas400 = cadenaas400.Replace("{PASSWORD}", row("CCNOT1"))
                    End If
                ElseIf row("CCCODE") = "AS400_LIB" Then
                    'If My.Settings.PRUEBAS = "SI" Then
                    '    cadenaas400 = cadenaas400.Replace("{DEFAULT}", row("CCDESC"))
                    'Else
                    '    cadenaas400 = cadenaas400.Replace("{DEFAULT}", row("CCNOT1"))
                    'End If
                    cadenaas400 = cadenaas400.Replace("{LIBRARYLIST}", row("CCNOT2"))
                End If
            Next
        Else
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\CadenaConexionAS400Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No hay datos de Cadena Conexion AS400" & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()

            data = Nothing
            data = New BFLOG
            With data
                .USUARIO = Net.Dns.GetHostName()
                .OPERACION = "CadenaConexionAS400"
                .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                .EVENTO = "No hay datos de Cadena Conexion AS400"
                .TXTSQL = fSql
                .ALERT = 1
            End With
            GuardarBFLOGRow(data)
            Return ""
        End If
        'Return "Provider=IBMDA400;Data Source=213.62.216.85;User ID=COSIS001;Password=ZXC09ASD;Library List=PP61BPCSO,CP61BPCSF,CP61BPCESP,PP61BPCPTF,PP61CPEUSR,CP61BPCUSR,CP61BPCUSF,PP61BAMUSF,M6168345C,QGPL,QTEMP;Force Translate=0;Naming Convention=1;"
        'Return "Data Source=213.62.216.85;Password=ZXC09ASD;User ID=COSIS001;LibraryList=PP61BPCSO,CP61BPCSF,CP61BPCESP,PP61BPCPTF,PP61CPEUSR,CP61BPCUSR,CP61BPCUSF,PP61BAMUSF,M6168345C,QGPL,QTEMP;SchemaSearchList=PP61BPCSO,CP61BPCSF,CP61BPCESP,PP61BPCPTF,PP61CPEUSR,CP61BPCUSR,CP61BPCUSF,PP61BAMUSF,M6168345C,QGPL,QTEMP;Naming=System"
        Return cadenaas400
    End Function

    Public Function AbrirConexion() As Boolean
        Dim abierta As Boolean = False
        Try
            Console.WriteLine("Creando Directorio")
            If Not Directory.Exists(appPath) Then
                Directory.CreateDirectory(appPath)
            End If
            Console.WriteLine("Validando Conexion abierta")
            If conexion IsNot Nothing Then
                If conexion.State = ConnectionState.Open Then
                    abierta = True
                    Console.WriteLine("Conexion Existente Abierta")
                Else
                    Console.WriteLine("Abriendo Conexion")
                    conexion = New MySqlConnection

                    If My.Settings.PRUEBAS = "SI" Then
                        conexion.ConnectionString = My.Settings.BASEDATOSLOCAL
                    Else
                        conexion.ConnectionString = My.Settings.BASEDATOS
                    End If
                    conexion.Open()
                    Console.WriteLine("Conexion Abierta")
                    abierta = True
                End If
            Else
                Console.WriteLine("Abriendo Conexion")
                conexion = New MySqlConnection

                If My.Settings.PRUEBAS = "SI" Then
                    conexion.ConnectionString = My.Settings.BASEDATOSLOCAL
                Else
                    conexion.ConnectionString = My.Settings.BASEDATOS
                End If
                conexion.Open()
                Console.WriteLine("Conexion Abierta")
                abierta = True
            End If

        Catch ex1 As MySqlException
            abierta = False
            mensaje = ex1.Message
        Catch ex As Exception
            abierta = False
            mensaje = ex.Message
        End Try
        If abierta = False Then
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine(mensaje)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End If
        Return abierta
    End Function

    Public Function CerrarConexion() As Boolean
        Dim cerrada As Boolean = False
        Try
            conexion.Close()
            cerrada = True
        Catch ex1 As MySqlException
            cerrada = False
            mensaje = ex1.ToString
        Catch ex As Exception
            cerrada = False
            mensaje = ex.ToString
        End Try
        Return cerrada
    End Function

    ''' <summary>
    ''' Bloquea tabla enviada
    ''' </summary>
    ''' <param name="Table">Tabla a bloquear</param>
    ''' <returns>un booleano</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 15/04/2016</remarks>
    Public Function LockTable(Table As String) As Boolean
        Dim continuarguardando As Boolean
        Using sqlCommand As New MySqlCommand()
            With sqlCommand
                .CommandText = "LOCK TABLES " & Table & " LOW_PRIORITY WRITE"
                .Connection = conexion
                '
                '.Parameters.Add("?Tabla", Table)
            End With
            Try
                sqlCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As MySqlException
                '
                continuarguardando = False
                'Mensaje = "No se pudo eliminar en la tabla sequence_data.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje : " & ex.ToString
            Catch ex As Exception
                '
                continuarguardando = False
                'Mensaje = "No se pudo eliminar en la tabla sequence_data.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje : " & ex.ToString
            Finally
                If continuarguardando = True Then
                    '
                    'conexion.Close()
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Bloquea tabla enviada
    ''' </summary>
    ''' <returns>un booleano</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 15/04/2016</remarks>
    Public Function UnLockTable() As Boolean
        Dim continuarguardando As Boolean
        fSql = "UNLOCK TABLES"
        command = New MySqlCommand()
        command.Connection = conexion
        '
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
        Catch ex As MySqlException
            '
            continuarguardando = False
            'mensaje = "No se pudo eliminar la tabla Table.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje : " + ex.ToString()
        Catch ex As Exception
            '
            continuarguardando = False
            'mensaje = "No se pudo eliminar la tabla Table.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje : " + ex.ToString()
        Finally
            If continuarguardando = True Then
                '
            End If
            'conexion.Close()
        End Try
        Return continuarguardando
    End Function

    Public Function CalcConsec(ByVal sID As String) As String
        Dim fmt As String
        Dim result As String = ""
        Dim resultmax As String = ""

        Try
            command = New MySqlCommand
            command.Connection = conexion
            fSql = "LOCK TABLES sequence_data LOW_PRIORITY WRITE"
            command.CommandText = fSql
            command.ExecuteNonQuery()
            fSql = "SELECT sequence_cur_value FROM sequence_data WHERE sequence_name = '" & sID & "'"
            command.CommandText = fSql
            result = command.ExecuteScalar()
            If result <> "" Then
                fSql = "UPDATE sequence_data SET sequence_cur_value = If((sequence_cur_value + sequence_increment) > sequence_max_value,If(sequence_cycle = True,sequence_min_value,NULL),sequence_cur_value +sequence_increment) WHERE sequence_name = '" & sID & "'"
                command.CommandText = fSql
                command.ExecuteNonQuery()
                fSql = "SELECT sequence_max_value FROM SEQUENCE_DATA WHERE sequence_name = '" & sID & "'"
                command.CommandText = fSql
                resultmax = command.ExecuteScalar()
                If resultmax <> "" Then
                    fmt = Replace(resultmax, "9", "0")
                    result = Right(fmt & result, Len(fmt))
                    fSql = "UNLOCK TABLES"
                    command.CommandText = fSql
                    command.ExecuteNonQuery()
                End If
            End If
        Catch ex As Exception
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\CalcConsecError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("LOG " & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try

        Return result
    End Function

    Function Ceros(ByVal Val As Object, ByVal C As Integer)
        If C > Len(Trim(Val)) Then
            Ceros = Right(RepiteChar(C, "0") & Trim(Val), C)
        Else
            Ceros = Val
        End If
    End Function

    Function RepiteChar(ByVal Cant As Integer, ByVal Txt As Char) As String
        Dim i As Integer

        RepiteChar = ""
        For i = 1 To Cant
            RepiteChar = RepiteChar & Txt
        Next

    End Function

#End Region

#Region "GUARDAR"

    ''' <summary>
    ''' Guarda BFPARAM
    ''' </summary>
    ''' <param name="datos">BFPARAM</param>
    ''' <param name="tipo">0- (CCID, CCCODE, CCCODE2, CCTABL),
    ''' 1- (CCID, CCCODE, CCTABL)</param>
    ''' <returns>true si se guarda con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function GuardarBFPARAMRow(ByVal datos As BFPARAM, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean
        Using sqlCommand As New MySqlCommand()
            With sqlCommand
                Select Case tipo
                    Case 0
                        fSql = " INSERT INTO BFPARAM (CCID, CCCODE, CCCODE2, CCTABL) VALUES ("
                        fSql &= "'" & datos.CCID & "', "
                        fSql &= "'" & datos.CCCODE & "', "
                        fSql &= "'" & datos.CCCODE2 & "', "
                        fSql &= "'" & datos.CCTABL & "')"
                    Case 1
                        fSql = " INSERT INTO BFPARAM (CCID, CCCODE, CCTABL) VALUES ("
                        fSql &= "'" & datos.CCID & "', "
                        fSql &= "'" & datos.CCCODE & "', "
                        fSql &= "'" & datos.CCTABL & "')"
                End Select
                .CommandText = fSql
                .Connection = conexion
            End With
            Try
                sqlCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As MySqlException

                continuarguardando = False
                mensaje = ex.Message
            Catch ex As Exception

                continuarguardando = False
                mensaje = ex.Message
            Finally
                If continuarguardando = False Then
                    Dim sb As New StringBuilder()
                    Dim sw As StreamWriter = New StreamWriter(appPath & "\BFPARAMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                    sb.AppendLine("No se pudo guardar en la tabla BFPARAM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                    sb.AppendLine(Now().ToString)
                    sw.WriteLine(sb.ToString())
                    sw.Close()

                    data = Nothing
                    data = New BFLOG
                    With data
                        .USUARIO = Net.Dns.GetHostName()
                        .OPERACION = "Guardar BFPARAM"
                        .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                        .EVENTO = mensaje
                        .TXTSQL = fSql
                        .ALERT = 1
                        .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                    End With
                    GuardarBFLOGRow(data)
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Guarda BFCIM
    ''' </summary>
    ''' <param name="datos">BFCIM</param>
    ''' <param name="tipo">0- (IFUENTE, IBATCH, INID, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREAS, INREF, INLPGM, INPROD, INWHSE, INLOT, INLOC, INQTY, INCBY, INCDT, INCTM)</param>
    ''' <returns>true si se guarda con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function GuardarBFCIMRow(ByVal datos As BFCIM, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean
        Using sqlCommand As New MySqlCommand()
            With sqlCommand
                Select Case tipo
                    Case 0
                        fSql = "INSERT INTO BFCIM (IFUENTE, IBATCH, INLINE, INID, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREAS, INREF, INPROD, INWHSE, INLOT, INLOC, INQTY, INCBY, INLDT, INLTM, INCDT, INCTM) VALUES (" &
                         "'" & datos.IFUENTE & "', " &
                        datos.IBATCH & ", " &
                        "'" & datos.INLINE & "', " &
                        "'" & datos.INID & "', " &
                        "'" & datos.INTRAN & "', " &
                        datos.INCTNR & ", " &
                        "'" & datos.INMFGR & "', " &
                        datos.INDATE & ", " &
                        datos.INTRN & ", " &
                        "'" & datos.INMLOT & "', " &
                        "'" & datos.INREAS & "', " &
                        datos.INREF & ", " &
                        "'" & datos.INPROD & "', " &
                        "'" & datos.INWHSE & "', " &
                        "'" & datos.INLOT & "', " &
                        "'" & datos.INLOC & "', " &
                        datos.INQTY & ", " &
                        "'" & datos.INCBY & "', " &
                        datos.INLDT & ", " &
                        datos.INLTM & ", " &
                        datos.INCDT & ", " &
                        datos.INCTM & ")"
                    Case 1
                        fSql = "INSERT INTO BFCIM (IFUENTE, IBATCH, INID, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREAS, INREF, INPROD, INWHSE, INLOT, INLOC, INQTY, INCBY, INLDT, INLTM, INCDT, INCTM) VALUES (" &
                         "'" & datos.IFUENTE & "', " &
                        datos.IBATCH & ", " &
                        "'" & datos.INID & "', " &
                        "'" & datos.INTRAN & "', " &
                        "'" & datos.INCTNR & "', " &
                        "'" & datos.INMFGR & "', " &
                        datos.INDATE & ", " &
                        datos.INTRN & ", " &
                        "'" & datos.INMLOT & "', " &
                        "'" & datos.INREAS & "', " &
                        datos.INREF & ", " &
                        "'" & datos.INPROD & "', " &
                        "'" & datos.INWHSE & "', " &
                        "'" & datos.INLOT & "', " &
                        "'" & datos.INLOC & "', " &
                        datos.INQTY & ", " &
                        "'" & datos.INCBY & "', " &
                        datos.INLDT & ", " &
                        datos.INLTM & ", " &
                        datos.INCDT & ", " &
                        datos.INCTM & ")"
                    Case 2
                        fSql = "INSERT INTO BFCIM (IFUENTE, IBATCH, INID, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREF, INPROD, INLOT, INLOC, INQTY, INCBY, INLDT, INLTM, INCDT, INCTM, INUMERO, INCOST, INWHSE, INFACTU) VALUES (" &
                        "'" & datos.IFUENTE & "', " &
                        datos.IBATCH & ", " &
                        "'" & datos.INID & "', " &
                        "'" & datos.INTRAN & "', " &
                        "'" & datos.INCTNR & "', " &
                        "'" & datos.INMFGR & "', " &
                        datos.INDATE & ", " &
                        datos.INTRN & ", " &
                        "'" & datos.INMLOT & "', " &
                        datos.INREF & ", " &
                        "'" & datos.INPROD & "', " &
                        "'" & datos.INLOT & "', " &
                        "'" & datos.INLOC & "', " &
                        datos.INQTY & ", " &
                        "'" & datos.INCBY & "', " &
                        datos.INLDT & ", " &
                        datos.INLTM & ", " &
                        datos.INCDT & ", " &
                        datos.INCTM & ", " &
                        datos.INUMERO & ", " &
                        datos.INCOST & ", " &
                        "'" & datos.INWHSE & "', " &
                        "'" & datos.INFACTU & "')"
                    Case 3
                        fSql = "INSERT INTO BFCIM (IFUENTE, INID, IBATCH, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREAS, INREF, INPROD, INWHSE, INLOT, INLOC, INQTY, INCBY, INCDT, INCTM) VALUES (" &
                        "'" & datos.IFUENTE & "', " &
                        "'" & datos.INID & "', " &
                        datos.IBATCH & ", " &
                        "'" & datos.INTRAN & "', " &
                        datos.INCTNR & ", " &
                        "'" & datos.INMFGR & "', " &
                        datos.INDATE & ", " &
                        datos.INTRN & ", " &
                        "'" & datos.INMLOT & "', " &
                        "'" & datos.INREAS & "', " &
                        datos.INREF & ", " &
                        "'" & datos.INPROD & "', " &
                        "'" & datos.INWHSE & "', " &
                        "'" & datos.INLOT & "', " &
                        "'" & datos.INLOC & "', " &
                        datos.INQTY & ", " &
                        "'" & datos.INCBY & "', " &
                        datos.INCDT & ", " &
                        datos.INCTM & ")"
                    Case 4
                        fSql = "INSERT INTO BFCIM (IFUENTE, IBATCH, INID, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREAS, INREF, INLPGM, INPROD, INWHSE, INLOT, INLOC, INQTY, INCBY, INCDT, INCTM) VALUES ("
                        fSql &= "'" & datos.IFUENTE & "', "
                        fSql &= datos.IBATCH & ", "
                        fSql &= "'" & datos.INID & "', "
                        fSql &= "'" & datos.INTRAN & "', "
                        fSql &= datos.INCTNR & ", "
                        fSql &= "'" & datos.INMFGR & "', "
                        fSql &= datos.INDATE & ", "
                        fSql &= datos.INTRN & ", "
                        fSql &= "'" & datos.INMLOT & "', "
                        fSql &= "'" & datos.INREAS & "', "
                        fSql &= datos.INREF & ", "
                        fSql &= "'" & datos.INLPGM & "', "
                        fSql &= "'" & datos.INPROD & "', "
                        fSql &= "'" & datos.INWHSE & "', "
                        fSql &= "'" & datos.INLOT & "', "
                        fSql &= "'" & datos.INLOC & "', "
                        fSql &= datos.INQTY & ", "
                        fSql &= "'" & datos.INCBY & "', "
                        fSql &= datos.INCDT & ", "
                        fSql &= datos.INCTM & ")"
                    Case 5
                        fSql = "INSERT INTO BFCIM (IFUENTE, IBATCH, INID, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREAS, INREF, INPROD, INWHSE, INLOT, INLOC, INQTY, INCBY, INCDT, INCTM) VALUES ("
                        fSql &= "'" & datos.IFUENTE & "', "
                        fSql &= datos.IBATCH & ", "
                        fSql &= "'" & datos.INID & "', "
                        fSql &= "'" & datos.INTRAN & "', "
                        fSql &= datos.INCTNR & ", "
                        fSql &= "'" & datos.INMFGR & "', "
                        fSql &= datos.INDATE & ", "
                        fSql &= datos.INTRN & ", "
                        fSql &= "'" & datos.INMLOT & "', "
                        fSql &= "'" & datos.INREAS & "', "
                        fSql &= datos.INREF & ", "
                        fSql &= "'" & datos.INPROD & "', "
                        fSql &= "'" & datos.INWHSE & "', "
                        fSql &= "'" & datos.INLOT & "', "
                        fSql &= "'" & datos.INLOC & "', "
                        fSql &= datos.INQTY & ", "
                        fSql &= "'" & datos.INCBY & "', "
                        fSql &= datos.INCDT & ", "
                        fSql &= datos.INCTM & ")"
                End Select

                .CommandText = fSql
                .Connection = conexion

            End With
            Try
                sqlCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As MySqlException

                continuarguardando = False
                mensaje = ex.Message
            Catch ex As Exception

                continuarguardando = False
                mensaje = ex.Message
            Finally
                If continuarguardando = True Then

                Else
                    Dim sb As New StringBuilder()
                    Dim sw As StreamWriter = New StreamWriter(appPath & "\BFCIMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                    sb.AppendLine("No se pudo guardar en la tabla BFCIM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                    sb.AppendLine(Now().ToString)
                    sw.WriteLine(sb.ToString())
                    sw.Close()

                    data = Nothing
                    data = New BFLOG
                    With data
                        .USUARIO = Net.Dns.GetHostName()
                        .OPERACION = "Guardar BFCIM"
                        .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                        .EVENTO = mensaje
                        .TXTSQL = fSql
                        .ALERT = 1
                    End With
                    GuardarBFLOGRow(data)
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Guarda BFDESPA
    ''' </summary>
    ''' <param name="datos">BFDESPA</param>
    ''' <returns>true si se guarda con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function GuardarBFDESPARow(ByVal datos As BFDESPA) As Boolean
        Dim continuarguardando As Boolean
        Using sqlCommand As New MySqlCommand()
            With sqlCommand
                fSql = "INSERT INTO BFDESPA (DBATCH, DPEDID, DLINEA, DPRODU, DCANTI, DBODEG, DLOCAL, DLOTE, DUSER, DFLAG, DFECHA) VALUES ("
                fSql &= "'" & datos.DBATCH & "', "
                fSql &= datos.DPEDID & ", "
                fSql &= "'" & datos.DLINEA & "', "
                fSql &= "'" & datos.DPRODU & "', "
                fSql &= datos.DCANTI & ", "
                fSql &= "'" & datos.DBODEG & "', "
                fSql &= "'" & datos.DLOCAL & "', "
                fSql &= "'" & datos.DLOTE & "', "
                fSql &= "'" & datos.DUSER & "', "
                fSql &= datos.DFLAG & ", "
                fSql &= "NOW())"
                .CommandText = fSql
                .Connection = conexion

            End With
            Try
                sqlCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As MySqlException

                continuarguardando = False
                mensaje = ex.Message
            Catch ex As Exception

                continuarguardando = False
                mensaje = ex.Message
            Finally
                If continuarguardando = True Then

                Else
                    Dim sb As New StringBuilder()
                    Dim sw As StreamWriter = New StreamWriter(appPath & "\BFDESPAError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                    sb.AppendLine("No se pudo guardar en la tabla BFDESPA.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje : " & vbCrLf & mensaje & vbCrLf & fSql)
                    sb.AppendLine(Now().ToString)
                    sw.WriteLine(sb.ToString())
                    sw.Close()

                    data = Nothing
                    data = New BFLOG
                    With data
                        .USUARIO = Net.Dns.GetHostName()
                        .OPERACION = "Guardar BFDESPA"
                        .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                        .EVENTO = mensaje
                        .TXTSQL = fSql
                        .ALERT = 1
                        .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                    End With
                    GuardarBFLOGRow(data)
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Guarda BFLOG
    ''' </summary>
    ''' <param name="datos">BFLOG</param>
    ''' <returns>true si se guarda con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 15/04/2016</remarks>
    Public Function GuardarBFLOGRow(ByVal datos As BFLOG) As Boolean
        Dim continuarguardandoBFLOG As Boolean

        Using sqlCommand As New MySqlCommand()
            With sqlCommand
                fSql = "INSERT INTO BFLOG (USUARIO, OPERACION, PROGRAMA, ALERT, EVENTO, TXTSQL) VALUES ("
                fSql &= "'" & datos.usuario & "', "
                fSql &= "'" & datos.OPERACION & "', "
                fSql &= "'" & datos.programa & "', "
                fSql &= datos.alert & ", "
                fSql &= "'" & datos.evento.Replace("'", "''") & "', "
                fSql &= "'" & datos.txtsql.Replace("'", "''") & "')"
                .CommandText = fSql
                .Connection = conexion
            End With
            Try
                sqlCommand.ExecuteNonQuery()
                continuarguardandoBFLOG = True
            Catch ex As MySqlException
                continuarguardandoBFLOG = False
                mensaje = ex.Message
            Catch ex As Exception
                continuarguardandoBFLOG = False
                mensaje = ex.Message
            Finally
                If continuarguardandoBFLOG = False Then
                    Dim sb As New StringBuilder()
                    Dim sw As StreamWriter = New StreamWriter(appPath & "\BFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                    sb.AppendLine("No se pudo guardar en la tabla BFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                    sb.AppendLine(Now().ToString)
                    sw.WriteLine(sb.ToString())
                    sw.Close()
                End If
            End Try
        End Using
        Return continuarguardandoBFLOG
    End Function

#End Region

End Class

Public Class BFLOG
    Public usuario As String
    Public programa As String
    Public alert As String
    Public evento As String
    Public lkey As String
    Public txtsql As String
    Public OPERACION As String
End Class

Public Class BFCIM
    Public INFACTU As String
    Public INLPGM As String
    Public IFUENTE As String
    Public IBATCH As String
    Public INLINE As String
    Public INID As String
    Public INTRAN As String
    Public INCTNR As String
    Public INMFGR As String
    Public INDATE As String
    Public INTRN As String
    Public INMLOT As String
    Public INREAS As String
    Public INREF As String
    Public INPROD As String
    Public INWHSE As String
    Public INLOT As String
    Public INLOC As String
    Public INQTY As String
    Public INCBY As String
    Public INLDT As String
    Public INLTM As String
    Public INCDT As String
    Public INCTM As String
    Public INUMERO As String
    Public INCOST As String
    Public IFLAG As String
    Public INERROR As String
End Class

Public Class BFREPCONC
    Public NUMREGISTO As String
    Public CNUMCON As String
End Class

Public Class BFREPCONCD
    Public NUMREGISTO As String
    Public CNUMCON As String
    Public CNUMREP As String
End Class

Public Class BFTASK
    Public STS2 As String
    Public TMSG As String
    Public TNUMREG As String

End Class

Public Class BFPARAM
    Public CCNOT1 As String
    Public CCMNDT As String
    Public CCNUM As String
    Public CCID As String
    Public CCCODE As String
    Public CCCODE2 As String
    Public CCTABL As String
    Public CCENDT As String
    Public CCUDTS As String
    Public CCDESC As String
    Public CCNOT2 As String
    Public CCNOTE As String
    Public CCUDV1 As String
    Public CCUDC2 As String
End Class

Public Class BFREP
    Public TQRECB As String
    Public TQREO As String
    Public TORD As String
    Public TNUMREP As String
    Public TQREQACEB As String
    Public TQREDB As String
    Public TRPODACEB As String
    Public TQREC As String
End Class

Public Class BFREPACE
    Public CENVIADO As String
    Public WRKID As String
End Class

Public Class BFREPTURRJ
    Public RENVIADO As String
    Public RWRKID As String
End Class

Public Class BFCIMWS
    Public INMLOT As String
    Public IFLAG As String
    Public IFUENTE As String
    Public IBATCH As String
    Public INLINE As String
    Public INID As String
    Public INTRAN As String
    Public INCTNR As String
    Public INMFGR As String
    Public INDATE As String
    Public INTRN As String
    Public INREAS As String
    Public INREF As String
    Public INPROD As String
    Public INWHSE As String
    Public INLOT As String
    Public INLOC As String
    Public INQTY As String
    Public INCBY As String
    Public INLDT As String
    Public INLTM As String
    Public INCDT As String
    Public INCTM As String
    Public INUMERO As String
    Public INCOST As String
    Public IFLAG2 As String
End Class

Public Class BFDESPA
    Public DBATCH As String
    Public DPEDID As String
    Public DLINEA As String
    Public DPRODU As String
    Public DCANTI As String
    Public DBODEG As String
    Public DLOCAL As String
    Public DLOTE As String
    Public DUSER As String
    Public DFLAG As String
    Public DDESER As String
    Public DERROR As String
End Class

Public Class BFDESPAWS
    Public DFLAG2 As String
    Public DBATCH As String
    Public DPEDID As String
    Public DLINEA As String
End Class

Public Class BFREPD
    Public TQREC As String
    Public TNUMREP As String
End Class