﻿Imports CLDatos
Imports System.Text
Imports System.IO

Module R
    Private IDWORK As String
    Private ORDEN As String
    Private USUARIO As String
    Private RUTINA As String
    Private ProcesoActual As String
    Private TAREA As String
    Private datos As New ClsDatos
    Private datosas400 As New ClsDatosAS400
    Private data
    Private cadenaAS400 As String
    Dim wsNum As String
    Dim wsResult As String

    Sub Main()
        Dim aSubParam() As String
        Try
            Environment.GetCommandLineArgs()
            For Each parametro In Environment.GetCommandLineArgs()
                aSubParam = Split(parametro, "=")
                Select Case UCase(aSubParam(0))
                    Case "BATCH"
                        IDWORK = aSubParam(1)
                    Case "ORDEN"
                        ORDEN = aSubParam(1)
                    Case "USUARIO"
                        USUARIO = aSubParam(1)
                    Case "RUTINA"
                        RUTINA = aSubParam(1)
                    Case "TAREA"
                        TAREA = aSubParam(1)
                End Select
            Next
            Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("es-CO")
            Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator = "."
            Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyGroupSeparator = ","
            Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator = "."
            Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberGroupSeparator = ","

            Console.WriteLine("Abriendo conexion")
            ProcesoActual = "Abriendo conexion"
            If Not datos.AbrirConexion() Then
                Console.WriteLine("Error de conexion")
                Exit Sub
            End If

            ProcesoActual = "Consultando Cadena Conexion AS400"
            cadenaAS400 = datos.CadenaConexionAS400

            If cadenaAS400 <> "" Then
                Console.WriteLine("Abriendo conexion AS400")
                ProcesoActual = "Abriendo conexion AS400" & vbCrLf & cadenaAS400
                datosas400.dat = datos
                If Not datosas400.AbrirConexion(cadenaAS400, ProcesoActual) Then
                    Console.WriteLine("Error de conexion AS400")
                    Exit Sub
                End If
            Else
                Console.WriteLine("Sin Cadena conexion AS400")
                Exit Sub
            End If

            Console.Write(RUTINA)
            Select Case RUTINA
                Case "CONSUMOS"
                    Consumos()
                Case "CONSUMOSD"
                    ConsumosD()
                Case "SINCRONIZAESTADO"
                    registroWS("Operacion", "Sincroniza Estado")
                    Console.WriteLine(" ")
                    Console.WriteLine("...ACTUALIZABFCIM...")
                    Console.WriteLine(" ")
                    actualizaBFCIMMySQL()
                Case "ACTUALIZACANTORDEN"
                    registroWS("Operacion", "Actualiza Cant Orden")
                    Console.WriteLine(" ")
                    Console.WriteLine("...ACTUALIZACIONPR...")
                    Console.WriteLine(" ")
                    actualizacionPR()
                Case "TRANSACCIONACERO"
                    registroWS("Operacion", "Transaccion Acero")
                    Console.WriteLine(" ")
                    Console.WriteLine("...TRANSACCIONACERO...")
                    Console.WriteLine(" ")
                    TransaccionAcero()
                Case "TRANSACCIONRJ"
                    registroWS("Operacion", "Transaccion RJ")
                    Console.WriteLine(" ")
                    Console.WriteLine("...TRANSACCIONRJ...")
                    Console.WriteLine(" ")
                    TransaccionRJ()
                Case "TRASLADOS"
                    registroWS("Operacion", "Traslados")
                    Console.WriteLine(" ")
                    Console.WriteLine("...ACTUALIZACIONBFCIM...")
                    Console.WriteLine(" ")
                    ActualizacionBFCIM()
                Case "TRANSACCIONINV"
                    registroWS("Operacion", "Transacciones")
                    Console.WriteLine(" ")
                    Console.WriteLine("...TRANSACCIONINV...")
                    Console.WriteLine(" ")
                    ActualizacionBFCIMTransacInv()
                Case "ASIGNACION"
                    'registroWS("Operacion", "Asignacion")
                    'Para los webservices de batch
                    Console.WriteLine(" ")
                    Console.WriteLine("...ACTUALIZACIONBFDESPA...")
                    Console.WriteLine(" ")
                    ActualizacionBFDESPA()
                Case "ACTUALIZAPR"
                    'CORRE CADA HORA
                    Console.WriteLine(" ")
                    If registroWS("Actualiza PR", "Actualiza PR") Then
                        Console.WriteLine(" ")
                        Console.WriteLine("...ACTUALIZAPR...")
                        Console.WriteLine(" ")
                        actualizaPR()
                    End If
            End Select

            Console.WriteLine("... FIN RUTINAS ...")

            datosas400.CerrarConexion()
            datos.CerrarConexion()
            Console.WriteLine("... CONEXIONES CERRADAS ...")

        Catch ex As Exception
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\" & RUTINA & "Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine(ex.Message & vbCrLf & ProcesoActual & vbCrLf & datos.fSql & vbCrLf & datosas400.fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()

            data = Nothing
            data = New BFLOG
            With data
                .USUARIO = Net.Dns.GetHostName()
                .OPERACION = RUTINA
                .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                .EVENTO = ex.Message
                .TXTSQL = datos.fSql
                .ALERT = 1
            End With
            datos.GuardarBFLOGRow(data)
        End Try
    End Sub

    Private Sub Consumos()
        Dim reas
        Dim cons As String
        Dim batch As String
        Dim parametro(0) As String
        Dim dtBFREPCONC As DataTable
        Dim continuar As Boolean

        ProcesoActual = "Consultando BFREPCONC CNUMREP " & IDWORK
        Console.WriteLine(IDWORK)
        parametro(0) = IDWORK
        dtBFREPCONC = datos.ConsultarBFREPCONCdt(parametro, 0)
        If dtBFREPCONC IsNot Nothing Then
            batch = datos.CalcConsec("CIMNUM")
            Console.WriteLine("Recorriendo BFREPCONC")
            ProcesoActual = "Recorriendo BFREPCONC"
            For Each row As DataRow In dtBFREPCONC.Rows
                cons = datos.CalcConsec("CIMNUM")
                reas = "01"
                data = Nothing
                data = New BFCIM
                With data
                    .IFUENTE = "PRODUCCION"                             '//15A
                    .IBATCH = batch                                     '//8"
                    .INID = "IN"                                        '//2A    Record Identifier
                    .INTRAN = "CI"                                      '//2A    Transaction Type
                    .INCTNR = 0                                         '//2A    Transaction Type"
                    .INMFGR = 0                                         '//2A    Transaction Type"
                    .INDATE = datosas400.DB2_DATEDEC                    '//8P0   Date		"
                    .INTRN = Right(cons, 5)                             '//5P0   INTRN"
                    .INMLOT = "RP" & datos.Ceros(cons, 8)               '//5P0   INMLOT
                    .INREAS = "01"                                      '//5P0   INREAS
                    .INREF = ORDEN                                      '//8P0   Date"
                    .INLPGM = row("CNUMCON")                            '
                    .INPROD = row("CPROD")                              '//15A   Item Number
                    .INWHSE = row("CQBOD")                              '//2A    Warehouse
                    .INLOT = ""                                         '//10A   Lot Number"
                    .INLOC = ""                                         '//6A    Location Code"
                    .INQTY = Replace(row("CQCON"), ",", ".")            '//11P3  Month to Date Adjustments
                    .INCBY = USUARIO                                    '//10A   Creted by"
                    .INCDT = datosas400.DB2_DATEDEC                     '//8P0   Date Created"
                    .INCTM = datosas400.DB2_TIMEDEC                     '//6P0   Time Created"
                End With
                ProcesoActual = "Guardando BFCIM AS400"
                continuar = datosas400.GuardarBFCIMRow(data, 4)
                If continuar = True Then
                    continuar = datos.GuardarBFCIMRow(data, 4)
                    If continuar = True Then
                        data = Nothing
                        data = New BFREPCONC
                        With data
                            .NUMREGISTO = cons
                            .CNUMCON = row("CNUMCON")
                        End With
                        continuar = datos.ActualizarBFREPCONCRow(data, 0)
                        If continuar = False Then
                            Return
                        End If
                    Else
                        Return
                    End If
                Else
                    Return
                End If
            Next
            If TAREA <> "" Then
                data = Nothing
                data = New BFTASK
                With data
                    .STS2 = 1
                    .TMSG = "TERMINADO"
                    .TNUMREG = TAREA
                End With
                continuar = datos.ActualizarBFTASKRow(data, 0)
            End If
            datosas400.LlamaPrograma("BCTRAINVB", batch & "', 'PROD")
        Else
            Return
        End If
        Console.WriteLine("...FIN CONSUMOS...")
    End Sub

    Private Sub ConsumosD()
        Dim reas
        Dim cons As String
        Dim batch As String
        Dim parametro(0) As String
        Dim dtBFREPCONCD As DataTable
        Dim continuar As Boolean

        Console.WriteLine(IDWORK)
        parametro(0) = IDWORK
        dtBFREPCONCD = datos.ConsultarBFREPCONCDdt(parametro, 0)
        If dtBFREPCONCD IsNot Nothing Then
            batch = datos.CalcConsec("CIMNUM")
            Console.WriteLine("Recorriendo BFREPCONCD")
            For Each row As DataRow In dtBFREPCONCD.Rows
                cons = datos.CalcConsec("CIMNUM")
                reas = "01"

                data = Nothing
                data = New BFCIM
                With data
                    .IFUENTE = "PRODUCCION"                             '//15A
                    .IBATCH = batch                                     '//8"
                    .INID = "IN"                                        '//2A    Record Identifier
                    .INTRAN = "IT"                                      '//2A    Transaction Type
                    .INCTNR = 0                                         '//2A    Transaction Type"
                    .INMFGR = 0                                         '//2A    Transaction Type"
                    .INDATE = datosas400.DB2_DATEDEC                    '//8P0   Date		"
                    .INTRN = Right(cons, 5)                             '//5P0   INTRN"
                    .INMLOT = "RP" & datos.Ceros(cons, 8)               '//5P0   INTRN
                    .INREAS = "01"                                      '//5P0   INTRN
                    .INREF = ORDEN                                      '//8P0   Date"
                    .INPROD = row("CPROD")                              '//15A   Item Number
                    .INWHSE = row("CQBOD")                              '//2A    Warehouse
                    .INLOT = ""                                         '//10A   Lot Number"
                    .INLOC = ""                                         '//6A    Location Code"
                    .INQTY = Replace(row("CQCON"), ",", ".")            '//11P3  Month to Date Adjustments
                    .INCBY = USUARIO                                    '//10A   Creted by"
                    .INCDT = datosas400.DB2_DATEDEC                     '//8P0   Date Created"
                    .INCTM = datosas400.DB2_TIMEDEC                     '//6P0   Time Created"
                End With
                continuar = datosas400.GuardarBFCIMRow(data, 5)
                If continuar = True Then
                    continuar = datos.GuardarBFCIMRow(data, 5)
                    If continuar = True Then
                        data = Nothing
                        data = New BFREPCONCD
                        With data
                            .NUMREGISTO = cons
                            .CNUMCON = row("CNUMCON")
                            .CNUMREP = IDWORK
                        End With
                        continuar = datos.ActualizarBFREPCONCDRow(data, 0)
                        If continuar = False Then
                            Return
                        End If
                    Else
                        Return
                    End If
                Else
                    Return
                End If
            Next
            If TAREA <> "" Then
                data = Nothing
                data = New BFTASK
                With data
                    .STS2 = 1
                    .TMSG = "TERMINADO"
                    .TNUMREG = TAREA
                End With
                continuar = datos.ActualizarBFTASKRow(data, 0)
            End If
            datosas400.LlamaPrograma("BCTRAINVB", batch & "', 'PROD")
        Else
            Return
        End If
        Console.WriteLine("...FIN CONSUMOSD...")
    End Sub

    Function registroWS(tipo As String, op As String) As Boolean
        Dim servWin As String = "TareasBellota"
        Dim resultado As Boolean = True
        Dim hora As Integer
        Dim continuar As Boolean
        Dim dtBFPARAM As DataTable
        Dim parametro(2) As String


        'If wsNum <> "" Then
        '    data = Nothing
        '    data = New BFPARAM
        '    With data
        '        .CCNOT1 = wsResult
        '        .CCNUM = wsNum
        '    End With
        '    Console.WriteLine("Actualizando BFPARAM MySQL...")
        '    continuar = datos.ActualizarBFPARAMRow(data, 0)
        '    If continuar = False Then
        '        Console.WriteLine("Error Actualizando BFPARAM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
        '        'ERROR DE ACTUALIZADO
        '    Else
        '        Console.WriteLine("BFPARAM MySQL Actualizado")
        '    End If
        'End If

        If op <> "" Then
            Console.WriteLine("Consultando BFPARAM MySQL...")
            parametro(0) = servWin
            parametro(1) = "02"
            parametro(2) = op
            dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 1)
            If dtBFPARAM Is Nothing Then
                data = Nothing
                data = New BFPARAM
                With data
                    .CCID = "CC"
                    .CCCODE = "02"
                    .CCCODE2 = op
                    .CCTABL = servWin
                End With
                Console.WriteLine("Guardando BFPARAM MySQL...")
                continuar = datos.GuardarBFPARAMRow(data, 0)
                If continuar = False Then
                    Console.WriteLine("Error Guardando BFPARAM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                    'ERROR DE GUARDADO
                Else
                    Console.WriteLine("BFPARAM MySQL Guardado")
                End If
            End If
            Console.WriteLine("Consultando BFPARAM MySQL...")
            dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 1)
            If dtBFPARAM IsNot Nothing Then
                wsNum = dtBFPARAM.Rows(0).Item("CCNUM")
                data = Nothing
                data = New BFPARAM
                With data
                    '.CCNOT1 = "Inicia Rutina"
                    '.CCNOT1 = wsResult
                    .CCENDT = Format(Now(), "yyy-MM-dd HH:mm:ss")
                    .CCTABL = servWin
                    .CCCODE = "02"
                    .CCCODE2 = op
                End With
                Console.WriteLine("Actualizando BFPARAM MySQL...")
                continuar = datos.ActualizarBFPARAMRow(data, 1)
                If continuar = False Then
                    Console.WriteLine("Error Actualizando BFPARAM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                    'ERROR DE ACTUALIZADO
                Else
                    Console.WriteLine("BFPARAM MySQL Actualizado")
                End If
            End If
        End If
        Console.WriteLine("Consultando BFPARAM MySQL...")
        parametro(0) = servWin
        parametro(1) = "01"
        dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 2)
        If dtBFPARAM Is Nothing Then
            data = Nothing
            data = New BFPARAM
            With data
                .CCID = "CC"
                .CCCODE = "01"
                .CCTABL = servWin
            End With
            Console.WriteLine("Guardando BFPARAM MySQL...")
            continuar = datos.GuardarBFPARAMRow(data, 1)
            If continuar = False Then
                Console.WriteLine("Error Guardando BFPARAM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                'ERROR DE GUARDADO
            Else
                Console.WriteLine("BFPARAM MySQL Guardado")
            End If
        Else
            Console.WriteLine(tipo)
            Console.WriteLine(dtBFPARAM.Rows(0).Item("CCUDC1"))
            Console.WriteLine(dtBFPARAM.Rows(0).Item("CCUDV1"))
            Select Case tipo
                Case "Inicio"
                    resultado = False
                    If IsDBNull(dtBFPARAM.Rows(0).Item("CCUDC1")) Then
                        resultado = True
                    ElseIf dtBFPARAM.Rows(0).Item("CCUDC1") = 1 Then
                        resultado = True
                    End If
                Case "Actualiza PR"
                    resultado = False
                    hora = Hour(Now())
                    If IsDBNull(dtBFPARAM.Rows(0).Item("CCUDV1")) Then
                        resultado = True
                    ElseIf hora <> dtBFPARAM.Rows(0).Item("CCUDV1") Then
                        resultado = True
                    End If
            End Select
        End If
        Dim tipoconsu As Integer
        data = Nothing
        data = New BFPARAM
        With data
            .CCUDTS = Format(Now(), "yyy-MM-dd HH:mm:ss")
            tipoconsu = 6
            Select Case tipo
                Case "Inicio"
                    Dim App As clsApp = New clsApp(System.Reflection.Assembly.GetExecutingAssembly)
                    Dim gsAppVersion = App.Major.ToString & "." & App.Minor.ToString & "." & App.Revision.ToString
                    .CCDESC = gsAppVersion
                    .CCNOT2 = Replace("BFPARAM", "\", "\\")
                    .CCNOTE = My.Application.Info.ProductName
                    .CCENDT = Format(Now(), "yyy-MM-dd HH:mm:ss")
                    tipoconsu = 2
                Case "Fin"
                    tipoconsu = 3
                Case "Operacion"
                    .CCNOT1 = op
                    tipoconsu = 4
                Case "Actualiza PR"
                    If resultado Then
                        .CCUDV1 = hora
                        .CCNOT1 = op
                        tipoconsu = 5
                    End If
            End Select
            .CCTABL = servWin
            .CCCODE = "01"
        End With
        Console.WriteLine("Actualizando BFPARAM MySQL...")
        continuar = datos.ActualizarBFPARAMRow(data, tipoconsu)
        If continuar = False Then
            Console.WriteLine("Error Actualizando BFPARAM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
            'ERROR DE ACTUALIZADO
        Else
            Console.WriteLine("BFPARAM MySQL Actualizado")
        End If
        Console.WriteLine(resultado)
        Console.WriteLine("...FIN REGISTROWS...")
        Return resultado
    End Function

#Region "SINCRONIZA ESTADO"

    Private Sub actualizaBFCIMMySQL()
        'ACTUALIZA MYSQL CON ENVIADOS A CIM600
        Dim parametro(2) As String
        Dim dtBFPARAM As DataTable
        Dim continuar As Boolean
        Dim r As Integer = 0
        Dim dtBFCIM As DataTable

        'VALIDA QUE NO ESTE CORRIENDO EL PROCESO ACTUALMENTE
        parametro(0) = "TareasBellota"
        parametro(1) = "Sincroniza Estado"
        parametro(2) = "0"
        dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 5)
        If dtBFPARAM IsNot Nothing Then
            data = Nothing
            data = New BFPARAM
            With data
                .CCUDC2 = "1"
                .CCNUM = dtBFPARAM.Rows(0).Item("CCNUM")
                .CCNOT1 = "Inicia Rutina"
            End With
            'INICIA PROCESO
            continuar = datos.ActualizarBFPARAMRow(data, 7)
            If continuar = True Then
                Console.WriteLine("Consultando BFCIM AS400 1...")
                dtBFCIM = datosas400.ConsultarBFCIMdt(parametro, 0)
                If dtBFCIM IsNot Nothing Then
                    Console.WriteLine("Recorriendo BFCIM AS400...")
                    For Each row As DataRow In dtBFCIM.Rows
                        data = Nothing
                        data = New BFCIM
                        With data
                            .IFLAG = row("IFLAG")
                            .INMLOT = row("INMLOT")
                            .INERROR = row("INERROR")
                        End With
                        Console.WriteLine("Actualizando BFCIM MySQL 1...")
                        continuar = datos.ActualizarBFCIMRow(data, 0)
                        If continuar = False Then
                            Console.WriteLine("Error Actualizando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                            'ERROR DE ACTUALIZADO
                        End If
                        r += 1
                    Next
                End If
                wsResult = "Registros actualizados: " & r
                Console.WriteLine(wsResult)
                Console.WriteLine("...FIN ACTUALIZA BFCIM 1...")
                actualizaBFCIM(dtBFPARAM.Rows(0).Item("CCNUM"))
            End If
        End If
    End Sub

    Private Sub actualizaBFCIM(ByVal CCNUM As String)
        Dim parametro(2) As String
        Dim continuar As Boolean
        Dim r As Integer = 0
        Dim dtBFCIM As DataTable

        Console.WriteLine("Consultando BFCIM AS400 2 y 7...")
        dtBFCIM = datosas400.ConsultarBFCIMdt(parametro, 1)
        If dtBFCIM IsNot Nothing Then
            Console.WriteLine("Recorriendo BFCIM AS400...")
            For Each row As DataRow In dtBFCIM.Rows
                data = Nothing
                data = New BFCIM
                With data
                    .IFLAG = row("IFLAG")
                    .INMLOT = row("INMLOT")
                    .INERROR = row("INERROR")
                End With
                Console.WriteLine("Actualizando BFCIM MySQL 2 y 7...")
                continuar = datos.ActualizarBFCIMRow(data, 0)
                If continuar = True Then
                    Console.WriteLine("BFCIM MySQL Actualizado")
                    data = Nothing
                    data = New BFCIM
                    With data
                        .INMLOT = row("INMLOT")
                    End With
                    Console.WriteLine("Actualizando BFCIM AS400 4 y 9...")

                    continuar = datosas400.ActualizarBFCIMRow(data, 1)
                    If continuar = False Then
                        Console.WriteLine("Error Actualizando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                        'ERROR AL ACTUALIZAR
                    Else
                        Console.WriteLine("BFCIM AS400 Actualizado")
                    End If
                Else
                    Console.WriteLine("Error Actualizando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                    'ERROR DE ACTUALIZADO
                End If
                r += 1
            Next
        End If
        wsResult = "Registros actualizados: " & r
        Console.WriteLine(wsResult)
        Console.WriteLine("...FIN ACTUALIZA BFCIM 2 Y 7...")

        actualizaBFDESPAMySQL(CCNUM)
    End Sub

    Private Sub actualizaBFDESPAMySQL(ByVal CCNUM As String)
        Dim parametro(0) As String
        Dim continuar As Boolean
        Dim r As Integer = 0
        Dim dtBFDESPA As DataTable

        Console.WriteLine("Consultando BFDESPA AS400 1...")
        dtBFDESPA = datosas400.ConsultarBFDESPAdt(parametro, 0)
        If dtBFDESPA IsNot Nothing Then
            Console.WriteLine("Recorriendo BFDESPA AS400...")
            For Each row As DataRow In dtBFDESPA.Rows
                data = Nothing
                data = New BFDESPA
                With data
                    .DFLAG = row("DFLAG")
                    .DERROR = row("DERROR")
                    .DDESER = row("DDESER")
                    .DBATCH = row("DBATCH")
                    .DPEDID = row("DPEDID")
                    .DLINEA = row("DLINEA")
                End With
                Console.WriteLine("Actualizando BFDESPA MySQL...")
                continuar = datos.ActualizarBFDESPARow(data, 0)
                If continuar = False Then
                    Console.WriteLine("Error Actualizando BFDESPA MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                    'ERROR DE ACTUALIZADO
                End If
                r += 1
            Next
        End If
        wsResult = "Registros actualizados: " & r
        Console.WriteLine(wsResult)
        Console.WriteLine("...FIN ACTUALIZA BFDESPA 1...")
        actualizaBFDESPA(CCNUM)
    End Sub

    Private Sub actualizaBFDESPA(ByVal CCNUM As String)
        Dim parametro(2) As String
        Dim continuar As Boolean
        Dim r As Integer = 0
        Dim dtBFDESPA As DataTable

        Console.WriteLine("Consultando BFDESPA AS400 2 y 7...")
        dtBFDESPA = datosas400.ConsultarBFDESPAdt(parametro, 1)
        If dtBFDESPA IsNot Nothing Then
            Console.WriteLine("Recorriendo BFDESPA AS400...")
            For Each row As DataRow In dtBFDESPA.Rows
                data = Nothing
                data = New BFDESPA
                With data
                    .DFLAG = row("DFLAG")
                    .DERROR = row("DERROR")
                    .DDESER = row("DDESER")
                    .DBATCH = row("DBATCH")
                    .DPEDID = row("DPEDID")
                    .DLINEA = row("DLINEA")
                End With
                Console.WriteLine("Actualizando BFDESPA MySQL...")
                continuar = datos.ActualizarBFDESPARow(data, 0)
                If continuar = True Then
                    Console.WriteLine("BFDESPA MySQL Actualizado")
                    data = Nothing
                    data = New BFDESPA
                    With data
                        .DBATCH = row("DBATCH")
                        .DPEDID = row("DPEDID")
                        .DLINEA = row("DLINEA")
                    End With
                    Console.WriteLine("Actualizando BFDESPA AS400...")
                    continuar = datosas400.ActualizarBFDESPARow(data, 0)
                    If continuar = False Then
                        Console.WriteLine("Error Actualizando BFDESPA AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                        'ERROR AL ACTUALIZAR
                    Else
                        Console.WriteLine("BFDESPA AS400 Actualizado")
                    End If
                Else
                    Console.WriteLine("Error Actualizando BFDESPA MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                    'ERROR DE ACTUALIZADO
                End If
                r += 1
            Next
        End If
        wsResult = "Registros actualizados: " & r
        Console.WriteLine(wsResult)

        data = Nothing
        data = New BFPARAM
        With data
            .CCUDC2 = "0"
            .CCNUM = CCNUM
            .CCNOT1 = wsResult
        End With
        'FINALIZA PROCESO
        continuar = datos.ActualizarBFPARAMRow(data, 7)
        If continuar = True Then
            Console.WriteLine("...FIN ACTUALIZA BFDESPA 2 y 7...")
        End If
    End Sub

#End Region

#Region "BLOQUE ACTUALIZACION PR PRD"

    Private Sub actualizacionPR()
        Dim parametro(2) As String
        Dim dtBFPARAM As DataTable
        Dim continuar As Boolean
        Dim r As Integer = 0
        Dim dtBFREP As DataTable
        Dim dtFSO As DataTable

        'VALIDA QUE NO ESTE CORRIENDO EL PROCESO ACTUALMENTE
        parametro(0) = "TareasBellota"
        parametro(1) = "Actualiza Cant Orden"
        parametro(2) = "0"
        dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 5)
        If dtBFPARAM IsNot Nothing Then
            data = Nothing
            data = New BFPARAM
            With data
                .CCUDC2 = "1"
                .CCNUM = dtBFPARAM.Rows(0).Item("CCNUM")
                .CCNOT1 = "Inicia Rutina"
            End With
            'INICIA PROCESO BLOQUE
            continuar = datos.ActualizarBFPARAMRow(data, 7)
            If continuar = True Then
                'PRIMER BLOQUE PR
                Console.WriteLine("Consultando BFREP MySQL...")
                dtBFREP = datos.ConsultarBFREPdt(parametro, 0)
                If dtBFREP IsNot Nothing Then
                    Console.WriteLine("Recorriendo BFREP MySQL..." & dtBFREP.Rows.Count)
                    For Each row As DataRow In dtBFREP.Rows
                        parametro(0) = row("TORD")
                        Console.WriteLine("Consultando FSO AS400...")
                        dtFSO = datosas400.ConsultarFSOdt(parametro, 0)
                        If dtFSO IsNot Nothing Then
                            data = Nothing
                            data = New BFREP
                            With data
                                .TQRECB = dtFSO.Rows(0).Item("SQFIN")
                                .TQREO = dtFSO.Rows(0).Item("SQREQ")
                                .TORD = row("TORD")
                            End With
                            Console.WriteLine("Actualizando BFREP MySQL...")
                            continuar = datos.ActualizarBFREPRow(data, 0)
                            If continuar = False Then
                                Console.WriteLine("Error Actualizando BFREP MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                'ERROR DE ACTUALIZADO
                            Else
                                Console.WriteLine("BFREP MySQL Actualizado")
                            End If
                        End If
                        r += 1
                    Next
                Else
                    wsResult = "Sin Novedad"
                    Console.WriteLine(wsResult)

                    data = Nothing
                    data = New BFPARAM
                    With data
                        .CCUDC2 = "0"
                        .CCNUM = dtBFPARAM.Rows(0).Item("CCNUM")
                        .CCNOT1 = wsResult
                    End With
                    'FINALIZA PROCESO BLOQUE
                    continuar = datos.ActualizarBFPARAMRow(data, 7)
                    If continuar = True Then
                        Console.WriteLine("...FIN ACTUALIZACION PR...")
                    End If
                    Return
                End If
                wsResult = "Registros actualizados: " & r
                Console.WriteLine(wsResult)
                Console.WriteLine("...FIN ACTUALIZACION PR...")
                Console.WriteLine(" ")
                Console.WriteLine("...ACTUALIZACIONPRD...")
                Console.WriteLine(" ")
                actualizacionPRD(dtBFPARAM.Rows(0).Item("CCNUM"))

            End If
        End If
    End Sub

    Private Sub actualizacionPRD(ByVal CCNUM As String)
        Dim r As Integer = 0
        Dim parametro(0) As String
        Dim dtBFREPD As DataTable
        Dim dtFSO As DataTable
        Dim continuar As Boolean

        'SEGUNDO BLOQUE PRD
        Console.WriteLine("Consultando BFREPD MySQL...")
        dtBFREPD = datos.ConsultarBFREPDdt(parametro, 0)
        If dtBFREPD IsNot Nothing Then
            Console.WriteLine("Recorriendo BFREPD MySQL...")
            For Each row As DataRow In dtBFREPD.Rows
                parametro(0) = row("TORD")
                Console.WriteLine("Consultando FSO AS400...")
                dtFSO = datosas400.ConsultarFSOdt(parametro, 0)
                Console.WriteLine("FSO AS400 Consultado")
                If dtFSO IsNot Nothing Then
                    data = Nothing
                    data = New BFREP
                    With data
                        .TQRECB = dtFSO.Rows(0).Item("SQFIN")
                        .TQREO = dtFSO.Rows(0).Item("SQREQ")
                        .TORD = row("TORD")
                    End With
                    Console.WriteLine("Actualizando BFREP MySQL...")
                    continuar = datos.ActualizarBFREPRow(data, 0)
                    If continuar = False Then
                        Console.WriteLine("Error Actualizando BFREP MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                        'ERROR DE ACTUALIZADO
                    Else
                        Console.WriteLine("BFREP MySQL Actualizado")
                    End If
                End If
                r += 1
            Next
        Else
            wsResult = "Sin Novedad"
            Console.WriteLine(wsResult)

            data = Nothing
            data = New BFPARAM
            With data
                .CCUDC2 = "0"
                .CCNUM = CCNUM
                .CCNOT1 = wsResult
            End With
            'FINALIZA PROCESO BLOQUE
            continuar = datos.ActualizarBFPARAMRow(data, 7)
            If continuar = True Then
                Console.WriteLine("...FIN ACTUALIZACION PRD...")
            End If

            Return
        End If
        wsResult = "Registros actualizados: " & r
        Console.WriteLine(wsResult)

        Console.WriteLine("ACTUALIZANDO BFPARAM: " & CCNUM)

        data = Nothing
        data = New BFPARAM
        With data
            .CCUDC2 = "0"
            .CCNUM = CCNUM
            .CCNOT1 = wsResult
        End With
        'FINALIZA PROCESO BLOQUE
        continuar = datos.ActualizarBFPARAMRow(data, 7)
        If continuar = True Then
            Console.WriteLine("...FIN ACTUALIZACION PRD...")
        End If
    End Sub

#End Region

#Region "BLOQUE TRASANCION ACERO R ACERO"

    Private Sub TransaccionAceroOLD()
        Dim parametro(2) As String
        Dim dtBFPARAM As DataTable
        Dim continuar As Boolean
        Dim cons As String
        Dim batch As String = datos.CalcConsec("CIMNUM")
        Dim canti As Double
        Dim canti1 As Double
        Dim dtBFREPACE As DataTable
        Dim dtCIC As DataTable

        'VALIDA QUE NO ESTE CORRIENDO EL PROCESO ACTUALMENTE
        parametro(0) = "TareasBellota"
        parametro(1) = "Transaccion Acero"
        parametro(2) = "0"
        dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 5)
        If dtBFPARAM IsNot Nothing Then
            data = Nothing
            data = New BFPARAM
            With data
                .CCUDC2 = "1"
                .CCNUM = dtBFPARAM.Rows(0).Item("CCNUM")
                .CCNOT1 = "Inicia Rutina"
            End With
            'INICIA PROCESO
            continuar = datos.ActualizarBFPARAMRow(data, 7)
            If continuar = True Then
                'PRIMER BLOQUE TRANSACCION ACERO
                Console.WriteLine("Consultando BFREPACE MySQL...")
                parametro(0) = "REPORTE"
                dtBFREPACE = datos.ConsultarBFREPACEdt(parametro, 0)
                If dtBFREPACE IsNot Nothing Then
                    Console.WriteLine("Recorriendo BFREPACE MySQL...")
                    For Each row As DataRow In dtBFREPACE.Rows

                        cons = datos.CalcConsec("CIMNUM")
                        data = Nothing
                        data = New BFCIM
                        With data
                            .IFUENTE = "PRODUCCION"                         '//15A
                            .IBATCH = batch                                 '//8"
                            .INID = "IN"                                    '//2A    Record Identifier
                            .INTRAN = "WT"                                  '//2A    Transaction Type
                            .INCTNR = 0                                     '//2A    Transaction Type"
                            .INMFGR = 0                                     '//2A    Transaction Type"
                            .INDATE = datosas400.DB2_DATEDEC             '//8P0   Date		"
                            .INTRN = Right(cons, 5)                         '//5P0   INTRN"
                            .INMLOT = "RP" & cons                           '//5P0   INTRN
                            .INREAS = "01"                                  '//5P0   INTRN
                            .INREF = row("CORDEN")                          '//8P0   Date
                            .INPROD = row("CPRODPT")                        '//15A   Item Number
                            .INWHSE = row("PBODPP")                         '//2A    Warehouse
                            .INLOT = row("PLOTE")                           '//10A   Lot Number
                            .INLOC = row("PLOCPP")                          '//6A    Location Code
                            canti = CDbl(row("CQREQPT"))
                            .INQTY = canti                                  '//10A   Creted by"
                            .INCBY = "COSIS001"                             '//10A   Creted by
                            .INLDT = Format(Now(), "yyyyMMdd")              '//8P0
                            .INLTM = Format(Now(), "hhmmss")                '//6P0
                            .INCDT = datosas400.DB2_DATEDEC              '//8P0   Date Created"
                            .INCTM = datosas400.DB2_TIMEDEC              '//6P0   Time Created"
                        End With
                        Console.WriteLine("Guardando BFCIM AS400...")
                        continuar = datosas400.GuardarBFCIMRow(data, 1)
                        If continuar = False Then
                            Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                            'ERROR DE GUARDADO AS400
                        Else
                            Console.WriteLine("BFCIM AS400 Guardado")
                            Console.WriteLine("Guardando BFCIM MySQL...")
                            continuar = datos.GuardarBFCIMRow(data, 1)
                            If continuar = False Then
                                Console.WriteLine("Error Guardando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                'ERROR DE GUARDADO
                            Else
                                Console.WriteLine("BFCIM MySQL Guardado")

                                Console.WriteLine("Consultando CIC MySQL...")
                                parametro(0) = row("CPRODPP")
                                dtCIC = datos.ConsultarCICdt(parametro, 0)
                                If dtCIC IsNot Nothing Then
                                    cons = datos.CalcConsec("CIMNUM")
                                    data = Nothing
                                    data = New BFCIM
                                    With data
                                        .IFUENTE = "PRODUCCION"                         '//15A
                                        .IBATCH = batch                                 '//8"
                                        .INID = "IN"                                    '//2A    Record Identifier
                                        .INTRAN = "CI"                                  '//2A    Transaction Type
                                        .INCTNR = 0                                     '//2A    Transaction Type"
                                        .INMFGR = 0                                     '//2A    Transaction Type"
                                        .INDATE = datosas400.DB2_DATEDEC             '//8P0   Date		"
                                        .INTRN = Right(cons, 5)                         '//5P0   INTRN"
                                        .INMLOT = "RP" & cons                           '//5P0   INTRN
                                        .INREAS = "01"                                  '//5P0   INTRN
                                        .INREF = row("CORDEN")                          '//8P0   Date
                                        .INPROD = row("CPRODPP")                        '//15A   Item Number
                                        .INWHSE = dtCIC.Rows(0).Item("ICPLN")           '//2A    Warehouse
                                        .INLOT = ""                                     '//10A   Lot Number"
                                        .INLOC = ""                                     '//6A    Location Code"
                                        canti1 = CDbl(row("CQREQPP"))
                                        .INQTY = canti1                                 '//10A   Creted by"
                                        .INCBY = "COSIS001"                             '//10A   Creted by
                                        .INLDT = Format(Now(), "yyyyMMdd")              '//8P0
                                        .INLTM = Format(Now(), "hhmmss")                '//6P0
                                        .INCDT = datosas400.DB2_DATEDEC              '//8P0   Date Created"
                                        .INCTM = datosas400.DB2_TIMEDEC              '//6P0   Time Created"
                                    End With
                                    Console.WriteLine("Guardando BFCIM AS400...")
                                    continuar = datosas400.GuardarBFCIMRow(data, 1)
                                    If continuar = False Then
                                        Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                                        'ERROR DE GUARDADO AS400
                                    Else
                                        Console.WriteLine("BFCIM AS400 Guardado")
                                        Console.WriteLine("Guardando BFCIM MySQL...")
                                        continuar = datos.GuardarBFCIMRow(data, 1)
                                        If continuar = False Then
                                            Console.WriteLine("Error Guardando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                            'ERROR DE GUARDADO
                                        Else
                                            Console.WriteLine("BFCIM MySQL Guardado")
                                            data = Nothing
                                            data = New BFREP
                                            With data
                                                .TQREQACEB = canti
                                                .TNUMREP = row("CNUMREP")
                                            End With
                                            Console.WriteLine("Actualizando BFREP MySQL...")
                                            continuar = datos.ActualizarBFREPRow(data, 1)
                                            If continuar = False Then
                                                Console.WriteLine("Error Actualizando BFREP MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                                'ERROR DE ACTUALIZADO
                                            Else
                                                Console.WriteLine("BFREP MySQL Actualizado")
                                                data = Nothing
                                                data = New BFREP
                                                With data
                                                    .TRPODACEB = canti1
                                                    .TNUMREP = row("CNUMREP")
                                                End With
                                                Console.WriteLine("Actualizando BFREP MySQL...")
                                                continuar = datos.ActualizarBFREPRow(data, 3)
                                                If continuar = False Then
                                                    Console.WriteLine("Error Actualizando BFREP MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                                    'ERROR DE ACTUALIZADO
                                                Else
                                                    Console.WriteLine("BFREP MySQL Actualizado")
                                                    data = Nothing
                                                    data = New BFREPACE
                                                    With data
                                                        .CENVIADO = "1"
                                                        .WRKID = row("WRKID")
                                                    End With
                                                    Console.WriteLine("Actualizando BFREPACE MySQL...")
                                                    continuar = datos.ActualizarBFREPACERow(data, 0)
                                                    If continuar = False Then
                                                        Console.WriteLine("Error Actualizando BFREPACE MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                                        'ERROR DE ACTUALIZADO
                                                    Else
                                                        Console.WriteLine("BFREPACE MySQL Actualizado")
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                Else

                                    Console.WriteLine("BFREP MySQL Actualizado")
                                    data = Nothing
                                    data = New BFREP
                                    With data
                                        .TRPODACEB = canti1
                                        .TNUMREP = row("CNUMREP")
                                    End With
                                    Console.WriteLine("Actualizando BFREP MySQL...")
                                    continuar = datos.ActualizarBFREPRow(data, 3)
                                    If continuar = False Then
                                        Console.WriteLine("Error Actualizando BFREP MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                        'ERROR DE ACTUALIZADO
                                    Else
                                        Console.WriteLine("BFREP MySQL Actualizado")
                                        data = Nothing
                                        data = New BFREPACE
                                        With data
                                            .CENVIADO = "1"
                                            .WRKID = row("WRKID")
                                        End With
                                        Console.WriteLine("Actualizando BFREPACE MySQL...")
                                        continuar = datos.ActualizarBFREPACERow(data, 0)
                                        If continuar = False Then
                                            Console.WriteLine("Error Actualizando BFREPACE MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                            'ERROR DE ACTUALIZADO
                                        Else
                                            Console.WriteLine("BFREPACE MySQL Actualizado")
                                        End If
                                    End If

                                End If
                            End If
                        End If
                    Next
                Else
                    wsResult = "Sin Novedad"
                    Console.WriteLine(wsResult)

                    'data = Nothing
                    'data = New BFPARAM
                    'With data
                    '    .CCUDC2 = "0"
                    '    .CCNUM = dtBFPARAM.Rows(0).Item("CCNUM")
                    '    .CCNOT1 = wsResult
                    'End With
                    ''FINALIZA PROCESO BLOQUES
                    'continuar = datos.ActualizarBFPARAMRow(data, 7)
                    'If continuar = True Then
                    '    Console.WriteLine("...FIN TRANSACCION ACERO...")
                    '    parametro(0) = "TareasBellota"
                    '    parametro(1) = "Transaccion R Acero"
                    '    parametro(2) = "0"
                    '    dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 5)
                    '    If dtBFPARAM IsNot Nothing Then
                    '        data = Nothing
                    '        data = New BFPARAM
                    '        With data
                    '            .CCUDC2 = "0"
                    '            .CCNUM = dtBFPARAM.Rows(0).Item("CCNUM")
                    '            .CCNOT1 = wsResult
                    '        End With
                    '        'FINALIZA PROCESO BLOQUES
                    '        continuar = datos.ActualizarBFPARAMRow(data, 7)
                    '    End If
                    'End If
                    'Return


                    registroWS("Operacion", "Transaccion R Acero")
                    Console.WriteLine(" ")
                    Console.WriteLine("TransaccionRAcero...")
                    Console.WriteLine(" ")
                    TransaccionRAcero(dtBFPARAM.Rows(0).Item("CCNUM"))
                End If
                Console.WriteLine("Llamando Programa BCTRAINVB " & batch & "', 'PROD")
                datosas400.LlamaPrograma("BCTRAINVB", batch & "', 'PROD")
                wsResult = "Transacciones enviadas"
                Console.WriteLine(wsResult)

                Console.WriteLine("...FIN TRANSACCION ACERO...")

                registroWS("Operacion", "Transaccion R Acero")
                Console.WriteLine(" ")
                Console.WriteLine("TransaccionRAcero...")
                Console.WriteLine(" ")
                TransaccionRAcero(dtBFPARAM.Rows(0).Item("CCNUM"))
            End If
        End If
    End Sub

    Private Sub TransaccionAcero()
        Dim parametro(2) As String
        Dim dtBFPARAM As DataTable
        Dim continuar As Boolean
        Dim cons As String
        Dim batch As String
        Dim canti As Double
        Dim canti1 As Double
        Dim dtBFREPACE As DataTable
        Dim dtCIC As DataTable

        'VALIDA QUE NO ESTE CORRIENDO EL PROCESO ACTUALMENTE
        parametro(0) = "TareasBellota"
        parametro(1) = "Transaccion Acero"
        parametro(2) = "0"
        dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 5)
        If dtBFPARAM IsNot Nothing Then
            data = Nothing
            data = New BFPARAM
            With data
                .CCUDC2 = "1"
                .CCNUM = dtBFPARAM.Rows(0).Item("CCNUM")
                .CCNOT1 = "Inicia Rutina"
            End With
            'INICIA PROCESO
            continuar = datos.ActualizarBFPARAMRow(data, 7)
            If continuar = True Then

                'PRIMER BLOQUE TRANSACCION ACERO
                Console.WriteLine("Consultando BFREPACE MySQL...")
                parametro(0) = "REPORTE"
                dtBFREPACE = datos.ConsultarBFREPACEdt(parametro, 0)
                If dtBFREPACE IsNot Nothing Then
                    batch = datos.CalcConsec("CIMNUM")
                    Console.WriteLine("Recorriendo BFREPACE MySQL...")
                    For Each row As DataRow In dtBFREPACE.Rows

                        Console.WriteLine("Consultando CIC MySQL...")
                        parametro(0) = row("CPRODPP")
                        dtCIC = datos.ConsultarCICdt(parametro, 0)
                        If dtCIC IsNot Nothing Then
                            cons = datos.CalcConsec("CIMNUM")
                            data = Nothing
                            data = New BFCIM
                            With data
                                .IFUENTE = "PRODUCCION"                         '//15A
                                .IBATCH = batch                                 '//8"
                                .INID = "IN"                                    '//2A    Record Identifier
                                .INTRAN = "WT"                                  '//2A    Transaction Type
                                .INCTNR = 0                                     '//2A    Transaction Type"
                                .INMFGR = 0                                     '//2A    Transaction Type"
                                .INDATE = datosas400.DB2_DATEDEC             '//8P0   Date		"
                                .INTRN = Right(cons, 5)                         '//5P0   INTRN"
                                .INMLOT = "RP" & cons                           '//5P0   INTRN
                                .INREAS = "01"                                  '//5P0   INTRN
                                .INREF = row("CORDEN")                          '//8P0   Date
                                .INPROD = row("CPRODPT")                        '//15A   Item Number
                                .INWHSE = row("PBODPP")                         '//2A    Warehouse
                                .INLOT = row("PLOTE")                           '//10A   Lot Number
                                .INLOC = row("PLOCPP")                          '//6A    Location Code
                                canti = CDbl(row("CQREQPT"))
                                .INQTY = canti                                  '//10A   Creted by"
                                .INCBY = "COSIS001"                             '//10A   Creted by
                                .INLDT = Format(Now(), "yyyyMMdd")              '//8P0
                                .INLTM = Format(Now(), "hhmmss")                '//6P0
                                .INCDT = datosas400.DB2_DATEDEC              '//8P0   Date Created"
                                .INCTM = datosas400.DB2_TIMEDEC              '//6P0   Time Created"
                            End With
                            Console.WriteLine("Guardando BFCIM AS400...")
                            continuar = datosas400.GuardarBFCIMRow(data, 1)
                            If continuar = False Then
                                Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                                'ERROR DE GUARDADO AS400
                            Else
                                Console.WriteLine("BFCIM AS400 Guardado")
                                Console.WriteLine("Guardando BFCIM MySQL...")
                                continuar = datos.GuardarBFCIMRow(data, 1)
                                If continuar = False Then
                                    Console.WriteLine("Error Guardando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                    'ERROR DE GUARDADO
                                Else
                                    Console.WriteLine("BFCIM MySQL Guardado")
                                End If
                            End If

                            cons = datos.CalcConsec("CIMNUM")
                            data = Nothing
                            data = New BFCIM
                            With data
                                .IFUENTE = "PRODUCCION"                         '//15A
                                .IBATCH = batch                                 '//8"
                                .INID = "IN"                                    '//2A    Record Identifier
                                .INTRAN = "CI"                                  '//2A    Transaction Type
                                .INCTNR = 0                                     '//2A    Transaction Type"
                                .INMFGR = 0                                     '//2A    Transaction Type"
                                .INDATE = datosas400.DB2_DATEDEC             '//8P0   Date		"
                                .INTRN = Right(cons, 5)                         '//5P0   INTRN"
                                .INMLOT = "RP" & cons                           '//5P0   INTRN
                                .INREAS = "01"                                  '//5P0   INTRN
                                .INREF = row("CORDEN")                          '//8P0   Date
                                .INPROD = row("CPRODPP")                        '//15A   Item Number
                                .INWHSE = dtCIC.Rows(0).Item("ICPLN")           '//2A    Warehouse
                                .INLOT = ""                                     '//10A   Lot Number"
                                .INLOC = ""                                     '//6A    Location Code"
                                canti1 = CDbl(row("CQREQPP"))
                                .INQTY = canti1                                 '//10A   Creted by"
                                .INCBY = "COSIS001"                             '//10A   Creted by
                                .INLDT = Format(Now(), "yyyyMMdd")              '//8P0
                                .INLTM = Format(Now(), "hhmmss")                '//6P0
                                .INCDT = datosas400.DB2_DATEDEC              '//8P0   Date Created"
                                .INCTM = datosas400.DB2_TIMEDEC              '//6P0   Time Created"
                            End With
                            Console.WriteLine("Guardando BFCIM AS400...")
                            continuar = datosas400.GuardarBFCIMRow(data, 1)
                            If continuar = False Then
                                Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                                'ERROR DE GUARDADO AS400
                            Else
                                Console.WriteLine("BFCIM AS400 Guardado")
                                Console.WriteLine("Guardando BFCIM MySQL...")
                                continuar = datos.GuardarBFCIMRow(data, 1)
                                If continuar = False Then
                                    Console.WriteLine("Error Guardando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                    'ERROR DE GUARDADO
                                Else
                                    Console.WriteLine("BFCIM MySQL Guardado")
                                    data = Nothing
                                    data = New BFREP
                                    With data
                                        .TQREQACEB = canti
                                        .TNUMREP = row("CNUMREP")
                                    End With
                                    Console.WriteLine("Actualizando BFREP MySQL...")
                                    continuar = datos.ActualizarBFREPRow(data, 1)
                                    If continuar = False Then
                                        Console.WriteLine("Error Actualizando BFREP MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                        'ERROR DE ACTUALIZADO
                                    Else
                                        Console.WriteLine("BFREP MySQL Actualizado")
                                        data = Nothing
                                        data = New BFREP
                                        With data
                                            .TRPODACEB = canti1
                                            .TNUMREP = row("CNUMREP")
                                        End With
                                        Console.WriteLine("Actualizando BFREP MySQL...")
                                        continuar = datos.ActualizarBFREPRow(data, 3)
                                        If continuar = False Then
                                            Console.WriteLine("Error Actualizando BFREP MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                            'ERROR DE ACTUALIZADO
                                        Else
                                            Console.WriteLine("BFREP MySQL Actualizado")
                                            data = Nothing
                                            data = New BFREPACE
                                            With data
                                                .CENVIADO = "1"
                                                .WRKID = row("WRKID")
                                            End With
                                            Console.WriteLine("Actualizando BFREPACE MySQL...")
                                            continuar = datos.ActualizarBFREPACERow(data, 0)
                                            If continuar = False Then
                                                Console.WriteLine("Error Actualizando BFREPACE MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                                'ERROR DE ACTUALIZADO
                                            Else
                                                Console.WriteLine("BFREPACE MySQL Actualizado")
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        Else
                            data = Nothing
                            data = New BFLOG
                            With data
                                .USUARIO = Net.Dns.GetHostName()
                                .OPERACION = "Consultar CIC (Bodega)"
                                .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                                .EVENTO = "No se encontro el producto " & row("CPRODPP")
                                .TXTSQL = datos.fSql
                                .ALERT = 1
                            End With
                            datos.GuardarBFLOGRow(data)
                        End If
                    Next
                    Console.WriteLine("Llamando Programa BCTRAINVB " & batch & "', 'PROD")
                    datosas400.LlamaPrograma("BCTRAINVB", batch & "', 'PROD")
                    wsResult = "Transacciones enviadas"
                    Console.WriteLine(wsResult)
                    Console.WriteLine("...FIN TRANSACCION ACERO...")
                Else
                    wsResult = "Sin Novedad"
                    Console.WriteLine(wsResult)
                    registroWS("Operacion", "Transaccion R Acero")
                    Console.WriteLine(" ")
                    Console.WriteLine("TransaccionRAcero...")
                    Console.WriteLine(" ")
                    TransaccionRAcero(dtBFPARAM.Rows(0).Item("CCNUM"))
                End If


                registroWS("Operacion", "Transaccion R Acero")
                Console.WriteLine(" ")
                Console.WriteLine("TransaccionRAcero...")
                Console.WriteLine(" ")
                TransaccionRAcero(dtBFPARAM.Rows(0).Item("CCNUM"))
            End If
        End If
    End Sub

    Private Sub TransaccionRAcero(ByVal CCNUM As String)
        Dim canti As Double
        Dim canti1 As Double
        Dim cons As String
        Dim Bodega As String = ""
        Dim batch As String
        Dim parametro(2) As String
        Dim dtBFREPACE As DataTable
        Dim dtBFREPACE1 As DataTable
        Dim dtBFREP As DataTable
        Dim dtCIC As DataTable
        Dim continuar As Boolean
        Dim dtBFPARAM As DataTable

        'SEGUNDO BLOQUE TRANSANCCION R ACERO
        Console.WriteLine("Consultando BFREPACE MySQL...")
        parametro(0) = "REVERSION"
        dtBFREPACE = datos.ConsultarBFREPACEdt(parametro, 0)
        If dtBFREPACE IsNot Nothing Then
            batch = datos.CalcConsec("CIMNUM")
            'Recibe
            '1. Transacc CI con el Acero (CPRODPP) con la Orden que va a recibir el acero con cantidad del acero positiva
            '2. Transacc WT con el prod terminado (CPRODPT) con la Orden que va a recibir con cantidad del producto terminado positiva

            'Saca
            '3. Transacc CI con el Acero (el mismo) con la Orden desde donde sale con cantidad negativa del acero
            '4. Transacc WT con el prod terminado de la Orden desde donde sale con cantidad negativa del producto terminado	
            Console.WriteLine("Recorriendo BFREPACE MySQL...")
            For Each row As DataRow In dtBFREPACE.Rows
                Console.WriteLine("Consultando BFREP MySQL...")
                parametro(0) = row("CORDEN")
                dtBFREP = datos.ConsultarBFREPdt(parametro, 1)
                If dtBFREP IsNot Nothing Then
                    '1. Transacc CI con el ACERO (CPRODPP) con la Orden que va a recibir el acero con cantidad del acero positiva
                    Console.WriteLine("Consultando CIC MySQL...")
                    parametro(0) = row("CPRODPP")
                    dtCIC = datos.ConsultarCICdt(parametro, 0)
                    If dtCIC IsNot Nothing Then
                        Bodega = dtCIC.Rows(0).Item("ICPLN")
                    End If
                    If Bodega <> "" Then
                        cons = datos.CalcConsec("CIMNUM")
                        data = Nothing
                        data = New BFCIM
                        With data
                            .IFUENTE = "PRODUCCION"                         '//15A
                            .IBATCH = batch                                 '//8"
                            .INID = "IN"                                    '//2A    Record Identifier
                            .INTRAN = "CI"                                  '//2A    Transaction Type
                            .INCTNR = 0                                     '//2A    Transaction Type"
                            .INMFGR = 0                                     '//2A    Transaction Type"
                            .INDATE = datosas400.DB2_DATEDEC             '//8P0   Date		"
                            .INTRN = Right(cons, 5)                         '//5P0   INTRN"
                            .INMLOT = "RP" & cons                           '//5P0   INTRN
                            .INREAS = "01"                                  '//5P0   INTRN
                            .INREF = dtBFREP.Rows(0).Item("TORD")           '//8P0   Date
                            .INPROD = row("CPRODPP")                        '//15A   Item Number
                            .INWHSE = Bodega                                '//2A    Warehouse"
                            .INLOT = ""                                     '//10A   Lot Number"
                            .INLOC = ""                                     '//6A    Location Code"
                            canti1 = CDbl(row("CQREQPP"))
                            .INQTY = canti1                                 '//10A   Creted by"
                            .INCBY = "COSIS001"                             '//10A   Creted by
                            .INLDT = Format(Now(), "yyyyMMdd")              '//8P0
                            .INLTM = Format(Now(), "hhmmss")                '//6P0
                            .INCDT = datosas400.DB2_DATEDEC              '//8P0   Date Created"
                            .INCTM = datosas400.DB2_TIMEDEC              '//6P0   Time Created"
                        End With
                        Console.WriteLine("Guardando BFCIM AS400...")
                        continuar = datosas400.GuardarBFCIMRow(data, 1)
                        If continuar = False Then
                            Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                            'ERROR DE GUARDADO
                        Else
                            Console.WriteLine("BFCIM AS400 Guardado")
                            Console.WriteLine("Guardando BFCIM MySQL...")
                            continuar = datos.GuardarBFCIMRow(data, 1)
                            If continuar = False Then
                                Console.WriteLine("Error Guardando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                'ERROR DE GUARDADO
                            Else
                                Console.WriteLine("BFCIM MySQL Guardado")
                                '2. Transacc WT con el prod terminado (CPRODPT) con la Orden que va a recibir con cantidad del producto terminado positiva
                                cons = datos.CalcConsec("CIMNUM")
                                data = Nothing
                                data = New BFCIM
                                With data
                                    .IFUENTE = "PRODUCCION"                         '//15A
                                    .IBATCH = batch                                 '//8"
                                    .INID = "IN"                                    '//2A    Record Identifier
                                    .INTRAN = "WT"                                  '//2A    Transaction Type
                                    .INCTNR = 0                                     '//2A    Transaction Type"
                                    .INMFGR = 0                                     '//2A    Transaction Type"
                                    .INDATE = datosas400.DB2_DATEDEC             '//8P0   Date		"
                                    .INTRN = Right(cons, 5)                         '//5P0   INTRN"
                                    .INMLOT = "RP" & cons                           '//5P0   INTRN
                                    .INREAS = "01"                                  '//5P0   INTRN
                                    .INREF = dtBFREP.Rows(0).Item("TORD")           '//8P0   Date
                                    .INPROD = row("CPRODPT")                        '//15A   Item Number
                                    .INWHSE = dtBFREP.Rows(0).Item("PBODPP")        '//2A    Warehouse
                                    .INLOT = dtBFREP.Rows(0).Item("PLOTE")          '//10A   Lot Number
                                    .INLOC = dtBFREP.Rows(0).Item("PLOCPP")         '//6A    Location Code
                                    canti = CDbl(row("CQREQPT"))
                                    .INQTY = canti                                  '//10A   Creted by"
                                    .INCBY = "COSIS001"                             '//10A   Creted by
                                    .INLDT = Format(Now(), "yyyyMMdd")              '//8P0
                                    .INLTM = Format(Now(), "hhmmss")                '//6P0
                                    .INCDT = datosas400.DB2_DATEDEC              '//8P0   Date Created"
                                    .INCTM = datosas400.DB2_TIMEDEC              '//6P0   Time Created"
                                End With
                                Console.WriteLine("Guardando BFCIM AS400...")
                                continuar = datosas400.GuardarBFCIMRow(data, 1)
                                If continuar = False Then
                                    Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                                    'ERROR AL GUARDAR
                                Else
                                    Console.WriteLine("BFCIM AS400 Guardado")
                                    Console.WriteLine("Guardando BFCIM MySQL...")
                                    continuar = datos.GuardarBFCIMRow(data, 1)
                                    If continuar = False Then
                                        Console.WriteLine("Error Guardando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                        'ERROR AL GUARDAR
                                    Else
                                        Console.WriteLine("BFCIM MySQL Guardado")
                                    End If
                                End If
                            End If
                        End If
                    Else
                        data = Nothing
                        data = New BFLOG
                        With data
                            .USUARIO = System.Net.Dns.GetHostName()
                            .OPERACION = "Consultar CIC (Bodega)"
                            .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                            .EVENTO = "No se encontro el producto " & row("CPRODPP")
                            .TXTSQL = datos.fSql
                            .ALERT = 1
                        End With
                        datos.GuardarBFLOGRow(data)
                    End If
                End If
                Dim aReversar() As String
                aReversar = Split(row("CORDENR"), "-")
                Console.WriteLine("Consultando BFREPACE MySQL...")
                parametro(0) = aReversar(1)
                dtBFREPACE1 = datos.ConsultarBFREPACEdt(parametro, 1)
                If dtBFREPACE1 IsNot Nothing Then
                    '3. Transacc CI con el Acero (el mismo) con la Orden desde donde sale con cantidad negativa del acero
                    Bodega = ""
                    Console.WriteLine("Consultando CIC MySQL...")
                    parametro(0) = row("CPRODPP")
                    dtCIC = datos.ConsultarCICdt(parametro, 0)
                    If dtCIC IsNot Nothing Then
                        Bodega = dtCIC.Rows(0).Item("ICPLN")
                    End If
                    If Bodega <> "" Then
                        cons = datos.CalcConsec("CIMNUM")
                        data = Nothing
                        data = New BFCIM
                        With data
                            .IFUENTE = "PRODUCCION"                                 '//15A
                            .IBATCH = batch                                         '//8"
                            .INID = "IN"                                            '//2A    Record Identifier
                            .INTRAN = "CI"                                          '//2A    Transaction Type
                            .INCTNR = 0                                             '//2A    Transaction Type"
                            .INMFGR = 0                                             '//2A    Transaction Type"
                            .INDATE = datosas400.DB2_DATEDEC                     '//8P0   Date		"
                            .INTRN = Right(cons, 5)                                 '//5P0   INTRN"
                            .INMLOT = "RP" & cons                                   '//5P0   INTRN
                            .INREAS = "01"                                          '//5P0   INTRN
                            .INREF = dtBFREPACE1.Rows(0).Item("CORDEN")             '//8P0   Date
                            .INPROD = dtBFREPACE1.Rows(0).Item("CPRODPP")           '//15A   Item Number
                            .INWHSE = Bodega                                        '//2A    Warehouse"
                            .INLOT = ""                                             '//10A   Lot Number"
                            .INLOC = ""                                             '//6A    Location Code"
                            .INQTY = -canti1                                        '//10A   Creted by"
                            .INCBY = "COSIS001"                                     '//10A   Creted by
                            .INLDT = Format(Now(), "yyyyMMdd")                      '//8P0
                            .INLTM = Format(Now(), "hhmmss")                        '//6P0
                            .INCDT = datosas400.DB2_DATEDEC                      '//8P0   Date Created"
                            .INCTM = datosas400.DB2_TIMEDEC                      '//6P0   Time Created"
                        End With
                        Console.WriteLine("Guardando BFCIM AS400...")
                        continuar = datosas400.GuardarBFCIMRow(data, 1)
                        If continuar = False Then
                            Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                            'ERROR DE GUARDADO
                        Else
                            Console.WriteLine("BFCIM AS400 Guardado")
                            Console.WriteLine("Guardando BFCIM MySQL...")
                            continuar = datos.GuardarBFCIMRow(data, 1)
                            If continuar = False Then
                                Console.WriteLine("Error Guardando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                'ERROR DE GUARDADO
                            Else
                                Console.WriteLine("BFCIM MySQL Guardado")
                                '4. Transacc WT con el prod terminado de la Orden desde donde sale con cantidad negativa del producto terminado
                                cons = datos.CalcConsec("CIMNUM")
                                data = Nothing
                                data = New BFCIM
                                With data
                                    .IFUENTE = "PRODUCCION"                                 '//15A
                                    .IBATCH = batch                                         '//8"
                                    .INID = "IN"                                            '//2A    Record Identifier
                                    .INTRAN = "WT"                                          '//2A    Transaction Type
                                    .INCTNR = 0                                             '//2A    Transaction Type"
                                    .INMFGR = 0                                             '//2A    Transaction Type"
                                    .INDATE = datosas400.DB2_DATEDEC                     '//8P0   Date		"
                                    .INTRN = Right(cons, 5)                                 '//5P0   INTRN"
                                    .INMLOT = "RP" & cons                                   '//5P0   INTRN
                                    .INREAS = "01"                                          '//5P0   INTRN
                                    .INREF = dtBFREPACE1.Rows(0).Item("CORDEN")             '//8P0   Date
                                    .INPROD = dtBFREPACE1.Rows(0).Item("CPRODPT")           '//15A   Item Number
                                    .INWHSE = dtBFREPACE1.Rows(0).Item("PBODPP")            '//2A    Warehouse
                                    .INLOT = dtBFREPACE1.Rows(0).Item("PLOTE")              '//10A   Lot Number
                                    .INLOC = dtBFREPACE1.Rows(0).Item("PLOCPP")             '//6A    Location Code
                                    .INQTY = -canti                                         '//10A   Creted by"
                                    .INCBY = "COSIS001"                                     '//10A   Creted by
                                    .INLDT = Format(Now(), "yyyyMMdd")                      '//8P0
                                    .INLTM = Format(Now(), "hhmmss")                        '//6P0
                                    .INCDT = datosas400.DB2_DATEDEC                      '//8P0   Date Created"
                                    .INCTM = datosas400.DB2_TIMEDEC                      '//6P0   Time Created"
                                End With
                                Console.WriteLine("Guardando BFCIM AS400...")
                                continuar = datosas400.GuardarBFCIMRow(data, 1)
                                If continuar = False Then
                                    Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                                    'ERROR DE GUARDADO
                                Else
                                    Console.WriteLine("BFCIM AS400 Guardado")
                                    Console.WriteLine("Guardando BFCIM MySQL...")
                                    continuar = datos.GuardarBFCIMRow(data, 1)
                                    If continuar = False Then
                                        Console.WriteLine("Error Guardando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                        'ERROR DE GUARDADO
                                    Else
                                        Console.WriteLine("BFCIM MySQL Guardado")
                                        data = Nothing
                                        data = New BFREPACE
                                        With data
                                            .CENVIADO = "1"
                                            .WRKID = row("WRKID")
                                        End With
                                        Console.WriteLine("Actualizando BFREPACE MySQL...")
                                        continuar = datos.ActualizarBFREPACERow(data, 0)
                                        If continuar = False Then
                                            Console.WriteLine("Error Actualizando BFREPACE MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                            'ERROR DE ACTUALIZADO
                                        Else
                                            Console.WriteLine("BFREPACE MySQL Actualizado")
                                            data = Nothing
                                            data = New BFREP
                                            With data
                                                .TQREQACEB = canti
                                                .TNUMREP = row("CNUMREP")
                                            End With
                                            Console.WriteLine("Actualizando BFREP MySQL...")
                                            continuar = datos.ActualizarBFREPRow(data, 1)
                                            If continuar = False Then
                                                Console.WriteLine("Error Actualizando BFREP MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                                'ERROR DE ACTUALIZADO
                                            Else
                                                Console.WriteLine("BFREP MySQL Actualizado")
                                                data = Nothing
                                                data = New BFREP
                                                With data
                                                    .TRPODACEB = canti1
                                                    .TNUMREP = row("CNUMREP")
                                                End With
                                                Console.WriteLine("Actualizando BFREP MySQL...")
                                                continuar = datos.ActualizarBFREPRow(data, 3)
                                                If continuar = False Then
                                                    Console.WriteLine("Error Actualizando BFREP MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                                    'ERROR DE ACTUALIZADO
                                                Else
                                                    Console.WriteLine("BFREP MySQL Actualizado")
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    Else
                        data = Nothing
                        data = New BFLOG
                        With data
                            .USUARIO = Net.Dns.GetHostName()
                            .OPERACION = "Consultar CIC (Bodega)"
                            .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                            .EVENTO = "No se encontro el producto " & row("CPRODPP")
                            .TXTSQL = datos.fSql
                            .ALERT = 1
                        End With
                        datos.GuardarBFLOGRow(data)
                    End If

                End If
            Next
            Console.WriteLine("Llamando Programa BCTRAINVB... " & batch & "', 'PROD")
            datosas400.LlamaPrograma("BCTRAINVB", batch & "', 'PROD")
            wsResult = "Transacciones enviadas"
            Console.WriteLine(wsResult)
        Else
            wsResult = "Sin Novedad"
            Console.WriteLine(wsResult)

            data = Nothing
            data = New BFPARAM
            With data
                .CCUDC2 = "0"
                .CCNUM = CCNUM
                .CCNOT1 = wsResult
            End With
            'FINALIZA PROCESO BLOQUES
            continuar = datos.ActualizarBFPARAMRow(data, 7)
            If continuar = True Then
                Console.WriteLine("...FIN TRANSACCION R ACERO...")
            End If
            parametro(0) = "TareasBellota"
            parametro(1) = "Transaccion R Acero"
            parametro(2) = "0"
            dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 5)
            If dtBFPARAM IsNot Nothing Then
                data = Nothing
                data = New BFPARAM
                With data
                    .CCUDC2 = "0"
                    .CCNUM = dtBFPARAM.Rows(0).Item("CCNUM")
                    .CCNOT1 = wsResult
                End With
                'FINALIZA PROCESO BLOQUES
                continuar = datos.ActualizarBFPARAMRow(data, 7)
            End If
            Return
        End If
        Console.WriteLine("ACTUALIZANDO BFPARAM: " & CCNUM)

        data = Nothing
        data = New BFPARAM
        With data
            .CCUDC2 = "0"
            .CCNUM = CCNUM
            .CCNOT1 = wsResult
        End With
        'FINALIZA PROCESO BLOQUES
        continuar = datos.ActualizarBFPARAMRow(data, 7)
        If continuar = True Then
            Console.WriteLine("...FIN TRANSACCION R ACERO...")
            parametro(0) = "TareasBellota"
            parametro(1) = "Transaccion R Acero"
            parametro(2) = "0"
            dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 5)
            If dtBFPARAM IsNot Nothing Then
                data = Nothing
                data = New BFPARAM
                With data
                    .CCUDC2 = "0"
                    .CCNUM = dtBFPARAM.Rows(0).Item("CCNUM")
                    .CCNOT1 = wsResult
                End With
                'FINALIZA PROCESO BLOQUES
                continuar = datos.ActualizarBFPARAMRow(data, 7)
            End If
        End If
    End Sub

#End Region

    Private Sub TransaccionRJ()
        Dim parametro(2) As String
        Dim dtBFPARAM As DataTable
        Dim continuar As Boolean
        Dim canti As Double
        Dim batch As String
        Dim cons As String
        Dim dtBFREP As DataTable

        'VALIDA QUE NO ESTE CORRIENDO EL PROCESO ACTUALMENTE
        parametro(0) = "TareasBellota"
        parametro(1) = "Transaccion RJ"
        parametro(2) = "0"
        dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 5)
        If dtBFPARAM IsNot Nothing Then
            data = Nothing
            data = New BFPARAM
            With data
                .CCUDC2 = "1"
                .CCNUM = dtBFPARAM.Rows(0).Item("CCNUM")
                .CCNOT1 = "Inicia Rutina"
            End With
            'INICIA PROCESO
            continuar = datos.ActualizarBFPARAMRow(data, 7)
            If continuar = True Then

                Console.WriteLine("Consultando BFREP MySQL...")
                dtBFREP = datos.ConsultarBFREPdt(parametro, 2)
                If dtBFREP IsNot Nothing Then
                    batch = datos.CalcConsec("CIMNUM")
                    Console.WriteLine("Recorriendo BFREP MySQL...")
                    For Each row As DataRow In dtBFREP.Rows
                        cons = datos.CalcConsec("CIMNUM")
                        data = Nothing
                        data = New BFCIM
                        With data
                            .IFUENTE = "PRODUCCION"                         '//15A
                            .IBATCH = batch                                 '//8"
                            .INID = "IN"                                    '//2A    Record Identifier
                            .INTRAN = "RJ"                                  '//2A    Transaction Type
                            .INCTNR = row("RPROCESO")                       '//2A    Transaction Type
                            .INMFGR = row("RPROCESO")                       '//2A    Transaction Type
                            .INDATE = datosas400.DB2_DATEDEC             '//8P0   Date		"
                            .INTRN = Right(cons, 5)                         '//5P0   INTRN"
                            .INMLOT = "RP" & cons                           '//5P0   INTRN
                            .INREAS = row("RCAUSAL")                        '//5P0   INTRN
                            .INREF = row("TORD")                            '//8P0   Date
                            .INPROD = row("TPROD")                          '//15A   Item Number
                            .INWHSE = row("PBODPP")                         '//2A    Warehouse
                            .INLOT = row("PLOTE")                           '//10A   Lot Number
                            .INLOC = row("PLOCPP")                          '//6A    Location Code
                            canti = (CDbl(row("RQREP")))
                            .INQTY = canti                                  '//10A   Creted by"
                            .INCBY = "COSIS001"                             '//10A   Creted by
                            .INLDT = Format(Now(), "yyyyMMdd")              '//8P0
                            .INLTM = Format(Now(), "hhmmss")                '//6P0
                            .INCDT = datosas400.DB2_DATEDEC              '//8P0   Date Created"
                            .INCTM = datosas400.DB2_TIMEDEC              '//6P0   Time Created"
                        End With
                        Console.WriteLine("Guardando BFCIM AS400...")
                        continuar = datosas400.GuardarBFCIMRow(data, 1)
                        If continuar = False Then
                            Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                            'ERROR DE GUARDADO
                        Else
                            Console.WriteLine("BFCIM AS400 Guardado")
                            Console.WriteLine("Guardando BFCIM MySQL...")
                            continuar = datos.GuardarBFCIMRow(data, 1)
                            If continuar = False Then
                                Console.WriteLine("Error Guardando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                'ERROR DE GUARDADO
                            Else
                                Console.WriteLine("BFCIM MySQL Guardado")
                                data = Nothing
                                data = New BFREPTURRJ
                                With data
                                    .RENVIADO = "1"
                                    .RWRKID = row("RWRKID")
                                End With
                                Console.WriteLine("Actualizando BFREPTURRJ MySQL...")
                                continuar = datos.ActualizarBFREPTURRJRow(data, 0)
                                If continuar = False Then
                                    Console.WriteLine("Error Actualizando BFREPTURRJ MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                    'ERROR DE ACTUALIZADO
                                Else
                                    Console.WriteLine("BFREPTURRJ MySQL Actualizado")
                                    data = Nothing
                                    data = New BFREP
                                    With data
                                        .TQREDB = row("RQREP")
                                        .TNUMREP = row("TNUMREP")
                                    End With
                                    Console.WriteLine("Actualizando BFREP MySQL...")
                                    continuar = datos.ActualizarBFREPRow(data, 2)
                                    If continuar = False Then
                                        Console.WriteLine("Error Actualizando BFREP MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                        'ERROR DE ACTUALIZADO
                                    Else
                                        Console.WriteLine("BFREP MySQL Actualizado")
                                    End If
                                End If
                            End If
                        End If
                    Next
                    Console.WriteLine("Llama Programa BCTRAINVB " & batch & "', 'PROD")
                    datosas400.LlamaPrograma("BCTRAINVB", batch & "', 'PROD")
                    wsResult = "Transacciones enviadas"
                    Console.WriteLine(wsResult)
                Else
                    wsResult = "Sin Novedad"
                    Console.WriteLine(wsResult)

                    data = Nothing
                    data = New BFPARAM
                    With data
                        .CCUDC2 = "0"
                        .CCNUM = dtBFPARAM.Rows(0).Item("CCNUM")
                        .CCNOT1 = wsResult
                    End With
                    'FINALIZA PROCESO
                    continuar = datos.ActualizarBFPARAMRow(data, 7)
                    If continuar = True Then
                        Console.WriteLine("...FIN TRANSACCION RJ...")
                    End If

                    Return
                End If
                Console.WriteLine("ACTUALIZANDO BFPARAM: " & dtBFPARAM.Rows(0).Item("CCNUM"))

                data = Nothing
                data = New BFPARAM
                With data
                    .CCUDC2 = "0"
                    .CCNUM = dtBFPARAM.Rows(0).Item("CCNUM")
                    .CCNOT1 = wsResult
                End With
                'FINALIZA PROCESO
                continuar = datos.ActualizarBFPARAMRow(data, 7)
                If continuar = True Then
                    Console.WriteLine("...FIN TRANSACCION RJ...")
                End If

            End If
        End If
    End Sub

    Private Sub ActualizacionBFCIM()
        Dim parametro(2) As String
        Dim dtBFPARAM As DataTable
        Dim continuar As Boolean
        Dim dtBFCIMWS As DataTable

        'VALIDA QUE NO ESTE CORRIENDO EL PROCESO ACTUALMENTE
        parametro(0) = "TareasBellota"
        parametro(1) = "Traslados"
        parametro(2) = "0"
        dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 5)
        If dtBFPARAM IsNot Nothing Then
            data = Nothing
            data = New BFPARAM
            With data
                .CCUDC2 = "1"
                .CCNUM = dtBFPARAM.Rows(0).Item("CCNUM")
                .CCNOT1 = "Inicia Rutina"
            End With
            'INICIA PROCESO
            continuar = datos.ActualizarBFPARAMRow(data, 7)
            If continuar = True Then
                Console.WriteLine("Consultando BFCIMWS MySQL...")
                parametro(0) = "0"
                dtBFCIMWS = datos.ConsultarBFCIMWSdt(parametro, 0)
                If dtBFCIMWS IsNot Nothing Then
                    Console.WriteLine("Recorriendo BFCIMWS MySQL...")
                    For Each row As DataRow In dtBFCIMWS.Rows
                        data = Nothing
                        data = New BFCIM
                        With data
                            .IFUENTE = row("IFUENTE")               '//15A
                            .IBATCH = row("IBATCH")                 '//8
                            .INLINE = row("INLINE")                 '//2A    Record Identifier
                            .INID = row("INID")                     '//2A    Record Identifier
                            .INTRAN = row("INTRAN")                 '//2A    Transaction Type
                            .INCTNR = row("INCTNR")                 '//2A    Transaction Type
                            .INMFGR = row("INMFGR")                 '//2A    Transaction Type
                            .INDATE = row("INDATE")                 '//8P0   Date		
                            .INTRN = row("INTRN")                   '//5P0   INTRN
                            .INMLOT = row("INMLOT")                 '//5P0   INTRN
                            .INREAS = row("INREAS")                 '//5P0   INTRN
                            .INREF = row("INREF")                   '//8P0   Date
                            .INPROD = row("INPROD")                 '//15A   Item Number
                            .INWHSE = row("INWHSE")                 '//2A    Warehouse
                            .INLOT = row("INLOT")                   '//10A   Lot Number
                            .INLOC = row("INLOC")                   '//6A    Location Code
                            .INQTY = row("INQTY")                   '//10A   Creted by
                            .INCBY = row("INCBY")                   '//10A   Creted by
                            .INLDT = row("INLDT")                   '//8P0
                            .INLTM = row("INLTM")                   '//6P0
                            .INCDT = row("INCDT")                   '//8P0   Date Created
                            .INCTM = row("INCTM")                   '//6P0   Time Created
                        End With
                        Console.WriteLine("Guardando BFCIM AS400...")
                        continuar = datosas400.GuardarBFCIMRow(data, 0)
                        If continuar = False Then
                            Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                            'ERROR DE GUARDADO
                        Else
                            Console.WriteLine("BFCIM AS400 Guardado")
                            Console.WriteLine("Guardando BFCIM MySQL...")
                            continuar = datos.GuardarBFCIMRow(data, 0)
                            If continuar = False Then
                                Console.WriteLine("Error Guardando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                'ERROR DE GUARDADO
                            Else
                                Console.WriteLine("BFCIM MySQL Guardado")
                                data = Nothing
                                data = New BFCIMWS
                                With data
                                    .IFLAG2 = 1
                                    .IBATCH = row("IBATCH")
                                    .INTRN = row("INTRN")
                                End With
                                Console.WriteLine("Actualizando BFCIMWS MySQL...")
                                continuar = datos.ActualizarBFCIMWSRow(data, 0)
                                If continuar = False Then
                                    Console.WriteLine("Error Actualizando BFCIMWS MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                    'ERROR DE ACTUALIZADO
                                Else
                                    Console.WriteLine("BFCIMWS MySQL Actualizado")
                                End If
                            End If
                        End If
                    Next
                    parametro(0) = "1"
                    Console.WriteLine("Consultando BFCIMWS MySQL...")
                    dtBFCIMWS = datos.ConsultarBFCIMWSdt(parametro, 1)
                    If dtBFCIMWS IsNot Nothing Then
                        Console.WriteLine("Recorriendo BFCIMWS MySQL...")
                        For Each row As DataRow In dtBFCIMWS.Rows
                            Console.WriteLine("Llamando Programa BCTRAINVB AS400... " & datos.Ceros(row("IBATCH"), 8) & "', 'DESP")
                            datosas400.LlamaPrograma("BCTRAINVB", datos.Ceros(row("IBATCH"), 8) & "', 'DESP")
                            data = Nothing
                            data = New BFCIMWS
                            With data
                                .IFLAG2 = 2
                                .IBATCH = row("IBATCH")
                            End With
                            Console.WriteLine("Actualizando BFCIMWS MySQL...")
                            continuar = datos.ActualizarBFCIMWSRow(data, 1)
                            If continuar = False Then
                                Console.WriteLine("Error Actualizando BFCIMWS MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                'ERROR DE ACTUALIZADO
                            Else
                                Console.WriteLine("BFCIMWS MySQL Actualizado")
                            End If
                        Next
                        wsResult = "Transacciones enviadas"
                        Console.WriteLine(wsResult)
                    End If
                Else
                    wsResult = "Sin Novedad"
                    Console.WriteLine(wsResult)
                End If

                data = Nothing
                data = New BFPARAM
                With data
                    .CCUDC2 = "0"
                    .CCNUM = dtBFPARAM.Rows(0).Item("CCNUM")
                    .CCNOT1 = wsResult
                End With
                'FINALIZA PROCESO
                continuar = datos.ActualizarBFPARAMRow(data, 7)

                Console.WriteLine("...FIN ACTUALIZACION BFCIM...")
            End If
        End If
    End Sub

    Private Sub ActualizacionBFCIMTransacInv()
        Dim parametro(2) As String
        Dim dtBFCIM As DataTable
        Dim continuar As Boolean
        Dim dtBFPARAM As DataTable

        parametro(0) = 0
        parametro(1) = "TRANSAC. INV."
        Console.WriteLine("Consultando BFCIM MySQL...")
        dtBFCIM = datos.ConsultarBFCIMdt(parametro, 1)
        If dtBFCIM IsNot Nothing Then
            Console.WriteLine("Recorriendo BFCIM MySQL...")
            For Each row As DataRow In dtBFCIM.Rows
                data = Nothing
                data = New BFCIM
                With data
                    .IFUENTE = row("IFUENTE")
                    .IBATCH = row("IBATCH")
                    .INID = row("INID")
                    .INTRAN = row("INTRAN")
                    .INCTNR = row("INCTNR")
                    .INMFGR = row("INMFGR")
                    .INDATE = row("INDATE")
                    .INTRN = row("INTRN")
                    .INMLOT = row("INMLOT")
                    .INREF = row("INREF")
                    .INPROD = row("INPROD")
                    .INLOT = row("INLOT")
                    .INLOC = row("INLOC")
                    .INQTY = row("INQTY")
                    .INCBY = row("INCBY")
                    .INLDT = row("INLDT")
                    .INLTM = row("INLTM")
                    .INCDT = row("INCDT")
                    .INCTM = row("INCTM")
                    .INUMERO = row("INUMERO")
                    .INCOST = row("INCOST")
                    .INWHSE = row("INWHSE")
                    .INFACTU = IIf(IsDBNull(row("INFACTU")), "0", row("INFACTU"))
                End With
                Console.WriteLine("Guardando BFCIM AS400...")
                continuar = datosas400.GuardarBFCIMRow(data, 2)
                If continuar = False Then
                    Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                    'ERROR DE GUARDADO
                    Exit Sub
                Else
                    Console.WriteLine("BFCIM AS400 Guardado")
                    data = Nothing
                    data = New BFCIM
                    With data
                        .IFLAG = 1
                        .IBATCH = row("IBATCH")
                        .INTRN = row("INTRN")
                    End With
                    Console.WriteLine("Actualizando BFCIM MySQL...")
                    continuar = datos.ActualizarBFCIMRow(data, 2)
                    If continuar = False Then
                        Console.WriteLine("Error Actualizando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                        'ERROR DE ACTUALIZADO
                    Else
                        Console.WriteLine("BFCIM MySQL Actualizado")
                    End If
                End If
            Next
            parametro(0) = "1"
            parametro(1) = "TRANSAC. INV."
            Console.WriteLine("Consultando BFCIM MySQL...")
            dtBFCIM = datos.ConsultarBFCIMdt(parametro, 3)
            If dtBFCIM IsNot Nothing Then
                Console.WriteLine("Recorriendo BFCIM MySQL...")
                For Each row As DataRow In dtBFCIM.Rows
                    Console.WriteLine("Llamando Programa BCTRAINVB... " & datos.Ceros(row("IBATCH"), 8) & "', 'DESP")
                    datosas400.LlamaPrograma("BCTRAINVB", datos.Ceros(row("IBATCH"), 8) & "', 'DESP")
                Next
                wsResult = "Transacciones enviadas"
                Console.WriteLine(wsResult)
            End If
        Else
            wsResult = "Sin Novedad"
            Console.WriteLine(wsResult)
        End If
        parametro(0) = "TareasBellota"
        parametro(1) = "Transacciones"
        parametro(2) = "0"
        dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 5)
        If dtBFPARAM IsNot Nothing Then
            data = Nothing
            data = New BFPARAM
            With data
                .CCUDC2 = "0"
                .CCNUM = dtBFPARAM.Rows(0).Item("CCNUM")
                .CCNOT1 = wsResult
            End With
            'FINALIZA PROCESO
            continuar = datos.ActualizarBFPARAMRow(data, 7)
            If continuar = True Then
                Console.WriteLine("...FIN TRANSACCION INVENTARIO...")
            End If
        End If
    End Sub

    Private Sub ActualizacionBFDESPA()
        Dim parametro(2) As String
        Dim dtBFPARAM As DataTable
        Dim continuar As Boolean
        Dim dtBFDESPAWS As DataTable

        'VALIDA QUE NO ESTE CORRIENDO EL PROCESO ACTUALMENTE
        parametro(0) = "TareasBellota"
        parametro(1) = "Asignacion"
        parametro(2) = "0"
        ProcesoActual = "Consultar BFPARAM"
        dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 5)
        If dtBFPARAM IsNot Nothing Then
            data = Nothing
            data = New BFPARAM
            With data
                .CCUDC2 = "1"
                .CCNUM = dtBFPARAM.Rows(0).Item("CCNUM")
                .CCNOT1 = "Inicia Rutina"
            End With
            'INICIA PROCESO
            ProcesoActual = "Actualizar BFPARAM 1"
            continuar = datos.ActualizarBFPARAMRow(data, 8)
            If continuar = True Then
                Console.WriteLine("Consultando BFDESPAWS MySQL...")
                parametro(0) = 0
                ProcesoActual = "Consultar BFDESPAWS"
                dtBFDESPAWS = datos.ConsultarBFDESPAWSdt(parametro, 0)
                If dtBFDESPAWS IsNot Nothing Then
                    Console.WriteLine("Recorriendo BFDESPAWS MySQL...")
                    ProcesoActual = "Recorriendo BFDESPAWS"
                    For Each row As DataRow In dtBFDESPAWS.Rows
                        data = Nothing
                        data = New BFDESPA
                        With data
                            .DBATCH = row("DBATCH")                         '//15A
                            .DPEDID = row("DPEDID")                         '//8
                            .DLINEA = row("DLINEA")                         '//2A    Record Identifier
                            .DPRODU = row("DPRODU")                         '//2A    Transaction Type
                            .DCANTI = row("DCANTI")                         '//2A    Transaction Type
                            .DBODEG = row("DBODEG")                         '//2A    Transaction Type
                            .DLOCAL = row("DLOCAL")                         '//8P0   Date		
                            .DLOTE = row("DLOTE")                           '//5P0   INTRN
                            .DUSER = row("DUSER")                           '//5P0   INTRN
                            .DFLAG = row("DFLAG")                           '//2A    Warehouse
                        End With
                        Console.WriteLine("Guardando BFDESPAWS AS400...")
                        ProcesoActual = "Guardando BFDESPAWS AS400"
                        continuar = datosas400.GuardarBFDESPARow(data)
                        If continuar = False Then
                            Console.WriteLine("Error Guardando BFDESPAWS AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                            'ERROR DE GUARDADO
                        Else
                            Console.WriteLine("BFDESPAWS AS400 Guardado")
                            Console.WriteLine("Guardando BFDESPAWS MySQL...")
                            ProcesoActual = "Guardando BFDESPAWS MySQL"
                            continuar = datos.GuardarBFDESPARow(data)
                            If continuar = False Then
                                Console.WriteLine("Error Guardando BFDESPAWS MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                'ERROR DE GUARDADO
                            Else
                                Console.WriteLine("BFDESPAWS MySQL Guardado")
                                data = Nothing
                                data = New BFDESPAWS
                                With data
                                    .DFLAG2 = 1
                                    .DBATCH = row("DBATCH")
                                    .DPEDID = row("DPEDID")
                                    .DLINEA = row("DLINEA")
                                End With
                                Console.WriteLine("Actualizando BFDESPAWS MySQL...")
                                ProcesoActual = "Actualizando BFDESPAWS MySQL"
                                continuar = datos.ActualizarBFDESPAWSRow(data, 0)
                                If continuar = False Then
                                    Console.WriteLine("Error Actualizando BFDESPAWS MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                    'ERROR DE ACTUALIZADO
                                Else
                                    Console.WriteLine("BFDESPAWS MySQL Actualizado")
                                End If
                            End If
                        End If
                    Next
                Else
                    wsResult = "Sin Novedad"
                    Console.WriteLine(wsResult)
                End If
                Console.WriteLine("Consultando BFDESPAWS MySQL...")
                ProcesoActual = "Consultando BFDESPAWS MySQL"
                parametro(0) = 1
                dtBFDESPAWS = datos.ConsultarBFDESPAWSdt(parametro, 1)
                If dtBFDESPAWS IsNot Nothing Then
                    Console.WriteLine("Recorriendo BFDESPAWS MySQL...")
                    ProcesoActual = "Recorriendo BFDESPAWS MySQL"
                    For Each row As DataRow In dtBFDESPAWS.Rows
                        Console.WriteLine("Llamando Programa BCCONFIR... " & datos.Ceros(row("DBATCH"), 8))
                        ProcesoActual = "Llamando Programa BCCONFIR" & datos.Ceros(row("DBATCH"), 8)
                        datosas400.LlamaPrograma("BCCONFIR", datos.Ceros(row("DBATCH"), 8))
                        data = Nothing
                        data = New BFDESPAWS
                        With data
                            .DFLAG2 = 2
                            .DBATCH = row("DBATCH")
                        End With
                        Console.WriteLine("Actualizando BFDESPAWS MySQL...")
                        ProcesoActual = "Actualizando BFDESPAWS MySQL"
                        continuar = datos.ActualizarBFDESPAWSRow(data, 1)
                        If continuar = False Then
                            Console.WriteLine("Error Actualizando BFDESPAWS MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                            'ERROR DE ACTUALIZADO
                        Else
                            Console.WriteLine("BFDESPAWS MySQL Actualizado")
                        End If
                    Next
                    wsResult = "Transacciones enviadas"
                    Console.WriteLine(wsResult)
                End If

                data = Nothing
                data = New BFPARAM
                With data
                    .CCUDC2 = "0"
                    .CCNUM = dtBFPARAM.Rows(0).Item("CCNUM")
                    .CCNOT1 = wsResult
                End With
                'FINALIZA PROCESO
                ProcesoActual = "Actualizando BFPARAM MySQL 0"
                continuar = datos.ActualizarBFPARAMRow(data, 7)
                If continuar = True Then
                    Console.WriteLine("...FIN ACTUALIZACION BFDESPA...")
                End If

            End If
        End If
    End Sub

#Region "BLOQUE ACTUALIZA PR PRD"

    Private Sub actualizaPR()
        Dim parametro(2) As String
        Dim dtBFPARAM As DataTable
        Dim continuar As Boolean
        Dim cons As String
        Dim batch As String
        Dim dtBFREP As DataTable
        Dim canti

        'VALIDA QUE NO ESTE CORRIENDO EL PROCESO ACTUALMENTE
        parametro(0) = "TareasBellota"
        parametro(1) = "Actualiza PR"
        parametro(2) = "0"
        dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 5)
        If dtBFPARAM IsNot Nothing Then
            data = Nothing
            data = New BFPARAM
            With data
                .CCUDC2 = "1"
                .CCNUM = dtBFPARAM.Rows(0).Item("CCNUM")
                .CCNOT1 = "Inicia Rutina"
            End With
            'INICIA PROCESO
            continuar = datos.ActualizarBFPARAMRow(data, 7)
            If continuar = True Then
                'PRIMER BLOQUE ACTUALIZA PR
                Console.WriteLine("Consultando BFREP MySQL...")
                dtBFREP = datos.ConsultarBFREPdt(parametro, 3)
                If dtBFREP IsNot Nothing Then
                    batch = datos.CalcConsec("CIMNUM")
                    Console.WriteLine("Recorriendo BFREP MySQL...")
                    For Each row As DataRow In dtBFREP.Rows
                        cons = datos.CalcConsec("CIMNUM")
                        data = Nothing
                        data = New BFCIM
                        With data
                            .IFUENTE = "PRODUCCION"                     '//15A
                            .INID = "IN"                                '//2A    Record Identifier
                            .IBATCH = batch                             '//8"
                            .INTRAN = "WF"                              '//2A    Transaction Type
                            .INCTNR = 0                                 '//2A    Transaction Type"
                            .INMFGR = 0                                 '//2A    Transaction Type"
                            .INDATE = datosas400.DB2_DATEDEC            '//8P0   Date		"
                            .INTRN = Right(cons, 5)                     '//5P0   INTRN"
                            .INMLOT = "RP" & cons                       '//5P0   INTRN
                            .INREAS = "01"                              '//5P0   INTRN
                            .INREF = row("TORD")                        '//8P0   Date
                            .INPROD = row("TPROD")                      '//15A   Item Number
                            .INWHSE = row("PBODPP")                     '//2A    Warehouse
                            .INLOT = row("PLOTE")                       '//10A   Lot Number
                            .INLOC = row("PLOCPP")                      '//6A    Location Code
                            canti = -(CDbl(row("TQREP")) - CDbl(row("TQREC")))
                            .INQTY = canti                              '//10A   Creted by"
                            .INCBY = "COSIS001"                         '//10A   Creted by
                            .INCDT = datosas400.DB2_DATEDEC             '//8P0   Date Created"
                            .INCTM = datosas400.DB2_TIMEDEC             '//6P0   Time Created"
                        End With
                        Console.WriteLine("Guardando BFCIM AS400...")
                        continuar = datosas400.GuardarBFCIMRow(data, 3)
                        If continuar = False Then
                            Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                            'ERROR GUARDANDO
                        Else
                            Console.WriteLine("BFCIM AS400 Guardado")
                            Console.WriteLine("Guardando BFCIM MySQL...")
                            continuar = datos.GuardarBFCIMRow(data, 3)
                            If continuar = False Then
                                Console.WriteLine("Error Guardando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                'ERROR GUARDANDO
                            Else
                                Console.WriteLine("BFCIM MySQL Guardado")
                                cons = datos.CalcConsec("CIMNUM")
                                data = Nothing
                                data = New BFCIM
                                With data
                                    .IFUENTE = "PRODUCCION"                         '//15A
                                    .INID = "IN"                                    '//2A    Record Identifier
                                    .IBATCH = batch                                 '//8"
                                    .INTRAN = "PR"                                  '//2A    Transaction Type
                                    .INCTNR = 0                                     '//2A    Transaction Type"
                                    .INMFGR = 0                                     '//2A    Transaction Type"
                                    .INDATE = datosas400.DB2_DATEDEC             '//8P0   Date		"
                                    .INTRN = Right(cons, 5)                         '//5P0   INTRN"
                                    .INMLOT = "RP" & cons                           '//5P0   INTRN
                                    .INREAS = "01"                                  '//5P0   INTRN
                                    .INREF = row("TORD")                            '//8P0   Date
                                    .INPROD = row("TPROD")                          '//15A   Item Number
                                    .INWHSE = row("PBODPT")                         '//2A    Warehouse
                                    .INLOT = row("PLOTE")                           '//10A   Lot Number
                                    .INLOC = row("PLOCPT")                          '//6A    Location Code
                                    canti = (CDbl(row("TQREP")) - CDbl(row("TQREC")))
                                    .INQTY = canti                                  '//10A   Creted by"
                                    .INCBY = "COSIS001"                             '//10A   Creted by
                                    .INCDT = datosas400.DB2_DATEDEC              '//8P0   Date Created"
                                    .INCTM = datosas400.DB2_TIMEDEC              '//6P0   Time Created"
                                End With
                                Console.WriteLine("Guardando BFCIM AS400...")
                                continuar = datosas400.GuardarBFCIMRow(data, 3)
                                If continuar = False Then
                                    Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                                    'ERROR GUARDANDO
                                Else
                                    Console.WriteLine("BFCIM AS400 Guardado")
                                    Console.WriteLine("Guardando BFCIM MySQL...")
                                    continuar = datos.GuardarBFCIMRow(data, 3)
                                    If continuar = False Then
                                        Console.WriteLine("Error Guardando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                        'ERROR GUARDANDO
                                    Else
                                        Console.WriteLine("BFCIM MySQL Guardado")
                                        data = Nothing
                                        data = New BFREP
                                        With data
                                            .TQREC = canti
                                            .TNUMREP = row("TNUMREP")
                                        End With
                                        Console.WriteLine("Actualizando BFREP MySQL...")
                                        continuar = datos.ActualizarBFREPRow(data, 4)
                                        If continuar = False Then
                                            Console.WriteLine("Error Actualizando BFREP MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                            'ERROR ACTUALIZANDO
                                        Else
                                            Console.WriteLine("BFREP MySQL Actualizado")
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    Next
                    Try
                        Console.WriteLine("Llamando Programa BCTRAINVB " & batch & "', 'PROD")
                        datosas400.LlamaPrograma("BCTRAINVB", batch & "', 'PROD")
                        wsResult = "Transacciones enviadas"
                        Console.WriteLine(wsResult)
                    Catch ex As Exception
                        data = Nothing
                        data = New BFLOG
                        With data
                            .USUARIO = Net.Dns.GetHostName()
                            .OPERACION = "Llamar Programa "
                            .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                            .EVENTO = ex.Message
                            .TXTSQL = datosas400.fSql
                            .ALERT = 1
                            .LKEY = Left(datosas400.gsKeyWords.Trim.ToUpper, 30)
                        End With
                        datos.GuardarBFLOGRow(data)
                    End Try
                    Console.WriteLine("...FIN ACTUALIZA PR...")
                Else
                    wsResult = "Sin Novedad"
                    Console.WriteLine(wsResult)
                    Console.WriteLine(" ")
                    Console.WriteLine("actualizaPRD...")
                    Console.WriteLine(" ")
                    actualizaPRD(dtBFPARAM.Rows(0).Item("CCNUM"))
                End If
                Console.WriteLine(" ")
                Console.WriteLine("actualizaPRD...")
                Console.WriteLine(" ")
                actualizaPRD(dtBFPARAM.Rows(0).Item("CCNUM"))
            End If
        End If
    End Sub

    Private Sub actualizaPRD(ByVal CCNUM As String)
        Dim cons As String
        Dim batch As String
        Dim parametro(0) As String
        Dim dtBFREPD As DataTable
        Dim continuar As Boolean
        Dim canti

        'SEGUNDO BLOQUE ACTUALIZA PRD
        Console.WriteLine("Consultando BFREPD MySQL...")
        dtBFREPD = datos.ConsultarBFREPDdt(parametro, 1)
        If dtBFREPD IsNot Nothing Then
            batch = datos.CalcConsec("CIMNUM")
            Console.WriteLine("Recorriendo BFREPD MySQL...")
            For Each row As DataRow In dtBFREPD.Rows
                cons = datos.CalcConsec("CIMNUM")
                data = Nothing
                data = New BFCIM
                With data
                    .IFUENTE = "PRODUCCION"                         '//15A
                    .INID = "IN"                                    '//2A    Record Identifier
                    .IBATCH = batch                                 '//8"
                    .INTRAN = "R"                                   '//2A    Transaction Type
                    .INCTNR = 0                                     '//2A    Transaction Type"
                    .INMFGR = 0                                     '//2A    Transaction Type"
                    .INDATE = datosas400.DB2_DATEDEC                '//8P0   Date		"
                    .INTRN = Right(cons, 5)                         '//5P0   INTRN"
                    .INMLOT = "RP" & cons                           '//5P0   INTRN
                    .INREAS = "01"                                  '//5P0   INTRN
                    .INREF = row("TORD")                            '//8P0   Date
                    .INPROD = row("TPROD")                          '//15A   Item Number
                    .INWHSE = row("PBODPT")                         '//2A    Warehouse
                    .INLOT = IIf(row("PLOTE") Is DBNull.Value, " ", row("PLOTE"))                           '//10A   Lot Number
                    .INLOC = row("PLOCPT")                          '//6A    Location Code
                    canti = (CDbl(row("TQREP")) - CDbl(row("TQREC")))
                    .INQTY = canti                                  '//10A   Creted by"
                    .INCBY = "COSIS001"                             '//10A   Creted by
                    .INCDT = datosas400.DB2_DATEDEC                 '//8P0   Date Created"
                    .INCTM = datosas400.DB2_TIMEDEC                 '//6P0   Time Created"
                End With
                Console.WriteLine("Guardando BFCIM AS400...")
                continuar = datosas400.GuardarBFCIMRow(data, 3)
                If continuar = False Then
                    Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                    '
                Else
                    Console.WriteLine("BFCIM AS400 Guardado")
                    Console.WriteLine("Guardando BFCIM MySQL...")
                    continuar = datos.GuardarBFCIMRow(data, 3)
                    If continuar = False Then
                        Console.WriteLine("Error Guardando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                        '
                    Else
                        Console.WriteLine("BFCIM MySQL Guardado")
                        data = Nothing
                        data = New BFREPD
                        With data
                            .TQREC = canti
                            .TNUMREP = row("TNUMREP")
                        End With
                        Console.WriteLine("Actualizando BFREPD MySQL...")
                        continuar = datos.ActualizarBFREPDRow(data, 0)
                        If continuar = False Then
                            Console.WriteLine("Error Actualizando BFREPD MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                            '
                        Else
                            Console.WriteLine("BFREPD MySQL Actualizado")
                        End If
                    End If
                End If
            Next
            Try
                Console.WriteLine("Llamando Programa BCTRAINVB " & batch & "', 'PROD")
                datosas400.LlamaPrograma("BCTRAINVB", batch & "', 'PROD")
                wsResult = "Transacciones enviadas"
                Console.WriteLine(wsResult)
            Catch ex As Exception
                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = Net.Dns.GetHostName()
                    .OPERACION = "Llamar Programa "
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = ex.Message
                    .TXTSQL = datosas400.fSql
                    .ALERT = 1
                    .LKEY = Left(datosas400.gsKeyWords.Trim.ToUpper, 30)
                End With
                datos.GuardarBFLOGRow(data)
            End Try
        Else
            wsResult = "Sin Novedad"
            Console.WriteLine(wsResult)

            data = Nothing
            data = New BFPARAM
            With data
                .CCUDC2 = "0"
                .CCNUM = CCNUM
                .CCNOT1 = wsResult
            End With
            'FINALIZA PROCESO BLOQUES
            continuar = datos.ActualizarBFPARAMRow(data, 7)
            If continuar = True Then
                Console.WriteLine("...FIN ACTUALIZA PRD...")
            End If

            Return
        End If

        data = Nothing
        data = New BFPARAM
        With data
            .CCUDC2 = "0"
            .CCNUM = CCNUM
            .CCNOT1 = wsResult
        End With
        'FINALIZA PROCESO BLOQUES
        continuar = datos.ActualizarBFPARAMRow(data, 7)
        If continuar = True Then
            Console.WriteLine("...FIN ACTUALIZA PRD...")
        End If

    End Sub

#End Region

End Module
