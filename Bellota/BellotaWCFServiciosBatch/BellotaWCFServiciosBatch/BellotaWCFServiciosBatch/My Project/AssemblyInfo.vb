﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' La información general de un ensamblado se controla mediante el siguiente 
' conjunto de atributos. Cambie estos atributos para modificar la información
' asociada a un ensamblado.

' Revisar los valores de los atributos del ensamblado

<Assembly: AssemblyTitle("BellotaWCFServiciosBatch")> 
<Assembly: AssemblyDescription("Web Service Traslados en Batch")> 
<Assembly: AssemblyCompany("BSS LTDA")> 
<Assembly: AssemblyProduct("BellotaWCFServiciosBatch")> 
<Assembly: AssemblyCopyright("Copyright ©  2016")> 
<Assembly: AssemblyTrademark("BSSWebServiceTraslao")> 

<Assembly: ComVisible(False)>

'El siguiente GUID sirve como identificador de la biblioteca de tipos si este proyecto se expone a COM
<Assembly: Guid("88c0a4a5-4a28-4b6b-bf15-73fe10a3555d")> 

' La información de versión de un ensamblado consta de los siguientes cuatro valores:
'
'      Versión principal
'      Versión secundaria 
'      Número de compilación
'      Revisión
'
' Puede especificar todos los valores o usar los valores predeterminados (número de versión de compilación y de revisión) 
' usando el símbolo '*' como se muestra a continuación:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
