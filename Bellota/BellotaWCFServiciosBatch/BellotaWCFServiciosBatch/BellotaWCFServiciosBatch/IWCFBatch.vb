﻿Imports System.ServiceModel
Imports System.IO

' NOTA: puede usar el comando "Cambiar nombre" del menú contextual para cambiar el nombre de interfaz "IWCFBatch" en el código y en el archivo de configuración a la vez.
<ServiceContract()>
Public Interface IWCFBatch

    <OperationContract()>
    Function TrasladosBatch(ByVal xml As String) As TrasladoBatchRespuesta

    <OperationContract()>
    Function TrasladosBatchEstado(ByVal batch As Integer) As TrasladoBatchEstado

    <OperationContract()>
    Function AsignacionBatch(ByVal xml As String) As AsignacionBatchRespuesta

    <OperationContract()>
    Function AsignacionesBatchEstado(ByVal batch As Integer) As AsignacionBatchEstado

    <OperationContract()>
    Function Version() As qVersion

    <OperationContract()>
    Function z_PruebaConexion() As qPruebaConexion

    <OperationContract()>
    Function TransaccionInventario(ByVal xml As String) As TransaccionInventarioRespuesta

    '<OperationContract()>
    'Function TransaccionInventario2(ByVal xml As String) As TransaccionInventarioRespuesta

    <OperationContract()>
    Function TransaccionInventarioEstado(ByVal batch As Integer) As TransaccionInventarioRespuesta

End Interface

<DataContract()>
Public Class TrasladoBatchEstado
    <DataMember()>
    Public Property batch As Integer
    <DataMember()>
    Public Property estado As Integer
    <DataMember()>
    Public Property estadoDescripcion As String
    <DataMember()>
    Public Property lineasTotal As Integer
    <DataMember()>
    Public Property lineasEnviadasWs As Integer
    <DataMember()>
    Public Property lineasEnviadasCIM600 As Integer
    <DataMember()>
    Public Property lineasBPCS As Integer
    <DataMember()>
    Public Property lineasConNovedad As List(Of novedadLineaTraslado)
End Class

<DataContract()>
Public Class novedadLineaTraslado
    <DataMember()>
    Public Property linea As Integer
    <DataMember()>
    Public Property codMensaje As Integer
    <DataMember()>
    Public Property mensaje As String
    <DataMember()>
    Public Property Producto As String
    <DataMember()>
    Public Property Lote As String
    <DataMember()>
    Public Property Bodega As String
    <DataMember()>
    Public Property Ubicacion As String
    <DataMember()>
    Public Property Cantidad As Double
    <DataMember()>
    Public Property OrdenFabricacion As Integer
    <DataMember()>
    Public Property CodigoCausa As String
End Class

<DataContract()>
Public Class Traslado
    <DataMember()>
    Public Property Fecha As String
    <DataMember()>
    Public Property Usuario As String
    <DataMember()>
    Public Property Lineas As List(Of TrasladoDet)
End Class

<DataContract()>
Public Class TrasladoDet
    <DataMember()>
    Public Property Linea As Double
    <DataMember()>
    Public Property Producto As String
    <DataMember()>
    Public Property Lote As String
    <DataMember()>
    Public Property Bodega_Desde As String
    <DataMember()>
    Public Property Ubicacion_Desde As String
    <DataMember()>
    Public Property Bodega_Hacia As String
    <DataMember()>
    Public Property Ubicacion_Hacia As String
    <DataMember()>
    Public Property Cantidad As Double
    <DataMember()>
    Public Property OrdenFabricacion As Integer
    <DataMember()>
    Public Property CodigoCausa As String
End Class

<DataContract()>
Public Class TrasladoBatchRespuesta
    <DataMember()>
    Public Property batch As Integer
    <DataMember()>
    Public Property estado As Integer
    <DataMember()>
    Public Property estadoDescripcion As String
    <DataMember()>
    Public Property lineasTotal As Integer
End Class

<DataContract()>
Public Class Asignacion
    <DataMember()>
    Public Property BatchT As String
    <DataMember()>
    Public Property Usuario As String
    <DataMember()>
    Public Property Fecha As Date
    <DataMember()>
    Public Property Pedidos As List(Of DespacharPedido)
End Class

<DataContract()>
Public Class DespacharPedido
    <DataMember()>
    Public Property Pedido As Integer
    <DataMember()>
    Public Property LineasPedido As List(Of LineaPedido)
End Class

<DataContract()>
Public Class LineaPedido
    <DataMember()>
    Public Property Linea As Double
    <DataMember()>
    Public Property Producto As String
    <DataMember()>
    Public Property Bodega As String
    <DataMember()>
    Public Property Ubicacion As String
    <DataMember()>
    Public Property Lote As String
    <DataMember()>
    Public Property Cantidad As Double
End Class

<DataContract()>
Public Class AsignacionBatchRespuesta
    <DataMember()>
    Public Property batch As Integer
    <DataMember()>
    Public Property estado As Integer
    <DataMember()>
    Public Property estadoDescripcion As String
    <DataMember()>
    Public Property pedidosTotal As Integer
End Class

<DataContract()>
Public Class AsignacionBatchEstado
    <DataMember()>
    Public Property batch As Integer
    <DataMember()>
    Public Property estado As Integer
    <DataMember()>
    Public Property estadoDescripcion As String
    <DataMember()>
    Public Property pedidosTotal As Integer
    <DataMember()>
    Public Property pedidosProcesados As Integer
    <DataMember()>
    Public Property pedidosConError As Integer
    <DataMember()>
    Public Property pedidosConNovedad As List(Of novedadPedidoAsignacion)
End Class

<DataContract()>
Public Class novedadPedidoAsignacion
    <DataMember()>
    Public Property codMensaje As Integer
    <DataMember()>
    Public Property mensaje As String
    <DataMember()>
    Public Property Pedido As String
    <DataMember()>
    Public Property Linea As Integer
    <DataMember()>
    Public Property Producto As String
    <DataMember()>
    Public Property Bodega As String
    <DataMember()>
    Public Property Ubicacion As String
    <DataMember()>
    Public Property Lote As String
    <DataMember()>
    Public Property Cantidad As Double
End Class

<DataContract()>
Public Class qVersion
    <DataMember()>
    Public Property Version As String
    <DataMember()>
    Public Property Fecha As String
    <DataMember()>
    Public Property Parametros As String
    <DataMember()>
    Public Property MySQL As String
    <DataMember()>
    Public Property AS400 As String
End Class

<DataContract()>
Public Class qPruebaConexion
    <DataMember()>
    Public Property MySQL_Server As String
    <DataMember()>
    Public Property MySQL_database As String
    <DataMember()>
    Public Property AS400 As String
    <DataMember()>
    Public Property Mensaje As String
End Class

<DataContract()>
Public Class TransaccionInventarioRespuesta
    <DataMember()>
    Public Property Batch As Integer
    <DataMember()>
    Public Property TipoRespuesta As Integer
    <DataMember()>
    Public Property TipoRespuestaDescripcion As String
    <DataMember()>
    Public Property estadoGeneral As Integer
    <DataMember()>
    Public Property estadoDescripcionGeneral As String
    <DataMember()>
    Public Property Transacciones As List(Of Transaccion)
End Class

<DataContract()>
Public Class Transaccion
    <DataMember()>
    Public Property NumeroID As Integer
    <DataMember()>
    Public Property estado As String
    <DataMember()>
    Public Property estadoDescripcion As String
End Class

<DataContract()>
Public Class Transac
    <DataMember()>
    Public Property Usuario As String
    <DataMember()>
    Public Property Detalle As List(Of TransacDet)
End Class

<DataContract()>
Public Class TransacDet
    <DataMember()>
    Public Property NumeroID As String
    <DataMember()>
    Public Property TipoTransaccion As String
    <DataMember()>
    Public Property Fecha As String
    <DataMember()>
    Public Property OrdenFabricacion As String
    <DataMember()>
    Public Property OrdenCompra As String
    <DataMember()>
    Public Property Referencia As String
    <DataMember()>
    Public Property NumeroFactura As String
    <DataMember()>
    Public Property Producto As String
    <DataMember()>
    Public Property Lote As String
    <DataMember()>
    Public Property Almacen As String
    <DataMember()>
    Public Property Ubicacion As String
    <DataMember()>
    Public Property Cantidad As String
    <DataMember()>
    Public Property Valor As String
End Class
