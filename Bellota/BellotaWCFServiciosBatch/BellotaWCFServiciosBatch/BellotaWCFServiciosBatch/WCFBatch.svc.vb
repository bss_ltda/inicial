﻿Imports CLDatos
Imports System.IO
Imports System.Globalization
Imports System.Threading

Public Class WCFBatch
    Implements IWCFBatch
    Dim resultConn As String
    Dim c_ID As String
    Private datos As New ClsDatos
    Private datosas400 As New ClsDatosAS400

    Public Function TrasladosBatch(ByVal xml As String) As TrasladoBatchRespuesta Implements IWCFBatch.TrasladosBatch
        Dim docEntrada As XDocument = XDocument.Parse(xml)
        Dim err As New TrasladoBatchRespuesta
        Dim continuar As Boolean
        Dim r As New Globalization.CultureInfo("es-ES")
        r.NumberFormat.CurrencyDecimalSeparator = "."
        r.NumberFormat.NumberDecimalSeparator = "."
        System.Threading.Thread.CurrentThread.CurrentCulture = r



        If Not datos.AbrirConexion() Then
            err.estado = -1
            err.estadoDescripcion = datos.mensaje
            err.lineasTotal = 0
            Return err
        End If

        If Not AbreConexion(docEntrada.ToString) Then
            err.estado = -1
            err.estadoDescripcion = resultConn
            err.lineasTotal = 0
            datos.CerrarConexion()
            Return err
        End If

        Try
            Dim result As String = ""
            Dim itH As Traslado
            Dim itL As New TrasladoDet
            Dim Lineas As New List(Of TrasladoDet)
            Dim qx = From xe In docEntrada.Elements("TrasladoBatch") Select New With { _
                .Fecha = xe.Element("Fecha").Value,
                .Usuario = xe.Element("Usuario").Value
            }

            '   .Batch = xe.Element("Batch").Value,

            itH = New Traslado
            With itH
                .Fecha = qx.First.Fecha
                .Usuario = qx.First.Usuario
                .Lineas = New List(Of TrasladoDet)
            End With

            Dim qd = From xe In docEntrada.Descendants.Elements("Traslado") Select New With { _
                .Linea = xe.Element("Linea").Value,
                .Producto = xe.Element("Producto").Value,
                .Lote = xe.Element("Lote").Value,
                .Bodega_Desde = xe.Element("Bodega_Desde").Value,
                .Ubicacion_Desde = xe.Element("Ubicacion_Desde").Value,
                .Bodega_Hacia = xe.Element("Bodega_Hacia").Value,
                .Ubicacion_Hacia = xe.Element("Ubicacion_Hacia").Value,
                .Cantidad = xe.Element("Cantidad").Value,
                .OrdenFabricacion = xe.Element("OrdenFabricacion").Value,
                .CodigoCausa = xe.Element("CodigoCausa").Value
            }

            For Each e In qd
                itL = New TrasladoDet
                With itL
                    .Linea = e.Linea
                    .Producto = e.Producto
                    .Lote = e.Lote
                    .Bodega_Desde = e.Bodega_Desde
                    .Ubicacion_Desde = e.Ubicacion_Desde
                    .Bodega_Hacia = e.Bodega_Hacia
                    .Ubicacion_Hacia = e.Ubicacion_Hacia
                    .Cantidad = e.Cantidad
                    .OrdenFabricacion = e.OrdenFabricacion
                    .CodigoCausa = e.CodigoCausa
                End With
                itH.Lineas.Add(itL)
            Next
            result &= "[" & itH.Fecha & "]"
            result &= "[" & itH.Usuario & "]"
            Dim cons
            Dim batch
            Dim canti1
            batch = datos.CalcConsec("CIMNUM")

            For Each e In itH.Lineas
                With e
                    cons = datos.CalcConsec("CIMNUM")
                    Dim tabla As New BFCIMWS
                    With tabla
                        .IFUENTE = "DESPACHOS"
                        .IBATCH = batch
                        .INLINE = e.Linea
                        .INID = "IN"
                        .INTRAN = "T1"
                        .INCTNR = 0
                        .INMFGR = 0
                        .INDATE = Replace(itH.Fecha, "-", "")
                        .INTRN = Right(cons, 5)
                        .INMLOT = "DE" & cons
                        .INREAS = e.CodigoCausa
                        .INREF = e.OrdenFabricacion
                        .INPROD = e.Producto
                        .INWHSE = e.Bodega_Desde
                        .INLOT = e.Lote
                        .INLOC = e.Ubicacion_Desde
                        canti1 = -(CDbl(e.Cantidad))
                        .INQTY = canti1
                        .INCBY = itH.Usuario
                        .INLDT = Format(Now(), "yyyyMMdd")
                        .INLTM = Format(Now(), "hhmmss")
                        .INCDT = Year(Now).ToString & Month(Now).ToString("00") & Day(Now).ToString("00")
                        '.INCDT = DB2_DATEDEC()
                        .INCTM = Now.Hour.ToString("00") & Now.Minute.ToString("00") & Now.Second.ToString("00")
                        '.INCTM = DB2_TIMEDEC()
                    End With
                    continuar = datos.GuardarBFCIMWSRow(tabla, 0)
                    If continuar = False Then
                        datos.CerrarConexion()
                        err.estado = -1
                        err.estadoDescripcion = "1. Recibido, -1 Con Errores <A>" & docEntrada.ToString & "</A><B>" & datos.mensaje & "</B>"
                        err.lineasTotal = 0
                        Return err
                    End If
                    cons = datos.CalcConsec("CIMNUM")
                    With tabla
                        .IFUENTE = "DESPACHOS"
                        .IBATCH = batch
                        .INLINE = e.Linea
                        .INID = "IN"
                        .INTRAN = "T1"
                        .INCTNR = 0
                        .INMFGR = 0
                        .INDATE = Replace(itH.Fecha, "-", "")
                        .INTRN = Right(cons, 5)
                        .INMLOT = "DE" & cons
                        .INREAS = e.CodigoCausa
                        .INREF = e.OrdenFabricacion
                        .INPROD = e.Producto
                        .INWHSE = e.Bodega_Hacia
                        .INLOT = e.Lote
                        .INLOC = e.Ubicacion_Hacia
                        canti1 = CDbl(e.Cantidad)
                        .INQTY = canti1
                        .INCBY = itH.Usuario
                        .INLDT = Format(Now(), "yyyyMMdd")
                        .INLTM = Format(Now(), "hhmmss")
                        .INCDT = Year(Now).ToString & Month(Now).ToString("00") & Day(Now).ToString("00")
                        '.INCDT = DB2_DATEDEC()
                        .INCTM = Now.Hour.ToString("00") & Now.Minute.ToString("00") & Now.Second.ToString("00")
                        '.INCTM = DB2_TIMEDEC()
                    End With
                    continuar = datos.GuardarBFCIMWSRow(tabla, 0)
                    If continuar = False Then
                        datos.CerrarConexion()
                        err.estado = -1
                        err.estadoDescripcion = "1. Recibido, -1 Con Errores <A>" & docEntrada.ToString & "</A><B>" & datos.mensaje & "</B>"
                        err.lineasTotal = 0
                        Return err
                    End If
                End With
            Next

            err.batch = batch
            err.estado = 1
            err.estadoDescripcion = "1. Recibido, 1 Sin Errores " & itH.Fecha
            err.lineasTotal = itH.Lineas.Count
            datos.CerrarConexion()
            Return err

        Catch ex As Exception
            err.estado = -1
            err.estadoDescripcion = "1. Recibido, -1 Con Errores <A>" & docEntrada.ToString & "</A><B>" & ex.ToString & "</B>"
            err.lineasTotal = 0
            datos.CerrarConexion()
            Return err
        End Try
    End Function

    Public Function TrasladosBatchEstado(ByVal batch As Integer) As TrasladoBatchEstado Implements IWCFBatch.TrasladosBatchEstado
        Dim err As New TrasladoBatchEstado
        Dim errLin As New novedadLineaTraslado
        Dim parametro(0) As String
        Dim dtBFCIMWS As DataTable
        Dim dtBFCIMWS3 As DataTable
        Dim dtBFCIM As DataTable
        Dim dtBFCIM2 As DataTable
        Dim dtBFCIM3 As DataTable
        Dim dtBFCIM4 As DataTable
        Dim dtBFCIM5 As DataTable
        Dim dtBFCIM6 As DataTable

        If Not datos.AbrirConexion() Then
            err.estado = -1
            err.estadoDescripcion = datos.mensaje
            err.lineasTotal = 0
            Return err
        End If

        AbreConexion("Estado Batch")

        parametro(0) = batch
        dtBFCIMWS = datos.ConsultarBFCIMwsdt(parametro, 0)
        If dtBFCIMWS IsNot Nothing Then
            dtBFCIMWS3 = datos.ConsultarBFCIMwsdt(parametro, 2)

            parametro(0) = batch
            dtBFCIM = datos.ConsultarBFCIMdt(parametro, 2)
            If dtBFCIM IsNot Nothing Then
                dtBFCIM2 = datos.ConsultarBFCIMdt(parametro, 3)
                dtBFCIM3 = datos.ConsultarBFCIMdt(parametro, 4)
                dtBFCIM4 = datos.ConsultarBFCIMdt(parametro, 5)
                dtBFCIM5 = datos.ConsultarBFCIMdt(parametro, 6)
                dtBFCIM6 = datos.ConsultarBFCIMdt(parametro, 7)

                With err
                    .batch = batch
                    If dtBFCIM6.Rows(0).Item("TRANSACCIONES") > 0 Then
                        .estado = -1
                    Else
                        If dtBFCIM3.Rows(0).Item("PEDIDOSS") > 0 Or dtBFCIM4.Rows(0).Item("PEDIDOSS") > 0 Then
                            .estado = 1
                        Else
                            .estado = 2
                        End If
                    End If
                    .estadoDescripcion = "1. En Proceso, 2. Terminado, -1 Con Error"
                    .lineasTotal = dtBFCIM2.Rows(0).Item("PEDIDOSS")
                    .lineasEnviadasWs = dtBFCIM3.Rows(0).Item("PEDIDOSS")
                    .lineasEnviadasCIM600 = dtBFCIM4.Rows(0).Item("PEDIDOSS")
                    .lineasBPCS = dtBFCIM5.Rows(0).Item("PEDIDOSS")
                    .lineasConNovedad = New List(Of novedadLineaTraslado)
                End With

                For Each row As DataRow In dtBFCIM.Rows
                    errLin = New novedadLineaTraslado
                    With errLin
                        .linea = row("INLINE")

                        Select Case row("IFLAG")
                            Case 0
                                .codMensaje = "1"
                                .mensaje = "Enviado desde Web Service"
                            Case 1
                                .codMensaje = row("IFLAG")
                                .mensaje = "Enviado a CIM600"
                            Case 2
                                .codMensaje = row("IFLAG")
                                .mensaje = "En BPCS"
                            Case 7
                                .codMensaje = "-1"
                                .mensaje = Trim(row("INERROR"))
                        End Select
                        .Producto = row("INPROD")
                        .Bodega = row("INWHSE")
                        .Ubicacion = row("INLOC")
                        .Lote = row("INLOT")
                        .Cantidad = row("INQTY")
                        .CodigoCausa = row("INREAS")
                        .OrdenFabricacion = row("INREF")
                    End With
                    err.lineasConNovedad.Add(errLin)
                Next
            Else
                With err
                    .batch = batch
                    .estado = 1
                    .estadoDescripcion = "1. En Proceso, 2. Terminado, -1 Con Error"
                    .lineasEnviadasWs = dtBFCIMWS3.Rows(0).Item("PEDIDOSS")
                End With
            End If
        End If

        datos.CerrarConexion()
        Return err
    End Function

    Public Function AsignacionBatch(ByVal xml As String) As AsignacionBatchRespuesta Implements IWCFBatch.AsignacionBatch
        Dim docEntrada As XDocument = XDocument.Parse(xml)
        Dim err As New AsignacionBatchRespuesta
        Dim tabla As New BFDESPAWS
        Dim continuar As Boolean
        Dim parametro(0) As String
        Dim dtBFCIMWS As DataTable

        Dim newCultureDefinition As CultureInfo = CType(Thread.CurrentThread.CurrentCulture.Clone(), CultureInfo)
        newCultureDefinition.DateTimeFormat.DateSeparator = "-"
        newCultureDefinition.DateTimeFormat.ShortDatePattern = "yyyy-MM-dd"
        Thread.CurrentThread.CurrentCulture = newCultureDefinition
        Dim r As New Globalization.CultureInfo("es-ES")
        r.NumberFormat.CurrencyDecimalSeparator = "."
        r.NumberFormat.NumberDecimalSeparator = "."
        System.Threading.Thread.CurrentThread.CurrentCulture = r


        If Not datos.AbrirConexion() Then
            err.estado = -1
            err.estadoDescripcion = datos.mensaje
            err.pedidosTotal = 0
            Return err
        End If

        If Not AbreConexion(docEntrada.ToString) Then
            err.estado = -1
            err.estadoDescripcion = "Cadena de Conexion con error: " & resultConn
            err.pedidosTotal = 0
            datos.CerrarConexion()
            Return err
        End If

        Try
            Dim doc As XDocument = XDocument.Parse(xml)
            Dim asignacion As New Asignacion
            Dim Pedidos As New DespacharPedido
            Dim lstPedidos As New List(Of qPedidos)
            Dim wPedido As DespacharPedido
            Dim wLinea As LineaPedido

            Console.WriteLine(doc)

            Dim qA = From xe In doc.Elements("AsignacionBatch") Select New With { _
                .Batch = xe.Element("Batch").Value,
                .Usuario = xe.Element("Usuario").Value,
                .Fecha = xe.Element("Fecha").Value
            }
            Try
                asignacion.BatchT = qA.First.Batch
                parametro(0) = asignacion.BatchT
                dtBFCIMWS = datos.ConsultarBFCIMwsdt(parametro, 5)
                If dtBFCIMWS IsNot Nothing Then
                    err.estado = -1
                    err.estadoDescripcion = "-1 El Traslado " & asignacion.BatchT & " presenta novedades y/o errores"
                    err.pedidosTotal = 0
                    datos.CerrarConexion()
                    Return err
                End If
            Catch ex2 As Exception
                err.estado = -1
                err.estadoDescripcion = ex2.Message
                err.pedidosTotal = 0
                datos.CerrarConexion()
                Return err
            End Try

            asignacion.Usuario = qA.First.Usuario
            asignacion.Fecha = qA.First.Fecha
            asignacion.Pedidos = New List(Of DespacharPedido)

            Dim qP = From xe In doc.Descendants.Elements("DespacharPedido")
                         Select New With {.Pedido = xe.Element("Pedido").Value}
            For Each e In qP
                lstPedidos.Add(New qPedidos(e.Pedido))
            Next

            Dim result = From pedido In lstPedidos _
                         From order In doc...<DespacharPedido> _
                         Where pedido.Pedido = CStr(order.<Pedido>.Value) _
                         Select Pedido = CStr(pedido.Pedido), Cliente = order.<Cliente>.Value, LinPed = order.Descendants("LineaPedido")

            For Each tuple In result
                wPedido = New DespacharPedido
                wPedido.Pedido = CInt(tuple.Pedido)
                wPedido.LineasPedido = New List(Of LineaPedido)
                Dim linPed = From lineas In tuple.LinPed Select New With {
                    .Linea = lineas.<Linea>.Value,
                    .Producto = lineas.<Producto>.Value,
                    .Bodega = lineas.<Bodega>.Value,
                    .Ubicacion = lineas.<Ubicacion>.Value,
                    .Lote = lineas.<Lote>.Value,
                    .Cantidad = lineas.<Cantidad>.Value}
                For Each e In linPed
                    wLinea = New LineaPedido
                    With wLinea
                        .Linea = CInt(e.Linea)
                        .Producto = e.Producto
                        .Bodega = e.Bodega
                        .Ubicacion = e.Ubicacion
                        .Lote = e.Lote
                        .Cantidad = CDbl(e.Cantidad)
                    End With
                    wPedido.LineasPedido.Add(wLinea)
                Next
                asignacion.Pedidos.Add(wPedido)
            Next
            Dim cons
            cons = datos.CalcConsec("CIMNUM")

            For Each p In asignacion.Pedidos
                Console.WriteLine("Pedido " & p.Pedido)

                For Each L In p.LineasPedido
                    With tabla
                        .DBATCH = cons
                        .DPEDID = p.Pedido
                        .DLINEA = L.Linea
                        .DPRODU = L.Producto
                        .DCANTI = L.Cantidad
                        .DBODEG = L.Bodega
                        .DLOCAL = L.Ubicacion
                        .DLOTE = L.Lote
                        .DUSER = asignacion.Usuario
                        .DFECHA = Convert.ToDateTime(asignacion.Fecha).ToString("yyyy-MM-dd 00:00:00")
                        .DFLAG = 0
                    End With
                    continuar = datos.GuardarBFDESPAWSRow(tabla, 0)
                    If continuar = False Then
                        datos.CerrarConexion()
                        err.estado = -1
                        err.estadoDescripcion = "1. Recibido, -1 Con Errores" & datos.mensaje
                        Return err
                    End If
                Next
            Next

            'Dim err As New AsignacionBatchRespuesta
            err.batch = cons
            err.estado = 1
            err.estadoDescripcion = "1. Recibido, 1 Sin Errores " & Format(tabla.DFECHA & "         " & asignacion.Fecha, "yyyy-MM-dd")
            err.pedidosTotal = asignacion.Pedidos.Count
            datos.CerrarConexion()
            Return err

        Catch ex As Exception
            'Dim err As New AsignacionBatchRespuesta
            err.estado = -1
            err.estadoDescripcion = "1. Recibido, -1 Con Errores" & ex.Message
            datos.CerrarConexion()
            Return err
        End Try
    End Function

    'Public Function AsignacionesBatchEstadoOLD(ByVal batch As Integer) As AsignacionBatchEstado Implements IWCFBatch.AsignacionesBatchEstado
    '    Dim err As New AsignacionBatchEstado
    '    Dim errPed As New novedadPedidoAsignacion
    '    Dim parametro(0) As String
    '    Dim dtBFDESPAws As DataTable
    '    Dim dtBFDESPAws2 As DataTable
    '    Dim dtBFDESPAws3 As DataTable
    '    Dim dtBFDESPAws4 As DataTable
    '    Dim dtBFDESPAws5 As DataTable
    '    Dim dtBFDESPAws6 As DataTable
    '    Dim Errores

    '    If Not datos.AbrirConexion() Then
    '        err.estado = -1
    '        err.estadoDescripcion = datos.mensaje
    '        err.pedidosTotal = 0
    '        Return err
    '    End If

    '    AbreConexion("Estado Batch")

    '    parametro(0) = batch
    '    dtBFDESPAws = datos.ConsultarBFDESPAwsdt(parametro, 0)
    '    If dtBFDESPAws IsNot Nothing Then
    '        dtBFDESPAws2 = datos.ConsultarBFDESPAwsdt(parametro, 1)
    '        dtBFDESPAws3 = datos.ConsultarBFDESPAwsdt(parametro, 2)
    '        dtBFDESPAws4 = datos.ConsultarBFDESPAwsdt(parametro, 3)
    '        dtBFDESPAws5 = datos.ConsultarBFDESPAwsdt(parametro, 4)
    '        dtBFDESPAws6 = datos.ConsultarBFDESPAwsdt(parametro, 5)
    '        If dtBFDESPAws3.Rows(0).Item("PEDIDOSS") > 0 Then
    '            Errores = -1
    '        Else
    '            If dtBFDESPAws5.Rows(0).Item("PEDIDOSS") > 0 Or dtBFDESPAws6.Rows(0).Item("PEDIDOSS") > 0 Then
    '                Errores = 1
    '            Else
    '                Errores = 2
    '            End If
    '        End If
    '        With err
    '            .batch = batch
    '            .estado = Errores
    '            .estadoDescripcion = "1. En Proceso, 2. Terminado, -1 Con Error"
    '            .pedidosTotal = dtBFDESPAws2.Rows(0).Item("PEDIDOSS")
    '            .pedidosProcesados = dtBFDESPAws4.Rows(0).Item("PEDIDOSS")
    '            .pedidosConError = dtBFDESPAws3.Rows(0).Item("PEDIDOSS")
    '            .pedidosConNovedad = New List(Of novedadPedidoAsignacion)
    '        End With

    '        For i = 0 To dtBFDESPAws.Rows.Count - 1
    '            errPed = New novedadPedidoAsignacion
    '            With errPed
    '                If dtBFDESPAws.Rows(i).Item("DFLAG2") = 2 Then
    '                    .codMensaje = "2"
    '                    .mensaje = "En BPCS"
    '                Else
    '                    .codMensaje = "1"
    '                    .mensaje = "Enviado desde Web"
    '                End If
    '                '.codMensaje = dtBFDESPAws.Rows(i).Item("DERROR")
    '                '.mensaje = dtBFDESPAws.Rows(i).Item("DDESER")
    '                .Pedido = dtBFDESPAws.Rows(i).Item("DPEDID")
    '                .Linea = dtBFDESPAws.Rows(i).Item("DLINEA")
    '                .Producto = dtBFDESPAws.Rows(i).Item("DPRODU")
    '                .Ubicacion = dtBFDESPAws.Rows(i).Item("DLOCAL")
    '                .Bodega = dtBFDESPAws.Rows(i).Item("DBODEG")
    '                .Cantidad = dtBFDESPAws.Rows(i).Item("DCANTI")
    '                .Lote = dtBFDESPAws.Rows(i).Item("DLOTE")
    '            End With
    '            err.pedidosConNovedad.Add(errPed)
    '        Next
    '    End If

    '    '   Dim resp As String = XLinq15()
    '    datos.CerrarConexion()
    '    Return err
    'End Function

    Public Function AsignacionesBatchEstado(ByVal batch As Integer) As AsignacionBatchEstado Implements IWCFBatch.AsignacionesBatchEstado
        Dim err As New AsignacionBatchEstado
        Dim errPed As New novedadPedidoAsignacion
        Dim parametro(0) As String
        Dim dtBFDESPAWS As DataTable

        Dim dtBFDESPA As DataTable
        Dim dtBFDESPA2 As DataTable
        Dim dtBFDESPA3 As DataTable
        Dim dtBFDESPA4 As DataTable
        Dim dtBFDESPA5 As DataTable
        Dim dtBFDESPA6 As DataTable
        Dim dtBFDESPA7 As DataTable
        'Dim Errores

        If Not datos.AbrirConexion() Then
            err.estado = -1
            err.estadoDescripcion = datos.mensaje
            err.pedidosTotal = 0
            Return err
        End If

        AbreConexion("Estado Batch")

        parametro(0) = batch
        dtBFDESPAWS = datos.ConsultarBFDESPAwsdt(parametro, 0)
        If dtBFDESPAWS IsNot Nothing Then
            dtBFDESPA = datos.ConsultarBFDESPAdt(parametro, 0)
            dtBFDESPA2 = datos.ConsultarBFDESPAdt(parametro, 1)
            dtBFDESPA3 = datos.ConsultarBFDESPAdt(parametro, 2)
            dtBFDESPA4 = datos.ConsultarBFDESPAdt(parametro, 3)
            dtBFDESPA5 = datos.ConsultarBFDESPAdt(parametro, 4)
            dtBFDESPA6 = datos.ConsultarBFDESPAdt(parametro, 5)
            dtBFDESPA7 = datos.ConsultarBFDESPAdt(parametro, 6)
            With err
                .batch = batch
                If dtBFDESPA4.Rows(0).Item("PEDIDOSS") > 0 Then
                    .estado = -1
                Else
                    If dtBFDESPA5.Rows(0).Item("PEDIDOSS") > 0 Or dtBFDESPA6.Rows(0).Item("PEDIDOSS") > 0 Then
                        .estado = 1
                    Else
                        .estado = 2
                    End If
                End If
                .estadoDescripcion = "1. En Proceso, 2. Terminado, -1 Con Error"
                .pedidosTotal = dtBFDESPA3.Rows(0).Item("PEDIDOSS")
                .pedidosProcesados = dtBFDESPA.Rows(0).Item("PEDIDOSS")
                .pedidosConError = dtBFDESPA2.Rows(0).Item("PEDIDOSS")
                .pedidosConNovedad = New List(Of novedadPedidoAsignacion)
            End With

            For Each row As DataRow In dtBFDESPA7.Rows
                errPed = New novedadPedidoAsignacion
                With errPed
                    Select Case row("DFLAG")
                        Case 0
                            .codMensaje = "1"
                            .mensaje = "Enviado desde Web Service"
                        Case 1
                            .codMensaje = row("DFLAG")
                            .mensaje = "Enviado a BPCS"
                        Case 2
                            .codMensaje = row("DFLAG")
                            .mensaje = "En BPCS"
                        Case 7
                            .codMensaje = "-1"
                            .mensaje = Trim(row("DDESER"))
                    End Select

                    .Pedido = row("DPEDID")
                    .Linea = row("DLINEA")
                    .Producto = row("DPRODU")
                    .Ubicacion = row("DLOCAL")
                    .Bodega = row("DBODEG")
                    .Cantidad = row("DCANTI")
                    .Lote = row("DLOTE")
                End With
                err.pedidosConNovedad.Add(errPed)
            Next
        End If

        '   Dim resp As String = XLinq15()
        datos.CerrarConexion()
        Return err
    End Function

    Public Function Version() As qVersion Implements IWCFBatch.Version
        Dim detallecadenamysql() As String
        Dim detallemysql() As String
        Dim qVer As New qVersion

        If Not datos.AbrirConexion() Then
            qVer.Version = -1
            qVer.Fecha = Now.ToShortDateString
            qVer.Parametros = ""
            qVer.MySQL = "ERROR DE CONEXION"
            qVer.AS400 = ""
            Return qVer
        End If

        detallecadenamysql = datos.DetalleCadena
        For i = 0 To detallecadenamysql.Count - 1
            detallemysql = Split(detallecadenamysql(i), "=")
            If detallemysql(0).ToUpper = "DATABASE" Then
                qVer.MySQL = detallemysql(1)
                Exit For
            End If
        Next
        qVer.Version = Me.GetType.Assembly.GetName.Version.ToString()
        qVer.Fecha = "2016-04-15"
        qVer.Parametros = "RFPARAM"
        'qVer.MySQL = paramWebConfig(ubicWebConfig, "MYSQL_LIB")
        qVer.AS400 = qAS400()
        Return (qVer)
    End Function

    'Public Function z_PruebaConexion() As qPruebaConexion Implements IWCFBatch.z_PruebaConexion
    '    Dim App As clsApp = New clsApp(System.Reflection.Assembly.GetExecutingAssembly)
    '    Dim appSettings = ConfigurationManager.AppSettings
    '    Dim qVer As New qPruebaConexion
    '    Dim parametro(0) As String
    '    Dim dtBFPARAM As DataTable

    '    If Not datos.AbrirConexion() Then
    '        qVer.MySQL_Server = ""
    '        qVer.MySQL_database = ""
    '        qVer.AS400 = ""
    '        qVer.Mensaje = datos.mensaje
    '        Return qVer
    '    End If

    '    Dim webConfig As String = appSettings("PARAMETROS")
    '    SetConexion()
    '    qVer.MySQL_Server = paramWebConfig(My.Settings.PARAMETROS, "MYSQL_SERVER")
    '    qVer.MySQL_database = paramWebConfig(My.Settings.PARAMETROS, "MYSQL_LIB")
    '    parametro(0) = "AS400_SERVER"
    '    dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 1)
    '    qVer.AS400 = dtBFPARAM.Rows(0).Item(0)

    '    If Not Open() Then
    '        qVer.Mensaje = "Error Abriendo Conexion. " & Descripcion_Error
    '    Else
    '        Try
    '            Dim rs As New ADODB.Recordset
    '            Cursor(rs, "SELECT NOW(), CURRENT_TIMEZONE  FROM SYSIBM.SYSDUMMY1")
    '            If Not rs.EOF Then
    '                qVer.Mensaje = String.Format("Hora AS400: {0:yyyy-MM-dd HH:mm:ss}, Zona horaria: {1}", rs(0).Value, rs(1).Value)
    '            Else
    '                qVer.Mensaje = "error"
    '            End If
    '            Close()
    '        Catch ex As Exception
    '            Dim appPath As String = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath
    '            Dim cadena As String = Replace(appPath & "\cadena.txt", "\\", "\")
    '            Try
    '                ' Create an instance of StreamReader to read from a file.
    '                Dim sb As New StringBuilder()
    '                Dim sw As StreamWriter = New StreamWriter(cadena)
    '                sb.AppendLine("LOG " & ex.ToString)
    '                sb.AppendLine(Now().ToString)
    '                sw.WriteLine(sb.ToString())
    '                sw.Close()
    '            Catch E As Exception

    '            End Try
    '        End Try
    '    End If
    '    datos.CerrarConexion()
    '    Return (qVer)
    'End Function

    Public Function z_PruebaConexion() As qPruebaConexion Implements IWCFBatch.z_PruebaConexion
        Dim App As clsApp = New clsApp(System.Reflection.Assembly.GetExecutingAssembly)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim qVer As New qPruebaConexion
        Dim cadenaAS400 As String
        Dim detcadmysql() As String
        Dim detmyqsl() As String
        Dim detcadas400() As String
        Dim detas400() As String
        Dim dtSys As DataTable

        If Not datos.AbrirConexion() Then
            qVer.MySQL_Server = ""
            qVer.MySQL_database = ""
            qVer.AS400 = ""
            qVer.Mensaje = "9091: " & "Error Abriendo Conexion. " & datos.mensaje
            Return qVer
        End If

        cadenaAS400 = datos.CadenaConexionoledb()

        If Not datosas400.AbrirConexion(cadenaAS400) Then
            qVer.MySQL_Server = ""
            qVer.MySQL_database = ""
            qVer.AS400 = ""
            qVer.Mensaje = "9091: " & "Error Abriendo Conexion. " & datosas400.mensaje & vbCrLf & cadenaAS400 & vbCrLf & "Conexión: " & datosas400.conexionIBM.ConnectionString
            Return qVer
        End If

        detcadmysql = datos.DetalleCadena
        For i = 0 To detcadmysql.Count - 1
            detmyqsl = Split(detcadmysql(i), "=")
            If detmyqsl(0).ToUpper = "SERVER" Then
                qVer.MySQL_Server = detmyqsl(1)
            ElseIf detmyqsl(0).ToUpper = "DATABASE" Then
                qVer.MySQL_database = detmyqsl(1)
            End If
        Next

        detcadas400 = datosas400.DetalleCadena
        For i = 0 To detcadas400.Count - 1
            detas400 = Split(detcadas400(i), "=")
            If detas400(0).ToUpper = "DATA SOURCE" Then
                qVer.AS400 = detas400(1)
            End If
        Next
        dtSys = datosas400.ConsultarSYSIBMSYSDUMMY1dtoledb()
        If dtSys IsNot Nothing Then
            qVer.Mensaje = String.Format("Hora AS400: {0:yyyy-MM-dd HH:mm:ss}, Zona horaria: {1}", dtSys.Rows(0).Item(0), dtSys.Rows(0).Item(1))
        Else
            qVer.Mensaje = "error"
        End If
        datos.CerrarConexion()
        datosas400.CerrarConexion()

        Return (qVer)
    End Function

    'Public Function TransaccionInventario(ByVal xml As String) As TransaccionInventarioRespuesta Implements IWCFBatch.TransaccionInventario
    '    Dim docEntrada As XDocument = XDocument.Parse(xml)
    '    Dim err As New TransaccionInventarioRespuesta
    '    Dim err2 As New Transaccion
    '    Dim continuar As Boolean
    '    Dim parametro(1) As String
    '    Dim dtIIM As DataTable
    '    Dim dtBFPARAM As DataTable
    '    Dim dtILM As DataTable
    '    Dim dtILN As DataTable
    '    Dim dtHPO As DataTable
    '    Dim dtIWM As DataTable
    '    Dim dtFSO As DataTable
    '    Dim tipo As String = ""
    '    Dim itL As TransacDet

    '    If Not datos.AbrirConexion() Then
    '        err.Batch = -1
    '        err.TipoRespuesta = 1
    '        err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '        err.estadoGeneral = -1
    '        err.estadoDescripcionGeneral = "1. Recibido, -1 Con Errores en Conexion" & datos.mensaje
    '        err2.NumeroID = -1
    '        err2.estado = -1
    '        err2.estadoDescripcion = "1. Recibido, -1 Con Errores en Conexion"
    '        err.Transacciones = New List(Of Transaccion)
    '        err.Transacciones.Add(err2)
    '        Return err
    '    End If

    '    If Not AbreConexion(docEntrada.ToString) Then
    '        err.Batch = -1
    '        err.TipoRespuesta = 1
    '        err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '        err.estadoGeneral = -1
    '        err.estadoDescripcionGeneral = "1. Recibido, -1 Con Errores en Conexion"
    '        err2.NumeroID = -1
    '        err2.estado = -1
    '        err2.estadoDescripcion = "1. Recibido, -1 Con Errores en Conexion"
    '        err.Transacciones = New List(Of Transaccion)
    '        err.Transacciones.Add(err2)
    '        datos.CerrarConexion()
    '        Return err
    '    End If

    '    Try
    '        Dim result As String = ""
    '        Dim itH As Transac

    '        'Dim Lineas As New List(Of TransacDet)
    '        Dim qx = From xe In docEntrada.Elements("TransaccionInventario") Select New With { _
    '            .Usuario = xe.Element("Usuario").Value
    '        }

    '        Dim d As New TransacDet


    '        itH = New Transac
    '        With itH
    '            .Usuario = qx.First.Usuario
    '            .Detalle = New List(Of TransacDet)
    '        End With



    '        Dim qd = From xe In docEntrada.Descendants.Elements("Transaccion") Select New With { _
    '        .NumeroID = xe.Element("NumeroID").Value,
    '        .TipoTransaccion = xe.Element("TipoTransaccion").Value,
    '        .Fecha = xe.Element("Fecha").Value,
    '        .OrdenFabricacion = xe.Element("OrdenFabricacion").Value,
    '        .OrdenCompra = xe.Element("OrdenCompra").Value,
    '        .Referencia = xe.Element("Referencia").Value,
    '        .NumeroFactura = xe.Element("NumeroFactura").Value,
    '        .Producto = xe.Element("Producto").Value,
    '        .Lote = xe.Element("Lote").Value,
    '        .Almacen = xe.Element("Almacen").Value,
    '        .Ubicacion = xe.Element("Ubicacion").Value,
    '        .Cantidad = xe.Element("Cantidad").Value,
    '        .Valor = xe.Element("Valor").Value
    '        }

    '        Try
    '            For Each e In qd

    '                itL = New TransacDet
    '                With itL
    '                    'TIPO TRANSACCION
    '                    If e.TipoTransaccion <> "" Then
    '                        parametro(0) = "DITIPTRA"
    '                        parametro(1) = e.TipoTransaccion
    '                        dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 2)
    '                        If dtBFPARAM IsNot Nothing Then
    '                            .TipoTransaccion = e.TipoTransaccion
    '                            tipo = dtBFPARAM.Rows(0).Item(0)
    '                        End If
    '                    Else
    '                        err.Batch = -1
    '                        err.TipoRespuesta = 1
    '                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                        err.estadoGeneral = -1
    '                        err.estadoDescripcionGeneral = "1. Recibido, -1 TipoTransaccion no puede ser nulo"
    '                        err2.NumeroID = -1
    '                        err2.estado = -1
    '                        err2.estadoDescripcion = "1. Recibido, -1 TipoTransaccion no puede ser nulo"
    '                        err.Transacciones = New List(Of Transaccion)
    '                        err.Transacciones.Add(err2)
    '                        Return err
    '                    End If
    '                    'PRODUCTO
    '                    If e.Producto <> "" Then
    '                        parametro(0) = e.Producto
    '                        dtIIM = datos.ConsultarIIMdt(parametro, 0)
    '                        If dtIIM IsNot Nothing Then
    '                            .Producto = e.Producto
    '                        Else
    '                            err.Batch = -1
    '                            err.TipoRespuesta = 1
    '                            err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                            err.estadoGeneral = -1
    '                            err.estadoDescripcionGeneral = "1. Recibido, -1 Producto no existe en base de datos"
    '                            err2.NumeroID = -1
    '                            err2.estado = -1
    '                            err2.estadoDescripcion = "1. Recibido, -1 Producto no existe en base de datos"
    '                            err.Transacciones = New List(Of Transaccion)
    '                            err.Transacciones.Add(err2)
    '                            Return err
    '                        End If
    '                    Else
    '                        err.Batch = -1
    '                        err.TipoRespuesta = 1
    '                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                        err.estadoGeneral = -1
    '                        err.estadoDescripcionGeneral = "1. Recibido, -1 Producto  no puede ser nulo"
    '                        err2.NumeroID = -1
    '                        err2.estado = -1
    '                        err2.estadoDescripcion = "1. Recibido, -1 Producto  no puede ser nulo"
    '                        err.Transacciones = New List(Of Transaccion)
    '                        err.Transacciones.Add(err2)
    '                        Return err
    '                    End If
    '                    'ALMACEN
    '                    If e.Almacen <> "" Then
    '                        parametro(0) = e.Almacen
    '                        dtIWM = datos.ConsultarIWMdt(parametro, 0)
    '                        If dtIWM IsNot Nothing Then
    '                            .Almacen = e.Almacen
    '                        Else
    '                            err.Batch = -1
    '                            err.TipoRespuesta = 1
    '                            err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                            err.estadoGeneral = -1
    '                            err.estadoDescripcionGeneral = "1. Recibido, -1 Almacen no existe en base de datos"
    '                            err2.NumeroID = -1
    '                            err2.estado = -1
    '                            err2.estadoDescripcion = "1. Recibido, -1 Almacen no existe en base de datos"
    '                            err.Transacciones = New List(Of Transaccion)
    '                            err.Transacciones.Add(err2)
    '                            Return err
    '                        End If
    '                    Else
    '                        err.Batch = -1
    '                        err.TipoRespuesta = 1
    '                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                        err.estadoGeneral = -1
    '                        err.estadoDescripcionGeneral = "1. Recibido, -1 Almacen  no puede ser nulo"
    '                        err2.NumeroID = -1
    '                        err2.estado = -1
    '                        err2.estadoDescripcion = "1. Recibido, -1 Almacen  no puede ser nulo"
    '                        err.Transacciones = New List(Of Transaccion)
    '                        err.Transacciones.Add(err2)
    '                        Return err
    '                    End If
    '                    Select Case tipo
    '                        Case "E1"
    '                            'ORDEN DE COMPRA
    '                            If e.OrdenCompra = "" Then
    '                                err.Batch = -1
    '                                err.TipoRespuesta = 1
    '                                err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                                err.estadoGeneral = -1
    '                                err.estadoDescripcionGeneral = "1. Recibido, -1 OrdenCompra no puede ser nulo"
    '                                err2.NumeroID = -1
    '                                err2.estado = -1
    '                                err2.estadoDescripcion = "1. Recibido, -1 OrdenCompra no puede ser nulo"
    '                                err.Transacciones = New List(Of Transaccion)
    '                                err.Transacciones.Add(err2)
    '                                Return err
    '                            Else
    '                                parametro(0) = e.OrdenCompra
    '                                parametro(1) = e.Producto
    '                                dtHPO = datos.ConsultarHPOdt(parametro, 0)
    '                                If dtHPO IsNot Nothing Then
    '                                    .OrdenCompra = e.OrdenCompra
    '                                Else
    '                                    err.Batch = -1
    '                                    err.TipoRespuesta = 1
    '                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                                    err.estadoGeneral = -1
    '                                    err.estadoDescripcionGeneral = "1. Recibido, -1 No existe OrdenCompra / Producto"
    '                                    err2.NumeroID = -1
    '                                    err2.estado = -1
    '                                    err2.estadoDescripcion = "1. Recibido, -1 No existe OrdenCompra / Producto"
    '                                    err.Transacciones = New List(Of Transaccion)
    '                                    err.Transacciones.Add(err2)
    '                                    Return err
    '                                End If
    '                            End If
    '                            'ALMACEN / UBICACION
    '                            If e.Ubicacion <> "" Then
    '                                parametro(0) = e.Almacen
    '                                parametro(1) = e.Ubicacion
    '                                dtILM = datos.ConsultarILMdt(parametro, 0)
    '                                If dtILM IsNot Nothing Then
    '                                    .Ubicacion = e.Ubicacion
    '                                Else
    '                                    err.Batch = -1
    '                                    err.TipoRespuesta = 1
    '                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                                    err.estadoGeneral = -1
    '                                    err.estadoDescripcionGeneral = "1. Recibido, -1 No existe el Almacen con Ubicacion especificada"
    '                                    err2.NumeroID = -1
    '                                    err2.estado = -1
    '                                    err2.estadoDescripcion = "1. Recibido, -1 No existe el Almacen con Ubicacion especificada"
    '                                    err.Transacciones = New List(Of Transaccion)
    '                                    err.Transacciones.Add(err2)
    '                                    Return err
    '                                End If
    '                            Else
    '                                err.Batch = -1
    '                                err.TipoRespuesta = 1
    '                                err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                                err.estadoGeneral = -1
    '                                err.estadoDescripcionGeneral = "1. Recibido, -1 Ubicacion no puede ser nulo"
    '                                err2.NumeroID = -1
    '                                err2.estado = -1
    '                                err2.estadoDescripcion = "1. Recibido, -1 Ubicacion no puede ser nulo"
    '                                err.Transacciones = New List(Of Transaccion)
    '                                err.Transacciones.Add(err2)
    '                                Return err
    '                            End If
    '                        Case "E2"
    '                            'ORDEN DE COMPRA
    '                            If e.OrdenCompra = "" Then
    '                                err.Batch = -1
    '                                err.TipoRespuesta = 1
    '                                err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                                err.estadoGeneral = -1
    '                                err.estadoDescripcionGeneral = "1. Recibido, -1 OrdenCompra no puede ser nulo"
    '                                err2.NumeroID = -1
    '                                err2.estado = -1
    '                                err2.estadoDescripcion = "1. Recibido, -1 OrdenCompra no puede ser nulo"
    '                                err.Transacciones = New List(Of Transaccion)
    '                                err.Transacciones.Add(err2)
    '                                Return err
    '                            Else
    '                                parametro(0) = e.OrdenCompra
    '                                parametro(1) = e.Producto
    '                                dtHPO = datos.ConsultarHPOdt(parametro, 0)
    '                                If dtHPO IsNot Nothing Then
    '                                    .OrdenCompra = e.OrdenCompra
    '                                Else
    '                                    err.Batch = -1
    '                                    err.TipoRespuesta = 1
    '                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                                    err.estadoGeneral = -1
    '                                    err.estadoDescripcionGeneral = "1. Recibido, -1 No existe OrdenCompra"
    '                                    err2.NumeroID = -1
    '                                    err2.estado = -1
    '                                    err2.estadoDescripcion = "1. Recibido, -1 No existe OrdenCompra"
    '                                    err.Transacciones = New List(Of Transaccion)
    '                                    err.Transacciones.Add(err2)
    '                                    Return err
    '                                End If
    '                            End If
    '                            'ALMACEN / UBICACION
    '                            If e.Ubicacion <> "" Then
    '                                parametro(0) = e.Almacen
    '                                parametro(1) = e.Ubicacion
    '                                dtILM = datos.ConsultarILMdt(parametro, 0)
    '                                If dtILM IsNot Nothing Then
    '                                    .Ubicacion = e.Ubicacion
    '                                Else
    '                                    err.Batch = -1
    '                                    err.TipoRespuesta = 1
    '                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                                    err.estadoGeneral = -1
    '                                    err.estadoDescripcionGeneral = "1. Recibido, -1 No existe el Almacen con Ubicacion especificada"
    '                                    err2.NumeroID = -1
    '                                    err2.estado = -1
    '                                    err2.estadoDescripcion = "1. Recibido, -1 No existe el Almacen con Ubicacion especificada"
    '                                    err.Transacciones = New List(Of Transaccion)
    '                                    err.Transacciones.Add(err2)
    '                                    Return err
    '                                End If
    '                            Else
    '                                err.Batch = -1
    '                                err.TipoRespuesta = 1
    '                                err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                                err.estadoGeneral = -1
    '                                err.estadoDescripcionGeneral = "1. Recibido, -1 Ubicacion no puede ser nulo"
    '                                err2.NumeroID = -1
    '                                err2.estado = -1
    '                                err2.estadoDescripcion = "1. Recibido, -1 Ubicacion no puede ser nulo"
    '                                err.Transacciones = New List(Of Transaccion)
    '                                err.Transacciones.Add(err2)
    '                                Return err
    '                            End If
    '                            'FACTURA
    '                            If e.NumeroFactura <> "" Then
    '                                If CDbl(e.NumeroFactura) > 0 Then
    '                                    .NumeroFactura = e.NumeroFactura
    '                                Else
    '                                    err.Batch = -1
    '                                    err.TipoRespuesta = 1
    '                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                                    err.estadoGeneral = -1
    '                                    err.estadoDescripcionGeneral = "1. Recibido, -1 NumeroFactura solo acepta valores numericos mayores a cero"
    '                                    err2.NumeroID = -1
    '                                    err2.estado = -1
    '                                    err2.estadoDescripcion = "1. Recibido, -1 NumeroFactura solo acepta valores numericos mayores a cero"
    '                                    err.Transacciones = New List(Of Transaccion)
    '                                    err.Transacciones.Add(err2)
    '                                    Return err
    '                                End If
    '                            Else
    '                                err.Batch = -1
    '                                err.TipoRespuesta = 1
    '                                err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                                err.estadoGeneral = -1
    '                                err.estadoDescripcionGeneral = "1. Recibido, -1 NumeroFactura no puede ser nulo"
    '                                err2.NumeroID = -1
    '                                err2.estado = -1
    '                                err2.estadoDescripcion = "1. Recibido, -1 NumeroFactura no puede ser nulo"
    '                                err.Transacciones = New List(Of Transaccion)
    '                                err.Transacciones.Add(err2)
    '                                Return err
    '                            End If
    '                        Case "E3"
    '                            'ORDEN FABRICACION
    '                            If e.OrdenFabricacion <> "" Then
    '                                parametro(0) = e.OrdenFabricacion
    '                                dtFSO = datos.ConsultarFSOdt(parametro, 0)
    '                                If dtFSO IsNot Nothing Then
    '                                    .OrdenFabricacion = e.OrdenFabricacion
    '                                Else
    '                                    err.Batch = -1
    '                                    err.TipoRespuesta = 1
    '                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                                    err.estadoGeneral = -1
    '                                    err.estadoDescripcionGeneral = "1. Recibido, -1 No existe OrdenFabricacion"
    '                                    err2.NumeroID = -1
    '                                    err2.estado = -1
    '                                    err2.estadoDescripcion = "1. Recibido, -1 No existe OrdenFabricacion"
    '                                    err.Transacciones = New List(Of Transaccion)
    '                                    err.Transacciones.Add(err2)
    '                                    Return err
    '                                End If
    '                            Else
    '                                err.Batch = -1
    '                                err.TipoRespuesta = 1
    '                                err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                                err.estadoGeneral = -1
    '                                err.estadoDescripcionGeneral = "1. Recibido, -1 OrdenFabricacion no puede ser nulo"
    '                                err2.NumeroID = -1
    '                                err2.estado = -1
    '                                err2.estadoDescripcion = "1. Recibido, -1 OrdenFabricacion no puede ser nulo"
    '                                err.Transacciones = New List(Of Transaccion)
    '                                err.Transacciones.Add(err2)
    '                                Return err
    '                            End If
    '                            'LOTE / PRODUCTO
    '                            If e.Lote <> "" Then
    '                                parametro(0) = e.Lote
    '                                parametro(1) = e.Producto
    '                                dtILN = datos.ConsultarILNdt(parametro, 0)
    '                                If dtILN IsNot Nothing Then
    '                                    .Lote = e.Lote
    '                                Else
    '                                    err.Batch = -1
    '                                    err.TipoRespuesta = 1
    '                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                                    err.estadoGeneral = -1
    '                                    err.estadoDescripcionGeneral = "1. Recibido, -1 No existe Lote/Producto"
    '                                    err2.NumeroID = -1
    '                                    err2.estado = -1
    '                                    err2.estadoDescripcion = "1. Recibido, -1 No existe Lote/Producto"
    '                                    err.Transacciones = New List(Of Transaccion)
    '                                    err.Transacciones.Add(err2)
    '                                    Return err
    '                                End If
    '                            Else
    '                                err.Batch = -1
    '                                err.TipoRespuesta = 1
    '                                err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                                err.estadoGeneral = -1
    '                                err.estadoDescripcionGeneral = "1. Recibido, -1 Lote no puede ser nulo"
    '                                err2.NumeroID = -1
    '                                err2.estado = -1
    '                                err2.estadoDescripcion = "1. Recibido, -1 Lote no puede ser nulo"
    '                                err.Transacciones = New List(Of Transaccion)
    '                                err.Transacciones.Add(err2)
    '                                Return err
    '                            End If
    '                        Case "E4"
    '                            'LOTE / PRODUCTO
    '                            If e.Lote <> "" Then
    '                                parametro(0) = e.Lote
    '                                parametro(1) = e.Producto
    '                                dtILN = datos.ConsultarILNdt(parametro, 0)
    '                                If dtILN IsNot Nothing Then
    '                                    .Lote = e.Lote
    '                                Else
    '                                    err.Batch = -1
    '                                    err.TipoRespuesta = 1
    '                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                                    err.estadoGeneral = -1
    '                                    err.estadoDescripcionGeneral = "1. Recibido, -1 No existe Lote/Producto"
    '                                    err2.NumeroID = -1
    '                                    err2.estado = -1
    '                                    err2.estadoDescripcion = "1. Recibido, -1 No existe Lote/Producto"
    '                                    err.Transacciones = New List(Of Transaccion)
    '                                    err.Transacciones.Add(err2)
    '                                    Return err
    '                                End If
    '                            Else
    '                                err.Batch = -1
    '                                err.TipoRespuesta = 1
    '                                err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                                err.estadoGeneral = -1
    '                                err.estadoDescripcionGeneral = "1. Recibido, -1 Lote no puede ser nulo"
    '                                err2.NumeroID = -1
    '                                err2.estado = -1
    '                                err2.estadoDescripcion = "1. Recibido, -1 Lote no puede ser nulo"
    '                                err.Transacciones = New List(Of Transaccion)
    '                                err.Transacciones.Add(err2)
    '                                Return err
    '                            End If
    '                        Case "E5"
    '                            'REFERENCIA
    '                            If e.Referencia <> "" Then
    '                                If CDbl(e.Referencia) > 0 Then
    '                                    .Referencia = e.Referencia
    '                                Else
    '                                    err.Batch = -1
    '                                    err.TipoRespuesta = 1
    '                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                                    err.estadoGeneral = -1
    '                                    err.estadoDescripcionGeneral = "1. Recibido, -1 Referencia solo acepta valores numericos mayores a cero"
    '                                    err2.NumeroID = -1
    '                                    err2.estado = -1
    '                                    err2.estadoDescripcion = "1. Recibido, -1 Referencia solo acepta valores numericos mayores a cero"
    '                                    err.Transacciones = New List(Of Transaccion)
    '                                    err.Transacciones.Add(err2)
    '                                    Return err
    '                                End If
    '                            Else
    '                                err.Batch = -1
    '                                err.TipoRespuesta = 1
    '                                err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                                err.estadoGeneral = -1
    '                                err.estadoDescripcionGeneral = "1. Recibido, -1 Referencia no puede ser nulo"
    '                                err2.NumeroID = -1
    '                                err2.estado = -1
    '                                err2.estadoDescripcion = "1. Recibido, -1 Referencia no puede ser nulo"
    '                                err.Transacciones = New List(Of Transaccion)
    '                                err.Transacciones.Add(err2)
    '                                Return err
    '                            End If
    '                        Case "E6"
    '                            'REFERENCIA
    '                            If e.Referencia <> "" Then
    '                                If CDbl(e.Referencia) > 0 Then
    '                                    .Referencia = e.Referencia
    '                                Else
    '                                    err.Batch = -1
    '                                    err.TipoRespuesta = 1
    '                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                                    err.estadoGeneral = -1
    '                                    err.estadoDescripcionGeneral = "1. Recibido, -1 Referencia solo acepta valores numericos mayores a cero"
    '                                    err2.NumeroID = -1
    '                                    err2.estado = -1
    '                                    err2.estadoDescripcion = "1. Recibido, -1 Referencia solo acepta valores numericos mayores a cero"
    '                                    err.Transacciones = New List(Of Transaccion)
    '                                    err.Transacciones.Add(err2)
    '                                    Return err
    '                                End If
    '                            Else
    '                                err.Batch = -1
    '                                err.TipoRespuesta = 1
    '                                err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                                err.estadoGeneral = -1
    '                                err.estadoDescripcionGeneral = "1. Recibido, -1 Referencia no puede ser nulo"
    '                                err2.NumeroID = -1
    '                                err2.estado = -1
    '                                err2.estadoDescripcion = "1. Recibido, -1 Referencia no puede ser nulo"
    '                                err.Transacciones = New List(Of Transaccion)
    '                                err.Transacciones.Add(err2)
    '                                Return err
    '                            End If
    '                            'ALMACEN / UBICACION
    '                            If e.Ubicacion <> "" Then
    '                                parametro(0) = e.Almacen
    '                                parametro(1) = e.Ubicacion
    '                                dtILM = datos.ConsultarILMdt(parametro, 0)
    '                                If dtILM IsNot Nothing Then
    '                                    .Ubicacion = e.Ubicacion
    '                                Else
    '                                    err.Batch = -1
    '                                    err.TipoRespuesta = 1
    '                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                                    err.estadoGeneral = -1
    '                                    err.estadoDescripcionGeneral = "1. Recibido, -1 No existe el Almacen con Ubicacion especificada"
    '                                    err2.NumeroID = -1
    '                                    err2.estado = -1
    '                                    err2.estadoDescripcion = "1. Recibido, -1 No existe el Almacen con Ubicacion especificada"
    '                                    err.Transacciones = New List(Of Transaccion)
    '                                    err.Transacciones.Add(err2)
    '                                    Return err
    '                                End If
    '                            Else
    '                                err.Batch = -1
    '                                err.TipoRespuesta = 1
    '                                err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                                err.estadoGeneral = -1
    '                                err.estadoDescripcionGeneral = "1. Recibido, -1 Ubicacion no puede ser nulo"
    '                                err2.NumeroID = -1
    '                                err2.estado = -1
    '                                err2.estadoDescripcion = "1. Recibido, -1 Ubicacion no puede ser nulo"
    '                                err.Transacciones = New List(Of Transaccion)
    '                                err.Transacciones.Add(err2)
    '                                Return err
    '                            End If
    '                    End Select
    '                    'NUMEROID
    '                    If e.NumeroID <> "" Then
    '                        If IsNumeric(e.NumeroID) Then
    '                            .NumeroID = e.NumeroID
    '                        Else
    '                            err.Batch = -1
    '                            err.TipoRespuesta = 1
    '                            err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                            err.estadoGeneral = -1
    '                            err.estadoDescripcionGeneral = "1. Recibido, -1 NumeroID solo acepta valores numericos"
    '                            err2.NumeroID = -1
    '                            err2.estado = -1
    '                            err2.estadoDescripcion = "1. Recibido, -1 NumeroID solo acepta valores numericos"
    '                            err.Transacciones = New List(Of Transaccion)
    '                            err.Transacciones.Add(err2)
    '                            Return err
    '                        End If
    '                    Else
    '                        err.Batch = -1
    '                        err.TipoRespuesta = 1
    '                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                        err.estadoGeneral = -1
    '                        err.estadoDescripcionGeneral = "1. Recibido, -1 NumeroID no puede ser nulo"
    '                        err2.NumeroID = -1
    '                        err2.estado = -1
    '                        err2.estadoDescripcion = "1. Recibido, -1 NumeroID no puede ser nulo"
    '                        err.Transacciones = New List(Of Transaccion)
    '                        err.Transacciones.Add(err2)
    '                        Return err
    '                    End If
    '                    'FECHA
    '                    If e.Fecha <> "" Then
    '                        'If IsNumeric(e.Fecha) Then
    '                        .Fecha = CDate(e.Fecha).ToString("yyyyMMdd")
    '                        'Else
    '                        '    err.Batch = -1
    '                        '    err.TipoRespuesta = 1
    '                        '    err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                        '    err.estadoGeneral = -1
    '                        '    err.estadoDescripcionGeneral = "1. Recibido, -1 Fecha debe ser numerico"
    '                        '    err2.NumeroID = -1
    '                        '    err2.estado = -1
    '                        '    err2.estadoDescripcion = "1. Recibido, -1 Fecha debe ser numerico"
    '                        '    err.Transacciones = New List(Of Transaccion)
    '                        '    err.Transacciones.Add(err2)
    '                        '    Return err
    '                        'End If
    '                    Else
    '                        err.Batch = -1
    '                        err.TipoRespuesta = 1
    '                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                        err.estadoGeneral = -1
    '                        err.estadoDescripcionGeneral = "1. Recibido, -1 Fecha no puede ser nulo"
    '                        err2.NumeroID = -1
    '                        err2.estado = -1
    '                        err2.estadoDescripcion = "1. Recibido, -1 Fecha no puede ser nulo"
    '                        err.Transacciones = New List(Of Transaccion)
    '                        err.Transacciones.Add(err2)
    '                        Return err
    '                    End If
    '                    'CANTIDAD
    '                    If e.Cantidad <> "" Then
    '                        If CDbl(e.Cantidad) > 0 Then
    '                            .Cantidad = e.Cantidad
    '                        Else
    '                            err.Batch = -1
    '                            err.TipoRespuesta = 1
    '                            err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                            err.estadoGeneral = -1
    '                            err.estadoDescripcionGeneral = "1. Recibido, -1 Cantidad solo acepta valores numericos mayores a cero"
    '                            err2.NumeroID = -1
    '                            err2.estado = -1
    '                            err2.estadoDescripcion = "1. Recibido, -1 Cantidad solo acepta valores numericos mayores a cero"
    '                            err.Transacciones = New List(Of Transaccion)
    '                            err.Transacciones.Add(err2)
    '                            Return err
    '                        End If
    '                    Else
    '                        err.Batch = -1
    '                        err.TipoRespuesta = 1
    '                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                        err.estadoGeneral = -1
    '                        err.estadoDescripcionGeneral = "1. Recibido, -1 Cantidad  no puede ser nulo"
    '                        err2.NumeroID = -1
    '                        err2.estado = -1
    '                        err2.estadoDescripcion = "1. Recibido, -1 Cantidad  no puede ser nulo"
    '                        err.Transacciones = New List(Of Transaccion)
    '                        err.Transacciones.Add(err2)
    '                        Return err
    '                    End If
    '                    .Valor = IIf(e.Valor = "", "0", e.Valor)

    '                End With
    '                itH.Detalle.Add(itL)
    '            Next
    '        Catch ex2 As Exception
    '            err.Batch = -1
    '            err.TipoRespuesta = 1
    '            err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '            err.estadoGeneral = -1
    '            err.estadoDescripcionGeneral = "1. Recibido, -1 Posible error en estructura"
    '            err2.NumeroID = -1
    '            err2.estado = -1
    '            err2.estadoDescripcion = "1. Recibido, -1 Posible error en estructura"
    '            err.Transacciones = New List(Of Transaccion)
    '            err.Transacciones.Add(err2)
    '            datos.CerrarConexion()
    '            Return err
    '        End Try
    '        result &= "[" & itH.Usuario & "]"
    '        Dim cons
    '        Dim batch
    '        batch = datos.CalcConsec("CIMNUM")
    '        err.Transacciones = New List(Of Transaccion)
    '        For Each e In itH.Detalle
    '            err2 = New Transaccion
    '            With e
    '                cons = datos.CalcConsec("CIMNUM")
    '                Dim tabla As New BFCIM
    '                With tabla
    '                    .IFUENTE = "TRANSAC. INV."
    '                    .IBATCH = batch
    '                    .INID = "IN"
    '                    .INTRAN = e.TipoTransaccion
    '                    .INCTNR = 0
    '                    .INMFGR = 0
    '                    .INDATE = Replace(e.Fecha, "-", "")
    '                    .INTRN = Right(cons, 5)
    '                    .INMLOT = "DE" & cons
    '                    If e.NumeroFactura <> "" Then
    '                        .INFACTU = e.NumeroFactura
    '                    Else
    '                        .INFACTU = "NULL"
    '                    End If
    '                    If e.OrdenFabricacion <> "" Then
    '                        .INREF = e.OrdenFabricacion
    '                    ElseIf e.OrdenCompra <> "" Then
    '                        .INREF = e.OrdenCompra
    '                    ElseIf e.Referencia <> "" Then
    '                        .INREF = e.Referencia
    '                    Else
    '                        .INREF = 0
    '                    End If
    '                    .INPROD = e.Producto
    '                    .INLOT = e.Lote
    '                    '.INLOT = IIf(e.Lote = Nothing, "NULL", e.Lote)
    '                    .INLOC = e.Ubicacion
    '                    'canti1 = -(CDbl(e.Cantidad))
    '                    '.INQTY = canti1
    '                    .INQTY = e.Cantidad
    '                    .INCBY = itH.Usuario
    '                    .INLDT = Format(Now(), "yyyyMMdd")
    '                    .INLTM = Format(Now(), "hhmmss")
    '                    .INCDT = Year(Now).ToString & Month(Now).ToString("00") & Day(Now).ToString("00")
    '                    .INCTM = Now.Hour.ToString("00") & Now.Minute.ToString("00") & Now.Second.ToString("00")
    '                    .INUMERO = e.NumeroID
    '                    .INCOST = e.Valor
    '                    .INWHSE = e.Almacen

    '                End With
    '                continuar = datos.GuardarBFCIMRow(tabla, 0)
    '                If continuar = False Then
    '                    err.Batch = -1
    '                    err.TipoRespuesta = 1
    '                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '                    err.estadoGeneral = -1
    '                    err.estadoDescripcionGeneral = "1. Recibido, -1 Error " & datos.mensaje
    '                    err2.NumeroID = -1
    '                    err2.estado = -1
    '                    err2.estadoDescripcion = "1. Recibido, -1 Posible error en estructura"
    '                    err.Transacciones = New List(Of Transaccion)
    '                    err.Transacciones.Add(err2)
    '                    datos.CerrarConexion()
    '                    Return err
    '                End If
    '            End With
    '            err2.NumeroID = e.NumeroID
    '            err2.estado = 1
    '            err2.estadoDescripcion = "1. Recibido, 1 Sin Errores"
    '            err.Transacciones.Add(err2)
    '        Next
    '        err.Batch = batch
    '        err.TipoRespuesta = 1
    '        err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '        err.estadoGeneral = 1
    '        err.estadoDescripcionGeneral = "1. Recibido, 1 Sin Errores"
    '        datos.CerrarConexion()
    '        Return err
    '    Catch ex As Exception
    '        err.Batch = -1
    '        err.TipoRespuesta = 1
    '        err.TipoRespuestaDescripcion = "1. Validacion de datos"
    '        err.estadoGeneral = -1
    '        err.estadoDescripcionGeneral = "1. Recibido, -1 Con Errores <A>" & docEntrada.ToString & "</A><B>" & ex.ToString & "</B>"
    '        err2.NumeroID = -1
    '        err2.estado = 1
    '        err2.estadoDescripcion = "1. Recibido, -1 Con Errores <A>" & docEntrada.ToString & "</A><B>" & ex.ToString & "</B>"
    '        err.Transacciones = New List(Of Transaccion)
    '        err.Transacciones.Add(err2)
    '        datos.CerrarConexion()
    '        Return err
    '    End Try
    'End Function

    Public Function TransaccionInventarioEstado(ByVal batch As Integer) As TransaccionInventarioRespuesta Implements IWCFBatch.TransaccionInventarioEstado
        Dim err As New TransaccionInventarioRespuesta
        Dim err2 As Transaccion
        Dim parametro(0) As String
        Dim dtBFCIM As DataTable
        Dim dtBFCIM2 As DataTable

        If Not datos.AbrirConexion() Then
            err.Batch = -1
            err.TipoRespuesta = 1
            err.TipoRespuestaDescripcion = "1. Validacion de datos"
            err.estadoGeneral = -1
            err.estadoDescripcionGeneral = "1. Recibido, -1 Con Errores en Conexion" & datos.mensaje
            err2 = New Transaccion
            err2.NumeroID = -1
            err2.estado = -1
            err2.estadoDescripcion = "1. Recibido, -1 Con Errores en Conexion"
            err.Transacciones = New List(Of Transaccion)
            err.Transacciones.Add(err2)
            Return err
        End If

        AbreConexion("Estado Batch")

        parametro(0) = batch
        dtBFCIM = datos.ConsultarBFCIMdt(parametro, 0)
        If dtBFCIM IsNot Nothing Then
            dtBFCIM2 = datos.ConsultarBFCIMdt(parametro, 1)
            If dtBFCIM2.Rows(0).Item("TRANSACCIONES") > 0 Then
                err.Batch = batch
                err.TipoRespuesta = 1
                err.TipoRespuestaDescripcion = "1. Validación de datos"
                err.estadoGeneral = -1
                err.estadoDescripcionGeneral = "-1 Con Novedad"
                err.Transacciones = New List(Of Transaccion)
                For Each row As DataRow In dtBFCIM.Rows
                    err2 = New Transaccion
                    Select Case row("IFLAG")
                        Case 0
                            err2.NumeroID = row("INUMERO")
                            err2.estado = 0
                            err2.estadoDescripcion = "0. Enviado desde Web"
                        Case 1
                            err2.NumeroID = row("INUMERO")
                            err2.estado = 1
                            err2.estadoDescripcion = "1. Enviado a CIM 600"
                        Case 2
                            err2.NumeroID = row("INUMERO")
                            err2.estado = 2
                            err2.estadoDescripcion = "2. En BPCS"
                        Case 7
                            err2.NumeroID = row("INUMERO")
                            err2.estado = 7
                            err2.estadoDescripcion = "7. " & Trim(row("INERROR"))
                    End Select
                    err.Transacciones.Add(err2)
                Next
            Else
                err.Batch = batch
                err.TipoRespuesta = 1
                err.TipoRespuestaDescripcion = "1. Validación de datos"
                err.estadoGeneral = 1
                err.estadoDescripcionGeneral = "1. Recibido, OK"
                err.Transacciones = New List(Of Transaccion)
                For Each row As DataRow In dtBFCIM.Rows
                    err2 = New Transaccion
                    Select Case row("IFLAG")
                        Case 0
                            err2.NumeroID = row("INUMERO")
                            err2.estado = 0
                            err2.estadoDescripcion = "0. Enviado desde Web"
                        Case 1
                            err2.NumeroID = row("INUMERO")
                            err2.estado = 1
                            err2.estadoDescripcion = "1. Enviado a CIM 600"
                        Case 2
                            err2.NumeroID = row("INUMERO")
                            err2.estado = 2
                            err2.estadoDescripcion = "2. En BPCS"
                        Case 7
                            err2.NumeroID = row("INUMERO")
                            err2.estado = 7
                            err2.estadoDescripcion = "7. " & Trim(row("INERROR"))
                    End Select
                    err.Transacciones.Add(err2)
                Next
            End If
        End If
        datos.CerrarConexion()
        Return err
    End Function

    Function AbreConexion(docEntrada As String) As Boolean
        Dim tabla As New BFWSINBOX
        Dim continuar As Boolean

        c_ID = datos.CalcConsec("BFWSINBOX")
        With tabla
            .CID = c_ID
            .CXML = docEntrada
        End With
        continuar = datos.GuardarBFWSINBOXRow(tabla, 0)
        If continuar = True Then
            Return True
        Else
            Return False
        End If
    End Function

    Function qAS400() As String
        Dim dtRFPARAM As DataTable
        Dim parametro(1) As String

        If Not datos.AbrirConexion() Then
            Return datos.mensaje
        End If

        parametro(1) = "AS400_SERVER"
        dtRFPARAM = datos.ConsultarRFPARAMdt(parametro, 0)
        If dtRFPARAM IsNot Nothing Then
            Return dtRFPARAM.Rows(0).Item(0)
        Else
            Return ""
        End If
    End Function

    Class qPedidos
        Public Pedido As String
        Public Sub New(ByVal Pedido As String)
            Me.Pedido = Pedido
        End Sub
    End Class

    Public Function TransaccionInventario(ByVal xml As String) As TransaccionInventarioRespuesta Implements IWCFBatch.TransaccionInventario
        Dim docEntrada As XDocument = XDocument.Parse(xml)
        Dim err As New TransaccionInventarioRespuesta
        Dim err2 As New Transaccion
        Dim continuar As Boolean
        Dim parametro(1) As String
        Dim dtIIM As DataTable
        Dim dtBFPARAM As DataTable
        Dim dtILM As DataTable
        Dim dtILN As DataTable
        Dim dtHPO As DataTable
        Dim dtIWM As DataTable
        Dim dtFSO As DataTable
        Dim tipo As String = ""
        Dim itL As TransacDet
        Dim UltimaLinea As Integer
        Dim r As New Globalization.CultureInfo("es-ES")
        r.NumberFormat.CurrencyDecimalSeparator = "."
        r.NumberFormat.NumberDecimalSeparator = "."
        System.Threading.Thread.CurrentThread.CurrentCulture = r

        If Not datos.AbrirConexion() Then
            err.Batch = -1
            err.TipoRespuesta = 1
            err.TipoRespuestaDescripcion = "1. Validacion de datos"
            err.estadoGeneral = -1
            err.estadoDescripcionGeneral = "1. Recibido, -1 Con Errores en Conexion" & datos.mensaje
            err2.NumeroID = -1
            err2.estado = -1
            err2.estadoDescripcion = "1. Recibido, -1 Con Errores en Conexion"
            err.Transacciones = New List(Of Transaccion)
            err.Transacciones.Add(err2)
            Return err
        End If

        If Not AbreConexion(docEntrada.ToString) Then
            err.Batch = -1
            err.TipoRespuesta = 1
            err.TipoRespuestaDescripcion = "1. Validacion de datos"
            err.estadoGeneral = -1
            err.estadoDescripcionGeneral = "1. Recibido, -1 Con Errores en Conexion"
            err2.NumeroID = -1
            err2.estado = -1
            err2.estadoDescripcion = "1. Recibido, -1 Con Errores en Conexion"
            err.Transacciones = New List(Of Transaccion)
            err.Transacciones.Add(err2)
            datos.CerrarConexion()
            Return err
        End If

        Try
            Dim result As String = ""
            Dim itH As Transac

            Dim qx = From xe In docEntrada.Elements("TransaccionInventario") Select New With { _
                .Usuario = xe.Element("Usuario").Value
            }

            itH = New Transac
            With itH
                .Usuario = qx.First.Usuario
                .Detalle = New List(Of TransacDet)
            End With

            Dim qd = From xe In docEntrada.Descendants.Elements("Transaccion") Select New With { _
            .NumeroID = xe.Element("NumeroID").Value,
            .TipoTransaccion = xe.Element("TipoTransaccion").Value,
            .Fecha = xe.Element("Fecha").Value,
            .OrdenFabricacion = xe.Element("OrdenFabricacion").Value,
            .OrdenCompra = xe.Element("OrdenCompra").Value,
            .Referencia = xe.Element("Referencia").Value,
            .NumeroFactura = xe.Element("NumeroFactura").Value,
            .Producto = xe.Element("Producto").Value,
            .Lote = xe.Element("Lote").Value,
            .Almacen = xe.Element("Almacen").Value,
            .Ubicacion = xe.Element("Ubicacion").Value,
            .Cantidad = xe.Element("Cantidad").Value,
            .Valor = xe.Element("Valor").Value
            }

            Try
                For Each e In qd

                    itL = New TransacDet
                    With itL
                        'TIPO TRANSACCION
                        If e.TipoTransaccion <> "" Then
                            parametro(0) = "DITIPTRA"
                            parametro(1) = e.TipoTransaccion
                            dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 2)
                            If dtBFPARAM IsNot Nothing Then
                                .TipoTransaccion = e.TipoTransaccion
                                tipo = dtBFPARAM.Rows(0).Item(0)
                            End If
                        Else
                            err.Batch = -1
                            err.TipoRespuesta = 1
                            err.TipoRespuestaDescripcion = "1. Validacion de datos"
                            err.estadoGeneral = -1
                            err.estadoDescripcionGeneral = "1. Recibido, -1 TipoTransaccion no puede ser nulo"
                            err2.NumeroID = -1
                            err2.estado = -1
                            err2.estadoDescripcion = "1. Recibido, -1 TipoTransaccion no puede ser nulo"
                            err.Transacciones = New List(Of Transaccion)
                            err.Transacciones.Add(err2)
                            Return err
                        End If
                        'PRODUCTO
                        If e.Producto <> "" Then
                            parametro(0) = e.Producto
                            dtIIM = datos.ConsultarIIMdt(parametro, 0)
                            If dtIIM IsNot Nothing Then
                                .Producto = e.Producto
                            Else
                                err.Batch = -1
                                err.TipoRespuesta = 1
                                err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                err.estadoGeneral = -1
                                err.estadoDescripcionGeneral = "1. Recibido, -1 Producto no existe en base de datos"
                                err2.NumeroID = -1
                                err2.estado = -1
                                err2.estadoDescripcion = "1. Recibido, -1 Producto no existe en base de datos"
                                err.Transacciones = New List(Of Transaccion)
                                err.Transacciones.Add(err2)
                                Return err
                            End If
                        Else
                            err.Batch = -1
                            err.TipoRespuesta = 1
                            err.TipoRespuestaDescripcion = "1. Validacion de datos"
                            err.estadoGeneral = -1
                            err.estadoDescripcionGeneral = "1. Recibido, -1 Producto  no puede ser nulo"
                            err2.NumeroID = -1
                            err2.estado = -1
                            err2.estadoDescripcion = "1. Recibido, -1 Producto  no puede ser nulo"
                            err.Transacciones = New List(Of Transaccion)
                            err.Transacciones.Add(err2)
                            Return err
                        End If
                        'ALMACEN
                        If e.Almacen <> "" Then
                            parametro(0) = e.Almacen
                            dtIWM = datos.ConsultarIWMdt(parametro, 0)
                            If dtIWM IsNot Nothing Then
                                .Almacen = e.Almacen
                            Else
                                err.Batch = -1
                                err.TipoRespuesta = 1
                                err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                err.estadoGeneral = -1
                                err.estadoDescripcionGeneral = "1. Recibido, -1 Almacen no existe en base de datos"
                                err2.NumeroID = -1
                                err2.estado = -1
                                err2.estadoDescripcion = "1. Recibido, -1 Almacen no existe en base de datos"
                                err.Transacciones = New List(Of Transaccion)
                                err.Transacciones.Add(err2)
                                Return err
                            End If
                        Else
                            err.Batch = -1
                            err.TipoRespuesta = 1
                            err.TipoRespuestaDescripcion = "1. Validacion de datos"
                            err.estadoGeneral = -1
                            err.estadoDescripcionGeneral = "1. Recibido, -1 Almacen  no puede ser nulo"
                            err2.NumeroID = -1
                            err2.estado = -1
                            err2.estadoDescripcion = "1. Recibido, -1 Almacen  no puede ser nulo"
                            err.Transacciones = New List(Of Transaccion)
                            err.Transacciones.Add(err2)
                            Return err
                        End If
                        'NUMEROID
                        If e.NumeroID <> "" Then
                            If IsNumeric(e.NumeroID) Then
                                .NumeroID = e.NumeroID
                            Else
                                err.Batch = -1
                                err.TipoRespuesta = 1
                                err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                err.estadoGeneral = -1
                                err.estadoDescripcionGeneral = "1. Recibido, -1 NumeroID solo acepta valores numericos"
                                err2.NumeroID = -1
                                err2.estado = -1
                                err2.estadoDescripcion = "1. Recibido, -1 NumeroID solo acepta valores numericos"
                                err.Transacciones = New List(Of Transaccion)
                                err.Transacciones.Add(err2)
                                Return err
                            End If
                        Else
                            err.Batch = -1
                            err.TipoRespuesta = 1
                            err.TipoRespuestaDescripcion = "1. Validacion de datos"
                            err.estadoGeneral = -1
                            err.estadoDescripcionGeneral = "1. Recibido, -1 NumeroID no puede ser nulo"
                            err2.NumeroID = -1
                            err2.estado = -1
                            err2.estadoDescripcion = "1. Recibido, -1 NumeroID no puede ser nulo"
                            err.Transacciones = New List(Of Transaccion)
                            err.Transacciones.Add(err2)
                            Return err
                        End If
                        'FECHA
                        If e.Fecha <> "" Then
                            .Fecha = CDate(e.Fecha).ToString("yyyyMMdd")
                        Else
                            err.Batch = -1
                            err.TipoRespuesta = 1
                            err.TipoRespuestaDescripcion = "1. Validacion de datos"
                            err.estadoGeneral = -1
                            err.estadoDescripcionGeneral = "1. Recibido, -1 Fecha no puede ser nulo"
                            err2.NumeroID = -1
                            err2.estado = -1
                            err2.estadoDescripcion = "1. Recibido, -1 Fecha no puede ser nulo"
                            err.Transacciones = New List(Of Transaccion)
                            err.Transacciones.Add(err2)
                            Return err
                        End If
                        'CANTIDAD
                        If e.Cantidad <> "" Then
                            If IsNumeric(e.Cantidad) Then
                                If CDbl(e.Cantidad) <> 0 Then
                                    .Cantidad = e.Cantidad
                                Else
                                    err.Batch = -1
                                    err.TipoRespuesta = 1
                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    err.estadoGeneral = -1
                                    err.estadoDescripcionGeneral = "1. Recibido, -1 Cantidad solo acepta valores numericos mayores a cero"
                                    err2.NumeroID = -1
                                    err2.estado = -1
                                    err2.estadoDescripcion = "1. Recibido, -1 Cantidad solo acepta valores numericos mayores a cero"
                                    err.Transacciones = New List(Of Transaccion)
                                    err.Transacciones.Add(err2)
                                    Return err
                                End If
                            Else
                                err.Batch = -1
                                err.TipoRespuesta = 1
                                err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                err.estadoGeneral = -1
                                err.estadoDescripcionGeneral = "1. Recibido, -1 Cantidad solo acepta valores numericos mayores a cero"
                                err2.NumeroID = -1
                                err2.estado = -1
                                err2.estadoDescripcion = "1. Recibido, -1 Cantidad solo acepta valores numericos mayores a cero"
                                err.Transacciones = New List(Of Transaccion)
                                err.Transacciones.Add(err2)
                                Return err
                            End If
                        Else
                            err.Batch = -1
                            err.TipoRespuesta = 1
                            err.TipoRespuestaDescripcion = "1. Validacion de datos"
                            err.estadoGeneral = -1
                            err.estadoDescripcionGeneral = "1. Recibido, -1 Cantidad  no puede ser nulo"
                            err2.NumeroID = -1
                            err2.estado = -1
                            err2.estadoDescripcion = "1. Recibido, -1 Cantidad  no puede ser nulo"
                            err.Transacciones = New List(Of Transaccion)
                            err.Transacciones.Add(err2)
                            Return err
                        End If

                        'REFERENCIA
                        If e.Referencia <> "" Then
                            .Referencia = e.Referencia
                        End If
                        .Valor = IIf(e.Valor = "", "0", e.Valor)
                        'FACTURA
                        If e.NumeroFactura <> "" Then
                            'If IsNumeric(e.NumeroFactura) Then
                            .NumeroFactura = e.NumeroFactura
                            'Else
                            '    err.Batch = -1
                            '    err.TipoRespuesta = 1
                            '    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                            '    err.estadoGeneral = -1
                            '    err.estadoDescripcionGeneral = "1. Recibido, -1 NumeroFactura solo acepta valores numericos mayores a cero"
                            '    err2.NumeroID = -1
                            '    err2.estado = -1
                            '    err2.estadoDescripcion = "1. Recibido, -1 NumeroFactura solo acepta valores numericos mayores a cero"
                            '    err.Transacciones = New List(Of Transaccion)
                            '    err.Transacciones.Add(err2)
                            '    Return err
                            'End If
                        End If
                        'LOTE
                        If e.Lote <> "" Then
                            .Lote = e.Lote
                        End If

                        'UBICACIOIN
                        If e.Ubicacion <> "" Then
                            .Ubicacion = e.Ubicacion
                        End If
                        Select Case tipo
                            Case "E1"
                                'ORDEN DE COMPRA
                                If e.OrdenCompra = "" Then
                                    err.Batch = -1
                                    err.TipoRespuesta = 1
                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    err.estadoGeneral = -1
                                    err.estadoDescripcionGeneral = "1. Recibido, -1 OrdenCompra no puede ser nulo"
                                    err2.NumeroID = -1
                                    err2.estado = -1
                                    err2.estadoDescripcion = "1. Recibido, -1 OrdenCompra no puede ser nulo"
                                    err.Transacciones = New List(Of Transaccion)
                                    err.Transacciones.Add(err2)
                                    Return err
                                Else
                                    parametro(0) = e.OrdenCompra
                                    parametro(1) = e.Producto
                                    dtHPO = datos.ConsultarHPOdt(parametro, 0)
                                    If dtHPO IsNot Nothing Then
                                        .OrdenCompra = e.OrdenCompra
                                    Else
                                        err.Batch = -1
                                        err.TipoRespuesta = 1
                                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                        err.estadoGeneral = -1
                                        err.estadoDescripcionGeneral = "1. Recibido, -1 No existe OrdenCompra / Producto"
                                        err2.NumeroID = -1
                                        err2.estado = -1
                                        err2.estadoDescripcion = "1. Recibido, -1 No existe OrdenCompra / Producto"
                                        err.Transacciones = New List(Of Transaccion)
                                        err.Transacciones.Add(err2)
                                        Return err
                                    End If
                                End If
                                'ALMACEN / UBICACION
                                If e.Ubicacion <> "" Then
                                    parametro(0) = e.Almacen
                                    parametro(1) = e.Ubicacion
                                    dtILM = datos.ConsultarILMdt(parametro, 0)
                                    If dtILM IsNot Nothing Then
                                        .Ubicacion = e.Ubicacion
                                    Else
                                        err.Batch = -1
                                        err.TipoRespuesta = 1
                                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                        err.estadoGeneral = -1
                                        err.estadoDescripcionGeneral = "1. Recibido, -1 No existe el Almacen con Ubicacion especificada"
                                        err2.NumeroID = -1
                                        err2.estado = -1
                                        err2.estadoDescripcion = "1. Recibido, -1 No existe el Almacen con Ubicacion especificada"
                                        err.Transacciones = New List(Of Transaccion)
                                        err.Transacciones.Add(err2)
                                        Return err
                                    End If
                                Else
                                    .Ubicacion = " "
                                    'err.Batch = -1
                                    'err.TipoRespuesta = 1
                                    'err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    'err.estadoGeneral = -1
                                    'err.estadoDescripcionGeneral = "1. Recibido, -1 Ubicacion no puede ser nulo"
                                    'err2.NumeroID = -1
                                    'err2.estado = -1
                                    'err2.estadoDescripcion = "1. Recibido, -1 Ubicacion no puede ser nulo"
                                    'err.Transacciones = New List(Of Transaccion)
                                    'err.Transacciones.Add(err2)
                                    'Return err
                                End If
                            Case "E2"
                                'ORDEN DE COMPRA
                                If e.OrdenCompra = "" Then
                                    err.Batch = -1
                                    err.TipoRespuesta = 1
                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    err.estadoGeneral = -1
                                    err.estadoDescripcionGeneral = "1. Recibido, -1 OrdenCompra no puede ser nulo"
                                    err2.NumeroID = -1
                                    err2.estado = -1
                                    err2.estadoDescripcion = "1. Recibido, -1 OrdenCompra no puede ser nulo"
                                    err.Transacciones = New List(Of Transaccion)
                                    err.Transacciones.Add(err2)
                                    Return err
                                Else
                                    parametro(0) = e.OrdenCompra
                                    parametro(1) = e.Producto
                                    dtHPO = datos.ConsultarHPOdt(parametro, 0)
                                    If dtHPO IsNot Nothing Then
                                        .OrdenCompra = e.OrdenCompra
                                    Else
                                        err.Batch = -1
                                        err.TipoRespuesta = 1
                                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                        err.estadoGeneral = -1
                                        err.estadoDescripcionGeneral = "1. Recibido, -1 No existe OrdenCompra"
                                        err2.NumeroID = -1
                                        err2.estado = -1
                                        err2.estadoDescripcion = "1. Recibido, -1 No existe OrdenCompra"
                                        err.Transacciones = New List(Of Transaccion)
                                        err.Transacciones.Add(err2)
                                        Return err
                                    End If
                                End If
                                'ALMACEN / UBICACION
                                If e.Ubicacion <> "" Then
                                    parametro(0) = e.Almacen
                                    parametro(1) = e.Ubicacion
                                    dtILM = datos.ConsultarILMdt(parametro, 0)
                                    If dtILM IsNot Nothing Then
                                        .Ubicacion = e.Ubicacion
                                    Else
                                        err.Batch = -1
                                        err.TipoRespuesta = 1
                                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                        err.estadoGeneral = -1
                                        err.estadoDescripcionGeneral = "1. Recibido, -1 No existe el Almacen con Ubicacion especificada"
                                        err2.NumeroID = -1
                                        err2.estado = -1
                                        err2.estadoDescripcion = "1. Recibido, -1 No existe el Almacen con Ubicacion especificada"
                                        err.Transacciones = New List(Of Transaccion)
                                        err.Transacciones.Add(err2)
                                        Return err
                                    End If
                                Else
                                    .Ubicacion = " "
                                    'err.Batch = -1
                                    'err.TipoRespuesta = 1
                                    'err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    'err.estadoGeneral = -1
                                    'err.estadoDescripcionGeneral = "1. Recibido, -1 Ubicacion no puede ser nulo"
                                    'err2.NumeroID = -1
                                    'err2.estado = -1
                                    'err2.estadoDescripcion = "1. Recibido, -1 Ubicacion no puede ser nulo"
                                    'err.Transacciones = New List(Of Transaccion)
                                    'err.Transacciones.Add(err2)
                                    'Return err
                                End If
                                'FACTURA
                                If e.NumeroFactura <> "" Then
                                    'If IsNumeric(e.NumeroFactura) Then
                                    'If CDbl(e.NumeroFactura) > 0 Then
                                    .NumeroFactura = e.NumeroFactura
                                    'Else
                                    '    err.Batch = -1
                                    '    err.TipoRespuesta = 1
                                    '    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    '    err.estadoGeneral = -1
                                    '    err.estadoDescripcionGeneral = "1. Recibido, -1 NumeroFactura solo acepta valores numericos mayores a cero"
                                    '    err2.NumeroID = -1
                                    '    err2.estado = -1
                                    '    err2.estadoDescripcion = "1. Recibido, -1 NumeroFactura solo acepta valores numericos mayores a cero"
                                    '    err.Transacciones = New List(Of Transaccion)
                                    '    err.Transacciones.Add(err2)
                                    '    Return err
                                    'End If
                                    'Else
                                    '    err.Batch = -1
                                    '    err.TipoRespuesta = 1
                                    '    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    '    err.estadoGeneral = -1
                                    '    err.estadoDescripcionGeneral = "1. Recibido, -1 NumeroFactura solo acepta valores numericos mayores a cero"
                                    '    err2.NumeroID = -1
                                    '    err2.estado = -1
                                    '    err2.estadoDescripcion = "1. Recibido, -1 NumeroFactura solo acepta valores numericos mayores a cero"
                                    '    err.Transacciones = New List(Of Transaccion)
                                    '    err.Transacciones.Add(err2)
                                    '    Return err
                                    'End If
                                Else
                                    err.Batch = -1
                                    err.TipoRespuesta = 1
                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    err.estadoGeneral = -1
                                    err.estadoDescripcionGeneral = "1. Recibido, -1 NumeroFactura no puede ser nulo"
                                    err2.NumeroID = -1
                                    err2.estado = -1
                                    err2.estadoDescripcion = "1. Recibido, -1 NumeroFactura no puede ser nulo"
                                    err.Transacciones = New List(Of Transaccion)
                                    err.Transacciones.Add(err2)
                                    Return err
                                End If
                            Case "E3"
                                'ORDEN FABRICACION
                                If e.OrdenFabricacion <> "" Then
                                    parametro(0) = e.OrdenFabricacion
                                    dtFSO = datos.ConsultarFSOdt(parametro, 0)
                                    If dtFSO IsNot Nothing Then
                                        .OrdenFabricacion = e.OrdenFabricacion
                                    Else
                                        err.Batch = -1
                                        err.TipoRespuesta = 1
                                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                        err.estadoGeneral = -1
                                        err.estadoDescripcionGeneral = "1. Recibido, -1 No existe OrdenFabricacion"
                                        err2.NumeroID = -1
                                        err2.estado = -1
                                        err2.estadoDescripcion = "1. Recibido, -1 No existe OrdenFabricacion"
                                        err.Transacciones = New List(Of Transaccion)
                                        err.Transacciones.Add(err2)
                                        Return err
                                    End If
                                Else
                                    err.Batch = -1
                                    err.TipoRespuesta = 1
                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    err.estadoGeneral = -1
                                    err.estadoDescripcionGeneral = "1. Recibido, -1 OrdenFabricacion no puede ser nulo"
                                    err2.NumeroID = -1
                                    err2.estado = -1
                                    err2.estadoDescripcion = "1. Recibido, -1 OrdenFabricacion no puede ser nulo"
                                    err.Transacciones = New List(Of Transaccion)
                                    err.Transacciones.Add(err2)
                                    Return err
                                End If
                                parametro(0) = e.Producto
                                dtIIM = datos.ConsultarIIMdt(parametro, 1)
                                If dtIIM IsNot Nothing Then
                                    If dtIIM.Rows(0).Item(0) IsNot DBNull.Value Then
                                        If dtIIM.Rows(0).Item(0) = 2 Then
                                            'LOTE / PRODUCTO
                                            If e.Lote <> "" Then
                                                parametro(0) = e.Lote
                                                parametro(1) = e.Producto
                                                dtILN = datos.ConsultarILNdt(parametro, 0)
                                                If dtILN IsNot Nothing Then
                                                    .Lote = e.Lote
                                                Else
                                                    err.Batch = -1
                                                    err.TipoRespuesta = 1
                                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                                    err.estadoGeneral = -1
                                                    err.estadoDescripcionGeneral = "1. Recibido, -1 No existe Lote/Producto"
                                                    err2.NumeroID = -1
                                                    err2.estado = -1
                                                    err2.estadoDescripcion = "1. Recibido, -1 No existe Lote/Producto"
                                                    err.Transacciones = New List(Of Transaccion)
                                                    err.Transacciones.Add(err2)
                                                    Return err
                                                End If
                                            Else
                                                err.Batch = -1
                                                err.TipoRespuesta = 1
                                                err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                                err.estadoGeneral = -1
                                                err.estadoDescripcionGeneral = "1. Recibido, -1 Lote no puede ser nulo"
                                                err2.NumeroID = -1
                                                err2.estado = -1
                                                err2.estadoDescripcion = "1. Recibido, -1 Lote no puede ser nulo"
                                                err.Transacciones = New List(Of Transaccion)
                                                err.Transacciones.Add(err2)
                                                Return err
                                            End If
                                        Else
                                            .Lote = e.Lote
                                        End If
                                    Else
                                        err.Batch = -1
                                        err.TipoRespuesta = 1
                                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                        err.estadoGeneral = -1
                                        err.estadoDescripcionGeneral = "1. Recibido, -1 Falta definir Lot Control"
                                        err2.NumeroID = -1
                                        err2.estado = -1
                                        err2.estadoDescripcion = "1. Recibido, -1 Falta definir Lot Control"
                                        err.Transacciones = New List(Of Transaccion)
                                        err.Transacciones.Add(err2)
                                        Return err
                                    End If
                                End If
                            Case "E4"
                                parametro(0) = e.Producto
                                dtIIM = datos.ConsultarIIMdt(parametro, 1)
                                If dtIIM IsNot Nothing Then
                                    If dtIIM.Rows(0).Item(0) IsNot DBNull.Value Then
                                        If dtIIM.Rows(0).Item(0) = 2 Then
                                            'LOTE / PRODUCTO
                                            If e.Lote <> "" Then
                                                parametro(0) = e.Lote
                                                parametro(1) = e.Producto
                                                dtILN = datos.ConsultarILNdt(parametro, 0)
                                                If dtILN IsNot Nothing Then
                                                    .Lote = e.Lote
                                                Else
                                                    err.Batch = -1
                                                    err.TipoRespuesta = 1
                                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                                    err.estadoGeneral = -1
                                                    err.estadoDescripcionGeneral = "1. Recibido, -1 No existe Lote/Producto"
                                                    err2.NumeroID = -1
                                                    err2.estado = -1
                                                    err2.estadoDescripcion = "1. Recibido, -1 No existe Lote/Producto"
                                                    err.Transacciones = New List(Of Transaccion)
                                                    err.Transacciones.Add(err2)
                                                    Return err
                                                End If
                                            Else
                                                err.Batch = -1
                                                err.TipoRespuesta = 1
                                                err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                                err.estadoGeneral = -1
                                                err.estadoDescripcionGeneral = "1. Recibido, -1 Lote no puede ser nulo"
                                                err2.NumeroID = -1
                                                err2.estado = -1
                                                err2.estadoDescripcion = "1. Recibido, -1 Lote no puede ser nulo"
                                                err.Transacciones = New List(Of Transaccion)
                                                err.Transacciones.Add(err2)
                                                Return err
                                            End If
                                        Else
                                            .Lote = e.Lote
                                        End If
                                    Else
                                        err.Batch = -1
                                        err.TipoRespuesta = 1
                                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                        err.estadoGeneral = -1
                                        err.estadoDescripcionGeneral = "1. Recibido, -1 Falta definir Lot Control"
                                        err2.NumeroID = -1
                                        err2.estado = -1
                                        err2.estadoDescripcion = "1. Recibido, -1 Falta definir Lot Control"
                                        err.Transacciones = New List(Of Transaccion)
                                        err.Transacciones.Add(err2)
                                        Return err
                                    End If
                                End If
                            Case "E5"
                                'REFERENCIA
                                If e.Referencia <> "" Then
                                    If IsNumeric(e.Referencia) Then
                                        If CDbl(e.Referencia) > 0 Then
                                            .Referencia = e.Referencia
                                        Else
                                            err.Batch = -1
                                            err.TipoRespuesta = 1
                                            err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                            err.estadoGeneral = -1
                                            err.estadoDescripcionGeneral = "1. Recibido, -1 Referencia solo acepta valores numericos mayores a cero"
                                            err2.NumeroID = -1
                                            err2.estado = -1
                                            err2.estadoDescripcion = "1. Recibido, -1 Referencia solo acepta valores numericos mayores a cero"
                                            err.Transacciones = New List(Of Transaccion)
                                            err.Transacciones.Add(err2)
                                            Return err
                                        End If
                                    Else
                                        err.Batch = -1
                                        err.TipoRespuesta = 1
                                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                        err.estadoGeneral = -1
                                        err.estadoDescripcionGeneral = "1. Recibido, -1 Referencia solo acepta valores numericos mayores a cero"
                                        err2.NumeroID = -1
                                        err2.estado = -1
                                        err2.estadoDescripcion = "1. Recibido, -1 Referencia solo acepta valores numericos mayores a cero"
                                        err.Transacciones = New List(Of Transaccion)
                                        err.Transacciones.Add(err2)
                                        Return err
                                    End If
                                Else
                                    err.Batch = -1
                                    err.TipoRespuesta = 1
                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    err.estadoGeneral = -1
                                    err.estadoDescripcionGeneral = "1. Recibido, -1 Referencia no puede ser nulo"
                                    err2.NumeroID = -1
                                    err2.estado = -1
                                    err2.estadoDescripcion = "1. Recibido, -1 Referencia no puede ser nulo"
                                    err.Transacciones = New List(Of Transaccion)
                                    err.Transacciones.Add(err2)
                                    Return err
                                End If
                            Case "E6"
                                'REFERENCIA
                                If e.Referencia <> "" Then
                                    If IsNumeric(e.Referencia) Then
                                        If CDbl(e.Referencia) > 0 Then
                                            .Referencia = e.Referencia
                                        Else
                                            err.Batch = -1
                                            err.TipoRespuesta = 1
                                            err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                            err.estadoGeneral = -1
                                            err.estadoDescripcionGeneral = "1. Recibido, -1 Referencia solo acepta valores numericos mayores a cero"
                                            err2.NumeroID = -1
                                            err2.estado = -1
                                            err2.estadoDescripcion = "1. Recibido, -1 Referencia solo acepta valores numericos mayores a cero"
                                            err.Transacciones = New List(Of Transaccion)
                                            err.Transacciones.Add(err2)
                                            Return err
                                        End If
                                    Else
                                        err.Batch = -1
                                        err.TipoRespuesta = 1
                                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                        err.estadoGeneral = -1
                                        err.estadoDescripcionGeneral = "1. Recibido, -1 Referencia solo acepta valores numericos mayores a cero"
                                        err2.NumeroID = -1
                                        err2.estado = -1
                                        err2.estadoDescripcion = "1. Recibido, -1 Referencia solo acepta valores numericos mayores a cero"
                                        err.Transacciones = New List(Of Transaccion)
                                        err.Transacciones.Add(err2)
                                        Return err
                                    End If
                                Else
                                    err.Batch = -1
                                    err.TipoRespuesta = 1
                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    err.estadoGeneral = -1
                                    err.estadoDescripcionGeneral = "1. Recibido, -1 Referencia no puede ser nulo"
                                    err2.NumeroID = -1
                                    err2.estado = -1
                                    err2.estadoDescripcion = "1. Recibido, -1 Referencia no puede ser nulo"
                                    err.Transacciones = New List(Of Transaccion)
                                    err.Transacciones.Add(err2)
                                    Return err
                                End If
                                'ALMACEN / UBICACION
                                If e.Ubicacion <> "" Then
                                    parametro(0) = e.Almacen
                                    parametro(1) = e.Ubicacion
                                    dtILM = datos.ConsultarILMdt(parametro, 0)
                                    If dtILM IsNot Nothing Then
                                        .Ubicacion = e.Ubicacion
                                    Else
                                        err.Batch = -1
                                        err.TipoRespuesta = 1
                                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                        err.estadoGeneral = -1
                                        err.estadoDescripcionGeneral = "1. Recibido, -1 No existe el Almacen con Ubicacion especificada"
                                        err2.NumeroID = -1
                                        err2.estado = -1
                                        err2.estadoDescripcion = "1. Recibido, -1 No existe el Almacen con Ubicacion especificada"
                                        err.Transacciones = New List(Of Transaccion)
                                        err.Transacciones.Add(err2)
                                        Return err
                                    End If
                                Else
                                    err.Batch = -1
                                    err.TipoRespuesta = 1
                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    err.estadoGeneral = -1
                                    err.estadoDescripcionGeneral = "1. Recibido, -1 Ubicacion no puede ser nulo"
                                    err2.NumeroID = -1
                                    err2.estado = -1
                                    err2.estadoDescripcion = "1. Recibido, -1 Ubicacion no puede ser nulo"
                                    err.Transacciones = New List(Of Transaccion)
                                    err.Transacciones.Add(err2)
                                    Return err
                                End If
                        End Select
                    End With
                    itH.Detalle.Add(itL)
                Next
            Catch ex2 As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\InfoError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine(ex2.Message & vbCrLf & ex2.ToString)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                err.Batch = -1
                err.TipoRespuesta = 1
                err.TipoRespuestaDescripcion = "1. Validacion de datos"
                err.estadoGeneral = -1
                err.estadoDescripcionGeneral = "1. Recibido, -1 Posible error en estructura"
                err2.NumeroID = -1
                err2.estado = -1
                err2.estadoDescripcion = "1. Recibido, -1 Posible error en estructura"
                err.Transacciones = New List(Of Transaccion)
                err.Transacciones.Add(err2)
                datos.CerrarConexion()
                Return err
            End Try
            result &= "[" & itH.Usuario & "]"
            Dim cons
            Dim batch
            batch = datos.CalcConsec("CIMNUM")
            err.Transacciones = New List(Of Transaccion)
            For Each e In itH.Detalle
                err2 = New Transaccion
                With e

                    Dim tabla As New BFCIM
                    If e.TipoTransaccion = "UC" Then
                        parametro(0) = e.OrdenCompra
                        parametro(1) = e.Producto
                        dtHPO = datos.ConsultarHPOdt(parametro, 1)
                        If dtHPO IsNot Nothing Then
                            UltimaLinea = dtHPO.Rows(0).Item("MAXLINE")
                        End If

                        Dim CantidadLinea As Double
                        Dim ValorUnitario As Double = e.Valor / e.Cantidad
                        parametro(0) = e.OrdenCompra
                        parametro(1) = e.Producto
                        dtHPO = datos.ConsultarHPOdt(parametro, 2)
                        If dtHPO IsNot Nothing Then
                            For Each row As DataRow In dtHPO.Rows
                                cons = datos.CalcConsec("CIMNUM")
                                If row("PLINE") = UltimaLinea Then
                                    CantidadLinea = e.Cantidad
                                    e.Cantidad = 0
                                ElseIf row("PQREC") + e.Cantidad > row("PQORD") Then
                                    CantidadLinea = row("PQORD") - row("PQREC")
                                    e.Cantidad -= CantidadLinea
                                Else
                                    CantidadLinea = e.Cantidad
                                    e.Cantidad = 0
                                End If
                                If CantidadLinea <> 0 Then
                                    With tabla
                                        .IFUENTE = "TRANSAC. INV."
                                        .IBATCH = batch
                                        .INID = "IN"
                                        .INTRAN = e.TipoTransaccion
                                        .INCTNR = 0
                                        .INMFGR = 0
                                        .INDATE = Replace(e.Fecha, "-", "")
                                        .INTRN = Right(cons, 5)
                                        .INMLOT = "DE" & cons
                                        .INFACTU = IIf(e.NumeroFactura = "", "NULL", e.NumeroFactura)
                                        If e.OrdenFabricacion <> "" Then
                                            .INREF = e.OrdenFabricacion
                                        ElseIf e.OrdenCompra <> "" Then
                                            .INREF = e.OrdenCompra
                                        ElseIf e.Referencia <> "" Then
                                            .INREF = e.Referencia
                                        Else
                                            .INREF = 0
                                        End If
                                        .INPROD = e.Producto
                                        .INLOT = IIf(e.Lote = "", " ", e.Lote)
                                        .INLOC = e.Ubicacion
                                        .INQTY = CantidadLinea
                                        .INCBY = itH.Usuario
                                        .INLDT = Format(Now(), "yyyyMMdd")
                                        .INLTM = Format(Now(), "hhmmss")
                                        .INCDT = Year(Now).ToString & Month(Now).ToString("00") & Day(Now).ToString("00")
                                        .INCTM = Now.Hour.ToString("00") & Now.Minute.ToString("00") & Now.Second.ToString("00")
                                        .INUMERO = e.NumeroID
                                        .INCOST = CantidadLinea * ValorUnitario
                                        .INWHSE = e.Almacen
                                    End With
                                    continuar = datos.GuardarBFCIMRow(tabla, 0)
                                    If continuar = False Then
                                        err.Batch = -1
                                        err.TipoRespuesta = 1
                                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                        err.estadoGeneral = -1
                                        err.estadoDescripcionGeneral = "1. Recibido, -1 Error " & datos.mensaje
                                        err2.NumeroID = -1
                                        err2.estado = -1
                                        err2.estadoDescripcion = "1. Recibido, -1 Posible error en estructura"
                                        err.Transacciones = New List(Of Transaccion)
                                        err.Transacciones.Add(err2)
                                        datos.CerrarConexion()
                                        Return err
                                    End If
                                End If
                            Next
                        End If
                    Else
                        cons = datos.CalcConsec("CIMNUM")
                        With tabla
                            .IFUENTE = "TRANSAC. INV."
                            .IBATCH = batch
                            .INID = "IN"
                            .INTRAN = e.TipoTransaccion
                            .INCTNR = 0
                            .INMFGR = 0
                            .INDATE = Replace(e.Fecha, "-", "")
                            .INTRN = Right(cons, 5)
                            .INMLOT = "DE" & cons
                            .INFACTU = IIf(e.NumeroFactura = "", "NULL", e.NumeroFactura)
                            If e.OrdenFabricacion <> "" Then
                                .INREF = e.OrdenFabricacion
                            ElseIf e.OrdenCompra <> "" Then
                                .INREF = e.OrdenCompra
                            ElseIf e.Referencia <> "" Then
                                .INREF = e.Referencia
                            Else
                                .INREF = 0
                            End If
                            .INPROD = e.Producto
                            .INLOT = IIf(e.Lote = "", " ", e.Lote)
                            .INLOC = e.Ubicacion
                            .INQTY = e.Cantidad
                            .INCBY = itH.Usuario
                            .INLDT = Format(Now(), "yyyyMMdd")
                            .INLTM = Format(Now(), "hhmmss")
                            .INCDT = Year(Now).ToString & Month(Now).ToString("00") & Day(Now).ToString("00")
                            .INCTM = Now.Hour.ToString("00") & Now.Minute.ToString("00") & Now.Second.ToString("00")
                            .INUMERO = e.NumeroID
                            .INCOST = e.Valor
                            .INWHSE = e.Almacen
                        End With
                        continuar = datos.GuardarBFCIMRow(tabla, 0)
                        continuar = True

                        If continuar = False Then
                            err.Batch = -1
                            err.TipoRespuesta = 1
                            err.TipoRespuestaDescripcion = "1. Validacion de datos"
                            err.estadoGeneral = -1
                            err.estadoDescripcionGeneral = "1. Recibido, -1 Error " & datos.mensaje
                            err2.NumeroID = -1
                            err2.estado = -1
                            err2.estadoDescripcion = "1. Recibido, -1 Posible error en estructura"
                            err.Transacciones = New List(Of Transaccion)
                            err.Transacciones.Add(err2)
                            datos.CerrarConexion()
                            Return err
                        End If
                    End If
                End With
                err2.NumeroID = e.NumeroID
                err2.estado = 1
                err2.estadoDescripcion = "1. Recibido, 1 Sin Errores"
                err.Transacciones.Add(err2)
            Next
            err.Batch = batch
            err.TipoRespuesta = 1
            err.TipoRespuestaDescripcion = "1. Validacion de datos"
            err.estadoGeneral = 1
            err.estadoDescripcionGeneral = "1. Recibido, 1 Sin Errores"
            datos.CerrarConexion()
            Return err
        Catch ex As Exception
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\InfoError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine(ex.Message & vbCrLf & ex.ToString)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            err.Batch = -1
            err.TipoRespuesta = 1
            err.TipoRespuestaDescripcion = "1. Validacion de datos"
            err.estadoGeneral = -1
            err.estadoDescripcionGeneral = "1. Recibido, -1 Con Errores <A>" & docEntrada.ToString & "</A><B>" & ex.ToString & "</B>"
            err2.NumeroID = -1
            err2.estado = 1
            err2.estadoDescripcion = "1. Recibido, -1 Con Errores <A>" & docEntrada.ToString & "</A><B>" & ex.ToString & "</B>"
            err.Transacciones = New List(Of Transaccion)
            err.Transacciones.Add(err2)
            datos.CerrarConexion()
            Return err
        End Try
    End Function

    Public Function TransaccionInventario2(ByVal xml As String) As TransaccionInventarioRespuesta
        Dim docEntrada As XDocument = XDocument.Parse(xml)
        Dim err As New TransaccionInventarioRespuesta
        Dim err2 As New Transaccion
        Dim continuar As Boolean
        Dim parametro(1) As String
        Dim dtIIM As DataTable
        Dim dtBFPARAM As DataTable
        Dim dtILM As DataTable
        Dim dtILN As DataTable
        Dim dtHPO As DataTable
        Dim dtIWM As DataTable
        Dim dtFSO As DataTable
        Dim tipo As String = ""
        Dim itL As TransacDet
        Dim UltimaLinea As Integer
        Dim r As New Globalization.CultureInfo("es-ES")
        r.NumberFormat.CurrencyDecimalSeparator = "."
        r.NumberFormat.NumberDecimalSeparator = "."
        System.Threading.Thread.CurrentThread.CurrentCulture = r

        If Not datos.AbrirConexion() Then
            err.Batch = -1
            err.TipoRespuesta = 1
            err.TipoRespuestaDescripcion = "1. Validacion de datos"
            err.estadoGeneral = -1
            err.estadoDescripcionGeneral = "1. Recibido, -1 Con Errores en Conexion" & datos.mensaje
            err2.NumeroID = -1
            err2.estado = -1
            err2.estadoDescripcion = "1. Recibido, -1 Con Errores en Conexion"
            err.Transacciones = New List(Of Transaccion)
            err.Transacciones.Add(err2)
            Return err
        End If

        If Not AbreConexion(docEntrada.ToString) Then
            err.Batch = -1
            err.TipoRespuesta = 1
            err.TipoRespuestaDescripcion = "1. Validacion de datos"
            err.estadoGeneral = -1
            err.estadoDescripcionGeneral = "1. Recibido, -1 Con Errores en Conexion"
            err2.NumeroID = -1
            err2.estado = -1
            err2.estadoDescripcion = "1. Recibido, -1 Con Errores en Conexion"
            err.Transacciones = New List(Of Transaccion)
            err.Transacciones.Add(err2)
            datos.CerrarConexion()
            Return err
        End If

        Try
            Dim result As String = ""
            Dim itH As Transac

            Dim qx = From xe In docEntrada.Elements("TransaccionInventario") Select New With { _
                .Usuario = xe.Element("Usuario").Value
            }

            itH = New Transac
            With itH
                .Usuario = qx.First.Usuario
                .Detalle = New List(Of TransacDet)
            End With

            Dim qd = From xe In docEntrada.Descendants.Elements("Transaccion") Select New With { _
            .NumeroID = xe.Element("NumeroID").Value,
            .TipoTransaccion = xe.Element("TipoTransaccion").Value,
            .Fecha = xe.Element("Fecha").Value,
            .OrdenFabricacion = xe.Element("OrdenFabricacion").Value,
            .OrdenCompra = xe.Element("OrdenCompra").Value,
            .Referencia = xe.Element("Referencia").Value,
            .NumeroFactura = xe.Element("NumeroFactura").Value,
            .Producto = xe.Element("Producto").Value,
            .Lote = xe.Element("Lote").Value,
            .Almacen = xe.Element("Almacen").Value,
            .Ubicacion = xe.Element("Ubicacion").Value,
            .Cantidad = xe.Element("Cantidad").Value,
            .Valor = xe.Element("Valor").Value
            }

            Try
                For Each e In qd

                    itL = New TransacDet
                    With itL
                        'TIPO TRANSACCION
                        If e.TipoTransaccion <> "" Then
                            parametro(0) = "DITIPTRA"
                            parametro(1) = e.TipoTransaccion
                            dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 2)
                            If dtBFPARAM IsNot Nothing Then
                                .TipoTransaccion = e.TipoTransaccion
                                tipo = dtBFPARAM.Rows(0).Item(0)
                            End If
                        Else
                            err.Batch = -1
                            err.TipoRespuesta = 1
                            err.TipoRespuestaDescripcion = "1. Validacion de datos"
                            err.estadoGeneral = -1
                            err.estadoDescripcionGeneral = "1. Recibido, -1 TipoTransaccion no puede ser nulo"
                            err2.NumeroID = -1
                            err2.estado = -1
                            err2.estadoDescripcion = "1. Recibido, -1 TipoTransaccion no puede ser nulo"
                            err.Transacciones = New List(Of Transaccion)
                            err.Transacciones.Add(err2)
                            Return err
                        End If
                        'PRODUCTO
                        If e.Producto <> "" Then
                            parametro(0) = e.Producto
                            dtIIM = datos.ConsultarIIMdt(parametro, 0)
                            If dtIIM IsNot Nothing Then
                                .Producto = e.Producto
                            Else
                                err.Batch = -1
                                err.TipoRespuesta = 1
                                err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                err.estadoGeneral = -1
                                err.estadoDescripcionGeneral = "1. Recibido, -1 Producto no existe en base de datos"
                                err2.NumeroID = -1
                                err2.estado = -1
                                err2.estadoDescripcion = "1. Recibido, -1 Producto no existe en base de datos"
                                err.Transacciones = New List(Of Transaccion)
                                err.Transacciones.Add(err2)
                                Return err
                            End If
                            '===========================================================
                            'LOTE VALIDACION
                            parametro(0) = e.Producto
                            dtIIM = datos.ConsultarIIMdt(parametro, 1)
                            If dtIIM IsNot Nothing Then
                                If dtIIM.Rows(0).Item(0) IsNot DBNull.Value Then
                                    If dtIIM.Rows(0).Item(0) = 2 Then
                                        'LOTE / PRODUCTO
                                        If e.Lote <> "" Then
                                            parametro(0) = e.Lote
                                            parametro(1) = e.Producto
                                            dtILN = datos.ConsultarILNdt(parametro, 0)
                                            If dtILN IsNot Nothing Then
                                                .Lote = e.Lote
                                            Else
                                                err.Batch = -1
                                                err.TipoRespuesta = 1
                                                err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                                err.estadoGeneral = -1
                                                err.estadoDescripcionGeneral = "1. Recibido, -1 No existe Lote/Producto"
                                                err2.NumeroID = -1
                                                err2.estado = -1
                                                err2.estadoDescripcion = "1. Recibido, -1 No existe Lote/Producto"
                                                err.Transacciones = New List(Of Transaccion)
                                                err.Transacciones.Add(err2)
                                                Return err
                                            End If
                                        Else
                                            parametro(0) = " "
                                            parametro(1) = e.Producto
                                            dtILN = datos.ConsultarILNdt(parametro, 0)
                                            If dtILN IsNot Nothing Then
                                                .Lote = " "
                                            Else
                                                err.Batch = -1
                                                err.TipoRespuesta = 1
                                                err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                                err.estadoGeneral = -1
                                                err.estadoDescripcionGeneral = "1. Recibido, -1 Lote no puede ser nulo"
                                                err2.NumeroID = -1
                                                err2.estado = -1
                                                err2.estadoDescripcion = "1. Recibido, -1 Lote no puede ser nulo"
                                                err.Transacciones = New List(Of Transaccion)
                                                err.Transacciones.Add(err2)
                                                Return err
                                            End If
                                        End If
                                    Else
                                        .Lote = e.Lote
                                    End If
                                Else
                                    err.Batch = -1
                                    err.TipoRespuesta = 1
                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    err.estadoGeneral = -1
                                    err.estadoDescripcionGeneral = "1. Recibido, -1 Falta definir Lot Control"
                                    err2.NumeroID = -1
                                    err2.estado = -1
                                    err2.estadoDescripcion = "1. Recibido, -1 Falta definir Lot Control"
                                    err.Transacciones = New List(Of Transaccion)
                                    err.Transacciones.Add(err2)
                                    Return err
                                End If
                            End If
                        Else
                            err.Batch = -1
                            err.TipoRespuesta = 1
                            err.TipoRespuestaDescripcion = "1. Validacion de datos"
                            err.estadoGeneral = -1
                            err.estadoDescripcionGeneral = "1. Recibido, -1 Producto  no puede ser nulo"
                            err2.NumeroID = -1
                            err2.estado = -1
                            err2.estadoDescripcion = "1. Recibido, -1 Producto  no puede ser nulo"
                            err.Transacciones = New List(Of Transaccion)
                            err.Transacciones.Add(err2)
                            Return err
                        End If
                        'ALMACEN
                        If e.Almacen <> "" Then
                            parametro(0) = e.Almacen
                            dtIWM = datos.ConsultarIWMdt(parametro, 0)
                            If dtIWM IsNot Nothing Then
                                .Almacen = e.Almacen
                            Else
                                err.Batch = -1
                                err.TipoRespuesta = 1
                                err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                err.estadoGeneral = -1
                                err.estadoDescripcionGeneral = "1. Recibido, -1 Almacen no existe en base de datos"
                                err2.NumeroID = -1
                                err2.estado = -1
                                err2.estadoDescripcion = "1. Recibido, -1 Almacen no existe en base de datos"
                                err.Transacciones = New List(Of Transaccion)
                                err.Transacciones.Add(err2)
                                Return err
                            End If
                        Else
                            err.Batch = -1
                            err.TipoRespuesta = 1
                            err.TipoRespuestaDescripcion = "1. Validacion de datos"
                            err.estadoGeneral = -1
                            err.estadoDescripcionGeneral = "1. Recibido, -1 Almacen  no puede ser nulo"
                            err2.NumeroID = -1
                            err2.estado = -1
                            err2.estadoDescripcion = "1. Recibido, -1 Almacen  no puede ser nulo"
                            err.Transacciones = New List(Of Transaccion)
                            err.Transacciones.Add(err2)
                            Return err
                        End If
                        'NUMEROID
                        If e.NumeroID <> "" Then
                            If IsNumeric(e.NumeroID) Then
                                .NumeroID = e.NumeroID
                            Else
                                err.Batch = -1
                                err.TipoRespuesta = 1
                                err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                err.estadoGeneral = -1
                                err.estadoDescripcionGeneral = "1. Recibido, -1 NumeroID solo acepta valores numericos"
                                err2.NumeroID = -1
                                err2.estado = -1
                                err2.estadoDescripcion = "1. Recibido, -1 NumeroID solo acepta valores numericos"
                                err.Transacciones = New List(Of Transaccion)
                                err.Transacciones.Add(err2)
                                Return err
                            End If
                        Else
                            err.Batch = -1
                            err.TipoRespuesta = 1
                            err.TipoRespuestaDescripcion = "1. Validacion de datos"
                            err.estadoGeneral = -1
                            err.estadoDescripcionGeneral = "1. Recibido, -1 NumeroID no puede ser nulo"
                            err2.NumeroID = -1
                            err2.estado = -1
                            err2.estadoDescripcion = "1. Recibido, -1 NumeroID no puede ser nulo"
                            err.Transacciones = New List(Of Transaccion)
                            err.Transacciones.Add(err2)
                            Return err
                        End If
                        'FECHA
                        If e.Fecha <> "" Then
                            .Fecha = CDate(e.Fecha).ToString("yyyyMMdd")
                        Else
                            err.Batch = -1
                            err.TipoRespuesta = 1
                            err.TipoRespuestaDescripcion = "1. Validacion de datos"
                            err.estadoGeneral = -1
                            err.estadoDescripcionGeneral = "1. Recibido, -1 Fecha no puede ser nulo"
                            err2.NumeroID = -1
                            err2.estado = -1
                            err2.estadoDescripcion = "1. Recibido, -1 Fecha no puede ser nulo"
                            err.Transacciones = New List(Of Transaccion)
                            err.Transacciones.Add(err2)
                            Return err
                        End If
                        'CANTIDAD
                        If e.Cantidad <> "" Then
                            If IsNumeric(e.Cantidad) Then
                                If CDbl(e.Cantidad) > 0 Then
                                    .Cantidad = e.Cantidad
                                Else
                                    err.Batch = -1
                                    err.TipoRespuesta = 1
                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    err.estadoGeneral = -1
                                    err.estadoDescripcionGeneral = "1. Recibido, -1 Cantidad solo acepta valores numericos mayores a cero"
                                    err2.NumeroID = -1
                                    err2.estado = -1
                                    err2.estadoDescripcion = "1. Recibido, -1 Cantidad solo acepta valores numericos mayores a cero"
                                    err.Transacciones = New List(Of Transaccion)
                                    err.Transacciones.Add(err2)
                                    Return err
                                End If
                            Else
                                err.Batch = -1
                                err.TipoRespuesta = 1
                                err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                err.estadoGeneral = -1
                                err.estadoDescripcionGeneral = "1. Recibido, -1 Cantidad solo acepta valores numericos mayores a cero"
                                err2.NumeroID = -1
                                err2.estado = -1
                                err2.estadoDescripcion = "1. Recibido, -1 Cantidad solo acepta valores numericos mayores a cero"
                                err.Transacciones = New List(Of Transaccion)
                                err.Transacciones.Add(err2)
                                Return err
                            End If
                        Else
                            err.Batch = -1
                            err.TipoRespuesta = 1
                            err.TipoRespuestaDescripcion = "1. Validacion de datos"
                            err.estadoGeneral = -1
                            err.estadoDescripcionGeneral = "1. Recibido, -1 Cantidad  no puede ser nulo"
                            err2.NumeroID = -1
                            err2.estado = -1
                            err2.estadoDescripcion = "1. Recibido, -1 Cantidad  no puede ser nulo"
                            err.Transacciones = New List(Of Transaccion)
                            err.Transacciones.Add(err2)
                            Return err
                        End If

                        'REFERENCIA
                        If e.Referencia <> "" Then
                            .Referencia = e.Referencia
                        End If
                        .Valor = IIf(e.Valor = "", "0", e.Valor)
                        'FACTURA
                        If e.NumeroFactura <> "" Then
                            'If IsNumeric(e.NumeroFactura) Then
                            .NumeroFactura = e.NumeroFactura
                            'Else
                            '    err.Batch = -1
                            '    err.TipoRespuesta = 1
                            '    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                            '    err.estadoGeneral = -1
                            '    err.estadoDescripcionGeneral = "1. Recibido, -1 NumeroFactura solo acepta valores numericos mayores a cero"
                            '    err2.NumeroID = -1
                            '    err2.estado = -1
                            '    err2.estadoDescripcion = "1. Recibido, -1 NumeroFactura solo acepta valores numericos mayores a cero"
                            '    err.Transacciones = New List(Of Transaccion)
                            '    err.Transacciones.Add(err2)
                            '    Return err
                            'End If
                        End If
                        ''LOTE
                        'If e.Lote <> "" Then
                        '    .Lote = e.Lote
                        'End If


                        'UBICACIOIN
                        If e.Ubicacion <> "" Then
                            .Ubicacion = e.Ubicacion
                        End If
                        Select Case tipo
                            Case "E1"
                                'ORDEN DE COMPRA
                                If e.OrdenCompra = "" Then
                                    err.Batch = -1
                                    err.TipoRespuesta = 1
                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    err.estadoGeneral = -1
                                    err.estadoDescripcionGeneral = "1. Recibido, -1 OrdenCompra no puede ser nulo"
                                    err2.NumeroID = -1
                                    err2.estado = -1
                                    err2.estadoDescripcion = "1. Recibido, -1 OrdenCompra no puede ser nulo"
                                    err.Transacciones = New List(Of Transaccion)
                                    err.Transacciones.Add(err2)
                                    Return err
                                Else
                                    parametro(0) = e.OrdenCompra
                                    parametro(1) = e.Producto
                                    dtHPO = datos.ConsultarHPOdt(parametro, 0)
                                    If dtHPO IsNot Nothing Then
                                        .OrdenCompra = e.OrdenCompra
                                    Else
                                        err.Batch = -1
                                        err.TipoRespuesta = 1
                                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                        err.estadoGeneral = -1
                                        err.estadoDescripcionGeneral = "1. Recibido, -1 No existe OrdenCompra / Producto"
                                        err2.NumeroID = -1
                                        err2.estado = -1
                                        err2.estadoDescripcion = "1. Recibido, -1 No existe OrdenCompra / Producto"
                                        err.Transacciones = New List(Of Transaccion)
                                        err.Transacciones.Add(err2)
                                        Return err
                                    End If
                                End If
                                'ALMACEN / UBICACION
                                If e.Ubicacion <> "" Then
                                    parametro(0) = e.Almacen
                                    parametro(1) = e.Ubicacion
                                    dtILM = datos.ConsultarILMdt(parametro, 0)
                                    If dtILM IsNot Nothing Then
                                        .Ubicacion = e.Ubicacion
                                    Else
                                        err.Batch = -1
                                        err.TipoRespuesta = 1
                                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                        err.estadoGeneral = -1
                                        err.estadoDescripcionGeneral = "1. Recibido, -1 No existe el Almacen con Ubicacion especificada"
                                        err2.NumeroID = -1
                                        err2.estado = -1
                                        err2.estadoDescripcion = "1. Recibido, -1 No existe el Almacen con Ubicacion especificada"
                                        err.Transacciones = New List(Of Transaccion)
                                        err.Transacciones.Add(err2)
                                        Return err
                                    End If
                                Else
                                    .Ubicacion = " "
                                    'err.Batch = -1
                                    'err.TipoRespuesta = 1
                                    'err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    'err.estadoGeneral = -1
                                    'err.estadoDescripcionGeneral = "1. Recibido, -1 Ubicacion no puede ser nulo"
                                    'err2.NumeroID = -1
                                    'err2.estado = -1
                                    'err2.estadoDescripcion = "1. Recibido, -1 Ubicacion no puede ser nulo"
                                    'err.Transacciones = New List(Of Transaccion)
                                    'err.Transacciones.Add(err2)
                                    'Return err
                                End If
                            Case "E2"
                                'ORDEN DE COMPRA
                                If e.OrdenCompra = "" Then
                                    err.Batch = -1
                                    err.TipoRespuesta = 1
                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    err.estadoGeneral = -1
                                    err.estadoDescripcionGeneral = "1. Recibido, -1 OrdenCompra no puede ser nulo"
                                    err2.NumeroID = -1
                                    err2.estado = -1
                                    err2.estadoDescripcion = "1. Recibido, -1 OrdenCompra no puede ser nulo"
                                    err.Transacciones = New List(Of Transaccion)
                                    err.Transacciones.Add(err2)
                                    Return err
                                Else
                                    parametro(0) = e.OrdenCompra
                                    parametro(1) = e.Producto
                                    dtHPO = datos.ConsultarHPOdt(parametro, 0)
                                    If dtHPO IsNot Nothing Then
                                        .OrdenCompra = e.OrdenCompra
                                    Else
                                        err.Batch = -1
                                        err.TipoRespuesta = 1
                                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                        err.estadoGeneral = -1
                                        err.estadoDescripcionGeneral = "1. Recibido, -1 No existe OrdenCompra"
                                        err2.NumeroID = -1
                                        err2.estado = -1
                                        err2.estadoDescripcion = "1. Recibido, -1 No existe OrdenCompra"
                                        err.Transacciones = New List(Of Transaccion)
                                        err.Transacciones.Add(err2)
                                        Return err
                                    End If
                                End If
                                'ALMACEN / UBICACION
                                If e.Ubicacion <> "" Then
                                    parametro(0) = e.Almacen
                                    parametro(1) = e.Ubicacion
                                    dtILM = datos.ConsultarILMdt(parametro, 0)
                                    If dtILM IsNot Nothing Then
                                        .Ubicacion = e.Ubicacion
                                    Else
                                        err.Batch = -1
                                        err.TipoRespuesta = 1
                                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                        err.estadoGeneral = -1
                                        err.estadoDescripcionGeneral = "1. Recibido, -1 No existe el Almacen con Ubicacion especificada"
                                        err2.NumeroID = -1
                                        err2.estado = -1
                                        err2.estadoDescripcion = "1. Recibido, -1 No existe el Almacen con Ubicacion especificada"
                                        err.Transacciones = New List(Of Transaccion)
                                        err.Transacciones.Add(err2)
                                        Return err
                                    End If
                                Else
                                    .Ubicacion = " "
                                    'err.Batch = -1
                                    'err.TipoRespuesta = 1
                                    'err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    'err.estadoGeneral = -1
                                    'err.estadoDescripcionGeneral = "1. Recibido, -1 Ubicacion no puede ser nulo"
                                    'err2.NumeroID = -1
                                    'err2.estado = -1
                                    'err2.estadoDescripcion = "1. Recibido, -1 Ubicacion no puede ser nulo"
                                    'err.Transacciones = New List(Of Transaccion)
                                    'err.Transacciones.Add(err2)
                                    'Return err
                                End If
                                'FACTURA
                                If e.NumeroFactura <> "" Then
                                    'If IsNumeric(e.NumeroFactura) Then
                                    'If CDbl(e.NumeroFactura) > 0 Then
                                    .NumeroFactura = e.NumeroFactura
                                    'Else
                                    '    err.Batch = -1
                                    '    err.TipoRespuesta = 1
                                    '    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    '    err.estadoGeneral = -1
                                    '    err.estadoDescripcionGeneral = "1. Recibido, -1 NumeroFactura solo acepta valores numericos mayores a cero"
                                    '    err2.NumeroID = -1
                                    '    err2.estado = -1
                                    '    err2.estadoDescripcion = "1. Recibido, -1 NumeroFactura solo acepta valores numericos mayores a cero"
                                    '    err.Transacciones = New List(Of Transaccion)
                                    '    err.Transacciones.Add(err2)
                                    '    Return err
                                    'End If
                                    'Else
                                    '    err.Batch = -1
                                    '    err.TipoRespuesta = 1
                                    '    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    '    err.estadoGeneral = -1
                                    '    err.estadoDescripcionGeneral = "1. Recibido, -1 NumeroFactura solo acepta valores numericos mayores a cero"
                                    '    err2.NumeroID = -1
                                    '    err2.estado = -1
                                    '    err2.estadoDescripcion = "1. Recibido, -1 NumeroFactura solo acepta valores numericos mayores a cero"
                                    '    err.Transacciones = New List(Of Transaccion)
                                    '    err.Transacciones.Add(err2)
                                    '    Return err
                                    'End If
                                Else
                                    err.Batch = -1
                                    err.TipoRespuesta = 1
                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    err.estadoGeneral = -1
                                    err.estadoDescripcionGeneral = "1. Recibido, -1 NumeroFactura no puede ser nulo"
                                    err2.NumeroID = -1
                                    err2.estado = -1
                                    err2.estadoDescripcion = "1. Recibido, -1 NumeroFactura no puede ser nulo"
                                    err.Transacciones = New List(Of Transaccion)
                                    err.Transacciones.Add(err2)
                                    Return err
                                End If
                            Case "E3"
                                'ORDEN FABRICACION
                                If e.OrdenFabricacion <> "" Then
                                    parametro(0) = e.OrdenFabricacion
                                    dtFSO = datos.ConsultarFSOdt(parametro, 0)
                                    If dtFSO IsNot Nothing Then
                                        .OrdenFabricacion = e.OrdenFabricacion
                                    Else
                                        err.Batch = -1
                                        err.TipoRespuesta = 1
                                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                        err.estadoGeneral = -1
                                        err.estadoDescripcionGeneral = "1. Recibido, -1 No existe OrdenFabricacion"
                                        err2.NumeroID = -1
                                        err2.estado = -1
                                        err2.estadoDescripcion = "1. Recibido, -1 No existe OrdenFabricacion"
                                        err.Transacciones = New List(Of Transaccion)
                                        err.Transacciones.Add(err2)
                                        Return err
                                    End If
                                Else
                                    err.Batch = -1
                                    err.TipoRespuesta = 1
                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    err.estadoGeneral = -1
                                    err.estadoDescripcionGeneral = "1. Recibido, -1 OrdenFabricacion no puede ser nulo"
                                    err2.NumeroID = -1
                                    err2.estado = -1
                                    err2.estadoDescripcion = "1. Recibido, -1 OrdenFabricacion no puede ser nulo"
                                    err.Transacciones = New List(Of Transaccion)
                                    err.Transacciones.Add(err2)
                                    Return err
                                End If
                                'parametro(0) = e.Producto
                                'dtIIM = datos.ConsultarIIMdt(parametro, 1)
                                'If dtIIM IsNot Nothing Then
                                '    If dtIIM.Rows(0).Item(0) IsNot DBNull.Value Then
                                '        If dtIIM.Rows(0).Item(0) = 2 Then
                                '            'LOTE / PRODUCTO
                                '            If e.Lote <> "" Then
                                '                parametro(0) = e.Lote
                                '                parametro(1) = e.Producto
                                '                dtILN = datos.ConsultarILNdt(parametro, 0)
                                '                If dtILN IsNot Nothing Then
                                '                    .Lote = e.Lote
                                '                Else
                                '                    err.Batch = -1
                                '                    err.TipoRespuesta = 1
                                '                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                '                    err.estadoGeneral = -1
                                '                    err.estadoDescripcionGeneral = "1. Recibido, -1 No existe Lote/Producto"
                                '                    err2.NumeroID = -1
                                '                    err2.estado = -1
                                '                    err2.estadoDescripcion = "1. Recibido, -1 No existe Lote/Producto"
                                '                    err.Transacciones = New List(Of Transaccion)
                                '                    err.Transacciones.Add(err2)
                                '                    Return err
                                '                End If
                                '            Else
                                '                err.Batch = -1
                                '                err.TipoRespuesta = 1
                                '                err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                '                err.estadoGeneral = -1
                                '                err.estadoDescripcionGeneral = "1. Recibido, -1 Lote no puede ser nulo"
                                '                err2.NumeroID = -1
                                '                err2.estado = -1
                                '                err2.estadoDescripcion = "1. Recibido, -1 Lote no puede ser nulo"
                                '                err.Transacciones = New List(Of Transaccion)
                                '                err.Transacciones.Add(err2)
                                '                Return err
                                '            End If
                                '        Else
                                '            .Lote = e.Lote
                                '        End If
                                '    Else
                                '        err.Batch = -1
                                '        err.TipoRespuesta = 1
                                '        err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                '        err.estadoGeneral = -1
                                '        err.estadoDescripcionGeneral = "1. Recibido, -1 Falta definir Lot Control"
                                '        err2.NumeroID = -1
                                '        err2.estado = -1
                                '        err2.estadoDescripcion = "1. Recibido, -1 Falta definir Lot Control"
                                '        err.Transacciones = New List(Of Transaccion)
                                '        err.Transacciones.Add(err2)
                                '        Return err
                                '    End If
                                'End If
                                'LOTE
                                If e.Lote = "" Then
                                    err.Batch = -1
                                    err.TipoRespuesta = 1
                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    err.estadoGeneral = -1
                                    err.estadoDescripcionGeneral = "1. Recibido, -1 Lote no puede ser nulo"
                                    err2.NumeroID = -1
                                    err2.estado = -1
                                    err2.estadoDescripcion = "1. Recibido, -1 Lote no puede ser nulo"
                                    err.Transacciones = New List(Of Transaccion)
                                    err.Transacciones.Add(err2)
                                    Return err
                                End If
                            Case "E4"
                                'parametro(0) = e.Producto
                                'dtIIM = datos.ConsultarIIMdt(parametro, 1)
                                'If dtIIM IsNot Nothing Then
                                '    If dtIIM.Rows(0).Item(0) IsNot DBNull.Value Then
                                '        If dtIIM.Rows(0).Item(0) = 2 Then
                                '            'LOTE / PRODUCTO
                                '            If e.Lote <> "" Then
                                '                parametro(0) = e.Lote
                                '                parametro(1) = e.Producto
                                '                dtILN = datos.ConsultarILNdt(parametro, 0)
                                '                If dtILN IsNot Nothing Then
                                '                    .Lote = e.Lote
                                '                Else
                                '                    err.Batch = -1
                                '                    err.TipoRespuesta = 1
                                '                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                '                    err.estadoGeneral = -1
                                '                    err.estadoDescripcionGeneral = "1. Recibido, -1 No existe Lote/Producto"
                                '                    err2.NumeroID = -1
                                '                    err2.estado = -1
                                '                    err2.estadoDescripcion = "1. Recibido, -1 No existe Lote/Producto"
                                '                    err.Transacciones = New List(Of Transaccion)
                                '                    err.Transacciones.Add(err2)
                                '                    Return err
                                '                End If
                                '            Else
                                '                err.Batch = -1
                                '                err.TipoRespuesta = 1
                                '                err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                '                err.estadoGeneral = -1
                                '                err.estadoDescripcionGeneral = "1. Recibido, -1 Lote no puede ser nulo"
                                '                err2.NumeroID = -1
                                '                err2.estado = -1
                                '                err2.estadoDescripcion = "1. Recibido, -1 Lote no puede ser nulo"
                                '                err.Transacciones = New List(Of Transaccion)
                                '                err.Transacciones.Add(err2)
                                '                Return err
                                '            End If
                                '        Else
                                '            .Lote = e.Lote
                                '        End If
                                '    Else
                                '        err.Batch = -1
                                '        err.TipoRespuesta = 1
                                '        err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                '        err.estadoGeneral = -1
                                '        err.estadoDescripcionGeneral = "1. Recibido, -1 Falta definir Lot Control"
                                '        err2.NumeroID = -1
                                '        err2.estado = -1
                                '        err2.estadoDescripcion = "1. Recibido, -1 Falta definir Lot Control"
                                '        err.Transacciones = New List(Of Transaccion)
                                '        err.Transacciones.Add(err2)
                                '        Return err
                                '    End If
                                'End If
                                'LOTE
                                If e.Lote = "" Then
                                    err.Batch = -1
                                    err.TipoRespuesta = 1
                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    err.estadoGeneral = -1
                                    err.estadoDescripcionGeneral = "1. Recibido, -1 Lote no puede ser nulo"
                                    err2.NumeroID = -1
                                    err2.estado = -1
                                    err2.estadoDescripcion = "1. Recibido, -1 Lote no puede ser nulo"
                                    err.Transacciones = New List(Of Transaccion)
                                    err.Transacciones.Add(err2)
                                    Return err
                                End If
                            Case "E5"
                                'REFERENCIA
                                If e.Referencia <> "" Then
                                    If IsNumeric(e.Referencia) Then
                                        If CDbl(e.Referencia) > 0 Then
                                            .Referencia = e.Referencia
                                        Else
                                            err.Batch = -1
                                            err.TipoRespuesta = 1
                                            err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                            err.estadoGeneral = -1
                                            err.estadoDescripcionGeneral = "1. Recibido, -1 Referencia solo acepta valores numericos mayores a cero"
                                            err2.NumeroID = -1
                                            err2.estado = -1
                                            err2.estadoDescripcion = "1. Recibido, -1 Referencia solo acepta valores numericos mayores a cero"
                                            err.Transacciones = New List(Of Transaccion)
                                            err.Transacciones.Add(err2)
                                            Return err
                                        End If
                                    Else
                                        err.Batch = -1
                                        err.TipoRespuesta = 1
                                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                        err.estadoGeneral = -1
                                        err.estadoDescripcionGeneral = "1. Recibido, -1 Referencia solo acepta valores numericos mayores a cero"
                                        err2.NumeroID = -1
                                        err2.estado = -1
                                        err2.estadoDescripcion = "1. Recibido, -1 Referencia solo acepta valores numericos mayores a cero"
                                        err.Transacciones = New List(Of Transaccion)
                                        err.Transacciones.Add(err2)
                                        Return err
                                    End If
                                Else
                                    err.Batch = -1
                                    err.TipoRespuesta = 1
                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    err.estadoGeneral = -1
                                    err.estadoDescripcionGeneral = "1. Recibido, -1 Referencia no puede ser nulo"
                                    err2.NumeroID = -1
                                    err2.estado = -1
                                    err2.estadoDescripcion = "1. Recibido, -1 Referencia no puede ser nulo"
                                    err.Transacciones = New List(Of Transaccion)
                                    err.Transacciones.Add(err2)
                                    Return err
                                End If
                            Case "E6"
                                'REFERENCIA
                                If e.Referencia <> "" Then
                                    If IsNumeric(e.Referencia) Then
                                        If CDbl(e.Referencia) > 0 Then
                                            .Referencia = e.Referencia
                                        Else
                                            err.Batch = -1
                                            err.TipoRespuesta = 1
                                            err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                            err.estadoGeneral = -1
                                            err.estadoDescripcionGeneral = "1. Recibido, -1 Referencia solo acepta valores numericos mayores a cero"
                                            err2.NumeroID = -1
                                            err2.estado = -1
                                            err2.estadoDescripcion = "1. Recibido, -1 Referencia solo acepta valores numericos mayores a cero"
                                            err.Transacciones = New List(Of Transaccion)
                                            err.Transacciones.Add(err2)
                                            Return err
                                        End If
                                    Else
                                        err.Batch = -1
                                        err.TipoRespuesta = 1
                                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                        err.estadoGeneral = -1
                                        err.estadoDescripcionGeneral = "1. Recibido, -1 Referencia solo acepta valores numericos mayores a cero"
                                        err2.NumeroID = -1
                                        err2.estado = -1
                                        err2.estadoDescripcion = "1. Recibido, -1 Referencia solo acepta valores numericos mayores a cero"
                                        err.Transacciones = New List(Of Transaccion)
                                        err.Transacciones.Add(err2)
                                        Return err
                                    End If
                                Else
                                    err.Batch = -1
                                    err.TipoRespuesta = 1
                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    err.estadoGeneral = -1
                                    err.estadoDescripcionGeneral = "1. Recibido, -1 Referencia no puede ser nulo"
                                    err2.NumeroID = -1
                                    err2.estado = -1
                                    err2.estadoDescripcion = "1. Recibido, -1 Referencia no puede ser nulo"
                                    err.Transacciones = New List(Of Transaccion)
                                    err.Transacciones.Add(err2)
                                    Return err
                                End If
                                'ALMACEN / UBICACION
                                If e.Ubicacion <> "" Then
                                    parametro(0) = e.Almacen
                                    parametro(1) = e.Ubicacion
                                    dtILM = datos.ConsultarILMdt(parametro, 0)
                                    If dtILM IsNot Nothing Then
                                        .Ubicacion = e.Ubicacion
                                    Else
                                        err.Batch = -1
                                        err.TipoRespuesta = 1
                                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                        err.estadoGeneral = -1
                                        err.estadoDescripcionGeneral = "1. Recibido, -1 No existe el Almacen con Ubicacion especificada"
                                        err2.NumeroID = -1
                                        err2.estado = -1
                                        err2.estadoDescripcion = "1. Recibido, -1 No existe el Almacen con Ubicacion especificada"
                                        err.Transacciones = New List(Of Transaccion)
                                        err.Transacciones.Add(err2)
                                        Return err
                                    End If
                                Else
                                    err.Batch = -1
                                    err.TipoRespuesta = 1
                                    err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                    err.estadoGeneral = -1
                                    err.estadoDescripcionGeneral = "1. Recibido, -1 Ubicacion no puede ser nulo"
                                    err2.NumeroID = -1
                                    err2.estado = -1
                                    err2.estadoDescripcion = "1. Recibido, -1 Ubicacion no puede ser nulo"
                                    err.Transacciones = New List(Of Transaccion)
                                    err.Transacciones.Add(err2)
                                    Return err
                                End If
                        End Select
                    End With
                    itH.Detalle.Add(itL)
                Next
            Catch ex2 As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\InfoError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine(ex2.Message & vbCrLf & ex2.ToString)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                err.Batch = -1
                err.TipoRespuesta = 1
                err.TipoRespuestaDescripcion = "1. Validacion de datos"
                err.estadoGeneral = -1
                err.estadoDescripcionGeneral = "1. Recibido, -1 Posible error en estructura"
                err2.NumeroID = -1
                err2.estado = -1
                err2.estadoDescripcion = "1. Recibido, -1 Posible error en estructura"
                err.Transacciones = New List(Of Transaccion)
                err.Transacciones.Add(err2)
                datos.CerrarConexion()
                Return err
            End Try
            result &= "[" & itH.Usuario & "]"
            Dim cons
            Dim batch
            batch = datos.CalcConsec("CIMNUM")
            err.Transacciones = New List(Of Transaccion)
            For Each e In itH.Detalle
                err2 = New Transaccion
                With e

                    Dim tabla As New BFCIM
                    If e.TipoTransaccion = "UC" Then
                        parametro(0) = e.OrdenCompra
                        parametro(1) = e.Producto
                        dtHPO = datos.ConsultarHPOdt(parametro, 1)
                        If dtHPO IsNot Nothing Then
                            UltimaLinea = dtHPO.Rows(0).Item("MAXLINE")
                        End If

                        Dim CantidadLinea As Double
                        Dim ValorUnitario As Double = e.Valor / e.Cantidad
                        parametro(0) = e.OrdenCompra
                        parametro(1) = e.Producto
                        dtHPO = datos.ConsultarHPOdt(parametro, 2)
                        If dtHPO IsNot Nothing Then
                            For Each row As DataRow In dtHPO.Rows
                                cons = datos.CalcConsec("CIMNUM")
                                If row("PLINE") = UltimaLinea Then
                                    CantidadLinea = e.Cantidad
                                    e.Cantidad = 0
                                ElseIf row("PQREC") + e.Cantidad > row("PQORD") Then
                                    CantidadLinea = row("PQORD") - row("PQREC")
                                    e.Cantidad -= CantidadLinea
                                Else
                                    CantidadLinea = e.Cantidad
                                    e.Cantidad = 0
                                End If
                                If CantidadLinea <> 0 Then
                                    With tabla
                                        .IFUENTE = "TRANSAC. INV."
                                        .IBATCH = batch
                                        .INID = "IN"
                                        .INTRAN = e.TipoTransaccion
                                        .INCTNR = 0
                                        .INMFGR = 0
                                        .INDATE = Replace(e.Fecha, "-", "")
                                        .INTRN = Right(cons, 5)
                                        .INMLOT = "DE" & cons
                                        .INFACTU = IIf(e.NumeroFactura = "", "NULL", e.NumeroFactura)
                                        If e.OrdenFabricacion <> "" Then
                                            .INREF = e.OrdenFabricacion
                                        ElseIf e.OrdenCompra <> "" Then
                                            .INREF = e.OrdenCompra
                                        ElseIf e.Referencia <> "" Then
                                            .INREF = e.Referencia
                                        Else
                                            .INREF = 0
                                        End If
                                        .INPROD = e.Producto
                                        .INLOT = IIf(e.Lote = "", " ", e.Lote)
                                        .INLOC = e.Ubicacion
                                        .INQTY = CantidadLinea
                                        .INCBY = itH.Usuario
                                        .INLDT = Format(Now(), "yyyyMMdd")
                                        .INLTM = Format(Now(), "hhmmss")
                                        .INCDT = Year(Now).ToString & Month(Now).ToString("00") & Day(Now).ToString("00")
                                        .INCTM = Now.Hour.ToString("00") & Now.Minute.ToString("00") & Now.Second.ToString("00")
                                        .INUMERO = e.NumeroID
                                        .INCOST = CantidadLinea * ValorUnitario
                                        .INWHSE = e.Almacen
                                    End With
                                    continuar = datos.GuardarBFCIMRow(tabla, 0)
                                    If continuar = False Then
                                        err.Batch = -1
                                        err.TipoRespuesta = 1
                                        err.TipoRespuestaDescripcion = "1. Validacion de datos"
                                        err.estadoGeneral = -1
                                        err.estadoDescripcionGeneral = "1. Recibido, -1 Error " & datos.mensaje
                                        err2.NumeroID = -1
                                        err2.estado = -1
                                        err2.estadoDescripcion = "1. Recibido, -1 Posible error en estructura"
                                        err.Transacciones = New List(Of Transaccion)
                                        err.Transacciones.Add(err2)
                                        datos.CerrarConexion()
                                        Return err
                                    End If
                                End If
                            Next
                        End If
                    Else
                        cons = datos.CalcConsec("CIMNUM")
                        With tabla
                            .IFUENTE = "TRANSAC. INV."
                            .IBATCH = batch
                            .INID = "IN"
                            .INTRAN = e.TipoTransaccion
                            .INCTNR = 0
                            .INMFGR = 0
                            .INDATE = Replace(e.Fecha, "-", "")
                            .INTRN = Right(cons, 5)
                            .INMLOT = "DE" & cons
                            .INFACTU = IIf(e.NumeroFactura = "", "NULL", e.NumeroFactura)
                            If e.OrdenFabricacion <> "" Then
                                .INREF = e.OrdenFabricacion
                            ElseIf e.OrdenCompra <> "" Then
                                .INREF = e.OrdenCompra
                            ElseIf e.Referencia <> "" Then
                                .INREF = e.Referencia
                            Else
                                .INREF = 0
                            End If
                            .INPROD = e.Producto
                            .INLOT = IIf(e.Lote = "", " ", e.Lote)
                            .INLOC = e.Ubicacion
                            .INQTY = e.Cantidad
                            .INCBY = itH.Usuario
                            .INLDT = Format(Now(), "yyyyMMdd")
                            .INLTM = Format(Now(), "hhmmss")
                            .INCDT = Year(Now).ToString & Month(Now).ToString("00") & Day(Now).ToString("00")
                            .INCTM = Now.Hour.ToString("00") & Now.Minute.ToString("00") & Now.Second.ToString("00")
                            .INUMERO = e.NumeroID
                            .INCOST = e.Valor
                            .INWHSE = e.Almacen
                        End With
                        continuar = datos.GuardarBFCIMRow(tabla, 0)
                        If continuar = False Then
                            err.Batch = -1
                            err.TipoRespuesta = 1
                            err.TipoRespuestaDescripcion = "1. Validacion de datos"
                            err.estadoGeneral = -1
                            err.estadoDescripcionGeneral = "1. Recibido, -1 Error " & datos.mensaje
                            err2.NumeroID = -1
                            err2.estado = -1
                            err2.estadoDescripcion = "1. Recibido, -1 Posible error en estructura"
                            err.Transacciones = New List(Of Transaccion)
                            err.Transacciones.Add(err2)
                            datos.CerrarConexion()
                            Return err
                        End If
                    End If
                End With
                err2.NumeroID = e.NumeroID
                err2.estado = 1
                err2.estadoDescripcion = "1. Recibido, 1 Sin Errores"
                err.Transacciones.Add(err2)
            Next
            err.Batch = batch
            err.TipoRespuesta = 1
            err.TipoRespuestaDescripcion = "1. Validacion de datos"
            err.estadoGeneral = 1
            err.estadoDescripcionGeneral = "1. Recibido, 1 Sin Errores"
            datos.CerrarConexion()
            Return err
        Catch ex As Exception
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\InfoError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine(ex.Message & vbCrLf & ex.ToString)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            err.Batch = -1
            err.TipoRespuesta = 1
            err.TipoRespuestaDescripcion = "1. Validacion de datos"
            err.estadoGeneral = -1
            err.estadoDescripcionGeneral = "1. Recibido, -1 Con Errores <A>" & docEntrada.ToString & "</A><B>" & ex.ToString & "</B>"
            err2.NumeroID = -1
            err2.estado = 1
            err2.estadoDescripcion = "1. Recibido, -1 Con Errores <A>" & docEntrada.ToString & "</A><B>" & ex.ToString & "</B>"
            err.Transacciones = New List(Of Transaccion)
            err.Transacciones.Add(err2)
            datos.CerrarConexion()
            Return err
        End Try
    End Function

End Class
