﻿Imports IBM.Data.DB2.iSeries
Imports System.Text
Imports System.IO
Imports System.Data.OleDb

Public Class ClsDatosAS400
    Public ReadOnly conexionIBM As New iDB2Connection()
    Public ReadOnly conexionoledb As New OleDbConnection()
    Private adapterIBM As iDB2DataAdapter = New iDB2DataAdapter()
    Private builderIBM As iDB2CommandBuilder
    Private adapterOLEDB As OleDbDataAdapter = New OleDbDataAdapter()
    Private builderOLEDB As OleDbCommandBuilder
    Private commandIBM As iDB2Command
    Private numFilas As Integer
    Private dt As DataTable
    Private WithEvents ds As DataSet
    Private appPath As String = Path.GetFullPath(My.Application.Info.DirectoryPath & "\Log\")
    Public mensaje As String
    Private data
    Public fSql As String
    Public Const DB2_DATEDEC = " ( YEAR( NOW() ) * 10000 + MONTH( NOW()  ) * 100 + DAY( NOW() ) )"
    Public Const DB2_TIMEDEC0 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    Public Const DB2_TIMEDEC1 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 ) "

#Region "CONSULTAR"

    ''' <summary>
    ''' Consulta la tabla SYSIBM.SYSDUMMY1 con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 02/05/2016</remarks>
    Public Function ConsultarSYSIBMSYSDUMMY1dt() As DataTable
        dt = Nothing

        fSql = "SELECT NOW(), CURRENT_TIMEZONE  FROM SYSIBM.SYSDUMMY1"

        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)
        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla SYSIBM.SYSDUMMY1 con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 02/05/2016</remarks>
    Public Function ConsultarSYSIBMSYSDUMMY1dtoledb() As DataTable
        dt = Nothing

        fSql = "SELECT NOW(), CURRENT_TIMEZONE  FROM SYSIBM.SYSDUMMY1"

        adapterOLEDB = New OleDbDataAdapter(fSql, conexionoledb)
        adapterOLEDB.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderOLEDB = New OleDbCommandBuilder(adapterOLEDB)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterOLEDB.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterOLEDB = Nothing
        builderOLEDB = Nothing
        Return dt
    End Function

#End Region

#Region "FUNCIONES"

    Public Function AbrirConexion(ByVal cadena As String) As Boolean
        Dim abierta As Boolean = False
        Try
            If conexionoledb.State = ConnectionState.Open Then
                abierta = True
            Else
                conexionoledb.ConnectionString = cadena
                conexionoledb.Open()
                abierta = True
            End If
            'If conexionIBM.State = ConnectionState.Open Then
            '    abierta = True
            'Else
            '    conexionIBM.ConnectionString = cadena
            '    conexionIBM.Open()
            '    abierta = True
            'End If
        Catch ex5 As OleDbException
            abierta = False
            mensaje = ex5.Message
        Catch ex3 As iDB2DCFunctionErrorException
            abierta = False
            mensaje = ex3.InnerException.ToString & vbCrLf & ex3.Message
        Catch ex4 As AccessViolationException
            abierta = False
            mensaje = ex4.InnerException.ToString & vbCrLf & ex4.Message
        Catch ex2 As iDB2InvalidConnectionStringException
            abierta = False
            mensaje = ex2.InnerException.ToString & vbCrLf & ex2.Message
        Catch ex1 As iDB2Exception
            abierta = False
            mensaje = ex1.MessageDetails & vbCrLf & ex1.Message
        Catch ex As Exception
            abierta = False
            mensaje = ex.Message
        Finally
            If abierta = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionAS400Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo realizar la conexión AS400.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & cadena)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End If
        End Try
        Return abierta
    End Function

    Public Function CerrarConexion() As Boolean
        Dim cerrada As Boolean = False
        Try
            conexionIBM.Close()
            cerrada = True
        Catch ex1 As iDB2Exception
            cerrada = False
            mensaje = ex1.ToString
        Catch ex As Exception
            cerrada = False
            mensaje = ex.ToString
        End Try
        Return cerrada
    End Function

    ''' <summary>
    ''' Split de cadena (;)
    ''' </summary>
    ''' <returns>Vector con Cadena</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 02/05/2016</remarks>
    Public Function DetalleCadena() As String()
        Dim detalle() As String
        'detalle = Split(conexionIBM.ConnectionString, ";")
        detalle = Split(conexionoledb.ConnectionString, ";")
        Return detalle
    End Function

#End Region

End Class
