﻿Imports System.IO
Imports MySql.Data.MySqlClient
Imports System.Web
Imports System.Text

Public Module BssCommon
    Public conn As New ADODB.Connection
    Public local As String
    Public gsAS400 As String = "192.168.40.10"
    Public gsLib As String = ""
    Public sUser As String
    Public sUserApl As String
    Public sUserPass As String
    Public bBatchApp As Boolean = False
    'Public ubicWebConfig As String = My.Settings.PARAMETROS
    Public Sentencia As String
    Public bLog As Boolean = False
    Public iNumLog As Integer
    Public msgErr As String
    Public gsConnString As String
    Public rAfectados As Integer
    Public Descripcion_Error As String
    Public conexion As MySqlConnection
    'Public DB As New clsConexion
    'Public DBMySql As New clsConexion
    Public rs As New ADODB.Recordset
    Public fSql As String
    Public vSql As String
    Public lastSQL As String = ""
    Public bSalto
    Public esInsert
    Public esUpdate
    Public esFin
    Private datos As New ClsDatos
    Public Sesiones As Integer

    Function paramWebConfig(webConfig, param)
        Dim oXML, oNode, oChild, oAttr

        oXML = CreateObject("Microsoft.XMLDOM")
        oXML.Async = "false"
        oXML.Load(webConfig)
        oNode = oXML.GetElementsByTagName("appSettings").Item(0)
        oChild = oNode.GetElementsByTagName("add")
        For Each oAttr In oChild
            If UCase(oAttr.getAttribute("key")) = UCase(param) Then
                Return oAttr.getAttribute("value")
            End If
        Next
        Return ""

    End Function

    Function paramWebConfig400(param) As String
        Dim Valor As String
        Dim parametro(0) As String
        Dim dtBFPARAM As DataTable

        parametro(0) = param
        dtBFPARAM = datos.ConsultarBFPARAMdt(param, 0)
        If dtBFPARAM IsNot Nothing Then
            Valor = dtBFPARAM.Rows(0).Item("CCDESC")
            Return Valor
        Else
            Return ""
        End If
        'fSql = "SELECT CCDESC FROM BFPARAM WHERE CCTABL = 'LXLONG' AND CCCODE = '" & param & "' "
        'If DBMySql.OpenRS(rs, fSql) Then
        '    Valor = rs("CCDESC").Value
        '    rs.Close()
        '    Return Valor
        'Else
        '    rs.Close()
        '    Return ""
        'End If
    End Function

    Sub mkSql_h(sep)
        mkSql(sep, "", "", 0)
    End Sub

    Sub mkSql_f(sep)
        mkSql(sep, "", "", 0)
    End Sub

    Sub mkSql_d(sep, Campo, valor, Fin)
        mkSql(sep, Campo, valor, Fin)
    End Sub

    Sub mkSql(sep, Campo, valor, Fin)

        If Campo = "" Then
            If InStr(sep, "INSERT INTO") > 0 Then
                esInsert = True
                fSql = sep & IIf(bSalto, vbCrLf, "")
            ElseIf InStr(sep, "VALUES") Then
                vSql = sep & IIf(bSalto, vbCrLf, "")
            ElseIf InStr(sep, "UPDATE") Then
                fSql = sep & IIf(bSalto, vbCrLf, "")
                vSql = ""
                esUpdate = True
                esInsert = False
            ElseIf esFin Then
                vSql = vSql & " " & sep & IIf(bSalto, vbCrLf, "")
            ElseIf esInsert Then
                vSql = sep
            Else
                fSql = sep
            End If
        Else
            sep = Trim(sep)
            If esInsert Then
                fSql = fSql & Campo
                Select Case Fin
                    Case 0
                        fSql = fSql & " , "
                    Case 10, 11, -1, 1
                        fSql = fSql & " ) "
                End Select
                If valor = Nothing Then
                    valor = IIf(sep = "'", "", "0")
                End If
                vSql = vSql & sep & CStr(valor) & sep
                Select Case Fin
                    Case 10
                        vSql = vSql & "  "
                    Case 11, -1, 1
                        vSql = vSql & " )"
                    Case 0
                        vSql = vSql & " , "
                End Select
            Else
                If valor = Nothing Then
                    valor = IIf(sep = "", "", "0")
                End If
                fSql = fSql & Campo & " = " & sep & CStr(valor) & sep & IIf(Fin = 0, ", ", "")
            End If
            esFin = Fin <> 0
            If bSalto Then
                fSql = fSql & vbCrLf
                If vSql <> "" Then
                    vSql = vSql & vbCrLf
                End If
            End If
        End If

    End Sub

    Function v_fSql()
        v_fSql = fSql
    End Function

    Function v_Sql()
        v_Sql = vSql
    End Function

    Sub salto(v)
        bSalto = v
    End Sub

    Function DB2_DATEDEC()
        DB2_DATEDEC = " ( YEAR( NOW() ) * 10000 + MONTH( NOW()  ) * 100 + DAY( NOW() ) )"
    End Function

    Function DB2_TIMEDEC()
        DB2_TIMEDEC = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    End Function

    Function DB2_TIMEDEC0()
        DB2_TIMEDEC0 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    End Function

    Function DB2_TIMEDEC1()
        DB2_TIMEDEC1 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 ) "
    End Function

    Sub SetConexion()
        Dim parametro(0) As String
        Dim dtBFPARAM As DataTable
        Dim s As String = ""
        s = s & "Provider=IBMDA400.DataSource;Data Source={DATASOURCE};User ID={USERID};Password={PASSWORD};Persist Security Info=True;"
        s = s & "Convert Date Time To Char=FALSE;Default Collection={DEFAULT_COLLECTION};Force Translate=0;"
        parametro(0) = "AS400_SERVER"
        dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 1)
        s = s.Replace("{DATASOURCE}", dtBFPARAM.Rows(0).Item(0))
        parametro(0) = "AS400_USER"
        dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 1)
        s = s.Replace("{USERID}", dtBFPARAM.Rows(0).Item(0))
        parametro(0) = "AS400_PASS"
        dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 1)
        s = s.Replace("{PASSWORD}", dtBFPARAM.Rows(0).Item(0))
        parametro(0) = "AS400_LIB_F"
        dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 1)
        s = s.Replace("{DEFAULT_COLLECTION}", dtBFPARAM.Rows(0).Item(0))
        gsConnString = s
        
    End Sub

    Public Function Open() As Boolean
        Try
            'If (DB.State = 1) Then DB.Close()
            If (conn.State = 0) Then
                conn.Open(gsConnString)
                Sesiones = 1
            Else
                Sesiones += 1
            End If
            SetNumLog()
            crtLog(iNumLog.ToString, iNumLog.ToString)
            Return (conn.State = 1)
        Catch
            Descripcion_Error = Err.Description
            wrtLog(Err.Description, iNumLog)
            Return False
        End Try

    End Function

    Function SetNumLog() As Integer
        iNumLog = CInt(datos.CalcConsec("WMSLOG"))
        Return iNumLog
    End Function

    Public Function crtLog(ByVal txt As String, ByRef iNumLog As String) As String
        Dim fEntrada As String = ""
        Dim num As String = Right("000" & iNumLog, 3)

        'Dim appPath As String = System.Web.HttpContext.Current.Request.ApplicationPath
        Dim appPath As String = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath
        'Dim CarpetaWeb As String = HttpContext.Current.Request.MapPath(appPath)
        Dim CarpetaWeb As String = appPath

        fEntrada = Replace(CarpetaWeb & "\wmslog" & num & ".txt", "\\", "\")
        Try
            ' Create an instance of StreamReader to read from a file.
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(fEntrada)
            sb.AppendLine("LOG " & txt)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            Return "OK"
        Catch E As Exception
            Return E.Message
        End Try

    End Function

    Public Function wrtLog(ByVal txt As String, ByRef iNumLog As Integer) As String
        Dim fEntrada As String = ""
        Dim num As String = Right("000" & iNumLog, 3)

        Dim appPath As String = System.Web.HttpContext.Current.Request.ApplicationPath
        Dim CarpetaWeb As String = HttpContext.Current.Request.MapPath(appPath)

        fEntrada = Replace(CarpetaWeb & "\wmslog" & num & ".txt", "\\", "\")
        Try
            ' Create an instance of StreamReader to read from a file.
            Dim sr As StreamReader = New StreamReader(fEntrada)
            Dim sb As New StringBuilder()

            sb.Append(sr.ReadToEnd())

            sb.AppendLine("SistemaDeBodega (" & My.Application.Info.Version.Major & "." & _
                   My.Application.Info.Version.Minor & "." & _
                   My.Application.Info.Version.Revision & ")")
            '===>>>REVISAR LOG
            'sb.AppendLine("[" & System.Configuration.ConfigurationSettings.AppSettings("AS400_SERVER") & "][" & System.Configuration.ConfigurationSettings.AppSettings("AS400_USER") & "][" & System.Configuration.ConfigurationSettings.AppSettings("AS400_LIB_F") & "]")

            sb.AppendLine(txt)
            sr.Close()

            Dim sw As StreamWriter = New StreamWriter(fEntrada)
            sw.WriteLine(sb.ToString())
            sw.Close()

            Return "OK"
        Catch E As Exception
            Return E.Message
        End Try

    End Function

    Public Function Cursor(ByRef rs As ADODB.Recordset, ByVal sql As String) As Integer

        Dim Result As Integer
        Sentencia = sql
        Try
            rs.Open(sql, Conn)
        Catch
            Descripcion_Error = Sentencia & " " & Err.Description
            wrtLog(Err.Description, iNumLog)
            Return -99
        End Try
        Result = IIf(rs.EOF, 0, 1)
        Return Result
    End Function

    Public Sub Close()
        If Sesiones = 1 Then
            Sesiones = 0
            Conn.Close()
        Else
            Sesiones -= 1
        End If
    End Sub
End Module
