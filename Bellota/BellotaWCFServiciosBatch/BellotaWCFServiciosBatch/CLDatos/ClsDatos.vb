﻿Imports MySql.Data.MySqlClient
Imports System.Text
Imports System.IO

Public Class ClsDatos
    Private Cadena As String
    Private ReadOnly conexion As MySqlConnection
    Private adapter As MySqlDataAdapter = New MySqlDataAdapter()
    Private builder As MySqlCommandBuilder
    Private command As MySqlCommand
    Private WithEvents ds As DataSet
    Public fSql As String
    Private numFilas As Integer
    Private dt As DataTable
    Public appPath As String = Path.GetFullPath(My.Application.Info.DirectoryPath & "\Log\")
    Public gsQueDB As String
    Public gsDatasource As String
    Public gsLib As String
    Public gsConnstring As String
    Public gsUser As String
    Public gsAppName As String
    Public gsAppPath
    Public gsAppVersion As String
    Public gsKeyWords As String = ""
    Public ModuloActual As String = ""
    Public lastError As String = ""
    Public bDateTimeToChar As Boolean = False
    Dim regA As Double
    Public Const DB2_DATEDEC = " ( YEAR( NOW() ) * 10000 + MONTH( NOW()  ) * 100 + DAY( NOW() ) )"
    Public Const DB2_TIMEDEC0 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    Public Const DB2_TIMEDEC1 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 ) "
    Public mensaje As String
    Private data

    Sub New()
        If My.Settings.PRUEBAS = "SI" Then
            Cadena = My.Settings.BASEDATOSLOCAL
        Else
            Cadena = My.Settings.BASEDATOS
        End If
        Try
            conexion = New MySqlConnection(Cadena)
            ' Si el directorio no existe, crearlo
            If Not Directory.Exists(appPath) Then
                Directory.CreateDirectory(appPath)
            End If
        Catch ex As Exception
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\CONEXIONError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo Establecer conexion." & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try
    End Sub

#Region "ACTUALIZAR"

    ''' <summary>
    ''' Actualiza la tabla RFPARAM 
    ''' </summary>
    ''' <param name="datos">RFPARAM</param>
    ''' <param name="tipo">0- SET @campo = @valor FROM RFPARAM WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER(@codigo)"</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 15/04/2016</remarks>
    Public Function ActualizarRFPARAMRow(ByVal datos As RFPARAM, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean
        Select Case tipo
            Case 0
                If datos.campo = "" Then
                    datos.campo = "CCDESC"
                End If
                fSql = "UPDATE RFPARAM SET " & datos.campo & " = '" & datos.valor & "'"
                fSql &= " FROM RFPARAM "
                fSql &= " WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER('" & datos.codigo & "')"
        End Select
        command = New MySqlCommand()
        command.Connection = conexion
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As MySqlException
            continuarguardando = False
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\RFPARAMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla RFPARAM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()

                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .OPERACION = "Guardar RFPARAM"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla sequence_data 
    ''' </summary>
    ''' <param name="seq_name">seq_name</param>
    ''' <param name="tipo">0- SET sequence_cur_value = If((sequence_cur_value + sequence_increment) > sequence_max_value,If(sequence_cycle = True,sequence_min_value,NULL),sequence_cur_value +sequence_increment) WHERE sequence_name = @seq_name</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 15/04/2016</remarks>
    Public Function Actualizarsequence_dataRow(ByVal seq_name As String, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean
        Select Case tipo
            Case 0
                fSql = "UPDATE sequence_data SET sequence_cur_value = If((sequence_cur_value + sequence_increment) > sequence_max_value,If(sequence_cycle = True,sequence_min_value,NULL),sequence_cur_value +sequence_increment) WHERE sequence_name = '" & seq_name & "'"
        End Select

        command = New MySqlCommand()
        command.Connection = conexion
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As MySqlException
            continuarguardando = False
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\sequence_dataError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla sequence_data.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()

                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .OPERACION = "Guardar sequence_data"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

#End Region

#Region "CONSULTAR"

    ''' <summary>
    ''' Consulta la tabla BFCIM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por IBATCH, 1- Busca por IBATCH e IFLAG != 2 devuelve count distinct, 2- Busca por IBATCH(0), 3- Busca por IBATCH devuelve count, 4- Busca por IBATCH - IFLAG=0 devuelve count, 5- Busca por IBATCH - IFLAG=1 devuelve count, 6- Busca por IBATCH - IFLAG=2 devuelve count</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 29/04/2016</remarks>
    Public Function ConsultarBFCIMdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing
       
        Select Case tipoparametro
            Case 0
                fSql = "SELECT IFLAG, INUMERO, INERROR FROM BFCIM WHERE IBATCH =  " & parametroBusqueda(0)
            Case 1
                fSql = "SELECT COUNT(DISTINCT IFLAG) TRANSACCIONES FROM BFCIM WHERE IBATCH =  " & parametroBusqueda(0) & " AND IFLAG <> 2"
            Case 2
                fSql = "SELECT * FROM BFCIM WHERE IBATCH =  " & parametroBusqueda(0)
            Case 3
                fSql = "SELECT COUNT(DISTINCT INLINE) PEDIDOSS FROM BFCIM WHERE IBATCH =  " & parametroBusqueda(0)
            Case 4
                fSql = "SELECT COUNT(DISTINCT INLINE) PEDIDOSS FROM BFCIM WHERE IBATCH =  " & parametroBusqueda(0) & " AND IFLAG =  0 "
            Case 5
                fSql = "SELECT COUNT(DISTINCT INLINE) PEDIDOSS FROM BFCIM WHERE IBATCH =  " & parametroBusqueda(0) & " AND IFLAG =  1 "
            Case 6
                fSql = "SELECT COUNT(DISTINCT INLINE) PEDIDOSS FROM BFCIM WHERE IBATCH =  " & parametroBusqueda(0) & " AND IFLAG =  2 "
            Case 7
                fSql = "SELECT COUNT(DISTINCT IFLAG) TRANSACCIONES FROM BFCIM WHERE IBATCH =  " & parametroBusqueda(0) & " AND IFLAG = 7"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla BFCIMws con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por IBATCH devuelve todo, 1- Busca por IBATCH devuelve count, 2- Busca por IBATCH - IFLAG=0 devuelve count, 3- Busca por IBATCH - IFLAG=1 devuelve count, 4- Busca por IBATCH - IFLAG=2 devuelve count</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 15/04/2016</remarks>
    Public Function ConsultarBFCIMwsdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT * FROM BFCIMws WHERE IBATCH =  " & parametroBusqueda(0)
            Case 1
                fSql = "SELECT COUNT(DISTINCT INLINE) PEDIDOSS FROM BFCIMws WHERE IBATCH =  " & parametroBusqueda(0)
            Case 2
                fSql = "SELECT COUNT(DISTINCT INLINE) PEDIDOSS FROM BFCIMws WHERE IBATCH =  " & parametroBusqueda(0) & " AND IFLAG =  0 "
            Case 3
                fSql = "SELECT COUNT(DISTINCT INLINE) PEDIDOSS FROM BFCIMws WHERE IBATCH =  " & parametroBusqueda(0) & " AND IFLAG =  1 "
            Case 4
                fSql = "SELECT COUNT(DISTINCT INLINE) PEDIDOSS FROM BFCIMws WHERE IBATCH =  " & parametroBusqueda(0) & " AND IFLAG =  2 "
            Case 5
                fSql = "SELECT * FROM BFCIMws WHERE IBATCH =  " & parametroBusqueda(0) & " AND IFLAG2 <> 2"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla BFDESPAws con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por DBATCH devuelve todo, 1- Busca por DBATCH devuelve count, 2- Busca por DBATCH - DERROR!=0 devuelve count, 3- Busca por DBATCH - DERROR=0 devuelve count</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 15/04/2016</remarks>
    Public Function ConsultarBFDESPAwsdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT * FROM BFDESPAws WHERE DBATCH = " & parametroBusqueda(0)
            Case 1
                fSql = "SELECT COUNT(DISTINCT DPEDID) PEDIDOSS FROM BFDESPAws WHERE DBATCH = " & parametroBusqueda(0)
            Case 2
                fSql = "SELECT COUNT(DISTINCT DPEDID) PEDIDOSS FROM BFDESPAWS WHERE DBATCH = " & parametroBusqueda(0) & " AND DERROR <> 0"
            Case 3
                fSql = "SELECT COUNT(DISTINCT DPEDID) PEDIDOSS FROM BFDESPAWS WHERE DBATCH = " & parametroBusqueda(0) & " AND DERROR = 0"
            Case 4
                fSql = "SELECT COUNT(DISTINCT DPEDID) PEDIDOSS FROM BFDESPAWS WHERE DBATCH = " & parametroBusqueda(0) & " AND DFLAG2 = 0"
            Case 5
                fSql = "SELECT COUNT(DISTINCT DPEDID) PEDIDOSS FROM BFDESPAWS WHERE DBATCH = " & parametroBusqueda(0) & " AND DFLAG2 = 1"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla BFDESPA con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por DBATCH devuelve todo, 1- Busca por DBATCH devuelve count, 2- Busca por DBATCH - DERROR!=0 devuelve count, 3- Busca por DBATCH - DERROR=0 devuelve count</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 15/04/2016</remarks>
    Public Function ConsultarBFDESPAdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT COUNT(DISTINCT DPEDID) PEDIDOSS FROM BFDESPA WHERE DBATCH = " & parametroBusqueda(0) & " AND DFLAG = 2"
            Case 1
                fSql = "SELECT COUNT(DISTINCT DPEDID) PEDIDOSS FROM BFDESPA WHERE DBATCH = " & parametroBusqueda(0) & " AND DFLAG = 7"
            Case 2
                fSql = "SELECT COUNT(DISTINCT DPEDID) PEDIDOSS FROM BFDESPA WHERE DBATCH = " & parametroBusqueda(0) & " "
            Case 3
                fSql = "SELECT COUNT(DISTINCT DFLAG) PEDIDOSS FROM BFDESPA WHERE DBATCH =  " & parametroBusqueda(0) & " AND DFLAG = 7"
            Case 4
                fSql = "SELECT COUNT(DISTINCT DLINEA) PEDIDOSS FROM BFDESPA WHERE DBATCH =  " & parametroBusqueda(0) & " AND DFLAG =  0 "
            Case 5
                fSql = "SELECT COUNT(DISTINCT DLINEA) PEDIDOSS FROM BFDESPA WHERE DBATCH =  " & parametroBusqueda(0) & " AND DFLAG =  1 "
            Case 6
                fSql = "SELECT * FROM BFDESPA WHERE DBATCH =  " & parametroBusqueda(0)
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing

        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla BFPARAM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por CCTABL = 'LXLONG' y CCCODE(0), 
    ''' 1- Busca por CCCODE(0), 
    ''' 2- Busca por CCTABL(0) y CCCODE(1),
    ''' 3- Busca por CCTABL(0) y CCCODE(%1%)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 15/04/2016</remarks>
    Public Function ConsultarBFPARAMdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT CCDESC FROM BFPARAM WHERE CCTABL = 'LXLONG' AND CCCODE = '" & parametroBusqueda(0) & "' "
            Case 1
                fSql = "SELECT CCDESC FROM BFPARAM WHERE CCCODE = '" & parametroBusqueda(0) & "' "
            Case 2
                fSql = "SELECT CCCODE2 FROM BFPARAM WHERE CCTABL = '" & parametroBusqueda(0) & "' AND CCCODE = '" & parametroBusqueda(1) & "' "
            Case 3
                fSql = "SELECT * FROM BFPARAM WHERE CCTABL = '" & parametroBusqueda(0) & "' AND CCCODE LIKE '%" & parametroBusqueda(1) & "%'"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla FSO con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por Orden de fabricacion(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarFSOdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT SORD FROM FSO WHERE SORD = " & parametroBusqueda(0) & ""
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla HPH con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por Orden de compra(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarHPHdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT PHORD FROM HPH WHERE PHORD = '" & parametroBusqueda(0) & "'"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla HPO con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por Orden de compra(0) Y Producto(1)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarHPOdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT PORD, PPROD FROM HPO WHERE PORD = " & parametroBusqueda(0) & " AND PPROD = '" & parametroBusqueda(1) & "'"
            Case 1
                fSql = " SELECT  "
                fSql &= " IFNULL( SUM(PQORD), 0 ) AS PQORD, IFNULL( SUM(PQREC), 0 ) AS PQREC, IFNULL( MAX(PLINE), 0 ) AS MAXLINE "
                fSql &= " FROM HPO"
                fSql &= " WHERE PORD=" & parametroBusqueda(0)
                fSql &= " AND PPROD='" & parametroBusqueda(1) & "'"
            Case 2
                fSql = " SELECT  * "
                fSql &= " FROM HPO"
                fSql &= " WHERE PORD=" & parametroBusqueda(0)
                fSql &= " AND PPROD='" & parametroBusqueda(1) & "'"
                fSql &= " AND PQREC < PQORD"
                fSql &= " ORDER BY PLINE"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        ds.EnforceConstraints = False
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla IIM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por IPROD(0) devuelve IDESC, 1- Busca por IPROD(0) devuelve SAFLG</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 25/04/2016</remarks>
    Public Function ConsultarIIMdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT IDESC FROM IIM WHERE IPROD = '" & parametroBusqueda(0) & "' "
            Case 1
                fSql = "SELECT SAFLG FROM IIM WHERE IPROD = '" & parametroBusqueda(0) & "'"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla ILM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por Almacen(0) y Ubicacion(1)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarILMdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT WWHS, WLOC FROM ILM WHERE WWHS = '" & parametroBusqueda(0) & "' AND WLOC = '" & parametroBusqueda(1) & "'"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla ILN con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por Lote(0) y Producto(1)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarILNdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT LLOT, LPROD FROM ILN WHERE LLOT = '" & parametroBusqueda(0) & "' AND LPROD = '" & parametroBusqueda(1) & "'"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla IWM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por LWHS(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarIWMdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT LDESC FROM IWM WHERE LWHS = '" & parametroBusqueda(0) & "'"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla RCAU con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por UUSR(0) y UPASS(1)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 15/04/2016</remarks>
    Public Function ConsultarRCAUdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing

        fSql = "SELECT * FROM RCAU"
        Select Case tipoparametro
            Case 0
                fSql = "SELECT UUSR FROM RCAU WHERE UUSR='" & parametroBusqueda(0) & "' AND UPASS='" & parametroBusqueda(1) & "'"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing

        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla RFPARAM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca donde CCTABL = LXLONG - CCCODE(1) - devuelve (0), 1- Busca donde CCTABL = LXLONG - CCCODE(1) - CCCODE2(2) - devuelve (0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 15/04/2016</remarks>
    Public Function ConsultarRFPARAMdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing

        fSql = "SELECT * FROM RFPARAM"
        Select Case tipoparametro
            Case 0
                If parametroBusqueda(0) = "" Then
                    parametroBusqueda(0) = "CCDESC"
                End If
                fSql = "SELECT " & parametroBusqueda(0) & " AS DATO FROM RFPARAM WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER('" & parametroBusqueda(1) & "')"
            Case 1
                If parametroBusqueda(0) = "" Then
                    parametroBusqueda(0) = "CCDESC"
                End If
                fSql = "SELECT " & parametroBusqueda(0) & " AS DATO "
                fSql &= " FROM RFPARAM "
                fSql &= " WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER('" & parametroBusqueda(1) & "')"
                fSql &= " AND UPPER(CCCODE2) = UPPER('" & parametroBusqueda(2) & "')"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla sequence_data con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por sequence_name(0) devuelve sequence_cur_value, 1- Busca por sequence_name(0) devuelve sequence_max_value</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 15/04/2016</remarks>
    Public Function Consultarsequence_datadt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT sequence_cur_value FROM sequence_data WHERE sequence_name = '" & parametroBusqueda(0) & "'"
            Case 1
                fSql = "SELECT sequence_max_value FROM SEQUENCE_DATA WHERE sequence_name = '" & parametroBusqueda(0) & "'"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla ZCCL01 con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por CCCODE(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 15/04/2016</remarks>
    Public Function ConsultarZCCL01dt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing
        ds = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT CCSDSC, CCUDC1, CCNOT2, CCDESC  FROM ZCCL01 WHERE CCTABL='RFVBVER' AND UPPER(CCCODE) = '" & parametroBusqueda(0) & "'"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        
        Return dt
    End Function

#End Region

#Region "ELIMINAR"

    ''' <summary>
    ''' Elimina tabla enviada
    ''' </summary>
    ''' <param name="Table">Tabla a eliminar</param>
    ''' <returns>un booleano</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 15/04/2016</remarks>
    Public Function DropTable(Table As String) As Boolean
        Dim continuarguardando As Boolean
        Using sqlCommand As New MySqlCommand()
            With sqlCommand
                .CommandText = "DROP TABLE ?Tabla"
                .Connection = conexion
                .Parameters.Add("?Tabla", Table)
            End With
            Try
                sqlCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As MySqlException
                continuarguardando = False
                mensaje = ex.Message
            Catch ex As Exception
                continuarguardando = False
                mensaje = ex.Message
            Finally
                If continuarguardando = False Then
                    Dim sb As New StringBuilder()
                    Dim sw As StreamWriter = New StreamWriter(appPath & "\" & Table & "Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                    sb.AppendLine("No se pudo eliminar la tabla " & Table & ".  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje)
                    sb.AppendLine(Now().ToString)
                    sw.WriteLine(sb.ToString())
                    sw.Close()
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Elimina sequence_data
    ''' </summary>
    ''' <param name="sId">sId</param>
    ''' <returns>un booleano</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 15/04/2016</remarks>
    Public Function Eliminarsequence_data(ByVal sId As String) As Boolean
        Dim continuarguardando As Boolean
        Using sqlCommand As New MySqlCommand()
            With sqlCommand
                .CommandText = "DELETE FROM sequence_data WHERE sequence_name = ?sequence_name"
                .Connection = conexion
                .Parameters.Add("?sequence_name", sId)
            End With
            Try
                sqlCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As MySqlException
                continuarguardando = False
                mensaje = ex.Message
            Catch ex As Exception
                continuarguardando = False
                mensaje = ex.Message
            Finally
                If continuarguardando = False Then
                    Dim sb As New StringBuilder()
                    Dim sw As StreamWriter = New StreamWriter(appPath & "\sequence_dataError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                    sb.AppendLine("No se pudo eliminar fila(s) de la tabla sequence_data.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje)
                    sb.AppendLine(Now().ToString)
                    sw.WriteLine(sb.ToString())
                    sw.Close()
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

#End Region

#Region "FUNCIONES"

    ''' <summary>
    ''' Arma cadena de conexion de AS400
    ''' </summary>
    ''' <returns>String con cadena</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 29-04-2016</remarks>
    Public Function CadenaConexionAS400() As String
        Dim cadenaas400 As String = "DataSource={SERVER};UserID={USER};Password={PASSWORD};LibraryList={LIBRARYLIST};SchemaSearchList={LIBRARYLIST};Naming=System"
        'Dim cadenaas400 As String = "Data Source={SERVER};Password={PASSWORD};User ID={USER};LibraryList={LIBRARYLIST};DefaultCollection={DEFAULT}"

        Dim parametro(1) As String
        Dim dtBFPARAM As DataTable

        parametro(0) = "LXLONG"
        parametro(1) = "AS400"
        dtBFPARAM = ConsultarBFPARAMdt(parametro, 3)
        If dtBFPARAM IsNot Nothing Then
            For Each row As DataRow In dtBFPARAM.Rows
                If row("CCCODE") = "AS400_SERVER" Then
                    If My.Settings.PRUEBAS = "SI" Then
                        cadenaas400 = cadenaas400.Replace("{SERVER}", row("CCDESC"))
                    Else
                        cadenaas400 = cadenaas400.Replace("{SERVER}", row("CCNOT1"))
                    End If
                ElseIf row("CCCODE") = "AS400_USER" Then
                    If My.Settings.PRUEBAS = "SI" Then
                        cadenaas400 = cadenaas400.Replace("{USER}", row("CCDESC"))
                    Else
                        cadenaas400 = cadenaas400.Replace("{USER}", row("CCNOT1"))
                    End If
                ElseIf row("CCCODE") = "AS400_PASS" Then
                    If My.Settings.PRUEBAS = "SI" Then
                        cadenaas400 = cadenaas400.Replace("{PASSWORD}", row("CCDESC"))
                    Else
                        cadenaas400 = cadenaas400.Replace("{PASSWORD}", row("CCNOT1"))
                    End If
                ElseIf row("CCCODE") = "AS400_LIB" Then
                    'If My.Settings.PRUEBAS = "SI" Then
                    '    cadenaas400 = cadenaas400.Replace("{DEFAULT}", row("CCDESC"))
                    'Else
                    '    cadenaas400 = cadenaas400.Replace("{DEFAULT}", row("CCNOT1"))
                    'End If
                    cadenaas400 = cadenaas400.Replace("{LIBRARYLIST}", row("CCNOT2"))
                End If
            Next
        Else
            Return ""
        End If
        Return "Provider=IBMDA400;Data Source=213.62.216.85;User ID=COSIS001;Password=ZXC09ASD;Library List=PP61BPCSO,CP61BPCSF,CP61BPCESP,PP61BPCPTF,PP61CPEUSR,CP61BPCUSR,CP61BPCUSF,PP61BAMUSF,M6168345C,QGPL,QTEMP;Force Translate=0;Naming Convention=1;"
        'Return "Data Source=213.62.216.85;Password=ZXC09ASD;User ID=COSIS001;LibraryList=PP61BPCSO,CP61BPCSF,CP61BPCESP,PP61BPCPTF,PP61CPEUSR,CP61BPCUSR,CP61BPCUSF,PP61BAMUSF,M6168345C,QGPL,QTEMP;SchemaSearchList=PP61BPCSO,CP61BPCSF,CP61BPCESP,PP61BPCPTF,PP61CPEUSR,CP61BPCUSR,CP61BPCUSF,PP61BAMUSF,M6168345C,QGPL,QTEMP;Naming=System"
        'Return cadenaas400
    End Function

    ''' <summary>
    ''' Arma cadena de conexion de AS400
    ''' </summary>
    ''' <returns>String con cadena</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 29-04-2016</remarks>
    Public Function CadenaConexionoledb() As String
        Dim cadenaas400 As String = "Provider=IBMDA400;Data Source={SERVER};User ID={USER};Password={PASSWORD};Library List={LIBRARYLIST};Force Translate=0;Naming Convention=1;"
        'Dim cadenaas400 As String = "Data Source={SERVER};Password={PASSWORD};User ID={USER};LibraryList={LIBRARYLIST};SchemaSearchList={LIBRARYLIST};Naming=System"
        'Dim cadenaas400 As String = "Data Source={SERVER};Password={PASSWORD};User ID={USER};LibraryList={LIBRARYLIST};DefaultCollection={DEFAULT}"

        Dim parametro(1) As String
        Dim dtBFPARAM As DataTable

        parametro(0) = "LXLONG"
        parametro(1) = "AS400"
        dtBFPARAM = ConsultarBFPARAMdt(parametro, 3)
        If dtBFPARAM IsNot Nothing Then
            For Each row As DataRow In dtBFPARAM.Rows
                If row("CCCODE") = "AS400_SERVER" Then
                    If My.Settings.PRUEBAS = "SI" Then
                        cadenaas400 = cadenaas400.Replace("{SERVER}", row("CCDESC"))
                    Else
                        cadenaas400 = cadenaas400.Replace("{SERVER}", row("CCNOT1"))
                    End If
                ElseIf row("CCCODE") = "AS400_USER" Then
                    If My.Settings.PRUEBAS = "SI" Then
                        cadenaas400 = cadenaas400.Replace("{USER}", row("CCDESC"))
                    Else
                        cadenaas400 = cadenaas400.Replace("{USER}", row("CCNOT1"))
                    End If
                ElseIf row("CCCODE") = "AS400_PASS" Then
                    If My.Settings.PRUEBAS = "SI" Then
                        cadenaas400 = cadenaas400.Replace("{PASSWORD}", row("CCDESC"))
                    Else
                        cadenaas400 = cadenaas400.Replace("{PASSWORD}", row("CCNOT1"))
                    End If
                ElseIf row("CCCODE") = "AS400_LIB" Then
                    'If My.Settings.PRUEBAS = "SI" Then
                    '    cadenaas400 = cadenaas400.Replace("{DEFAULT}", row("CCDESC"))
                    'Else
                    '    cadenaas400 = cadenaas400.Replace("{DEFAULT}", row("CCNOT1"))
                    'End If
                    cadenaas400 = cadenaas400.Replace("{LIBRARYLIST}", row("CCNOT2"))
                End If
            Next
        Else
            Return ""
        End If

        Return cadenaas400
    End Function

    ''' <summary>
    ''' Bloquea tabla enviada
    ''' </summary>
    ''' <param name="Table">Tabla a bloquear</param>
    ''' <returns>un booleano</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 15/04/2016</remarks>
    Public Function LockTable(Table As String) As Boolean
        Dim continuarguardando As Boolean
        Using sqlCommand As New MySqlCommand()
            With sqlCommand
                .CommandText = "LOCK TABLES " & Table & " LOW_PRIORITY WRITE"
                .Connection = conexion
                '.Parameters.Add("?Tabla", Table)
            End With
            Try
                sqlCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As MySqlException
                continuarguardando = False
                'Mensaje = "No se pudo eliminar en la tabla sequence_data.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje : " & ex.ToString
            Catch ex As Exception
                continuarguardando = False
                'Mensaje = "No se pudo eliminar en la tabla sequence_data.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje : " & ex.ToString
            Finally
                If continuarguardando = True Then
                    'conexion.Close()
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Bloquea tabla enviada
    ''' </summary>
    ''' <returns>un booleano</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 15/04/2016</remarks>
    Public Function UnLockTable() As Boolean
        Dim continuarguardando As Boolean
        fSql = "UNLOCK TABLES"
        command = New MySqlCommand()
        command.Connection = conexion
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
        Catch ex As MySqlException
            continuarguardando = False
            'mensaje = "No se pudo eliminar la tabla Table.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje : " + ex.ToString()
        Catch ex As Exception
            continuarguardando = False
            'mensaje = "No se pudo eliminar la tabla Table.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje : " + ex.ToString()
        Finally
            If continuarguardando = True Then
            End If
            'conexion.Close()
        End Try
        Return continuarguardando
    End Function

    Public Function CheckVer() As Boolean
        Dim dtZCCL01 As DataTable
        Dim parametro(0) As String
        Dim lVer As String
        Dim msg As String
        Dim NomApp As String
        Dim App As clsApp = New clsApp(System.Reflection.Assembly.GetExecutingAssembly)
        Dim Resultado As Boolean = True

        gsAppPath = App.Path
        gsAppName = App.EXEName
        gsAppVersion = App.Major.ToString & "." & App.Minor.ToString & "." & App.Revision.ToString
        lVer = "No hay registro"

        parametro(0) = gsAppName.ToUpper
        dtZCCL01 = ConsultarZCCL01dt(parametro, 0)
        If dtZCCL01 IsNot Nothing Then
            lVer = dtZCCL01.Rows(0).Item("CCSDSC")
            NomApp = dtZCCL01.Rows(0).Item("CCDESC")
            If InStr(lVer, "*NOCHK") > 0 Then
                Resultado = True
            ElseIf InStr(lVer, "(" & gsAppVersion & ")") > 0 Then
                Resultado = True
            End If
            If Resultado And dtZCCL01.Rows(0).Item("CCUDC1") = 0 Then
                msg = "La aplicacion " & NomApp & " no se puede usar en este momento." & vbCr
                msg = msg & "Razon: " & vbCr
                msg = msg & "=========================================" & vbCr
                If Trim(dtZCCL01.Rows(0).Item("CCNOT2")) = "" Then
                    msg = msg & "Aplicacion en Mantenimiento." & vbCr
                Else
                    msg = msg & dtZCCL01.Rows(0).Item("CCNOT2") & vbCr
                End If
                msg = msg & "=========================================" & vbCr
                Return False
            End If
        End If
        If Not Resultado Then
            msg = "Aplicacion " & App.EXEName.ToUpper & vbCr & _
                   "=========================================" & vbCr & _
                   "Versión incorrecta del programa." & vbCr & _
                   "Versión Registrada: " & lVer & vbCr & _
                   "Versión Actual: " & "(" & gsAppVersion & ")"
            lastError = msg
        End If
        Return Resultado
    End Function

    Public Function CalcConsec(ByVal sID As String) As String
        'Dim fmt As String
        'Dim result As String = ""
        'Dim continuar As Boolean
        'Dim dtsequence_data As DataTable
        'Dim parametro(0) As String
        ''Dim mensajeError As String = ""
        'Try
        '    continuar = LockTable("sequence_data")
        '    If continuar = True Then
        '        parametro(0) = sID
        '        dtsequence_data = Consultarsequence_datadt(parametro, 0)
        '        If dtsequence_data IsNot Nothing Then
        '            result = dtsequence_data.Rows(0).Item("sequence_cur_value")
        '        End If
        '        If result <> "" Then
        '            continuar = Actualizarsequence_dataRow(sID, 0)
        '            If continuar <> False Then
        '                dtsequence_data = Consultarsequence_datadt(parametro, 1)
        '                If dtsequence_data IsNot Nothing Then
        '                    fmt = Replace(dtsequence_data.Rows(0).Item("sequence_max_value"), "9", "0")
        '                    result = Right(fmt & result, Len(fmt))
        '                    continuar = UnLockTable()
        '                End If
        '            End If
        '        End If
        '    End If
        'Catch ex As Exception
        '    Dim sb As New StringBuilder()
        '    Dim sw As StreamWriter = New StreamWriter(appPath & "\CalcConsecError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
        '    sb.AppendLine("LOG " & ex.ToString)
        '    sb.AppendLine(Now().ToString)
        '    sw.WriteLine(sb.ToString())
        '    sw.Close()
        'End Try

        'Return result
        Dim fmt As String
        Dim result As String = ""
        Dim resultmax As String = ""

        Try
            command = New MySqlCommand
            command.Connection = conexion
            fSql = "LOCK TABLES sequence_data LOW_PRIORITY WRITE"
            command.CommandText = fSql
            command.ExecuteNonQuery()
            fSql = "SELECT sequence_cur_value FROM sequence_data WHERE sequence_name = '" & sID & "'"
            command.CommandText = fSql
            result = command.ExecuteScalar()
            If result <> "" Then
                fSql = "UPDATE sequence_data SET sequence_cur_value = If((sequence_cur_value + sequence_increment) > sequence_max_value,If(sequence_cycle = True,sequence_min_value,NULL),sequence_cur_value +sequence_increment) WHERE sequence_name = '" & sID & "'"
                command.CommandText = fSql
                command.ExecuteNonQuery()
                fSql = "SELECT sequence_max_value FROM SEQUENCE_DATA WHERE sequence_name = '" & sID & "'"
                command.CommandText = fSql
                resultmax = command.ExecuteScalar()
                If resultmax <> "" Then
                    fmt = Replace(resultmax, "9", "0")
                    result = Right(fmt & result, Len(fmt))
                    fSql = "UNLOCK TABLES"
                    command.CommandText = fSql
                    command.ExecuteNonQuery()
                End If
            End If
        Catch ex As Exception
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\CalcConsecError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("LOG " & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try

        Return result
    End Function

    Public Function AbrirConexion() As Boolean
        Dim abierta As Boolean = False
        Try
            If conexion.State = ConnectionState.Open Then
                abierta = True
            Else
                conexion.Open()
                abierta = True
            End If
        Catch ex1 As MySqlException
            abierta = False
            mensaje = ex1.Message & vbCrLf & ex1.ToString
        Catch ex As Exception
            abierta = False
            mensaje = ex.Message & vbCrLf & ex.ToString
        Finally
            If abierta = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine(mensaje)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End If
        End Try
        Return abierta
    End Function

    Public Function CerrarConexion() As Boolean
        Dim cerrada As Boolean = False
        Try
            conexion.Close()
            cerrada = True
        Catch ex1 As MySqlException
            cerrada = False
            mensaje = ex1.ToString
        Catch ex As Exception
            cerrada = False
            mensaje = ex.ToString
        End Try
        Return cerrada
    End Function

    ''' <summary>
    ''' Split de cadena (;)
    ''' </summary>
    ''' <returns>Vector con Cadena</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 02/05/2016</remarks>
    Public Function DetalleCadena() As String()
        Dim detalle() As String
        detalle = Split(Cadena, ";")
        Return detalle
    End Function

#End Region

#Region "GUARDAR"

    ''' <summary>
    ''' Guarda BFCIMWS
    ''' </summary>
    ''' <param name="datos">BFCIMWS</param>
    ''' <param name="tipo">0- (IFUENTE, IBATCH, INLINE, INID, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREAS, INREF, INPROD, INWHSE, INLOT, INLOC, INQTY, INCBY, INLDT, INLTM, INCDT, INCTM)</param>
    ''' <returns>true si se guarda con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 15/04/2016</remarks>
    Public Function GuardarBFCIMWSRow(ByVal datos As BFCIMWS, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean
        Using sqlCommand As New MySqlCommand()
            With sqlCommand
                Select Case tipo
                    Case 0
                        fSql = "INSERT INTO BFCIMWS (IFUENTE, IBATCH, INLINE, INID, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREAS, INREF, INPROD, INWHSE, INLOT, INLOC, INQTY, INCBY, INLDT, INLTM, INCDT, INCTM) VALUES ("
                        fSql &= "'" & datos.IFUENTE & "', "
                        fSql &= datos.IBATCH & ", "
                        fSql &= datos.INLINE & ", "
                        fSql &= "'" & datos.INID & "', "
                        fSql &= "'" & datos.INTRAN & "', "
                        fSql &= datos.INCTNR & ", "
                        fSql &= "'" & datos.INMFGR & "', "
                        fSql &= datos.INDATE & ", "
                        fSql &= datos.INTRN & ", "
                        fSql &= "'" & datos.INMLOT & "', "
                        fSql &= "'" & datos.INREAS & "', "
                        fSql &= datos.INREF & ", "
                        fSql &= "'" & datos.INPROD & "', "
                        fSql &= "'" & datos.INWHSE & "', "
                        fSql &= "'" & datos.INLOT & "', "
                        fSql &= "'" & datos.INLOC & "', "
                        fSql &= "'" & datos.INQTY & "', "
                        fSql &= "'" & datos.INCBY & "', "
                        fSql &= datos.INLDT & ", "
                        fSql &= datos.INLTM & ", "
                        fSql &= datos.INCDT & ", "
                        fSql &= datos.INCTM & ")"
                End Select
                .CommandText = fSql
                .Connection = conexion
            End With
            Try
                sqlCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As MySqlException
                continuarguardando = False
                mensaje = ex.Message
            Catch ex As Exception
                continuarguardando = False
                mensaje = ex.Message
            Finally
                If continuarguardando = False Then
                    Dim sb As New StringBuilder()
                    Dim sw As StreamWriter = New StreamWriter(appPath & "\BFCIMWSError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                    sb.AppendLine("No se pudo guardar en la tabla BFCIMWS.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje)
                    sb.AppendLine(Now().ToString)
                    sw.WriteLine(sb.ToString())
                    sw.Close()

                    data = Nothing
                    data = New BFLOG
                    With data
                        .USUARIO = System.Net.Dns.GetHostName()
                        .OPERACION = "Guardar BFCIMWS"
                        .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                        .EVENTO = mensaje
                        .TXTSQL = fSql
                        .ALERT = 1
                    End With
                    GuardarBFLOGRow(data)
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Guarda BFCIM
    ''' </summary>
    ''' <param name="datos">BFCIM</param>
    ''' <param name="tipo">0- (IFUENTE, IBATCH, INID, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREF, INPROD, INLOT, INLOC, INQTY, INCBY, INLDT, INLTM, INCDT, INCTM, INUMERO, INCOST, INWHSE, INFACTU)</param>
    ''' <returns>true si se guarda con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function GuardarBFCIMRow(ByVal datos As BFCIM, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean
        Using sqlCommand As New MySqlCommand()
            With sqlCommand
                Select Case tipo
                    Case 0
                        fSql = "INSERT INTO BFCIM (IFUENTE, IBATCH, INID, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREF, INPROD, INLOT, INLOC, INQTY, INCBY, INLDT, INLTM, INCDT, INCTM, INUMERO, INCOST, INWHSE, INFACTU) " &
                "VALUES (" &
                "'" & datos.IFUENTE & "', " &
                datos.IBATCH & ", " &
                "'" & datos.INID & "', " &
                "'" & datos.INTRAN & "', " &
                "'" & datos.INCTNR & "', " &
                "'" & datos.INMFGR & "', " &
                datos.INDATE & ", " &
                datos.INTRN & ", " &
                "'" & datos.INMLOT & "', " &
                datos.INREF & ", " &
                "'" & datos.INPROD & "', " &
                "'" & datos.INLOT & "', " &
                "'" & datos.INLOC & "', " &
                datos.INQTY & ", " &
                "'" & datos.INCBY & "', " &
                datos.INLDT & ", " &
                datos.INLTM & ", " &
                datos.INCDT & ", " &
                datos.INCTM & ", " &
                datos.INUMERO & ", " &
                datos.INCOST & ", " &
                "'" & datos.INWHSE & "', " &
                "'" & datos.INFACTU & "')"
                End Select
                .CommandText = fSql
                .Connection = conexion
            End With
            Try
                sqlCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As MySqlException
                continuarguardando = False
                mensaje = ex.Message
            Catch ex As Exception
                continuarguardando = False
                mensaje = ex.Message
            Finally
                If continuarguardando = False Then
                    Dim sb As New StringBuilder()
                    Dim sw As StreamWriter = New StreamWriter(appPath & "\BFCIMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                    sb.AppendLine("No se pudo guardar en la tabla BFCIM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje)
                    sb.AppendLine(Now().ToString)
                    sw.WriteLine(sb.ToString())
                    sw.Close()

                    data = Nothing
                    data = New BFLOG
                    With data
                        .USUARIO = System.Net.Dns.GetHostName()
                        .OPERACION = "Guardar BFCIM"
                        .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                        .EVENTO = mensaje
                        .TXTSQL = fSql
                        .ALERT = 1
                    End With
                    GuardarBFLOGRow(data)
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Guarda BFDESPAWS
    ''' </summary>
    ''' <param name="datos">BFCIMWS</param>
    ''' <param name="tipo">0- (DBATCH, DPEDID, DLINEA, DPRODU, DCANTI, DBODEG, DLOCAL, DLOTE, DUSER, DFECHA, DFLAG)</param>
    ''' <returns>true si se guarda con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 15/04/2016</remarks>
    Public Function GuardarBFDESPAWSRow(ByVal datos As BFDESPAWS, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean
        Using sqlCommand As New MySqlCommand()
            With sqlCommand
                Select Case tipo
                    Case 0
                        fSql = "INSERT INTO BFDESPAWS (DBATCH, DPEDID, DLINEA, DPRODU, DCANTI, DBODEG, DLOCAL, DLOTE, DUSER, DFECHA, DFLAG) VALUES ("
                        fSql &= "'" & datos.DBATCH & "', "
                        fSql &= datos.DPEDID & ", "
                        fSql &= "'" & datos.DLINEA & "', "
                        fSql &= "'" & datos.DPRODU & "', "
                        fSql &= datos.DCANTI & ", "
                        fSql &= "'" & datos.DBODEG & "', "
                        fSql &= "'" & datos.DLOCAL & "', "
                        fSql &= "'" & datos.DLOTE & "', "
                        fSql &= "'" & datos.DUSER & "', "
                        fSql &= "'" & datos.DFECHA & "', "
                        fSql &= datos.DFLAG & ")"
                End Select
                .CommandText = fSql
                .Connection = conexion
            End With
            Try
                sqlCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As MySqlException
                continuarguardando = False
                mensaje = ex.Message
            Catch ex As Exception
                continuarguardando = False
                mensaje = ex.Message
            Finally
                If continuarguardando = False Then
                    Dim sb As New StringBuilder()
                    Dim sw As StreamWriter = New StreamWriter(appPath & "\BFDESPAWSError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                    sb.AppendLine("No se pudo guardar en la tabla BFDESPAWS.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje)
                    sb.AppendLine(Now().ToString)
                    sw.WriteLine(sb.ToString())
                    sw.Close()

                    data = Nothing
                    data = New BFLOG
                    With data
                        .USUARIO = System.Net.Dns.GetHostName()
                        .OPERACION = "Guardar BFDESPAWS"
                        .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                        .EVENTO = mensaje
                        .TXTSQL = fSql
                        .ALERT = 1
                    End With
                    GuardarBFLOGRow(data)
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Guarda BFLOG
    ''' </summary>
    ''' <param name="datos">BFLOG</param>
    ''' <returns>true si se guarda con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 15/04/2016</remarks>
    Public Function GuardarBFLOGRow(ByVal datos As BFLOG) As Boolean
        Dim continuarguardandoBFLOG As Boolean

        Using sqlCommand As New MySqlCommand()
            With sqlCommand
                .CommandText = "INSERT INTO BFLOG (USUARIO, OPERACION, PROGRAMA, ALERT, EVENTO, TXTSQL) VALUES (" &
                    "'" & datos.usuario & "', " &
                    "'" & datos.OPERACION & "', " &
                    "'" & datos.programa & "', " &
                    datos.alert & ", " &
                    "'" & datos.evento.Replace("'", "''") & "', " &
                    "'" & datos.txtsql.Replace("'", "''") & "')"
                .Connection = conexion
            End With
            Try
                sqlCommand.ExecuteNonQuery()
                continuarguardandoBFLOG = True
            Catch ex As MySqlException
                continuarguardandoBFLOG = False
                mensaje = ex.Message
            Catch ex As Exception
                continuarguardandoBFLOG = False
                mensaje = ex.Message
            Finally
                If continuarguardandoBFLOG = False Then
                    Dim sb As New StringBuilder()
                    Dim sw As StreamWriter = New StreamWriter(appPath & "\BFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                    sb.AppendLine("No se pudo guardar en la tabla BFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                    sb.AppendLine(Now().ToString)
                    sw.WriteLine(sb.ToString())
                    sw.Close()
                End If
            End Try
        End Using
        Return continuarguardandoBFLOG
    End Function

    ''' <summary>
    ''' Guarda BFWSINBOX
    ''' </summary>
    ''' <param name="datos">BFWSINBOX</param>
    ''' <param name="tipo">0- (C_ID, C_XML)</param>
    ''' <returns>true si se guarda con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 15/04/2016</remarks>
    Public Function GuardarBFWSINBOXRow(ByVal datos As BFWSINBOX, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean
        Using sqlCommand As New MySqlCommand()
            With sqlCommand
                Select Case tipo
                    Case 0
                        fSql = "INSERT INTO BFWSINBOX (C_ID, C_XML) VALUES ("
                        fSql &= "'" & datos.CID & "', "
                        fSql &= "'" & datos.CXML & "')"
                End Select
                .CommandText = fSql
                .Connection = conexion
            End With
            Try
                sqlCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As MySqlException
                continuarguardando = False
                mensaje = ex.Message
            Catch ex As Exception
                continuarguardando = False
                mensaje = ex.Message
            Finally
                If continuarguardando = False Then
                    Dim sb As New StringBuilder()
                    Dim sw As StreamWriter = New StreamWriter(appPath & "\BFWSINBOXError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                    sb.AppendLine("No se pudo guardar en la tabla BFWSINBOX.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje)
                    sb.AppendLine(Now().ToString)
                    sw.WriteLine(sb.ToString())
                    sw.Close()

                    data = Nothing
                    data = New BFLOG
                    With data
                        .USUARIO = System.Net.Dns.GetHostName()
                        .OPERACION = "Guardar BFWSINBOX"
                        .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                        .EVENTO = mensaje
                        .TXTSQL = fSql
                        .ALERT = 1
                    End With
                    GuardarBFLOGRow(data)
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Guarda sequence_data
    ''' </summary>
    ''' <param name="datos">SEQUENCEDATA</param>
    ''' <param name="tipo">0- (sequence_name, sequence_increment, sequence_max_value)</param>
    ''' <returns>true si se guarda con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 15/04/2016</remarks>
    Public Function Guardarsequence_dataRow(ByVal datos As SEQUENCEDATA, ByVal tipo As Integer) As Boolean
        Dim continuarguardando As Boolean
        Using sqlCommand As New MySqlCommand()
            With sqlCommand
                Select Case tipo
                    Case 0
                        fSql = "INSERT INTO sequence_data (sequence_name, sequence_increment, sequence_max_value) VALUES ("
                        fSql &= "'" & datos.sequence_name & "', "
                        fSql &= datos.sequence_increment & ", "
                        fSql &= datos.sequence_max_value & ")"
                End Select
                .CommandText = fSql
                .Connection = conexion
            End With
            Try
                sqlCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As MySqlException
                continuarguardando = False
                mensaje = ex.Message
            Catch ex As Exception
                continuarguardando = False
                mensaje = ex.Message
            Finally
                If continuarguardando = False Then
                    Dim sb As New StringBuilder()
                    Dim sw As StreamWriter = New StreamWriter(appPath & "\sequence_dataError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                    sb.AppendLine("No se pudo guardar en la tabla sequence_data.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje)
                    sb.AppendLine(Now().ToString)
                    sw.WriteLine(sb.ToString())
                    sw.Close()

                    data = Nothing
                    data = New BFLOG
                    With data
                        .USUARIO = System.Net.Dns.GetHostName()
                        .OPERACION = "Guardar sequence_data"
                        .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                        .EVENTO = mensaje
                        .TXTSQL = fSql
                        .ALERT = 1
                    End With
                    GuardarBFLOGRow(data)
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

#End Region

End Class

Public Class BFCIMWS
    Public IFUENTE As String
    Public IBATCH As String
    Public INLINE As String
    Public INID As String
    Public INTRAN As String
    Public INCTNR As String
    Public INMFGR As String
    Public INDATE As String
    Public INTRN As String
    Public INMLOT As String
    Public INREAS As String
    Public INREF As String
    Public INPROD As String
    Public INWHSE As String
    Public INLOT As String
    Public INLOC As String
    Public INQTY As String
    Public INCBY As String
    Public INLDT As String
    Public INLTM As String
    Public INCDT As String
    Public INCTM As String
End Class

Public Class BFCIM
    Public INFACTU As String
    Public IFUENTE As String
    Public IBATCH As String
    Public INLINE As String
    Public INID As String
    Public INTRAN As String
    Public INCTNR As String
    Public INMFGR As String
    Public INDATE As String
    Public INTRN As String
    Public INMLOT As String
    Public INREAS As String
    Public INREF As String
    Public INPROD As String
    Public INWHSE As String
    Public INLOT As String
    Public INLOC As String
    Public INQTY As String
    Public INCBY As String
    Public INLDT As String
    Public INLTM As String
    Public INCDT As String
    Public INCTM As String
    Public INUMERO As String
    Public INCOST As String
End Class

Public Class BFDESPAWS
    Public DBATCH As String
    Public DPEDID As String
    Public DLINEA As String
    Public DPRODU As String
    Public DCANTI As String
    Public DBODEG As String
    Public DLOCAL As String
    Public DLOTE As String
    Public DUSER As String
    Public DFECHA As String
    Public DFLAG As String
End Class

Public Class BFLOG
    Public usuario As String
    Public programa As String
    Public alert As String
    Public evento As String
    Public lkey As String
    Public txtsql As String
    Public OPERACION As String
End Class

Public Class BFWSINBOX
    Public CID As String
    Public CXML As String
    Public CCREATEDDATETIME As String
    Public CWASPROCESSED As String
    Public CNOUNID As String
    Public CMESSAGEID As String
    Public CPASSFAIL As String
    Public CSOURCENAME As String
    Public CUSER As String
    Public CREPLY As String
End Class

Public Class RFPARAM
    Public codigo As String
    Public campo As String
    Public valor As String
End Class

Public Class SEQUENCEDATA
    Public sequence_name As String
    Public sequence_increment As String
    Public sequence_max_value As String
End Class