﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports System.Text
Imports IBM.Data.DB2.iSeries


Public Class ClsDatos
    Public Cadena As String
    Private ReadOnly conexion As MySqlConnection
    Private adapter As MySqlDataAdapter = New MySqlDataAdapter()
    Private trans As MySqlTransaction
    Private builder As MySqlCommandBuilder
    Private command As MySqlCommand
    Private WithEvents ds As DataSet
    Public fSql As String
    Private numFilas As Integer
    Private continuarguardando As Boolean
    Private dt As DataTable
    Public appPath As String = Path.GetFullPath(My.Application.Info.DirectoryPath & "\Log\")
    Public gsQueDB As String
    Public gsDatasource As String
    Public gsLib As String
    Public gsConnstring As String
    Public gsUser As String
    Public gsAppName As String
    Public gsAppPath
    Public gsAppVersion As String
    Public gsKeyWords As String = ""
    Public ModuloActual As String = ""
    Public lastError As String = ""
    Public bDateTimeToChar As Boolean = False
    Dim regA As Double
    Private data
    Public mensaje As String

    Sub New()
        If My.Settings.PRUEBAS = "SI" Then
            Cadena = My.Settings.BASEDATOSLOCAL
        Else
            Cadena = My.Settings.BASEDATOS
        End If
        Try
            conexion = New MySqlConnection(Cadena)
            ' Si el directorio no existe, crearlo
            If Not Directory.Exists(appPath) Then
                Directory.CreateDirectory(appPath)
            End If
        Catch ex As Exception
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\CONEXIONError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo Establecer conexion." & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try

    End Sub

#Region "ACTUALIZAR"

    ''' <summary>
    ''' Actualiza la tabla BFCIM 
    ''' </summary>
    ''' <param name="datos">BFCIM</param>
    ''' <param name="tipo">0- SET IFLAG = @IFLAG, INERROR = @INERROR WHERE INMLOT = @INMLOT,
    ''' 1- SET IFLAG = IFLAG + 2 WHERE INMLOT = @INMLOT,
    ''' 2- SET IFLAG = @IFLAG WHERE IBATCH = @IBATCH AND INTRN = @INTRN,
    ''' 3- SET IFLAG = @IFLAG WHERE IBATCH = @IBATCH</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function ActualizarBFCIMRow(ByVal datos As BFCIM, ByVal tipo As Integer) As Boolean
        trans = conexion.BeginTransaction()
        Select Case tipo
            Case 0
                fSql = " UPDATE BFCIM SET "
                fSql &= " IFLAG = " & datos.IFLAG & ", "
                fSql &= " INERROR = '" & datos.INERROR & "'"
                fSql &= " WHERE INMLOT ='" & datos.INMLOT & "' "
            Case 1
                fSql = " UPDATE BFCIM SET "
                fSql &= " IFLAG = IFLAG + 2 "
                fSql &= " WHERE INMLOT ='" & datos.INMLOT & "' "
            Case 2
                fSql = " UPDATE BFCIM SET "
                fSql &= " IFLAG = " & datos.IFLAG
                fSql &= " WHERE IBATCH =" & datos.IBATCH
                fSql &= " AND INTRN = " & datos.INTRN
            Case 3
                fSql = " UPDATE BFCIM SET "
                fSql &= " IFLAG = " & datos.IFLAG
                fSql &= " WHERE IBATCH =" & datos.IBATCH
        End Select

        command = New MySqlCommand
        command.Connection = conexion
        command.Transaction = trans
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As MySqlException
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFCIMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla BFCIM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Catch ex As Exception
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFCIMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla BFCIM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Finally
            If continuarguardando = True Then
                trans.Commit()
            Else
                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .OPERACION = "Actualizar BFCIM"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                    .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                End With
                GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla BFCIMWS
    ''' </summary>
    ''' <param name="datos">BFCIMWS</param>
    ''' <param name="tipo">0- SET IFLAG2 = @IFLAG2 WHERE IBATCH = @IBATCH AND INTRN = @INTRN,
    ''' 1- SET IFLAG2 = @IFLAG2 WHERE IBATCH = @IBATCH</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function ActualizarBFCIMWSRow(ByVal datos As BFCIMWS, ByVal tipo As Integer) As Boolean
        'conexion.Open()
        trans = conexion.BeginTransaction()
        Select Case tipo
            Case 0
                fSql = " UPDATE BFCIMWS SET "
                fSql &= " IFLAG2 = " & datos.IFLAG2
                fSql &= " WHERE IBATCH =" & datos.IBATCH
                fSql &= " AND INTRN = " & datos.INTRN
            Case 1
                fSql = " UPDATE BFCIMWS SET "
                fSql &= " IFLAG2 = " & datos.IFLAG2
                fSql &= " WHERE IBATCH =" & datos.IBATCH
        End Select

        command = New MySqlCommand
        command.Connection = conexion
        command.Transaction = trans
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As MySqlException
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFCIMWSError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla BFCIMWS.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Catch ex As Exception
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFCIMWSError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla BFCIMWS.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Finally
            If continuarguardando = True Then
                trans.Commit()
                'conexion.Close()
            Else
                'conexion.Close()
                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .OPERACION = "Actualizar BFCIMWS"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                    .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                End With
                GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla BFDESPAWS
    ''' </summary>
    ''' <param name="datos">BFDESPAWS</param>
    ''' <param name="tipo">0- SET DFLAG2 = @DFLAG2 WHERE DBATCH = @DBATCH AND DPEDID = @DPEDID AND DLINEA = @DLINEA,
    ''' 1- SET DFLAG2 = @DFLAG2 WHERE DBATCH = @DBATCH</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function ActualizarBFDESPAWSRow(ByVal datos As BFDESPAWS, ByVal tipo As Integer) As Boolean

        trans = conexion.BeginTransaction()
        Select Case tipo
            Case 0
                fSql = " UPDATE BFDESPAWS SET "
                fSql &= " DFLAG2 = " & datos.DFLAG2
                fSql &= " WHERE DBATCH =" & datos.DBATCH
                fSql &= " AND DPEDID = " & datos.DPEDID
                fSql &= " AND DLINEA =" & datos.DLINEA
            Case 1
                fSql = " UPDATE BFDESPAWS SET "
                fSql &= " DFLAG2 = " & datos.DFLAG2
                fSql &= " WHERE DBATCH =" & datos.DBATCH
        End Select

        command = New MySqlCommand
        command.Connection = conexion
        command.Transaction = trans
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As MySqlException
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFDESPAWSError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla BFDESPAWS.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Catch ex As Exception
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFDESPAWSError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla BFDESPAWS.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Finally
            If continuarguardando = True Then
                trans.Commit()
            Else
                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .OPERACION = "Actualizar BFDESPAWS"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                    .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                End With
                GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla BFPARAM
    ''' </summary>
    ''' <param name="datos">BFPARAM</param>
    ''' <param name="tipo">0- SET CCNOT1 = @CCNOT1, CCMNDT = @CCMNDT WHERE CCNUM = @CCNUM,
    ''' 1- SET CCNOT1 = @CCNOT1, CCENDT = @CCENDT WHERE CCTABL = @CCTABL AND CCCODE = @CCCODE AND CCCODE2 = @CCCODE2,
    ''' 2- SET CCUDTS = @CCUDTS, CCDESC = @CCDESC, CCNOT2 = @CCNOT2, CCNOTE = @CCNOTE, CCENDT = @CCENDT WHERE CCTABL = @CCTABL AND CCCODE = @CCCODE,
    ''' 3- SET CCUDTS = @CCUDTS, CCMNDT = @CCMNDT WHERE CCTABL = @CCTABL AND CCCODE = @CCCODE,
    ''' 4- SET CCUDTS = @CCUDTS, CCNOT1 = @CCNOT1 WHERE CCTABL = @CCTABL AND CCCODE = @CCCODE,
    ''' 5- SET CCUDTS = @CCUDTS, CCUDV1 = @CCUDV1, CCNOT1 = @CCNOT1 WHERE CCTABL = @CCTABL AND CCCODE = @CCCODE,
    ''' 6- SET CCUDTS = @CCUDTS WHERE CCTABL = @CCTABL AND CCCODE = @CCCODE</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function ActualizarBFPARAMRow(ByVal datos As BFPARAM, ByVal tipo As Integer) As Boolean
        trans = conexion.BeginTransaction()
        Select Case tipo
            Case 0
                fSql = "UPDATE BFPARAM SET CCNOT1 = '" & datos.CCNOT1 & "'"
                fSql &= ", CCMNDT ='" & datos.CCMNDT & "' "     '//Z     CCENDT
                fSql &= " WHERE CCNUM = " & datos.CCNUM
            Case 1
                fSql = "UPDATE BFPARAM SET CCNOT1 = '" & datos.CCNOT1 & "'"
                fSql &= ", CCENDT = '" & datos.CCENDT & "' "
                fSql &= " WHERE CCTABL = '" & datos.CCTABL & "' "
                fSql &= " AND CCCODE = '" & datos.CCCODE & "' "
                fSql &= " AND CCCODE2 = '" & datos.CCCODE2 & "' "
            Case 2
                fSql = " UPDATE BFPARAM SET "
                fSql &= " CCUDTS = '" & datos.CCUDTS & "' "
                fSql &= ", CCDESC = '" & datos.CCDESC & "' "
                fSql &= ", CCNOT2 = '" & datos.CCNOT2 & "' "
                fSql &= ", CCNOTE = '" & datos.CCNOTE & "' "
                fSql &= ", CCENDT = '" & datos.CCENDT & "' "
                fSql &= " WHERE CCTABL = '" & datos.CCTABL & "' AND CCCODE = '" & datos.CCCODE & "'"
            Case 3
                fSql = " UPDATE BFPARAM SET "
                fSql &= " CCUDTS = '" & datos.CCUDTS & "' "
                fSql &= ", CCMNDT ='" & datos.CCMNDT & "' "
                fSql &= " WHERE CCTABL = '" & datos.CCTABL & "' AND CCCODE = '" & datos.CCCODE & "'"
            Case 4
                fSql = " UPDATE BFPARAM SET "
                fSql &= " CCUDTS = '" & datos.CCUDTS & "'"
                fSql &= ", CCNOT1    ='" & datos.CCNOT1 & "'"
                fSql &= " WHERE CCTABL = '" & datos.CCTABL & "' AND CCCODE = '" & datos.CCCODE & "'"
            Case 5
                fSql = " UPDATE BFPARAM SET "
                fSql &= " CCUDTS = '" & datos.CCUDTS & "'"
                fSql &= ", CCUDV1    = " & datos.CCUDV1 & " "
                fSql &= ", CCNOT1    ='" & datos.CCNOT1 & "' "
                fSql &= " WHERE CCTABL = '" & datos.CCTABL & "' AND CCCODE = '" & datos.CCCODE & "'"
            Case 6
                fSql = " UPDATE BFPARAM SET "
                fSql &= " CCUDTS = '" & datos.CCUDTS & "'"
                fSql &= " WHERE CCTABL = '" & datos.CCTABL & "' AND CCCODE = '" & datos.CCCODE & "'"
            Case 7
                fSql = "UPDATE RFPARAM SET CCUDTS = Now(), CCNOTE = '" & datos.CCNOTE & "', CCUDC3 = " & datos.CCUDC3
                fSql &= " WHERE CCTABL = '" & datos.CCTABL & "' AND CCCODE3 = '" & datos.CCCODE3 & "' "
        End Select

        command = New MySqlCommand
        command.Connection = conexion
        command.Transaction = trans
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As MySqlException
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFPARAMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla BFPARAM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Catch ex As Exception
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFPARAMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla BFPARAM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Finally
            If continuarguardando = True Then
                trans.Commit()
                'conexion.Close()
            Else
                'conexion.Close()
                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .OPERACION = "Actualizar BFPARAM"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                    .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                End With
                GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla BFREP
    ''' </summary>
    ''' <param name="datos">BFREP</param>
    ''' <param name="tipo">0- SET TQRECB = @TQRECB, TQREO = @TQREO WHERE TORD = @TORD,
    ''' 1- SET TQREQACEB = TQREQACEB + @TQREQACEB WHERE TNUMREP = @TNUMREP,| 
    ''' 2- SET TQREDB = TQREDB +  @TQREDB WHERE TNUMREP = @TNUMREP,| 
    ''' 3- SET TRPODACEB = TRPODACEB + @TRPODACEB WHERE TNUMREP = @TNUMREP,| 
    ''' 4- SET TQREC = TQREC + @TQREC WHERE TNUMREP = @TNUMREP</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function ActualizarBFREPRow(ByVal datos As BFREP, ByVal tipo As Integer) As Boolean
        'conexion.Open()
        trans = conexion.BeginTransaction()
        Select Case tipo
            Case 0
                fSql = "UPDATE BFREP SET TQRECB = " & datos.TQRECB
                fSql &= ", TQREO = " & datos.TQREO
                fSql &= " WHERE TORD = " & datos.TORD
            Case 1
                fSql = "UPDATE BFREP SET TQREQACEB = TQREQACEB + " & datos.TQREQACEB & " WHERE TNUMREP = " & datos.TNUMREP
            Case 2
                fSql = "UPDATE BFREP SET TQREDB = TQREDB +  " & datos.TQREDB & " WHERE TNUMREP = " & datos.TNUMREP
            Case 3
                fSql = "UPDATE BFREP SET TRPODACEB = TRPODACEB + " & datos.TRPODACEB & " WHERE TNUMREP = " & datos.TNUMREP
            Case 4
                fSql = "UPDATE BFREP SET TQREC = TQREC + " & datos.TQREC & " WHERE TNUMREP = " & datos.TNUMREP
        End Select

        command = New MySqlCommand
        command.Connection = conexion
        command.Transaction = trans
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As MySqlException
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFREPError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla BFREP.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Catch ex As Exception
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFREPError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla BFREP.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Finally
            If continuarguardando = True Then
                trans.Commit()
                'conexion.Close()
            Else
                'conexion.Close()
                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .OPERACION = "Actualizar BFREP"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                    .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                End With
                GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla BFREPACE
    ''' </summary>
    ''' <param name="datos">BFREPACE</param>
    ''' <param name="tipo">0- SET CENVIADO = @CENVIADO WHERE WRKID = @WRKID</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function ActualizarBFREPACERow(ByVal datos As BFREPACE, ByVal tipo As Integer) As Boolean
        'conexion.Open()
        trans = conexion.BeginTransaction()
        Select Case tipo
            Case 0
                fSql = "UPDATE BFREPACE SET CENVIADO = " & datos.CENVIADO & " WHERE WRKID = " & datos.WRKID
        End Select

        command = New MySqlCommand
        command.Connection = conexion
        command.Transaction = trans
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As MySqlException
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFREPACEError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla BFREPACE.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Catch ex As Exception
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFREPACEError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla BFREPACE.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Finally
            If continuarguardando = True Then
                trans.Commit()
                'conexion.Close()
            Else
                'conexion.Close()
                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .OPERACION = "Actualizar BFREPACE"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                    .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                End With
                GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla BFREPD
    ''' </summary>
    ''' <param name="datos">BFREPD</param>
    ''' <param name="tipo">0- SET TQREC = TQREC + @TQREC WHERE TNUMREP = @TNUMREP</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function ActualizarBFREPDRow(ByVal datos As BFREPD, ByVal tipo As Integer) As Boolean
        'conexion.Open()
        trans = conexion.BeginTransaction()
        Select Case tipo
            Case 0
                fSql = "UPDATE BFREPD SET TQREC = TQREC + " & datos.TQREC & " WHERE TNUMREP = " & datos.TNUMREP
        End Select

        command = New MySqlCommand
        command.Connection = conexion
        command.Transaction = trans
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As MySqlException
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFREPDError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla BFREPD.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Catch ex As Exception
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFREPDError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla BFREPD.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Finally
            If continuarguardando = True Then
                trans.Commit()
                'conexion.Close()
            Else
                'conexion.Close()
                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .OPERACION = "Actualizar BFREPD"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                    .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                End With
                GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla BFREPTURRJ
    ''' </summary>
    ''' <param name="datos">BFREPTURRJ</param>
    ''' <param name="tipo">0- SET RENVIADO = @RENVIADO WHERE RWRKID = @RWRKID</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function ActualizarBFREPTURRJRow(ByVal datos As BFREPTURRJ, ByVal tipo As Integer) As Boolean
        'conexion.Open()
        trans = conexion.BeginTransaction()
        Select Case tipo
            Case 0
                fSql = "UPDATE BFREPTURRJ SET RENVIADO = " & datos.RENVIADO & " WHERE RWRKID = " & datos.RWRKID
        End Select

        command = New MySqlCommand
        command.Connection = conexion
        command.Transaction = trans
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As MySqlException
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFREPTURRJError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla BFREPTURRJ.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Catch ex As Exception
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFREPTURRJError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla BFREPTURRJ.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Finally
            If continuarguardando = True Then
                trans.Commit()
                'conexion.Close()
            Else
                'conexion.Close()
                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .OPERACION = "Actualizar BFREPTURRJ"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                    .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                End With
                GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla RFPARAM
    ''' </summary>
    ''' <param name="datos">RFPARAM</param>
    ''' <param name="tipo">0- </param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function ActualizarRFPARAMRow(ByVal datos As RFPARAM, ByVal tipo As Integer) As Boolean
        trans = conexion.BeginTransaction()
        Select Case tipo
            Case 0
                If datos.Campo = "" Then
                    datos.Campo = "CCDESC"
                End If
                fSql = "UPDATE RFPARAM SET  " & datos.Campo & " = '" & datos.Valor & "'"
                fSql &= " FROM RFPARAM "
                fSql &= " WHERE CCTABL= '" & datos.CCTABL & "' AND UPPER(CCCODE) = UPPER('" & datos.CCCODE & "')"
            Case 1
                If datos.Campo = "" Then
                    datos.Campo = "CCDESC"
                End If
                fSql = "UPDATE RFPARAM SET  " & datos.Campo & " = " & CDbl(datos.Valor)
                fSql &= " WHERE CCTABL='" & datos.CCTABL & "' AND UPPER(CCCODE) = UPPER('" & datos.CCCODE & "')"
        End Select

        command = New MySqlCommand
        command.Connection = conexion
        command.Transaction = trans
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As MySqlException
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFPARAMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla RFPARAM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Catch ex As Exception
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFPARAMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla RFPARAM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Finally
            If continuarguardando = True Then
                trans.Commit()
                'conexion.Close()
            Else
                'conexion.Close()
                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .OPERACION = "Actualizar RFPARAM"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                    .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                End With
                GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla sequence_data 
    ''' </summary>
    ''' <param name="seq_name">seq_name</param>
    ''' <param name="mensaje">Variable donde se va a guardar mensaje en caso de error</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 15/04/2016</remarks>
    Public Function Actualizarsequence_dataRow(ByVal seq_name As String, ByRef mensaje As String) As Boolean
        continuarguardando = True
        'conexion.Open()
        trans = conexion.BeginTransaction()
        fSql = "UPDATE sequence_data SET sequence_cur_value = If((sequence_cur_value + sequence_increment) > sequence_max_value,If(sequence_cycle = True,sequence_min_value,NULL),sequence_cur_value +sequence_increment) WHERE sequence_name = ?seq_name"
        command = New MySqlCommand()
        command.Connection = conexion
        command.Transaction = trans
        Try
            command.CommandText = fSql
            command.Parameters.Add(New MySqlParameter("?seq_name", seq_name))
            command.ExecuteNonQuery()
        Catch ex As MySqlException
            trans.Rollback()
            continuarguardando = False
            mensaje = "No se pudo actualizar la tabla sequence_data.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje : " + ex.ToString()
        Catch ex As Exception
            trans.Rollback()
            continuarguardando = False
            mensaje = "No se pudo actualizar la tabla sequence_data.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje : " + ex.ToString()
        Finally
            If continuarguardando = True Then
                trans.Commit()
            End If
            'conexion.Close()
        End Try
        Return continuarguardando
    End Function

#End Region

#Region "CONSULTAR"

    ''' <summary>
    ''' Consulta la tabla BFCIM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca donde IFLAG in (1,2),
    ''' 1- Busca por IFLAG(0) e IFUENTE(1),
    ''' 2- Busca por IFLAG(0) agrupado por IBATCH,
    ''' 3- Busca por IFLAG(0) e IFUENTE(1) agrupado por IBATCH</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarBFCIMdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        'conexion.Open()
        trans = conexion.BeginTransaction()

        Select Case tipoparametro
            Case 0
                fSql = "SELECT IFLAG, INMLOT FROM BFCIM WHERE IFLAG IN( 1, 2 )"
            Case 1
                fSql = "SELECT * FROM BFCIM WHERE IFLAG = " & parametroBusqueda(0) & " AND IFUENTE = '" & parametroBusqueda(1) & "'"
            Case 2
                fSql = "SELECT * FROM BFCIM WHERE IFLAG = " & parametroBusqueda(0) & " GROUP BY IBATCH"
            Case 3
                fSql = "SELECT * FROM BFCIM WHERE IFLAG = " & parametroBusqueda(0) & " AND IFUENTE = '" & parametroBusqueda(1) & "' GROUP BY IBATCH"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adapter.SelectCommand.Transaction = trans
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        trans.Commit()
        'conexion.Close()
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla BFCIMWS con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca donde IFLAG2(0),
    ''' 1- Busca donde IFLAG2(0) agrupado por IBATCH</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarBFCIMWSdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        'conexion.Open()
        trans = conexion.BeginTransaction()

        Select Case tipoparametro
            Case 0
                fSql = "SELECT * FROM BFCIMWS WHERE IFLAG2 = " & parametroBusqueda(0)
            Case 1
                fSql = "SELECT * FROM BFCIMWS WHERE IFLAG2 = " & parametroBusqueda(0) & " GROUP BY IBATCH"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adapter.SelectCommand.Transaction = trans
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        trans.Commit()
        'conexion.Close()
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla BFDESPAWS con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por DFLAG2(0),
    ''' 1- Busca por DFLAG2(0) agrupado por DBATCH</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarBFDESPAWSdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        trans = conexion.BeginTransaction()

        Select Case tipoparametro
            Case 0
                fSql = "SELECT * FROM BFDESPAWS WHERE DFLAG2 = " & parametroBusqueda(0)
            Case 1
                fSql = "SELECT * FROM BFDESPAWS WHERE DFLAG2 = " & parametroBusqueda(0) & " GROUP BY DBATCH"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adapter.SelectCommand.Transaction = trans
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        trans.Commit()

        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla BFPARAM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por CCTABL(0) y CCCODE2(1),
    ''' 1- Busca por CCTABL(0) CCCODE(1) y CCCODE2(2),
    ''' 2- Busca por CCTABL(0) y CCCODE(1),
    ''' 3- Busca por CCTABL(0) y CCCODE(1),
    ''' 4- Busca por CCTABL(0) y CCCODE LIKE %(1)%</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarBFPARAMdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        'conexion.Open()
        trans = conexion.BeginTransaction()

        Select Case tipoparametro
            Case 0
                fSql = " SELECT CCDESC FROM BFPARAM WHERE CCTABL = '" & parametroBusqueda(0) & "' AND CCCODE2 = '" & parametroBusqueda(1) & "' "
            Case 1
                fSql = "SELECT * FROM BFPARAM WHERE CCTABL = '" & parametroBusqueda(0) & "' AND CCCODE = '" & parametroBusqueda(1) & "' AND CCCODE2 = '" & parametroBusqueda(2) & "' "
            Case 2
                fSql = "SELECT * FROM BFPARAM WHERE CCTABL = '" & parametroBusqueda(0) & "' AND CCCODE = '" & parametroBusqueda(1) & "' "
            Case 3
                fSql = "SELECT CCDESC FROM BFPARAM WHERE CCTABL = '" & parametroBusqueda(0) & "' AND CCCODE = '" & parametroBusqueda(1) & "' "
            Case 4
                fSql = "SELECT * FROM BFPARAM WHERE CCTABL = '" & parametroBusqueda(0) & "' AND CCCODE LIKE '%" & parametroBusqueda(1) & "%'"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adapter.SelectCommand.Transaction = trans
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        trans.Commit()
        'conexion.Close()
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla BFPARAM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por CCTABL(0) y CCCODE2(1),
    ''' 1- Busca por CCTABL(0) CCCODE(1) y CCCODE2(2),
    ''' 2- Busca por CCTABL(0) y CCCODE(1),
    ''' 3- Busca por CCTABL(0) y CCCODE(1),
    ''' 4- Busca por CCTABL(0) y CCCODE LIKE %(1)%</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarBFPARAMst(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As String
        dt = Nothing

        trans = conexion.BeginTransaction()

        Select Case tipoparametro
            Case 0
                fSql = " SELECT CCNOT1 FROM BFPARAM WHERE CCTABL = '" & parametroBusqueda(0) & "' "
            
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adapter.SelectCommand.Transaction = trans
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        trans.Commit()

        Return dt.Rows(0).Item("CCNOT1")
    End Function

    ''' <summary>
    ''' Consulta la tabla BFREP con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por TSTS != 2 agrupado por TORD,
    ''' 1- Busca por TORD(0),
    ''' 2- Busca por RENVIADO = 0,
    ''' 3- Busca donde TQREP != TQREC</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarBFREPdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        trans = conexion.BeginTransaction()

        Select Case tipoparametro
            Case 0
                fSql = "SELECT TORD FROM BFREP WHERE TSTS <> 2 GROUP BY TORD "
            Case 1
                fSql = "SELECT * FROM BFREP INNER JOIN BFREPP ON TPROD = PPROD WHERE TORD ='" & parametroBusqueda(0) & "'"
            Case 2
                fSql = "SELECT * FROM BFREP INNER JOIN BFREPP ON TPROD = PPROD INNER JOIN BFREPTURRJ ON TNUMREP = RNUMREP WHERE RENVIADO = 0"
            Case 3
                fSql = "SELECT * FROM BFREP INNER JOIN BFREPP ON TPROD = PPROD WHERE TQREP <> TQREC "
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adapter.SelectCommand.Transaction = trans
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        trans.Commit()

        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla BFREPACE con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca donde CENVIADO = 0 y CTIPO(0),
    ''' 1- Busca por CORDEN(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarBFREPACEdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        'conexion.Open()
        trans = conexion.BeginTransaction()

        Select Case tipoparametro
            Case 0
                fSql = "SELECT * FROM BFREPACE INNER JOIN BFREPP ON CPRODPT = PPROD WHERE CENVIADO = 0 AND CTIPO = '" & parametroBusqueda(0) & "'"
            Case 1
                fSql = "SELECT * FROM BFREPACE INNER JOIN BFREPP ON CPRODPT  = PPROD WHERE CORDEN =" & parametroBusqueda(0)
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adapter.SelectCommand.Transaction = trans
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        trans.Commit()
        'conexion.Close()
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla BFREPD con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca donde TSTS != 2 agrupado por TORD,
    ''' 1- Busca donde TQREP mayor TQREC</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarBFREPDdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        trans = conexion.BeginTransaction()

        Select Case tipoparametro
            Case 0
                fSql = "SELECT TORD FROM BFREPD WHERE TSTS <> 2 GROUP BY TORD "
            Case 1
                fSql = "SELECT * FROM BFREPD INNER JOIN BFREPP ON TPROD = PPROD WHERE TQREP > TQREC"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adapter.SelectCommand.Transaction = trans
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        trans.Commit()

        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla CIC con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por ICPROD(0) e ICFAC = IC</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarCICdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        trans = conexion.BeginTransaction()

        Select Case tipoparametro
            Case 0
                fSql = "SELECT * FROM CIC WHERE ICPROD = '" & parametroBusqueda(0) & "' AND ICFAC = 'IC'"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adapter.SelectCommand.Transaction = trans
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        trans.Commit()

        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla FSO con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por SORD(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarFSOdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        'conexion.Open()
        trans = conexion.BeginTransaction()

        Select Case tipoparametro
            Case 0
                fSql = "SELECT SQREQ, SQFIN FROM FSO WHERE SORD =" & parametroBusqueda(0)
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adapter.SelectCommand.Transaction = trans
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        trans.Commit()
        'conexion.Close()
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla RCAU con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por UUSR(0) y UPASS(1)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarRCAUdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        'conexion.Open()
        trans = conexion.BeginTransaction()

        Select Case tipoparametro
            Case 0
                fSql = "SELECT UUSR FROM RCAU WHERE UUSR='" & parametroBusqueda(0) & "' AND UPASS='" & parametroBusqueda(1) & "'"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adapter.SelectCommand.Transaction = trans
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        trans.Commit()
        'conexion.Close()
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla RFPARAM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por CCTABL(1) y CCCODE(2) retorna(0),
    ''' 1- Busca por CCTABL(1) - CCCODE(2) y CCCODE2(3) retorna (0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarRFPARAMdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        'conexion.Open()
        trans = conexion.BeginTransaction()

        Select Case tipoparametro
            Case 0
                If parametroBusqueda(0) = "" Then
                    parametroBusqueda(0) = "CCDESC"
                End If
                fSql = "SELECT " & parametroBusqueda(0) & " AS DATO FROM RFPARAM WHERE CCTABL='" & parametroBusqueda(1) & "' AND UPPER(CCCODE) = UPPER('" & parametroBusqueda(2) & "')"
            Case 1
                If parametroBusqueda(0) = "" Then
                    parametroBusqueda(0) = "CCDESC"
                End If
                fSql = "SELECT " & parametroBusqueda(0) & " AS DATO "
                fSql &= " FROM RFPARAM "
                fSql &= " WHERE CCTABL='" & parametroBusqueda(1) & "' AND UPPER(CCCODE) = UPPER('" & parametroBusqueda(2) & "')"
                fSql &= " AND UPPER(CCCODE2) = UPPER('" & parametroBusqueda(3) & "')"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adapter.SelectCommand.Transaction = trans
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        trans.Commit()
        'conexion.Close()
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla sequence_data con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por sequence_name(0) devuelve sequence_cur_value, 1- Busca por sequence_name(0) devuelve sequence_max_value</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 15/04/2016</remarks>
    Public Function Consultarsequence_datadt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        'conexion.Open()
        trans = conexion.BeginTransaction()

        Select Case tipoparametro
            Case 0
                fSql = "SELECT sequence_cur_value FROM sequence_data WHERE sequence_name = '" & parametroBusqueda(0) & "'"
            Case 1
                fSql = "SELECT sequence_max_value FROM SEQUENCE_DATA WHERE sequence_name = '" & parametroBusqueda(0) & "'"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adapter.SelectCommand.Transaction = trans
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        trans.Commit()
        'conexion.Close()
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla ZCC con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por CCTABL(0) y CCCODE(1)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 27/04/2016</remarks>
    Public Function ConsultarZCCdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        'conexion.Open()
        trans = conexion.BeginTransaction()

        Select Case tipoparametro
            Case 0
                fSql = " SELECT CCNOT1 FROM ZCC WHERE CCTABL = '" & parametroBusqueda(0) & "' AND CCCODE = '" & parametroBusqueda(1) & "' "
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adapter.SelectCommand.Transaction = trans
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        trans.Commit()
        'conexion.Close()
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla ZCCL01 con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por CCCODE(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 27/04/2016</remarks>
    Public Function ConsultarZCCL01dt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        'conexion.Open()
        trans = conexion.BeginTransaction()

        Select Case tipoparametro
            Case 0
                fSql = "SELECT CCSDSC, CCUDC1, CCNOT2, CCDESC  FROM ZCCL01 WHERE CCTABL='RFVBVER' AND UPPER(CCCODE) = '" & parametroBusqueda(0) & "'"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adapter.SelectCommand.Transaction = trans
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        trans.Commit()
        'conexion.Close()
        Return dt
    End Function

#End Region

#Region "FUNCIONES"

    ''' <summary>
    ''' Bloquea tabla enviada
    ''' </summary>
    ''' <param name="Table">Tabla a bloquear</param>
    ''' <returns>un booleano</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 15/04/2016</remarks>
    Public Function LockTable(Table As String) As Boolean
        trans = conexion.BeginTransaction()
        Using sqlCommand As New MySqlCommand()
            With sqlCommand
                .CommandText = "LOCK TABLES " & Table & " LOW_PRIORITY WRITE"
                .Connection = conexion
                .Transaction = trans
            End With
            Try
                sqlCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As MySqlException
                trans.Rollback()
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\LOCKError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Catch ex As Exception
                trans.Rollback()
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\LOCKError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Finally
                If continuarguardando = True Then
                    trans.Commit()
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Bloquea tabla enviada
    ''' </summary>
    ''' <returns>un booleano</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 15/04/2016</remarks>
    Public Function UnLockTable() As Boolean
        continuarguardando = True
        trans = conexion.BeginTransaction()
        fSql = "UNLOCK TABLES"
        command = New MySqlCommand()
        command.Connection = conexion
        command.Transaction = trans
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
        Catch ex As MySqlException
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\UNLOCKError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.ToString()
        Catch ex As Exception
            trans.Rollback()
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\UNLOCKError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.ToString()
        Finally
            If continuarguardando = True Then
                trans.Commit()
            End If
        End Try
        Return continuarguardando
    End Function

    Public Function CheckVer() As Boolean
        Dim dtZCCL01 As DataTable
        Dim parametro(0) As String
        Dim lVer As String
        Dim msg As String
        Dim NomApp As String
        Dim App As clsApp = New clsApp(System.Reflection.Assembly.GetExecutingAssembly)
        Dim Resultado As Boolean = True

        gsAppPath = App.Path
        gsAppName = App.EXEName
        gsAppVersion = App.Major.ToString & "." & App.Minor.ToString & "." & App.Revision.ToString
        lVer = "No hay registro"

        parametro(0) = gsAppName.ToUpper
        dtZCCL01 = ConsultarZCCL01dt(parametro, 0)
        If dtZCCL01 IsNot Nothing Then
            lVer = dtZCCL01.Rows(0).Item("CCSDSC")
            NomApp = dtZCCL01.Rows(0).Item("CCDESC")
            If InStr(lVer, "*NOCHK") > 0 Then
                Resultado = True
            ElseIf InStr(lVer, "(" & gsAppVersion & ")") > 0 Then
                Resultado = True
            End If
            If Resultado And dtZCCL01.Rows(0).Item("CCUDC1") = 0 Then
                msg = "La aplicacion " & NomApp & " no se puede usar en este momento." & vbCr
                msg = msg & "Razon: " & vbCr
                msg = msg & "=========================================" & vbCr
                If Trim(dtZCCL01.Rows(0).Item("CCNOT2")) = "" Then
                    msg = msg & "Aplicacion en Mantenimiento." & vbCr
                Else
                    msg = msg & dtZCCL01.Rows(0).Item("CCNOT2") & vbCr
                End If
                msg = msg & "=========================================" & vbCr
                Return False
            End If
        End If
        If Not Resultado Then
            msg = "Aplicacion " & App.EXEName.ToUpper & vbCr & _
                   "=========================================" & vbCr & _
                   "Versión incorrecta del programa." & vbCr & _
                   "Versión Registrada: " & lVer & vbCr & _
                   "Versión Actual: " & "(" & gsAppVersion & ")"
            lastError = msg
        End If
        Return Resultado
    End Function

    Public Function CalcConsec(ByVal sID As String) As String
        Dim fmt As String
        Dim result As String = ""
        Dim continuar As Boolean
        Dim dtsequence_data As DataTable
        Dim parametro(0) As String
        Dim mensajeError As String = ""
        Try
            continuar = LockTable("sequence_data")
            If continuar = True Then
                parametro(0) = sID
                dtsequence_data = Consultarsequence_datadt(parametro, 0)
                If dtsequence_data IsNot Nothing Then
                    result = dtsequence_data.Rows(0).Item("sequence_cur_value")
                End If
                If result <> "" Then
                    continuar = Actualizarsequence_dataRow(sID, mensajeError)
                    If continuar <> False Then
                        dtsequence_data = Consultarsequence_datadt(parametro, 1)
                        If dtsequence_data IsNot Nothing Then
                            fmt = Replace(dtsequence_data.Rows(0).Item("sequence_max_value"), "9", "0")
                            result = Right(fmt & result, Len(fmt))
                            continuar = UnLockTable()
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\CalcConsecError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("LOG " & ex.ToString)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try

        Return result
    End Function

    Public Function CadenaConexionAS400() As String
        Dim cadenaas400 As String = "Data Source={SERVER};Password={PASSWORD};User ID={USER};LibraryList={LIBRARYLIST};SchemaSearchList={LIBRARYLIST};Naming=System"
        'Dim cadenaas400 As String = "Data Source={SERVER};Password={PASSWORD};User ID={USER};LibraryList={LIBRARYLIST};DefaultCollection={DEFAULT}"

        Dim parametro(1) As String
        Dim dtBFPARAM As DataTable

        parametro(0) = "LXLONG"
        parametro(1) = "AS400"
        dtBFPARAM = ConsultarBFPARAMdt(parametro, 4)
        If dtBFPARAM IsNot Nothing Then
            For Each row As DataRow In dtBFPARAM.Rows
                If row("CCCODE") = "AS400_SERVER" Then
                    If My.Settings.PRUEBAS = "SI" Then
                        cadenaas400 = cadenaas400.Replace("{SERVER}", row("CCDESC"))
                    Else
                        cadenaas400 = cadenaas400.Replace("{SERVER}", row("CCNOT1"))
                    End If
                ElseIf row("CCCODE") = "AS400_USER" Then
                    If My.Settings.PRUEBAS = "SI" Then
                        cadenaas400 = cadenaas400.Replace("{USER}", row("CCDESC"))
                    Else
                        cadenaas400 = cadenaas400.Replace("{USER}", row("CCNOT1"))
                    End If
                ElseIf row("CCCODE") = "AS400_PASS" Then
                    If My.Settings.PRUEBAS = "SI" Then
                        cadenaas400 = cadenaas400.Replace("{PASSWORD}", row("CCDESC"))
                    Else
                        cadenaas400 = cadenaas400.Replace("{PASSWORD}", row("CCNOT1"))
                    End If
                ElseIf row("CCCODE") = "AS400_LIB" Then
                    'If My.Settings.PRUEBAS = "SI" Then
                    '    cadenaas400 = cadenaas400.Replace("{DEFAULT}", row("CCDESC"))
                    'Else
                    '    cadenaas400 = cadenaas400.Replace("{DEFAULT}", row("CCNOT1"))
                    'End If
                    cadenaas400 = cadenaas400.Replace("{LIBRARYLIST}", row("CCNOT2"))
                End If
            Next
        Else
            Return ""
        End If

        Return cadenaas400
    End Function

    ''' <summary>
    ''' Split de cadena (;)
    ''' </summary>
    ''' <returns>Vector con Cadena</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 28/04/2016</remarks>
    Public Function DetalleCadena() As String()
        Dim detalle() As String
        detalle = Split(Cadena, ";")
        Return detalle
    End Function

    Public Function AbrirConexion() As Boolean
        Dim abierta As Boolean = False
        Try
            If conexion.State = ConnectionState.Open Then
                abierta = True
            Else
                conexion.Open()
                abierta = True
            End If
        Catch ex As MySqlException
            abierta = False
            mensaje = ex.Message
        Catch ex As Exception
            abierta = False
            mensaje = ex.Message
        Finally
            If abierta = False Then
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\AbrirConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("AbrirConexion.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & conexion.ConnectionString)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End If
        End Try

        Return abierta
    End Function

    Public Function CerrarConexion() As Boolean
        Dim cerrada As Boolean = False
        Try
            conexion.Close()
            cerrada = True
        Catch ex1 As MySqlException
            cerrada = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\AbrirConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("AbrirConexion.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex1.Message & vbCrLf & conexion.ConnectionString)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex1.ToString
        Catch ex As Exception
            cerrada = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\AbrirConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("AbrirConexion.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & conexion.ConnectionString)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.ToString
        End Try
        Return cerrada
    End Function

    Function Ceros(ByVal val As Object, ByVal C As Integer)
        Dim sCeros As String = New String("0", C)

        If CInt(C) > Len(Trim(val)) Then
            Ceros = Right(sCeros & Trim(val), C)
        Else
            Ceros = val
        End If
    End Function

    Sub WrtSqlError(sql As String, ByVal Descrip As String)
        Dim continuar As Boolean
        Dim application As String

        If gsKeyWords.Trim <> "" Then
            Descrip = gsKeyWords & "<br>" & Descrip
        End If
        Descrip = Left(Descrip, 20000)

        Try
            application = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
            data = Nothing
            data = New BFLOG
            With data
                .USUARIO = System.Net.Dns.GetHostName()
                .PROGRAMA = application
                .ALERT = "1"
                .EVENTO = Replace(Descrip, "'", "''")
                .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                .TXTSQL = Replace(sql, "'", "''")
            End With
            continuar = GuardarBFLOGRow(data)
            If continuar = False Then
                'ERROR DE GUARDADO
            End If
        Catch ex As Exception

        End Try
    End Sub

#End Region

#Region "GUARDAR"

    ''' <summary>
    ''' Guarda BFCIM
    ''' </summary>
    ''' <param name="datos">BFCIM</param>
    ''' <param name="tipo">0- (IFUENTE, IBATCH, INLINE, INID, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREAS, INREF, INPROD, INWHSE, INLOT, INLOC, INQTY, INCBY, INLDT, INLTM, INCDT, INCTM),
    ''' 1- (IFUENTE, IBATCH, INID, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREAS, INREF, INPROD, INWHSE, INLOT, INLOC, INQTY, INCBY, INLDT, INLTM, INCDT, INCTM)</param>
    ''' <returns>true si se guarda con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function GuardarBFCIMRow(ByVal datos As BFCIM, ByVal tipo As Integer) As Boolean
        trans = conexion.BeginTransaction()
        Using sqlCommand As New MySqlCommand()
            With sqlCommand
                Select Case tipo
                    Case 0
                        fSql = "INSERT INTO BFCIM (IFUENTE, IBATCH, INLINE, INID, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREAS, INREF, INPROD, INWHSE, INLOT, INLOC, INQTY, INCBY, INLDT, INLTM, INCDT, INCTM) VALUES (" &
                         "'" & datos.IFUENTE & "', " &
                        datos.IBATCH & ", " &
                        datos.INLINE & ", " &
                        "'" & datos.INID & "', " &
                        "'" & datos.INTRAN & "', " &
                        "'" & datos.INCTNR & "', " &
                        "'" & datos.INMFGR & "', " &
                        datos.INDATE & ", " &
                        datos.INTRN & ", " &
                        "'" & datos.INMLOT & "', " &
                        "'" & datos.INREAS & "', " &
                        datos.INREF & ", " &
                        "'" & datos.INPROD & "', " &
                        "'" & datos.INWHSE & "', " &
                        "'" & datos.INLOT & "', " &
                        "'" & datos.INLOC & "', " &
                        datos.INQTY & ", " &
                        "'" & datos.INCBY & "', " &
                        datos.INLDT & ", " &
                        datos.INLTM & ", " &
                        datos.INCDT & ", " &
                        datos.INCTM & ")"
                    Case 1
                        fSql = "INSERT INTO BFCIM (IFUENTE, IBATCH, INID, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREAS, INREF, INPROD, INWHSE, INLOT, INLOC, INQTY, INCBY, INLDT, INLTM, INCDT, INCTM) VALUES (" &
                         "'" & datos.IFUENTE & "', " &
                        datos.IBATCH & ", " &
                        "'" & datos.INID & "', " &
                        "'" & datos.INTRAN & "', " &
                        "'" & datos.INCTNR & "', " &
                        "'" & datos.INMFGR & "', " &
                        datos.INDATE & ", " &
                        datos.INTRN & ", " &
                        "'" & datos.INMLOT & "', " &
                        "'" & datos.INREAS & "', " &
                        datos.INREF & ", " &
                        "'" & datos.INPROD & "', " &
                        "'" & datos.INWHSE & "', " &
                        "'" & datos.INLOT & "', " &
                        "'" & datos.INLOC & "', " &
                        datos.INQTY & ", " &
                        "'" & datos.INCBY & "', " &
                        datos.INLDT & ", " &
                        datos.INLTM & ", " &
                        datos.INCDT & ", " &
                        datos.INCTM & ")"
                    Case 2
                        fSql = "INSERT INTO BFCIM (IFUENTE, INID, IBATCH, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREAS, INREF, INPROD, INWHSE, INLOT, INLOC, INQTY, INCBY, INCDT, INCTM) VALUES (" &
                        "'" & datos.IFUENTE & "', " &
                        "'" & datos.INID & "', " &
                        datos.IBATCH & ", " &
                        "'" & datos.INTRAN & "', " &
                        datos.INCTNR & ", " &
                        "'" & datos.INMFGR & "', " &
                        datos.INDATE & ", " &
                        datos.INTRN & ", " &
                        "'" & datos.INMLOT & "', " &
                        "'" & datos.INREAS & "', " &
                        datos.INREF & ", " &
                        "'" & datos.INPROD & "', " &
                        "'" & datos.INWHSE & "', " &
                        "'" & datos.INLOT & "', " &
                        "'" & datos.INLOC & "', " &
                        datos.INQTY & ", " &
                        "'" & datos.INCBY & "', " &
                        datos.INCDT & ", " &
                        datos.INCTM & ")"
                End Select
                .CommandText = fSql
                .Connection = conexion
                .Transaction = trans
            End With
            Try
                sqlCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As MySqlException
                trans.Rollback()
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFCIMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla BFCIM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Catch ex As Exception
                trans.Rollback()
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFCIMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla BFCIM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Finally
                If continuarguardando = True Then
                    trans.Commit()
                Else
                    data = Nothing
                    data = New BFLOG
                    With data
                        .USUARIO = System.Net.Dns.GetHostName()
                        .OPERACION = "Guardar BFCIM"
                        .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                        .EVENTO = mensaje
                        .TXTSQL = fSql
                        .ALERT = 1
                        .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                    End With
                    GuardarBFLOGRow(data)
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Guarda BFLOG
    ''' </summary>
    ''' <param name="datos">BFLOG</param>
    ''' <returns>true si se guarda con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function GuardarBFLOGRow(ByVal datos As BFLOG) As Boolean
        trans = conexion.BeginTransaction()
        Using sqlCommand As New MySqlCommand()
            With sqlCommand
                .CommandText = "INSERT INTO BFLOG (OPERACION, USUARIO, PROGRAMA, ALERT, EVENTO, LKEY, TXTSQL) VALUES (" &
                    "'" & datos.OPERACION & "', " &
                    "'" & datos.usuario & "', " &
                    "'" & datos.programa & "', " &
                    datos.alert & ", " &
                    "'" & datos.evento & "', " &
                    "'" & datos.lkey & "', " &
                    "'" & datos.txtsql & "')"
                .Connection = conexion
                .Transaction = trans
            End With
            Try
                sqlCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As MySqlException
                trans.Rollback()
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla BFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Catch ex As Exception
                trans.Rollback()
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla BFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Finally
                If continuarguardando = True Then
                    trans.Commit()
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Guarda BFDESPA
    ''' </summary>
    ''' <param name="datos">BFDESPA</param>
    ''' <returns>true si se guarda con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function GuardarBFDESPARow(ByVal datos As BFDESPA) As Boolean
        trans = conexion.BeginTransaction()
        Using sqlCommand As New MySqlCommand()
            With sqlCommand
                .CommandText = "INSERT INTO BFDESPA (DBATCH, DPEDID, DLINEA, DPRODU, DCANTI, DBODEG, DLOCAL, DLOTE, DUSER, DFLAG) VALUES (" &
                    "'" & datos.DBATCH & "', " &
                    datos.DPEDID & ", " &
                    "'" & datos.DLINEA & "', " &
                    "'" & datos.DPRODU & "', " &
                    datos.DCANTI & ", " &
                    "'" & datos.DBODEG & "', " &
                    "'" & datos.DLOCAL & "', " &
                    "'" & datos.DLOTE & "', " &
                    "'" & datos.DUSER & "', " &
                    datos.DFLAG & ")"
                .Connection = conexion
                .Transaction = trans
            End With
            Try
                sqlCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As MySqlException
                trans.Rollback()
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFDESPAError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla BFDESPA.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Catch ex As Exception
                trans.Rollback()
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFDESPAError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla BFDESPA.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Finally
                If continuarguardando = True Then
                    trans.Commit()
                Else
                    data = Nothing
                    data = New BFLOG
                    With data
                        .USUARIO = System.Net.Dns.GetHostName()
                        .OPERACION = "Guardar BFDESPA"
                        .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                        .EVENTO = mensaje
                        .TXTSQL = fSql
                        .ALERT = 1
                        .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                    End With
                    GuardarBFLOGRow(data)
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Guarda BFPARAM
    ''' </summary>
    ''' <param name="datos">BFPARAM</param>
    ''' <param name="tipo">0- (CCID, CCCODE, CCCODE2, CCTABL),
    ''' 1- (CCID, CCCODE, CCTABL)</param>
    ''' <returns>true si se guarda con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function GuardarBFPARAMRow(ByVal datos As BFPARAM, ByVal tipo As Integer) As Boolean
        trans = conexion.BeginTransaction()
        Using sqlCommand As New MySqlCommand()
            With sqlCommand
                Select Case tipo
                    Case 0
                        fSql = " INSERT INTO BFPARAM (CCID, CCCODE, CCCODE2, CCTABL) VALUES ("
                        fSql &= "'" & datos.CCID & "', "
                        fSql &= "'" & datos.CCCODE & "', "
                        fSql &= "'" & datos.CCCODE2 & "', "
                        fSql &= "'" & datos.CCTABL & "')"
                    Case 1
                        fSql = " INSERT INTO BFPARAM (CCID, CCCODE, CCTABL) VALUES ("
                        fSql &= "'" & datos.CCID & "', "
                        fSql &= "'" & datos.CCCODE & "', "
                        fSql &= "'" & datos.CCTABL & "')"
                End Select
                .CommandText = fSql
                .Connection = conexion
                .Transaction = trans
            End With
            Try
                sqlCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As MySqlException
                trans.Rollback()
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFPARAMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla BFPARAM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Catch ex As Exception
                trans.Rollback()
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFPARAMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla BFPARAM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Finally
                If continuarguardando = True Then
                    trans.Commit()
                Else
                    data = Nothing
                    data = New BFLOG
                    With data
                        .USUARIO = System.Net.Dns.GetHostName()
                        .OPERACION = "Guardar BFPARAM"
                        .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                        .EVENTO = mensaje
                        .TXTSQL = fSql
                        .ALERT = 1
                        .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                    End With
                    GuardarBFLOGRow(data)
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Guarda RMAILX
    ''' </summary>
    ''' <param name="datos">RMAILX</param>
    ''' <param name="tipo">0- (LID, LMOD, LREF, LASUNTO, LENVIA, LPARA, LCOPIA, LCUERPO, LADJUNTOS)</param>
    ''' <returns>true si se guarda con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 27/04/2016</remarks>
    Public Function GuardarRMAILXRow(ByVal datos As RMAILX, ByVal tipo As Integer) As Boolean
        trans = conexion.BeginTransaction()
        Using sqlCommand As New MySqlCommand()
            With sqlCommand
                Select Case tipo
                    Case 0
                        fSql = " INSERT INTO RMAILX (LID, LMOD, LREF, LASUNTO, LENVIA, LPARA, LCOPIA, LCUERPO, LADJUNTOS) VALUES ("
                        fSql &= datos.LID & ", "
                        fSql &= datos.LMOD & ", "
                        fSql &= datos.LREF & ", "
                        fSql &= "'" & datos.LASUNTO & "', "
                        fSql &= "'" & datos.LENVIA & "', "
                        fSql &= "'" & datos.LPARA & "', "
                        fSql &= "'" & datos.LCOPIA & "', "
                        fSql &= "'" & datos.LCUERPO & "', "
                        fSql &= "'" & datos.LADJUNTOS & "')"
                End Select
                .CommandText = fSql
                .Connection = conexion
                .Transaction = trans
            End With
            Try
                sqlCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As MySqlException
                trans.Rollback()
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\RMAILXError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla RMAILX.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Catch ex As Exception
                trans.Rollback()
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\RMAILXError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla RMAILX.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Finally
                If continuarguardando = True Then
                    trans.Commit()
                Else
                    data = Nothing
                    data = New BFLOG
                    With data
                        .USUARIO = System.Net.Dns.GetHostName()
                        .OPERACION = "Guardar RMAILX"
                        .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                        .EVENTO = mensaje
                        .TXTSQL = fSql
                        .ALERT = 1
                        .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                    End With
                    GuardarBFLOGRow(data)
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

#End Region

End Class

Public Class BFLOG
    Public usuario As String
    Public programa As String
    Public alert As String
    Public evento As String
    Public lkey As String
    Public txtsql As String
    Public OPERACION As String
End Class

Public Class BFCIM
    Public INMLOT As String
    Public IFLAG As String
    Public IFUENTE As String
    Public IBATCH As String
    Public INLINE As String
    Public INID As String
    Public INTRAN As String
    Public INCTNR As String
    Public INMFGR As String
    Public INDATE As String
    Public INTRN As String
    Public INREAS As String
    Public INREF As String
    Public INPROD As String
    Public INWHSE As String
    Public INLOT As String
    Public INLOC As String
    Public INQTY As String
    Public INCBY As String
    Public INLDT As String
    Public INLTM As String
    Public INCDT As String
    Public INCTM As String
    Public INUMERO As String
    Public INCOST As String
    Public INFACTU As String
    Public INERROR As String
End Class

Public Class BFCIMWS
    Public INMLOT As String
    Public IFLAG As String
    Public IFUENTE As String
    Public IBATCH As String
    Public INLINE As String
    Public INID As String
    Public INTRAN As String
    Public INCTNR As String
    Public INMFGR As String
    Public INDATE As String
    Public INTRN As String
    Public INREAS As String
    Public INREF As String
    Public INPROD As String
    Public INWHSE As String
    Public INLOT As String
    Public INLOC As String
    Public INQTY As String
    Public INCBY As String
    Public INLDT As String
    Public INLTM As String
    Public INCDT As String
    Public INCTM As String
    Public INUMERO As String
    Public INCOST As String
    Public IFLAG2 As String
End Class

Public Class BFDESPA
    Public DBATCH As String
    Public DPEDID As String
    Public DLINEA As String
    Public DPRODU As String
    Public DCANTI As String
    Public DBODEG As String
    Public DLOCAL As String
    Public DLOTE As String
    Public DUSER As String
    Public DFLAG As String
End Class

Public Class BFDESPAWS
    Public DFLAG2 As String
    Public DBATCH As String
    Public DPEDID As String
    Public DLINEA As String
End Class

Public Class BFREP
    Public TQRECB As String
    Public TQREO As String
    Public TORD As String
    Public TNUMREP As String
    Public TQREQACEB As String
    Public TQREDB As String
    Public TRPODACEB As String
    Public TQREC As String
End Class

Public Class BFREPACE
    Public CENVIADO As String
    Public WRKID As String
End Class

Public Class BFREPTURRJ
    Public RENVIADO As String
    Public RWRKID As String
End Class

Public Class BFREPD
    Public TQREC As String
    Public TNUMREP As String
End Class

Public Class BFPARAM
    Public CCNOT1 As String
    Public CCMNDT As String
    Public CCNUM As String
    Public CCID As String
    Public CCCODE As String
    Public CCCODE2 As String
    Public CCCODE3 As String
    Public CCTABL As String
    Public CCENDT As String
    Public CCUDTS As String
    Public CCDESC As String
    Public CCNOT2 As String
    Public CCNOTE As String
    Public CCUDV1 As String
    Public CCUDC3 As String
End Class

Public Class RFPARAM
    Public CCNOT1 As String
    Public CCMNDT As String
    Public CCNUM As String
    Public CCID As String
    Public CCCODE As String
    Public CCCODE2 As String
    Public CCTABL As String
    Public CCENDT As String
    Public CCUDTS As String
    Public CCDESC As String
    Public CCNOT2 As String
    Public CCNOTE As String
    Public CCUDV1 As String
    Public Campo As String
    Public Valor As String
End Class

Public Class RMAILX
    Public LID As String
    Public LMOD As String
    Public LREF As String
    Public LASUNTO As String
    Public LENVIA As String
    Public LPARA As String
    Public LCOPIA As String
    Public LCUERPO As String
    Public LADJUNTOS As String
End Class