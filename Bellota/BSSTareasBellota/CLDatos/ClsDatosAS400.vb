﻿Imports IBM.Data.DB2.iSeries
Imports System.Text
Imports System.IO

Public Class ClsDatosAS400
    Public ReadOnly conexionIBM As New iDB2Connection()
    Private adapterIBM As iDB2DataAdapter = New iDB2DataAdapter()
    Private transIBM As iDB2Transaction
    Private builderIBM As iDB2CommandBuilder
    Private commandIBM As iDB2Command
    Private numFilas As Integer
    Private dt As DataTable
    Private WithEvents ds As DataSet
    Private continuarguardando As Boolean
    Private appPath As String = Path.GetFullPath(My.Application.Info.DirectoryPath & "\Log\")
    Public mensaje As String
    Private data
    Private dat As New ClsDatos
    Public fSql As String
    Public gsKeyWords As String = ""
    Public Const DB2_DATEDEC = " ( YEAR( NOW() ) * 10000 + MONTH( NOW()  ) * 100 + DAY( NOW() ) )"
    Public Const DB2_TIMEDEC0 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    Public Const DB2_TIMEDEC1 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 ) "
    Public Const DB2_TIMEDEC = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "

#Region "ACTUALIZAR"

    ''' <summary>
    ''' Actualiza la tabla BFCIM 
    ''' </summary>
    ''' <param name="datos">BFCIM</param>
    ''' <param name="tipo">0- SET IFLAG = @IFLAG WHERE INMLOT = @INMLOT,
    ''' 1- SET IFLAG = IFLAG + 2 WHERE INMLOT = @INMLOT</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 27/04/2016</remarks>
    Public Function ActualizarBFCIMRow(ByVal datos As BFCIM, ByVal tipo As Integer) As Boolean
        Select Case tipo
            Case 0
                fSql = " UPDATE BFCIM SET "
                fSql &= " IFLAG = " & datos.IFLAG & "  "
                fSql &= " WHERE INMLOT ='" & datos.INMLOT & "' "
            Case 1
                fSql = " UPDATE BFCIM SET "
                fSql &= " IFLAG = IFLAG + 2 "
                fSql &= " WHERE INMLOT ='" & datos.INMLOT & "' "
        End Select

        commandIBM = New iDB2Command
        commandIBM.Connection = conexionIBM
        Try
            commandIBM.CommandText = fSql
            commandIBM.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As iDB2Exception
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFCIMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla BFCIM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\BFCIMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla BFCIM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                data = Nothing
                data = New BFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .OPERACION = "Actualizar BFCIM AS400"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                    .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                End With
                dat.GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

#End Region

#Region "CONSULTAR"

    ''' <summary>
    ''' Consulta la tabla BFCIM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca donde IFLAG in (1,2,7)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 27/04/2016</remarks>
    Public Function ConsultarBFCIMdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT IFLAG, INMLOT, INERROR FROM BFCIM WHERE IFLAG IN( 1, 2, 7 )"
        End Select
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)
        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla FSO con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por SORD(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 26/04/2016</remarks>
    Public Function ConsultarFSOdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT SQREQ, SQFIN FROM FSO WHERE SORD =" & parametroBusqueda(0)
        End Select
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)

        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

#End Region

#Region "FUNCIONES"

    Public Sub LlamaPrograma(ByVal PGM As String, ByVal Batch As String)
        Dim llamado As Boolean = True
        Dim cmd As iDB2Command = New iDB2Command()
        cmd.Connection = conexionIBM
        fSql = String.Format("Call " & PGM & "('{0}')", Batch)
        cmd.CommandText = fSql
        cmd.CommandType = CommandType.Text
        Try
            cmd.ExecuteReader()
        Catch ex As iDB2Exception
            llamado = False
            mensaje = ex.Message
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\LlamarProgramaError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("Error al Llamar Programa.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Catch ex1 As Exception
            llamado = False
            mensaje = ex1.Message
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\LlamarProgramaError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("Error al Llamar Programa.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex1.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try
        If llamado = False Then
            data = Nothing
            data = New BFLOG
            With data
                .USUARIO = System.Net.Dns.GetHostName()
                .OPERACION = "Llamar Programa"
                .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                .EVENTO = mensaje
                .TXTSQL = cmd.CommandText
                .ALERT = 1
                .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
            End With
            dat.GuardarBFLOGRow(data)
        End If
    End Sub

    Public Function AbrirConexion(ByVal cadena As String) As Boolean
        Dim abierta As Boolean = False
        Try
            If conexionIBM.State = ConnectionState.Open Then
                abierta = True
            Else
                conexionIBM.ConnectionString = cadena
                conexionIBM.Open()
                abierta = True
            End If
        Catch ex1 As iDB2Exception
            abierta = False
            mensaje = ex1.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\AbrirConexionAS400Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("Error al abrir conexion AS400.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Catch ex As Exception
            abierta = False
            mensaje = ex.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\AbrirConexionAS400Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("Error al abrir conexion AS400.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try
        Return abierta
    End Function

    Public Function CerrarConexion() As Boolean
        Dim cerrada As Boolean = False
        Try
            conexionIBM.Close()
            cerrada = True
        Catch ex1 As iDB2Exception
            cerrada = False
            mensaje = ex1.ToString
        Catch ex As Exception
            cerrada = False
            mensaje = ex.ToString
        End Try
        Return cerrada
    End Function

#End Region

#Region "GUARDAR"

    ''' <summary>
    ''' Guarda BFCIM
    ''' </summary>
    ''' <param name="datos">BFCIM</param>
    ''' <param name="tipo">0- (IFUENTE, IBATCH, INLINE, INID, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREAS, INREF, INPROD, INWHSE, INLOT, INLOC, INQTY, INCBY, INLDT, INLTM, INCDT, INCTM),
    ''' 1- (IFUENTE, IBATCH, INID, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREAS, INREF, INPROD, INWHSE, INLOT, INLOC, INQTY, INCBY, INLDT, INLTM, INCDT, INCTM)</param>
    ''' <returns>true si se guarda con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function GuardarBFCIMRow(ByVal datos As BFCIM, ByVal tipo As Integer) As Boolean
        Using ibmCommand As New iDB2Command()
            With ibmCommand
                Select Case tipo
                    Case 0
                        fSql = "INSERT INTO BFCIM (IFUENTE, IBATCH, INLINE, INID, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREAS, INREF, INPROD, INWHSE, INLOT, INLOC, INQTY, INCBY, INLDT, INLTM, INCDT, INCTM) VALUES (" &
                         "'" & datos.IFUENTE & "', " &
                        datos.IBATCH & ", " &
                        "'" & datos.INLINE & "', " &
                        "'" & datos.INID & "', " &
                        "'" & datos.INTRAN & "', " &
                        datos.INCTNR & ", " &
                        "'" & datos.INMFGR & "', " &
                        datos.INDATE & ", " &
                        datos.INTRN & ", " &
                        "'" & datos.INMLOT & "', " &
                        "'" & datos.INREAS & "', " &
                        datos.INREF & ", " &
                        "'" & datos.INPROD & "', " &
                        "'" & datos.INWHSE & "', " &
                        "'" & datos.INLOT & "', " &
                        "'" & datos.INLOC & "', " &
                        datos.INQTY & ", " &
                        "'" & datos.INCBY & "', " &
                        datos.INLDT & ", " &
                        datos.INLTM & ", " &
                        datos.INCDT & ", " &
                        datos.INCTM & ")"
                    Case 1
                        fSql = "INSERT INTO BFCIM (IFUENTE, IBATCH, INID, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREAS, INREF, INPROD, INWHSE, INLOT, INLOC, INQTY, INCBY, INLDT, INLTM, INCDT, INCTM) VALUES (" &
                         "'" & datos.IFUENTE & "', " &
                        datos.IBATCH & ", " &
                        "'" & datos.INID & "', " &
                        "'" & datos.INTRAN & "', " &
                        "'" & datos.INCTNR & "', " &
                        "'" & datos.INMFGR & "', " &
                        datos.INDATE & ", " &
                        datos.INTRN & ", " &
                        "'" & datos.INMLOT & "', " &
                        "'" & datos.INREAS & "', " &
                        datos.INREF & ", " &
                        "'" & datos.INPROD & "', " &
                        "'" & datos.INWHSE & "', " &
                        "'" & datos.INLOT & "', " &
                        "'" & datos.INLOC & "', " &
                        datos.INQTY & ", " &
                        "'" & datos.INCBY & "', " &
                        datos.INLDT & ", " &
                        datos.INLTM & ", " &
                        datos.INCDT & ", " &
                        datos.INCTM & ")"
                    Case 2
                        fSql = "INSERT INTO BFCIM (IFUENTE, IBATCH, INID, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREF, INPROD, INLOT, INLOC, INQTY, INCBY, INLDT, INLTM, INCDT, INCTM, INUMERO, INCOST, INWHSE, INFACTU) VALUES (" &
                        "'" & datos.IFUENTE & "', " &
                        datos.IBATCH & ", " &
                        "'" & datos.INID & "', " &
                        "'" & datos.INTRAN & "', " &
                        "'" & datos.INCTNR & "', " &
                        "'" & datos.INMFGR & "', " &
                        datos.INDATE & ", " &
                        datos.INTRN & ", " &
                        "'" & datos.INMLOT & "', " &
                        datos.INREF & ", " &
                        "'" & datos.INPROD & "', " &
                        "'" & datos.INLOT & "', " &
                        "'" & datos.INLOC & "', " &
                        datos.INQTY & ", " &
                        "'" & datos.INCBY & "', " &
                        datos.INLDT & ", " &
                        datos.INLTM & ", " &
                        datos.INCDT & ", " &
                        datos.INCTM & ", " &
                        datos.INUMERO & ", " &
                        datos.INCOST & ", " &
                        "'" & datos.INWHSE & "', " &
                        datos.INFACTU & ")"
                    Case 3
                        fSql = "INSERT INTO BFCIM (IFUENTE, INID, IBATCH, INTRAN, INCTNR, INMFGR, INDATE, INTRN, INMLOT, INREAS, INREF, INPROD, INWHSE, INLOT, INLOC, INQTY, INCBY, INCDT, INCTM) VALUES (" &
                        "'" & datos.IFUENTE & "', " &
                        "'" & datos.INID & "', " &
                        datos.IBATCH & ", " &
                        "'" & datos.INTRAN & "', " &
                        datos.INCTNR & ", " &
                        "'" & datos.INMFGR & "', " &
                        datos.INDATE & ", " &
                        datos.INTRN & ", " &
                        "'" & datos.INMLOT & "', " &
                        "'" & datos.INREAS & "', " &
                        datos.INREF & ", " &
                        "'" & datos.INPROD & "', " &
                        "'" & datos.INWHSE & "', " &
                        "'" & datos.INLOT & "', " &
                        "'" & datos.INLOC & "', " &
                        datos.INQTY & ", " &
                        "'" & datos.INCBY & "', " &
                        datos.INCDT & ", " &
                        datos.INCTM & ")"
                End Select
                .CommandText = fSql
                .Connection = conexionIBM
            End With
            Try
                ibmCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As iDB2Exception
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFCIMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla BFCIM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Catch ex As Exception
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFCIMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla BFCIM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Finally
                If continuarguardando = False Then
                    data = Nothing
                    data = New BFLOG
                    With data
                        .USUARIO = System.Net.Dns.GetHostName()
                        .OPERACION = "Guardar BFCIM AS400"
                        .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                        .EVENTO = mensaje
                        .TXTSQL = fSql
                        .ALERT = 1
                        .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                    End With
                    dat.GuardarBFLOGRow(data)
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Guarda BFDESPA
    ''' </summary>
    ''' <param name="datos">BFDESPA</param>
    ''' <returns>true si se guarda con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function GuardarBFDESPARow(ByVal datos As BFDESPA) As Boolean
        Using ibmCommand As New iDB2Command()
            With ibmCommand
                .CommandText = "INSERT INTO BFDESPA (DBATCH, DPEDID, DLINEA, DPRODU, DCANTI, DBODEG, DLOCAL, DLOTE, DUSER, DFLAG) VALUES (" &
                    "'" & datos.DBATCH & "', " &
                    datos.DPEDID & ", " &
                    "'" & datos.DLINEA & "', " &
                    "'" & datos.DPRODU & "', " &
                    datos.DCANTI & ", " &
                    "'" & datos.DBODEG & "', " &
                    "'" & datos.DLOCAL & "', " &
                    "'" & datos.DLOTE & "', " &
                    "'" & datos.DUSER & "', " &
                    datos.DFLAG & ")"
                .Connection = conexionIBM
            End With
            Try
                ibmCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As iDB2Exception
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFDESPAError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla BFDESPA.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Catch ex As Exception
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFDESPAError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla BFDESPA.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Finally
                If continuarguardando = False Then
                    data = Nothing
                    data = New BFLOG
                    With data
                        .USUARIO = System.Net.Dns.GetHostName()
                        .OPERACION = "Guardar BFDESPA AS400"
                        .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                        .EVENTO = mensaje
                        .TXTSQL = fSql
                        .ALERT = 1
                        .LKEY = Left(gsKeyWords.Trim.ToUpper, 30)
                    End With
                    dat.GuardarBFLOGRow(data)
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

#End Region

End Class
