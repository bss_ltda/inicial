﻿Option Explicit On

Imports ADODB

Public Class clsConexion
    Public DB As New ADODB.Connection
    Public gsQueDB As String
    Public gsDatasource As String
    Public gsLib As String
    Public gsConnstring As String
    Public gsUser As String
    Public gsAppName As String
    Public gsAppPath
    Public gsAppVersion As String
    Public gsKeyWords As String = ""

    Public ModuloActual As String = ""
    Public lastError As String = ""
    Public bDateTimeToChar As Boolean = False
    Dim regA As Double

    Public Const DB2_DATEDEC = " ( YEAR( NOW() ) * 10000 + MONTH( NOW()  ) * 100 + DAY( NOW() ) )"
    Public Const DB2_TIMEDEC0 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    Public Const DB2_TIMEDEC1 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 ) "

    Function Conexion() As Boolean
        Dim gsProvider As String = "IBMDA400"
        Dim Resultado As Boolean
        Dim rs As New ADODB.Recordset

        gsKeyWords = ""
        gsConnstring = "Provider=" & gsProvider & ";Data Source=" & gsDatasource & ";"
        gsConnstring = gsConnstring & "Persist Security Info=False;Default Collection=" & gsLib & ";"
        gsConnstring = gsConnstring & "Password=LXAPL;User ID=APLLX;"

        DB = New ADODB.Connection
        DB.Open(gsConnstring)

        Dim gsPass As String = getLXParam("LXPASS")
        gsUser = getLXParam("LXUSER")

        If gsUser <> "" And gsPass <> "" Then
            gsConnstring = "Provider=" & gsProvider & ";Data Source=" & gsDatasource & ";"
            gsConnstring = gsConnstring & "Persist Security Info=False;Default Collection=" & gsLib & ";"
            gsConnstring = gsConnstring & "Password=" & gsPass & ";User ID=" & gsUser & ";"
            gsConnstring = gsConnstring & "Force Translate=0;"
            If bDateTimeToChar Then
                gsConnstring &= "Convert Date Time To Char=FALSE;"
            End If
            DB.Close()
            DB.Open(gsConnstring)
            Resultado = True
        Else
            Resultado = False
        End If

        Return Resultado

    End Function

    Function abrirConexionMySql() As String
        Dim gsConnMySql As String = ""
        Dim resultado As String = ""
        gsConnstring = ""
        gsQueDB = "<br /><strong>MySql:</strong> "
        Try
            If DB.State = 0 Then
                gsConnMySql = "Provider=MSDASQL.1;Password={PASSWORD};Persist Security Info=True;User ID={USER};Data Source={ODC};Initial Catalog={DATABASE}"

                gsConnMySql = "DRIVER=MySQL ODBC 5.3 ANSI Driver;UID={USER};PWD={PASSWORD};SERVER={SERVER};DATABASE={DATABASE};DBQ={ODBC}"

                gsConnMySql = gsConnMySql.Replace("{USER}", paramWebConfig(ubicWebConfig, "MYSQL_USER"))
                gsConnMySql = gsConnMySql.Replace("{PASSWORD}", paramWebConfig(ubicWebConfig, "MYSQL_PASS"))
                gsConnMySql = gsConnMySql.Replace("{SERVER}", paramWebConfig(ubicWebConfig, "MYSQL_SERVER"))
                gsConnMySql = gsConnMySql.Replace("{DATABASE}", paramWebConfig(ubicWebConfig, "MYSQL_LIB"))
                gsConnMySql = gsConnMySql.Replace("{ODBC}", paramWebConfig(ubicWebConfig, "MYSQL_ODBC"))
                DB.Open(gsConnMySql)
            End If
        Catch ex As Exception
            Return gsConnMySql & "----" & ex.Message.ToString
        End Try
        Return resultado
    End Function

    Function abrirConexionAS400() As String
        Dim ConnectionString As String = ""
        Dim resultado As String = ""

        gsConnstring = ""
        gsQueDB = "<br /><strong>AS400:</strong> "

        Try
            If DB.State = 0 Then
                gsConnstring = "Provider=IBMDA400;Data Source=" & paramWebConfig400("AS400_SERVER") & ";"
                gsConnstring = gsConnstring & "Persist Security Info=False;Library List=PP61BPCSO,CP61BPCSF,CP61BPCESP,PP61BPCPTF,PP61CPEUSR,CP61BPCUSR,CP61BPCUSF,PP61BAMUSF,M6168345C;"
                gsConnstring = gsConnstring & "Password=" & paramWebConfig400("AS400_PASS") & ";User ID=" & paramWebConfig400("AS400_USER") & ";"
                gsConnstring = gsConnstring & "Force Translate=0;Naming Convention=1;"
                'DBMySql.WrtSqlError("Conexion", gsConnstring)
                DB.Open(gsConnstring)
                'DBMySql.WrtSqlError("Conexion", gsConnstring)

            End If
        Catch ex As Exception
            DBMySql.WrtSqlError(lastSQL, ex.Message & "<br />" & ex.StackTrace.Replace(" en ", "<br />"))
            Return ex.ToString
        End Try
        Return resultado
    End Function

    Public Sub DropTable(Table As String)
        Try
            DB.Execute("DROP TABLE " & Table)
        Catch ex As Exception

        End Try
    End Sub

    Public Function ExecuteSQL(ByVal sql As String) As ADODB.Recordset

        sql = CStr(sql)
        lastSQL = gsQueDB & sql
        ExecuteSQL = DB.Execute(sql, regA)

    End Function

    Public Function OpenRS(ByRef rs As ADODB.Recordset, fSql As String, CursorType As CursorTypeEnum, LockType As LockTypeEnum) As Boolean
        lastSQL = gsQueDB & fSql
        rs.Open(fSql, DB, CursorType, LockType)
        Return Not rs.EOF

    End Function

    Public Function OpenRS(ByRef rs As ADODB.Recordset, fSql As String) As Boolean
        lastSQL = gsQueDB & fSql
        rs.Open(fSql, DB)
        Return Not rs.EOF

    End Function

    Public Function RS(ByRef rsX As ADODB.Recordset, fSql As String) As Boolean

        lastSQL = gsQueDB & fSql
        rsX.Open(fSql, DB)
        Return Not rsX.EOF

    End Function

    Sub WrtSqlError(sql As String, ByVal Descrip As String)
        Dim conn As New ADODB.Connection
        Dim fSql As String
        Dim vSql As String
        Dim aApl() As String
        Dim sApp As String = "VB.NET"

        Dim gsConnMySql As String = "DRIVER=MySQL ODBC 5.3 ANSI Driver;UID={USER};PWD={PASSWORD};SERVER={SERVER};DATABASE={DATABASE};DBQ={ODBC}"
        gsConnMySql = gsConnMySql.Replace("{USER}", paramWebConfig(ubicWebConfig, "MYSQL_USER"))
        gsConnMySql = gsConnMySql.Replace("{PASSWORD}", paramWebConfig(ubicWebConfig, "MYSQL_PASS"))
        gsConnMySql = gsConnMySql.Replace("{SERVER}", paramWebConfig(ubicWebConfig, "MYSQL_SERVER"))
        gsConnMySql = gsConnMySql.Replace("{DATABASE}", paramWebConfig(ubicWebConfig, "MYSQL_LIB"))
        gsConnMySql = gsConnMySql.Replace("{ODBC}", paramWebConfig(ubicWebConfig, "MYSQL_ODBC"))

        If gsKeyWords.Trim <> "" Then
            Descrip = gsKeyWords & "<br>" & Descrip
        End If
        Descrip = Left(Descrip, 20000)

        Try
            aApl = Split(System.Reflection.Assembly.GetExecutingAssembly.FullName, ", ")
            If UBound(aApl) > 0 Then
                sApp = aApl(0) & IIf(ModuloActual <> "", "." & ModuloActual, "")
            End If

            conn.Open(gsConnMySql)
            fSql = " INSERT INTO BFLOG( "
            vSql = " VALUES ( "
            fSql = fSql & " USUARIO   , " : vSql = vSql & "'" & System.Net.Dns.GetHostName() & "', "        '//502A
            fSql = fSql & " PROGRAMA  , " : vSql = vSql & "'" & "" & sApp & "', "                           '//502A
            fSql = fSql & " ALERT     , " : vSql = vSql & "1, "                                             '//1P0
            fSql = fSql & " EVENTO    , " : vSql = vSql & "'" & Replace(Descrip, "'", "''") & "', "         '//20002A
            fSql = fSql & " LKEY      , " : vSql = vSql & "'" & Left(gsKeyWords.Trim.ToUpper, 30) & "', "
            fSql = fSql & " TXTSQL    ) " : vSql = vSql & "'" & Replace(sql, "'", "''") & "' ) "            '//5002A
            conn.Execute(fSql & vSql)
            conn.Close()
        Catch ex As Exception

        End Try

    End Sub

    Public Function getLXParam(prm As String) As String
        Return getLXParam(prm, "")
    End Function


    Public Function getLXParam(prm As String, Campo As String) As String
        Dim rs As New ADODB.Recordset
        Dim resultado As String = ""
        Dim fSql As String

        If Campo = "" Then
            Campo = "CCDESC"
        End If
        fSql = "SELECT " & Campo & " AS DATO FROM RFPARAM WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER('" & prm & "')"
        lastSQL = gsQueDB & fSql
        rs.Open(fSql, Me.DB)
        If Not rs.EOF Then
            resultado = rs("DATO").Value
        End If
        rs.Close()

        Return resultado.Trim

    End Function

    Public Function getLXParam(prm As String, codigo As String, Campo As String) As String
        Dim rs As New ADODB.Recordset
        Dim resultado As String = ""
        Dim fSql As String

        If Campo = "" Then
            Campo = "CCDESC"
        End If
        fSql = "SELECT " & Campo & " AS DATO "
        fSql &= " FROM RFPARAM "
        fSql &= " WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER('" & codigo & "')"
        fSql &= " AND CCTABL='LXLONG' AND UPPER(CCCODE2) = UPPER('" & prm & "')"
        lastSQL = gsQueDB & fSql

        rs.Open(fSql, DB)
        If Not rs.EOF Then
            resultado = rs("DATO").Value
        End If
        rs.Close()
        Return resultado

    End Function

    Public Sub setLXParam(codigo As String, Campo As String, Valor As String)
        Dim fSql As String

        If Campo = "" Then
            Campo = "CCDESC"
        End If
        fSql = "UPDATE RFPARAM SET  " & Campo & " = '" & Valor & "'"
        fSql &= " FROM RFPARAM "
        fSql &= " WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER('" & codigo & "')"
        ExecuteSQL(fSql)

    End Sub

    Public Sub setLXParamNum(codigo As String, Campo As String, Valor As String)
        Dim fSql As String

        If Campo = "" Then
            Campo = "CCDESC"
        End If
        fSql = "UPDATE RFPARAM SET  " & Campo & " = " & CDbl(Valor)
        fSql &= " WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER('" & codigo & "')"
        ExecuteSQL(fSql)

    End Sub


    Public Function regActualizados() As Long
        Return regA
    End Function

    Public Function Login(Usuario As String, Password As String) As Boolean
        Dim rs As New ADODB.Recordset
        Dim resultado As Boolean = False
        Dim fSql As String = "SELECT UUSR FROM RCAU WHERE UUSR='" & Usuario & "' AND UPASS='" & Password & "'"
        lastSQL = gsQueDB & fSql
        rs.Open(fSql, DB)
        If Not rs.EOF Then
            resultado = True
        End If
        rs.Close()
        Return resultado

    End Function


    Public Function CalcConsec(ByVal sID As String, ByVal TopeMax As String) As String
        Dim rs As New ADODB.Recordset
        Dim fmt As String
        Dim result As String

        fSql = " LOCK TABLES sequence_data LOW_PRIORITY WRITE "
        ExecuteSQL(fSql)

        rs.Open("SELECT nextval('" & sID & "')", DB)
        result = rs(0).Value
        rs.Close()

        fSql = "SELECT sequence_max_value FROM SEQUENCE_DATA WHERE sequence_name = '" & sID & "'"
        rs.Open(fSql, DB)
        fmt = Replace(rs(0).Value, "9", "0")
        result = Right(fmt & result, Len(fmt))
        rs.Close()

        fSql = " UNLOCK TABLES "
        ExecuteSQL(fSql)
        Return result

    End Function

    Public Function CheckVer() As Boolean
        Dim rs As New ADODB.Recordset
        Dim lVer As String
        Dim msg As String
        Dim NomApp As String
        Dim App As clsApp = New clsApp(System.Reflection.Assembly.GetExecutingAssembly)
        Dim Resultado As Boolean = True
        Dim fSql As String

        gsAppPath = App.Path
        gsAppName = App.EXEName
        gsAppVersion = App.Major.ToString & "." & App.Minor.ToString & "." & App.Revision.ToString

        lVer = "No hay registro"

        fSql = "SELECT CCSDSC, CCUDC1, CCNOT2, CCDESC  FROM ZCCL01 WHERE CCTABL='RFVBVER' AND UPPER(CCCODE) = '" & gsAppName.ToUpper & "'"
        lastSQL = gsQueDB & lastSQL
        rs.Open(fSql, DB)
        If Not (rs.EOF) Then
            lVer = Trim(rs("CCSDSC").Value)
            NomApp = rs("CCDESC").Value
            If InStr(lVer, "*NOCHK") > 0 Then
                Resultado = True
            ElseIf InStr(lVer, "(" & gsAppVersion & ")") > 0 Then
                Resultado = True
            End If
            If Resultado And rs("CCUDC1").Value = 0 Then
                msg = "La aplicacion " & NomApp & " no se puede usar en este momento." & vbCr
                msg = msg & "Razon: " & vbCr
                msg = msg & "=========================================" & vbCr
                If Trim(rs("CCNOT2").Value) = "" Then
                    msg = msg & "Aplicacion en Mantenimiento." & vbCr
                Else
                    msg = msg & rs("CCNOT2").Value & vbCr
                End If
                msg = msg & "=========================================" & vbCr
                rs.Close()
                Return False
            End If
            rs.Close()
        End If

        If Not Resultado Then
            msg = "Aplicacion " & App.EXEName.ToUpper & vbCr & _
                   "=========================================" & vbCr & _
                   "Versión incorrecta del programa." & vbCr & _
                   "Versión Registrada: " & lVer & vbCr & _
                   "Versión Actual: " & "(" & gsAppVersion & ")"
            lastError = msg
        End If

        Return Resultado

    End Function

    Sub Close()
        If DB.State = 1 Then
            DB.Close()
        End If
    End Sub

    Function Abierta() As Boolean
        Return DB.State = 1
    End Function

End Class
