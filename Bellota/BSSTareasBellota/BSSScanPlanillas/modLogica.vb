﻿Imports MySql.Data.MySqlClient
Imports System.Globalization
Imports CLDatos
Imports System.Text
Imports System.IO

Module modLogica
    Dim wsNum As String
    Dim wsResult As String
    Private data
    Private datos As New ClsDatos
    Private datosas400 As New ClsDatosAS400
    Private cadenaAS400 As String

    Sub revisaTareas()
        If My.Settings.Pruebas = "No" Then
            Try
                EjecutaProcesoNEW()
            Catch ex As Exception
                datos.WrtSqlError(datos.fSql, ex.Message & "<br />" & ex.StackTrace.Replace(" en ", "<br />"))
            End Try
        Else
            EjecutaProcesoNEW()
        End If
    End Sub

    Sub EjecutaProcesoNEW()
        Dim parametro(0) As String
        Dim exe As String = ""

        Console.WriteLine("Abriendo conexión...")
        If Not datos.AbrirConexion() Then
            Console.WriteLine("Error Abriendo conexión " & datos.mensaje & vbCrLf & datos.Cadena)
            Exit Sub
        End If

        parametro(0) = "PGMRUTINAS"
        Dim pgm As String = """" & datos.ConsultarBFPARAMst(parametro, 0) & """"
        Dim rut As String = ""

        If registroWS("Inicio", "") Then
            rut = "SINCRONIZAESTADO"
            exe = pgm & " RUTINA=" & rut
            ejecutarut(rut, exe)

            rut = "ACTUALIZACANTORDEN"
            exe = pgm & " RUTINA=ACTUALIZACANTORDEN"
            ejecutarut(rut, exe)

            rut = "TRANSACCIONACERO"
            exe = pgm & " RUTINA=TRANSACCIONACERO"
            ejecutarut(rut, exe)

            rut = "TRANSACCIONRJ"
            exe = pgm & " RUTINA=TRANSACCIONRJ"
            ejecutarut(rut, exe)

            rut = "TRASLADOS"
            exe = pgm & " RUTINA=TRASLADOS"
            ejecutarut(rut, exe)

            rut = "TRANSACCIONINV"
            exe = pgm & " RUTINA=TRANSACCIONINV"
            ejecutarut(rut, exe)

            rut = "ASIGNACION"
            exe = pgm & " RUTINA=ASIGNACION"
            ejecutarut(rut, exe)

            rut = "ACTUALIZAPR"
            exe = pgm & " RUTINA=ACTUALIZAPR"
            ejecutarut(rut, exe)
            Console.WriteLine("..... Fin .....")
        End If
        datos.CerrarConexion()
    End Sub

    Private Sub ejecutarut(ByVal rut As String, ByVal exe As String)
        Console.WriteLine(rut)
        Try
            data = Nothing
            data = New BFPARAM
            With data
                .CCNOTE = exe.Replace("\", "\\")
                .CCTABL = "TareasBellota"
                .CCUDC3 = 0
                .CCCODE3 = rut
            End With
            datos.ActualizarBFPARAMRow(data, 7)
            Shell(exe, AppWinStyle.Hide, False, -1)
            data.CCUDC3 = 1
            datos.ActualizarBFPARAMRow(data, 7)

        Catch ex As Exception
            Console.WriteLine("Error Ejecutando: " & exe & vbCrLf & ex.Message)
            log(ex.Message, rut, exe)
        End Try
    End Sub


    Sub EjecutaProceso()
        Dim pgm As String = "C:\Program Files (x86)\BSSLTDA\BSSRutinas\BSSRutinas.exe"
        Dim exe As String = ""
        Dim resultado As String = ""
        System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("es-CO")
        System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator = "."
        System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyGroupSeparator = ","
        System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator = "."
        System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberGroupSeparator = ","

        Console.WriteLine("Abriendo conexión...")
        If Not datos.AbrirConexion() Then
            Console.WriteLine("Error Abriendo conexión " & datos.mensaje & vbCrLf & datos.Cadena)
            Exit Sub
        End If

        cadenaAS400 = datos.CadenaConexionAS400()
        Console.WriteLine("Cadena conexión AS400 " & cadenaAS400)

        Console.WriteLine("Abriendo conexión AS400...")
        If Not datosas400.AbrirConexion(cadenaAS400) Then
            Console.WriteLine("Error Abriendo conexión AS400 " & datosas400.mensaje & vbCrLf & cadenaAS400)
            Exit Sub
        End If

        If registroWS("Inicio", "") Then
            registroWS("Operacion", "Sincroniza Estado")
            Console.WriteLine(" ")
            Console.WriteLine("actualizaBFCIM...")
            Console.WriteLine(" ")
            actualizaBFCIM()

            registroWS("Operacion", "Actualiza Cant Orden")
            Console.WriteLine(" ")
            Console.WriteLine("actualizacionPR...")
            Console.WriteLine(" ")
            actualizacionPR()
            Console.WriteLine(" ")
            Console.WriteLine("actualizacionPRD...")
            Console.WriteLine(" ")
            actualizacionPRD()
            registroWS("Operacion", "Transaccion Acero")
            Console.WriteLine(" ")
            Console.WriteLine("TransaccionAcero...")
            Console.WriteLine(" ")
            TransaccionAcero()
            registroWS("Operacion", "Transaccion R Acero")
            Console.WriteLine(" ")
            Console.WriteLine("TransaccionRAcero...")
            Console.WriteLine(" ")
            TransaccionRAcero()
            registroWS("Operacion", "Transaccion RJ")
            Console.WriteLine(" ")
            Console.WriteLine("TransaccionRJ...")
            Console.WriteLine(" ")
            TransaccionRJ()
            registroWS("Operacion", "Traslados")
            Console.WriteLine(" ")
            Console.WriteLine("ActualizacionBFCIM...")
            Console.WriteLine(" ")
            ActualizacionBFCIM()
            registroWS("Operacion", "Transacciones")
            Console.WriteLine(" ")
            Console.WriteLine("ActualizacionBFCIMTransacInv...")
            Console.WriteLine(" ")
            ActualizacionBFCIMTransacInv()
            'registroWS("Operacion", "Asignacion")
            'Para los webservices de batch
            Console.WriteLine(" ")
            Console.WriteLine("ActualizacionBFDESPA...")
            Console.WriteLine(" ")
            ActualizacionBFDESPA()
            'Revisa para ejecutar cada hora
            Console.WriteLine(" ")
            If registroWS("Actualiza PR", "Actualiza PR") Then
                Console.WriteLine(" ")
                Console.WriteLine("actualizaPR...")
                Console.WriteLine(" ")
                actualizaPR()
                Console.WriteLine(" ")
                Console.WriteLine("actualizaPRD...")
                Console.WriteLine(" ")
                actualizaPRD()
            End If
            registroWS("Fin", "")
            Console.WriteLine("..... Fin .....")
        End If
        datos.CerrarConexion()
        datosas400.CerrarConexion()

    End Sub

    Private Sub log(ByVal menlog As String, ByVal proceso As String, ByVal exellamado As String)
        Dim sb As New StringBuilder()
        Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\" & proceso & "Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
        sb.AppendLine("Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & menlog & vbCrLf & exellamado)
        sb.AppendLine(Now().ToString)
        sw.WriteLine(sb.ToString())
        sw.Close()

        data = Nothing
        data = New BFLOG
        With data
            .USUARIO = System.Net.Dns.GetHostName()
            .OPERACION = proceso
            .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
            .EVENTO = menlog
            .TXTSQL = exellamado
            .ALERT = 1
            .LKEY = Left(datos.gsKeyWords.Trim.ToUpper, 30)
        End With
        datos.GuardarBFLOGRow(data)
    End Sub

    'SINCRONIZA BFCIM MySql/AS400
    Sub actualizaBFCIM()
        Dim r As Integer = 0
        Dim parametro(0) As String
        Dim dtBFCIM As DataTable
        Dim continuar As Boolean

        If cadenaAS400 <> "" Then
            Console.WriteLine("Consultando BFCIM AS400...")
            'parametro(0) = cadenaAS400
            dtBFCIM = datosas400.ConsultarBFCIMdt(parametro, 0)
            If dtBFCIM IsNot Nothing Then
                Console.WriteLine("Recorriendo BFCIM AS400...")
                For Each row As DataRow In dtBFCIM.Rows

                    data = Nothing
                    data = New BFCIM
                    With data
                        .IFLAG = row("IFLAG")
                        .INMLOT = row("INMLOT")
                        .INERROR = row("INERROR")
                    End With
                    Console.WriteLine("Actualizando BFCIM MySQL...")
                    continuar = datos.ActualizarBFCIMRow(data, 0)
                    If continuar = True Then
                        Console.WriteLine("BFCIM MySQL Actualizado")
                        data = Nothing
                        data = New BFCIM
                        With data
                            .INMLOT = row("INMLOT")
                        End With
                        Console.WriteLine("Actualizando BFCIM AS400...")
                        continuar = datosas400.ActualizarBFCIMRow(data, 1)
                        If continuar = False Then
                            Console.WriteLine("Error Actualizando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                            'ERROR AL ACTUALIZAR
                        Else
                            Console.WriteLine("BFCIM AS400 Actualizado")
                        End If
                    Else
                        Console.WriteLine("Error Actualizando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                        'ERROR DE ACTUALIZADO
                    End If
                    r += 1
                Next
            End If
        End If
        wsResult = "Registros actualizados: " & r
        Console.WriteLine(wsResult)

        ''// Cambio | 2016-02-23 10:01:43 | para webservice nuevos
        '' '' ''fSql = "SELECT DFLAG, DBATCH, DPEDID, DLINEA, DERROR, DDESER FROM BFDESPA WHERE DFLAG IN( 1, 2, 7 ) "
        '' '' ''DB.OpenRS(rs, fSql)
        '' '' ''While Not rs.EOF
        '' '' ''    fSql = " UPDATE BFDESPA SET "
        '' '' ''    fSql &= " DFLAG = " & rs("DFLAG").Value & ",  "
        '' '' ''    fSql &= " DERROR = " & rs("DERROR").Value & ",  "
        '' '' ''    fSql &= " DDESER = '" & rs("DDESER").Value & "'  "
        '' '' ''    fSql &= " WHERE DBATCH =" & rs("DBATCH").Value
        '' '' ''    fSql &= " AND DPEDID = " & rs("DPEDID").Value
        '' '' ''    fSql &= " AND DLINEA =" & rs("DLINEA").Value
        '' '' ''    DBMySql.ExecuteSQL(fSql)

        '' '' ''    fSql = " UPDATE BFDESPA SET "
        '' '' ''    fSql &= " DFLAG = DFLAG + 2 "
        '' '' ''    fSql &= " WHERE DBATCH =" & rs("DBATCH").Value
        '' '' ''    fSql &= " AND DPEDID =" & rs("DPEDID").Value
        '' '' ''    fSql &= " AND DLINEA =" & rs("DLINEA").Value
        '' '' ''    DB.ExecuteSQL(fSql)
        '' '' ''    r += 1
        '' '' ''    rs.MoveNext()
        '' '' ''End While
        '' '' ''rs.Close()
    End Sub

    Sub ActualizacionBFCIM()
        Dim parametro(0) As String
        Dim dtBFCIMWS As DataTable
        Dim continuar As Boolean

        Console.WriteLine("Consultando BFCIMWS MySQL...")
        parametro(0) = "0"
        dtBFCIMWS = datos.ConsultarBFCIMWSdt(parametro, 0)
        If dtBFCIMWS IsNot Nothing Then
            Console.WriteLine("Recorriendo BFCIMWS MySQL...")
            For Each row As DataRow In dtBFCIMWS.Rows
                data = Nothing
                data = New BFCIM
                With data
                    .IFUENTE = row("IFUENTE")               '//15A
                    .IBATCH = row("IBATCH")                 '//8
                    .INLINE = row("INLINE")                 '//2A    Record Identifier
                    .INID = row("INID")                     '//2A    Record Identifier
                    .INTRAN = row("INTRAN")                 '//2A    Transaction Type
                    .INCTNR = row("INCTNR")                 '//2A    Transaction Type
                    .INMFGR = row("INMFGR")                 '//2A    Transaction Type
                    .INDATE = row("INDATE")                 '//8P0   Date		
                    .INTRN = row("INTRN")                   '//5P0   INTRN
                    .INMLOT = row("INMLOT")                 '//5P0   INTRN
                    .INREAS = row("INREAS")                 '//5P0   INTRN
                    .INREF = row("INREF")                   '//8P0   Date
                    .INPROD = row("INPROD")                 '//15A   Item Number
                    .INWHSE = row("INWHSE")                 '//2A    Warehouse
                    .INLOT = row("INLOT")                   '//10A   Lot Number
                    .INLOC = row("INLOC")                   '//6A    Location Code
                    .INQTY = row("INQTY")                   '//10A   Creted by
                    .INCBY = row("INCBY")                   '//10A   Creted by
                    .INLDT = row("INLDT")                   '//8P0
                    .INLTM = row("INLTM")                   '//6P0
                    .INCDT = row("INCDT")                   '//8P0   Date Created
                    .INCTM = row("INCTM")                   '//6P0   Time Created
                End With
                Console.WriteLine("Guardando BFCIM AS400...")
                continuar = datosas400.GuardarBFCIMRow(data, 0)
                If continuar = False Then
                    Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                    'ERROR DE GUARDADO
                Else
                    Console.WriteLine("BFCIM AS400 Guardado")
                    Console.WriteLine("Guardando BFCIM MySQL...")
                    continuar = datos.GuardarBFCIMRow(data, 0)
                    If continuar = False Then
                        Console.WriteLine("Error Guardando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                        'ERROR DE GUARDADO
                    Else
                        Console.WriteLine("BFCIM MySQL Guardado")
                        data = Nothing
                        data = New BFCIMWS
                        With data
                            .IFLAG2 = 1
                            .IBATCH = row("IBATCH")
                            .INTRN = row("INTRN")
                        End With
                        Console.WriteLine("Actualizando BFCIMWS MySQL...")
                        continuar = datos.ActualizarBFCIMWSRow(data, 0)
                        If continuar = False Then
                            Console.WriteLine("Error Actualizando BFCIMWS MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                            'ERROR DE ACTUALIZADO
                        Else
                            Console.WriteLine("BFCIMWS MySQL Actualizado")
                        End If
                    End If
                End If
            Next
            parametro(0) = "1"
            Console.WriteLine("Consultando BFCIMWS MySQL...")
            dtBFCIMWS = datos.ConsultarBFCIMWSdt(parametro, 1)
            If dtBFCIMWS IsNot Nothing Then
                Console.WriteLine("Recorriendo BFCIMWS MySQL...")
                For Each row As DataRow In dtBFCIMWS.Rows
                    Console.WriteLine("Llamando Programa BCTRAINVB AS400... " & datos.Ceros(row("IBATCH"), 8) & "', 'DESP")
                    datosas400.LlamaPrograma("BCTRAINVB", datos.Ceros(row("IBATCH"), 8) & "', 'DESP")
                    'fSql = "{{ CALL PGM(" & paramWebConfig400("AS400_PGM") & "/BCTRAINVB) PARM('" & Ceros(row("IBATCH"), 8) & "' 'DESP' ) }}"
                    'DB.ExecuteSQL(fSql)
                    data = Nothing
                    data = New BFCIMWS
                    With data
                        .IFLAG2 = 2
                        .IBATCH = row("IBATCH")
                    End With
                    Console.WriteLine("Actualizando BFCIMWS MySQL...")
                    continuar = datos.ActualizarBFCIMWSRow(data, 1)
                    If continuar = False Then
                        Console.WriteLine("Error Actualizando BFCIMWS MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                        'ERROR DE ACTUALIZADO
                    Else
                        Console.WriteLine("BFCIMWS MySQL Actualizado")
                    End If
                Next
            End If
        End If
    End Sub

    Sub ActualizacionBFCIMTransacInv()
        Dim parametro(1) As String
        Dim dtBFCIM As DataTable
        Dim continuar As Boolean

        parametro(0) = 0
        parametro(1) = "TRANSAC. INV."
        Console.WriteLine("Consultando BFCIM MySQL...")
        dtBFCIM = datos.ConsultarBFCIMdt(parametro, 1)
        If dtBFCIM IsNot Nothing Then
            Console.WriteLine("Recorriendo BFCIM MySQL...")
            For Each row As DataRow In dtBFCIM.Rows

                data = Nothing
                data = New BFCIM
                With data
                    .IFUENTE = row("IFUENTE")
                    .IBATCH = row("IBATCH")
                    .INID = row("INID")
                    .INTRAN = row("INTRAN")
                    .INCTNR = row("INCTNR")
                    .INMFGR = row("INMFGR")
                    .INDATE = row("INDATE")
                    .INTRN = row("INTRN")
                    .INMLOT = row("INMLOT")
                    .INREF = row("INREF")
                    .INPROD = row("INPROD")
                    .INLOT = row("INLOT")
                    .INLOC = row("INLOC")
                    .INQTY = row("INQTY")
                    .INCBY = row("INCBY")
                    .INLDT = row("INLDT")
                    .INLTM = row("INLTM")
                    .INCDT = row("INCDT")
                    .INCTM = row("INCTM")
                    .INUMERO = row("INUMERO")
                    .INCOST = row("INCOST")
                    .INWHSE = row("INWHSE")
                    .INFACTU = IIf(IsDBNull(row("INFACTU")), "0", row("INFACTU"))
                End With
                Console.WriteLine("Guardando BFCIM AS400...")
                continuar = datosas400.GuardarBFCIMRow(data, 2)
                If continuar = False Then
                    Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                    'ERROR DE GUARDADO
                    Exit Sub
                Else
                    Console.WriteLine("BFCIM AS400 Guardado")
                    data = Nothing
                    data = New BFCIM
                    With data
                        .IFLAG = 1
                        .IBATCH = row("IBATCH")
                        .INTRN = row("INTRN")
                    End With
                    Console.WriteLine("Actualizando BFCIM MySQL...")
                    continuar = datos.ActualizarBFCIMRow(data, 2)
                    If continuar = False Then
                        Console.WriteLine("Error Actualizando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                        'ERROR DE ACTUALIZADO
                    Else
                        Console.WriteLine("BFCIM MySQL Actualizado")
                    End If
                End If
            Next
            parametro(0) = "1"
            parametro(1) = "TRANSAC. INV."
            Console.WriteLine("Consultando BFCIM MySQL...")
            dtBFCIM = datos.ConsultarBFCIMdt(parametro, 3)
            If dtBFCIM IsNot Nothing Then
                Console.WriteLine("Recorriendo BFCIM MySQL...")
                For Each row As DataRow In dtBFCIM.Rows
                    Console.WriteLine("Llamando Programa BCTRAINVB... " & datos.Ceros(row("IBATCH"), 8) & "', 'DESP")
                    datosas400.LlamaPrograma("BCTRAINVB", datos.Ceros(row("IBATCH"), 8) & "', 'DESP")
                    'fSql = "{{ CALL PGM(" & paramWebConfig400("AS400_PGM") & "/BCTRAINVB) PARM('" & Ceros(row("IBATCH"), 8) & "' 'DESP' ) }}"
                    'DB.ExecuteSQL(fSql)
                    'data = Nothing
                    'data = New BFCIM
                    'With data
                    '    .IFLAG = 2
                    '    .IBATCH = row("IBATCH")
                    'End With
                    'Console.WriteLine("Actualizando BFCIM MySQL...")
                    'continuar = datos.ActualizarBFCIMRow(data, 3)
                    'If continuar = False Then
                    '    Console.WriteLine("Error Actualizando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                    '    'ERROR DE ACTUALIZADO
                    'Else
                    '    Console.WriteLine("BFCIM MySQL Actualizado")
                    'End If
                Next
            End If
        End If
    End Sub

    Sub ActualizacionBFDESPA()
        Dim parametro(0) As String
        Dim dtBFDESPAWS As DataTable
        Dim continuar As Boolean

        Console.WriteLine("Consultando BFDESPAWS MySQL...")
        parametro(0) = 0
        dtBFDESPAWS = datos.ConsultarBFDESPAWSdt(parametro, 0)
        If dtBFDESPAWS IsNot Nothing Then
            Console.WriteLine("Recorriendo BFDESPAWS MySQL...")
            For Each row As DataRow In dtBFDESPAWS.Rows
                data = Nothing
                data = New BFDESPA
                With data
                    .DBATCH = row("DBATCH")                         '//15A
                    .DPEDID = row("DPEDID")                         '//8
                    .DLINEA = row("DLINEA")                         '//2A    Record Identifier
                    .DPRODU = row("DPRODU")                         '//2A    Transaction Type
                    .DCANTI = row("DCANTI")                         '//2A    Transaction Type
                    .DBODEG = row("DBODEG")                         '//2A    Transaction Type
                    .DLOCAL = row("DLOCAL")                         '//8P0   Date		
                    .DLOTE = row("DLOTE")                           '//5P0   INTRN
                    .DUSER = row("DUSER")                           '//5P0   INTRN
                    .DFLAG = row("DFLAG")                           '//2A    Warehouse
                End With
                Console.WriteLine("Guardando BFDESPAWS AS400...")
                continuar = datosas400.GuardarBFDESPARow(data)
                If continuar = False Then
                    Console.WriteLine("Error Guardando BFDESPAWS AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                    'ERROR DE GUARDADO
                Else
                    Console.WriteLine("BFDESPAWS AS400 Guardado")
                    Console.WriteLine("Guardando BFDESPAWS MySQL...")
                    continuar = datos.GuardarBFDESPARow(data)
                    If continuar = False Then
                        Console.WriteLine("Error Guardando BFDESPAWS MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                        'ERROR DE GUARDADO
                    Else
                        Console.WriteLine("BFDESPAWS MySQL Guardado")
                        data = Nothing
                        data = New BFDESPAWS
                        With data
                            .DFLAG2 = 1
                            .DBATCH = row("DBATCH")
                            .DPEDID = row("DPEDID")
                            .DLINEA = row("DLINEA")
                        End With
                        Console.WriteLine("Actualizando BFDESPAWS MySQL...")
                        continuar = datos.ActualizarBFDESPAWSRow(data, 0)
                        If continuar = False Then
                            Console.WriteLine("Error Actualizando BFDESPAWS MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                            'ERROR DE ACTUALIZADO
                        Else
                            Console.WriteLine("BFDESPAWS MySQL Actualizado")
                        End If
                    End If
                End If
            Next
        End If
        Console.WriteLine("Consultando BFDESPAWS MySQL...")
        parametro(0) = 1
        dtBFDESPAWS = datos.ConsultarBFDESPAWSdt(parametro, 1)
        If dtBFDESPAWS IsNot Nothing Then
            Console.WriteLine("Recorriendo BFDESPAWS MySQL...")
            For Each row As DataRow In dtBFDESPAWS.Rows
                Console.WriteLine("Llamando Programa BCCONFIR... " & datos.Ceros(row("DBATCH"), 8))
                datosas400.LlamaPrograma("BCCONFIR", datos.Ceros(row("DBATCH"), 8))
                'fSql = "{{ CALL PGM(" & paramWebConfig400("AS400_PGM") & "/BCCONFIR) PARM('" & Ceros(rs2("DBATCH").Value, 8) & "') }}"
                'DB.ExecuteSQL(fSql)
                data = Nothing
                data = New BFDESPAWS
                With data
                    .DFLAG2 = 2
                    .DBATCH = row("DBATCH")
                End With
                Console.WriteLine("Actualizando BFDESPAWS MySQL...")
                continuar = datos.ActualizarBFDESPAWSRow(data, 1)
                If continuar = False Then
                    Console.WriteLine("Error Actualizando BFDESPAWS MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                    'ERROR DE ACTUALIZADO
                Else
                    Console.WriteLine("BFDESPAWS MySQL Actualizado")
                End If
            Next
            wsResult = "Transacciones enviadas"
            Console.WriteLine(wsResult)
        End If
    End Sub

    Sub actualizacionPR()
        Dim r As Integer = 0
        Dim parametro(0) As String
        Dim dtBFREP As DataTable
        Dim dtFSO As DataTable
        Dim continuar As Boolean

        Console.WriteLine("Consultando BFREP MySQL...")
        dtBFREP = datos.ConsultarBFREPdt(parametro, 0)
        If dtBFREP IsNot Nothing Then
            Console.WriteLine("Recorriendo BFREP MySQL...")
            For Each row As DataRow In dtBFREP.Rows
                'parametro(0) = cadenaAS400
                parametro(0) = row("TORD")
                Console.WriteLine("Consultando FSO AS400...")
                dtFSO = datosas400.ConsultarFSOdt(parametro, 0)
                If dtFSO IsNot Nothing Then
                    data = Nothing
                    data = New BFREP
                    With data
                        .TQRECB = dtFSO.Rows(0).Item("SQFIN")
                        .TQREO = dtFSO.Rows(0).Item("SQREQ")
                        .TORD = row("TORD")
                    End With
                    Console.WriteLine("Actualizando BFREP MySQL...")
                    continuar = datos.ActualizarBFREPRow(data, 0)
                    If continuar = False Then
                        Console.WriteLine("Error Actualizando BFREP MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                        'ERROR DE ACTUALIZADO
                    Else
                        Console.WriteLine("BFREP MySQL Actualizado")
                    End If
                End If
                r += 1
            Next
        Else
            wsResult = "Sin Novedad"
            Console.WriteLine(wsResult)
            Return
        End If
        wsResult = "Registros actualizados: " & r
        Console.WriteLine(wsResult)
    End Sub

    Sub actualizacionPRD()
        Dim r As Integer = 0
        Dim parametro(0) As String
        Dim dtBFREPD As DataTable
        Dim dtFSO As DataTable
        Dim continuar As Boolean

        Console.WriteLine("Consultando BFREPD MySQL...")
        dtBFREPD = datos.ConsultarBFREPDdt(parametro, 0)
        If dtBFREPD IsNot Nothing Then
            Console.WriteLine("Recorriendo BFREPD MySQL...")
            For Each row As DataRow In dtBFREPD.Rows
                'parametro(0) = cadenaAS400
                parametro(0) = row("TORD")
                Console.WriteLine("Consultando FSO AS400...")
                dtFSO = datosas400.ConsultarFSOdt(parametro, 0)
                Console.WriteLine("FSO AS400 Consultado")
                If dtFSO IsNot Nothing Then
                    data = Nothing
                    data = New BFREP
                    With data
                        .TQRECB = dtFSO.Rows(0).Item("SQFIN")
                        .TQREO = dtFSO.Rows(0).Item("SQREQ")
                        .TORD = row("TORD")
                    End With
                    Console.WriteLine("Actualizando BFREP MySQL...")
                    continuar = datos.ActualizarBFREPRow(data, 0)
                    If continuar = False Then
                        Console.WriteLine("Error Actualizando BFREP MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                        'ERROR DE ACTUALIZADO
                    Else
                        Console.WriteLine("BFREP MySQL Actualizado")
                    End If
                End If
                r += 1
            Next
        Else
            wsResult = "Sin Novedad"
            Console.WriteLine(wsResult)
            Return
        End If
        wsResult = "Registros actualizados: " & r
        Console.WriteLine(wsResult)
    End Sub

    Sub TransaccionRAcero()
        Dim canti As Double
        Dim canti1 As Double
        Dim cons As String
        Dim Bodega As String = ""
        Dim batch As String = datos.CalcConsec("BFCIM")
        Dim parametro(0) As String
        Dim dtBFREPACE As DataTable
        Dim dtBFREPACE1 As DataTable
        Dim dtBFREP As DataTable
        Dim dtCIC As DataTable
        Dim continuar As Boolean

        Console.WriteLine("Consultando BFREPACE MySQL...")
        parametro(0) = "REVERSION"
        dtBFREPACE = datos.ConsultarBFREPACEdt(parametro, 0)
        If dtBFREPACE IsNot Nothing Then
            'Recibe
            '1. Transacc CI con el Acero (CPRODPP) con la Orden que va a recibir el acero con cantidad del acero positiva
            '2. Transacc WT con el prod terminado (CPRODPT) con la Orden que va a recibir con cantidad del producto terminado positiva

            'Saca
            '3. Transacc CI con el Acero (el mismo) con la Orden desde donde sale con cantidad negativa del acero
            '4. Transacc WT con el prod terminado de la Orden desde donde sale con cantidad negativa del producto terminado	
            Console.WriteLine("Recorriendo BFREPACE MySQL...")
            For Each row As DataRow In dtBFREPACE.Rows
                Console.WriteLine("Consultando BFREP MySQL...")
                parametro(0) = row("CORDEN")
                dtBFREP = datos.ConsultarBFREPdt(parametro, 1)
                If dtBFREP IsNot Nothing Then
                    '1. Transacc CI con el ACERO (CPRODPP) con la Orden que va a recibir el acero con cantidad del acero positiva
                    Console.WriteLine("Consultando CIC MySQL...")
                    parametro(0) = row("CPRODPP")
                    dtCIC = datos.ConsultarCICdt(parametro, 0)
                    If dtCIC IsNot Nothing Then
                        Bodega = dtCIC.Rows(0).Item("ICPLN")
                    End If
                    cons = datos.CalcConsec("CIMNUM")
                    data = Nothing
                    data = New BFCIM
                    With data
                        .IFUENTE = "PRODUCCION"                         '//15A
                        .IBATCH = batch                                 '//8"
                        .INID = "IN"                                    '//2A    Record Identifier
                        .INTRAN = "CI"                                  '//2A    Transaction Type
                        .INCTNR = 0                                     '//2A    Transaction Type"
                        .INMFGR = 0                                     '//2A    Transaction Type"
                        .INDATE = ClsDatosAS400.DB2_DATEDEC             '//8P0   Date		"
                        .INTRN = Right(cons, 5)                         '//5P0   INTRN"
                        .INMLOT = "RP" & cons                           '//5P0   INTRN
                        .INREAS = "01"                                  '//5P0   INTRN
                        .INREF = dtBFREP.Rows(0).Item("TORD")           '//8P0   Date
                        .INPROD = row("CPRODPP")                        '//15A   Item Number
                        .INWHSE = Bodega                                '//2A    Warehouse"
                        .INLOT = ""                                     '//10A   Lot Number"
                        .INLOC = ""                                     '//6A    Location Code"
                        canti1 = CDbl(row("CQREQPP"))
                        .INQTY = canti1                                 '//10A   Creted by"
                        .INCBY = "COSIS001"                             '//10A   Creted by
                        .INLDT = Format(Now(), "yyyyMMdd")              '//8P0
                        .INLTM = Format(Now(), "hhmmss")                '//6P0
                        .INCDT = ClsDatosAS400.DB2_DATEDEC              '//8P0   Date Created"
                        .INCTM = ClsDatosAS400.DB2_TIMEDEC              '//6P0   Time Created"
                    End With
                    Console.WriteLine("Guardando BFCIM AS400...")
                    continuar = datosas400.GuardarBFCIMRow(data, 1)
                    If continuar = False Then
                        Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                        'ERROR DE GUARDADO
                    Else
                        Console.WriteLine("BFCIM AS400 Guardado")
                        Console.WriteLine("Guardando BFCIM MySQL...")
                        continuar = datos.GuardarBFCIMRow(data, 1)
                        If continuar = False Then
                            Console.WriteLine("Error Guardando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                            'ERROR DE GUARDADO
                        Else
                            Console.WriteLine("BFCIM MySQL Guardado")
                            '2. Transacc WT con el prod terminado (CPRODPT) con la Orden que va a recibir con cantidad del producto terminado positiva
                            cons = datos.CalcConsec("CIMNUM")
                            data = Nothing
                            data = New BFCIM
                            With data
                                .IFUENTE = "PRODUCCION"                         '//15A
                                .IBATCH = batch                                 '//8"
                                .INID = "IN"                                    '//2A    Record Identifier
                                .INTRAN = "WT"                                  '//2A    Transaction Type
                                .INCTNR = 0                                     '//2A    Transaction Type"
                                .INMFGR = 0                                     '//2A    Transaction Type"
                                .INDATE = ClsDatosAS400.DB2_DATEDEC             '//8P0   Date		"
                                .INTRN = Right(cons, 5)                         '//5P0   INTRN"
                                .INMLOT = "RP" & cons                           '//5P0   INTRN
                                .INREAS = "01"                                  '//5P0   INTRN
                                .INREF = dtBFREP.Rows(0).Item("TORD")           '//8P0   Date
                                .INPROD = row("CPRODPT")                        '//15A   Item Number
                                .INWHSE = dtBFREP.Rows(0).Item("PBODPP")        '//2A    Warehouse
                                .INLOT = dtBFREP.Rows(0).Item("PLOTE")          '//10A   Lot Number
                                .INLOC = dtBFREP.Rows(0).Item("PLOCPP")         '//6A    Location Code
                                canti = CDbl(row("CQREQPT"))
                                .INQTY = canti                                  '//10A   Creted by"
                                .INCBY = "COSIS001"                             '//10A   Creted by
                                .INLDT = Format(Now(), "yyyyMMdd")              '//8P0
                                .INLTM = Format(Now(), "hhmmss")                '//6P0
                                .INCDT = ClsDatosAS400.DB2_DATEDEC              '//8P0   Date Created"
                                .INCTM = ClsDatosAS400.DB2_TIMEDEC              '//6P0   Time Created"
                            End With
                            Console.WriteLine("Guardando BFCIM AS400...")
                            continuar = datosas400.GuardarBFCIMRow(data, 1)
                            If continuar = False Then
                                Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                                'ERROR AL GUARDAR
                            Else
                                Console.WriteLine("BFCIM AS400 Guardado")
                                Console.WriteLine("Guardando BFCIM MySQL...")
                                continuar = datos.GuardarBFCIMRow(data, 1)
                                If continuar = False Then
                                    Console.WriteLine("Error Guardando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                    'ERROR AL GUARDAR
                                Else
                                    Console.WriteLine("BFCIM MySQL Guardado")
                                End If
                            End If
                        End If
                    End If
                End If
                Dim aReversar() As String
                aReversar = Split(row("CORDENR"), "-")
                Console.WriteLine("Consultando BFREPACE MySQL...")
                parametro(0) = aReversar(1)
                dtBFREPACE1 = datos.ConsultarBFREPACEdt(parametro, 1)
                If dtBFREPACE1 IsNot Nothing Then
                    '3. Transacc CI con el Acero (el mismo) con la Orden desde donde sale con cantidad negativa del acero
                    Bodega = ""
                    Console.WriteLine("Consultando CIC MySQL...")
                    parametro(0) = row("CPRODPP")
                    dtCIC = datos.ConsultarCICdt(parametro, 0)
                    If dtCIC IsNot Nothing Then
                        Bodega = dtCIC.Rows(0).Item("ICPLN")
                    End If
                    cons = datos.CalcConsec("CIMNUM")
                    data = Nothing
                    data = New BFCIM
                    With data
                        .IFUENTE = "PRODUCCION"                                 '//15A
                        .IBATCH = batch                                         '//8"
                        .INID = "IN"                                            '//2A    Record Identifier
                        .INTRAN = "CI"                                          '//2A    Transaction Type
                        .INCTNR = 0                                             '//2A    Transaction Type"
                        .INMFGR = 0                                             '//2A    Transaction Type"
                        .INDATE = ClsDatosAS400.DB2_DATEDEC                     '//8P0   Date		"
                        .INTRN = Right(cons, 5)                                 '//5P0   INTRN"
                        .INMLOT = "RP" & cons                                   '//5P0   INTRN
                        .INREAS = "01"                                          '//5P0   INTRN
                        .INREF = dtBFREPACE1.Rows(0).Item("CORDEN")             '//8P0   Date
                        .INPROD = dtBFREPACE1.Rows(0).Item("CPRODPP")           '//15A   Item Number
                        .INWHSE = Bodega                                        '//2A    Warehouse"
                        .INLOT = ""                                             '//10A   Lot Number"
                        .INLOC = ""                                             '//6A    Location Code"
                        .INQTY = -canti1                                        '//10A   Creted by"
                        .INCBY = "COSIS001"                                     '//10A   Creted by
                        .INLDT = Format(Now(), "yyyyMMdd")                      '//8P0
                        .INLTM = Format(Now(), "hhmmss")                        '//6P0
                        .INCDT = ClsDatosAS400.DB2_DATEDEC                      '//8P0   Date Created"
                        .INCTM = ClsDatosAS400.DB2_TIMEDEC                      '//6P0   Time Created"
                    End With
                    Console.WriteLine("Guardando BFCIM AS400...")
                    continuar = datosas400.GuardarBFCIMRow(data, 1)
                    If continuar = False Then
                        Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                        'ERROR DE GUARDADO
                    Else
                        Console.WriteLine("BFCIM AS400 Guardado")
                        Console.WriteLine("Guardando BFCIM MySQL...")
                        continuar = datos.GuardarBFCIMRow(data, 1)
                        If continuar = False Then
                            Console.WriteLine("Error Guardando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                            'ERROR DE GUARDADO
                        Else
                            Console.WriteLine("BFCIM MySQL Guardado")
                            '4. Transacc WT con el prod terminado de la Orden desde donde sale con cantidad negativa del producto terminado
                            cons = datos.CalcConsec("CIMNUM")
                            data = Nothing
                            data = New BFCIM
                            With data
                                .IFUENTE = "PRODUCCION"                                 '//15A
                                .IBATCH = batch                                         '//8"
                                .INID = "IN"                                            '//2A    Record Identifier
                                .INTRAN = "WT"                                          '//2A    Transaction Type
                                .INCTNR = 0                                             '//2A    Transaction Type"
                                .INMFGR = 0                                             '//2A    Transaction Type"
                                .INDATE = ClsDatosAS400.DB2_DATEDEC                     '//8P0   Date		"
                                .INTRN = Right(cons, 5)                                 '//5P0   INTRN"
                                .INMLOT = "RP" & cons                                   '//5P0   INTRN
                                .INREAS = "01"                                          '//5P0   INTRN
                                .INREF = dtBFREPACE1.Rows(0).Item("CORDEN")             '//8P0   Date
                                .INPROD = dtBFREPACE1.Rows(0).Item("CPRODPT")           '//15A   Item Number
                                .INWHSE = dtBFREPACE1.Rows(0).Item("PBODPP")            '//2A    Warehouse
                                .INLOT = dtBFREPACE1.Rows(0).Item("PLOTE")              '//10A   Lot Number
                                .INLOC = dtBFREPACE1.Rows(0).Item("PLOCPP")             '//6A    Location Code
                                .INQTY = -canti                                         '//10A   Creted by"
                                .INCBY = "COSIS001"                                     '//10A   Creted by
                                .INLDT = Format(Now(), "yyyyMMdd")                      '//8P0
                                .INLTM = Format(Now(), "hhmmss")                        '//6P0
                                .INCDT = ClsDatosAS400.DB2_DATEDEC                      '//8P0   Date Created"
                                .INCTM = ClsDatosAS400.DB2_TIMEDEC                      '//6P0   Time Created"
                            End With
                            Console.WriteLine("Guardando BFCIM AS400...")
                            continuar = datosas400.GuardarBFCIMRow(data, 1)
                            If continuar = False Then
                                Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                                'ERROR DE GUARDADO
                            Else
                                Console.WriteLine("BFCIM AS400 Guardado")
                                Console.WriteLine("Guardando BFCIM MySQL...")
                                continuar = datos.GuardarBFCIMRow(data, 1)
                                If continuar = False Then
                                    Console.WriteLine("Error Guardando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                    'ERROR DE GUARDADO
                                Else
                                    Console.WriteLine("BFCIM MySQL Guardado")
                                    data = Nothing
                                    data = New BFREPACE
                                    With data
                                        .CENVIADO = "1"
                                        .WRKID = row("WRKID")
                                    End With
                                    Console.WriteLine("Actualizando BFREPACE MySQL...")
                                    continuar = datos.ActualizarBFREPACERow(data, 0)
                                    If continuar = False Then
                                        Console.WriteLine("Error Actualizando BFREPACE MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                        'ERROR DE ACTUALIZADO
                                    Else
                                        Console.WriteLine("BFREPACE MySQL Actualizado")
                                        data = Nothing
                                        data = New BFREP
                                        With data
                                            .TQREQACEB = canti
                                            .TNUMREP = row("CNUMREP")
                                        End With
                                        Console.WriteLine("Actualizando BFREP MySQL...")
                                        continuar = datos.ActualizarBFREPRow(data, 1)
                                        If continuar = False Then
                                            Console.WriteLine("Error Actualizando BFREP MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                            'ERROR DE ACTUALIZADO
                                        Else
                                            Console.WriteLine("BFREP MySQL Actualizado")
                                            data = Nothing
                                            data = New BFREP
                                            With data
                                                .TRPODACEB = canti1
                                                .TNUMREP = row("CNUMREP")
                                            End With
                                            Console.WriteLine("Actualizando BFREP MySQL...")
                                            continuar = datos.ActualizarBFREPRow(data, 3)
                                            If continuar = False Then
                                                Console.WriteLine("Error Actualizando BFREP MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                                'ERROR DE ACTUALIZADO
                                            Else
                                                Console.WriteLine("BFREP MySQL Actualizado")
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            Next
        Else
            wsResult = "Sin Novedad"
            Console.WriteLine(wsResult)
            Return
        End If
        Console.WriteLine("Llamando Programa BCTRAINVB... " & batch & "', 'PROD")
        datosas400.LlamaPrograma("BCTRAINVB", batch & "', 'PROD")
        'fSql = "{{ CALL PGM(" & paramWebConfig400("AS400_PGM") & "/BCTRAINVB) PARM('" & batch & "' 'PROD' ) }}"
        'DB.ExecuteSQL(fSql)
        wsResult = "Transacciones enviadas"
        Console.WriteLine(wsResult)
    End Sub

    Sub TransaccionRJ()
        Dim canti As Double
        Dim batch As String = datos.CalcConsec("BFCIM")
        Dim cons As String
        Dim dtBFREP As DataTable
        Dim parametro(0) As String
        Dim continuar As Boolean

        Console.WriteLine("Consultando BFREP MySQL...")
        dtBFREP = datos.ConsultarBFREPdt(parametro, 2)
        If dtBFREP IsNot Nothing Then
            Console.WriteLine("Recorriendo BFREP MySQL...")
            For Each row As DataRow In dtBFREP.Rows
                cons = datos.CalcConsec("CIMNUM")
                data = Nothing
                data = New BFCIM
                With data
                    .IFUENTE = "PRODUCCION"                         '//15A
                    .IBATCH = batch                                 '//8"
                    .INID = "IN"                                    '//2A    Record Identifier
                    .INTRAN = "RJ"                                  '//2A    Transaction Type
                    .INCTNR = row("RPROCESO")                       '//2A    Transaction Type
                    .INMFGR = row("RPROCESO")                       '//2A    Transaction Type
                    .INDATE = ClsDatosAS400.DB2_DATEDEC             '//8P0   Date		"
                    .INTRN = Right(cons, 5)                         '//5P0   INTRN"
                    .INMLOT = "RP" & cons                           '//5P0   INTRN
                    .INREAS = row("RCAUSAL")                        '//5P0   INTRN
                    .INREF = row("TORD")                            '//8P0   Date
                    .INPROD = row("TPROD")                          '//15A   Item Number
                    .INWHSE = row("PBODPP")                         '//2A    Warehouse
                    .INLOT = row("PLOTE")                           '//10A   Lot Number
                    .INLOC = row("PLOCPP")                          '//6A    Location Code
                    canti = (CDbl(row("RQREP")))
                    .INQTY = canti                                  '//10A   Creted by"
                    .INCBY = "COSIS001"                             '//10A   Creted by
                    .INLDT = Format(Now(), "yyyyMMdd")              '//8P0
                    .INLTM = Format(Now(), "hhmmss")                '//6P0
                    .INCDT = ClsDatosAS400.DB2_DATEDEC              '//8P0   Date Created"
                    .INCTM = ClsDatosAS400.DB2_TIMEDEC              '//6P0   Time Created"
                End With
                Console.WriteLine("Guardando BFCIM AS400...")
                continuar = datosas400.GuardarBFCIMRow(data, 1)
                If continuar = False Then
                    Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                    'ERROR DE GUARDADO
                Else
                    Console.WriteLine("BFCIM AS400 Guardado")
                    Console.WriteLine("Guardando BFCIM MySQL...")
                    continuar = datos.GuardarBFCIMRow(data, 1)
                    If continuar = False Then
                        Console.WriteLine("Error Guardando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                        'ERROR DE GUARDADO
                    Else
                        Console.WriteLine("BFCIM MySQL Guardado")
                        data = Nothing
                        data = New BFREPTURRJ
                        With data
                            .RENVIADO = "1"
                            .RWRKID = row("RWRKID")
                        End With
                        Console.WriteLine("Actualizando BFREPTURRJ MySQL...")
                        continuar = datos.ActualizarBFREPTURRJRow(data, 0)
                        If continuar = False Then
                            Console.WriteLine("Error Actualizando BFREPTURRJ MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                            'ERROR DE ACTUALIZADO
                        Else
                            Console.WriteLine("BFREPTURRJ MySQL Actualizado")
                            data = Nothing
                            data = New BFREP
                            With data
                                .TQREDB = row("RQREP")
                                .TNUMREP = row("TNUMREP")
                            End With
                            Console.WriteLine("Actualizando BFREP MySQL...")
                            continuar = datos.ActualizarBFREPRow(data, 2)
                            If continuar = False Then
                                Console.WriteLine("Error Actualizando BFREP MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                'ERROR DE ACTUALIZADO
                            Else
                                Console.WriteLine("BFREP MySQL Actualizado")
                            End If
                        End If
                    End If
                End If
            Next
        Else
            wsResult = "Sin Novedad"
            Console.WriteLine(wsResult)
            Return
        End If
        Console.WriteLine("Llama Programa BCTRAINVB " & batch & "', 'PROD")
        datosas400.LlamaPrograma("BCTRAINVB", batch & "', 'PROD")
        'fSql = "{{ CALL PGM(" & paramWebConfig400("AS400_PGM") & "/BCTRAINVB) PARM('" & batch & "' 'PROD' ) }}"
        'DB.ExecuteSQL(fSql)
        wsResult = "Transacciones enviadas"
        Console.WriteLine(wsResult)
    End Sub

    Sub TransaccionAcero()
        Dim cons As String
        Dim batch As String = datos.CalcConsec("BFCIM")
        Dim canti As Double
        Dim canti1 As Double
        Dim parametro(0) As String
        Dim dtBFREPACE As DataTable
        Dim dtCIC As DataTable
        Dim continuar As Boolean

        Console.WriteLine("Consultando BFREPACE MySQL...")
        parametro(0) = "REPORTE"
        dtBFREPACE = datos.ConsultarBFREPACEdt(parametro, 0)
        If dtBFREPACE IsNot Nothing Then
            Console.WriteLine("Recorriendo BFREPACE MySQL...")
            For Each row As DataRow In dtBFREPACE.Rows
                cons = datos.CalcConsec("CIMNUM")
                data = Nothing
                data = New BFCIM
                With data
                    .IFUENTE = "PRODUCCION"                         '//15A
                    .IBATCH = batch                                 '//8"
                    .INID = "IN"                                    '//2A    Record Identifier
                    .INTRAN = "WT"                                  '//2A    Transaction Type
                    .INCTNR = 0                                     '//2A    Transaction Type"
                    .INMFGR = 0                                     '//2A    Transaction Type"
                    .INDATE = ClsDatosAS400.DB2_DATEDEC             '//8P0   Date		"
                    .INTRN = Right(cons, 5)                         '//5P0   INTRN"
                    .INMLOT = "RP" & cons                           '//5P0   INTRN
                    .INREAS = "01"                                  '//5P0   INTRN
                    .INREF = row("CORDEN")                          '//8P0   Date
                    .INPROD = row("CPRODPT")                        '//15A   Item Number
                    .INWHSE = row("PBODPP")                         '//2A    Warehouse
                    .INLOT = row("PLOTE")                           '//10A   Lot Number
                    .INLOC = row("PLOCPP")                          '//6A    Location Code
                    canti = CDbl(row("CQREQPT"))
                    .INQTY = canti                                  '//10A   Creted by"
                    .INCBY = "COSIS001"                             '//10A   Creted by
                    .INLDT = Format(Now(), "yyyyMMdd")              '//8P0
                    .INLTM = Format(Now(), "hhmmss")                '//6P0
                    .INCDT = ClsDatosAS400.DB2_DATEDEC              '//8P0   Date Created"
                    .INCTM = ClsDatosAS400.DB2_TIMEDEC              '//6P0   Time Created"
                End With
                Console.WriteLine("Guardando BFCIM AS400...")
                continuar = datosas400.GuardarBFCIMRow(data, 1)
                If continuar = False Then
                    Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                    'ERROR DE GUARDADO AS400
                Else
                    Console.WriteLine("BFCIM AS400 Guardado")
                    Console.WriteLine("Guardando BFCIM MySQL...")
                    continuar = datos.GuardarBFCIMRow(data, 1)
                    If continuar = False Then
                        Console.WriteLine("Error Guardando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                        'ERROR DE GUARDADO
                    Else
                        Console.WriteLine("BFCIM MySQL Guardado")
                        Console.WriteLine("Consultando CIC MySQL...")
                        parametro(0) = row("CPRODPP")
                        dtCIC = datos.ConsultarCICdt(parametro, 0)
                        If dtCIC IsNot Nothing Then
                            cons = datos.CalcConsec("CIMNUM")
                            data = Nothing
                            data = New BFCIM
                            With data
                                .IFUENTE = "PRODUCCION"                         '//15A
                                .IBATCH = batch                                 '//8"
                                .INID = "IN"                                    '//2A    Record Identifier
                                .INTRAN = "CI"                                  '//2A    Transaction Type
                                .INCTNR = 0                                     '//2A    Transaction Type"
                                .INMFGR = 0                                     '//2A    Transaction Type"
                                .INDATE = ClsDatosAS400.DB2_DATEDEC             '//8P0   Date		"
                                .INTRN = Right(cons, 5)                         '//5P0   INTRN"
                                .INMLOT = "RP" & cons                           '//5P0   INTRN
                                .INREAS = "01"                                  '//5P0   INTRN
                                .INREF = row("CORDEN")                          '//8P0   Date
                                .INPROD = row("CPRODPP")                        '//15A   Item Number
                                .INWHSE = dtCIC.Rows(0).Item("ICPLN")           '//2A    Warehouse
                                .INLOT = ""                                     '//10A   Lot Number"
                                .INLOC = ""                                     '//6A    Location Code"
                                canti1 = CDbl(row("CQREQPP"))
                                .INQTY = canti1                                 '//10A   Creted by"
                                .INCBY = "COSIS001"                             '//10A   Creted by
                                .INLDT = Format(Now(), "yyyyMMdd")              '//8P0
                                .INLTM = Format(Now(), "hhmmss")                '//6P0
                                .INCDT = ClsDatosAS400.DB2_DATEDEC              '//8P0   Date Created"
                                .INCTM = ClsDatosAS400.DB2_TIMEDEC              '//6P0   Time Created"
                            End With
                            Console.WriteLine("Guardando BFCIM AS400...")
                            continuar = datosas400.GuardarBFCIMRow(data, 1)
                            If continuar = False Then
                                Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                                'ERROR DE GUARDADO AS400
                            Else
                                Console.WriteLine("BFCIM AS400 Guardado")
                                Console.WriteLine("Guardando BFCIM MySQL...")
                                continuar = datos.GuardarBFCIMRow(data, 1)
                                If continuar = False Then
                                    Console.WriteLine("Error Guardando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                    'ERROR DE GUARDADO
                                Else
                                    Console.WriteLine("BFCIM MySQL Guardado")
                                    data = Nothing
                                    data = New BFREP
                                    With data
                                        .TQREQACEB = canti
                                        .TNUMREP = row("CNUMREP")
                                    End With
                                    Console.WriteLine("Actualizando BFREP MySQL...")
                                    continuar = datos.ActualizarBFREPRow(data, 1)
                                    If continuar = False Then
                                        Console.WriteLine("Error Actualizando BFREP MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                        'ERROR DE ACTUALIZADO
                                    Else
                                        Console.WriteLine("BFREP MySQL Actualizado")
                                        data = Nothing
                                        data = New BFREP
                                        With data
                                            .TRPODACEB = canti1
                                            .TNUMREP = row("CNUMREP")
                                        End With
                                        Console.WriteLine("Actualizando BFREP MySQL...")
                                        continuar = datos.ActualizarBFREPRow(data, 3)
                                        If continuar = False Then
                                            Console.WriteLine("Error Actualizando BFREP MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                            'ERROR DE ACTUALIZADO
                                        Else
                                            Console.WriteLine("BFREP MySQL Actualizado")
                                            data = Nothing
                                            data = New BFREPACE
                                            With data
                                                .CENVIADO = "1"
                                                .WRKID = row("WRKID")
                                            End With
                                            Console.WriteLine("Actualizando BFREPACE MySQL...")
                                            continuar = datos.ActualizarBFREPACERow(data, 0)
                                            If continuar = False Then
                                                Console.WriteLine("Error Actualizando BFREPACE MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                                'ERROR DE ACTUALIZADO
                                            Else
                                                Console.WriteLine("BFREPACE MySQL Actualizado")
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            Next
        Else
            wsResult = "Sin Novedad"
            Console.WriteLine(wsResult)
            Return
        End If
        Console.WriteLine("Llamando Programa BCTRAINVB " & batch & "', 'PROD")
        datosas400.LlamaPrograma("BCTRAINVB", batch & "', 'PROD")
        'fSql = "{{ CALL PGM(" & paramWebConfig400("AS400_PGM") & "/BCTRAINVB) PARM('" & batch & "' 'PROD' ) }}"
        'DB.ExecuteSQL(fSql)
        wsResult = "Transacciones enviadas"
        Console.WriteLine(wsResult)
    End Sub

    Sub actualizaPR()
        Dim cons As String
        Dim batch As String = datos.CalcConsec("BFCIM")
        Dim parametro(0) As String
        Dim dtBFREP As DataTable
        Dim continuar As Boolean
        Dim canti

        Console.WriteLine("Consultando BFREP MySQL...")
        dtBFREP = datos.ConsultarBFREPdt(parametro, 3)
        If dtBFREP IsNot Nothing Then
            Console.WriteLine("Recorriendo BFREP MySQL...")
            For Each row As DataRow In dtBFREP.Rows
                cons = datos.CalcConsec("CIMNUM")
                data = Nothing
                data = New BFCIM
                With data
                    .IFUENTE = "PRODUCCION"                     '//15A
                    .INID = "IN"                                '//2A    Record Identifier
                    .IBATCH = batch                             '//8"
                    .INTRAN = "WF"                              '//2A    Transaction Type
                    .INCTNR = 0                                 '//2A    Transaction Type"
                    .INMFGR = 0                                 '//2A    Transaction Type"
                    .INDATE = ClsDatosAS400.DB2_DATEDEC         '//8P0   Date		"
                    .INTRN = Right(cons, 5)                     '//5P0   INTRN"
                    .INMLOT = "RP" & cons                       '//5P0   INTRN
                    .INREAS = "01"                              '//5P0   INTRN
                    .INREF = row("TORD")                        '//8P0   Date
                    .INPROD = row("TPROD")                      '//15A   Item Number
                    .INWHSE = row("PBODPP")                     '//2A    Warehouse
                    .INLOT = row("PLOTE")                       '//10A   Lot Number
                    .INLOC = row("PLOCPP")                      '//6A    Location Code
                    canti = -(CDbl(row("TQREP")) - CDbl(row("TQREC")))
                    .INQTY = canti                              '//10A   Creted by"
                    .INCBY = "COSIS001"                         '//10A   Creted by
                    .INCDT = ClsDatosAS400.DB2_DATEDEC          '//8P0   Date Created"
                    .INCTM = ClsDatosAS400.DB2_TIMEDEC          '//6P0   Time Created"
                End With
                Console.WriteLine("Guardando BFCIM AS400...")
                continuar = datosas400.GuardarBFCIMRow(data, 3)
                If continuar = False Then
                    Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                    'ERROR GUARDANDO
                Else
                    Console.WriteLine("BFCIM AS400 Guardado")
                    Console.WriteLine("Guardando BFCIM MySQL...")
                    continuar = datos.GuardarBFCIMRow(data, 2)
                    If continuar = False Then
                        Console.WriteLine("Error Guardando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                        'ERROR GUARDANDO
                    Else
                        Console.WriteLine("BFCIM MySQL Guardado")
                        cons = datos.CalcConsec("CIMNUM")
                        data = Nothing
                        data = New BFCIM
                        With data
                            .IFUENTE = "PRODUCCION"                         '//15A
                            .INID = "IN"                                    '//2A    Record Identifier
                            .IBATCH = batch                                 '//8"
                            .INTRAN = "PR"                                  '//2A    Transaction Type
                            .INCTNR = 0                                     '//2A    Transaction Type"
                            .INMFGR = 0                                     '//2A    Transaction Type"
                            .INDATE = ClsDatosAS400.DB2_DATEDEC             '//8P0   Date		"
                            .INTRN = Right(cons, 5)                         '//5P0   INTRN"
                            .INMLOT = "RP" & cons                           '//5P0   INTRN
                            .INREAS = "01"                                  '//5P0   INTRN
                            .INREF = row("TORD")                            '//8P0   Date
                            .INPROD = row("TPROD")                          '//15A   Item Number
                            .INWHSE = row("PBODPT")                         '//2A    Warehouse
                            .INLOT = row("PLOTE")                           '//10A   Lot Number
                            .INLOC = row("PLOCPT")                          '//6A    Location Code
                            canti = (CDbl(row("TQREP")) - CDbl(row("TQREC")))
                            .INQTY = canti                                  '//10A   Creted by"
                            .INCBY = "COSIS001"                             '//10A   Creted by
                            .INCDT = ClsDatosAS400.DB2_DATEDEC              '//8P0   Date Created"
                            .INCTM = ClsDatosAS400.DB2_TIMEDEC              '//6P0   Time Created"
                        End With
                        Console.WriteLine("Guardando BFCIM AS400...")
                        continuar = datosas400.GuardarBFCIMRow(data, 3)
                        If continuar = False Then
                            Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                            'ERROR GUARDANDO
                        Else
                            Console.WriteLine("BFCIM AS400 Guardado")
                            Console.WriteLine("Guardando BFCIM MySQL...")
                            continuar = datos.GuardarBFCIMRow(data, 2)
                            If continuar = False Then
                                Console.WriteLine("Error Guardando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                'ERROR GUARDANDO
                            Else
                                Console.WriteLine("BFCIM MySQL Guardado")
                                data = Nothing
                                data = New BFREP
                                With data
                                    .TQREC = canti
                                    .TNUMREP = row("TNUMREP")
                                End With
                                Console.WriteLine("Actualizando BFREP MySQL...")
                                continuar = datos.ActualizarBFREPRow(data, 4)
                                If continuar = False Then
                                    Console.WriteLine("Error Actualizando BFREP MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                                    'ERROR ACTUALIZANDO
                                Else
                                    Console.WriteLine("BFREP MySQL Actualizado")
                                End If
                            End If
                        End If
                    End If
                End If
            Next
        Else
            wsResult = "Sin Novedad"
            Console.WriteLine(wsResult)
            Return
        End If
        Try
            Console.WriteLine("Llamando Programa BCTRAINVB " & batch & "', 'PROD")
            datosas400.LlamaPrograma("BCTRAINVB", batch & "', 'PROD")
            wsResult = "Transacciones enviadas"
            Console.WriteLine(wsResult)
        Catch ex As Exception
            data = Nothing
            data = New BFLOG
            With data
                .USUARIO = System.Net.Dns.GetHostName()
                .OPERACION = "Llamar Programa "
                .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                .EVENTO = ex.Message
                .TXTSQL = datosas400.fSql
                .ALERT = 1
                .LKEY = Left(datosas400.gsKeyWords.Trim.ToUpper, 30)
            End With
            datos.GuardarBFLOGRow(data)
        End Try

    End Sub

    Sub actualizaPRD()
        Dim cons As String
        Dim batch As String = datos.CalcConsec("BFCIM")
        Dim parametro(0) As String
        Dim dtBFREPD As DataTable
        Dim continuar As Boolean
        Dim canti

        Console.WriteLine("Consultando BFREPD MySQL...")
        dtBFREPD = datos.ConsultarBFREPDdt(parametro, 1)
        If dtBFREPD IsNot Nothing Then
            Console.WriteLine("Recorriendo BFREPD MySQL...")
            For Each row As DataRow In dtBFREPD.Rows
                cons = datos.CalcConsec("CIMNUM")
                data = Nothing
                data = New BFCIM
                With data
                    .IFUENTE = "PRODUCCION"                         '//15A
                    .INID = "IN"                                    '//2A    Record Identifier
                    .IBATCH = batch                                 '//8"
                    .INTRAN = "R"                                   '//2A    Transaction Type
                    .INCTNR = 0                                     '//2A    Transaction Type"
                    .INMFGR = 0                                     '//2A    Transaction Type"
                    .INDATE = ClsDatosAS400.DB2_DATEDEC             '//8P0   Date		"
                    .INTRN = Right(cons, 5)                         '//5P0   INTRN"
                    .INMLOT = "RP" & cons                           '//5P0   INTRN
                    .INREAS = "01"                                  '//5P0   INTRN
                    .INREF = row("TORD")                            '//8P0   Date
                    .INPROD = row("TPROD")                          '//15A   Item Number
                    .INWHSE = row("PBODPT")                         '//2A    Warehouse
                    .INLOT = row("PLOTE")                           '//10A   Lot Number
                    .INLOC = row("PLOCPT")                          '//6A    Location Code
                    canti = (CDbl(row("TQREP")) - CDbl(row("TQREC")))
                    .INQTY = canti                                  '//10A   Creted by"
                    .INCBY = "COSIS001"                             '//10A   Creted by
                    .INCDT = ClsDatosAS400.DB2_DATEDEC              '//8P0   Date Created"
                    .INCTM = ClsDatosAS400.DB2_TIMEDEC              '//6P0   Time Created"
                End With
                Console.WriteLine("Guardando BFCIM AS400...")
                continuar = datosas400.GuardarBFCIMRow(data, 3)
                If continuar = False Then
                    Console.WriteLine("Error Guardando BFCIM AS400 " & datosas400.mensaje & vbCrLf & datosas400.fSql)
                    '
                Else
                    Console.WriteLine("BFCIM AS400 Guardado")
                    Console.WriteLine("Guardando BFCIM MySQL...")
                    continuar = datos.GuardarBFCIMRow(data, 2)
                    If continuar = False Then
                        Console.WriteLine("Error Guardando BFCIM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                        '
                    Else
                        Console.WriteLine("BFCIM MySQL Guardado")
                        data = Nothing
                        data = New BFREPD
                        With data
                            .TQREC = canti
                            .TNUMREP = row("TNUMREP")
                        End With
                        Console.WriteLine("Actualizando BFREPD MySQL...")
                        continuar = datos.ActualizarBFREPDRow(data, 0)
                        If continuar = False Then
                            Console.WriteLine("Error Actualizando BFREPD MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                            '
                        Else
                            Console.WriteLine("BFREPD MySQL Actualizado")
                        End If
                    End If
                End If
            Next
        Else
            wsResult = "Sin Novedad"
            Console.WriteLine(wsResult)
            Return
        End If
        Try
            Console.WriteLine("Llamando Programa BCTRAINVB " & batch & "', 'PROD")
            datosas400.LlamaPrograma("BCTRAINVB", batch & "', 'PROD")
            wsResult = "Transacciones enviadas"
            Console.WriteLine(wsResult)
        Catch ex As Exception
            data = Nothing
            data = New BFLOG
            With data
                .USUARIO = System.Net.Dns.GetHostName()
                .OPERACION = "Llamar Programa "
                .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                .EVENTO = ex.Message
                .TXTSQL = datosas400.fSql
                .ALERT = 1
                .LKEY = Left(datosas400.gsKeyWords.Trim.ToUpper, 30)
            End With
            datos.GuardarBFLOGRow(data)
        End Try
    End Sub

    Function chkVersion() As String
        Dim result As String = ""
        Dim locVer As String = FileVersionInfo.GetVersionInfo(Reflection.Assembly.GetExecutingAssembly().Location).FileVersion
        Dim dtBFPARAM As DataTable
        Dim parametro(1) As String
        Dim detallecadena() As String
        Dim detalle() As String

        Console.WriteLine("Consultando BFPARAM...")
        parametro(0) = "TareasBellota"
        parametro(1) = "Version"
        dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 0)
        If dtBFPARAM IsNot Nothing Then
            If dtBFPARAM.Rows(0).Item("CCDESC") <> locVer Then
                Console.WriteLine("Consultando Detalle Cadena...")
                detallecadena = datos.DetalleCadena
                result = "Version incorrecta. Registrada: " & dtBFPARAM.Rows(0).Item("CCDESC") & " Actual: " & locVer
                For i = 0 To detallecadena.Count - 1
                    detalle = Split(detallecadena(i), "=")
                    If detalle(0).ToUpper = "SERVER" Then
                        result &= "<br>" & detalle(1)
                    ElseIf detalle(0).ToUpper = "DATABASE" Then
                        result &= "/" & detalle(1)
                    End If
                Next
            End If
        Else
            result = "Aplicacion no registrada."
            Console.WriteLine(result)
        End If
        Console.WriteLine(result)
        Return result
    End Function

    Function registroWS(tipo As String, op As String) As Boolean
        Dim servWin As String = "TareasBellota"
        Dim resultado As Boolean = True
        Dim hora As Integer
        Dim continuar As Boolean
        Dim dtBFPARAM As DataTable
        Dim parametro(2) As String

        If wsNum <> "" Then
            data = Nothing
            data = New BFPARAM
            With data
                .CCNOT1 = wsResult
                .CCMNDT = Format(Now(), "yyy-MM-dd HH:mm:ss")
                .CCNUM = wsNum
            End With
            Console.WriteLine("Actualizando BFPARAM MySQL...")
            continuar = datos.ActualizarBFPARAMRow(data, 0)
            If continuar = False Then
                Console.WriteLine("Error Actualizando BFPARAM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                'ERROR DE ACTUALIZADO
            Else
                Console.WriteLine("BFPARAM MySQL Actualizado")
            End If
        End If

        If op <> "" Then
            Console.WriteLine("Consultando BFPARAM MySQL...")
            parametro(0) = servWin
            parametro(1) = "02"
            parametro(2) = op
            dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 1)
            If dtBFPARAM Is Nothing Then
                data = Nothing
                data = New BFPARAM
                With data
                    .CCID = "CC"
                    .CCCODE = "02"
                    .CCCODE2 = op
                    .CCTABL = servWin
                End With
                Console.WriteLine("Guardando BFPARAM MySQL...")
                continuar = datos.GuardarBFPARAMRow(data, 0)
                If continuar = False Then
                    Console.WriteLine("Error Guardando BFPARAM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                    'ERROR DE GUARDADO
                Else
                    Console.WriteLine("BFPARAM MySQL Guardado")
                End If
            End If
            Console.WriteLine("Consultando BFPARAM MySQL...")
            dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 1)
            If dtBFPARAM IsNot Nothing Then
                wsNum = dtBFPARAM.Rows(0).Item("CCNUM")
                data = Nothing
                data = New BFPARAM
                With data
                    .CCNOT1 = "Inicia Rutina"
                    .CCENDT = Format(Now(), "yyy-MM-dd HH:mm:ss")
                    .CCTABL = servWin
                    .CCCODE = "02"
                    .CCCODE2 = op
                End With
                Console.WriteLine("Actualizando BFPARAM MySQL...")
                continuar = datos.ActualizarBFPARAMRow(data, 1)
                If continuar = False Then
                    Console.WriteLine("Error Actualizando BFPARAM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                    'ERROR DE ACTUALIZADO
                Else
                    Console.WriteLine("BFPARAM MySQL Actualizado")
                End If
            End If
        End If
        Console.WriteLine("Consultando BFPARAM MySQL...")
        parametro(0) = servWin
        parametro(1) = "01"
        dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 2)
        If dtBFPARAM Is Nothing Then
            data = Nothing
            data = New BFPARAM
            With data
                .CCID = "CC"
                .CCCODE = "01"
                .CCTABL = servWin
            End With
            Console.WriteLine("Guardando BFPARAM MySQL...")
            continuar = datos.GuardarBFPARAMRow(data, 1)
            If continuar = False Then
                Console.WriteLine("Error Guardando BFPARAM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
                'ERROR DE GUARDADO
            Else
                Console.WriteLine("BFPARAM MySQL Guardado")
            End If
        Else
            Console.WriteLine(tipo)
            Console.WriteLine(dtBFPARAM.Rows(0).Item("CCUDC1"))
            Console.WriteLine(dtBFPARAM.Rows(0).Item("CCUDV1"))
            Select Case tipo
                Case "Inicio"
                    resultado = False
                    If IsDBNull(dtBFPARAM.Rows(0).Item("CCUDC1")) Then
                        resultado = True
                    ElseIf dtBFPARAM.Rows(0).Item("CCUDC1") = 1 Then
                        resultado = True
                    End If
                Case "Actualiza PR"
                    resultado = False
                    hora = Hour(Now())
                    If IsDBNull(dtBFPARAM.Rows(0).Item("CCUDV1")) Then
                        resultado = True
                    ElseIf hora <> dtBFPARAM.Rows(0).Item("CCUDV1") Then
                        resultado = True
                    End If
            End Select
        End If
        Dim tipoconsu As Integer
        data = Nothing
        data = New BFPARAM
        With data
            .CCUDTS = Format(Now(), "yyy-MM-dd HH:mm:ss")
            tipoconsu = 6
            Select Case tipo
                Case "Inicio"
                    Dim App As clsApp = New clsApp(System.Reflection.Assembly.GetExecutingAssembly)
                    Dim gsAppVersion = App.Major.ToString & "." & App.Minor.ToString & "." & App.Revision.ToString
                    .CCDESC = gsAppVersion
                    .CCNOT2 = Replace("BFPARAM", "\", "\\")
                    .CCNOTE = My.Application.Info.ProductName
                    .CCENDT = Format(Now(), "yyy-MM-dd HH:mm:ss")
                    tipoconsu = 2
                Case "Fin"
                    .CCMNDT = Format(Now(), "yyy-MM-dd HH:mm:ss")
                    tipoconsu = 3
                Case "Operacion"
                    .CCNOT1 = op
                    tipoconsu = 4
                Case "Actualiza PR"
                    If resultado Then
                        .CCUDV1 = hora
                        .CCNOT1 = op
                        tipoconsu = 5
                    End If
            End Select
            .CCTABL = servWin
            .CCCODE = "01"
        End With
        Console.WriteLine("Actualizando BFPARAM MySQL...")
        continuar = datos.ActualizarBFPARAMRow(data, tipoconsu)
        If continuar = False Then
            Console.WriteLine("Error Actualizando BFPARAM MySQL " & datos.mensaje & vbCrLf & datos.fSql)
            'ERROR DE ACTUALIZADO
        Else
            Console.WriteLine("BFPARAM MySQL Actualizado")
        End If
        Console.WriteLine(resultado)
        Return resultado
    End Function

    Function comi(txt) As String
        Return Replace(CStr(txt), "'", "''")
    End Function

End Module
