﻿Imports System.IO
Imports IBM.Data.DB2.iSeries
Imports CLDatos

Public Class wsTareas
    Private WithEvents m_timer As New Timers.Timer(30000)
    Private datos As New ClsDatos
    Private datosas400 As New ClsDatosAS400

    Protected Overrides Sub OnStart(ByVal args() As String)
        Me.EventLog1.WriteEntry("Inicia")
        m_timer.Enabled = True
    End Sub

    Private Sub m_timer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles m_timer.Elapsed
        Try
            m_timer.Enabled = False
            revisaTareas()
            m_timer.Enabled = True
        Catch ex4 As iDB2NullValueException
            Me.EventLog1.WriteEntry(datos.fSql & vbCrLf & datosas400.fSql & vbCrLf & ex4.InnerException.ToString & vbCrLf & ex4.Message & vbCrLf & ex4.StackTrace, EventLogEntryType.Error)
        Catch ex3 As iDB2ConnectionTimeoutException
            Me.EventLog1.WriteEntry(datos.fSql & vbCrLf & datosas400.fSql & vbCrLf & ex3.InnerException.ToString & vbCrLf & ex3.Message & vbCrLf & ex3.StackTrace, EventLogEntryType.Error)
        Catch ex2 As iDB2DCFunctionErrorException
            Me.EventLog1.WriteEntry(datos.fSql & vbCrLf & datosas400.fSql & vbCrLf & ex2.InnerException.ToString & vbCrLf & ex2.Message & vbCrLf & ex2.StackTrace, EventLogEntryType.Error)
        Catch ex1 As iDB2Exception
            Me.EventLog1.WriteEntry(datos.fSql & vbCrLf & datosas400.fSql & vbCrLf & ex1.InnerException.ToString & vbCrLf & ex1.Message & vbCrLf & ex1.StackTrace, EventLogEntryType.Error)
        Catch ex As Exception
            Me.EventLog1.WriteEntry(datos.fSql & vbCrLf & datosas400.fSql & vbCrLf & ex.Message & vbCrLf & ex.StackTrace, EventLogEntryType.Error)
        End Try
    End Sub

    Protected Overrides Sub OnStop()
        m_timer.Stop()
    End Sub

End Class
