﻿Imports MySql.Data.MySqlClient
Imports System.IO
Imports System.Text

Public Class ClsDatos
    Private Cadena As String
    Private ReadOnly conexion As MySqlConnection
    Private adapter As MySqlDataAdapter = New MySqlDataAdapter()
    Private trans As MySqlTransaction
    Private builder As MySqlCommandBuilder
    Private command As MySqlCommand
    Private WithEvents ds As DataSet
    Private fSql As String
    Private numFilas As Integer
    Private continuarguardando As Boolean
    Private dt As DataTable
    Private appPath As String = Path.GetFullPath(My.Application.Info.DirectoryPath & "\Log\")
    'Private errorlocal As String = Replace(appPath & "\error.txt", "\\", "\")
    Public mensaje As String

    Sub New()
        If My.Settings.PRUEBAS = "SI" Then
            Cadena = My.Settings.BASEDATOSLOCAL
        Else
            Cadena = My.Settings.BASEDATOS
        End If
        Try
            conexion = New MySqlConnection(Cadena)
            ' Si el directorio no existe, crearlo
            If Not Directory.Exists(appPath) Then
                Directory.CreateDirectory(appPath)
            End If
        Catch ex As Exception
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\CONEXIONError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo Establecer conexion." & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try
    End Sub

#Region "ACTUALIZAR"

    ''' <summary>
    ''' Actualiza la tabla sequence_data 
    ''' </summary>
    ''' <param name="seq_name">seq_name</param>
    ''' <param name="mensaje">Variable donde se va a guardar mensaje en caso de error</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 29/04/2016</remarks>
    Public Function Actualizarsequence_dataRow(ByVal seq_name As String, ByRef mensaje As String) As Boolean
        continuarguardando = True
        'conexion.Open()
        trans = conexion.BeginTransaction()
        fSql = "UPDATE sequence_data SET sequence_cur_value = If((sequence_cur_value + sequence_increment) > sequence_max_value,If(sequence_cycle = True,sequence_min_value,NULL),sequence_cur_value +sequence_increment) WHERE sequence_name = ?seq_name"
        command = New MySqlCommand()
        command.Connection = conexion
        command.Transaction = trans
        Try
            command.CommandText = fSql
            command.Parameters.Add(New MySqlParameter("?seq_name", seq_name))
            command.ExecuteNonQuery()
        Catch ex As MySqlException
            trans.Rollback()
            continuarguardando = False
            mensaje = "No se pudo actualizar la tabla sequence_data.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje : " + ex.ToString()
        Catch ex As Exception
            trans.Rollback()
            continuarguardando = False
            mensaje = "No se pudo actualizar la tabla sequence_data.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje : " + ex.ToString()
        Finally
            If continuarguardando = True Then
                trans.Commit()
            End If
            'conexion.Close()
        End Try
        Return continuarguardando
    End Function

#End Region

#Region "CONSULTAR"

    ''' <summary>
    ''' Consulta la tabla BFPARAM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por CCTABL(0) y CCCODE LIKE %(1)%</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 29/04/2016</remarks>
    Public Function ConsultarBFPARAMdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        'conexion.Open()
        trans = conexion.BeginTransaction()

        Select Case tipoparametro
            Case 0
                fSql = "SELECT * FROM BFPARAM WHERE CCTABL = '" & parametroBusqueda(0) & "' AND CCCODE LIKE '%" & parametroBusqueda(1) & "%'"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adapter.SelectCommand.Transaction = trans
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        trans.Commit()
        'conexion.Close()
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla ILM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por WWHS(0) y WLOC(1)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 29/04/2016</remarks>
    Public Function ConsultarILMdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        'conexion.Open()
        trans = conexion.BeginTransaction()

        Select Case tipoparametro
            Case 0
                fSql = "SELECT * FROM ILM WHERE WWHS='" & parametroBusqueda(0) & "' AND WLOC='" & parametroBusqueda(1) & "'"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adapter.SelectCommand.Transaction = trans
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        trans.Commit()
        'conexion.Close()
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla sequence_data con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por sequence_name(0) devuelve sequence_cur_value, 1- Busca por sequence_name(0) devuelve sequence_max_value</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 29/04/2016</remarks>
    Public Function Consultarsequence_datadt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        'conexion.Open()
        trans = conexion.BeginTransaction()

        Select Case tipoparametro
            Case 0
                fSql = "SELECT sequence_cur_value FROM sequence_data WHERE sequence_name = '" & parametroBusqueda(0) & "'"
            Case 1
                fSql = "SELECT sequence_max_value FROM SEQUENCE_DATA WHERE sequence_name = '" & parametroBusqueda(0) & "'"
        End Select
        adapter = New MySqlDataAdapter(fSql, conexion)

        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adapter.SelectCommand.Transaction = trans
        builder = New MySqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        trans.Commit()
        'conexion.Close()
        Return dt
    End Function

#End Region

#Region "FUNCIONES"

    ''' <summary>
    ''' Arma cadena de conexion de AS400
    ''' </summary>
    ''' <returns>String con cadena</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 29-04-2016</remarks>
    Public Function CadenaConexionAS400() As String
        Dim cadenaas400 As String = "Data Source={SERVER};Password={PASSWORD};User ID={USER};LibraryList={LIBRARYLIST};SchemaSearchList={LIBRARYLIST};Naming=System"
        'Dim cadenaas400 As String = "Data Source={SERVER};Password={PASSWORD};User ID={USER};LibraryList={LIBRARYLIST};DefaultCollection={DEFAULT}"

        Dim parametro(1) As String
        Dim dtBFPARAM As DataTable

        parametro(0) = "LXLONG"
        parametro(1) = "AS400"
        dtBFPARAM = ConsultarBFPARAMdt(parametro, 0)
        If dtBFPARAM IsNot Nothing Then
            For Each row As DataRow In dtBFPARAM.Rows
                If row("CCCODE") = "AS400_SERVER" Then
                    If My.Settings.PRUEBAS = "SI" Then
                        cadenaas400 = cadenaas400.Replace("{SERVER}", row("CCDESC"))
                    Else
                        cadenaas400 = cadenaas400.Replace("{SERVER}", row("CCNOT1"))
                    End If
                ElseIf row("CCCODE") = "AS400_USER" Then
                    If My.Settings.PRUEBAS = "SI" Then
                        cadenaas400 = cadenaas400.Replace("{USER}", row("CCDESC"))
                    Else
                        cadenaas400 = cadenaas400.Replace("{USER}", row("CCNOT1"))
                    End If
                ElseIf row("CCCODE") = "AS400_PASS" Then
                    If My.Settings.PRUEBAS = "SI" Then
                        cadenaas400 = cadenaas400.Replace("{PASSWORD}", row("CCDESC"))
                    Else
                        cadenaas400 = cadenaas400.Replace("{PASSWORD}", row("CCNOT1"))
                    End If
                ElseIf row("CCCODE") = "AS400_LIB" Then
                    'If My.Settings.PRUEBAS = "SI" Then
                    '    cadenaas400 = cadenaas400.Replace("{DEFAULT}", row("CCDESC"))
                    'Else
                    '    cadenaas400 = cadenaas400.Replace("{DEFAULT}", row("CCNOT1"))
                    'End If
                    cadenaas400 = cadenaas400.Replace("{LIBRARYLIST}", row("CCNOT2"))
                End If
            Next
        Else
            Return ""
        End If

        Return cadenaas400
    End Function

    ''' <summary>
    ''' Arma cadena de conexion de AS400
    ''' </summary>
    ''' <returns>String con cadena</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 29-04-2016</remarks>
    Public Function CadenaConexionoledb() As String
        Dim cadenaas400 As String = "Provider=IBMDA400;Data Source={SERVER};User ID={USER};Password={PASSWORD};Library List={LIBRARYLIST};Force Translate=0;Naming Convention=1;"
        'Dim cadenaas400 As String = "Data Source={SERVER};Password={PASSWORD};User ID={USER};LibraryList={LIBRARYLIST};SchemaSearchList={LIBRARYLIST};Naming=System"
        'Dim cadenaas400 As String = "Data Source={SERVER};Password={PASSWORD};User ID={USER};LibraryList={LIBRARYLIST};DefaultCollection={DEFAULT}"

        Dim parametro(1) As String
        Dim dtBFPARAM As DataTable

        parametro(0) = "LXLONG"
        parametro(1) = "AS400"
        dtBFPARAM = ConsultarBFPARAMdt(parametro, 0)
        If dtBFPARAM IsNot Nothing Then
            For Each row As DataRow In dtBFPARAM.Rows
                If row("CCCODE") = "AS400_SERVER" Then
                    If My.Settings.PRUEBAS = "SI" Then
                        cadenaas400 = cadenaas400.Replace("{SERVER}", row("CCDESC"))
                    Else
                        cadenaas400 = cadenaas400.Replace("{SERVER}", row("CCNOT1"))
                    End If
                ElseIf row("CCCODE") = "AS400_USER" Then
                    If My.Settings.PRUEBAS = "SI" Then
                        cadenaas400 = cadenaas400.Replace("{USER}", row("CCDESC"))
                    Else
                        cadenaas400 = cadenaas400.Replace("{USER}", row("CCNOT1"))
                    End If
                ElseIf row("CCCODE") = "AS400_PASS" Then
                    If My.Settings.PRUEBAS = "SI" Then
                        cadenaas400 = cadenaas400.Replace("{PASSWORD}", row("CCDESC"))
                    Else
                        cadenaas400 = cadenaas400.Replace("{PASSWORD}", row("CCNOT1"))
                    End If
                ElseIf row("CCCODE") = "AS400_LIB" Then
                    'If My.Settings.PRUEBAS = "SI" Then
                    '    cadenaas400 = cadenaas400.Replace("{DEFAULT}", row("CCDESC"))
                    'Else
                    '    cadenaas400 = cadenaas400.Replace("{DEFAULT}", row("CCNOT1"))
                    'End If
                    cadenaas400 = cadenaas400.Replace("{LIBRARYLIST}", row("CCNOT2"))
                End If
            Next
        Else
            Return ""
        End If

        Return cadenaas400
    End Function

    Public Function CalcConsec(ByVal sID As String) As String
        Dim fmt As String
        Dim result As String = ""
        Dim continuar As Boolean
        Dim dtsequence_data As DataTable
        Dim parametro(0) As String
        Dim mensajeError As String = ""
        Try
            continuar = LockTable("sequence_data")
            If continuar = True Then
                parametro(0) = sID
                dtsequence_data = Consultarsequence_datadt(parametro, 0)
                If dtsequence_data IsNot Nothing Then
                    result = dtsequence_data.Rows(0).Item("sequence_cur_value")
                End If
                If result <> "" Then
                    continuar = Actualizarsequence_dataRow(sID, mensajeError)
                    If continuar <> False Then
                        dtsequence_data = Consultarsequence_datadt(parametro, 1)
                        If dtsequence_data IsNot Nothing Then
                            fmt = Replace(dtsequence_data.Rows(0).Item("sequence_max_value"), "9", "0")
                            result = Right(fmt & result, Len(fmt))
                            continuar = UnLockTable()
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\CalcConsecError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("LOG " & ex.ToString)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try

        Return result
    End Function

    ''' <summary>
    ''' Bloquea tabla enviada
    ''' </summary>
    ''' <param name="Table">Tabla a bloquear</param>
    ''' <returns>un booleano</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 29/04/2016</remarks>
    Public Function LockTable(Table As String) As Boolean
        'conexion.Open()
        trans = conexion.BeginTransaction()
        Using sqlCommand As New MySqlCommand()
            With sqlCommand
                .CommandText = "LOCK TABLES " & Table & " LOW_PRIORITY WRITE"
                .Connection = conexion
                .Transaction = trans
                '.Parameters.Add("?Tabla", Table)
            End With
            Try
                sqlCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As MySqlException
                trans.Rollback()
                continuarguardando = False
                'Mensaje = "No se pudo eliminar en la tabla sequence_data.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje : " & ex.ToString
            Catch ex As Exception
                trans.Rollback()
                continuarguardando = False
                'Mensaje = "No se pudo eliminar en la tabla sequence_data.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje : " & ex.ToString
            Finally
                If continuarguardando = True Then
                    trans.Commit()
                    'conexion.Close()
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Bloquea tabla enviada
    ''' </summary>
    ''' <returns>un booleano</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 29/04/2016</remarks>
    Public Function UnLockTable() As Boolean
        continuarguardando = True
        'conexion.Open()
        trans = conexion.BeginTransaction()
        fSql = "UNLOCK TABLES"
        command = New MySqlCommand()
        command.Connection = conexion
        command.Transaction = trans
        Try
            command.CommandText = fSql
            command.ExecuteNonQuery()
        Catch ex As MySqlException
            trans.Rollback()
            continuarguardando = False
            'mensaje = "No se pudo eliminar la tabla Table.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje : " + ex.ToString()
        Catch ex As Exception
            trans.Rollback()
            continuarguardando = False
            'mensaje = "No se pudo eliminar la tabla Table.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje : " + ex.ToString()
        Finally
            If continuarguardando = True Then
                trans.Commit()
            End If
            'conexion.Close()
        End Try
        Return continuarguardando
    End Function

    Public Function AbrirConexion() As Boolean
        Dim abierta As Boolean = False
        Try
            If conexion.State = ConnectionState.Open Then
                abierta = True
            Else
                conexion.Open()
                abierta = True
            End If
        Catch ex1 As MySqlException
            abierta = False
            mensaje = ex1.ToString
        Catch ex As Exception
            abierta = False
            mensaje = ex.ToString
        End Try
        Return abierta
    End Function

    Public Function CerrarConexion() As Boolean
        Dim cerrada As Boolean = False
        Try
            conexion.Close()
            cerrada = True
        Catch ex1 As MySqlException
            cerrada = False
            mensaje = ex1.ToString
        Catch ex As Exception
            cerrada = False
            mensaje = ex.ToString
        End Try
        Return cerrada
    End Function

    Function Ceros(ByVal Val As Object, ByVal C As Integer)
        If CInt(C) > Len(Trim(Val)) Then
            Ceros = Right(RepiteChar(CInt(C), "0") & Trim(Val), CInt(C))
        Else
            Ceros = Val
        End If
    End Function

    Function RepiteChar(ByVal Cant As Integer, ByVal Txt As Char) As String
        Dim i As Integer

        RepiteChar = ""
        For i = 1 To Cant
            RepiteChar = RepiteChar & Txt
        Next
    End Function

    ''' <summary>
    ''' Split de cadena (;)
    ''' </summary>
    ''' <returns>Vector con Cadena</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 02/05/2016</remarks>
    Public Function DetalleCadena() As String()
        Dim detalle() As String
        detalle = Split(Cadena, ";")
        Return detalle
    End Function

    Function ParmItem(ByRef arreglo() As String, ByVal tag As String, ByVal valor As String) As String
        Dim i As Integer = UBound(arreglo)
        ReDim Preserve arreglo(i + 1)
        arreglo(i) = tag & ": " & valor & vbCrLf
        Return tag & ": " & valor & vbCrLf
    End Function

#End Region

#Region "GUARDAR"

    ''' <summary>
    ''' Guarda BFLOG
    ''' </summary>
    ''' <param name="datos">BFLOG</param>
    ''' <returns>true si se guarda con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 26/04/2016</remarks>
    Public Function GuardarBFLOGRow(ByVal datos As BFLOG) As Boolean
        Dim continuarguardandoBFLOG As Boolean

        trans = conexion.BeginTransaction()
        Using sqlCommand As New MySqlCommand()
            With sqlCommand
                .CommandText = "INSERT INTO BFLOG (USUARIO, PROGRAMA, ALERT, EVENTO, LKEY, TXTSQL) VALUES (" &
                    "'" & datos.usuario & "', " &
                    "'" & datos.programa & "', " &
                    datos.alert & ", " &
                    "'" & datos.evento.Replace("'", "''") & "', " &
                    "'" & datos.lkey & "', " &
                    "'" & datos.txtsql.Replace("'", "''") & "')"
                .Connection = conexion
                .Transaction = trans
            End With
            Try
                sqlCommand.ExecuteNonQuery()
                continuarguardandoBFLOG = True
            Catch ex As MySqlException
                trans.Rollback()
                continuarguardandoBFLOG = False
                mensaje = ex.Message
            Catch ex As Exception
                trans.Rollback()
                continuarguardandoBFLOG = False
                mensaje = ex.Message
            Finally
                If continuarguardandoBFLOG = True Then
                    trans.Commit()
                Else
                    Dim sb As New StringBuilder()
                    Dim sw As StreamWriter = New StreamWriter(appPath & "\BFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                    sb.AppendLine("No se pudo guardar en la tabla BFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & mensaje & vbCrLf & fSql)
                    sb.AppendLine(Now().ToString)
                    sw.WriteLine(sb.ToString())
                    sw.Close()
                End If
            End Try
        End Using
        Return continuarguardandoBFLOG
    End Function

#End Region

End Class

Public Class BFLOG
    Public usuario As String
    Public programa As String
    Public alert As String
    Public evento As String
    Public lkey As String
    Public txtsql As String
    Public OPERACION As String
End Class

Public Class CIMWMS
    Public CADENA As String
    Public INID As String
    Public INTRN As String
    Public INLINE As String
    Public INPROD As String
    Public INWHSE As String
    Public INREF As String
    Public INREAS As String
    Public INLOT As String
    Public INLOC As String
    Public INQTY As String
    Public INDATE As String
    Public INTRAN As String
    Public INCBY As String
    Public INCDT As String
    Public INCTM As String
    Public INMLOT As String
    Public INREF2 As String
    Public INCOST As String
    Public INEMCC As String
End Class

Public Class ILN
    Public LPROD As String
    Public LLOT As String
End Class

Public Class ELA
    Public AID As String
    Public ATYPE As String
    Public AORD As String
    Public ALINE As String
    Public ASEQ As String
    Public APROD As String
    Public AWHS As String
    Public ALOC As String
    Public ALOT As String
    Public LQALL As String
End Class

Public Class ECL
    Public LQALL As String
    Public LORD As String
    Public LLINE As String
End Class

Public Class ILIL01
    Public LIALOC As String
    Public LPROD As String
    Public LWHS As String
    Public LLOT As String
    Public LLOC As String

End Class

Public Class ILM
    Public WID As String
    Public WWHS As String
    Public WLOC As String
    Public WFLOC As String
    Public WLTYP As String
    Public WDESC As String
    Public LMPRF As String
End Class

Public Class BFPEDID
    Public ADOCTP As String
    Public ADOCNR As String
    Public APO As String
    Public ANOTE As String
    Public ALINE As String
    Public ACUST As String
    Public ASHIP As String
    Public APROD As String
    Public AQORD As String
    Public AVALOR As String
    Public ARDTE As String
    Public ALNOTE As String
    Public AUSER As String
End Class