﻿Imports IBM.Data.DB2.iSeries
Imports System.Text
Imports System.IO
Imports System.Data.OleDb

Public Class ClsDatosAS400
    Public ReadOnly conexionIBM As New iDB2Connection()
    Public ReadOnly conexionoledb As New OleDbConnection()
    Private adapterIBM As iDB2DataAdapter = New iDB2DataAdapter()
    Private adapterOLEDB As OleDbDataAdapter = New OleDbDataAdapter()
    Private builderOLEDB As OleDbCommandBuilder
    'Private transIBM As iDB2Transaction
    Private builderIBM As iDB2CommandBuilder
    Private commandIBM As iDB2Command
    Private numFilas As Integer
    Private dt As DataTable
    Private WithEvents ds As DataSet
    Private continuarguardando As Boolean
    Private appPath As String = Path.GetFullPath(My.Application.Info.DirectoryPath & "\Log\")
    Public mensaje As String
    Private data
    Private dat As New ClsDatos
    Public fSql As String
    Public Const DB2_DATEDEC = " ( YEAR( NOW() ) * 10000 + MONTH( NOW()  ) * 100 + DAY( NOW() ) )"
    Public Const DB2_TIMEDEC0 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    Public Const DB2_TIMEDEC1 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 ) "

#Region "ACTUALIZAR"

    ''' <summary>
    ''' Actualiza la tabla ILIL01 
    ''' </summary>
    ''' <param name="datos">ILIL01</param>
    ''' <param name="tipo">SET LIALOC = LIALOC + @LIALOC WHERE LPROD = @LPROD AND LWHS = @LWHS AND LLOT = @LLOT AND LLOC = @LLOC"</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 02/05/2016</remarks>
    Public Function ActualizarILIL01Row(ByVal datos As ILIL01, ByVal tipo As Integer) As Boolean
        continuarguardando = True

        commandIBM = New iDB2Command()
        commandIBM.Connection = conexionIBM
        Select Case tipo
            Case 0
                fSql = " UPDATE ILIL01 SET LIALOC = LIALOC + " & CDbl(datos.LIALOC)
                fSql &= " WHERE LPROD='" & datos.LPROD & "' AND LWHS='" & datos.LWHS & "' AND LLOT='" & datos.LLOT & "' AND LLOC='" & datos.LLOC & "'"
        End Select

        Try
            commandIBM.CommandText = fSql
            commandIBM.ExecuteNonQuery()
        Catch ex As iDB2Exception
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ILIL01Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla ILIL01.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ILIL01Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla ILIL01.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                data = Nothing
                data = New BFLOG
                With data
                    .OPERACION = "Actualizar ILIL01 AS400"
                    .PROGRAMA = "WcfBellota"
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                dat.GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla ILN 
    ''' </summary>
    ''' <param name="datos">ILN</param>
    ''' <param name="tipo">SET LMRB = 'A' WHERE LLOT = @LLOT AND LPROD = @LPROD</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 02/05/2016</remarks>
    Public Function ActualizarILNRow(ByVal datos As ILN, ByVal tipo As Integer) As Boolean
        continuarguardando = True

        commandIBM = New iDB2Command()
        commandIBM.Connection = conexionIBM
        Select Case tipo
            Case 0
                fSql = "UPDATE ILN SET LMRB = 'A' WHERE LLOT = '" & datos.LLOT & "' AND LPROD = '" & datos.LPROD & "'"
        End Select

        Try
            commandIBM.CommandText = fSql
            commandIBM.ExecuteNonQuery()
        Catch ex As iDB2Exception
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ILNError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla ILN.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ILNError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla ILN.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                data = Nothing
                data = New BFLOG
                With data
                    .OPERACION = "Actualizar ILN AS400"
                    .PROGRAMA = "WcfBellota"
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                dat.GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Actualiza la tabla ECL 
    ''' </summary>
    ''' <param name="datos">ILN</param>
    ''' <param name="tipo">SET LQALL = LQALL + @LQALL WHERE LORD = @LORD AND LLINE = @LLINE</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 02/05/2016</remarks>
    Public Function ActualizarECLRow(ByVal datos As ECL, ByVal tipo As Integer) As Boolean
        continuarguardando = True

        commandIBM = New iDB2Command()
        commandIBM.Connection = conexionIBM
        Select Case tipo
            Case 0
                fSql = "UPDATE ECL SET LQALL = LQALL + " & CDbl(datos.LQALL) & " WHERE LORD=" & datos.LORD & " AND LLINE=" & datos.LLINE
        End Select

        Try
            commandIBM.CommandText = fSql
            commandIBM.ExecuteNonQuery()
        Catch ex As iDB2Exception
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ECLError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla ECL.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ECLError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla ECL.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                data = Nothing
                data = New BFLOG
                With data
                    .OPERACION = "Actualizar ECL AS400"
                    .PROGRAMA = "WcfBellota"
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                dat.GuardarBFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function


#End Region

#Region "CONSULTAR"

    ''' <summary>
    ''' Consulta la tabla BFPEDID con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por ADOCNR(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 03/05/2016</remarks>
    Public Function ConsultarBFPEDIDdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT * FROM BFPEDID WHERE ADOCNR = '" & parametroBusqueda(0) & "'"
        End Select
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)
        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        Try
            numFilas = adapterIBM.Fill(ds)
        Catch ex As Exception
            mensaje = ex.ToString
        End Try

        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla ECL con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por LORD(0) y LLINE(1)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 02/05/2016</remarks>
    Public Function ConsultarECLdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT LPROD, LWHS FROM ECL WHERE LORD=" & parametroBusqueda(0) & " AND LLINE=" & parametroBusqueda(1)
        End Select
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)
        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla FSOL01 con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por SORD(0) y SPROD(1)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 02/05/2016</remarks>
    Public Function ConsultarFSOL01dt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        Select Case tipoparametro
            Case 0
                fSql = " SELECT * FROM FSOL01 WHERE SORD = " & parametroBusqueda(0) & " AND SPROD = '" & parametroBusqueda(1) & "'"
        End Select
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)
        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla HPOL01 con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por PORD(0) y PPROD(1),
    ''' 1- Busca por PORD(0) y PPROD(1),
    ''' 2- Busca por PORD(0) - PPROD(1) y PQREC menor PQORD ordenado por PLINE,
    ''' 3- Busca por PORD(0) y PPROD(1) retorna PLINE</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 02/05/2016</remarks>
    Public Function ConsultarHPOL01dt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        Select Case tipoparametro
            Case 0
                fSql = " SELECT * "
                fSql &= " FROM HPOL01"
                fSql &= " WHERE PORD = " & parametroBusqueda(0)
                fSql &= " AND PPROD = '" & parametroBusqueda(1) & "'"
            Case 1
                fSql = " SELECT  "
                fSql &= " IFNULL( SUM(PQORD), 0 ) AS PQORD, IFNULL( SUM(PQREC), 0 ) AS PQREC, IFNULL( MAX(PLINE), 0 ) AS MAXLINE "
                fSql &= " FROM HPOL01"
                fSql &= " WHERE PORD=" & parametroBusqueda(0)
                fSql &= " AND PPROD='" & parametroBusqueda(1) & "'"
            Case 2
                fSql = " SELECT  * "
                fSql &= " FROM HPOL01"
                fSql &= " WHERE PORD=" & parametroBusqueda(0)
                fSql &= " AND PPROD='" & parametroBusqueda(1) & "'"
                fSql &= " AND PQREC < PQORD"
                fSql &= " ORDER BY PLINE"
            Case 3
                fSql = " SELECT  "
                fSql &= " PLINE "
                fSql &= " FROM HPOL01"
                fSql &= " WHERE PORD=" & parametroBusqueda(0) & " AND PPROD='" & parametroBusqueda(1) & "'"
        End Select
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)
        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla IIML01 con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por IPROD(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 02/05/2016</remarks>
    Public Function ConsultarIIML01dt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        Select Case tipoparametro
            Case 0
                fSql = " SELECT * FROM  IIML01 WHERE IPROD = '" & parametroBusqueda(0) & "'"
        End Select
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)
        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla ILIL01 con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por LPROD(0) - LWHS(1) - LLOT(2) y LLOC(3)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 02/05/2016</remarks>
    Public Function ConsultarILIL01dt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT "
                fSql &= " LOPB - LISSU + LADJU + LRCT - LIALOC AS SALDO"
                fSql &= " FROM ILIL01"
                fSql &= " WHERE LPROD='" & parametroBusqueda(0) & "' AND LWHS='" & parametroBusqueda(1) & "' AND LLOT='" & parametroBusqueda(2) & "' AND LLOC='" & parametroBusqueda(3) & "'"

        End Select
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)
        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla ILM con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca WWHS(0) y WLOC(1)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 29/04/2016</remarks>
    Public Function ConsultarILMdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT * FROM ILM WHERE WWHS='" & parametroBusqueda(0) & "' AND WLOC='" & parametroBusqueda(1) & "'"
        End Select
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)
        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla IWML01 con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por WWHS(0) y WLOC(1)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 02/05/2016</remarks>
    Public Function ConsultarIWML01dt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT * FROM IWML01 WHERE LWHS ='" & parametroBusqueda(0) & "'"
        End Select
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)
        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla ZPA con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por PKEY = RCOD + (0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 02/05/2016</remarks>
    Public Function ConsultarZPAdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT * FROM ZPA WHERE IDPA ='TR' AND PKEY = 'RCOD" & parametroBusqueda(0) & "'"
        End Select
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)
        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla SYSIBM.SYSDUMMY1 con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 02/05/2016</remarks>
    Public Function ConsultarSYSIBMSYSDUMMY1dt() As DataTable
        dt = Nothing

        fSql = "SELECT NOW(), CURRENT_TIMEZONE  FROM SYSIBM.SYSDUMMY1"

        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)
        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla SYSIBM.SYSDUMMY1 con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 02/05/2016</remarks>
    Public Function ConsultarSYSIBMSYSDUMMY1dtoledb() As DataTable
        dt = Nothing

        fSql = "SELECT NOW(), CURRENT_TIMEZONE  FROM SYSIBM.SYSDUMMY1"

        adapterOLEDB = New OleDbDataAdapter(fSql, conexionoledb)
        adapterOLEDB.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderOLEDB = New OleDbCommandBuilder(adapterOLEDB)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterOLEDB.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterOLEDB = Nothing
        builderOLEDB = Nothing
        Return dt
    End Function

#End Region

#Region "FUNCIONES"

    Public Function AbrirConexion(ByVal cadena As String) As Boolean
        Dim abierta As Boolean = False
        Try
            If conexionIBM.State = ConnectionState.Open Then
                abierta = True
            Else
                conexionIBM.ConnectionString = cadena
                conexionIBM.Open()
                abierta = True
            End If
        Catch ex1 As iDB2Exception
            abierta = False
            mensaje = ex1.ToString
        Catch ex As Exception
            abierta = False
            mensaje = ex.ToString
        End Try
        Return abierta
    End Function

    Public Function AbrirConexionOLEDB(ByVal cadena As String) As Boolean
        Dim abierta As Boolean = False
        Try
            'cadena = "Provider=IBMDA400;Data Source=213.62.216.85;User ID=COSIS001;Password=ZXC09ASD;Library List=PP61BPCSO,CP61BPCSF,CP61BPCESP,PP61BPCPTF,PP61CPEUSR,CP61BPCUSR,CP61BPCUSF,PP61BAMUSF,M6168345C,QGPL,QTEMP;Force Translate=0;Naming Convention=1;"
            If conexionoledb.State = ConnectionState.Open Then
                abierta = True
            Else
                conexionoledb.ConnectionString = cadena
                conexionoledb.Open()
                abierta = True
            End If


            'If conexionIBM.State = ConnectionState.Open Then
            '    abierta = True
            'Else
            '    conexionIBM.ConnectionString = cadena
            '    conexionIBM.Open()
            '    abierta = True
            'End If
        Catch ex5 As OleDbException
            mensaje = ex5.Message
        Catch ex3 As iDB2DCFunctionErrorException
            abierta = False
            mensaje = ex3.InnerException.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionAS400Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo realizar la conexión AS400.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex3.InnerException.ToString & vbCrLf & cadena & vbCrLf & ex3.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Catch ex4 As AccessViolationException
            abierta = False
            mensaje = ex4.InnerException.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionAS400Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo realizar la conexión AS400.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex4.InnerException.ToString & vbCrLf & cadena & vbCrLf & ex4.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Catch ex2 As iDB2InvalidConnectionStringException
            abierta = False
            mensaje = ex2.InnerException.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionAS400Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo realizar la conexión AS400.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex2.InnerException.ToString & vbCrLf & cadena & vbCrLf & ex2.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Catch ex1 As iDB2Exception
            abierta = False
            mensaje = ex1.InnerException.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionAS400Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo realizar la conexión AS400.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex1.InnerException.ToString & vbCrLf & cadena & vbCrLf & ex1.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Catch ex As Exception
            abierta = False
            mensaje = ex.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionAS400Error" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo guardar en la tabla ConexionAS400.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & cadena)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try
        Return abierta
    End Function

    Public Function CerrarConexion() As Boolean
        Dim cerrada As Boolean = False
        Try
            conexionIBM.Close()
            cerrada = True
        Catch ex1 As iDB2Exception
            cerrada = False
            mensaje = ex1.ToString
        Catch ex As Exception
            cerrada = False
            mensaje = ex.ToString
        End Try
        Return cerrada
    End Function

    Public Function CreaAlias(ByVal tabla As String, ByVal aliast As String, ByVal miembro As String) As Boolean
        continuarguardando = False
        'Dim cmd As iDB2Command = New iDB2Command("create alias myschema.fileMbr2 for myschema.myfile(member2)", cn)
        'cmd.ExecuteNonQuery()
        '        ' Now access the second member using the alias we just created
        'cmd.CommandText = "select * from myschema.fileMbr2";
        'iDB2DataReader dr = cmd.ExecuteReader();
        Dim cmd As iDB2Command = New iDB2Command()
        cmd.Connection = conexionIBM
        cmd.CommandText = "create alias " & aliast & " for " & tabla & "(" & miembro & ")"
        cmd.CommandType = CommandType.Text
        Try
            cmd.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As iDB2Exception
            continuarguardando = False
            mensaje = ex.ToString
        Catch ex As Exception
            continuarguardando = False
            mensaje = ex.ToString
        End Try
        Return continuarguardando
    End Function

    Public Function LlamaPrograma(ByVal PGM As String, ByVal Batch As String) As String
        Dim llamado As String = ""
        Dim cmd As iDB2Command = New iDB2Command()
        cmd.Connection = conexionIBM
        cmd.CommandText = String.Format("Call " & PGM & "('{0}')", Batch)
        cmd.CommandType = CommandType.Text
        Try
            cmd.ExecuteReader()
            llamado = "OK"
        Catch ex As iDB2Exception
            llamado = ex.ToString
        Catch ex As Exception
            llamado = ex.ToString
        End Try
        Return llamado
    End Function

    Public Function TransaccionEntrada(ByVal dataTE As TransacEntrada) As String
        Dim Result As String = ""
        Dim sTCOM As String
        Dim fSql As String
        Dim FechaTransaccion As Integer
        Dim chkFecha As Date
        Dim parametro(3) As String
        Dim continuar As Boolean
        Dim cadenaAS400 As String
        Dim dtILM As DataTable
        Dim dtZPA As DataTable
        Dim dtIIML01 As DataTable
        Dim dtFSOL01 As DataTable

        If dataTE.Cantidad <= 0 Then
            Return "9505: Error en Cantidad. "
        End If
        If Len(dataTE.Comentario) > 15 Then
            Return "9510: Comentario excede 15. "
        End If

        If IsDate(Left(dataTE.Fecha, 4) & "-" & Mid(dataTE.Fecha, 5, 2) & "-" & Right(dataTE.Fecha, 2)) Then
            chkFecha = DateSerial(Left(dataTE.Fecha, 4), Mid(dataTE.Fecha, 5, 2), Right(dataTE.Fecha, 2))
            If Year(chkFecha) * 100 + Month(chkFecha) = Year(Now()) * 100 + Month(Now()) Then
                FechaTransaccion = CInt(dataTE.Fecha)
            Else
                Result = "9031 Fecha no corresponde al mes. Use AAAAMMDD. [" & dataTE.Fecha & "]"
            End If
        Else
            Result = "9030 Error en Fecha. Use AAAAMMDD. [" & dataTE.Fecha & "]"
        End If

        dataTE.Bodega = Trim(dataTE.Bodega.ToUpper())
        dataTE.Ubicacion = Trim(dataTE.Ubicacion.ToUpper())
        dataTE.Transaccion = Left(dataTE.Transaccion & " ", 2)

        If Not dat.AbrirConexion() Then
            Return "9091: " & "Error Abriendo Conexion. " & dat.mensaje
        End If

        cadenaAS400 = dat.CadenaConexionAS400()

        If Not AbrirConexion(cadenaAS400) Then
            Return "9091: " & "Error Abriendo Conexion. " & mensaje
        End If
        parametro(0) = dataTE.Bodega
        parametro(1) = dataTE.Ubicacion
        dtILM = ConsultarILMdt(parametro, 0)
        If dtILM Is Nothing Then
            Return "9501 La Ubicación NO Existe. " & dataTE.Bodega & "/" & dataTE.Ubicacion
        End If

        parametro(0) = dataTE.Transaccion & dataTE.CodigoCausa
        dtZPA = ConsultarZPAdt(parametro, 0)
        If dtZPA Is Nothing Then
            Return "9507 Codigo de Causa Incorrecto. " & dataTE.CodigoCausa
        End If

        parametro(0) = dataTE.Producto
        dtIIML01 = ConsultarIIML01dt(parametro, 0)
        If dtIIML01 Is Nothing Then
            Return "9400 No encontro Producto. " & dataTE.Producto
        End If

        If dataTE.Transaccion = "R" Or dataTE.Transaccion = "IT" Then
            parametro(0) = dataTE.Referencia
            parametro(1) = dataTE.Producto
            dtFSOL01 = ConsultarFSOL01dt(parametro, 0)
            If dtFSOL01 Is Nothing Then
                Return "9400 No encontro Orden Fabricacion/Producto. " & dataTE.Referencia & "/" & dataTE.Producto
            End If
        End If

        sTCOM = dat.CalcConsec("WMSTRAS")
        data = Nothing
        data = New ILN
        With data
            .LLOT = dataTE.Lote
            .LPROD = dataTE.Producto
        End With
        continuar = ActualizarILNRow(data, 0)
        If continuar = False Then
            dat.CerrarConexion()
            CerrarConexion()
            Return mensaje & vbCrLf & fSql
        End If

        continuar = CreaAlias("CIM", "QTEMP.CIMWMS", "CIMMBR")
        If continuar = False Then
            dat.CerrarConexion()
            CerrarConexion()
            Return mensaje & vbCrLf & fSql
        End If

        '-------------------------------------------------------------------------------------------------------
        '                   ENTRADA
        '-------------------------------------------------------------------------------------------------------
        data = Nothing
        data = New CIMWMS
        With data
            .INID = "IN"                            '2A    Record Identifier
            .INTRN = sTCOM                          '5P0   INTRN"
            .INREF = dataTE.Referencia              '8P0   Ordene de Compra"
            .INEMCC = dataTE.Comentario             '15A   Item Number"
            .INPROD = dataTE.Producto               '15A   Item Number"
            .INWHSE = dataTE.Bodega                 '2A    Warehouse"
            .INREAS = dataTE.CodigoCausa            '2A    Codigo de Razon"
            .INLOT = dataTE.Lote                    '10A   Lot Number"
            .INLOC = dataTE.Ubicacion               '6A    Location Code"
            .INQTY = dataTE.Cantidad                '11P3  Month to Date Adjustments"
            .INCOST = dataTE.Valor                  '"
            .INDATE = FechaTransaccion              '8P0   Date"
            .INTRAN = dataTE.Transaccion            '2A    Transaction Type"
            .INCBY = dataTE.Usuario                 '10A   Creted by"
            .INCDT = DB2_DATEDEC                    '8P0   Date Created"
            .INCTM = DB2_TIMEDEC0                   '6P0   Time Created"
        End With
        continuar = GuardarCIMWMSRow(data, 2)
        If continuar = False Then
            dat.CerrarConexion()
            CerrarConexion()
            Return mensaje & vbCrLf & fSql
        End If

        Result = LlamaPrograma("BCTRAINV", "")
        'Result = DB.Call_PGM("CALL PGM(" & LibParam("PGMS", "") & "/BCTRAINV)")
        dat.CerrarConexion()
        CerrarConexion()

        If Result = "OK" Then
            Return "0000 Entrada Realizada: " & sTCOM
        Else
            Return Result
        End If

    End Function

    ''' <summary>
    ''' Split de cadena (;)
    ''' </summary>
    ''' <returns>Vector con Cadena</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 02/05/2016</remarks>
    Public Function DetalleCadena() As String()
        Dim detalle() As String
        detalle = Split(conexionIBM.ConnectionString, ";")
        Return detalle
    End Function

#End Region

#Region "GUARDAR"

    ''' <summary>
    ''' Guarda BFPEDID
    ''' </summary>
    ''' <param name="datos">BFPEDID</param>
    ''' <param name="tipo">0- (ADOCTP, ADOCNR, APO, ANOTE, ALINE, ACUST, ASHIP, APROD, AQORD, AVALOR, ARDTE, ALNOTE, AUSER)</param>
    ''' <returns>true si se guarda con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 03/05/2016</remarks>
    Public Function GuardarBFPEDIDRow(ByVal datos As BFPEDID, ByVal tipo As Integer) As Boolean

        Using ibmCommand As New iDB2Command()
            With ibmCommand
                Select Case tipo
                    Case 0
                        fSql = "INSERT INTO BFPEDID (ADOCTP, ADOCNR, APO, ANOTE, ALINE, ACUST, ASHIP, APROD, AQORD, AVALOR, ARDTE, ALNOTE, AUSER) VALUES ("
                        fSql &= "'" & datos.ADOCTP & "', "
                        fSql &= "'" & datos.ADOCNR & "', "
                        fSql &= "'" & datos.APO & "', "
                        fSql &= "'" & datos.ANOTE & "', "
                        fSql &= datos.ALINE & ", "
                        fSql &= datos.ACUST & ", "
                        fSql &= datos.ASHIP & ", "
                        fSql &= "'" & datos.APROD & "', "
                        fSql &= datos.AQORD & ", "
                        fSql &= datos.AVALOR & ", "
                        fSql &= datos.ARDTE & ", "
                        fSql &= "'" & datos.ALNOTE & "', "
                        fSql &= "'" & datos.AUSER & "')"
                End Select
                .CommandText = fSql
                .Connection = conexionIBM
            End With
            Try
                ibmCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As iDB2Exception
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFPEDIDError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla BFPEDID.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Catch ex As Exception
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\BFPEDIDError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla BFPEDID.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Finally
                If continuarguardando = False Then
                    data = Nothing
                    data = New BFLOG
                    With data
                        .OPERACION = "Guardar BFPEDID AS400"
                        .PROGRAMA = "WcfBellota"
                        .EVENTO = mensaje
                        .TXTSQL = fSql
                        .ALERT = 1
                    End With
                    dat.GuardarBFLOGRow(data)
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Guarda CIMWMS
    ''' </summary>
    ''' <param name="datos">CIMWMS</param>
    ''' <param name="tipo">0- (INID, INTRN, INLINE, INPROD, INWHSE, INREF, INREAS, INLOT, INLOC, INQTY, INDATE, INTRAN, INCBY, INCDT, INCTM),
    ''' 1- (INID, INTRN, INLINE, INREF, INMLOT, INREF2, INPROD, INWHSE, INREAS, INLOT, INLOC, INQTY, INCOST, INDATE, INTRAN, INCBY, INCDT, INCTM),
    ''' 2- (INID, INTRN, INREF, INEMCC, INPROD, INWHSE, INREAS, INLOT, INLOC, INQTY, INCOST, INDATE, INTRAN, INCBY, INCDT, INCTM)</param>
    ''' <returns>true si se guarda con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 29/04/2016</remarks>
    Public Function GuardarCIMWMSRow(ByVal datos As CIMWMS, ByVal tipo As Integer) As Boolean

        Using ibmCommand As New iDB2Command()
            With ibmCommand
                Select Case tipo
                    Case 0
                        fSql = "INSERT INTO CIMWMS (INID, INTRN, INLINE, INPROD, INWHSE, INREF, INREAS, INLOT, INLOC, INQTY, INDATE, INTRAN, INCBY, INCDT, INCTM) VALUES (" &
                        "'" & datos.INID & "', " &
                        datos.INTRN & ", " &
                        datos.INLINE & ", " &
                        "'" & datos.INPROD & "', " &
                        "'" & datos.INWHSE & "', " &
                        datos.INREF & ", " &
                        "'" & datos.INREAS & "', " &
                        "'" & datos.INLOT & "', " &
                        "'" & datos.INLOC & "', " &
                        datos.INQTY & ", " &
                        datos.INDATE & ", " &
                        "'" & datos.INTRAN & "', " &
                        "'" & datos.INCBY & "', " &
                        datos.INCDT & ", " &
                        datos.INCTM & ")"
                    Case 1
                        fSql = "INSERT INTO CIMWMS (INID, INTRN, INLINE, INREF, INMLOT, INREF2, INPROD, INWHSE, INREAS, INLOT, INLOC, INQTY, INCOST, INDATE, INTRAN, INCBY, INCDT, INCTM) VALUES ("
                        fSql &= "'" & datos.INID & "', "
                        fSql &= datos.INTRN & ", "
                        fSql &= datos.INLINE & ", "
                        fSql &= datos.INREF & ", "
                        fSql &= "'" & datos.INMLOT & "', "
                        fSql &= datos.INREF2 & ", "
                        fSql &= "'" & datos.INPROD & "', "
                        fSql &= "'" & datos.INWHSE & "', "
                        fSql &= "'" & datos.INREAS & "', "
                        fSql &= "'" & datos.INLOT & "', "
                        fSql &= "'" & datos.INLOC & "', "
                        fSql &= datos.INQTY & ", "
                        fSql &= datos.INCOST & ", "
                        fSql &= datos.INDATE & ", "
                        fSql &= "'" & datos.INTRAN & "', "
                        fSql &= "'" & datos.INCBY & "', "
                        fSql &= datos.INCDT & ", "
                        fSql &= datos.INCTM & ")"
                    Case 2
                        fSql = " INSERT INTO CIMWMS (INID, INTRN, INREF, INEMCC, INPROD, INWHSE, INREAS, INLOT, INLOC, INQTY, INCOST, INDATE, INTRAN, INCBY, INCDT, INCTM) VALUES ( "
                        fSql &= "'" & datos.INID & "', "
                        fSql &= datos.INTRN & ", "
                        fSql &= datos.INREF & ", "
                        fSql &= "'" & datos.INEMCC & "', "
                        fSql &= "'" & datos.INPROD & "', "
                        fSql &= "'" & datos.INWHSE & "', "
                        fSql &= "'" & datos.INREAS & "', "
                        fSql &= "'" & datos.INLOT & "', "
                        fSql &= "'" & datos.INLOC & "', "
                        fSql &= datos.INQTY & ", "
                        fSql &= datos.INCOST & ", "
                        fSql &= datos.INDATE & ", "
                        fSql &= "'" & datos.INTRAN & "', "
                        fSql &= "'" & datos.INCBY & "', "
                        fSql &= datos.INCDT & ", "
                        fSql &= datos.INCTM & ")"
                End Select
                .CommandText = fSql
                .Connection = conexionIBM
            End With
            Try
                ibmCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As iDB2Exception
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\CIMWMSError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla CIMWMS.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Catch ex As Exception
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\CIMWMSError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla CIMWMS.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Finally
                If continuarguardando = False Then
                    data = Nothing
                    data = New BFLOG
                    With data
                        .OPERACION = "Guardar CIMWMS AS400"
                        .PROGRAMA = "WcfBellota"
                        .EVENTO = mensaje
                        .TXTSQL = fSql
                        .ALERT = 1
                    End With
                    dat.GuardarBFLOGRow(data)
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Guarda ELA
    ''' </summary>
    ''' <param name="datos">ELA</param>
    ''' <param name="tipo">0- (AID, ATYPE, AORD, ALINE, ASEQ, APROD, AWHS, ALOC, ALOT, LQALL)</param>
    ''' <returns>true si se guarda con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 02/05/2016</remarks>
    Public Function GuardarELARow(ByVal datos As ELA, ByVal tipo As Integer) As Boolean

        Using ibmCommand As New iDB2Command()
            With ibmCommand
                Select Case tipo
                    Case 0
                        fSql = " INSERT INTO ELA (AID, ATYPE, AORD, ALINE, ASEQ, APROD, AWHS, ALOC, ALOT, LQALL) VALUES ( "
                        fSql &= "'" & datos.AID & "', "
                        fSql &= "'" & datos.ATYPE & "', "
                        fSql &= datos.AORD
                        fSql &= datos.ALINE
                        fSql &= datos.ASEQ
                        fSql &= "'" & datos.APROD & "', "
                        fSql &= "'" & datos.AWHS & "', "
                        fSql &= "'" & datos.ALOC & "', "
                        fSql &= "'" & datos.ALOT & "', "
                        fSql &= datos.LQALL
                End Select
                .CommandText = fSql
                .Connection = conexionIBM
            End With
            Try
                ibmCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As iDB2Exception
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\ELAError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla ELA.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Catch ex As Exception
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\ELAError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla ELA.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Finally
                If continuarguardando = False Then
                    data = Nothing
                    data = New BFLOG
                    With data
                        .OPERACION = "Guardar ELA AS400"
                        .PROGRAMA = "WcfBellota"
                        .EVENTO = mensaje
                        .TXTSQL = fSql
                        .ALERT = 1
                    End With
                    dat.GuardarBFLOGRow(data)
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

    ''' <summary>
    ''' Guarda ILM
    ''' </summary>
    ''' <param name="datos">ILM</param>
    ''' <param name="tipo">0- (WID, WWHS, WLOC, WFLOC, WLTYP, WDESC, LMPRF)</param>
    ''' <returns>true si se guarda con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 02/05/2016</remarks>
    Public Function GuardarILMRow(ByVal datos As ILM, ByVal tipo As Integer) As Boolean

        Using ibmCommand As New iDB2Command()
            With ibmCommand
                Select Case tipo
                    Case 0
                        fSql = " INSERT INTO ILM (WID, WWHS, WLOC, WFLOC, WLTYP, WDESC, LMPRF) VALUES ( "
                        fSql &= "'" & datos.WID & "', "
                        fSql &= "'" & datos.WWHS & "', "
                        fSql &= "'" & datos.WLOC & "', "
                        fSql &= "'" & datos.WFLOC & "', "
                        fSql &= "'" & datos.WLTYP & "', "
                        fSql &= "'" & datos.WDESC & "', "
                        fSql &= "'" & datos.LMPRF & "', "
                End Select
                .CommandText = fSql
                .Connection = conexionIBM
            End With
            Try
                ibmCommand.ExecuteNonQuery()
                continuarguardando = True
            Catch ex As iDB2Exception
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\ILMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla ILM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Catch ex As Exception
                continuarguardando = False
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(appPath & "\ILMError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo guardar en la tabla ILM.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
                mensaje = ex.ToString
            Finally
                If continuarguardando = False Then
                    data = Nothing
                    data = New BFLOG
                    With data
                        .OPERACION = "Guardar ILM AS400"
                        .PROGRAMA = "WcfBellota"
                        .EVENTO = mensaje
                        .TXTSQL = fSql
                        .ALERT = 1
                    End With
                    dat.GuardarBFLOGRow(data)
                End If
            End Try
        End Using
        Return continuarguardando
    End Function

#End Region

End Class

Public Class TransacEntrada
    Public Transaccion As String
    Public Fecha As String
    Public Referencia As Integer
    Public Producto As String
    Public Lote As String
    Public Bodega As String
    Public Ubicacion As String
    Public CodigoCausa As String
    Public Cantidad As Double
    Public Valor As Double
    Public Comentario As String
    Public Usuario As String
End Class
