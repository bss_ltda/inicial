﻿Imports System.ServiceModel

' NOTA: puede usar el comando "Cambiar nombre" del menú contextual para cambiar el nombre de interfaz "IBodega" en el código y en el archivo de configuración a la vez.
<ServiceContract()>
Public Interface IBodega

    <OperationContract()>
    Function Traslado(ByVal dataT As TrasladoData) As String

    <OperationContract()>
    Function Asignacion(ByVal dataA As AsignacionData) As String

    <OperationContract()>
    Function CreaUbicacion(ByVal dataCU As CreaUbicacionData) As String

    <OperationContract()>
    Function EntradaPorCompra(ByVal dataEC As EntradaPorCompraData) As String

    <OperationContract()>
    Function Devolucion_A_Proveedor(ByVal dataDP As Devolucion_A_ProveedorData) As String

    <OperationContract()>
    Function EntradaSobranteProduccion(ByVal dataESP As EntradaSobranteProduccionData) As String

    <OperationContract()>
    Function EntradaSobranteComercializados(ByVal dataESC As EntradaSobranteProduccionData) As String

    <OperationContract()>
    Function EntradaDevolucionClientes(ByVal dataEDC As EntradaSobranteProduccionData) As String

    <OperationContract()>
    Function SalidaConsumoMachete(ByVal dataSCM As EntradaSobranteProduccionData) As String

    <OperationContract()>
    Function SalidaDeLimas(ByVal dataSL As EntradaSobranteProduccionData) As String

    <OperationContract()>
    Function SalidaComercializados(ByVal dataSC As EntradaSobranteProduccionData) As String

    <OperationContract()>
    Function SalidaProductosEnAjuste(ByVal dataSPA As EntradaSobranteProduccionData) As String

    <OperationContract()>
    Function ComercializadosTransformadosEntrada(ByVal dataCTE As EntradaSobranteProduccionData) As String

    <OperationContract()>
    Function ComercializadosTransformadosSalida(ByVal dataCTS As EntradaSobranteProduccionData) As String

    <OperationContract()>
    Function Version() As qVersion

    <OperationContract()>
    Function z_PruebaConexion() As qPruebaConexion

    <OperationContract()>
    Function CreaPedido(ByVal Pedido As String) As String()

End Interface

<DataContract()>
Public Class TrasladoData
    <DataMember()>
    Public Property Producto As String
    <DataMember()>
    Public Property Lote As String
    <DataMember()>
    Public Property Bodega_Desde As String
    <DataMember()>
    Public Property Ubicacion_Desde As String
    <DataMember()>
    Public Property Bodega_Hacia As String
    <DataMember()>
    Public Property Ubicacion_Hacia As String
    <DataMember()>
    Public Property Cantidad As Double
    <DataMember()>
    Public Property OrdenFabricacion As Integer
    <DataMember()>
    Public Property CodigoCausa As String
    <DataMember()>
    Public Property Fecha As String
    <DataMember()>
    Public Property Usuario As String
End Class

<DataContract()>
Public Class AsignacionData
    <DataMember()>
    Public Property Pedido As Integer
    <DataMember()>
    Public Property Linea As Integer
    <DataMember()>
    Public Property Producto As String
    <DataMember()>
    Public Property Bodega As String
    <DataMember()>
    Public Property Ubicacion As String
    <DataMember()>
    Public Property Lote As String
    <DataMember()>
    Public Property Cantidad As Double
    <DataMember()>
    Public Property Usuario As String
End Class

<DataContract()>
Public Class CreaUbicacionData
    <DataMember()>
    Public Property Bodega As String
    <DataMember()>
    Public Property Ubicacion As String
    <DataMember()>
    Public Property Descripcion As String
    <DataMember()>
    Public Property CentroDeCosto As String
End Class

<DataContract()>
Public Class EntradaPorCompraData
    <DataMember()>
    Public Property Fecha As String
    <DataMember()>
    Public Property OrdenCompra As Integer
    <DataMember()>
    Public Property NumeroFactura As String
    <DataMember()>
    Public Property Producto As String
    <DataMember()>
    Public Property Lote As String
    <DataMember()>
    Public Property Bodega As String
    <DataMember()>
    Public Property Ubicacion As String
    <DataMember()>
    Public Property Cantidad As Double
    <DataMember()>
    Public Property Valor As Double
    <DataMember()>
    Public Property Usuario As String
End Class

<DataContract()>
Public Class Devolucion_A_ProveedorData
    <DataMember()>
    Public Property Fecha As String
    <DataMember()>
    Public Property OrdenCompra As Integer
    <DataMember()>
    Public Property Producto As String
    <DataMember()>
    Public Property Lote As String
    <DataMember()>
    Public Property Bodega As String
    <DataMember()>
    Public Property Ubicacion As String
    <DataMember()>
    Public Property Cantidad As Double
    <DataMember()>
    Public Property Valor As Double
    <DataMember()>
    Public Property Usuario As String
End Class

<DataContract()>
Public Class EntradaSobranteProduccionData
    <DataMember()>
    Public Property Fecha As String
    <DataMember()>
    Public Property Referencia As Integer
    <DataMember()>
    Public Property Producto As String
    <DataMember()>
    Public Property Lote As String
    <DataMember()>
    Public Property Bodega As String
    <DataMember()>
    Public Property Ubicacion As String
    <DataMember()>
    Public Property CodigoCausa As String
    <DataMember()>
    Public Property Cantidad As Double
    <DataMember()>
    Public Property Comentario As String
    <DataMember()>
    Public Property Usuario As String
End Class

<DataContract()>
Public Class qVersion
    <DataMember()>
    Public Property ArchivoDeParametros As String
    <DataMember()>
    Public Property Version As String
    <DataMember()>
    Public Property Fecha As String
    <DataMember()>
    Public Property Servidor As String
    <DataMember()>
    Public Property Usuario As String
    <DataMember()>
    Public Property Biblioteca As String
End Class

<DataContract()>
Public Class qPruebaConexion
    <DataMember()>
        Public Property MySQL_Server As String
    <DataMember()>
    Public Property MySQL_database As String
    <DataMember()>
    Public Property AS400 As String
    <DataMember()>
    Public Property Mensaje As String
End Class

<DataContract()>
Public Class CabeceraPedido
    <DataMember()>
    Public Property CustomerCode As String
    <DataMember()>
    Public Property ShiptoCode As String
    <DataMember()>
    Public Property RequestShipDate As String
    <DataMember()>
    Public Property CustomerPurchaseOrderCode As String
    <DataMember()>
    Public Property BidContractId As String
    <DataMember()>
    Public Property OrderNote As String
    <DataMember()>
    Public Property Lines As List(Of DetallePedido)
End Class

<DataContract()>
Public Class DetallePedido
    <DataMember()>
    Public Property ItemCode As String
    <DataMember()>
    Public Property OrderedSellingUnitQty As Double
    <DataMember()>
    Public Property Price As Double
    <DataMember()>
    Public Property LineNote As String
End Class

