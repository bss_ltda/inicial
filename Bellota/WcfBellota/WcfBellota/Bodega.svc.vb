﻿Imports CLDatos

Public Class Bodega
    Implements IBodega
    Private datos As New ClsDatos
    Private datosas400 As New ClsDatosAS400
    Private data

    Public Function Traslado(ByVal dataT As TrasladoData) As String Implements IBodega.Traslado
        dataT.Cantidad.ToString(System.Globalization.CultureInfo.InvariantCulture)
        Dim Result As String = ""
        Dim sTCOM As String
        Dim FechaTransaccion As Integer
        Dim chkFecha As Date
        Dim cadenaAS400 As String
        Dim dtILM As DataTable
        Dim dtILIL01 As DataTable
        Dim parametro(3) As String
        Dim continuar As Boolean

        dataT.Bodega_Desde = Trim(dataT.Bodega_Desde.ToUpper())
        dataT.Ubicacion_Desde = Trim(dataT.Ubicacion_Desde.ToUpper())
        dataT.Bodega_Hacia = Trim(dataT.Bodega_Hacia.ToUpper())
        dataT.Ubicacion_Hacia = Trim(dataT.Ubicacion_Hacia.ToUpper())

        If dataT.Cantidad <= 0 Then
            Return "9505: Error en Cantidad. "
        End If

        Try
            If Not datos.AbrirConexion() Then
                Return "9091: " & "Error Abriendo Conexion. " & datos.mensaje
            End If

            cadenaAS400 = datos.CadenaConexionAS400()

            If Not datosas400.AbrirConexion(cadenaAS400) Then
                Return "9091: " & "Error Abriendo Conexion. " & datos.mensaje
            End If

            parametro(0) = dataT.Bodega_Desde
            parametro(1) = dataT.Ubicacion_Desde
            dtILM = datosas400.ConsultarILMdt(parametro, 0)
            If dtILM Is Nothing Then
                Result = "9501 La Ubicación NO Existe. " & dataT.Bodega_Desde & "/" & dataT.Ubicacion_Desde
            End If

            parametro(0) = dataT.Bodega_Hacia
            parametro(1) = dataT.Ubicacion_Hacia
            dtILM = datosas400.ConsultarILMdt(parametro, 0)
            If dtILM Is Nothing Then
                Result = "9502 La Ubicación NO Existe. " & dataT.Bodega_Hacia & "/" & dataT.Ubicacion_Hacia
            End If

            If IsDate(Left(dataT.Fecha, 4) & "-" & Mid(dataT.Fecha, 5, 2) & "-" & Right(dataT.Fecha, 2)) Then
                chkFecha = DateSerial(Left(dataT.Fecha, 4), Mid(dataT.Fecha, 5, 2), Right(dataT.Fecha, 2))
                If Year(chkFecha) * 100 + Month(chkFecha) = Year(Now()) * 100 + Month(Now()) Then
                    FechaTransaccion = CInt(dataT.Fecha)
                Else
                    Result = "9031 Fecha no corresponde al mes. Use AAAAMMDD. [" & dataT.Fecha & "]"
                End If
            Else
                Result = "9030 Error en Fecha. Use AAAAMMDD. [" & dataT.Fecha & "]"
            End If
            dataT.CodigoCausa = Left(dataT.CodigoCausa, 2)

            If Result = "" Then
                parametro(0) = dataT.Producto
                parametro(1) = dataT.Bodega_Desde
                parametro(2) = dataT.Lote
                parametro(3) = dataT.Ubicacion_Desde
                dtILIL01 = datosas400.ConsultarILIL01dt(parametro, 0)
                If dtILIL01 IsNot Nothing Then
                    If dataT.Cantidad > dtILIL01.Rows(0).Item("SALDO") Then
                        Result = "9503 No hay inventario disponible " & datosas400.fSql
                    End If
                Else
                    Result = "9503 No hay inventario disponible " & datosas400.fSql
                End If
            End If

            If Result <> "" Then
                datosas400.CerrarConexion()
                datos.CerrarConexion()
                Return Result
            End If

            sTCOM = datos.CalcConsec("WMSTRAS")
            data = Nothing
            data = New ILN
            With data
                .LLOT = dataT.Lote
                .LPROD = dataT.Producto
            End With
            continuar = datosas400.ActualizarILNRow(data, 0)
            If continuar = False Then
                datosas400.CerrarConexion()
                datos.CerrarConexion()
                Return datosas400.mensaje
            End If

            continuar = datosas400.CreaAlias("CIM", "QTEMP.CIMWMS", "CIMMBR")
            If continuar = False Then
                datosas400.CerrarConexion()
                datos.CerrarConexion()
                Return datosas400.mensaje
            End If

            '-------------------------------------------------------------------------------------------------------
            '                   DESDE
            '-------------------------------------------------------------------------------------------------------
            data = Nothing
            data = New CIMWMS
            With data
                .INID = "IN"                                '2A    Record Identifier
                .INTRN = sTCOM                              '5P0   INTRN"
                .INLINE = 1                                 '3P0   Line Number"
                .INPROD = dataT.Producto                    '15A   Item Number"
                .INWHSE = dataT.Bodega_Desde                '2A    Warehouse"
                .INREF = dataT.OrdenFabricacion             '8P0   Orden de Fabricacion"
                .INREAS = dataT.CodigoCausa                 '2A    Codigo de Razon"
                .INLOT = dataT.Lote                         '10A   Lot Number"
                .INLOC = dataT.Ubicacion_Desde              '6A    Location Code"
                .INQTY = -dataT.Cantidad                    '11P3  Month to Date Adjustments"
                .INDATE = FechaTransaccion                  '8P0   Date"
                .INTRAN = "T1"                              '2A    Transaction Type
                .INCBY = dataT.Usuario                      '10A   Creted by"
                .INCDT = ClsDatosAS400.DB2_DATEDEC          '8P0   Date Created"
                .INCTM = ClsDatosAS400.DB2_TIMEDEC0         '6P0   Time Created"
            End With
            continuar = datosas400.GuardarCIMWMSRow(data, 0)
            If continuar = False Then
                datosas400.CerrarConexion()
                datos.CerrarConexion()
                Return datosas400.mensaje & vbCrLf & datosas400.fSql
            End If

            '-------------------------------------------------------------------------------------------------------
            '                   HACIA
            '-------------------------------------------------------------------------------------------------------
            data = Nothing
            data = New CIMWMS
            With data
                
                .INID = "IN"                                '2A    Record Identifier
                .INTRN = sTCOM                              '5P0   INTRN"
                .INLINE = 2                                 '3P0   Line Number"
                .INPROD = dataT.Producto                    '15A   Item Number"
                .INWHSE = dataT.Bodega_Hacia                '2A    Warehouse"
                .INREF = dataT.OrdenFabricacion             '8P0   Orden de Fabricacion"
                .INREAS = dataT.CodigoCausa                 '2A    Codigo de Razon"
                .INLOT = dataT.Lote                         '10A   Lot Number"
                .INLOC = dataT.Ubicacion_Hacia              '6A    Location Code"
                .INQTY = dataT.Cantidad                     '11P3  Month to Date Adjustments"
                .INDATE = FechaTransaccion                  '8P0   Date"
                .INTRAN = "T1"                              '2A    Transaction Type
                .INCBY = dataT.Usuario                      '10A   Creted by"
                .INCDT = ClsDatosAS400.DB2_DATEDEC          '8P0   Date Created"
                .INCTM = ClsDatosAS400.DB2_TIMEDEC0         '6P0   Time Created"
            End With
            continuar = datosas400.GuardarCIMWMSRow(data, 0)
            If continuar = False Then
                datosas400.CerrarConexion()
                datos.CerrarConexion()
                Return datosas400.mensaje & vbCrLf & datosas400.fSql
            End If

            Result = datosas400.LlamaPrograma("BCTRAINV", "")
            'Result = DB.Call_PGM("CALL PGM(" & LibParam("PGMS") & "BCTRAINV)")
            datosas400.CerrarConexion()
            datos.CerrarConexion()

            If Result = "OK" Then
                Return "0000 Traslado Realizado: " & sTCOM
            Else
                Return Result
            End If

        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function Asignacion(ByVal dataA As AsignacionData) As String Implements IBodega.Asignacion
        dataA.Cantidad.ToString(System.Globalization.CultureInfo.InvariantCulture)
        Dim Result As String = ""
        Dim parametro(3) As String
        Dim continuar As Boolean
        Dim cadenaAS400 As String
        Dim dtILM As DataTable
        Dim dtECL As DataTable
        Dim dtILIL01 As DataTable

        dataA.Producto = Trim(dataA.Producto.ToUpper())
        dataA.Bodega = Trim(dataA.Bodega.ToUpper())
        dataA.Ubicacion = Trim(dataA.Ubicacion.ToUpper())
        dataA.Lote = Trim(dataA.Lote.ToUpper())

        If dataA.Cantidad <= 0 And dataA.Linea <> -99 Then
            Return "9505: Error en Cantidad. "
        End If

        If Not datos.AbrirConexion() Then
            Return "9091: " & "Error Abriendo Conexion. " & datos.mensaje
        End If

        cadenaAS400 = datos.CadenaConexionAS400()

        If Not datosas400.AbrirConexion(cadenaAS400) Then
            Return "9091: " & "Error Abriendo Conexion. " & datos.mensaje
        End If

        'Ejcuta confirmacion y facturacion
        If dataA.Linea = -99 Then
            Result = ""
            Result = datosas400.LlamaPrograma("BCCONFIR", datos.Ceros(dataA.Pedido, 6))
            'Result = DB.Call_PGM("CALL PGM(" & LibParam("PGMS", "") & "/BCCONFIR) PARM('" & datos.Ceros(dataA.Pedido, 6) & "')")
            If Result = "OK" Then
                Result = "0000 Pedido " & datos.Ceros(dataA.Pedido, 6) & " Confirmado"
            End If
            Return Result
        End If
        parametro(0) = dataA.Bodega
        parametro(1) = dataA.Ubicacion
        dtILM = datosas400.ConsultarILMdt(parametro, 0)
        If dtILM Is Nothing Then
            Result = "9400 La Ubicación NO Existe. " & dataA.Bodega & "/" & dataA.Ubicacion
        End If

        parametro(0) = dataA.Pedido
        parametro(1) = dataA.Linea
        dtECL = datosas400.ConsultarECLdt(parametro, 0)
        If dtECL Is Nothing Then
            Result = "9500 Pedido NO Existe. " & dataA.Pedido & "/" & dataA.Linea
        Else
            If dtECL.Rows(0).Item("LPROD") <> dataA.Producto Then
                Result = "9600 Producto no coincide. " & dtECL.Rows(0).Item("LPROD") & "/" & dataA.Producto
            ElseIf dtECL.Rows(0).Item("LWHS") <> dataA.Bodega Then
                Result = "9700 Bodega NO Coincide. " & dtECL.Rows(0).Item("LWHS") & "/" & dataA.Bodega
            End If
        End If

        If Result = "" Then
            parametro(0) = dataA.Producto
            parametro(1) = dataA.Bodega
            parametro(2) = dataA.Lote
            parametro(3) = dataA.Ubicacion
            dtILIL01 = datosas400.ConsultarILIL01dt(parametro, 0)
            If dtILIL01 Is Nothing Then
                Result = "9503 No hay inventario disponible"
            Else
                If dataA.Cantidad > dtILIL01.Rows(0).Item("SALDO") Then
                    Result = "9503 No hay inventario disponible"
                End If
            End If
        End If

        If Result <> "" Then
            datos.CerrarConexion()
            datosas400.CerrarConexion()
            Return Result
        End If

        '-------------------------------------------------------------------------------------------------------
        '                   ASIGNACION
        '-------------------------------------------------------------------------------------------------------

        data = Nothing
        data = New ILN
        With data
            .LLOT = dataA.Lote
            .LPROD = dataA.Producto
        End With
        continuar = datosas400.ActualizarILNRow(data, 0)
        If continuar = False Then
            datosas400.CerrarConexion()
            datos.CerrarConexion()
            Return datosas400.mensaje & vbCrLf & datosas400.fSql
        End If

        data = Nothing
        data = New ELA
        With data
            .AID = "LA"                     '2A    Record Identifier
            .ATYPE = "C"                    '1A    C=cust order S=shop order
            .AORD = dataA.Pedido            '6P0   Shop order#/Customer order#"
            .ALINE = dataA.Linea            '3P0   Line #"
            .ASEQ = 1                       '3P0   Sequence Number"
            .APROD = dataA.Producto         '15A   Item Number"
            .AWHS = dataA.Bodega            '2A    Warehouse"
            .ALOC = dataA.Ubicacion         '6A    Location"
            .ALOT = dataA.Lote              '10A   Lot #"
            .LQALL = dataA.Cantidad         '11P3  Quantity Allocated
        End With
        continuar = datosas400.GuardarELARow(data, 0)
        If continuar = False Then
            datosas400.CerrarConexion()
            datos.CerrarConexion()
            Return datosas400.mensaje & vbCrLf & datosas400.fSql
        End If
        data = Nothing
        data = New ECL
        With data
            .LQALL = dataA.Cantidad
            .LORD = dataA.Pedido
            .LLINE = data.Linea
        End With
        continuar = datosas400.ActualizarECLRow(data, 0)
        If continuar = False Then
            datosas400.CerrarConexion()
            datos.CerrarConexion()
            Return datosas400.mensaje & vbCrLf & datosas400.fSql
        End If

        data = Nothing
        data = New ILIL01
        With data
            .LIALOC = dataA.Cantidad
            .LPROD = dataA.Producto
            .LWHS = dataA.Bodega
            .LLOT = dataA.Lote
            .LLOC = dataA.Ubicacion
        End With
        continuar = datosas400.ActualizarILIL01Row(data, 0)
        If continuar = False Then
            datosas400.CerrarConexion()
            datos.CerrarConexion()
            Return datosas400.mensaje & vbCrLf & datosas400.fSql
        End If

        Result = "0000 Asignacion Realizada"

        datosas400.CerrarConexion()
        datos.CerrarConexion()
        Return Result
    End Function

    Public Function CreaUbicacion(ByVal dataCU As CreaUbicacionData) As String Implements IBodega.CreaUbicacion
        Dim Result As String = ""
        Dim parametro(3) As String
        Dim continuar As Boolean
        Dim cadenaAS400 As String
        Dim dtILM As DataTable
        Dim dtIWML01 As DataTable

        dataCU.Bodega = Trim(dataCU.Bodega.ToUpper())
        dataCU.Ubicacion = Trim(dataCU.Ubicacion.ToUpper())
        dataCU.Descripcion = Trim(dataCU.Descripcion.ToUpper())
        dataCU.CentroDeCosto = Trim(dataCU.CentroDeCosto.ToUpper())

        If dataCU.Bodega.Length > 2 Then
            Return "9901 Longitud Codigo de Bodega"
        ElseIf dataCU.Ubicacion.Length > 6 Then
            Return "9901 Longitud Ubicacion"
        ElseIf dataCU.Descripcion.Length > 30 Then
            Return "9901 Longitud Descripcion"
        ElseIf dataCU.CentroDeCosto.Length > 10 Then
            Return "9901 Longitud Centro De Costo"
        End If

        If Not datos.AbrirConexion() Then
            Return "9091: " & "Error Abriendo Conexion. " & datos.mensaje
        End If

        cadenaAS400 = datos.CadenaConexionAS400()

        If Not datosas400.AbrirConexion(cadenaAS400) Then
            Return "9091: " & "Error Abriendo Conexion. " & datos.mensaje
        End If

        parametro(0) = dataCU.Bodega
        parametro(1) = dataCU.Ubicacion
        dtILM = datosas400.ConsultarILMdt(parametro, 0)
        If dtILM Is Nothing Then
            Result = "9100 La Ubicación ya Existe."
        End If

        If Result = "" Then
            parametro(0) = dataCU.Bodega
            dtIWML01 = datosas400.ConsultarIWML01dt(parametro, 0)
            If dtIWML01 Is Nothing Then
                Result = "9200 Bodega No Existe."
            End If
        End If

        If Result = "" Then
            data = Nothing
            data = New ILM
            With data
                .WID = "WL"                             '2A    Record ID; WL/WZ
                .WWHS = dataCU.Bodega                   '2A    Warehouse Code"
                .WLOC = dataCU.Ubicacion                '6A    Location Code"
                .WFLOC = "1"                            '1A    Default Forced Loc Flag
                .WLTYP = "P"                            '1A    Location Type
                .WDESC = dataCU.Descripcion             '30A   Description"
                .LMPRF = dataCU.CentroDeCosto           '10A   Profit Center"
            End With
            continuar = datosas400.GuardarILMRow(data, 0)
            If continuar = False Then
                datosas400.CerrarConexion()
                datos.CerrarConexion()
                Return datosas400.mensaje & vbCrLf & datosas400.fSql
            End If

            parametro(0) = dataCU.Bodega
            parametro(1) = dataCU.Ubicacion
            dtILM = datosas400.ConsultarILMdt(parametro, 0)
            If dtILM Is Nothing Then
                Result = "9300 Ubicación NO Creada."
            Else
                Result = "0000 Ubicación Creada."
            End If
        End If

        datosas400.CerrarConexion()
        datos.CerrarConexion()

        Return Result
    End Function

    Public Function EntradaPorCompra(ByVal dataEC As EntradaPorCompraData) As String Implements IBodega.EntradaPorCompra
        dataEC.Cantidad.ToString(System.Globalization.CultureInfo.InvariantCulture)
        dataEC.Valor.ToString(System.Globalization.CultureInfo.InvariantCulture)
        Dim Result As String = ""
        Dim sTCOM As String
        Dim CantidadMax As Double
        Dim CantidadActual As Double
        Dim UltimaLinea As Integer
        Dim FechaTransaccion As Integer
        Dim chkFecha As Date
        Dim parametro(3) As String
        Dim continuar As Boolean
        Dim cadenaAS400 As String
        Dim dtILM As DataTable
        Dim dtHPOL01 As DataTable

        If dataEC.Cantidad <= 0 Then
            Return "9505: Error en Cantidad. "
        End If

        dataEC.Bodega = Trim(dataEC.Bodega.ToUpper())
        dataEC.Ubicacion = Trim(dataEC.Ubicacion.ToUpper())

        If dataEC.NumeroFactura.Trim.Length > 10 Then
            Return "9041 Factura mayor a 10 caracteres"
        End If

        If IsDate(Left(dataEC.Fecha, 4) & "-" & Mid(dataEC.Fecha, 5, 2) & "-" & Right(dataEC.Fecha, 2)) Then
            chkFecha = DateSerial(Left(dataEC.Fecha, 4), Mid(dataEC.Fecha, 5, 2), Right(dataEC.Fecha, 2))
            If Year(chkFecha) * 100 + Month(chkFecha) = Year(Now()) * 100 + Month(Now()) Then
                FechaTransaccion = CInt(dataEC.Fecha)
            Else
                Return "9031 Fecha no corresponde al mes. Use AAAAMMDD. [" & dataEC.Fecha & "]"
            End If
        Else
            Return "9030 Error en Fecha. Use AAAAMMDD. [" & dataEC.Fecha & "]"
        End If

        If Not datos.AbrirConexion() Then
            Return "9091: " & "Error Abriendo Conexion. " & datos.mensaje
        End If

        cadenaAS400 = datos.CadenaConexionAS400()

        If Not datosas400.AbrirConexion(cadenaAS400) Then
            Return "9091: " & "Error Abriendo Conexion. " & datos.mensaje
        End If

        parametro(0) = dataEC.Bodega
        parametro(1) = dataEC.Ubicacion
        dtILM = datosas400.ConsultarILMdt(parametro, 0)
        If dtILM Is Nothing Then
            Result = "9501 La Ubicación NO Existe. " & dataEC.Bodega & "/" & dataEC.Ubicacion
        End If
        If Result = "" Then
            parametro(0) = dataEC.OrdenCompra
            parametro(1) = dataEC.Producto
            dtHPOL01 = datosas400.ConsultarHPOL01dt(parametro, 0)
            If dtHPOL01 Is Nothing Then
                Result = "9541 No se encontró Orden: " & dataEC.OrdenCompra & " Producto: " & dataEC.Producto
            Else
                parametro(0) = dataEC.OrdenCompra
                parametro(1) = dataEC.Producto
                dtHPOL01 = datosas400.ConsultarHPOL01dt(parametro, 1)
                If dtHPOL01 Is Nothing Then
                    Result = "9541 No se encontró Orden: " & dataEC.OrdenCompra & " Producto: " & dataEC.Producto
                Else
                    CantidadMax = dtHPOL01.Rows(0).Item("PQORD") * 1.05
                    CantidadActual = dtHPOL01.Rows(0).Item("PQREC")
                    UltimaLinea = dtHPOL01.Rows(0).Item("MAXLINE")
                    If dtHPOL01.Rows(0).Item("PQREC") + dataEC.Cantidad > dtHPOL01.Rows(0).Item("PQORD") * 1.05 Then
                        Result = "9542 Cantidad recibida excede el limite."
                    End If
                End If
            End If
        End If
        If Result <> "" Then
            datos.CerrarConexion()
            datosas400.CerrarConexion()
            Return Result
        End If

        sTCOM = datos.CalcConsec("WMSTRAS")

        data = Nothing
        data = New ILN
        With data
            .LLOT = dataEC.Lote
            .LPROD = dataEC.Producto
        End With
        continuar = datosas400.ActualizarILNRow(data, 0)
        If continuar = False Then
            datos.CerrarConexion()
            datosas400.CerrarConexion()
            Return datosas400.mensaje & vbCrLf & datosas400.fSql
        End If

        continuar = datosas400.CreaAlias("CIM", "QTEMP.CIMWMS", "CIMMBR")
        If continuar = False Then
            datosas400.CerrarConexion()
            datos.CerrarConexion()
            Return datosas400.mensaje
        End If

        '-------------------------------------------------------------------------------------------------------
        '                   RECEPCION
        '-------------------------------------------------------------------------------------------------------
        parametro(0) = dataEC.OrdenCompra
        parametro(1) = dataEC.Producto
        dtHPOL01 = datosas400.ConsultarHPOL01dt(parametro, 2)
        If dtHPOL01 Is Nothing Then
            Result = "9541 No se encontró Orden: " & dataEC.OrdenCompra & " Producto: " & dataEC.Producto
        End If

        Dim CantidadLinea As Double
        Dim NumLinea As Integer = 1
        Dim ValorUnitario As Double = dataEC.Valor / dataEC.Cantidad

        For Each row As DataRow In dtHPOL01.Rows
            If row("PLINE") = UltimaLinea Then
                CantidadLinea = dataEC.Cantidad
                dataEC.Cantidad = 0
            ElseIf row("PQREC") + dataEC.Cantidad > row("PQORD") Then
                CantidadLinea = row("PQORD") - row("PQREC")
                dataEC.Cantidad -= CantidadLinea
            Else
                CantidadLinea = dataEC.Cantidad
                dataEC.Cantidad = 0
            End If
            If CantidadLinea <> 0 Then
                data = Nothing
                data = New CIMWMS
                With data
                    .INID = "IN"                                '2A    Record Identifier
                    .INTRN = sTCOM                              '5P0   INTRN"
                    .INLINE = NumLinea                          '3P0   Line Number"
                    .INREF = dataEC.OrdenCompra                 '8P0   Orden de Compra"
                    .INMLOT = dataEC.NumeroFactura              '"
                    .INREF2 = row("PLINE")                      '8P0   Linea de la Orden de Compra
                    .INPROD = dataEC.Producto                   '15A   Item Number"
                    .INWHSE = dataEC.Bodega                     '2A    Warehouse"
                    .INREAS = "01"                              '2A    Codigo de Razon
                    .INLOT = dataEC.Lote                        '10A   Lot Number"
                    .INLOC = dataEC.Ubicacion                   '6A    Location Code"
                    .INQTY = CantidadLinea                      '11P3  Month to Date Adjustments"
                    .INCOST = CantidadLinea * ValorUnitario                '"
                    .INDATE = FechaTransaccion                  '8P0   Date"
                    .INTRAN = "UC"                              '2A    Transaction Type
                    .INCBY = dataEC.Usuario                     '10A   Creted by"
                    .INCDT = ClsDatosAS400.DB2_DATEDEC          '8P0   Date Created"
                    .INCTM = ClsDatosAS400.DB2_TIMEDEC0         '6P0   Time Created"
                End With
                continuar = datosas400.GuardarCIMWMSRow(data, 1)
                If continuar = False Then
                    datosas400.CerrarConexion()
                    datos.CerrarConexion()
                    Return datosas400.mensaje & vbCrLf & datosas400.fSql
                End If
            End If
            
            If dataEC.Cantidad = 0 Then
                Exit For
            End If
            NumLinea += 1
        Next
        Result = datosas400.LlamaPrograma("BCTRAINV", "")
        'Result = DB.Call_PGM("CALL PGM(" & LibParam("PGMS", "") & "/BCTRAINV)")

        datos.CerrarConexion()
        datosas400.CerrarConexion()

        If Result = "OK" Then
            Return "0000 Entrada Realizada: " & sTCOM
        Else
            Return Result
        End If
    End Function

    Public Function Devolucion_A_Proveedor(ByVal dataDP As Devolucion_A_ProveedorData) As String Implements IBodega.Devolucion_A_Proveedor
        dataDP.Cantidad.ToString(System.Globalization.CultureInfo.InvariantCulture)
        dataDP.Valor.ToString(System.Globalization.CultureInfo.InvariantCulture)
        Dim Result As String = ""
        Dim sTCOM As String
        Dim lineaOrden As String = "0"
        Dim parametro(3) As String
        Dim continuar As Boolean
        Dim cadenaAS400 As String
        Dim dtILM As DataTable
        Dim dtHPOL01 As DataTable

        dataDP.Bodega = Trim(dataDP.Bodega.ToUpper())
        dataDP.Ubicacion = Trim(dataDP.Ubicacion.ToUpper())

        If dataDP.Cantidad <= 0 Then
            Return "9505: Error en Cantidad. "
        End If

        Dim FechaTransaccion As Integer
        Dim chkFecha As Date

        If IsDate(Left(dataDP.Fecha, 4) & "-" & Mid(dataDP.Fecha, 5, 2) & "-" & Right(dataDP.Fecha, 2)) Then
            chkFecha = DateSerial(Left(dataDP.Fecha, 4), Mid(dataDP.Fecha, 5, 2), Right(dataDP.Fecha, 2))
            If Year(chkFecha) * 100 + Month(chkFecha) = Year(Now()) * 100 + Month(Now()) Then
                FechaTransaccion = CInt(dataDP.Fecha)
            Else
                Return "9031 Fecha no corresponde al mes. Use AAAAMMDD. [" & dataDP.Fecha & "]"
            End If
        Else
            Return "9030 Error en Fecha. Use AAAAMMDD. [" & dataDP.Fecha & "]"
        End If

        If Not datos.AbrirConexion() Then
            Return "9091: " & "Error Abriendo Conexion. " & datos.mensaje
        End If

        cadenaAS400 = datos.CadenaConexionAS400()

        If Not datosas400.AbrirConexion(cadenaAS400) Then
            Return "9091: " & "Error Abriendo Conexion. " & datos.mensaje
        End If

        parametro(0) = dataDP.Bodega
        parametro(1) = dataDP.Ubicacion
        dtILM = datosas400.ConsultarILMdt(parametro, 0)
        If dtILM Is Nothing Then
            Result = "9501 La Ubicación NO Existe. " & dataDP.Bodega & "/" & dataDP.Ubicacion
        End If

        If Result = "" Then
            parametro(0) = dataDP.OrdenCompra
            parametro(1) = dataDP.Producto
            dtHPOL01 = datosas400.ConsultarHPOL01dt(parametro, 3)
            If dtHPOL01 Is Nothing Then
                Result = "9541 No se encontró Orden/Linea"
            Else
                lineaOrden = dtHPOL01.Rows(0).Item("PLINE")
            End If
        End If

        If Result <> "" Then
            datos.CerrarConexion()
            datosas400.CerrarConexion()
            Return Result
        End If

        sTCOM = datos.CalcConsec("WMSTRAS")
        continuar = datosas400.CreaAlias("CIM", "QTEMP.CIMWMS", "CIMMBR")
        If continuar = False Then
            datosas400.CerrarConexion()
            datos.CerrarConexion()
            Return datosas400.mensaje
        End If

        '-------------------------------------------------------------------------------------------------------
        '                   DEVOLUCION
        '-------------------------------------------------------------------------------------------------------
        data = Nothing
        data = New ILN
        With data
            .LLOT = dataDP.Lote
            .LPROD = dataDP.Producto
        End With
        continuar = datosas400.ActualizarILNRow(data, 0)
        If continuar = False Then
            datosas400.CerrarConexion()
            datos.CerrarConexion()
            Return datosas400.mensaje & vbCrLf & datosas400.fSql
        End If

        data = Nothing
        data = New CIMWMS
        With data
            .INID = "IN"                                '2A    Record Identifier
            .INTRN = sTCOM                              '5P0   INTRN"
            .INLINE = 1                                 '3P0   Line Number"
            .INPROD = dataDP.Producto                   '15A   Item Number"
            .INWHSE = dataDP.Bodega                     '2A    Warehouse"
            .INREAS = "01"                              '2A    Codigo de Razon
            .INLOT = dataDP.Lote                        '10A   Lot Number"
            .INLOC = dataDP.Ubicacion                   '6A    Location Code"
            .INQTY = dataDP.Cantidad                    '11P3  Month to Date Adjustments"
            .INCOST = dataDP.Valor                      '"
            .INREF = dataDP.OrdenCompra                 '8P0   Ordene de Compra"
            .INREF2 = lineaOrden                        '8P0   Linea de la Orden de Compra"
            .INDATE = FechaTransaccion                  '8P0   Date"
            .INTRAN = "DC"                              '2A    Transaction Type
            .INCBY = dataDP.Usuario                     '10A   Creted by"
            .INCDT = ClsDatosAS400.DB2_DATEDEC          '8P0   Date Created"
            .INCTM = ClsDatosAS400.DB2_TIMEDEC0         '6P0   Time Created"
        End With
        continuar = datosas400.GuardarCIMWMSRow(data, 1)
        If continuar = False Then
            datosas400.CerrarConexion()
            datos.CerrarConexion()
            Return datosas400.mensaje & vbCrLf & datosas400.fSql
        End If

        continuar = datosas400.LlamaPrograma("BCTRAINV", "")
        If continuar = False Then
            datosas400.CerrarConexion()
            datos.CerrarConexion()
            Return datosas400.mensaje & vbCrLf & datosas400.fSql
        End If
        datosas400.CerrarConexion()
        datos.CerrarConexion()

        'DB.Call_PGM("CALL PGM(" & LibParam("PGMS", "") & "/BCTRAINV)")
        'DB.Close()
        Return "0000 Entrada Realizada: " & sTCOM
    End Function

    Public Function EntradaSobranteProduccion(ByVal dataDP As EntradaSobranteProduccionData) As String Implements IBodega.EntradaSobranteProduccion
        dataDP.Cantidad.ToString(System.Globalization.CultureInfo.InvariantCulture)
        data = Nothing
        data = New TransacEntrada
        With data
            .Transaccion = "R1"
            .Fecha = dataDP.Fecha
            .Referencia = dataDP.Referencia
            .Producto = dataDP.Producto
            .Lote = dataDP.Lote
            .Bodega = dataDP.Bodega
            .Ubicacion = dataDP.Ubicacion
            .CodigoCausa = dataDP.CodigoCausa
            .Cantidad = dataDP.Cantidad
            .Valor = 0
            .Comentario = dataDP.Comentario
            .Usuario = dataDP.Usuario
        End With
        Return datosas400.TransaccionEntrada(data)
    End Function

    Public Function EntradaSobranteComercializados(ByVal dataESC As EntradaSobranteProduccionData) As String Implements IBodega.EntradaSobranteComercializados
        dataESC.Cantidad.ToString(System.Globalization.CultureInfo.InvariantCulture)
        data = Nothing
        data = New TransacEntrada
        With data
            .Transaccion = "ES"
            .Fecha = dataESC.Fecha
            .Referencia = dataESC.Referencia
            .Producto = dataESC.Producto
            .Lote = dataESC.Lote
            .Bodega = dataESC.Bodega
            .Ubicacion = dataESC.Ubicacion
            .CodigoCausa = dataESC.CodigoCausa
            .Cantidad = dataESC.Cantidad
            .Valor = 0
            .Comentario = dataESC.Comentario
            .Usuario = dataESC.Usuario
        End With
        Return datosas400.TransaccionEntrada(data)
    End Function

    Public Function EntradaDevolucionClientes(ByVal dataEDC As EntradaSobranteProduccionData) As String Implements IBodega.EntradaDevolucionClientes
        dataEDC.Cantidad.ToString(System.Globalization.CultureInfo.InvariantCulture)
        data = Nothing
        data = New TransacEntrada
        With data
            .Transaccion = "ED"
            .Fecha = dataEDC.Fecha
            .Referencia = dataEDC.Referencia
            .Producto = dataEDC.Producto
            .Lote = dataEDC.Lote
            .Bodega = dataEDC.Bodega
            .Ubicacion = dataEDC.Ubicacion
            .CodigoCausa = dataEDC.CodigoCausa
            .Cantidad = dataEDC.Cantidad
            .Valor = 0
            .Comentario = dataEDC.Comentario
            .Usuario = dataEDC.Usuario
        End With
        Return datosas400.TransaccionEntrada(data)
    End Function

    Public Function SalidaConsumoMachete(ByVal dataSCM As EntradaSobranteProduccionData) As String Implements IBodega.SalidaConsumoMachete
        dataSCM.Cantidad.ToString(System.Globalization.CultureInfo.InvariantCulture)
        data = Nothing
        data = New TransacEntrada
        With data
            .Transaccion = "SM"
            .Fecha = dataSCM.Fecha
            .Referencia = dataSCM.Referencia
            .Producto = dataSCM.Producto
            .Lote = dataSCM.Lote
            .Bodega = dataSCM.Bodega
            .Ubicacion = dataSCM.Ubicacion
            .CodigoCausa = dataSCM.CodigoCausa
            .Cantidad = dataSCM.Cantidad
            .Valor = 0
            .Comentario = dataSCM.Comentario
            .Usuario = dataSCM.Usuario
        End With
        Return datosas400.TransaccionEntrada(data)
    End Function

    Public Function SalidaDeLimas(ByVal dataSL As EntradaSobranteProduccionData) As String Implements IBodega.SalidaDeLimas
        dataSL.Cantidad.ToString(System.Globalization.CultureInfo.InvariantCulture)
        data = Nothing
        data = New TransacEntrada
        With data
            .Transaccion = "SL"
            .Fecha = dataSL.Fecha
            .Referencia = dataSL.Referencia
            .Producto = dataSL.Producto
            .Lote = dataSL.Lote
            .Bodega = dataSL.Bodega
            .Ubicacion = dataSL.Ubicacion
            .CodigoCausa = dataSL.CodigoCausa
            .Cantidad = dataSL.Cantidad
            .Valor = 0
            .Comentario = dataSL.Comentario
            .Usuario = dataSL.Usuario
        End With
        Return datosas400.TransaccionEntrada(data)
    End Function

    Public Function SalidaComercializados(ByVal dataSC As EntradaSobranteProduccionData) As String Implements IBodega.SalidaComercializados
        dataSC.Cantidad.ToString(System.Globalization.CultureInfo.InvariantCulture)
        data = Nothing
        data = New TransacEntrada
        With data
            .Transaccion = "SC"
            .Fecha = dataSC.Fecha
            .Referencia = dataSC.Referencia
            .Producto = dataSC.Producto
            .Lote = dataSC.Lote
            .Bodega = dataSC.Bodega
            .Ubicacion = dataSC.Ubicacion
            .CodigoCausa = dataSC.CodigoCausa
            .Cantidad = dataSC.Cantidad
            .Valor = 0
            .Comentario = dataSC.Comentario
            .Usuario = dataSC.Usuario
        End With
        Return datosas400.TransaccionEntrada(data)
    End Function

    Public Function SalidaProductosEnAjuste(ByVal dataSPA As EntradaSobranteProduccionData) As String Implements IBodega.SalidaProductosEnAjuste
        dataSPA.Cantidad.ToString(System.Globalization.CultureInfo.InvariantCulture)
        data = Nothing
        data = New TransacEntrada
        With data
            .Transaccion = "SS"
            .Fecha = dataSPA.Fecha
            .Referencia = dataSPA.Referencia
            .Producto = dataSPA.Producto
            .Lote = dataSPA.Lote
            .Bodega = dataSPA.Bodega
            .Ubicacion = dataSPA.Ubicacion
            .CodigoCausa = dataSPA.CodigoCausa
            .Cantidad = dataSPA.Cantidad
            .Valor = 0
            .Comentario = dataSPA.Comentario
            .Usuario = dataSPA.Usuario
        End With
        Return datosas400.TransaccionEntrada(data)
    End Function

    Public Function ComercializadosTransformadosEntrada(ByVal dataCTA As EntradaSobranteProduccionData) As String Implements IBodega.ComercializadosTransformadosEntrada
        dataCTA.Cantidad.ToString(System.Globalization.CultureInfo.InvariantCulture)
        data = Nothing
        data = New TransacEntrada
        With data
            .Transaccion = "R"
            .Fecha = dataCTA.Fecha
            .Referencia = dataCTA.Referencia
            .Producto = dataCTA.Producto
            .Lote = dataCTA.Lote
            .Bodega = dataCTA.Bodega
            .Ubicacion = dataCTA.Ubicacion
            .CodigoCausa = dataCTA.CodigoCausa
            .Cantidad = dataCTA.Cantidad
            .Valor = 0
            .Comentario = dataCTA.Comentario
            .Usuario = dataCTA.Usuario
        End With
        Return datosas400.TransaccionEntrada(data)
    End Function

    Public Function ComercializadosTransformadosSalida(ByVal dataCTS As EntradaSobranteProduccionData) As String Implements IBodega.ComercializadosTransformadosSalida
        dataCTS.Cantidad.ToString(System.Globalization.CultureInfo.InvariantCulture)
        data = Nothing
        data = New TransacEntrada
        With data
            .Transaccion = "IT"
            .Fecha = dataCTS.Fecha
            .Referencia = dataCTS.Referencia
            .Producto = dataCTS.Producto
            .Lote = dataCTS.Lote
            .Bodega = dataCTS.Bodega
            .Ubicacion = dataCTS.Ubicacion
            .CodigoCausa = dataCTS.CodigoCausa
            .Cantidad = dataCTS.Cantidad
            .Valor = 0
            .Comentario = dataCTS.Comentario
            .Usuario = dataCTS.Usuario
        End With
        Return datosas400.TransaccionEntrada(data)
    End Function

    Public Function Version() As qVersion Implements IBodega.Version
        Dim App As clsApp = New clsApp(System.Reflection.Assembly.GetExecutingAssembly)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim parametro(1) As String
        Dim dtBFPARAM As DataTable
        Dim qVer As New qVersion

        If Not datos.AbrirConexion() Then
            qVer.ArchivoDeParametros = "ERROR DE CONEXIÓN"
            qVer.Version = App.FileVersion
            qVer.Fecha = "2016-05-02"
            qVer.Servidor = ""
            qVer.Usuario = ""
            qVer.Biblioteca = ""
            Return qVer
        End If

        parametro(0) = "LXLONG"
        parametro(1) = "AS400"
        dtBFPARAM = datos.ConsultarBFPARAMdt(parametro, 0)
        If dtBFPARAM IsNot Nothing Then

            qVer.ArchivoDeParametros = "BFPARAM"
            qVer.Version = App.FileVersion
            qVer.Fecha = "2016-05-02"
            For Each row As DataRow In dtBFPARAM.Rows
                If row("CCCODE") = "AS400_SERVER" Then
                    qVer.Servidor = row("CCDESC")
                ElseIf row("CCCODE") = "AS400_USER" Then
                    qVer.Usuario = row("CCDESC")
                ElseIf row("CCCODE") = "AS400_LIB_F" Then
                    qVer.Biblioteca = row("CCDESC")
                End If
            Next
            
        End If

        Return (qVer)
    End Function

    'Public Function z_PruebaConexion() As qPruebaConexion Implements IBodega.z_PruebaConexion
    '    Dim App As clsApp = New clsApp(System.Reflection.Assembly.GetExecutingAssembly)
    '    Dim appSettings = ConfigurationManager.AppSettings
    '    Dim qVer As New qPruebaConexion
    '    Dim cadenaAS400 As String
    '    Dim detcadmysql() As String
    '    Dim detmyqsl() As String
    '    Dim detcadas400() As String
    '    Dim detas400() As String
    '    Dim dtSys As DataTable

    '    If Not datos.AbrirConexion() Then
    '        qVer.MySQL_Server = ""
    '        qVer.MySQL_database = ""
    '        qVer.AS400 = ""
    '        qVer.Mensaje = "9091: " & "Error Abriendo Conexion. " & datos.mensaje
    '        Return qVer
    '    End If

    '    cadenaAS400 = datos.CadenaConexionAS400()

    '    If Not datosas400.AbrirConexion(cadenaAS400) Then
    '        qVer.MySQL_Server = ""
    '        qVer.MySQL_database = ""
    '        qVer.AS400 = ""
    '        qVer.Mensaje = "9091: " & "Error Abriendo Conexion. " & datosas400.mensaje & vbCrLf & cadenaAS400
    '        Return qVer
    '    End If

    '    detcadmysql = datos.DetalleCadena
    '    For i = 0 To detcadmysql.Count - 1
    '        detmyqsl = Split(detcadmysql(i), "=")
    '        If detmyqsl(0).ToUpper = "SERVER" Then
    '            qVer.MySQL_Server = detmyqsl(1)
    '        ElseIf detmyqsl(0).ToUpper = "DATABASE" Then
    '            qVer.MySQL_database = detmyqsl(1)
    '        End If
    '    Next

    '    detcadas400 = datosas400.DetalleCadena
    '    For i = 0 To detcadas400.Count - 1
    '        detas400 = Split(detcadas400(i), "=")
    '        If detas400(0).ToUpper = "DATA SOURCE" Then
    '            qVer.AS400 = detas400(1)
    '        End If
    '    Next
    '    dtSys = datosas400.ConsultarSYSIBMSYSDUMMY1dt()
    '    If dtSys IsNot Nothing Then
    '        qVer.Mensaje = String.Format("Hora AS400: {0:yyyy-MM-dd HH:mm:ss}, Zona horaria: {1}", dtSys.Rows(0).Item(0), dtSys.Rows(0).Item(1))
    '    Else
    '        qVer.Mensaje = "error"
    '    End If
    '    datos.CerrarConexion()
    '    datosas400.CerrarConexion()

    '    Return (qVer)
    'End Function

    Public Function z_PruebaConexion() As qPruebaConexion Implements IBodega.z_PruebaConexion
        Dim App As clsApp = New clsApp(System.Reflection.Assembly.GetExecutingAssembly)
        Dim appSettings = ConfigurationManager.AppSettings
        Dim qVer As New qPruebaConexion
        Dim cadenaAS400 As String
        Dim detcadmysql() As String
        Dim detmyqsl() As String
        Dim detcadas400() As String
        Dim detas400() As String
        Dim dtSys As DataTable

        If Not datos.AbrirConexion() Then
            qVer.MySQL_Server = ""
            qVer.MySQL_database = ""
            qVer.AS400 = ""
            qVer.Mensaje = "9091: " & "Error Abriendo Conexion. " & datos.mensaje
            Return qVer
        End If

        cadenaAS400 = datos.CadenaConexionoledb()

        If Not datosas400.AbrirConexionOLEDB(cadenaAS400) Then
            qVer.MySQL_Server = ""
            qVer.MySQL_database = ""
            qVer.AS400 = ""
            qVer.Mensaje = "9091: " & "Error Abriendo Conexion. " & datosas400.mensaje & vbCrLf & cadenaAS400 & vbCrLf & "Conexión: " & datosas400.conexionIBM.ConnectionString
            Return qVer
        End If

        detcadmysql = datos.DetalleCadena
        For i = 0 To detcadmysql.Count - 1
            detmyqsl = Split(detcadmysql(i), "=")
            If detmyqsl(0).ToUpper = "SERVER" Then
                qVer.MySQL_Server = detmyqsl(1)
            ElseIf detmyqsl(0).ToUpper = "DATABASE" Then
                qVer.MySQL_database = detmyqsl(1)
            End If
        Next

        detcadas400 = datosas400.DetalleCadena
        For i = 0 To detcadas400.Count - 1
            detas400 = Split(detcadas400(i), "=")
            If detas400(0).ToUpper = "DATA SOURCE" Then
                qVer.AS400 = detas400(1)
            End If
        Next
        dtSys = datosas400.ConsultarSYSIBMSYSDUMMY1dtoledb()
        If dtSys IsNot Nothing Then
            qVer.Mensaje = String.Format("Hora AS400: {0:yyyy-MM-dd HH:mm:ss}, Zona horaria: {1}", dtSys.Rows(0).Item(0), dtSys.Rows(0).Item(1))
        Else
            qVer.Mensaje = "error"
        End If
        datos.CerrarConexion()
        datosas400.CerrarConexion()

        Return (qVer)
    End Function

    Public Function CreaPedido(ByVal Pedido As String) As String() Implements IBodega.CreaPedido
        Dim pedH As CabeceraPedido
        Dim pedL As DetallePedido
        Dim Usuario As String = "WEBPED"
        Dim numLinea As Integer = 1
        Dim Result() As String
        Dim cmdResult As String = ""
        Dim codErr As String = ""
        Dim cadenaAS400 As String
        Dim continuar As Boolean
        Dim parametro(1) As String
        Dim dtBFPEDID As DataTable
        Dim gsKeyWords As String = ""

        ReDim Result(0)
        Try

            Dim docEntrada As XDocument = XDocument.Parse(Pedido)
            Dim qx = From xe In docEntrada.Elements("Order") Select New With { _
                   .CustomerCode = xe.Element("CustomerCode").Value,
                   .ShiptoCode = xe.Element("ShiptoCode").Value,
                   .RequestShipDate = xe.Element("RequestShipDate").Value,
                   .CustomerPurchaseOrderCode = xe.Element("CustomerPurchaseOrderCode").Value,
                   .BidContractId = xe.Element("BidContractId").Value,
                   .OrderNote = xe.Element("OrderNote").Value
            }

            pedH = New CabeceraPedido
            With pedH
                .CustomerCode = qx.First.CustomerCode
                .ShiptoCode = qx.First.ShiptoCode
                .RequestShipDate = qx.First.RequestShipDate
                .CustomerPurchaseOrderCode = qx.First.CustomerPurchaseOrderCode
                .BidContractId = qx.First.BidContractId
                .OrderNote = qx.First.OrderNote
                .Lines = New List(Of DetallePedido)
            End With

            Dim qd = From xe In docEntrada.Descendants.Elements("Line") Select New With { _
                .ItemCode = xe.Element("ItemCode").Value,
                .OrderedSellingUnitQty = xe.Element("OrderedSellingUnitQty").Value,
                .Price = xe.Element("Price").Value,
                .LineNote = xe.Element("LineNote").Value
            }

            For Each e In qd
                pedL = New DetallePedido
                With pedL
                    .ItemCode = e.ItemCode
                    .OrderedSellingUnitQty = e.OrderedSellingUnitQty
                    .Price = e.Price
                    .LineNote = e.LineNote
                End With
                pedH.Lines.Add(pedL)
            Next

            gsKeyWords = "antes de setconexion"
            gsKeyWords = "PARAMETROS= BFPARAM"
            If Not datos.AbrirConexion() Then
                Result(0) = "9091: " & "Error Abriendo Conexion. " & datos.mensaje
                Return Result
            End If

            cadenaAS400 = datos.CadenaConexionAS400()

            gsKeyWords = "despues de setconexion"
            If Not datosas400.AbrirConexion(cadenaAS400) Then
                Result(0) = "9091: " & "Error Abriendo Conexion. " & datosas400.mensaje & vbCrLf & cadenaAS400
                Return Result
            End If

            parametro(0) = pedH.BidContractId
            dtBFPEDID = datosas400.ConsultarBFPEDIDdt(parametro, 0)
            If dtBFPEDID IsNot Nothing Then
                datos.ParmItem(Result, "8099", "El documento " & pedH.BidContractId & " ya fue procesado. Pedido = " & dtBFPEDID.Rows(0).Item("APEDLX"))
            End If

            For Each e In pedH.Lines
                With e
                    data = Nothing
                    data = New BFPEDID
                    With data
                        .ADOCTP = "W"                                   '1A    Tipo W/E/P
                        .ADOCNR = pedH.BidContractId                    '20A   Documen"
                        .APO = pedH.CustomerPurchaseOrderCode           '15A   Orden de Compra"
                        .ANOTE = pedH.OrderNote                         '50A   Nota al pedido"
                        .ALINE = numLinea                               '4S0   Linea del Docume"
                        .ACUST = pedH.CustomerCode                      '8S0   Cliente"
                        .ASHIP = pedH.ShiptoCode                        '4S0   Punto de Envio"
                        .APROD = e.ItemCode                             '15A   Producto"
                        .AQORD = e.OrderedSellingUnitQty                '11S3  Cantidad"
                        .AVALOR = e.Price                               '14S4  Valor"
                        .ARDTE = pedH.RequestShipDate                   '8S0   Fecha de Requerido"
                        .ALNOTE = e.LineNote                            '50A   Nota a la linea"
                        .AUSER = Usuario                                '10A   Usuario Ingreso"
                    End With
                    continuar = datosas400.GuardarBFPEDIDRow(data, 0)
                    If continuar = False Then
                        datos.ParmItem(Result, "9088", datosas400.fSql)
                        datos.ParmItem(Result, "9089", datosas400.mensaje)
                    Else
                        numLinea += 1
                    End If
                End With
            Next

            If Result(0) = "" Then
                If pedH.CustomerCode = 0 Then
                    datos.ParmItem(Result, "9011", "Falta Cliente")
                ElseIf pedH.BidContractId = "" Then
                    datos.ParmItem(Result, "9040", "Falta Numero de Documento Enlace")
                Else
                    cmdResult = datosas400.LlamaPrograma("BCGENPED", pedH.BidContractId)
                    'cmdResult = DB.Call_PGM("CALL PGM(" & LibParam("PGMS") & "BCGENPED) PARM('" & pedH.BidContractId & "')")
                    If cmdResult = "OK" Then
                        parametro(0) = pedH.BidContractId
                        dtBFPEDID = datosas400.ConsultarBFPEDIDdt(parametro, 0)
                        If dtBFPEDID Is Nothing Then
                            datos.ParmItem(Result, "9088", datosas400.fSql)
                            datos.ParmItem(Result, "9089", datosas400.mensaje)
                            datos.CerrarConexion()
                            datosas400.CerrarConexion()
                            Return Result
                        End If
                        For Each row As DataRow In dtBFPEDID.Rows
                            codErr = row("APEDLX")
                            If row("AERROR") <> 0 Then
                                codErr = 8000 + row("AERROR")
                                datos.ParmItem(Result, codErr.ToString(), "LINEA: " & row("ALINE") & " " & row("ADESER"))
                            End If
                        Next
                    Else
                        datos.ParmItem(Result, "9088", datosas400.fSql)
                        datos.ParmItem(Result, "9088", cmdResult)
                    End If
                End If
            End If

            datos.CerrarConexion()
            datosas400.CerrarConexion()
            If UBound(Result) = 0 Then
                ReDim Result(1)
                Result(0) = "0000: PEDIDO CREADO"
                Result(1) = "NUMERO: " & codErr
            Else
                ReDim Preserve Result(UBound(Result) - 1)
            End If
            Return Result
        Catch ex As Exception
            ReDim Result(3)
            Result(0) = "9088: ERROR!!!!"
            Result(1) = ex.ToString
            Result(2) = datosas400.conexionIBM.ConnectionString
            Result(3) = gsKeyWords
            Return Result
        End Try
    End Function

End Class
