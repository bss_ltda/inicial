﻿Module Module1

    Sub Main()

    End Sub

    Sub revisaTareasEjecucion()
        Dim fSql As String
        Dim pgm, param, exe As String
        Dim rs As New ADODB.Recordset
        Dim sts As Integer
        Dim msgT As String
        Dim tLXmsg As String
        Dim Accion As String
        Dim ejecutar As String = ""
        Dim bRevisando As Boolean = True
        Dim i As Integer = 1

        DB = New ADODB.Connection
        'DB.Open("Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=APLLX;Password=LXAPL;Persist Security Info=True;Convert Date Time To Char=TRUE;Application Name=DFU001;Default Collection=ERPLX834F;Force Translate=0;")
        'DB.Open("Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=DESLXSSA;Password=DIC2013;Persist Security Info=True;Convert Date Time To Char=TRUE;Application Name=TAREAS;Default Collection=DESLX834F;Force Translate=0;")
        DB.Open(My.Settings.SERVIDOR)
        If DB.State = 0 Then
            DB.Close()
            Return
        End If
        AppTarea = My.Settings.TAPP
        If curDia <> Day(Now()) Then
            curDia = Day(Now())
            fSql = " DELETE FROM RFTASK  "
            fSql &= " WHERE "
            fSql &= "     TAPP = '" & AppTarea & "' "
            fSql &= " AND STS1 <> 0   "
            fSql &= " AND TFRECUENCIA = 0  "
            fSql &= " AND DATE(TFPROXIMA) <= DATE(NOW()) - 45 DAYS"
            ExecuteSQL(fSql)

            fSql = " DELETE FROM RFTASKLOG  "
            fSql &= " WHERE "
            fSql &= " DATE(CRTDAT) <= DATE(NOW()) - 5 DAYS"
            ExecuteSQL(fSql)

            Directory.CreateDirectory(My.Application.Info.DirectoryPath & "\Log")
        End If

        If AppTarea = "WMS" Then
            WMSInterfaces("WMS INTERFACE - CENDIS")
            WMSInterfaces("WMA INTERFACE - ALMACEN")
            MMMReporteCSV("REPORTE CSV")
            MMMReporteCSV("REENVIA PENDIENTES")
            bPrimerVez = False
        End If

        fSql = " Select * FROM  RFTASK "
        fSql &= " WHERE "
        fSql &= "     TAPP = '" & AppTarea & "' "
        fSql &= " AND STS1 = 0 "
        fSql &= " AND STS4 = 0 "
        fSql &= " AND THLD = 0 "
        fSql &= " AND TFPROXIMA <= NOW() "

        rs.Open(fSql, DB)
        Do While Not rs.EOF
            tareaNumero = rs("TNUMREG").Value
            Accion = UCase(rs("TTIPO").Value)
            If rs("FWRKID").Value <> 0 Then
                If Accion <> "ULTIMO" Then
                    Accion = "PAQUETE"
                End If
            Else
                Accion = UCase(rs("TTIPO").Value)
            End If

            Select Case Accion
                Case UCase("Shell")
                    pgm = rs("TPGM").Value
                    param = rs("TPRM").Value
                    exe = pgm & " " & param.Replace("|", " ")
                    If rs("TNUMTAREA").Value = 1 Then
                        exe &= " TAREA=" & tareaNumero
                    End If
                    sts = 1
                    msgT = "Comando Enviado."
                    tLXmsg = "ENVIADA"
                    fSql = " UPDATE RFTASK Set "
                    fSql &= "  TLXMSG = '" & tLXmsg & "' "
                    fSql &= ", STS1 = 1"
                    fSql &= ", TUPDDAT = NOW()  "
                    fSql &= ", TMSG = '" & msgT & "'"
                    fSql &= ", TCMD = '" & Replace(exe, "'", "''") & "'"
                    fSql &= " WHERE TNUMREG = " & tareaNumero
                    ExecuteSQL(fSql)
                    EnviaRutina(tareaNumero, exe)
                    Try
                        If bRevisando Then
                            ejecutar &= " call " & exe & vbCrLf
                        Else
                            Shell(exe, AppWinStyle.Hide, False, -1)
                        End If
                    Catch ex As Exception
                        WrtSqlError("TAREAS." & My.Settings.TAPP, ex.Message.Replace("'", "''"))
                        fSql = " UPDATE RFTASK SET "
                        fSql &= "  STS1 = -1 "
                        fSql &= ", TUPDDAT = NOW() "
                        fSql &= ", TMSG = '" & Left("TAREAS." & My.Settings.TAPP & "<br>" & ex.Message.Replace("'", "''"), 5000) & "'"
                        fSql &= ", TCMD = '" & exe.Replace("'", "''") & "'"
                        fSql &= " WHERE TNUMREG = " & tareaNumero
                        ExecuteSQL(fSql)
                    End Try
                Case UCase("Ultimo")
                    exe = "ImprimeDocumentos.exe " & rs("FWRKID").Value
                    sts = 1
                    msgT = "Paquete Enviado."
                    tLXmsg = "ENVIADO"
                    fSql = " UPDATE RFTASK SET "
                    fSql &= "  TLXMSG = '" & tLXmsg & "' "
                    fSql &= ", STS1 = 1 "
                    fSql &= ", TUPDDAT = NOW()  "
                    fSql &= ", TMSG = '" & msgT & "'"
                    fSql &= " WHERE TNUMREG = " & tareaNumero
                    ExecuteSQL(fSql)
                    EnviaRutina(tareaNumero, exe)
                    Try
                        If bRevisando Then
                            ejecutar &= exe & vbCrLf
                        Else
                            Shell(exe, AppWinStyle.Hide, False, -1)
                        End If
                    Catch ex As Exception
                        WrtSqlError("TAREAS." & My.Settings.TAPP, ex.Message.Replace("'", "''"))
                        fSql = " UPDATE RFTASK SET "
                        fSql &= "  STS1 = -1 "
                        fSql &= ", TUPDDAT = NOW() "
                        fSql &= ", TMSG = '" & Left("TAREAS." & My.Settings.TAPP & "<br>" & ex.Message.Replace("'", "''"), 5000) & "'"
                        fSql &= ", TCMD = '" & exe.Replace("'", "''") & "'"
                        fSql &= " WHERE TNUMREG = " & tareaNumero
                        ExecuteSQL(fSql)
                    End Try
                Case UCase("Rutina")
                    pgm = Trim(rs("TPGM").Value)
                    If Left(pgm, 1) <> """" Then
                        pgm = """" & pgm & """"
                    End If
                    'Ejecutable de rutinas con parametro TNUMREG de RFTASK
                    exe = pgm & " " & tareaNumero
                    msgT = "Comando Enviado."
                    tLXmsg = "ENVIADA"
                    fSql = " UPDATE RFTASK SET "
                    fSql &= "  TLXMSG = '" & tLXmsg & "' "
                    fSql &= ", STS1 = 1"
                    fSql &= ", TUPDDAT = NOW() "
                    fSql &= ", TMSG = '" & msgT & "'"
                    fSql &= ", TCMD = '" & exe.Replace("'", "''") & "'"
                    fSql &= " WHERE TNUMREG = " & tareaNumero
                    ExecuteSQL(fSql)
                    EnviaRutina(tareaNumero, exe)

                    Try
                        If bRevisando Then
                            ejecutar &= exe & vbCrLf
                        Else
                            Shell(exe, AppWinStyle.Hide, False, -1)
                        End If
                    Catch ex As Exception
                        WrtSqlError("TAREAS." & My.Settings.TAPP, ex.Message.Replace("'", "''"))
                        fSql = " UPDATE RFTASK SET "
                        fSql &= "  STS1 = -1 "
                        fSql &= ", TUPDDAT = NOW() "
                        fSql &= ", TMSG = '" & Left("TAREAS." & My.Settings.TAPP & "<br>" & ex.Message.Replace("'", "''"), 5000) & "'"
                        fSql &= ", TCMD = '" & exe.Replace("'", "''") & "'"
                        fSql &= " WHERE TNUMREG = " & tareaNumero
                        ExecuteSQL(fSql)
                    End Try
                Case UCase("Java")

            End Select
            rs.MoveNext()
            If i Mod 500 = 0 Then
                notepad(ejecutar, i)
                ejecutar = ""
            End If
            i += 1

        Loop
        rs.Close()
        DB.Close()




    End Sub

    Sub WMSInterfaces(qInterface As String)
        Dim rs As New ADODB.Recordset
        Dim fSql, vSql As String

        fSql = " SELECT TNUMREG, STS1, STS4, "
        Select Case qInterface
            Case "WMS INTERFACE - CENDIS"
                fSql &= "  CASE WHEN NOW() + " & getLXParam("WMS_FRECUENCIA", "CCUDC2") & " MINUTES > TFPROXIMA THEN 1 ELSE 0 END AS EN2MINUTOS "
                fSql &= ", CASE WHEN TFPROXIMA > NOW() + 20 MINUTES THEN 1 ELSE 0 END AS REPROGRAM "
            Case "WMA INTERFACE - ALMACEN"
                fSql &= "  CASE WHEN NOW() + " & getLXParam("WMA_FRECUENCIA", "CCUDC2") & " MINUTES > TFPROXIMA THEN 1 ELSE 0 END AS EN2MINUTOS "
                fSql &= ", CASE WHEN TFPROXIMA > NOW() + 20 MINUTES THEN 1 ELSE 0 END AS REPROGRAM "
        End Select
        fSql &= ", CASE WHEN NOW() - 20 MINUTES > TFPROXIMA THEN 1 ELSE 0 END AS BLOQUEADA "
        fSql &= " FROM RFTASK "
        fSql &= " WHERE TAPP = 'WMS' AND TCATEG = '" & qInterface & "' "
        fSql &= " AND  TSUBCAT = 'ACTUAL' "
        rs = ExecuteSQL(fSql)
        If Not rs.EOF Then
            If rs("EN2MINUTOS").Value = 1 Or rs("REPROGRAM").Value = 1 Then
                If rs("STS4").Value = 0 Or bPrimerVez Then
                    If rs("STS1").Value = 1 Or bPrimerVez Then
                        fSql = "UPDATE RFTASK SET"
                        fSql &= "  STS1 = 0, STS4 = 0 "
                        Select Case qInterface
                            Case "WMS INTERFACE - CENDIS"
                                fSql &= ", TFPROXIMA = NOW() + " & getLXParam("WMS_FRECUENCIA", "CCUDC2") & " MINUTES"
                                fSql &= ", TFRECUENCIA = " & getLXParam("WMS_FRECUENCIA", "CCUDC2")
                            Case "WMA INTERFACE - ALMACEN"
                                fSql &= ", TFPROXIMA = NOW() + " & getLXParam("WMA_FRECUENCIA", "CCUDC2") & " MINUTES"
                                fSql &= ", TFRECUENCIA = " & getLXParam("WMA_FRECUENCIA", "CCUDC2")
                        End Select
                        fSql &= " WHERE TNUMREG = " & rs("TNUMREG").Value
                        ExecuteSQL(fSql)
                    End If
                End If
            End If
        Else
            fSql = " INSERT INTO RFTASK( "
            vSql = " VALUES ( "
            fSql &= " TAPP      , " : vSql &= "'WMS', "     '//15A   
            fSql &= " TCATEG    , " : vSql &= "'" & qInterface & "', "     '//25A   Categoria
            fSql &= " TSUBCAT   , " : vSql &= "'ACTUAL', "     '//20A   Categoria
            fSql &= " TTIPO     , " : vSql &= "'Rutina', "     '//22A   Tipo
            fSql &= " TPGM      , " : vSql &= "'" & getLXParam("RUTINAS_WMS") & "', "     '//502A  Programa
            fSql &= " TCRTUSR   ) " : vSql &= "'SISTEMA' ) "     '//10A   Usuario
            ExecuteSQL(fSql & vSql)

            rs.Requery()
            tareaNumero = rs("TNUMREG").Value

            fSql = "UPDATE RFTASK SET"
            fSql &= "  STS1 = 0, STS4 = 0 "
            Select Case qInterface
                Case "WMS INTERFACE - CENDIS"
                    fSql &= ", TFPROXIMA = NOW() + 10 SECONDS "
                    fSql &= ", TFRECUENCIA = " & getLXParam("WMS_FRECUENCIA", "CCUDC2")
                Case "WMA INTERFACE - ALMACEN"
                    fSql &= ", TFPROXIMA = NOW() + 10 SECONDS "
                    fSql &= ", TFRECUENCIA = " & getLXParam("WMA_FRECUENCIA", "CCUDC2")
            End Select
            fSql &= " WHERE TNUMREG = " & tareaNumero
            ExecuteSQL(fSql)
        End If
        rs.Close()

    End Sub

    Sub MMMReporteCSV(mmOperacion As String)
        Dim fSql As String
        Dim vSql As String
        Dim rs As ADODB.Recordset

        Select Case mmOperacion
            Case "REPORTE CSV"
                If DateDiff(DateInterval.Minute, fecHoraMM1, Now()) >= 2 Then
                    fecHoraMM1 = Now()
                Else
                    Return
                End If
            Case "REENVIA PENDIENTES"
                If DateDiff(DateInterval.Minute, fecHoraMM2, Now()) >= 60 Then
                    fecHoraMM2 = Now()
                Else
                    Return
                End If
        End Select

        fSql = " SELECT TNUMREG, STS1 FROM RFTASK WHERE TAPP = 'WMS' AND TCATEG = 'MMM' AND TSUBCAT = '" & mmOperacion & "' "
        rs = ExecuteSQL(fSql)
        If rs.EOF Then
            fSql = " INSERT INTO RFTASK( "
            vSql = " VALUES ( "
            fSql &= " TAPP      , " : vSql &= "'WMS', "
            fSql &= " TCATEG    , " : vSql &= "'MMM', "
            fSql &= " TSUBCAT   , " : vSql &= "'" & mmOperacion & "', "
            fSql &= " TTIPO     , " : vSql &= "'Rutina', "
            fSql &= " STS1      , " : vSql &= " 1, "
            fSql &= " TPGM      , " : vSql &= "'" & getLXParam("RUTINAS_WMS") & "', "     '//502A  Programa
            fSql &= " TCRTUSR   ) " : vSql &= "'SISTEMA' ) "
            ExecuteSQL(fSql & vSql)
        End If
        rs.Requery()
        fSql = "UPDATE RFTASK SET STS1 = 0, STS4 = 0 WHERE TNUMREG = " & rs("TNUMREG").Value
        ExecuteSQL(fSql)
        rs.Close()

    End Sub
    Sub WrtSqlError(sql As String, Descrip As String)
        Dim conn As New ADODB.Connection
        Dim fSql As String
        Dim vSql As String

        Dim gsConnString As String = "Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=APLLX;Password=LXAPL;Persist Security Info=True;Default Collection=ERPLX834F;Force Translate=0;"

        Try
            Dim logArchivo As String = My.Application.Info.DirectoryPath & "\Log\Log-" & Now().ToString("MMddHH") & ".txt"
            Using writer As StreamWriter =
                New StreamWriter(logArchivo, True)
                writer.WriteLine(Now().ToString("yyyy-MM-dd HH:mm:ss"))
            End Using
            Using writer As StreamWriter =
                New StreamWriter(logArchivo, True)
                writer.WriteLine(lastSQL)
            End Using
            Using writer As StreamWriter =
                New StreamWriter(logArchivo, True)
                writer.WriteLine("-".PadRight(60, "-"))
            End Using
            Using writer As StreamWriter =
                New StreamWriter(logArchivo, True)
                writer.WriteLine(sql)
            End Using
            Using writer As StreamWriter =
                New StreamWriter(logArchivo, True)
                writer.WriteLine("-".PadRight(60, "-"))
            End Using
            Using writer As StreamWriter =
                New StreamWriter(logArchivo, True)
                writer.WriteLine(Descrip)
            End Using
            Using writer As StreamWriter =
                New StreamWriter(logArchivo, True)
                writer.WriteLine("-".PadRight(60, "-"))
            End Using
            Using writer As StreamWriter =
                New StreamWriter(logArchivo, True)
                writer.WriteLine(Descrip)
            End Using
            Using writer As StreamWriter =
                New StreamWriter(logArchivo, True)
                writer.WriteLine("=".PadRight(60, "="))
            End Using

            conn.Open(gsConnString)
            fSql = " UPDATE RFTASK SET "
            fSql &= " TLXMSG = 'ERROR', "
            fSql &= " STS1 = -1, "
            fSql &= " TUPDDAT = NOW() , "
            fSql &= " TMSG = '" & Descrip & "'"
            fSql &= " WHERE TNUMREG = " & tareaNumero
            conn.Execute(fSql)

            fSql = " INSERT INTO RFLOG( "
            vSql = " VALUES ( "
            fSql &= " LKEY      , " : vSql &= "'TAREAS." & My.Settings.TAPP & "', "
            fSql &= " USUARIO   , " : vSql &= "'" & System.Net.Dns.GetHostName() & "', "      '//502A
            fSql &= " PROGRAMA  , " : vSql &= "'" & System.Reflection.Assembly.GetExecutingAssembly.FullName & "', "      '//502A
            fSql &= " ALERT     , " : vSql &= "1, "      '//1P0
            fSql &= " EVENTO    , " : vSql &= "'" & Replace(Descrip, "'", "''") & "', "      '//20002A
            fSql &= " TXTSQL    ) " : vSql &= "'" & Replace(lastSQL.Replace("'", "''") & "<br>" & sql, "'", "''") & "' ) "      '//5002A
            conn.Execute(fSql & vSql)
            conn.Close()

        Catch ex As Exception
        End Try

    End Sub

    Function getLXParam(prm As String) As String
        Return getLXParam(prm, "")
    End Function

    Function getLXParam(prm As String, Campo As String) As String
        Dim rs As New ADODB.Recordset
        Dim resultado As String = ""
        Dim fSql As String

        If Campo = "" Then
            Campo = "CCNOT1"
        End If
        fSql = "SELECT " & Campo & " AS DATO FROM RFPARAM WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER('" & prm & "')"
        rs.Open(fSql, Me.DB)
        If Not rs.EOF Then
            resultado = rs("DATO").Value
        End If
        rs.Close()
        Return resultado.Trim

    End Function

    Public Function ExecuteSQL(ByVal sql As String) As ADODB.Recordset
        Dim txtError As String
        Try
            sql = CStr(sql)
            lastSQL = sql
            ExecuteSQL = DB.Execute(sql, regA)
        Catch ex As Exception
            txtError = ex.ToString.Replace("'", "''")
            WrtSqlError(CStr(sql), txtError)
            End
        End Try

    End Function

    Sub notepad(txt As String, consec As Integer)
        Dim logArchivo As String = My.Application.Info.DirectoryPath & "\Log\Log-" & Now().ToString("MMddHH") & consec & ".txt"
        Using writer As StreamWriter =
                New StreamWriter(logArchivo, True)
            writer.WriteLine(txt)
        End Using
        Shell("""C:\Program Files (x86)\Notepad++\notepad++.exe"" " & logArchivo, AppWinStyle.MaximizedFocus, -1)

        'Shell """C:\Program Files (x86)\Notepad++\notepad++.exe"" " & sArchivo, 1

    End Sub

    Sub EnviaRutina(tarea As String, exe As String)
        Dim fSql, vSql As String

        fSql = " INSERT INTO ERPLX834F.RFTASKLOG( "
        vSql = " VALUES ( "
        fSql &= " TTIPO     , " : vSql &= "'WINSERVICE', "     '//8B    
        fSql &= " TTAREA    , " : vSql &= " " & tarea & ", "     '//8B    
        fSql &= " TDESDE    , " : vSql &= "'" & System.Net.Dns.GetHostName() & "<br>" & Me.qVersion & "', "     '//502A  
        fSql &= " TCMD      ) " : vSql &= "'" & exe & "' ) "     '//5002A 
        DB.Execute(fSql & vSql)


    End Sub


End Module
