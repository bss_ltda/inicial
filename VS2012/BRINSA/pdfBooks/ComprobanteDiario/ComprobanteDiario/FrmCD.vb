﻿Imports CLDatos
Imports PDF_In_The_BoxCtl
Imports System.IO
Imports System.Text

Public Class FrmCD
    Private datos As New ClsDatosAS400
    Private flag As Integer
    Private bColor As Boolean
    Private FechaImpresion
    Private Referencia As String
    Private Errores As String
    Private NumPagina
    Private data
    Private CARPETA_SITIO As String
    Private CARPETA_IMG As String
    Private Control As String
    Private Consecutivo As String
    Private tarea As String
    Private bPRUEBAS As Boolean
    Private PDF_Folder As String
    Private LOG_File As String
    Private Comprobante
    Private Periodo
    Private Book
    Private LOGO As String
    Private NOMEMP As String
    Private NIT As String

    Private Sub FrmCD_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim campoParam As String = ""
        Dim aSubParam() As String
        Try
            flag = 0

            Environment.GetCommandLineArgs()
            For Each parametro In Environment.GetCommandLineArgs()
                aSubParam = Split(parametro, "=")
                Select Case UCase(aSubParam(0))
                    Case "TAREA"
                        TAREA = aSubParam(1)
                    Case "COMPROBANTE"
                        Comprobante = aSubParam(1)
                    Case "CONSECUTIVO"
                        Consecutivo = aSubParam(1)
                    Case "REFERENCIA"
                        Referencia = aSubParam(1)
                    Case "FECHA"
                        FechaImpresion = aSubParam(1)
                    Case "BOOK"
                        Book = aSubParam(1)
                    Case "PERIODO"
                        Periodo = aSubParam(1)
                End Select
            Next

            If Not datos.AbrirConexion() Then
                Application.Exit()
            End If

            If datos.Piloto.ToUpper() = "SI" Then
                campoParam = "CCNOT2"
            End If
            CARPETA_SITIO = datos.ConsultarRFPARAMString("SITIO_CARPETA", campoParam, "Finanzas16")
            CARPETA_IMG = datos.ConsultarRFPARAMString("CARPETA_IMG", campoParam, "Finanzas16")
            LOGO = datos.ConsultarRFPARAMString("LOGO", campoParam, "Finanzas16")
            NOMEMP = datos.ConsultarRFPARAMString("NOMBRE", campoParam, "Finanzas16")
            NIT = datos.ConsultarRFPARAMString("NIT", campoParam, "Finanzas16")

            datos.wrtLog("CARPETA_SITIO" & CARPETA_SITIO, "111")
            datos.wrtLog("Periodo" & Periodo, "111")
            datos.wrtLog("Comprobante" & Comprobante, "111")
            datos.wrtLog("Consecutivo" & Consecutivo, "111")
            datos.wrtLog("Referencia" & Referencia, "111")
            datos.wrtLog("FechaImpresion" & FechaImpresion, "111")

            bPRUEBAS = datos.Piloto.ToUpper() = "SI"
            PDF_Folder = CARPETA_IMG & "pdf\Libros\"
            Try
                If Not Directory.Exists(PDF_Folder) Then
                    Directory.CreateDirectory(PDF_Folder)
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\DirectorioError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo crear Directorio." & vbCrLf & ex.Message)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try

            LOG_File = PDF_Folder & "LD" & Control & ".log"
            datos.WrtTxtError(LOG_File, Control)

            Balancee()
            datos.CerrarConexion()
            Application.Exit()
        Catch ex As Exception
            datos.wrtLogHtml("", ex.Message.ToString)
            datos.CerrarConexion()
            Application.Exit()
        End Try
    End Sub

    Sub Balancee()
        Dim rs As New ADODB.Recordset
        Dim rs2 As New ADODB.Recordset
        Dim rs3 As New ADODB.Recordset
        Dim ArchivoPDF As String
        Dim parametro(3) As String
        Dim continuar As Boolean

        ArchivoPDF = PDF_Folder & "ComproDiario" & Periodo & Comprobante & ".pdf"
        With AxPdfBox1
            .FileName = ArchivoPDF
            .Title = "Libro Diario - "
            .WantShow = bPRUEBAS
            .WantPageCount = True
            .PaperSizeName = "Letter"
            .Orientation = TPrinterOrientation.poLandscape
            .AutoPageFeed = True
            .BeginDoc()
            Const NUM_FORMAT As String = "#,##0.00"
            Dim b As TBoxBand
            data = Nothing
            data = New RFTASK
            With data
                .TLXMSG = "EJECUTANDO"
                .TMSG = "Generando Comprobante Diario"
                .TNUMREG = tarea
            End With
            If datos.Piloto = "NO" Then
                continuar = datos.ActualizarRFTASKRow(data, 0)
            Else
                continuar = True
            End If
            If continuar = False Then
                'ERROR DE ACTUALIZADO
            Else
                parametro(0) = Periodo
                parametro(1) = Comprobante
                parametro(2) = Book
                rs2 = datos.ConsultarBSSFFNCOMrs(parametro, 0)
                datos.wrtLog(datos.fSql, "111")
                If rs2 Is Nothing Then
                    datos.WrtTxtError(LOG_File, "Factura no se encontro.")
                    datos.wrtLog("no existe", "111")
                    data = Nothing
                    data = New RFTASK
                    With data
                        .TLXMSG = "EJECUTADA"
                        .TMSG = "Comprobante Diario no Existe"
                        .STS2 = "1"
                        .TNUMREG = tarea
                    End With
                    If datos.Piloto = "NO" Then
                        continuar = datos.ActualizarRFTASKRow(data, 1)
                    End If
                    If continuar = False Then
                        'ERROR DE ACTUALIZADO
                    End If
                    Exit Sub
                End If
            End If
            rs2.MoveFirst()
            While Not rs2.EOF
                datos.wrtLog("While", "111")
                b = .CreateBand("BorderStyle=none;Margins=130,200,100,180")
                b.Height = 50
                b.Put()

                b = .CreateBand("BorderStyle=none;Margins=130,200,100,180")
                b.ChildrenStyle = "BorderStyle=rect;Alignment=Left"
                b.Height = 50
                b.CreateCell(637, "BorderStyle=none;FontSize=8;Alignment=Left").CreateText("BorderRightMargin=10").Assign("Comprobante: " & rs2("DCOMPRO").Value)
                b.CreateCell(639, "BorderStyle=none;FontSize=8;Alignment=Left").CreateText("BorderRightMargin=10").Assign("Nombre Evento: " & rs2("DEVENOM").Value)
                b.CreateCell(637, "BorderStyle=none;FontSize=8;Alignment=Left").CreateText("BorderRightMargin=10").Assign("Evento: " & rs2("DEVENUM").Value)
                b.CreateCell(637, "BorderStyle=none;FontSize=8;Alignment=Left").CreateText("BorderRightMargin=10").Assign("")
                b.Put()

                b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
                b.ChildrenStyle = "BorderStyle=rect"
                b.Height = 50
                b.CreateCell(200, "BorderStyle=rect;FontSize=8;FontBold=1;Alignment=Center;VertAlignment=Center").CreateText("BorderRightMargin=10").Assign("DOCUMENTO")
                b.CreateCell(250, "BorderStyle=rect;FontSize=8;FontBold=1;Alignment=Center;VertAlignment=Center").CreateText("BorderRightMargin=10").Assign("CUENTA")
                b.CreateCell(550, "BorderStyle=rect;FontSize=8;FontBold=1;Alignment=Center;VertAlignment=Center").CreateText("BorderRightMargin=10").Assign("DESCRIPCIÓN")
                b.CreateCell(550, "BorderStyle=rect;FontSize=8;FontBold=1;Alignment=Center;VertAlignment=Center").CreateText("BorderRightMargin=10").Assign("CONCEPTO")
                b.CreateCell(300, "BorderStyle=rect;FontSize=8;FontBold=1;Alignment=Center;VertAlignment=Center").CreateText("BorderRightMargin=10").Assign("TERCERO")
                b.CreateCell(250, "BorderStyle=rect;FontSize=8;FontBold=1;Alignment=Center;VertAlignment=Center").CreateText("BorderRightMargin=10").Assign("COD TERCERO")
                b.CreateCell(150, "BorderStyle=rect;FontSize=8;FontBold=1;Alignment=Center;VertAlignment=Center").CreateText("BorderRightMargin=10").Assign("NIT")
                b.CreateCell(150, "BorderStyle=rect;FontSize=8;FontBold=1;Alignment=Center;VertAlignment=Center").CreateText("BorderRightMargin=10").Assign("DÉBITO")
                b.CreateCell(150, "BorderStyle=rect;FontSize=8;FontBold=1;Alignment=Center;VertAlignment=Center").CreateText("BorderRightMargin=10").Assign("CRÉDITO")
                b.Put()

                b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
                b.Height = 2
                b.Put()

                parametro(0) = rs2("DCOMPRO").Value
                parametro(1) = Periodo
                parametro(2) = Comprobante
                parametro(3) = Book
                rs = datos.ConsultarBSSFFNCOMrs(parametro, 1)
                datos.wrtLog("Sentencia 2" & datos.fSql, "111")

                Dim Ta As TBoxTable
                Ta = .CreateTable("BorderStyle=Bottom")
                Ta.Assign(rs)

                Dim Detail As TBoxBand
                Detail = Ta.CreateBand()
                Detail.Role = TBandRole.rlDetail
                Detail.ChildrenStyle = "FontSize=6;Fontname=Arial; VertAlignment=Center"
                Detail.Height = 40
                Detail.ChildrenStyle = "FontSize=6;Fontname=Arial; VertAlignment=Center;BorderStyle=LeftRight"
                Detail.CreateCell(200, "BorderStyle=Left").CreateText("Alignment=Left;BorderLeftMargin=3").Bind("DDOCUM")
                Detail.CreateCell(250, "BorderStyle=Left").CreateText("Alignment=Left;BorderLeftMargin=3").Bind("DCUENTA")
                Detail.CreateCell(550, "BorderStyle=Left").CreateText("Alignment=Left;BorderLeftMargin=3").Bind("DDESS02")
                Detail.CreateCell(550, "BorderStyle=Left").CreateText("Alignment=Left;BorderLeftMargin=3").Bind("DCONCEP")
                Detail.CreateCell(300, "BorderStyle=Left").CreateText("Alignment=Left;BorderLeftMargin=3").Bind("DTERNOM")
                Detail.CreateCell(250, "BorderStyle=Left").CreateText("Alignment=Left;BorderLeftMargin=3").Bind("DTERCOD")
                Detail.CreateCell(150, "BorderStyle=Left").CreateText("Alignment=Left;BorderLeftMargin=3").Bind("DTERNIT")
                With Detail.CreateCell(150, "BorderStyle=Left").CreateText("Alignment=Right;BorderRightMargin=3")
                    .Bind("DDEBITO")
                    .Format = NUM_FORMAT
                End With
                With Detail.CreateCell(150, "BorderStyle=LeftRight").CreateText("Alignment=Right;BorderRightMargin=3")
                    .Bind("DCREDITO")
                    .Format = NUM_FORMAT
                End With

                Detail.Breakable = True
                Ta.Put()

                parametro(0) = rs2("DCOMPRO").Value
                parametro(1) = Periodo
                parametro(2) = Comprobante
                parametro(3) = Book
                rs3 = datos.ConsultarBSSFFNCOMrs(parametro, 2)

                b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
                b.ChildrenStyle = "BorderStyle=rect"
                b.Height = 50
                b.CreateCell(2250, "BorderStyle=Top;FontSize=6;FontBold=1").CreateText("VertAlignment=Center;Alignment=Right;BorderRightMargin=10").Assign("Totales")

                With b.CreateCell(150, "BorderStyle=Left").CreateText("VertAlignment=Center;Alignment=Right;BorderRightMargin=3")
                    .Assign(CStr((rs3("DEBITO").Value)))
                    .Format = "#,##0.00"
                End With
                With b.CreateCell(150, "BorderStyle=LeftRight").CreateText("VertAlignment=Center;Alignment=Right;BorderRightMargin=3")
                    .Assign(CStr((rs3("CREDITO").Value)))
                    .Format = "#,##0.00"
                End With
                b.Put()
                rs.Close()
                rs2.MoveNext()
            End While
            b = .CreateBand("BorderStyle=none;Margins=130,200,100,180")
            b.Height = 100
            b.Put()

            data = Nothing
            data = New RFTASK
            With data
                .TLXMSG = "EJECUTADA"
                .TMSG = "Comprobante Diario Generado No Paginas: " & NumPagina
                .TURL = Replace(ArchivoPDF, CARPETA_SITIO, "\")
                .STS2 = "1"
                .TNUMREG = tarea
            End With
            If datos.Piloto = "NO" Then
                continuar = datos.ActualizarRFTASKRow(data, 2)
            End If
            If continuar = False Then
                'ERROR DE ACTUALIZADO
            End If
            rs2.Close()
            .EndDoc()
        End With
    End Sub

    Private Sub Balance_BeforePutBand(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_BeforePutBandEvent) Handles AxPdfBox1.BeforePutBand
        If e.aBand.Role = TBandRole.rlDetail Then
            If bColor And flag = 0 Then
                e.aBand.BrushColor = RGB(215, 215, 215)
            Else
                e.aBand.BrushColor = RGB(255, 255, 255)
            End If
            bColor = Not bColor
        End If

        Debug.Print(AxPdfBox1.PenY)

Manejo_Error:
        If Err.Number <> 0 Then
            datos.WrtTxtError(LOG_File, "BeforePutBand")
            End
        End If
    End Sub

    Private Sub Balance_OnBottomOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnBottomOfPageEvent) Handles AxPdfBox1.OnBottomOfPage
        Dim b As TBoxBand
        With AxPdfBox1
            If e.lastPage Then
                b = .CreateBand("Normal;BorderStyle=None;Margins=130,200,100,180")
                b.ChildrenStyle = "BorderStyle=rect;Alignment=Center;FontSize=12"
                b.Height = 50
                b.CreateCell(1040).CreateText().Assign("")
                b.CreateCell(480).CreateText("FontBold=1").Assign("Fin del Informe")
                'b.CreateCell(480).CreateImage("Alignment=Right;VertAlignment=Top").Assign(My.Application.Info.DirectoryPath + "\Fin del Informe.png")
                b.CreateCell(1030).CreateText().Assign("")
                b.Put()
                If AxPdfBox1.PageCount Mod 2 = 0 Then
                    FinDocumento()
                End If
            End If
            NumPagina = NumPagina + 1
        End With
    End Sub

    Private Sub Balance_OnTopOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnTopOfPageEvent) Handles AxPdfBox1.OnTopOfPage
        Dim b As TBoxBand
        With AxPdfBox1
            .DefineStyle("Normal", "0; Margins=130,200,100,150; FontSize=6;Fontname=Arial;BorderColor=Black")
            .Style = "Normal"

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=12;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(1200).CreateText("Alignment=Left;FontSize=12;FontBold=1").Assign("COMPROBANTE DIARIO")
            b.CreateCell(1000).CreateText("Alignment=Right;FontSize=8;FontColor=Red").Assign(Referencia.ToString)
            b.CreateCell(100).CreateText("Alignment=Left;FontSize=8;FontColor=Blue").Assign(Consecutivo + AxPdfBox1.PageCount)
            b.CreateCell(200).CreateText("Alignment=Right;FontSize=8").Assign(FechaImpresion.ToString)
            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.Height = 50
            b.ChildrenStyle = "Normal;FontSize=8;Fontname=Arial;BorderColor=Black;BorderStyle=None;Alignment=Center;VertAlignment=Bottom"
            b.CreateCell(400).CreateText("Alignment=Left;FontSize=8;FontBold=1").Assign(NOMEMP.ToString)
            b.CreateCell(2100).CreateText().Assign(" ")
            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=8;Fontname=Arial;BorderColor=Black;BorderStyle=None;Alignment=Left"
            b.CreateCell(300).CreateText().Assign(NIT.ToString)
            b.CreateCell(2200).CreateText().Assign(" ")
            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=8;Fontname=Arial;BorderColor=Black;BorderStyle=None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(150).CreateText("Alignment=Left;FontSize=8").Assign("PERIODO")
            b.CreateCell(350).CreateText("Alignment=Center;FontName=Arial; FontSize=8; FontBold=1").Assign(Periodo.ToString)
            b.CreateCell(120).CreateText("Alignment=Left;FontSize=8").Assign("LIBRO")
            b.CreateCell(300).CreateText("FontName=Arial; FontSize=8;FontBold=1").Assign(Book.ToString)
            b.CreateCell(1264).CreateText("Alignment=Left;VertAlignment=Center").Assign(" ")
            b.CreateCell(316).CreateImage("Alignment=Right;VertAlignment=Center").Assign(LOGO)
            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.Height = 20
            b.Put()

        End With
    End Sub

    Sub FinDocumento()
        Dim b As TBoxBand
        With Me.AxPdfBox1
            AxPdfBox1.NewPage()
            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "BorderStyle=rect;Alignment=Center;FontSize=12"
            b.Height = 50
            b.CreateCell(1040).CreateText().Assign("")
            b.CreateCell(480).CreateText("FontBold=1").Assign("Fin del Informe")
            'b.CreateCell(480).CreateImage("Alignment=Right;VertAlignment=Top").Assign(My.Application.Info.DirectoryPath & "\Fin del Informe.png")
            b.CreateCell(1030).CreateText().Assign("")
            b.Put()
        End With
    End Sub

End Class
