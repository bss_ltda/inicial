﻿Imports CLDatos
Imports PDF_In_The_BoxCtl
Imports System.IO
Imports System.Text

Public Class FrmMyB
    Private datos As New ClsDatosAS400
    Private flag As Integer
    Private bColor As Boolean
    Private FechaImpresion
    Private Referencia As String
    Private Errores As String
    Private NumPagina
    Private data
    Private CARPETA_SITIO As String
    Private CARPETA_IMG As String
    Private Control As String
    Private Consecutivo As String
    Private tarea As String
    Private bPRUEBAS As Boolean
    Private PDF_Folder As String
    Private LOG_File As String
    Private LOGO As String
    Private NOMEMP As String
    Private NIT As String
    Private PGM400 As String
    Private AAAA As String
    Private PERIODO As String
    Private LIBRO As String
    Private AMBIENTE As String

    Private Sub FrmMyB_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim campoParam As String = ""
        Dim aSubParam() As String
        Dim continuar As Boolean
        Try
            flag = 0
            NumPagina = 1
            Environment.GetCommandLineArgs()
            For Each parametro In Environment.GetCommandLineArgs()
                aSubParam = Split(parametro, "=")
                Select Case UCase(aSubParam(0))
                    Case "TAREA"
                        tarea = aSubParam(1)
                    Case "IDREPORTE"
                        Control = aSubParam(1)
                    Case "CONSECUTIVO"
                        Consecutivo = aSubParam(1)
                    Case "REFERENCIA"
                        Referencia = aSubParam(1)
                    Case "FECHA"
                        FechaImpresion = aSubParam(1)
                    Case "PGM400"
                        PGM400 = aSubParam(1)
                    Case "AAAA"
                        AAAA = aSubParam(1)
                    Case "PERIODO"
                        PERIODO = aSubParam(1)
                    Case "LIBRO"
                        LIBRO = aSubParam(1)
                    Case "AMBIENTE"
                        AMBIENTE = aSubParam(1)
                End Select
            Next

            If Not datos.AbrirConexion() Then
                Application.Exit()
            End If

            Try
                'LLAMA PROGRAMA AS400
                datos.LlamaPrograma(PGM400, AAAA & "', '" & PERIODO & "', '" & Control & "', '" & LIBRO & "', '" & AMBIENTE)
            Catch ex As Exception
                data = Nothing
                data = New RFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .OPERACION = "Llamar Programa"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = ex.Message
                    .TXTSQL = datos.fSql
                    .ALERT = 1
                End With
                continuar = datos.GuardarRFLOGRow(data)
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\ProgramaError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo llamar al programa." & vbCrLf & ex.Message)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try

            If datos.Piloto = "SI" Then
                campoParam = "CCNOT2"
            End If
            CARPETA_SITIO = datos.ConsultarRFPARAMString("SITIO_CARPETA", campoParam, "Finanzas16")
            CARPETA_IMG = datos.ConsultarRFPARAMString("CARPETA_IMG", campoParam, "Finanzas16")
            LOGO = datos.ConsultarRFPARAMString("LOGO", campoParam, "Finanzas16")
            NOMEMP = datos.ConsultarRFPARAMString("NOMBRE", campoParam, "Finanzas16")
            NIT = datos.ConsultarRFPARAMString("NIT", campoParam, "Finanzas16")
            bPRUEBAS = datos.Piloto.ToUpper() = "SI"
            PDF_Folder = CARPETA_IMG & "pdf\Libros\"
            Try
                If Not Directory.Exists(PDF_Folder) Then
                    Directory.CreateDirectory(PDF_Folder)
                End If
            Catch ex As Exception
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(datos.appPath & "\DirectorioError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
                sb.AppendLine("No se pudo crear Directorio." & vbCrLf & ex.Message)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            End Try

            LOG_File = PDF_Folder & "LD" & Control & ".log"

            Balancee()
            datos.CerrarConexion()
            Application.Exit()
        Catch ex As Exception
            Errores = ex.Message.ToString
            datos.wrtLogHtml("", Errores)
            datos.CerrarConexion()
            Application.Exit()
        End Try
    End Sub

    Sub Balancee()
        Dim rs As New ADODB.Recordset
        Dim ArchivoPDF As String
        Dim parametro(0) As String
        Dim continuar As Boolean

        data = Nothing
        data = New RFTASK
        With data
            .TLXMSG = "EJECUTANDO"
            .TMSG = "Generando Libro Mayor y Balances"
            .TNUMREG = tarea
        End With
        If datos.Piloto = "NO" Then
            continuar = datos.ActualizarRFTASKRow(data, 0)
        Else
            continuar = True
        End If
        If continuar = False Then
            'ERROR DE ACTUALIZADO
        Else
            parametro(0) = Control
            rs = datos.ConsultarBSSFFNBALrs(parametro, 0)
            If rs Is Nothing Then
                datos.WrtTxtError(LOG_File, "Factura no se encontro.")
                data = Nothing
                data = New RFTASK
                With data
                    .TLXMSG = "EJECUTADA"
                    .TMSG = "Libro Mayor y Banances no Existe"
                    .STS2 = "1"
                    .TNUMREG = tarea
                End With
                If datos.Piloto = "NO" Then
                    continuar = datos.ActualizarRFTASKRow(data, 1)
                End If
                If continuar = False Then
                    'ERROR DE ACTUALIZADO
                End If
            End If
            ArchivoPDF = PDF_Folder & "MyB" & Control & ".pdf"
            With AxPdfBox1
                .FileName = ArchivoPDF
                .Title = "Libro MyB - "
                .WantShow = bPRUEBAS
                .WantPageCount = True
                .PaperSizeName = "Letter"
                .Orientation = TPrinterOrientation.poLandscape
                .AutoPageFeed = True
                .BeginDoc()
                Const NUM_FORMAT As String = "#,##0.00"
                Dim b As TBoxBand

                Dim Ta As TBoxTable
                Ta = .CreateTable("BorderStyle=Bottom")
                Ta.Assign(rs)

                Dim Detail As TBoxBand
                Detail = Ta.CreateBand()
                Detail.Role = TBandRole.rlDetail
                Detail.ChildrenStyle = "FontSize=8;Fontname=Arial; VertAlignment=Center"
                Detail.Height = 40
                Detail.ChildrenStyle = "FontSize=8;Fontname=Arial; VertAlignment=Center;BorderStyle=LeftRight"
                Detail.CreateCell(200, "BorderStyle=Left").CreateText("Alignment=Left;BorderLeftMargin=3").Bind("BCUENTA")
                Detail.CreateCell(700, "BorderStyle=Left").CreateText("Alignment=Left;BorderLeftMargin=3").Bind("BDESCTA")
                With Detail.CreateCell(400, "BorderStyle=Left").CreateText("Alignment=Right;BorderRightMargin=3")
                    .Bind("BSALANT")
                    .Format = NUM_FORMAT
                End With
                With Detail.CreateCell(400, "BorderStyle=Left").CreateText("Alignment=Right;BorderRightMargin=3")
                    .Bind("BDEBITO")
                    .Format = NUM_FORMAT
                End With
                With Detail.CreateCell(400, "BorderStyle=Left").CreateText("Alignment=Right;BorderRightMargin=3")
                    .Bind("BCREDITO")
                    .Format = NUM_FORMAT
                End With
                With Detail.CreateCell(400, "BorderStyle=LeftRight").CreateText("Alignment=Right;BorderRightMargin=3")
                    .Bind("BSALFIN")
                    .Format = NUM_FORMAT
                End With
                Detail.Breakable = True
                Ta.Put()

                b = .CreateBand("BorderStyle=none;Margins=130,200,100,180")
                b.Height = 10
                b.Put()

                data = Nothing
                data = New RFTASK
                With data
                    .TLXMSG = "EJECUTADA"
                    .TMSG = "Libro Mayor y Banances Generado N° Paginas: " & NumPagina
                    .TURL = Replace(ArchivoPDF, CARPETA_SITIO, "\")
                    .STS2 = "1"
                    .TNUMREG = tarea
                End With
                If datos.Piloto = "NO" Then
                    continuar = datos.ActualizarRFTASKRow(data, 2)
                End If
                If continuar = False Then
                    'ERROR DE ACTUALIZADO
                End If
                rs.Close()
                .EndDoc()
            End With
        End If
    End Sub

    Private Sub Balance_BeforePutBand(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_BeforePutBandEvent) Handles AxPdfBox1.BeforePutBand
        If e.aBand.Role = TBandRole.rlDetail Then
            If bColor And flag = 0 Then
                e.aBand.BrushColor = RGB(215, 215, 215)
            Else
                e.aBand.BrushColor = RGB(255, 255, 255)
            End If
            bColor = Not bColor
        End If
        Debug.Print(AxPdfBox1.PenY)
Manejo_Error:
        If Err.Number <> 0 Then
            datos.WrtTxtError(LOG_File, "BeforePutBand")
            End
        End If
    End Sub

    Private Sub Balance_OnBottomOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnBottomOfPageEvent) Handles AxPdfBox1.OnBottomOfPage
        Dim b As TBoxBand
        With AxPdfBox1
            If e.lastPage Then
                b = .CreateBand("Normal;BorderStyle=None;Margins=130,200,100,180")
                b.ChildrenStyle = "BorderStyle=None;Alignment=Center;FontSize=12"
                b.Height = 50
                b.CreateCell(1040).CreateText().Assign("")
                b.CreateCell(480).CreateText("FontBold=1").Assign("Fin del Informe")
                b.CreateCell(1030).CreateText().Assign("")
                b.Put()
                If AxPdfBox1.PageCount Mod 2 = 0 Then
                    FinDocumento()
                End If
            End If
            NumPagina = NumPagina + 1
        End With
    End Sub

    Private Sub Balance_OnTopOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnTopOfPageEvent) Handles AxPdfBox1.OnTopOfPage
        Dim b As TBoxBand
        Dim parametro(0) As String
        Dim rs As New ADODB.Recordset

        With AxPdfBox1
            .DefineStyle("Normal", "0; Margins=130,200,100,150; fontsize=10;Fontname=Arial;BorderColor=Black")
            .Style = "Normal"
            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=12;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(1200).CreateText("Alignment=Left;FontSize=12").Assign("LIBRO MAYOR Y BALANCES")
            b.CreateCell(1000).CreateText("Alignment=Right;FontSize=8;FontColor=Red").Assign(" " + Referencia.ToString)
            b.CreateCell(100).CreateText("Alignment=Left;FontSize=8;FontColor=Blue").Assign(" " + Consecutivo + AxPdfBox1.PageCount)
            b.CreateCell(200).CreateText("Alignment=Right;FontSize=8").Assign("" + FechaImpresion.ToString)
            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.Height = 50
            b.ChildrenStyle = "Normal;FontSize=8;Fontname=Arial;BorderColor=Black;BorderStyle=None;Alignment=Center;VertAlignment=Bottom"
            b.CreateCell(400).CreateText("Alignment=Left;FontSize=8;FontBold=1").Assign(NOMEMP.ToString)
            b.CreateCell(2100).CreateText().Assign(" ")
            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=8;Fontname=Arial;BorderColor=Black;BorderStyle=None;Alignment=Left"
            b.CreateCell(300).CreateText().Assign(NIT.ToString)
            b.CreateCell(2200).CreateText().Assign(" ")
            b.Put()

            parametro(0) = Control
            rs = datos.ConsultarBSSFFNBALrs(parametro, 1)

            If rs Is Nothing Then
                datos.WrtTxtError(LOG_File, "Libro sin datos")
                Exit Sub
            End If

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=8;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(140).CreateText("Alignment=Left").Assign("PERIODO")
            b.CreateCell(350).CreateText("FontBold=1").Assign(rs("BYEAR").Value & "" & rs("BPERIO").Value)
            b.CreateCell(1680).CreateText("Alignment=Left").Assign(" ")
            b.CreateCell(320).CreateImage("Alignment=Right").Assign(LOGO)
            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.Height = 20
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.ChildrenStyle = "BorderStyle=rect"
            b.Height = 50
            b.CreateCell(200, "BorderStyle=rect;FontSize=8;FontBold=1;Alignment=Center;VertAlignment=Center").CreateText("BorderRightMargin=10").Assign("CUENTA")
            b.CreateCell(700, "BorderStyle=rect;FontSize=8;FontBold=1;Alignment=Center;VertAlignment=Center").CreateText("BorderRightMargin=10").Assign("DESCRIPCIÓN")
            b.CreateCell(400, "BorderStyle=rect;FontSize=8;FontBold=1;Alignment=Center;VertAlignment=Center").CreateText("BorderRightMargin=10").Assign("INICIAL")
            b.CreateCell(400, "BorderStyle=rect;FontSize=8;FontBold=1;Alignment=Center;VertAlignment=Center").CreateText("BorderRightMargin=10").Assign("DÉBITO")
            b.CreateCell(400, "BorderStyle=rect;FontSize=8;FontBold=1;Alignment=Center;VertAlignment=Center").CreateText("BorderRightMargin=10").Assign("CRÉDITO")
            b.CreateCell(400, "BorderStyle=rect;FontSize=8;FontBold=1;Alignment=Center;VertAlignment=Center").CreateText("BorderRightMargin=10").Assign("FINAL")
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.Height = 2
            b.Put()
        End With
    End Sub

    Sub FinDocumento()
        Dim b As TBoxBand
        With Me.AxPdfBox1
            AxPdfBox1.NewPage()
            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "BorderStyle=rect;Alignment=Center;FontSize=12"
            b.Height = 50
            b.CreateCell(1010).CreateText().Assign("")
            b.CreateCell(480).CreateText("FontBold=1").Assign("Fin del Informe")
            'b.CreateCell(480).CreateImage("Alignment=Right;VertAlignment=Top").Assign(My.Application.Info.DirectoryPath & "\Fin del Informe.png")
            b.CreateCell(1010).CreateText().Assign("")
            b.Put()
        End With
    End Sub
End Class
