﻿Public Class wsTareas

    Private WithEvents m_timer As New Timers.Timer(30000)

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Agregue el código aquí para iniciar el servicio. Este método debería poner
        ' en movimiento los elementos para que el servicio pueda funcionar.
        'Me.EventLog1.WriteEntry("Inicia")
        wrtLogHtml("servicio", "entra")
        m_timer.Enabled = True

    End Sub

    Private Sub m_timer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles m_timer.Elapsed
        wrtLogHtml("servicio", "INICIA")
        revisaTareas()
    End Sub

    Protected Overrides Sub OnStop()
        ' Agregue el código aquí para realizar cualquier anulación necesaria para detener el servicio.
        m_timer.Stop()
    End Sub

End Class
