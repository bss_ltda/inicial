﻿Module Module1

    Sub Main()
        Dim pass As String
        pass = getEncryptedCode("hola")
        Console.WriteLine(pass.Length)
        Console.WriteLine(pass)
        Console.ReadKey()

    End Sub


    Public Function getEncryptedCode(ByVal inputString As String) As String
        Dim Hash As Byte() = New System.Security.Cryptography.SHA512Managed().ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(inputString))
        Dim outputString As New System.Text.StringBuilder()
        For i As Integer = 0 To Hash.Length - 1
            outputString.Append(Hash(i).ToString("x2"))
        Next
        Return outputString.ToString()
    End Function

End Module
