﻿Option Explicit On

Module ReportSLK

    'ReportSLK(xlsXID)
    Function ReportSLK(xlsXID)
        Dim rs, i, J, aTit, aFormato, aTmp1, nomArch
        Dim qRow, qCol
        Dim f, fld, xlsSQL, sep, sepDec, sepLst
        Dim fs, fSql
        Dim x

        fSql = "SELECT * FROM RFREPORTX WHERE XID = " & xlsXID
        rs = ExecuteSql(fSql)

        If Not rs.EOF Then
            xlsSQL = rs("XSQL") & " FETCH FIRST 200000 ROWS ONLY "
            aTit = Split(rs("XTIT"), "|")
            ReDim aFormato(UBound(aTit))
        End If
        rs.Close()

        fSql = "UPDATE RFREPORTX SET XXLS = '" & nomArch & "' WHERE XID = " & xlsXID
        ExecuteSql fSql
        nomArch = Replace(CARPETA_IMG & "XLS\xls" & xlsXID & ".slk", "\\", "\")

        fsAbreArchivo(f, fs, nomArch)

        'Encabezado
        f_WriteLine(f, "ID;P")
        'Formatos de celdas
        SLK_Formatos f

        For i = 1 To UBound(aTit)
            aFormato(i) = Split(aTit(i), ",")
            f_WriteLine(f, SLK_Linea(1, i, """", Trim(aFormato(i)(0))))
        Next

        rs = ExecuteSql(xlsSQL)
        qRow = 2
        Do While Not rs.EOF
            qCol = 1
            For Each fld In rs.Fields
                On Error Resume Next
                If Not IsNull(fld.Value) Then
                    Select Case aFormato(qCol)(1)
                        Case "A"
                            f_WriteLine(f, SLK_Linea(qRow, qCol, """", Trim(fld.Value)))
                        Case "N"
                            f_WriteLine(f, SLK_Linea(qRow, qCol, "", Replace(fld.Value, ",", ".")))
                        Case "D"
                            f_WriteLine(f, SLK_Linea(qRow, qCol, "", CDate(fld.Value) - DateSerial(1900, 1, 1)))
                    End Select
                Else
                    f_WriteLine(f, SLK_Linea(qRow, qCol, """", ""))
                End If
                If Err.Number <> 0 Then
                    f_WriteLine(f, SLK_Linea(qRow, qCol, """", ""))
                End If
                On Error GoTo 0
                qCol = qCol + 1
            Next
            rs.MoveNext()
            qRow = qRow + 1
        Loop

        qCol = 0
        For Each x In aFormato
            If qCol > 0 Then
                f_WriteLine(f, "F;" & aFormato(qCol)(2) & ";C" & qCol)
                If CInt(aFormato(qCol)(3)) > 0 Then
                    f_WriteLine(f, "F;W" & qCol & " " & qCol & " " & aFormato(qCol)(3))
                End If
            End If
            qCol = qCol + 1
        Next

        If J >= 200000 Then
            f_WriteLine(f, SLK_Linea(qRow, 1, """", "Excedio 100,000 Registros"))
        End If

        f_WriteLine(f, "E")
        f_Close f
        f = Nothing
        fs = Nothing

        rs.Close()
        rs = Nothing

        fSql = "UPDATE RFREPORTX SET XSTS = 1 WHERE XID = " & xlsXID
        ExecuteSql fSql

        ReportSLK = "/ImagenesTransportes/XLS/xls" & xlsXID & ".slk"

    End Function

    'SLK_Linea
    Function SLK_Linea(qRow, qCol, qSep, qDato)

        SLK_Linea = "C;Y" & qRow & ";X" & qCol & ";K" & qSep & qDato & qSep

    End Function

    'SLK_Formatos(f)
    Function SLK_Formatos(f)
        Dim P, x

        ReDim P(54)
        'Formatos
        P(0) = "P;P" & "General"                                                                       '               -12345.678
        P(30) = "P;P" & "@"                                                                            '1105.0501                  Texto
        P(3) = "P;P" & "#,##0"                                                                         '                  -12,346
        P(13) = "P;P" & "0%"                                                                           '                      38%
        P(14) = "P;P" & "0.00%"                                                                        '                   37.75%
        P(37) = "P;P" & "yyyy\-mm\-dd"                                                                 '               2015-02-04
        P(27) = "P;P" & "yyyy/mm/dd\ hh:mm"                                                            '         2015-02-04 21:16

        P(1) = "P;P" & "0"                                                                             '                   -12346
        P(2) = "P;P" & "0.00"                                                                          '                -12345.68
        P(4) = "P;P" & "#,##0.00"                                                                      '               -12,345.68
        P(5) = "P;P" & "#,##0_);;\(#,##0\)"                                                            '                 (12,346)
        P(6) = "P;P" & "#,##0_);;[Red]\(#,##0\)"                                                       '                 (12,346)  En Rojo
        P(7) = "P;P" & "#,##0.00_);;\(#,##0.00\)"                                                      '              (12,345.68)
        P(8) = "P;P" & "#,##0.00_);;[Red]\(#,##0.00\)"                                                 '              (12,345.68)  En Rojo
        P(9) = "P;P" & """$""\ #,##0_);;\(""$""\ #,##0\)"                                              '               ($ 12,346)
        P(10) = "P;P" & """$""\ #,##0_);;[Red]\(""$""\ #,##0\)"                                        '               ($ 12,346)  En Rojo
        P(11) = "P;P" & """$""\ #,##0.00_);;\(""$""\ #,##0.00\)"                                       '            ($ 12,345.68)
        P(12) = "P;P" & """$""\ #,##0.00_);;[Red]\(""$""\ #,##0.00\)"                                  '            ($ 12,345.68)  En Rojo
        P(15) = "P;P" & "0.00E+00"                                                                     '                -1.23E+04
        P(16) = "P;P" & "##0.0E+0"                                                                     '                 -12.3E+3
        P(17) = "P;P" & "#\ ?/?"                                                                       '                42039 8/9  Fraccion
        P(18) = "P;P" & "#\ ??/??"                                                                     '              42039 39/44  Fraccion
        P(19) = "P;P" & "yyyy/mm/dd"                                                                   '               2015-02-04
        P(20) = "P;P" & "dd/mmm/yy"                                                                    '                04-feb-15
        P(21) = "P;P" & "dd/mmm"                                                                       '                   04-feb
        P(22) = "P;P" & "mmm/yy"                                                                       '                   feb-15
        P(23) = "P;P" & "hh:mm\ AM/PM"                                                                 '              09:16 p. m.
        P(24) = "P;P" & "hh:mm:ss\ AM/PM"                                                              '           09:16:31 p. m.
        P(25) = "P;P" & "hh:mm"                                                                        '                    21:16
        P(26) = "P;P" & "hh:mm:ss"                                                                     '                 21:16:31
        P(28) = "P;P" & "mm:ss"                                                                        '                    16:31
        P(29) = "P;P" & "mm:ss.0"                                                                      '                  16:31.0
        P(31) = "P;P" & "[h]:mm:ss"                                                                    '            1008957:16:31
        P(32) = "P;P" & "_(""$""\ * #,##0_);;_(""$""\ * \(#,##0\);;_(""$""\ * ""-""_);;_(@_)"          '               $ (12,346)
        P(33) = "P;P" & "_(* #,##0_);;_(* \(#,##0\);;_(* ""-""_);;_(@_)"                               '                 (12,346)
        P(34) = "P;P" & "_(""$""\ * #,##0.00_);;_(""$""\ * \(#,##0.00\);;_(""$""\ * ""-""??_);;_(@_)"  '            $ (12,345.68)
        P(35) = "P;P" & "_(* #,##0.00_);;_(* \(#,##0.00\);;_(* ""-""??_);;_(@_)"                       '              (12,345.68)
        P(36) = "P;P" & "_(* #,##0_);;_(* \-#,##0_);;_(* ""-""_);;_(@_)"                               '                  -12,346
        P(38) = "P;P" & "yyyy\-mm\-dd\ hh:mm:ss"                                                       '      2015-02-04 21:16:31
        P(39) = "P;P" & """$""#,##0_);;\(""$""#,##0\)"                                                 '                ($12,346)
        P(40) = "P;P" & """$""#,##0_);;[Red]\(""$""#,##0\)"                                            '                ($12,346)  En Rojo
        P(41) = "P;P" & """$""#,##0.00_);;\(""$""#,##0.00\)"                                           '             ($12,345.68)
        P(42) = "P;P" & """$""#,##0.00_);;[Red]\(""$""#,##0.00\)"                                      '             ($12,345.68)  En Rojo
        P(43) = "P;P" & "_(""$""* #,##0.00_);;_(""$""* \(#,##0.00\);;_(""$""* ""-""??_);;_(@_)"        '             $(12,345.68)
        P(44) = "P;P" & "yyyy\-mm\-dd\ h:mm:ss\ AM/PM"                                                 ' 2015-02-04 9:16:31 p. m.
        P(45) = "P;P" & "m/d/yyyy"                                                                     '                 2-4-2015
        P(46) = "P;P" & "d\-mmm\-yy"                                                                   '                 4-feb-15
        P(47) = "P;P" & "d\-mmm"                                                                       '                    4-feb
        P(48) = "P;P" & "mmm\-yy"                                                                      '                   feb-15
        P(49) = "P;P" & "h:mm\ AM/PM"                                                                  '               9:16 p. m.
        P(50) = "P;P" & "h:mm:ss\ AM/PM"                                                               '            9:16:31 p. m.
        P(51) = "P;P" & "h:mm"                                                                         '                    21:16
        P(52) = "P;P" & "h:mm:ss"                                                                      '                 21:16:31
        P(53) = "P;P" & "m/d/yyyy\ h:mm"                                                               '           2-4-2015 21:16

        For Each x In P
            f_WriteLine(f, x)
        Next

    End Function


End Module
