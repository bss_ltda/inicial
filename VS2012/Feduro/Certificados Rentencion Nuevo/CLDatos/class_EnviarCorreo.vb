﻿Imports Chilkat_v9_5_0

Public Class class_EnviarCorreo
    Public Asunto As String
    Public url As String

    Dim mht As Chilkat_v9_5_0.ChilkatMht
    Dim email As Chilkat_v9_5_0.ChilkatEmail
    Dim mailman As Chilkat_v9_5_0.ChilkatMailMan
    Dim sErrorXML, sErrorHTML, sErrorTXT

    Function InicializaMHT()

        ' Create a mailman
        mht = New Chilkat_v9_5_0.ChilkatMht
        mailman = New Chilkat_v9_5_0.ChilkatMailMan
        email = New Chilkat_v9_5_0.ChilkatEmail

        ' Unlock the component - any string automatically begins a 30-day trial.
        mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
        mht.UnlockComponent("SMANRIMHT_dEnsKQ0g3Rue")

        mailman.SmtpHost = "192.168.2.5"
        mailman.SmtpUsername = "informes@feduro.net"
        mailman.SmtpPassword = "FeDuRo@2008"

        InicializaMHT = correoMHT(email, mht, url)


    End Function

    'correoMHT()
    Function correoMHT(email, mht, url)
        Dim emlStr

        mht.UseCids = 1
        emlStr = mht.GetEML(url)
        If (emlStr = vbNullString) Then
            Console.WriteLine(mht.LastErrorText)
        End If
        correoMHT = email.SetFromMimeText(emlStr)
        If (correoMHT <> 1) Then
            Console.WriteLine(email.LastErrorText)
        End If

    End Function

    Sub Desde(Nombre, Correo)
        email.fromName = Nombre
        email.FromAddress = Correo
    End Sub


    Sub Destinatario(Nombre, Correo)
        email.AddTo(Nombre, Correo)
        email.AddBcc("",  "AlertasCorreo2015@gmail.com" )
    End Sub

    Sub DestinatarioUsuario(Usuario)
        'Dim rs, fSql

        'fSql = " SELECT UNOM, UEMAIL FROM RCAU WHERE UUSR = '" & Usuario & "' "
        'rs = ExecuteSql(fSql)
        'If Not rs.EOF Then
        '    email.AddTo(rs("UNOM").Value, rs("UEMAIL").Value)
        'End If
        'rs.Close()
        'rs = Nothing

    End Sub

    Function Envia()

        email.subject = Asunto
        If email.fromName = "" Then
            email.fromName = "Agencias Feduro S.A."
            email.FromAddress = "noresponder@feduro.net"
        End If

        If mailman.SendEmail(email) Then
            Envia = True
        Else
            Envia = False
            sErrorHTML = mailman.LastErrorHtml
            sErrorTXT = mailman.LastErrorText
            sErrorXML = mailman.LastErrorXml
            'notepad mailman.LastErrorText, ""
        End If
        If (mailman.CloseSmtpConnection() <> 1) Then

        End If
        ' Standard ASP cleanup of objects
        email = Nothing
        mailman = Nothing
        mht = Nothing

    End Function

    Function Archivo(nombreArchivo As String) As Boolean
        Dim contentType As String

        contentType = email.AddFileAttachment(nombreArchivo)
        If (contentType = vbNullString) Then
            Console.WriteLine(email.LastErrorText)
            Return False
        End If
        Return True

    End Function

    Function ErrorHtml()
        ErrorHtml = sErrorHTML
    End Function

    Function ErrorTXT()
        ErrorTXT = sErrorTXT
    End Function

    Function ErrorXML()
        ErrorXML = sErrorXML
    End Function

End Class
