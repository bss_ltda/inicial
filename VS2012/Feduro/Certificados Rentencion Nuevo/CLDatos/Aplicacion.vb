﻿
Public Module Aplicacion
    Public Const APP_NAME As String = "ListadoEmpaque"
    Public APP_PATH As String
    Public APP_VERSION As String

    Public Origen As String
    'Public NumeroDocumento As String
    'Public PeriodoAAAA As String
    'Public PeriodoMM As String
    Public PDF_Folder As String
    Public LOG_File As String
    Public TASK_No As String
    Public FDESDE As String
    Public FHASTA As String
    Public FTERNIT As String

    Public Impresora_PDF As String
    Public PDFEXE As String
    Public Idioma As String
    Public Factura_Marca As String
    Public IAL_Prt As Integer
    Public Flag As Integer
    Public aPermisos() As Boolean
    Public Const ACC_TOTAL As Integer = 21

    Public bPRUEBAS As Boolean

    Public qConso As String
    Public qPedid As String
    Public qPlanilla As String
    Public qCliente As String
    Public qBodega As String
    Public qAPCHK01 As String
    Public qAPCHK02 As String
    Public qAFLAG01 As Integer

    Public aMarca() As String

    Function PDFPrtExe() As String
        PDFPrtExe = """E:\CLPrint\CLPrint.exe""{sp}/print{sp}/copies:{copias}{sp}/printer:""{printer}""{sp}/pdffile:""{doc}"""
    End Function



    Sub Imprimir(qImpresora As String)
        Dim rs As New ADODB.Recordset
        Dim qPrinter As String
        Dim i As Integer
        Dim ArchivoPDF As String
        Dim comando As String

        If qImpresora <> "" Then
            fSql = "SELECT CCNOT1, CCUDC1 FROM ZCC WHERE CCTABL ='PRTFACTU' AND CCCODE = '" & qImpresora & "' AND CCUDC2 = 1"
            If DB.OpenRS(rs, fSql) Then
                For i = 0 To 2
                    'If Not bPRUEBAS Then
                    If aMarca(i) <> "" Then
                        If Impresora_PDF = "" Then Impresora_PDF = CStr(rs("CCNOT1").Value)
                        ArchivoPDF = PDF_Folder & "\" & FTERNIT & "_" & aMarca(i) & ".pdf"
                        WrtTxtError(LOG_File, "Imprime " & ArchivoPDF)
                        'LogConso qConso, Now(), "Imprime Factura" & NumeroDocumento
                        comando = Replace(PDFEXE, "{printer}", Impresora_PDF)
                        comando = Replace(comando, "{copias}", "1")
                        comando = Replace(comando, "{doc}", ArchivoPDF)
                        comando = Replace(comando, "{sp}", " ")
                        WrtTxtError(LOG_File, comando)
                        Shell(comando, AppWinStyle.Hide, True, 300000) '5 minutos
                    End If
                Next
            Else
                WrtTxtError(LOG_File, "No econtró la impresora " & qImpresora)
            End If
            rs.Close()
            rs = Nothing
            Exit Sub
        End If

    End Sub

End Module
