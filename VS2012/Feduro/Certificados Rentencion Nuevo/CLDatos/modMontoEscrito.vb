﻿Module modMontoEscrito

    'Código para generar el monto escrito
    '(Convertir cantidades numéricas a su texto en letras)
    'Juan M. BeltranV. - Bogotá, Colombia 2002
    'Se reciben aportes, criticas y observaciones en jmbeltranv@yahoo.com
    Dim SALIDA As String, SAPO As String, SEX As String
    Function MontoEscrito(DATO As Double, SITEX As Integer, SEXO As String, Moneda As String, mCent As String) As String
        Dim ENTE As String, DECI As String, DECIMA, SIGNO As String
        Dim Letras As String = ""
        Dim CONDE, MILLONES As Integer
        Dim txtMoneda As String = ""

        SALIDA = ""
        If DATO < 0 Then
            SIGNO = "M"
        Else
            SIGNO = ""
        End If

        DATO = Math.Abs(DATO)
        ENTE = Format$(str(Int(DATO)), "000000000000")
        DECI = Format$(str((DATO - val(ENTE)) * 100), "00")
        SAPO = Mid$(ENTE, 1, 3)
        SEX = SEXO
        Call SUBNT()
        If SALIDA <> "" Then
            Letras = SALIDA + "mil "
            CONDE = 1
            MILLONES = 1
        End If
        SALIDA = ""
        SAPO = Mid$(ENTE, 4, 3)
        Call SUBNT()
        If SALIDA <> "" Then
            If SALIDA = "un " Then
                SALIDA = SALIDA + "millón "
            Else
                SALIDA = SALIDA + "millones "
            End If
            CONDE = 1
            MILLONES = 0
        End If

        If SAPO = "000" And Letras <> "" Then
            Letras = Letras + "millones "
            MILLONES = 0
        End If

        Letras = Letras + SALIDA
        SALIDA = ""
        SAPO = Mid$(ENTE, 7, 3)
        Call SUBNT()
        If SALIDA <> "" Then
            Letras = Letras + SALIDA + "mil "
            CONDE = 0
            MILLONES = 0
        End If
        If SALIDA = "un" Then
            Letras = Letras + SALIDA + "mil "
            CONDE = 0
            MILLONES = 1
        End If
        SALIDA = ""
        SAPO = Mid$(ENTE, 10, 3)
        Call SUBNT()
        If SALIDA <> "" Then
            Letras = Letras + SALIDA
            CONDE = 0
            MILLONES = 0
        End If

        If SIGNO = "M" Then
            Letras = "menos " + Letras
        End If

        If Letras = "" Then Letras = "cero "
        If SITEX = 1 Then
            If CONDE = 1 Then
                If MILLONES = 1 Then Letras = Letras + "millones "
                Select Case Moneda
                    Case "", "COP"
                        txtMoneda = " pesos "
                    Case "USD"
                        txtMoneda = " dolares "
                End Select
                Letras = Letras + " de " & txtMoneda
            Else
                If (Letras = "Un " Or Letras = "Menos un ") Then
                    Select Case Moneda
                        Case "", "COP"
                            txtMoneda = " peso "
                        Case "USD"
                            txtMoneda = " dolar "
                    End Select
                    Letras = Letras + txtMoneda
                Else
                    Select Case Moneda
                        Case "", "COP"
                            txtMoneda = " pesos "
                        Case "USD"
                            txtMoneda = " dolares "
                    End Select
                    '                If Right(DATO, 3) = "000" Then
                    '                    Letras = Letras & "de " & txtMoneda
                    '                Else
                    '                    Letras = Letras & txtMoneda
                    '                End If
                    Letras = Letras & txtMoneda
                End If
            End If
            If Trim(DECI) <> "00" Then
                Letras = Letras & " " & Moneda & " " & Replace(mCent, "{XX}", Trim(DECI))
            Else
                Letras = Letras & " " & Moneda
                'Letras = Letras + " m/cte."
            End If
        Else
            If CONDE = 1 Then
                If MILLONES = 1 Then Letras = Letras + "millones "
            End If
            SALIDA = ""
            SAPO = "0" + DECI
            Call SUBNT()
            If SALIDA <> "" Then
                DECIMA = SALIDA
                Letras = Letras + "con " + DECIMA
            End If
        End If

        MontoEscrito = Replace(UCase(Left(Letras, 1)) + Right(Letras, Len(Letras) - 1), "  ", " ")

    End Function

    Sub SUBNT()
        Dim S1, S2, S3 As String, PALABRA As String, CONY As Integer
        PALABRA = ""
        S1 = Left$(SAPO, 1)
        S2 = Mid$(SAPO, 2, 1)
        S3 = Right$(SAPO, 1)
        If (S1 <> "0") Then
            If UCase(SEX) = "F" Then
                Select Case S1
                    Case "1"
                        PALABRA = "ciento "
                        If S2 = "0" And S3 = "0" Then PALABRA = "cien "
                    Case "2"
                        PALABRA = "doscientas "
                    Case "3"
                        PALABRA = "trescientas "
                    Case "4"
                        PALABRA = "cuatrocientas "
                    Case "5"
                        PALABRA = "quinientas "
                    Case "6"
                        PALABRA = "seiscientas "
                    Case "7"
                        PALABRA = "setecientas "
                    Case "8"
                        PALABRA = "ochocientas "
                    Case "9"
                        PALABRA = "novecientas "
                End Select
                SALIDA = SALIDA + PALABRA
            Else
                Select Case S1
                    Case "1"
                        PALABRA = "ciento "
                        If S2 = "0" And S3 = "0" Then PALABRA = "cien "
                    Case "2"
                        PALABRA = "doscientos "
                    Case "3"
                        PALABRA = "trescientos "
                    Case "4"
                        PALABRA = "cuatrocientos "
                    Case "5"
                        PALABRA = "quinientos "
                    Case "6"
                        PALABRA = "seiscientos "
                    Case "7"
                        PALABRA = "setecientos "
                    Case "8"
                        PALABRA = "ochocientos "
                    Case "9"
                        PALABRA = "novecientos "
                End Select
                SALIDA = SALIDA + PALABRA
            End If
        End If
        S1 = Mid$(SAPO, 2, 1)
        S2 = Mid$(SAPO, 3, 1)
        If S1 <> "" And S1 <> "0" Then
            CONY = 0
            If S1 = "1" Then
                Select Case S2
                    Case "0"
                        PALABRA = "diez "
                        S2 = "0"
                    Case "1"
                        PALABRA = "once "
                        S2 = "0"
                    Case "2"
                        PALABRA = "doce "
                        S2 = "0"
                    Case "3"
                        PALABRA = "trece "
                        S2 = "0"
                    Case "4"
                        PALABRA = "catorce "
                        S2 = "0"
                    Case "5"
                        PALABRA = "quince "
                        S2 = "0"
                    Case "6"
                        PALABRA = "dieciséis "
                        S2 = "0"
                    Case "7"
                        PALABRA = "diecisiete "
                        S2 = "0"
                    Case "8"
                        PALABRA = "dieciocho "
                        S2 = "0"
                    Case "9"
                        PALABRA = "diecinueve "
                        S2 = "0"
                End Select
            Else
                CONY = 1
                Select Case S1
                    Case "2"
                        CONY = 0
                        If S2 = "0" Then
                            PALABRA = "veinte "
                        Else
                            PALABRA = "veinti"
                        End If
                    Case "3"
                        PALABRA = "treinta "
                    Case "4"
                        PALABRA = "cuarenta "
                    Case "5"
                        PALABRA = "cincuenta "
                    Case "6"
                        PALABRA = "sesenta "
                    Case "7"
                        PALABRA = "setenta "
                    Case "8"
                        PALABRA = "ochenta "
                    Case "9"
                        PALABRA = "noventa "
                End Select
            End If

            SALIDA = SALIDA + PALABRA
            If CONY = 1 And S2 <> "0" Then SALIDA = SALIDA + "y "
        End If

        If (S3 <> "" And S2 <> "0") Then
            Select Case S3
                Case "1"
                    If SEX = "F" Then
                        PALABRA = "una "
                    Else
                        PALABRA = "un "
                    End If
                Case "2"
                    PALABRA = "dos "
                Case "3"
                    PALABRA = "tres "
                Case "4"
                    PALABRA = "cuatro "
                Case "5"
                    PALABRA = "cinco "
                Case "6"
                    PALABRA = "seis "
                Case "7"
                    PALABRA = "siete "
                Case "8"
                    PALABRA = "ocho "
                Case "9"
                    PALABRA = "nueve "
            End Select
            SALIDA = SALIDA + PALABRA
        End If
    End Sub

    '============================================================================================================
    ' ENGLISH
    '============================================================================================================

    Function ConvertCurrencyToEnglish(ByVal MyNumber As Double, mUni As String, mCent As String) As String
        Dim Temp
        Dim Dollars
        Dim Cents As String = ""
        Dim DecimalPlace, Count
        Dim sCents
        Dim place() As String

        ReDim place(9)

        Place(2) = " Thousand "
        Place(3) = " Million "
        Place(4) = " Billion "
        Place(5) = " Trillion "

        ' Convert MyNumber to a string, trimming extra spaces.
        MyNumber = Trim(Str(MyNumber))

        ' Find decimal place.
        DecimalPlace = InStr(MyNumber, ".")

        ' If we find decimal place...
        If DecimalPlace > 0 Then
            ' Convert cents
            Temp = Left(Mid(MyNumber, DecimalPlace + 1) & "00", 2)
            Cents = ConvertTens(Temp)
            sCents = Temp

            ' Strip off cents from remainder to convert.
            MyNumber = Trim(Left(MyNumber, DecimalPlace - 1))
        End If

        Count = 1
        Do While MyNumber <> ""
            ' Convert last 3 digits of MyNumber to English dollars.
            Temp = ConvertHundreds(Right(MyNumber, 3))
            If Temp <> "" Then Dollars = Temp & Place(Count) & Dollars
            If Len(MyNumber) > 3 Then
                ' Remove last 3 converted digits from MyNumber.
                MyNumber = Left(MyNumber, Len(MyNumber) - 3)
            Else
                MyNumber = ""
            End If
            Count = Count + 1
        Loop

        ' Clean up dollars.
        Select Case Dollars
            Case ""
                Dollars = "No Dollars"
            Case "One"
                Dollars = "One Dollar"
            Case Else
                Dollars = Dollars & " Dollars"
        End Select

        ' Clean up cents.
        '         Select Case Cents
        '            Case ""
        '               Cents = " And No Cents"
        '            Case "One"
        '               Cents = " And One Cent"
        '            Case Else
        '               Cents = " " & Replace(mCent, "{XX}", sCents)
        '         End Select
        If Cents <> "" Then
            Cents = " " & Replace(mCent, "{XX}", sCents)
        End If

        ConvertCurrencyToEnglish = Dollars & Cents

    End Function

    Private Function ConvertHundreds(ByVal MyNumber)
        Dim Result As String = ""

        ' Exit if there is nothing to convert.
        If Val(MyNumber) = 0 Then Return ""

        ' Append leading zeros to number.
        MyNumber = Right("000" & MyNumber, 3)

        ' Do we have a hundreds place digit to convert?
        If Left(MyNumber, 1) <> "0" Then
            Result = ConvertDigit(Left(MyNumber, 1)) & " Hundred "
        End If

        ' Do we have a tens place digit to convert?
        If Mid(MyNumber, 2, 1) <> "0" Then
            Result = Result & ConvertTens(Mid(MyNumber, 2))
        Else
            ' If not, then convert the ones place digit.
            Result = Result & ConvertDigit(Mid(MyNumber, 3))
        End If

        ConvertHundreds = Trim(Result)
    End Function

    Private Function ConvertTens(ByVal MyTens)
        Dim Result As String

        ' Is value between 10 and 19?
        If val(Left(MyTens, 1)) = 1 Then
            Select Case val(MyTens)
                Case 10 : Result = "Ten"
                Case 11 : Result = "Eleven"
                Case 12 : Result = "Twelve"
                Case 13 : Result = "Thirteen"
                Case 14 : Result = "Fourteen"
                Case 15 : Result = "Fifteen"
                Case 16 : Result = "Sixteen"
                Case 17 : Result = "Seventeen"
                Case 18 : Result = "Eighteen"
                Case 19 : Result = "Nineteen"
                Case Else
            End Select
        Else
            ' .. otherwise it's between 20 and 99.
            Select Case val(Left(MyTens, 1))
                Case 2 : Result = "Twenty "
                Case 3 : Result = "Thirty "
                Case 4 : Result = "Forty "
                Case 5 : Result = "Fifty "
                Case 6 : Result = "Sixty "
                Case 7 : Result = "Seventy "
                Case 8 : Result = "Eighty "
                Case 9 : Result = "Ninety "
                Case Else
            End Select

            ' Convert ones place digit.
            Result = Result & ConvertDigit(Right(MyTens, 1))
        End If

        ConvertTens = Result
    End Function

    Private Function ConvertDigit(ByVal MyDigit)
        Select Case val(MyDigit)
            Case 1 : ConvertDigit = "One"
            Case 2 : ConvertDigit = "Two"
            Case 3 : ConvertDigit = "Three"
            Case 4 : ConvertDigit = "Four"
            Case 5 : ConvertDigit = "Five"
            Case 6 : ConvertDigit = "Six"
            Case 7 : ConvertDigit = "Seven"
            Case 8 : ConvertDigit = "Eight"
            Case 9 : ConvertDigit = "Nine"
            Case Else : ConvertDigit = ""
        End Select
    End Function


End Module
