﻿Imports System.IO
Imports System.Xml.Serialization

Public Class ReporteDestinoSeguro
    Public Consolidado As String
    Public DB As clsConexion

    Private fSql, vSql As String

    Function reporte() As String
        Dim rsRHCC As New ADODB.Recordset
        Dim rs As New ADODB.Recordset
        Dim errProceso As String = ""

        Dim DestinoSeguro As New net.destinoseguro.www.WSdestino
        Dim DespachoTransp As New ServiceReferenceDestinoSeguro.setDespachoTranspRequest
        DB.gsKeyWords = Consolidado
        fSql = "SELECT CHAR( VARCHAR_FORMAT( HSALE,'YYYY-MM-DD HH24:MI:SS'), 19 ) AS SALIDA  "
        fSql &= ", HBOD, HPROVE, HMANIF, HCIUDAD, HDEPTO "
        fSql &= ", HPLACA, HCEDUL, HCELU, HCHOFE"
        fSql &= " FROM RHCC H WHERE HCONSO = '" & Consolidado & "'"
        If DB.OpenRS(rsRHCC, fSql) Then
            errProceso = datosOK(rsRHCC)
            If errProceso = "" Then
                DespachoTransp.usr = "WSBRINSA"
                DespachoTransp.pwd = "LvL3qP2"
                'salida
                DespachoTransp.salida = Replace(rsRHCC("SALIDA").Value, " ", "T")
                'nittra
                fSql = "SELECT VMFSCD FROM AVM WHERE VENDOR = " & rsRHCC("HPROVE").Value
                If DB.OpenRS(rs, fSql) Then
                    Dim aNit() As String = Split(rs(0).Value, "-")
                    DespachoTransp.nittra = CampoNum(aNit(0))
                Else
                    errProceso &= "NIT Transportadora|"
                End If
                rs.Close()

                'manifi
                DespachoTransp.manifi = rsRHCC("HMANIF").Value
                'corigen
                fSql = "SELECT WMSLOC FROM IWM INNER JOIN RFDANE ON WMSLOC = CPOBLADO "
                fSql &= " WHERE LWHS = '" & rsRHCC("HBOD").Value & "'"
                If DB.OpenRS(rs, fSql) Then
                    DespachoTransp.corigen = rs(0).Value
                Else
                    errProceso &= "Codigo dane ciudad origen|"
                End If
                rs.Close()

                'cdestino
                fSql = " SELECT CPOBLADO FROM RFDANE "
                fSql &= " WHERE CLXDPTO = '" & IIf(rsRHCC("HDEPTO").Value = "D.C", "CUN", rsRHCC("HDEPTO").Value) & "' "
                fSql &= " AND CLXCIUD = '" & rsRHCC("HCIUDAD").Value & "' "
                fSql &= " AND CTIPO = 'CM' "
                If DB.OpenRS(rs, fSql) Then
                    DespachoTransp.cdestino = rs(0).Value
                Else
                    rs.Close()
                    fSql = " SELECT PCENUS  "
                    fSql = fSql & " FROM LPCL01  "
                    fSql = fSql & " WHERE PCSTCD = '" & IIf(rsRHCC("HDEPTO").Value = "D.C", "CUN", rsRHCC("HDEPTO").Value) & "'  "
                    fSql = fSql & " AND PCPSCD = '" & rsRHCC("HCIUDAD").Value & "'  "
                    fSql = fSql & " AND PCENUS <> '' "
                    If DB.OpenRS(rs, fSql) Then
                        DespachoTransp.cdestino = rs(0).Value & "000"
                    Else
                        errProceso &= "Codigo dane ciudad destino|"
                    End If
                End If
                rs.Close()

                DespachoTransp.placa = rsRHCC("HPLACA").Value
                DespachoTransp.cedula = rsRHCC("HCEDUL").Value
                DespachoTransp.nombres = campoAlfa(rsRHCC("HCHOFE").Value)
                DespachoTransp.cel = Replace(rsRHCC("HCELU").Value, " ", "")
                DespachoTransp.nitgen = "800221789"
                DespachoTransp.nomgen = "BRINSA S.A."
                DespachoTransp.sucursal = "1"
                DespachoTransp.it1 = Consolidado
                DespachoTransp.rut = "0"
            End If
        End If

        If errProceso <> "" Then
            'avisa()
            Return errProceso
        End If
        Dim msg As String = ""
        msg &= "<br />usr      = " & DespachoTransp.usr
        msg &= "<br />pwd      = " & DespachoTransp.pwd
        msg &= "<br />salida   = " & DespachoTransp.salida
        msg &= "<br />nittra   = " & DespachoTransp.nittra
        msg &= "<br />manifi   = " & DespachoTransp.manifi
        msg &= "<br />corigen  = " & DespachoTransp.corigen
        msg &= "<br />cdestino = " & DespachoTransp.cdestino
        msg &= "<br />placa    = " & DespachoTransp.placa
        msg &= "<br />cedula   = " & DespachoTransp.cedula
        msg &= "<br />nombres  = " & DespachoTransp.nombres
        msg &= "<br />cel      = " & DespachoTransp.cel
        msg &= "<br />nitgen   = " & DespachoTransp.nitgen
        msg &= "<br />nomgen   = " & DespachoTransp.nomgen
        msg &= "<br />sucursal = " & DespachoTransp.sucursal
        msg &= "<br />it1      = " & DespachoTransp.it1
        msg &= "<br />rut      = " & DespachoTransp.rut

        With (DespachoTransp)
            Try
                errProceso = DestinoSeguro.setDespachoTransp( _
                                .usr, _
                                .pwd, _
                                .salida, _
                                .nittra, _
                                .manifi, _
                                .corigen, _
                                .cdestino, _
                                .via, _
                                .rut, _
                                .placa, _
                                .cmarca, _
                                .ccarroceria, _
                                .ccolor, _
                                .modelo, _
                                .cedula, _
                                .nombres, _
                                .apellidos, _
                                .dir, _
                                .tels, _
                                .cel, _
                                .nitgen, _
                                .nomgen, _
                                .sucursal, _
                                .obs, _
                                .it1, _
                                .it2, _
                                .it3, _
                                .itemadicional, _
                                .usrinterno)
            Catch ex As Exception
                Console.WriteLine(ex.Message)
                DB.WrtSqlError("", ex.ToString)
            End Try

        End With

        Console.WriteLine(errProceso)
        DB.Bitacora("DESTINOSEGURO", msg & "<br>Respuesta webservice: " & procesaError(errProceso), Consolidado)

        Return errProceso
    End Function


    Private Function procesaError(msg As String) As String
        Dim aMsg() As String = Split(msg, "|")

        If aMsg(0) = "0" Then
            DB.WrtSqlError("", aMsg(1) & " " & DB.gsKeyWords)
        End If
        LogConso("DSEGURO " & aMsg(1))
        Return aMsg(1)
    End Function

    'CampoNum()
    Private Function CampoNum(Dato) As String
        Dim i As Integer
        Dim Ascii As Integer
        Dim resultado As String = ""
        For i = 1 To Len(Dato)
            Ascii = Asc(Mid(Dato, i, 1))
            If Ascii >= Asc("0") And Ascii <= Asc("9") Then
                resultado &= Chr(Ascii)
            End If
        Next
        Return resultado

    End Function


    Private Sub LogConso(reporte As String)
        fSql = "UPDATE RHCC SET HREPORT = '" & Left(reporte, 50) & "', "
        fSql = fSql & " HFECREP = NOW()"
        fSql = fSql & " WHERE HCONSO = '" & Consolidado & "'"
        DB.ExecuteSQL(fSql)

        fSql = "INSERT INTO RRCC("
        vSql = "VALUES( "
        fSql = fSql & "RCONSO, " : vSql = vSql & "'" & Consolidado & "'" & ", "
        fSql = fSql & "RSTSC, "
        vSql = vSql & "( SELECT IFNULL(SUM(HESTAD), 9) FROM RHCC WHERE HCONSO='" & Consolidado & "' )" & ", "
        fSql = fSql & "RTREP, " : vSql = vSql & " " & "3" & " " & ", "
        fSql = fSql & "RFECREP, " : vSql = vSql & " NOW(), "
        fSql = fSql & "RREPORT, " : vSql = vSql & "'" & Left(Trim(reporte), 15000) & "'" & ", "
        fSql = fSql & "RAPP, " : vSql = vSql & "'PCDS'" & ", "
        fSql = fSql & "RCRTUSR) " : vSql = vSql & "'SISTEMA'" & ") "
        DB.ExecuteSQL(fSql & vSql)

    End Sub

    Private Function datosOK(rs As ADODB.Recordset) As String
        If Not PlacaOK(rs("HPLACA").Value) Then
            Return "Error en PLACA " & rs("HPLACA").Value
        ElseIf CDbl(rs("HMANIF").Value) = 0 Then
            Return "Error manifiesto " & rs("HMANIF").Value
        End If
        Return ""
    End Function

    Private Function PlacaOK(placa As String) As Boolean
        Dim i As Integer
        Dim Ascii As Integer

        If Len(placa) <> 6 Then
            Return False
        End If

        For i = 1 To 3
            Ascii = Asc(Mid(placa, i, 1))
            If Ascii < Asc("A") Or Ascii > Asc("Z") Then
                Return False
            End If
        Next
        For i = 4 To 6
            Ascii = Asc(Mid(placa, i, 1))
            If Ascii < Asc("0") Or Ascii > Asc("9") Then
                Return False
            End If
        Next

        Return True

    End Function

End Class
