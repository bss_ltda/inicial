﻿Module BSSCommon

    Function cAlfa(Dato As String, Optional ByVal qLargo As Integer = 0) As String
        Dim i, ascii, Largo As Integer
        Dim result As String = ""

        If qLargo = 0 Then
            Largo = Len(Dato)
        Else
            Largo = qLargo
        End If

        For i = 1 To Len(Dato)
            ascii = Asc(Mid(Dato, i, 1))
            If ascii < 32 Then
                result &= "?"
            ElseIf ascii > 128 Then
                result &= "?"
            Else
                Select Case Chr(ascii)
                    Case "'"
                        result &= "''"
                    Case "á", "Á"
                        result &= "á"
                    Case "é", "É"
                        result &= "é"
                    Case "í", "Í"
                        result &= "í"
                    Case "ó", "Ó"
                        result &= "ó"
                    Case "ú", "Ú"
                        result &= "ú"
                    Case "ñ"
                        result &= "ñ"
                    Case "Ñ", "#"
                        result &= "Ñ"
                    Case Else
                        result &= Chr(ascii)
                End Select
            End If
        Next

        Return Left(result, Largo)

    End Function

    Function campoAlfa(Dato As String, Optional ByVal qLargo As Integer = 0) As String
        Dim i, ascii, Largo As Integer
        Dim result As String = ""

        If qLargo = 0 Then
            Largo = Len(Dato)
        Else
            Largo = qLargo
        End If

        For i = 1 To Len(Dato)
            ascii = Asc(Mid(Dato, i, 1))
            If ascii < 32 Then
                result &= "?"
            ElseIf ascii > 128 Then
                result &= "?"
            Else
                Select Case Chr(ascii)
                    Case "á", "Á"
                        result &= "á"
                    Case "é", "É"
                        result &= "é"
                    Case "í", "Í"
                        result &= "í"
                    Case "ó", "Ó"
                        result &= "ó"
                    Case "ú", "Ú"
                        result &= "ú"
                    Case "ñ"
                        result &= "ñ"
                    Case "Ñ", "#"
                        result &= "Ñ"
                    Case Else
                        result &= Chr(ascii)
                End Select
            End If
        Next

        Return Left(result, Largo)

    End Function


    Function ceros(val As Object, C As Integer) As String
        Dim sCeros As String = New String("0", C)

        If CInt(C) > Len(Trim(val)) Then
            ceros = Right(sCeros & Trim(val), C)
        Else
            ceros = val
        End If
        Return ceros

    End Function


    Public Function tsAS400(Fecha As Date) As String
        Return Fecha.ToString("yyyy-mm-dd-hh.mm.ss.000000")
    End Function

    Public Function dtAS400(Fecha As String) As String
        Return Left(Fecha, 10) & " " & Replace(Mid(Fecha, 12, 5), ".", ":")
    End Function

    Public Function dateDecToDate(qFecha As String) As Date
        Dim Fecha As Date
        Dim iFecha As Integer = CInt(qFecha)
        DateTime.TryParseExact(iFecha.ToString("####-##-##"), "yyyy-MM-dd", Nothing, Globalization.DateTimeStyles.None, Fecha)
        Return Fecha
    End Function
    Public Function dateDecToDate(qFecha As Integer) As Date
        Dim Fecha As Date
        DateTime.TryParseExact(qFecha.ToString("####-##-##"), "yyyy-MM-dd", Nothing, Globalization.DateTimeStyles.None, Fecha)
        Return Fecha
    End Function

    Function DateDec(Fecha)
        Return Fecha.ToString("yyyymmdd")
    End Function

    Function TimeDec(Fecha)
        Return Fecha.ToString("hhnnss")
    End Function

    Function HoraFromDEC(Fecha As String) As Date
        Return DateSerial(Left(Fecha, 4), Mid(Fecha, 5, 2), Mid(Fecha, 7, 2)) & " " & TimeSerial(Mid(Fecha, 9, 2), Mid(Fecha, 11, 2), Mid(Fecha, 13, 2))
    End Function

    Function LastDayMonth(Fecha) As Date

        LastDayMonth = DateSerial(Year(Fecha), Month(Fecha), 1)
        LastDayMonth = DateAdd("m", 1, LastDayMonth)
        LastDayMonth = DateAdd("d", -1, LastDayMonth)
        Return LastDayMonth

    End Function

    Function paramWebConfig(webConfig, param)
        Dim oXML, oNode, oChild, oAttr

        oXML = CreateObject("Microsoft.XMLDOM")
        oXML.Async = "false"
        oXML.Load(webConfig)
        oNode = oXML.GetElementsByTagName("appSettings").Item(0)
        oChild = oNode.GetElementsByTagName("add")
        For Each oAttr In oChild
            If UCase(oAttr.getAttribute("key")) = UCase(param) Then
                Return oAttr.getAttribute("value")
            End If
        Next
        Return ""

    End Function


End Module
