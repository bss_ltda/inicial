﻿Imports System.Xml.Serialization
Imports System.IO


Module Principal
    Private DB As clsConexion

    Sub Main(ByVal Parametros As String())
        Dim i As Integer
        Dim fSql As String
        Dim rs As New ADODB.Recordset
        Dim resultado As String

        If Not AbreConexion(Parametros(0)) Then
            Exit Sub
        End If

        fSql = " SELECT * FROM  RFTASK WHERE TNUMREG = " & Parametros(0)
        If DB.OpenRS(rs, fSql) Then
            Console.WriteLine(rs("TCATEG").Value)
            For i = 0 To UBound(Parametros)
                DB.gsKeyWords &= Parametros(i) & " "
            Next
            Select Case (rs("TCATEG").Value)
                Case "INDPROD"
                    IndicesProduccion(Parametros(1), Parametros(2))
                Case "REP_DESTINOSEGURO"
                    Dim rep As New ReporteDestinoSeguro
                    rep.DB = DB
                    rep.Consolidado = rs("TPRM").Value
                    resultado = rep.reporte()
                    fSql = " UPDATE RFTASK SET "
                    fSql = fSql & " STS1 = " & IIf(resultado <> "", -1, 1) & ", "
                    fSql = fSql & " TUPDDAT = NOW() , "
                    fSql = fSql & " TMSG = '" & resultado & "'"
                    fSql = fSql & " WHERE TNUMREG = " & rs("TNUMREG").Value
                    DB.ExecuteSQL(fSql)
                Case "BODSAT_SALDO"
                    DB.ExecuteSQL(fSql)
            End Select
        End If
        rs.Close()
        DB.Close()

    End Sub

    Sub IndicesProduccion(Desde As String, Hasta As String)
        Dim rs As New ADODB.Recordset
        Dim fDesde As Date = dateDecToDate(Desde)
        Dim fHasta As Date = dateDecToDate(Hasta)

        Do While fDesde <= fHasta
            Console.WriteLine(Format(fDesde, "yyyyMMdd"))
            DB.ExecuteSQL("{{ CALL RRPRIND '" & Format(fDesde, "yyyyMMdd") & "' }}")
            fDesde = DateAdd("d", 1, fDesde)
        Loop

        comandoEjecutado("INDPROD", "OK")

    End Sub

    Function AbreConexion(ModuloActual As String) As Boolean

        DB = New clsConexion
        DB.gsDatasource = My.Settings.AS400
        DB.gsLib = My.Settings.AMBIENTE
        DB.ModuloActual = ModuloActual
        Return DB.Conexion()

    End Function

    Sub comandoEjecutado(ByVal rutina As String, ByVal msg As String)
        Dim fSql As String

        fSql = " UPDATE ZCC SET CCUDC1 = 0"
        fSql = fSql & ", CCNOT1 = 'Finalizó " & Now.ToString("yyyy-MM-dd HH:mm:ss") & "' "
        fSql = fSql & ", CCNOT2 = '" & Left(msg, 30) & "'  "
        fSql = fSql & " WHERE CCTABL = 'RUTINAS'  AND CCCODE = '" & UCase(rutina) & "' "
        DB.ExecuteSQL(fSql)

    End Sub

    <Serializable()> _
    Public Class SaldoInventario
        Public Usuario As String
        Public Password As String
        Public Bodega As String
        Public CodigoProducto As String

        'Public Sub New(Usuario As String, Password As String, Bodega As String, CodigoProducto As String)
        '    Me.Usuario = Usuario
        '    Me.Password = Password
        '    Me.Bodega = Bodega
        '    Me.CodigoProducto = CodigoProducto
        'End Sub

    End Class

    Public Sub saldito()

        Dim si As New SaldoInventario '("u", "p", "b", "p")

        With si
            .Usuario = "u"
            .Password = "p"
            .Bodega = "EM"
            .CodigoProducto = "p"
        End With

        Dim xml_serializer As New XmlSerializer(GetType(SaldoInventario))
        Dim string_writer As New StringWriter
        xml_serializer.Serialize(string_writer, si)
        Console.WriteLine(string_writer.ToString())
        string_writer.Close()


    End Sub

End Module
