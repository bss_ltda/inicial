﻿Imports System.IO
Imports IBM.Data.DB2.iSeries
Imports System.Text

Public Class ClsDatos
    Public conexionIBM As New iDB2Connection()
    Private adapterIBM As iDB2DataAdapter = New iDB2DataAdapter()
    Private transIBM As iDB2Transaction
    Private builderIBM As iDB2CommandBuilder
    Private commandIBM As iDB2Command
    Private numFilas As Integer
    Private dt As DataTable
    Private WithEvents ds As DataSet
    Private continuarguardando As Boolean
    Public appPath As String = Path.GetFullPath(My.Application.Info.DirectoryPath & "\Log\")
    Public mensaje As String
    Public fSql As String
    Private data
    Public lastError As String = ""
    Public ModuloActual As String = ""
    Public bDateTimeToChar As Boolean = False

    Sub New()
        Try
            ' Si el directorio no existe, crearlo
            Console.WriteLine("Creando directorio..." & appPath)
            If Not Directory.Exists(appPath) Then
                Directory.CreateDirectory(appPath)
            End If
        Catch ex As Exception
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\DirectorioError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo Establecer Directorio." & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try
    End Sub

#Region "ACTUALIZAR"

    ''' <summary>
    ''' Actualiza la tabla RFTASK 
    ''' </summary>
    ''' <param name="datos">RFTASK</param>
    ''' <param name="tipo">0- SET STS2 = @STS2 WHERE TNUMREG = @TNUMREG</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 23/05/2016</remarks>
    Public Function ActualizarRFTASKRow(ByVal datos As RFTASK, ByVal tipo As Integer) As Boolean
        Select Case tipo
            Case 0
                fSql = "UPDATE RFTASK SET STS2 = " & datos.STS2 & " WHERE TNUMREG = " & datos.TNUMREG
        End Select

        commandIBM = New iDB2Command
        commandIBM.Connection = conexionIBM
        Try
            commandIBM.CommandText = fSql
            commandIBM.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As Data.SqlClient.SqlException
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFTASKError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla RFTASK.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFTASKError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla RFTASK. Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                data = Nothing
                data = New RFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .OPERACION = "Actualizar RFTASK"
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarRFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

#End Region

#Region "CONSULTAR"

    ''' <summary>
    ''' Consulta la tabla BSSFFNFAC con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por FTERNIT(0) - FDESDE(1) y FHASTA(2), 1- FDESDE(0) - FHASTA(1) y FTERNIT(2)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 23/05/2016</remarks>
    Public Function ConsultarBSSFFNFACdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As ADODB.Recordset
        dt = Nothing

        Select Case tipoparametro
            Case 0
                fSql = " SELECT FDESDE, FHASTA, FTERNIT, VENDOR, VNDNAM, VNDAD1, VMUF02 "
                fSql &= " FROM BSSFFNFAC INNER JOIN BSSAVM ON BSSFFNFAC.FPROCOD = BSSAVM.VENDOR  "
                fSql &= String.Format(" WHERE FTERNIT = '{0}' ", parametroBusqueda(0))
                fSql &= String.Format(" AND BSSFFNFAC.FDESDE = {0} ", parametroBusqueda(1))
                fSql &= String.Format("AND BSSFFNFAC.FHASTA = {0} ", parametroBusqueda(2))
            Case 1
                fSql = " SELECT FDOCUM, Sum(CAST(FVALFAC AS FLOAT)) AS VALOR, CAST(FPORIVA AS FLOAT) AS FPORIVA, Sum(CAST(FVALIVA AS FLOAT)) AS IVA, CAST(FPORRTI AS FLOAT) AS FPORRTI, Sum(CAST(FVALRTI AS FLOAT)) AS RETENCION, Sum(CAST(FVALNET AS FLOAT)) AS NETO  "
                fSql &= " FROM BSSFFNFAC  "
                fSql &= String.Format(" WHERE FDESDE = {0} ", parametroBusqueda(0))
                fSql &= String.Format(" AND FHASTA = {0} ", parametroBusqueda(1))
                fSql &= String.Format(" AND FTERNIT = '{0}' ", parametroBusqueda(2))
                fSql &= " GROUP BY FDOCUM, FPORIVA, FPORRTI "
        End Select
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)

        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return ConvertToRecordset(dt)
    End Function

    ''' <summary>
    ''' Consulta la tabla BSSFFNFAC con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por FTERNIT(0) - FDESDE(1) y FHASTA(2), 1- FDESDE(0) - FHASTA(1) y FTERNIT(2)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 23/05/2016</remarks>
    Public Function ConsultarBSSFFNFAC2dt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        Select Case tipoparametro
            Case 0
                fSql = " SELECT VNDNAM, VENDOR, VMID, VMDATN, FDESDE, FHASTA, FTERNIT "
                fSql &= " FROM BSSFFNFAC INNER JOIN BSSAVM ON BSSFFNFAC.FPROCOD = BSSAVM.VENDOR  "
                fSql &= " GROUP BY VNDNAM, VENDOR, VMID, VMDATN, FDESDE, FHASTA, FTERNIT "
        End Select
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)

        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla RFPARAM con un determinado parametro de busqueda.Retorna un String
    ''' </summary>
    ''' <param name="prm">parametro de Busqueda</param>
    ''' <param name="Campo"></param>
    ''' <returns>Un String</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 23/05/2016</remarks>
    Public Function ConsultarRFPARAMString(ByVal prm As String, ByVal Campo As String) As String
        dt = Nothing


        If Campo = "" Then
            Campo = "CCNOT1"
        End If
        fSql = "SELECT " & Campo & " AS DATO "
        fSql &= " FROM RFPARAM "
        fSql &= " WHERE CCTABL='LXLONG' AND UPPER(CCCODE) = UPPER('" & prm & "')"

        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)

        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt.Rows(0).Item("DATO")
    End Function

    ''' <summary>
    ''' Consulta la tabla ZCC con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por CCCODE(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 23/05/2016</remarks>
    Public Function ConsultarZCCdt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT CCNOT1, CCUDC1 FROM ZCC WHERE CCTABL ='PRTFACTU' AND CCCODE = '" & parametroBusqueda(0) & "' AND CCUDC2 = 1"
        End Select
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)

        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

    ''' <summary>
    ''' Consulta la tabla ZCCL01 con un determinado parametro de busqueda.Retorna un DataTable
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por CCCODE(0)</param>
    ''' <returns>Un DataTable</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 23/05/2016</remarks>
    Public Function ConsultarZCCL01dt(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As DataTable
        dt = Nothing

        Select Case tipoparametro
            Case 0
                fSql = "SELECT CCSDSC, CCUDC1, CCNOT2, CCDESC  FROM ZCCL01 WHERE CCTABL='RFVBVER' AND UPPER(CCCODE) = '" & parametroBusqueda(0) & "'"
        End Select
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)

        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt
    End Function

#End Region

#Region "FUNCIONES"

    Public Sub WrtTxtError(Archivo As String, ByVal Texto As String)
        Dim thisFile As System.IO.FileInfo
        Try
            Using w As StreamWriter = File.AppendText(Archivo)
                Log(Texto, w)
            End Using
        Catch ex As Exception
            thisFile = My.Computer.FileSystem.GetFileInfo(Archivo)
            ' Si el directorio no existe, crearlo
            If Not Directory.Exists(thisFile.Directory.ToString) Then
                Directory.CreateDirectory(thisFile.Directory.ToString)
            End If
            Try
                Using w As StreamWriter = File.AppendText(Archivo)
                    Log(Texto, w)
                End Using
            Catch ex1 As Exception

            End Try
        End Try
    End Sub

    Public Sub Log(logMessage As String, w As TextWriter)
        w.Write(vbCrLf + "Entrada de Log : ")
        w.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString())
        w.WriteLine("  :{0}", logMessage)
        w.WriteLine(" ")
    End Sub

    Public Function CheckVer() As Boolean
        Dim lVer As String
        Dim msg As String
        Dim NomApp As String
        Dim App As clsApp = New clsApp(System.Reflection.Assembly.GetExecutingAssembly)
        Dim Resultado As Boolean = True
        Dim gsAppName As String
        Dim gsAppPath
        Dim gsAppVersion As String
        Dim dtZCCL01 As DataTable
        Dim parametro(0) As String


        gsAppPath = App.Path
        gsAppName = App.EXEName
        gsAppVersion = App.Major.ToString & "." & App.Minor.ToString & "." & App.Revision.ToString

        lVer = "No hay registro"

        parametro(0) = gsAppName.ToUpper
        dtZCCL01 = ConsultarZCCL01dt(parametro, 0)
        If dtZCCL01 IsNot Nothing Then
            lVer = Trim(dtZCCL01.Rows(0).Item("CCSDSC"))
            NomApp = dtZCCL01.Rows(0).Item("CCDESC")
            If InStr(lVer, "*NOCHK") > 0 Then
                Resultado = True
            ElseIf InStr(lVer, "(" & gsAppVersion & ")") > 0 Then
                Resultado = True
            End If
            If Resultado And dtZCCL01.Rows(0).Item("CCUDC1") = 0 Then
                msg = "La aplicacion " & NomApp & " no se puede usar en este momento." & vbCr
                msg = msg & "Razon: " & vbCr
                msg = msg & "=========================================" & vbCr
                If Trim(dtZCCL01.Rows(0).Item("CCNOT2")) = "" Then
                    msg = msg & "Aplicacion en Mantenimiento." & vbCr
                Else
                    msg = msg & dtZCCL01.Rows(0).Item("CCNOT2") & vbCr
                End If
                msg = msg & "=========================================" & vbCr
                Return False
            End If
        End If

        If Not Resultado Then
            msg = "Aplicacion " & App.EXEName.ToUpper & vbCr & _
                   "=========================================" & vbCr & _
                   "Versión incorrecta del programa." & vbCr & _
                   "Versión Registrada: " & lVer & vbCr & _
                   "Versión Actual: " & "(" & gsAppVersion & ")"
            lastError = msg
        End If

        Return Resultado

    End Function

    Public Sub WrtSqlError(sql As String, Descrip As String)
        Dim aApl() As String
        Dim sApp As String = "VB.NET"

        Try
            aApl = Split(System.Reflection.Assembly.GetExecutingAssembly.FullName, ", ")
            If UBound(aApl) > 0 Then
                sApp = aApl(0) & IIf(ModuloActual <> "", "." & ModuloActual, "")
            End If

            data = Nothing
            data = New RFLOG
            With data
                .USUARIO = System.Net.Dns.GetHostName()
                .PROGRAMA = sApp
                .ALERT = "1"
                .EVENTO = Replace(Descrip, "'", "''")
                .TXTSQL = Replace(sql, "'", "''")
            End With
            GuardarRFLOGRow(data)

        Catch ex As Exception

        End Try

    End Sub

    Public Function AbrirConexion() As Boolean
        Dim abierta As Boolean = False
        Try
            If conexionIBM.State = ConnectionState.Open Then
                abierta = True
            Else
                conexionIBM = New iDB2Connection(My.Settings.BASEDATOS)
                conexionIBM.Open()
                abierta = True
            End If
        Catch ex1 As iDB2Exception
            abierta = False
            mensaje = ex1.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo realizar la conexión.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex1.InnerException.ToString & vbCrLf & "Conexion: " & conexionIBM.ConnectionString & vbCrLf & ex1.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Catch ex As Exception
            abierta = False
            mensaje = ex.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo realizar la conexión.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.InnerException.ToString & vbCrLf & "Conexion: " & conexionIBM.ConnectionString & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try
        Return abierta
    End Function

    Public Function CerrarConexion() As Boolean
        Dim cerrada As Boolean = False
        Try
            conexionIBM.Close()
            cerrada = True
        Catch ex1 As iDB2Exception
            cerrada = False
            mensaje = ex1.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo Cerrar la conexión.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex1.InnerException.ToString & vbCrLf & "Conexion: " & conexionIBM.ConnectionString & vbCrLf & ex1.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Catch ex As Exception
            cerrada = False
            mensaje = ex.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo Cerrar la conexión.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.InnerException.ToString & vbCrLf & "Conexion: " & conexionIBM.ConnectionString & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try
        Return cerrada
    End Function

    Public Function ConvertToRecordset(inTable As DataTable) As ADODB.Recordset
        Dim result As ADODB.Recordset = New ADODB.Recordset()
        result.CursorLocation = ADODB.CursorLocationEnum.adUseClient
        Dim resultFields As ADODB.Fields = result.Fields
        Dim inColumns As System.Data.DataColumnCollection = inTable.Columns
        For Each inColumn As DataColumn In inColumns
            resultFields.Append(inColumn.ColumnName, TranslateType(inColumn.DataType), inColumn.MaxLength, IIf(inColumn.AllowDBNull = inColumn.AllowDBNull, ADODB.FieldAttributeEnum.adFldIsNullable, Nothing), Nothing)
        Next

        result.Open(System.Reflection.Missing.Value, System.Reflection.Missing.Value, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic, 0)
        For Each dr As DataRow In inTable.Rows
            result.AddNew(System.Reflection.Missing.Value, System.Reflection.Missing.Value)
            For columnIndex As Integer = 0 To inColumns.Count - 1
                resultFields(columnIndex).Value = dr(columnIndex)
            Next
        Next

        Return result
    End Function

    Function TranslateType(columnType As Type) As ADODB.DataTypeEnum
        Select Case columnType.UnderlyingSystemType.ToString()
            Case "System.Boolean"
                Return ADODB.DataTypeEnum.adBoolean
            Case "System.Byte"
                Return ADODB.DataTypeEnum.adUnsignedTinyInt
            Case "System.Char"
                Return ADODB.DataTypeEnum.adChar
            Case "System.DateTime"
                Return ADODB.DataTypeEnum.adDate
            Case "System.Decimal"
                Return ADODB.DataTypeEnum.adCurrency
            Case "System.Double"
                Return ADODB.DataTypeEnum.adDouble
            Case "System.Int16"
                Return ADODB.DataTypeEnum.adSmallInt
            Case "System.Int32"
                Return ADODB.DataTypeEnum.adInteger
            Case "System.Int64"
                Return ADODB.DataTypeEnum.adBigInt
            Case "System.SByte"
                Return ADODB.DataTypeEnum.adTinyInt
            Case "System.Single"
                Return ADODB.DataTypeEnum.adSingle
            Case "System.UInt16"
                Return ADODB.DataTypeEnum.adUnsignedSmallInt
            Case "System.UInt32"
                Return ADODB.DataTypeEnum.adUnsignedInt
            Case "System.UInt64"
                Return ADODB.DataTypeEnum.adUnsignedBigInt
            Case "System.String"

                Return ADODB.DataTypeEnum.adVarChar
        End Select

    End Function

    Public Sub wrtLog(Descrip As String, gsKeyWords As String, Sql As String)
        Console.WriteLine(Descrip)
        data = Nothing
        data = New RFLOG
        With data
            .USUARIO = System.Net.Dns.GetHostName()
            .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
            .ALERT = "1"
            .EVENTO = Replace(Descrip, "'", "''")
            .LKEY = gsKeyWords
            .TXTSQL = Replace(Sql, "'", "''")
        End With
        GuardarRFLOGRow(data)


        'fSql = " INSERT INTO RFLOG( "
        'vSql = " VALUES ( "
        'fSql = fSql & " USUARIO   , " : vSql = vSql & "'" & System.Net.Dns.GetHostName() & "', "      '//502A
        'fSql = fSql & " PROGRAMA  , " : vSql = vSql & "'DETAFACT', "      '//502A
        'fSql = fSql & " ALERT     , " : vSql = vSql & "1, "      '//1P0
        'fSql = fSql & " EVENTO    , " : vSql = vSql & "'" & Replace(Descrip, "'", "''") & "', "      '//20002A
        'fSql = fSql & " LKEY      , " : vSql = vSql & "'" & gsKeyWords & "', "     '//30A   Clave         
        'fSql = fSql & " TXTSQL    ) " : vSql = vSql & "'" & Replace(Sql, "'", "''") & "' ) "      '//5002A
        'DB.Execute(fSql & vSql)

    End Sub

#End Region

#Region "GUARDAR"

    ''' <summary>
    ''' Guarda RFLOG
    ''' </summary>
    ''' <param name="datos">RFLOG</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 23/05/2016</remarks>
    Public Function GuardarRFLOGRow(ByVal datos As RFLOG) As Boolean

        fSql = "INSERT INTO RFLOG (USUARIO, OPERACION, PROGRAMA, EVENTO, TXTSQL, ALERT, LKEY)"
        fSql &= " VALUES ('" & datos.USUARIO & "', '" & datos.OPERACION & "', '" & datos.PROGRAMA & "', '" & datos.EVENTO.Replace("'", "''") & "', '" & datos.TXTSQL.Replace("'", "''") & "', " & datos.ALERT & ", '" & IIf(datos.LKEY = "", DBNull.Value, datos.LKEY) & "')"
        'System.Net.Dns.GetHostName()
        'My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString

        commandIBM = New iDB2Command
        commandIBM.Connection = conexionIBM
        Try
            commandIBM.CommandText = fSql
            commandIBM.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As Data.SqlClient.SqlException
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo guardar en la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Catch ex As Exception
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo guardar en la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Finally

        End Try
        Return continuarguardando
    End Function

#End Region


End Class

Public Class RFLOG
    Public USUARIO As String
    Public PROGRAMA As String
    Public OPERACION As String
    Public EVENTO As String
    Public TXTSQL As String
    Public ALERT As String
    Public LKEY As String
End Class

Public Class RFTASK
    Public TLXMSG As String
    Public STS1 As String
    Public STS2 As String
    Public TMSG As String
    Public TNUMREG As String
End Class
