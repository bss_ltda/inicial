﻿Imports PDF_In_The_BoxCtl
Imports CLDatos
Imports ADODB
Imports System.Text
Imports System.IO

Public Class FrmDetalle
    Private datos As New ClsDatos
    Dim bPaginaNueva As Boolean
    Private dtCab As ADODB.Recordset
    Private dtDet As ADODB.Recordset

    Dim DocScanner As Integer, Observ As String, ImprimeIAL As String
    Dim bColor As Boolean
    Dim bQuimicos As Boolean
    Dim bExport As Boolean
    Dim Flag3
    Dim Sellos() As String
    Dim Fila As Integer
    Dim TotNotas As Integer
    Dim TotFilas As Integer
    Dim Ta As TBoxTable
    Dim totalvalor As Double

    Const CELDA_NORMAL As String = "BrushColor = 255, 255, 255"
    Const CELDA_SOMBRA As String = "BrushColor = 255, 255, 255"
    Const TABLE_HEADER_STYLE As String = "ChildrenStyle=""BorderStyle=rect;FontSize=9;Fontname=Arial;Alignment=Center;VertAlignment=Center"""
    Const ESTILO_TIT As String = "Normal;fontsize=6;Fontname=Corbel;BorderColor=Black;Alignment=Center;VertAlignment=Center"
    Private data
    Private CARPETA_SITIO As String
    Private CARPETA_IMG As String


    Private Sub FrmDetalle_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim parametro As String
        Dim aSubParam() As String
        Dim bGenerar As Boolean
        Dim campoParam As String = ""
        Dim continuar As Boolean

        Flag3 = 1
        Try
            Environment.GetCommandLineArgs()
            For Each parametro In Environment.GetCommandLineArgs()
                aSubParam = Split(parametro, "=")
                Select Case UCase(aSubParam(0))
                    Case "TAREA"
                        TASK_No = aSubParam(1)
                    Case "FDESDE"
                        FDESDE = aSubParam(1)
                    Case "FHASTA"
                        FHASTA = aSubParam(1)
                    Case "FTERNIT"
                        FTERNIT = aSubParam(1)
                End Select
            Next

            datos.bDateTimeToChar = True

            If Not datos.AbrirConexion() Then
                Application.Exit()
            End If

            If My.Settings.LOCAL.ToUpper() = "SI" Then
                campoParam = "CCNOT2"
            End If
            CARPETA_SITIO = datos.ConsultarRFPARAMString("SITIO_CARPETA", campoParam)
            'DB.getLXParam("SITIO_CARPETA", "TM", campoParam)
            CARPETA_IMG = datos.ConsultarRFPARAMString("CARPETA_IMG", campoParam)
            'DB.getLXParam("CARPETA_IMG", "TM", campoParam)
            APP_PATH = My.Application.Info.DirectoryPath & "\logo_FEDURO.jpg" '  DB.getLXParam("LOGOEMPRESA", "TM", campoParam)
            If My.Settings.PRUEBAS = "SI" Then
                bPRUEBAS = True
            Else
                bPRUEBAS = False
            End If
        Catch ex As Exception
            Dim cadena As String = Replace("C:\Feduro\DetalleFactura\errordetalle.txt", "\\", "\")
            Try
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(cadena)
                sb.AppendLine("LOG " & PDF_Folder & ": " & ex.ToString)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            Catch ex1 As Exception

            End Try
        End Try

        Try
            PDF_Folder = CARPETA_SITIO & "xm\pdf\DetaFact"
            LOG_File = PDF_Folder & "\" & FTERNIT & ".txt"
            datos.WrtTxtError(LOG_File, FTERNIT)
        Catch ex As Exception
            Dim cadena As String = Replace("C:\Feduro\DetalleFactura\errordetalle.txt", "\\", "\")
            Try
                ' Create an instance of StreamReader to read from a file.
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(cadena)
                sb.AppendLine("LOG " & PDF_Folder & ": " & ex.ToString)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            Catch ex1 As Exception

            End Try
        End Try

        Dim arguments As String() = Environment.GetCommandLineArgs()
        datos.WrtTxtError(LOG_File, String.Join(", ", arguments))

        If Not datos.CheckVer() Then
            datos.WrtSqlError(datos.lastError, "")
            End
        End If

        If My.Settings.PRUEBAS = "SI" Then
            GeneraPdf(bGenerar)
        Else
            Try
                If GeneraPdf(bGenerar) Then
                    Data = Nothing
                    data = New RFTASK
                    With data
                        .STS2 = 1
                        .TNUMREG = TASK_No
                    End With
                    continuar = datos.ActualizarRFTASKRow(data, 0)
                    If continuar = False Then
                        'ERROR DE ACTUALIZADO
                    End If
                End If
            Catch ex As Exception
                Debug.Print(ex.Message)
                datos.WrtTxtError(LOG_File, ex.Message)
                datos.WrtSqlError(ex.Message, "GeneraPDF")
                End
            End Try
        End If

        datos.CerrarConexion()
        Application.Exit()
    End Sub

    Public Function GeneraPdf(bGenerar As Boolean) As Boolean
        Dim ArchivoPDF As String
        Dim Resultado As Boolean = False
        Dim parametro(2) As String

        Fila = 0
        parametro(0) = FTERNIT
        parametro(1) = FDESDE
        parametro(2) = FHASTA
        dtCab = datos.ConsultarBSSFFNFACdt(parametro, 0)
        If dtCab Is Nothing Then
            dtCab.Close()
            datos.WrtTxtError(LOG_File, "Factura no encontrada.")
            Return False
        End If



        'fSql = " SELECT FDESDE, FHASTA, FTERNIT, VENDOR, VNDNAM, VNDAD1, VMUF02 "
        'fSql &= " FROM BSSFFNFAC INNER JOIN BSSAVM ON BSSFFNFAC.FPROCOD = BSSAVM.VENDOR  "
        'fSql &= String.Format(" WHERE FTERNIT = '{0}' ", FTERNIT)
        'fSql &= String.Format(" AND BSSFFNFAC.FDESDE = {0} ", FDESDE)
        'fSql &= String.Format("AND BSSFFNFAC.FHASTA = {0} ", FHASTA)
        'If Not DB.OpenRS(rsCab, fSql, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly) Then
        '    rsCab.Close()
        '    WrtTxtError(LOG_File, "Factura no encontrada.")
        '    Return False
        'End If

        '*****************************************************************
        '   DETALLE
        '*****************************************************************
        parametro(0) = FDESDE
        parametro(1) = FHASTA
        parametro(2) = FTERNIT
        dtDet = datos.ConsultarBSSFFNFACdt(parametro, 1)
        If dtDet Is Nothing Then
            dtDet.Close()
            dtCab.Close()
            datos.WrtTxtError(LOG_File, "Factura sin detalle 1.")
            Return False
        End If

        'fSql = " SELECT FDOCUM, Sum(CAST(FVALFAC AS FLOAT)) AS VALOR, CAST(FPORIVA AS FLOAT) AS FPORIVA, Sum(CAST(FVALIVA AS FLOAT)) AS IVA, CAST(FPORRTI AS FLOAT) AS FPORRTI, Sum(CAST(FVALRTI AS FLOAT)) AS RETENCION, Sum(CAST(FVALNET AS FLOAT)) AS NETO  "
        'fSql &= " FROM BSSFFNFAC  "
        'fSql &= String.Format(" WHERE FDESDE = {0} ", FDESDE)
        'fSql &= String.Format(" AND FHASTA = {0} ", FHASTA)
        'fSql &= String.Format(" AND FTERNIT = '{0}' ", FTERNIT)
        'fSql &= " GROUP BY FDOCUM, FPORIVA, FPORRTI "
        'If Not DB.OpenRS(rsDet, fSql, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly) Then
        '    rsTit.Close()
        '    rsDet.Close()
        '    rsCab.Close()
        '    WrtTxtError(LOG_File, "Factura sin detalle 1.")
        '    Return False
        'End If
        '*****************************************************************
        '   FIN DETALLE
        '*****************************************************************

        'fSql = " SELECT '' VACIO1, '' VACIO2, RPNDEC, RPFDEC, RPNSTI, RPFSTI"
        'fSql &= " FROM RMMTPI"
        'fSql &= String.Format(" WHERE RPAFS = {0} ", PeriodoAAAA)
        'fSql &= String.Format(" AND RPBIM = {0} ", PeriodoMM)
        'If Not DB.OpenRS(rsNotas, fSql, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly) Then
        '    rsTit.Close()
        '    rsDet.Close()
        '    rsCab.Close()
        '    WrtTxtError(LOG_File, "Sin declaracion.")
        '    Return False
        'End If

        ArchivoPDF = PDF_Folder & "\" & FTERNIT & ".pdf"
        Resultado = ArmaPDF(ArchivoPDF)

        'dtDet.Close()
        'dtCab.Close()
        Return Resultado

    End Function

    Function ArmaPDF(ArchivoPDF As String) As Boolean
        Dim Ta As TBoxTable
        Dim Ta1 As TBoxTable
        Dim Detail As TBoxBand
        Dim Note As TBoxBand
        Dim Header As TBoxBand
        Dim Footer As TBoxBand
        Dim AfterFooter As TBoxBand
        Dim b2 As TBoxBand
        Dim sError As String
        ArmaPDF = False

        If Dir(ArchivoPDF) <> "" Then
            On Error Resume Next
            Kill(ArchivoPDF)
            If Err.Number <> 0 Then
                sError = Err.Description
                datos.WrtTxtError(LOG_File, "ArmaPDF: Borrar " & ArchivoPDF & " " & sError)
                dtDet.Close()
                dtCab.Close()
                dtDet = Nothing
                dtCab = Nothing
                datos.CerrarConexion()
                End
            End If
            On Error GoTo 0
        End If

        ' Titulos
        With Me.AxPdfBox1
            .FileName = ArchivoPDF
            .Title = "Certificado de Retencion  - " & FTERNIT
            .WantShow = bPRUEBAS
            .WantPageCount = True
            .PaperSizeName = "Letter"
            .BeginDoc()
            Fila = 0
            Flag = 0
            Ta = .CreateTable("BorderStyle=None;Margins=130,200,100,150")
            Ta.Assign(dtDet)

            bPaginaNueva = True
            b2 = .CreateBand("BorderStyle=none;Margins=130,200,100,150;FontBold=1;Alignment=Center")
            b2.Height = 50
            b2.CreateCell(1910).CreateText().Assign("DETALLE DE FACTURAS")
            b2.Put()

            totalvalor = 0

            If dtDet.BOF = True And dtDet.EOF = True Then
                dtDet.MoveFirst()
            Else
                dtDet.MoveFirst()
                Do While Not dtDet.EOF = True
                    totalvalor += CDbl(dtDet("NETO").Value)
                    dtDet.MoveNext()
                Loop
            End If

            'Tabla del Detalle
            Detail = Ta.CreateBand()
            Detail.Role = TBandRole.rlDetail
            Detail.ChildrenStyle = "FontSize=8;Fontname=Arial; VertAlignment=Center;BorderStyle=none;Alignment=Right;BorderRightMargin=10"
            Detail.CreateCell(180).CreateText("Alignment=Center").Bind("FDOCUM")
            With Detail.CreateCell(352).CreateText
                .Bind("VALOR")
                .Format = "#,##0.00"
            End With
            With Detail.CreateCell(160).CreateText
                .Bind("FPORIVA")
                .Format = "#,##0.0"
            End With
            With Detail.CreateCell(352).CreateText
                .Bind("IVA")
                .Format = "#,##0.00"
            End With
            With Detail.CreateCell(160).CreateText
                .Bind("FPORRTI")
                .Format = "#,##0.0"
            End With
            With Detail.CreateCell(352).CreateText
                .Bind("RETENCION")
                .Format = "#,##0.00"
            End With
            With Detail.CreateCell(354).CreateText
                .Bind("NETO")
                .Format = "#,##0.00"
            End With

            Detail.Breakable = True
            Header = Ta.CreateBand() 'Detail.CreateClone(TABLE_HEADER_STYLE)
            Header.Role = TBandRole.rlBodyHeader
            Header.ChildrenStyle = "fontsize=8; fontbold=1; Alignment=Center; VertAlignment=Center; BorderStyle=rect; BorderRightMargin=10"
            Header.CreateCell(180).CreateText().Assign("FATURA")
            Header.CreateCell(352).CreateText().Assign("VLR. FACTURA")
            Header.CreateCell(160).CreateText().Assign("%ITBMS")
            Header.CreateCell(352).CreateText().Assign("VALOR ITBMS")
            Header.CreateCell(160).CreateText().Assign("% RET")
            Header.CreateCell(352).CreateText().Assign("VLR RETENCIÓN")
            Header.CreateCell(354).CreateText().Assign("NETO A PAGAR")

            Ta.Put()

            b2 = .CreateBand("BorderStyle=none;Margins=130,200,100,150")
            b2.Height = 50
            b2.CreateCell(1910).CreateText().Assign("")
            b2.Put()

            b2 = .CreateBand("BorderStyle=none;Margins=130,200,100,150;Alignment=Center")
            b2.ChildrenStyle = "fontsize=8;Fontname=Arial;Alignment=Center; VertAlignment=Center; BorderStyle=none"
            b2.Height = 50
            b2.CreateCell(1519).CreateText("FontBold=1;Alignment=Right").Assign("TOTAL: ")
            b2.CreateCell(354).CreateText("Alignment=Right").Assign(FormatNumber(totalvalor.ToString, 2))
            b2.Put()
            Flag = 1

            .EndDoc()
            Debug.Print(.FileName())
            If bPRUEBAS Then
                .ShowDoc()
            End If

            ArchivoPDF = .FileName

        End With
        Return True

    End Function

    Sub Renglon(txt As String)
        Dim b As TBoxBand
        b = AxPdfBox1.CreateBand("BorderStyle=None;Margins=130,200,100,180")
        b.CreateCell(1910).CreateText("FontSize=8;Fontname=Arial;BorderStyle=None;Alignment=Center;VertAlignment=Center").Assign(txt.ToString)
        b.Put()

    End Sub

    Private Sub AxPdfBox1_OnBottomOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnBottomOfPageEvent) Handles AxPdfBox1.OnBottomOfPage
        'Dim b As TBoxBand
        Dim rs As New ADODB.Recordset

        With AxPdfBox1

        End With

    End Sub

    Private Sub AxPdfBox1_OnTopOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnTopOfPageEvent) Handles AxPdfBox1.OnTopOfPage
        Dim b As TBoxBand
        With Me.AxPdfBox1
            .DefineStyle("Normal", "0; Margins=130,200,100,150; fontsize=10;Fontname=Corbel;BorderColor=Black")
            .Style = "Normal"
            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "BorderStyle=None;Alignment=Center"

            b.CreateCell(380).CreateImage("Alignment=Center;VertAlignment=Top").Assign(My.Application.Info.DirectoryPath & "\logo_FEDURO.jpg")
            b.CreateCell(1530).CreateText("Alignment=Center;VertAlignment=Top").Assign(My.Application.Info.DirectoryPath & "\Cabecera.rtf")
            b.Put()

            b = .CreateBand("Margins=130,200,100,150")
            b.Height = 35
            b.CreateCell(1910, "BorderStyle=None").CreateText().Assign("")
            b.Put()

            b = .CreateBand("Margins=130,200,100,150")
            b.Height = 35
            b.CreateCell(1910, "BorderStyle=None").CreateText().Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Left;VertAlignment=Center"
            Dim fechad As String = FDESDE.Substring(0, 4) & "-" & FDESDE.Substring(4, 2) & "-" & FDESDE.Substring(6, 2)
            Dim fechah As String = FHASTA.Substring(0, 4) & "-" & FHASTA.Substring(4, 2) & "-" & FHASTA.Substring(6, 2)
            b.CreateCell(345, "BorderStyle=none").CreateText("FontName=Arial; FontSize=11; FontBold=1").Assign("FECHA DESDE: ")
            b.CreateCell(920, "BorderStyle=none").CreateText("FontName=Arial; FontSize=11").Assign(CDate(fechad).ToString("yyyy-MM-dd"))
            b.CreateCell(345, "BorderStyle=none").CreateText("FontName=Arial; FontSize=11; FontBold=1;Alignment=Right").Assign("FECHA HASTA: ")
            b.CreateCell(300, "BorderStyle=none").CreateText("FontName=Arial; FontSize=11;Alignment=Right").Assign(CDate(fechah).ToString("yyyy-MM-dd").ToUpper)
            b.Put()

            b = .CreateBand("Margins=130,200,100,150")
            b.Height = 40
            b.CreateCell(1910, "BorderStyle=None").CreateText().Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=11;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Left;VertAlignment=Center;FontBold=1"
            b.CreateCell(290).CreateText().Assign("PROVEEDOR: ")
            b.CreateCell(1050).CreateText("FontBold=0").Assign(CStr(Trim(dtCab("VNDNAM").Value)))
            b.CreateCell(570).CreateText("Alignment=Right").Assign("RUC: " & CStr(Trim(FTERNIT.ToString)) & " DV " & CStr(Trim(dtCab("VMUF02").Value)))
            b.Put()

            b = .CreateBand("Margins=130,200,100,150")
            b.Height = 40
            b.CreateCell(1910, "BorderStyle=None").CreateText().Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=11;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Left;VertAlignment=Center"
            b.CreateCell(210).CreateText("FontBold=1").Assign("PAGAR A: ")
            b.CreateCell(1700).CreateText().Assign(CStr(Trim(dtCab("VNDNAM").Value)))
            b.Put()

            b = .CreateBand("Margins=130,200,100,150")
            b.Height = 30
            b.CreateCell(1910, "BorderStyle=None").CreateText().Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=11;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Left;VertAlignment=Center"
            b.CreateCell(270).CreateText("fontbold=1").Assign("DIRECCIÓN: ")
            b.CreateCell(1640).CreateText().Assign(CStr(Trim(dtCab("VNDAD1").Value)))
            b.Put()

            b = .CreateBand("Margins=130,200,100,150")
            b.Height = 40
            b.CreateCell(1910, "BorderStyle=None").CreateText().Assign("")
            b.Put()

        End With

    End Sub

    Private Sub AxPdfBox1_BeforePutBand4(sender As System.Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_BeforePutBandEvent) Handles AxPdfBox1.BeforePutBand
        If e.aBand.Role = 1 Then ' TBandRole.rlDetail  rlDetail = 1
            If Flag3 = -1 Then
                e.aBand.BrushColor = RGB(215, 215, 215)
            Else
                e.aBand.BrushColor = RGB(255, 255, 255)
            End If
            Flag3 = Flag3 * -1
            bColor = Not bColor
        End If

        Fila = Fila + 1
        If Fila > 44 Then
            AxPdfBox1.NewPage()
            Fila = 0
        End If
    End Sub

End Class