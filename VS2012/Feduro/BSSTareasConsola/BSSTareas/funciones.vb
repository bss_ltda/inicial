﻿Imports System.IO
Imports System.Text

Module funciones
    Public gsKeyWords As String = ""
    Public gsConnectionString As String


    Public Sub wrtLogHtml(sql As String, ByVal Descrip As String)
        Dim fEntrada As String = ""
        Dim linea As String = Format(Now(), "yyyy-MM-dd HH:mm:ss") & "|"
        Dim strStreamW As Stream = Nothing
        Dim sApp As String = My.Application.Info.ProductName & "." & _
                                   My.Application.Info.Version.Major & "." & _
                                   My.Application.Info.Version.Minor & "." & _
                                   My.Application.Info.Version.Revision

        Descrip = Descrip.Replace(vbCrLf, vbCr)
        Descrip = Descrip.Replace(vbLf, vbCr)
        Descrip = Descrip.Replace(vbCr, "<BR/>")

        If gsKeyWords.Trim <> "" Then
            Descrip = gsKeyWords & "<br>" & Descrip
        End If

        linea &= System.Net.Dns.GetHostName() & "|" & sApp & "|" & Descrip.Replace("|", "?") & "|" & sql.Replace("|", "?")

        fEntrada = Replace("c:\Feduro\log\log" & Format(Now(), "yyyyMM") & ".csv", "\\", "\")

        If File.Exists(fEntrada) Then
            strStreamW = File.Open(fEntrada, FileMode.Open) 'Abrimos el archivo
        Else
            strStreamW = File.Create(fEntrada) ' lo creamos
        End If
        strStreamW.Close()
        Try
            ' Create an instance of StreamReader to read from a file.
            Dim sr As StreamReader = New StreamReader(fEntrada)
            Dim sb As New StringBuilder()
            sb.Append(sr.ReadToEnd())
            sb.Append(linea)
            sr.Close()

            Dim sw As StreamWriter = New StreamWriter(fEntrada)
            sw.WriteLine(sb.ToString())
            sw.Close()

        Catch E As Exception

        End Try

    End Sub


End Module
