﻿Module Tareas

    Sub revisaTareas()
        Dim fSql As String
        Dim pgm, param, exe As String
        Dim rs As New ADODB.Recordset
        Dim sts As Integer
        Dim msgT As String
        Dim tLXmsg As String
        Dim i As Single

        Dim DB As New ADODB.Connection
        For i = 1 To 2
            If i = 1 Then
                gsConnectionString = "Provider=IBMDA400.DataSource;Data Source=192.168.2.220;" & _
                                     "User ID=" & My.Settings.AS400_USER & ";Password=" & My.Settings.AS400_PASS & ";" & _
                                     "Persist Security Info=True;Convert Date Time To Char=TRUE;Application Name=DFU001;" & _
                                     "Default Collection=BSSPTY;Force Translate=0;"
            Else
                gsConnectionString = "Provider=IBMDA400.DataSource;Data Source=192.168.2.220;" & _
                                     "User ID=" & My.Settings.AS400_USER & ";Password=" & My.Settings.AS400_PASS & ";" & _
                                     "Persist Security Info=True;Convert Date Time To Char=TRUE;Application Name=DFU001;" & _
                                     "Default Collection=BSSCRC;Force Translate=0;"
            End If
            Try
                DB.Open(gsConnectionString)
            Catch ex As Exception
                wrtLogHtml(DB.ConnectionString, ex.ToString)
                Exit Sub
            End Try
            Try
                fSql = " SELECT * FROM  BSSFTASK WHERE STS1 = 0 "
                rs.Open(fSql, DB)
                Do While Not rs.EOF
                      Select Case UCase(rs("TTIPO").Value.ToString)
                        Case UCase("Shell")
                            pgm = rs("TPGM").Value
                            param = rs("TPRM").Value
                            exe = pgm & " " & param
                            sts = 1
                            msgT = "Comando Enviado."
                            tLXmsg = "EJECUTADA"
                            Try
                                Shell(exe, AppWinStyle.Hide, False, -1)
                            Catch ex As Exception
                                sts = -1
                                msgT = ex.Message.Replace("'", "''")
                                tLXmsg = "ERROR"
                            End Try
                            fSql = " UPDATE BSSFTASK SET "
                            fSql = fSql & " TLXMSG = '" & tLXmsg & "', "
                            fSql = fSql & " STS1 = " & sts & ", "
                            fSql = fSql & " TUPDDAT = NOW() , "
                            fSql = fSql & " TMSG = '" & msgT & "'"
                            fSql = fSql & " WHERE TNUMREG = " & rs("TNUMREG").Value
                            DB.Execute(fSql)
                        Case UCase("Java")

                    End Select
                    rs.MoveNext()
                Loop
                rs.Close()
            Catch ex As Exception
                wrtLogHtml("tareas", ex.ToString)
                sts = -1
                msgT = ex.Message.Replace("'", "''")
                tLXmsg = "ERROR"
                Try
                    fSql = " UPDATE BSSFTASK SET "
                    fSql = fSql & " TLXMSG = '" & tLXmsg & "', "
                    fSql = fSql & " STS1 = " & sts & ", "
                    fSql = fSql & " TUPDDAT = NOW() , "
                    fSql = fSql & " TMSG = '" & msgT & "'"
                    fSql = fSql & " WHERE TNUMREG = " & rs("TNUMREG").Value
                    DB.Execute(fSql)
                Catch ex2 As Exception

                End Try
            End Try
            DB.Close()
        Next

    End Sub

End Module
