﻿Imports System.IO
Imports System.Text
Imports IBM.Data.DB2.iSeries

Public Class ClsDatosAS400
    Public ReadOnly conexionIBM As New iDB2Connection()
    Private adapterIBM As iDB2DataAdapter = New iDB2DataAdapter()
    Private transIBM As iDB2Transaction
    Private builderIBM As iDB2CommandBuilder
    Private commandIBM As iDB2Command
    Private numFilas As Integer
    Public dt As DataTable
    Private WithEvents ds As DataSet
    Private continuarguardando As Boolean
    Public appPath As String = Path.GetFullPath(My.Application.Info.DirectoryPath & "\Log\")
    Public mensaje As String
    Private data
    Public fSql As String
    Private sApp As String
    Public gsKeyWords As String = ""
    Public Piloto As String = My.Settings.PILOTO

#Region "ACTUALIZAR"

    ''' <summary>
    ''' Actualiza la tabla RFTASK 
    ''' </summary>
    ''' <param name="datos">RFTASK</param>
    ''' <param name="tipo">0- SET TLXMSG = @TLXMSG, TMSG = @TMSG WHERE TNUMREG = @TNUMREG,
    ''' 1- SET TLXMSG = @TLXMSG, TMSG = @TMSG, STS2 = @STS2 WHERE TNUMREG = @TNUMREG,
    ''' 2- SET TLXMSG = @TLXMSG, TMSG = @TMSG, TURL = @TURL, STS2 = @STS2 WHERE TNUMREG = @TNUMREG</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 24/05/2016</remarks>
    Public Function ActualizarRFTASKRow(ByVal datos As RFTASK, ByVal tipo As Integer) As Boolean
        Select Case tipo
            Case 0
                fSql = " UPDATE RFTASK SET "
                fSql &= " TLXMSG = '" & datos.TLXMSG & "',  "
                fSql &= " TMSG = '" & datos.TMSG & "' "
                fSql &= " WHERE TNUMREG = '" & datos.TNUMREG & "'"
            Case 1
                fSql = " UPDATE RFTASK SET "
                fSql &= " TLXMSG = '" & datos.TLXMSG & "',  "
                fSql &= " TMSG = '" & datos.TMSG & "',  "
                fSql &= " STS2 = " & datos.STS2 & " "
                fSql &= " WHERE TNUMREG = '" & datos.TNUMREG & "'"
            Case 2
                fSql = " UPDATE RFTASK SET "
                fSql = fSql & " TLXMSG = '" & datos.TLXMSG & "',  "
                fSql = fSql & " TMSG = '" & datos.TMSG & "',  "
                fSql = fSql & " TURL = '" & datos.TURL & "',  "
                fSql = fSql & " STS2 = " & datos.STS2 & " "
                fSql = fSql & " WHERE TNUMREG = '" & datos.TNUMREG & "' "
        End Select

        commandIBM = New iDB2Command
        commandIBM.Connection = conexionIBM
        Try
            commandIBM.CommandText = fSql
            commandIBM.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As Data.SqlClient.SqlException
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFTASKError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla RFTASK.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Catch ex As Exception
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFTASKError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo actualizar la tabla RFTASK.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
            mensaje = ex.Message
        Finally
            If continuarguardando = False Then
                data = Nothing
                data = New RFLOG
                With data
                    .USUARIO = System.Net.Dns.GetHostName()
                    .OPERACION = "Actualizar RFLOG"
                    .PROGRAMA = My.Application.Info.AssemblyName & "- V" & My.Application.Info.Version.ToString
                    .EVENTO = mensaje
                    .TXTSQL = fSql
                    .ALERT = 1
                End With
                GuardarRFLOGRow(data)
            End If
        End Try
        Return continuarguardando
    End Function

#End Region

#Region "CONSULTAR"

    ''' <summary>
    ''' Consulta la tabla RFPARAM con un determinado parametro de busqueda.Retorna un String
    ''' </summary>
    ''' <param name="prm">parametro de Busqueda</param>
    ''' <param name="Campo"></param>
    ''' <returns>Un String</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 24/05/2016</remarks>
    Public Function ConsultarRFPARAMString(ByVal prm As String, ByVal Campo As String, ByVal cctabl As String) As String
        dt = Nothing

        If Campo = "" Then
            Campo = "CCNOT1"
        End If
        If cctabl = "" Then
            cctabl = "LXLONG"
        End If
        fSql = "SELECT " & Campo & " AS DATO "
        fSql &= " FROM RFPARAM "
        fSql &= " WHERE CCTABL='" & cctabl & "' AND UPPER(CCCODE) = UPPER('" & prm & "')"

        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)

        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        Return dt.Rows(0).Item("DATO")
    End Function

    ''' <summary>
    ''' Consulta la tabla BSSFFNCOM con un determinado parametro de busqueda.Retorna un Recordset
    ''' </summary>
    ''' <param name="parametroBusqueda">parametro de Busqueda</param>
    ''' <param name="tipoparametro">0- Busca por DPER(0) y DTIPCOM(1), 1- Busca por DCOMPRO(0) - DPER(1) y DTIPCOM(0), 2- Busca por DCOMPRO(0) - DPER(1) y DTIPCOM(0)</param>
    ''' <returns>Un Recordset</returns>
    ''' <remarks>Autor: Jesus Santamaria Fecha: 25/05/2016</remarks>
    Public Function ConsultarBSSFFNCOMrs(ByVal parametroBusqueda() As String, ByVal tipoparametro As Integer) As ADODB.Recordset
        dt = Nothing

        Select Case tipoparametro
            Case 0
                fSql = " SELECT "
                fSql &= " DCOMPRO,  DEVENOM, DOUBLE(DEVENUM) DEVENUM "
                fSql &= " FROM BSSFFNCOM "
                fSql &= " WHERE "
                fSql &= " DPER = " & parametroBusqueda(0) & " "
                fSql &= " AND DTIPCOM = '" & parametroBusqueda(1) & "'"
                fSql &= " AND DBOOK = '" & parametroBusqueda(2) & "'"
                fSql &= " AND DESTCOM =3 GROUP BY DCOMPRO,  DEVENOM, DEVENUM"
            Case 1
                fSql = " SELECT "
                fSql &= " DDOCUM  ,"
                fSql &= " DCUENTA ,"
                fSql &= " DDESS02 ,"
                fSql &= " DCODPRD ,"
                fSql &= " DCONCEP ,"
                fSql &= " DTERNOM ,"
                fSql &= " DOUBLE(DTERCOD)DTERCOD ,"
                fSql &= " DTERNIT ,"
                fSql &= " DOUBLE(DDEBITO) DDEBITO,"
                fSql &= " DOUBLE(DCREDITO)DCREDITO "
                fSql &= " FROM BSSFFNCOM "
                fSql &= " WHERE "
                fSql &= " DCOMPRO ='" & parametroBusqueda(0) & "'"
                fSql &= "AND DPER = " & parametroBusqueda(1) & " "
                fSql &= " AND DTIPCOM = '" & parametroBusqueda(2) & "'"
                fSql &= " AND DBOOK = '" & parametroBusqueda(3) & "'"
                fSql &= " AND DESTCOM =3  "
            Case 2
                fSql = " SELECT "
                fSql = fSql & " DOUBLE(SUM(DDEBITO)) DEBITO, "
                fSql = fSql & " DOUBLE(SUM(DCREDITO)) CREDITO"
                fSql = fSql & " FROM BSSFFNCOM "
                fSql = fSql & " WHERE "
                fSql = fSql & " DCOMPRO ='" & parametroBusqueda(0) & "'"
                fSql = fSql & "AND DPER = " & parametroBusqueda(1) & " "
                fSql = fSql & " AND DTIPCOM = '" & parametroBusqueda(2) & "'"
                fSql &= " AND DBOOK = '" & parametroBusqueda(3) & "'"
                fSql = fSql & " AND DESTCOM =3 "
        End Select
        If Piloto = "SI" Then
            fSql &= " FETCH FIRST 8 ROWS ONLY "
        End If
        adapterIBM = New iDB2DataAdapter(fSql, conexionIBM)

        adapterIBM.MissingSchemaAction = MissingSchemaAction.AddWithKey
        builderIBM = New iDB2CommandBuilder(adapterIBM)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapterIBM.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapterIBM = Nothing
        builderIBM = Nothing
        If dt Is Nothing Then
            Return Nothing
        Else
            Return ConvertToRecordset(dt)
        End If
    End Function

#End Region

#Region "FUNCIONES"

    Public Function AbrirConexion() As Boolean
        Dim abierta As Boolean = False
        Try
            If Not Directory.Exists(appPath) Then
                Directory.CreateDirectory(appPath)
            End If
            If conexionIBM.State = ConnectionState.Open Then
                abierta = True
            Else
                If Piloto = "NO" Then
                    conexionIBM.ConnectionString = My.Settings.BASEDATOSAS400
                Else
                    conexionIBM.ConnectionString = My.Settings.BASEDATOSAS400Piloto
                End If
                conexionIBM.Open()
                abierta = True
            End If
        Catch ex1 As iDB2Exception
            abierta = False
            mensaje = ex1.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo establecer Conexion." & vbCrLf & ex1.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Catch ex As Exception
            abierta = False
            mensaje = ex.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo establecer Conexion." & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try
        Return abierta
    End Function

    Public Function CerrarConexion() As Boolean
        Dim cerrada As Boolean = False
        Try
            conexionIBM.Close()
            cerrada = True
        Catch ex1 As iDB2Exception
            cerrada = False
            mensaje = ex1.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo finalizar Conexion." & vbCrLf & ex1.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Catch ex As Exception
            cerrada = False
            mensaje = ex.ToString
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\ConexionError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo finalizar Conexion." & vbCrLf & ex.Message)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        End Try
        Return cerrada
    End Function

    Public Sub wrtLogHtml(sql As String, ByVal Descrip As String)
        Dim fEntrada As String = ""
        Dim linea As String = Format(Now(), "yyyy-MM-dd HH:mm:ss") & "|"
        Dim strStreamW As Stream = Nothing
        Dim sApp As String = My.Application.Info.ProductName & "." & _
                                   My.Application.Info.Version.Major & "." & _
                                   My.Application.Info.Version.Minor & "." & _
                                   My.Application.Info.Version.Revision

        Descrip = Descrip.Replace(vbCrLf, vbCr)
        Descrip = Descrip.Replace(vbLf, vbCr)
        Descrip = Descrip.Replace(vbCr, "<BR/>")

        If gsKeyWords.Trim <> "" Then
            Descrip = gsKeyWords & "<br>" & Descrip
        End If

        linea &= System.Net.Dns.GetHostName() & "|" & sApp & "|" & Descrip.Replace("|", "?") & "|" & sql.Replace("|", "?")

        fEntrada = Replace(appPath & "\" & Format(Now(), "yyyyMM") & ".csv", "\\", "\")
        If Not Directory.Exists(appPath) Then
            Directory.CreateDirectory(appPath)
        End If
        If File.Exists(fEntrada) Then
            strStreamW = File.Open(fEntrada, FileMode.Open) 'Abrimos el archivo
        Else
            strStreamW = File.Create(fEntrada) ' lo creamos
        End If
        strStreamW.Close()
        Try
            ' Create an instance of StreamReader to read from a file.
            Dim sr As StreamReader = New StreamReader(fEntrada)
            Dim sb As New StringBuilder()
            sb.Append(sr.ReadToEnd())
            sb.Append(linea)
            sr.Close()

            Dim sw As StreamWriter = New StreamWriter(fEntrada)
            sw.WriteLine(sb.ToString())
            sw.Close()

        Catch E As Exception

        End Try
        'Application.Exit()
    End Sub

    Public Shared Function ConvertToRecordset(ByVal inTable As DataTable) As ADODB.Recordset
        '=============================================
        '
        'This is a VB conversion of the code found here:
        'http://www.codeproject.com/cs/database/DataTableToRecordset.asp 
        'Please see the original link for a C# version and to read the 
        'original article.
        '
        '=============================================
        Dim result As ADODB.Recordset = New ADODB.Recordset()
        result.CursorLocation = ADODB.CursorLocationEnum.adUseClient
        Dim resultFields As ADODB.Fields = result.Fields
        Dim inColumns As System.Data.DataColumnCollection = inTable.Columns
        For Each inColumn As DataColumn In inColumns
            resultFields.Append(inColumn.ColumnName, TranslateType(inColumn.DataType), inColumn.MaxLength, ADODB.FieldAttributeEnum.adFldIsNullable, Nothing)
        Next
        result.Open(System.Reflection.Missing.Value, System.Reflection.Missing.Value, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic)
        For Each dr As DataRow In inTable.Rows
            result.AddNew(System.Reflection.Missing.Value, System.Reflection.Missing.Value)
            For columnIndex As Integer = 0 To inColumns.Count - 1
                resultFields(columnIndex).Value = dr(columnIndex)
            Next
        Next
        Return result
    End Function

    Shared Function TranslateType(ByVal columnType As Type) As ADODB.DataTypeEnum
        Select Case columnType.UnderlyingSystemType.ToString()
            '=============================================
            '
            'This is a VB conversion of the code found here:
            'http://www.codeproject.com/cs/database/DataTableToRecordset.asp 
            'Please see the original link for a C# version and to read the 
            'original article.
            '
            '=============================================
            Case "System.Boolean"
                Return ADODB.DataTypeEnum.adBoolean
            Case "System.Byte"
                Return ADODB.DataTypeEnum.adUnsignedTinyInt
            Case "System.Char"
                Return ADODB.DataTypeEnum.adChar
            Case "System.DateTime"
                Return ADODB.DataTypeEnum.adDate
            Case "System.Decimal"
                Return ADODB.DataTypeEnum.adCurrency
            Case "System.Double"
                Return ADODB.DataTypeEnum.adDouble
            Case "System.Int16"
                Return ADODB.DataTypeEnum.adSmallInt
            Case "System.Int32"
                Return ADODB.DataTypeEnum.adInteger
            Case "System.Int64"
                Return ADODB.DataTypeEnum.adBigInt
            Case "System.SByte"
                Return ADODB.DataTypeEnum.adTinyInt
            Case "System.Single"
                Return ADODB.DataTypeEnum.adSingle
            Case "System.UInt16"
                Return ADODB.DataTypeEnum.adUnsignedSmallInt
            Case "System.UInt32"
                Return ADODB.DataTypeEnum.adUnsignedInt
            Case "System.UInt64"
                Return ADODB.DataTypeEnum.adUnsignedBigInt
        End Select
        'Note Strings are not cased and will return here:
        Return ADODB.DataTypeEnum.adVarChar
    End Function

    Public Function wrtLog(ByVal txt As String, ByRef iNumLog As Integer) As String
        Dim fEntrada As String = ""
        Dim num As String = Date.Today.Month & Date.Today.Day & Date.Today.Year & Date.Today.Hour & Date.Today.Minute & Date.Today.Second

        Dim strStreamW As Stream = Nothing
        'Dim appPath As String = System.Web.HttpContext.Current.Request.ApplicationPath
        'Dim CarpetaWeb As String = HttpContext.Current.Request.MapPath(appPath)
        fEntrada = Replace(appPath & "\" & num & ".txt", "\\", "\")
        If Not Directory.Exists(appPath) Then
            Directory.CreateDirectory(appPath)
        End If
        If File.Exists(fEntrada) Then
            strStreamW = File.Open(fEntrada, FileMode.Open) 'Abrimos el archivo
        Else
            strStreamW = File.Create(fEntrada) ' lo creamos
        End If
        strStreamW.Close()
        Try
            ' Create an instance of StreamReader to read from a file.
            Dim sr As StreamReader = New StreamReader(fEntrada)
            Dim sb As New StringBuilder()

            sb.Append(sr.ReadToEnd())

            sb.AppendLine("SistemaDeBodega (" & My.Application.Info.Version.Major & "." & _
                   My.Application.Info.Version.Minor & "." & _
                   My.Application.Info.Version.Revision & ")")
            '  sb.AppendLine("[" & My.Settings.SERVER & "][" & My.Settings.USER & "][" & My.Settings.LIBRARY & "]")

            sb.AppendLine(txt)
            sr.Close()

            Dim sw As StreamWriter = New StreamWriter(fEntrada)
            sw.WriteLine(sb.ToString())
            sw.Close()

            Return "OK"
        Catch E As Exception
            Return E.Message
        End Try

    End Function

    Public Sub WrtTxtError(ByVal Archivo As String, ByVal Texto As String)
        wrtLogHtml("", "Archivo:  " & Archivo & " Texto: " & Texto)
    End Sub

#End Region

#Region "GUARDAR"

    ''' <summary>
    ''' Guarda RFLOG
    ''' </summary>
    ''' <param name="datos">RFLOG</param>
    ''' <returns>true si se actualiza con exito</returns>
    ''' <remarks> Autor: Jesús Santamaria Fecha: 24/05/2016</remarks>
    Public Function GuardarRFLOGRow(ByVal datos As RFLOG) As Boolean

        fSql = "INSERT INTO RFLOG (USUARIO, OPERACION, PROGRAMA, EVENTO, TXTSQL, ALERT)"
        fSql &= " VALUES ('" & datos.USUARIO & "', '" & datos.OPERACION & "', '" & datos.PROGRAMA & "', '" & datos.EVENTO.Replace("'", "''") & "', '" & datos.TXTSQL.Replace("'", "''") & "', " & datos.ALERT & ")"

        commandIBM = New iDB2Command
        commandIBM.Connection = conexionIBM
        Try
            commandIBM.CommandText = fSql
            commandIBM.ExecuteNonQuery()
            continuarguardando = True
        Catch ex As Data.SqlClient.SqlException
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo guardar en la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Catch ex As Exception
            continuarguardando = False
            Dim sb As New StringBuilder()
            Dim sw As StreamWriter = New StreamWriter(appPath & "\RFLOGError" & Now.ToString("ddMMyyyyHHmmss") & ".txt")
            sb.AppendLine("No se pudo guardar en la tabla RFLOG.  Comuniquese con soporte tecnico  y reporte el siguiente mensaje :  " & vbCrLf & ex.Message & vbCrLf & fSql)
            sb.AppendLine(Now().ToString)
            sw.WriteLine(sb.ToString())
            sw.Close()
        Finally
        End Try
        Return continuarguardando
    End Function

#End Region

End Class

Public Class RFLOG
    Public OPERACION As String
    Public PROGRAMA As String
    Public EVENTO As String
    Public TXTSQL As String
    Public ALERT As String
    Public ID As String
    Public USUARIO As String
End Class

Public Class RFTASK
    Public TNUMREG As String
    Public TLXMSG As String
    Public TMSG As String
    Public STS2 As String
    Public TURL As String
End Class
