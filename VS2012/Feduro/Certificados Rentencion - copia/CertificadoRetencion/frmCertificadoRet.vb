﻿Imports PDF_In_The_BoxCtl
Imports AxPDF_In_The_BoxCtl
Imports ADODB

Public Class frmCertificadoRet
    Dim bPaginaNueva As Boolean
    Dim rsTit As New ADODB.Recordset
    Dim rsCab As New ADODB.Recordset
    Dim rsPed As New ADODB.Recordset
    Dim rsDet As New ADODB.Recordset
    Dim rsNotas As New ADODB.Recordset
    Dim DocScanner As Integer, Observ As String, ImprimeIAL As String
    Dim bColor As Boolean
    Dim bQuimicos As Boolean
    Dim bExport As Boolean
    Dim Flag3
    Dim Sellos() As String
    Dim Fila As Integer
    Dim TotNotas As Integer
    Dim TotFilas As Integer
    Dim Ta As TBoxTable

    Const CELDA_NORMAL As String = "BrushColor = 255, 255, 255"
    'Const CELDA_SOMBRA As String = "BrushColor = 215, 215, 215"
    Const CELDA_SOMBRA As String = "BrushColor = 255, 255, 255"
    Const TABLE_HEADER_STYLE As String = "ChildrenStyle=""BorderStyle=rect;FontSize=9;Fontname=Arial;Alignment=Center;VertAlignment=Center"""
    Const ESTILO_TIT As String = "Normal;fontsize=6;Fontname=Corbel;BorderColor=Black;Alignment=Center;VertAlignment=Center"


    Private Sub frmCertificadoRet_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim parametro As String
        Dim aSubParam() As String
        Dim qPrinter As String = ""
        Dim rs As New ADODB.Recordset
        Dim bGenerar As Boolean
        Dim campoParam As String = ""
        Flag3 = 1
        Environment.GetCommandLineArgs()
        For Each parametro In Environment.GetCommandLineArgs()
            aSubParam = Split(parametro, "=")
            Select Case UCase(aSubParam(0))
                Case "C"
                    NumeroDocumento = Ceros(Trim(aSubParam(1)), 8)
                Case "A"
                    PeriodoAAAA = aSubParam(1)
                Case "M"
                    PeriodoMM = Ceros(Trim(aSubParam(1)), 2)
                Case "TAREA"
                    TASK_No = aSubParam(1)
            End Select
        Next


        DB.bDateTimeToChar = True

        If Not AbreConexion() Then
            Application.Exit()
        End If

        If My.Settings.LOCAL.ToUpper() = "SI" Then
            campoParam = "CCNOT2"
        End If
        CARPETA_SITIO = DB.getLXParam("SITIO_CARPETA", "TM", campoParam)
        CARPETA_IMG = DB.getLXParam("CARPETA_IMG", "TM", campoParam)
        APP_PATH = My.Application.Info.DirectoryPath & "\logo_FEDURO.jpg" '  DB.getLXParam("LOGOEMPRESA", "TM", campoParam)
        bPRUEBAS = My.Settings.PRUEBAS.ToUpper() = "SI"

        PDF_Folder = CARPETA_SITIO & "xm\pdf\CertifRet\" & PeriodoAAAA & PeriodoMM
        LOG_File = PDF_Folder & "\" & NumeroDocumento & ".txt"
        WrtTxtError(LOG_File, NumeroDocumento)
        Dim arguments As String() = Environment.GetCommandLineArgs()
        WrtTxtError(LOG_File, String.Join(", ", arguments))

        If Not DB.CheckVer() Then
            DB.WrtSqlError(DB.lastError, "")
            End
        End If

        If My.Settings.PRUEBAS = "SI" Then
            GeneraPdf(bGenerar)
        Else
            Try
                If GeneraPdf(bGenerar) Then
                    fSql = " UPDATE RFTASK SET STS2 = 1  "
                    fSql = fSql & " WHERE TNUMREG = " & TASK_No
                    DB.ExecuteSQL(fSql)
                End If
            Catch ex As Exception
                Debug.Print(ex.Message)
                WrtTxtError(LOG_File, ex.Message)
                DB.WrtSqlError(ex.Message, "GeneraPDF")
                End
            End Try
        End If

        DB.Close()
        Application.Exit()
    End Sub

    Public Function GeneraPdf(bGenerar As Boolean) As Boolean
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim ArchivoPDF As String
        Dim Resultado As Boolean = False

        Fila = 0

        fSql = " SELECT CYEAR, CPERIO, CSECUEN, TRIM(VARCHAR(CTERNIT)) CTERNIT, VNDNAM, VENDOR, VMID, VMDATN, CUSR1, VMUF02  "
        fSql &= " FROM BSSFFNCER INNER JOIN BSSAVM ON "
        fSql &= " BSSFFNCER.CPROCOD = BSSAVM.VENDOR "
        fSql &= String.Format(" WHERE CYEAR = {0} ", PeriodoAAAA)
        fSql &= String.Format(" AND CPERIO = {0} ", PeriodoMM)
        fSql &= String.Format(" AND CSECUEN = {0} ", NumeroDocumento)
        fSql &= " AND CUSR1 = '0' "
        fSql &= " GROUP BY CYEAR, CPERIO, CSECUEN, CTERNIT, VNDNAM, VENDOR, VMID, VMDATN, CUSR1, VMUF02  "
        If Not DB.OpenRS(rsCab, fSql, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly) Then
            rsCab.Close()
            WrtTxtError(LOG_File, "Certificado no se encontro.")
            Return False
        End If

        '*****************************************************************
        '   DETALLE
        '*****************************************************************
        fSql = " SELECT '' VACIO1, '' VACIO2, VARCHAR(CCUEIVA) CCUEIVA, VARCHAR(CTASIVA) CTASIVA, DOUBLE(CPORIVA) CPORIVA, VARCHAR(CCUERTI) CCUERTI, VARCHAR(CTASRTI) CTASRTI,  "
        fSql &= " DOUBLE(Abs(CPORRTI)) AS CPORRTI, DOUBLE(Sum(CVALBAS)) AS BASE,  "
        fSql &= " DOUBLE(Sum(CVALIVA)) AS IVA, DOUBLE(Abs(Sum(CVALRTI))) AS RTI  "
        fSql &= " FROM BSSFFNCER "
        fSql &= String.Format(" WHERE CYEAR = {0} ", PeriodoAAAA)
        fSql &= String.Format(" AND CPERIO = {0} ", PeriodoMM)
        fSql &= String.Format(" AND CSECUEN = {0} ", NumeroDocumento)
        fSql &= " AND CUSR1 = '0' "
        fSql &= " GROUP BY CCUEIVA, CTASIVA, CPORIVA, CCUERTI, CTASRTI, CPORRTI  "

        If Not DB.OpenRS(rsDet, fSql, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly) Then
            rsTit.Close()
            rsDet.Close()
            rsCab.Close()
            WrtTxtError(LOG_File, "Certificado sin detalle 1.")
            Return False
        End If
        '*****************************************************************
        '   FIN DETALLE
        '*****************************************************************

        fSql = " SELECT '' VACIO1, '' VACIO2, RPNDEC, RPFDEC, RPNSTI, RPFSTI"
        fSql &= " FROM RMMTPI"
        fSql &= String.Format(" WHERE RPAFS = {0} ", PeriodoAAAA)
        fSql &= String.Format(" AND RPBIM = {0} ", PeriodoMM)
        If Not DB.OpenRS(rsNotas, fSql, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly) Then
            rsTit.Close()
            rsDet.Close()
            rsCab.Close()
            WrtTxtError(LOG_File, "Sin declaracion.")
            Return False
        End If

        ArchivoPDF = PDF_Folder & "\" & NumeroDocumento & ".pdf"
        Resultado = ArmaPDF(ArchivoPDF)

        rsDet.Close()
        rsNotas.Close()
        rsCab.Close()
        Return Resultado

    End Function

    Function ArmaPDF(ArchivoPDF As String) As Boolean
        Dim Ta As TBoxTable
        Dim Ta1 As TBoxTable
        Dim Detail As TBoxBand
        Dim Note As TBoxBand
        Dim Header As TBoxBand
        Dim Footer As TBoxBand
        Dim AfterFooter As TBoxBand
        Dim b2 As TBoxBand
        Dim sError As String
        ArmaPDF = False

        If Dir(ArchivoPDF) <> "" Then
            On Error Resume Next
            Kill(ArchivoPDF)
            If Err.Number <> 0 Then
                sError = Err.Description
                WrtTxtError(LOG_File, "ArmaPDF: Borrar " & ArchivoPDF & " " & sError)
                rsDet.Close()
                rsNotas.Close()
                rsTit.Close()
                rsCab.Close()
                rsDet = Nothing
                rsNotas = Nothing
                rsTit = Nothing
                rsCab = Nothing
                DB.Close()
                End
            End If
            On Error GoTo 0
        End If

        ' Titulos
        With Me.AxPdfBox1
            .FileName = ArchivoPDF
            .Title = "Certificado de Retencion  - " & NumeroDocumento
            .WantShow = bPRUEBAS
            .WantPageCount = True
            .PaperSizeName = "Letter"
            .BeginDoc()
            Fila = 0
            Flag = 0
            Ta = .CreateTable("BorderStyle=None;Margins=130,200,100,150")
            Ta.Assign(rsDet)

            bPaginaNueva = True
            'Tabla del Detalle
            Detail = Ta.CreateBand()
            Detail.Role = TBandRole.rlDetail
            Detail.ChildrenStyle = "FontSize=8;Fontname=Arial; VertAlignment=Center;BorderStyle=rect;Alignment=Right;BorderRightMargin=10"
            Detail.CreateCell(285, "BorderStyle=none").CreateText().Bind("VACIO1")
            Detail.CreateCell(200).CreateText().Bind("CTASIVA")
            With Detail.CreateCell(300).CreateText
                .Bind("BASE")
                .Format = "#,##0.00"
            End With
            With Detail.CreateCell(200).CreateText
                .Bind("CPORIVA")
                .Format = "#,##0.00"
            End With
            With Detail.CreateCell(200).CreateText
                .Bind("IVA")
                .Format = "#,##0.00"
            End With
            With Detail.CreateCell(200).CreateText
                .Bind("CPORRTI")
                .Format = "#,##0.00"
            End With

            With Detail.CreateCell(240).CreateText
                .Bind("RTI")
                .Format = "#,##0.00"
            End With
            Detail.CreateCell(285, "BorderStyle=none").CreateText().Bind("VACIO2")

            Detail.Breakable = True
            Header = Ta.CreateBand() 'Detail.CreateClone(TABLE_HEADER_STYLE)
            Header.Role = TBandRole.rlBodyHeader
            Header.ChildrenStyle = "fontsize=8;  Alignment=Right; VertAlignment=Center; BorderStyle=rect; BorderRightMargin=10"
            Header.CreateCell(285, "BorderStyle=none").CreateText().Assign(" ")
            Header.CreateCell(200).CreateText().Assign("CONCEPTO")
            Header.CreateCell(300).CreateText().Assign("BASE DE OPERACION")
            Header.CreateCell(200).CreateText().Assign("% ITBMS")
            Header.CreateCell(200).CreateText().Assign("VALOR ITBMS")
            Header.CreateCell(200).CreateText().Assign("% RET")
            Header.CreateCell(240).CreateText().Assign("VLR RETENCIÓN")
            Header.CreateCell(285, "BorderStyle=none").CreateText().Assign(" ")

            Ta.Put()

            Flag = 1
            b2 = .CreateBand("BorderStyle=none;Margins=130,200,100,150")
            b2.Height = 50
            b2.CreateCell(1910).CreateText().Assign("")
            b2.Put()

            Flag3 = 1
            Flag = 0
            Ta1 = .CreateTable("BorderStyle=None;Margins=130,200,100,150")
            'Set Ta = .CreateTable("BorderStyle=none;Alignment=Center;Margins=113,200,100,150")
            Ta1.Assign(rsNotas)

            bPaginaNueva = True
            'Tabla del Detalle
            Detail = Ta1.CreateBand()
            Detail.Role = TBandRole.rlDetail
            'Detail.ChildrenStyle = "BorderStyle=box;FontSize=8;Fontname=Arial;VertAlignment=Center"
            'Detail.Height = 45
            Detail.ChildrenStyle = "FontSize=8;Fontname=Arial;Alignment=Center;VertAlignment=Center;BorderStyle=Rect"

            Detail.CreateCell(505, "BorderStyle=none").CreateText().Bind("VACIO1")
            Detail.CreateCell(200).CreateText("BorderLeftMargin = 10;BorderLeftMargin = 10").Bind("RPNDEC")
            Detail.CreateCell(300).CreateText("BorderLeftMargin = 10;BorderLeftMargin = 10").Bind("RPFDEC")
            Detail.CreateCell(200).CreateText("BorderLeftMargin = 10;BorderLeftMargin = 10").Bind("RPNSTI")
            Detail.CreateCell(200).CreateText("BorderLeftMargin = 10;BorderLeftMargin = 10").Bind("RPFSTI")
            Detail.CreateCell(505, "BorderStyle=none").CreateText().Bind("VACIO2")

            Detail.Breakable = True
            Header = Ta1.CreateBand() 'Detail.CreateClone(TABLE_HEADER_STYLE)
            Header.Role = TBandRole.rlBodyHeader
            Header.ChildrenStyle = "fontsize=8;  Alignment=Center; VertAlignment=Center; BorderStyle=rect"
            Header.CreateCell(505, "BorderStyle=none").CreateText().Assign("")
            Header.CreateCell(200).CreateText().Assign("Declaracion")
            Header.CreateCell(300).CreateText().Assign("Fecha")
            Header.CreateCell(200).CreateText().Assign("Codigo de Pago")
            Header.CreateCell(200).CreateText().Assign("Fecha")
            Header.CreateCell(505, "BorderStyle=none").CreateText().Assign("")

            Ta1.Put()
            .EndDoc()
            Debug.Print(.FileName())
            If bPRUEBAS Then
                .ShowDoc()
            End If

            ArchivoPDF = .FileName

        End With
        Return True

    End Function

    Sub Renglon(txt As String)
        Dim b As TBoxBand
        b = AxPdfBox1.CreateBand("BorderStyle=None;Margins=130,200,100,180")
        b.CreateCell(1910).CreateText("FontSize=8;Fontname=Arial;BorderStyle=None;Alignment=Center;VertAlignment=Center").Assign(txt.ToString)
        b.Put()

    End Sub
    Private Sub AxPdfBox1_OnBottomOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnBottomOfPageEvent) Handles AxPdfBox1.OnBottomOfPage
        Dim b As TBoxBand
        Dim rs As New ADODB.Recordset

        With AxPdfBox1
            b = .CreateBand("BorderStyle=None;Margins=130,200,100,150")
            b.Breakable = False
            b.Height = 150
            b.CreateCell(1910, "BorderStyle=None").CreateText("Alignment=left").Assign("")
            b.Put()

            Dim txt As String = "LAS RETENCIONES FUERON PRACTICADAS EN LA CIUDAD DE PANAMA CONSIGNADAS OPORTUNAMENTE"
            Renglon("LAS RETENCIONES FUERON PRACTICADAS EN LA CIUDAD DE PANAMA CONSIGNADAS OPORTUNAMENTE")
            Renglon("A LA ORDEN DE LA ADMINISTRACIÓN DE IMPUESTOS NACIONALES ""DGI"", AGENTE RETENEDOR")
            Renglon("SIN FIRMA AUTÓGRAFA - Resolucion No.201-17679 del 19 de Octubre de 2015")
            Renglon("SE EXPIDE LA PRESENTE CERTIFICACIÓN A: " & ExpedicionCertif())
            Renglon("ESTE CERTIFICADO REEMPLAZA TODOS EMITIDOS ANTERIORMENTE PARA EL MISMO PERIODO")

        End With

    End Sub

    Function PeriodoCertif() As String
        Dim rs As New ADODB.Recordset
        Dim result As String = ""
        fSql = " SELECT CCNOT2 FROM RFPARAM WHERE CCTABL = 'MESES' AND CCCODEN = " & PeriodoMM
        If DB.OpenRS(rs, fSql) Then
            result = rs("CCNOT2").Value & " DE: " & PeriodoAAAA
        End If
        rs.Close()
        rs = Nothing
        Return result
    End Function

    Function ExpedicionCertif() As String
        Dim rs As New ADODB.Recordset
        Dim result As String = ""
        fSql = " SELECT CCNOT2 FROM RFPARAM WHERE CCTABL = 'MESES' AND CCCODEN = " & Month(Now())
        If DB.OpenRS(rs, fSql) Then
            result = rs("CCNOT2").Value & ", " & String.Format("{0:dd yyyy}", Now())
        End If
        rs.Close()
        rs = Nothing
        Return result
    End Function


    Private Sub AxPdfBox1_OnTopOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnTopOfPageEvent) Handles AxPdfBox1.OnTopOfPage
        Dim b As TBoxBand
        With Me.AxPdfBox1
            .DefineStyle("Normal", "0; Margins=130,200,100,150; fontsize=10;Fontname=Corbel;BorderColor=Black")
            .Style = "Normal"
            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "BorderStyle=None;Alignment=Center"

            b.CreateCell(380).CreateImage("Alignment=Right;VertAlignment=Top").Assign(My.Application.Info.DirectoryPath & "\logo_FEDURO.jpg")
            b.CreateCell(530).CreateText().Assign("")
            b.CreateCell(1000).CreateText().Assign(My.Application.Info.DirectoryPath & "\Cabecera.rtf")
            b.Put()

            b = .CreateBand("Margins=130,200,100,150")
            b.Height = 35
            b.CreateCell(1910, "BorderStyle=None").CreateText().Assign("")
            b.Put()

            b = .CreateBand("Margins=130,200,100,150")
            b.Height = 35
            b.CreateCell(1910, "BorderStyle=None").CreateText().Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Left;VertAlignment=Center"
            b.CreateCell(1030, "BorderStyle=none").CreateText("FontName=Arial; FontSize=12").Assign("Certificado No. " & NumeroDocumento)
            b.CreateCell(880).CreateText("Alignment=Right;FontSize=9").Assign("")
            b.Put()

            b = .CreateBand("Margins=130,200,100,150")
            b.Height = 50
            b.CreateCell(1910, "BorderStyle=None").CreateText().Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(1910, "BorderStyle=none").CreateText("FontName=Arial; FontSize=9").Assign("CERTIFICADO MENSUAL DE RETENCION I.T.B.M.S RETENIDO Y PAGADO")
            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(1910, "BorderStyle=none").CreateText("FontName=Arial; FontSize=9").Assign("CERTIFICAMOS")
            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(1910, "BorderStyle=none").CreateText("FontName=Arial; FontSize=9").Assign("QUE DURANTE EL PERIODO: " & PeriodoCertif() & " PRACTICAMOS RETENCION ITBMS A:")
            b.Put()

            Dim s As String = rsCab("VNDNAM").Value
            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.Height = 100
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(1910, "BorderStyle=none").CreateText("FontName=Arial; FontSize=12").Assign(s.Trim())
            b.Put()

            Dim ruc As String = "CON RUC No.: " & CStr(Trim(rsCab("CTERNIT").Value)) & " DV " & CStr(Trim(rsCab("VMUF02").Value)) & " POR LOS SIGUIENTES CONCEPTOS Y VALORES:"
            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Center;VertAlignment=Center"
            b.CreateCell(1910, "BorderStyle=none").CreateText("FontName=Arial; FontSize=9").Assign(ruc.Trim())
            b.Put()

            b = .CreateBand("Margins=130,200,100,150")
            b.Height = 100
            b.CreateCell(1910, "BorderStyle=None").CreateText().Assign("")
            b.Put()

        End With

    End Sub


    Private Sub AxPdfBox1_BeforePutBand4(sender As System.Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_BeforePutBandEvent) Handles AxPdfBox1.BeforePutBand
        If e.aBand.Role = 1 Then ' TBandRole.rlDetail  rlDetail = 1
            If Flag3 = -1 Then
                e.aBand.BrushColor = RGB(215, 215, 215)
            Else
                e.aBand.BrushColor = RGB(255, 255, 255)
            End If
            Flag3 = Flag3 * -1
            bColor = Not bColor
        End If


        Fila = Fila + 1
        If Fila > 38 Then
            AxPdfBox1.NewPage()
            Fila = 0
        End If
    End Sub


End Class

