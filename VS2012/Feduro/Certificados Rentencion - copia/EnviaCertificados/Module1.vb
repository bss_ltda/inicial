﻿Module Module1
    Public DB As New ADODB.Connection
    Public PeriodoAAAA As String
    Public PeriodoMM As String
    Public TASK_No As String
    Public NumeroDocumento As String = ""

    Sub Main()
        Dim parametro As String
        Dim aSubParam() As String

        Environment.GetCommandLineArgs()
        For Each parametro In Environment.GetCommandLineArgs()
            aSubParam = Split(parametro, "=")
            Select Case UCase(aSubParam(0))
                Case "A"
                    PeriodoAAAA = aSubParam(1)
                Case "M"
                    PeriodoMM = aSubParam(1)
                Case "TAREA"
                    TASK_No = aSubParam(1)
                Case "C"
                    NumeroDocumento = Trim(aSubParam(1))
            End Select
        Next

        GeneraCertificados()

    End Sub

    Sub GeneraCertificados()
        Dim rs As New ADODB.Recordset
        Dim fSql As String
        Dim sCmd As String
        Dim exeCert As String = "C:\Feduro\CertifRete\CertificadosRetencion.exe"
        Dim msgErr As String

        DB.Open("Provider=IBMDA400.DataSource;Data Source=192.168.2.220;User ID=DEFAULT;Password=ABCD1234;Persist Security Info=True;Convert Date Time To Char=TRUE;Default Collection=BSSPTY;Force Translate=0;")

        fSql = " SELECT CYEAR, CPERIO, CSECUEN, VNDNAM, VMDATN, CCNOT2 "
        fSql = fSql & " FROM BSSFFNCER INNER JOIN BSSAVM ON "
        fSql = fSql & " BSSFFNCER.CPROCOD = BSSAVM.VENDOR "
        fSql = fSql & " INNER JOIN RFPARAM ON CCTABL = 'MESES' AND CCCODEN = CPERIO "
        fSql = fSql & " WHERE BSSFFNCER.CYEAR = " & PeriodoAAAA
        fSql = fSql & " AND BSSFFNCER.CPERIO = " & PeriodoMM
        fSql = fSql & " AND CUSR1 = '0' "
        If NumeroDocumento <> "" Then
            fSql = fSql & " AND CSECUEN = " & NumeroDocumento
        End If
        fSql = fSql & " AND CSECUEN >= 63"
        fSql = fSql & " GROUP BY CYEAR, CPERIO, CSECUEN, VNDNAM, VMDATN, CCNOT2 "
        fSql = fSql & " ORDER BY CSECUEN"
        rs.Open(fSql, DB)
        Do While Not rs.EOF
            'EnviarCorreo(String.Format("{0} {1}", rs("CCNOT2").Value, PeriodoAAAA), "7", rs("VNDNAM").Value, "fredy.manrique@bssltda.com", "")
            sCmd = String.Format("{0} P=01195872 C={1} A={2} M={3} TAREA=203737", exeCert, rs("CSECUEN").Value, PeriodoAAAA, PeriodoMM)
            Shell(sCmd, AppWinStyle.NormalNoFocus, True)
            If InStr(rs("VMDATN").Value, "@") = 0 Or InStr(rs("VMDATN").Value, ".") = 0 Then
                wrtLog("Error en direccion de correo", rs("CSECUEN").Value, "")
            End If
            msgErr = EnviarCorreo(String.Format("{0} {1}", rs("CCNOT2").Value, PeriodoAAAA), rs("CSECUEN").Value, rs("VNDNAM").Value, rs("VMDATN").Value, "")
            If msgErr <> "" Then
                wrtLog(msgErr, rs("CSECUEN").Value, "")
            Else
                wrtLog(String.Format("Certificado {0:00000000}.pdf enviado a {1} de {2}", rs("CSECUEN").Value, rs("VMDATN").Value, rs("VNDNAM").Value), rs("CSECUEN").Value, "")
            End If
            'EnviarCorreo(String.Format("{0} {1}", rs("CCNOT2").Value, PeriodoAAAA), rs("CSECUEN").Value, rs("VNDNAM").Value, "fredy.manrique@bssltda.com", "")
            rs.MoveNext()
        Loop
        rs.Close()
        DB.Close()

    End Sub
    Function EnviarCorreo(per As String, No As String, Nombre As String, cuentaCorreo As String, PDFCertificado As String) As String
        Dim Correo As New class_EnviarCorreo
        With Correo
            .Asunto = "CERTIFICADO MENSUAL DE RETENCION"
            .url = String.Format("http://192.168.2.22/Common/mht/certif_rete.asp?per={0}&No={1}", per, No)
            If .InicializaMHT() Then
                .Destinatario(Nombre, cuentaCorreo)
                .Archivo(String.Format("C:\Feduro\xm\pdf\CertifRet\{0}{1:00}\{2:00000000}.pdf", CInt(PeriodoAAAA), CInt(PeriodoMM), CLng(No)))
                If .Envia() Then
                    Return ""
                Else
                    Return .ErrorTXT()
                End If
            Else
                Return "Error " & .ErrorTXT()
            End If
        End With

    End Function

    Sub wrtLog(Descrip As String, gsKeyWords As String, Sql As String)
        Dim fSql, vSql As String
        Console.WriteLine(Descrip)

        fSql = " INSERT INTO RFLOG( "
        vSql = " VALUES ( "
        fSql = fSql & " USUARIO   , " : vSql = vSql & "'" & System.Net.Dns.GetHostName() & "', "      '//502A
        fSql = fSql & " PROGRAMA  , " : vSql = vSql & "'CERTIFRETE', "      '//502A
        fSql = fSql & " ALERT     , " : vSql = vSql & "1, "      '//1P0
        fSql = fSql & " EVENTO    , " : vSql = vSql & "'" & Replace(Descrip, "'", "''") & "', "      '//20002A
        fSql = fSql & " LKEY      , " : vSql = vSql & "'" & gsKeyWords & "', "     '//30A   Clave         
        fSql = fSql & " TXTSQL    ) " : vSql = vSql & "'" & Replace(Sql, "'", "''") & "' ) "      '//5002A
        DB.Execute(fSql & vSql)

    End Sub

End Module
