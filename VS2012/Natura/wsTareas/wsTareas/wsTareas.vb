﻿Imports System.IO

Public Class wsTareas

    Private WithEvents tim As New Timers.Timer(1000)

    Private DB As ADODB.Connection

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Agregue el código aquí para iniciar el servicio. Este método debería poner
        ' en movimiento los elementos para que el servicio pueda funcionar.
        tim.Enabled = True
    End Sub

    Protected Overrides Sub OnStop()
        ' Agregue el código aquí para realizar cualquier anulación necesaria para detener el servicio.
    End Sub

    Private Sub tim_Elapsed(sender As Object, e As System.Timers.ElapsedEventArgs) Handles tim.Elapsed

        revisaTareas()

    End Sub

    Sub revisaTareas()
        Dim fSql As String
        Dim pgm, param, exe As String
        Dim rs As New ADODB.Recordset
        Dim sts As Integer
        Dim msgT As String
        Dim tLXmsg As String
        Dim Accion As String

        Dim DB As New ADODB.Connection
        DB.Open("Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=APLLX;Password=LXAPL;Persist Security Info=True;Convert Date Time To Char=TRUE;Application Name=DFU001;Default Collection=ERPLX834F;Force Translate=0;")
        'DB.Open("Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=DESLXSSA;Password=DIC2013;Persist Security Info=True;Convert Date Time To Char=TRUE;Application Name=DFU001;Default Collection=DESLX834F;Force Translate=0;")
        Try
            fSql = " SELECT * FROM  RFTASK WHERE STS1 = 0 AND THLD = 0 AND TFPROXIMA <= NOW() "
            rs.Open(fSql, DB)
            Do While Not rs.EOF
                Accion = UCase(rs("TTIPO").Value)
                If rs("FWRKID").Value <> 0 Then
                    If Accion <> "ULTIMO" Then
                        Accion = "PAQUETE"
                    End If
                Else
                    Accion = UCase(rs("TTIPO").Value)
                End If

                Select Case Accion
                    Case UCase("Shell")
                        pgm = rs("TPGM").Value
                        param = rs("TPRM").Value
                        exe = pgm & " " & param.Replace("|", " ")
                        sts = 1
                        msgT = "Comando Enviado."
                        tLXmsg = "EJECUTADA"
                        Try
                            Shell(exe, AppWinStyle.Hide, False, -1)
                        Catch ex As Exception
                            sts = -1
                            msgT = ex.Message.Replace("'", "''")
                            tLXmsg = "ERROR"
                        End Try
                        fSql = " UPDATE RFTASK SET "
                        fSql = fSql & " TLXMSG = '" & tLXmsg & "', "
                        fSql = fSql & " STS1 = " & sts & ", "
                        fSql = fSql & " TUPDDAT = NOW() , "
                        fSql = fSql & " TMSG = '[" & exe & "]" & msgT & "'"
                        fSql = fSql & " WHERE TNUMREG = " & rs("TNUMREG").Value
                        DB.Execute(fSql)
                    Case UCase("Ultimo")
                        exe = "ImprimeDocumentos.exe " & rs("FWRKID").Value
                        sts = 1
                        msgT = "Paquete Enviado."
                        tLXmsg = "ENVIADO"
                        fSql = " UPDATE RFTASK SET "
                        fSql = fSql & " TLXMSG = '" & tLXmsg & "', "
                        fSql = fSql & " STS1 = " & sts & ", "
                        fSql = fSql & " TUPDDAT = NOW() , "
                        fSql = fSql & " TMSG = '" & msgT & "'"
                        fSql = fSql & " WHERE TNUMREG = " & rs("TNUMREG").Value
                        DB.Execute(fSql)
                        Try
                            Shell(exe, AppWinStyle.Hide, False, -1)
                        Catch ex As Exception
                            msgT = ex.Message.Replace("'", "''")
                            WrtSqlError(exe, msgT)
                        End Try
                    Case UCase("Rutina")
                        pgm = rs("TPGM").Value
                        'Ejecutable de rutinas con parametro TNUMREG de RFTASK
                        exe = pgm & " " & rs("TNUMREG").Value
                        sts = 1
                        msgT = "Comando Enviado."
                        tLXmsg = "EJECUTADA"
                        Try
                            Shell(exe, AppWinStyle.Hide, False, -1)
                        Catch ex As Exception
                            sts = -1
                            msgT = ex.Message.Replace("'", "''")
                            tLXmsg = "ERROR"
                        End Try
                        fSql = " UPDATE RFTASK SET "
                        fSql = fSql & " TLXMSG = '" & tLXmsg & "', "
                        fSql = fSql & " STS1 = " & sts & ", "
                        fSql = fSql & " TUPDDAT = NOW() , "
                        fSql = fSql & " TMSG = '" & msgT & "'"
                        fSql = fSql & " WHERE TNUMREG = " & rs("TNUMREG").Value
                        DB.Execute(fSql)
                    Case UCase("Java")

                End Select
                rs.MoveNext()
            Loop
            rs.Close()
            DB.Close()
        Catch ex As Exception
            sts = -1
            msgT = ex.Message.Replace("'", "''")
            tLXmsg = "ERROR"
            fSql = " UPDATE RFTASK SET "
            fSql = fSql & " TLXMSG = '" & tLXmsg & "', "
            fSql = fSql & " STS1 = " & sts & ", "
            fSql = fSql & " TUPDDAT = NOW() , "
            fSql = fSql & " TMSG = '" & msgT & "'"
            fSql = fSql & " WHERE TNUMREG = " & rs("TNUMREG").Value
            DB.Execute(fSql)
        End Try

    End Sub

    Sub WrtSqlError(sql As String, Descrip As String)
        Dim conn As New ADODB.Connection
        Dim fSql As String
        Dim vSql As String

        Dim gsConnString As String = "Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=APLLX;Password=LXAPL;Persist Security Info=True;Default Collection=ERPLX834F;Force Translate=0;"

        Try
            conn.Open(gsConnString)
            fSql = " INSERT INTO RFLOG( "
            vSql = " VALUES ( "
            fSql = fSql & " USUARIO   , " : vSql = vSql & "'" & System.Net.Dns.GetHostName() & "', "      '//502A
            fSql = fSql & " PROGRAMA  , " : vSql = vSql & "'" & System.Reflection.Assembly.GetExecutingAssembly.FullName & "', "      '//502A
            fSql = fSql & " ALERT     , " : vSql = vSql & "1, "      '//1P0
            fSql = fSql & " EVENTO    , " : vSql = vSql & "'" & Replace(Descrip, "'", "''") & "', "      '//20002A
            fSql = fSql & " TXTSQL    ) " : vSql = vSql & "'" & Replace(sql, "'", "''") & "' ) "      '//5002A
            conn.Execute(fSql & vSql)
            conn.Close()
        Catch ex As Exception

        End Try

    End Sub

End Class
