﻿Imports System.IO
Imports System.Data.SqlClient
Imports System.Text

Module Module1
    Dim db As SqlConnection = New SqlConnection
    Sub Main()

        db.ConnectionString = My.Settings.Conexion
        db.Open()

        Dim cmd As SqlCommand = New SqlCommand()
        Dim fSql, vSql As New StringBuilder()
        fSql.Clear() : vSql.Clear()
        fSql.Append(" INSERT INTO EVENTOS( ")
        vSql.Append(" VALUES( ")
        fSql.Append("Macro     , ") : vSql.AppendFormat("'{0}', ", "DespachosInicia")         'A     Macro
        fSql.Append("Estado    , ") : vSql.AppendFormat("'{0}', ", "Enviado")            'A     Estado
        fSql.Append("Notas     ) ") : vSql.AppendFormat("'{0}') ", "Envio Automatico")         'A     Notas
        cmd.CommandText = fSql.Append(vSql.ToString()).ToString()
        cmd.Connection = db
        cmd.ExecuteNonQuery()

        fSql.Clear()
        'fSql.Append("Insert into DespachosPlanillaHistorico Select *  from DespachosPlanilla  ")

        fSql.Append("Insert into DespachosPlanillaHistorico( IdUnico, Id , EMBALAJE , NoCaja, CantCaja, Pedido, Numpedido, PedidoATG")
        fSql.Append(", CodigoCN, NombreCN, Direccion, Celular, Telefono, Ciudad, Departamento, TipoEntrega, DetalleEntrega, FechaPrevistaEntrega")
        fSql.Append(", RangoHoraEntrega, Fecha, NumMinuta, Peso, Transportadora, Estiba, Estado1, Estado2, Estado3, Estado4, Estado5, TipoDeCaja, PesoFinal, PoliticaEntrega) ")
        fSql.Append(" Select IdUnico, Id , EMBALAJE , NoCaja, CantCaja, Pedido, Numpedido, PedidoATG, CodigoCN, NombreCN, Direccion, ")
        fSql.Append(" Celular, Telefono, Ciudad, Departamento, TipoEntrega, DetalleEntrega, FechaPrevistaEntrega, RangoHoraEntrega, Fecha, ")
        fSql.Append(" NumMinuta, Peso, Transportadora, Estiba, Estado1, Estado2, Estado3, Estado4, Estado5, TipoDeCaja, PesoFinal, PoliticaEntrega")
        fSql.Append(" from DespachosPlanilla ")


        cmd.CommandText = fSql.ToString()
        cmd.ExecuteNonQuery()

        fSql.Clear()
        fSql.Append("Delete from DespachosPlanilla")
        cmd.CommandText = fSql.ToString()
        cmd.ExecuteNonQuery()


    End Sub

End Module
