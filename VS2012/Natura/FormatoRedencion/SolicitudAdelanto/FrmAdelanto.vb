﻿Imports PDF_In_The_BoxCtl
Imports CLDatos
Imports ADODB

Public Class FrmAdelanto
    Dim rsCab As New ADODB.Recordset
    Dim bColor As Boolean
    Dim Flag3
    Dim Ta As TBoxTable
    Dim totalvalor As Double
    Dim totalcop As Double
    Dim totalcambio As Double

    Const CELDA_NORMAL As String = "BrushColor = 255, 255, 255"
    'Const CELDA_SOMBRA As String = "BrushColor = 215, 215, 215"
    Const CELDA_SOMBRA As String = "BrushColor = 255, 255, 255"
    Const TABLE_HEADER_STYLE As String = "ChildrenStyle=""BorderStyle=rect;FontSize=9;Fontname=Arial;Alignment=Center;VertAlignment=Center"""
    Const ESTILO_TIT As String = "Normal;fontsize=6;Fontname=Corbel;BorderColor=Black;Alignment=Center;VertAlignment=Center"

    Private Sub FrmAdelanto_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim parametro As String
        Dim aSubParam() As String
        Dim qPrinter As String = ""
        Dim rs As New ADODB.Recordset
        Dim bGenerar As Boolean
        Dim campoParam As String = ""
        Flag3 = 1
        Environment.GetCommandLineArgs()
        For Each parametro In Environment.GetCommandLineArgs()
            aSubParam = Split(parametro, "=")
            Select Case UCase(aSubParam(0))
                Case "DOC"
                    NumeroDocumento = Ceros(Trim(aSubParam(1)), 8)
            End Select
        Next

        DB.bDateTimeToChar = True
        If Not AbreConexion() Then
            Application.Exit()
        End If

        If My.Settings.LOCAL.ToUpper() = "SI" Then
            campoParam = "CCNOT2"
        End If
        CARPETA_SITIO = DB.getLXParam("SITIO_CARPETA", "TM", campoParam)
        CARPETA_IMG = DB.getLXParam("CARPETA_IMG", "TM", campoParam)
        bPRUEBAS = My.Settings.PRUEBAS.ToUpper() = "SI"

        PDF_Folder = CARPETA_SITIO & "xm\SRG\docs\SolicitudAdelanto\" & NumeroDocumento & "\"
        Firma_Folder = CARPETA_SITIO & "xm\SRG\firma\"
        LOG_File = PDF_Folder & NumeroDocumento & ".txt"
        WrtTxtError(LOG_File, NumeroDocumento)
        Dim arguments As String() = Environment.GetCommandLineArgs()
        WrtTxtError(LOG_File, String.Join(", ", arguments))

        If Not DB.CheckVer() Then
            DB.WrtSqlError(DB.lastError, "")
            End
        End If

        If My.Settings.PRUEBAS = "SI" Then
            GeneraPdf(bGenerar)
        Else
            Try
                If GeneraPdf(bGenerar) Then
                    'fSql = " UPDATE RFTASK SET STS2 = 1  "
                    'fSql = fSql & " WHERE TNUMREG = " & TASK_No
                    'DB.ExecuteSQL(fSql)
                End If
            Catch ex As Exception
                Debug.Print(ex.Message)
                WrtTxtError(LOG_File, ex.Message)
                DB.WrtSqlError(ex.Message, "GeneraPDF")
                End
            End Try
        End If

        DB.Close()
        Application.Exit()
    End Sub

    Public Function GeneraPdf(bGenerar As Boolean) As Boolean
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim ArchivoPDF As String
        Dim Resultado As Boolean = False

        fSql = " SELECT RCAU.UTIPO, RCAU.UNOM, RCAU.UCATEG1, RCAU.UCATEG2, RCAU.UUSR, RCAU.UCATEG3, RFSRGH.SCENCCOST,   "
        fSql &= " (SELECT CCDESC   "
        fSql &= " FROM RFPARAM   "
        fSql &= " WHERE RFPARAM.CCTABL = 'CENTROCOSTO' And RFPARAM.CCCODE = RFSRGH.SCENCCOST) AS CENTROCOSTO,   "
        fSql &= " (SELECT RFPARAM.CCDESC   "
        fSql &= " FROM RFPARAM   "
        fSql &= " WHERE RFPARAM.CCTABL = 'ORIGEN' And RFPARAM.CCCODE = RFSRGH.SORIGEN) As SORIGEN,   "
        fSql &= " (SELECT RFPARAM.CCDESC   "
        fSql &= " FROM  RFPARAM   "
        fSql &= " WHERE RFPARAM.CCTABL = 'DESTINO' And RFPARAM.CCCODE = RFSRGH.SDESTINO) As SDESTINO,   "
        fSql &= " (SELECT RFPARAM.CCDESC   "
        fSql &= " FROM RFPARAM   "
        fSql &= " WHERE RFPARAM.CCTABL = 'LOCALIDAD'  "
        fSql &= " And RFPARAM.CCCODE = RFSRGH.SLOCALIDAD) As SLOCALIDAD,   "
        fSql &= " RFSRGH.SFSALIDA, RFSRGH.SFLLEGADA, RFSRGH.SNUMDOC, RFSRGH.SVALORLOC, RFSRGH.SFECDOC, RCAU.UUPLFILE1,  "
        fSql &= " DateDiff(day, RFSRGH.SFSALIDA, RFSRGH.SFLLEGADA) As DIAS,  "
        fSql &= " RFSRGH.SJUSTIFIC, AP.UNOM As UNOMGESTOR, AP.UUPLFILE1 As UUPLFILE1GESTOR,  "
        fSql &= " RFSRGH.SFSOLICITA, RFSRGH.SFAPRUEBA, RFSRGH.SSOLICITA, RFSRGH.SAPRUEBA  "
        fSql &= " FROM RCAU Inner Join RFSRGH On RCAU.UUSR = RFSRGH.SSOLICITA Inner Join RCAU AP On RFSRGH.SAPRUEBA = AP.UUSR   "
        fSql &= String.Format(" WHERE RFSRGH.SNUMDOC = {0} ", NumeroDocumento)

        If Not DB.OpenRS(rsCab, fSql, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly) Then
            rsCab.Close()
            WrtTxtError(LOG_File, "Datos no encontrados.")
            Return False
        End If

        ArchivoPDF = PDF_Folder & NumeroDocumento & ".pdf"
        Resultado = ArmaPDF(ArchivoPDF)

        rsCab.Close()
        Return Resultado
    End Function

    Function ArmaPDF(ArchivoPDF As String) As Boolean
        Dim Ta As TBoxTable
        Dim Ta1 As TBoxTable
        Dim Detail As TBoxBand
        Dim Note As TBoxBand
        Dim Header As TBoxBand
        Dim Footer As TBoxBand
        Dim AfterFooter As TBoxBand
        Dim b2 As TBoxBand
        Dim b As TBoxBand
        Dim sError As String
        ArmaPDF = False

        If Dir(ArchivoPDF) <> "" Then
            On Error Resume Next
            Kill(ArchivoPDF)
            If Err.Number <> 0 Then
                sError = Err.Description
                WrtTxtError(LOG_File, "ArmaPDF: Borrar " & ArchivoPDF & " " & sError)
                rsCab.Close()
                rsCab = Nothing
                DB.Close()
                End
            End If
            On Error GoTo 0
        End If

        ' Titulos
        With Me.AxPdfBox1
            .FileName = ArchivoPDF
            .Title = "Solicitud de Adelanto - " & NumeroDocumento
            .WantShow = bPRUEBAS
            .WantPageCount = True
            .PaperSizeName = "Letter"
            .BeginDoc()
            .AutoPageFeed = True
            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180;BrushColor = 215, 215, 215")
            b.ChildrenStyle = "Normal;FontSize=9;FontBold=1;Fontname=Arial;BorderColor=Black;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10;BorderLeftMargin=20"
            b.CreateCell(1030, "BorderStyle=none").CreateText("FontName=Arial; FontSize=12").Assign("Datos de Colaborador")
            b.CreateCell(880).CreateText("Alignment=Right;FontSize=9").Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10"
            b.CreateCell(280, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Tipo")
            b.CreateCell(560, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10").Assign(" " + rsCab("UTIPO").Value)
            b.CreateCell(280, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Código SAP")
            b.CreateCell(280, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10").Assign(" " + Trim(rsCab("UUSR").Value))
            b.CreateCell(210, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Proceso")
            b.CreateCell(300, "BorderStyle=none").CreateText("FontName=Arial; FontSize=10").Assign(" " + Ceros(Trim(rsCab("SNUMDOC").Value), 4))
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle=None;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10"
            b.CreateCell(280, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Nombre")
            b.CreateCell(800, "BorderStyle=NONE").CreateText("FontName=Arial; FontSize=10").Assign(" " + Trim(rsCab("UNOM").Value))
            b.CreateCell(830, "BorderStyle=none").CreateText("FontName=Arial; FontSize=10").Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10"
            b.CreateCell(280, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Empresa")
            b.CreateCell(660, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10").Assign(" " + rsCab("UCATEG1").Value)
            b.CreateCell(320, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Centro de Costo")
            If rsCab("CENTROCOSTO").Value IsNot DBNull.Value Then
                b.CreateCell(650, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=9").Assign(" " + Trim(rsCab("SCENCCOST").Value) + " " + Trim(rsCab("CENTROCOSTO").Value))
            Else
                b.CreateCell(650, "BorderStyle=none").CreateText("FontName=Arial; FontSize=10").Assign("")
            End If
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.Height = 80
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle=None;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10"
            b.CreateCell(280, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Área")
            b.CreateCell(800, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10").Assign(" " + Trim(rsCab("UCATEG2").Value))
            b.CreateCell(220, "BorderStyle =none")
            Dim celda As TBoxCell
            celda = b.CreateCell(370, "BorderStyle =none") '500
            With celda.CreateBarcode
                .BarcodeType = TBarcodeType.bcCode39
                .Assign(NumeroDocumento.ToString)
                .ShowCheckDigit = False
                .ShowText = False
            End With
            b.CreateCell(240, "BorderStyle=none").CreateText("FontName=Arial; FontSize=10").Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180;BrushColor = 215, 215, 215")
            b.ChildrenStyle = "Normal;FontSize=9;FontBold=1;Fontname=Arial;BorderColor=Black;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10;BorderLeftMargin=20"
            b.CreateCell(1030, "BorderStyle=none").CreateText("FontName=Arial; FontSize=12").Assign("Viaje o Evento")
            b.CreateCell(880).CreateText("Alignment=Right;FontSize=9").Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle=None;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10"
            b.CreateCell(280, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Origen")
            b.CreateCell(900, "BorderStyle=NONE").CreateText("FontName=Arial; FontSize=10").Assign(" " + Trim(rsCab("SORIGEN").Value))
            b.CreateCell(320, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Documento SAP")
            b.CreateCell(410, "BorderStyle=none").CreateText("FontName=Arial; FontSize=10").Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle=None;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10"
            b.CreateCell(280, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Destino")
            b.CreateCell(530, "BorderStyle=NONE").CreateText("FontName=Arial; FontSize=10").Assign(" " + Trim(rsCab("SDESTINO").Value))
            b.CreateCell(300, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Fecha Salida")
            b.CreateCell(250, "BorderStyle=NONE").CreateText("FontName=Arial; FontSize=10").Assign(" " + CDate(rsCab("SFSALIDA").Value).ToString("dd-MMM-yy"))
            b.CreateCell(300, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Fecha Llegada")
            b.CreateCell(250, "BorderStyle=NONE").CreateText("FontName=Arial; FontSize=10").Assign(" " + CDate(rsCab("SFLLEGADA").Value).ToString("dd-MMM-yy"))
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle=None;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10"
            b.CreateCell(280, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Localidad")
            b.CreateCell(900, "BorderStyle=NONE").CreateText("FontName=Arial; FontSize=10").Assign(" " + Trim(rsCab("SLOCALIDAD").Value))
            b.CreateCell(320, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Días de viaje")
            b.CreateCell(200, "BorderStyle=NONE").CreateText("FontName=Arial; FontSize=10").Assign(" " + rsCab("DIAS").Value.ToString)
            b.CreateCell(210, "BorderStyle=none").CreateText("FontName=Arial; FontSize=10").Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle=None;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10"
            b.CreateCell(400, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Justificación del viaje")
            b.CreateCell(1510, "BorderStyle=NONE").CreateText("FontName=Arial; FontSize=10").Assign(" " + Trim(rsCab("SJUSTIFIC").Value))
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180;BrushColor = 215, 215, 215")
            b.ChildrenStyle = "Normal;FontSize=9;FontBold=1;Fontname=Arial;BorderColor=Black;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10;BorderLeftMargin=20"
            b.CreateCell(1030, "BorderStyle=none").CreateText("FontName=Arial; FontSize=12").Assign("Adelanto")
            b.CreateCell(880).CreateText("Alignment=Right;FontSize=9").Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle=None;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10"
            b.CreateCell(600, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Valor del adelanto en moneda local")
            Dim valor As String
            valor = FormatNumber(rsCab("SVALORLOC").Value, 2)
            b.CreateCell(890, "BorderStyle=NONE").CreateText("FontName=Arial; FontSize=10").Assign(" " + valor)
            b.CreateCell(200, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Fecha")
            b.CreateCell(220, "BorderStyle=NONE").CreateText("FontName=Arial; FontSize=10").Assign(" " + CDate(rsCab("SFECDOC").Value).ToString("dd-MMM-yy"))
            b.Put()

            .EndDoc()
            Debug.Print(.FileName())
            If bPRUEBAS Then
                .ShowDoc()
            End If
            ArchivoPDF = .FileName
        End With
        Return True

    End Function

    Sub Renglon(txt As String)
        Dim b As TBoxBand
        b = AxPdfBox1.CreateBand("BorderStyle=None;Margins=130,200,100,180")
        b.CreateCell(1910).CreateText("FontSize=9;Fontname=Arial;BorderStyle=None;Alignment=Center;VertAlignment=Center;FontColor=red").Assign(txt.ToString)
        b.Put()

    End Sub

    'Private Sub AxPdfBox1_AfterPutBand(sender As Object, e As IPdfBoxEvents_AfterPutBandEvent) Handles AxPdfBox1.AfterPutBand
    '    Dim v As Double
    '    Dim c As Double
    '    If (e.aBand.Role = TBandRole.rlDetail) And Not e.broken Then
    '        v = e.aBand.Table.FieldValue("VALOR")
    '        c = e.aBand.Table.FieldValue("VALORLOC")
    '        totalvalor += v
    '        totalcop += c
    '    End If
    'End Sub

    'PIE DE PAGINA
    Private Sub AxPdfBox1_OnBottomOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnBottomOfPageEvent) Handles AxPdfBox1.OnBottomOfPage
        Dim b As TBoxBand
        Dim rs As New ADODB.Recordset
        'SI NO ES ULTIMA PAGINA
        If Not e.lastPage Then
            Return
        End If
        With AxPdfBox1
            b = .CreateBand("BorderStyle=None;Margins=130,200,100,150")
            b.Breakable = False
            b.Height = 50
            b.CreateCell(1910, "BorderStyle=None").CreateText("Alignment=left").Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180;BrushColor = 215, 215, 215")
            b.ChildrenStyle = "Normal;FontSize=8;FontBold=1;Fontname=Arial;BorderColor=Black;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10;BorderLeftMargin=20"
            b.CreateCell(1910, "BorderStyle=none").CreateText().Assign("Su rendición de cuentas solo será valida con la entrega de esta planilla debidamente aprobada y con los comprobantes en adjunto.")
            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,150")
            b.Height = 35
            b.CreateCell(1910, "BorderStyle=None").CreateText("Alignment=left").Assign("")
            b.Put()

            Renglon("Por medio del presente, declaro de manera expresa y bajo juramento que la información aquí  mencionada y los documentos anexos ")
            Renglon("(de ser aplicable) son exactos, veraces e íntegros. Asimismo, declaro conocer que las eventuales irregularidades sobre estas ")
            Renglon("declaraciones están sujetas a las sanciones previstas en los reglamentos internos, en el Código de Conducta de Natura y las leyes ")
            Renglon("laborales locales. En el supuesto que no sea presentado el informe de gastos y sus respectivos sustentos hasta el día 20 del mes ")
            Renglon("siguiente a la fecha de mi regreso a labores o de la utilización del anticipo conforme con lo descrito en la Norma de Gastos con Viajes, ")
            Renglon("autorizo de manera expresa y libre, el descuento directo sin previo aviso en la planilla del valor total del anticipo recibido en tanto será considerado este monto como un préstamo a mi persona por parte de la compañía. Caso por cuenta de las leyes laborales locales no sea posible realizar el descuento del monto total en apenas un salario, estoy de acuerdo que Natura haga el descuento en cuotas.")

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,150")
            b.Height = 35
            b.CreateCell(1910, "BorderStyle=None").CreateText("Alignment=left").Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180;BrushColor = 215, 215, 215")
            b.ChildrenStyle = "Normal;FontSize=9;FontBold=1;Fontname=Arial;BorderColor=Black;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10;BorderLeftMargin=20"
            b.CreateCell(1910, "BorderStyle=none").CreateText("FontName=Arial; FontSize=12").Assign("Firmas")
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Left;VertAlignment=Center;BorderTopMargin=20;BorderBottomMargin=20"
            b.CreateCell(700, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30").Assign("Nº de identificación del Colaborador: ")
            b.CreateCell(700, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30").Assign(rsCab("SSOLICITA").Value.ToString)
            b.CreateCell(230, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Fecha: ")
            b.CreateCell(280, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30").Assign(CDate(rsCab("SFSOLICITA").Value).ToString("dd-MMM-yy"))
            b.Put()

            If Not rsCab("UUPLFILE1").Value Is DBNull.Value Then
                b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
                b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Left;VertAlignment=Center;BorderTopMargin=20;BorderBottomMargin=20"
                b.Height = 240
                b.CreateCell(700, "BorderStyle=right").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30;VertAlignment=Bottom").Assign("")
                b.CreateCell(1210).CreateImage("BorderStyle=none;Alignment=Right;VertAlignment=Top;BorderLeftMargin=10;Alignment=Center").Assign(Firma_Folder & Trim(rsCab("UUPLFILE1").Value))
                b.Put()
            Else
                b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
                b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Left;VertAlignment=Center;BorderTopMargin=20;BorderBottomMargin=20"
                b.CreateCell(700, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30;VertAlignment=Bottom").Assign("")
                b.CreateCell(1210).CreateText("BorderStyle=rect;Alignment=Right;VertAlignment=Top;BorderLeftMargin=10").Assign("")
                b.Put()
            End If
            b = .CreateBand("Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle=rect;Alignment=Left;VertAlignment=Center;BorderTopMargin=20;BorderBottomMargin=20"
            'b.BorderStyle = TBoxBorderStyle.brRect
            b.CreateCell(700).CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30;VertAlignment=Bottom").Assign("Nombre del Colaborador: ")
            b.CreateCell(1210).CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30;Alignment=Center;VertAlignment=Bottom").Assign(Trim(rsCab("UNOM").Value.ToString))
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180;BrushColor = 215, 215, 215")
            b.Height = 30
            b.ChildrenStyle = "Normal;FontSize=9;FontBold=1;Fontname=Arial;BorderColor=Black;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10;BorderLeftMargin=20"
            b.CreateCell(1910).CreateText("Alignment=Right;FontSize=12").Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Left;VertAlignment=Center;BorderTopMargin=20;BorderBottomMargin=20"
            b.CreateCell(700, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30").Assign("Nº de identificación del Gestor: ")
            b.CreateCell(700, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30").Assign(rsCab("SAPRUEBA").Value.ToString)
            b.CreateCell(230, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Fecha: ")
            b.CreateCell(280, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30").Assign(CDate(rsCab("SFAPRUEBA").Value).ToString("dd-MMM-yy"))
            b.Put()

            If Not rsCab("UUPLFILE1GESTOR").Value Is DBNull.Value Then
                b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
                b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Left;VertAlignment=Center;BorderTopMargin=20;BorderBottomMargin=20"
                b.Height = 240
                b.CreateCell(700, "BorderStyle=right").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30;VertAlignment=Bottom").Assign("")
                b.CreateCell(1210).CreateImage("BorderStyle=none;Alignment=Right;VertAlignment=Top;BorderLeftMargin=10;Alignment=Center").Assign(Firma_Folder & Trim(rsCab("UUPLFILE1GESTOR").Value))
                b.Put()
            Else
                b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
                b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Left;VertAlignment=Center;BorderTopMargin=20;BorderBottomMargin=20"
                b.CreateCell(700, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30;VertAlignment=Bottom").Assign("")
                b.CreateCell(1210).CreateText("BorderStyle=rect;Alignment=Right;VertAlignment=Top;BorderLeftMargin=10").Assign("")
                b.Put()
            End If
            b = .CreateBand("Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle=rect;Alignment=Left;VertAlignment=Center;BorderTopMargin=20;BorderBottomMargin=20"
            'b.BorderStyle = TBoxBorderStyle.brRect
            b.CreateCell(700).CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30;VertAlignment=Bottom").Assign("Nombre del Gestor: ")
            b.CreateCell(1210).CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30;Alignment=Center;VertAlignment=Bottom").Assign(Trim(rsCab("UNOMGESTOR").Value.ToString))
            b.Put()
        End With

    End Sub

    'ENCABEZADO DE PAGINA
    Private Sub AxPdfBox1_OnTopOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnTopOfPageEvent) Handles AxPdfBox1.OnTopOfPage
        Dim b As TBoxBand
        With Me.AxPdfBox1
            .DefineStyle("Normal", "0; Margins=100,100,100,100; fontsize=10;Fontname=Corbel;BorderColor=Black")
            .Style = "Normal"
            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "BorderStyle=None;Alignment=Center"

            b.ChildrenStyle = "Normal;fontsize=16;FontBold=1;BorderColor=Black;Alignment=Center;VertAlignment=Center"

            b.CreateCell(209).CreateImage().Assign(CARPETA_SITIO & "images\logo\LogoNatura90x80.jpg")
            'b.CreateCell(30).CreateText().Assign("")
            'b.CreateCell(1000).CreateText().Assign(My.Application.Info.DirectoryPath & "\Cabecera.rtf")
            b.CreateCell(1525).CreateText().Assign("Solicitud Adelanto")
            b.CreateCell(176).CreateImage().Assign(CARPETA_SITIO & "images\logo\adelanto80x80.jpg")
            b.Put()

            b = .CreateBand("Margins=130,200,100,150")
            b.Height = 40
            b.CreateCell(1910, "BorderStyle=None").CreateText().Assign("")
            b.Put()


        End With

    End Sub

    'BANDAS O FILAS (COLOR ALTERNO)
    Private Sub AxPdfBox1_BeforePutBand4(sender As System.Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_BeforePutBandEvent) Handles AxPdfBox1.BeforePutBand
        If e.aBand.Role = 1 Then ' TBandRole.rlDetail  rlDetail = 1
            If Flag3 = -1 Then
                e.aBand.BrushColor = RGB(215, 215, 215)
            Else
                e.aBand.BrushColor = RGB(255, 255, 255)
            End If
            Flag3 = Flag3 * -1
            bColor = Not bColor
        End If

        'Fila = Fila + 1
        'If Fila > 38 Then
        '    AxPdfBox1.NewPage()
        '    Fila = 0
        'End If

        'Select Case e.aBand.Role
        '    Case rlTotalValor
        '        e.aBand.Cells(1).FirstChild.Assign(totalvalor)
        '    Case rlTotalCOP
        '        e.aBand.Cells(1).FirstChild.Assign(totalcop)
        'End Select

    End Sub
End Class
