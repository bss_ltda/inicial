﻿Imports PDF_In_The_BoxCtl
Imports AxPDF_In_The_BoxCtl
Imports ADODB
Imports System.Data
Imports CLDatos
Imports System.Text
Imports System.IO

Public Class frmCertificadoRet
    Dim rsCab As New ADODB.Recordset
    Dim rsDet As New ADODB.Recordset
    Dim rsNotas As New ADODB.Recordset
    Dim bColor As Boolean
    Dim Flag3
    Dim Ta As TBoxTable
    Dim totalvalor As Double
    Dim totalcop As Double
    Dim totalcambio As Double

    Const CELDA_NORMAL As String = "BrushColor = 255, 255, 255"
    'Const CELDA_SOMBRA As String = "BrushColor = 215, 215, 215"
    Const CELDA_SOMBRA As String = "BrushColor = 255, 255, 255"
    Const TABLE_HEADER_STYLE As String = "ChildrenStyle=""BorderStyle=rect;FontSize=9;Fontname=Arial;Alignment=Center;VertAlignment=Center"""
    Const ESTILO_TIT As String = "Normal;fontsize=6;Fontname=Corbel;BorderColor=Black;Alignment=Center;VertAlignment=Center"


    Private Sub frmCertificadoRet_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim parametro As String
        Dim aSubParam() As String
        Dim qPrinter As String = ""
        Dim rs As New ADODB.Recordset
        Dim bGenerar As Boolean
        Dim campoParam As String = ""
        Flag3 = 1
        Environment.GetCommandLineArgs()
        For Each parametro In Environment.GetCommandLineArgs()
            aSubParam = Split(parametro, "=")
            Select Case UCase(aSubParam(0))
                Case "DOC"
                    NumeroDocumento = Ceros(Trim(aSubParam(1)), 8)
            End Select
        Next

        DB.bDateTimeToChar = True
        Try
            If Not AbreConexion() Then
                Application.Exit()
            End If
        Catch ex As Exception
            Dim cadena As String = Replace("C:\Program Files (x86)\BSSLTDA\FormatoRedencion\errordetalle.txt", "\\", "\")
            Try
                Dim sb As New StringBuilder()
                Dim sw As StreamWriter = New StreamWriter(cadena)
                sb.AppendLine("LOG " & PDF_Folder & ": " & ex.ToString)
                sb.AppendLine(Now().ToString)
                sw.WriteLine(sb.ToString())
                sw.Close()
            Catch ex1 As Exception

            End Try
        End Try

        If My.Settings.LOCAL.ToUpper() = "SI" Then
            campoParam = "CCNOT2"
        End If
        CARPETA_SITIO = DB.getLXParam("SITIO_CARPETA", "TM", campoParam)
        CARPETA_IMG = DB.getLXParam("CARPETA_IMG", "TM", campoParam)
        bPRUEBAS = My.Settings.PRUEBAS.ToUpper() = "SI"

        PDF_Folder = CARPETA_SITIO & "xm\SRG\docs\RedencionAdelanto\" & NumeroDocumento & "\"
        Firma_Folder = CARPETA_SITIO & "xm\SRG\firma\"
        LOG_File = PDF_Folder & NumeroDocumento & ".txt"
        WrtTxtError(LOG_File, NumeroDocumento)
        Dim arguments As String() = Environment.GetCommandLineArgs()
        WrtTxtError(LOG_File, String.Join(", ", arguments))

        If Not DB.CheckVer() Then
            DB.WrtSqlError(DB.lastError, "")
            End
        End If

        If My.Settings.PRUEBAS = "SI" Then
            GeneraPdf(bGenerar)
        Else
            Try
                If GeneraPdf(bGenerar) Then
                    'fSql = " UPDATE RFTASK SET STS2 = 1  "
                    'fSql = fSql & " WHERE TNUMREG = " & TASK_No
                    'DB.ExecuteSQL(fSql)
                End If
            Catch ex As Exception
                Debug.Print(ex.Message)
                WrtTxtError(LOG_File, ex.Message)
                DB.WrtSqlError(ex.Message, "GeneraPDF")
                End
            End Try
        End If

        DB.Close()
        Application.Exit()
    End Sub

    Public Function GeneraPdf(bGenerar As Boolean) As Boolean
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim ArchivoPDF As String
        Dim Resultado As Boolean = False

        fSql = " SELECT RCAU.UTIPO, RCAU.UNOM, RCAU.UCATEG1, RCAU.UCATEG2, RCAU.UUSR, RCAU.UCATEG3, RFSRGH.SCENCCOST,   "
        fSql &= " (SELECT CCDESC   "
        fSql &= " FROM RFPARAM   "
        fSql &= " WHERE RFPARAM.CCTABL = 'CENTROCOSTO' And RFPARAM.CCCODE = RFSRGH.SCENCCOST) AS CENTROCOSTO,   "
        fSql &= " (SELECT RFPARAM.CCDESC   "
        fSql &= " FROM RFPARAM   "
        fSql &= " WHERE RFPARAM.CCTABL = 'ORIGEN' And RFPARAM.CCCODE = RFSRGH.SORIGEN) As SORIGEN,   "
        fSql &= " (SELECT RFPARAM.CCDESC   "
        fSql &= " FROM  RFPARAM   "
        fSql &= " WHERE RFPARAM.CCTABL = 'DESTINO' And RFPARAM.CCCODE = RFSRGH.SDESTINO) As SDESTINO,   "
        fSql &= " (SELECT RFPARAM.CCDESC   "
        fSql &= " FROM RFPARAM   "
        fSql &= " WHERE RFPARAM.CCTABL = 'LOCALIDAD'  "
        fSql &= " And RFPARAM.CCCODE = RFSRGH.SLOCALIDAD) As SLOCALIDAD,   "
        fSql &= " RFSRGH.SFSALIDA, RFSRGH.SFLLEGADA, RFSRGH.SNUMDOC, RFSRGH.SVALORLOC, RFSRGH.SFECDOC, RCAU.UUPLFILE1,  "
        fSql &= " DateDiff(day, RFSRGH.SFSALIDA, RFSRGH.SFLLEGADA) As DIAS,  "
        fSql &= " RFSRGH.SJUSTIFIC, AP.UNOM As UNOMGESTOR, AP.UUPLFILE1 As UUPLFILE1GESTOR,  "
        fSql &= " RFSRGH.SFSOLICITA, RFSRGH.SFAPRUEBA, RFSRGH.SSOLICITA, RFSRGH.SAPRUEBA  "
        fSql &= " FROM RCAU Inner Join RFSRGH On RCAU.UUSR = RFSRGH.SSOLICITA Inner Join RCAU AP On RFSRGH.SAPRUEBA = AP.UUSR   "
        fSql &= String.Format(" WHERE RFSRGH.SNUMDOC = {0} ", NumeroDocumento)

        If Not DB.OpenRS(rsCab, fSql, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly) Then
            rsCab.Close()
            WrtTxtError(LOG_File, "Datos no encontrados.")
            Return False
        End If

        '*****************************************************************
        '   DETALLE
        '*****************************************************************

        fSql = " SELECT DNUMDOC, DNUMLIN, DFECDOC,  "
        fSql &= " (SELECT RTRIM(CCDESC) + ' - ' + DCUENTA "
        fSql &= " FROM RFPARAM  "
        fSql &= " WHERE (CCTABL = 'CUENTA') AND (CCCODE = RFSRGL.DCUENTA) AND CCCODE2 =  "
        fSql &= " (SELECT UTIPO  "
        fSql &= " FROM RCAU WHERE UUSR =  "
        fSql &= " (SELECT SSOLICITA  FROM RFSRGH  "
        fSql &= String.Format(" WHERE SNUMDOC = {0} ", NumeroDocumento)
        fSql &= "))) AS CUENTA,  "
        fSql &= " DCUENTA, DPROV, DDESCRIP, CAST(DVALOR AS Float) AS DVALOR,  "
        fSql &= " DMONEDA,  "
        fSql &= " (SELECT RTRIM(CCALTC)  "
        fSql &= " FROM RFPARAM AS RFPARAM_1  "
        fSql &= " WHERE (CCTABL = 'MONEDA') AND (CCCODE = RFSRGL.DMONEDA)) AS MONEDA,  "
        fSql &= " DTRMUSD, CAST(DTRMLOC AS Float) AS DTRMLOC, CAST(DVALORLOC AS Float) AS DVALORLOC,  "
        fSql &= " DNUMUPL, FFLAG, STS1, STS2, STS3, STS4, STS5, DCRTUSR, DCRTDAT, DUPDDAT, DIMPUESTOS, DREFER  "
        fSql &= " FROM RFSRGL  "
        fSql &= String.Format(" WHERE DNUMDOC = {0} ", NumeroDocumento)
        fSql &= " ORDER BY DFECDOC "

        If Not DB.OpenRS(rsDet, fSql, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly) Then
            rsDet.Close()
            rsCab.Close()
            WrtTxtError(LOG_File, "Registro de Gastos no encontrado.")
            Return False
        End If
        '*****************************************************************
        '   FIN DETALLE
        '*****************************************************************

        fSql = " SELECT DCUENTA,  "
        fSql &= " (SELECT RTRIM(CCDESC) + ' - ' + DCUENTA   "
        fSql &= " FROM RFPARAM  "
        fSql &= " WHERE (CCTABL = 'CUENTA') AND (CCCODE = RFSRGL.DCUENTA) AND CCCODE2 =  "
        fSql &= " (SELECT UTIPO  "
        fSql &= " FROM RCAU  "
        fSql &= " WHERE UUSR =  "
        fSql &= " (SELECT SSOLICITA  "
        fSql &= " FROM RFSRGH  "
        fSql &= String.Format(" WHERE SNUMDOC = {0} ", NumeroDocumento)
        fSql &= " ))) AS CUENTA,  "
        fSql &= " Sum(CAST(DVALOR AS FLOAT)) AS VALOR,  "
        fSql &= " Sum(CAST(DVALORLOC AS FLOAT)) AS VALORLOC  "
        fSql &= " FROM RFSRGL  "
        fSql &= String.Format(" WHERE DNUMDOC = {0} ", NumeroDocumento)
        fSql &= " GROUP BY DCUENTA  "
        fSql &= " ORDER BY DCUENTA "

        If Not DB.OpenRS(rsNotas, fSql, CursorTypeEnum.adOpenDynamic, LockTypeEnum.adLockReadOnly) Then
            rsDet.Close()
            rsCab.Close()
            WrtTxtError(LOG_File, "Gastos por cuenta contable no encontrados.")
            Return False
        End If

        ArchivoPDF = PDF_Folder & NumeroDocumento & ".pdf"
        Resultado = ArmaPDF(ArchivoPDF)

        rsDet.Close()
        rsNotas.Close()
        rsCab.Close()
        Return Resultado

    End Function

    Function ArmaPDF(ArchivoPDF As String) As Boolean
        Dim Ta As TBoxTable
        Dim Ta1 As TBoxTable
        Dim Detail As TBoxBand
        Dim Note As TBoxBand
        Dim Header As TBoxBand
        Dim Footer As TBoxBand
        Dim AfterFooter As TBoxBand
        Dim b2 As TBoxBand
        Dim b As TBoxBand
        Dim sError As String
        ArmaPDF = False

        If Dir(ArchivoPDF) <> "" Then
            On Error Resume Next
            Kill(ArchivoPDF)
            If Err.Number <> 0 Then
                sError = Err.Description
                WrtTxtError(LOG_File, "ArmaPDF: Borrar " & ArchivoPDF & " " & sError)
                rsDet.Close()
                rsNotas.Close()
                rsCab.Close()
                rsDet = Nothing
                rsNotas = Nothing
                rsCab = Nothing
                DB.Close()
                End
            End If
            On Error GoTo 0
        End If

        ' Titulos
        With Me.AxPdfBox1
            .FileName = ArchivoPDF
            .Title = "Redención de Cuentas de Adelanto - " & NumeroDocumento
            .WantShow = bPRUEBAS
            .WantPageCount = True
            .PaperSizeName = "Letter"
            .AutoPageFeed = True
            .BeginDoc()
            
            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180;BrushColor = 215, 215, 215")
            b.ChildrenStyle = "Normal;FontSize=9;FontBold=1;Fontname=Arial;BorderColor=Black;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10;BorderLeftMargin=20"
            b.CreateCell(1030, "BorderStyle=none").CreateText("FontName=Arial; FontSize=12").Assign("Datos de Colaborador")
            b.CreateCell(880).CreateText("Alignment=Right;FontSize=9").Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10"
            b.CreateCell(280, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Tipo")
            b.CreateCell(560, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10").Assign(" " + rsCab("UTIPO").Value)
            b.CreateCell(280, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Código SAP")
            b.CreateCell(280, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10").Assign(" " + Trim(rsCab("UUSR").Value))
            b.CreateCell(210, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Proceso")
            b.CreateCell(300, "BorderStyle=none").CreateText("FontName=Arial; FontSize=10").Assign(" " + Ceros(Trim(rsCab("SNUMDOC").Value), 4))
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle=None;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10"
            b.CreateCell(280, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Nombre")
            b.CreateCell(800, "BorderStyle=NONE").CreateText("FontName=Arial; FontSize=10").Assign(" " + Trim(rsCab("UNOM").Value))
            b.CreateCell(830, "BorderStyle=none").CreateText("FontName=Arial; FontSize=10").Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10"
            b.CreateCell(280, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Empresa")
            b.CreateCell(660, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10").Assign(" " + rsCab("UCATEG1").Value)
            b.CreateCell(320, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Centro de Costo")
            If rsCab("CENTROCOSTO").Value IsNot DBNull.Value Then
                b.CreateCell(650, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=9").Assign(" " + Trim(rsCab("SCENCCOST").Value) + " " + Trim(rsCab("CENTROCOSTO").Value))
            Else
                b.CreateCell(650, "BorderStyle=none").CreateText("FontName=Arial; FontSize=10").Assign("")
            End If
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.Height = 80
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle=None;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10"
            b.CreateCell(280, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Área")
            b.CreateCell(800, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10").Assign(" " + Trim(rsCab("UCATEG2").Value))
            b.CreateCell(220, "BorderStyle =none")
            Dim celda As TBoxCell
            celda = b.CreateCell(370, "BorderStyle =none") '500
            With celda.CreateBarcode
                .BarcodeType = TBarcodeType.bcCode39
                .Assign(NumeroDocumento.ToString)
                .ShowCheckDigit = False
                .ShowText = False
            End With
            b.CreateCell(240, "BorderStyle=none").CreateText("FontName=Arial; FontSize=10").Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180;BrushColor = 215, 215, 215")
            b.ChildrenStyle = "Normal;FontSize=9;FontBold=1;Fontname=Arial;BorderColor=Black;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10;BorderLeftMargin=20"
            b.CreateCell(1030, "BorderStyle=none").CreateText("FontName=Arial; FontSize=12").Assign("Viaje o Evento")
            b.CreateCell(880).CreateText("Alignment=Right;FontSize=9").Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle=None;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10"
            b.CreateCell(280, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Origen")
            b.CreateCell(900, "BorderStyle=NONE").CreateText("FontName=Arial; FontSize=10").Assign(" " + Trim(rsCab("SORIGEN").Value))
            b.CreateCell(320, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Documento SAP")
            'b.CreateCell(200, "BorderStyle=NONE").CreateText("FontName=Arial; FontSize=10").Assign(" " + Trim(rsCab("SNUMDOC").Value))
            b.CreateCell(410, "BorderStyle=none").CreateText("FontName=Arial; FontSize=10").Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle=None;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10"
            b.CreateCell(280, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Destino")
            b.CreateCell(530, "BorderStyle=NONE").CreateText("FontName=Arial; FontSize=10").Assign(" " + Trim(rsCab("SDESTINO").Value))
            b.CreateCell(300, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Fecha Salida")
            b.CreateCell(250, "BorderStyle=NONE").CreateText("FontName=Arial; FontSize=10").Assign(" " + CDate(rsCab("SFSALIDA").Value).ToString("dd-MMM-yy"))
            b.CreateCell(300, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Fecha Llegada")
            b.CreateCell(250, "BorderStyle=NONE").CreateText("FontName=Arial; FontSize=10").Assign(" " + CDate(rsCab("SFLLEGADA").Value).ToString("dd-MMM-yy"))
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle=None;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10"
            b.CreateCell(280, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Localidad")
            b.CreateCell(900, "BorderStyle=NONE").CreateText("FontName=Arial; FontSize=10").Assign(" " + Trim(rsCab("SLOCALIDAD").Value))
            b.CreateCell(320, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Días de viaje")
            b.CreateCell(200, "BorderStyle=NONE").CreateText("FontName=Arial; FontSize=10").Assign(" " + rsCab("DIAS").Value.ToString)
            b.CreateCell(210, "BorderStyle=none").CreateText("FontName=Arial; FontSize=10").Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle=None;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10"
            b.CreateCell(400, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Justificación del viaje")
            b.CreateCell(1510, "BorderStyle=NONE").CreateText("FontName=Arial; FontSize=10").Assign(" " + Trim(rsCab("SJUSTIFIC").Value))
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180;BrushColor = 215, 215, 215")
            b.ChildrenStyle = "Normal;FontSize=9;FontBold=1;Fontname=Arial;BorderColor=Black;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10;BorderLeftMargin=20"
            b.CreateCell(1030, "BorderStyle=none").CreateText("FontName=Arial; FontSize=12").Assign("Adelanto")
            b.CreateCell(880).CreateText("Alignment=Right;FontSize=9").Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle=None;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10"
            b.CreateCell(600, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Valor del adelanto en moneda local")
            Dim valor As String
            valor = FormatNumber(rsCab("SVALORLOC").Value, 2)
            b.CreateCell(890, "BorderStyle=NONE").CreateText("FontName=Arial; FontSize=10").Assign(" " + valor)
            b.CreateCell(200, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Fecha")
            b.CreateCell(220, "BorderStyle=NONE").CreateText("FontName=Arial; FontSize=10").Assign(" " + CDate(rsCab("SFECDOC").Value).ToString("dd-MMM-yy"))
            b.Put()

            totalvalor = 0
            totalcambio = 0
            totalcop = 0

            If rsNotas.BOF = True And rsNotas.EOF = True Then
                rsNotas.MoveFirst()
            Else
                Do While Not rsNotas.EOF = True
                    totalvalor += CDbl(rsNotas("VALOR").Value)
                    totalcop += CDbl(rsNotas("VALORLOC").Value)
                    rsNotas.MoveNext()
                Loop
            End If

            Dim valordev As String
            valordev = FormatNumber(CDbl(rsCab("SVALORLOC").Value) - totalvalor, 2)

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle=None;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10"
            If CDbl(valordev) > 0 Then
                b.CreateCell(600, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Valor a regresar COP")
            Else
                b.CreateCell(600, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Valor a reembolsar COP")
            End If
            b.CreateCell(1310, "BorderStyle=NONE").CreateText("FontName=Arial; FontSize=10").Assign(" " & valordev.Replace("-", ""))
            b.Put()

            b = .CreateBand("Margins=130,200,100,150")
            b.Height = 2
            b.CreateCell(1910, "BorderStyle=None").CreateText().Assign("")
            b.Put()

            b2 = .CreateBand("BorderStyle=none;Margins=130,200,100,150;BrushColor = 215, 215, 215;FontBold=1")
            b2.Height = 50
            b2.CreateCell(1910).CreateText().Assign("Gastos por Cuenta Contable")
            b2.Put()

            b2 = .CreateBand("BorderStyle=none;Margins=130,200,100,150")
            b2.Height = 50
            b2.CreateCell(1910).CreateText().Assign("")
            b2.Put()

            Ta1 = .CreateTable("BorderStyle=None;Margins=130,200,100,150;Alignment=Center")
            Ta1.Assign(rsNotas)
            'Tabla del Detalle
            Detail = Ta1.CreateBand()
            Detail.Role = TBandRole.rlDetail
            Detail.ChildrenStyle = "FontSize=8;Fontname=Arial;Alignment=Center;VertAlignment=Center;BorderStyle=none"
            Detail.CreateCell(855, "BorderStyle=none").CreateText("Alignment=Right").Bind("CUENTA")
            With Detail.CreateCell(350).CreateText("Alignment=Right")
                .Bind("VALOR")
                .Format = "#,##0.00"
            End With
            With Detail.CreateCell(350).CreateText("Alignment=Right")
                .Bind("VALORLOC")
                .Format = "#,##0.00"
            End With
            Detail.Breakable = True
            Header = Ta1.CreateBand() 'Detail.CreateClone(TABLE_HEADER_STYLE)
            Header.Role = TBandRole.rlBodyHeader
            Header.ChildrenStyle = "fontsize=8;Alignment=Center;VertAlignment=Center;BorderStyle=rect"
            Header.CreateCell(855).CreateText("fontbold=1").Assign("Cuenta")
            Header.CreateCell(350).CreateText("fontbold=1").Assign("Valor Neto")
            Header.CreateCell(350).CreateText("fontbold=1").Assign("COP")
            Ta1.Put()

            b2 = .CreateBand("BorderStyle=none;Margins=130,200,100,150;Alignment=Center")
            b2.ChildrenStyle = "fontsize=8;Fontname=Arial;Alignment=Center; VertAlignment=Center; BorderStyle=none"
            b2.Height = 50
            b2.CreateCell(855).CreateText("FontBold=1;Alignment=Right").Assign("TOTAL")
            b2.CreateCell(350).CreateText("Alignment=Right").Assign(FormatNumber(totalvalor.ToString, 2))
            b2.CreateCell(350).CreateText("Alignment=Right").Assign(FormatNumber(totalcop.ToString, 2))
            b2.Put()

            b2 = .CreateBand("BorderStyle=none;Margins=130,200,100,150")
            b2.Height = 50
            b2.CreateCell(1910).CreateText().Assign("")
            b2.Put()

            b2 = .CreateBand("BorderStyle=none;Margins=130,200,100,150;BrushColor = 215, 215, 215;FontBold=1")
            b2.Height = 50
            b2.CreateCell(1910).CreateText().Assign("Registro de los Gastos")
            b2.Put()

            b2 = .CreateBand("BorderStyle=none;Margins=130,200,100,150")
            b2.Height = 50
            b2.CreateCell(1910).CreateText().Assign("")
            b2.Put()

            totalvalor = 0
            totalcambio = 0
            totalcop = 0

            If rsDet.BOF = True And rsDet.EOF = True Then
                rsDet.MoveFirst()
            Else
                Do While Not rsDet.EOF = True
                    totalvalor += CDbl(rsDet("DVALOR").Value)
                    totalcambio += CDbl(rsDet("DTRMLOC").Value)
                    totalcop += CDbl(rsDet("DVALORLOC").Value)
                    rsDet.MoveNext()
                Loop
            End If
            Flag3 = 1
            Ta = .CreateTable("BorderStyle=None;Margins=130,200,100,150;Alignment=Center")
            Ta.Assign(rsDet)
            'LLENA LA TABLA
            Detail = Ta.CreateBand()
            Detail.Role = TBandRole.rlDetail
            Detail.ChildrenStyle = "fontsize=7;Fontname=Arial;VertAlignment=Center;BorderStyle=none"
            With Detail.CreateCell(160).CreateText("Alignment=center")
                .Bind("DFECDOC")
                .Format = "dd-MMM-yy"
            End With
            Detail.CreateCell(555).CreateText("Alignment=Left;fontsize=6").Bind("CUENTA")
            Detail.CreateCell(515).CreateText("Alignment=Left;fontsize=7").Bind("DDESCRIP")
            With Detail.CreateCell(180).CreateText("Alignment=Right")
                .Bind("DVALOR")
                .Format = "#,##0.00"
            End With
            Detail.CreateCell(180).CreateText("Alignment=center").Bind("MONEDA")
            With Detail.CreateCell(160).CreateText("Alignment=Right")
                .Bind("DTRMLOC")
                .Format = "#,##0.00"
            End With
            With Detail.CreateCell(160).CreateText("Alignment=Right")
                .Bind("DVALORLOC")
                .Format = "#,##0.00"
            End With
            Detail.Breakable = True
            Header = Ta.CreateBand() 'Detail.CreateClone(TABLE_HEADER_STYLE)
            Header.Role = TBandRole.rlBodyHeader
            Header.ChildrenStyle = "fontsize=8;Alignment=Center;VertAlignment=Center;BorderStyle=rect"
            Header.CreateCell(160).CreateText("fontbold=1").Assign("Fecha Doc")
            Header.CreateCell(555).CreateText("fontbold=1").Assign("Cuenta Contable")
            Header.CreateCell(515).CreateText("fontbold=1").Assign("Descripción")
            Header.CreateCell(180).CreateText("fontbold=1").Assign("Vlr. Neto")
            Header.CreateCell(180).CreateText("fontbold=1").Assign("Moneda")
            Header.CreateCell(160).CreateText("fontbold=1").Assign("Cambio")
            Header.CreateCell(160).CreateText("fontbold=1").Assign("COP")
            Ta.Put()

            b2 = .CreateBand("BorderStyle=none;Margins=130,200,100,150;Alignment=Center")
            b2.ChildrenStyle = "fontsize=8;Fontname=Arial;Alignment=Center; VertAlignment=Center; BorderStyle=none"
            b2.Height = 50
            b2.CreateCell(1230).CreateText("Alignment=Right;FontBold=1").Assign("TOTAL")
            b2.CreateCell(180).CreateText("Alignment=Right").Assign(FormatNumber(totalvalor.ToString, 2))
            b2.CreateCell(180).CreateText().Assign("")
            b2.CreateCell(160).CreateText("Alignment=Right").Assign(FormatNumber(totalcambio.ToString, 2))
            b2.CreateCell(160).CreateText("Alignment=Right").Assign(FormatNumber(totalcop.ToString, 2))
            b2.Put()

            .EndDoc()
            Debug.Print(.FileName())
            If bPRUEBAS Then
                .ShowDoc()
            End If
            ArchivoPDF = .FileName
        End With
        Return True

    End Function

    Sub Renglon(txt As String)
        Dim b As TBoxBand
        b = AxPdfBox1.CreateBand("BorderStyle=None;Margins=130,200,100,180")
        b.CreateCell(1910).CreateText("FontSize=9;Fontname=Arial;BorderStyle=None;Alignment=Center;VertAlignment=Center;FontColor=red").Assign(txt.ToString)
        b.Put()

    End Sub

    'Private Sub AxPdfBox1_AfterPutBand(sender As Object, e As IPdfBoxEvents_AfterPutBandEvent) Handles AxPdfBox1.AfterPutBand
    '    Dim v As Double
    '    Dim c As Double
    '    If (e.aBand.Role = TBandRole.rlDetail) And Not e.broken Then
    '        v = e.aBand.Table.FieldValue("VALOR")
    '        c = e.aBand.Table.FieldValue("VALORLOC")
    '        totalvalor += v
    '        totalcop += c
    '    End If
    'End Sub

    'PIE DE PAGINA
    Private Sub AxPdfBox1_OnBottomOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnBottomOfPageEvent) Handles AxPdfBox1.OnBottomOfPage
        Dim b As TBoxBand
        Dim rs As New ADODB.Recordset
        'SI NO ES ULTIMA PAGINA
        If Not e.lastPage Then
            Return
        End If
        With AxPdfBox1
            b = .CreateBand("BorderStyle=None;Margins=130,200,100,150")
            b.Breakable = False
            b.Height = 50
            b.CreateCell(1910, "BorderStyle=None").CreateText("Alignment=left").Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180;BrushColor = 215, 215, 215")
            b.ChildrenStyle = "Normal;FontSize=8;FontBold=1;Fontname=Arial;BorderColor=Black;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10;BorderLeftMargin=20"
            b.CreateCell(1910, "BorderStyle=none").CreateText().Assign("Su rendición de cuentas solo será valida con la entrega de esta planilla debidamente aprobada y con los comprobantes en adjunto.")
            b.Put()

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,150")
            b.Height = 35
            b.CreateCell(1910, "BorderStyle=None").CreateText("Alignment=left").Assign("")
            b.Put()

            Renglon("Por medio del presente, declaro de manera expresa y bajo juramento que la información aquí  mencionada y los documentos anexos ")
            Renglon("(de ser aplicable) son exactos, veraces e íntegros. Asimismo, declaro conocer que las eventuales irregularidades sobre estas ")
            Renglon("declaraciones están sujetas a las sanciones previstas en los reglamentos internos, en el Código de Conducta de Natura y las leyes ")
            Renglon("laborales locales. En el supuesto que no sea presentado el informe de gastos y sus respectivos sustentos hasta el día 20 del mes ")
            Renglon("siguiente a la fecha de mi regreso a labores o de la utilización del anticipo conforme con lo descrito en la Norma de Gastos con Viajes, ")
            Renglon("autorizo de manera expresa y libre, el descuento directo sin previo aviso en la planilla del valor total del anticipo recibido en tanto será considerado este monto como un préstamo a mi persona por parte de la compañía. Caso por cuenta de las leyes laborales locales no sea posible realizar el descuento del monto total en apenas un salario, estoy de acuerdo que Natura haga el descuento en cuotas.")

            b = .CreateBand("BorderStyle=None;Margins=130,200,100,150")
            b.Height = 35
            b.CreateCell(1910, "BorderStyle=None").CreateText("Alignment=left").Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180;BrushColor = 215, 215, 215")
            b.ChildrenStyle = "Normal;FontSize=9;FontBold=1;Fontname=Arial;BorderColor=Black;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10;BorderLeftMargin=20"
            b.CreateCell(1910, "BorderStyle=none").CreateText("FontName=Arial; FontSize=12").Assign("Firmas")
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Left;VertAlignment=Center;BorderTopMargin=20;BorderBottomMargin=20"
            b.CreateCell(700, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30").Assign("Nº de identificación del Colaborador: ")
            b.CreateCell(700, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30").Assign(rsCab("SSOLICITA").Value.ToString)
            b.CreateCell(230, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Fecha: ")
            b.CreateCell(280, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30").Assign(CDate(rsCab("SFSOLICITA").Value).ToString("dd-MMM-yy"))
            b.Put()

            If Not rsCab("UUPLFILE1").Value Is DBNull.Value Then
                b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
                b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Left;VertAlignment=Center;BorderTopMargin=20;BorderBottomMargin=20"
                b.Height = 240
                b.CreateCell(700, "BorderStyle=right").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30;VertAlignment=Bottom").Assign("")
                b.CreateCell(1210).CreateImage("BorderStyle=none;Alignment=Right;VertAlignment=Top;BorderLeftMargin=10;Alignment=Center").Assign(Firma_Folder & Trim(rsCab("UUPLFILE1").Value))
                b.Put()
            Else
                b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
                b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Left;VertAlignment=Center;BorderTopMargin=20;BorderBottomMargin=20"
                b.CreateCell(700, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30;VertAlignment=Bottom").Assign("")
                b.CreateCell(1210).CreateText("BorderStyle=rect;Alignment=Right;VertAlignment=Top;BorderLeftMargin=10").Assign("")
                b.Put()
            End If
            b = .CreateBand("Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle=rect;Alignment=Left;VertAlignment=Center;BorderTopMargin=20;BorderBottomMargin=20"
            'b.BorderStyle = TBoxBorderStyle.brRect
            b.CreateCell(700).CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30;VertAlignment=Bottom").Assign("Nombre del Colaborador: ")
            b.CreateCell(1210).CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30;Alignment=Center;VertAlignment=Bottom").Assign(Trim(rsCab("UNOM").Value.ToString))
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180;BrushColor = 215, 215, 215")
            b.Height = 30
            b.ChildrenStyle = "Normal;FontSize=9;FontBold=1;Fontname=Arial;BorderColor=Black;Alignment=Left;VertAlignment=Center;BorderTopMargin=10;BorderBottomMargin=10;BorderLeftMargin=20"
            b.CreateCell(1910).CreateText("Alignment=Right;FontSize=12").Assign("")
            b.Put()

            b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Left;VertAlignment=Center;BorderTopMargin=20;BorderBottomMargin=20"
            b.CreateCell(700, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30").Assign("Nº de identificación del Gestor: ")
            b.CreateCell(700, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30").Assign(rsCab("SAPRUEBA").Value.ToString)
            b.CreateCell(230, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=10").Assign("Fecha: ")
            b.CreateCell(280, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30").Assign(CDate(rsCab("SFAPRUEBA").Value).ToString("dd-MMM-yy"))
            b.Put()

            
            If Not rsCab("UUPLFILE1GESTOR").Value Is DBNull.Value Then
                b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
                b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Left;VertAlignment=Center;BorderTopMargin=20;BorderBottomMargin=20"
                b.Height = 240
                b.CreateCell(700, "BorderStyle=right").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30;VertAlignment=Bottom").Assign("")
                b.CreateCell(1210).CreateImage("BorderStyle=none;Alignment=Right;VertAlignment=Top;BorderLeftMargin=10;Alignment=Center").Assign(Firma_Folder & Trim(rsCab("UUPLFILE1GESTOR").Value))
                b.Put()
            Else
                b = .CreateBand("BorderStyle=rect;Margins=130,200,100,180")
                b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle =None;Alignment=Left;VertAlignment=Center;BorderTopMargin=20;BorderBottomMargin=20"
                b.CreateCell(700, "BorderStyle=rect").CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30;VertAlignment=Bottom").Assign("")
                b.CreateCell(1210).CreateText("BorderStyle=rect;Alignment=Right;VertAlignment=Top;BorderLeftMargin=10").Assign("")
                b.Put()
            End If
            b = .CreateBand("Margins=130,200,100,180")
            b.ChildrenStyle = "Normal;FontSize=9;Fontname=Arial;BorderColor=Black;BorderStyle=rect;Alignment=Left;VertAlignment=Center;BorderTopMargin=20;BorderBottomMargin=20"
            'b.BorderStyle = TBoxBorderStyle.brRect
            b.CreateCell(700).CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30;VertAlignment=Bottom").Assign("Nombre del Gestor: ")
            b.CreateCell(1210).CreateText("FontName=Arial; FontSize=10; BorderLeftMargin=30;Alignment=Center;VertAlignment=Bottom").Assign(Trim(rsCab("UNOMGESTOR").Value.ToString))
            b.Put()
        End With

    End Sub

    'ENCABEZADO DE PAGINA
    Private Sub AxPdfBox1_OnTopOfPage(sender As Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_OnTopOfPageEvent) Handles AxPdfBox1.OnTopOfPage
        Dim b As TBoxBand
        With Me.AxPdfBox1
            .DefineStyle("Normal", "0; Margins=100,100,100,100; fontsize=10;Fontname=Corbel;BorderColor=Black")
            .Style = "Normal"
            b = .CreateBand("BorderStyle=None;Margins=130,200,100,180")
            b.ChildrenStyle = "BorderStyle=None;Alignment=Center"
            b.ChildrenStyle = "Normal;fontsize=16;FontBold=1;BorderColor=Black;Alignment=Center;VertAlignment=Center;BorderLeftMargin=20;BorderTopMargin=20"
            b.CreateCell(222).CreateImage("Alignment=Right;VertAlignment=Top").Assign(CARPETA_SITIO & "images\logo\LogoNatura90x80.jpg")
            'b.CreateCell(30).CreateText().Assign("")
            'b.CreateCell(1000).CreateText().Assign(My.Application.Info.DirectoryPath & "\Cabecera.rtf")
            'b.CreateCell(1500).CreateText().Assign("Informe para Reembolso, Adelanto y Rendición de Cuentas de Colaboradores")
            b.CreateCell(1488).CreateText().Assign("Redención de Cuentas de Adelanto")
            'b.Height = 240
            b.CreateCell(200).CreateImage("Alignment=Right;VertAlignment=Top").Assign(CARPETA_SITIO & "images\logo\redencion80.jpg")
            b.Put()

            b = .CreateBand("Margins=130,200,100,150")
            b.Height = 40
            b.CreateCell(1910, "BorderStyle=None").CreateText().Assign("")
            b.Put()


        End With

    End Sub

    'BANDAS O FILAS (COLOR ALTERNO)
    Private Sub AxPdfBox1_BeforePutBand4(sender As System.Object, e As AxPDF_In_The_BoxCtl.IPdfBoxEvents_BeforePutBandEvent) Handles AxPdfBox1.BeforePutBand
        If e.aBand.Role = 1 Then ' TBandRole.rlDetail  rlDetail = 1
            If Flag3 = -1 Then
                e.aBand.BrushColor = RGB(215, 215, 215)
            Else
                e.aBand.BrushColor = RGB(255, 255, 255)
            End If
            Flag3 = Flag3 * -1
            bColor = Not bColor
        End If

        'Fila = Fila + 1
        'If Fila > 41 Then
        'AxPdfBox1.NewPage()
        'Fila = 0
        'End If

        'Select Case e.aBand.Role
        '    Case rlTotalValor
        '        e.aBand.Cells(1).FirstChild.Assign(totalvalor)
        '    Case rlTotalCOP
        '        e.aBand.Cells(1).FirstChild.Assign(totalcop)
        'End Select

    End Sub

End Class

