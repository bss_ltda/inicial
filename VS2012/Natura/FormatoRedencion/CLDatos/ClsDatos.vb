﻿Imports System.Data.SqlClient

Public Class ClsDatos
    Private command As New SqlCommand
    Private cadena As String = "server=BSSLTDA-DEVELOP\BSSLTDA_DEV;database=InfoNatura;user id=sa;password=Oficina2016"
    Private conexion As SqlConnection = New SqlConnection(cadena)
    Private trans As SqlTransaction
    Private builder As SqlCommandBuilder
    Private adapter As SqlDataAdapter = New SqlDataAdapter()
    Private dt As DataTable
    Private ds As DataSet
    Private sqlComando As String
    Private numFilas As Integer
    Private continuarguardando As Boolean

    ''' <summary>
    ''' Consulta la tabla RFSRGL con un determinado parametro de busqueda.Retorna un datatable
    ''' </summary>
    ''' <param name="ParametroBusqueda">Parametro de Busqueda</param>
    ''' <param name="TipoParametro">0- Busca por DNUMDOC(0)</param>
    ''' <returns>DataTable</returns>
    ''' <remarks>Autor: Jesús Santamaria Fecha: 13/04/2016</remarks>
    Public Function ConsultarRFSRGLdt(ByVal parametroBusqueda() As String, ByVal tipoParametro As Integer) As DataTable
        dt = Nothing
        conexion.Open()
        trans = conexion.BeginTransaction()
        Select Case tipoParametro
            Case 0
                sqlComando = "SELECT DCUENTA, Sum(CAST(DVALOR AS FLOAT)) AS VALOR, Sum(CAST(DVALORLOC AS FLOAT)) AS VALORLOC FROM RFSRGL WHERE DNUMDOC = " & parametroBusqueda(0) & " GROUP BY DCUENTA ORDER BY DCUENTA"
        End Select
        adapter = New SqlDataAdapter(sqlComando, conexion)
        adapter.MissingSchemaAction = MissingSchemaAction.AddWithKey
        adapter.SelectCommand.Transaction = trans
        builder = New SqlCommandBuilder(adapter)
        dt = New DataTable()
        ds = New DataSet()
        numFilas = adapter.Fill(ds)
        If numFilas > 0 Then
            dt = ds.Tables(0)
        Else
            dt = Nothing
        End If
        adapter = Nothing
        builder = Nothing
        trans.Commit()
        conexion.Close()
        Return dt
    End Function
End Class
