﻿Public Class clsMHT

    Public Modulo, IdMsg, Adjuntos, Asunto, De, Para, CC, url, local As String
    Public DB As clsConexion
    Private campoMail As String = "UEMAIL"
    Private mht As New Chilkat.Mht
    Private mailman As New Chilkat.MailMan
    Private email As New Chilkat.Email

    Function enviaMail() As String
        Dim resultado As String = ""
        Dim destBCC As String
        Dim campoParam As String = IIf(local = "SI", "CCNOT1", "")

        mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
        mht.UnlockComponent("SMANRIMHT_dEnsKQ0g3Rue")

        mailman.SmtpHost = DB.getLXParam("SMTPHOST", campoParam)
        If DB.getLXParam("SMTPAUTH", campoParam) <> "DIRECTO" Then
            mailman.SmtpUsername = DB.getLXParam("SMTPUSERNAME", campoParam)
            mailman.SmtpPassword = DB.getLXParam("SMTPPASSWORD", campoParam)
        End If
        resultado = correoMHT()
        If resultado = "OK" Then
            If Adjuntos.Trim <> "" Then
                If Not Adjuntar() Then
                    Return "Error adjuntando archivos."
                End If
            End If
            email.FromName = IIf(De <> "", De, "Alertas BRINSA")
            email.FromAddress = DB.getLXParam("SMTPUSERMAIL", "CCNOT1") '"admin.logistica@brinsa.com.co"
            destBCC = getBCC()
            Destinatarios("TO", Para)
            If CC.Trim <> "" Then
                Destinatarios("CC", CC)
            End If
            Destinatarios("BCC", destBCC)
            email.Subject = Asunto
            If mailman.SendEmail(email) Then
                resultado = "OK"
            Else
                DB.WrtSqlError(IdMsg, mailman.LastErrorHtml)
                DB.setLXParamNum("SMTPAUTH", "CCUDC2", -1)
                resultado = "Error en Envio"
            End If
            mailman.CloseSmtpConnection()
        End If
        Return resultado

    End Function

    Sub Destinatarios(Tipo As String, Correos As String)
        Dim aDest() As String = Split(Correos, ",")
        Dim rs As New ADODB.Recordset
        Dim fSql As String

        For Each dest In aDest

            If InStr(dest, "@") = 0 Then
                If InStr(dest.ToUpper.Trim, "SELECT ") = 0 Then
                    fSql = "SELECT UNOM, " & campoMail & " AS UEMAIL FROM RCAU WHERE UUSR = '" & dest.ToUpper.Trim & "'"
                Else
                    fSql = dest.ToUpper.Trim
                End If
                rs = DB.ExecuteSQL(fSql)
                If Not rs.EOF Then
                    Select Case Tipo
                        Case "TO"
                            email.AddTo(rs("UNOM").Value, rs("UEMAIL").Value)
                        Case "CC"
                            email.AddCC(rs("UNOM").Value, rs("UEMAIL").Value)
                        Case "BCC"
                            email.AddBcc(rs("UNOM").Value, rs("UEMAIL").Value)
                    End Select
                End If
                rs.Close()
            Else
                Select Case Tipo
                    Case "TO"
                        email.AddTo("", dest)
                    Case "CC"
                        email.AddCC("", dest)
                    Case "BCC"
                        email.AddBcc("", dest)
                End Select
            End If
        Next

    End Sub

    Function Adjuntar() As Boolean

        Dim aAdjuntos() As String = Split(Adjuntos, ",")
        For Each adjunto In aAdjuntos
            If email.AddFileAttachment(adjunto) = vbNullString Then
                DB.WrtSqlError(IdMsg, email.LastErrorHtml)
                Return False
            End If
        Next
        Return True

    End Function

    'correoMHT()	
    Function correoMHT() As String
        Dim emlStr As String

        mht.UseCids = 1
        emlStr = mht.GetEML(url)
        If (emlStr = vbNullString) Then
            DB.WrtSqlError(IdMsg, mht.LastErrorHtml)
            Return "Error en MHT."
        End If
        If Not (email.SetFromMimeText(emlStr)) Then
            DB.WrtSqlError(IdMsg, email.LastErrorHtml)
            Return "Error en MimeText"
        End If
        Return "OK"

    End Function

    Public Function getBCC() As String
        Dim rs As New ADODB.Recordset
        Dim resultado As String = ""

        campoMail = "UEMAIL"
        If DB.OpenRS(rs, "SELECT CCDESC, CCSDSC FROM RFPARAM WHERE CCTABL='CORREOMHT' AND UPPER(CCCODE) = UPPER('" & Modulo & "') AND CCUDC1 = 1") Then
            resultado = rs("CCDESC").Value
            If rs("CCSDSC").Value <> "" Then
                campoMail = rs("CCSDSC").Value
            End If
        End If
        rs.Close()
        Return resultado.Trim

    End Function

    Sub avisoDesborde(Archivo As String)
        Dim resultado As String = ""
        Dim campoParam As String = IIf(local = "SI", "CCNOT1", "")
        DB.gsKeyWords &= " avisoDesborde (1) " & Archivo & " " & DB.Estado()

        'CCUDC1
        resultado = DB.getLXParam("SMTPAUTH", "CCUDC1")
        DB.gsKeyWords = "avisoDesborde (2)"
        If CInt(resultado) < Hour(Now()) Then
            DB.gsKeyWords = "avisoDesborde (3)"
            DB.setLXParamNum("SMTPAUTH", "CCUDC1", Hour(Now()))
        Else
            Return
        End If
        DB.gsKeyWords = "avisoDesborde (4)"
        mailman.UnlockComponent("SMANRIMAILQ_ZEKrtWHSpOpZ")
        mht.UnlockComponent("SMANRIMHT_dEnsKQ0g3Rue")

        mailman.SmtpHost = DB.getLXParam("SMTPHOST", campoParam)
        If DB.getLXParam("SMTPAUTH", campoParam) <> "DIRECTO" Then
            mailman.SmtpUsername = DB.getLXParam("SMTPUSERNAME", campoParam)
            mailman.SmtpPassword = DB.getLXParam("SMTPPASSWORD", campoParam)
        End If
        DB.gsKeyWords = "avisoDesborde (5)"

        email.FromName = "Correo MHT BRINSA"
        email.FromAddress = DB.getLXParam("SMTPUSERMAIL", "CCNOT1") '"admin.logistica@brinsa.com.co"
        Destinatarios("TO", "BSS")
        email.Subject = Asunto
        email.Body = "Desbordamiento de archivo " & Archivo
        If mailman.SendEmail(email) Then
            resultado = "OK"
        Else
            DB.WrtSqlError(IdMsg, mailman.LastErrorHtml)
            DB.gsKeyWords = "avisoDesborde (6)"
            DB.setLXParamNum("SMTPAUTH", "CCUDC2", -1)
        End If
        mailman.CloseSmtpConnection()
    End Sub

    Sub Inicializa()
        Modulo = ""
        IdMsg = ""
        Adjuntos = ""
        Asunto = ""
        De = ""
        Para = ""
        CC = ""
        url = ""
        local = ""
    End Sub


End Class
