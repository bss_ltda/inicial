﻿Module modLogica
    Dim wsNum As String
    Dim wsResult As String
    Dim DB As New ADODB.Connection

    Sub revisaTareas()
        Dim wsNum As String
        Dim wsResult As String
        Dim DB As New ADODB.Connection

        'Para no capturar el error durante el debug poner False
        If False Then
            Try
                EjecutaProceso()
            Catch ex As Exception
                'WrtSqlError(lastSQL, ex.Message & "<br />" & ex.StackTrace.Replace(" en ", "<br />"))
            End Try
        Else
            EjecutaProceso()
        End If
    End Sub

    Sub EjecutaProceso()
        Dim fSql As String
        Dim pgm, param, exe As String
        Dim rs As New ADODB.Recordset
        Dim sts As Integer
        Dim msgT As String
        Dim tLXmsg As String
        Dim Accion As String
        Dim basesDatos() As String

        basesDatos = My.Settings.BASESDEDATOS.Split(",")

        DB.Open(My.Settings.BASEDATOS)
        For i = 0 To basesDatos.Length - 1
            'Try
            fSql = "USE " & basesDatos(i) & " SELECT * FROM  RFTASK WHERE STS1 = 0 AND THLD = 0 AND TFPROXIMA <= getDate() "
            rs.Open(fSql, DB)
            Do While Not rs.EOF
                Accion = UCase(rs("TTIPO").Value)
                If rs("FWRKID").Value <> 0 Then
                    If Accion <> "ULTIMO" Then
                        Accion = "PAQUETE"
                    End If
                Else
                    Accion = UCase(rs("TTIPO").Value)
                End If

                Select Case Accion
                    Case UCase("Shell")
                        pgm = rs("TPGM").Value
                        param = rs("TPRM").Value
                        exe = pgm & " " & param.Replace("|", " ")
                        sts = 1
                        msgT = "Comando Enviado."
                        tLXmsg = "EN PROCESO"
                        fSql = "USE " & basesDatos(i) & " UPDATE RFTASK SET "
                        fSql = fSql & " TLXMSG = '" & tLXmsg & "', "
                        fSql = fSql & " STS1 = " & sts & ", "
                        fSql = fSql & " TUPDDAT = NOW() , "
                        fSql = fSql & " TMSG = '[" & exe & "]" & msgT & "'"
                        fSql = fSql & " WHERE TNUMREG = " & rs("TNUMREG").Value
                        DB.Execute(fSql)
                        Try
                            Shell(exe, AppWinStyle.Hide, False, -1)
                        Catch ex As Exception
                            sts = -1
                            msgT = ex.Message.Replace("'", "''")
                            tLXmsg = "ERROR"
                            fSql = "USE " & basesDatos(i) & " UPDATE RFTASK SET "
                            fSql = fSql & " TLXMSG = '" & tLXmsg & "', "
                            fSql = fSql & " STS1 = " & sts & ", "
                            fSql = fSql & " TUPDDAT = NOW() , "
                            fSql = fSql & " TMSG = '[" & exe & "]" & msgT & "'"
                            fSql = fSql & " WHERE TNUMREG = " & rs("TNUMREG").Value
                            DB.Execute(fSql)
                        End Try
                    Case UCase("Ultimo")
                        exe = "ImprimeDocumentos.exe " & rs("FWRKID").Value
                        sts = 1
                        msgT = "Paquete Enviado."
                        tLXmsg = "ENVIADO"
                        fSql = "USE " & basesDatos(i) & " UPDATE RFTASK SET "
                        fSql = fSql & " TLXMSG = '" & tLXmsg & "', "
                        fSql = fSql & " STS1 = " & sts & ", "
                        fSql = fSql & " TUPDDAT = NOW() , "
                        fSql = fSql & " TMSG = '" & msgT & "'"
                        fSql = fSql & " WHERE TNUMREG = " & rs("TNUMREG").Value
                        DB.Execute(fSql)
                        Try
                            Shell(exe, AppWinStyle.Hide, False, -1)
                        Catch ex As Exception
                            msgT = ex.Message.Replace("'", "''")
                            WrtSqlError(exe, msgT)
                        End Try
                    Case UCase("Rutina")
                        pgm = rs("TPGM").Value
                        'Ejecutable de rutinas con parametro TNUMREG de RFTASK
                        exe = pgm & " " & rs("TNUMREG").Value
                        sts = 1
                        msgT = "Comando Enviado."
                        tLXmsg = "EJECUTADA"
                        Try
                            Shell(exe, AppWinStyle.Hide, False, -1)
                        Catch ex As Exception
                            sts = -1
                            msgT = ex.Message.Replace("'", "''")
                            tLXmsg = "ERROR"
                        End Try
                        fSql = "USE " & basesDatos(i) & " UPDATE RFTASK SET "
                        fSql = fSql & " TLXMSG = '" & tLXmsg & "', "
                        fSql = fSql & " STS1 = " & sts & ", "
                        fSql = fSql & " TUPDDAT = NOW() , "
                        fSql = fSql & " TMSG = '" & msgT & "'"
                        fSql = fSql & " WHERE TNUMREG = " & rs("TNUMREG").Value
                        DB.Execute(fSql)
                    Case UCase("Java")

                End Select
                rs.MoveNext()
            Loop
            rs.Close()
            DB.Close()
            'Catch ex As Exception
            '    sts = -1
            '    msgT = ex.Message.Replace("'", "''")
            '    tLXmsg = "ERROR"
            '    fSql = " UPDATE RFTASK SET "
            '    fSql = fSql & " TLXMSG = '" & tLXmsg & "', "
            '    fSql = fSql & " STS1 = " & sts & ", "
            '    fSql = fSql & " TUPDDAT = NOW() , "
            '    fSql = fSql & " TMSG = '" & msgT & "'"
            '    fSql = fSql & " WHERE TNUMREG = " & rs("TNUMREG").Value
            '    DB.Execute(fSql)
            'End Try
        Next
    End Sub

    Sub EjecutaProcedimiento()
        'Dim conn As New SqlConnection(My.Settings.BASEDATOS_ADO)
        'conn.Open()

        'Dim adaptador As SqlDataAdapter = New SqlDataAdapter()
        'Dim sql As SqlCommand = New SqlCommand("select * from Eventos where Estado='Enviado'", conn)
        'adaptador.SelectCommand = sql

        'sql = New SqlCommand("SP_CarteraSubirArchivos", conn)
        'sql.CommandTimeout = 0
        'sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
        'sql.CommandType = CommandType.StoredProcedure
        'sql.ExecuteNonQuery()


        'Dim tareas As DataSet = New DataSet
        'adaptador.Fill(tareas, "Eventos")
        'Dim dr As DataRow
        'For Each dr In tareas.Tables("Eventos").Rows
        '    fSql = "UPDATE Eventos SET Estado=@Estado Where id=@Trabajo"
        '    sql = New SqlCommand(fSql, conn)
        '    sql.Parameters.AddWithValue("@Estado", "Iniciado")
        '    sql.Parameters.AddWithValue("@Trabajo", dr("ID").ToString())
        '    Dim r As Integer = sql.ExecuteNonQuery()
        '    Dim aParams() As String = Split(dr("Parametros").ToString.Trim(), ",")
        '    Select Case dr("Macro").ToString().Trim()
        '        Case "Cartera"
        '        Case "Datacredito"
        '            sql = New SqlCommand("SP_DataCreditoSubirArchivos", conn)
        '            sql.CommandTimeout = 0
        '            sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
        '            sql.CommandType = CommandType.StoredProcedure
        '            sql.ExecuteNonQuery()
        '        Case "DatacreditoHistorico"
        '            sql = New SqlCommand("SP_DataCreditoSubirArchivos2", conn)
        '            sql.CommandTimeout = 0
        '            sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
        '            sql.CommandType = CommandType.StoredProcedure
        '            sql.ExecuteNonQuery()
        '        Case "Logistica1"
        '            sql = New SqlCommand("SP_LogisticaCargueArchivo1", conn)
        '            sql.CommandTimeout = 0
        '            sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
        '            sql.CommandType = CommandType.StoredProcedure
        '            sql.ExecuteNonQuery()
        '        Case "Logistica2"
        '            sql = New SqlCommand("SP_LogisticaCargueArchivo2", conn)
        '            sql.CommandTimeout = 0
        '            sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
        '            sql.CommandType = CommandType.StoredProcedure
        '            sql.ExecuteNonQuery()
        '        Case "Logistica3"
        '            sql = New SqlCommand("SP_LogisticaCargueArchivo3", conn)
        '            sql.CommandTimeout = 0
        '            sql.Parameters.Add("@id", SqlDbType.BigInt).Value = dr("id")
        '            sql.CommandType = CommandType.StoredProcedure
        '            sql.ExecuteNonQuery()
        '    End Select
        '    sql = New SqlCommand(fSql, conn)
        '    sql.Parameters.AddWithValue("@Estado", "Terminado")
        '    sql.Parameters.AddWithValue("@Trabajo", dr("ID").ToString())
        '    r = sql.ExecuteNonQuery()
        'Next

    End Sub

    Sub WrtSqlError(sql As String, Descrip As String)
        Dim conn As New ADODB.Connection
        Dim fSql As String
        Dim vSql As String

        Dim gsConnString As String = "Provider=IBMDA400.DataSource;Data Source=192.168.10.1;User ID=APLLX;Password=LXAPL;Persist Security Info=True;Default Collection=ERPLX834F;Force Translate=0;"

        Try
            conn.Open(gsConnString)
            fSql = " INSERT INTO RFLOG( "
            vSql = " VALUES ( "
            fSql = fSql & " USUARIO   , " : vSql = vSql & "'" & System.Net.Dns.GetHostName() & "', "      '//502A
            fSql = fSql & " PROGRAMA  , " : vSql = vSql & "'" & System.Reflection.Assembly.GetExecutingAssembly.FullName & "', "      '//502A
            fSql = fSql & " ALERT     , " : vSql = vSql & "1, "      '//1P0
            fSql = fSql & " EVENTO    , " : vSql = vSql & "'" & Replace(Descrip, "'", "''") & "', "      '//20002A
            fSql = fSql & " TXTSQL    ) " : vSql = vSql & "'" & Replace(sql, "'", "''") & "' ) "      '//5002A
            conn.Execute(fSql & vSql)
            conn.Close()
        Catch ex As Exception

        End Try

    End Sub
End Module
