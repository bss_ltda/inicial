﻿Public Class wsCorreoMHT
    Private WithEvents m_timer As New Timers.Timer(30000)
    Private DB As clsConexion
    Private Correo As New clsMHT
    Public sEstado As String = ""

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Agregue el código aquí para iniciar el servicio. Este método debería poner
        ' en movimiento los elementos para que el servicio pueda funcionar.
        Me.EventLog1.WriteEntry("Inicia")
        m_timer.Enabled = True

    End Sub

    Private Sub m_timer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles m_timer.Elapsed

        Try
            m_timer.Enabled = False
            If Not AbreConexion("") Then
                Exit Sub
            End If
            If AvisoLog() Then
                revisaTareasMHT()
            End If
            DB.Close()
        Catch ex As Exception
            DB.WrtSqlError(DB.lastSQL, ex.StackTrace & "<br />" & ex.Message)
            Me.EventLog1.WriteEntry(DB.lastSQL & vbCrLf & ex.Message & vbCrLf & ex.StackTrace)
        End Try
        m_timer.Enabled = True

    End Sub

    Sub revisaTareasMHT()
        Dim fSql As String
        Dim rs As New ADODB.Recordset
        Dim serverMHT As String
        Dim local As String = IIf(My.Settings.LOCAL.ToUpper = "SI", "CCNOT1", "")
        Dim resultado As String = ""
        Dim basesDatos() As String

        If Not AbreConexion("") Then
            Exit Sub
        End If

        DB.gsKeyWords = "revisaTareasMHT (1)"
        serverMHT = DB.getLXParam("SMTPMHTSERVER", local)

        basesDatos = My.Settings.BASESDEDATOS.Split(",")

        'For i = 0 To basesDatos.Length - 1
        fSql = " SELECT COUNT(*) AS TOT FROM  RMAILX WHERE LENVIADO = 0"
        If DB.OpenRS(rs, fSql) Then
            DB.gsKeyWords = "revisaTareasMHT (2)"
            If rs("TOT").Value > 50 Then
                DB.gsKeyWords = "revisaTareasMHT (3)"
                With Correo
                    .Inicializa()
                    .DB = DB
                    DB.gsKeyWords = "revisaTareasMHT (4)"
                    .avisoDesborde("RMAILX")
                    DB.gsKeyWords = "Sale de avisoDesborde"
                End With
                rs.Close()
                Me.EventLog1.WriteEntry("Mas de 50 Correos Pendientes.")
                Return
            End If
        End If
        rs.Close()

        fSql = " SELECT * FROM  RMAILX WHERE LENVIADO = 0"
        DB.OpenRS(rs, fSql)
        Do While Not rs.EOF
            DB.gsKeyWords = "revisaTareasMHT (5)"
            DB.gsKeyWords = "revisaTareasMHT, LID = " & rs("LID").Value
            fSql = " UPDATE RMAILX SET  "
            fSql = fSql & " LUPDDAT = NOW() , "
            fSql = fSql & " LENVIADO = -1,  "
            fSql = fSql & " LMSGERR = 'En proceso'  "
            fSql = fSql & " WHERE LID = " & rs("LID").Value
            DB.ExecuteSQL(fSql)

            With Correo
                .Inicializa()
                .DB = DB
                If Not rs("LMOD").Value Is DBNull.Value Then
                    .Modulo = rs("LMOD").Value
                Else
                    .Modulo = ""
                End If
                .Asunto = rs("LASUNTO").Value
                If Not rs("LENVIA").Value Is DBNull.Value Then
                    If Not rs("LREF").Value Is DBNull.Value Then
                        .De = IIf(rs("LENVIA").Value = "", rs("LREF").Value, rs("LENVIA").Value)
                    Else
                        .De = rs("LENVIA").Value
                    End If
                Else
                    If Not rs("LREF").Value Is DBNull.Value Then
                        .De = rs("LREF").Value
                    Else
                        .De = ""
                    End If
                End If
                .Para = rs("LPARA").Value
                If Not rs("LCOPIA").Value Is DBNull.Value Then
                    .CC = rs("LCOPIA").Value
                Else
                    .CC = ""
                End If

                .url = rs("LCUERPO").Value

                If InStr(.url.ToUpper, "HREF") <> 0 Then
                    .url = serverMHT & DB.getLXParam("SMTPAVISOGRAL", local) & "?LID=" & rs("LID").Value
                Else
                    If InStr(.url.ToUpper, "HTTP") = 0 Then
                        If InStr(.url.ToUpper, ".ASP") = 0 Then
                            .url = serverMHT & DB.getLXParam("SMTPAVISOGRAL", local) & "?LID=" & rs("LID").Value
                        Else
                            .url = serverMHT & .url
                        End If
                    End If
                End If

                If Not rs("LADJUNTOS").Value Is DBNull.Value Then
                    .Adjuntos = rs("LADJUNTOS").Value
                Else
                    .Adjuntos = ""
                End If

                .local = My.Settings.LOCAL.ToUpper
                resultado = .enviaMail()
                If (resultado = "OK") Then
                    fSql = " UPDATE RMAILX SET  "
                    fSql = fSql & " LUPDDAT = NOW() , "
                    fSql = fSql & " LENVIADO = 1,  "
                    fSql = fSql & " LMSGERR = 'OK'  "
                    fSql = fSql & " WHERE LID = " & rs("LID").Value
                Else
                    fSql = " UPDATE RMAILX SET  "
                    fSql = fSql & " LUPDDAT = NOW() , "
                    fSql = fSql & " LENVIADO = -1,  "
                    fSql = fSql & " LMSGERR = '" & resultado & "'  "
                    fSql = fSql & " WHERE LID = " & rs("LID").Value
                End If
                DB.ExecuteSQL(fSql)
            End With
            rs.MoveNext()
        Loop
        rs.Close()
        'Next
    End Sub

    Function AvisoLog() As Boolean
        Dim fSql As String
        Dim rs As New ADODB.Recordset
        Dim serverMHT As String
        Dim local As String = IIf(My.Settings.LOCAL.ToUpper = "SI", "CCNOT1", "")
        Dim resultado As String = ""

        DB.gsKeyWords = "AvisoLog"
        serverMHT = DB.getLXParam("SMTPMHTSERVER", local)
        fSql = " SELECT COUNT(*) AS TOT FROM RFLOG WHERE ALERT = 1 "
        If DB.OpenRS(rs, fSql) Then
            If rs("TOT").Value > 50 Then
                With Correo
                    .Inicializa()
                    .DB = DB
                    .avisoDesborde("RFLOG")
                End With
                rs.Close()
                Me.EventLog1.WriteEntry("Mas de 50 Correos Pendientes.")
                Return False
            End If
        End If
        rs.Close()

        fSql = " SELECT * FROM  RFLOG WHERE ALERT = 1 "
        DB.OpenRS(rs, fSql)
        Do While Not rs.EOF
            With Correo
                .Inicializa()
                .DB = DB
                .Modulo = "PGMERR"
                .Asunto = rs("PROGRAMA").Value
                .De = "Error en Programa"
                If Not rs("ALERTTO").Value Is DBNull.Value Then
                    .Para = "BSS" & IIf(rs("ALERTTO").Value <> "", "," & rs("ALERTTO").Value, "")
                Else
                    .Para = "BSS"
                End If

                .url = serverMHT & DB.getLXParam("SMTPAVISOERROR", "") & "?ID=" & rs("ID").Value
                .local = My.Settings.LOCAL.ToUpper
                resultado = .enviaMail()
                If (resultado = "OK") Then
                    fSql = " UPDATE RFLOG SET ALERT = 2"
                    fSql = fSql & " WHERE ID = " & rs("ID").Value
                Else
                    fSql = " UPDATE RFLOG SET ALERT = -1"
                    fSql = fSql & " WHERE ID = " & rs("ID").Value
                End If
                DB.ExecuteSQL(fSql)
            End With
            rs.MoveNext()
        Loop
        rs.Close()
        Return True

    End Function

    Function AbreConexion(ModuloActual As String) As Boolean

        DB = New clsConexion
        DB.gsDatasource = My.Settings.AS400
        DB.gsLib = My.Settings.AMBIENTE
        DB.ModuloActual = ModuloActual
        Return DB.Conexion()

    End Function

    Protected Overrides Sub OnStop()
        ' Agregue el código aquí para realizar cualquier anulación necesaria para detener el servicio.
        m_timer.Stop()
    End Sub


 
End Class
