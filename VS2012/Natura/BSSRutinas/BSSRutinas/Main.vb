﻿Imports System.Xml.Serialization
Imports System.IO
Imports BSSRutinas.modConstantes

Module Principal

    Private DB As clsConexion

    Sub Main(ByVal Parametros As String())
        Dim i As Integer
        Dim fSql As String
        Dim rs As New ADODB.Recordset
        Dim resultado As String = ""
        Dim msgTask As String = ""

        Try
            If Not AbreConexion(Parametros(0)) Then
                Exit Sub
            End If
            fSql = " SELECT * FROM  RFTASK WHERE TNUMREG = " & Parametros(0)
            If DB.OpenRS(rs, fSql) Then
                Console.WriteLine(rs("TCATEG").Value)
                For i = 0 To UBound(Parametros)
                    DB.gsKeyWords &= Parametros(i) & " "
                Next

                DB.gsKeyWords &= " " & rs("TCATEG").Value & " " & rs("TPGM").Value

                Select Case (rs("TCATEG").Value)
                    Case "RECC"
                        msgTask = ActualizaRECC(rs("TKEY").Value)
                    Case "INDPROD"
                        IndicesProduccion(Parametros(1), Parametros(2))
                    Case "REP_DESTINOSEGURO"
                        Dim rep As New ReporteDestinoSeguro
                        rep.DB = DB
                        rep.Consolidado = rs("TPRM").Value
                        resultado = rep.reporte()
                        msgTask = resultado
                    Case "BODSAT_SALDO"
                        DB.ExecuteSQL(fSql)
                    Case Else
                        resultado = "Rutina no parametrizada"
                End Select
                fSql = " UPDATE RFTASK SET "
                fSql = fSql & " STS1 = " & IIf(resultado <> "", -1, 1) & ", STS2 = 1, "
                fSql = fSql & " TUPDDAT = NOW() , "
                fSql = fSql & " TMSG = '" & Trim(resultado & " " & msgTask) & "'"
                fSql = fSql & " WHERE TNUMREG = " & rs("TNUMREG").Value
                DB.ExecuteSQL(fSql)
            End If
            rs.Close()
            DB.Close()
        Catch ex As Exception
            DB.WrtSqlError(DB.lastSQL, ex.ToString)
        End Try


    End Sub

    Function ActualizaRECC(Conso As String) As String
        Dim RECC As New clsEntregasConso

        DB.gsKeyWords = Conso
        RECC.Consolidado = Conso
        RECC.DB = DB
        RECC.ActualizaRECC()
        Return "ACTUALIZADO"

    End Function


    Function AbreConexion(ModuloActual As String) As Boolean

        DB = New clsConexion
        DB.gsDatasource = My.Settings.AS400
        DB.gsLib = My.Settings.AMBIENTE
        DB.ModuloActual = ModuloActual
        Return DB.Conexion()

    End Function

    Sub comandoEjecutado(ByVal rutina As String, ByVal msg As String)
        Dim fSql As String

        fSql = " UPDATE ZCC SET CCUDC1 = 0"
        fSql = fSql & ", CCNOT1 = 'Finalizó " & Now.ToString("yyyy-MM-dd HH:mm:ss") & "' "
        fSql = fSql & ", CCNOT2 = '" & Left(msg, 30) & "'  "
        fSql = fSql & " WHERE CCTABL = 'RUTINAS'  AND CCCODE = '" & UCase(rutina) & "' "
        DB.ExecuteSQL(fSql)

    End Sub


End Module
