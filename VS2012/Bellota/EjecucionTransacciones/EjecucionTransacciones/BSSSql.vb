﻿

Imports System.IO
Imports MySql.Data.MySqlClient

Module BSSSql


    Public conn As New ADODB.Connection
    Public local As String
    Public gsAS400 As String = "192.168.40.10"
    Public gsLib As String = ""
    Public sUser As String
    Public sUserApl As String
    Public sUserPass As String
    Public bBatchApp As Boolean = False
    Public nomApp As String = "VB.NET." & My.Application.Info.ProductName & "." & _
                               My.Application.Info.Version.Major & "." & _
                               My.Application.Info.Version.Minor & "." & _
                               My.Application.Info.Version.Revision
    Public Sentencia As String
    Public bLog As Boolean = False
    Public iNumLog As Integer
    Public msgErr As String
    Public gsConnString As String
    Public rAfectados As Integer
    Public Descripcion_Error As String

    Public DB As New clsConexion
    Public DBMySql As New clsConexion
    Public rs As New ADODB.Recordset
    Public fSql As String
    Public vSql As String
    Public lastSQL As String = ""
    Public bSalto
    Public esInsert
    Public esUpdate
    Public esFin



    Function paramWebConfig(webConfig, param)
        Dim oXML, oNode, oChild, oAttr

        oXML = CreateObject("Microsoft.XMLDOM")
        oXML.Async = "false"
        oXML.Load(webConfig)
        oNode = oXML.GetElementsByTagName("appSettings").Item(0)
        oChild = oNode.GetElementsByTagName("add")
        For Each oAttr In oChild
            If UCase(oAttr.getAttribute("key")) = UCase(param) Then
                Return oAttr.getAttribute("value")
            End If
        Next
        Return ""

    End Function

    Function paramWebConfig400(param)

        Dim Valor As String
        Dim rs As New ADODB.Recordset
        fSql = "SELECT CCDESC FROM BFPARAM WHERE CCCODE = '" & param & "' "
        If DBMySql.OpenRS(rs, fSql) Then
            Valor = rs("CCDESC").Value
            rs.Close()
            Return Valor
        Else
            rs.Close()
            Return ""
        End If
    End Function

    Sub mkSql_h(sep)
        mkSql(sep, "", "", 0)
    End Sub

    Sub mkSql_f(sep)
        mkSql(sep, "", "", 0)
    End Sub


    Sub mkSql_d(sep, Campo, valor, Fin)
        mkSql(sep, Campo, valor, Fin)
    End Sub

    Sub mkSql(sep, Campo, valor, Fin)

        If Campo = "" Then
            If InStr(sep, "INSERT INTO") > 0 Then
                esInsert = True
                fSql = sep & IIf(bSalto, vbCrLf, "")
            ElseIf InStr(sep, "VALUES") Then
                vSql = sep & IIf(bSalto, vbCrLf, "")
            ElseIf InStr(sep, "UPDATE") Then
                fSql = sep & IIf(bSalto, vbCrLf, "")
                vSql = ""
                esUpdate = True
                esInsert = False
            ElseIf esFin Then
                vSql = vSql & " " & sep & IIf(bSalto, vbCrLf, "")
            ElseIf esInsert Then
                vSql = sep
            Else
                fSql = sep
            End If
        Else
            sep = Trim(sep)
            If esInsert Then
                fSql = fSql & Campo
                Select Case Fin
                    Case 0
                        fSql = fSql & " , "
                    Case 10, 11, -1, 1
                        fSql = fSql & " ) "
                End Select
                If valor = Nothing Then
                    valor = IIf(sep = "'", "", "0")
                End If
                vSql = vSql & sep & CStr(valor) & sep
                Select Case Fin
                    Case 10
                        vSql = vSql & "  "
                    Case 11, -1, 1
                        vSql = vSql & " )"
                    Case 0
                        vSql = vSql & " , "
                End Select
            Else
                If valor = Nothing Then
                    valor = IIf(sep = "", "", "0")
                End If
                fSql = fSql & Campo & " = " & sep & CStr(valor) & sep & IIf(Fin = 0, ", ", "")
            End If
            esFin = Fin <> 0
            If bSalto Then
                fSql = fSql & vbCrLf
                If vSql <> "" Then
                    vSql = vSql & vbCrLf
                End If
            End If
        End If

    End Sub

    Function v_fSql()
        v_fSql = fSql
    End Function

    Function v_Sql()
        v_Sql = vSql
    End Function

    Sub salto(v)
        bSalto = v
    End Sub

    Function DB2_DATEDEC()
        DB2_DATEDEC = " ( YEAR( NOW() ) * 10000 + MONTH( NOW()  ) * 100 + DAY( NOW() ) )"
    End Function
    Function DB2_TIMEDEC()
        DB2_TIMEDEC = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    End Function
    Function DB2_TIMEDEC0()
        DB2_TIMEDEC0 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    End Function
    Function DB2_TIMEDEC1()
        DB2_TIMEDEC1 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 ) "
    End Function


End Module
