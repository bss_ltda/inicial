﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Xml.Serialization
Imports System.Xml
Imports System.IO


<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class ServiciosBatch
    Inherits System.Web.Services.WebService

    Dim c_ID As String
    Dim resultConn As String

    Structure TrasladoBatchEstado
        Dim batch As Integer
        Dim estado As Integer
        Dim estadoDescripcion As String
        Dim lineasTotal As Integer
        Dim lineasEnviadasWs As Integer
        Dim lineasEnviadasCIM600 As Integer
        Dim lineasBPCS As Integer
        Dim lineasConNovedad As List(Of novedadLineaTraslado)
    End Structure

    Structure novedadLineaTraslado
        Dim linea As Integer
        Dim codMensaje As Integer
        Dim mensaje As String
        Dim Producto As String
        Dim Lote As String
        Dim Bodega As String
        Dim Ubicacion As String
        Dim Cantidad As Double
        Dim OrdenFabricacion As Integer
        Dim CodigoCausa As String
    End Structure

    Structure Traslado
        Dim Fecha As String
        Dim Usuario As String
        Dim Lineas As List(Of TrasladoDet)
    End Structure

    Structure TrasladoDet
        Dim Linea As Double
        Dim Producto As String
        Dim Lote As String
        Dim Bodega_Desde As String
        Dim Ubicacion_Desde As String
        Dim Bodega_Hacia As String
        Dim Ubicacion_Hacia As String
        Dim Cantidad As Double
        Dim OrdenFabricacion As Integer
        Dim CodigoCausa As String
    End Structure

    Structure TrasladoBatchRespuesta
        Dim batch As Integer
        Dim estado As Integer
        Dim estadoDescripcion As String
        Dim lineasTotal As Integer
    End Structure

    Structure Asignacion
        Dim Usuario As String
        Dim Fecha As Date
        Dim Pedidos As List(Of DespacharPedido)
    End Structure

    Structure DespacharPedido
        Dim Pedido As Integer
        Dim LineasPedido As List(Of LineaPedido)
    End Structure

    Structure LineaPedido
        Dim Linea As Double
        Dim Producto As String
        Dim Bodega As String
        Dim Ubicacion As String
        Dim Lote As String
        Dim Cantidad As Double
    End Structure

    Structure AsignacionBatchRespuesta
        Dim batch As Integer
        Dim estado As Integer
        Dim estadoDescripcion As String
        Dim pedidosTotal As Integer
    End Structure

    Structure AsignacionBatchEstado
        Dim batch As Integer
        Dim estado As Integer
        Dim estadoDescripcion As String
        Dim pedidosTotal As Integer
        Dim pedidosProcesados As Integer
        Dim pedidosConError As Integer
        Dim pedidosConNovedad As List(Of novedadPedidoAsignacion)
    End Structure

    Structure novedadPedidoAsignacion
        
        Dim codMensaje As Integer
        Dim mensaje As String
        Dim Pedido As String
        Dim Linea As Integer
        Dim Producto As String
        Dim Bodega As String
        Dim Ubicacion As String
        Dim Lote As String
        Dim Cantidad As Double
    End Structure


    <WebMethod()> _
    Public Function TrasladosBatch(ByVal xml As String) As TrasladoBatchRespuesta
        Dim docEntrada As XDocument = XDocument.Parse(xml)
        Dim err As New TrasladoBatchRespuesta

        If Not AbreConexion(docEntrada.ToString) Then
            err.estado = -1
            err.estadoDescripcion = resultConn
            err.lineasTotal = 0
            Return err
        End If

        Try
            Dim result As String = ""
            Dim itH As Traslado
            Dim itL As New TrasladoDet
            Dim Lineas As New List(Of TrasladoDet)
            Dim qx = From xe In docEntrada.Elements("TrasladoBatch") Select New With { _
                .Fecha = xe.Element("Fecha").Value,
                .Usuario = xe.Element("Usuario").Value
            }

            '   .Batch = xe.Element("Batch").Value,

            itH = New Traslado
            With itH
                .Fecha = qx.First.Fecha
                .Usuario = qx.First.Usuario
                .Lineas = New List(Of TrasladoDet)
            End With

            Dim qd = From xe In docEntrada.Descendants.Elements("Traslado") Select New With { _
                .Linea = xe.Element("Linea").Value,
                .Producto = xe.Element("Producto").Value,
                .Lote = xe.Element("Lote").Value,
                .Bodega_Desde = xe.Element("Bodega_Desde").Value,
                .Ubicacion_Desde = xe.Element("Ubicacion_Desde").Value,
                .Bodega_Hacia = xe.Element("Bodega_Hacia").Value,
                .Ubicacion_Hacia = xe.Element("Ubicacion_Hacia").Value,
                .Cantidad = xe.Element("Cantidad").Value,
                .OrdenFabricacion = xe.Element("OrdenFabricacion").Value,
                .CodigoCausa = xe.Element("CodigoCausa").Value
            }

            For Each e In qd
                itL = New TrasladoDet
                With itL
                    .Linea = e.Linea
                    .Producto = e.Producto
                    .Lote = e.Lote
                    .Bodega_Desde = e.Bodega_Desde
                    .Ubicacion_Desde = e.Ubicacion_Desde
                    .Bodega_Hacia = e.Bodega_Hacia
                    .Ubicacion_Hacia = e.Ubicacion_Hacia
                    .Cantidad = e.Cantidad
                    .OrdenFabricacion = e.OrdenFabricacion
                    .CodigoCausa = e.CodigoCausa
                End With
                itH.Lineas.Add(itL)
            Next
             result &= "[" & itH.Fecha & "]"
            result &= "[" & itH.Usuario & "]"
            Dim cons
            Dim batch
            Dim canti1
            batch = DBMySql.CalcConsec("CIMNUM", "99999999")

            For Each e In itH.Lineas
                With e
    
                    cons = DBMySql.CalcConsec("CIMNUM", "99999999")
                    mkSql_h(" INSERT INTO BFCIMWS ( ")
                    mkSql_h(" VALUES ( ")
                    mkSql_d("'", " IFUENTE", "DESPACHOS", 0)                       '//15A
                    mkSql_d(" ", " IBATCH ", batch, 0)                          '//8
                    mkSql_d(" ", " INLINE ", e.Linea, 0)                          '//8
                    mkSql_d("'", " INID   ", "IN", 0)                           '//2A    Record Identifier
                    mkSql_d("'", " INTRAN ", "T1", 0)                           '//2A    Transaction Type
                    mkSql_d(" ", " INCTNR ", 0, 0)                              '//2A    Transaction Type
                    mkSql_d("'", " INMFGR ", 0, 0)                              '//2A    Transaction Type
                    mkSql_d(" ", " INDATE ", Replace(itH.Fecha, "-", ""), 0)                          '//8P0   Date		
                    mkSql_d(" ", " INTRN  ", Right(cons, 5), 0)                 '//5P0   INTRN
                    mkSql_d("'", " INMLOT  ", "DE" & cons, 0)                   '//5P0   INTRN
                    mkSql_d("'", " INREAS ", e.CodigoCausa, 0)                           '//5P0   INTRN
                    mkSql_d(" ", " INREF  ", e.OrdenFabricacion, 0)            '//8P0   Date
                    mkSql_d("'", " INPROD ", e.Producto, 0)           '//15A   Item Number
                    mkSql_d("'", " INWHSE ", e.Bodega_Desde, 0)             '//2A    Warehouse
                    mkSql_d("'", " INLOT  ", e.Lote, 0)                             '//10A   Lot Number
                    mkSql_d("'", " INLOC  ", e.Ubicacion_Desde, 0)                             '//6A    Location Code

                    canti1 = -(CDbl(e.Cantidad))
                    mkSql_d("'", " INQTY  ", canti1, 0)                         '//10A   Creted by
                    mkSql_d("'", " INCBY  ", itH.Usuario, 0)                     '//10A   Creted by
                    mkSql_d(" ", " INLDT  ", Format(Now(), "yyyyMMdd"), 0)      '//8P0
                    mkSql_d(" ", " INLTM  ", Format(Now(), "hhmmss"), 0)        '//6P0
                    mkSql_d(" ", " INCDT  ", DB2_DATEDEC(), 0)                  '//8P0   Date Created
                    mkSql_d(" ", " INCTM  ", DB2_TIMEDEC(), 1)                  '//6P0   Time Created
                    DBMySql.ExecuteSQL(fSql & vSql)



                    cons = DBMySql.CalcConsec("CIMNUM", "99999999")
                    mkSql_h(" INSERT INTO BFCIMWS ( ")
                    mkSql_h(" VALUES ( ")
                    mkSql_d("'", " IFUENTE", "DESPACHOS", 0)                       '//15A
                    mkSql_d(" ", " IBATCH ", batch, 0)                          '//8
                    mkSql_d(" ", " INLINE ", e.Linea, 0)                          '//8
                    mkSql_d("'", " INID   ", "IN", 0)                           '//2A    Record Identifier
                    mkSql_d("'", " INTRAN ", "T1", 0)                           '//2A    Transaction Type
                    mkSql_d(" ", " INCTNR ", 0, 0)                              '//2A    Transaction Type
                    mkSql_d("'", " INMFGR ", 0, 0)                              '//2A    Transaction Type
                    mkSql_d(" ", " INDATE ", Replace(itH.Fecha, "-", ""), 0)                          '//8P0   Date		
                    mkSql_d(" ", " INTRN  ", Right(cons, 5), 0)                 '//5P0   INTRN
                    mkSql_d("'", " INMLOT  ", "DE" & cons, 0)                   '//5P0   INTRN
                    mkSql_d("'", " INREAS ", e.CodigoCausa, 0)                           '//5P0   INTRN
                    mkSql_d(" ", " INREF  ", e.OrdenFabricacion, 0)            '//8P0   Date
                    mkSql_d("'", " INPROD ", e.Producto, 0)           '//15A   Item Number
                    mkSql_d("'", " INWHSE ", e.Bodega_Hacia, 0)             '//2A    Warehouse
                    mkSql_d("'", " INLOT  ", e.Lote, 0)                             '//10A   Lot Number
                    mkSql_d("'", " INLOC  ", e.Ubicacion_Hacia, 0)                             '//6A    Location Code

                    canti1 = CDbl(e.Cantidad)
                    mkSql_d("'", " INQTY  ", canti1, 0)                         '//10A   Creted by
                    mkSql_d("'", " INCBY  ", itH.Usuario, 0)                     '//10A   Creted by
                    mkSql_d(" ", " INLDT  ", Format(Now(), "yyyyMMdd"), 0)      '//8P0
                    mkSql_d(" ", " INLTM  ", Format(Now(), "hhmmss"), 0)        '//6P0
                    mkSql_d(" ", " INCDT  ", DB2_DATEDEC(), 0)                  '//8P0   Date Created
                    mkSql_d(" ", " INCTM  ", DB2_TIMEDEC(), 1)                  '//6P0   Time Created
                    DBMySql.ExecuteSQL(fSql & vSql)
                End With
            Next


            err.batch = batch
            err.estado = 1
            err.estadoDescripcion = "1. Recibido, 1 Sin Errores " & itH.Fecha
            err.lineasTotal = itH.Lineas.Count
            Return err

        Catch ex As Exception
            err.estado = -1
            err.estadoDescripcion = "1. Recibido, -1 Con Errores <A>" & docEntrada.ToString & "</A><B>" & ex.ToString & "</B>"
            err.lineasTotal = 0
            Return err
        End Try

    End Function

    <WebMethod()> _
    Public Function PruebaArchivo(archivo As Stream) As String

        Return "q"
    End Function

    <WebMethod()> _
    Public Function TrasladosBatchEstado(ByVal batch As Integer) As TrasladoBatchEstado
        Dim err As New TrasladoBatchEstado
        Dim errLin As New novedadLineaTraslado
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim rs2 As New ADODB.Recordset
        Dim rs3 As New ADODB.Recordset
        Dim rs4 As New ADODB.Recordset
        Dim rs5 As New ADODB.Recordset
        Dim Errores
        AbreConexion("Estado Batch")

        fSql = "SELECT * FROM BFCIMws WHERE IBATCH =  " & batch
        If DBMySql.OpenRS(rs1, fSql) Then
            If Not rs1.EOF Then

                fSql = "SELECT COUNT(DISTINCT INLINE) PEDIDOSS FROM BFCIMws WHERE IBATCH =  " & batch
                DBMySql.OpenRS(rs2, fSql)

                fSql = "SELECT COUNT(DISTINCT INLINE) PEDIDOSS FROM BFCIMws WHERE IBATCH =  " & batch & " AND IFLAG =  0 "
                DBMySql.OpenRS(rs3, fSql)

                fSql = "SELECT COUNT(DISTINCT INLINE) PEDIDOSS FROM BFCIMws WHERE IBATCH =  " & batch & " AND IFLAG =  1 "
                DBMySql.OpenRS(rs4, fSql)

                fSql = "SELECT COUNT(DISTINCT INLINE) PEDIDOSS FROM BFCIMws WHERE IBATCH =  " & batch & " AND IFLAG =  2 "
                DBMySql.OpenRS(rs5, fSql)

                If rs3("PEDIDOSS").Value > 0 Then
                    Errores = 0
                Else
                    If rs4("PEDIDOSS").Value > 0 Then
                        Errores = 0
                    Else
                        Errores = 2
                    End If
                End If
                With err
                    .batch = batch
                    .estado = 1
                    .estadoDescripcion = "1. En Proceso, 2. Terminado, -1 Con Error"
                    .lineasTotal = rs2("PEDIDOSS").Value
                    .lineasEnviadasWs = rs3("PEDIDOSS").Value
                    .lineasEnviadasCIM600 = rs4("PEDIDOSS").Value
                    .lineasBPCS = rs5("PEDIDOSS").Value
                    .lineasConNovedad = New List(Of novedadLineaTraslado)
                End With
            End If
        End If

        While Not rs1.EOF
            With errLin
                .linea = rs1("INLINE").Value
                .codMensaje = rs1("IFLAG").Value
                If rs1("IFLAG").Value = 0 Then
                    .mensaje = "Enviado desde Web Service"
                End If
                If rs1("IFLAG").Value = 1 Then
                    .mensaje = "Enviado a CIM600"
                End If
                If rs1("IFLAG").Value = 2 Then
                    .mensaje = "En BPCS"
                End If
                .Producto = rs1("INPROD").Value
                .Bodega = rs1("INWHSE").Value
                .Ubicacion = rs1("INLOC").Value
                .Lote = rs1("INLOT").Value
                .Cantidad = rs1("INQTY").Value
                .CodigoCausa = rs1("INREAS").Value
                .OrdenFabricacion = rs1("INREF").Value

            End With
            err.lineasConNovedad.Add(errLin)
            rs1.MoveNext()
        End While
        rs1.Close()
        rs2.Close()
        rs3.Close()
        rs4.Close()
        rs5.Close()

        'Dim resp As String = XLinq15()

        Return err
    End Function

    <WebMethod()> _
    Public Function AsignacionBatch(ByVal xml As String) As AsignacionBatchRespuesta


        Dim docEntrada As XDocument = XDocument.Parse(xml)

        Dim err As New AsignacionBatchRespuesta

        If Not AbreConexion(docEntrada.ToString) Then

            err.estado = -1
            err.estadoDescripcion = "Cadena de Conexion con error: " & resultConn
            err.pedidosTotal = 0
            Return err

        End If

        Try
            Dim doc As XDocument = XDocument.Parse(xml)
            Dim asignacion As New Asignacion
            Dim Pedidos As New DespacharPedido
            Dim lstPedidos As New List(Of qPedidos)
            Dim wPedido As DespacharPedido
            Dim wLinea As LineaPedido

            Console.WriteLine(doc)

            Dim qA = From xe In doc.Elements("AsignacionBatch") Select New With { _
                .Usuario = xe.Element("Usuario").Value,
                .Fecha = xe.Element("Fecha").Value
            }

            asignacion.Usuario = qA.First.Usuario
            asignacion.Fecha = qA.First.Fecha
            asignacion.Pedidos = New List(Of DespacharPedido)

            Dim qP = From xe In doc.Descendants.Elements("DespacharPedido")
                         Select New With {.Pedido = xe.Element("Pedido").Value}

            For Each e In qP
                lstPedidos.Add(New qPedidos(e.Pedido))
            Next

            Dim result = From pedido In lstPedidos _
                         From order In doc...<DespacharPedido> _
                         Where pedido.Pedido = CStr(order.<Pedido>.Value) _
                         Select Pedido = CStr(pedido.Pedido), Cliente = order.<Cliente>.Value, LinPed = order.Descendants("LineaPedido")

            For Each tuple In result
                wPedido = New DespacharPedido
                wPedido.Pedido = CInt(tuple.Pedido)
                wPedido.LineasPedido = New List(Of LineaPedido)
                Dim linPed = From lineas In tuple.LinPed Select New With {
                    .Linea = lineas.<Linea>.Value,
                    .Producto = lineas.<Producto>.Value,
                    .Bodega = lineas.<Bodega>.Value,
                    .Ubicacion = lineas.<Ubicacion>.Value,
                    .Lote = lineas.<Lote>.Value,
                    .Cantidad = lineas.<Cantidad>.Value}
                For Each e In linPed
                    wLinea = New LineaPedido
                    With wLinea
                        .Linea = CInt(e.Linea)
                        .Producto = e.Producto
                        .Bodega = e.Bodega
                        .Ubicacion = e.Ubicacion
                        .Lote = e.Lote
                        .Cantidad = CDbl(e.Cantidad)
                    End With
                    wPedido.LineasPedido.Add(wLinea)
                Next
                asignacion.Pedidos.Add(wPedido)
            Next
            Dim cons
            cons = DBMySql.CalcConsec("CIMNUM", "99999999")
            For Each p In asignacion.Pedidos
                Console.WriteLine("Pedido " & p.Pedido)


                For Each L In p.LineasPedido
                    mkSql_h(" INSERT INTO BFDESPAWS ( ")
                    mkSql_h(" VALUES ( ")
                    mkSql_d("'", " DBATCH       ", cons, 0)                       '//15A
                    mkSql_d(" ", " DPEDID       ", p.Pedido, 0)                          '//8
                    mkSql_d("'", " DLINEA       ", L.Linea, 0)                           '//2A    Record Identifier
                    mkSql_d("'", " DPRODU       ", L.Producto, 0)                           '//2A    Transaction Type
                    mkSql_d(" ", " DCANTI       ", L.Cantidad, 0)                              '//2A    Transaction Type
                    mkSql_d("'", " DBODEG       ", L.Bodega, 0)                              '//2A    Transaction Type
                    mkSql_d("'", " DLOCAL       ", L.Ubicacion, 0)                          '//8P0   Date		
                    mkSql_d("'", " DLOTE        ", L.Lote, 0)                 '//5P0   INTRN
                    mkSql_d("'", " DUSER        ", asignacion.Usuario, 0)                 '//5P0   INTRN
                    mkSql_d(" ", " DFECHA        ", Format(asignacion.Fecha, ""), 0)                 '//5P0   INTRN
                    mkSql_d(" ", " DFLAG        ", 0, 1)             '//2A    Warehouse
                    DBMySql.ExecuteSQL(fSql & vSql)
                Next
            Next

            'Dim err As New AsignacionBatchRespuesta
            err.batch = cons
            err.estado = 1
            err.estadoDescripcion = "1. Recibido, 1 Sin Errores " & Format(fSql & vSql & "         " & asignacion.Fecha, "yyyy-MM-dd")
            err.pedidosTotal = asignacion.Pedidos.Count
            Return err

        Catch ex As Exception
            'Dim err As New AsignacionBatchRespuesta
            err.estado = -1
            err.estadoDescripcion = "1. Recibido, -1 Con Errores" & ex.Message
            Return err
        End Try

    End Function

    <WebMethod()> _
    Public Function AsignacionesBatchEstado(ByVal batch As Integer) As AsignacionBatchEstado
        Dim err As New AsignacionBatchEstado
        Dim errPed As New novedadPedidoAsignacion
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim rs2 As New ADODB.Recordset
        Dim rs3 As New ADODB.Recordset
        Dim rs4 As New ADODB.Recordset
        Dim Errores
        AbreConexion("Estado Batch")

        fSql = "SELECT * FROM BFDESPAws WHERE DBATCH =  " & batch
        If DBMySql.OpenRS(rs1, fSql) Then
            If Not rs1.EOF Then

                fSql = "SELECT COUNT(DISTINCT DPEDID) PEDIDOSS FROM BFDESPAws WHERE DBATCH =  " & batch
                DBMySql.OpenRS(rs2, fSql)
                fSql = "SELECT COUNT(DISTINCT DPEDID) PEDIDOSS FROM BFDESPAWS WHERE DBATCH =  " & batch & " AND DERROR <> 0 "
                DBMySql.OpenRS(rs3, fSql)
                If rs3("PEDIDOSS").Value > 0 Then
                    Errores = -1
                Else
                    Errores = 1
                End If
                With err
                    .batch = batch
                    .estado = Errores
                    .estadoDescripcion = "1. En Proceso, 2. Terminado, -1 Con Error"
                    .pedidosTotal = rs2("PEDIDOSS").Value
                    .pedidosProcesados = 20
                    .pedidosConError = rs3("PEDIDOSS").Value
                    .pedidosConNovedad = New List(Of novedadPedidoAsignacion)
                End With
            End If



            While Not rs1.EOF
                With errPed
                    .codMensaje = rs1("DERROR").Value
                    .mensaje = rs1("DDESER").Value
                    .Pedido = rs1("DPEDID").Value
                    .Linea = rs1("DLINEA").Value
                    .Producto = rs1("DPRODU").Value
                    .Ubicacion = rs1("DLOCAL").Value
                    .Bodega = rs1("DBODEG").Value
                    .Cantidad = rs1("DCANTI").Value
                    .Lote = rs1("DLOTE").Value
                End With
                err.pedidosConNovedad.Add(errPed)
                rs1.MoveNext()
            End While


            '   Dim resp As String = XLinq15()

            Return err
            rs2.Close()
            rs3.Close()
            rs4.Close()
        End If
    End Function

    Class qPedidos
        Public Pedido As String
        Public Sub New(ByVal Pedido As String)
            Me.Pedido = Pedido
        End Sub
    End Class

    Function AbreConexion(docEntrada As String) As Boolean
        resultConn = DBMySql.abrirConexionMySql()
        If resultConn = "" Then
            c_ID = DBMySql.CalcConsec("BFWSINBOX", "99999999")
            fSql = " INSERT INTO bfwsinbox(C_ID, C_XML)  "
            fSql = fSql & " VALUES ('" & c_ID & "', '" & docEntrada & "') "
            DBMySql.ExecuteSQL(fSql)
            Return True
        Else
            Return False
        End If

    End Function

    Function errXMLTraslados(err As TrasladoBatchRespuesta) As String
        Dim errMsg = _
            <TrasladoBatchRespuesta>
                <batch><%= err.batch %></batch>
                <estado><%= err.estado %></estado>
                <estadoDescripcion><%= err.estadoDescripcion %></estadoDescripcion>
                <lineasTotal><%= err.lineasTotal %></lineasTotal>
            </TrasladoBatchRespuesta>
        Return errMsg.ToString
    End Function

    Public Function XLinq15() As String
        Dim msg As New AsignacionBatchEstado

        Dim po1
        Dim cab = <Cabecera>
                      <batch>55</batch>
                      <estado>1</estado>
                      <estadoDescripcion>1. En Proceso, 2. Terminado, -1 Con Error</estadoDescripcion>
                      <pedidosTotal>30</pedidosTotal>
                      <pedidosProcesados>20</pedidosProcesados>
                      <pedidosConError>2</pedidosConError>
                  </Cabecera>

        Dim sw = New StringWriter()
        Dim settings = New XmlWriterSettings()
        settings.Indent = True
        Dim writer = XmlWriter.Create(sw, settings)
        writer.WriteStartElement("AsignacionBatchEstado")
        cab.WriteTo(writer)

        For i = 1 To 3
            po1 = det(i)
            po1.WriteTo(writer)
        Next

        writer.WriteEndElement()
        writer.Close()
        Return sw.ToString()

    End Function

    Function det(i As Integer)
        Dim linDet = _
            <novedadPedidoAsignacion>
                <pedido>444555</pedido>
                <codMensaje><%= i %></codMensaje>
                <mensaje>Producto sin inventario</mensaje>
            </novedadPedidoAsignacion>
        Return linDet
    End Function

    Public Class qVersion
        Public Version As String
        Public Fecha As String
        Public Parametros As String
        Public MySQL As String
        Public AS400 As String
    End Class

    <WebMethod()> _
    Public Function Version() As qVersion
        Dim qVer As New qVersion
        qVer.Version = Me.GetType.Assembly.GetName.Version.ToString()
        qVer.Fecha = "2016-03-16"
        qVer.Parametros = My.Settings.PARAMETROS
        qVer.MySQL = paramWebConfig(ubicWebConfig, "MYSQL_LIB")
        qVer.AS400 = qAS400()

        Return (qVer)

    End Function

    Function qAS400() As String
        Dim resultConn As String = DBMySql.abrirConexionMySql()
        If resultConn = "" Then
            Return DBMySql.getLXParam("AS400_SERVER")
        Else
            Return resultConn
        End If

    End Function

End Class
