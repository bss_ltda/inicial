﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' La información general sobre un ensamblado se controla mediante el siguiente 
' conjunto de atributos. Cambie los valores de estos atributos para modificar la información
' asociada a un ensamblado.

' Revisar los valores de los atributos del ensamblado
<Assembly: AssemblyTitle("wsBellotaTR")> 
<Assembly: AssemblyDescription("Web Service Traslados en Batch")> 
<Assembly: AssemblyCompany("BSS LTDA")> 
<Assembly: AssemblyProduct("wsBellotaTR")> 
<Assembly: AssemblyCopyright("Copyright © Microsoft 2015")> 
<Assembly: AssemblyTrademark("BSSWebServiceTraslao")> 

<Assembly: ComVisible(False)> 

'El siguiente GUID es para el Id. typelib cuando este proyecto esté expuesto a COM
<Assembly: Guid("c775a1e4-949d-482f-88ab-fd0128bacd27")> 

' La información de versión de un ensamblado consta de los siguientes cuatro valores:
'
'      Versión principal
'      Versión secundaria 
'      Número de compilación
'      Revisión
'
' Puede especificar todos los valores o aceptar los valores predeterminados de los números de compilación y de revisión 
' mediante el carácter '*', como se muestra a continuación:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
