﻿Module Module1

    Dim wsNum As String
    Dim wsResult As String

    Sub Prueba()

        wsNum = ""
        wsResult = ""
        Try

            Console.WriteLine(My.Settings.WebConfig)
            rw(DBMySql.abrirConexionMySql())

            If registroWS("Inicio", "") Then
                registroWS("Operacion", "Actualiza Cant Orden")
                registroWS("Operacion", "TransaccionAcero")
                registroWS("Operacion", "Transaccion R Acero")
                registroWS("Operacion", "Transaccion RJ")
                'Revisa para ejecutar cada hora
                registroWS("Fin", "")

                registroWS("Operacion", "TransaccionAcero")
                registroWS("Fin", "")
                If registroWS("Actualiza PR", "Actualiza PR") Then
                    Console.WriteLine("*****************")
                End If
            End If

            DBMySql.Close()
        Catch ex As Exception
            rw(ex.ToString)
            rw("SQL", lastSQL)
        End Try


    End Sub


    Private Function registroWS(tipo As String, op As String) As Boolean
        Dim rs As New ADODB.Recordset
        Dim servWin As String = "TareasBellota"
        Dim fSql, vSql As String
        Dim resultado As Boolean = True
        Dim hora As Integer

        If wsNum <> "" Then
            fSql = "UPDATE BFPARAM SET CCNOT1 = '" & wsResult & "'"
            fSql &= ", CCMNDT    ='" & Format(Now(), "yyy-MM-dd HH:mm:ss") & "' "     '//Z     CCENDT
            fSql &= " WHERE CCNUM = " & wsNum
            DBMySql.ExecuteSQL(fSql)
        End If

        If op <> "" Then
            fSql = "SELECT * FROM BFPARAM"
            fSql = fSql & " WHERE "
            fSql = fSql & " CCTABL = '" & servWin & "' "
            fSql = fSql & " AND CCCODE = '02' "
            fSql = fSql & " AND CCCODE2 = '" & op & "' "
            rw("Estado Conn", DBMySql.Estado)

            If Not DBMySql.OpenRS(rs, fSql) Then
                fSql = " INSERT INTO BFPARAM( "
                vSql = " VALUES ( "
                fSql = fSql & " CCID    , " : vSql = vSql & "'CC', "                '//A     CCID
                fSql = fSql & " CCCODE  , " : vSql = vSql & "'02', "                '//A     CCCODE
                fSql = fSql & " CCCODE2  , " : vSql = vSql & "'" & op & "', "                '//A     CCCODE
                fSql = fSql & " CCTABL )  " : vSql = vSql & "'" & servWin & "') "   '//A     CCTABL
                DBMySql.ExecuteSQL(fSql & vSql)
                rs.Requery()
            End If
            wsNum = rs("CCNUM").Value
            wsResult = "resultado " & op
            rs.Close()
            fSql = "UPDATE BFPARAM SET CCNOT1 = 'Inicia Rutina'"
            fSql &= ", CCENDT = '" & Format(Now(), "yyy-MM-dd HH:mm:ss") & "' "     '//Z     CCENDT
            fSql &= " WHERE "
            fSql &= " CCTABL = '" & servWin & "' "
            fSql &= " AND CCCODE = '02' "
            fSql &= " AND CCCODE2 = '" & op & "' "
            DBMySql.ExecuteSQL(fSql)
        End If


        fSql = "SELECT * FROM BFPARAM"
        fSql = fSql & " WHERE "
        fSql = fSql & " CCTABL = '" & servWin & "' "
        fSql = fSql & " AND CCCODE = '01' "
        rw("Estado Conn", DBMySql.Estado)
        rw(fSql)
        If Not DBMySql.OpenRS(rs, fSql) Then
            fSql = " INSERT INTO BFPARAM( "
            vSql = " VALUES ( "
            fSql = fSql & " CCID    , " : vSql = vSql & "'CC', "                '//A     CCID
            fSql = fSql & " CCCODE  , " : vSql = vSql & "'01', "                '//A     CCCODE
            fSql = fSql & " CCTABL )  " : vSql = vSql & "'" & servWin & "') "   '//A     CCTABL
            DBMySql.ExecuteSQL(fSql & vSql)
        Else
            rw("Encontro 2", rs.Source)
            'Dim x As ADODB.Field
            'For Each x In rs.Fields
            '    rw("for each")
            '    rw(x.Name, CStr(x.Value))
            'Next

            Select Case tipo
                Case "Inicio"
                    resultado = False
                    If IsDBNull(rs("CCUDC1").Value) Then
                        resultado = True
                    ElseIf rs("CCUDC1").Value = 1 Then
                        resultado = True
                    End If
                Case "Actualiza PR"
                    resultado = False
                    hora = Hour(Now())
                    If IsDBNull(rs("CCUDV1").Value) Then
                        resultado = True
                    ElseIf hora <> rs("CCUDV1").Value Then
                        resultado = True
                    End If
            End Select
        End If
        rs.Close()

        fSql = " UPDATE BFPARAM SET "
        fSql = fSql & " CCUDTS    ='" & Format(Now(), "yyy-MM-dd HH:mm:ss") & "' "     '//Z     CCUDTS
        Select Case tipo
            Case "Inicio"
                fSql = fSql & ", CCENDT    ='" & Format(Now(), "yyy-MM-dd HH:mm:ss") & "' "     '//Z     CCENDT
            Case "Fin"
                fSql = fSql & ", CCMNDT    ='" & Format(Now(), "yyy-MM-dd HH:mm:ss") & "' "     '//Z     CCENDT
            Case "Operacion"
                fSql = fSql & ", CCNOT1    ='" & op & "' "     '//A     CCNOT1
            Case "Actualiza PR"
                If resultado Then
                    fSql = fSql & ", CCUDV1    = " & hora & " "      '//P     CCUDV1
                    fSql = fSql & ", CCNOT1    ='" & op & "' "     '//A     CCNOT1
                End If
        End Select
        fSql = fSql & " WHERE "
        fSql = fSql & " CCTABL = '" & servWin & "' AND CCCODE = '01'"
        DBMySql.ExecuteSQL(fSql)
        Return resultado

    End Function


End Module
