﻿Imports System.IO
Imports MySql.Data.MySqlClient
Imports System.Globalization

Module Produccion
    Dim wsNum As String
    Dim wsResult As String


    Public sEstado As String = ""

    Sub Main()
        Dim resultado As String = ""

        Try
            ubicWebConfig = My.Settings.WebConfig
            System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("es-CO")
            System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator = "."
            System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyGroupSeparator = ","
            System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator = "."
            System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberGroupSeparator = ","
            If Dir(ubicWebConfig) = "" Then
                rw("err web.config")
                Exit Sub
            End If
            resultado = DBMySql.abrirConexionMySql()
            If resultado = "Conexion OK" Then
                If registroWS("Inicio", "") Then
                    registroWS("Operacion", "Sincroniza Estado")
                    actualizaBFCIM()
                    registroWS("Operacion", "Actualiza Cant Orden")
                    actualizacionPR()
                    registroWS("Operacion", "Transaccion Acero")
                    TransaccionAcero()
                    registroWS("Operacion", "Transaccion R Acero")
                    TransaccionRAcero()
                    registroWS("Operacion", "Transaccion RJ")
                    TransaccionRJ()
                    'Revisa para ejecutar cada hora
                    If registroWS("Actualiza PR", "Actualiza PR") Then
                        actualizaPR()
                    End If
                    registroWS("Fin", "")
                End If
                DB.Close()
                DBMySql.Close()
            Else
            End If
        Catch ex As Exception
            DBMySql.WrtSqlError(lastSQL, ex.Message & "<br />" & ex.StackTrace.Replace(" en ", "<br />"))
        End Try
        Console.WriteLine("Presione una tecla")
        Console.ReadKey()

    End Sub

    'Sincroniza BFCIM MySql/AS400
    Sub actualizaBFCIM()
        Dim rs As New ADODB.Recordset
        Dim r As Integer = 0

        DB.abrirConexionAS400()
        fSql = "SELECT IFLAG, INMLOT FROM BFCIM WHERE IFLAG IN( 1, 2 ) "
        DB.OpenRS(rs, fSql)
        While Not rs.EOF
            fSql = " UPDATE BFCIM SET "
            fSql &= " IFLAG = " & rs("IFLAG").Value & "  "
            fSql &= " WHERE INMLOT ='" & rs("INMLOT").Value & "' "
            DBMySql.ExecuteSQL(fSql)
            fSql = " UPDATE BFCIM SET "
            fSql &= " IFLAG = IFLAG + 2 "
            fSql &= " WHERE INMLOT ='" & rs("INMLOT").Value & "' "
            DB.ExecuteSQL(fSql)
            r += 1
            rs.MoveNext()
        End While
        rs.Close()
        wsResult = "Registros actualizados: " & r

    End Sub

    Sub actualizacionPR()
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim r As Integer = 0
        fSql = "SELECT TORD FROM BFREP WHERE TSTS <> 2 GROUP BY TORD "
        If DBMySql.OpenRS(rs, fSql) Then
            DB.abrirConexionAS400()
        Else
            rs.Close()
            wsResult = "Sin Novedad"
            Return
        End If
        While Not rs.EOF
            fSql = "SELECT SQFIN FROM FSO WHERE SORD =" & rs("TORD").Value
            If DB.OpenRS(rs1, fSql) Then
                fSql = "UPDATE BFREP SET TQRECB = " & rs1("SQFIN").Value & " WHERE  TORD =" & rs("TORD").Value
                DBMySql.ExecuteSQL(fSql)
            End If
            rs1.Close()
            rs.MoveNext()
            r += 1
        End While
        rs.Close()
        wsResult = "Registros actualizados: " & r

    End Sub

    Sub TransaccionRAcero()

        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim rs2 As New ADODB.Recordset
        Dim canti As Double
        Dim canti1 As Double
        Dim cons As String
        Dim batch As String = DBMySql.CalcConsec("BFCIM", "99999999")

        fSql = "SELECT * FROM BFREPACE INNER JOIN BFREPP ON CPRODPT  = PPROD "
        fSql = fSql & " WHERE CENVIADO = 0 AND CTIPO = 'REVERSION'"
        If DBMySql.OpenRS(rs, fSql) Then
            DB.abrirConexionAS400()
        Else
            rs.Close()
            wsResult = "Sin Novedad"
            Return
        End If
        While Not rs.EOF
            fSql = "SELECT * FROM BFREP INNER JOIN BFREPP ON TPROD  = PPROD "
            fSql = fSql & " WHERE TORD ='" & rs("CORDEN").Value & "'"
            If DBMySql.OpenRS(rs2, fSql) Then

                cons = DBMySql.CalcConsec("CIMNUM", "99999999")

                mkSql_h(" INSERT INTO BFCIM ( ")
                mkSql_h(" VALUES ( ")
                mkSql_d("'", " IFUENTE", "PRODUCCION", 0)                       '//15A
                mkSql_d(" ", " IBATCH ", batch, 0)                          '//8
                mkSql_d("'", " INID   ", "IN", 0)                           '//2A    Record Identifier
                mkSql_d("'", " INTRAN ", "WT", 0)                           '//2A    Transaction Type
                mkSql_d(" ", " INCTNR ", 0, 0)                              '//2A    Transaction Type
                mkSql_d("'", " INMFGR ", 0, 0)                              '//2A    Transaction Type
                mkSql_d(" ", " INDATE ", DB2_DATEDEC(), 0)                          '//8P0   Date		
                mkSql_d(" ", " INTRN  ", Right(cons, 5), 0)                 '//5P0   INTRN
                mkSql_d("'", " INMLOT ", "RP" & cons, 0)                   '//5P0   INTRN
                mkSql_d("'", " INREAS ", "01", 0)                           '//5P0   INTRN
                mkSql_d(" ", " INREF  ", rs2("TORD").Value, 0)              '//8P0   Date
                mkSql_d("'", " INPROD ", rs("CPRODPP").Value, 0)            '//15A   Item Number
                mkSql_d("'", " INWHSE ", rs2("PBODPP").Value, 0)            '//2A    Warehouse
                mkSql_d("'", " INLOT  ", rs2("PLOTE").Value, 0)             '//10A   Lot Number
                mkSql_d("'", " INLOC  ", rs2("PLOCPP").Value, 0)            '//6A    Location Code
                canti = CDbl(rs("CQREQPP").Value)
                mkSql_d("'", " INQTY  ", canti, 0)                          '//10A   Creted by
                mkSql_d("'", " INCBY  ", "COSIS001", 0)                     '//10A   Creted by

                mkSql_d(" ", " INLDT  ", Format(Now(), "yyyyMMdd"), 0)      '//8P0
                mkSql_d(" ", " INLTM  ", Format(Now(), "hhmmss"), 0)        '//6P0
                mkSql_d(" ", " INCDT  ", DB2_DATEDEC(), 0)                  '//8P0   Date Created
                mkSql_d(" ", " INCTM  ", DB2_TIMEDEC(), 1)                  '//6P0   Time Created
                DB.ExecuteSQL(fSql & vSql)
                DBMySql.ExecuteSQL(fSql & vSql)


                fSql = "SELECT * FROM CIC "
                fSql = fSql & " WHERE ICPROD = '" & rs("CPRODPP").Value & "' AND ICFAC = 'IC'"
                If DBMySql.OpenRS(rs1, fSql) Then
                    cons = DBMySql.CalcConsec("CIMNUM", "99999999")
                    mkSql_h(" INSERT INTO BFCIM ( ")
                    mkSql_h(" VALUES ( ")
                    mkSql_d("'", " IFUENTE", "PRODUCCION", 0)                       '//15A
                    mkSql_d(" ", " IBATCH ", batch, 0)                          '//8
                    mkSql_d("'", " INID   ", "IN", 0)                           '//2A    Record Identifier
                    mkSql_d("'", " INTRAN ", "CI", 0)                           '//2A    Transaction Type
                    mkSql_d(" ", " INCTNR ", 0, 0)                              '//2A    Transaction Type
                    mkSql_d("'", " INMFGR ", 0, 0)                              '//2A    Transaction Type
                    mkSql_d(" ", " INDATE ", DB2_DATEDEC(), 0)                          '//8P0   Date		
                    mkSql_d(" ", " INTRN  ", Right(cons, 5), 0)                 '//5P0   INTRN
                    mkSql_d("'", " INMLOT  ", "RP" & cons, 0)                   '//5P0   INTRN
                    mkSql_d("'", " INREAS ", "01", 0)                           '//5P0   INTRN
                    mkSql_d(" ", " INREF  ", rs2("TORD").Value, 0)            '//8P0   Date
                    mkSql_d("'", " INPROD ", rs("CPRODPT").Value, 0)           '//15A   Item Number
                    mkSql_d("'", " INWHSE ", rs1("ICPLN").Value, 0)             '//2A    Warehouse
                    mkSql_d("'", " INLOT  ", "", 0)                             '//10A   Lot Number
                    mkSql_d("'", " INLOC  ", "", 0)                             '//6A    Location Code

                    canti1 = CDbl(rs("CQREQPP").Value)
                    mkSql_d("'", " INQTY  ", canti1, 0)                         '//10A   Creted by
                    mkSql_d("'", " INCBY  ", "COSIS001", 0)                     '//10A   Creted by
                    mkSql_d(" ", " INLDT  ", Format(Now(), "yyyyMMdd"), 0)      '//8P0
                    mkSql_d(" ", " INLTM  ", Format(Now(), "hhmmss"), 0)        '//6P0
                    mkSql_d(" ", " INCDT  ", DB2_DATEDEC(), 0)                  '//8P0   Date Created
                    mkSql_d(" ", " INCTM  ", DB2_TIMEDEC(), 1)                  '//6P0   Time Created
                    DB.ExecuteSQL(fSql & vSql)
                    DBMySql.ExecuteSQL(fSql & vSql)
                End If
                rs1.Close()
            End If
            rs2.Close()

            Dim aReversar() As String
            aReversar = Split(rs("CORDENR").Value, "-")
            fSql = "SELECT * FROM BFREPACE INNER JOIN BFREPP ON CPRODPT  = PPROD "
            fSql = fSql & " WHERE CORDEN =" & aReversar(1)
            If DBMySql.OpenRS(rs2, fSql) Then

                cons = DBMySql.CalcConsec("CIMNUM", "99999999")

                mkSql_h(" INSERT INTO BFCIM ( ")
                mkSql_h(" VALUES ( ")
                mkSql_d("'", " IFUENTE", "PRODUCCION", 0)                       '//15A
                mkSql_d(" ", " IBATCH ", batch, 0)                          '//8
                mkSql_d("'", " INID   ", "IN", 0)                           '//2A    Record Identifier
                mkSql_d("'", " INTRAN ", "WT", 0)                           '//2A    Transaction Type
                mkSql_d(" ", " INCTNR ", 0, 0)                              '//2A    Transaction Type
                mkSql_d("'", " INMFGR ", 0, 0)                              '//2A    Transaction Type
                mkSql_d(" ", " INDATE ", DB2_DATEDEC(), 0)                          '//8P0   Date		
                mkSql_d(" ", " INTRN  ", Right(cons, 5), 0)                 '//5P0   INTRN
                mkSql_d("'", " INMLOT  ", "RP" & cons, 0)                   '//5P0   INTRN
                mkSql_d("'", " INREAS ", "01", 0)                           '//5P0   INTRN
                mkSql_d(" ", " INREF  ", rs2("CORDEN").Value, 0)            '//8P0   Date
                mkSql_d("'", " INPROD ", rs2("CPRODPT").Value, 0)           '//15A   Item Number
                mkSql_d("'", " INWHSE ", rs2("PBODPP").Value, 0)            '//2A    Warehouse
                mkSql_d("'", " INLOT  ", rs2("PLOTE").Value, 0)             '//10A   Lot Number
                mkSql_d("'", " INLOC  ", rs2("PLOCPP").Value, 0)            '//6A    Location Code

                canti = -canti
                mkSql_d("'", " INQTY  ", canti, 0)                          '//10A   Creted by
                mkSql_d("'", " INCBY  ", "COSIS001", 0)                     '//10A   Creted by
                mkSql_d(" ", " INLDT  ", Format(Now(), "yyyyMMdd"), 0)      '//8P0
                mkSql_d(" ", " INLTM  ", Format(Now(), "hhmmss"), 0)        '//6P0
                mkSql_d(" ", " INCDT  ", DB2_DATEDEC(), 0)                  '//8P0   Date Created
                mkSql_d(" ", " INCTM  ", DB2_TIMEDEC(), 1)                  '//6P0   Time Created
                DB.ExecuteSQL(fSql & vSql)
                DBMySql.ExecuteSQL(fSql & vSql)

                fSql = "SELECT * FROM CIC "
                fSql = fSql & " WHERE ICPROD = '" & rs2("CPRODPP").Value & "' AND ICFAC = 'IC'"
                If DBMySql.OpenRS(rs1, fSql) Then
                    cons = DBMySql.CalcConsec("CIMNUM", "99999999")
                    mkSql_h(" INSERT INTO BFCIM ( ")
                    mkSql_h(" VALUES ( ")
                    mkSql_d("'", " IFUENTE", "PRODUCCION", 0)                       '//15A
                    mkSql_d(" ", " IBATCH ", batch, 0)                          '//8
                    mkSql_d("'", " INID   ", "IN", 0)                           '//2A    Record Identifier
                    mkSql_d("'", " INTRAN ", "CI", 0)                           '//2A    Transaction Type
                    mkSql_d(" ", " INCTNR ", 0, 0)                              '//2A    Transaction Type
                    mkSql_d("'", " INMFGR ", 0, 0)                              '//2A    Transaction Type
                    mkSql_d(" ", " INDATE ", DB2_DATEDEC(), 0)                          '//8P0   Date		
                    mkSql_d(" ", " INTRN  ", Right(cons, 5), 0)                 '//5P0   INTRN
                    mkSql_d("'", " INMLOT  ", "RP" & cons, 0)                   '//5P0   INTRN
                    mkSql_d("'", " INREAS ", "01", 0)                           '//5P0   INTRN
                    mkSql_d(" ", " INREF  ", rs2("CORDEN").Value, 0)            '//8P0   Date
                    mkSql_d("'", " INPROD ", rs2("CPRODPP").Value, 0)           '//15A   Item Number
                    mkSql_d("'", " INWHSE ", rs1("ICPLN").Value, 0)             '//2A    Warehouse
                    mkSql_d("'", " INLOT  ", "", 0)                             '//10A   Lot Number
                    mkSql_d("'", " INLOC  ", "", 0)                             '//6A    Location Code

                    canti1 = -canti1
                    mkSql_d("'", " INQTY  ", canti1, 0)                         '//10A   Creted by
                    mkSql_d("'", " INCBY  ", "COSIS001", 0)                     '//10A   Creted by
                    mkSql_d(" ", " INLDT  ", Format(Now(), "yyyyMMdd"), 0)      '//8P0
                    mkSql_d(" ", " INLTM  ", Format(Now(), "hhmmss"), 0)        '//6P0
                    mkSql_d(" ", " INCDT  ", DB2_DATEDEC(), 0)                  '//8P0   Date Created
                    mkSql_d(" ", " INCTM  ", DB2_TIMEDEC(), 1)                  '//6P0   Time Created
                    DB.ExecuteSQL(fSql & vSql)
                    DBMySql.ExecuteSQL(fSql & vSql)

                    fSql = "UPDATE bfrepace SET CENVIADO =1 WHERE WRKID = " & rs("WRKID").Value
                    DBMySql.ExecuteSQL(fSql)

                    fSql = "UPDATE BFREP SET TQREQACEB = TQREQACEB + " & canti & " WHERE TNUMREP = " & rs("CNUMREP").Value
                    DBMySql.ExecuteSQL(fSql)

                    fSql = "UPDATE BFREP SET TRPODACEB = TRPODACEB + " & canti1 & " WHERE TNUMREP = " & rs("CNUMREP").Value
                    DBMySql.ExecuteSQL(fSql)
                End If
                rs1.Close()
            End If
            rs2.Close()
            rs.MoveNext()
        End While
        rs.Close()

        fSql = "{{ CALL PGM(" & paramWebConfig(ubicWebConfig, "AS400_PGM") & "/BCTRAINVB) PARM('" & batch & "') }}"
        DB.ExecuteSQL(fSql)
        wsResult = "Transacciones enviadas"

    End Sub

    Sub TransaccionRJ()
        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim canti As Double
        Dim batch As String = DBMySql.CalcConsec("BFCIM", "99999999")
        Dim cons As String

        fSql = "SELECT * FROM BFREP INNER JOIN BFREPP ON TPROD = PPROD INNER JOIN BFREPTURRJ ON TNUMREP = RNUMREP WHERE RENVIADO = 0"
        If DBMySql.OpenRS(rs, fSql) Then
            DB.abrirConexionAS400()
        Else
            rs.Close()
            wsResult = "Sin Novedad"
            Return
        End If
        While Not rs.EOF
            cons = DBMySql.CalcConsec("CIMNUM", "99999999")

            mkSql_h(" INSERT INTO BFCIM ( ")
            mkSql_h(" VALUES ( ")
            mkSql_d("'", " IFUENTE", "PRODUCCION", 0)                       '//15A
            mkSql_d(" ", " IBATCH ", batch, 0)                              '//8
            mkSql_d("'", " INID   ", "IN", 0)                               '//2A    Record Identifier
            mkSql_d("'", " INTRAN ", "RJ", 0)                               '//2A    Transaction Type
            mkSql_d(" ", " INCTNR ", rs("RPROCESO").Value, 0)                     '//2A    Transaction Type
            mkSql_d("'", " INMFGR ", rs("RPROCESO").Value, 0)                     '//2A    Transaction Type
            mkSql_d(" ", " INDATE ", DB2_DATEDEC(), 0)                              '//8P0   Date		
            mkSql_d(" ", " INTRN  ", Right(cons, 5), 0)                     '//5P0   INTRN
            mkSql_d("'", " INMLOT  ", "RP" & cons, 0)                       '//5P0   INTRN
            mkSql_d("'", " INREAS ", rs("RCAUSAL").Value, 0)                '//5P0   INTRN
            mkSql_d(" ", " INREF  ", rs("TORD").Value, 0)                   '//8P0   Date
            mkSql_d("'", " INPROD ", rs("TPROD").Value, 0)                  '//15A   Item Number
            mkSql_d("'", " INWHSE ", rs("PBODPP").Value, 0)                 '//2A    Warehouse
            mkSql_d("'", " INLOT  ", rs("PLOTE").Value, 0)                  '//10A   Lot Number
            mkSql_d("'", " INLOC  ", rs("PLOCPP").Value, 0)                 '//6A    Location Code
            canti = (CDbl(rs("RQREP").Value))
            mkSql_d("'", " INQTY  ", canti, 0)                              '//10A   Creted by
            mkSql_d("'", " INCBY  ", "COSIS001", 0)                         '//10A   Creted by
            mkSql_d(" ", " INLDT  ", Format(Now(), "yyyyMMdd"), 0)          '//8P0
            mkSql_d(" ", " INLTM  ", Format(Now(), "hhmmss"), 0)            '//6P0
            mkSql_d(" ", " INCDT  ", DB2_DATEDEC(), 0)                      '//8P0   Date Created
            mkSql_d(" ", " INCTM  ", DB2_TIMEDEC(), 1)                      '//6P0   Time Created
            DB.ExecuteSQL(fSql & vSql)
            DBMySql.ExecuteSQL(fSql & vSql)

            fSql = "UPDATE BFREPTURRJ SET RENVIADO = 1  WHERE RWRKID = " & rs("RWRKID").Value
            DBMySql.ExecuteSQL(fSql)

            fSql = "UPDATE BFREP SET TQREDB = TQREDB +  " & rs("RQREP").Value & " WHERE TNUMREP = " & rs("TNUMREP").Value
            DBMySql.ExecuteSQL(fSql)

            rs.MoveNext()
        End While
        rs.Close()

        fSql = "{{ CALL PGM(" & paramWebConfig(ubicWebConfig, "AS400_PGM") & "/BCTRAINVB) PARM('" & batch & "') }}"
        DB.ExecuteSQL(fSql)

        wsResult = "Transacciones enviadas"

    End Sub

    Sub TransaccionAcero()

        Dim rs As New ADODB.Recordset
        Dim rs1 As New ADODB.Recordset
        Dim cons As String
        Dim batch As String = DBMySql.CalcConsec("BFCIM", "99999999")

        fSql = "SELECT * FROM BFREPACE INNER JOIN BFREPP ON CPRODPT  = PPROD "
        fSql = fSql & " WHERE CENVIADO = 0 AND CTIPO = 'REPORTE'"
        If DBMySql.OpenRS(rs, fSql) Then
            DB.abrirConexionAS400()
        Else
            rs.Close()
            wsResult = "Sin Novedad"
            Return
        End If
        While Not rs.EOF
            cons = DBMySql.CalcConsec("CIMNUM", "99999999")

            mkSql_h(" INSERT INTO BFCIM ( ")
            mkSql_h(" VALUES ( ")
            mkSql_d("'", " IFUENTE", "PRODUCCION", 0)                       '//15A
            mkSql_d(" ", " IBATCH ", batch, 0)                          '//8
            mkSql_d("'", " INID   ", "IN", 0)                       '//2A    Record Identifier
            mkSql_d("'", " INTRAN ", "WT", 0)                       '//2A    Transaction Type
            mkSql_d(" ", " INCTNR ", 0, 0)                          '//2A    Transaction Type
            mkSql_d("'", " INMFGR ", 0, 0)                          '//2A    Transaction Type
            mkSql_d(" ", " INDATE ", DB2_DATEDEC(), 0)                      '//8P0   Date		
            mkSql_d(" ", " INTRN  ", Right(cons, 5), 0)             '//5P0   INTRN
            mkSql_d("'", " INMLOT  ", "RP" & cons, 0)               '//5P0   INTRN
            mkSql_d("'", " INREAS ", "01", 0)                       '//5P0   INTRN
            mkSql_d(" ", " INREF  ", rs("CORDEN").Value, 0)         '//8P0   Date
            mkSql_d("'", " INPROD ", rs("CPRODPT").Value, 0)        '//15A   Item Number
            mkSql_d("'", " INWHSE ", rs("PBODPP").Value, 0)         '//2A    Warehouse
            mkSql_d("'", " INLOT  ", rs("PLOTE").Value, 0)          '//10A   Lot Number
            mkSql_d("'", " INLOC  ", rs("PLOCPP").Value, 0)         '//6A    Location Code
            Dim canti
            Dim canti1
            canti = CDbl(rs("CQREQPT").Value)
            mkSql_d("'", " INQTY  ", canti, 0)                      '//10A   Creted by
            mkSql_d("'", " INCBY  ", "COSIS001", 0)                 '//10A   Creted by
            mkSql_d(" ", " INLDT  ", Format(Now(), "yyyyMMdd"), 0)      '//8P0
            mkSql_d(" ", " INLTM  ", Format(Now(), "hhmmss"), 0)        '//6P0
            mkSql_d(" ", " INCDT  ", DB2_DATEDEC(), 0)              '//8P0   Date Created
            mkSql_d(" ", " INCTM  ", DB2_TIMEDEC(), 1)              '//6P0   Time Created
            DB.ExecuteSQL(fSql & vSql)
            DBMySql.ExecuteSQL(fSql & vSql)

            fSql = "SELECT * FROM CIC "
            fSql = fSql & " WHERE ICPROD = '" & rs("CPRODPP").Value & "' AND ICFAC = 'IC'"
            If DBMySql.OpenRS(rs1, fSql) Then
                cons = DBMySql.CalcConsec("CIMNUM", "99999999")
                mkSql_h(" INSERT INTO BFCIM ( ")
                mkSql_h(" VALUES ( ")
                mkSql_d("'", " IFUENTE", "PRODUCCION", 0)                       '//15A
                mkSql_d(" ", " IBATCH ", batch, 0)                          '//8
                mkSql_d("'", " INID   ", "IN", 0)                           '//2A    Record Identifier
                mkSql_d("'", " INTRAN ", "CI", 0)                           '//2A    Transaction Type
                mkSql_d(" ", " INCTNR ", 0, 0)                              '//2A    Transaction Type
                mkSql_d("'", " INMFGR ", 0, 0)                              '//2A    Transaction Type
                mkSql_d(" ", " INDATE ", DB2_DATEDEC(), 0)                  '//8P0   Date		
                mkSql_d(" ", " INTRN  ", Right(cons, 5), 0)                 '//5P0   INTRN
                mkSql_d("'", " INMLOT  ", "RP" & cons, 0)                   '//5P0   INTRN
                mkSql_d("'", " INREAS ", "01", 0)                           '//5P0   INTRN
                mkSql_d(" ", " INREF  ", rs("CORDEN").Value, 0)             '//8P0   Date
                mkSql_d("'", " INPROD ", rs("CPRODPP").Value, 0)            '//15A   Item Number
                mkSql_d("'", " INWHSE ", rs1("ICPLN").Value, 0)             '//2A    Warehouse
                mkSql_d("'", " INLOT  ", "", 0)                             '//10A   Lot Number
                mkSql_d("'", " INLOC  ", "", 0)                             '//6A    Location Code

                canti1 = CDbl(rs("CQREQPP").Value)
                mkSql_d("'", " INQTY  ", canti1, 0)                         '//10A   Creted by
                mkSql_d("'", " INCBY  ", "COSIS001", 0)                     '//10A   Creted by
                mkSql_d(" ", " INLDT  ", Format(Now(), "yyyyMMdd"), 0)      '//8P0
                mkSql_d(" ", " INLTM  ", Format(Now(), "hhmmss"), 0)        '//6P0
                mkSql_d(" ", " INCDT  ", DB2_DATEDEC(), 0)                  '//8P0   Date Created
                mkSql_d(" ", " INCTM  ", DB2_TIMEDEC(), 1)                  '//6P0   Time Created
                DB.ExecuteSQL(fSql & vSql)
                DBMySql.ExecuteSQL(fSql & vSql)

                fSql = "UPDATE BFREP SET TQREQACEB = TQREQACEB + " & canti & " WHERE TNUMREP = " & rs("CNUMREP").Value
                DBMySql.ExecuteSQL(fSql)

                fSql = "UPDATE BFREP SET TRPODACEB = TRPODACEB + " & canti1 & " WHERE TNUMREP = " & rs("CNUMREP").Value
                DBMySql.ExecuteSQL(fSql)

                fSql = "UPDATE bfrepace SET CENVIADO = 1 WHERE WRKID = " & rs("WRKID").Value
                DBMySql.ExecuteSQL(fSql)

            End If
            rs1.Close()
            rs.MoveNext()
        End While
        rs.Close()
        fSql = "{{ CALL PGM(" & paramWebConfig(ubicWebConfig, "AS400_PGM") & "/BCTRAINVB) PARM('" & batch & "') }}"
        DB.ExecuteSQL(fSql)
        wsResult = "Transacciones enviadas"

    End Sub

    Sub actualizaPR()
        Dim cons As String
        Dim batch As String = DBMySql.CalcConsec("BFCIM", "99999999")

        fSql = "SELECT * FROM BFREP INNER JOIN BFREPP ON TPROD = PPROD WHERE TQREP > TQREC"

        If DBMySql.OpenRS(rs, fSql) Then
            DB.abrirConexionAS400()
        Else
            rs.Close()
            wsResult = "Sin Novedad"
            Return
        End If
        While Not rs.EOF

            cons = DBMySql.CalcConsec("BFCIM", "99999999")

            mkSql_h(" INSERT INTO BFCIM ( ")
            mkSql_h(" VALUES ( ")
            mkSql_d("'", " IFUENTE", "PRODUCCION", 0)                       '//15A
            mkSql_d("'", " INID   ", "IN", 0)                   '//2A    Record Identifier
            mkSql_d(" ", " IBATCH ", batch, 0)                          '//8
            mkSql_d("'", " INTRAN ", "WF", 0)               '//2A    Transaction Type
            mkSql_d(" ", " INCTNR ", 0, 0)            '//2A    Transaction Type
            mkSql_d("'", " INMFGR ", 0, 0)            '//2A    Transaction Type
            mkSql_d(" ", " INDATE ", DB2_DATEDEC(), 0)                  '//8P0   Date		
            mkSql_d(" ", " INTRN  ", Right(cons, 5), 0)                  '//5P0   INTRN
            mkSql_d("'", " INMLOT  ", "RP" & cons, 0)                '//5P0   INTRN
            mkSql_d("'", " INREAS ", "01", 0)                  '//5P0   INTRN
            mkSql_d(" ", " INREF  ", rs("TORD").Value, 0)                  '//8P0   Date
            mkSql_d("'", " INPROD ", rs("TPROD").Value, 0)               '//15A   Item Number
            mkSql_d("'", " INWHSE ", rs("PBODPP").Value, 0)                 '//2A    Warehouse
            mkSql_d("'", " INLOT  ", rs("PLOTE").Value, 0)                   '//10A   Lot Number
            mkSql_d("'", " INLOC  ", rs("PLOCPP").Value, 0)              '//6A    Location Code
            Dim canti
            canti = -(CDbl(rs("TQREP").Value) - CDbl(rs("TQREC").Value))
            mkSql_d("'", " INQTY  ", canti, 0)                '//10A   Creted by
            mkSql_d("'", " INCBY  ", "COSIS001", 0)                '//10A   Creted by
            mkSql_d(" ", " INCDT  ", DB2_DATEDEC(), 0)      '//8P0   Date Created
            mkSql_d(" ", " INCTM  ", DB2_TIMEDEC(), 1)      '//6P0   Time Created

            DB.ExecuteSQL(fSql & vSql)
            DBMySql.ExecuteSQL(fSql & vSql)

            cons = DBMySql.CalcConsec("BFCIM", "99999999")
            mkSql_h(" INSERT INTO BFCIM ( ")
            mkSql_h(" VALUES ( ")
            mkSql_d("'", " IFUENTE", "PRODUCCION", 0)                       '//15A
            mkSql_d("'", " INID   ", "IN", 0)                   '//2A    Record Identifier
            mkSql_d(" ", " IBATCH ", batch, 0)                          '//8
            mkSql_d("'", " INTRAN ", "PR", 0)               '//2A    Transaction Type
            mkSql_d(" ", " INCTNR ", 0, 0)            '//2A    Transaction Type
            mkSql_d("'", " INMFGR ", 0, 0)            '//2A    Transaction Type
            mkSql_d(" ", " INDATE ", DB2_DATEDEC(), 0)                  '//8P0   Date		
            mkSql_d(" ", " INTRN  ", Right(cons, 5), 0)                  '//5P0   INTRN
            mkSql_d("'", " INMLOT  ", "RP" & cons, 0)                '//5P0   INTRN
            mkSql_d("'", " INREAS ", "01", 0)                  '//5P0   INTRN
            mkSql_d(" ", " INREF  ", rs("TORD").Value, 0)                  '//8P0   Date
            mkSql_d("'", " INPROD ", rs("TPROD").Value, 0)               '//15A   Item Number
            mkSql_d("'", " INWHSE ", rs("PBODPT").Value, 0)                 '//2A    Warehouse
            mkSql_d("'", " INLOT  ", rs("PLOTE").Value, 0)                   '//10A   Lot Number
            mkSql_d("'", " INLOC  ", rs("PLOCPT").Value, 0)              '//6A    Location Code

            canti = (CDbl(rs("TQREP").Value) - CDbl(rs("TQREC").Value))
            mkSql_d("'", " INQTY  ", canti, 0)                '//10A   Creted by
            mkSql_d("'", " INCBY  ", "COSIS001", 0)                '//10A   Creted by
            mkSql_d(" ", " INCDT  ", DB2_DATEDEC(), 0)      '//8P0   Date Created
            mkSql_d(" ", " INCTM  ", DB2_TIMEDEC(), 1)      '//6P0   Time Created

            DB.ExecuteSQL(fSql & vSql)
            DBMySql.ExecuteSQL(fSql & vSql)

            fSql = "UPDATE BFREP SET TQREC = TQREC + " & canti & " WHERE TNUMREP = " & rs("TNUMREP").Value
            DBMySql.ExecuteSQL(fSql)

            rs.MoveNext()
        End While
        rs.Close()
        fSql = "{{ CALL PGM(" & paramWebConfig(ubicWebConfig, "AS400_PGM") & "/BCTRAINVB) PARM('" & batch & "') }}"
        DB.ExecuteSQL(fSql)
        wsResult = "Transacciones enviadas"

    End Sub

    Function registroWS(tipo As String, op As String) As Boolean
        Dim rs As New ADODB.Recordset
        Dim servWin As String = "TareasBellota"
        Dim fSql, vSql As String
        Dim resultado As Boolean = True
        Dim hora As Integer

        rw(tipo, op)
        If wsNum <> "" Then
            fSql = "UPDATE BFPARAM SET CCNOT1 = '" & wsResult & "'"
            fSql &= ", CCMNDT    ='" & Format(Now(), "yyy-MM-dd HH:mm:ss") & "' "     '//Z     CCENDT
            fSql &= " WHERE CCNUM = " & wsNum
            DBMySql.ExecuteSQL(fSql)
        End If

        If op <> "" Then
            fSql = "SELECT * FROM BFPARAM"
            fSql = fSql & " WHERE "
            fSql = fSql & " CCTABL = '" & servWin & "' "
            fSql = fSql & " AND CCCODE = '02' "
            fSql = fSql & " AND CCCODE2 = '" & op & "' "
            If Not DBMySql.OpenRS(rs, fSql) Then
                fSql = " INSERT INTO BFPARAM( "
                vSql = " VALUES ( "
                fSql = fSql & " CCID    , " : vSql = vSql & "'CC', "                '//A     CCID
                fSql = fSql & " CCCODE  , " : vSql = vSql & "'02', "                '//A     CCCODE
                fSql = fSql & " CCCODE2  , " : vSql = vSql & "'" & op & "', "                '//A     CCCODE
                fSql = fSql & " CCTABL )  " : vSql = vSql & "'" & servWin & "') "   '//A     CCTABL
                DBMySql.ExecuteSQL(fSql & vSql)
                rs.Requery()
            End If
            wsNum = rs("CCNUM").Value
            rs.Close()
            fSql = "UPDATE BFPARAM SET CCNOT1 = 'Inicia Rutina'"
            fSql &= ", CCENDT = '" & Format(Now(), "yyy-MM-dd HH:mm:ss") & "' "     '//Z     CCENDT
            fSql &= " WHERE "
            fSql &= " CCTABL = '" & servWin & "' "
            fSql &= " AND CCCODE = '02' "
            fSql &= " AND CCCODE2 = '" & op & "' "
            DBMySql.ExecuteSQL(fSql)
        End If


        fSql = "SELECT * FROM BFPARAM"
        fSql = fSql & " WHERE "
        fSql = fSql & " CCTABL = '" & servWin & "' "
        fSql = fSql & " AND CCCODE = '01' "
        If Not DBMySql.OpenRS(rs, fSql) Then
            fSql = " INSERT INTO BFPARAM( "
            vSql = " VALUES ( "
            fSql = fSql & " CCID    , " : vSql = vSql & "'CC', "                '//A     CCID
            fSql = fSql & " CCCODE  , " : vSql = vSql & "'01', "                '//A     CCCODE
            fSql = fSql & " CCTABL )  " : vSql = vSql & "'" & servWin & "') "   '//A     CCTABL
            DBMySql.ExecuteSQL(fSql & vSql)
        Else
            Select Case tipo
                Case "Inicio"
                    resultado = False
                    If IsDBNull(rs("CCUDC1").Value) Then
                        resultado = True
                    ElseIf rs("CCUDC1").Value = 1 Then
                        resultado = True
                    End If
                Case "Actualiza PR"
                    resultado = False
                    hora = Hour(Now())
                    If IsDBNull(rs("CCUDV1").Value) Then
                        resultado = True
                    ElseIf hora <> rs("CCUDV1").Value Then
                        resultado = True
                    End If
            End Select
        End If
        rs.Close()

        fSql = " UPDATE BFPARAM SET "
        fSql = fSql & " CCUDTS    ='" & Format(Now(), "yyy-MM-dd HH:mm:ss") & "' "     '//Z     CCUDTS
        Select Case tipo
            Case "Inicio"
                Dim App As clsApp = New clsApp(System.Reflection.Assembly.GetExecutingAssembly)
                Dim gsAppVersion = App.Major.ToString & "." & App.Minor.ToString & "." & App.Revision.ToString
                fSql = fSql & ", CCDESC    ='" & gsAppVersion & "' "     '//Z     CCENDT
                ' My.Application.Info.ProductName
                fSql = fSql & ", CCNOTE    ='" & My.Application.Info.ProductName & "' "     '//Z     CCENDT
                fSql = fSql & ", CCNOT2    ='" & Replace(ubicWebConfig, "\", "\\") & "' "     '//Z     CCENDT
                fSql = fSql & ", CCENDT    ='" & Format(Now(), "yyy-MM-dd HH:mm:ss") & "' "     '//Z     CCENDT
            Case "Fin"
                fSql = fSql & ", CCMNDT    ='" & Format(Now(), "yyy-MM-dd HH:mm:ss") & "' "     '//Z     CCENDT
            Case "Operacion"
                fSql = fSql & ", CCNOT1    ='" & op & "' "     '//A     CCNOT1
            Case "Actualiza PR"
                If resultado Then
                    fSql = fSql & ", CCUDV1    = " & hora & " "      '//P     CCUDV1
                    fSql = fSql & ", CCNOT1    ='" & op & "' "     '//A     CCNOT1
                End If
        End Select
        fSql = fSql & " WHERE "
        fSql = fSql & " CCTABL = '" & servWin & "' AND CCCODE = '01'"
        DBMySql.ExecuteSQL(fSql)
        Return resultado

    End Function


    Function comi(txt) As String
        Return Replace(CStr(txt), "'", "''")
    End Function

End Module
