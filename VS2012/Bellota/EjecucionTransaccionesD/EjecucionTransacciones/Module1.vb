﻿
Imports System.IO


Module Module1
    Dim gsConnString
    ' Dim conexion As MySqlConnection
    'Dim conexion1 As MySqlConnection
    Dim DB_DB2
    Dim IdWork As Integer
    Dim Orden As String
    Dim Usuario As String
    Dim sRenglon As String = Nothing
    Dim strStreamW As StreamWriter = Nothing
    Dim strStreamW1 As Stream = Nothing
    Dim fic As String
    Dim strStreamWriter As StreamWriter = Nothing
    Dim ContenidoArchivo As String = Nothing
    ' Donde guardamos los paths de los archivos que vamos a estar utilizando ..
    Dim PathArchivo As String
    Dim Command


    Function Transaccion(ByVal IdWork As Integer, ByVal Orden As String, ByVal Usuario As String)
        Dim reas
        Dim cons As String
        Dim batch As String = DBMySql.CalcConsec("BFCIM", "99999999")
        Dim rsss As New ADODB.Recordset
        fSql = "Select * from " & paramWebConfig(My.Settings.WebConfig, "MYSQL_LIB") & ".BFREPCONCD where CNUMREP = " & IdWork & " AND PROCESADO = 0 "
        DBMySql.OpenRS(rsss, fSql)
        If rsss.EOF Then
            Return 0
        Else
            DB.abrirConexionAS400()
        End If
        While Not rsss.EOF

            ' While Not rs.EOF
            cons = DBMySql.CalcConsec("CIMNUM", "99999999")
            reas = "01"

            mkSql_h(" INSERT INTO BFCIM ( ")
            mkSql_h(" VALUES ( ")
            mkSql_d("'", " IFUENTE", "PRODUCCION", 0)                       '//15A
            mkSql_d(" ", " IBATCH ", batch, 0)                          '//8
            mkSql_d("'", " INID   ", "IN", 0)                   '//2A    Record Identifier
            mkSql_d("'", " INTRAN ", "IT", 0)               '//2A    Transaction Type
            mkSql_d(" ", " INCTNR ", 0, 0)            '//2A    Transaction Type
            mkSql_d("'", " INMFGR ", 0, 0)            '//2A    Transaction Type
            'sql.mkSql_d  "'", " INTRAN ", Proceso, 0       		'//2A    Transaction Type
            mkSql_d(" ", " INDATE ", DB2_DATEDEC(), 0)                  '//8P0   Date		
            mkSql_d(" ", " INTRN  ", Right(cons, 5), 0)                   '//5P0   INTRN
            mkSql_d("'", " INMLOT  ", "RP" & Ceros(cons, 8), 0)                   '//5P0   INTRN
            mkSql_d("'", " INREAS ", "01", 0)                  '//5P0   INTRN
            mkSql_d(" ", " INREF  ", Orden, 0)                  '//8P0   Date
            mkSql_d("'", " INPROD ", rsss("CPROD").Value, 0)               '//15A   Item Number
            mkSql_d("'", " INWHSE ", rsss("CQBOD").Value, 0)                 '//2A    Warehouse
            mkSql_d("'", " INLOT  ", "", 0)                   '//10A   Lot Number
            mkSql_d("'", " INLOC  ", "", 0)              '//6A    Location Code
            mkSql_d(" ", " INQTY  ", Replace(rsss("CQCON").Value, ",", "."), 0)              '//11P3  Month to Date Adjustments
            mkSql_d("'", " INCBY  ", Usuario, 0)                '//10A   Creted by
            mkSql_d(" ", " INCDT  ", DB2_DATEDEC(), 0)      '//8P0   Date Created
            mkSql_d(" ", " INCTM  ", DB2_TIMEDEC(), 1)      '//6P0   Time Created
            DB.ExecuteSQL(fSql & vSql)
            DBMySql.ExecuteSQL(fSql & vSql)

            fSql = "UPDATE " & paramWebConfig(My.Settings.WebConfig, "MYSQL_LIB") & ".BFREPCONCD  SET NUMREGISTO = " & cons & ", PROCESADO = 1 where CNUMREP = " & IdWork & " AND CNUMCON = " & rsss("CNUMCON").Value & " AND NUMREGISTO = 0 "
            DBMySql.ExecuteSQL(fSql)
            rsss.MoveNext()
        End While
        rsss.Close()


        fSql = "{{ CALL PGM(" & paramWebConfig400("AS400_PGM") & "/BCTRAINVB) PARM('" & batch & "' 'PROD' ) }}"
        DB.ExecuteSQL(fSql)



    End Function




    Function Ceros(ByVal Val As Object, ByVal C As Integer)

        If CInt(C) > Len(Trim(Val)) Then
            Ceros = Right(RepiteChar(CInt(C), "0") & Trim(Val), CInt(C))
        Else
            Ceros = Val
        End If

    End Function

    Function RepiteChar(ByVal Cant As Integer, ByVal Txt As Char) As String
        Dim i As Integer

        RepiteChar = ""
        For i = 1 To Cant
            RepiteChar = RepiteChar & Txt
        Next

    End Function

    Sub Main(ByVal Id As String())
        Dim resultado As String = ""
        fic = My.Settings.ArchivoLog & IdWork & Date.Now.Day & Date.Now.Month & Date.Now.Year & Date.Now.Hour & Date.Now.Minute & Date.Now.Second & Date.Now.Millisecond & ".txt"

        'Console.WriteLine("Ingreso")
        Try
            IdWork = Id(0)
            Orden = Id(1)
            Usuario = Id(2)
            System.Threading.Thread.CurrentThread.CurrentCulture = New System.Globalization.CultureInfo("es-CO")
            System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyDecimalSeparator = "."
            System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.CurrencyGroupSeparator = ","
            System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator = "."
            System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberGroupSeparator = ","
            resultado = DBMySql.abrirConexionMySql()
            If resultado = "" Then
                Transaccion(IdWork, Orden, Usuario)
            End If
        Catch ex As Exception

            DBMySql.WrtSqlError(lastSQL, ex.Message & "<br />" & ex.StackTrace.Replace(" en ", "<br />"))
        End Try
    End Sub



End Module
