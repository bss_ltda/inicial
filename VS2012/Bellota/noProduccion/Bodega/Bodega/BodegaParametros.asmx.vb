﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class BodegaParametros
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function VerParametros() As String()
        Dim Result As String()

        ReDim Result(11)

        'Result(0) = ParmItem("Servidor", My.Settings.SERVER)
        'Result(1) = ParmItem("Usuario", My.Settings.USER)
        'Result(2) = ParmItem("Biblioteca", My.Settings.LIBRARY)
        'Result(3) = ParmItem("Programas", My.Settings.LIB_PGM)
        'Result(4) = ParmItem("IWM", My.Settings.IWM)
        'Result(5) = ParmItem("ECL", My.Settings.ECL)
        'Result(6) = ParmItem("ILI", My.Settings.ILI)
        'Result(7) = ParmItem("ELA", My.Settings.ELA)
        'Result(8) = ParmItem("CIM", My.Settings.CIM)
        'Result(9) = ParmItem("ILM", My.Settings.ILM)
        'Result(10) = ParmItem("HPO", My.Settings.HPO)
        'Return Result
    End Function

    <WebMethod()> _
    Public Function CambiarParametros(
                                ByVal CarpetaWeb As String, _
                                ByVal Servidor As String, _
                                ByVal Usuario As String, _
                                ByVal Password As String, _
                                ByVal Lib_BPCS As String, _
                                ByVal Lib_Pgms As String, _
                                ByVal Lib_IWM As String, _
                                ByVal Lib_ECL As String, _
                                ByVal Lib_ELA As String, _
                                ByVal Lib_ILI As String, _
                                ByVal Lib_CIM As String, _
                                ByVal Lib_ILM As String, _
                                ByVal Lib_HPO As String _
                                ) As String

        Servidor = IIf(Servidor = "", My.Settings.SERVER, Servidor)
        Usuario = IIf(Usuario = "", My.Settings.USER, Usuario)
        Password = IIf(Password = "", My.Settings.PASSWORD, Password)
        Lib_BPCS = IIf(Lib_BPCS = "", My.Settings.LIBRARY, Lib_BPCS)
        Lib_Pgms = IIf(Lib_Pgms = "", My.Settings.LIB_PGM, Lib_Pgms)
        Lib_IWM = IIf(Lib_IWM = "", My.Settings.IWM, Lib_IWM)
        Lib_ECL = IIf(Lib_ECL = "", My.Settings.ECL, Lib_ECL)
        Lib_ILI = IIf(Lib_ILI = "", My.Settings.ILI, Lib_ILI)
        Lib_ELA = IIf(Lib_ELA = "", My.Settings.ELA, Lib_ELA)
        Lib_CIM = IIf(Lib_CIM = "", My.Settings.CIM, Lib_CIM)
        Lib_ILM = IIf(Lib_ILM = "", My.Settings.ILM, Lib_ILM)
        Lib_HPO = IIf(Lib_HPO = "", My.Settings.ILM, Lib_HPO)

        CarpetaWeb &= "\"
        CarpetaWeb = Replace(CarpetaWeb, "\\", "\")

        '     Return (SetParametros(CarpetaWeb, Servidor, Usuario, Password, Lib_BPCS, Lib_Pgms, _
        '                  Lib_IWM, Lib_ECL, Lib_ILI, Lib_ELA, Lib_CIM, Lib_ILM, Lib_HPO))

    End Function

    <WebMethod()> _
    Public Function Version() As String

        Return ("(" & My.Application.Info.Version.Major & "." & _
                 My.Application.Info.Version.Minor & "." & _
                 My.Application.Info.Version.Revision & ")")

    End Function

End Class