﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

<System.Web.Services.WebService(Namespace:="http://soap.bellota.co/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class SistemaDeBodega
    Inherits System.Web.Services.WebService

    'http://localhost:82/Bodega/Bodega.asmx

    <WebMethod()> _
    Public Function Traslado(
                                ByVal Producto As String, _
                                ByVal Lote As String, _
                                ByVal Bodega_Desde As String, _
                                ByVal Ubicacion_Desde As String, _
                                ByVal Bodega_Hacia As String, _
                                ByVal Ubicacion_Hacia As String, _
                                ByVal Cantidad As Double, _
                                ByVal OrdenFabricacion As Integer, _
                                ByVal CodigoCausa As String, _
                                ByVal Fecha As String, _
                                ByVal Usuario As String _
                                ) As String

        Dim sql As New mk_Sql
        Dim Result As String = ""
        Dim sTCOM As String
        Dim rs As New ADODB.Recordset
        Dim fSql As String
        Dim DB As New ExecuteSQL
        Dim FechaTransaccion As Integer
        Dim chkFecha As Date

        Bodega_Desde = Trim(Bodega_Desde.ToUpper())
        Ubicacion_Desde = Trim(Ubicacion_Desde.ToUpper())
        Bodega_Hacia = Trim(Bodega_Hacia.ToUpper())
        Ubicacion_Hacia = Trim(Ubicacion_Hacia.ToUpper())

        If Cantidad <= 0 Then
            Return "9505: Error en Cantidad. "
        End If

        Try
            SetConexion(DB)
            If Not DB.Open() Then
                Return "9091: " & "Error Abriendo Conexion. " & Descripcion_Error
            End If
            LibParamLoad(DB)

            Select Case DB.Cursor(rs, "SELECT * FROM " & LibParam("ILM") & " WHERE WWHS='" & Bodega_Desde & "' AND WLOC='" & Ubicacion_Desde & "'")
                Case 0
                    Result = "9501 La Ubicación NO Existe. " & Bodega_Desde & "/" & Ubicacion_Desde
                Case -99
                    DB.Close()
                    Result = "9092 " & Descripcion_Error
            End Select
            rs.Close()

            DB.Cursor(rs, "SELECT * FROM " & LibParam("ILM") & " WHERE WWHS='" & Bodega_Hacia & "' AND WLOC='" & Ubicacion_Hacia & "'")
            If rs.EOF Then
                Result = "9502 La Ubicación NO Existe. " & Bodega_Hacia & "/" & Ubicacion_Hacia
            End If
            rs.Close()

            If IsDate(Left(Fecha, 4) & "-" & Mid(Fecha, 5, 2) & "-" & Right(Fecha, 2)) Then
                chkFecha = DateSerial(Left(Fecha, 4), Mid(Fecha, 5, 2), Right(Fecha, 2))
                If Year(chkFecha) * 100 + Month(chkFecha) = Year(Now()) * 100 + Month(Now()) Then
                    FechaTransaccion = CInt(Fecha)
                Else
                    Result = "9031 Fecha no corresponde al mes. Use AAAAMMDD. [" & Fecha & "]"
                End If
            Else
                Result = "9030 Error en Fecha. Use AAAAMMDD. [" & Fecha & "]"
            End If
            CodigoCausa = Left(CodigoCausa, 2)

            'If Result = "" Then
            '    fSql = " SELECT  "
            '    fSql &= " LOPB - LISSU + LADJU + LRCT - LIALOC AS SALDO"
            '    fSql &= " FROM " & LibParam("ILIL01")
            '    fSql &= " WHERE LPROD='" & Producto & "' AND LWHS='" & Bodega_Desde & "' AND LLOT='" & Lote & "' AND LLOC='" & Ubicacion_Desde & "'"
            '    DB.Cursor(rs, fSql)
            '    If Not rs.EOF Then
            '        If Cantidad > rs("SALDO").Value Then
            '            Result = "9503 No hay inventario disponible " & fSql
            '        End If
            '    Else
            '        Result = "9503 No hay inventario disponible " & fSql
            '    End If
            'End If

            If Result <> "" Then
                DB.Close()
                Return Result
            End If

            sTCOM = DB.CalcConsec("WMSTRAS", "99999")

            fSql = " UPDATE ILN SET LMRB = 'A'  WHERE LLOT = '" & Lote & "' AND LPROD = '" & Producto & "'"
            DB.ExecuteSQL(fSql)

            fSql = " CREATE ALIAS QTEMP.CIMWMS FOR " & LibParam("CIM") & "(" & LibParam("CIMMBR", "") & ") "
            DB.ExecuteSQL(fSql)

            '-------------------------------------------------------------------------------------------------------
            '                   DESDE
            '-------------------------------------------------------------------------------------------------------
            sql.mkSql(t_iF, " INSERT INTO QTEMP.CIMWMS( ")
            sql.mkSql(t_iV, " VALUES ( ")
            sql.mkSql(tStr, " INID      ", "IN", 0)                 '2A    Record Identifier
            sql.mkSql(tNum, " INTRN     ", sTCOM, 0)                '5P0   INTRN
            sql.mkSql(tNum, " INLINE    ", 1, 0)                    '3P0   Line Number
            sql.mkSql(tStr, " INPROD    ", Producto, 0)             '15A   Item Number
            sql.mkSql(tStr, " INWHSE    ", Bodega_Desde, 0)         '2A    Warehouse
            sql.mkSql(tNum, " INREF     ", OrdenFabricacion, 0)     '8P0   Orden de Fabricacion
            sql.mkSql(tStr, " INREAS    ", CodigoCausa, 0)          '2A    Codigo de Razon
            sql.mkSql(tStr, " INLOT     ", Lote, 0)                 '10A   Lot Number
            sql.mkSql(tStr, " INLOC     ", Ubicacion_Desde, 0)      '6A    Location Code
            sql.mkSql(tNum, " INQTY     ", -Cantidad, 0)            '11P3  Month to Date Adjustments
            sql.mkSql(tNum, " INDATE    ", FechaTransaccion, 0)     '8P0   Date
            sql.mkSql(tStr, " INTRAN    ", "T1", 0)                 '2A    Transaction Type
            sql.mkSql(tStr, " INCBY     ", Usuario, 0)              '10A   Creted by
            sql.mkSql(tNum, " INCDT     ", DB2_DATEDEC, 0)          '8P0   Date Created
            sql.mkSql(tNum, " INCTM     ", DB2_TIMEDEC0, 1)         '6P0   Time Created
            If sql.Insert(DB) = -99 Then
                Return sql.Sentencia() & vbCrLf & Descripcion_Error
            End If

            '-------------------------------------------------------------------------------------------------------
            '                   HACIA
            '-------------------------------------------------------------------------------------------------------
            sql.mkSql(t_iF, " INSERT INTO QTEMP.CIMWMS( ")
            sql.mkSql(t_iV, " VALUES ( ")
            sql.mkSql(tStr, " INID      ", "IN", 0)                 '2A    Record Identifier
            sql.mkSql(tNum, " INTRN     ", sTCOM, 0)                '5P0   INTRN
            sql.mkSql(tNum, " INLINE    ", 2, 0)                    '3P0   Line Number
            sql.mkSql(tStr, " INPROD    ", Producto, 0)             '15A   Item Number
            sql.mkSql(tStr, " INWHSE    ", Bodega_Hacia, 0)         '2A    Warehouse
            sql.mkSql(tNum, " INREF     ", OrdenFabricacion, 0)     '8P0   Orden de Fabricacion
            sql.mkSql(tStr, " INREAS    ", CodigoCausa, 0)          '2A    Codigo de Razon
            sql.mkSql(tStr, " INLOT     ", Lote, 0)                 '10A   Lot Number
            sql.mkSql(tStr, " INLOC     ", Ubicacion_Hacia, 0)      '6A    Location Code
            sql.mkSql(tNum, " INQTY     ", Cantidad, 0)             '11P3  Month to Date Adjustments
            sql.mkSql(tNum, " INDATE    ", FechaTransaccion, 0)     '8P0   Date
            sql.mkSql(tStr, " INTRAN    ", "T1", 0)                 '2A    Transaction Type
            sql.mkSql(tStr, " INCBY     ", Usuario, 0)              '10A   Creted by
            sql.mkSql(tNum, " INCDT     ", DB2_DATEDEC, 0)          '8P0   Date Created
            sql.mkSql(tNum, " INCTM     ", DB2_TIMEDEC0, 1)         '6P0   Time Created
            If sql.Insert(DB) = -99 Then
                Return sql.Sentencia() & vbCrLf & Descripcion_Error
            End If

            'fSql = " SBMJOB CMD(CALL PGM(BHCIM600BE)) JOB(TRASLADO) "

            Result = DB.Call_PGM("CALL PGM(" & LibParam("PGMS") & "BCTRAINV)")
            DB.Close()

            If Result = "OK" Then
                Return "0000 Traslado Realizado: " & sTCOM
            Else
                Return Result
            End If

        Catch ex As Exception
            Return ex.Message
        End Try

    End Function
    <WebMethod()> _
    Public Function Asignacion(ByVal Pedido As Integer, _
                                ByVal Linea As Integer, _
                                ByVal Producto As String, _
                                ByVal Bodega As String, _
                                ByVal Ubicacion As String, _
                                ByVal Lote As String, _
                                ByVal Cantidad As Double, _
                                ByVal Usuario As String
                                ) As String
        Dim sql As New mk_Sql
        Dim Result As String = ""
        Dim rs As New ADODB.Recordset
        Dim fSql As String
        Dim DB As New ExecuteSQL


        Producto = Trim(Producto.ToUpper())
        Bodega = Trim(Bodega.ToUpper())
        Ubicacion = Trim(Ubicacion.ToUpper())
        Lote = Trim(Lote.ToUpper())

        If Cantidad <= 0 And Linea <> -99 Then
            Return "9505: Error en Cantidad. "
        End If

        SetConexion(DB)
        If Not DB.Open() Then
            Return "9091: " & Descripcion_Error
        End If
        LibParamLoad(DB)

        'Ejcuta confirmacion y facturacion
        If Linea = -99 Then
            Result = ""
            Result = DB.Call_PGM("CALL PGM(" & LibParam("PGMS", "") & "/BCCONFIR) PARM('" & Ceros(Pedido, 6) & "')")
            If Result = "OK" Then
                Result = "0000 Pedido " & Ceros(Pedido, 6) & " Confirmado"
            End If
            Return Result
        End If


        DB.Sentencia = "SELECT * FROM " & LibParam("ILM") & " WHERE WWHS='" & Bodega & "' AND WLOC='" & Ubicacion & "'"
        Select Case DB.Cursor(rs)
            Case 1
                Result = ""
            Case 0
                Result = "9400 La Ubicación NO Existe. " & Bodega & "/" & Ubicacion
            Case -99
                DB.Close()
                Return "9092:" & DB.DspErr()
        End Select
        rs.Close()

        DB.Sentencia = "SELECT LPROD, LWHS FROM " & LibParam("ECL") & " WHERE LORD=" & Pedido & " AND LLINE=" & Linea
        Select Case DB.Cursor(rs)
            Case 1
                Result = ""
            Case 0
                Result = "9500 Pedido NO Existe. " & Pedido & "/" & Linea
            Case -99
                DB.Close()
                Return "9092:" & DB.DspErr()
        End Select

        If rs("LPROD").Value <> Producto Then
            Result = "9600 Producto no coincide. " & rs("LPROD").Value & "/" & Producto
        ElseIf rs("LWHS").Value <> Bodega Then
            Result = "9700 Bodega NO Coincide. " & rs("LWHS").Value & "/" & Bodega
        End If
        rs.Close()

        If Result = "" Then
            fSql = " SELECT  "
            fSql &= " LOPB - LISSU + LADJU + LRCT - LIALOC AS SALDO"
            fSql &= " FROM " & LibParam("ILIL01")
            fSql &= " WHERE LPROD='" & Producto & "' AND LWHS='" & Bodega & "' AND LLOT='" & Lote & "' AND LLOC='" & Ubicacion & "'"
            Select Case DB.Cursor(rs, fSql)
                Case 1
                    If Cantidad > rs("SALDO").Value Then
                        Result = "9503 No hay inventario disponible"
                    End If
                Case 0
                    Result = "9503 No hay inventario disponible"
                Case -99
                    DB.Close()
                    Return "9092:" & DB.DspErr()
            End Select
            rs.Close()
        End If

        If Result <> "" Then
            DB.Close()
            Return Result
        End If

        '-------------------------------------------------------------------------------------------------------
        '                   ASIGNACION
        '-------------------------------------------------------------------------------------------------------
        fSql = " UPDATE ILN SET LMRB = 'A'  WHERE LLOT = '" & Lote & "' AND LPROD = '" & Producto & "'"
        DB.ExecuteSQL(fSql)

        sql.mkSql(t_iF, " INSERT INTO " & LibParam("ELA") & "( ")
        sql.mkSql(t_iV, " VALUES ( ")
        sql.mkSql(tStr, " AID       ", "LA", 0)         '2A    Record Identifier
        sql.mkSql(tStr, " ATYPE     ", "C", 0)          '1A    C=cust order S=shop order
        sql.mkSql(tNum, " AORD      ", Pedido, 0)       '6P0   Shop order#/Customer order#
        sql.mkSql(tNum, " ALINE     ", Linea, 0)        '3P0   Line #
        sql.mkSql(tNum, " ASEQ      ", 1, 0)            '3P0   Sequence Number
        sql.mkSql(tStr, " APROD     ", Producto, 0)     '15A   Item Number
        sql.mkSql(tStr, " AWHS      ", Bodega, 0)       '2A    Warehouse
        sql.mkSql(tStr, " ALOC      ", Ubicacion, 0)    '6A    Location
        sql.mkSql(tStr, " ALOT      ", Lote, 0)         '10A   Lot #
        sql.mkSql(tNum, " LQALL     ", Cantidad, 1)     '11P3  Quantity Allocated
        If sql.Insert(DB) = -99 Then
            DB.Close()
            Return sql.Sentencia() & vbCrLf & Descripcion_Error
        End If

        fSql = "UPDATE " & LibParam("ECL") & " SET LQALL = LQALL + " & Cantidad & " WHERE LORD=" & Pedido & " AND LLINE=" & Linea
        If DB.ExecuteSQL(fSql) = -99 Then
            DB.Close()
            Return sql.Sentencia() & vbCrLf & Descripcion_Error
        End If

        fSql = " UPDATE " & LibParam("ILIL01") & " SET LIALOC = LIALOC + " & Cantidad
        fSql &= " WHERE LPROD='" & Producto & "' AND LWHS='" & Bodega & "' AND LLOT='" & Lote & "' AND LLOC='" & Ubicacion & "'"
        If DB.ExecuteSQL(fSql) = -99 Then
            DB.Close()
            Return sql.Sentencia() & vbCrLf & Descripcion_Error
        End If

        Result = "0000 Asignacion Realizada"

        DB.Close()
        Return Result

    End Function
    <WebMethod()> _
    Public Function CreaUbicacion(ByVal Bodega As String, _
                            ByVal Ubicacion As String, _
                            ByVal Descripcion As String, _
                            ByVal CentroDeCosto As String
                          ) As String
        Dim rs As New ADODB.Recordset
        Dim Result As String = ""
        Dim DB As New ExecuteSQL

        Bodega = Trim(Bodega.ToUpper())
        Ubicacion = Trim(Ubicacion.ToUpper())
        Descripcion = Trim(Descripcion.ToUpper())
        CentroDeCosto = Trim(CentroDeCosto.ToUpper())

        If Bodega.Length > 2 Then
            Return "9901 Longitud Codigo de Bodega"
        ElseIf Ubicacion.Length > 6 Then
            Return "9901 Longitud Ubicacion"
        ElseIf Descripcion.Length > 30 Then
            Return "9901 Longitud Descripcion"
        ElseIf CentroDeCosto.Length > 10 Then
            Return "9901 Longitud Centro De Costo"
        End If

        SetConexion(DB)
        If Not DB.Open() Then
            Return "9091: " & Descripcion_Error
        End If
        LibParamLoad(DB)

        Select Case DB.Cursor(rs, "SELECT * FROM " & LibParam("ILM") & " WHERE WWHS='" & Bodega & "' AND WLOC='" & Ubicacion & "'")
            Case -99
                DB.Close()
                Return DB.DspErr()
            Case 1
                Result = "9100 La Ubicación ya Existe."
        End Select
        rs.Close()
        If Result = "" Then
            DB.Sentencia = "SELECT * FROM " & LibParam("IWML01") & " WHERE LWHS ='" & Bodega & "'"
            Select Case DB.Cursor(rs)
                Case -99 'Error en la sentencia
                    DB.Close()
                    Return DB.DspErr()
                Case 0 'No encontró registro
                    Result = "9200 Bodega No Existe."
            End Select
            rs.Close()
        End If

        If Result = "" Then
            Dim sql As New mk_Sql
            sql.mkSql(t_iF, " INSERT INTO " & LibParam("ILM") & "( ")
            sql.mkSql(t_iV, " VALUES ( ")
            sql.mkSql(tStr, " WID       ", "WL", 0)                 '2A    Record ID; WL/WZ
            sql.mkSql(tStr, " WWHS      ", Bodega, 0)               '2A    Warehouse Code
            sql.mkSql(tStr, " WLOC      ", Ubicacion, 0)            '6A    Location Code
            sql.mkSql(tStr, " WFLOC     ", "1", 0)                  '1A    Default Forced Loc Flag
            sql.mkSql(tStr, " WLTYP     ", "P", 0)                  '1A    Location Type
            sql.mkSql(tStr, " WDESC     ", Descripcion, 0)          '30A   Description
            sql.mkSql(tStr, " LMPRF     ", CentroDeCosto, 1)        '10A   Profit Center
            If sql.Insert(DB) = -99 Then
                DB.Close()
                Return sql.Sentencia() & vbCrLf & Descripcion_Error
            End If

            DB.Sentencia = "SELECT * FROM " & LibParam("ILM") & " WHERE WWHS='" & Bodega & "' AND WLOC='" & Ubicacion & "'"
            Select Case DB.Cursor(rs)
                Case 1
                    Result = "0000 Ubicación Creada."
                Case 0
                    Result = "9300 Ubicación NO Creada."
                Case -99
                    DB.Close()
                    Return "9092:" & DB.DspErr()
            End Select
            rs.Close()
        End If

        DB.Close()

        Return Result

    End Function

    <WebMethod()> _
    Public Function EntradaPorCompra(
                                ByVal Fecha As String, _
                                ByVal OrdenCompra As Integer, _
                                ByVal Producto As String, _
                                ByVal Lote As String, _
                                ByVal Bodega As String, _
                                ByVal Ubicacion As String, _
                                ByVal Cantidad As Double, _
                                ByVal Valor As Double, _
                                ByVal Usuario As String _
                                ) As String

        Dim sql As New mk_Sql
        Dim Result As String = ""
        Dim sTCOM As String
        Dim rs As New ADODB.Recordset
        Dim fSql As String
        Dim CantidadMax As Double
        Dim CantidadActual As Double
        Dim UltimaLinea As Integer
        Dim DB As New ExecuteSQL
        Dim FechaTransaccion As Integer
        Dim chkFecha As Date

        If Cantidad <= 0 Then
            Return "9505: Error en Cantidad. "
        End If

        Bodega = Trim(Bodega.ToUpper())
        Ubicacion = Trim(Ubicacion.ToUpper())

        If IsDate(Left(Fecha, 4) & "-" & Mid(Fecha, 5, 2) & "-" & Right(Fecha, 2)) Then
            chkFecha = DateSerial(Left(Fecha, 4), Mid(Fecha, 5, 2), Right(Fecha, 2))
            If Year(chkFecha) * 100 + Month(chkFecha) = Year(Now()) * 100 + Month(Now()) Then
                FechaTransaccion = CInt(Fecha)
            Else
                Return "9031 Fecha no corresponde al mes. Use AAAAMMDD. [" & Fecha & "]"
            End If
        Else
            Return "9030 Error en Fecha. Use AAAAMMDD. [" & Fecha & "]"
        End If

        SetConexion(DB)
        If Not DB.Open() Then
            Return "9091: Error en Conexion. " & Descripcion_Error
        End If
        LibParamLoad(DB)

        Select Case DB.Cursor(rs, "SELECT * FROM " & LibParam("ILM") & " WHERE WWHS='" & Bodega & "' AND WLOC='" & Ubicacion & "'")
            Case 0
                Result = "9501 La Ubicación NO Existe. " & Bodega & "/" & Ubicacion
            Case -99
                DB.Close()
                Return "9092:" & DB.DspErr()
        End Select
        rs.Close()

        If Result = "" Then
            fSql = " SELECT * "
            fSql &= " FROM " & LibParam("HPOL01")
            fSql &= " WHERE PORD=" & OrdenCompra
            fSql &= " AND PPROD='" & Producto & "'"
            DB.Cursor(rs, fSql)
            If rs.EOF Then
                Result = "9541 No se encontró Orden: " & OrdenCompra & " Producto: " & Producto
            Else
                rs.Close()
                fSql = " SELECT  "
                fSql &= " IFNULL( SUM(PQORD), 0 ) AS PQORD, IFNULL( SUM(PQREC), 0 ) AS PQREC, IFNULL( MAX(PLINE), 0 ) AS MAXLINE "
                fSql &= " FROM " & LibParam("HPOL01")
                fSql &= " WHERE PORD=" & OrdenCompra
                fSql &= " AND PPROD='" & Producto & "'"
                Select Case DB.Cursor(rs, fSql)
                    Case 1
                        CantidadMax = rs("PQORD").Value * 1.05
                        CantidadActual = rs("PQREC").Value
                        UltimaLinea = rs("MAXLINE").Value
                        If rs("PQREC").Value + Cantidad > rs("PQORD").Value * 1.05 Then
                            Result = "9542 Cantidad recibida excede el limite."
                        End If
                    Case 0
                        Result = "9541 No se encontró Orden: " & OrdenCompra & " Producto: " & Producto
                    Case -99
                        DB.Close()
                        Return "9092:" & DB.DspErr()
                End Select
                rs.Close()
            End If
        End If
        If Result <> "" Then
            DB.Close()
            Return Result
        End If

        sTCOM = DB.CalcConsec("WMSTRAS", "99999")

        fSql = " UPDATE ILN SET LMRB = 'A'  WHERE LLOT = '" & Lote & "' AND LPROD = '" & Producto & "'"
        DB.ExecuteSQL(fSql)

        fSql = " CREATE ALIAS QTEMP.CIMWMS FOR " & LibParam("CIM") & "(" & LibParam("CIMMBR", "") & ") "
        If DB.ExecuteSQL(fSql) = -99 Then
            DB.Close()
            Return "9901 " & DB.DspErr()
        End If

        '-------------------------------------------------------------------------------------------------------
        '                   RECEPCION
        '-------------------------------------------------------------------------------------------------------

        fSql = " SELECT  * "
        fSql &= " FROM " & LibParam("HPOL01")
        fSql &= " WHERE PORD=" & OrdenCompra
        fSql &= " AND PPROD='" & Producto & "'"
        fSql &= " AND PQREC < PQORD"
        fSql &= " ORDER BY PLINE"
        If DB.Cursor(rs, fSql) = -99 Then
            DB.Close()
            Return "9092:" & DB.DspErr()
        End If

        Dim CantidadLinea As Double
        Dim NumLinea As Integer = 1

        Do While Not rs.EOF
            If rs("PLINE").Value = UltimaLinea Then
                CantidadLinea = Cantidad
                Cantidad = 0
            ElseIf rs("PQREC").Value + Cantidad > rs("PQORD").Value Then
                CantidadLinea = rs("PQORD").Value - rs("PQREC").Value
                Cantidad -= CantidadLinea
            Else
                CantidadLinea = Cantidad
                Cantidad = 0
            End If

            sql.mkSql(t_iF, " INSERT INTO QTEMP.CIMWMS( ")
            sql.mkSql(t_iV, " VALUES ( ")
            sql.mkSql(tStr, " INID      ", "IN", 0)                 '2A    Record Identifier
            sql.mkSql(tNum, " INTRN     ", sTCOM, 0)                '5P0   INTRN
            sql.mkSql(tNum, " INLINE    ", NumLinea, 0)                    '3P0   Line Number

            sql.mkSql(tNum, " INREF     ", OrdenCompra, 0)          '8P0   Ordene de Compra
            sql.mkSql(tNum, " INREF2    ", rs("PLINE").Value, 0)    '8P0   Linea de la Orden de Compra

            sql.mkSql(tStr, " INPROD    ", Producto, 0)             '15A   Item Number
            sql.mkSql(tStr, " INWHSE    ", Bodega, 0)               '2A    Warehouse
            sql.mkSql(tStr, " INREAS    ", "01", 0)                 '2A    Codigo de Razon
            sql.mkSql(tStr, " INLOT     ", Lote, 0)                 '10A   Lot Number
            sql.mkSql(tStr, " INLOC     ", Ubicacion, 0)            '6A    Location Code
            sql.mkSql(tNum, " INQTY     ", CantidadLinea, 0)             '11P3  Month to Date Adjustments
            sql.mkSql(tNum, " INCOST    ", Valor, 0)                '
            sql.mkSql(tNum, " INDATE    ", FechaTransaccion, 0)          '8P0   Date
            sql.mkSql(tStr, " INTRAN    ", "UC", 0)                 '2A    Transaction Type
            sql.mkSql(tStr, " INCBY     ", Usuario, 0)              '10A   Creted by
            sql.mkSql(tNum, " INCDT     ", DB2_DATEDEC, 0)          '8P0   Date Created
            sql.mkSql(tNum, " INCTM     ", DB2_TIMEDEC0, 1)         '6P0   Time Created
            If sql.Insert(DB) = -99 Then
                DB.Close()
                Return sql.Sentencia() & vbCrLf & Descripcion_Error
            End If
            If Cantidad = 0 Then
                Exit Do
            End If
            NumLinea += 1
            rs.MoveNext()
        Loop

        rs.Close()

        Result = DB.Call_PGM("CALL PGM(" & LibParam("PGMS", "") & "/BCTRAINV)")
        DB.Close()
        If Result = "OK" Then
            Return "0000 Entrada Realizada: " & sTCOM
        Else
            Return Result
        End If

    End Function

    <WebMethod()> _
    Public Function Devolucion_A_Proveedor(
                                ByVal Fecha As String, _
                                ByVal OrdenCompra As Integer, _
                                ByVal Producto As String, _
                                ByVal Lote As String, _
                                ByVal Bodega As String, _
                                ByVal Ubicacion As String, _
                                ByVal Cantidad As Double, _
                                ByVal Valor As Double, _
                                ByVal Usuario As String _
                                ) As String
        Dim sql As New mk_Sql
        Dim Result As String = ""
        Dim sTCOM As String
        Dim rs As New ADODB.Recordset
        Dim fSql As String
        Dim DB As New ExecuteSQL

        Bodega = Trim(Bodega.ToUpper())
        Ubicacion = Trim(Ubicacion.ToUpper())

        If Cantidad <= 0 Then
            Return "9505: Error en Cantidad. "
        End If

        Dim FechaTransaccion As Integer
        Dim chkFecha As Date

        If IsDate(Left(Fecha, 4) & "-" & Mid(Fecha, 5, 2) & "-" & Right(Fecha, 2)) Then
            chkFecha = DateSerial(Left(Fecha, 4), Mid(Fecha, 5, 2), Right(Fecha, 2))
            If Year(chkFecha) * 100 + Month(chkFecha) = Year(Now()) * 100 + Month(Now()) Then
                FechaTransaccion = CInt(Fecha)
            Else
                Return "9031 Fecha no corresponde al mes. Use AAAAMMDD. [" & Fecha & "]"
            End If
        Else
            Return "9030 Error en Fecha. Use AAAAMMDD. [" & Fecha & "]"
        End If

        SetConexion(DB)
        If Not DB.Open() Then
            Return "9091: " & Descripcion_Error
        End If
        LibParamLoad(DB)

        DB.Sentencia = "SELECT * FROM " & LibParam("ILM") & " WHERE WWHS='" & Bodega & "' AND WLOC='" & Ubicacion & "'"
        Select Case DB.Cursor(rs)
            Case 1
                Result = ""
            Case 0
                Result = "9501 La Ubicación NO Existe. " & Bodega & "/" & Ubicacion
            Case -99
                DB.Close()
                Return "9092:" & DB.DspErr()
        End Select
        rs.Close()

        If Result = "" Then
            fSql = " SELECT  "
            fSql &= " PLINE "
            fSql &= " FROM " & LibParam("HPOL01")
            fSql &= " WHERE PORD=" & OrdenCompra & " AND PPROD='" & Producto & "'"
            Select Case DB.Cursor(rs, fSql)
                Case 0
                    Result = "9541 No se encontró Orden/Linea"
                Case -99
                    DB.Close()
                    Return "9092:" & DB.DspErr()
            End Select
            rs.Close()
        End If

        If Result <> "" Then
            DB.Close()
            Return Result
        End If

        sTCOM = DB.CalcConsec("WMSTRAS", "99999")

        fSql = " CREATE ALIAS QTEMP.CIMWMS FOR " & LibParam("CIM") & "(" & LibParam("CIMMBR", "") & ") "
        DB.ExecuteSQL(fSql)

        '-------------------------------------------------------------------------------------------------------
        '                   DEVOLUCION
        '-------------------------------------------------------------------------------------------------------
        fSql = " UPDATE ILN SET LMRB = 'A'  WHERE LLOT = '" & Lote & "' AND LPROD = '" & Producto & "'"
        DB.ExecuteSQL(fSql)

        sql.mkSql(t_iF, " INSERT INTO QTEMP.CIMWMS( ")
        sql.mkSql(t_iV, " VALUES ( ")
        sql.mkSql(tStr, " INID      ", "IN", 0)                 '2A    Record Identifier
        sql.mkSql(tNum, " INTRN     ", sTCOM, 0)                '5P0   INTRN
        sql.mkSql(tNum, " INLINE    ", 1, 0)                    '3P0   Line Number
        sql.mkSql(tStr, " INPROD    ", Producto, 0)             '15A   Item Number
        sql.mkSql(tStr, " INWHSE    ", Bodega, 0)               '2A    Warehouse
        sql.mkSql(tStr, " INREAS    ", "01", 0)                 '2A    Codigo de Razon
        sql.mkSql(tStr, " INLOT     ", Lote, 0)                 '10A   Lot Number
        sql.mkSql(tStr, " INLOC     ", Ubicacion, 0)            '6A    Location Code
        sql.mkSql(tNum, " INQTY     ", Cantidad, 0)             '11P3  Month to Date Adjustments
        sql.mkSql(tNum, " INCOST    ", Valor, 0)                '

        sql.mkSql(tNum, " INREF     ", OrdenCompra, 0)          '8P0   Ordene de Compra
        sql.mkSql(tNum, " INREF2    ", rs("PLINE").Value, 0)    '8P0   Linea de la Orden de Compra

        sql.mkSql(tNum, " INDATE    ", FechaTransaccion, 0)     '8P0   Date
        sql.mkSql(tStr, " INTRAN    ", "DC", 0)                 '2A    Transaction Type
        sql.mkSql(tStr, " INCBY     ", Usuario, 0)              '10A   Creted by
        sql.mkSql(tNum, " INCDT     ", DB2_DATEDEC, 0)          '8P0   Date Created
        sql.mkSql(tNum, " INCTM     ", DB2_TIMEDEC0, 1)         '6P0   Time Created
        If sql.Insert(DB) = -99 Then
            DB.Close()
            Return sql.Sentencia() & vbCrLf & Descripcion_Error
        End If

        'fSql = " SBMJOB CMD(CALL PGM(BHCIM600BE)) JOB(TRASLADO) "

        DB.Call_PGM("CALL PGM(" & LibParam("PGMS", "") & "/BCTRAINV)")
        DB.Close()
        Return "0000 Entrada Realizada: " & sTCOM

    End Function


    <WebMethod()> _
    Public Function EntradaSobranteProduccion(
                                ByVal Fecha As String, _
                                ByVal Referencia As Integer, _
                                ByVal Producto As String, _
                                ByVal Lote As String, _
                                ByVal Bodega As String, _
                                ByVal Ubicacion As String, _
                                ByVal CodigoCausa As String, _
                                ByVal Cantidad As Double, _
                                ByVal Comentario As String, _
                                ByVal Usuario As String _
                                ) As String

        Return TransaccionEntrada("R1", Fecha, Referencia, Producto, Lote, Bodega, Ubicacion, CodigoCausa, Cantidad, 0, Comentario, Usuario)

    End Function

    <WebMethod()> _
    Public Function EntradaSobranteComercializados(
                                ByVal Fecha As String, _
                                ByVal Referencia As Integer, _
                                ByVal Producto As String, _
                                ByVal Lote As String, _
                                ByVal Bodega As String, _
                                ByVal Ubicacion As String, _
                                ByVal CodigoCausa As String, _
                                ByVal Cantidad As Double, _
                                ByVal Comentario As String, _
                                ByVal Usuario As String _
                                ) As String

        Return TransaccionEntrada("ES", Fecha, Referencia, Producto, Lote, Bodega, Ubicacion, CodigoCausa, Cantidad, 0, Comentario, Usuario)

    End Function

    <WebMethod()> _
    Public Function EntradaDevolucionClientes(
                                ByVal Fecha As String, _
                                ByVal Referencia As Integer, _
                                ByVal Producto As String, _
                                ByVal Lote As String, _
                                ByVal Bodega As String, _
                                ByVal Ubicacion As String, _
                                ByVal CodigoCausa As String, _
                                ByVal Cantidad As Double, _
                                ByVal Comentario As String, _
                                ByVal Usuario As String _
                                ) As String

        Return TransaccionEntrada("ED", Fecha, Referencia, Producto, Lote, Bodega, Ubicacion, CodigoCausa, Cantidad, 0, Comentario, Usuario)

    End Function

    <WebMethod()> _
    Public Function SalidaConsumoMachete(
                                ByVal Fecha As String, _
                                ByVal Referencia As Integer, _
                                ByVal Producto As String, _
                                ByVal Lote As String, _
                                ByVal Bodega As String, _
                                ByVal Ubicacion As String, _
                                ByVal CodigoCausa As String, _
                                ByVal Cantidad As Double, _
                                ByVal Comentario As String, _
                                ByVal Usuario As String _
                                ) As String

        Return TransaccionEntrada("SM", Fecha, Referencia, Producto, Lote, Bodega, Ubicacion, CodigoCausa, Cantidad, 0, Comentario, Usuario)

    End Function

    <WebMethod()> _
    Public Function SalidaDeLimas(
                                ByVal Fecha As String, _
                                ByVal Referencia As Integer, _
                                ByVal Producto As String, _
                                ByVal Lote As String, _
                                ByVal Bodega As String, _
                                ByVal Ubicacion As String, _
                                ByVal CodigoCausa As String, _
                                ByVal Cantidad As Double, _
                                ByVal Comentario As String, _
                                ByVal Usuario As String _
                                ) As String

        Return TransaccionEntrada("SL", Fecha, Referencia, Producto, Lote, Bodega, Ubicacion, CodigoCausa, Cantidad, 0, Comentario, Usuario)

    End Function


    <WebMethod()> _
    Public Function SalidaComercializados(
                                ByVal Fecha As String, _
                                ByVal Referencia As Integer, _
                                ByVal Producto As String, _
                                ByVal Lote As String, _
                                ByVal Bodega As String, _
                                ByVal Ubicacion As String, _
                                ByVal CodigoCausa As String, _
                                ByVal Cantidad As Double, _
                                ByVal Comentario As String, _
                                ByVal Usuario As String _
                                ) As String

        Return TransaccionEntrada("SC", Fecha, Referencia, Producto, Lote, Bodega, Ubicacion, CodigoCausa, Cantidad, 0, Comentario, Usuario)

    End Function

    <WebMethod()> _
    Public Function SalidaProductosEnAjuste(
                                ByVal Fecha As String, _
                                ByVal Referencia As Integer, _
                                ByVal Producto As String, _
                                ByVal Lote As String, _
                                ByVal Bodega As String, _
                                ByVal Ubicacion As String, _
                                ByVal CodigoCausa As String, _
                                ByVal Cantidad As Double, _
                                ByVal Comentario As String, _
                                ByVal Usuario As String _
                                ) As String

        Return TransaccionEntrada("SS", Fecha, Referencia, Producto, Lote, Bodega, Ubicacion, CodigoCausa, Cantidad, 0, Comentario, Usuario)

    End Function

    <WebMethod()> _
    Public Function ComercializadosTransformadosEntrada(
                                ByVal Fecha As String, _
                                ByVal OrdenFabricacion As Integer, _
                                ByVal Producto As String, _
                                ByVal Lote As String, _
                                ByVal Bodega As String, _
                                ByVal Ubicacion As String, _
                                ByVal CodigoCausa As String, _
                                ByVal Cantidad As Double, _
                                ByVal Comentario As String, _
                                ByVal Usuario As String _
                                ) As String

        Return TransaccionEntrada("R", Fecha, OrdenFabricacion, Producto, Lote, Bodega, Ubicacion, CodigoCausa, Cantidad, 0, Comentario, Usuario)

    End Function

    <WebMethod()> _
    Public Function ComercializadosTransformadosSalida(
                                ByVal Fecha As String, _
                                ByVal OrdenFabricacion As Integer, _
                                ByVal Producto As String, _
                                ByVal Lote As String, _
                                ByVal Bodega As String, _
                                ByVal Ubicacion As String, _
                                ByVal CodigoCausa As String, _
                                ByVal Cantidad As Double, _
                                ByVal Comentario As String, _
                                ByVal Usuario As String _
                                ) As String

        Return TransaccionEntrada("IT", Fecha, OrdenFabricacion, Producto, Lote, Bodega, Ubicacion, CodigoCausa, Cantidad, 0, Comentario, Usuario)

    End Function


End Class