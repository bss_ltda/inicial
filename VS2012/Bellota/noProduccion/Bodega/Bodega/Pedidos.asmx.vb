﻿
Imports System
Imports System.Globalization
Imports System.IO
Imports System.Collections
Imports System.Xml.Serialization
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Xml
Imports System.Net
Imports System.Xml.Linq
Imports System.Linq


<System.Web.Services.WebService(Namespace:="http://soap.bellota.co/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Pedidos
    Inherits System.Web.Services.WebService

    Structure CabeceraPedido
        Dim CustomerCode As String
        Dim ShiptoCode As String
        Dim RequestShipDate As String
        Dim CustomerPurchaseOrderCode As String
        Dim BidContractId As String
        Dim OrderNote As String


    End Structure


    Structure DetallePedido

        Dim ItemCode As Integer
        Dim OrderedSellingUnitQty As String
        Dim Price As String
        Dim LineNote As Double

    End Structure



    <WebMethod()> _
    Public Function CreaPedido(ByVal Pedido As String) As String()
        ' Dim xml As New Chilkat.Xml()

        Dim Cliente As Integer = 0
        Dim PuntoDeEnvio As Integer = 0
        Dim OrdenDeCompra As String = ""
        Dim NumeroDocumento As String = ""
        Dim FechaRequerido As String = ""
        Dim NotaPedido As String = ""
        Dim Usuario As String = "WEBPED"
        Dim numLinea As Integer = 1
        Dim Result() As String
        Dim cmdResult As String = ""
        Dim codErr As String = ""
        Dim Cant As String = "0"
        Dim Precio As String = "0"
        Dim msgTime As String
        Dim DB As New ExecuteSQL
        Dim rs As New ADODB.Recordset

        ReDim Result(0)
        '     xml.LoadXml(Pedido)
        'Dim documento As XDocument = XDocument.Parse(Pedido)
        '       Dim dato As Chilkat.Xml
        '       dato = xml.FirstChild()


        Dim nuevaLinea As DetallePedido
        Dim Cabeceraa As New CabeceraPedido
        Dim Lineas As New List(Of DetallePedido)
        

        Debug.Print("Conexion")
        Debug.Print(Now())
        SetConexion(DB)
        If Not DB.Open() Then
            ParmItem(Result, "9091", Descripcion_Error)
            Return Result
        End If
        LibParamLoad(DB)

        Debug.Print("Lee xml")
        Debug.Print(Now())
        Dim documento As XDocument = XDocument.Parse(Pedido)
        Dim qx = From xee In documento.Elements("Order")
      Select New With { _
               .CustomerCode = xee.Element("CustomerCode").Value,
               .ShiptoCode = xee.Element("ShiptoCode").Value,
               .RequestShipDate = xee.Element("RequestShipDate").Value,
               .CustomerPurchaseOrderCode = xee.Element("CustomerPurchaseOrderCode").Value,
               .BidContractId = xee.Element("BidContractId").Value,
               .OrderNote = xee.Element("OrderNote").Value
                 }
        Try
            For Each elements In qx
                Cabeceraa = New CabeceraPedido
                Cabeceraa.CustomerCode = elements.CustomerCode
                If IsNumeric(Cabeceraa.CustomerCode) Then
                    Cliente = CInt(Cabeceraa.CustomerCode)
                Else
                    ParmItem(Result, "9010", "Error en Cliente [" & Cabeceraa.CustomerCode & "]")
                End If
                Cabeceraa.ShiptoCode = elements.ShiptoCode

                If IsNumeric(Cabeceraa.ShiptoCode) Then
                    PuntoDeEnvio = CInt(Cabeceraa.ShiptoCode)
                Else
                    ParmItem(Result, "9020", "Error en Punto de Envio [" & Cabeceraa.ShiptoCode & "]")
                End If

                Cabeceraa.RequestShipDate = elements.RequestShipDate

                If IsDate(Left(Cabeceraa.RequestShipDate, 4) & "-" & Mid(Cabeceraa.RequestShipDate, 5, 2) & "-" & Right(Cabeceraa.RequestShipDate, 2)) Then
                    FechaRequerido = CInt(Cabeceraa.RequestShipDate)
                Else
                    ParmItem(Result, "9030", "Error en Fecha de Requerido [" & Cabeceraa.RequestShipDate & "]")
                End If

                Cabeceraa.CustomerPurchaseOrderCode = elements.CustomerPurchaseOrderCode
                OrdenDeCompra = Cabeceraa.CustomerPurchaseOrderCode

                Cabeceraa.BidContractId = elements.BidContractId
                NumeroDocumento = Cabeceraa.BidContractId
                Sql.stm("")
                Sql.stm(" SELECT * FROM " & LibParam("BFPEDID"))
                Sql.stm(" WHERE ADOCNR = '" & NumeroDocumento & "'")
                Select Case DB.Cursor(rs, Sql.stm())
                    Case 1
                        ParmItem(Result, "8099", "El documento " & NumeroDocumento & " ya fue procesado. Pedido = " & rs("APEDLX").Value)
                    Case -99
                        ParmItem(Result, "9088", DB.Sentencia)
                        ParmItem(Result, "9089", Descripcion_Error)
                        DB.Close()
                        Return Result
                End Select
                rs.Close()
                Sql.stm("")
                Sql.stm(" DELETE FROM " & LibParam("BFPEDID"))
                Sql.stm(" WHERE ADOCNR = '" & NumeroDocumento & "'")


                Cabeceraa.OrderNote = elements.OrderNote
                NotaPedido = Cabeceraa.OrderNote

            Next
        Catch
        End Try


        Try
            Dim bPrimera As Boolean = True
            Dim qx2 = From xe In documento.Descendants.Elements("Linea")
        Select New With { _
                          .ItemCode = xe.Element("ItemCode").Value,
                          .OrderedSellingUnitQty = xe.Element("OrderedSellingUnitQty").Value,
                          .Price = xe.Element("Price").Value,
                          .LineNote = xe.Element("LineNote").Value
                        }
            For Each elementos In qx2
                'ContadorLineas = ContadorLineas + 1
                nuevaLinea = New DetallePedido
                nuevaLinea.ItemCode = elementos.ItemCode
                nuevaLinea.OrderedSellingUnitQty = elementos.OrderedSellingUnitQty
                nuevaLinea.Price = elementos.Price
                nuevaLinea.LineNote = elementos.LineNote
                Lineas.Add(nuevaLinea)
                If IsNumeric(nuevaLinea.OrderedSellingUnitQty) Then
                    Cant = Replace(nuevaLinea.OrderedSellingUnitQty, ",", ".")
                Else
                    ParmItem(Result, "9030", "Error en Cantidad")
                End If
                If IsNumeric(nuevaLinea.Price) Then
                    Precio = Replace(nuevaLinea.Price, ",", ",")
                Else
                    ParmItem(Result, "9030", "Error en Valor")
                End If
                Try
                    msgTime = "INICIA INSERT" & Now()
                    Sql.mkSql(t_iF, " INSERT INTO " & LibParam("BFPEDID") & "( ")
                    Sql.mkSql(t_iV, " VALUES ( ")
                    Sql.mkSql(tStr, " ADOCTP    ", "W", 0)                                  '1A    Tipo W/E/P
                    Sql.mkSql(tStr, " ADOCNR    ", NumeroDocumento, 0)                      '20A   Documen
                    Sql.mkSql(tStr, " APO       ", OrdenDeCompra, 0)                        '15A   Orden de Compra
                    Sql.mkSql(tStr, " ANOTE     ", NotaPedido, 0)                           '50A   Nota al pedido
                    Sql.mkSql(tNum, " ALINE     ", numLinea, 0)                             '4S0   Linea del Docume
                    Sql.mkSql(tNum, " ACUST     ", Cliente, 0)                              '8S0   Cliente
                    Sql.mkSql(tNum, " ASHIP     ", PuntoDeEnvio, 0)                         '4S0   Punto de Envio
                    Sql.mkSql(tStr, " APROD     ", nuevaLinea.ItemCode, 0)     '15A   Producto
                    Sql.mkSql(tNum, " AQORD     ", Cant, 0)                                 '11S3  Cantidad
                    Sql.mkSql(tNum, " AVALOR    ", Precio, 0)                               '14S4  Valor
                    Sql.mkSql(tNum, " ARDTE     ", FechaRequerido, 0)                       '8S0   Fecha de Requerido
                    Sql.mkSql(tStr, " ALNOTE    ", nuevaLinea.LineNote, 0)     '50A   Nota a la linea
                    Sql.mkSql(tStr, " AUSER     ", Usuario, 1)                              '10A   Usuario Ingreso
                    If Sql.Insert(DB) = -99 Then
                        ParmItem(Result, "9088", DB.Sentencia)
                        ParmItem(Result, "9089", Descripcion_Error)
                    Else
                        numLinea += 1
                    End If
                    msgTime &= " FIN INSERT" & Now()

                Catch

                End Try
            Next

        Catch
        End Try

        If Result(0) = "" Then
            If Cliente = 0 Then
                ParmItem(Result, "9011", "Falta Cliente")
            ElseIf NumeroDocumento = "" Then
                ParmItem(Result, "9040", "Falta Numero de Documento Enlace")
            Else
                cmdResult = DB.Call_PGM("CALL PGM(" & LibParam("PGMS") & "BCGENPED) PARM('" & NumeroDocumento.ToString & "')")
                'cmdResult = "OK"
                If cmdResult = "OK" Then
                    Sql.stm("")
                    Sql.stm(" SELECT * FROM " & LibParam("BFPEDID"))
                    Sql.stm(" WHERE ADOCNR = '" & NumeroDocumento & "'")
                    If DB.Cursor(rs, Sql.stm()) = -99 Then
                        ParmItem(Result, "9088", DB.Sentencia)
                        ParmItem(Result, "9089", Descripcion_Error)
                        DB.Close()
                        Return Result
                    End If
                    Do While Not rs.EOF
                        codErr = rs("APEDLX").Value
                        If rs("AERROR").Value <> 0 Then
                            codErr = 8000 + rs("AERROR").Value
                            ParmItem(Result, codErr.ToString(), "LINEA: " & rs("ALINE").Value & " " & rs("ADESER").Value)
                        End If
                        rs.MoveNext()
                    Loop
                    rs.Close()
                Else
                    ParmItem(Result, "9088", DB.Sentencia)
                    ParmItem(Result, "9088", cmdResult)
                End If
            End If
        End If

        DB.Close()
        If UBound(Result) = 0 Then
            ReDim Result(1)
            Result(0) = "0000: PEDIDO CREADO"
            Result(1) = "NUMERO: " & codErr
        Else
            ReDim Preserve Result(UBound(Result) - 1)
        End If

        Debug.Print("FIN PGM")
        Debug.Print(Now())

        Return Result

    End Function



End Class


