﻿Imports System
Imports System.IO

Module Module1
    Public Function SetParametros(
                                ByVal CarpetaWeb As String, _
                                ByVal Servidor As String, _
                                ByVal Usuario As String, _
                                ByVal Password As String, _
                                ByVal Lib_BPCS As String _
                            ) As String
        Dim fEntrada As String = ""
        Dim fSalida As String = ""

        CarpetaWeb = System.AppDomain.CurrentDomain.BaseDirectory()

        fEntrada = CarpetaWeb & "\web_config.txt"
        fSalida = CarpetaWeb & "\web.config"

        Try
            ' Create an instance of StreamReader to read from a file.
            Dim sr As StreamReader = New StreamReader(fEntrada)
            Dim sw As StreamWriter = New StreamWriter(fSalida)
            Dim line As String
            Do
                line = sr.ReadLine()
                If InStr(line, "{Servidor}") <> 0 Then
                    line = Replace(line, "{Servidor}", Servidor)
                End If
                If InStr(line, "{Usuario}") <> 0 Then
                    line = Replace(line, "{Usuario}", Usuario)
                End If
                If InStr(line, "{Password}") <> 0 Then
                    line = Replace(line, "{Password}", Password)
                End If
                If InStr(line, "{Lib_BPCS}") <> 0 Then
                    line = Replace(line, "{Lib_BPCS}", Lib_BPCS)
                End If
                sw.WriteLine(line)
            Loop Until line Is Nothing
            sr.Close()
            sw.Close()
            Return "OK"
        Catch E As Exception
            Return E.Message
        End Try
    End Function

    Public Function SetParametros(
                            ByVal Parametro As String, _
                            ByVal Valor As String _
                        ) As String
        Dim fEntrada As String = ""
        Dim sSalida As String = ""
        Dim aSalida As String()
        Dim i As Integer
        Dim appPath As String = System.Web.HttpContext.Current.Request.ApplicationPath
        Dim CarpetaWeb As String = HttpContext.Current.Request.MapPath(appPath)

        fEntrada = Replace(CarpetaWeb & "\web.config", "\\", "\")
        Try
            ' Create an instance of StreamReader to read from a file.
            Dim sr As StreamReader = New StreamReader(fEntrada)
            Dim line As String
            Do
                line = sr.ReadLine()
                sSalida &= line & vbCrLf
                If InStr(line, "name=""" & Parametro & """") <> 0 Then
                    line = sr.ReadLine()
                    line = "          <value>" & Valor & "</value>"
                    sSalida &= line & vbCrLf
                End If
            Loop Until line Is Nothing
            sr.Close()

            Dim sw As StreamWriter = New StreamWriter(fEntrada)
            aSalida = Split(sSalida, vbCrLf)
            For i = 0 To UBound(aSalida)
                sw.WriteLine(aSalida(i))
            Next
            sw.Close()
            Return "OK"
        Catch E As Exception
            Return E.Message
        End Try
    End Function

    Public Function TransaccionEntrada(
                            ByVal Transaccion As String, _
                            ByVal Fecha As String, _
                            ByVal Referencia As Integer, _
                            ByVal Producto As String, _
                            ByVal Lote As String, _
                            ByVal Bodega As String, _
                            ByVal Ubicacion As String, _
                            ByVal CodigoCausa As String, _
                            ByVal Cantidad As Double, _
                            ByVal Valor As Double, _
                            ByVal Comentario As String, _
                            ByVal Usuario As String _
                            ) As String

        Dim sql As New mk_Sql
        Dim Result As String = ""
        Dim sTCOM As String
        Dim rs As New ADODB.Recordset
        Dim fSql As String
        Dim DB As New ExecuteSQL
        Dim FechaTransaccion As Integer
        Dim chkFecha As Date

        If Cantidad <= 0 Then
            Return "9505: Error en Cantidad. "
        End If
        If Len(Comentario) > 15 Then
            Return "9510: Comentario excede 15. "
        End If

        If IsDate(Left(Fecha, 4) & "-" & Mid(Fecha, 5, 2) & "-" & Right(Fecha, 2)) Then
            chkFecha = DateSerial(Left(Fecha, 4), Mid(Fecha, 5, 2), Right(Fecha, 2))
            If Year(chkFecha) * 100 + Month(chkFecha) = Year(Now()) * 100 + Month(Now()) Then
                FechaTransaccion = CInt(Fecha)
            Else
                Result = "9031 Fecha no corresponde al mes. Use AAAAMMDD. [" & Fecha & "]"
            End If
        Else
            Result = "9030 Error en Fecha. Use AAAAMMDD. [" & Fecha & "]"
        End If

        Bodega = Trim(Bodega.ToUpper())
        Ubicacion = Trim(Ubicacion.ToUpper())
        Transaccion = Left(Transaccion & " ", 2)

        SetConexion(DB)
        If Not DB.Open() Then
            Return "9091: Error en Conexion. " & Descripcion_Error
        End If
        LibParamLoad(DB)

        Select Case DB.Cursor(rs, "SELECT * FROM " & LibParam("ILM") & " WHERE WWHS='" & Bodega & "' AND WLOC='" & Ubicacion & "'")
            Case 0
                Return "9501 La Ubicación NO Existe. " & Bodega & "/" & Ubicacion
            Case -99
                DB.Close()
                Return "9092:" & DB.DspErr()
        End Select
        rs.Close()

        Select Case DB.Cursor(rs, "SELECT * FROM " & LibParam("ZPA") & " WHERE IDPA ='TR'  AND PKEY = 'RCOD" & Transaccion & CodigoCausa & "'")
            Case 0
                Return "9507 Codigo de Causa Incorrecto. " & CodigoCausa
            Case -99
                DB.Close()
                Return "9092:" & DB.DspErr()
        End Select
        rs.Close()
        ' SELECT * FROM  ZPA WHERE IDPA ='TR'

        DB.Sentencia = " SELECT * FROM  " & LibParam("IIML01") & " WHERE IPROD = '" & Producto & "'"
        Select Case DB.Cursor(rs)
            Case 1
                Result = ""
            Case 0
                Return "9400 No encontro Producto. " & Producto
            Case -99
                DB.Close()
                Return "9092:" & DB.DspErr()
        End Select
        rs.Close()

        If Transaccion = "R" Or Transaccion = "IT" Then
            DB.Sentencia = " SELECT * FROM  " & LibParam("FSOL01") & " WHERE SORD = " & Referencia & " AND SPROD = '" & Producto & "'"
            Select Case DB.Cursor(rs)
                Case 1
                    Result = ""
                Case 0
                    Return "9400 No encontro Orden Fabricacion/Producto. " & Referencia & "/" & Producto
                Case -99
                    DB.Close()
                    Return "9092:" & DB.DspErr()
            End Select
            rs.Close()
        End If


        sTCOM = DB.CalcConsec("WMSTRAS", "99999")

        fSql = " UPDATE ILN SET LMRB = 'A'  WHERE LLOT = '" & Lote & "' AND LPROD = '" & Producto & "'"
        DB.ExecuteSQL(fSql)

        fSql = " CREATE ALIAS QTEMP.CIMWMS FOR " & LibParam("CIM") & "(" & LibParam("CIMMBR", "") & ") "
        If DB.ExecuteSQL(fSql) = -99 Then
            DB.Close()
            Return "9901 " & DB.DspErr()
        End If

        '-------------------------------------------------------------------------------------------------------
        '                   ENTRADA
        '-------------------------------------------------------------------------------------------------------

        sql.mkSql(t_iF, " INSERT INTO QTEMP.CIMWMS( ")
        sql.mkSql(t_iV, " VALUES ( ")
        sql.mkSql(tStr, " INID      ", "IN", 0)                 '2A    Record Identifier
        sql.mkSql(tNum, " INTRN     ", sTCOM, 0)                '5P0   INTRN

        sql.mkSql(tNum, " INREF     ", Referencia, 0)           '8P0   Ordene de Compra
        'sql.mkSql(tNum, " INREF2    ", rs("PLINE").Value, 0)    '8P0   Linea de la Orden de Compra

        sql.mkSql(tStr, " INEMCC    ", Comentario, 0)        '15A   Item Number

        sql.mkSql(tStr, " INPROD    ", Producto, 0)             '15A   Item Number
        sql.mkSql(tStr, " INWHSE    ", Bodega, 0)               '2A    Warehouse
        sql.mkSql(tStr, " INREAS    ", CodigoCausa, 0)          '2A    Codigo de Razon
        sql.mkSql(tStr, " INLOT     ", Lote, 0)                 '10A   Lot Number
        sql.mkSql(tStr, " INLOC     ", Ubicacion, 0)            '6A    Location Code
        sql.mkSql(tNum, " INQTY     ", Cantidad, 0)             '11P3  Month to Date Adjustments
        sql.mkSql(tNum, " INCOST    ", Valor, 0)                '
        sql.mkSql(tNum, " INDATE    ", FechaTransaccion, 0)     '8P0   Date
        sql.mkSql(tStr, " INTRAN    ", Transaccion, 0)          '2A    Transaction Type
        sql.mkSql(tStr, " INCBY     ", Usuario, 0)              '10A   Creted by
        sql.mkSql(tNum, " INCDT     ", DB2_DATEDEC, 0)          '8P0   Date Created
        sql.mkSql(tNum, " INCTM     ", DB2_TIMEDEC0, 1)         '6P0   Time Created
        If sql.Insert(DB) = -99 Then
            DB.Close()
            Return sql.Sentencia() & vbCrLf & Descripcion_Error
        End If

        Result = DB.Call_PGM("CALL PGM(" & LibParam("PGMS", "") & "/BCTRAINV)")
        DB.Close()
        If Result = "OK" Then
            Return "0000 Entrada Realizada: " & sTCOM
        Else
            Return Result
        End If

    End Function

End Module
