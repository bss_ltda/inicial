﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

<System.Web.Services.WebService(Namespace:="http://tempuri.org/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Pedido
    Inherits System.Web.Services.WebService
    Dim Total As Integer = 0

    <WebMethod()> _
    Public Function CrearCabecera(
                                    ByVal Cliente As Integer, _
                                    ByVal PuntoDeEnvio As Integer, _
                                    ByVal OrdenDeCompra As String, _
                                    ByVal FechaRequerido As String, _
                                    ByVal NotaPedido As String _
                                    ) As String
        Dim NumeroOrden As Long
        Randomize() ' inicializar la semilla
        NumeroOrden = CLng((50255 - 89666) * Rnd() + 89666)
        Return "NumeroOrden: " & NumeroOrden.ToString

    End Function
    <WebMethod()> _
    Public Function CrearLinea(
                                    ByVal NumeroOrden As Integer, _
                                    ByVal Producto As String, _
                                    ByVal Cantidad As Double, _
                                    ByVal Precio As Double _
                                    ) As String()

        ' If the XML Web service has not been accessed, initialize it to 1.
        If Application("MyServiceUsage") Is Nothing Then
            Application("MyServiceUsage") = 1
        Else
            ' Increment the usage count.
            Application("MyServiceUsage") = CInt(Application("MyServiceUsage")) + 1
        End If

        ' Return the usage count.
        Return {"Linea Creada", "NumeroOrden: " & NumeroOrden.ToString(), " Linea: " & CInt(Application("MyServiceUsage"))}

    End Function
    <WebMethod()> _
    Public Function CrearPedido(ByVal NumeroOrden As Integer) As String()
        Dim NumeroPedido As Long
        Randomize() ' inicializar la semilla
        NumeroPedido = CLng((150255 - 189666) * Rnd() + 189666)
        Return {"Pedido Creado. NumeroOrden: " & NumeroOrden.ToString, " Numero de Pedido: " & NumeroPedido.ToString(), " Total Lineas: " & CInt(Application("MyServiceUsage"))}
    End Function

    <WebMethod()> _
    Public Function CrearPedidoXML(ByVal xPedido As String) As String()
        Dim NumeroPedido As Long
        Randomize() ' inicializar la semilla
        NumeroPedido = CLng((150255 - 189666) * Rnd() + 189666)
        Return {"Pedido Creado"}
    End Function

End Class