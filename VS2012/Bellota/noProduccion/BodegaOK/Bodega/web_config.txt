<?xml version="1.0"?>
<configuration>

    <configSections>
        <sectionGroup name="applicationSettings" type="System.Configuration.ApplicationSettingsGroup, System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" >
            <section name="Bodega.My.MySettings" type="System.Configuration.ClientSettingsSection, System, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" requirePermission="false" />
        </sectionGroup>
    </configSections>
    <appSettings/>
    <connectionStrings/>
    <system.web>
        <compilation debug="true" >

        </compilation>
    <!--
      La sección <authentication> habilita la configuración 
      del modo de autenticación de seguridad que usa
      ASP.NET para identificar a un usuario entrante. 
-->
    <authentication mode="Windows" />
    <!--
       La sección <customErrors> habilita la configuración de 
       las acciones que se deben realizar si un error no controlado tiene lugar
       durante la ejecución de una solicitud. En concreto, 
       permite a los desarrolladores configurar páginas de error html 
       que se mostrarán en lugar de un seguimiento de pila de errores.

        <customErrors mode="RemoteOnly" defaultRedirect="GenericErrorPage.htm">
         <error statusCode="403" redirect="NoAccess.htm" />
         <error statusCode="404" redirect="FileNotFound.htm" />
       </customErrors>
-->
    </system.web>  
    <applicationSettings>
      <Bodega.My.MySettings>
        <setting name="LIBRARY" serializeAs="String">
          <value>{Lib_BPCS}</value>
        </setting>
        <setting name="SERVER" serializeAs="String">
          <value>{Servidor}</value>
        </setting>
        <setting name="USER" serializeAs="String">
          <value>{Usuario}</value>
        </setting>
        <setting name="PASSWORD" serializeAs="String">
          <value>{Password}</value>
        </setting>
        <setting name="ECL" serializeAs="String">
          <value>{Lib_ECL}</value>
        </setting>
        <setting name="ILI" serializeAs="String">
          <value>{Lib_ECL}</value>
        </setting>
        <setting name="ILM" serializeAs="String">
          <value>{Lib_ECL}</value>
        </setting>
        <setting name="ELA" serializeAs="String">
          <value>{LIb_ELA}</value>
        </setting>
        <setting name="CIM" serializeAs="String">
          <value>{Lib_CIM}</value>
        </setting>
        <setting name="IWM" serializeAs="String">
          <value>{Lib_IWM}</value>
        </setting>
        <setting name="CIMMBR" serializeAs="String">
          <value>COLOMBIA</value>
        </setting>
        <setting name="LIB_PGM" serializeAs="String">
          <value>{Lib_Pgms}</value>
        </setting>
        <setting name="HPO" serializeAs="String">
          <value>{Lib_HPO}</value>
        </setting>
      </Bodega.My.MySettings>
    </applicationSettings>
</configuration>
