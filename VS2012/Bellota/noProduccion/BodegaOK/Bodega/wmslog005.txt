LOG 5
8/28/2012 7:08:17 PM

SistemaDeBodega (0.1.9)
[213.62.216.85][COSIS001][CP61BPCSF]
 CREATE ALIAS QTEMP.CIMWMS FOR CP61BPCUSF.CIM(COLOMBIA) 

SistemaDeBodega (0.1.9)
[213.62.216.85][COSIS001][CP61BPCSF]
 CREATE ALIAS QTEMP.CIMWMS FOR CP61BPCUSF.CIM(COLOMBIA) 

SistemaDeBodega (0.1.9)
[213.62.216.85][COSIS001][CP61BPCSF]
SQL0601: CIMWMS en QTEMP de tipo *FILE ya existe.
Causa . . . . . :   Se ha intentado crear CIMWMS en QTEMP o redenominar una tabla, vista, alias o índice como CIMWMS, pero CIMWMS ya existe. Todas las tablas, vistas, alias, índices, paquetes SQL, secuencias, restricciones, desencadenantes y tipos definidos por usuario en el mismo esquema deben tener nombres exclusivos. -- Si CIMWMS es una tabla temporal, no puede sustituirse a menos que se especifique la cláusula WITH REPLACE. -- Si el nombre de esquema es *N, se trata de una sentencia CREATE SCHEMA. Si se trata de una sentencia CREATE TABLE o ALTER TABLE y el tipo es *N, CIMWMS es una restricción. Recuperación  . :   Cambie CIMWMS por un nombre que no exista, o suprima, mueva o redenomine el objeto existente. Si se trata de una tabla temporal, utilice la cláusula WITH REPLACE. Si está creando un paquete SQL, especifique REPLACE(*YES) en CRTSQLPKG. Vuelva a intentar la petición.

