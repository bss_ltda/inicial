﻿Imports System
Imports System.Globalization
Imports System.IO
Imports System.Collections
Imports System.Xml.Serialization
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

<System.Web.Services.WebService(Namespace:="http://soap.bellota.co/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Pedidos
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function CreaPedido(ByVal Pedido As String) As String()
        Dim xml As New Chilkat.Xml()

        Dim Cliente As Integer = 0
        Dim PuntoDeEnvio As Integer = 0
        Dim OrdenDeCompra As String = ""
        Dim NumeroDocumento As String = ""
        Dim FechaRequerido As String = ""
        Dim NotaPedido As String = ""
        Dim Usuario As String = "WEBPED"
        Dim numLinea As Integer = 1
        Dim Result() As String
        Dim cmdResult As String = ""
        Dim codErr As String = ""
        Dim Cant As String = "0"
        Dim Precio As String = "0"
        Dim msgTime As String
        Dim DB As New ExecuteSQL
        Dim rs As New ADODB.Recordset

        ReDim Result(0)
        xml.LoadXml(Pedido)
        Dim dato As Chilkat.Xml
        dato = xml.FirstChild()
        Debug.Print("Conexion")
        Debug.Print(Now())
        SetConexion(DB)
        If Not DB.Open() Then
            ParmItem(Result, "9091", Descripcion_Error)
            Return Result
        End If
        LibParamLoad(DB)

        Debug.Print("Lee xml")
        Debug.Print(Now())
        While Not (dato Is Nothing)
            Select Case dato.Tag
                Case "CustomerCode"
                    If IsNumeric(dato.Content) Then
                        Cliente = CInt(dato.Content)
                    Else
                        ParmItem(Result, "9010", "Error en Cliente [" & dato.Content & "]")
                    End If
                Case "ShiptoCode"
                    If IsNumeric(dato.Content) Then
                        PuntoDeEnvio = CInt(dato.Content)
                    Else
                        ParmItem(Result, "9020", "Error en Punto de Envio [" & dato.Content & "]")
                    End If
                Case "RequestShipDate"
                    If IsDate(Left(dato.Content, 4) & "-" & Mid(dato.Content, 5, 2) & "-" & Right(dato.Content, 2)) Then
                        FechaRequerido = CInt(dato.Content)
                    Else
                        ParmItem(Result, "9030", "Error en Fecha de Requerido [" & dato.Content & "]")
                    End If
                Case "CustomerPurchaseOrderCode"
                    OrdenDeCompra = dato.Content
                Case "User"
                    If Trim(dato.Content) <> "" Then
                        Usuario = dato.Content
                    End If
                Case "BidContractId"
                    NumeroDocumento = dato.Content
                    Sql.stm("")
                    Sql.stm(" SELECT * FROM " & LibParam("BFPEDID"))
                    Sql.stm(" WHERE ADOCNR = '" & NumeroDocumento & "'")
                    Select Case DB.Cursor(rs, Sql.stm())
                        Case 1
                            ParmItem(Result, "8099", "El documento " & NumeroDocumento & " ya fue procesado. Pedido = " & rs("APEDLX").Value)
                        Case -99
                            ParmItem(Result, "9088", DB.Sentencia)
                            ParmItem(Result, "9089", Descripcion_Error)
                            DB.Close()
                            Return Result
                    End Select
                    rs.Close()
                    Sql.stm("")
                    Sql.stm(" DELETE FROM " & LibParam("BFPEDID"))
                    Sql.stm(" WHERE ADOCNR = '" & NumeroDocumento & "'")
                    'Sql.Delete(DB)

                Case "OrderNote"
                    NotaPedido = dato.Content
                Case "Line"
                    If Result(0) = "" Then
                        Cant = "0"
                        Precio = "0"
                        If IsNumeric(dato.GetChildContent("OrderedSellingUnitQty")) Then
                            Cant = Replace(dato.GetChildContent("OrderedSellingUnitQty"), ",", ".")
                        Else
                            ParmItem(Result, "9030", "Error en Cantidad")
                        End If
                        If IsNumeric(dato.GetChildContent("Price")) Then
                            Precio = Replace(dato.GetChildContent("Price"), ",", ",")
                        Else
                            ParmItem(Result, "9030", "Error en Valor")
                        End If

                        msgTime = "INICIA INSERT" & Now()
                        Sql.mkSql(t_iF, " INSERT INTO " & LibParam("BFPEDID") & "( ")
                        Sql.mkSql(t_iV, " VALUES ( ")
                        Sql.mkSql(tStr, " ADOCTP    ", "W", 0)                                  '1A    Tipo W/E/P
                        Sql.mkSql(tStr, " ADOCNR    ", NumeroDocumento, 0)                      '20A   Documen
                        Sql.mkSql(tStr, " APO       ", OrdenDeCompra, 0)                        '15A   Orden de Compra
                        Sql.mkSql(tStr, " ANOTE     ", NotaPedido, 0)                           '50A   Nota al pedido
                        Sql.mkSql(tNum, " ALINE     ", numLinea, 0)                             '4S0   Linea del Docume
                        Sql.mkSql(tNum, " ACUST     ", Cliente, 0)                              '8S0   Cliente
                        Sql.mkSql(tNum, " ASHIP     ", PuntoDeEnvio, 0)                         '4S0   Punto de Envio
                        Sql.mkSql(tStr, " APROD     ", dato.GetChildContent("ItemCode"), 0)     '15A   Producto
                        Sql.mkSql(tNum, " AQORD     ", Cant, 0)                                 '11S3  Cantidad
                        Sql.mkSql(tNum, " AVALOR    ", Precio, 0)                               '14S4  Valor
                        Sql.mkSql(tNum, " ARDTE     ", FechaRequerido, 0)                       '8S0   Fecha de Requerido
                        Sql.mkSql(tStr, " ALNOTE    ", dato.GetChildContent("LineNote"), 0)     '50A   Nota a la linea
                        Sql.mkSql(tStr, " AUSER     ", Usuario, 1)                              '10A   Usuario Ingreso
                        If Sql.Insert(DB) = -99 Then
                            ParmItem(Result, "9088", DB.Sentencia)
                            ParmItem(Result, "9089", Descripcion_Error)
                        Else
                            numLinea += 1
                        End If
                        msgTime &= " FIN INSERT" & Now()
                    End If
            End Select
            dato = dato.NextSibling()
        End While

        If Result(0) = "" Then
            If Cliente = 0 Then
                ParmItem(Result, "9011", "Falta Cliente")
            ElseIf NumeroDocumento = "" Then
                ParmItem(Result, "9040", "Falta Numero de Documento Enlace")
            Else
                cmdResult = DB.Call_PGM("CALL PGM(" & LibParam("PGMS") & "BCGENPED) PARM('" & NumeroDocumento.ToString & "')")
                'cmdResult = "OK"
                If cmdResult = "OK" Then
                    Sql.stm("")
                    Sql.stm(" SELECT * FROM " & LibParam("BFPEDID"))
                    Sql.stm(" WHERE ADOCNR = '" & NumeroDocumento & "'")
                    If DB.Cursor(rs, Sql.stm()) = -99 Then
                        ParmItem(Result, "9088", DB.Sentencia)
                        ParmItem(Result, "9089", Descripcion_Error)
                        DB.Close()
                        Return Result
                    End If
                    Do While Not rs.EOF
                        codErr = rs("APEDLX").Value
                        If rs("AERROR").Value <> 0 Then
                            codErr = 8000 + rs("AERROR").Value
                            ParmItem(Result, codErr.ToString(), "LINEA: " & rs("ALINE").Value & " " & rs("ADESER").Value)
                        End If
                        rs.MoveNext()
                    Loop
                    rs.Close()
                Else
                    ParmItem(Result, "9088", DB.Sentencia)
                    ParmItem(Result, "9088", cmdResult)
                End If
            End If
        End If

        DB.Close()
        If UBound(Result) = 0 Then
            ReDim Result(1)
            Result(0) = "0000: PEDIDO CREADO"
            Result(1) = "NUMERO: " & codErr
        Else
            ReDim Preserve Result(UBound(Result) - 1)
        End If

        Debug.Print("FIN PGM")
        Debug.Print(Now())

        Return Result

    End Function



End Class


