﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel

<System.Web.Services.WebService(Namespace:="http://soap.bellota.co/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class BodegaParametros
    Inherits System.Web.Services.WebService

    <WebMethod()> _
    Public Function VerParametros() As String()

        Dim Result() As String
        Dim DB As New ExecuteSQL
        ReDim Result(0)

        SetConexion(DB)

        ParmItem(Result, "Version", "(" & My.Application.Info.Version.Major & "." & _
         My.Application.Info.Version.Minor & "." & _
         My.Application.Info.Version.Revision & ")")

        ParmItem(Result, "Servidor", My.Settings.SERVER)
        ParmItem(Result, "Usuario", My.Settings.USER)
        ParmItem(Result, "Biblioteca", My.Settings.LIBRARY)

        If Not DB.Open() Then
            ParmItem(Result, "9091", "open::" & Descripcion_Error)
            Return Result
        End If
        'ParmItem(Result, "tag", LibParamLoad(DB))
        LibParamLoad(DB)

        ParmItem(Result, "Programas", LibParam("PGMS", ""))
        ParmItem(Result, "IWM", LibParam("IWM"))
        ParmItem(Result, "ECL", LibParam("ECL"))
        ParmItem(Result, "ILI", LibParam("ILI"))
        ParmItem(Result, "ELA", LibParam("ELA"))
        ParmItem(Result, "CIM", LibParam("CIM"))
        ParmItem(Result, "ILM", LibParam("ILM"))
        ParmItem(Result, "HPO", LibParam("HPO"))
        ParmItem(Result, "BFPEDID", LibParam("BFPEDID"))
        ParmItem(Result, "MIEMBRO CIM", LibParam("CIMMBR", ""))

        ReDim Preserve Result(UBound(Result) - 1)

        DB.Close()

        Return Result

    End Function

    <WebMethod()> _
    Public Function CambiarParametros(
                                ByVal Servidor As String, _
                                ByVal Usuario As String, _
                                ByVal Password As String, _
                                ByVal Lib_BPCS As String _
                                ) As String()

        Dim DB As New ExecuteSQL
        If Servidor <> "" Then
            SetParametros("SERVER", Servidor)
        Else
            Servidor = My.Settings.SERVER
        End If

        If Usuario <> "" Then
            SetParametros("USER", Usuario)
        Else
            Usuario = My.Settings.USER
        End If

        If Password <> "" Then
            SetParametros("PASSWORD", Password)
        Else
            Password = My.Settings.PASSWORD
        End If

        If Lib_BPCS <> "" Then
            SetParametros("LIBRARY", Lib_BPCS)
        Else
            Lib_BPCS = My.Settings.LIBRARY
        End If

        Dim Result As String()
        ReDim Result(0)

        ParmItem(Result, "Servidor", My.Settings.SERVER)
        ParmItem(Result, "Usuario", My.Settings.USER)
        ParmItem(Result, "Biblioteca", My.Settings.LIBRARY)


        SetConexion(Servidor, Usuario, Password, Lib_BPCS, DB)

        If Not DB.Open() Then
            ParmItem(Result, "9091", Descripcion_Error)
            Return Result
        End If

        LibParamLoad(DB)

        ParmItem(Result, "Version", "(" & My.Application.Info.Version.Major & "." & _
         My.Application.Info.Version.Minor & "." & _
         My.Application.Info.Version.Revision & ")")

        ParmItem(Result, "Programas", LibParam("PGMS", ""))
        ParmItem(Result, "IWM", LibParam("IWM"))
        ParmItem(Result, "ECL", LibParam("ECL"))
        ParmItem(Result, "ILI", LibParam("ILI"))
        ParmItem(Result, "ELA", LibParam("ELA"))
        ParmItem(Result, "CIM", LibParam("CIM"))
        ParmItem(Result, "ILM", LibParam("ILM"))
        ParmItem(Result, "HPO", LibParam("HPO"))
        ParmItem(Result, "BFPEDID", LibParam("BFPEDID"))
        ParmItem(Result, "MIEMBRO CIM", LibParam("CIMMBR", ""))

        ReDim Preserve Result(UBound(Result) - 1)

        DB.Close()

        Return Result

    End Function

    <WebMethod()> _
    Public Function Version() As String

        Return ("(" & My.Application.Info.Version.Major & "." & _
                 My.Application.Info.Version.Minor & "." & _
                 My.Application.Info.Version.Revision & ")")

    End Function

End Class