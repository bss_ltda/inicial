﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' La información general sobre un ensamblado se controla mediante el siguiente 
' conjunto de atributos. Cambie estos atributos para modificar la información
' asociada con un ensamblado.

' Revisar los valores de los atributos del ensamblado
<Assembly: AssemblyTitle("Bodega")> 
<Assembly: AssemblyDescription("Servicios Bodega")> 
<Assembly: AssemblyCompany("BSS LTDA")> 
<Assembly: AssemblyProduct("Bodega")> 
<Assembly: AssemblyCopyright("Copyright ©  2014")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'El siguiente GUID sirve como identificador de typelib si este proyecto se expone a COM
<Assembly: Guid("8fa176c4-72a1-4f8a-a1b0-98a32ed23de2")> 

' La información de versión de un ensamblado consta de los cuatro valores siguientes:
'
'      Versión principal
'      Versión secundaria 
'      Número de compilación
'      Revisión
'
' Puede especificar todos los valores o usar los valores predeterminados de número de compilación y de revisión 
' mediante el carácter '*', como se muestra a continuación:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("2.0.0.1")> 
<Assembly: AssemblyFileVersion("2.0.0.1")> 
