﻿
Imports System
Imports System.Globalization
Imports System.IO
Imports System.Collections
Imports System.Xml.Serialization
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Xml
Imports System.Net
Imports System.Xml.Linq
Imports System.Linq


<System.Web.Services.WebService(Namespace:="http://soap.bellota.co/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class Pedidos
    Inherits System.Web.Services.WebService

    Structure CabeceraPedido
        Dim CustomerCode As String
        Dim ShiptoCode As String
        Dim RequestShipDate As String
        Dim CustomerPurchaseOrderCode As String
        Dim BidContractId As String
        Dim OrderNote As String
        Dim Lines As List(Of DetallePedido)
    End Structure

    Structure DetallePedido
        Dim ItemCode As String
        Dim OrderedSellingUnitQty As Double
        Dim Price As Double
        Dim LineNote As String
    End Structure

    <WebMethod()> _
    Public Function CreaPedido(ByVal Pedido As String) As String()
        Dim pedH As CabeceraPedido
        Dim pedL As DetallePedido
        Dim Usuario As String = "WEBPED"
        Dim numLinea As Integer = 1
        Dim Result() As String
        Dim cmdResult As String = ""
        Dim codErr As String = ""
        Dim DB As New ExecuteSQL
        Dim rs As New ADODB.Recordset

        ReDim Result(0)
        Try

            Dim docEntrada As XDocument = XDocument.Parse(Pedido)
            Dim qx = From xe In docEntrada.Elements("Order") Select New With { _
                   .CustomerCode = xe.Element("CustomerCode").Value,
                   .ShiptoCode = xe.Element("ShiptoCode").Value,
                   .RequestShipDate = xe.Element("RequestShipDate").Value,
                   .CustomerPurchaseOrderCode = xe.Element("CustomerPurchaseOrderCode").Value,
                   .BidContractId = xe.Element("BidContractId").Value,
                   .OrderNote = xe.Element("OrderNote").Value
            }

            pedH = New CabeceraPedido
            With pedH
                .CustomerCode = qx.First.CustomerCode
                .ShiptoCode = qx.First.ShiptoCode
                .RequestShipDate = qx.First.RequestShipDate
                .CustomerPurchaseOrderCode = qx.First.CustomerPurchaseOrderCode
                .BidContractId = qx.First.BidContractId
                .OrderNote = qx.First.OrderNote
                .Lines = New List(Of DetallePedido)
            End With

            Dim qd = From xe In docEntrada.Descendants.Elements("Line") Select New With { _
                .ItemCode = xe.Element("ItemCode").Value,
                .OrderedSellingUnitQty = xe.Element("OrderedSellingUnitQty").Value,
                .Price = xe.Element("Price").Value,
                .LineNote = xe.Element("LineNote").Value
            }

            For Each e In qd
                pedL = New DetallePedido
                With pedL
                    .ItemCode = e.ItemCode
                    .OrderedSellingUnitQty = e.OrderedSellingUnitQty
                    .Price = e.Price
                    .LineNote = e.LineNote
                End With
                pedH.Lines.Add(pedL)
            Next

            'leeParametrosMySql(p, DB)
            gsKeyWords = "antes de setconexion"
            gsKeyWords = "PARAMETROS=" & My.Settings.CONFIG
            SetConexion(DB)
            gsKeyWords = "despues de setconexion"
            If Not DB.Open() Then
                ParmItem(Result, "9091", Descripcion_Error)
                Return Result
            End If
            LibParamLoad(DB)

            Sql.stm("")

            Sql.stm(" SELECT * FROM " & LibParam("BFPEDID"))
            Sql.stm(" WHERE ADOCNR = '" & pedH.BidContractId & "'")
            Select Case DB.Cursor(rs, Sql.stm())
                Case 1
                    ParmItem(Result, "8099", "El documento " & pedH.BidContractId & " ya fue procesado. Pedido = " & rs("APEDLX").Value)
                Case -99
                    ParmItem(Result, "9088", DB.Sentencia)
                    ParmItem(Result, "9089", Descripcion_Error)
                    DB.Close()
                    Return Result
            End Select
            rs.Close()

            For Each e In pedH.Lines
                With e
                    Sql.mkSql(t_iF, " INSERT INTO " & LibParam("BFPEDID") & "( ")
                    Sql.mkSql(t_iV, " VALUES ( ")
                    Sql.mkSql(tStr, " ADOCTP    ", "W", 0)                                  '1A    Tipo W/E/P
                    Sql.mkSql(tStr, " ADOCNR    ", pedH.BidContractId, 0)                   '20A   Documen
                    Sql.mkSql(tStr, " APO       ", pedH.CustomerPurchaseOrderCode, 0)       '15A   Orden de Compra
                    Sql.mkSql(tStr, " ANOTE     ", pedH.OrderNote, 0)                       '50A   Nota al pedido
                    Sql.mkSql(tNum, " ALINE     ", numLinea, 0)                             '4S0   Linea del Docume
                    Sql.mkSql(tNum, " ACUST     ", pedH.CustomerCode, 0)                    '8S0   Cliente
                    Sql.mkSql(tNum, " ASHIP     ", pedH.ShiptoCode, 0)                      '4S0   Punto de Envio
                    Sql.mkSql(tStr, " APROD     ", e.ItemCode, 0)                           '15A   Producto
                    Sql.mkSql(tNum, " AQORD     ", e.OrderedSellingUnitQty, 0)              '11S3  Cantidad
                    Sql.mkSql(tNum, " AVALOR    ", e.Price, 0)                              '14S4  Valor
                    Sql.mkSql(tNum, " ARDTE     ", pedH.RequestShipDate, 0)                 '8S0   Fecha de Requerido
                    Sql.mkSql(tStr, " ALNOTE    ", e.LineNote, 0)                           '50A   Nota a la linea
                    Sql.mkSql(tStr, " AUSER     ", Usuario, 1)                              '10A   Usuario Ingreso
                    If Sql.Insert(DB) = -99 Then
                        ParmItem(Result, "9088", DB.Sentencia)
                        ParmItem(Result, "9089", Descripcion_Error)
                    Else
                        numLinea += 1
                    End If

                End With
            Next

            If Result(0) = "" Then
                If pedH.CustomerCode = 0 Then
                    ParmItem(Result, "9011", "Falta Cliente")
                ElseIf pedH.BidContractId = "" Then
                    ParmItem(Result, "9040", "Falta Numero de Documento Enlace")
                Else
                    cmdResult = DB.Call_PGM("CALL PGM(" & LibParam("PGMS") & "BCGENPED) PARM('" & pedH.BidContractId & "')")
                    If cmdResult = "OK" Then
                        Sql.stm("")
                        Sql.stm(" SELECT * FROM " & LibParam("BFPEDID"))
                        Sql.stm(" WHERE ADOCNR = '" & pedH.BidContractId & "'")
                        If DB.Cursor(rs, Sql.stm()) = -99 Then
                            ParmItem(Result, "9088", DB.Sentencia)
                            ParmItem(Result, "9089", Descripcion_Error)
                            DB.Close()
                            Return Result
                        End If
                        Do While Not rs.EOF
                            codErr = rs("APEDLX").Value
                            If rs("AERROR").Value <> 0 Then
                                codErr = 8000 + rs("AERROR").Value
                                ParmItem(Result, codErr.ToString(), "LINEA: " & rs("ALINE").Value & " " & rs("ADESER").Value)
                            End If
                            rs.MoveNext()
                        Loop
                        rs.Close()
                    Else
                        ParmItem(Result, "9088", DB.Sentencia)
                        ParmItem(Result, "9088", cmdResult)
                    End If
                End If
            End If

            DB.Close()
            If UBound(Result) = 0 Then
                ReDim Result(1)
                Result(0) = "0000: PEDIDO CREADO"
                Result(1) = "NUMERO: " & codErr
            Else
                ReDim Preserve Result(UBound(Result) - 1)
            End If
            Return Result
        Catch ex As Exception
            ReDim Result(3)
            Result(0) = "9088: ERROR!!!!"
            Result(1) = ex.ToString
            Result(2) = DB.getConnString()
            Result(3) = gsKeyWords
            Return Result
        End Try


    End Function



End Class


