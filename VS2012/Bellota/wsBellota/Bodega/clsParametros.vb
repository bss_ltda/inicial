﻿Public Class clsParametros
    Public gsDatasource As String
    Public gsLib As String
    Public gsLib_F As String
    Public gsLib_PGM As String
    Public gsConnstring As String
    Public gsUser As String
    Public gsPassword As String
    Public gsAppName As String
    Public gsAppPath As String
    Public gsAppVersion As String
    Public gsKeyWords As String
    Public ModuloActual As String = ""
    Public lastError As String = ""
    Public lastSQL As String = ""
    Public bDateTimeToChar As Boolean = False

    Public Const DB2_DATEDEC = " ( YEAR( NOW() ) * 10000 + MONTH( NOW()  ) * 100 + DAY( NOW() ) )"
    Public Const DB2_TIMEDEC0 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 + SECOND( NOW() ) ) "
    Public Const DB2_TIMEDEC1 = " ( HOUR( NOW() ) * 10000 + MINUTE( NOW() ) * 100 ) "

    Public lstParam As Dictionary(Of String, String)

    Sub Inicializa()
        Me.gsDatasource = ""
        Me.gsLib = ""
        Me.gsConnstring = ""
        Me.gsUser = ""
        Me.gsAppName = ""
        Me.gsAppPath = ""
        Me.gsAppVersion = ""
        Me.gsKeyWords = ""
        Me.ModuloActual = ""
        Me.lastError = ""
        Me.lastSQL = ""
        Me.bDateTimeToChar = False
        lstParam = New Dictionary(Of String, String)
    End Sub

    Sub setLibT(tabla As String, qLib As String)
        Me.lstParam.Add(tabla, qLib)
    End Sub

    Function getLibT(tabla As String) As String
        If tabla <> "PGMS" Then
            Return Me.lstParam(tabla) & "." & tabla
        Else
            Return Me.lstParam(tabla) & "/"
        End If
    End Function

End Class
