﻿Imports MySql.Data.MySqlClient

Public Class ExecuteSQL
    Public Conn As New ADODB.Connection
    Public DBMySQL As New MySqlConnection
    Public Sentencia As String
    Public bLog As Boolean = False
    Public iNumLog As Integer

    Dim gsConnString As String

    Public Function Open() As Boolean
        Try
            'If (DB.State = 1) Then DB.Close()
            If (Conn.State = 0) Then
                Conn.Open(gsConnString)
                Sesiones = 1
            Else
                Sesiones += 1
            End If
            SetNumLog()
            crtLog(iNumLog.ToString, iNumLog.ToString)
            Return (Conn.State = 1)
        Catch
            Descripcion_Error = Err.Description
            wrtLog(Err.Description, iNumLog)
            Return False
        End Try

    End Function

    Public Sub Close()
        If Sesiones = 1 Then
            Sesiones = 0
            Conn.Close()
        Else
            Sesiones -= 1
        End If
    End Sub

    Public Function State() As Integer
        Return Conn.State
    End Function

    Public Function DspErr() As String
        Return Error_Tabla & " " & Sentencia & " " & vbCrLf & Descripcion_Error
    End Function

    Public Function ExecuteSQL(ByVal sql As String) As Integer
        Dim r As Integer = 0

        Try
            Sentencia = sql
            wrtLog(sql, iNumLog)
            Conn.Execute(sql, r)
        Catch
            'MsgBox(Err.Description)
            'Debug.Print(sql)
            If InStr(sql, "QTEMP.CIMWMS") = 0 Then
                Descripcion_Error = Err.Description
                wrtLog(Err.Description, iNumLog)
                Return -99
            ElseIf InStr(Err.Description, "SQL0601") = 0 Then
                Descripcion_Error = Err.Description
                wrtLog(Err.Description, iNumLog)
                Return -99
            End If
        End Try

        Return r

    End Function

    Public Function Cursor(ByRef rs As ADODB.Recordset, ByVal sql As String) As Integer

        Dim Result As Integer
        Sentencia = sql
        Try
            rs.Open(sql, Conn)
        Catch
            Descripcion_Error = Sentencia & " " & Err.Description
            wrtLog(Err.Description, iNumLog)
            Return -99
        End Try
        Result = IIf(rs.EOF, 0, 1)
        Return Result

    End Function

    Public Function Cursor(ByRef rs As ADODB.Recordset) As Integer
        Dim Result As Integer
        Try
            rs.Open(Sentencia, Conn)
        Catch
            Descripcion_Error = Sentencia & " " & Err.Description
            wrtLog(Err.Description, iNumLog)
            Return -99
        End Try
        Result = IIf(rs.EOF, 0, 1)
        Return Result
    End Function

    Public Function CalcConsec(ByRef sID As String, ByRef TopeMax As String) As String
        Dim rs As New ADODB.Recordset
        Dim locID As String
        Dim sql As String
        Dim sConsec As String = ""
        Dim tMax As Double
        Dim sFmt As String
        Dim nDig As Integer

        Try

            nDig = Len(TopeMax)
            sFmt = New String("0", nDig)

            sql = "SELECT CCDESC FROM ZCCL01 WHERE CCTABL = 'SECUENCE' AND CCCODE='" & sID & "'"
            tMax = Val(TopeMax)

            With rs
                .CursorLocation = ADODB.CursorLocationEnum.adUseServer
                .Open(sql, Conn, ADODB.CursorTypeEnum.adOpenDynamic, ADODB.LockTypeEnum.adLockPessimistic)
                If Not (.BOF And .EOF) Then
                    locID = Val(.Fields("CCDESC").Value) + 1
                    If locID > tMax Then
                        locID = 1
                    End If
                    sConsec = Right(sFmt & locID, nDig)
                    .Fields("CCDESC").Value = sConsec
                    .Update()
                    .Close()
                Else
                    locID = 1
                    .Close()
                    sConsec = Right(sFmt & locID, nDig)
                    Conn.Execute(" INSERT INTO ZCC (CCID, CCTABL, CCCODE, CCDESC ) " & " VALUES( 'CC', 'SECUENCE', '" & sID & "', '" & sConsec & "' )")
                End If
            End With

        Catch ex As Exception
            Descripcion_Error = Sentencia & " " & Err.Description
            wrtLog(Err.Description, iNumLog)
            Return -99
        End Try

        Return sConsec


    End Function

    Sub setConnectionString(ByVal Prov As String, ByVal srv As String, ByVal usr As String, ByVal pass As String, ByVal sLib As String, ByVal sCatalogo As String)
        Dim g As String

        g = ""
        g = g & "Provider=" & Prov & ";"
        g = g & "Password=" & pass & ";"
        g = g & "User ID=" & usr & ";"
        g = g & "Data Source=" & srv & ";"
        g = g & "Persist Security Info=False;"
        If sLib <> "" Then
            g = g & "Default Collection=" & sLib & ";"
        End If
        If sCatalogo <> "" Then
            g = g & "Initial Catalog=" & sCatalogo & ";"
        End If
        gsConnString = g

        wrtLog(srv & "  " & sLib, iNumLog)


    End Sub

    Public Function CalcConsecV(ByRef sID As String, ByRef TopeMax As String) As Double
        Return CDbl(CalcConsec(sID, TopeMax))
    End Function

    Public Function Call_PGM(ByVal Pgm As String) As String
        Dim r As Integer = 0

        Try
            Sentencia = Pgm
            wrtLog(Pgm, iNumLog)
            Conn.Execute(" {{ " & Pgm & " }} ", r)
        Catch
            'MsgBox(Err.Description)
            'Debug.Print(sql)
            Return Err.Description
        End Try

        Return "OK"

    End Function

    'Sub WrtLog(sql As String)
    '    Dim fSql, vSql As String

    '    If bLog Then
    '        fSql = " INSERT INTO BFLOG( "
    '        vSql = " VALUES ( "
    '        fSql = fSql & " LNUM      , " : vSql = vSql & " " & iNumLog & ", "
    '        fSql = fSql & " LLOG      ) " : vSql = vSql & "'" & Replace(sql, "'", "''") & "' )"
    '        DB.Execute(fSql & vSql)
    '    End If

    'End Sub

    Function SetNumLog() As Integer
        iNumLog = CInt(CalcConsec("WMSLOG", "99"))
        Return iNumLog
    End Function

    Public Function GetNumLog() As Integer
        Return iNumLog
    End Function

    Public Function getConnString() As String
        Return Me.gsConnString
    End Function

    Sub ParametrosConexion400()
        If abrirConexionMySql() = "" Then
            Dim s As String = ""
            s = s & "Provider=IBMDA400.DataSource;Data Source={DATASOURCE};User ID={USERID};Password={PASSWORD};Persist Security Info=True;"
            s = s & "Convert Date Time To Char=FALSE;Default Collection={DEFAULT_COLLECTION};Force Translate=0;"
            s = s.Replace("{DATASOURCE}", parametroAplicacion("AS400_SERVER"))
            s = s.Replace("{USERID}", parametroAplicacion("AS400_USER"))
            s = s.Replace("{PASSWORD}", parametroAplicacion("AS400_PASS"))
            s = s.Replace("{DEFAULT_COLLECTION}", parametroAplicacion("AS400_LIB_F"))
            gsConnString = s
        End If
    End Sub

    Function abrirConexionMySql() As String
        Dim gsConnMySql As String = ""
        Dim resultado As String = ""
        Try
            If DBMySQL.State = 0 Then
                'gsConnMySql = "DRIVER=MySQL ODBC 5.3 ANSI Driver;UID={USER};PWD={PASSWORD};SERVER={SERVER};DATABASE={DATABASE};DBQ={ODBC}"
                gsConnMySql = "Server={SERVER};Database={DATABASE};Uid={USER};Pwd={PASSWORD};"
                gsConnMySql = gsConnMySql.Replace("{USER}", paramWebConfig(My.Settings.CONFIG, "MYSQL_USER"))
                gsConnMySql = gsConnMySql.Replace("{PASSWORD}", paramWebConfig(My.Settings.CONFIG, "MYSQL_PASS"))
                gsConnMySql = gsConnMySql.Replace("{SERVER}", paramWebConfig(My.Settings.CONFIG, "MYSQL_SERVER"))
                gsConnMySql = gsConnMySql.Replace("{DATABASE}", paramWebConfig(My.Settings.CONFIG, "MYSQL_LIB"))
                'gsConnMySql = gsConnMySql.Replace("{ODBC}", paramWebConfig(My.Settings.CONFIG, "MYSQL_ODBC"))

                DBMySQL.ConnectionString = gsConnMySql
                DBMySQL.Open()

            End If
        Catch ex As Exception
            Return gsConnMySql & "----" & ex.Message.ToString
        End Try
        Return resultado
    End Function


    Function parametroAplicacion(param)
        Dim fSql As String
        fSql = "SELECT CCDESC FROM BFPARAM WHERE CCCODE = '" & param & "' "
        Return BuscarValor(fSql)
    End Function

    Private Function BuscarValor(Sql As String)
        Dim cm As MySqlCommand = New MySqlCommand(Sql, DBMySQL)
        Using rs As MySqlDataReader = cm.ExecuteReader()
            If rs.Read() Then
                Return rs(0)
            End If
        End Using
        Return ""
    End Function



End Class

