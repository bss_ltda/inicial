﻿Imports Microsoft.AspNet.Membership.OpenAuth
Imports IBM.Data.DB2.iSeries

Public Class Register
    Inherits Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        RegisterUser.ContinueDestinationPageUrl = Request.QueryString("ReturnUrl")
    End Sub

    Protected Sub RegisterUser_CreatedUser(ByVal sender As Object, ByVal e As EventArgs) Handles RegisterUser.CreatedUser
        FormsAuthentication.SetAuthCookie(RegisterUser.UserName, createPersistentCookie:=False)

        Dim continueUrl As String = RegisterUser.ContinueDestinationPageUrl
        If Not OpenAuth.IsLocalUrl(continueUrl) Then
            continueUrl = "~/"
        End If

        Response.Redirect(continueUrl)
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Response.Clear()
        Response.Buffer = True
        Response.Charset = ""
        Response.AppendHeader("Content-Disposition", "attachment;filename=Customers.xls")
        Response.ContentEncoding = System.Text.Encoding.UTF8
        Response.ContentType = "Application/vnd.openxmlformats-officedocument.SpreadsheetML.Sheet"
        Dim oStringWriter As System.IO.StringWriter = New System.IO.StringWriter
        Dim oHtmlTextWriter As System.Web.UI.HtmlTextWriter = New System.Web.UI.HtmlTextWriter(oStringWriter)

        Dim DB As iDB2Connection = New IBM.Data.DB2.iSeries.iDB2Connection
        DB.ConnectionString = "Data Source=192.168.10.1;Default Collection=DESLX834F;Persist Security Info=True;User ID=DESLXSSA;Password=DIC2013"
        DB.Open()



        Dim fSql As String = "SELECT * FROM RFFLTIPVEH"
        Dim cmd As iDB2Command = New IBM.Data.DB2.iSeries.iDB2Command(fSql, DB)

        Dim ds As DataSet = New DataSet
        Dim dt As DataSet = New DataSet
        Dim da As iDB2DataAdapter = New iDB2DataAdapter(cmd)


        'adaptador.SelectCommand = Sql
        da.Fill(dt)
        Me.GridView1.DataSource = dt
        Me.GridView1.DataBind()

        Me.GridView1.RenderControl(oHtmlTextWriter)

        Response.Output.Write(oStringWriter.ToString)
        Response.Flush()
        Response.End()



    End Sub
    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As System.Web.UI.Control)

    End Sub
End Class